﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Emerge.Data
{
    public partial class EmergeDALDataContext
    {
        partial void OnCreated()
        {
            
            this.Connection.ConnectionString = ConfigurationManager.ConnectionStrings["EmergeConnectionString"].ToString();
            this.CommandTimeout = 0;

            //logging entry for glimpse
            //Log = FlexLabs.Glimpse.Linq2Sql.PluginTextWriter.Instance;
            //Connection.StateChange += FlexLabs.Glimpse.Linq2Sql.StateChangeHandler.OnStateChange;
        }
    }
}
