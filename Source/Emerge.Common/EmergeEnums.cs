﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
namespace Emerge.Common
{
    public enum LoginType
    {
        BasicUser = 1,
        BranchManager = 2,
        CorporateManager = 3
    }

    public enum OrderStatus
    {
        Pending = 1,
        Complete = 2
    }


    public enum OrderType
    {
        FromWebsite=1,
        LandingPage=2,
        XMLAPI=3
    }
    public enum ReportStatus
    {
        Pending = 1,
        Complete = 2
    }
    public enum EmailType
    {
        UserRegistration
    }
    public enum EmailTemplates
    {
        NewUserRegistration = 1,
        PendingReports = 5,
        ReportRequest = 6,
        PackageRequest = 7,
        PasswordRecovery = 12,
        EmailedReportRequest = 13,
        EmergeEdit = 15,
        ProductOrdered = 18,
        ReviewOrder = 19,
        HelpWithOrder = 20,
        EmergeReview = 27,
        DemoSchedule = 31,
        SendDocument = 32,
        AccountStatusChange = 33,
        QualityControlSurvey = 34,
        DemoCompleted = 35,
        EmailBillingStatement = 37,
        MonthlyBillingStatement = 38,
        ReportsNotGenerating = 41,
        SyncpodActivation = 42,
        PendingReportUpdate = 45,
        AdverseLetter = 48,
        ReportReminder=23,
        NewOrder = 29,
        IncompleteReport = 30,
        ManualOrderPlaced=36,
        BatchProcessNotification = 40,
        EmergeReviewReport= 43,
        PendingReport=44,
        Notice613ForPS = 63 
       
    }

    public enum ReportCategories
    {
        DrugTesting = 9,
        OtherAdjustmentsReports = 10
    }
}