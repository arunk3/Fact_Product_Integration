﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//using MomentumInfotech.EncryptDecrypt;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.IO;
//using BusinessLogicLayer;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web.Security;
using Emerge.Services;
using System.Security.Cryptography;

namespace Emerge.Common
{
    /// <summary>
    /// Summary description for Utility
    /// </summary>
    public static class Utility
    {
        /// <summary>
        /// This variable used in 4 reports PS3, EMV2, TDR2 and PL2 for Authorized Purposes.
        /// 1. PS3 report having 27 values.
        /// 2. TDR2 report having 8 values.  
        /// 3. EMV2 report having 9 values.
        /// <value>6 Fraud Prevention or Detection</value>
        /// </summary>
        public static int AuthorizedPurposes = 6;
        public static void Logout(string UserName)
        {
            MembershipUser ObjMembershipUser = Membership.GetUser(UserName);
            System.Web.Security.FormsAuthentication.SignOut();

            if (ObjMembershipUser != null)
            {
                BLLMembership ObjBLLMembership = new BLLMembership();
                ObjBLLMembership.SignOut(ObjMembershipUser.UserName, DateTime.Now.ToUniversalTime().AddMinutes(-(Membership.UserIsOnlineTimeWindow)));
            }

            HttpContext.Current.Response.Redirect(FormsAuthentication.LoginUrl);

        }

        public static string BillingId
        {
            get
            {
                return ConfigurationManager.AppSettings["BillingId"] != null ? ConfigurationManager.AppSettings["BillingId"] : string.Empty;
            }
        }

        public static string BillingPhone
        {
            get
            {
                return ConfigurationManager.AppSettings["BillingPhone"] != null ? ConfigurationManager.AppSettings["BillingPhone"] : string.Empty;
            }
        }

        /// <summary>
        /// This variable used for PS3 Search Type.   
        /// <value>25 – LTA</value>
        /// </summary>
        public static string SearchType = "25 – LTA";

       // private static string _sCookiesU = "vewgjkgkmdfsdhhgdferyutsfejhwfjhekjfjkefkj";
        private static string _sCookiesU = "UserCookie";

        private static string _sCookieUserName = "fjhefvk";

        private static string _sCookieUserPassword = "kejgfj9";

        private static string _sCookieUserRole = "kepqrj9";

        public static int PageSize = 5;

        public static void SetCookie(string UserName, string Password, string UserRole, bool IsCreate)
        {
            string cookietemp = _sCookiesU;

            HttpCookie cookie = HttpContext.Current.Request.Cookies[cookietemp];

            #region //Create Cookie
            if (IsCreate)
            {
                if (cookie == null)
                {
                    cookie = new HttpCookie(cookietemp);
                }
                try
                {
                    cookie[_sCookieUserName] = EncryptDecrypt.Encrypt(UserName);
                    cookie[_sCookieUserPassword] = EncryptDecrypt.Encrypt(Password);
                    cookie[_sCookieUserRole] = EncryptDecrypt.Encrypt(UserRole);
                    //cookie[_sCookieUserName] =UserName;
                    //cookie[_sCookieUserPassword] = MD5Hash(Password);
                    //cookie[_sCookieUserRole] = UserRole;
                    cookie.Expires = DateTime.Now.AddDays(10);
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
                catch
                {
                    throw;
                }

                finally
                {
                    cookie = null;
                }
            }
            #endregion

            #region //Remove Cookie
            if (cookie != null)
            {
                HttpContext.Current.Response.Cookies[cookietemp].Expires = DateTime.Now.AddYears(-1);
            }
            #endregion
        }

        public static void GetCookie(out string UserName, out string Password, out string UserRole, out bool IsFound)
        {
            UserName = Password = UserRole = "";
            IsFound = false;
            string cookieTemp = _sCookiesU;

            HttpCookie cookie = HttpContext.Current.Request.Cookies[cookieTemp];
            if (cookie != null)
            {
                if (cookie.Values.Get(_sCookieUserName) != null && cookie.Values.Get(_sCookieUserPassword) != null && cookie.Values.Get(_sCookieUserRole) != null)
                {
                    UserName = EncryptDecrypt.Decrypt(cookie[_sCookieUserName]);
                    Password = EncryptDecrypt.Decrypt(cookie[_sCookieUserPassword]);
                    UserRole = EncryptDecrypt.Decrypt(cookie[_sCookieUserRole]);
                    //UserName = cookie[_sCookieUserName];
                    //Password = cookie[_sCookieUserPassword];
                    //UserRole = cookie[_sCookieUserRole];
                    IsFound = true;
                }
            }
        }

        public static string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            //compute hash from the bytes of text
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

            //get hash result after compute it
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits
                //for each byte
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }

        public static void SetCache(this Page page)
        {
            TimeSpan freshness = new TimeSpan(0, 0, 5, 0);
            DateTime now = DateTime.Now;
            page.Response.Cache.SetExpires(now.Add(freshness));
            page.Response.Cache.SetMaxAge(freshness);
            page.Response.Cache.SetCacheability(HttpCacheability.Private);
            page.Response.Cache.SetValidUntilExpires(true);
        }

        /// <summary>
        /// Clear all textboxes
        /// </summary>
        /// <param name="container"></param>
        public static void ClearTextboxes(this Control container)
        {
            foreach (Control ctl in container.Controls)
            {
                var textBox = ctl as TextBox;
                if (textBox != null)
                {
                    textBox.Text = String.Empty;
                }
                var chkBox = ctl as CheckBox;
                if (chkBox != null)
                {
                    chkBox.Checked = false;
                }
                if (ctl.Controls.Count > 0)
                {
                    ClearTextboxes(ctl);
                }
            }
        }

        /// <summary>
        /// Convert date format.
        /// </summary>
        /// <param name="DateString"></param>
        /// <returns></returns>
        public static string ConvertDateString(object DateString)
        {
            return Convert.ToDateTime(DateString).ToShortDateString();
        }

        /// <summary>
        /// Check valid GUID format
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static bool IsGuid(this string guid)
        {
            Regex isGuid = new Regex(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", RegexOptions.Compiled);
            bool isValid = false;
            if (!guid.Equals(new Guid("00000000-0000-0000-0000-000000000000")))
            {
                if (isGuid.IsMatch(guid))
                {
                    isValid = true;
                }
            }
            return isValid;
        }

        public static string GetPhysicalPath(this Page page, string fileName)
        {
            string Filepath = page.Server.MapPath(fileName);
            if (File.Exists(Filepath))
            {
                return Filepath;
            }
            return "";
        }

        /// <summary>
        /// This method is used to generate page numbers
        /// </summary>
        /// <param name="TotalRecords"></param>
        /// <param name="ObjDdl"></param>
        /// <param name="CurrentValue"></param>
        public static void GeneratePageNumbers(int TotalRecords, DropDownList ObjDdl, int PageSize, string CurrentValue)
        {
            if (TotalRecords == 0 || PageSize == 0) { return; }
            int iTotalPages = (TotalRecords % PageSize > 0) ? ((TotalRecords / PageSize) + 1) : (TotalRecords / PageSize);

            ObjDdl.Items.Clear();
            for (int iRow = 1; iRow <= iTotalPages; iRow++)
            {
                ObjDdl.Items.Add(new ListItem(iRow.ToString(), iRow.ToString()));
            }

            ListItem LI = ObjDdl.Items.FindByValue(CurrentValue);
            if (LI != null)
            {
                ObjDdl.SelectedValue = CurrentValue;
            }
        }

        public static string GetProductXMLFilePath()
        {
            return @"~\Content\ProductXml\ProductRequireData.xml";
        }
        public static string GetProductBatchXMLFilePath()
        {
            return @"~\Content\ProductXml\ProductRequireDataForBatch.xml";
        }
        /// <summary>
        /// Function to Generate Company Code
        /// </summary>
        /// <param name="CompnayName"></param>
        /// <returns></returns>
        public static string GenerateCompanyCode(string CompanyName, DateTime DateString)
        {

            return CompanyName.Substring(0, 2).ToUpper() + DateString.ToString("yyMMddHHmmss");

        }

        /// <summary>
        /// Function to Generate Location Code
        /// </summary>
        /// <param name="CompnayName"></param>
        /// <returns></returns>
        public static string GenerateLocationCode(string CompanyName, DateTime DateString)
        {
            if (string.IsNullOrEmpty(CompanyName))
            {
                CompanyName = "Intelifi";
            }
            return CompanyName.Substring(0, 2).ToUpper() + DateString.ToString("yyMMddHHmmssmmss");
        }

        /// <summary>
        /// Function To Get UID of Particular User
        /// </summary>
        /// <param name="CompanyName"></param>
        /// <param name="DateString"></param>
        /// <returns></returns>
        public static string GenerateUserCode(string CompanyName, DateTime DateString)
        {

            if (string.IsNullOrEmpty(CompanyName))
            {
                CompanyName = "Intelifi";
            }
            return CompanyName.Substring(0, 2).ToUpper() + DateString.ToString("yyHHmmssMMdd"); ;
        }

        /// <summary>
        /// Function to Bind DropDownList When no Record is there in the Database
        /// </summary>
        /// <param name="ObjDropDownList"></param>
        /// <param name="message"></param>
        public static void BindDropDownWithEmptyValue(DropDownList ObjDropDownList, string message)
        {
            ObjDropDownList.Items.Insert(0, new ListItem("No " + message + " Found", "-1"));
        }

        public static Guid SiteApplicationId = new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852");

        public static string SiteApplicationName = "emerge";

        //public static void Logout(string UserName)
        //{
        //    MembershipUser ObjMembershipUser = Membership.GetUser(UserName);
        //    System.Web.Security.FormsAuthentication.SignOut();

        //    if (ObjMembershipUser != null)
        //    {
        //        BLLMembership ObjBLLMembership = new BLLMembership();
        //        ObjBLLMembership.SignOut(ObjMembershipUser.UserName, DateTime.Now.ToUniversalTime().AddMinutes(-(Membership.UserIsOnlineTimeWindow)));
        //    }

        //    HttpContext.Current.Response.Redirect(FormsAuthentication.LoginUrl);

        //}

        /// <summary>
        /// Function To Send Request To Vendor Thru HttpRequest Object
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public static string SendRequest(string uri, string content)
        {

            if (uri == "")
            {
                return "";
            }


            string Response = "";
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(uri);
            request.Timeout = 65000;
            request.ProtocolVersion = HttpVersion.Version11;
            request.Method = WebRequestMethods.Http.Post;
            request.ContentLength = content.Length;
            request.ContentType = "application/x-www-form-urlencoded";
            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(content);
            writer.Close();
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            Response = reader.ReadToEnd();
            response.Close();

            return Response;
        }

        public static void DeleteFile(string FilePath)
        {
            try
            {
                if (File.Exists(FilePath))
                {
                    File.Delete(FilePath);
                }
            }
            catch (Exception)
            {


            }
        }

        public static string ResolveWebUrl(string relativeUrl)
        {
            if (relativeUrl == null) throw new ArgumentNullException("relativeUrl");

            if (relativeUrl.Length == 0 || relativeUrl[0] == '/' || relativeUrl[0] == '\\')
                return relativeUrl;

            int idxOfScheme = relativeUrl.IndexOf(@"://", StringComparison.Ordinal);
            if (idxOfScheme != -1)
            {
                int idxOfQM = relativeUrl.IndexOf('?');
                if (idxOfQM == -1 || idxOfQM > idxOfScheme) return relativeUrl;
            }

            StringBuilder sbUrl = new StringBuilder();
            sbUrl.Append(HttpRuntime.AppDomainAppVirtualPath);
            if (sbUrl.Length == 0 || sbUrl[sbUrl.Length - 1] != '/') sbUrl.Append('/');

            // found question mark already? query string, do not touch!
            bool foundQM = false;
            bool foundSlash; // the latest char was a slash?
            if (relativeUrl.Length > 1
                && relativeUrl[0] == '~'
                && (relativeUrl[1] == '/' || relativeUrl[1] == '\\'))
            {
                relativeUrl = relativeUrl.Substring(2);
                foundSlash = true;
            }
            else foundSlash = false;
            foreach (char c in relativeUrl)
            {
                if (!foundQM)
                {
                    if (c == '?') foundQM = true;
                    else
                    {
                        if (c == '/' || c == '\\')
                        {
                            if (foundSlash) continue;
                            else
                            {
                                sbUrl.Append('/');
                                foundSlash = true;
                                continue;
                            }
                        }
                        else if (foundSlash) foundSlash = false;
                    }
                }
                sbUrl.Append(c);
            }

            return sbUrl.ToString();
        }

        //public static bool SendMail(string mailTo, string CC, string mailFrom, string subject, string MessageBody)
        //{
        //    bool mailSent = false;
        //    try
        //    {
        //        MomentumInfotech.EmailProvider.MailProvider ObjMailProvider = new MomentumInfotech.EmailProvider.MailProvider();

        //        ObjMailProvider.IsBodyHtml = true;


        //        string FromAddress = "";
        //        SmtpClient SmtpClient = new SmtpClient();
        //        System.Net.Mail.MailMessage message = new MailMessage();
        //        message.IsBodyHtml = true;
        //        message.Priority = MailPriority.High;
        //        message.Subject = subject;

        //        message.Body = MessageBody;
        //        SmtpClient.Host =ConfigurationManager.AppSettings["SmtpHostAddress"].ToString();
        //        SmtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPortNo"]);

        //        if (CC != "")
        //        {
        //            message.CC.Add(CC);
        //        }


        //        if (mailFrom == "")
        //        {
        //            FromAddress = ConfigurationManager.AppSettings["FromAddress"].ToString();
        //        }
        //        else
        //        {
        //            FromAddress = mailFrom;
        //        }
        //        message.From = new MailAddress(FromAddress.ToString());
        //        message.To.Add(new MailAddress(mailTo));

        //        SmtpClient.Send(message);            
        //        message.To.Clear();
        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return mailSent;
        //    }

        //}

        public static void DownloadFile(string FilePath)
        {
            if (System.IO.File.Exists(FilePath))
            {
                try
                {
                    string fileext = System.IO.Path.GetExtension(FilePath);
                    string FileName = System.IO.Path.GetFileName(FilePath);
                    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + FileName);
                    HttpContext.Current.Response.TransmitFile(FilePath);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    HttpContext.Current.Response.End();
                    HttpContext.Current.Response.Close();
                }
            }
        }

        /// <summary>
        /// Function to Fetch the Last Request 
        /// </summary>
        /// <param name="Requests"></param>
        /// <returns></returns>
        public static string GetLastRequest(string Requests)
        {
            string Vendor_Request = "";
            string[] String_Separator = new string[] { "$#$#" };

            if (Requests.Contains("$#$#"))
            {
                string[] Request_array = Requests.Split(String_Separator, StringSplitOptions.RemoveEmptyEntries);
                Vendor_Request = Request_array.Last();
            }
            else
            {
                Vendor_Request = Requests;
            }
            return Vendor_Request;
        }

        /// <summary>
        /// Function to Send an email
        /// </summary>
        /// <param name="TO"></param>
        /// <param name="FROM"></param>
        /// <param name="MESSAGE"></param>
        /// <param name="SUBJECT"></param>
        /// <returns></returns>
        public static bool SendMail(string TO, string FROM, string MESSAGE, string SUBJECT)
        {
            int SendingStatus = 0;

            MailProvider ObjMailProvider = new MailProvider();

            List<string> BCC_List = new List<string>();
            BCC_List.Add(ConfigurationManager.AppSettings["BccAdmin"].ToString());

            ObjMailProvider.Message = MESSAGE;
            ObjMailProvider.MailTo.Add(TO);
            ObjMailProvider.MailFrom = FROM;
            ObjMailProvider.MailBCC = BCC_List;
            ObjMailProvider.Subject = SUBJECT;
            ObjMailProvider.IsBodyHtml = true;
            ObjMailProvider.Priority = MailPriority.High;
            ObjMailProvider.SendMail(out SendingStatus);

            return (SendingStatus == 1 ? true : false);
        }
        /// <summary>
        /// send mail to users for ticket #101
        /// </summary>
        /// <param name="TO"></param>
        /// <param name="CC"></param>
        /// <param name="FROM"></param>
        /// <param name="MESSAGE"></param>
        /// <param name="SUBJECT"></param>
        /// <returns></returns>

        public static bool SendMail(string TO, string CC, string FROM, string MESSAGE, string SUBJECT)
        {
            int SendingStatus = 0;

            MailProvider ObjMailProvider = new MailProvider();

            List<string> BCC_List = new List<string>();
            List<string> CC_List = new List<string>();
            if (!string.IsNullOrEmpty(CC))
            {
                CC_List.Add(CC);
            }
            BCC_List.Add(ConfigurationManager.AppSettings["BccAdmin"].ToString());

            ObjMailProvider.Message = MESSAGE;
            ObjMailProvider.MailTo.Add(TO);
            ObjMailProvider.MailFrom = FROM;
            ObjMailProvider.MailBCC = BCC_List;
            ObjMailProvider.MailCC = CC_List;
            ObjMailProvider.Subject = SUBJECT;
            ObjMailProvider.IsBodyHtml = true;
            ObjMailProvider.Priority = MailPriority.High;
            ObjMailProvider.SendMail(out SendingStatus);

            return (SendingStatus == 1 ? true : false);
        }

        public static bool SendMail(List<string> toList, string FROM, string MESSAGE, string SUBJECT)
        {
            int SendingStatus = 0;

            MailProvider ObjMailProvider = new MailProvider();

            List<string> BCC_List = new List<string>();
            BCC_List.Add(ConfigurationManager.AppSettings["BccAdmin"].ToString());

            ObjMailProvider.Message = MESSAGE;
            ObjMailProvider.MailTo = toList;
            ObjMailProvider.MailFrom = FROM;
            ObjMailProvider.MailBCC = BCC_List;
            ObjMailProvider.Subject = SUBJECT;
            ObjMailProvider.IsBodyHtml = true;
            ObjMailProvider.Priority = MailPriority.High;
            ObjMailProvider.SendMail(out SendingStatus);

            return (SendingStatus == 1 ? true : false);
        }

        /// <summary>
        /// Function to Send an email With CC
        /// </summary>
        /// <param name="TO"></param>
        /// <param name="CC"></param>
        /// <param name="BCC"></param>
        /// <param name="FROM"></param>
        /// <param name="MESSAGE"></param>
        /// <param name="SUBJECT"></param>
        /// <returns></returns>
        public static bool SendMail(string TO, string CC, string BCC, string FROM, string MESSAGE, string SUBJECT)
        {
            int SendingStatus = 0;

            MailProvider ObjMailProvider = new MailProvider();

            ObjMailProvider.Message = MESSAGE;
            ObjMailProvider.MailTo.Add(TO);

            if (CC != "")
            {
                string[] CCInfo = CC.Split(',');
                ObjMailProvider.MailCC = CCInfo.ToList<string>();
            }

            if (BCC != "")
            {
                string[] BCCInfo = BCC.Split(',');
                ObjMailProvider.MailBCC = BCCInfo.ToList<string>();
            }


            /*
            if (!BCC.Contains(ConfigurationManager.AppSettings["BccAdmin"].ToString()))
            {
                ObjMailProvider.MailBCC.Add(ConfigurationManager.AppSettings["BccAdmin"].ToString());
            }*/

            ObjMailProvider.MailFrom = FROM;
            ObjMailProvider.Subject = SUBJECT;
            ObjMailProvider.IsBodyHtml = true;
            ObjMailProvider.Priority = MailPriority.High;
            ObjMailProvider.SendMail(out SendingStatus);

            return (SendingStatus == 1 ? true : false);
        }

        /// <summary>
        /// Function to Send an email With ATTACHMENT
        /// </summary>
        /// <param name="TO"></param>
        /// <param name="CC"></param>
        /// <param name="BCC"></param>
        /// <param name="FROM"></param>
        /// <param name="MESSAGE"></param>
        /// <param name="SUBJECT"></param>
        /// <param name="ATTACHMENT"></param>
        /// <returns></returns>
        public static bool SendMail(string TO, string CC, string BCC, string FROM, string MESSAGE, string SUBJECT, Attachment ATTACHMENT)
        {
            int SendingStatus = 0;

            MailProvider ObjMailProvider = new MailProvider();

            ObjMailProvider.Message = MESSAGE;
            ObjMailProvider.MailTo.Add(TO);

            if (CC != "")
            {
                string[] CCInfo = CC.Split(',');
                ObjMailProvider.MailCC = CCInfo.ToList<string>();
            }

            if (BCC != "")
            {
                string[] BCCInfo = BCC.Split(',');
                ObjMailProvider.MailBCC = BCCInfo.ToList<string>();
            }

            if (!BCC.Contains(ConfigurationManager.AppSettings["BccAdmin"].ToString()))
            {
                ObjMailProvider.MailBCC.Add(ConfigurationManager.AppSettings["BccAdmin"].ToString());
            }

            ObjMailProvider.MailAttachments.Add(ATTACHMENT);


            ObjMailProvider.MailFrom = FROM;
            ObjMailProvider.Subject = SUBJECT;
            ObjMailProvider.IsBodyHtml = true;
            ObjMailProvider.Priority = MailPriority.High;
            ObjMailProvider.SendMail(out SendingStatus);

            return (SendingStatus == 1 ? true : false);
        }

        /// <summary>
        /// Function to Send an email With multiple ATTACHMENTs
        /// </summary>
        /// <param name="TO"></param>
        /// <param name="CC"></param>
        /// <param name="BCC"></param>
        /// <param name="FROM"></param>
        /// <param name="MESSAGE"></param>
        /// <param name="SUBJECT"></param>
        /// <param name="ATTACHMENT"></param>
        /// <returns></returns>
        public static bool SendMail(string TO, string CC, string BCC, string FROM, string MESSAGE, string SUBJECT, List<Attachment> ATTACHMENT)
        {
            int SendingStatus = 0;

            MailProvider ObjMailProvider = new MailProvider();

            ObjMailProvider.Message = MESSAGE;
            ObjMailProvider.MailTo.Add(TO);

            if (CC != "")
            {
                string[] CCInfo = CC.Split(',');
                ObjMailProvider.MailCC = CCInfo.ToList<string>();
            }

            if (BCC != "")
            {
                string[] BCCInfo = BCC.Split(',');
                ObjMailProvider.MailBCC = BCCInfo.ToList<string>();
            }

            if (!BCC.Contains(ConfigurationManager.AppSettings["BccAdmin"].ToString()))
            {
                ObjMailProvider.MailBCC.Add(ConfigurationManager.AppSettings["BccAdmin"].ToString());
            }
            for (int i = 0; i < ATTACHMENT.Count; i++)
            {
                ObjMailProvider.MailAttachments.Add(ATTACHMENT.ElementAt(i));
            }


            ObjMailProvider.MailFrom = FROM;
            ObjMailProvider.Subject = SUBJECT;
            ObjMailProvider.IsBodyHtml = true;
            ObjMailProvider.Priority = MailPriority.High;
            ObjMailProvider.SendMail(out SendingStatus);

            return (SendingStatus == 1 ? true : false);
        }


        /// <summary>
        /// Function To Get The Content of the  Sample  Report .
        /// </summary>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public static string GetSampleReportContent(string FileName)
        {
            if (File.Exists(HttpContext.Current.Server.MapPath("~/Resources/SampleReports/" + FileName + ".html")))
            {
                return File.ReadAllText(HttpContext.Current.Server.MapPath("~/Resources/SampleReports/" + FileName + ".html"), Encoding.Default);
            }
            else
            {
                return "";
            }
        }

        public static string GetTextWithLength(string str, int Length)
        {
            string sContent = string.Empty;
            sContent = str;
            if (sContent.Length > Length)
            {
                sContent = sContent.Substring(0, Length);
            }
            return sContent;
        }

        public static Guid GetUID(string Username)
        {
            Guid UserId = Guid.Empty;
            try
            {
                MembershipUser Obj = Membership.GetUser(Username);
                if (Obj != null)
                {
                    UserId = new Guid(Obj.ProviderUserKey.ToString());
                }
            }
            catch { }
            return UserId;
        }

        public static string GeneraterRandom()
        {
            Random ObjRandom = new Random();
            string strChars = "Q,E,W,R,T,Y,U,I,O,P,A,S,D,F,G,H,J,K,L,Z,X,C,V,B,N,M";
            string strNums = "8,6,3,1,9,2,5,4,7,0";
            string[] arrNumSplit = strNums.Split(',');
            string[] arrCharSplit = strChars.Split(',');
            string strRandom = "";


            for (int i = 0; i < 1; i++)
            {
                int iRandom = ObjRandom.Next(0, arrCharSplit.Length - 1);
                strRandom += arrCharSplit[iRandom].ToString();
            }
            for (int i = 0; i < 5; i++)
            {
                int iRandom = ObjRandom.Next(0, arrNumSplit.Length - 1);
                strRandom += arrNumSplit[iRandom].ToString();
            }

            return strRandom;
        }
        public static string RandomStr()
        {
            string rStr = Path.GetRandomFileName();
            rStr = rStr.Replace(".", "");
            return rStr;
        }
        public static object setval4null(object myObject)
        {
            #region Null Properties convert to String.empty
            object anv = new object();
            foreach (var propertyInfo in myObject.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(myObject, null) == null)
                    {
                        propertyInfo.SetValue(myObject, string.Empty, null);
                    }
                }
            }
            anv = myObject;
            return anv;
            #endregion
        }
        public static void setval4nullAnother(object myObject)
        {
            #region Null Properties convert to String.empty

            foreach (var propertyInfo in myObject.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(myObject, null) == null)
                    {
                        propertyInfo.SetValue(myObject, string.Empty, null);
                    }
                }
            }

            #endregion
        }
        public static object Cell2Property(object myObject, List<string> ProertyColl, string PropertyValue)
        {
            #region Null Properties convert to String.empty
            object anv = new object();
            foreach (var propertyInfo in myObject.GetType().GetProperties())
            {
                if (ProertyColl.Any(d => d.Contains(propertyInfo.Name)))
                {
                    object prop = propertyInfo.GetValue(myObject, null);
                    propertyInfo.SetValue(prop, PropertyValue, null);
                    //if (propertyInfo.GetValue(myObject, null) == null)
                    //{
                    //    propertyInfo.SetValue(myObject, DateTime.Now.ToFileTime().ToString(), null);
                    //}
                }
            }
            anv = myObject;
            return anv;
            #endregion
        }


    }
}