﻿using System;
using System.Web;
using System.Web.Profile;


namespace Emerge.Common
{
    public class ProfileGroupSearchParams : System.Web.Profile.ProfileGroupBase
    {

        public virtual string User
        {
            get
            {
                return ((string)(this.GetPropertyValue("User")));
            }
            set
            {
                this.SetPropertyValue("User", value);
            }
        }

        public virtual string SalesRep
        {
            get
            {
                return ((string)(this.GetPropertyValue("SalesRep")));
            }
            set
            {
                this.SetPropertyValue("SalesRep", value);
            }
        }

        public virtual string SortBy
        {
            get
            {
                return ((string)(this.GetPropertyValue("SortBy")));
            }
            set
            {
                this.SetPropertyValue("SortBy", value);
            }
        }

        public virtual string DefaultPage
        {
            get
            {
                return ((string)(this.GetPropertyValue("DefaultPage")));
            }
            set
            {
                this.SetPropertyValue("DefaultPage", value);
            }
        }

        public virtual string DataSource
        {
            get
            {
                return ((string)(this.GetPropertyValue("DataSource")));
            }
            set
            {
                this.SetPropertyValue("DataSource", value);
            }
        }

        public virtual string CallLogCompany
        {
            get
            {
                return ((string)(this.GetPropertyValue("CallLogCompany")));
            }
            set
            {
                this.SetPropertyValue("CallLogCompany", value);
            }
        }

        public virtual string SortOrder
        {
            get
            {
                return ((string)(this.GetPropertyValue("SortOrder")));
            }
            set
            {
                this.SetPropertyValue("SortOrder", value);
            }
        }

        public virtual string SalesManager
        {
            get
            {
                return ((string)(this.GetPropertyValue("SalesManager")));
            }
            set
            {
                this.SetPropertyValue("SalesManager", value);
            }
        }

        public virtual string Location
        {
            get
            {
                return ((string)(this.GetPropertyValue("Location")));
            }
            set
            {
                this.SetPropertyValue("Location", value);
            }
        }

        public virtual string SortColumn
        {
            get
            {
                return ((string)(this.GetPropertyValue("SortColumn")));
            }
            set
            {
                this.SetPropertyValue("SortColumn", value);
            }
        }

        public virtual string Company
        {
            get
            {
                return ((string)(this.GetPropertyValue("Company")));
            }
            set
            {
                this.SetPropertyValue("Company", value);
            }
        }

        public virtual string IsDataEntryOnly_Enable
        {
            get
            {
                return ((string)(this.GetPropertyValue("IsDataEntryOnly_Enable")));
            }
            set
            {
                this.SetPropertyValue("IsDataEntryOnly_Enable", value);
            }
        }

        public virtual string IsReportNotGenerating
        {
            get
            {
                return ((string)(this.GetPropertyValue("IsReportNotGenerating")));
            }
            set
            {
                this.SetPropertyValue("IsReportNotGenerating", value);
            }
        }

        public virtual string PageNo
        {
            get
            {
                return ((string)(this.GetPropertyValue("PageNo")));
            }
            set
            {
                this.SetPropertyValue("PageNo", value);
            }
        }

        public virtual string OrderStatus
        {
            get
            {
                return ((string)(this.GetPropertyValue("OrderStatus")));
            }
            set
            {
                this.SetPropertyValue("OrderStatus", value);
            }
        }

        public virtual string SearchedText
        {
            get
            {
                return ((string)(this.GetPropertyValue("SearchedText")));
            }
            set
            {
                this.SetPropertyValue("SearchedText", value);
            }
        }
    }

    public class ProfileCommon : System.Web.Profile.ProfileBase
    {

        public virtual string RecordSize
        {
            get
            {
                return ((string)(this.GetPropertyValue("RecordSize")));
            }
            set
            {
                this.SetPropertyValue("RecordSize", value);
            }
        }

        public virtual ProfileGroupSearchParams SearchParams
        {
            get
            {
                return ((this.GetProfileGroup("SearchParams"))) as ProfileGroupSearchParams;
            }
        }

        public ProfileModel GetProfile(string username)
        {
            ProfileBase _userProfile = ProfileBase.Create(username);
            ProfileModel _profile = new ProfileModel();
            if (_userProfile.LastUpdatedDate > DateTime.MinValue)
            {
                _profile.IsReportNotGenerating = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("IsReportNotGenerating"));
                _profile.DefaultPage = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("DefaultPage"));
                _profile.OrderStatus = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("OrderStatus"));
                _profile.DataSource = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("DataSource"));
                _profile.Company = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("Company"));
                _profile.Location = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("Location"));
                _profile.User = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("User"));
                _profile.SortColumn = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("SortColumn"));
                _profile.SortOrder = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("SortOrder"));
                _profile.PageNo = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("PageNo"));
                _profile.SalesManager = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("SalesManager"));
                _profile.SalesRep = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("SalesRep"));
                _profile.SortBy = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("SortBy"));
                _profile.CallLogCompany = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("CallLogCompany"));
                _profile.IsDataEntryOnly_Enable = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("IsDataEntryOnly_Enable"));
                _profile.RecordSize = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("RecordSize"));
                _profile.SearchedText = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("SearchedText"));

                _profile.LeadSearchedText = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("LeadSearchedText"));
                _profile.LeadStatus = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("LeadStatus"));
                _profile.LeadSource = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("LeadSource"));
                _profile.LeadCompanyType = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("LeadCompanyType"));
                _profile.LeadSalesManager = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("LeadSalesManager"));
                _profile.LeadSortColumn = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("LeadSortColumn"));
                _profile.LeadSortOrder = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("LeadSortOrder"));
                _profile.LeadPageNo = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("LeadPageNo"));
                _profile.LeadPageSize = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("LeadPageSize"));
                _profile.LeadToDate = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("LeadToDate"));
                _profile.LeadFromDate = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("LeadFromDate"));
                _profile.LeadSalesRep = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("LeadSalesRep"));
                _profile.LeadCurrentUser = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("LeadCurrentUser"));
                _profile.LeadType = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("LeadType"));
                _profile.LeadDateList = Convert.ToString(_userProfile.GetProfileGroup("SearchParams").GetPropertyValue("LeadDateList"));
            }
            // return ((ProfileCommon)(ProfileBase.Create(username)));
            return _profile;
        }
        public void EditDefaultPageOfProfile(string username, string DefaultPage, string IsReportNotGenerating)
        {
            ProfileBase _userProfile = ProfileBase.Create(username);
            if (_userProfile != null)
            {
                if (!string.IsNullOrEmpty(DefaultPage))
                {
                    _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("DefaultPage", DefaultPage);
                }
                if (!string.IsNullOrEmpty(IsReportNotGenerating))
                {
                    _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("IsReportNotGenerating", IsReportNotGenerating);
                }
                _userProfile.Save();
            }
        }

        public void SavePreferencesPageProfile(string username, string DefaultPage, string SortColumn, string RecordSize, string IsDataEntryOnly)
        {
            ProfileBase _userProfile = ProfileBase.Create(username);
            if (_userProfile != null)
            {
                if (!string.IsNullOrEmpty(DefaultPage))
                {
                    _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("DefaultPage", DefaultPage);
                }
                if (!string.IsNullOrEmpty(SortColumn))
                {
                    _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("SortColumn", SortColumn);
                }
                if (!string.IsNullOrEmpty(RecordSize))
                {
                    _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("RecordSize", RecordSize);
                }
                if (!string.IsNullOrEmpty(IsDataEntryOnly))
                {
                    _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("IsDataEntryOnly_Enable", IsDataEntryOnly);
                }
                _userProfile.Save();
            }
        }

        public void SetCallLogCompanyProfile(string username, string CallLogCompany)
        {
            ProfileBase _userProfile = ProfileBase.Create(username);
            if (_userProfile != null)
            {
                _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("CallLogCompany", CallLogCompany);
                _userProfile.Save();
            }
        }
        public void SetReportNotGeneratingProfile(string username, string ReportNotGenerating)
        {
            ProfileBase _userProfile = ProfileBase.Create(username);
            if (_userProfile != null)
            {
                _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("IsReportNotGenerating", ReportNotGenerating);
                _userProfile.Save();
            }
        }

        public void SetGeneralProfile(string username, string ProflieType, string ProfileValue)
        {
            ProfileBase _userProfile = ProfileBase.Create(username);
            if (_userProfile != null)
            {
                _userProfile.GetProfileGroup("SearchParams").SetPropertyValue(ProflieType, ProfileValue);
                _userProfile.Save();
            }
        }


        public void SaveUserProfile(string User)
        {
            ProfileBase _userProfile = ProfileBase.Create(User);
            _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("SearchedText", string.Empty);
            _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("OrderStatus", string.Empty);
            _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("DataSource", string.Empty);
            _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("Location", string.Empty);
            _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("User", string.Empty);
            _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("SortColumn", string.Empty);
            _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("SortOrder", string.Empty);
            _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("PageNo", string.Empty);
            _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("CallLogCompany", string.Empty);

            _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("LeadSearchedText", string.Empty);
            _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("LeadStatus", string.Empty);
            _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("LeadSource", string.Empty);
            _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("LeadCompanyType", string.Empty);
            _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("LeadSalesManager", string.Empty);
            _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("LeadSortColumn", string.Empty);
            _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("LeadSortOrder", string.Empty);
            _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("LeadPageNo", "1");
            _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("LeadPageSize", "25");
            _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("LeadToDate", string.Empty);
            _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("LeadFromDate", string.Empty);
            _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("LeadSalesRep", string.Empty);
            _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("LeadCurrentUser", string.Empty);
            _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("LeadType", string.Empty);
            _userProfile.GetProfileGroup("SearchParams").SetPropertyValue("LeadDateList", string.Empty);


            _userProfile.Save();
        }
    }
    public class ProfileModel
    {
        public string DefaultPage { get; set; }
        public string IsReportNotGenerating { get; set; }
        public string SearchedText { get; set; }
        public string OrderStatus { get; set; }
        public string DataSource { get; set; }
        public string Company { get; set; }
        public string Location { get; set; }
        public string User { get; set; }
        public string SortColumn { get; set; }
        public string SortOrder { get; set; }
        public string PageNo { get; set; }
        public string SalesManager { get; set; }
        public string SalesRep { get; set; }
        public string SortBy { get; set; }
        public string CallLogCompany { get; set; }
        public string IsDataEntryOnly_Enable { get; set; }
        public string RecordSize { get; set; }

        public string LeadSearchedText { get; set; }
        public string LeadStatus { get; set; }
        public string LeadSource { get; set; }
        public string LeadCompanyType { get; set; }
        public string LeadSalesManager { get; set; }
        public string LeadSortColumn { get; set; }
        public string LeadSortOrder { get; set; }
        public string LeadPageNo { get; set; }
        public string LeadPageSize { get; set; }
        public string LeadToDate { get; set; }
        public string LeadFromDate { get; set; }
        public string LeadSalesRep { get; set; }
        public string LeadCurrentUser { get; set; }
        public string LeadType { get; set; }
        public string LeadDateList { get; set; }
    }
}