﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Configuration;

namespace Emerge.Common
{
    public class ErrorReportingService
    {
        public static void ReportError(Exception Error, string PageUrl)
        {
            #region //Build Error Message
            System.Text.StringBuilder sb = new StringBuilder();
            sb.Append("<table><tr><td>");
            sb.Append("<span style='Color:Red'>Error Caught in Application_Error event</span></td></tr>");
            sb.Append("<tr><td><span style='Color:Red'>Time of error:</span> " + DateTime.Now.ToString() + "</td></tr>");

            try
            {
                if (System.Web.HttpContext.Current.User.Identity.Name != null)
                    sb.Append("<tr><td><span style='Color:Red'>User ID:</span> " + System.Web.HttpContext.Current.User.Identity.Name + "</td></tr>");
            }
            catch { }

            sb.Append("<tr><td><span style='Color:Red'>Error in:</span> " + PageUrl + "</td></tr>");
            sb.Append("<tr><td><span style='Color:Red'>Url Referrer :</span> " + HttpContext.Current.Request.UrlReferrer + "</td></tr>");
            sb.Append("<tr><td><span style='Color:Red'>Request Type :</span> " + HttpContext.Current.Request.RequestType + "</td></tr>");
            sb.Append("<tr><td><span style='Color:Red'>DSN :</span> " + HttpContext.Current.Request.UserHostName + "</td></tr>");
            sb.Append("<tr><td><span style='Color:Red'>IP :</span> " + HttpContext.Current.Request.UserHostAddress + "</td></tr>");
            sb.Append("<tr><td><span style='Color:Red'>Browser Agent:</span> " + HttpContext.Current.Request.UserAgent + "</td></tr>");


            sb.Append("<tr><td><span style='Color:Red'>Error Message: </span>" + Error.Message.ToString() + "");
            sb.Append("</td></tr><tr><td><span style='Color:Red'>Error Details :</span>" + Error.ToString() + "</td></tr></table>");
            #endregion
            SendErrorMail(sb);

        }

        public static void ReportError(Exception Error, string PageUrl, string AdditionalInformation)
        {
            #region //Build Error Message
            System.Text.StringBuilder sb = new StringBuilder();
            sb.Append("<table><tr><td>");
            sb.Append("<span style='Color:Red'>Error Caught in Application_Error event</span></td></tr>");
            sb.Append("<tr><td><span style='Color:Red'>Time of error:</span> " + DateTime.Now.ToString() + "</td></tr>");

            try
            {
                if (System.Web.HttpContext.Current.User.Identity.Name != null)
                    sb.Append("<tr><td><span style='Color:Red'>User ID:</span> " + System.Web.HttpContext.Current.User.Identity.Name + "</td></tr>");
            }
            catch { }

            sb.Append("<tr><td><span style='Color:Red'>Error in:</span> " + PageUrl + "</td></tr>");
            sb.Append("<tr><td><span style='Color:Red'>Url Referrer :</span> " + HttpContext.Current.Request.UrlReferrer + "</td></tr>");
            sb.Append("<tr><td><span style='Color:Red'>Request Type :</span> " + HttpContext.Current.Request.RequestType + "</td></tr>");
            sb.Append("<tr><td><span style='Color:Red'>DSN :</span> " + HttpContext.Current.Request.UserHostName + "</td></tr>");
            sb.Append("<tr><td><span style='Color:Red'>IP :</span> " + HttpContext.Current.Request.UserHostAddress + "</td></tr>");
            sb.Append("<tr><td><span style='Color:Red'>Browser Agent:</span> " + HttpContext.Current.Request.UserAgent + "</td></tr>");

            sb.Append("<tr><td><span style='Color:Red'>Error Message: </span>" + Error.Message.ToString() + "");
            sb.Append("</td></tr><tr><td><span style='Color:Red'>Error Details :</span>" + Error.ToString() + "</td></tr></table>");
            if (!string.IsNullOrEmpty(AdditionalInformation))
                sb.Append("</td></tr><tr><td><span style='Color:Red'>Additional Information:</span>" + AdditionalInformation + "</td></tr></table>");

            #endregion

            SendErrorMail(sb);
        }


        private static void SendErrorMail(System.Text.StringBuilder ErrorMessage)
        {
            try
            {

                MailProvider ObjMailProvider = new MailProvider();
                ObjMailProvider.MailFrom = ConfigurationManager.AppSettings["FromAddress"].ToString();
                ObjMailProvider.MailTo.Add(ConfigurationManager.AppSettings["ApplicationErrorToAddress"].ToString());
                ObjMailProvider.Subject = "Error in EmergeMVC";
                ObjMailProvider.Message = ErrorMessage.ToString();
                ObjMailProvider.SendMail();
            }
            catch { }
        }
    }
}
