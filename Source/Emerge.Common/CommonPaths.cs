﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace Emerge.Common
{
    public static class CommonPaths
    {

        public static string ImagesPath
        {
            get
            {
                return "~/Content/themes/base/images/";
            }
        }

        public static string SitePath
        {

            get
            {            
              return  Common.ApplicationPath.GetApplicationPath();                  
               
            }
        }

        public static string ContentPath
        {
            get
            {
                return "~/Content/";
            }
        }

        public static string ScriptPath
        {
            get
            {
                return "~/Scripts/";
            }
        }


    }

}