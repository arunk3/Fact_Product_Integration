﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SecurePage
/// </summary>

public class SecurePage
{

    string _path = "";

    string _pathType = "";



    public string Path
    {

        get { return this._path; }

        set { this._path = value; }

    }



    public string PathType
    {

        get { return this._pathType; }

        set { this._pathType = value; }

    }

}

