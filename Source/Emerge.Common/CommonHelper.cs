﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;

namespace Emerge.Common
{
    public static class CommonHelper
    {
        public static bool IsGuid(string guid)
        {
            Regex isGuid = new Regex(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", RegexOptions.Compiled);
            bool isValid = false;
            if (!guid.Equals(new Guid("00000000-0000-0000-0000-000000000000")))
            {
                if (isGuid.IsMatch(guid))
                {
                    isValid = true;
                }
            }
            return isValid;
        }

        public static void DeleteFile(string FilePath)
        {
            try
            {
               /*
                var directory = new DirectoryInfo(FilePath);
                var fileName = (from f in directory.GetFiles()
                                orderby f.LastWriteTime ascending
                                select f).ToList();

                for (int i = 0; i < fileName.Count(); i++)
                {
                    string Filename = fileName[i].ToString();
                    if (System.IO.File.Exists(FilePath + "\\" + fileName[i].ToString()))
                    {
                        System.IO.File.Delete(FilePath);
                    }
                }
                */

            }
            catch
            {

            }
        }
    }
}
