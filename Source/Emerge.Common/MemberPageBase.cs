﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for MemberPageBase
/// </summary>
public class MemberPageBase : ServerSideViewState
{
   // public Guid GlobalMemberUserId { get; set; }
    public Guid GlobalMemberUserId = Guid.Empty;
    protected string GlobalMemberShipUserName = string.Empty;

    public MemberPageBase()
    {
        Guid UserId = Guid.Empty;

        MembershipUser ObjMembershipUser = Membership.GetUser(Page.User.Identity.Name);
        if (ObjMembershipUser != null)
        {
            UserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
        }
        GlobalMemberUserId = UserId;
        GlobalMemberShipUserName = Page.User.Identity.Name;
    }
}
