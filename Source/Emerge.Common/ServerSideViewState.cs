﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Configuration;
using System.Diagnostics;
using System.Web.Caching;
using System.Data;

/// <summary>
/// Summary description for ServerSideViewState
/// </summary>
public class ServerSideViewState : System.Web.UI.Page
{
    public ServerSideViewState()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    protected override void SavePageStateToPersistenceMedium(object viewState)
    {

        string VSKey;//String that will hold the Unique Key used to reference this ViewState data
        Debug.WriteLine(base.Session.SessionID);
        //Create the key based on the SessionID, on the Request.RawUrl
        //and on the Ticks representated by the exact time while the page is being saved
        VSKey = "VIEWSTATE_" + base.Session.SessionID + "_" + Request.RawUrl + "_" + DateTime.Now.Ticks.ToString();
        //Check if the ServerSideViewState is Activated
        if (ConfigurationManager.AppSettings["ServerSideViewState"].ToUpper() == "TRUE")
        {
            //Check were we will save the ViewState Data
            if (ConfigurationManager.AppSettings["ViewStateStore"].ToUpper() == "CACHE")
            {
                //Store the ViewState on Cache
                Cache.Add(VSKey, viewState, null, DateTime.Now.AddMinutes(Session.Timeout), Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Default, null);
                //The ViewStateData will be Saved on the SESSION
            }
            else
            {
                DataTable VSDataTable;
                DataRow DbRow;
                //Check if the ViewState DataTable are on the Session
                if (Session["__VSDataTable"] == null)
                {
                    //No, it's not. Create it...
                    DataColumn[] PkColumn = new DataColumn[2];
                    DataColumn DbColumn;
                    VSDataTable = new DataTable("VState"); //Create the DataTable
                    //Column 1 - Name: VSKey - PrimaryKey
                    DbColumn = new DataColumn("VSKey", typeof(string));
                    VSDataTable.Columns.Add(DbColumn);
                    PkColumn[0] = DbColumn;
                    VSDataTable.PrimaryKey = PkColumn;
                    //Column 2 - Name: ViewStateData
                    DbColumn = new DataColumn("VSData", typeof(object));
                    VSDataTable.Columns.Add(DbColumn);
                    //Column 3 - Name: DateTime
                    DbColumn = new DataColumn("DateTime", typeof(DateTime));
                    VSDataTable.Columns.Add(DbColumn);
                }
                else
                {
                    //The ViewState DataTable is already on the UserSession
                    VSDataTable = Session["__VSDataTable"] as DataTable;
                }
                //Check if we already have a ViewState saved with the same key.
                //If yes, update it instead of creating a new row. (This is very dificult to happen)
                DbRow = VSDataTable.Rows.Find(VSKey);
                if (DbRow != null)
                {
                    //Row found!!! Update instead of creating a new one...
                    DbRow["VSData"] = viewState;
                }
                else
                {
                    //Create a new row...
                    DbRow = VSDataTable.NewRow();
                    DbRow["VSKey"] = VSKey;
                    DbRow["VSData"] = viewState;
                    DbRow["DateTime"] = DateTime.Now;
                    VSDataTable.Rows.Add(DbRow);
                }
                //Check if our DataTable is OverSized...
                if (Convert.ToInt16(ConfigurationManager.AppSettings["ViewStateTableSize"]) < VSDataTable.Rows.Count)
                {
                    Debug.WriteLine("Deleting ViewState Created On " + DbRow[2] + ",ID " + DbRow[0]);
                    VSDataTable.Rows[0].Delete(); //Delete the 1st line.
                }
                //Store the DataTable on the Session.
                Session["__VSDataTable"] = VSDataTable;
                // Response.Write("<script>alert('"+Session["__VSDataTable"].ToString()+"')");
            }
            //Register a HiddenField on the Page, that contains ONLY the UniqueKey generated.
            //With this, we'll be able to find with ViewState is from this page, by retrieving these value.
            ClientScript.RegisterHiddenField("__VIEWSTATE_KEY", VSKey);
        }
        else
        {
            //Call the normal process.
            base.SavePageStateToPersistenceMedium(viewState);
        }
    }
    protected override object LoadPageStateFromPersistenceMedium()
    {
        //Verifica se o ServerSideViewState está ativado
        if (ConfigurationManager.AppSettings["ServerSideViewState"].ToUpper() == "TRUE")
        {
            string VSKey; //ViewState UniqueKey
            VSKey = Request.Form["__VIEWSTATE_KEY"]; //Request the Key from the page and validade it.
            if (!VSKey.StartsWith("VIEWSTATE_"))
            {
                //Throw New Exception("Invalid VIEWSTATE Key: " & VSKey)
            }
            //Verify which modality was used to save ViewState
            if (ConfigurationManager.AppSettings["ViewStateStore"].ToUpper() == "CACHE")
            {
                return Cache[VSKey];
            }
            else
            {
                DataTable VSDataTable;
                DataRow DbRow;
                try
                {
                    VSDataTable = Session["__VSDataTable"] as DataTable;
                    DbRow = VSDataTable.Rows.Find(VSKey);
                    if (DbRow == null)
                    {
                        //Throw New Exception("VIEWStateKey not Found. Consider increasing the ViewStateTableSize parameter on Web.Config file.")
                    }
                    return DbRow["VSData"];
                }
                catch
                {
                    return base.LoadPageStateFromPersistenceMedium();
                }
            }
        }
        else
        {
            //Return the ViewState using the Norma Method
            return base.LoadPageStateFromPersistenceMedium();
        }
    }
}
