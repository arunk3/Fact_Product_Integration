﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web;

namespace Emerge.Common
{
    public class WriteToTxtFile
    {
        public WriteToTxtFile()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public static void WriteToFile(List<string> strlst)
        {
            try
            {
                StreamWriter SW;
                string savedPath = string.Empty;
                DateTime dt = DateTime.Now;
                savedPath = HttpContext.Current.Server.MapPath("~/Resources/Upload/ErrorLog.txt");
                if (!File.Exists(savedPath))
                {
                    savedPath = HttpContext.Current.Server.MapPath("~/Resources/Upload/ErrorLog.txt");
                    SW = File.CreateText(savedPath);
                }
                else
                {
                    SW = File.AppendText(savedPath);
                }
                SW.WriteLine("\n");
                SW.WriteLine("****************************************");
                SW.WriteLine("Error Created Date :- " + dt.ToString());
                SW.WriteLine("****************************************");
                foreach (string str in strlst)
                {
                    if (str != "")
                    {
                        SW.WriteLine(str);
                    }
                }
                SW.Close();

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        public static void WriteToFile(Exception Ex, string PageUrl)
        {
            try
            {
                List<string> strlst = new List<string>();
                if (Ex.Message != null)
                    strlst.Add(Ex.Message.ToString());
                if (Ex.InnerException != null)
                    strlst.Add(Ex.InnerException.ToString());
                if (Ex.HelpLink != null)
                    strlst.Add(Ex.HelpLink.ToString());
                if (Ex.Source != null)
                    strlst.Add(Ex.Source.ToString());
                if (Ex.StackTrace != null)
                    strlst.Add(Ex.StackTrace.ToString());
                if (Ex.TargetSite != null)
                    strlst.Add(Ex.TargetSite.ToString());
                StreamWriter SW;
                string savedPath = string.Empty;
                string fileName = string.Empty;
                DateTime dt = DateTime.Now;
                bool IsExists = false;
                fileName = dt.ToString("yyMMdd");
                savedPath = HttpContext.Current.Server.MapPath("~/Resources/Upload/ErrorLog/" + fileName + ".txt");
                if (!File.Exists(savedPath))
                {
                    savedPath = HttpContext.Current.Server.MapPath("~/Resources/Upload/ErrorLog/" + fileName + ".txt");
                    SW = File.CreateText(savedPath);
                }
                else
                {
                    string errorString = "";
                    using (StreamReader responseFile = new StreamReader(savedPath))
                    {
                        errorString = responseFile.ReadToEnd();
                        responseFile.Close();
                    }
                    if (errorString.Contains(Ex.Message.ToString()) && errorString.Contains(PageUrl))
                    {
                        IsExists = true;
                    }
                    SW = File.AppendText(savedPath);
                }
                if (IsExists == false)
                {
                    SW.WriteLine("\n");
                    SW.WriteLine("****************************************");
                    SW.WriteLine("Error Created Date :- " + dt.ToString());
                    SW.WriteLine("****************************************");
                    //foreach (string str in strlst)
                    //{
                    //    if (str != "")
                    //    {
                    //        SW.WriteLine(str);
                    //    }
                    //}
                    SW.WriteLine("Error Caught in Application_Error event");
                    SW.WriteLine("Time of error : " + DateTime.Now.ToString());
                    try
                    {
                        if (System.Web.HttpContext.Current.User.Identity.Name != null)
                            SW.WriteLine("User ID : " + System.Web.HttpContext.Current.User.Identity.Name);
                    }
                    catch { }
                    SW.WriteLine("Error in : " + PageUrl);
                    SW.WriteLine("Url Referrer : " + HttpContext.Current.Request.UrlReferrer);
                    SW.WriteLine("Request Type : " + HttpContext.Current.Request.RequestType);
                    SW.WriteLine("DSN : " + HttpContext.Current.Request.UserHostName);
                    SW.WriteLine("IP : " + HttpContext.Current.Request.UserHostAddress);
                    SW.WriteLine("Browser Agent : " + HttpContext.Current.Request.UserAgent);
                    SW.WriteLine("Error Message : " + Ex.Message.ToString());
                    SW.WriteLine("Error Details : " + Ex.ToString());
                }

                SW.Close();

            }
            catch { }
        }
    }
}
