﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Mail;


namespace Emerge.Common
{
    public class MailProvider
    {
        SmtpClient objSmtpClient = null;

        #region PrivateVeriable   //Declare all Local Veriables related to email
        //========== Message Related ==========
        private string _MailFrom;
        private List<string> _MailTo = new List<string>();
        private List<string> _MailCC = new List<string>();
        private List<string> _MailBCC = new List<string>();

        private List<Attachment> _MailAttachments = new List<Attachment>();

        private string _Subject;
        private string _Message;
        private bool _IsBodyHtml = true;
        private bool _EnableSSL = false;

        private MailPriority _MailPriority = MailPriority.Normal;

        private System.Text.Encoding _MessageEncoding = System.Text.Encoding.UTF8;
        //=====================================
        #endregion


        #region Properties   //Declare all Properties related to email

        /// <summary>
        ///  Add Sender Address 
        /// </summary>
        public string MailFrom
        {
            get
            {
                return _MailFrom;
            }
            set
            {
                _MailFrom = value;
            }
        }

        /// <summary>
        /// Add Receipient Address (TO) 
        /// </summary>
        public List<string> MailTo
        {
            get
            {
                return _MailTo;
            }
            set
            {
                _MailTo = value;
            }
        }

        /// <summary>
        ///  Add Receipient Address (CC)
        /// </summary>
        public List<string> MailCC
        {
            get
            {
                return _MailCC;
            }
            set
            {
                _MailCC = value;
            }
        }

        /// <summary>
        ///  Add Receipient Address (BCC) 
        /// </summary>
        public List<string> MailBCC
        {
            get
            {
                return _MailBCC;
            }
            set
            {
                _MailBCC = value;
            }
        }

        /// <summary>
        ///  Subject 
        /// </summary>
        public string Subject
        {
            get
            {
                return _Subject;
            }
            set
            {
                _Subject = value;
            }
        }

        /// <summary>
        ///  Message Body 
        /// </summary>
        public string Message
        {
            get
            {
                return _Message;
            }
            set
            {
                _Message = value;
            }
        }

        /// <summary>
        ///  Message Attachment (Optional) 
        ///  File Path of the attachment(file)
        ///  Server.MapPath
        /// </summary>
        public List<Attachment> MailAttachments
        {
            get
            {
                return _MailAttachments;
            }
            set
            {
                _MailAttachments = value;
            }
        }

        /// <summary>
        /// SSL required to send mail
        /// </summary>
        public bool EnableSSL
        {
            get
            {
                return _EnableSSL;
            }
            set
            {
                _EnableSSL = value;
            }


        }

        /// <summary>
        ///  Message Body Html or Text 
        /// </summary>
        public bool IsBodyHtml
        {
            get
            {
                return _IsBodyHtml;
            }
            set
            {
                _IsBodyHtml = value;
            }


        }

        /// <summary>
        /// Specifies the priority of a System.Net.Mail.MailMessage.
        /// </summary>
        public MailPriority Priority
        {
            get
            {
                return _MailPriority;
            }
            set
            {
                _MailPriority = value;
            }
        }


        /// <summary>
        /// Specifies the Message Encoding of a System.Net.Mail.MailMessage.
        /// Default encoding UTF-8
        /// System.Text.Encoding
        /// </summary>
        public System.Text.Encoding MessageEncoding
        {
            get
            {
                return _MessageEncoding;
            }
            set
            {
                MessageEncoding = value;
            }
        }

        /// <summary>
        /// Send Email
        /// </summary>
        #endregion


        #region Constructor   //Declare Constructor related to email

        public MailProvider()
        {
            try
            {
                objSmtpClient = new SmtpClient();
                _EnableSSL = bool.Parse(ConfigurationManager.AppSettings["emailssl"].ToLower());

            }
            catch
            {

                _EnableSSL = false;
            }
        }
        public MailProvider(string SMTPHost)
        {
            objSmtpClient = new SmtpClient();
            objSmtpClient.Host = SMTPHost;
            objSmtpClient.Port = 25;
            try
            {
                _EnableSSL = bool.Parse(ConfigurationManager.AppSettings["emailssl"].ToLower());

            }
            catch
            {

                _EnableSSL = false;
            }
        }
        public MailProvider(string SMTPHost, int SMTPPort)
        {
            objSmtpClient = new SmtpClient();
            objSmtpClient.Host = SMTPHost;
            objSmtpClient.Port = SMTPPort;
            try
            {
                _EnableSSL = bool.Parse(ConfigurationManager.AppSettings["emailssl"].ToLower());

            }
            catch
            {

                _EnableSSL = false;
            }
        }
        public MailProvider(string SMTPHost, int SMTPPort, string UserName, string Password)
        {
            objSmtpClient = new SmtpClient();
            objSmtpClient.Host = SMTPHost;
            objSmtpClient.Port = SMTPPort;
            objSmtpClient.Credentials = new NetworkCredential(UserName, Password);

            try
            {
                _EnableSSL = bool.Parse(ConfigurationManager.AppSettings["emailssl"].ToLower());
            }
            catch
            {
                _EnableSSL = false;
            }

        }


        #endregion


        #region Methods //Declare methods related to mail
        public void SendMail()
       {
            MailMessage objMailMessage = new MailMessage();

            try
            {
                #region //Add Sender Address
                if (_MailFrom != null)
                    objMailMessage.From = new MailAddress(_MailFrom);
                #endregion

                #region //Add Receipient Address (TO)
                foreach (string to in _MailTo)
                {
                    objMailMessage.To.Add(to);
                }
                #endregion

                #region //Add Receipient Address (CC)
                foreach (string cc in _MailCC)
                {
                    objMailMessage.CC.Add(cc);
                }
                #endregion

                #region //Add Receipient Address (BCC)
                foreach (string bcc in _MailBCC)
                {
                    objMailMessage.Bcc.Add(bcc);
                }

                #endregion

                #region //Add Subject
                objMailMessage.Subject = _Subject;
                #endregion

                #region //Message Body Html or Text
                objMailMessage.IsBodyHtml = _IsBodyHtml;
                #endregion

                #region //Message Priority
                objMailMessage.Priority = _MailPriority;
                #endregion

                #region // Message Delivery Notification
                objMailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                #endregion

                #region // Message Body
                if (_IsBodyHtml == true)
                {
                    #region // This way prevent message to be SPAM
                    objMailMessage.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                    AlternateView plainView = AlternateView.CreateAlternateViewFromString
                        (System.Text.RegularExpressions.Regex.Replace(_Message, @"<(.|\n)*?>", string.Empty),
                        null, "text/plain");
                    AlternateView htmlView = AlternateView.CreateAlternateViewFromString(_Message, null, "text/html");

                    objMailMessage.AlternateViews.Add(plainView);
                    objMailMessage.AlternateViews.Add(htmlView);
                    #endregion
                }
                else
                    objMailMessage.Body = _Message;
                #endregion

                #region //Message Encoding
                objMailMessage.BodyEncoding = _MessageEncoding;
                #endregion

                #region //Attachment
                foreach (Attachment ObjAttachment in _MailAttachments)
                {
                    objMailMessage.Attachments.Add(ObjAttachment);
                }
                #endregion

                #region //SSL checking
                objSmtpClient.EnableSsl = _EnableSSL;
                #endregion

                #region // Message Sent
                objSmtpClient.Send(objMailMessage);
                #endregion


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objMailMessage.Dispose();
            }

        }

        /// <summary>
        /// send email pass the SendingStatus parameter with this methods
        /// O for failed, 1 for sucess
        /// </summary>
        /// <param name="SendingStatus"></param>
        public void SendMail(out int SendingStatus)
        {
            MailMessage objMailMessage = new MailMessage();

            try
            {
                #region //Add Sender Address
                if (_MailFrom != null)
                    objMailMessage.From = new MailAddress(_MailFrom);
                #endregion

                #region //Add Receipient Address (TO)
                foreach (string to in _MailTo)
                {
                    objMailMessage.To.Add(to);
                }
                #endregion

                #region //Add Receipient Address (CC)
                foreach (string cc in _MailCC)
                {
                    objMailMessage.CC.Add(cc);
                }
                #endregion

                #region //Add Receipient Address (BCC)
                foreach (string bcc in _MailBCC)
                {
                    objMailMessage.Bcc.Add(bcc);
                }

                #endregion

                #region //Add Subject
                objMailMessage.Subject = _Subject;
                #endregion

                #region //Message Body Html or Text
                objMailMessage.IsBodyHtml = _IsBodyHtml;
                #endregion

                #region //Message Priority
                objMailMessage.Priority = _MailPriority;
                #endregion

                #region // Message Delivery Notification
                objMailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                #endregion

                #region //======== Message Body ==========
                if (_IsBodyHtml == true)
                {
                    #region // This way prevent message to be SPAM
                    objMailMessage.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                    AlternateView plainView = AlternateView.CreateAlternateViewFromString
                        (System.Text.RegularExpressions.Regex.Replace(_Message, @"<(.|\n)*?>", string.Empty),
                        null, "text/plain");
                    AlternateView htmlView = AlternateView.CreateAlternateViewFromString(_Message, null, "text/html");

                    objMailMessage.AlternateViews.Add(plainView);
                    objMailMessage.AlternateViews.Add(htmlView);
                    #endregion
                }
                else
                    objMailMessage.Body = _Message;
                #endregion

                #region //Message Encoding
                objMailMessage.BodyEncoding = _MessageEncoding;
                #endregion

                #region //Attachment
                foreach (Attachment ObjAttachment in _MailAttachments)
                {
                    objMailMessage.Attachments.Add(ObjAttachment);
                }
                #endregion

                #region //SSL checking
                objSmtpClient.EnableSsl = _EnableSSL;
                #endregion

                #region // Message Sent
                //objSmtpClient.Host = "mail.dotnetoutsourcing.com";
                //objSmtpClient.Credentials = new NetworkCredential("support@dotnetoutsourcing.com", "test123");
                objSmtpClient.Send(objMailMessage);
                #endregion
                SendingStatus = 1;
            }
            catch// (Exception ex)
            {
                SendingStatus = 0;
            }
            finally
            {
                objMailMessage.Dispose();

            }

        }

        /// <summary>
        /// send email pass the SendingStatus parameter with this methods
        /// ex message for failed, "Success" for success
        /// </summary>
        /// <param name="SendingStatus"></param>
        public void SendMail(out string SendingStatus)
        {
            MailMessage objMailMessage = new MailMessage();

            try
            {
                #region //Add Sender Address
                if (_MailFrom != null)
                    objMailMessage.From = new MailAddress(_MailFrom);
                #endregion

                #region //Add Recipient Address (TO)
                foreach (string to in _MailTo)
                {
                    objMailMessage.To.Add(to);
                }
                #endregion

                #region //Add Recipient Address (CC)
                foreach (string cc in _MailCC)
                {
                    objMailMessage.CC.Add(cc);
                }
                #endregion

                #region //Add Recipient Address (BCC)
                foreach (string bcc in _MailBCC)
                {
                    objMailMessage.Bcc.Add(bcc);
                }

                #endregion

                #region //Add Subject
                objMailMessage.Subject = _Subject;
                #endregion

                #region //Message Body Html or Text
                objMailMessage.IsBodyHtml = _IsBodyHtml;
                #endregion

                #region //Message Priority
                objMailMessage.Priority = _MailPriority;
                #endregion

                #region // Message Delivery Notification
                objMailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                #endregion

                #region //======== Message Body ==========
                if (_IsBodyHtml == true)
                {
                    #region // This way prevent message to be SPAM
                    objMailMessage.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                    AlternateView plainView = AlternateView.CreateAlternateViewFromString
                        (System.Text.RegularExpressions.Regex.Replace(_Message, @"<(.|\n)*?>", string.Empty),
                        null, "text/plain");
                    AlternateView htmlView = AlternateView.CreateAlternateViewFromString(_Message, null, "text/html");

                    objMailMessage.AlternateViews.Add(plainView);
                    objMailMessage.AlternateViews.Add(htmlView);
                    #endregion
                }
                   
                   
                else
                    objMailMessage.Body = _Message;
                #endregion

                #region //Message Encoding
                objMailMessage.BodyEncoding = _MessageEncoding;
                #endregion

                #region //Attachment
                foreach (Attachment ObjAttachment in _MailAttachments)
                {
                    objMailMessage.Attachments.Add(ObjAttachment);
                }
                #endregion

                #region //SSL checking
                objSmtpClient.EnableSsl = _EnableSSL;
                #endregion

                #region // Message Sent
                //objSmtpClient.Host = "mail.dotnetoutsourcing.com";
                //objSmtpClient.Credentials = new NetworkCredential("support@dotnetoutsourcing.com", "test123");
                objSmtpClient.Send(objMailMessage);
                #endregion
                SendingStatus = "Success";
            }
            catch (Exception ex)
            {
                SendingStatus = ex.Message.ToString();
            }
            finally
            {
                objMailMessage.Dispose();

            }

        }


        /// <summary>
        /// Message Sent Asynchronously
        /// </summary>
        public bool SendMailAsc()
        {
            using (MailMessage objMailMessage = new MailMessage())
            {

                try
                {
                    #region //Add Sender Address
                    if (_MailFrom != null)
                        objMailMessage.From = new MailAddress(_MailFrom);
                    #endregion

                    #region //Add Receipient Address (TO)
                    foreach (string to in _MailTo)
                    {
                        objMailMessage.To.Add(to);
                    }
                    #endregion

                    #region //Add Receipient Address (CC)
                    foreach (string cc in _MailCC)
                    {
                        objMailMessage.CC.Add(cc);
                    }
                    #endregion

                    #region //Add Receipient Address (BCC)
                    foreach (string bcc in _MailBCC)
                    {
                        objMailMessage.Bcc.Add(bcc);
                    }

                    #endregion

                    #region //Add Subject
                    objMailMessage.Subject = _Subject;
                    #endregion

                    #region //Message Body Html or Text
                    objMailMessage.IsBodyHtml = _IsBodyHtml;
                    #endregion

                    #region //Message Priority High
                    objMailMessage.Priority = _MailPriority;
                    #endregion

                    #region//Message Delivery Notification
                    objMailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                    #endregion

                    #region //Message Body
                    if (_IsBodyHtml == true)
                    {
                        #region // This way prevent message to be SPAM
                        objMailMessage.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                        AlternateView plainView = AlternateView.CreateAlternateViewFromString
                            (System.Text.RegularExpressions.Regex.Replace(_Message, @"<(.|\n)*?>", string.Empty),
                            null, "text/plain");
                        AlternateView htmlView = AlternateView.CreateAlternateViewFromString(_Message, null, "text/html");

                        objMailMessage.AlternateViews.Add(plainView);
                        objMailMessage.AlternateViews.Add(htmlView);
                        #endregion
                    }
                    else
                        objMailMessage.Body = _Message;
                    #endregion

                    #region //Message Encoding
                    objMailMessage.BodyEncoding = _MessageEncoding;
                    #endregion

                    #region //Attachment
                    foreach (Attachment ObjAttachment in _MailAttachments)
                    {
                        objMailMessage.Attachments.Add(ObjAttachment);
                    }
                    #endregion

                    #region //SSl Settings
                    objSmtpClient.EnableSsl = _EnableSSL;
                    #endregion

                    #region //wire up the event for when the Async send is completed
                    objSmtpClient.SendCompleted += new SendCompletedEventHandler(smtp_SendCompleted);
                    #endregion

                    #region //======== Message Sent Asynchronously ==========
                    object userState = objMailMessage;
                    objSmtpClient.SendAsync(objMailMessage, userState);
                    #endregion

                    return true;

                }
                catch (Exception ex)
                {
                    throw ex;
                }
                //finally
                //{
                //    objMailMessage = null;
                //}
            }

        }

        void smtp_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {

           //// MailMessage mail = (MailMessage)e.UserState;

           // if (e.Error != null)
           // {
           //     //Exception ex = new Exception(e.Error.ToString());
           //     try
           //     {
           //         // throw ex;
           //     }
           //     finally
           //     {
           //       //  ex = null;
           //     }

           // }
        }
        #endregion


    }




}