using System;
namespace EmergeBilling
{
    partial class BillingReport
    {
        public static string Month { get; set; }
        public static string Year { get; set; }

        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.InstanceReportSource instanceReportSource1 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource2 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource3 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource4 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource5 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource6 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource7 = new Telerik.Reporting.InstanceReportSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BillingReport));
            Telerik.Reporting.InstanceReportSource instanceReportSource8 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource9 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource10 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource11 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource12 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource13 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource14 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource15 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource16 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource17 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource18 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource19 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource20 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource21 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource22 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource23 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource24 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource25 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource26 = new Telerik.Reporting.InstanceReportSource();
            this.subBillingSummary1 = new EmergeBilling.SubBillingSummary();
            this.subCompanyTransactions1 = new EmergeBilling.SubCompanyTransactions();
            this.subSalesTotals1 = new EmergeBilling.SubSalesTotals();
            this.subVolumeTotals1 = new EmergeBilling.SubVolumeTotals();
            this.srPageBreack1 = new EmergeBilling.SRPageBreack();
            this.subProducts1 = new EmergeBilling.SubProducts();
            this.subAdjustmentsReport2 = new EmergeBilling.SubAdjustmentsReport();
            this.srPageBreack21 = new EmergeBilling.SRPageBreack2();
            this.subBillingSummary2 = new EmergeBilling.SubBillingSummary();
            this.subProducts2 = new EmergeBilling.SubProducts();
            this.subAdjustmentsReport1 = new EmergeBilling.SubAdjustmentsReport();
            this.srPageBreack2 = new EmergeBilling.SRPageBreack();
            this.subCompanyTransactions2 = new EmergeBilling.SubCompanyTransactions();
            this.subCompanyTransactions3 = new EmergeBilling.SubCompanyTransactions();
            this.subCompanyTransactions4 = new EmergeBilling.SubCompanyTransactions();
            this.subCompanyTransactions5 = new EmergeBilling.SubCompanyTransactions();
            this.subCompanyTransactions6 = new EmergeBilling.SubCompanyTransactions();
            this.subCompanyTransactions7 = new EmergeBilling.SubCompanyTransactions();
            this.subCompanyTransactions8 = new EmergeBilling.SubCompanyTransactions();
            this.detail = new Telerik.Reporting.DetailSection();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.subBillingSummary = new Telerik.Reporting.SubReport();
            this.panel1 = new Telerik.Reporting.Panel();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.subCompanyTransactions = new Telerik.Reporting.SubReport();
            this.lblSalesTotal = new Telerik.Reporting.TextBox();
            this.subSales = new Telerik.Reporting.SubReport();
            this.lblVolumeLable = new Telerik.Reporting.TextBox();
            this.subVolume = new Telerik.Reporting.SubReport();
            this.subPageBreack = new Telerik.Reporting.SubReport();
            this.panel4 = new Telerik.Reporting.Panel();
            this.subProducts = new Telerik.Reporting.SubReport();
            this.lblAdjustmentHeading = new Telerik.Reporting.Panel();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.panel2 = new Telerik.Reporting.Panel();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.subAdjustments = new Telerik.Reporting.SubReport();
            this.pnlHeader = new Telerik.Reporting.Panel();
            this.lblLeftHeader = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.imgGraph = new Telerik.Reporting.PictureBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.subReport1 = new Telerik.Reporting.SubReport();
            this.txtApiHeader = new Telerik.Reporting.TextBox();
            this.subBillingSummaryApi = new Telerik.Reporting.SubReport();
            this.pnlApiProductHead = new Telerik.Reporting.Panel();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.pnlContainer = new Telerik.Reporting.Panel();
            this.panel6 = new Telerik.Reporting.Panel();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.subProductsApi = new Telerik.Reporting.SubReport();
            this.pnlAdjustmentApi = new Telerik.Reporting.Panel();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.subAdjustmentsApi = new Telerik.Reporting.SubReport();
            this.subReport2 = new Telerik.Reporting.SubReport();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.subCompanyOneMonth = new Telerik.Reporting.SubReport();
            this.subReport3 = new Telerik.Reporting.SubReport();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.subCompanyThreeMonth = new Telerik.Reporting.SubReport();
            this.subReport5 = new Telerik.Reporting.SubReport();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.subCompanySixMonth = new Telerik.Reporting.SubReport();
            this.subReport7 = new Telerik.Reporting.SubReport();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.subCompanyTwelveMonth = new Telerik.Reporting.SubReport();
            this.subReport9 = new Telerik.Reporting.SubReport();
            this.subReport11 = new Telerik.Reporting.SubReport();
            this.subCompanyInCollection = new Telerik.Reporting.SubReport();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.subReport13 = new Telerik.Reporting.SubReport();
            this.subCompanyPastDue = new Telerik.Reporting.SubReport();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.subCompanyLocked = new Telerik.Reporting.SubReport();
            this.subReport16 = new Telerik.Reporting.SubReport();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.panel5 = new Telerik.Reporting.Panel();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.textBox75 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.subBillingSummary1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subCompanyTransactions1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subSalesTotals1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subVolumeTotals1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.srPageBreack1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subProducts1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subAdjustmentsReport2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.srPageBreack21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subBillingSummary2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subProducts2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subAdjustmentsReport1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.srPageBreack2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subCompanyTransactions2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subCompanyTransactions3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subCompanyTransactions4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subCompanyTransactions5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subCompanyTransactions6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subCompanyTransactions7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subCompanyTransactions8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // subBillingSummary1
            // 
            this.subBillingSummary1.Name = "SubBillingSummary1";
            // 
            // subCompanyTransactions1
            // 
            this.subCompanyTransactions1.Name = "SubCompanyTransactions";
            // 
            // subSalesTotals1
            // 
            this.subSalesTotals1.Name = "SubSalesTotals";
            // 
            // subVolumeTotals1
            // 
            this.subVolumeTotals1.Name = "SubVolumeTotals";
            // 
            // srPageBreack1
            // 
            this.srPageBreack1.Name = "SRPageBreack1";
            // 
            // subProducts1
            // 
            this.subProducts1.Name = "SubProducts1";
            // 
            // subAdjustmentsReport2
            // 
            this.subAdjustmentsReport2.Name = "SubAdjustmentsReport2";
            // 
            // srPageBreack21
            // 
            this.srPageBreack21.Name = "SRPageBreack21";
            // 
            // subBillingSummary2
            // 
            this.subBillingSummary2.Name = "SubBillingSummary2";
            // 
            // subProducts2
            // 
            this.subProducts2.Name = "SubProducts2";
            // 
            // subAdjustmentsReport1
            // 
            this.subAdjustmentsReport1.Name = "SubAdjustmentsReport1";
            // 
            // srPageBreack2
            // 
            this.srPageBreack2.Name = "SRPageBreack2";
            // 
            // subCompanyTransactions2
            // 
            this.subCompanyTransactions2.Name = "SubCompanyTransactions";
            // 
            // subCompanyTransactions3
            // 
            this.subCompanyTransactions3.Name = "SubCompanyTransactions";
            // 
            // subCompanyTransactions4
            // 
            this.subCompanyTransactions4.Name = "SubCompanyTransactions";
            // 
            // subCompanyTransactions5
            // 
            this.subCompanyTransactions5.Name = "SubCompanyTransactions";
            // 
            // subCompanyTransactions6
            // 
            this.subCompanyTransactions6.Name = "SubCompanyTransactions";
            // 
            // subCompanyTransactions7
            // 
            this.subCompanyTransactions7.Name = "SubCompanyTransactions";
            // 
            // subCompanyTransactions8
            // 
            this.subCompanyTransactions8.Name = "SubCompanyTransactions";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(21.92283821105957D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1,
            this.subBillingSummary,
            this.panel1,
            this.textBox14,
            this.subCompanyTransactions,
            this.lblSalesTotal,
            this.subSales,
            this.lblVolumeLable,
            this.subVolume,
            this.subPageBreack,
            this.panel4,
            this.pnlHeader,
            this.subReport1,
            this.txtApiHeader,
            this.subBillingSummaryApi,
            this.pnlApiProductHead,
            this.pnlContainer,
            this.subReport2,
            this.textBox16,
            this.subCompanyOneMonth,
            this.subReport3,
            this.textBox29,
            this.subCompanyThreeMonth,
            this.subReport5,
            this.textBox30,
            this.subCompanySixMonth,
            this.subReport7,
            this.textBox31,
            this.subCompanyTwelveMonth,
            this.subReport9,
            this.subReport11,
            this.subCompanyInCollection,
            this.textBox32,
            this.subReport13,
            this.subCompanyPastDue,
            this.textBox33,
            this.textBox34,
            this.textBox35,
            this.subCompanyLocked,
            this.subReport16});
            this.detail.Name = "detail";
            this.detail.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.detail.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.detail.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.detail.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.detail.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.detail.Style.Font.Name = "Calibri";
            this.detail.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.detail.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            // 
            // textBox1
            // 
            this.textBox1.CanShrink = false;
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10000017285346985D), Telerik.Reporting.Drawing.Unit.Inch(7.1781249046325684D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999983787536621D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox1.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "Billing Summary";
            // 
            // subBillingSummary
            // 
            this.subBillingSummary.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10000017285346985D), Telerik.Reporting.Drawing.Unit.Inch(7.7781257629394531D));
            this.subBillingSummary.Name = "subBillingSummary";
            instanceReportSource1.ReportDocument = this.subBillingSummary1;
            this.subBillingSummary.ReportSource = instanceReportSource1;
            this.subBillingSummary.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(0.73966234922409058D));
            this.subBillingSummary.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subBillingSummary.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subBillingSummary.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subBillingSummary.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subBillingSummary.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subBillingSummary.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // panel1
            // 
            this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox5,
            this.textBox4,
            this.textBox3,
            this.textBox2});
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10003958642482758D), Telerik.Reporting.Drawing.Unit.Inch(7.5384631156921387D));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7999999523162842D), Telerik.Reporting.Drawing.Unit.Pixel(23D));
            this.panel1.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(187)))), ((int)(((byte)(187)))));
            this.panel1.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.panel1.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.panel1.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.panel1.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.panel1.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.panel1.Style.Font.Name = "Calibri";
            this.panel1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.panel1.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.panel1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.panel1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.panel1.Style.Visible = true;
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.3958332538604736D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.39996027946472168D), Telerik.Reporting.Drawing.Unit.Pixel(22.996223449707031D));
            this.textBox5.Style.Font.Bold = true;
            this.textBox5.Style.Font.Name = "Calibri";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox5.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.Value = "Price";
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.59999990463256836D), Telerik.Reporting.Drawing.Unit.Pixel(22.996223449707031D));
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Style.Font.Name = "Calibri";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox4.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.Value = "Quantity";
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4999605417251587D), Telerik.Reporting.Drawing.Unit.Pixel(22.636222839355469D));
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Name = "Calibri";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox3.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.Value = "Report Type";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.59375D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79999971389770508D), Telerik.Reporting.Drawing.Unit.Pixel(22.996223449707031D));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Name = "Calibri";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox2.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox2.Value = "Data Source";
            // 
            // textBox14
            // 
            this.textBox14.CanShrink = false;
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10003958642482758D), Telerik.Reporting.Drawing.Unit.Inch(12.54062557220459D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999983787536621D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox14.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox14.Value = "Company Transactions";
            // 
            // subCompanyTransactions
            // 
            this.subCompanyTransactions.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.08441460132598877D), Telerik.Reporting.Drawing.Unit.Inch(13.246874809265137D));
            this.subCompanyTransactions.Name = "subCompanyTransactions";
            instanceReportSource2.ReportDocument = this.subCompanyTransactions1;
            this.subCompanyTransactions.ReportSource = instanceReportSource2;
            this.subCompanyTransactions.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.69999885559082D), Telerik.Reporting.Drawing.Unit.Inch(0.66033762693405151D));
            this.subCompanyTransactions.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyTransactions.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyTransactions.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyTransactions.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyTransactions.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyTransactions.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // lblSalesTotal
            // 
            this.lblSalesTotal.CanShrink = false;
            this.lblSalesTotal.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1000400111079216D), Telerik.Reporting.Drawing.Unit.Inch(4.892707347869873D));
            this.lblSalesTotal.Name = "lblSalesTotal";
            this.lblSalesTotal.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999983787536621D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.lblSalesTotal.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.lblSalesTotal.Style.Font.Bold = true;
            this.lblSalesTotal.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.lblSalesTotal.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.lblSalesTotal.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblSalesTotal.Value = "Sales Totals";
            // 
            // subSales
            // 
            this.subSales.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10000017285346985D), Telerik.Reporting.Drawing.Unit.Inch(5.2011198997497559D));
            this.subSales.Name = "subSales";
            instanceReportSource3.ReportDocument = this.subSalesTotals1;
            this.subSales.ReportSource = instanceReportSource3;
            this.subSales.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.69999885559082D), Telerik.Reporting.Drawing.Unit.Inch(0.66033762693405151D));
            this.subSales.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subSales.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subSales.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subSales.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subSales.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subSales.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // lblVolumeLable
            // 
            this.lblVolumeLable.CanShrink = false;
            this.lblVolumeLable.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.095793828368186951D), Telerik.Reporting.Drawing.Unit.Inch(5.9698691368103027D));
            this.lblVolumeLable.Name = "lblVolumeLable";
            this.lblVolumeLable.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999983787536621D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.lblVolumeLable.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.lblVolumeLable.Style.Font.Bold = true;
            this.lblVolumeLable.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.lblVolumeLable.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.lblVolumeLable.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblVolumeLable.Value = "Volume Totals";
            // 
            // subVolume
            // 
            this.subVolume.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.095793828368186951D), Telerik.Reporting.Drawing.Unit.Inch(6.2823691368103027D));
            this.subVolume.Name = "subVolume";
            instanceReportSource4.ReportDocument = this.subVolumeTotals1;
            this.subVolume.ReportSource = instanceReportSource4;
            this.subVolume.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.69999885559082D), Telerik.Reporting.Drawing.Unit.Inch(0.7457541823387146D));
            this.subVolume.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subVolume.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subVolume.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subVolume.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subVolume.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subVolume.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // subPageBreack
            // 
            this.subPageBreack.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1000400111079216D), Telerik.Reporting.Drawing.Unit.Inch(7.078125D));
            this.subPageBreack.Name = "subPageBreack";
            instanceReportSource5.ReportDocument = this.srPageBreack1;
            this.subPageBreack.ReportSource = instanceReportSource5;
            this.subPageBreack.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.69999885559082D), Telerik.Reporting.Drawing.Unit.Inch(0.052083492279052734D));
            this.subPageBreack.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subPageBreack.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subPageBreack.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subPageBreack.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subPageBreack.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subPageBreack.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // panel4
            // 
            this.panel4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.subProducts,
            this.lblAdjustmentHeading,
            this.panel2,
            this.subAdjustments});
            this.panel4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4D), Telerik.Reporting.Drawing.Unit.Inch(7.4781241416931152D));
            this.panel4.Name = "panel4";
            this.panel4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8042445182800293D), Telerik.Reporting.Drawing.Unit.Inch(2.3000001907348633D));
            // 
            // subProducts
            // 
            this.subProducts.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0042449138127267361D), Telerik.Reporting.Drawing.Unit.Inch(0.2999996542930603D));
            this.subProducts.Name = "subProducts";
            instanceReportSource6.ReportDocument = this.subProducts1;
            this.subProducts.ReportSource = instanceReportSource6;
            this.subProducts.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(0.73966234922409058D));
            this.subProducts.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subProducts.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subProducts.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subProducts.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subProducts.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subProducts.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // lblAdjustmentHeading
            // 
            this.lblAdjustmentHeading.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox13});
            this.lblAdjustmentHeading.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.3113691788599908E-09D), Telerik.Reporting.Drawing.Unit.Inch(1.1603376865386963D));
            this.lblAdjustmentHeading.Name = "lblAdjustmentHeading";
            this.lblAdjustmentHeading.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8000001907348633D), Telerik.Reporting.Drawing.Unit.Pixel(23D));
            this.lblAdjustmentHeading.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(187)))), ((int)(((byte)(187)))));
            this.lblAdjustmentHeading.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.lblAdjustmentHeading.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.lblAdjustmentHeading.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.lblAdjustmentHeading.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.lblAdjustmentHeading.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.lblAdjustmentHeading.Style.Font.Name = "Calibri";
            this.lblAdjustmentHeading.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.lblAdjustmentHeading.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.lblAdjustmentHeading.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.lblAdjustmentHeading.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblAdjustmentHeading.Style.Visible = true;
            // 
            // textBox10
            // 
            this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.3958332538604736D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.39996027946472168D), Telerik.Reporting.Drawing.Unit.Pixel(22.996223449707031D));
            this.textBox10.Style.Font.Bold = true;
            this.textBox10.Style.Font.Name = "Calibri";
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox10.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox10.Value = "Price";
            // 
            // textBox11
            // 
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.59999990463256836D), Telerik.Reporting.Drawing.Unit.Pixel(22.996223449707031D));
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Font.Name = "Calibri";
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox11.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox11.Value = "Quantity";
            // 
            // textBox12
            // 
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4999605417251587D), Telerik.Reporting.Drawing.Unit.Pixel(22.636222839355469D));
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.Font.Name = "Calibri";
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox12.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox12.Value = "Products";
            // 
            // textBox13
            // 
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.59375D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79999971389770508D), Telerik.Reporting.Drawing.Unit.Pixel(22.996223449707031D));
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.Font.Name = "Calibri";
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox13.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox13.Value = "Data Source";
            // 
            // panel2
            // 
            this.panel2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9});
            this.panel2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0042449138127267361D), Telerik.Reporting.Drawing.Unit.Inch(0.06033749133348465D));
            this.panel2.Name = "panel2";
            this.panel2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7999999523162842D), Telerik.Reporting.Drawing.Unit.Pixel(23D));
            this.panel2.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(187)))), ((int)(((byte)(187)))));
            this.panel2.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.panel2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.panel2.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.panel2.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.panel2.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.panel2.Style.Font.Name = "Calibri";
            this.panel2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.panel2.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.panel2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.panel2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.panel2.Style.Visible = true;
            // 
            // textBox6
            // 
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.3958332538604736D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.39996027946472168D), Telerik.Reporting.Drawing.Unit.Pixel(22.996223449707031D));
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.Font.Name = "Calibri";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox6.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.Value = "Price";
            // 
            // textBox7
            // 
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.59999990463256836D), Telerik.Reporting.Drawing.Unit.Pixel(22.996223449707031D));
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.Font.Name = "Calibri";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox7.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.Value = "Quantity";
            // 
            // textBox8
            // 
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4999605417251587D), Telerik.Reporting.Drawing.Unit.Pixel(22.636222839355469D));
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Name = "Calibri";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox8.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.Value = "Products";
            // 
            // textBox9
            // 
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.59375D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79999971389770508D), Telerik.Reporting.Drawing.Unit.Pixel(22.996223449707031D));
            this.textBox9.Style.Font.Bold = true;
            this.textBox9.Style.Font.Name = "Calibri";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox9.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.Value = "Data Source";
            // 
            // subAdjustments
            // 
            this.subAdjustments.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.3113691788599908E-09D), Telerik.Reporting.Drawing.Unit.Inch(1.3999998569488525D));
            this.subAdjustments.Name = "subAdjustments";
            instanceReportSource7.ReportDocument = this.subAdjustmentsReport2;
            this.subAdjustments.ReportSource = instanceReportSource7;
            this.subAdjustments.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(0.66033762693405151D));
            this.subAdjustments.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subAdjustments.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subAdjustments.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subAdjustments.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subAdjustments.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subAdjustments.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // pnlHeader
            // 
            this.pnlHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.lblLeftHeader,
            this.textBox46,
            this.pictureBox1,
            this.imgGraph,
            this.textBox15});
            this.pnlHeader.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10004011541604996D), Telerik.Reporting.Drawing.Unit.Inch(0.12503938376903534D));
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.70420503616333D), Telerik.Reporting.Drawing.Unit.Inch(4.6614189147949219D));
            // 
            // lblLeftHeader
            // 
            this.lblLeftHeader.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.9365557313431054E-05D));
            this.lblLeftHeader.Name = "lblLeftHeader";
            this.lblLeftHeader.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3083333969116211D), Telerik.Reporting.Drawing.Unit.Pixel(22.636222839355469D));
            this.lblLeftHeader.Style.Font.Bold = true;
            this.lblLeftHeader.Style.Font.Name = "Calibri";
            this.lblLeftHeader.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(16D);
            this.lblLeftHeader.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.lblLeftHeader.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.lblLeftHeader.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblLeftHeader.Value = "Billing Report";
            // 
            // textBox46
            // 
            this.textBox46.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.8957529067993164D), Telerik.Reporting.Drawing.Unit.Inch(0.010456030257046223D));
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7999998331069946D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox46.Style.Font.Bold = true;
            this.textBox46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(16D);
            this.textBox46.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox46.Value = "= GetDate()";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.2999601364135742D), Telerik.Reporting.Drawing.Unit.Inch(0.2999606728553772D));
            this.pictureBox1.MimeType = "image/png";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3999987840652466D), Telerik.Reporting.Drawing.Unit.Inch(0.29408794641494751D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // imgGraph
            // 
            this.imgGraph.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.035376548767089844D), Telerik.Reporting.Drawing.Unit.Inch(1.0155855417251587D));
            this.imgGraph.Name = "imgGraph";
            this.imgGraph.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.59375D), Telerik.Reporting.Drawing.Unit.Pixel(350D));
            this.imgGraph.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            // 
            // textBox15
            // 
            this.textBox15.CanShrink = false;
            this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.68229168653488159D));
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999983787536621D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox15.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox15.Style.Font.Bold = true;
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox15.Value = "Sales Graph";
            // 
            // subReport1
            // 
            this.subReport1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10004011541604996D), Telerik.Reporting.Drawing.Unit.Inch(12.440625190734863D));
            this.subReport1.Name = "subReport1";
            instanceReportSource8.ReportDocument = this.srPageBreack21;
            this.subReport1.ReportSource = instanceReportSource8;
            this.subReport1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999998092651367D), Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D));
            // 
            // txtApiHeader
            // 
            this.txtApiHeader.CanShrink = false;
            this.txtApiHeader.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1041666641831398D), Telerik.Reporting.Drawing.Unit.Inch(10.088541984558106D));
            this.txtApiHeader.Name = "txtApiHeader";
            this.txtApiHeader.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999983787536621D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.txtApiHeader.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.txtApiHeader.Style.Font.Bold = true;
            this.txtApiHeader.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.txtApiHeader.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtApiHeader.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtApiHeader.Value = "API Billing Summary";
            // 
            // subBillingSummaryApi
            // 
            this.subBillingSummaryApi.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1041666641831398D), Telerik.Reporting.Drawing.Unit.Inch(10.661458015441895D));
            this.subBillingSummaryApi.Name = "subBillingSummaryApi";
            instanceReportSource9.ReportDocument = this.subBillingSummary2;
            this.subBillingSummaryApi.ReportSource = instanceReportSource9;
            this.subBillingSummaryApi.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7916667461395264D), Telerik.Reporting.Drawing.Unit.Inch(0.67708331346511841D));
            // 
            // pnlApiProductHead
            // 
            this.pnlApiProductHead.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox17,
            this.textBox18,
            this.textBox19,
            this.textBox20});
            this.pnlApiProductHead.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10003956407308579D), Telerik.Reporting.Drawing.Unit.Inch(10.423879623413086D));
            this.pnlApiProductHead.Name = "pnlApiProductHead";
            this.pnlApiProductHead.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7999999523162842D), Telerik.Reporting.Drawing.Unit.Pixel(23D));
            this.pnlApiProductHead.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(187)))), ((int)(((byte)(187)))));
            this.pnlApiProductHead.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.pnlApiProductHead.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.pnlApiProductHead.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.pnlApiProductHead.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.pnlApiProductHead.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.pnlApiProductHead.Style.Font.Name = "Calibri";
            this.pnlApiProductHead.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.pnlApiProductHead.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.pnlApiProductHead.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.pnlApiProductHead.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pnlApiProductHead.Style.Visible = true;
            // 
            // textBox17
            // 
            this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.3958332538604736D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.39996027946472168D), Telerik.Reporting.Drawing.Unit.Pixel(22.996223449707031D));
            this.textBox17.Style.Font.Bold = true;
            this.textBox17.Style.Font.Name = "Calibri";
            this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox17.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox17.Value = "Price";
            // 
            // textBox18
            // 
            this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.59999990463256836D), Telerik.Reporting.Drawing.Unit.Pixel(22.996223449707031D));
            this.textBox18.Style.Font.Bold = true;
            this.textBox18.Style.Font.Name = "Calibri";
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox18.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox18.Value = "Quantity";
            // 
            // textBox19
            // 
            this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4999605417251587D), Telerik.Reporting.Drawing.Unit.Pixel(22.636222839355469D));
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Style.Font.Name = "Calibri";
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox19.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox19.Value = "Report Type";
            // 
            // textBox20
            // 
            this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.59375D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79999971389770508D), Telerik.Reporting.Drawing.Unit.Pixel(22.996223449707031D));
            this.textBox20.Style.Font.Bold = true;
            this.textBox20.Style.Font.Name = "Calibri";
            this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox20.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox20.Value = "Data Source";
            // 
            // pnlContainer
            // 
            this.pnlContainer.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.panel6,
            this.subProductsApi,
            this.pnlAdjustmentApi,
            this.subAdjustmentsApi});
            this.pnlContainer.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9541270732879639D), Telerik.Reporting.Drawing.Unit.Inch(10.421875D));
            this.pnlContainer.Name = "pnlContainer";
            this.pnlContainer.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8416664600372314D), Telerik.Reporting.Drawing.Unit.Inch(1.9728375673294067D));
            // 
            // panel6
            // 
            this.panel6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox21,
            this.textBox22,
            this.textBox23,
            this.textBox24});
            this.panel6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(0.03125D));
            this.panel6.Name = "panel6";
            this.panel6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7999999523162842D), Telerik.Reporting.Drawing.Unit.Pixel(23D));
            this.panel6.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(187)))), ((int)(((byte)(187)))));
            this.panel6.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.panel6.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.panel6.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.panel6.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.panel6.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.panel6.Style.Font.Name = "Calibri";
            this.panel6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.panel6.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.panel6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.panel6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.panel6.Style.Visible = true;
            // 
            // textBox21
            // 
            this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.3958332538604736D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.39996027946472168D), Telerik.Reporting.Drawing.Unit.Pixel(22.996223449707031D));
            this.textBox21.Style.Font.Bold = true;
            this.textBox21.Style.Font.Name = "Calibri";
            this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox21.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox21.Value = "Price";
            // 
            // textBox22
            // 
            this.textBox22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.59999990463256836D), Telerik.Reporting.Drawing.Unit.Pixel(22.996223449707031D));
            this.textBox22.Style.Font.Bold = true;
            this.textBox22.Style.Font.Name = "Calibri";
            this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox22.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox22.Value = "Quantity";
            // 
            // textBox23
            // 
            this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4999605417251587D), Telerik.Reporting.Drawing.Unit.Pixel(22.636222839355469D));
            this.textBox23.Style.Font.Bold = true;
            this.textBox23.Style.Font.Name = "Calibri";
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox23.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox23.Value = "Products";
            // 
            // textBox24
            // 
            this.textBox24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.59375D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79999971389770508D), Telerik.Reporting.Drawing.Unit.Pixel(22.996223449707031D));
            this.textBox24.Style.Font.Bold = true;
            this.textBox24.Style.Font.Name = "Calibri";
            this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox24.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox24.Value = "Data Source";
            // 
            // subProductsApi
            // 
            this.subProductsApi.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(0.2708333432674408D));
            this.subProductsApi.Name = "subProductsApi";
            instanceReportSource10.ReportDocument = this.subProducts2;
            this.subProductsApi.ReportSource = instanceReportSource10;
            this.subProductsApi.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8020832538604736D), Telerik.Reporting.Drawing.Unit.Inch(0.66666668653488159D));
            // 
            // pnlAdjustmentApi
            // 
            this.pnlAdjustmentApi.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox25,
            this.textBox26,
            this.textBox27,
            this.textBox28});
            this.pnlAdjustmentApi.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(1.03125D));
            this.pnlAdjustmentApi.Name = "pnlAdjustmentApi";
            this.pnlAdjustmentApi.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8000001907348633D), Telerik.Reporting.Drawing.Unit.Pixel(23D));
            this.pnlAdjustmentApi.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(187)))), ((int)(((byte)(187)))));
            this.pnlAdjustmentApi.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.pnlAdjustmentApi.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.pnlAdjustmentApi.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.pnlAdjustmentApi.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.pnlAdjustmentApi.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.pnlAdjustmentApi.Style.Font.Name = "Calibri";
            this.pnlAdjustmentApi.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.pnlAdjustmentApi.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.pnlAdjustmentApi.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.pnlAdjustmentApi.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pnlAdjustmentApi.Style.Visible = true;
            // 
            // textBox25
            // 
            this.textBox25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.3958332538604736D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.39996027946472168D), Telerik.Reporting.Drawing.Unit.Pixel(22.996223449707031D));
            this.textBox25.Style.Font.Bold = true;
            this.textBox25.Style.Font.Name = "Calibri";
            this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox25.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox25.Value = "Price";
            // 
            // textBox26
            // 
            this.textBox26.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.59999990463256836D), Telerik.Reporting.Drawing.Unit.Pixel(22.996223449707031D));
            this.textBox26.Style.Font.Bold = true;
            this.textBox26.Style.Font.Name = "Calibri";
            this.textBox26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox26.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox26.Value = "Quantity";
            // 
            // textBox27
            // 
            this.textBox27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4999605417251587D), Telerik.Reporting.Drawing.Unit.Pixel(22.636222839355469D));
            this.textBox27.Style.Font.Bold = true;
            this.textBox27.Style.Font.Name = "Calibri";
            this.textBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox27.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox27.Value = "Products";
            // 
            // textBox28
            // 
            this.textBox28.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.59375D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79999971389770508D), Telerik.Reporting.Drawing.Unit.Pixel(22.996223449707031D));
            this.textBox28.Style.Font.Bold = true;
            this.textBox28.Style.Font.Name = "Calibri";
            this.textBox28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox28.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox28.Value = "Data Source";
            // 
            // subAdjustmentsApi
            // 
            this.subAdjustmentsApi.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0250396728515625D), Telerik.Reporting.Drawing.Unit.Inch(1.2708333730697632D));
            this.subAdjustmentsApi.Name = "subAdjustmentsApi";
            instanceReportSource11.ReportDocument = this.subAdjustmentsReport1;
            this.subAdjustmentsApi.ReportSource = instanceReportSource11;
            this.subAdjustmentsApi.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7916667461395264D), Telerik.Reporting.Drawing.Unit.Inch(0.61458331346511841D));
            // 
            // subReport2
            // 
            this.subReport2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1458333283662796D), Telerik.Reporting.Drawing.Unit.Inch(9.9010419845581055D));
            this.subReport2.Name = "subReport2";
            instanceReportSource12.ReportDocument = this.srPageBreack2;
            this.subReport2.ReportSource = instanceReportSource12;
            this.subReport2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6041665077209473D), Telerik.Reporting.Drawing.Unit.Inch(0.1354166716337204D));
            // 
            // textBox16
            // 
            this.textBox16.CanShrink = false;
            this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.098958373069763184D), Telerik.Reporting.Drawing.Unit.Inch(14.083333969116211D));
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999983787536621D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox16.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox16.Style.Font.Bold = true;
            this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox16.Value = "InActive Companies (1 Month)";
            // 
            // subCompanyOneMonth
            // 
            this.subCompanyOneMonth.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.098958373069763184D), Telerik.Reporting.Drawing.Unit.Inch(14.385416984558106D));
            this.subCompanyOneMonth.Name = "subCompanyOneMonth";
            instanceReportSource13.ReportDocument = this.subCompanyTransactions2;
            this.subCompanyOneMonth.ReportSource = instanceReportSource13;
            this.subCompanyOneMonth.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.69999885559082D), Telerik.Reporting.Drawing.Unit.Inch(0.66033762693405151D));
            this.subCompanyOneMonth.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyOneMonth.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyOneMonth.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyOneMonth.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyOneMonth.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyOneMonth.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // subReport3
            // 
            this.subReport3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.098958373069763184D), Telerik.Reporting.Drawing.Unit.Inch(13.989583969116211D));
            this.subReport3.Name = "subReport3";
            instanceReportSource14.ReportDocument = this.srPageBreack21;
            this.subReport3.ReportSource = instanceReportSource14;
            this.subReport3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999998092651367D), Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D));
            // 
            // textBox29
            // 
            this.textBox29.CanShrink = false;
            this.textBox29.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.098958373069763184D), Telerik.Reporting.Drawing.Unit.Inch(15.223958969116211D));
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999983787536621D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox29.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox29.Style.Font.Bold = true;
            this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox29.Value = "InActive Companies (3 Months)";
            // 
            // subCompanyThreeMonth
            // 
            this.subCompanyThreeMonth.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.098958373069763184D), Telerik.Reporting.Drawing.Unit.Inch(15.526041984558106D));
            this.subCompanyThreeMonth.Name = "subCompanyThreeMonth";
            instanceReportSource15.ReportDocument = this.subCompanyTransactions3;
            this.subCompanyThreeMonth.ReportSource = instanceReportSource15;
            this.subCompanyThreeMonth.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.69999885559082D), Telerik.Reporting.Drawing.Unit.Inch(0.66033762693405151D));
            this.subCompanyThreeMonth.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyThreeMonth.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyThreeMonth.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyThreeMonth.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyThreeMonth.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyThreeMonth.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // subReport5
            // 
            this.subReport5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.098958373069763184D), Telerik.Reporting.Drawing.Unit.Inch(15.130208969116211D));
            this.subReport5.Name = "subReport5";
            instanceReportSource16.ReportDocument = this.srPageBreack21;
            this.subReport5.ReportSource = instanceReportSource16;
            this.subReport5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999998092651367D), Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D));
            // 
            // textBox30
            // 
            this.textBox30.CanShrink = false;
            this.textBox30.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.098958373069763184D), Telerik.Reporting.Drawing.Unit.Inch(16.364583969116211D));
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999983787536621D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox30.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox30.Style.Font.Bold = true;
            this.textBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox30.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox30.Value = "InActive Companies (6 Months)";
            // 
            // subCompanySixMonth
            // 
            this.subCompanySixMonth.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.098958373069763184D), Telerik.Reporting.Drawing.Unit.Inch(16.666666030883789D));
            this.subCompanySixMonth.Name = "subCompanySixMonth";
            instanceReportSource17.ReportDocument = this.subCompanyTransactions4;
            this.subCompanySixMonth.ReportSource = instanceReportSource17;
            this.subCompanySixMonth.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.69999885559082D), Telerik.Reporting.Drawing.Unit.Inch(0.66033762693405151D));
            this.subCompanySixMonth.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanySixMonth.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanySixMonth.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanySixMonth.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanySixMonth.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanySixMonth.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // subReport7
            // 
            this.subReport7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.098958373069763184D), Telerik.Reporting.Drawing.Unit.Inch(16.270833969116211D));
            this.subReport7.Name = "subReport7";
            instanceReportSource18.ReportDocument = this.srPageBreack21;
            this.subReport7.ReportSource = instanceReportSource18;
            this.subReport7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999998092651367D), Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D));
            // 
            // textBox31
            // 
            this.textBox31.CanShrink = false;
            this.textBox31.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.11458337306976318D), Telerik.Reporting.Drawing.Unit.Inch(17.489583969116211D));
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999983787536621D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox31.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox31.Style.Font.Bold = true;
            this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox31.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox31.Value = "InActive Companies (12+ Months)";
            // 
            // subCompanyTwelveMonth
            // 
            this.subCompanyTwelveMonth.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.11458337306976318D), Telerik.Reporting.Drawing.Unit.Inch(17.791666030883789D));
            this.subCompanyTwelveMonth.Name = "subCompanyTwelveMonth";
            instanceReportSource19.ReportDocument = this.subCompanyTransactions5;
            this.subCompanyTwelveMonth.ReportSource = instanceReportSource19;
            this.subCompanyTwelveMonth.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.69999885559082D), Telerik.Reporting.Drawing.Unit.Inch(0.66033762693405151D));
            this.subCompanyTwelveMonth.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyTwelveMonth.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyTwelveMonth.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyTwelveMonth.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyTwelveMonth.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyTwelveMonth.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // subReport9
            // 
            this.subReport9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.11458337306976318D), Telerik.Reporting.Drawing.Unit.Inch(17.395833969116211D));
            this.subReport9.Name = "subReport9";
            instanceReportSource20.ReportDocument = this.srPageBreack21;
            this.subReport9.ReportSource = instanceReportSource20;
            this.subReport9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999998092651367D), Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D));
            // 
            // subReport11
            // 
            this.subReport11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.098958373069763184D), Telerik.Reporting.Drawing.Unit.Inch(18.520833969116211D));
            this.subReport11.Name = "subReport11";
            instanceReportSource21.ReportDocument = this.srPageBreack21;
            this.subReport11.ReportSource = instanceReportSource21;
            this.subReport11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999998092651367D), Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D));
            // 
            // subCompanyInCollection
            // 
            this.subCompanyInCollection.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.098958373069763184D), Telerik.Reporting.Drawing.Unit.Inch(18.916666030883789D));
            this.subCompanyInCollection.Name = "subCompanyInCollection";
            instanceReportSource22.ReportDocument = this.subCompanyTransactions6;
            this.subCompanyInCollection.ReportSource = instanceReportSource22;
            this.subCompanyInCollection.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.69999885559082D), Telerik.Reporting.Drawing.Unit.Inch(0.66033762693405151D));
            this.subCompanyInCollection.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyInCollection.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyInCollection.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyInCollection.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyInCollection.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyInCollection.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // textBox32
            // 
            this.textBox32.CanShrink = false;
            this.textBox32.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.098958373069763184D), Telerik.Reporting.Drawing.Unit.Inch(18.614583969116211D));
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999983787536621D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox32.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox32.Style.Font.Bold = true;
            this.textBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox32.Value = "InCollection Companies";
            // 
            // subReport13
            // 
            this.subReport13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.098958373069763184D), Telerik.Reporting.Drawing.Unit.Inch(19.645833969116211D));
            this.subReport13.Name = "subReport13";
            instanceReportSource23.ReportDocument = this.srPageBreack21;
            this.subReport13.ReportSource = instanceReportSource23;
            this.subReport13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999998092651367D), Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D));
            // 
            // subCompanyPastDue
            // 
            this.subCompanyPastDue.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.098958373069763184D), Telerik.Reporting.Drawing.Unit.Inch(20.041667938232422D));
            this.subCompanyPastDue.Name = "subCompanyPastDue";
            instanceReportSource24.ReportDocument = this.subCompanyTransactions7;
            this.subCompanyPastDue.ReportSource = instanceReportSource24;
            this.subCompanyPastDue.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.69999885559082D), Telerik.Reporting.Drawing.Unit.Inch(0.66033762693405151D));
            this.subCompanyPastDue.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyPastDue.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyPastDue.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyPastDue.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyPastDue.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyPastDue.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // textBox33
            // 
            this.textBox33.CanShrink = false;
            this.textBox33.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.098958373069763184D), Telerik.Reporting.Drawing.Unit.Inch(19.739583969116211D));
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999983787536621D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox33.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox33.Style.Font.Bold = true;
            this.textBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox33.Value = "Past Due Companies";
            // 
            // textBox34
            // 
            this.textBox34.CanShrink = false;
            this.textBox34.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.11458337306976318D), Telerik.Reporting.Drawing.Unit.Inch(12.890625D));
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999983787536621D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox34.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox34.Style.Font.Bold = true;
            this.textBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox34.Value = "Active Companies";
            // 
            // textBox35
            // 
            this.textBox35.CanShrink = false;
            this.textBox35.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.093750119209289551D), Telerik.Reporting.Drawing.Unit.Inch(20.848958969116211D));
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999983787536621D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox35.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox35.Style.Font.Bold = true;
            this.textBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox35.Value = "Locked Companies";
            // 
            // subCompanyLocked
            // 
            this.subCompanyLocked.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.093750119209289551D), Telerik.Reporting.Drawing.Unit.Inch(21.151041030883789D));
            this.subCompanyLocked.Name = "subCompanyLocked";
            instanceReportSource25.ReportDocument = this.subCompanyTransactions8;
            this.subCompanyLocked.ReportSource = instanceReportSource25;
            this.subCompanyLocked.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.69999885559082D), Telerik.Reporting.Drawing.Unit.Inch(0.66033762693405151D));
            this.subCompanyLocked.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyLocked.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyLocked.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyLocked.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyLocked.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subCompanyLocked.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // subReport16
            // 
            this.subReport16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.093750119209289551D), Telerik.Reporting.Drawing.Unit.Inch(20.755208969116211D));
            this.subReport16.Name = "subReport16";
            instanceReportSource26.ReportDocument = this.srPageBreack21;
            this.subReport16.ReportSource = instanceReportSource26;
            this.subReport16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999998092651367D), Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D));
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.49930468201637268D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.panel5});
            this.pageFooterSection1.Name = "pageFooterSection1";
            this.pageFooterSection1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            // 
            // panel5
            // 
            this.panel5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox74,
            this.textBox75});
            this.panel5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1458333283662796D), Telerik.Reporting.Drawing.Unit.Inch(0.057638388127088547D));
            this.panel5.Name = "panel5";
            this.panel5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999540328979492D), Telerik.Reporting.Drawing.Unit.Inch(0.39999961853027344D));
            this.panel5.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Ridge;
            this.panel5.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Ridge;
            // 
            // textBox74
            // 
            this.textBox74.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.099997840821743011D), Telerik.Reporting.Drawing.Unit.Inch(0.099999748170375824D));
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.49999991059303284D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox74.Style.Font.Bold = true;
            this.textBox74.Style.Font.Name = "Calibri";
            this.textBox74.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.textBox74.Value = "= PageNumber";
            // 
            // textBox75
            // 
            this.textBox75.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.4999980926513672D), Telerik.Reporting.Drawing.Unit.Inch(0.099999748170375824D));
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1000397205352783D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox75.Style.Font.Bold = true;
            this.textBox75.Style.Font.Name = "Calibri";
            this.textBox75.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.textBox75.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox75.Value = "= Now().ToString(\"MMMM dd, yyyy hh:mm tt\")";
            // 
            // BillingReport
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail,
            this.pageFooterSection1});
            this.Name = "BillingReport";
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Pixel(0D), Telerik.Reporting.Drawing.Unit.Pixel(0D), Telerik.Reporting.Drawing.Unit.Pixel(0D), Telerik.Reporting.Drawing.Unit.Pixel(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.UnitOfMeasure = Telerik.Reporting.Drawing.UnitType.Inch;
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(8D);
            this.NeedDataSource += new System.EventHandler(this.BillingReport_NeedDataSource);
            ((System.ComponentModel.ISupportInitialize)(this.subBillingSummary1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subCompanyTransactions1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subSalesTotals1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subVolumeTotals1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.srPageBreack1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subProducts1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subAdjustmentsReport2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.srPageBreack21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subBillingSummary2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subProducts2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subAdjustmentsReport1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.srPageBreack2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subCompanyTransactions2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subCompanyTransactions3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subCompanyTransactions4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subCompanyTransactions5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subCompanyTransactions6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subCompanyTransactions7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subCompanyTransactions8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.SubReport subBillingSummary;
        private Telerik.Reporting.SubReport subProducts;
        private SubBillingSummary subBillingSummary1;
        private SubProducts subProducts1;
        public Telerik.Reporting.Panel panel1;
        public Telerik.Reporting.TextBox textBox5;
        public Telerik.Reporting.TextBox textBox4;
        public Telerik.Reporting.TextBox textBox3;
        public Telerik.Reporting.TextBox textBox2;
        public Telerik.Reporting.Panel panel2;
        public Telerik.Reporting.TextBox textBox6;
        public Telerik.Reporting.TextBox textBox7;
        public Telerik.Reporting.TextBox textBox8;
        public Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.SubReport subAdjustments;
        public Telerik.Reporting.Panel lblAdjustmentHeading;
        public Telerik.Reporting.TextBox textBox10;
        public Telerik.Reporting.TextBox textBox11;
        public Telerik.Reporting.TextBox textBox12;
        public Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.Panel panel5;
        private Telerik.Reporting.TextBox textBox74;
        private Telerik.Reporting.TextBox textBox75;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.SubReport subCompanyTransactions;
        private SubCompanyTransactions subCompanyTransactions1;
        private Telerik.Reporting.TextBox lblSalesTotal;
        private Telerik.Reporting.SubReport subSales;
        private SubSalesTotals subSalesTotals1;
        private Telerik.Reporting.TextBox lblVolumeLable;
        private Telerik.Reporting.SubReport subVolume;
        private Telerik.Reporting.SubReport subPageBreack;
        private SRPageBreack srPageBreack1;
        private SubVolumeTotals subVolumeTotals1;
        private Telerik.Reporting.Panel panel4;
        private Telerik.Reporting.Panel pnlHeader;
        public Telerik.Reporting.TextBox lblLeftHeader;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.PictureBox pictureBox1;

        #region Custom Functions
        public static string GetDate()
        {
            string month = !string.IsNullOrEmpty(Month) ? Month : string.Empty;
            if (!string.IsNullOrEmpty(Month))
            {
                int months = DateTime.Now.AddMonths(-int.Parse(month)).Month;
                DateTime dt = DateTime.Now.AddMonths(-months);
                month = dt.ToString("MMMM");
            }
            string year = !string.IsNullOrEmpty(Year) ? Year : string.Empty;
            string date = month + ", " + year;
            return date;
        }
        #endregion

        private Telerik.Reporting.SubReport subReport1;
        private SRPageBreack2 srPageBreack21;
        private Telerik.Reporting.PictureBox imgGraph;
        private Telerik.Reporting.TextBox txtApiHeader;
        private Telerik.Reporting.SubReport subBillingSummaryApi;
        public Telerik.Reporting.Panel pnlApiProductHead;
        public Telerik.Reporting.TextBox textBox17;
        public Telerik.Reporting.TextBox textBox18;
        public Telerik.Reporting.TextBox textBox19;
        public Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.Panel pnlContainer;
        public Telerik.Reporting.Panel panel6;
        public Telerik.Reporting.TextBox textBox21;
        public Telerik.Reporting.TextBox textBox22;
        public Telerik.Reporting.TextBox textBox23;
        public Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.SubReport subProductsApi;
        private SubProducts subProducts2;
        public Telerik.Reporting.Panel pnlAdjustmentApi;
        public Telerik.Reporting.TextBox textBox25;
        public Telerik.Reporting.TextBox textBox26;
        public Telerik.Reporting.TextBox textBox27;
        public Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.SubReport subAdjustmentsApi;
        private SubAdjustmentsReport subAdjustmentsReport1;
        private SubAdjustmentsReport subAdjustmentsReport2;
        private Telerik.Reporting.SubReport subReport2;
        private SRPageBreack srPageBreack2;
        private SubBillingSummary subBillingSummary2;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.SubReport subCompanyOneMonth;
        private SubCompanyTransactions subCompanyTransactions2;
        private Telerik.Reporting.SubReport subReport3;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.SubReport subCompanyThreeMonth;
        private SubCompanyTransactions subCompanyTransactions3;
        private Telerik.Reporting.SubReport subReport5;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.SubReport subCompanySixMonth;
        private SubCompanyTransactions subCompanyTransactions4;
        private Telerik.Reporting.SubReport subReport7;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.SubReport subCompanyTwelveMonth;
        private SubCompanyTransactions subCompanyTransactions5;
        private Telerik.Reporting.SubReport subReport9;
        private Telerik.Reporting.SubReport subReport11;
        private Telerik.Reporting.SubReport subCompanyInCollection;
        private SubCompanyTransactions subCompanyTransactions6;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.SubReport subReport13;
        private Telerik.Reporting.SubReport subCompanyPastDue;
        private SubCompanyTransactions subCompanyTransactions7;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.SubReport subCompanyLocked;
        private SubCompanyTransactions subCompanyTransactions8;
        private Telerik.Reporting.SubReport subReport16;
    }
}