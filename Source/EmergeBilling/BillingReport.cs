namespace EmergeBilling
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using System.Linq;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;
    using System.Collections.Generic;
    using Emerge.Services;
    using Emerge.Data;
    using System.Net;
    using System.IO;

    /// <summary>
    /// Summary description for BillingReport.
    /// </summary>
    public partial class BillingReport : Telerik.Reporting.Report
    {
        public Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALProductsPerLocation>, List<List<BALReportsByLocation>>, List<proc_GetBillingAdjustmentsResult>, Totals, Totals> ObjData { get; set; }
        public Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALReportsByLocation>, List<proc_GetBillingAdjustmentsResult>> ObjDataApi { get; set; }
        public int CompanyId { get; set; }
        public bool IsSalesReport { get; set; }
        public BillingReport()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        private void BillingReport_NeedDataSource(object sender, EventArgs e)
        {
            List<BALBillingSummary> ObjBillingSummary = new List<BALBillingSummary>();
            List<BALBillingSummary> ObjProducts = new List<BALBillingSummary>();
            List<proc_GetBillingAdjustmentsResult> ObjAdjustmentReports = new List<proc_GetBillingAdjustmentsResult>();
            List<List<BALReportsByLocation>> ObjCmpTransactions = new List<List<BALReportsByLocation>>();
            Totals ObjSalesReport = new Totals();
            Totals ObjVolumeReport = new Totals();
            List<BALBillingSummary> ObjBillingSummaryApi = new List<BALBillingSummary>();
            List<BALBillingSummary> ObjProductsApi = new List<BALBillingSummary>();
            List<proc_GetBillingAdjustmentsResult> ObjAdjustmentReportsApi = new List<proc_GetBillingAdjustmentsResult>();
            //List<BALCompanyLocations> ObjBALAllCompanyLocations = new List<BALCompanyLocations>();
            List<BALReportsByLocation> ObjBALReportsByLocation = new List<BALReportsByLocation>();
            List<BALReportsByLocation> ObjBALReportsByLocationOneMonth = new List<BALReportsByLocation>();
            List<BALReportsByLocation> ObjBALReportsByLocationThreeMonth = new List<BALReportsByLocation>();
            List<BALReportsByLocation> ObjBALReportsByLocationSixMonth = new List<BALReportsByLocation>();
            List<BALReportsByLocation> ObjBALReportsByLocationTwelveMonth = new List<BALReportsByLocation>();
            List<BALReportsByLocation> ObjBALReportsByLocationInCollection = new List<BALReportsByLocation>();
            List<BALReportsByLocation> ObjBALReportsByLocationPastDue = new List<BALReportsByLocation>();
            List<BALReportsByLocation> ObjBALReportsByLocationLocked = new List<BALReportsByLocation>();

            if (ObjData != null)
            {
                if (ObjData.Item1 != null)
                {
                    ObjBillingSummary = ObjData.Item1;
                }
                if (ObjData.Item2 != null)
                {
                    ObjProducts = ObjData.Item2;
                }
                if (ObjData.Item4 != null)
                {
                    ObjCmpTransactions = ObjData.Item4;

                    if (ObjCmpTransactions.ElementAt(0).Count > 0)
                    {
                        ObjBALReportsByLocation = ObjCmpTransactions.ElementAt(0).ToList();
                    }
                    if (ObjCmpTransactions.ElementAt(1).Count > 0)
                    {
                        ObjBALReportsByLocationOneMonth = ObjCmpTransactions.ElementAt(1).ToList();
                    }
                    if (ObjCmpTransactions.ElementAt(2).Count > 0)
                    {
                        ObjBALReportsByLocationThreeMonth = ObjCmpTransactions.ElementAt(2).ToList();
                    }
                    if (ObjCmpTransactions.ElementAt(3).Count > 0)
                    {
                        ObjBALReportsByLocationSixMonth = ObjCmpTransactions.ElementAt(3).ToList();
                    }
                    if (ObjCmpTransactions.ElementAt(4).Count > 0)
                    {
                        ObjBALReportsByLocationTwelveMonth = ObjCmpTransactions.ElementAt(4).ToList();
                    }
                    if (ObjCmpTransactions.ElementAt(5).Count > 0)
                    {
                        ObjBALReportsByLocationInCollection = ObjCmpTransactions.ElementAt(5).ToList();
                    }
                    if (ObjCmpTransactions.ElementAt(6).Count > 0)
                    {
                        ObjBALReportsByLocationPastDue = ObjCmpTransactions.ElementAt(6).ToList();
                    }
                    if (ObjCmpTransactions.ElementAt(7).Count > 0)
                    {
                        ObjBALReportsByLocationLocked = ObjCmpTransactions.ElementAt(7).ToList();
                    }
                }
                if (ObjData.Item5 != null)
                {
                    ObjAdjustmentReports = ObjData.Item5;
                }
                if (ObjData.Item6 != null)
                {
                    ObjSalesReport = ObjData.Item6;
                }
                if (ObjData.Item7 != null)
                {
                    ObjVolumeReport = ObjData.Item7;
                }
            }

            Telerik.Reporting.Report MainRpt = (sender as Telerik.Reporting.Report);
            Telerik.Reporting.SubReport SubBillingSummary = (Telerik.Reporting.SubReport)(detail.Items[1]);
            Telerik.Reporting.SubReport SubProducts = (Telerik.Reporting.SubReport)(panel4.Items[0]);
            Telerik.Reporting.SubReport SubAdjustmentReport = (Telerik.Reporting.SubReport)(panel4.Items[3]);
            Telerik.Reporting.SubReport SubCompanyTransactions = (Telerik.Reporting.SubReport)(detail.Items[4]);
            Telerik.Reporting.SubReport SubSales = (Telerik.Reporting.SubReport)(detail.Items[6]);
            Telerik.Reporting.SubReport SubVolume = (Telerik.Reporting.SubReport)(detail.Items[8]);
            Telerik.Reporting.Panel lblAdjustmentHeading = (Telerik.Reporting.Panel)(panel4.Items[1]);
            Telerik.Reporting.SubReport SubBillingSummaryApi = (Telerik.Reporting.SubReport)(subBillingSummaryApi);
            Telerik.Reporting.SubReport SubProductsApi = (Telerik.Reporting.SubReport)(subProductsApi);
            Telerik.Reporting.SubReport SubAdjustmentsApi = (Telerik.Reporting.SubReport)(subAdjustmentsApi);
            if (ObjBillingSummary.Count > 0)
            {
                subBillingSummary1.DataSource = ObjBillingSummary;
            }
            else
            {
                SubBillingSummary.Visible = false;
            }
            if (ObjProducts.Count > 0)
            {
                subProducts1.DataSource = ObjProducts;
            }
            else
            {
                SubProducts.Visible = false;
            }
            if (ObjAdjustmentReports.Count > 0)
            {
                subAdjustmentsReport2.DataSource = ObjAdjustmentReports;
            }
            else
            {
                SubAdjustmentReport.Visible = false;
                lblAdjustmentHeading.Visible = false;
            }
            if (ObjBALReportsByLocation.Count > 0)
            {
                subCompanyTransactions1.DataSource = ObjBALReportsByLocation;
            }
            else
            {
                subCompanyTransactions1.Visible = false;
            }
            if (ObjBALReportsByLocationOneMonth.Count > 0)
            {
                subCompanyTransactions2.DataSource = ObjBALReportsByLocationOneMonth;
            }
            else
            {
                subCompanyTransactions2.Visible = false;
            }
            if (ObjBALReportsByLocationThreeMonth.Count > 0)
            {
                subCompanyTransactions3.DataSource = ObjBALReportsByLocationThreeMonth;
            }
            else
            {
                subCompanyTransactions3.Visible = false;
            }
            if (ObjBALReportsByLocationSixMonth.Count > 0)
            {
                subCompanyTransactions4.DataSource = ObjBALReportsByLocationSixMonth;
            }
            else
            {
                subCompanyTransactions4.Visible = false;
            }
            if (ObjBALReportsByLocationTwelveMonth.Count > 0)
            {
                subCompanyTransactions5.DataSource = ObjBALReportsByLocationTwelveMonth;
            }
            else
            {
                subCompanyTransactions5.Visible = false;
            }
            if (ObjBALReportsByLocationInCollection.Count > 0)
            {
                subCompanyTransactions6.DataSource = ObjBALReportsByLocationInCollection;
            }
            else
            {
                subCompanyTransactions6.Visible = false;
            }
            if (ObjBALReportsByLocationPastDue.Count > 0)
            {
                subCompanyTransactions7.DataSource = ObjBALReportsByLocationPastDue;
            }
            else
            {
                subCompanyTransactions7.Visible = false;
            }
            if (ObjBALReportsByLocationLocked.Count > 0)
            {
                subCompanyTransactions8.DataSource = ObjBALReportsByLocationLocked;
            }
            else
            {
                subCompanyTransactions8.Visible = false;
            }

            if (CompanyId == 0)
            {
                SubSales.Visible = false;
                SubVolume.Visible = false;
                lblSalesTotal.Visible = false;
                lblVolumeLable.Visible = false;
                subPageBreack.Visible = false;
                pnlHeader.Visible = false;
            }
            else
            {
                if (ObjSalesReport != null)
                {
                    subSalesTotals1.DataSource = ObjSalesReport;
                }
                else
                {
                    SubSales.Visible = false;
                }
                if (ObjVolumeReport != null)
                {
                    subVolumeTotals1.DataSource = ObjVolumeReport;
                }
                else
                {
                    subVolume.Visible = false;
                }
                string FileName = GenerateImage();
                imgGraph.Value = System.Web.HttpContext.Current.Server.MapPath("~/Resources/Upload/" + FileName);
            }
            if (IsSalesReport)
            {
                lblLeftHeader.Value = "Sales Report";
                txtApiHeader.Visible = false;
                pnlApiProductHead.Visible = false;
                pnlContainer.Visible = false;
                subBillingSummaryApi.Visible = false;
                subReport2.Visible = false;
            }
            else
            {
                if (ObjDataApi != null)
                {
                    if (ObjDataApi.Item1 != null)
                    {
                        ObjBillingSummaryApi = ObjDataApi.Item1;
                    }
                    if (ObjDataApi.Item2 != null)
                    {
                        ObjProductsApi = ObjDataApi.Item2;
                    }
                    if (ObjDataApi.Item4 != null)
                    {
                        ObjAdjustmentReportsApi = ObjDataApi.Item4;
                    }

                }
                if (ObjBillingSummaryApi.Count > 0)
                {
                    subBillingSummary2.DataSource = ObjBillingSummaryApi;
                }
                else
                {
                    SubBillingSummaryApi.Visible = false;
                }
                if (ObjProductsApi.Count > 0)
                {
                    subProducts2.DataSource = ObjProductsApi;
                }
                else
                {
                    SubProductsApi.Visible = false;
                }
                if (ObjAdjustmentReportsApi.Count > 0)
                {
                    subAdjustmentsReport1.DataSource = ObjAdjustmentReports;
                }
                else
                {
                    SubAdjustmentsApi.Visible = false;
                    pnlAdjustmentApi.Visible = false;
                }
            }

        }

        #region Generate Graph Image
        public string GenerateGraphImage()
        {
            string FilePath = string.Empty;
            #region Generate Graph
            BALBillingData ObjBALBillingData = new BALBillingData();
            string strMonthYear = Month + "/" + Year;
            DateTime MonthYear = Convert.ToDateTime(strMonthYear);

            DateTime ObjDateTime = MonthYear.AddMonths(-11);
            string ProductData = "";
            string ReportData = "";
            string TotalData = "";
            string XvalueText = "|";
            decimal ProductMaxValue = 0;
            decimal CurrentProductMaxValue = 0;
            decimal ReportMaxValue = 0;
            decimal CurrentReportMaxValue = 0;
            decimal TotalMaxValue = 0;
            decimal CurrentTotalMaxValue = 0;

            string ProductOldData = "";
            string ReportOldData = "";
            string TotalOldData = "";

            //decimal TotalSales = 0, ReportSales = 0, ProductSales = 0;
            //decimal TotalVolume = 0, ReportVolume = 0, ProductVolume = 0;

            //decimal PreviousTotalSales = 0, PreviousReportSales = 0, PreviousProductSales = 0;
            //decimal PreviousTotalVolume = 0, PreviousReportVolume = 0, PreviousProductVolume = 0;

            //decimal TotalSalesPercentage = 0;
            //decimal ReportSalesPercentage = 0;
            //decimal ProductSalesPercentage = 0;
            //decimal TotalVolumePercentage = 0, ReportVolumePercentage = 0, ProductVolumePercentage = 0;

            var Obj = ObjBALBillingData.GetSalesValuesforGraphMVC(CompanyId, 11);

            for (int i = 0; i < Obj.Count; i++)
            {
                //BALCompany ObjBALCompany = new BALCompany();
                //var Obj = ObjBALCompany.GetSalesValuesforGraph(new Guid(ddlCompany.SelectedValue), Convert.ToInt16(ObjDateTime.AddMonths(i).Month), Convert.ToInt16(ObjDateTime.AddMonths(i).Year));
                //if (i == 10)
                //{
                //    var ObjPreviousVolumeData = ObjBALCompany.GetVolumeValuesforGraph(CompanyId, Convert.ToInt16(ObjDateTime.AddMonths(i).Month), Convert.ToInt16(ObjDateTime.AddMonths(i).Year));
                //    PreviousTotalSales = Convert.ToDecimal(Obj.ElementAt(i).TotalAmount);
                //    PreviousReportSales = Convert.ToDecimal(Obj.ElementAt(i).ReportAmount);
                //    PreviousProductSales = Convert.ToDecimal(Obj.ElementAt(i).ProductAmount);

                //    PreviousTotalVolume = Convert.ToDecimal(ObjPreviousVolumeData.ElementAt(i).TotalAmount);
                //    PreviousReportVolume = Convert.ToDecimal(ObjPreviousVolumeData.ElementAt(i).ReportAmount);
                //    PreviousProductVolume = Convert.ToDecimal(ObjPreviousVolumeData.ElementAt(i).ProductAmount);
                //}
                //if (i == 11)
                //{
                //    var ObjVolume = ObjBALCompany.GetVolumeValuesforGraph(CompanyId, Convert.ToInt16(ObjDateTime.AddMonths(i).Month), Convert.ToInt16(ObjDateTime.AddMonths(i).Year));
                //    TotalSales = Convert.ToDecimal(Obj.ElementAt(i).TotalAmount);
                //    ReportSales = Convert.ToDecimal(Obj.ElementAt(i).ReportAmount);
                //    ProductSales = Convert.ToDecimal(Obj.ElementAt(i).ProductAmount);

                //    TotalVolume = Convert.ToDecimal(ObjVolume.ElementAt(i).TotalAmount);
                //    ReportVolume = Convert.ToDecimal(ObjVolume.ElementAt(i).ReportAmount);
                //    ProductVolume = Convert.ToDecimal(ObjVolume.ElementAt(i).ProductAmount);

                //    TotalSalesPercentage = 0;
                //    ReportSalesPercentage = 0;
                //    ProductSalesPercentage = 0;
                //    TotalVolumePercentage = 0;
                //    ReportVolumePercentage = 0;
                //    ProductVolumePercentage = 0;

                //    if (PreviousTotalSales > 0)
                //    {
                //        TotalSalesPercentage = ((Convert.ToDecimal(Obj.ElementAt(i).TotalAmount) - PreviousTotalSales) / PreviousTotalSales) * 100;
                //    }
                //    if (PreviousReportSales > 0)
                //    {
                //        ReportSalesPercentage = ((Convert.ToDecimal(Obj.ElementAt(i).ReportAmount) - PreviousReportSales) / PreviousReportSales) * 100;
                //    }
                //    if (PreviousProductSales > 0)
                //    {
                //        ProductSalesPercentage = ((Convert.ToDecimal(Obj.ElementAt(i).ProductAmount) - PreviousProductSales) / PreviousProductSales) * 100;
                //    }


                //    if (PreviousTotalVolume > 0)
                //    {
                //        TotalVolumePercentage = ((Convert.ToDecimal(ObjVolume.ElementAt(i).TotalAmount) - PreviousTotalVolume) / PreviousTotalVolume) * 100;
                //    }
                //    if (PreviousReportVolume > 0)
                //    {
                //        ReportVolumePercentage = ((Convert.ToDecimal(ObjVolume.ElementAt(i).ReportAmount) - PreviousReportVolume) / PreviousReportVolume) * 100;
                //    }
                //    if (PreviousProductVolume > 0)
                //    {
                //        ProductVolumePercentage = ((Convert.ToDecimal(ObjVolume.ElementAt(i).ProductAmount) - PreviousProductVolume) / PreviousProductVolume) * 100;
                //    }
                //}
                //ProductData += Obj.ElementAt(i).ProductAmount.ToString() + ",";
                //ReportData += Obj.ElementAt(i).ReportAmount.ToString() + ",";
                //TotalData += Obj.ElementAt(i).TotalAmount.ToString() + ",";

                ProductOldData += Obj.ElementAt(i).ProductAmount.ToString() + ",";
                ReportOldData += Obj.ElementAt(i).ReportAmount.ToString() + ",";
                TotalOldData += Obj.ElementAt(i).TotalAmount.ToString() + ",";

                XvalueText += ObjDateTime.AddMonths(i).ToString("MMM, yyyy") + "|";
                ProductMaxValue = Convert.ToDecimal(Obj.ElementAt(i).ProductAmount);
                ReportMaxValue = Convert.ToDecimal(Obj.ElementAt(i).ReportAmount);
                TotalMaxValue = Convert.ToDecimal(Obj.ElementAt(i).TotalAmount);
                if (ProductMaxValue > CurrentProductMaxValue)
                {
                    CurrentProductMaxValue = ProductMaxValue;
                }
                if (ReportMaxValue > CurrentReportMaxValue)
                {
                    CurrentReportMaxValue = ReportMaxValue;
                }
                if (TotalMaxValue > CurrentTotalMaxValue)
                {
                    CurrentTotalMaxValue = TotalMaxValue;
                }
            }
            var ProductArray = ProductOldData.Remove(ProductOldData.Length - 1, 1).Split(',');
            var ReportArray = ReportOldData.Remove(ReportOldData.Length - 1, 1).Split(',');
            var TotalArray = TotalOldData.Remove(TotalOldData.Length - 1, 1).Split(',');
            for (int i = 0; i < ProductArray.Length; i++)
            {
                decimal Value = Convert.ToDecimal(ProductArray[i].ToString());
                if (CurrentProductMaxValue > 0)
                {
                    Value = (Value / CurrentProductMaxValue) * 100;
                }
                ProductData += Value.ToString() + ",";
            }
            for (int j = 0; j < ReportArray.Length; j++)
            {
                decimal Value = Convert.ToDecimal(ReportArray[j].ToString());
                if (CurrentReportMaxValue > 0)
                {
                    Value = (Value / CurrentReportMaxValue) * 100;
                }
                ReportData += Value.ToString() + ",";
            }
            for (int k = 0; k < TotalArray.Length; k++)
            {
                decimal Value = Convert.ToDecimal(TotalArray[k].ToString());
                if (CurrentTotalMaxValue > 0)
                {
                    Value = (Value / CurrentTotalMaxValue) * 100;
                }
                TotalData += Value.ToString() + ",";
            }
            string ChartMarkers = "&chm=d,FF0000,0,0,12,0|d,FF0000,0,1,12,0|d,FF0000,0,2,12,0|d,FF0000,0,3,12,0|d,FF0000,0,4,12,0|d,FF0000,0,5,12,0|d,FF0000,0,6,12,0|d,FF0000,0,7,12,0|d,FF0000,0,8,12,0|d,FF0000,0,9,12,0|d,FF0000,0,10,12,0|d,FF0000,0,11,12,0";
            ChartMarkers += "|d,00FF00,1,0,12,0|d,00FF00,1,1,12,0|d,00FF00,1,2,12,0|d,00FF00,1,3,12,0|d,00FF00,1,4,12,0|d,00FF00,1,5,12,0|d,00FF00,1,6,12,0|d,00FF00,1,7,12,0|d,00FF00,1,8,12,0|d,00FF00,1,9,12,0|d,00FF00,1,10,12,0|d,00FF00,1,11,12,0";
            ChartMarkers += "|d,0000FF,2,0,12,0|d,0000FF,2,1,12,0|d,0000FF,2,2,12,0|d,0000FF,2,3,12,0|d,0000FF,2,4,12,0|d,0000FF,2,5,12,0|d,0000FF,2,6,12,0|d,0000FF,2,7,12,0|d,0000FF,2,8,12,0|d,0000FF,2,9,12,0|d,0000FF,2,10,12,0|d,0000FF,2,11,12,0";
            string strURL = "http://chart.apis.google.com/chart?cht=lc&chs=850x350&chd=t:" + ProductData.Remove(ProductData.Length - 1, 1) + "|" + ReportData.Remove(ReportData.Length - 1, 1) + "|" + TotalData.Remove(TotalData.Length - 1, 1) + "&chds=0,100&chxt=x,y&chxl=0:" + XvalueText.Remove(XvalueText.Length - 1, 1) + "&chls=3,1,0|3,1,0|3,1,0&chco=FF0000,00FF00,0000FF&chdl=Products|Reports|Total&chg=0,9,1,1" + ChartMarkers + "";
            string graphfilename = DateTime.Now.ToFileTime() + "_01" + ".png";
            string FileName = System.Web.HttpContext.Current.Server.MapPath("~/Resources/Upload/" + graphfilename);
            FilePath = graphfilename;
            save_file_from_url(FileName, strURL);

            #endregion Generate Graph

            return FilePath;
        }


        public string GenerateImage()
        {
            string FilePath = string.Empty;
            #region Generate Graph
            string strMonthYear = Month + "/" + Year;
            DateTime MonthYear = Convert.ToDateTime(strMonthYear);

            DateTime ObjDateTime = MonthYear.AddMonths(-11);
            string ProductData = "";
            string ReportData = "";
            string TotalData = "";
            string XvalueText = "|";
            decimal ProductMaxValue = 0;
            decimal CurrentProductMaxValue = 0;
            decimal ReportMaxValue = 0;
            decimal CurrentReportMaxValue = 0;
            decimal TotalMaxValue = 0;
            decimal CurrentTotalMaxValue = 0;

            string ProductOldData = "";
            string ReportOldData = "";
            string TotalOldData = "";

           // decimal TotalSales = 0, ReportSales = 0, ProductSales = 0;
           // decimal TotalVolume = 0, ReportVolume = 0, ProductVolume = 0;

            //decimal PreviousTotalSales = 0, PreviousReportSales = 0, PreviousProductSales = 0;
            //decimal PreviousTotalVolume = 0, PreviousReportVolume = 0, PreviousProductVolume = 0;

            //decimal TotalSalesPercentage = 0, ReportSalesPercentage = 0, ProductSalesPercentage = 0;
            //decimal TotalVolumePercentage = 0, ReportVolumePercentage = 0, ProductVolumePercentage = 0;

            BALBillingData ObjBALBillingData = new BALBillingData();
            var Obj = ObjBALBillingData.GetSalesValuesforBillingGraph(CompanyId, 11, MonthYear);

            for (int i = 0; i < Obj.Count; i++)
            {
                //BALCompany ObjBALCompany = new BALCompany();
                //var Obj = ObjBALCompany.GetSalesValuesforGraph(CompanyId, Convert.ToInt16(ObjDateTime.AddMonths(i).Month), Convert.ToInt16(ObjDateTime.AddMonths(i).Year));
                //if (i == 10)
                //{
                //    var ObjPreviousVolumeData = ObjBALCompany.GetVolumeValuesforGraph(CompanyId, Convert.ToInt16(ObjDateTime.AddMonths(i).Month), Convert.ToInt16(ObjDateTime.AddMonths(i).Year));
                //    PreviousTotalSales = Convert.ToDecimal(Obj.ElementAt(0).TotalAmount);
                //    PreviousReportSales = Convert.ToDecimal(Obj.ElementAt(0).ReportAmount);
                //    PreviousProductSales = Convert.ToDecimal(Obj.ElementAt(0).ProductAmount);

                //    PreviousTotalVolume = Convert.ToDecimal(ObjPreviousVolumeData.ElementAt(0).TotalAmount);
                //    PreviousReportVolume = Convert.ToDecimal(ObjPreviousVolumeData.ElementAt(0).ReportAmount);
                //    PreviousProductVolume = Convert.ToDecimal(ObjPreviousVolumeData.ElementAt(0).ProductAmount);
                //}
                //if (i == 11)
                //{
                //    var ObjVolume = ObjBALCompany.GetVolumeValuesforGraph(CompanyId, Convert.ToInt16(ObjDateTime.AddMonths(i).Month), Convert.ToInt16(ObjDateTime.AddMonths(i).Year));
                //    TotalSales = Convert.ToDecimal(Obj.ElementAt(0).TotalAmount);
                //    ReportSales = Convert.ToDecimal(Obj.ElementAt(0).ReportAmount);
                //    ProductSales = Convert.ToDecimal(Obj.ElementAt(0).ProductAmount);

                //    TotalVolume = Convert.ToDecimal(ObjVolume.ElementAt(0).TotalAmount);
                //    ReportVolume = Convert.ToDecimal(ObjVolume.ElementAt(0).ReportAmount);
                //    ProductVolume = Convert.ToDecimal(ObjVolume.ElementAt(0).ProductAmount);

                //    TotalSalesPercentage = 0;
                //    ReportSalesPercentage = 0;
                //    ProductSalesPercentage = 0;
                //    TotalVolumePercentage = 0;
                //    ReportVolumePercentage = 0;
                //    ProductVolumePercentage = 0;

                //    if (PreviousTotalSales > 0)
                //    {
                //        TotalSalesPercentage = ((Convert.ToDecimal(Obj.ElementAt(0).TotalAmount) - PreviousTotalSales) / PreviousTotalSales) * 100;
                //    }
                //    if (PreviousReportSales > 0)
                //    {
                //        ReportSalesPercentage = ((Convert.ToDecimal(Obj.ElementAt(0).ReportAmount) - PreviousReportSales) / PreviousReportSales) * 100;
                //    }
                //    if (PreviousProductSales > 0)
                //    {
                //        ProductSalesPercentage = ((Convert.ToDecimal(Obj.ElementAt(0).ProductAmount) - PreviousProductSales) / PreviousProductSales) * 100;
                //    }


                //    if (PreviousTotalVolume > 0)
                //    {
                //        TotalVolumePercentage = ((Convert.ToDecimal(ObjVolume.ElementAt(0).TotalAmount) - PreviousTotalVolume) / PreviousTotalVolume) * 100;
                //    }
                //    if (PreviousReportVolume > 0)
                //    {
                //        ReportVolumePercentage = ((Convert.ToDecimal(ObjVolume.ElementAt(0).ReportAmount) - PreviousReportVolume) / PreviousReportVolume) * 100;
                //    }
                //    if (PreviousProductVolume > 0)
                //    {
                //        ProductVolumePercentage = ((Convert.ToDecimal(ObjVolume.ElementAt(0).ProductAmount) - PreviousProductVolume) / PreviousProductVolume) * 100;
                //    }
                //}
                //ProductData += Obj.ElementAt(0).ProductAmount.ToString() + ",";
                //ReportData += Obj.ElementAt(0).ReportAmount.ToString() + ",";
                //TotalData += Obj.ElementAt(0).TotalAmount.ToString() + ",";

                ProductOldData += Obj.ElementAt(i).ProductAmount.ToString() + ",";
                ReportOldData += Obj.ElementAt(i).ReportAmount.ToString() + ",";
                TotalOldData += Obj.ElementAt(i).TotalAmount.ToString() + ",";

                XvalueText += ObjDateTime.AddMonths(i).ToString("MMM, yyyy") + "|";
                ProductMaxValue = Convert.ToDecimal(Obj.ElementAt(i).ProductAmount);
                ReportMaxValue = Convert.ToDecimal(Obj.ElementAt(i).ReportAmount);
                TotalMaxValue = Convert.ToDecimal(Obj.ElementAt(i).TotalAmount);
                if (ProductMaxValue > CurrentProductMaxValue)
                {
                    CurrentProductMaxValue = ProductMaxValue;
                }
                if (ReportMaxValue > CurrentReportMaxValue)
                {
                    CurrentReportMaxValue = ReportMaxValue;
                }
                if (TotalMaxValue > CurrentTotalMaxValue)
                {
                    CurrentTotalMaxValue = TotalMaxValue;
                }
            }
            var ProductArray = ProductOldData.Remove(ProductOldData.Length - 1, 1).Split(',');
            var ReportArray = ReportOldData.Remove(ReportOldData.Length - 1, 1).Split(',');
            var TotalArray = TotalOldData.Remove(TotalOldData.Length - 1, 1).Split(',');
            for (int i = 0; i < ProductArray.Length; i++)
            {
                decimal Value = Convert.ToDecimal(ProductArray[i].ToString());
                if (CurrentProductMaxValue > 0)
                {
                    Value = (Value / CurrentProductMaxValue) * 100;
                }
                ProductData += Value.ToString() + ",";
            }
            for (int j = 0; j < ReportArray.Length; j++)
            {
                decimal Value = Convert.ToDecimal(ReportArray[j].ToString());
                if (CurrentReportMaxValue > 0)
                {
                    Value = (Value / CurrentReportMaxValue) * 100;
                }
                ReportData += Value.ToString() + ",";
            }
            for (int k = 0; k < TotalArray.Length; k++)
            {
                decimal Value = Convert.ToDecimal(TotalArray[k].ToString());
                if (CurrentTotalMaxValue > 0)
                {
                    Value = (Value / CurrentTotalMaxValue) * 100;
                }
                TotalData += Value.ToString() + ",";
            }
            string ChartMarkers = "&chm=d,FF0000,0,0,12,0|d,FF0000,0,1,12,0|d,FF0000,0,2,12,0|d,FF0000,0,3,12,0|d,FF0000,0,4,12,0|d,FF0000,0,5,12,0|d,FF0000,0,6,12,0|d,FF0000,0,7,12,0|d,FF0000,0,8,12,0|d,FF0000,0,9,12,0|d,FF0000,0,10,12,0|d,FF0000,0,11,12,0";
            ChartMarkers += "|d,00FF00,1,0,12,0|d,00FF00,1,1,12,0|d,00FF00,1,2,12,0|d,00FF00,1,3,12,0|d,00FF00,1,4,12,0|d,00FF00,1,5,12,0|d,00FF00,1,6,12,0|d,00FF00,1,7,12,0|d,00FF00,1,8,12,0|d,00FF00,1,9,12,0|d,00FF00,1,10,12,0|d,00FF00,1,11,12,0";
            ChartMarkers += "|d,0000FF,2,0,12,0|d,0000FF,2,1,12,0|d,0000FF,2,2,12,0|d,0000FF,2,3,12,0|d,0000FF,2,4,12,0|d,0000FF,2,5,12,0|d,0000FF,2,6,12,0|d,0000FF,2,7,12,0|d,0000FF,2,8,12,0|d,0000FF,2,9,12,0|d,0000FF,2,10,12,0|d,0000FF,2,11,12,0";
            string strURL = "http://chart.apis.google.com/chart?cht=lc&chs=850x350&chd=t:" + ProductData.Remove(ProductData.Length - 1, 1) + "|" + ReportData.Remove(ReportData.Length - 1, 1) + "|" + TotalData.Remove(TotalData.Length - 1, 1) + "&chds=0,100&chxt=x,y&chxl=0:" + XvalueText.Remove(XvalueText.Length - 1, 1) + "&chls=3,1,0|3,1,0|3,1,0&chco=FF0000,00FF00,0000FF&chdl=Products|Reports|Total&chg=0,9,1,1" + ChartMarkers + "";
            string graphfilename = DateTime.Now.ToFileTime() + "_01" + ".png";
            string FileName = System.Web.HttpContext.Current.Server.MapPath("~/Resources/Upload/" + graphfilename);
            FilePath = graphfilename;
            save_file_from_url(FileName, strURL);

            #endregion Generate Graph
            return FilePath;

        }
        public void save_file_from_url(string file_name, string url)
        {
            byte[] content;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            WebResponse response = request.GetResponse();

            Stream stream = response.GetResponseStream();

            using (BinaryReader br = new BinaryReader(stream))
            {
                content = br.ReadBytes(500000);
                br.Close();
            }
            response.Close();

            using (FileStream fs = new FileStream(file_name, FileMode.Create))
            {
                BinaryWriter bw = new BinaryWriter(fs);
                try
                {
                    bw.Write(content);
                }
                finally
                {
                    fs.Close();
                    bw.Close();
                }
            }
        }
        #endregion

    }
}