namespace EmergeBilling
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;
    using System.Collections.Generic;
    using Emerge.Services;
    using Emerge.Data;

    /// <summary>
    /// Summary description for BillingStatement.
    /// </summary>
    public partial class BillingStatement : Telerik.Reporting.Report
    {
        public Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALCompanyInformation>, List<BALReportsByLocation>, List<proc_LoadOpenInvoiceByCompanyIdResult>, List<proc_GetBillingAdjustmentsResult>> ObjData { get; set; }
        public Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALReportsByLocation>, List<proc_GetBillingAdjustmentsResult>> ObjDataApi { get; set; }
        public List<clsSavedReports> ObjSavedReports { get; set; }
        public string ApplicationPath { get; set; }
        public bool IsAdmin { get; set; }
        public string BillingId { get; set; }
        public string BillingPhone { get; set; }
        public bool IsDueDate { get; set; }

        public BillingStatement()
        {
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        private void BillingStatement_NeedDataSource(object sender, EventArgs e)
        {

            try
            {

                List<BALBillingSummary> item1 = new List<BALBillingSummary>();
                List<BALBillingSummary> item2 = new List<BALBillingSummary>();
                List<BALReportsByLocation> item3 = new List<BALReportsByLocation>();
                List<proc_GetBillingAdjustmentsResult> Adjustments = new List<proc_GetBillingAdjustmentsResult>();
                List<BALCompanyInformation> CompanyInformation = new List<BALCompanyInformation>();
                List<proc_LoadOpenInvoiceByCompanyIdResult> OpenInvoices = new List<proc_LoadOpenInvoiceByCompanyIdResult>();
               // List<BALBillingSummary> ObjSummeryDataApi = new List<BALBillingSummary>();
               // List<BALBillingSummary> ObjProductsDataApi = new List<BALBillingSummary>();
               // List<BALReportsByLocation> ObjLocationsDataApi = new List<BALReportsByLocation>();
                //List<proc_GetBillingAdjustmentsResult> AdjustmentsApi = new List<proc_GetBillingAdjustmentsResult>();
                if (ObjData != null)
                {
                    item1 = ObjData.Item1;
                    item2 = ObjData.Item2;
                    item3 = ObjData.Item4;
                    //List<BALCompanyLocationsForOthers> item3 = ObjData.Item4;
                    CompanyInformation = ObjData.Item3;
                    OpenInvoices = ObjData.Item5;
                    Adjustments = ObjData.Item6;
                }


                Telerik.Reporting.Report MainRpt = (sender as Telerik.Reporting.Report);
                Telerik.Reporting.SubReport SRBillingSummary = (Telerik.Reporting.SubReport)(srBillingSummary);
                Telerik.Reporting.SubReport SRProducts = (Telerik.Reporting.SubReport)(srProductsPerLocation);
                Telerik.Reporting.SubReport SRLocationReport = (Telerik.Reporting.SubReport)(srCompanyLocations);
                Telerik.Reporting.SubReport SRItemizedReport = (Telerik.Reporting.SubReport)(srItemized);
                Telerik.Reporting.SubReport SRBillingStatement = (Telerik.Reporting.SubReport)(srBillingStatement);
                // Telerik.Reporting.SubReport SRBillingSummaryApi = (Telerik.Reporting.SubReport)(srApiBillingSummary);
                //Telerik.Reporting.SubReport SRProductsApi = (Telerik.Reporting.SubReport)(srApiProductsPerLocation);
                //Telerik.Reporting.SubReport SRLocationReportApi = (Telerik.Reporting.SubReport)(srApiCompanyLocations);
                EmergeBilling.SRBillingStatement.ApplicationPath = ApplicationPath;
                (sender as Telerik.Reporting.Processing.Report).DataSource = CompanyInformation;
                if (ObjSavedReports != null)
                {
                    BillingStatement.TotalItemizedReports = ObjSavedReports.Count.ToString() + " Order(s) Found";
                    srItemizedReport1.DataSource = ObjSavedReports;
                }

                if (Adjustments.Count > 0)
                {
                    srAdjustments2.DataSource = Adjustments;
                }
                else
                {
                    srAdjustment.Visible = false;
                    pnlAdjustmentsHeadings.Visible = false;
                }

                srBillingSummary1.DataSource = item1;
                srProducts2.DataSource = item2;
                srCompanyLocations1.DataSource = item3;


                //if (ObjDataApi != null)
                //{
                //    ObjSummeryDataApi = ObjDataApi.Item1;
                //    EmergeBilling.SRProducts.ProductCategory = string.Empty;
                //    ObjProductsDataApi = ObjDataApi.Item2;
                //    ObjLocationsDataApi = ObjDataApi.Item3;
                //    AdjustmentsApi = ObjDataApi.Item4;
                //   // srBillingSummary3.DataSource = ObjSummeryDataApi;
                //   // srProducts1.DataSource = ObjProductsDataApi;
                //    //srCompanyLocations3.DataSource = ObjLocationsDataApi;
                //}
                //else
                //{
                //    SRBillingSummaryApi.Visible = false;
                //    SRProductsApi.Visible = false;
                //    srCompanyLocationsApi.Visible = false;
                //}

                if (OpenInvoices.Count > 0)
                {
                    srBillingStatement1.DataSource = OpenInvoices;

                }
                else
                {
                    SRBillingStatement.Visible = false;
                }
                //if (AdjustmentsApi.Count > 0)
                //{
                //    srAdjustments1.DataSource = AdjustmentsApi;
                //}
                //else
                //{
                //    srApiAdjustments.Visible = false;
                //    pnlApiuAjustmentHeadings.Visible = false;
                //}
                txtBillingId.Value = BillingId;
                txtBillingPhone.Value = BillingPhone;
                lblIsDue1.Visible = IsDueDate;
                lblIsDue2.Visible = IsDueDate;
            }
            catch { }
        }
    }
}