namespace EmergeBilling
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for SRBillingStatement.
    /// </summary>
    public partial class SRBillingStatement : Telerik.Reporting.Report
    {
        public int count { get; set; }
        public static bool IsAdmin { get; set; }
        public static string ApplicationPath { get; set; }
        public SRBillingStatement()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        private void btnPay_ItemDataBinding(object sender, EventArgs e)
        {
            Telerik.Reporting.NavigateToUrlAction PayPath = new Telerik.Reporting.NavigateToUrlAction();
            //string path = "http://" + System.Web.HttpContext.Current.Request.Url.Authority;
            PayPath.Url = ApplicationPath + "Corporate/BillingStatements?Type=" + count;
            Telerik.Reporting.PictureBox btnPay = (Telerik.Reporting.PictureBox)detail.Items[7];
            btnPay.Action = PayPath;
            count++;
        }

        private void btnPayAll_ItemDataBinding(object sender, EventArgs e)
        {
            Telerik.Reporting.NavigateToUrlAction PayAllPath = new Telerik.Reporting.NavigateToUrlAction();
            //string path = "http://" + System.Web.HttpContext.Current.Request.Url.Authority;
            PayAllPath.Url = ApplicationPath + "Corporate/BillingStatements?Type=-1";
            Telerik.Reporting.PictureBox btnPayAll = (Telerik.Reporting.PictureBox)groupFooterSection1.Items[5];
            btnPayAll.Action = PayAllPath;
        }

    }
}