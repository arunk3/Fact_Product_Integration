using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using Emerge.Services;
using Emerge.Data;

namespace EmergeBilling
{
    partial class SRItemizedReport
    {

        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.Drawing.FormattingRule formattingRule1 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule2 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule3 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule4 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule5 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule6 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule7 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule8 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule9 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule10 = new Telerik.Reporting.Drawing.FormattingRule();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SRItemizedReport));
            Telerik.Reporting.Drawing.FormattingRule formattingRule11 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule12 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule13 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule14 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule15 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule16 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule17 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule18 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule19 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule20 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule21 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule22 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule23 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Group group1 = new Telerik.Reporting.Group();
            Telerik.Reporting.Group group2 = new Telerik.Reporting.Group();
            Telerik.Reporting.Group group3 = new Telerik.Reporting.Group();
            this.groupFooterSection1 = new Telerik.Reporting.GroupFooterSection();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.groupHeaderSection1 = new Telerik.Reporting.GroupHeaderSection();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.groupFooterSection2 = new Telerik.Reporting.GroupFooterSection();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.groupHeaderSection2 = new Telerik.Reporting.GroupHeaderSection();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.groupFooterSection = new Telerik.Reporting.GroupFooterSection();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.groupHeaderSection = new Telerik.Reporting.GroupHeaderSection();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.textBox73 = new Telerik.Reporting.TextBox();
            this.textBox72 = new Telerik.Reporting.TextBox();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.pictureBox2 = new Telerik.Reporting.PictureBox();
            this.pictureBox3 = new Telerik.Reporting.PictureBox();
            this.pictureBox4 = new Telerik.Reporting.PictureBox();
            this.panel2 = new Telerik.Reporting.Panel();
            this.txtPCode1 = new Telerik.Reporting.TextBox();
            this.pLiveReview1 = new Telerik.Reporting.PictureBox();
            this.pLiveReview14 = new Telerik.Reporting.PictureBox();
            this.txtPCode14 = new Telerik.Reporting.TextBox();
            this.pLiveReview10 = new Telerik.Reporting.PictureBox();
            this.txtPCode13 = new Telerik.Reporting.TextBox();
            this.pLiveReview13 = new Telerik.Reporting.PictureBox();
            this.txtPCode12 = new Telerik.Reporting.TextBox();
            this.pLiveReview11 = new Telerik.Reporting.PictureBox();
            this.txtPCode11 = new Telerik.Reporting.TextBox();
            this.pLiveReview9 = new Telerik.Reporting.PictureBox();
            this.txtPCode10 = new Telerik.Reporting.TextBox();
            this.pLiveReview8 = new Telerik.Reporting.PictureBox();
            this.txtPCode9 = new Telerik.Reporting.TextBox();
            this.pLiveReview7 = new Telerik.Reporting.PictureBox();
            this.txtPCode7 = new Telerik.Reporting.TextBox();
            this.pLiveReview6 = new Telerik.Reporting.PictureBox();
            this.txtPCode6 = new Telerik.Reporting.TextBox();
            this.pLiveReview5 = new Telerik.Reporting.PictureBox();
            this.txtPCode5 = new Telerik.Reporting.TextBox();
            this.pLiveReview4 = new Telerik.Reporting.PictureBox();
            this.txtPCode4 = new Telerik.Reporting.TextBox();
            this.pLiveReview3 = new Telerik.Reporting.PictureBox();
            this.txtPCode3 = new Telerik.Reporting.TextBox();
            this.pLiveReview2 = new Telerik.Reporting.PictureBox();
            this.txtPCode2 = new Telerik.Reporting.TextBox();
            this.pLiveReview12 = new Telerik.Reporting.PictureBox();
            this.txtPCode8 = new Telerik.Reporting.TextBox();
            this.txtPCode32 = new Telerik.Reporting.TextBox();
            this.txtPCode33 = new Telerik.Reporting.TextBox();
            this.txtPCode34 = new Telerik.Reporting.TextBox();
            this.txtPCode35 = new Telerik.Reporting.TextBox();
            this.txtPCode36 = new Telerik.Reporting.TextBox();
            this.txtPCode37 = new Telerik.Reporting.TextBox();
            this.txtPCode38 = new Telerik.Reporting.TextBox();
            this.txtPCode31 = new Telerik.Reporting.TextBox();
            this.txtPCode30 = new Telerik.Reporting.TextBox();
            this.txtPCode29 = new Telerik.Reporting.TextBox();
            this.txtPCode28 = new Telerik.Reporting.TextBox();
            this.txtPCode27 = new Telerik.Reporting.TextBox();
            this.txtPCode26 = new Telerik.Reporting.TextBox();
            this.txtPCode25 = new Telerik.Reporting.TextBox();
            this.txtPCode23 = new Telerik.Reporting.TextBox();
            this.txtPCode24 = new Telerik.Reporting.TextBox();
            this.txtPCode22 = new Telerik.Reporting.TextBox();
            this.txtPCode21 = new Telerik.Reporting.TextBox();
            this.txtPCode20 = new Telerik.Reporting.TextBox();
            this.txtPCode19 = new Telerik.Reporting.TextBox();
            this.txtPCode17 = new Telerik.Reporting.TextBox();
            this.txtPCode18 = new Telerik.Reporting.TextBox();
            this.txtPCode16 = new Telerik.Reporting.TextBox();
            this.txtPCode15 = new Telerik.Reporting.TextBox();
            this.pLiveReview38 = new Telerik.Reporting.PictureBox();
            this.pLiveReview32 = new Telerik.Reporting.PictureBox();
            this.pLiveReview33 = new Telerik.Reporting.PictureBox();
            this.pLiveReview34 = new Telerik.Reporting.PictureBox();
            this.pLiveReview37 = new Telerik.Reporting.PictureBox();
            this.pLiveReview36 = new Telerik.Reporting.PictureBox();
            this.pLiveReview35 = new Telerik.Reporting.PictureBox();
            this.pLiveReview29 = new Telerik.Reporting.PictureBox();
            this.pLiveReview30 = new Telerik.Reporting.PictureBox();
            this.pLiveReview31 = new Telerik.Reporting.PictureBox();
            this.pLiveReview28 = new Telerik.Reporting.PictureBox();
            this.pLiveReview26 = new Telerik.Reporting.PictureBox();
            this.pLiveReview27 = new Telerik.Reporting.PictureBox();
            this.pLiveReview25 = new Telerik.Reporting.PictureBox();
            this.pLiveReview24 = new Telerik.Reporting.PictureBox();
            this.pLiveReview21 = new Telerik.Reporting.PictureBox();
            this.pLiveReview22 = new Telerik.Reporting.PictureBox();
            this.pLiveReview23 = new Telerik.Reporting.PictureBox();
            this.pLiveReview20 = new Telerik.Reporting.PictureBox();
            this.pLiveReview18 = new Telerik.Reporting.PictureBox();
            this.pLiveReview17 = new Telerik.Reporting.PictureBox();
            this.pLiveReview19 = new Telerik.Reporting.PictureBox();
            this.pLiveReview16 = new Telerik.Reporting.PictureBox();
            this.pLiveReview15 = new Telerik.Reporting.PictureBox();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.pictureBox6 = new Telerik.Reporting.PictureBox();
            this.pictureBox5 = new Telerik.Reporting.PictureBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.pictureBox7 = new Telerik.Reporting.PictureBox();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.reportFooterSection1 = new Telerik.Reporting.ReportFooterSection();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // groupFooterSection1
            // 
            formattingRule1.StopIfTrue = true;
            this.groupFooterSection1.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule1});
            this.groupFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.23954397439956665D);
            this.groupFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox4,
            this.textBox6});
            this.groupFooterSection1.Name = "groupFooterSection1";
            this.groupFooterSection1.Style.BackgroundColor = System.Drawing.Color.Gray;
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.7000393867492676D), Telerik.Reporting.Drawing.Unit.Inch(7.8837074397597462E-05D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(95.996253967285156D), Telerik.Reporting.Drawing.Unit.Inch(0.17500001192092896D));
            this.textBox4.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox4.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox4.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox4.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Style.Font.Name = "Calibri";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox4.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.Value = "=IIf(Fields.LastName=\"Discount\",\"\",ChangeFormat(Sum(Fields.Total).ToString())) ";
            // 
            // textBox6
            // 
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.7999606728553772D), Telerik.Reporting.Drawing.Unit.Inch(0.23954395949840546D));
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.Font.Name = "Calibri";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.Value = "= IIf(Fields.LastName=\"Discount\",\"\",\"All Total\")";
            // 
            // groupHeaderSection1
            // 
            formattingRule2.Filters.Add(new Telerik.Reporting.Filter("=Fields.Locationid <> \"0\"", Telerik.Reporting.FilterOperator.Equal, "False\r\n"));
            formattingRule2.Style.Visible = false;
            this.groupHeaderSection1.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule2});
            this.groupHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.20298147201538086D);
            this.groupHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox13});
            this.groupHeaderSection1.Name = "groupHeaderSection1";
            this.groupHeaderSection1.Style.Visible = true;
            // 
            // textBox13
            // 
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.0078125D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(175.5113525390625D), Telerik.Reporting.Drawing.Unit.Inch(0.18996064364910126D));
            this.textBox13.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox13.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox13.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox13.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox13.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.Font.Name = "Calibri";
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox13.Style.Font.Underline = false;
            this.textBox13.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox13.Value = "= Fields.Location";
            // 
            // groupFooterSection2
            // 
            formattingRule3.Filters.Add(new Telerik.Reporting.Filter("=Fields.Locationid <> \"0\"", Telerik.Reporting.FilterOperator.Equal, "false"));
            formattingRule3.Style.Visible = false;
            this.groupFooterSection2.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule3});
            this.groupFooterSection2.Height = Telerik.Reporting.Drawing.Unit.Inch(0.17500001192092896D);
            this.groupFooterSection2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox11,
            this.textBox12});
            this.groupFooterSection2.Name = "groupFooterSection2";
            this.groupFooterSection2.Style.BackgroundColor = System.Drawing.Color.DarkGray;
            this.groupFooterSection2.Style.Visible = true;
            // 
            // textBox11
            // 
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9577484130859375E-05D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.899960458278656D), Telerik.Reporting.Drawing.Unit.Inch(0.17496059834957123D));
            this.textBox11.Style.Color = System.Drawing.Color.Black;
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Font.Name = "Calibri";
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox11.Value = "Location Total";
            // 
            // textBox12
            // 
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.6999993324279785D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(95.996253967285156D), Telerik.Reporting.Drawing.Unit.Inch(0.17500001192092896D));
            this.textBox12.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox12.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox12.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox12.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox12.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.Font.Name = "Calibri";
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox12.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox12.Value = "= ChangeFormat(Sum(Fields.Total).ToString())";
            // 
            // groupHeaderSection2
            // 
            formattingRule4.Filters.Add(new Telerik.Reporting.Filter("=Fields.Rcode <> \"0\"", Telerik.Reporting.FilterOperator.Equal, "true"));
            formattingRule4.Style.Visible = true;
            this.groupHeaderSection2.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule4});
            this.groupHeaderSection2.Height = Telerik.Reporting.Drawing.Unit.Inch(0.19000005722045898D);
            this.groupHeaderSection2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox10});
            this.groupHeaderSection2.Name = "groupHeaderSection2";
            this.groupHeaderSection2.Style.BackgroundColor = System.Drawing.Color.DarkGray;
            this.groupHeaderSection2.Style.Visible = false;
            // 
            // textBox10
            // 
            this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(202.0113525390625D), Telerik.Reporting.Drawing.Unit.Inch(0.18996064364910126D));
            this.textBox10.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox10.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox10.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox10.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox10.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox10.Style.Font.Bold = true;
            this.textBox10.Style.Font.Name = "Calibri";
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox10.Style.Font.Underline = false;
            this.textBox10.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox10.Value = "= Fields.ReferenceCode";
            // 
            // groupFooterSection
            // 
            formattingRule5.Filters.Add(new Telerik.Reporting.Filter("=Fields.Rcode <> \"0\"", Telerik.Reporting.FilterOperator.Equal, "true"));
            formattingRule5.Style.Visible = true;
            this.groupFooterSection.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule5});
            this.groupFooterSection.Height = Telerik.Reporting.Drawing.Unit.Inch(0.17500001192092896D);
            this.groupFooterSection.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox5,
            this.textBox9});
            this.groupFooterSection.Name = "groupFooterSection";
            this.groupFooterSection.Style.BackgroundColor = System.Drawing.Color.Silver;
            this.groupFooterSection.Style.Visible = false;
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.7000002861022949D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(95.996253967285156D), Telerik.Reporting.Drawing.Unit.Inch(0.17500001192092896D));
            this.textBox5.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox5.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox5.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox5.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox5.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox5.Style.Font.Bold = true;
            this.textBox5.Style.Font.Name = "Calibri";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox5.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.Value = "= ChangeFormat(Sum(Fields.Total).ToString())";
            // 
            // textBox9
            // 
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1577731370925903D), Telerik.Reporting.Drawing.Unit.Inch(0.17496059834957123D));
            this.textBox9.Style.Color = System.Drawing.Color.Black;
            this.textBox9.Style.Font.Bold = true;
            this.textBox9.Style.Font.Name = "Calibri";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.Value = "Reference Code Total";
            // 
            // groupHeaderSection
            // 
            formattingRule6.Filters.Add(new Telerik.Reporting.Filter("=Fields.Ucode", Telerik.Reporting.FilterOperator.LessThan, "0"));
            formattingRule6.Style.Visible = true;
            this.groupHeaderSection.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule6});
            this.groupHeaderSection.Height = Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D);
            this.groupHeaderSection.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox3});
            this.groupHeaderSection.Name = "groupHeaderSection";
            this.groupHeaderSection.Style.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.groupHeaderSection.Style.Visible = false;
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.9365557313431054E-05D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(349.0113525390625D), Telerik.Reporting.Drawing.Unit.Inch(0.18996064364910126D));
            this.textBox3.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox3.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox3.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox3.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Name = "Calibri";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox3.Style.Font.Underline = false;
            this.textBox3.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.Value = "=Fields.UserEmail";
            // 
            // detail
            // 
            formattingRule7.Filters.Add(new Telerik.Reporting.Filter("= RowNumber() % 2", Telerik.Reporting.FilterOperator.Equal, "1"));
            formattingRule7.Style.BackgroundColor = System.Drawing.Color.White;
            this.detail.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule7});
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(0.28999993205070496D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox73,
            this.textBox72,
            this.textBox70,
            this.textBox68,
            this.textBox67,
            this.textBox64,
            this.pictureBox1,
            this.pictureBox2,
            this.pictureBox3,
            this.pictureBox4,
            this.panel2,
            this.textBox65,
            this.textBox1,
            this.pictureBox6,
            this.pictureBox5,
            this.textBox2,
            this.pictureBox7,
            this.textBox69,
            this.textBox7,
            this.textBox8});
            this.detail.Name = "detail";
            this.detail.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(221)))), ((int)(((byte)(221)))));
            this.detail.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.detail.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.detail.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.detail.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.detail.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.detail.Style.Font.Name = "Calibri";
            this.detail.ItemDataBinding += new System.EventHandler(this.detail_ItemDataBinding);
            // 
            // textBox73
            // 
            this.textBox73.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.8383522033691406D), Telerik.Reporting.Drawing.Unit.Inch(-3.3113691788599908E-09D));
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.46156924962997437D), Telerik.Reporting.Drawing.Unit.Inch(0.17499998211860657D));
            this.textBox73.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox73.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox73.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox73.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox73.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox73.Style.Font.Bold = false;
            this.textBox73.Style.Font.Name = "Calibri";
            this.textBox73.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox73.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox73.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox73.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox73.Value = "=  Fields.Fee";
            // 
            // textBox72
            // 
            this.textBox72.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.27724027633667D), Telerik.Reporting.Drawing.Unit.Inch(0.004206100944429636D));
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(51.674396514892578D), Telerik.Reporting.Drawing.Unit.Inch(0.17499998211860657D));
            this.textBox72.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox72.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox72.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox72.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox72.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox72.Style.Font.Bold = false;
            this.textBox72.Style.Font.Name = "Calibri";
            this.textBox72.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox72.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox72.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox72.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox72.Value = "= Fields.Price";
            // 
            // textBox70
            // 
            this.textBox70.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.2189078330993652D), Telerik.Reporting.Drawing.Unit.Inch(-3.3113691788599908E-09D));
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(66.333335876464844D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox70.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox70.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox70.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox70.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox70.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox70.Style.Font.Bold = false;
            this.textBox70.Style.Font.Name = "Calibri";
            this.textBox70.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox70.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox70.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox70.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox70.Value = "=Fields.OrderDt.ToString(\"yyyy-MM-dd\")";
            // 
            // textBox68
            // 
            this.textBox68.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.4542455673217773D), Telerik.Reporting.Drawing.Unit.Inch(0.0021030902862548828D));
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(73.399993896484375D), Telerik.Reporting.Drawing.Unit.Inch(0.10420607775449753D));
            this.textBox68.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox68.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox68.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox68.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox68.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox68.Style.Font.Bold = false;
            this.textBox68.Style.Font.Name = "Calibri";
            this.textBox68.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox68.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox68.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox68.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox68.Value = "= Fields.Location";
            // 
            // textBox67
            // 
            this.textBox67.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(139.60000610351563D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox67.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox67.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox67.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox67.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox67.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox67.Style.Font.Bold = false;
            this.textBox67.Style.Font.Name = "Calibri";
            this.textBox67.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox67.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox67.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox67.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox67.Value = "=Fields.UserEmail";
            // 
            // textBox64
            // 
            this.textBox64.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.3113691788599908E-09D), Telerik.Reporting.Drawing.Unit.Inch(0.099960595369338989D));
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(86.4000015258789D), Telerik.Reporting.Drawing.Unit.Inch(0.10000001639127731D));
            this.textBox64.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox64.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox64.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox64.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox64.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox64.Style.Font.Bold = false;
            this.textBox64.Style.Font.Name = "Calibri";
            this.textBox64.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox64.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox64.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox64.Value = "=Fields.FirstName";
            // 
            // pictureBox1
            // 
            formattingRule8.Filters.Add(new Telerik.Reporting.Filter("=Fields.OrderStatus", Telerik.Reporting.FilterOperator.Equal, "2"));
            formattingRule8.Style.Visible = false;
            formattingRule9.Filters.Add(new Telerik.Reporting.Filter("=Fields.OrderStatus", Telerik.Reporting.FilterOperator.Equal, "3"));
            formattingRule9.Style.Visible = false;
            formattingRule10.Filters.Add(new Telerik.Reporting.Filter("=Fields.OrderStatus", Telerik.Reporting.FilterOperator.Equal, "5"));
            formattingRule10.Style.Visible = false;
            this.pictureBox1.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule8,
            formattingRule9,
            formattingRule10});
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.1000781059265137D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.pictureBox1.MimeType = "image/png";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.17708353698253632D), Telerik.Reporting.Drawing.Unit.Pixel(18.399993896484375D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox1.Style.Font.Name = "Calibri";
            this.pictureBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.pictureBox1.Style.Visible = true;
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // pictureBox2
            // 
            formattingRule11.Filters.Add(new Telerik.Reporting.Filter("=Fields.OrderStatus", Telerik.Reporting.FilterOperator.Equal, "1"));
            formattingRule11.Style.Visible = false;
            formattingRule12.Filters.Add(new Telerik.Reporting.Filter("=Fields.OrderStatus", Telerik.Reporting.FilterOperator.Equal, "3"));
            formattingRule12.Style.Visible = false;
            formattingRule13.Filters.Add(new Telerik.Reporting.Filter("=Fields.OrderStatus", Telerik.Reporting.FilterOperator.Equal, "5"));
            formattingRule13.Style.Visible = false;
            this.pictureBox2.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule11,
            formattingRule12,
            formattingRule13});
            this.pictureBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.07716178894043D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.pictureBox2.MimeType = "image/png";
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.19999970495700836D), Telerik.Reporting.Drawing.Unit.Pixel(18.399993896484375D));
            this.pictureBox2.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox2.Style.Font.Name = "Calibri";
            this.pictureBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.pictureBox2.Style.Visible = true;
            this.pictureBox2.Value = ((object)(resources.GetObject("pictureBox2.Value")));
            // 
            // pictureBox3
            // 
            formattingRule14.Filters.Add(new Telerik.Reporting.Filter("=Fields.OrderStatus", Telerik.Reporting.FilterOperator.Equal, "1"));
            formattingRule14.Style.Visible = false;
            formattingRule15.Filters.Add(new Telerik.Reporting.Filter("=Fields.OrderStatus", Telerik.Reporting.FilterOperator.Equal, "2"));
            formattingRule15.Style.Visible = false;
            formattingRule16.Filters.Add(new Telerik.Reporting.Filter("=Fields.OrderStatus", Telerik.Reporting.FilterOperator.Equal, "5"));
            formattingRule16.Style.Visible = false;
            this.pictureBox3.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule14,
            formattingRule15,
            formattingRule16});
            this.pictureBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.07716178894043D), Telerik.Reporting.Drawing.Unit.Inch(0.00829399935901165D));
            this.pictureBox3.MimeType = "image/gif";
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.22283871471881867D), Telerik.Reporting.Drawing.Unit.Pixel(18.399993896484375D));
            this.pictureBox3.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox3.Style.Font.Name = "Calibri";
            this.pictureBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.pictureBox3.Style.Visible = true;
            this.pictureBox3.Value = ((object)(resources.GetObject("pictureBox3.Value")));
            // 
            // pictureBox4
            // 
            formattingRule17.Filters.Add(new Telerik.Reporting.Filter("=Fields.OrderStatus", Telerik.Reporting.FilterOperator.Equal, "1"));
            formattingRule17.Style.Visible = false;
            formattingRule18.Filters.Add(new Telerik.Reporting.Filter("=Fields.OrderStatus", Telerik.Reporting.FilterOperator.Equal, "2"));
            formattingRule18.Style.Visible = false;
            formattingRule19.Filters.Add(new Telerik.Reporting.Filter("=Fields.OrderStatus", Telerik.Reporting.FilterOperator.Equal, "3"));
            formattingRule19.Style.Visible = false;
            this.pictureBox4.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule17,
            formattingRule18,
            formattingRule19});
            this.pictureBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.1000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.pictureBox4.MimeType = "image/png";
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.20000053942203522D), Telerik.Reporting.Drawing.Unit.Pixel(19.196216583251953D));
            this.pictureBox4.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox4.Style.Font.Name = "Calibri";
            this.pictureBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.pictureBox4.Style.Visible = true;
            this.pictureBox4.Value = ((object)(resources.GetObject("pictureBox4.Value")));
            // 
            // panel2
            // 
            this.panel2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtPCode1,
            this.pLiveReview1,
            this.pLiveReview14,
            this.txtPCode14,
            this.pLiveReview10,
            this.txtPCode13,
            this.pLiveReview13,
            this.txtPCode12,
            this.pLiveReview11,
            this.txtPCode11,
            this.pLiveReview9,
            this.txtPCode10,
            this.pLiveReview8,
            this.txtPCode9,
            this.pLiveReview7,
            this.txtPCode7,
            this.pLiveReview6,
            this.txtPCode6,
            this.pLiveReview5,
            this.txtPCode5,
            this.pLiveReview4,
            this.txtPCode4,
            this.pLiveReview3,
            this.txtPCode3,
            this.pLiveReview2,
            this.txtPCode2,
            this.pLiveReview12,
            this.txtPCode8,
            this.txtPCode32,
            this.txtPCode33,
            this.txtPCode34,
            this.txtPCode35,
            this.txtPCode36,
            this.txtPCode37,
            this.txtPCode38,
            this.txtPCode31,
            this.txtPCode30,
            this.txtPCode29,
            this.txtPCode28,
            this.txtPCode27,
            this.txtPCode26,
            this.txtPCode25,
            this.txtPCode23,
            this.txtPCode24,
            this.txtPCode22,
            this.txtPCode21,
            this.txtPCode20,
            this.txtPCode19,
            this.txtPCode17,
            this.txtPCode18,
            this.txtPCode16,
            this.txtPCode15,
            this.pLiveReview38,
            this.pLiveReview32,
            this.pLiveReview33,
            this.pLiveReview34,
            this.pLiveReview37,
            this.pLiveReview36,
            this.pLiveReview35,
            this.pLiveReview29,
            this.pLiveReview30,
            this.pLiveReview31,
            this.pLiveReview28,
            this.pLiveReview26,
            this.pLiveReview27,
            this.pLiveReview25,
            this.pLiveReview24,
            this.pLiveReview21,
            this.pLiveReview22,
            this.pLiveReview23,
            this.pLiveReview20,
            this.pLiveReview18,
            this.pLiveReview17,
            this.pLiveReview19,
            this.pLiveReview16,
            this.pLiveReview15});
            this.panel2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2D), Telerik.Reporting.Drawing.Unit.Inch(3.9365557313431054E-05D));
            this.panel2.Name = "panel2";
            this.panel2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(96.000007629394531D), Telerik.Reporting.Drawing.Unit.Pixel(21.803779602050781D));
            // 
            // txtPCode1
            // 
            this.txtPCode1.CanShrink = true;
            this.txtPCode1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(0.0037841796875D), Telerik.Reporting.Drawing.Unit.Pixel(11.796223640441895D));
            this.txtPCode1.Name = "txtPCode1";
            this.txtPCode1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode1.Style.Font.Name = "Calibri";
            this.txtPCode1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.txtPCode1.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.txtPCode1.Value = "";
            // 
            // pLiveReview1
            // 
            this.pLiveReview1.Location = Telerik.Reporting.Drawing.PointU.Empty;
            this.pLiveReview1.Name = "pLiveReview1";
            this.pLiveReview1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // pLiveReview14
            // 
            this.pLiveReview14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1041666641831398D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.pLiveReview14.Name = "pLiveReview14";
            this.pLiveReview14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // txtPCode14
            // 
            this.txtPCode14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(25.0416259765625D), Telerik.Reporting.Drawing.Unit.Pixel(11.796223640441895D));
            this.txtPCode14.Name = "txtPCode14";
            this.txtPCode14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode14.Style.Font.Name = "Calibri";
            this.txtPCode14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.txtPCode14.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.txtPCode14.Value = "";
            // 
            // pLiveReview10
            // 
            this.pLiveReview10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(0.0075581870041787624D));
            this.pLiveReview10.Name = "pLiveReview10";
            this.pLiveReview10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // txtPCode13
            // 
            this.txtPCode13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(20.0340576171875D), Telerik.Reporting.Drawing.Unit.Pixel(11.796223640441895D));
            this.txtPCode13.Name = "txtPCode13";
            this.txtPCode13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode13.Style.Font.Name = "Calibri";
            this.txtPCode13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.txtPCode13.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.txtPCode13.Value = "";
            // 
            // pLiveReview13
            // 
            this.pLiveReview13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(15.007568359375D), Telerik.Reporting.Drawing.Unit.Pixel(0.0075581870041787624D));
            this.pLiveReview13.Name = "pLiveReview13";
            this.pLiveReview13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // txtPCode12
            // 
            this.txtPCode12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(10.0189208984375D), Telerik.Reporting.Drawing.Unit.Pixel(11.796223640441895D));
            this.txtPCode12.Name = "txtPCode12";
            this.txtPCode12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode12.Style.Font.Name = "Calibri";
            this.txtPCode12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.txtPCode12.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.txtPCode12.Value = "";
            // 
            // pLiveReview11
            // 
            this.pLiveReview11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.2084910124540329D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.pLiveReview11.Name = "pLiveReview11";
            this.pLiveReview11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // txtPCode11
            // 
            this.txtPCode11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(15.0264892578125D), Telerik.Reporting.Drawing.Unit.Pixel(11.796223640441895D));
            this.txtPCode11.Name = "txtPCode11";
            this.txtPCode11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode11.Style.Font.Name = "Calibri";
            this.txtPCode11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.txtPCode11.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.txtPCode11.Value = "";
            // 
            // pLiveReview9
            // 
            this.pLiveReview9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(60.07568359375D), Telerik.Reporting.Drawing.Unit.Pixel(0D));
            this.pLiveReview9.Name = "pLiveReview9";
            this.pLiveReview9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // txtPCode10
            // 
            this.txtPCode10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(5.0113525390625D), Telerik.Reporting.Drawing.Unit.Pixel(11.796223640441895D));
            this.txtPCode10.Name = "txtPCode10";
            this.txtPCode10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode10.Style.Font.Name = "Calibri";
            this.txtPCode10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.txtPCode10.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.txtPCode10.Value = "";
            // 
            // pLiveReview8
            // 
            this.pLiveReview8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(55.068115234375D), Telerik.Reporting.Drawing.Unit.Pixel(0D));
            this.pLiveReview8.Name = "pLiveReview8";
            this.pLiveReview8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // txtPCode9
            // 
            this.txtPCode9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(60.0946044921875D), Telerik.Reporting.Drawing.Unit.Pixel(11.796223640441895D));
            this.txtPCode9.Name = "txtPCode9";
            this.txtPCode9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode9.Style.Font.Name = "Calibri";
            this.txtPCode9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.txtPCode9.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.txtPCode9.Value = "";
            // 
            // pLiveReview7
            // 
            this.pLiveReview7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(50.060546875D), Telerik.Reporting.Drawing.Unit.Pixel(0D));
            this.pLiveReview7.Name = "pLiveReview7";
            this.pLiveReview7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // txtPCode7
            // 
            this.txtPCode7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(55.0870361328125D), Telerik.Reporting.Drawing.Unit.Pixel(11.796223640441895D));
            this.txtPCode7.Name = "txtPCode7";
            this.txtPCode7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode7.Style.Font.Name = "Calibri";
            this.txtPCode7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.txtPCode7.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.txtPCode7.Value = "";
            // 
            // pLiveReview6
            // 
            this.pLiveReview6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(45.052978515625D), Telerik.Reporting.Drawing.Unit.Pixel(0D));
            this.pLiveReview6.Name = "pLiveReview6";
            this.pLiveReview6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // txtPCode6
            // 
            this.txtPCode6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(50.0794677734375D), Telerik.Reporting.Drawing.Unit.Pixel(11.796223640441895D));
            this.txtPCode6.Name = "txtPCode6";
            this.txtPCode6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode6.Style.Font.Name = "Calibri";
            this.txtPCode6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.txtPCode6.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.txtPCode6.Value = "";
            // 
            // pLiveReview5
            // 
            this.pLiveReview5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(40.04541015625D), Telerik.Reporting.Drawing.Unit.Pixel(0.0075581870041787624D));
            this.pLiveReview5.Name = "pLiveReview5";
            this.pLiveReview5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // txtPCode5
            // 
            this.txtPCode5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(45.0718994140625D), Telerik.Reporting.Drawing.Unit.Pixel(11.796223640441895D));
            this.txtPCode5.Name = "txtPCode5";
            this.txtPCode5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode5.Style.Font.Name = "Calibri";
            this.txtPCode5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.txtPCode5.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.txtPCode5.Value = "";
            // 
            // pLiveReview4
            // 
            this.pLiveReview4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(35.037841796875D), Telerik.Reporting.Drawing.Unit.Pixel(0.0075581870041787624D));
            this.pLiveReview4.Name = "pLiveReview4";
            this.pLiveReview4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // txtPCode4
            // 
            this.txtPCode4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(40.0643310546875D), Telerik.Reporting.Drawing.Unit.Pixel(11.796223640441895D));
            this.txtPCode4.Name = "txtPCode4";
            this.txtPCode4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode4.Style.Font.Name = "Calibri";
            this.txtPCode4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.txtPCode4.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.txtPCode4.TextWrap = true;
            this.txtPCode4.Value = "";
            // 
            // pLiveReview3
            // 
            this.pLiveReview3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(30.0302734375D), Telerik.Reporting.Drawing.Unit.Pixel(0.0075581870041787624D));
            this.pLiveReview3.Name = "pLiveReview3";
            this.pLiveReview3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // txtPCode3
            // 
            this.txtPCode3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(35.0567626953125D), Telerik.Reporting.Drawing.Unit.Pixel(11.796223640441895D));
            this.txtPCode3.Name = "txtPCode3";
            this.txtPCode3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode3.Style.Font.Name = "Calibri";
            this.txtPCode3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.txtPCode3.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.txtPCode3.Value = "";
            // 
            // pLiveReview2
            // 
            this.pLiveReview2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(25.022705078125D), Telerik.Reporting.Drawing.Unit.Pixel(0D));
            this.pLiveReview2.Name = "pLiveReview2";
            this.pLiveReview2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // txtPCode2
            // 
            this.txtPCode2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(30.0491943359375D), Telerik.Reporting.Drawing.Unit.Pixel(11.796223640441895D));
            this.txtPCode2.Name = "txtPCode2";
            this.txtPCode2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode2.Style.Font.Name = "Calibri";
            this.txtPCode2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.txtPCode2.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.txtPCode2.Value = "";
            // 
            // pLiveReview12
            // 
            this.pLiveReview12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(65.083251953125D), Telerik.Reporting.Drawing.Unit.Pixel(0.0075581870041787624D));
            this.pLiveReview12.Name = "pLiveReview12";
            this.pLiveReview12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // txtPCode8
            // 
            this.txtPCode8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(65.1021728515625D), Telerik.Reporting.Drawing.Unit.Pixel(11.796223640441895D));
            this.txtPCode8.Name = "txtPCode8";
            this.txtPCode8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode8.Style.Font.Name = "Calibri";
            this.txtPCode8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.txtPCode8.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.txtPCode8.Value = "";
            // 
            // txtPCode32
            // 
            this.txtPCode32.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.62685269117355347D), Telerik.Reporting.Drawing.Unit.Inch(0.17503936588764191D));
            this.txtPCode32.Name = "txtPCode32";
            this.txtPCode32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode32.Value = "";
            // 
            // txtPCode33
            // 
            this.txtPCode33.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.67901486158370972D), Telerik.Reporting.Drawing.Unit.Inch(0.17503936588764191D));
            this.txtPCode33.Name = "txtPCode33";
            this.txtPCode33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode33.Value = "";
            // 
            // txtPCode34
            // 
            this.txtPCode34.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.731177031993866D), Telerik.Reporting.Drawing.Unit.Inch(0.17503936588764191D));
            this.txtPCode34.Name = "txtPCode34";
            this.txtPCode34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode34.Value = "";
            // 
            // txtPCode35
            // 
            this.txtPCode35.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.78333920240402222D), Telerik.Reporting.Drawing.Unit.Inch(0.17503936588764191D));
            this.txtPCode35.Name = "txtPCode35";
            this.txtPCode35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode35.Value = "";
            // 
            // txtPCode36
            // 
            this.txtPCode36.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.83550137281417847D), Telerik.Reporting.Drawing.Unit.Inch(0.17503936588764191D));
            this.txtPCode36.Name = "txtPCode36";
            this.txtPCode36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode36.Value = "";
            // 
            // txtPCode37
            // 
            this.txtPCode37.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.88766354322433472D), Telerik.Reporting.Drawing.Unit.Inch(0.17503936588764191D));
            this.txtPCode37.Name = "txtPCode37";
            this.txtPCode37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode37.Value = "";
            // 
            // txtPCode38
            // 
            this.txtPCode38.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.939825713634491D), Telerik.Reporting.Drawing.Unit.Inch(0.17503936588764191D));
            this.txtPCode38.Name = "txtPCode38";
            this.txtPCode38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode38.Value = "";
            // 
            // txtPCode31
            // 
            this.txtPCode31.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.57469052076339722D), Telerik.Reporting.Drawing.Unit.Inch(0.17503936588764191D));
            this.txtPCode31.Name = "txtPCode31";
            this.txtPCode31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode31.Value = "";
            // 
            // txtPCode30
            // 
            this.txtPCode30.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.522528350353241D), Telerik.Reporting.Drawing.Unit.Inch(0.17503936588764191D));
            this.txtPCode30.Name = "txtPCode30";
            this.txtPCode30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode30.Value = "";
            // 
            // txtPCode29
            // 
            this.txtPCode29.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.47036615014076233D), Telerik.Reporting.Drawing.Unit.Inch(0.17503936588764191D));
            this.txtPCode29.Name = "txtPCode29";
            this.txtPCode29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode29.Value = "";
            // 
            // txtPCode28
            // 
            this.txtPCode28.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.41820397973060608D), Telerik.Reporting.Drawing.Unit.Inch(0.17503936588764191D));
            this.txtPCode28.Name = "txtPCode28";
            this.txtPCode28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode28.Value = "";
            // 
            // txtPCode27
            // 
            this.txtPCode27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.36604180932044983D), Telerik.Reporting.Drawing.Unit.Inch(0.17503936588764191D));
            this.txtPCode27.Name = "txtPCode27";
            this.txtPCode27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode27.Value = "";
            // 
            // txtPCode26
            // 
            this.txtPCode26.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.31387963891029358D), Telerik.Reporting.Drawing.Unit.Inch(0.17503936588764191D));
            this.txtPCode26.Name = "txtPCode26";
            this.txtPCode26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode26.Value = "";
            // 
            // txtPCode25
            // 
            this.txtPCode25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.26171746850013733D), Telerik.Reporting.Drawing.Unit.Inch(0.17503936588764191D));
            this.txtPCode25.Name = "txtPCode25";
            this.txtPCode25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode25.Value = "";
            // 
            // txtPCode23
            // 
            this.txtPCode23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.15739314258098602D), Telerik.Reporting.Drawing.Unit.Inch(0.17503936588764191D));
            this.txtPCode23.Name = "txtPCode23";
            this.txtPCode23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode23.Value = "";
            // 
            // txtPCode24
            // 
            this.txtPCode24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.20955531299114227D), Telerik.Reporting.Drawing.Unit.Inch(0.17503936588764191D));
            this.txtPCode24.Name = "txtPCode24";
            this.txtPCode24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode24.Value = "";
            // 
            // txtPCode22
            // 
            this.txtPCode22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10523096472024918D), Telerik.Reporting.Drawing.Unit.Inch(0.17503936588764191D));
            this.txtPCode22.Name = "txtPCode22";
            this.txtPCode22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode22.Value = "";
            // 
            // txtPCode21
            // 
            this.txtPCode21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.053068798035383224D), Telerik.Reporting.Drawing.Unit.Inch(0.17503936588764191D));
            this.txtPCode21.Name = "txtPCode21";
            this.txtPCode21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode21.Value = "";
            // 
            // txtPCode20
            // 
            this.txtPCode20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0009066264028660953D), Telerik.Reporting.Drawing.Unit.Inch(0.17503936588764191D));
            this.txtPCode20.Name = "txtPCode20";
            this.txtPCode20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode20.Value = "";
            // 
            // txtPCode19
            // 
            this.txtPCode19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.938958466053009D), Telerik.Reporting.Drawing.Unit.Inch(0.12287735939025879D));
            this.txtPCode19.Name = "txtPCode19";
            this.txtPCode19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode19.Value = "";
            // 
            // txtPCode17
            // 
            this.txtPCode17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.88679629564285278D), Telerik.Reporting.Drawing.Unit.Inch(0.12287735939025879D));
            this.txtPCode17.Name = "txtPCode17";
            this.txtPCode17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode17.Value = "";
            // 
            // txtPCode18
            // 
            this.txtPCode18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.83463412523269653D), Telerik.Reporting.Drawing.Unit.Inch(0.12287735939025879D));
            this.txtPCode18.Name = "txtPCode18";
            this.txtPCode18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode18.Value = "";
            // 
            // txtPCode16
            // 
            this.txtPCode16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.78247195482254028D), Telerik.Reporting.Drawing.Unit.Inch(0.12287735939025879D));
            this.txtPCode16.Name = "txtPCode16";
            this.txtPCode16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode16.Value = "";
            // 
            // txtPCode15
            // 
            this.txtPCode15.Angle = 0D;
            this.txtPCode15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.730309784412384D), Telerik.Reporting.Drawing.Unit.Inch(0.12287735939025879D));
            this.txtPCode15.Name = "txtPCode15";
            this.txtPCode15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            this.txtPCode15.Value = "";
            // 
            // pLiveReview38
            // 
            this.pLiveReview38.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(90.0870361328125D), Telerik.Reporting.Drawing.Unit.Pixel(5.0075531005859375D));
            this.pLiveReview38.Name = "pLiveReview38";
            this.pLiveReview38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // pLiveReview32
            // 
            this.pLiveReview32.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(60.0870361328125D), Telerik.Reporting.Drawing.Unit.Pixel(5.0075531005859375D));
            this.pLiveReview32.Name = "pLiveReview32";
            this.pLiveReview32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // pLiveReview33
            // 
            this.pLiveReview33.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(65.0870361328125D), Telerik.Reporting.Drawing.Unit.Pixel(5.0075531005859375D));
            this.pLiveReview33.Name = "pLiveReview33";
            this.pLiveReview33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // pLiveReview34
            // 
            this.pLiveReview34.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.73007327318191528D), Telerik.Reporting.Drawing.Unit.Inch(0.052162010222673416D));
            this.pLiveReview34.Name = "pLiveReview34";
            this.pLiveReview34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // pLiveReview37
            // 
            this.pLiveReview37.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(85.0870361328125D), Telerik.Reporting.Drawing.Unit.Pixel(5.0075531005859375D));
            this.pLiveReview37.Name = "pLiveReview37";
            this.pLiveReview37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // pLiveReview36
            // 
            this.pLiveReview36.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(80.0870361328125D), Telerik.Reporting.Drawing.Unit.Pixel(5.0075531005859375D));
            this.pLiveReview36.Name = "pLiveReview36";
            this.pLiveReview36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // pLiveReview35
            // 
            this.pLiveReview35.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(75.0870361328125D), Telerik.Reporting.Drawing.Unit.Pixel(5.0075531005859375D));
            this.pLiveReview35.Name = "pLiveReview35";
            this.pLiveReview35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // pLiveReview29
            // 
            this.pLiveReview29.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(45.0870361328125D), Telerik.Reporting.Drawing.Unit.Pixel(5.0075531005859375D));
            this.pLiveReview29.Name = "pLiveReview29";
            this.pLiveReview29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // pLiveReview30
            // 
            this.pLiveReview30.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(50.0870361328125D), Telerik.Reporting.Drawing.Unit.Pixel(5.0075531005859375D));
            this.pLiveReview30.Name = "pLiveReview30";
            this.pLiveReview30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // pLiveReview31
            // 
            this.pLiveReview31.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(55.0870361328125D), Telerik.Reporting.Drawing.Unit.Pixel(5.0075531005859375D));
            this.pLiveReview31.Name = "pLiveReview31";
            this.pLiveReview31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // pLiveReview28
            // 
            this.pLiveReview28.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.41757330298423767D), Telerik.Reporting.Drawing.Unit.Inch(0.052162010222673416D));
            this.pLiveReview28.Name = "pLiveReview28";
            this.pLiveReview28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // pLiveReview26
            // 
            this.pLiveReview26.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.042573291808366776D), Telerik.Reporting.Drawing.Unit.Inch(0.052162010222673416D));
            this.pLiveReview26.Name = "pLiveReview26";
            this.pLiveReview26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // pLiveReview27
            // 
            this.pLiveReview27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(10.0870361328125D), Telerik.Reporting.Drawing.Unit.Pixel(5.0075531005859375D));
            this.pLiveReview27.Name = "pLiveReview27";
            this.pLiveReview27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // pLiveReview25
            // 
            this.pLiveReview25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(15.0870361328125D), Telerik.Reporting.Drawing.Unit.Pixel(5.0075531005859375D));
            this.pLiveReview25.Name = "pLiveReview25";
            this.pLiveReview25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // pLiveReview24
            // 
            this.pLiveReview24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.20923995971679688D), Telerik.Reporting.Drawing.Unit.Inch(0.052162010222673416D));
            this.pLiveReview24.Name = "pLiveReview24";
            this.pLiveReview24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // pLiveReview21
            // 
            this.pLiveReview21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(35.0870361328125D), Telerik.Reporting.Drawing.Unit.Pixel(5.0075531005859375D));
            this.pLiveReview21.Name = "pLiveReview21";
            this.pLiveReview21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // pLiveReview22
            // 
            this.pLiveReview22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(30.0870361328125D), Telerik.Reporting.Drawing.Unit.Pixel(5.0075531005859375D));
            this.pLiveReview22.Name = "pLiveReview22";
            this.pLiveReview22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // pLiveReview23
            // 
            this.pLiveReview23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(25.0870361328125D), Telerik.Reporting.Drawing.Unit.Pixel(5.0075531005859375D));
            this.pLiveReview23.Name = "pLiveReview23";
            this.pLiveReview23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // pLiveReview20
            // 
            this.pLiveReview20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(0.0870361328125D), Telerik.Reporting.Drawing.Unit.Pixel(5.0075531005859375D));
            this.pLiveReview20.Name = "pLiveReview20";
            this.pLiveReview20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // pLiveReview18
            // 
            this.pLiveReview18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(80.0794677734375D), Telerik.Reporting.Drawing.Unit.Pixel(0.792449951171875D));
            this.pLiveReview18.Name = "pLiveReview18";
            this.pLiveReview18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // pLiveReview17
            // 
            this.pLiveReview17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(85.0794677734375D), Telerik.Reporting.Drawing.Unit.Pixel(0.792449951171875D));
            this.pLiveReview17.Name = "pLiveReview17";
            this.pLiveReview17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // pLiveReview19
            // 
            this.pLiveReview19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(90.0794677734375D), Telerik.Reporting.Drawing.Unit.Pixel(0.792449951171875D));
            this.pLiveReview19.Name = "pLiveReview19";
            this.pLiveReview19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // pLiveReview16
            // 
            this.pLiveReview16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(75.0794677734375D), Telerik.Reporting.Drawing.Unit.Pixel(0.792449951171875D));
            this.pLiveReview16.Name = "pLiveReview16";
            this.pLiveReview16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // pLiveReview15
            // 
            this.pLiveReview15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(70.0794677734375D), Telerik.Reporting.Drawing.Unit.Pixel(0.792449951171875D));
            this.pLiveReview15.Name = "pLiveReview15";
            this.pLiveReview15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(5D), Telerik.Reporting.Drawing.Unit.Pixel(5D));
            // 
            // textBox65
            // 
            this.textBox65.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.90000003576278687D), Telerik.Reporting.Drawing.Unit.Inch(0.0042060744017362595D));
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(96D), Telerik.Reporting.Drawing.Unit.Inch(0.099999994039535522D));
            this.textBox65.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox65.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox65.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox65.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox65.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox65.Style.Font.Bold = false;
            this.textBox65.Style.Font.Name = "Calibri";
            this.textBox65.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox65.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox65.Value = "=Fields.OrderNo";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.3245476715439963E-08D), Telerik.Reporting.Drawing.Unit.Inch(0.0042060744017362595D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(86.4000015258789D), Telerik.Reporting.Drawing.Unit.Inch(0.10000001639127731D));
            this.textBox1.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox1.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox1.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox1.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox1.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox1.Style.Font.Bold = false;
            this.textBox1.Style.Font.Name = "Calibri";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox1.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "=Fields.LastName";
            // 
            // pictureBox6
            // 
            formattingRule20.Filters.Add(new Telerik.Reporting.Filter("Fields.OrderType", Telerik.Reporting.FilterOperator.Equal, "False"));
            formattingRule20.Style.Visible = false;
            this.pictureBox6.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule20});
            this.pictureBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.90007877349853516D), Telerik.Reporting.Drawing.Unit.Inch(0.17916671931743622D));
            this.pictureBox6.MimeType = "image/png";
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(12.396219253540039D), Telerik.Reporting.Drawing.Unit.Pixel(9.600001335144043D));
            this.pictureBox6.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox6.Style.Visible = false;
            this.pictureBox6.Value = ((object)(resources.GetObject("pictureBox6.Value")));
            // 
            // pictureBox5
            // 
            formattingRule21.Filters.Add(new Telerik.Reporting.Filter("Fields.IsSyncpod", Telerik.Reporting.FilterOperator.Equal, "False"));
            formattingRule21.Style.Visible = false;
            this.pictureBox5.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule21});
            this.pictureBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.0292849540710449D), Telerik.Reporting.Drawing.Unit.Inch(0.17507870495319367D));
            this.pictureBox5.MimeType = "image/png";
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(12.396219253540039D), Telerik.Reporting.Drawing.Unit.Pixel(9.9924478530883789D));
            this.pictureBox5.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox5.Style.Visible = false;
            this.pictureBox5.Value = ((object)(resources.GetObject("pictureBox5.Value")));
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.4542455673217773D), Telerik.Reporting.Drawing.Unit.Inch(0.12563534080982208D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(73.399993896484375D), Telerik.Reporting.Drawing.Unit.Inch(0.11115053296089172D));
            this.textBox2.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox2.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox2.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox2.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox2.Style.Font.Bold = false;
            this.textBox2.Style.Font.Name = "Calibri";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox2.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox2.Value = "= Fields.StateCode";
            // 
            // pictureBox7
            // 
            formattingRule22.Filters.Add(new Telerik.Reporting.Filter("Fields.IsTiered", Telerik.Reporting.FilterOperator.Equal, "False"));
            formattingRule22.Style.Visible = false;
            this.pictureBox7.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule22});
            this.pictureBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.1584911346435547D), Telerik.Reporting.Drawing.Unit.Inch(0.17507870495319367D));
            this.pictureBox7.MimeType = "image/png";
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(12.396219253540039D), Telerik.Reporting.Drawing.Unit.Pixel(9.9924478530883789D));
            this.pictureBox7.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox7.Style.Visible = false;
            this.pictureBox7.Value = ((object)(resources.GetObject("pictureBox7.Value")));
            // 
            // textBox69
            // 
            this.textBox69.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.3000006675720215D), Telerik.Reporting.Drawing.Unit.Inch(-3.3113691788599908E-09D));
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(38.4000244140625D), Telerik.Reporting.Drawing.Unit.Inch(0.17500001192092896D));
            this.textBox69.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox69.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox69.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox69.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox69.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox69.Style.Font.Bold = false;
            this.textBox69.Style.Font.Name = "Calibri";
            this.textBox69.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox69.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox69.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox69.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox69.Value = "= ChangeFormat(Fields.Total.ToString())";
            // 
            // textBox7
            // 
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.3375788927078247D), Telerik.Reporting.Drawing.Unit.Inch(0.10597109794616699D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(67.20001220703125D), Telerik.Reporting.Drawing.Unit.Inch(0.066627219319343567D));
            this.textBox7.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox7.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox7.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox7.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox7.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox7.Style.Font.Bold = false;
            this.textBox7.Style.Font.Name = "Calibri";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox7.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.Value = "=Fields.PackageName";
            // 
            // textBox8
            // 
            formattingRule23.Filters.Add(new Telerik.Reporting.Filter("=Fields.packagenamedisplay <> \"0\"", Telerik.Reporting.FilterOperator.Equal, "true"));
            formattingRule23.Style.Visible = false;
            this.textBox8.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule23});
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.90000009536743164D), Telerik.Reporting.Drawing.Unit.Inch(0.10428476333618164D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(42D), Telerik.Reporting.Drawing.Unit.Inch(0.070000000298023224D));
            this.textBox8.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox8.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox8.Style.Font.Name = "Calibri";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox8.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.Style.Visible = true;
            this.textBox8.Value = "Package:";
            // 
            // reportFooterSection1
            // 
            this.reportFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.22233159840106964D);
            this.reportFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox14,
            this.textBox15});
            this.reportFooterSection1.Name = "reportFooterSection1";
            this.reportFooterSection1.Style.BackgroundColor = System.Drawing.Color.Gray;
            // 
            // textBox14
            // 
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.013888888992369175D), Telerik.Reporting.Drawing.Unit.Inch(0.031641218811273575D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.736657977104187D), Telerik.Reporting.Drawing.Unit.Inch(0.16677601635456085D));
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Font.Name = "Calibri";
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox14.Value = "Grand Total";
            // 
            // textBox15
            // 
            this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.7152776718139648D), Telerik.Reporting.Drawing.Unit.Inch(0.0347222238779068D));
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.993602454662323D), Telerik.Reporting.Drawing.Unit.Inch(0.18760935962200165D));
            this.textBox15.Style.Font.Bold = true;
            this.textBox15.Style.Font.Name = "Calibri";
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox15.Value = "= ChangeFormat(Sum(IIf(Fields.LastName=\"Discount\",(-1)*Fields.Total,IIf(Fields.La" +
    "stName=\"Adjustment\",(-1)*Fields.Total,Fields.Total))).ToString())";
            // 
            // SRItemizedReport
            // 
            group1.GroupFooter = this.groupFooterSection1;
            group1.GroupHeader = this.groupHeaderSection1;
            group1.Groupings.Add(new Telerik.Reporting.Grouping("=Fields.Location"));
            group1.Name = "LocationGrouping";
            group2.GroupFooter = this.groupFooterSection2;
            group2.GroupHeader = this.groupHeaderSection2;
            group2.Groupings.Add(new Telerik.Reporting.Grouping("=Fields.ReferenceCode"));
            group2.Name = "Referencegrouping";
            group3.GroupFooter = this.groupFooterSection;
            group3.GroupHeader = this.groupHeaderSection;
            group3.Groupings.Add(new Telerik.Reporting.Grouping("=Fields.UserEmail"));
            group3.Name = "UserEmailgrouping";
            this.Groups.AddRange(new Telerik.Reporting.Group[] {
            group1,
            group2,
            group3});
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.groupHeaderSection1,
            this.groupFooterSection1,
            this.groupHeaderSection2,
            this.groupFooterSection2,
            this.groupHeaderSection,
            this.groupFooterSection,
            this.detail,
            this.reportFooterSection1});
            this.Name = "SRItemizedReport";
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(7.7227692604064941D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.TextBox textBox73;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox68;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.TextBox textBox64;

        #region Custom Functions
        public static string GetApplicantName(string FirstName, string LastName)
        {
            CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;
            string ApplicantName = "";
            if (!string.IsNullOrEmpty(LastName))
            {

                ApplicantName = textInfo.ToTitleCase(LastName.Trim().ToLower()) + " " + textInfo.ToTitleCase(FirstName.Trim().ToLower());
            }
            else
            {
                if (!string.IsNullOrEmpty(FirstName))
                    ApplicantName = textInfo.ToTitleCase(FirstName.ToLower());
            }
            return ApplicantName;
        }

        public static string GetLocation(string Location, string StateCode)
        {
            return Location + "," + StateCode;
        }
        public static string ChangeFormat(string value)// ND-29
        {
            string ChangedValue = string.Empty;
            if (!string.IsNullOrEmpty(value))
            {
                ChangedValue = string.Format("{0:C}", decimal.Parse(value));
            }
            return ChangedValue;
        }
        public static string GetDataSource(string ReportsWithStatus)
        {

            string DataSourse = string.Empty;
            if (!string.IsNullOrEmpty(ReportsWithStatus))
            {
                string ReportNames = ReportsWithStatus;

                string[] strSplit = ReportNames.Split(',');

                // str like (OFAC_0_1) means first _0 for live runner and second _1 for review status                       * /
                for (int Irow = 0; Irow < strSplit.Length; Irow++)
                {
                    if (strSplit[Irow] != "")
                    {
                        if (Irow > 0)
                        {
                            DataSourse = DataSourse + ", ";
                        }
                        string[] subSplit = strSplit[Irow].Split('_');
                        if (subSplit.Count() > 2)
                        {
                            DataSourse += subSplit[0];
                            ////this for live runner
                            //if (subSplit[1] == "1")
                            //{
                            //    if (File.Exists(Request.PhysicalApplicationPath + ImgLiveRun))
                            //    {
                            //        Cnk = new Chunk(ImgLogoLiveRun, 0, -2);
                            //        pr1.Add(Cnk);
                            //    }
                            //}
                            ////this for for review report
                            //if (subSplit[2] == "1")
                            //{
                            //    if (File.Exists(Request.PhysicalApplicationPath + ImgReview))
                            //    {
                            //        Cnk = new Chunk(ImgLogoReview, 0, -2);
                            //        pr1.Add(Cnk);
                            //    }
                            //}
                        }
                    }
                }
            }
            return DataSourse;
        }

        public static string GetTotalReportAmount(decimal? PackageAmount, decimal? ReportAmount, string IsDrugTestingOrder, int OrderId)
        {
            BALBillingData ObjBALBillingData = new BALBillingData();
            decimal? TotalReportAmount = 0.00m;
            decimal? TotalOrderCost = 0.00m;
            decimal TotalPackageAmount = 0;
            try
            {
                if (!string.IsNullOrEmpty(IsDrugTestingOrder))
                {
                    List<tblOrderDetail> ObjOrderDetails = ObjBALBillingData.GetOrderDetailByOrderId(OrderId);

                    foreach (tblOrderDetail ObjtblOrderDetail in ObjOrderDetails)
                    {
                        TotalReportAmount = TotalOrderCost + ((ObjtblOrderDetail.ProductQty != null) ? ObjtblOrderDetail.ProductQty : 0) * ((ObjtblOrderDetail.ReportAmount != null) ? ObjtblOrderDetail.ReportAmount : 0);
                        TotalOrderCost = TotalReportAmount;
                    }
                }
                else
                {
                    TotalPackageAmount = (PackageAmount != null) ? decimal.Parse(PackageAmount.ToString()) : 0;
                    TotalReportAmount = TotalPackageAmount + ReportAmount;
                }
            }
            catch
            {
                TotalOrderCost = 0;
            }
            return "$" + String.Format("{0:0.00}", TotalReportAmount);
        }

        public static string GetAdditionalFeeAmount(decimal? TotalAdditionalFeeAmount, bool IsDrugTestingOrder, int OrderId)
        {
            BALBillingData ObjBALBillingData = new BALBillingData();
            string TotalAdditionalFee = "";

            if (IsDrugTestingOrder)
            {
                TotalAdditionalFeeAmount = ObjBALBillingData.GetAdditionalFee(OrderId);
            }

            if (TotalAdditionalFeeAmount == null || TotalAdditionalFeeAmount == 0)
            {
                TotalAdditionalFee = "";
            }
            else
            {
                TotalAdditionalFee = "$" + String.Format("{0:0.00}", TotalAdditionalFeeAmount);
            }
            return TotalAdditionalFee;
        }

        public static string GetOrderCost(decimal? PackageAmount, decimal? ReportAmount, decimal? AdditionalFee, string IsDrugTestingOrder, int OrderId)
        {
            BALBillingData ObjBALBillingData = new BALBillingData();
            decimal TotalAdditionalFeeAmount = 0;
            decimal? TotalReportAmount = 0.00m;
            decimal? TotalOrderCost = 0.00m;
            decimal TotalPackageAmount = 0;
            try
            {
            if (!string.IsNullOrEmpty(IsDrugTestingOrder))
            {
                List<tblOrderDetail> ObjOrderDetails = ObjBALBillingData.GetOrderDetailByOrderId(OrderId);

                foreach (tblOrderDetail ObjtblOrderDetail in ObjOrderDetails)
                {
                    TotalReportAmount = TotalOrderCost + ((ObjtblOrderDetail.ProductQty != null) ? ObjtblOrderDetail.ProductQty : 0) * ((ObjtblOrderDetail.ReportAmount != null) ? ObjtblOrderDetail.ReportAmount : 0);
                    TotalOrderCost = TotalReportAmount;
                }
            }
            else
            {
                TotalPackageAmount = (PackageAmount!=null)?decimal.Parse(PackageAmount.ToString()):0;

                TotalReportAmount = TotalPackageAmount + ReportAmount;
                TotalAdditionalFeeAmount = (AdditionalFee != null) ? decimal.Parse(AdditionalFee.ToString()) : 0; 
                TotalOrderCost = TotalReportAmount + TotalAdditionalFeeAmount;
            }
            }
            catch
            {
                TotalOrderCost = 0;
            }
            return "$" + String.Format("{0:0.00}", TotalOrderCost);
        }
        #endregion

        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.PictureBox pictureBox2;
        private Telerik.Reporting.PictureBox pictureBox3;
        private Telerik.Reporting.PictureBox pictureBox4;
        private Telerik.Reporting.PictureBox pictureBox5;
        private Telerik.Reporting.Panel panel2;
        private Telerik.Reporting.TextBox txtPCode1;
        private Telerik.Reporting.PictureBox pLiveReview1;
        private Telerik.Reporting.PictureBox pLiveReview14;
        private Telerik.Reporting.TextBox txtPCode14;
        private Telerik.Reporting.PictureBox pLiveReview10;
        private Telerik.Reporting.TextBox txtPCode13;
        private Telerik.Reporting.PictureBox pLiveReview13;
        private Telerik.Reporting.TextBox txtPCode12;
        private Telerik.Reporting.PictureBox pLiveReview11;
        private Telerik.Reporting.TextBox txtPCode11;
        private Telerik.Reporting.PictureBox pLiveReview9;
        private Telerik.Reporting.TextBox txtPCode10;
        private Telerik.Reporting.PictureBox pLiveReview8;
        private Telerik.Reporting.TextBox txtPCode9;
        private Telerik.Reporting.PictureBox pLiveReview7;
        private Telerik.Reporting.TextBox txtPCode7;
        private Telerik.Reporting.PictureBox pLiveReview6;
        private Telerik.Reporting.TextBox txtPCode6;
        private Telerik.Reporting.PictureBox pLiveReview5;
        private Telerik.Reporting.TextBox txtPCode5;
        private Telerik.Reporting.PictureBox pLiveReview4;
        private Telerik.Reporting.TextBox txtPCode4;
        private Telerik.Reporting.PictureBox pLiveReview3;
        private Telerik.Reporting.TextBox txtPCode3;
        private Telerik.Reporting.PictureBox pLiveReview2;
        private Telerik.Reporting.TextBox txtPCode2;
        private Telerik.Reporting.PictureBox pLiveReview12;
        private Telerik.Reporting.TextBox txtPCode8;
        private Telerik.Reporting.PictureBox pictureBox6;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.PictureBox pictureBox7;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.GroupFooterSection groupFooterSection;
        private Telerik.Reporting.GroupHeaderSection groupHeaderSection;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.TextBox txtPCode32;
        private Telerik.Reporting.TextBox txtPCode33;
        private Telerik.Reporting.TextBox txtPCode34;
        private Telerik.Reporting.TextBox txtPCode35;
        private Telerik.Reporting.TextBox txtPCode36;
        private Telerik.Reporting.TextBox txtPCode37;
        private Telerik.Reporting.TextBox txtPCode38;
        private Telerik.Reporting.TextBox txtPCode31;
        private Telerik.Reporting.TextBox txtPCode30;
        private Telerik.Reporting.TextBox txtPCode29;
        private Telerik.Reporting.TextBox txtPCode28;
        private Telerik.Reporting.TextBox txtPCode27;
        private Telerik.Reporting.TextBox txtPCode26;
        private Telerik.Reporting.TextBox txtPCode25;
        private Telerik.Reporting.TextBox txtPCode23;
        private Telerik.Reporting.TextBox txtPCode24;
        private Telerik.Reporting.TextBox txtPCode22;
        private Telerik.Reporting.TextBox txtPCode21;
        private Telerik.Reporting.TextBox txtPCode20;
        private Telerik.Reporting.TextBox txtPCode19;
        private Telerik.Reporting.TextBox txtPCode17;
        private Telerik.Reporting.TextBox txtPCode18;
        private Telerik.Reporting.TextBox txtPCode16;
        private Telerik.Reporting.TextBox txtPCode15;
        private Telerik.Reporting.PictureBox pLiveReview18;
        private Telerik.Reporting.PictureBox pLiveReview17;
        private Telerik.Reporting.PictureBox pLiveReview19;
        private Telerik.Reporting.PictureBox pLiveReview16;
        private Telerik.Reporting.PictureBox pLiveReview15;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.PictureBox pLiveReview38;
        private Telerik.Reporting.PictureBox pLiveReview32;
        private Telerik.Reporting.PictureBox pLiveReview33;
        private Telerik.Reporting.PictureBox pLiveReview34;
        private Telerik.Reporting.PictureBox pLiveReview37;
        private Telerik.Reporting.PictureBox pLiveReview36;
        private Telerik.Reporting.PictureBox pLiveReview35;
        private Telerik.Reporting.PictureBox pLiveReview29;
        private Telerik.Reporting.PictureBox pLiveReview30;
        private Telerik.Reporting.PictureBox pLiveReview31;
        private Telerik.Reporting.PictureBox pLiveReview28;
        private Telerik.Reporting.PictureBox pLiveReview26;
        private Telerik.Reporting.PictureBox pLiveReview27;
        private Telerik.Reporting.PictureBox pLiveReview25;
        private Telerik.Reporting.PictureBox pLiveReview24;
        private Telerik.Reporting.PictureBox pLiveReview21;
        private Telerik.Reporting.PictureBox pLiveReview22;
        private Telerik.Reporting.PictureBox pLiveReview23;
        private Telerik.Reporting.PictureBox pLiveReview20;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.GroupHeaderSection groupHeaderSection2;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.GroupFooterSection groupFooterSection2;
        public Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox5;
        public Telerik.Reporting.TextBox textBox9;
        public Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.GroupFooterSection groupFooterSection1;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.GroupHeaderSection groupHeaderSection1;
        private Telerik.Reporting.ReportFooterSection reportFooterSection1;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox14;
    }
}