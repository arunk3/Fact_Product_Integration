using System;
namespace EmergeBilling
{
    partial class BillingStatement
    {
        public static string CompanyCode { get; set; }
        public static string TotalItemizedReports { get; set; }
        public static string Month { get; set; }
        public static string Year { get; set; }
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.InstanceReportSource instanceReportSource1 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource2 = new Telerik.Reporting.InstanceReportSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BillingStatement));
            Telerik.Reporting.InstanceReportSource instanceReportSource3 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource4 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource5 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource7 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource6 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource8 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource9 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource10 = new Telerik.Reporting.InstanceReportSource();
            this.srBillingSummary1 = new EmergeBilling.SRBillingSummary();
            this.srCompanyLocations1 = new EmergeBilling.SRCompanyLocations();
            this.srPageBreack21 = new EmergeBilling.SRPageBreack2();
            this.srBillingStatement1 = new EmergeBilling.SRBillingStatement();
            this.srProducts2 = new EmergeBilling.SRProducts();
            this.srAdjustments2 = new EmergeBilling.SRAdjustments();
            this.srPageBreack2 = new EmergeBilling.SRPageBreack();
            this.srBillingSummary2 = new EmergeBilling.SRBillingSummary();
            this.srItemizedReport1 = new EmergeBilling.SRItemizedReport();
            this.srCompanyLocations2 = new EmergeBilling.SRCompanyLocations();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.detail = new Telerik.Reporting.DetailSection();
            this.srBillingSummary = new Telerik.Reporting.SubReport();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.srCompanyLocations = new Telerik.Reporting.SubReport();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.srItemized = new Telerik.Reporting.SubReport();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.panel6 = new Telerik.Reporting.Panel();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox71 = new Telerik.Reporting.TextBox();
            this.textBox72 = new Telerik.Reporting.TextBox();
            this.lblReport = new Telerik.Reporting.TextBox();
            this.lblIsDue1 = new Telerik.Reporting.TextBox();
            this.lblIsDue2 = new Telerik.Reporting.TextBox();
            this.textBox86 = new Telerik.Reporting.TextBox();
            this.panel7 = new Telerik.Reporting.Panel();
            this.textBox87 = new Telerik.Reporting.TextBox();
            this.txtBillingPhone = new Telerik.Reporting.TextBox();
            this.textBox89 = new Telerik.Reporting.TextBox();
            this.textBox90 = new Telerik.Reporting.TextBox();
            this.txtBillingId = new Telerik.Reporting.TextBox();
            this.textBox92 = new Telerik.Reporting.TextBox();
            this.srPageBreak1 = new Telerik.Reporting.SubReport();
            this.panel8 = new Telerik.Reporting.Panel();
            this.srBillingStatement = new Telerik.Reporting.SubReport();
            this.panel12 = new Telerik.Reporting.Panel();
            this.srProductsPerLocation = new Telerik.Reporting.SubReport();
            this.pnlAdjustmentsHeadings = new Telerik.Reporting.Panel();
            this.textBox103 = new Telerik.Reporting.TextBox();
            this.textBox104 = new Telerik.Reporting.TextBox();
            this.textBox105 = new Telerik.Reporting.TextBox();
            this.srAdjustment = new Telerik.Reporting.SubReport();
            this.srPageBreak2 = new Telerik.Reporting.SubReport();
            this.panel4 = new Telerik.Reporting.Panel();
            this.textBox76 = new Telerik.Reporting.TextBox();
            this.textBox77 = new Telerik.Reporting.TextBox();
            this.textBox79 = new Telerik.Reporting.TextBox();
            this.textBox80 = new Telerik.Reporting.TextBox();
            this.textBox81 = new Telerik.Reporting.TextBox();
            this.textBox82 = new Telerik.Reporting.TextBox();
            this.textBox83 = new Telerik.Reporting.TextBox();
            this.textBox84 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.textBox78 = new Telerik.Reporting.TextBox();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.panel5 = new Telerik.Reporting.Panel();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.textBox75 = new Telerik.Reporting.TextBox();
            this.panel11 = new Telerik.Reporting.Panel();
            this.textBox99 = new Telerik.Reporting.TextBox();
            this.textBox100 = new Telerik.Reporting.TextBox();
            this.textBox101 = new Telerik.Reporting.TextBox();
            this.panel9 = new Telerik.Reporting.Panel();
            this.textBox93 = new Telerik.Reporting.TextBox();
            this.textBox94 = new Telerik.Reporting.TextBox();
            this.textBox95 = new Telerik.Reporting.TextBox();
            this.srBillingSummaryApi = new Telerik.Reporting.SubReport();
            this.srCompanyLocationsApi = new Telerik.Reporting.SubReport();
            ((System.ComponentModel.ISupportInitialize)(this.srBillingSummary1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.srCompanyLocations1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.srPageBreack21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.srBillingStatement1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.srProducts2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.srAdjustments2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.srPageBreack2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.srBillingSummary2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.srCompanyLocations2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.srItemizedReport1)).BeginInit();
            // 
            // srBillingSummary1
            // 
            this.srBillingSummary1.Name = "SRBillingSummary1";
            // 
            // srCompanyLocations1
            // 
            this.srCompanyLocations1.Name = "SRCompanyLocations1";
            // 
            // srItemizedReport1
            // 
            this.srItemizedReport1.Name = "SRItemizedReport";
            //
            // srPageBreack21
            // 
            this.srPageBreack21.Name = "SRPageBreack2";
            // 
            // srBillingStatement1
            // 
            this.srBillingStatement1.Name = "SRBillingStatement";
            // 
            // srProducts2
            // 
            this.srProducts2.Name = "SRProductsPerLocation2";
            // 
            // srAdjustments2
            // 
            this.srAdjustments2.Name = "SRAdjustments2";
            // 
            // srPageBreack2
            // 
            this.srPageBreack2.Name = "SRPageBreack";
            // 
            // srBillingSummary2
            // 
            this.srBillingSummary2.Name = "SRBillingSummary2";
            // 
            // srCompanyLocations2
            // 
            this.srCompanyLocations2.Name = "SRCompanyLocations2";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.1000000610947609D);
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            this.pageHeaderSection1.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(13.200000762939453D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.srBillingSummary,
            this.textBox1,
            this.srCompanyLocations,
            this.textBox45,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox18,
            this.textBox19,
            this.textBox20,
            this.textBox21,
            this.textBox22,
            this.textBox23,
            this.textBox44,
            this.textBox2,
            this.textBox46,
            this.pictureBox1,
            this.textBox47,
            this.textBox48,
            this.textBox50,
            this.textBox49,
            this.textBox51,
            this.textBox29,
            this.textBox30,
            this.textBox31,
            this.textBox32,
            this.textBox33,
            this.textBox35,
            this.textBox37,
            this.textBox39,
            this.textBox41,
            this.textBox42,
            this.textBox52,
            this.textBox53,
            this.textBox54,
            this.textBox55,
            this.textBox56,
            this.textBox57,
            this.textBox58,
            this.textBox59,
            this.textBox60,
            this.textBox61,
            this.textBox24,
            this.textBox62,
            this.textBox63,
            this.srItemized,
            this.textBox65,
            this.panel6,
            this.lblIsDue1,
            this.lblIsDue2,
            this.textBox86,
            this.panel7,
            this.srPageBreak1,
            this.panel8,
            this.panel12,
            this.srPageBreak2,
            this.panel4});
            this.detail.Name = "detail";
            this.detail.Style.Font.Name = "Calibri";
            this.detail.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            // 
            // srBillingSummary
            // 
            this.srBillingSummary.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10208383947610855D), Telerik.Reporting.Drawing.Unit.Inch(9.5D));
            this.srBillingSummary.Name = "srBillingSummary";
            instanceReportSource1.ReportDocument = this.srBillingSummary1;
            this.srBillingSummary.ReportSource = instanceReportSource1;
            this.srBillingSummary.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(0.79999983310699463D));
            this.srBillingSummary.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srBillingSummary.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srBillingSummary.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srBillingSummary.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srBillingSummary.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srBillingSummary.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.srBillingSummary.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // textBox1
            // 
            this.textBox1.CanShrink = false;
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.099999986588954926D), Telerik.Reporting.Drawing.Unit.Inch(9.1000003814697266D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999983787536621D), Telerik.Reporting.Drawing.Unit.Pixel(28.799966812133789D));
            this.textBox1.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "Statement Summary";
            // 
            // srCompanyLocations
            // 
            this.srCompanyLocations.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.3020811080932617D), Telerik.Reporting.Drawing.Unit.Inch(9.5D));
            this.srCompanyLocations.Name = "srCompanyLocations";
            instanceReportSource2.ReportDocument = this.srCompanyLocations1;
            this.srCompanyLocations.ReportSource = instanceReportSource2;
            this.srCompanyLocations.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(0.799999475479126D));
            this.srCompanyLocations.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srCompanyLocations.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srCompanyLocations.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srCompanyLocations.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srCompanyLocations.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srCompanyLocations.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.srCompanyLocations.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // textBox45
            // 
            this.textBox45.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1000001430511475D), Telerik.Reporting.Drawing.Unit.Inch(0.29999908804893494D));
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox45.Style.Font.Bold = true;
            this.textBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox45.Value = "Billing Information";
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D), Telerik.Reporting.Drawing.Unit.Inch(0.30208221077919006D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Name = "Calibri";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox3.Value = "Company Information";
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(0.60208225250244141D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Style.Font.Name = "Calibri";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox4.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox4.Value = "=Fields.CompanyName";
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D), Telerik.Reporting.Drawing.Unit.Inch(0.60208225250244141D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.10000016540288925D));
            this.textBox5.Style.Font.Bold = true;
            this.textBox5.Style.Font.Name = "Calibri";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox5.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox5.Value = "Company Name :";
            // 
            // textBox6
            // 
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19999964535236359D), Telerik.Reporting.Drawing.Unit.Inch(0.70208233594894409D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.10000016540288925D));
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.Font.Name = "Calibri";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox6.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox6.Value = "Main Contact :";
            // 
            // textBox7
            // 
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(0.70208233594894409D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.Font.Name = "Calibri";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox7.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox7.Value = "=Fields.MainContactPersonName";
            // 
            // textBox8
            // 
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D), Telerik.Reporting.Drawing.Unit.Inch(0.80208206176757812D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.10000016540288925D));
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Name = "Calibri";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox8.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox8.Value = "Address1 :";
            // 
            // textBox9
            // 
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(0.80208206176757812D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox9.Style.Font.Bold = true;
            this.textBox9.Style.Font.Name = "Calibri";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox9.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox9.Value = "=Fields.Address1";
            // 
            // textBox10
            // 
            this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19999964535236359D), Telerik.Reporting.Drawing.Unit.Inch(0.90208244323730469D));
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.10000016540288925D));
            this.textBox10.Style.Font.Bold = true;
            this.textBox10.Style.Font.Name = "Calibri";
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox10.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox10.Value = "Address2 :";
            // 
            // textBox11
            // 
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(0.90208244323730469D));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Font.Name = "Calibri";
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox11.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox11.Value = "=Fields.Address2";
            // 
            // textBox12
            // 
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D), Telerik.Reporting.Drawing.Unit.Inch(1.0020828247070313D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.10000016540288925D));
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.Font.Name = "Calibri";
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox12.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox12.Value = "City :";
            // 
            // textBox13
            // 
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(1.0020828247070313D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.Font.Name = "Calibri";
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox13.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox13.Value = "=Fields.City";
            // 
            // textBox14
            // 
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D), Telerik.Reporting.Drawing.Unit.Inch(1.5020828247070313D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.10000016540288925D));
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Font.Name = "Calibri";
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox14.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox14.Value = "Main Email :";
            // 
            // textBox15
            // 
            this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D), Telerik.Reporting.Drawing.Unit.Inch(1.4020824432373047D));
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.10000016540288925D));
            this.textBox15.Style.Font.Bold = true;
            this.textBox15.Style.Font.Name = "Calibri";
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox15.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox15.Value = "Main Fax :";
            // 
            // textBox16
            // 
            this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D), Telerik.Reporting.Drawing.Unit.Inch(1.3020826578140259D));
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.10000016540288925D));
            this.textBox16.Style.Font.Bold = true;
            this.textBox16.Style.Font.Name = "Calibri";
            this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox16.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox16.Value = "Main Phone :";
            // 
            // textBox17
            // 
            this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D), Telerik.Reporting.Drawing.Unit.Inch(1.2020822763442993D));
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.10000016540288925D));
            this.textBox17.Style.Font.Bold = true;
            this.textBox17.Style.Font.Name = "Calibri";
            this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox17.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox17.Value = "ZipCode :";
            // 
            // textBox18
            // 
            this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D), Telerik.Reporting.Drawing.Unit.Inch(1.10208261013031D));
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.10000016540288925D));
            this.textBox18.Style.Font.Bold = true;
            this.textBox18.Style.Font.Name = "Calibri";
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox18.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox18.Value = "State :";
            // 
            // textBox19
            // 
            this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(1.5020828247070313D));
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Style.Font.Name = "Calibri";
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox19.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox19.Value = "=Fields.MainEmailId";
            // 
            // textBox20
            // 
            this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(1.4020824432373047D));
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox20.Style.Font.Bold = true;
            this.textBox20.Style.Font.Name = "Calibri";
            this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox20.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox20.Value = "=Fields.Fax";
            // 
            // textBox21
            // 
            this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(1.3020826578140259D));
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox21.Style.Font.Bold = true;
            this.textBox21.Style.Font.Name = "Calibri";
            this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox21.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox21.Value = "=Fields.PhoneNo1";
            // 
            // textBox22
            // 
            this.textBox22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(1.2020822763442993D));
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox22.Style.Font.Bold = true;
            this.textBox22.Style.Font.Name = "Calibri";
            this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox22.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox22.Value = "=Fields.ZipCode";
            // 
            // textBox23
            // 
            this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(1.10208261013031D));
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox23.Style.Font.Bold = true;
            this.textBox23.Style.Font.Name = "Calibri";
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox23.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox23.Value = "=Fields.StateCode";
            // 
            // textBox44
            // 
            this.textBox44.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.6000008583068848D), Telerik.Reporting.Drawing.Unit.Inch(0.0020821888465434313D));
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.90000057220458984D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox44.Style.Font.Bold = true;
            this.textBox44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox44.Value = "Account # :";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.099999904632568359D), Telerik.Reporting.Drawing.Unit.Inch(0.0020821888465434313D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Name = "Arial";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox2.Value = "Monthly Statements";
            // 
            // textBox46
            // 
            this.textBox46.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6D), Telerik.Reporting.Drawing.Unit.Inch(0.30208221077919006D));
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7999998331069946D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox46.Style.Font.Bold = true;
            this.textBox46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox46.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox46.Value = "= GetDate()";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6D), Telerik.Reporting.Drawing.Unit.Inch(0.60208225250244141D));
            this.pictureBox1.MimeType = "image/png";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7999998331069946D), Telerik.Reporting.Drawing.Unit.Inch(0.40625D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // textBox47
            // 
            this.textBox47.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.399998664855957D), Telerik.Reporting.Drawing.Unit.Inch(1.0999999046325684D));
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3999999761581421D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox47.Style.Font.Bold = true;
            this.textBox47.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox47.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox47.Value = "Corporate Headquarters";
            // 
            // textBox48
            // 
            this.textBox48.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.399998664855957D), Telerik.Reporting.Drawing.Unit.Inch(1.2000000476837158D));
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3999999761581421D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox48.Style.Font.Bold = true;
            this.textBox48.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox48.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox48.Value = "8730 Wilshire Blvd. #412";
            // 
            // textBox50
            // 
            this.textBox50.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.399998664855957D), Telerik.Reporting.Drawing.Unit.Inch(1.4000000953674316D));
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3999999761581421D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox50.Style.Font.Bold = true;
            this.textBox50.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox50.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox50.Value = "www.intelifi.com";
            // 
            // textBox49
            // 
            this.textBox49.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.399998664855957D), Telerik.Reporting.Drawing.Unit.Inch(1.3000000715255737D));
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3999999761581421D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox49.Style.Font.Bold = true;
            this.textBox49.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox49.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox49.Value = "Beverly Hills, CA 90211";
            // 
            // textBox51
            // 
            this.textBox51.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.399998664855957D), Telerik.Reporting.Drawing.Unit.Inch(1.5D));
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3999999761581421D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox51.Style.Font.Bold = true;
            this.textBox51.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox51.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox51.Value = "888.409.1819";
            // 
            // textBox29
            // 
            this.textBox29.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(1.10208261013031D));
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox29.Style.Font.Bold = true;
            this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox29.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox29.Value = "=Fields.StateCode";
            // 
            // textBox30
            // 
            this.textBox30.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(1.2020822763442993D));
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox30.Style.Font.Bold = true;
            this.textBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox30.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox30.Value = "=Fields.ZipCode";
            // 
            // textBox31
            // 
            this.textBox31.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(1.3020826578140259D));
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox31.Style.Font.Bold = true;
            this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox31.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox31.Value = "=Fields.PhoneNo2";
            // 
            // textBox32
            // 
            this.textBox32.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(1.4020824432373047D));
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox32.Style.Font.Bold = true;
            this.textBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox32.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox32.Value = "=Fields.Fax2";
            // 
            // textBox33
            // 
            this.textBox33.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(1.5020828247070313D));
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox33.Style.Font.Bold = true;
            this.textBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox33.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox33.Value = "=Fields.BillingEmailId";
            // 
            // textBox35
            // 
            this.textBox35.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1000001430511475D), Telerik.Reporting.Drawing.Unit.Inch(1.10208261013031D));
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.095833562314510345D));
            this.textBox35.Style.Font.Bold = true;
            this.textBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox35.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox35.Value = "State :";
            // 
            // textBox37
            // 
            this.textBox37.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1000001430511475D), Telerik.Reporting.Drawing.Unit.Inch(1.2020822763442993D));
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.095833562314510345D));
            this.textBox37.Style.Font.Bold = true;
            this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox37.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox37.Value = "ZipCode :";
            // 
            // textBox39
            // 
            this.textBox39.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1000001430511475D), Telerik.Reporting.Drawing.Unit.Inch(1.3020826578140259D));
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.095833562314510345D));
            this.textBox39.Style.Font.Bold = true;
            this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox39.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox39.Value = "Main Phone :";
            // 
            // textBox41
            // 
            this.textBox41.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1000001430511475D), Telerik.Reporting.Drawing.Unit.Inch(1.4020824432373047D));
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.095833562314510345D));
            this.textBox41.Style.Font.Bold = true;
            this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox41.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox41.Value = "Main Fax :";
            // 
            // textBox42
            // 
            this.textBox42.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1000001430511475D), Telerik.Reporting.Drawing.Unit.Inch(1.5020828247070313D));
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.095833562314510345D));
            this.textBox42.Style.Font.Bold = true;
            this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox42.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox42.Value = "Main Email :";
            // 
            // textBox52
            // 
            this.textBox52.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(1.0020828247070313D));
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox52.Style.Font.Bold = true;
            this.textBox52.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox52.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox52.Value = "=Fields.City";
            // 
            // textBox53
            // 
            this.textBox53.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1000001430511475D), Telerik.Reporting.Drawing.Unit.Inch(1.0020828247070313D));
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.095833562314510345D));
            this.textBox53.Style.Font.Bold = true;
            this.textBox53.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox53.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox53.Value = "City :";
            // 
            // textBox54
            // 
            this.textBox54.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.90208244323730469D));
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox54.Style.Font.Bold = true;
            this.textBox54.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox54.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox54.Value = "=Fields.Address2";
            // 
            // textBox55
            // 
            this.textBox55.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1000001430511475D), Telerik.Reporting.Drawing.Unit.Inch(0.90208244323730469D));
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.095833562314510345D));
            this.textBox55.Style.Font.Bold = true;
            this.textBox55.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox55.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox55.Value = "Address2 :";
            // 
            // textBox56
            // 
            this.textBox56.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.80208206176757812D));
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox56.Style.Font.Bold = true;
            this.textBox56.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox56.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox56.Value = "=Fields.Address1";
            // 
            // textBox57
            // 
            this.textBox57.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1000001430511475D), Telerik.Reporting.Drawing.Unit.Inch(0.80208206176757812D));
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.095833562314510345D));
            this.textBox57.Style.Font.Bold = true;
            this.textBox57.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox57.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox57.Value = "Address1 :";
            // 
            // textBox58
            // 
            this.textBox58.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.70208233594894409D));
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox58.Style.Font.Bold = true;
            this.textBox58.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox58.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox58.Value = "=Fields.MainContactPersonName";
            // 
            // textBox59
            // 
            this.textBox59.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1000001430511475D), Telerik.Reporting.Drawing.Unit.Inch(0.70208233594894409D));
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.095833562314510345D));
            this.textBox59.Style.Font.Bold = true;
            this.textBox59.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox59.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox59.Value = "Main Contact :";
            // 
            // textBox60
            // 
            this.textBox60.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1000001430511475D), Telerik.Reporting.Drawing.Unit.Inch(0.60208225250244141D));
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.095833562314510345D));
            this.textBox60.Style.Font.Bold = true;
            this.textBox60.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox60.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox60.Value = "Company Name :";
            // 
            // textBox61
            // 
            this.textBox61.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.60208225250244141D));
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox61.Style.Font.Bold = true;
            this.textBox61.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox61.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox61.Value = "=Fields.CompanyName";
            // 
            // textBox24
            // 
            this.textBox24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.5D), Telerik.Reporting.Drawing.Unit.Inch(3.9339065551757812E-05D));
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2999982833862305D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox24.Style.Font.Bold = true;
            this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox24.Value = "= GetAccountNo()";
            // 
            // textBox62
            // 
            this.textBox62.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0894622802734375D), Telerik.Reporting.Drawing.Unit.Inch(11.5D));
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.7021603584289551D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox62.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox62.Style.Font.Bold = true;
            this.textBox62.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox62.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox62.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox62.Value = "Itemized Report";
            // 
            // textBox63
            // 
            this.textBox63.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0894622802734375D), Telerik.Reporting.Drawing.Unit.Inch(11.82066822052002D));
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.691622257232666D), Telerik.Reporting.Drawing.Unit.Pixel(23D));
            this.textBox63.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox63.Style.Font.Bold = true;
            this.textBox63.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox63.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox63.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox63.Value = "= GetTotalItemizedReports()";
            // 
            // srItemized
            // 
            this.srItemized.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.091623835265636444D), Telerik.Reporting.Drawing.Unit.Inch(12.299985885620117D));
            this.srItemized.Name = "srItemized";
            instanceReportSource3.ReportDocument = this.srItemizedReport1; 
            this.srItemized.ReportSource = instanceReportSource3;
            this.srItemized.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.691622257232666D), Telerik.Reporting.Drawing.Unit.Inch(0.70000070333480835D));
            this.srItemized.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srItemized.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srItemized.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srItemized.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srItemized.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srItemized.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // textBox65
            // 
            this.textBox65.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.099999904632568359D), Telerik.Reporting.Drawing.Unit.Inch(1.8000000715255737D));
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999983787536621D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox65.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox65.Style.Font.Bold = true;
            this.textBox65.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox65.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox65.Style.Visible = true;
            this.textBox65.Value = "Billing Statement";
            // 
            // panel6
            // 
            this.panel6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox66,
            this.textBox67,
            this.textBox68,
            this.textBox69,
            this.textBox70,
            this.textBox71,
            this.textBox72,
            this.lblReport});
            this.panel6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.099999904632568359D), Telerik.Reporting.Drawing.Unit.Inch(2.1000001430511475D));
            this.panel6.Name = "panel6";
            this.panel6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.695793628692627D), Telerik.Reporting.Drawing.Unit.Inch(0.19999988377094269D));
            this.panel6.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(241)))), ((int)(((byte)(242)))));
            this.panel6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.panel6.Style.Visible = false;
            // 
            // textBox66
            // 
            this.textBox66.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.0000004768371582D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.69579333066940308D), Telerik.Reporting.Drawing.Unit.Inch(0.19988170266151428D));
            this.textBox66.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(90)))), ((int)(((byte)(169)))));
            this.textBox66.Style.Font.Bold = true;
            this.textBox66.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox66.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox66.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox66.TextWrap = true;
            this.textBox66.Value = "Action";
            // 
            // textBox67
            // 
            this.textBox67.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.8000392913818359D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0998822450637817D), Telerik.Reporting.Drawing.Unit.Inch(0.19988170266151428D));
            this.textBox67.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(90)))), ((int)(((byte)(169)))));
            this.textBox67.Style.Font.Bold = true;
            this.textBox67.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox67.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox67.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox67.TextWrap = true;
            this.textBox67.Value = "Remaining Balance";
            // 
            // textBox68
            // 
            this.textBox68.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.9000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79996061325073242D), Telerik.Reporting.Drawing.Unit.Inch(0.19988170266151428D));
            this.textBox68.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(90)))), ((int)(((byte)(169)))));
            this.textBox68.Style.Font.Bold = true;
            this.textBox68.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox68.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox68.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox68.TextWrap = true;
            this.textBox68.Value = "Amount Paid";
            // 
            // textBox69
            // 
            this.textBox69.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.1000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.72079378366470337D), Telerik.Reporting.Drawing.Unit.Inch(0.19988170266151428D));
            this.textBox69.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(90)))), ((int)(((byte)(169)))));
            this.textBox69.Style.Font.Bold = true;
            this.textBox69.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox69.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox69.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox69.TextWrap = true;
            this.textBox69.Value = "Adjustment";
            // 
            // textBox70
            // 
            this.textBox70.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89992082118988037D), Telerik.Reporting.Drawing.Unit.Inch(0.19988170266151428D));
            this.textBox70.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(90)))), ((int)(((byte)(169)))));
            this.textBox70.Style.Font.Bold = true;
            this.textBox70.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox70.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox70.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox70.TextWrap = true;
            this.textBox70.Value = "Invoice Amount";
            // 
            // textBox71
            // 
            this.textBox71.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.8000000715255737D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79996061325073242D), Telerik.Reporting.Drawing.Unit.Inch(0.19988170266151428D));
            this.textBox71.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(90)))), ((int)(((byte)(169)))));
            this.textBox71.Style.Font.Bold = true;
            this.textBox71.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox71.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox71.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox71.TextWrap = true;
            this.textBox71.Value = "Due Date";
            // 
            // textBox72
            // 
            this.textBox72.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.80000007152557373D), Telerik.Reporting.Drawing.Unit.Inch(0.19988170266151428D));
            this.textBox72.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(90)))), ((int)(((byte)(169)))));
            this.textBox72.Style.Font.Bold = true;
            this.textBox72.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox72.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox72.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox72.TextWrap = true;
            this.textBox72.Value = "Invoice Date";
            // 
            // lblReport
            // 
            this.lblReport.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.lblReport.Name = "lblReport";
            this.lblReport.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79996061325073242D), Telerik.Reporting.Drawing.Unit.Inch(0.19988170266151428D));
            this.lblReport.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(90)))), ((int)(((byte)(169)))));
            this.lblReport.Style.Font.Bold = true;
            this.lblReport.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.lblReport.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.lblReport.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblReport.TextWrap = true;
            this.lblReport.Value = "Invoice #";
            // 
            // lblIsDue1
            // 
            this.lblIsDue1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.091623865067958832D), Telerik.Reporting.Drawing.Unit.Inch(7.5999999046325684D));
            this.lblIsDue1.Name = "lblIsDue1";
            this.lblIsDue1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999983787536621D), Telerik.Reporting.Drawing.Unit.Inch(0.19999994337558746D));
            this.lblIsDue1.Style.BackgroundColor = System.Drawing.Color.Empty;
            this.lblIsDue1.Style.Color = System.Drawing.Color.Red;
            this.lblIsDue1.Style.Font.Bold = true;
            this.lblIsDue1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.lblIsDue1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.lblIsDue1.Value = "Your Account is Past Due";
            // 
            // lblIsDue2
            // 
            this.lblIsDue2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.099999904632568359D), Telerik.Reporting.Drawing.Unit.Inch(7.7999987602233887D));
            this.lblIsDue2.Name = "lblIsDue2";
            this.lblIsDue2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999983787536621D), Telerik.Reporting.Drawing.Unit.Inch(0.19999994337558746D));
            this.lblIsDue2.Style.BackgroundColor = System.Drawing.Color.Empty;
            this.lblIsDue2.Style.Color = System.Drawing.Color.Red;
            this.lblIsDue2.Style.Font.Bold = true;
            this.lblIsDue2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.lblIsDue2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.lblIsDue2.Value = "Service interruption may occur at anytime. Please pay your balance immediately to" +
    " avoid any late fees or collections.";
            // 
            // textBox86
            // 
            this.textBox86.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.099999904632568359D), Telerik.Reporting.Drawing.Unit.Inch(7.9999980926513672D));
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999983787536621D), Telerik.Reporting.Drawing.Unit.Inch(0.19999994337558746D));
            this.textBox86.Style.BackgroundColor = System.Drawing.Color.Empty;
            this.textBox86.Style.Color = System.Drawing.Color.Black;
            this.textBox86.Style.Font.Bold = true;
            this.textBox86.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox86.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox86.Value = "Thank you for choosing Intelifi. We sincerely appreciate your business.";
            // 
            // panel7
            // 
            this.panel7.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox87,
            this.txtBillingPhone,
            this.textBox89,
            this.textBox90,
            this.txtBillingId,
            this.textBox92});
            this.panel7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10000220686197281D), Telerik.Reporting.Drawing.Unit.Inch(8.2999992370605469D));
            this.panel7.Name = "panel7";
            this.panel7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.7020788192749023D), Telerik.Reporting.Drawing.Unit.Pixel(67.192451477050781D));
            this.panel7.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // textBox87
            // 
            this.textBox87.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3999977111816406D), Telerik.Reporting.Drawing.Unit.Inch(0.19992142915725708D));
            this.textBox87.Style.BackgroundColor = System.Drawing.Color.Empty;
            this.textBox87.Style.Color = System.Drawing.Color.Black;
            this.textBox87.Style.Font.Bold = true;
            this.textBox87.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox87.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox87.Value = "Billing Inquiries";
            // 
            // txtBillingPhone
            // 
            this.txtBillingPhone.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.59999775886535645D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.txtBillingPhone.Name = "txtBillingPhone";
            this.txtBillingPhone.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.txtBillingPhone.Style.Font.Bold = true;
            this.txtBillingPhone.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtBillingPhone.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtBillingPhone.Value = "";
            // 
            // textBox89
            // 
            this.textBox89.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.49999776482582092D), Telerik.Reporting.Drawing.Unit.Inch(0.10000016540288925D));
            this.textBox89.Style.Font.Bold = true;
            this.textBox89.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox89.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox89.Value = "Call :";
            // 
            // textBox90
            // 
            this.textBox90.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.30416646599769592D));
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.49999776482582092D), Telerik.Reporting.Drawing.Unit.Inch(0.095833562314510345D));
            this.textBox90.Style.Font.Bold = true;
            this.textBox90.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox90.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox90.Value = "Email:";
            // 
            // txtBillingId
            // 
            this.txtBillingId.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.599879264831543D), Telerik.Reporting.Drawing.Unit.Inch(0.299999862909317D));
            this.txtBillingId.Name = "txtBillingId";
            this.txtBillingId.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.txtBillingId.Style.Font.Bold = true;
            this.txtBillingId.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtBillingId.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtBillingId.Value = "";
            // 
            // textBox92
            // 
            this.textBox92.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.5D));
            this.textBox92.Name = "textBox92";
            this.textBox92.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.3999977111816406D), Telerik.Reporting.Drawing.Unit.Inch(0.19992142915725708D));
            this.textBox92.Style.BackgroundColor = System.Drawing.Color.Empty;
            this.textBox92.Style.Color = System.Drawing.Color.Black;
            this.textBox92.Style.Font.Bold = true;
            this.textBox92.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox92.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox92.Value = "Please make checks payable to Intelifi Inc.";
            // 
            // srPageBreak1
            // 
            this.srPageBreak1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10007773339748383D), Telerik.Reporting.Drawing.Unit.Inch(8.9999980926513672D));
            this.srPageBreak1.Name = "srPageBreak1";
            instanceReportSource4.ReportDocument = this.srPageBreack21;
            this.srPageBreak1.ReportSource = instanceReportSource4;
            this.srPageBreak1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.700040340423584D), Telerik.Reporting.Drawing.Unit.Inch(0.0520833395421505D));
            // 
            // panel8
            // 
            this.panel8.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.srBillingStatement});
            this.panel8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10000000894069672D), Telerik.Reporting.Drawing.Unit.Inch(2.3000788688659668D));
            this.panel8.Name = "panel8";
            this.panel8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.7001180648803711D), Telerik.Reporting.Drawing.Unit.Inch(1.1999208927154541D));
            this.panel8.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.panel8.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.panel8.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.panel8.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.panel8.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.panel8.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.panel8.Style.Visible = false;
            // 
            // srBillingStatement
            // 
            this.srBillingStatement.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.00012265311670489609D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.srBillingStatement.Name = "srBillingStatement";
            instanceReportSource5.ReportDocument = this.srBillingStatement1;
            this.srBillingStatement.ReportSource = instanceReportSource5;
            this.srBillingStatement.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999955177307129D), Telerik.Reporting.Drawing.Unit.Inch(0.89999979734420776D));
            this.srBillingStatement.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srBillingStatement.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srBillingStatement.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srBillingStatement.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srBillingStatement.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srBillingStatement.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // panel12
            // 
            this.panel12.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pnlAdjustmentsHeadings,
            this.srAdjustment,
            this.srProductsPerLocation});
            this.panel12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.7000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(9.5D));
            this.panel12.Name = "panel12";
            this.panel12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.4999613761901855D), Telerik.Reporting.Drawing.Unit.Pixel(144D));
            this.panel12.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.panel12.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.panel12.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.panel12.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.panel12.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.panel12.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.panel12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            // 
            // srProductsPerLocation
            // 
            this.srProductsPerLocation.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.srProductsPerLocation.Name = "srProductsPerLocation";
            instanceReportSource7.ReportDocument = this.srProducts2;
            this.srProductsPerLocation.ReportSource = instanceReportSource7;
            this.srProductsPerLocation.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(0.49999982118606567D));
            this.srProductsPerLocation.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srProductsPerLocation.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srProductsPerLocation.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srProductsPerLocation.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srProductsPerLocation.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srProductsPerLocation.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.srProductsPerLocation.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // pnlAdjustmentsHeadings
            // 
            this.pnlAdjustmentsHeadings.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox103,
            this.textBox104,
            this.textBox105});
            this.pnlAdjustmentsHeadings.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.60000008344650269D));
            this.pnlAdjustmentsHeadings.Name = "pnlAdjustmentsHeadings";
            this.pnlAdjustmentsHeadings.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Pixel(23D));
            this.pnlAdjustmentsHeadings.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(218)))), ((int)(((byte)(237)))));
            this.pnlAdjustmentsHeadings.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            // 
            // textBox103
            // 
            this.textBox103.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox103.Name = "textBox103";
            this.textBox103.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.23946496844291687D));
            this.textBox103.Style.Font.Bold = true;
            this.textBox103.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox103.Value = "Product";
            // 
            // textBox104
            // 
            this.textBox104.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.3999999761581421D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.textBox104.Name = "textBox104";
            this.textBox104.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.699881374835968D), Telerik.Reporting.Drawing.Unit.Inch(0.23946496844291687D));
            this.textBox104.Style.Font.Bold = true;
            this.textBox104.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox104.Value = "Quantity";
            // 
            // textBox105
            // 
            this.textBox105.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.1000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.textBox105.Name = "textBox105";
            this.textBox105.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.39996066689491272D), Telerik.Reporting.Drawing.Unit.Inch(0.23946496844291687D));
            this.textBox105.Style.Font.Bold = true;
            this.textBox105.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox105.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox105.Value = "Price";
            // 
            // srAdjustment
            // 
            this.srAdjustment.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0062893759459257126D), Telerik.Reporting.Drawing.Unit.Inch(0.83966147899627686D));
            this.srAdjustment.Name = "srAdjustment";
            instanceReportSource6.ReportDocument = this.srAdjustments2;
            this.srAdjustment.ReportSource = instanceReportSource6;
            this.srAdjustment.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.4936323165893555D), Telerik.Reporting.Drawing.Unit.Inch(0.66033852100372314D));
            // 
            // srPageBreak2
            // 
            this.srPageBreak2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10208383947610855D), Telerik.Reporting.Drawing.Unit.Inch(11.260337829589844D));
            this.srPageBreak2.Name = "srPageBreak2";
            instanceReportSource8.ReportDocument = this.srPageBreack2;
            this.srPageBreak2.ReportSource = instanceReportSource8;
            this.srPageBreak2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6895389556884766D), Telerik.Reporting.Drawing.Unit.Inch(0.13966284692287445D));
            // 
            // panel4
            // 
            this.panel4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox76,
            this.textBox77,
            this.textBox79,
            this.textBox80,
            this.textBox81,
            this.textBox82,
            this.textBox83,
            this.textBox84,
            this.textBox64,
            this.textBox78});
            this.panel4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0894622802734375D), Telerik.Reporting.Drawing.Unit.Inch(12.060328483581543D));
            this.panel4.Name = "panel4";
            this.panel4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6916227340698242D), Telerik.Reporting.Drawing.Unit.Pixel(22.999675750732422D));
            this.panel4.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(187)))), ((int)(((byte)(187)))));
            this.panel4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            // 
            // textBox76
            // 
            this.textBox76.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.0679165285409908E-07D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.899921178817749D), Telerik.Reporting.Drawing.Unit.Inch(0.23954415321350098D));
            this.textBox76.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox76.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox76.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox76.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox76.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox76.Style.Font.Bold = true;
            this.textBox76.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox76.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox76.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox76.Value = "Order";
            // 
            // textBox77
            // 
            this.textBox77.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.90000003576278687D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0998818874359131D), Telerik.Reporting.Drawing.Unit.Inch(0.23954415321350098D));
            this.textBox77.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox77.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox77.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox77.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox77.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox77.Style.Font.Bold = true;
            this.textBox77.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox77.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox77.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox77.Value = "ReportID";
            // 
            // textBox79
            // 
            this.textBox79.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1790494918823242D), Telerik.Reporting.Drawing.Unit.Inch(0.23954415321350098D));
            this.textBox79.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox79.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox79.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox79.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox79.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox79.Style.Font.Bold = true;
            this.textBox79.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox79.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox79.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox79.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox79.Value = "User";
            // 
            // textBox80
            // 
            this.textBox80.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.479128360748291D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79991978406906128D), Telerik.Reporting.Drawing.Unit.Inch(0.23954415321350098D));
            this.textBox80.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox80.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox80.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox80.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox80.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox80.Style.Font.Bold = true;
            this.textBox80.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox80.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox80.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox80.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox80.Value = "Location";
            // 
            // textBox81
            // 
            this.textBox81.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.162539005279541D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.50283455848693848D), Telerik.Reporting.Drawing.Unit.Inch(0.23954415321350098D));
            this.textBox81.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox81.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox81.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox81.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox81.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox81.Style.Font.Bold = true;
            this.textBox81.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox81.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox81.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox81.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox81.Value = "Total";
            // 
            // textBox82
            // 
            this.textBox82.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.27912712097168D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.62079417705535889D), Telerik.Reporting.Drawing.Unit.Inch(0.23954415321350098D));
            this.textBox82.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox82.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox82.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox82.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox82.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox82.Style.Font.Bold = true;
            this.textBox82.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox82.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox82.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox82.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox82.Value = "Date";
            // 
            // textBox83
            // 
            this.textBox83.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.3104491233825684D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.41234210133552551D), Telerik.Reporting.Drawing.Unit.Inch(0.23954415321350098D));
            this.textBox83.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox83.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox83.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox83.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox83.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox83.Style.Font.Bold = true;
            this.textBox83.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox83.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox83.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox83.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox83.Value = "Price";
            // 
            // textBox84
            // 
            this.textBox84.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.7228693962097168D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.43959209322929382D), Telerik.Reporting.Drawing.Unit.Inch(0.23954415321350098D));
            this.textBox84.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox84.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox84.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox84.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox84.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox84.Style.Font.Bold = true;
            this.textBox84.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox84.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox84.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox84.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox84.Value = "Fee";
            // 
            // textBox64
            // 
            this.textBox64.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.9000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.39996126294136047D), Telerik.Reporting.Drawing.Unit.Inch(0.23954415321350098D));
            this.textBox64.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox64.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox64.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox64.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox64.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox64.Style.Font.Bold = true;
            this.textBox64.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox64.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox64.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox64.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox64.Value = "Status";
            // 
            // textBox78
            // 
            this.textBox78.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.0000004768371582D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2999205589294434D), Telerik.Reporting.Drawing.Unit.Inch(0.23954415321350098D));
            this.textBox78.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox78.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox78.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox78.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox78.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox78.Style.Font.Bold = true;
            this.textBox78.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox78.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox78.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox78.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox78.Value = "Data Sources";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.46035438776016235D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.panel5});
            this.pageFooterSection1.Name = "pageFooterSection1";
            this.pageFooterSection1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            // 
            // panel5
            // 
            this.panel5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox74,
            this.textBox75});
            this.panel5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10000228881835938D), Telerik.Reporting.Drawing.Unit.Inch(0.045833270996809006D));
            this.panel5.Name = "panel5";
            this.panel5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6916208267211914D), Telerik.Reporting.Drawing.Unit.Inch(0.39999961853027344D));
            this.panel5.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Ridge;
            this.panel5.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Ridge;
            // 
            // textBox74
            // 
            this.textBox74.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.099997840821743011D), Telerik.Reporting.Drawing.Unit.Inch(0.099999748170375824D));
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.49999991059303284D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox74.Style.Font.Bold = true;
            this.textBox74.Style.Font.Name = "Calibri";
            this.textBox74.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.textBox74.Value = "= PageNumber";
            // 
            // textBox75
            // 
            this.textBox75.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.4999980926513672D), Telerik.Reporting.Drawing.Unit.Inch(0.099999748170375824D));
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1000397205352783D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox75.Style.Font.Bold = true;
            this.textBox75.Style.Font.Name = "Calibri";
            this.textBox75.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.textBox75.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox75.Value = "= Now().ToString(\"MMMM dd, yyyy hh:mm tt\")";
            // 
            // panel11
            // 
            this.panel11.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox99,
            this.textBox100,
            this.textBox101});
            this.panel11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0020816591568291187D), Telerik.Reporting.Drawing.Unit.Inch(3.4603805541992188D));
            this.panel11.Name = "panel11";
            this.panel11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5000002384185791D), Telerik.Reporting.Drawing.Unit.Pixel(23D));
            this.panel11.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(218)))), ((int)(((byte)(237)))));
            this.panel11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            // 
            // textBox99
            // 
            this.textBox99.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox99.Name = "textBox99";
            this.textBox99.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.23946496844291687D));
            this.textBox99.Style.Font.Bold = true;
            this.textBox99.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox99.Value = "Report";
            // 
            // textBox100
            // 
            this.textBox100.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox100.Name = "textBox100";
            this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.69988173246383667D), Telerik.Reporting.Drawing.Unit.Inch(0.23946496844291687D));
            this.textBox100.Style.Font.Bold = true;
            this.textBox100.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox100.Value = "Quantity";
            // 
            // textBox101
            // 
            this.textBox101.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.1000001430511475D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox101.Name = "textBox101";
            this.textBox101.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.39999997615814209D), Telerik.Reporting.Drawing.Unit.Inch(0.23946496844291687D));
            this.textBox101.Style.Font.Bold = true;
            this.textBox101.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox101.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox101.Value = "Price";
            // 
            // panel9
            // 
            this.panel9.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox93,
            this.textBox94,
            this.textBox95});
            this.panel9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.1999926567077637D), Telerik.Reporting.Drawing.Unit.Inch(3.4603805541992188D));
            this.panel9.Name = "panel9";
            this.panel9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.4999606609344482D), Telerik.Reporting.Drawing.Unit.Pixel(23D));
            this.panel9.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(218)))), ((int)(((byte)(237)))));
            this.panel9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            // 
            // textBox93
            // 
            this.textBox93.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(7.8519187809433788E-05D));
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0999208688735962D), Telerik.Reporting.Drawing.Unit.Inch(0.23946496844291687D));
            this.textBox93.Style.Font.Bold = true;
            this.textBox93.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox93.Value = "Locations Report";
            // 
            // textBox94
            // 
            this.textBox94.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.3623822927474976D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox94.Name = "textBox94";
            this.textBox94.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.59995996952056885D), Telerik.Reporting.Drawing.Unit.Inch(0.23946496844291687D));
            this.textBox94.Style.Font.Bold = true;
            this.textBox94.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox94.Value = "Quantity";
            // 
            // textBox95
            // 
            this.textBox95.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.0999205112457275D), Telerik.Reporting.Drawing.Unit.Inch(0.00011793772137025371D));
            this.textBox95.Name = "textBox95";
            this.textBox95.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.4000401496887207D), Telerik.Reporting.Drawing.Unit.Inch(0.23946496844291687D));
            this.textBox95.Style.Font.Bold = true;
            this.textBox95.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox95.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox95.Value = "Price";
            // 
            // srBillingSummaryApi
            // 
            this.srBillingSummaryApi.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.7000002861022949D));
            this.srBillingSummaryApi.Name = "srBillingSummaryApi";
            instanceReportSource9.ReportDocument = this.srBillingSummary2;
            this.srBillingSummaryApi.ReportSource = instanceReportSource9;
            this.srBillingSummaryApi.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(0.76034045219421387D));
            // 
            // srCompanyLocationsApi
            // 
            this.srCompanyLocationsApi.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.2001166343688965D), Telerik.Reporting.Drawing.Unit.Inch(3.7000007629394531D));
            this.srCompanyLocationsApi.Name = "srCompanyLocationsApi";
            instanceReportSource10.ReportDocument = this.srCompanyLocations2;
            this.srCompanyLocationsApi.ReportSource = instanceReportSource10;
            this.srCompanyLocationsApi.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5019626617431641D), Telerik.Reporting.Drawing.Unit.Inch(0.76033860445022583D));
            // 
            // BillingStatement
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "BillingStatement";
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(8D);
            this.NeedDataSource += new System.EventHandler(this.BillingStatement_NeedDataSource);
            ((System.ComponentModel.ISupportInitialize)(this.srBillingSummary1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.srCompanyLocations1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.srPageBreack21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.srBillingStatement1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.srProducts2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.srAdjustments2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.srPageBreack2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.srBillingSummary2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.srCompanyLocations2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.srItemizedReport1)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.SubReport srBillingSummary;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.SubReport srCompanyLocations;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox24;

        #region Custom Functions
        public static string GetDate()
        {
            //string date = String.Format("{0:y}", System.DateTime.Now);
            string month = !string.IsNullOrEmpty(Month) ? Month : string.Empty;
            if (!string.IsNullOrEmpty(Month))
            {
                int months = DateTime.Now.AddMonths(-int.Parse(month)).Month;
                DateTime dt = DateTime.Now.AddMonths(-months);
                month = dt.ToString("MMMM");
            }
            string year = !string.IsNullOrEmpty(Year) ? Year : string.Empty;
            string date = month + ", " + year;
            return date;
        }

        public static string GetAccountNo()
        {

            return CompanyCode;
        }

        public static string GetTotalItemizedReports()
        {
            return TotalItemizedReports;
        }
        #endregion

        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.SubReport srItemized;
        private SRItemizedReport srItemizedReport1;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.Panel panel5;
        private Telerik.Reporting.TextBox textBox74;
        private Telerik.Reporting.TextBox textBox75;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.SubReport srBillingStatement;
        private SRBillingStatement srBillingStatement1;
        private Telerik.Reporting.Panel panel6;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox68;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.TextBox lblReport;
        private Telerik.Reporting.TextBox lblIsDue1;
        private Telerik.Reporting.TextBox lblIsDue2;
        private Telerik.Reporting.TextBox textBox86;
        private Telerik.Reporting.Panel panel7;
        private Telerik.Reporting.TextBox textBox87;
        private Telerik.Reporting.TextBox txtBillingPhone;
        private Telerik.Reporting.TextBox textBox89;
        private Telerik.Reporting.TextBox textBox90;
        private Telerik.Reporting.TextBox txtBillingId;
        private Telerik.Reporting.TextBox textBox92;
        private Telerik.Reporting.SubReport srPageBreak1;
        private SRPageBreack2 srPageBreack21;
        private Telerik.Reporting.Panel panel8;
        private SRBillingSummary srBillingSummary1;
        private SRCompanyLocations srCompanyLocations1;
        private Telerik.Reporting.Panel panel12;
        private Telerik.Reporting.SubReport srProductsPerLocation;
        private Telerik.Reporting.Panel pnlAdjustmentsHeadings;
        private Telerik.Reporting.TextBox textBox103;
        private Telerik.Reporting.TextBox textBox104;
        private Telerik.Reporting.TextBox textBox105;
        private Telerik.Reporting.SubReport srAdjustment;
        private SRProducts srProducts2;
        private SRAdjustments srAdjustments2;
        private Telerik.Reporting.SubReport srBillingSummaryApi;
        private SRBillingSummary srBillingSummary2;
        private Telerik.Reporting.Panel panel11;
        private Telerik.Reporting.TextBox textBox99;
        private Telerik.Reporting.TextBox textBox100;
        private Telerik.Reporting.TextBox textBox101;
        private Telerik.Reporting.SubReport srCompanyLocationsApi;
        private SRCompanyLocations srCompanyLocations2;
        private Telerik.Reporting.Panel panel9;
        private Telerik.Reporting.TextBox textBox93;
        private Telerik.Reporting.TextBox textBox94;
        private Telerik.Reporting.TextBox textBox95;
        private Telerik.Reporting.SubReport srPageBreak2;
        private SRPageBreack srPageBreack2;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.Panel panel4;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.TextBox textBox77;
        private Telerik.Reporting.TextBox textBox79;
        private Telerik.Reporting.TextBox textBox80;
        private Telerik.Reporting.TextBox textBox81;
        private Telerik.Reporting.TextBox textBox82;
        private Telerik.Reporting.TextBox textBox83;
        private Telerik.Reporting.TextBox textBox84;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox78;
    }
}