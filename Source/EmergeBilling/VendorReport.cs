namespace EmergeBilling
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;
    using System.Collections.Generic;
    using Emerge.Services;
    using Emerge.Data;

    /// <summary>
    /// Summary description for VendorReport.
    /// </summary>
    public partial class VendorReport : Telerik.Reporting.Report
    {
        public Tuple<List<clsVendorReport>, List<clsDuplicateTransactions>> ObjData { get; set; }
        public VendorReport()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        private void VendorReport_NeedDataSource(object sender, EventArgs e)
        {
            List<clsVendorReport> listVendorReports = new List<clsVendorReport>();
            List<clsDuplicateTransactions> listDuplicates = new List<clsDuplicateTransactions>();
            if (ObjData != null)
            {
                listVendorReports = ObjData.Item1;
                listDuplicates = ObjData.Item2;
            }

            Telerik.Reporting.Report MainRpt = (sender as Telerik.Reporting.Report);
            Telerik.Reporting.SubReport SRVendorSummary = (Telerik.Reporting.SubReport)(detail.Items[0]);
            Telerik.Reporting.SubReport SRVendorTransactions = (Telerik.Reporting.SubReport)(detail.Items[6]);
            Telerik.Reporting.SubReport SRDuplicateTransactions = (Telerik.Reporting.SubReport)(detail.Items[8]);
            if (listVendorReports.Count > 0)
            {
                srVendorSummary1.DataSource = listVendorReports;
                srVendorTransactions1.DataSource = listVendorReports;
            }
            else
            {
                SRVendorSummary.Visible = false;
                SRVendorTransactions.Visible = false;
            }

            if (listDuplicates.Count > 0)
            {
                srDuplicateTransactions1.DataSource = listDuplicates;
            }
            else
            {
                SRDuplicateTransactions.Visible = false;
            }
        }
    }
}