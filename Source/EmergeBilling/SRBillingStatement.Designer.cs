using System;




namespace EmergeBilling
{
    public partial class SRBillingStatement
    {
        //public static int Count { get; set; }
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SRBillingStatement));
            Telerik.Reporting.NavigateToUrlAction navigateToUrlAction3 = new Telerik.Reporting.NavigateToUrlAction();
            Telerik.Reporting.NavigateToUrlAction navigateToUrlAction4 = new Telerik.Reporting.NavigateToUrlAction();
            Telerik.Reporting.NavigateToUrlAction navigateToUrlAction5 = new Telerik.Reporting.NavigateToUrlAction();
            this.detail = new Telerik.Reporting.DetailSection();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.btnPay = new Telerik.Reporting.PictureBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.lblReport = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.lblAction = new Telerik.Reporting.TextBox();
            this.group1 = new Telerik.Reporting.Group();
            this.groupFooterSection1 = new Telerik.Reporting.GroupFooterSection();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.btnPayAll = new Telerik.Reporting.PictureBox();
            this.groupHeaderSection1 = new Telerik.Reporting.GroupHeaderSection();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(0.22700484097003937D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox7,
            this.textBox1,
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.btnPay,
            this.textBox16});
            this.detail.Name = "detail";
            this.detail.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // textBox7
            // 
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.0729166641831398D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D));
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.Font.Name = "Calibri";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            resources.ApplyResources(this.textBox7, "textBox7");
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.88958340883255D), Telerik.Reporting.Drawing.Unit.Inch(0.0729166641831398D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "Calibri";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            resources.ApplyResources(this.textBox1, "textBox1");
            // 
            // textBox10
            // 
            this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.7395833730697632D), Telerik.Reporting.Drawing.Unit.Inch(0.0729166641831398D));
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.76041650772094727D), Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D));
            this.textBox10.Style.Font.Bold = true;
            this.textBox10.Style.Font.Name = "Calibri";
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            resources.ApplyResources(this.textBox10, "textBox10");
            // 
            // textBox11
            // PKSelect
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1395835876464844D), Telerik.Reporting.Drawing.Unit.Inch(0.070833362638950348D));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79996031522750854D), Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D));
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Font.Name = "Calibri";
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            resources.ApplyResources(this.textBox11, "textBox11");
            // 
            // textBox12
            // 
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4D), Telerik.Reporting.Drawing.Unit.Inch(0.0729166641831398D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D));
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.Font.Name = "Calibri";
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            resources.ApplyResources(this.textBox12, "textBox12");
            // 
            // textBox13
            // 
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.9000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(0.0729166641831398D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D));
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.Font.Name = "Calibri";
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            resources.ApplyResources(this.textBox13, "textBox13");
            // 
            // textBox14
            // 
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.1000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(0.0729166641831398D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D));
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Font.Name = "Calibri";
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            resources.ApplyResources(this.textBox14, "textBox14");
            // 
            // btnPay
            // 
            navigateToUrlAction3.Url = null;
            this.btnPay.Action = navigateToUrlAction3;
            this.btnPay.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.1000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(0.047916729003190994D));
            this.btnPay.MimeType = "image/png";
            this.btnPay.Name = "btnPay";
            this.btnPay.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.599999725818634D), Telerik.Reporting.Drawing.Unit.Inch(0.11999999731779099D));
            this.btnPay.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.btnPay.Value = ((object)(resources.GetObject("btnPay.Value")));
            this.btnPay.ItemDataBinding += new System.EventHandler(this.btnPay_ItemDataBinding);
            // 
            // textBox16
            // 
            this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(0.0729166641831398D));
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.599921703338623D), Telerik.Reporting.Drawing.Unit.Inch(0.054166633635759354D));
            this.textBox16.Style.Color = System.Drawing.Color.Red;
            this.textBox16.Style.Font.Bold = true;
            this.textBox16.Style.Font.Name = "Calibri";
            this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            resources.ApplyResources(this.textBox16, "textBox16");
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.lblReport,
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox8,
            this.lblAction});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // lblReport
            // 
            this.lblReport.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.lblReport.Name = "lblReport";
            this.lblReport.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79996061325073242D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.lblReport.Style.Font.Bold = true;
            this.lblReport.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.lblReport.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.lblReport.TextWrap = true;
            resources.ApplyResources(this.lblReport, "lblReport");
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.88958340883255D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79996061325073242D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox2.TextWrap = true;
            resources.ApplyResources(this.textBox2, "textBox2");
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.7895833253860474D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79996061325073242D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox3.TextWrap = true;
            resources.ApplyResources(this.textBox3, "textBox3");
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1395835876464844D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79996061325073242D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox4.TextWrap = true;
            resources.ApplyResources(this.textBox4, "textBox4");
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79996061325073242D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox5.Style.Font.Bold = true;
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox5.TextWrap = true;
            resources.ApplyResources(this.textBox5, "textBox5");
            // 
            // textBox6
            // 
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.9000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79996061325073242D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox6.TextWrap = true;
            resources.ApplyResources(this.textBox6, "textBox6");
            // 
            // textBox8
            // 
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.1000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79996061325073242D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox8.TextWrap = true;
            resources.ApplyResources(this.textBox8, "textBox8");
            // 
            // lblAction
            // 
            this.lblAction.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.lblAction.Name = "lblAction";
            this.lblAction.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.69996088743209839D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.lblAction.Style.Font.Bold = true;
            this.lblAction.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.lblAction.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.lblAction.TextWrap = true;
            resources.ApplyResources(this.lblAction, "lblAction");
            // 
            // group1
            // 
            this.group1.GroupFooter = this.groupFooterSection1;
            this.group1.GroupHeader = this.groupHeaderSection1;
            this.group1.Name = "group1";
            // 
            // groupFooterSection1
            // 
            this.groupFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.22091197967529297D);
            this.groupFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox15,
            this.textBox70,
            this.textBox69,
            this.textBox68,
            this.textBox67,
            this.btnPayAll});
            this.groupFooterSection1.Name = "groupFooterSection1";
            this.groupFooterSection1.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.groupFooterSection1.Style.Font.Name = "Calibri";
            this.groupFooterSection1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            // 
            // textBox15
            // 
            this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.0729166641831398D));
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79996061325073242D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox15.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(90)))), ((int)(((byte)(169)))));
            this.textBox15.Style.Font.Bold = true;
            this.textBox15.Style.Font.Name = "Calibri";
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox15.TextWrap = true;
            resources.ApplyResources(this.textBox15, "textBox15");
            // 
            // textBox70
            // 
            this.textBox70.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.9396228790283203D), Telerik.Reporting.Drawing.Unit.Inch(0.079166732728481293D));
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99992102384567261D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox70.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(90)))), ((int)(((byte)(169)))));
            this.textBox70.Style.Font.Bold = true;
            this.textBox70.Style.Font.Name = "Calibri";
            this.textBox70.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox70.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox70.TextWrap = true;
            resources.ApplyResources(this.textBox70, "textBox70");
            // 
            // textBox69
            // 
            this.textBox69.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4D), Telerik.Reporting.Drawing.Unit.Inch(0.079166732728481293D));
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79996061325073242D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox69.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(90)))), ((int)(((byte)(169)))));
            this.textBox69.Style.Font.Bold = true;
            this.textBox69.Style.Font.Name = "Calibri";
            this.textBox69.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox69.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox69.TextWrap = true;
            resources.ApplyResources(this.textBox69, "textBox69");
            // 
            // textBox68
            // 
            this.textBox68.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.8958334922790527D), Telerik.Reporting.Drawing.Unit.Inch(0.0729166641831398D));
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79996061325073242D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox68.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(90)))), ((int)(((byte)(169)))));
            this.textBox68.Style.Font.Bold = true;
            this.textBox68.Style.Font.Name = "Calibri";
            this.textBox68.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox68.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox68.TextWrap = true;
            resources.ApplyResources(this.textBox68, "textBox68");
            // 
            // textBox67
            // 
            this.textBox67.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.7916665077209473D), Telerik.Reporting.Drawing.Unit.Inch(0.0729166641831398D));
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0998822450637817D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBox67.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(90)))), ((int)(((byte)(169)))));
            this.textBox67.Style.Font.Bold = true;
            this.textBox67.Style.Font.Name = "Calibri";
            this.textBox67.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox67.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox67.TextWrap = true;
            resources.ApplyResources(this.textBox67, "textBox67");
            // 
            // btnPayAll
            // 
            navigateToUrlAction4.Url = "";
            this.btnPayAll.Action = navigateToUrlAction4;
            this.btnPayAll.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.1000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D));
            this.btnPayAll.MimeType = "image/png";
            this.btnPayAll.Name = "btnPayAll";
            this.btnPayAll.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.5999603271484375D), Telerik.Reporting.Drawing.Unit.Inch(0.12708346545696259D));
            this.btnPayAll.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.btnPayAll.Value = ((object)(resources.GetObject("btnPayAll.Value")));
            this.btnPayAll.ItemDataBinding += new System.EventHandler(this.btnPayAll_ItemDataBinding);
            // 
            // groupHeaderSection1
            // 
            this.groupHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D);
            this.groupHeaderSection1.Name = "groupHeaderSection1";
            // 
            // SRBillingStatement
            // 
            navigateToUrlAction5.Url = "/Corporate/BillingStatements?Type=-1";
            this.Action = navigateToUrlAction5;
            this.Groups.AddRange(new Telerik.Reporting.Group[] {
            this.group1});
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.groupHeaderSection1,
            this.groupFooterSection1,
            this.detail,
            this.pageHeaderSection1});
            this.Name = "SRBillingStatement";
            this.PageSettings.Margins.Bottom = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.Margins.Left = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.Margins.Right = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.Margins.Top = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(7.7000789642333984D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.DetailSection detail;
        public Telerik.Reporting.TextBox textBox7;
        public Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        public Telerik.Reporting.TextBox textBox10;
        public Telerik.Reporting.TextBox textBox11;
        public Telerik.Reporting.TextBox textBox12;
        public Telerik.Reporting.TextBox textBox13;
        public Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox lblReport;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox lblAction;
        private Telerik.Reporting.Group group1;
        private Telerik.Reporting.GroupFooterSection groupFooterSection1;
        private Telerik.Reporting.GroupHeaderSection groupHeaderSection1;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.TextBox textBox68;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.PictureBox btnPayAll;

        #region Custom Functions
       
        public static string ChangeFormat(dynamic value)
        {
            string ChangedValue = string.Empty;
            if (value == null)
            {
                value = 0;
            }            
            ChangedValue = string.Format("{0:C}", Convert.ToDecimal(value));
            //ChangedValue = "$"+ string.Format("{0:C}",Convert.ToString( value));
            return ChangedValue;
        }
        //public static string ChangeFormat(double value)
        //{
        //    string ChangedValue = string.Empty;

        //    ChangedValue = string.Format("{0:C}", Convert.ToDecimal(value));
        //    return ChangedValue;
        //}
        public static decimal CheckNullValue(decimal value)
        {
            decimal ChangedValue = 0;

                ChangedValue =  value;
            //decimal.Parse()
            return ChangedValue;
        }

        public static string CheckPastDue(DateTime? date)
        {
            if (date != null)
            {
                if (date <= DateTime.Now)
                {
                    return "[Past Due]";
                }
            }
            return String.Empty;
        }
        //public static int GetCount()
        //{
        //    double count=0;
        //    SRBillingStatement.Count = Count + 1;
        //    return SRBillingStatement.Count;
        //}
        #endregion

        public Telerik.Reporting.TextBox textBox16;
        public Telerik.Reporting.PictureBox btnPay;
    }
}