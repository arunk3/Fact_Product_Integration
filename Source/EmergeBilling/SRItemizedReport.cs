namespace EmergeBilling
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using System.Linq;
    using Telerik.Reporting.Drawing;
    using System.Web;

    /// <summary>
    /// Summary description for SRItemizedReport.
    /// </summary>
    public partial class SRItemizedReport : Telerik.Reporting.Report
    {
        public SRItemizedReport()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        #region Custom Icon Adjustment

        //string LiveRunnerImagePath = HttpContext.Current.Server.MapPath("~/Content/themes/base/images/pdfLiveIcon.png");
        //string LiveRunnerImagePath = @"D:\Projects\EmergeMVCSln-v8-2\Source\Emerge\Content\themes\base\images/pdfLiveIcon.png";
        string ReviewImagePath = HttpContext.Current.Server.MapPath("~/Content/themes/base/images/pdfReviewIcon.png");
        //string ReviewImagePath = @"D:\Projects\EmergeMVCSln-v8-2\Source\Emerge\Content\themes\base\images/pdfLiveIcon.png";
        string LiveRunnerReviewImagePath = HttpContext.Current.Server.MapPath("~/Content/themes/base/images/pdfLiveReview.png");
        //string LiveRunnerReviewImagePath = @"D:\Projects\EmergeMVCSln-v8-2\Source\Emerge\Content\themes\base\images/pdfLiveIcon.png";
        public void detail_ItemDataBinding(object sender, EventArgs e)
        {
            HttpContext.Current.Session["PreLength"] = null;
            Telerik.Reporting.Processing.ReportItemBase itemBase = (Telerik.Reporting.Processing.ReportItemBase)sender;
            string ReportWithStatus = itemBase.DataObject["ReportsWithStatus"].ToString();
            GetDataSourceCustomized(ReportWithStatus, sender);
        }
        public void GetDataSourceCustomized(string ReportsWithStatus, object sender)
        {
            if (!string.IsNullOrEmpty(ReportsWithStatus))
            {
                string ReportNames = ReportsWithStatus;

                string[] strSplit = ReportNames.Split(',');
                string[] strDistinctProduct = strSplit.Distinct().ToArray();
                for (int Irow = 0; Irow < strDistinctProduct.Length; Irow++)
                {
                    int count = 0;
                    string product = string.Empty;
                    bool LiveRunner = false;
                    bool InReview = false;

                    if (strSplit[Irow] != "")
                    {
                        for (int i = 0; i < strSplit.Length; i++)
                        {
                            string[] subSplit = strDistinctProduct[Irow].Split('_');
                            if (strSplit[i] == strDistinctProduct[Irow])
                            {
                                count++;
                                if (count == 1)
                                {
                                    product = subSplit[0];
                                    //LiveRunner = subSplit[1].ToLower() == "0" ? false : true;
                                    //InReview = subSplit[2].ToLower() == "0" ? false : true;
                                }
                                else
                                {
                                    product = subSplit[0] + "(" + count + ")";
                                    //LiveRunner = subSplit[1].ToLower() == "0" ? false : true;
                                    //InReview = subSplit[2].ToLower() == "0" ? false : true;
                                }
                            }
                        }
                        SetValues(product, Irow + 1, LiveRunner, InReview, sender);
                    }
                }
            }
        }
        public void SetValues(string ProductCode, int ReportNumber, bool IsLiveRunner, bool IsInReview, object sender)
        {
            var procGroupHeader = (Telerik.Reporting.Processing.DetailSection)sender;
            //var procPanel = (Telerik.Reporting.Processing.Panel)(Telerik.Reporting.Processing.ElementTreeHelper.GetChildByName(procGroupHeader, "panel1"));
            var procPanel1 = (Telerik.Reporting.Processing.Panel)(Telerik.Reporting.Processing.ElementTreeHelper.GetChildByName(procGroupHeader, "panel2"));

            Telerik.Reporting.Processing.TextBox PCode = null;
            Telerik.Reporting.Processing.PictureBox pLiveReview = null;
            GenerateProductWithStatusLogo(ReportNumber, ProductCode, IsLiveRunner, IsInReview, procPanel1, ref PCode, ref pLiveReview);
        }

        private void GenerateProductWithStatusLogo(int ReportNumber, string ProductCode, bool IsLiveRunner, bool IsInReview, Telerik.Reporting.Processing.Panel procPanel1, ref Telerik.Reporting.Processing.TextBox PCode, ref Telerik.Reporting.Processing.PictureBox pLiveReview)
        {
            int MaxReportInARow = 2;


            PCode = (Telerik.Reporting.Processing.TextBox)(Telerik.Reporting.Processing.ElementTreeHelper.GetChildByName(procPanel1, "txtPCode" + ReportNumber));

            //pLiveReview = (Telerik.Reporting.Processing.PictureBox)(Telerik.Reporting.Processing.ElementTreeHelper.GetChildByName(procPanel1, "pLiveReview" + ReportNumber));
            int totalPreviousLength = 0;
            int sValue = 0;
            ProductCode = (ReportNumber > 1 ? "," : "") + ProductCode;



            int reportYUnit = 5;
            int reportCounter = ReportNumber;
            if (reportCounter > MaxReportInARow)
            {
                reportCounter = ReportNumber - MaxReportInARow;
                reportYUnit = 20;
                if ((ReportNumber == MaxReportInARow + 1) || (ReportNumber == (MaxReportInARow * 2) + 1) || (ReportNumber == (MaxReportInARow * 3) + 1) || (ReportNumber == (MaxReportInARow * 4) + 1) || (ReportNumber == (MaxReportInARow * 5) + 1) || (ReportNumber == (MaxReportInARow * 6) + 1) || (ReportNumber == (MaxReportInARow * 7) + 1) || (ReportNumber == (MaxReportInARow * 8) + 1) || (ReportNumber == (MaxReportInARow * 9) + 1) || (ReportNumber == (MaxReportInARow * 10) + 1) || (ReportNumber == (MaxReportInARow * 11) + 1) || (ReportNumber == (MaxReportInARow * 12) + 1) || (ReportNumber == (MaxReportInARow * 13) + 1))
                {
                    HttpContext.Current.Session["PreLength"] = null;
                }
                if (ReportNumber > (MaxReportInARow * 2))
                {
                    reportYUnit = 35;
                }
                if (ReportNumber > (MaxReportInARow * 3))
                {
                    reportYUnit = 50;
                }
                if (ReportNumber > (MaxReportInARow * 4))
                {
                    reportYUnit = 65;
                }
                if (ReportNumber > (MaxReportInARow * 5))
                {
                    reportYUnit = 80;
                }
                if (ReportNumber > (MaxReportInARow * 6))
                {
                    reportYUnit = 95;
                }
                if (ReportNumber > (MaxReportInARow * 7))
                {
                    reportYUnit = 110;
                }
                if (ReportNumber > (MaxReportInARow * 8))
                {
                    reportYUnit = 125;
                }
                // add more textboxes
                if (ReportNumber > (MaxReportInARow * 9))
                {
                    reportYUnit = 140;
                }
                if (ReportNumber > (MaxReportInARow * 10))
                {
                    reportYUnit = 155;
                }
                if (ReportNumber > (MaxReportInARow * 11))
                {
                    reportYUnit = 170;
                }
                if (ReportNumber > (MaxReportInARow * 12))
                {
                    reportYUnit = 185;
                }
                if (ReportNumber > (MaxReportInARow * 13))
                {
                    reportYUnit = 200;
                }

            }
            else
            {
                reportCounter = ReportNumber;
            }
            //int productSize2 = 12 * reportCounter, productSize3 = 21 * reportCounter, productSize4 = 26 * reportCounter, productSize5 = 30 * reportCounter;
            int productSize2 = 12, productSize3 = 18, productSize4 = 25, productSize5 = 28, productSize6 = 30, productSize7 = 37, productSize8 = 40, productSize9 = 42;
            if (HttpContext.Current.Session["PreLength"] != null)
            {
                sValue = Convert.ToInt32(HttpContext.Current.Session["PreLength"]);
            }
            if (ProductCode.Length == 2)
            {
                totalPreviousLength = sValue + productSize2;
                PCode.Size = new SizeU(new Unit(productSize2), new Unit(10));
                // pLiveReview.Location = new PointU(new Unit(sValue + productSize2), new Unit(reportYUnit));
                PCode.Location = new PointU(new Unit(sValue), new Unit(reportYUnit));
            }
            else if (ProductCode.Length == 3 && !ProductCode.ToLower().Contains("m") && !ProductCode.ToLower().Contains("w"))
            {
                productSize3 = (productSize3 - (ProductCode.ToLower().Contains(",") == true ? 3 : 0));
                totalPreviousLength = sValue + productSize3;
                PCode.Size = new SizeU(new Unit(productSize3), new Unit(10));
                //pLiveReview.Location = new PointU(new Unit(sValue + productSize3), new Unit(reportYUnit));
                PCode.Location = new PointU(new Unit(sValue), new Unit(reportYUnit));
            }
            else if (ProductCode.Length == 4 || ProductCode.ToLower().Contains("m") || ProductCode.ToLower().Contains("w"))
            {
                if (ProductCode.Length == 4)
                {
                    totalPreviousLength = sValue + productSize4;
                    PCode.Size = new SizeU(new Unit(productSize4), new Unit(10));
                    //pLiveReview.Location = new PointU(new Unit(sValue + productSize4), new Unit(reportYUnit));
                    PCode.Location = new PointU(new Unit(sValue), new Unit(reportYUnit));
                }
                else
                {
                    totalPreviousLength = sValue + productSize7;
                    PCode.Size = new SizeU(new Unit(productSize7), new Unit(10));
                    //pLiveReview.Location = new PointU(new Unit(sValue + productSize7), new Unit(reportYUnit));
                    PCode.Location = new PointU(new Unit(sValue), new Unit(reportYUnit));
                }

            }
            else if (ProductCode.Length == 5)
            {
                totalPreviousLength = sValue + productSize5;
                PCode.Size = new SizeU(new Unit(productSize5), new Unit(10));
                //pLiveReview.Location = new PointU(new Unit(sValue + productSize5), new Unit(reportYUnit));
                PCode.Location = new PointU(new Unit(sValue), new Unit(reportYUnit));
            }
            else if (ProductCode.Length == 6)
            {
                totalPreviousLength = sValue + productSize6;
                PCode.Size = new SizeU(new Unit(productSize6), new Unit(10));
                //pLiveReview.Location = new PointU(new Unit(sValue + productSize6), new Unit(reportYUnit));
                PCode.Location = new PointU(new Unit(sValue), new Unit(reportYUnit));

            }
            else if (ProductCode.Length == 7)
            {
                totalPreviousLength = sValue + productSize7;
                PCode.Size = new SizeU(new Unit(productSize7), new Unit(10));
                //pLiveReview.Location = new PointU(new Unit(sValue + productSize7), new Unit(reportYUnit));
                PCode.Location = new PointU(new Unit(sValue), new Unit(reportYUnit));

            }
            else if (ProductCode.Length == 8)
            {
                totalPreviousLength = sValue + productSize8;
                PCode.Size = new SizeU(new Unit(productSize8), new Unit(10));
                // pLiveReview.Location = new PointU(new Unit(sValue + productSize8), new Unit(reportYUnit));
                PCode.Location = new PointU(new Unit(sValue), new Unit(reportYUnit));
            }
            else if (ProductCode.Length == 9)
            {
                totalPreviousLength = sValue + productSize9;
                PCode.Size = new SizeU(new Unit(productSize9), new Unit(10));
                //pLiveReview.Location = new PointU(new Unit(sValue + productSize9), new Unit(reportYUnit));
                PCode.Location = new PointU(new Unit(sValue), new Unit(reportYUnit));
            }

            PCode.Value = PCode.Value + ProductCode;
            //if (IsLiveRunner && IsInReview)
            //{
            //    totalPreviousLength = totalPreviousLength + 24;
            //    pLiveReview.Size = new SizeU(new Unit(24), new Unit(10));
            //    pLiveReview.Image = Image.FromFile(LiveRunnerReviewImagePath);
            //}
            //else if (IsLiveRunner)
            //{
            //    totalPreviousLength = totalPreviousLength + 12;
            //    pLiveReview.Size = new SizeU(new Unit(12), new Unit(12));
            //    pLiveReview.Image = Image.FromFile(LiveRunnerImagePath);
            //}
            //else if (IsInReview)
            //{
            //    totalPreviousLength = totalPreviousLength + 12;
            //    pLiveReview.Size = new SizeU(new Unit(12), new Unit(12));
            //    pLiveReview.Image = Image.FromFile(ReviewImagePath);
            //}
            //else
            //{
            //    pLiveReview.Size = new SizeU(new Unit(0), new Unit(0));

            //}
            HttpContext.Current.Session["PreLength"] = totalPreviousLength;
        }


        #endregion

    }
}