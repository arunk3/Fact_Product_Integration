namespace EmergeBilling
{
    partial class SubSalesTotals
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.Drawing.FormattingRule formattingRule1 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule2 = new Telerik.Reporting.Drawing.FormattingRule();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SubSalesTotals));
            Telerik.Reporting.Drawing.FormattingRule formattingRule3 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule4 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule5 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule6 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule7 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule8 = new Telerik.Reporting.Drawing.FormattingRule();
            Telerik.Reporting.Drawing.FormattingRule formattingRule9 = new Telerik.Reporting.Drawing.FormattingRule();
            this.detail = new Telerik.Reporting.DetailSection();
            this.panel1 = new Telerik.Reporting.Panel();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.PicTotalRed = new Telerik.Reporting.PictureBox();
            this.PicTotalGreen = new Telerik.Reporting.PictureBox();
            this.panel2 = new Telerik.Reporting.Panel();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.picReportGreen = new Telerik.Reporting.PictureBox();
            this.picReportRed = new Telerik.Reporting.PictureBox();
            this.panel3 = new Telerik.Reporting.Panel();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.PicProductGreen = new Telerik.Reporting.PictureBox();
            this.PicProductRed = new Telerik.Reporting.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(0.800196647644043D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.panel1,
            this.panel2,
            this.panel3});
            this.detail.Name = "detail";
            // 
            // panel1
            // 
            this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.PicTotalRed,
            this.PicTotalGreen});
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(230.39619445800781D), Telerik.Reporting.Drawing.Unit.Pixel(76.815093994140625D));
            this.panel1.Style.BackgroundColor = System.Drawing.Color.Empty;
            this.panel1.Style.BorderColor.Bottom = System.Drawing.Color.Empty;
            this.panel1.Style.BorderColor.Default = System.Drawing.Color.Empty;
            this.panel1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.59996074438095093D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1000000238418579D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox1.Style.Font.Bold = false;
            this.textBox1.Style.Font.Name = "Calibri";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(16D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "= Fields.Total";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.59996074438095093D), Telerik.Reporting.Drawing.Unit.Inch(0.3000786304473877D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1000000238418579D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Name = "Calibri";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(20D);
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox2.Value = "= Fields.TotalValue.ToString(\"c\")";
            // 
            // textBox3
            // 
            /// formattingRule1.Filters.AddRange(new Telerik.Reporting.Data.Filter[] {    new Telerik.Reporting.Data.Filter("Fields.TotalPercentage", Telerik.Reporting.Data.FilterOperator.LessThan, "0")}); // change by pk
            formattingRule1.Filters.Add(new Telerik.Reporting.Filter("Fields.TotalPercentage", Telerik.Reporting.FilterOperator.LessThan, "0"));
            formattingRule1.Style.Color = System.Drawing.Color.Red;
            this.textBox3.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule1});
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.60000008344650269D), Telerik.Reporting.Drawing.Unit.Inch(0.50015729665756226D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1000000238418579D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox3.Style.Color = System.Drawing.Color.Green;
            this.textBox3.Style.Font.Bold = false;
            this.textBox3.Style.Font.Name = "Calibri";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(16D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.Value = "= ChangeFormat(Fields.TotalPercentage.ToString())";
            // 
            // PicTotalRed
            // 
            // formattingRule2.Filters.AddRange(new Telerik.Reporting.Data.Filter[] {     new Telerik.Reporting.Data.Filter("= GetColorByPercentage(Fields.TotalPercentage.ToString())", Telerik.Reporting.Data.FilterOperator.Equal, "0")}); // change by pk 
            formattingRule1.Filters.Add(new Telerik.Reporting.Filter("Fields.TotalPercentage", Telerik.Reporting.FilterOperator.LessThan, "0"));
            formattingRule2.Style.Visible = false;
            this.PicTotalRed.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule2});
            this.PicTotalRed.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.6999608278274536D), Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D));
            this.PicTotalRed.MimeType = "image/png";
            this.PicTotalRed.Name = "PicTotalRed";
            this.PicTotalRed.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(28.79998779296875D), Telerik.Reporting.Drawing.Unit.Pixel(38.407546997070312D));
            this.PicTotalRed.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.PicTotalRed.Style.Visible = true;
            this.PicTotalRed.Value = ((object)(resources.GetObject("PicTotalRed.Value")));
            // 
            // PicTotalGreen
            // 
            // formattingRule3.Filters.AddRange(new Telerik.Reporting.Data.Filter[] {    new Telerik.Reporting.Data.Filter("= GetColorByPercentage(Fields.TotalPercentage.ToString())", Telerik.Reporting.Data.FilterOperator.Equal, "1")}); // change by pk
            formattingRule3.Filters.Add(new Telerik.Reporting.Filter("= GetColorByPercentage(Fields.TotalPercentage.ToString())", Telerik.Reporting.FilterOperator.Equal, "1"));
            formattingRule3.Style.Visible = false;
            this.PicTotalGreen.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule3});
            this.PicTotalGreen.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.6999608278274536D), Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D));
            this.PicTotalGreen.MimeType = "image/png";
            this.PicTotalGreen.Name = "PicTotalGreen";
            this.PicTotalGreen.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(28.79998779296875D), Telerik.Reporting.Drawing.Unit.Pixel(38.407546997070312D));
            this.PicTotalGreen.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.PicTotalGreen.Style.Visible = true;
            this.PicTotalGreen.Value = ((object)(resources.GetObject("PicTotalGreen.Value")));
            // 
            // panel2
            // 
            this.panel2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.picReportGreen,
            this.picReportRed});
            this.panel2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.59375D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.panel2.Name = "panel2";
            this.panel2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(230D), Telerik.Reporting.Drawing.Unit.Pixel(76.815093994140625D));
            this.panel2.Style.BackgroundColor = System.Drawing.Color.Empty;
            this.panel2.Style.BorderColor.Bottom = System.Drawing.Color.Empty;
            this.panel2.Style.BorderColor.Default = System.Drawing.Color.Empty;
            this.panel2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.60625022649765015D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1000000238418579D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox4.Style.Font.Bold = false;
            this.textBox4.Style.Font.Name = "Calibri";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(16D);
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.Value = "= Fields.Report";
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.60625022649765015D), Telerik.Reporting.Drawing.Unit.Inch(0.3000786304473877D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1000000238418579D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox5.Style.Font.Bold = true;
            this.textBox5.Style.Font.Name = "Calibri";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(20D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.Value = "= Fields.ReportValue.ToString(\"c\")";
            // 
            // textBox6
            // 
            // formattingRule4.Filters.AddRange(new Telerik.Reporting.Data.Filter[] {   new Telerik.Reporting.Data.Filter("Fields.ReportPercentage", Telerik.Reporting.Data.FilterOperator.LessThan, "0")}); // change by pk
            formattingRule4.Filters.Add(new Telerik.Reporting.Filter("Fields.ReportPercentage", Telerik.Reporting.FilterOperator.LessThan, "0"));
            formattingRule4.Style.Color = System.Drawing.Color.Red;
            this.textBox6.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule4});
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.6062895655632019D), Telerik.Reporting.Drawing.Unit.Inch(0.50015729665756226D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1000000238418579D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox6.Style.Color = System.Drawing.Color.Green;
            this.textBox6.Style.Font.Bold = false;
            this.textBox6.Style.Font.Name = "Calibri";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(16D);
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.Value = "= ChangeFormat(Fields.ReportPercentage.ToString())";
            // 
            // picReportGreen
            // 
            // formattingRule5.Filters.AddRange(new Telerik.Reporting.Data.Filter[] {    new Telerik.Reporting.Data.Filter("= GetColorByPercentage(Fields.ReportPercentage.ToString())", Telerik.Reporting.Data.FilterOperator.Equal, "1")}); // change by pk
            formattingRule5.Filters.Add(new Telerik.Reporting.Filter("= GetColorByPercentage(Fields.ReportPercentage.ToString())", Telerik.Reporting.FilterOperator.Equal, "1"));
            formattingRule5.Style.Visible = false;
            this.picReportGreen.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule5});
            this.picReportGreen.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.7062501907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.30003926157951355D));
            this.picReportGreen.MimeType = "image/png";
            this.picReportGreen.Name = "picReportGreen";
            this.picReportGreen.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(28.79998779296875D), Telerik.Reporting.Drawing.Unit.Pixel(38.407546997070312D));
            this.picReportGreen.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.picReportGreen.Style.Visible = true;
            this.picReportGreen.Value = ((object)(resources.GetObject("picReportGreen.Value")));
            // 
            // picReportRed
            // 
            // formattingRule6.Filters.AddRange(new Telerik.Reporting.Data.Filter[] {      new Telerik.Reporting.Data.Filter("= GetColorByPercentage(Fields.ReportPercentage.ToString())", Telerik.Reporting.Data.FilterOperator.Equal, "0")}); // change by pk
            formattingRule6.Filters.Add(new Telerik.Reporting.Filter("= GetColorByPercentage(Fields.ReportPercentage.ToString())", Telerik.Reporting.FilterOperator.Equal, "0"));
            formattingRule6.Style.Visible = false;
            this.picReportRed.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule6});
            this.picReportRed.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.7062501907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.29996064305305481D));
            this.picReportRed.MimeType = "image/png";
            this.picReportRed.Name = "picReportRed";
            this.picReportRed.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(28.79998779296875D), Telerik.Reporting.Drawing.Unit.Pixel(38.407546997070312D));
            this.picReportRed.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.picReportRed.Style.Visible = true;
            this.picReportRed.Value = ((object)(resources.GetObject("picReportRed.Value")));
            // 
            // panel3
            // 
            this.panel3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.PicProductGreen,
            this.PicProductRed});
            this.panel3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.3020834922790527D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.panel3.Name = "panel3";
            this.panel3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(230D), Telerik.Reporting.Drawing.Unit.Pixel(76.815093994140625D));
            this.panel3.Style.BackgroundColor = System.Drawing.Color.Empty;
            this.panel3.Style.BorderColor.Default = System.Drawing.Color.Empty;
            this.panel3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            // 
            // textBox7
            // 
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.59791702032089233D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1000000238418579D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox7.Style.Font.Bold = false;
            this.textBox7.Style.Font.Name = "Calibri";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(16D);
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.Value = "= Fields.Product";
            // 
            // textBox8
            // 
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.59791702032089233D), Telerik.Reporting.Drawing.Unit.Inch(0.3000786304473877D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1000000238418579D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Name = "Calibri";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(20D);
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.Value = "= Fields.ProductValue.ToString(\"c\")";
            // 
            // textBox9
            // 
            // formattingRule7.Filters.AddRange(new Telerik.Reporting.Data.Filter[] {        new Telerik.Reporting.Data.Filter("Fields.ProductPercentage", Telerik.Reporting.Data.FilterOperator.LessThan, "0")}); // chaneg by pk
            formattingRule7.Filters.Add(new Telerik.Reporting.Filter("Fields.ProductPercentage", Telerik.Reporting.FilterOperator.LessThan, "0"));
            formattingRule7.Style.Color = System.Drawing.Color.Red;
            this.textBox9.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule7});
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.59795635938644409D), Telerik.Reporting.Drawing.Unit.Inch(0.50015729665756226D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1000000238418579D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox9.Style.Color = System.Drawing.Color.Green;
            this.textBox9.Style.Font.Bold = false;
            this.textBox9.Style.Font.Name = "Calibri";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(16D);
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.Value = "= ChangeFormat(Fields.ProductPercentage.ToString())";
            // 
            // PicProductGreen
            // 
            // formattingRule8.Filters.AddRange(new Telerik.Reporting.Data.Filter[] {        new Telerik.Reporting.Data.Filter("= GetColorByPercentage(Fields.ProductPercentage.ToString())", Telerik.Reporting.Data.FilterOperator.Equal, "1")}); change by pk
            formattingRule8.Filters.Add(new Telerik.Reporting.Filter("= GetColorByPercentage(Fields.ProductPercentage.ToString())", Telerik.Reporting.FilterOperator.Equal, "1"));
            formattingRule8.Style.Visible = false;
            this.PicProductGreen.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule8});
            this.PicProductGreen.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.6979166269302368D), Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D));
            this.PicProductGreen.MimeType = "image/png";
            this.PicProductGreen.Name = "PicProductGreen";
            this.PicProductGreen.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(28.99615478515625D), Telerik.Reporting.Drawing.Unit.Pixel(38.407546997070312D));
            this.PicProductGreen.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.PicProductGreen.Style.Visible = true;
            this.PicProductGreen.Value = ((object)(resources.GetObject("PicProductGreen.Value")));
            // 
            // PicProductRed
            // 
            // formattingRule9.Filters.AddRange(new Telerik.Reporting.Data.Filter[] {     new Telerik.Reporting.Data.Filter("= GetColorByPercentage(Fields.ProductPercentage.ToString())", Telerik.Reporting.Data.FilterOperator.Equal, "0")}); // change by pk
            formattingRule9.Filters.Add(new Telerik.Reporting.Filter("= GetColorByPercentage(Fields.ProductPercentage.ToString())", Telerik.Reporting.FilterOperator.Equal, "0"));
            formattingRule9.Style.Visible = false;
            this.PicProductRed.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule9});
            this.PicProductRed.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.6979166269302368D), Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D));
            this.PicProductRed.MimeType = "image/png";
            this.PicProductRed.Name = "PicProductRed";
            this.PicProductRed.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(28.99615478515625D), Telerik.Reporting.Drawing.Unit.Pixel(38.407546997070312D));
            this.PicProductRed.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.PicProductRed.Style.Visible = true;
            this.PicProductRed.Value = ((object)(resources.GetObject("PicProductRed.Value")));
            // 
            // SubSalesTotals
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "SubSalesTotals";
            this.PageNumberingStyle = Telerik.Reporting.PageNumberingStyle.Continue;
            this.PageSettings.Margins.Bottom = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.Margins.Left = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.Margins.Right = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.Margins.Top = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(7.6999998092651367D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        public Telerik.Reporting.DetailSection detail;

        #region Custom Functions
        public static int GetColorByPercentage(string percentage)
        {
            int result = 0;
            if (!string.IsNullOrEmpty(percentage))
            {
                if (decimal.Parse(percentage) < 0)
                {
                    result = 1;

                }
            }
            return result;
        }

        public static string ChangeFormat(string value)
        {
            string ChangedValue = string.Empty;
            if (!string.IsNullOrEmpty(value))
            {
                ChangedValue = string.Format("{0:0.0%}", decimal.Parse(value));
                if (!ChangedValue.Contains("-"))
                {
                    ChangedValue = "+" + ChangedValue;
                }
            }
            return ChangedValue;
        }
        #endregion

        private Telerik.Reporting.Panel panel1;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.PictureBox PicTotalRed;
        private Telerik.Reporting.PictureBox PicTotalGreen;
        private Telerik.Reporting.Panel panel2;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.PictureBox picReportGreen;
        private Telerik.Reporting.PictureBox picReportRed;
        private Telerik.Reporting.Panel panel3;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.PictureBox PicProductGreen;
        private Telerik.Reporting.PictureBox PicProductRed;

    }
}