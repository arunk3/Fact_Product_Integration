namespace EmergeBilling
{
    partial class SubAdjustmentsReport
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.detail = new Telerik.Reporting.DetailSection();
            this.txtPrice = new Telerik.Reporting.TextBox();
            this.txtQuantity = new Telerik.Reporting.TextBox();
            this.txtReport = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.group1 = new Telerik.Reporting.Group();
            this.groupFooterSection1 = new Telerik.Reporting.GroupFooterSection();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.groupHeaderSection1 = new Telerik.Reporting.GroupHeaderSection();
            this.textBox2 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Pixel(22.996223449707031D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtPrice,
            this.txtQuantity,
            this.txtReport,
            this.textBox1});
            this.detail.Name = "detail";
            this.detail.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.detail.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.detail.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.detail.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.detail.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.detail.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.9000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89996027946472168D), Telerik.Reporting.Drawing.Unit.Pixel(22.996223449707031D));
            this.txtPrice.Style.Font.Bold = true;
            this.txtPrice.Style.Font.Name = "Calibri";
            this.txtPrice.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.txtPrice.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtPrice.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtPrice.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtPrice.Value = "= ChangeFormat(CStr(Fields.ReportAmount*Fields.ProductQty))";
            // 
            // txtQuantity
            // 
            this.txtQuantity.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.5145835876464844D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.30000007152557373D), Telerik.Reporting.Drawing.Unit.Pixel(22.996223449707031D));
            this.txtQuantity.Style.Font.Bold = true;
            this.txtQuantity.Style.Font.Name = "Calibri";
            this.txtQuantity.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.txtQuantity.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtQuantity.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtQuantity.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtQuantity.Value = "=Fields.ProductQty";
            // 
            // txtReport
            // 
            this.txtReport.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.txtReport.Name = "txtReport";
            this.txtReport.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4999605417251587D), Telerik.Reporting.Drawing.Unit.Pixel(22.636222839355469D));
            this.txtReport.Style.Font.Bold = true;
            this.txtReport.Style.Font.Name = "Calibri";
            this.txtReport.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.txtReport.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtReport.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtReport.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtReport.Value = "= GetDisplayName(Fields.ProductDisplayName)";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.6083334684371948D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79999971389770508D), Telerik.Reporting.Drawing.Unit.Pixel(22.996223449707031D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "Calibri";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox1.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "=Fields.ProductCode";
            // 
            // group1
            // 
            this.group1.GroupFooter = this.groupFooterSection1;
            this.group1.GroupHeader = this.groupHeaderSection1;
            this.group1.Name = "group1";
            // 
            // groupFooterSection1
            // 
            this.groupFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Pixel(23D);
            this.groupFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox8,
            this.textBox7,
            this.textBox6});
            this.groupFooterSection1.Name = "groupFooterSection1";
            this.groupFooterSection1.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(187)))), ((int)(((byte)(187)))));
            // 
            // textBox8
            // 
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79996055364608765D), Telerik.Reporting.Drawing.Unit.Inch(0.23950459063053131D));
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Name = "Calibri";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.Value = "Total";
            // 
            // textBox7
            // 
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.09375D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.69996058940887451D), Telerik.Reporting.Drawing.Unit.Inch(0.23950459063053131D));
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.Font.Name = "Calibri";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.textBox7.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.Value = "= Sum(Fields.ProductQty)";
            // 
            // textBox6
            // 
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.9156253337860107D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.88433510065078735D), Telerik.Reporting.Drawing.Unit.Inch(0.23950459063053131D));
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.Font.Name = "Calibri";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.textBox6.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.Value = "= ChangeFormat(Sum(Fields.ReportAmount*Fields.ProductQty).ToString())";
            // 
            // groupHeaderSection1
            // 
            this.groupHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Pixel(23D);
            this.groupHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox2});
            this.groupHeaderSection1.Name = "groupHeaderSection1";
            this.groupHeaderSection1.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(187)))), ((int)(((byte)(187)))));
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4999605417251587D), Telerik.Reporting.Drawing.Unit.Pixel(22.636222839355469D));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Name = "Calibri";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox2.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox2.Value = "Adjustment Reports";
            // 
            // SubAdjustmentsReport
            // 
            this.Groups.AddRange(new Telerik.Reporting.Group[] {
            this.group1});
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.groupHeaderSection1,
            this.groupFooterSection1,
            this.detail});
            this.Name = "SubAdjustmentsReport";
            this.PageSettings.Margins.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.PageSettings.Margins.Left = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.PageSettings.Margins.Right = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.PageSettings.Margins.Top = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(3.7999999523162842D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        public Telerik.Reporting.TextBox txtPrice;
        public Telerik.Reporting.TextBox txtQuantity;
        public Telerik.Reporting.TextBox txtReport;
        public Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.Group group1;
        private Telerik.Reporting.GroupFooterSection groupFooterSection1;
        private Telerik.Reporting.GroupHeaderSection groupHeaderSection1;
        public Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox8;
        public Telerik.Reporting.TextBox textBox7;
        public Telerik.Reporting.TextBox textBox6;
        public Telerik.Reporting.DetailSection detail;

        #region Custom Functions
        public static string GetDisplayName(string DisplayName)
        {
            string DName = string.Empty;
            if (!string.IsNullOrEmpty(DisplayName))
            {
                if (DisplayName.Length > 41)
                {
                    DName = DisplayName.Substring(0, 41) + "*";
                }
                else
                {
                    DName = DisplayName + "*";
                }
            }
            return DName;
        }

        public static string ChangeFormat(string value)
        {
            string ChangedValue = string.Empty;
            if (!string.IsNullOrEmpty(value))
            {
                ChangedValue = string.Format("{0:C}", decimal.Parse(value));
            }
            return ChangedValue;
        }

        public static decimal GetPrice(decimal? ReportAmount, decimal? ProductQty)
        {
            decimal Price = 0;
            if (ReportAmount!=null && ProductQty!=null)
            {
                Price =System.Convert.ToDecimal(ProductQty * ReportAmount);
            }
            return Price;
        }
        #endregion
    }
}