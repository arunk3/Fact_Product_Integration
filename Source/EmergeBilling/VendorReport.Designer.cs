using System;
namespace EmergeBilling
{
    partial class VendorReport
    {
        public static string Month { get; set; }
        public static string Year { get; set; }

        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.detail = new Telerik.Reporting.DetailSection();
            this.srVedorSummary = new Telerik.Reporting.SubReport();
            this.srVendorSummary1 = new EmergeBilling.SRVendorSummary();
            this.lblVendorSummary = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.panel2 = new Telerik.Reporting.Panel();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.srVendorTransactions = new Telerik.Reporting.SubReport();
            this.srVendorTransactions1 = new EmergeBilling.SRVendorTransactions();
            this.subPageBreack = new Telerik.Reporting.SubReport();
            this.srDuplicateTransactions = new Telerik.Reporting.SubReport();
            this.srDuplicateTransactions1 = new EmergeBilling.SRDuplicateTransactions();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.subReport1 = new Telerik.Reporting.SubReport();
            this.srPageBreack21 = new EmergeBilling.SRPageBreack2();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.panel5 = new Telerik.Reporting.Panel();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.textBox75 = new Telerik.Reporting.TextBox();
            this.srPageBreack1 = new EmergeBilling.SRPageBreack();
            ((System.ComponentModel.ISupportInitialize)(this.srVendorSummary1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.srVendorTransactions1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.srDuplicateTransactions1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.srPageBreack21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.srPageBreack1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(4D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.srVedorSummary,
            this.lblVendorSummary,
            this.textBox46,
            this.textBox15,
            this.panel2,
            this.textBox9,
            this.srVendorTransactions,
            this.subPageBreack,
            this.srDuplicateTransactions,
            this.textBox10,
            this.subReport1});
            this.detail.Name = "detail";
            this.detail.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.detail.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.detail.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.detail.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.detail.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.detail.Style.Font.Name = "Calibri";
            this.detail.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.detail.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            this.detail.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            // 
            // srVedorSummary
            // 
            this.srVedorSummary.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10003937780857086D), Telerik.Reporting.Drawing.Unit.Inch(0.90000003576278687D));
            this.srVedorSummary.Name = "srVedorSummary";
            this.srVedorSummary.ReportSource = this.srVendorSummary1;
            this.srVedorSummary.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.69999885559082D), Telerik.Reporting.Drawing.Unit.Inch(0.66033762693405151D));
            this.srVedorSummary.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srVedorSummary.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srVedorSummary.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srVedorSummary.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srVedorSummary.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srVedorSummary.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // srVendorSummary1
            // 
            this.srVendorSummary1.Name = "SRVendorTransactions";
            // 
            // lblVendorSummary
            // 
            this.lblVendorSummary.CanShrink = false;
            this.lblVendorSummary.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10003937780857086D), Telerik.Reporting.Drawing.Unit.Inch(0.33595168590545654D));
            this.lblVendorSummary.Name = "lblVendorSummary";
            this.lblVendorSummary.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999983787536621D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.lblVendorSummary.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(218)))), ((int)(((byte)(237)))));
            this.lblVendorSummary.Style.Font.Bold = true;
            this.lblVendorSummary.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.lblVendorSummary.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.lblVendorSummary.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblVendorSummary.Value = "Vendor Summary";
            // 
            // textBox46
            // 
            this.textBox46.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.0000004768371582D), Telerik.Reporting.Drawing.Unit.Inch(0.13587284088134766D));
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7999998331069946D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox46.Style.Font.Bold = true;
            this.textBox46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox46.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox46.Value = "= GetDate()";
            // 
            // textBox15
            // 
            this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10003937780857086D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000894069672D));
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.9541667103767395D), Telerik.Reporting.Drawing.Unit.Pixel(22.636222839355469D));
            this.textBox15.Style.Font.Bold = true;
            this.textBox15.Style.Font.Name = "Calibri";
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox15.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox15.Value = "Billing Report";
            // 
            // panel2
            // 
            this.panel2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox8,
            this.textBox6,
            this.textBox5,
            this.textBox4,
            this.textBox3,
            this.textBox2,
            this.textBox1,
            this.textBox7});
            this.panel2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10003942996263504D), Telerik.Reporting.Drawing.Unit.Inch(0.66033786535263062D));
            this.panel2.Name = "panel2";
            this.panel2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.69999885559082D), Telerik.Reporting.Drawing.Unit.Pixel(24.0037784576416D));
            this.panel2.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(187)))), ((int)(((byte)(187)))));
            this.panel2.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.panel2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.panel2.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.panel2.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.panel2.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.panel2.Style.Font.Name = "Calibri";
            this.panel2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.panel2.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.panel2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.panel2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.panel2.Style.Visible = true;
            // 
            // textBox8
            // 
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.1999611854553223D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.49162662029266357D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Name = "Calibri";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.Value = "Profit";
            // 
            // textBox6
            // 
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.1999602317810059D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.80000007152557373D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.Font.Name = "Calibri";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.Value = "Total Price";
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.5999608039855957D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.585416853427887D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox5.Style.Font.Bold = true;
            this.textBox5.Style.Font.Name = "Calibri";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.Value = "Cost";
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.2999606132507324D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.69999974966049194D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Style.Font.Name = "Calibri";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.Value = "Total Cost";
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.0999608039855957D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89999961853027344D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Name = "Calibri";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.Value = "Average Price";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.7999606132507324D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.7000001072883606D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Name = "Calibri";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox2.Value = "Quantity";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.8999606370925903D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "Calibri";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "Data Source";
            // 
            // textBox7
            // 
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9368867874145508E-05D), Telerik.Reporting.Drawing.Unit.Inch(3.9368867874145508E-05D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5999212265014648D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.Font.Name = "Calibri";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.Value = "ReportType";
            // 
            // textBox9
            // 
            this.textBox9.CanShrink = false;
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1000790074467659D), Telerik.Reporting.Drawing.Unit.Inch(1.8771231174468994D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999983787536621D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox9.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(218)))), ((int)(((byte)(237)))));
            this.textBox9.Style.Font.Bold = true;
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.Value = "Vendor Transactions";
            // 
            // srVendorTransactions
            // 
            this.srVendorTransactions.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1000790074467659D), Telerik.Reporting.Drawing.Unit.Inch(2.2000000476837158D));
            this.srVendorTransactions.Name = "srVendorTransactions";
            this.srVendorTransactions.ReportSource = this.srVendorTransactions1;
            this.srVendorTransactions.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.69999885559082D), Telerik.Reporting.Drawing.Unit.Inch(0.66033762693405151D));
            this.srVendorTransactions.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srVendorTransactions.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srVendorTransactions.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srVendorTransactions.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srVendorTransactions.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srVendorTransactions.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // srVendorTransactions1
            // 
            this.srVendorTransactions1.Name = "SRVendorSummary";
            // 
            // subPageBreack
            // 
            this.subPageBreack.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.10000000894069672D), Telerik.Reporting.Drawing.Unit.Inch(1.7000000476837158D));
            this.subPageBreack.Name = "subPageBreack";
            this.subPageBreack.ReportSource = this.srPageBreack1;
            this.subPageBreack.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.69999885559082D), Telerik.Reporting.Drawing.Unit.Inch(0.052083492279052734D));
            this.subPageBreack.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subPageBreack.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subPageBreack.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subPageBreack.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subPageBreack.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.subPageBreack.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // srDuplicateTransactions
            // 
            this.srDuplicateTransactions.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1000790074467659D), Telerik.Reporting.Drawing.Unit.Inch(3.3000001907348633D));
            this.srDuplicateTransactions.Name = "srDuplicateTransactions";
            this.srDuplicateTransactions.ReportSource = this.srDuplicateTransactions1;
            this.srDuplicateTransactions.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.69999885559082D), Telerik.Reporting.Drawing.Unit.Inch(0.66033762693405151D));
            this.srDuplicateTransactions.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srDuplicateTransactions.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srDuplicateTransactions.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srDuplicateTransactions.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srDuplicateTransactions.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.srDuplicateTransactions.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // srDuplicateTransactions1
            // 
            this.srDuplicateTransactions1.Name = "SRDuplicateTransactions1";
            // 
            // textBox10
            // 
            this.textBox10.CanShrink = false;
            this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1000790074467659D), Telerik.Reporting.Drawing.Unit.Inch(3D));
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999983787536621D), Telerik.Reporting.Drawing.Unit.Pixel(24D));
            this.textBox10.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(218)))), ((int)(((byte)(237)))));
            this.textBox10.Style.Font.Bold = true;
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox10.Value = "Duplicate Transactions";
            // 
            // subReport1
            // 
            this.subReport1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.099999979138374329D), Telerik.Reporting.Drawing.Unit.Inch(2.8999998569488525D));
            this.subReport1.Name = "subReport1";
            this.subReport1.ReportSource = this.srPageBreack21;
            this.subReport1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999998092651367D), Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D));
            // 
            // srPageBreack21
            // 
            this.srPageBreack21.Name = "SRPageBreack2";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.60000002384185791D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.panel5});
            this.pageFooterSection1.Name = "pageFooterSection1";
            this.pageFooterSection1.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.pageFooterSection1.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.pageFooterSection1.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.pageFooterSection1.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.pageFooterSection1.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.pageFooterSection1.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.pageFooterSection1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            // 
            // panel5
            // 
            this.panel5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox74,
            this.textBox75});
            this.panel5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1458333283662796D), Telerik.Reporting.Drawing.Unit.Inch(0.099999956786632538D));
            this.panel5.Name = "panel5";
            this.panel5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6999540328979492D), Telerik.Reporting.Drawing.Unit.Inch(0.39999961853027344D));
            this.panel5.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Ridge;
            this.panel5.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Ridge;
            // 
            // textBox74
            // 
            this.textBox74.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.099997840821743011D), Telerik.Reporting.Drawing.Unit.Inch(0.099999748170375824D));
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.49999991059303284D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox74.Style.Font.Bold = true;
            this.textBox74.Style.Font.Name = "Calibri";
            this.textBox74.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.textBox74.Value = "= PageNumber";
            // 
            // textBox75
            // 
            this.textBox75.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.4999980926513672D), Telerik.Reporting.Drawing.Unit.Inch(0.099999748170375824D));
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1000397205352783D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox75.Style.Font.Bold = true;
            this.textBox75.Style.Font.Name = "Calibri";
            this.textBox75.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.textBox75.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox75.Value = "= Now().ToString(\"MMMM dd, yyyy hh:mm tt\")";
            // 
            // srPageBreack1
            // 
            this.srPageBreack1.Name = "SRPageBreack";
            // 
            // VendorReport
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail,
            this.pageFooterSection1});
            this.Name = "VendorReport";
            this.PageSettings.Margins.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.PageSettings.Margins.Left = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.PageSettings.Margins.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.PageSettings.Margins.Top = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(8D);
            this.NeedDataSource += new System.EventHandler(this.VendorReport_NeedDataSource);
            ((System.ComponentModel.ISupportInitialize)(this.srVendorSummary1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.srVendorTransactions1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.srDuplicateTransactions1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.srPageBreack21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.srPageBreack1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;

        #region Custom Functions
        public static string GetDate()
        {
            string month = !string.IsNullOrEmpty(Month) ? Month : string.Empty;
            if (!string.IsNullOrEmpty(Month))
            {
                int months = DateTime.Now.AddMonths(-int.Parse(month)).Month;
                DateTime dt = DateTime.Now.AddMonths(-months);
                month = dt.ToString("MMMM");
            }
            string year = !string.IsNullOrEmpty(Year) ? Year : string.Empty;
            string date = month + ", " + year;
            return date;
        }
        #endregion

        private Telerik.Reporting.SubReport srVedorSummary;
        private Telerik.Reporting.TextBox lblVendorSummary;
        private Telerik.Reporting.TextBox textBox46;
        public Telerik.Reporting.TextBox textBox15;
        private SRVendorSummary srVendorSummary1;
        private Telerik.Reporting.Panel panel5;
        private Telerik.Reporting.TextBox textBox74;
        private Telerik.Reporting.TextBox textBox75;
        public Telerik.Reporting.Panel panel2;
        public Telerik.Reporting.TextBox textBox8;
        public Telerik.Reporting.TextBox textBox6;
        public Telerik.Reporting.TextBox textBox5;
        public Telerik.Reporting.TextBox textBox4;
        public Telerik.Reporting.TextBox textBox3;
        public Telerik.Reporting.TextBox textBox2;
        public Telerik.Reporting.TextBox textBox1;
        public Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.SubReport srVendorTransactions;
        private SRVendorTransactions srVendorTransactions1;
        private Telerik.Reporting.SubReport subPageBreack;
        private Telerik.Reporting.SubReport srDuplicateTransactions;
        private SRDuplicateTransactions srDuplicateTransactions1;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.SubReport subReport1;
        private SRPageBreack2 srPageBreack21;
        private SRPageBreack srPageBreack1;
    }
}