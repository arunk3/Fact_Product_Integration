﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;
using System.Xml.Serialization;
using System.Data.Common;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace Emerge.Services
{
    public class BALJuno
    {


        public List<Proc_GetJunosResult> GetJunos(string SortingColumn, string SortingDirection,
                                                bool AllowPaging, string SearchString, int PageNum, int PageSize, byte LeadStatus)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {

                return ObjEmergeDALDataContext.GetJunos(SortingColumn, SortingDirection,
                                                         AllowPaging, SearchString, PageNum, PageSize, LeadStatus).ToList();
            }
        }

        public List<Proc_GetNewJunosResult> GetNewJunos(string SortingColumn, string SortingDirection,
                                        bool AllowPaging, int PageNum, int PageSize, byte LeadStatus)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {

                return ObjEmergeDALDataContext.GetNewJunos(SortingColumn, SortingDirection,
                                                         AllowPaging, PageNum, PageSize, LeadStatus).ToList();
            }
        }


        public List<Proc_GetNewJunosIntervalResult> GetNewJunosInterval(string SortingColumn, string SortingDirection,
                                      bool AllowPaging, int PageNum, int PageSize, byte LeadStatus, Guid UserId)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {

                return ObjEmergeDALDataContext.GetNewJunosInterval(SortingColumn, SortingDirection,
                                                         AllowPaging, PageNum, PageSize, LeadStatus, UserId).ToList();
            }
        }


        public List<tblCompany> GetEditLeadList(List<JunoList> JunoList)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {

                List<tblCompany> Collection = ObjEmergeDALDataContext.tblCompanies.Where(t => JunoList.Select(j => j.pkCompanyId).Contains(t.pkCompanyId)).ToList();
                return Collection;



            }
        }



        public List<tblCompany> CountJunoType()
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                //List<tblCompany> lstJunoType = new List<tblCompany>();
                return DX.tblCompanies.Where(db => (db.LeadStatus > 10 || (db.LeadStatus > 0 && db.LeadStatus < 5)) && db.IsDeleted == false).ToList <tblCompany>();
            }
        }

        ///// <summary>
        ///// Function To Move Juno 
        ///// </summary>
        ///// <returns></returns>
        public int MoveJuno(List<tblCompany> listCompany, byte LeadStatus)
        {
            DbTransaction DbTrans = null;
            tblCompany ObjtblCompany = new tblCompany();
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    ObjEmergeDALDataContext.Connection.Open();
                    DbTrans = ObjEmergeDALDataContext.Connection.BeginTransaction();
                    ObjEmergeDALDataContext.Transaction = DbTrans;
                    for (int i = 0; i < listCompany.Count; i++)
                    {
                        ObjtblCompany = ObjEmergeDALDataContext.tblCompanies.Where(d => d.pkCompanyId == listCompany.ElementAt(i).pkCompanyId).FirstOrDefault();
                        if (ObjtblCompany != null)
                        {
                            ObjtblCompany.LeadStatus = LeadStatus;
                            ObjtblCompany.StatusStartDate = DateTime.Now;
                            ObjEmergeDALDataContext.SubmitChanges();
                        }
                    }
                    DbTrans.Commit(); // Commit Transaction
                    return (int)1;
                }
                catch (Exception ex)
                {
                    if (DbTrans != null)
                    {
                        DbTrans.Rollback(); // Rollback Transaction
                    }
                    throw ex;
                }
                finally
                {
                    ObjEmergeDALDataContext.Transaction = null;
                    if (ObjEmergeDALDataContext.Connection.State == ConnectionState.Open)
                    {
                        ObjEmergeDALDataContext.Connection.Close(); // Close Connection
                    }
                }
            }
        }

        #region Juno Upload(Nitish)

        public List<Proc_Get_SalesManagersResult> GetManagerList(string role)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                //List<Proc_Get_SalesManagersResult> result = new List<Proc_Get_SalesManagersResult>();
                return DX.Get_SalesManagers(role).ToList < Proc_Get_SalesManagersResult>();
            }
        }

        public List<string> CheckIsDuplicateLeadNumber()
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return DX.tblCompanies.Select(D => D.LeadNumber).ToList();
            }
        }

        public int InsertJuno1(tblCompany ObjtblCompany, tblLocation ObjtblLocation)
        {
            int NewLocationId = 0;
            int CompanyId = 0;
            try
            {
                using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
                {

                    List<Proc_AddCompanyLeadResult> CompanyResult = ObjEmergeDALDataContext.AddCompanyLead(
                                                 ObjtblCompany.fkCompanyTypeId,
                                                 ObjtblCompany.fkApplicationId,
                                                 ObjtblCompany.CompanyCode,
                                                 ObjtblCompany.fkSalesRepId,
                                                 ObjtblCompany.fkSalesManagerId,
                                                 string.Empty,
                                                 ObjtblCompany.CompanyNote,
                                                 ObjtblCompany.CompanyName,
                                                 string.Empty,
                                                 ObjtblCompany.MainContactPersonName,
                                                 string.Empty,
                                                 ObjtblCompany.MainEmailId,
                                                 string.Empty,
                                                 ObjtblCompany.LeadSource,
                                                 ObjtblCompany.LeadNumber,
                                                 ObjtblCompany.CurrentProvider,
                                                 ObjtblCompany.NoOfLocations,
                                                 ObjtblCompany.LeadStatus,
                                                 ObjtblCompany.Priority1,
                                                 ObjtblCompany.Priority2,
                                                 ObjtblCompany.Priority3,
                                                 ObjtblCompany.IsEnabled,
                                                 false,
                                                 DateTime.Now,
                                                 DateTime.Now,
                                                 ObjtblCompany.CreatedById,
                                                 ObjtblCompany.LastModifiedById,
                                                 false,
                                                 string.Empty,
                                                 string.Empty,
                                                 string.Empty,
                                                 DateTime.Now,
                                                 true,
                                                 ObjtblCompany.CompanyUrl,
                                                 ObjtblCompany.SecondaryMainContactPersonName,
                                                 ObjtblCompany.MainTitle,
                                                 ObjtblCompany.SecondaryTitle,
                                                 ObjtblCompany.SecondaryEmail,
                                                 ObjtblCompany.MonthlyVolume,
                                                 ObjtblCompany.NoofEmployees,
                                                ObjtblCompany.LeadSourceId
                                                ,null
                                                ,"").ToList();
                    if (CompanyResult.First().CompanyId == 0)
                    {
                        return -2;//-2 error in company insertion
                    }

                    CompanyId = (int)CompanyResult.First().CompanyId;

                    NewLocationId = InsertNewLocation1(CompanyId, ObjtblLocation);
                    if (NewLocationId == 0)
                    {
                        return -3;// -3 error in Location insertion

                    }

                    return 1;//1 for success

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        public int InsertNewLocation1(int CompanyId, tblLocation ObjtblLocation)
        {
            int NewLocationId = 0;
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                List<Proc_AddLocationsResult> LocationResult = ObjEmergeDALDataContext.AddLocations(CompanyId
                                                        , ObjtblLocation.LocationCode
                                                        , string.Empty
                                                        , string.Empty
                                                        , ObjtblLocation.Address1
                                                        , ObjtblLocation.Address2
                                                        , ObjtblLocation.City
                                                        , ObjtblLocation.fkStateID
                                                        , ObjtblLocation.ZipCode
                                                        , ObjtblLocation.PhoneNo1
                                                        , string.Empty
                                                        , string.Empty
                                                        , ObjtblLocation.Ext2
                                                        , ObjtblLocation.Fax
                                                        , ObjtblLocation.Fax2
                                                        , true
                                                        , true
                                                        , DateTime.Now
                                                        , ObjtblLocation.CreatedById
                                                        , false
                                                        , string.Empty
                                                        , false, false
                                                        , ObjtblLocation.SecondaryPhone
                                                        , ObjtblLocation.SecondaryFax
                                                        ).ToList();

                NewLocationId = (int)LocationResult.First().LocationId;
            }
            return NewLocationId;
        }

        #endregion

        #region Juno Upload Old

        //public int InsertJuno(tblCompany ObjtblCompany, tblLocation ObjtblLocation)
        //{
        //    Guid NewLocationId = Guid.Empty;
        //    Guid CompanyId = Guid.Empty;
        //    try
        //    {
        //        using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
        //        {

        //            List<Proc_AddCompanyLeadResult> CompanyResult = ObjEmergeDALDataContext.AddCompanyLead(
        //                                         ObjtblCompany.fkCompanyTypeId,
        //                                         ObjtblCompany.fkApplicationId,
        //                                         ObjtblCompany.CompanyCode,
        //                                         null,
        //                                         null,
        //                                         string.Empty,
        //                                         string.Empty,
        //                                         ObjtblCompany.CompanyName,
        //                                         string.Empty,
        //                                         ObjtblCompany.MainContactPersonName,
        //                                         string.Empty,
        //                                         ObjtblCompany.MainEmailId,
        //                                         string.Empty,
        //                                         ObjtblCompany.LeadSource,
        //                                         string.Empty,
        //                                         string.Empty,
        //                                         0,
        //                                         ObjtblCompany.LeadStatus,
        //                                         string.Empty,
        //                                         string.Empty,
        //                                         string.Empty,
        //                                         ObjtblCompany.IsEnabled,
        //                                         false,
        //                                         DateTime.Now,
        //                                         DateTime.Now,
        //                                         ObjtblCompany.CreatedById,
        //                                         ObjtblCompany.LastModifiedById,
        //                                         false,
        //                                         string.Empty,
        //                                         string.Empty,
        //                                         string.Empty,
        //                                         DateTime.Now,
        //                                         true,
        //                                         string.Empty,
        //                                         string.Empty,
        //                                         string.Empty,
        //                                         string.Empty,
        //                                         string.Empty,
        //                                         0,
        //                                         0
        //            ).ToList();
        //            if (CompanyResult.First().CompanyId == Guid.Empty)
        //            {
        //                return -2;//-2 error in company insertion
        //            }

        //            CompanyId = new Guid(CompanyResult.First().CompanyId.ToString());

        //            NewLocationId = InsertNewLocation(CompanyId, ObjtblLocation);
        //            if (NewLocationId == Guid.Empty)
        //            {
        //                return -3;// -3 error in Location insertion

        //            }

        //            return 1;//1 for success

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //    }
        //}

        //public Guid InsertNewLocation(Guid CompanyId, tblLocation ObjtblLocation)
        //{
        //    Guid NewLocationId = Guid.Empty;
        //    using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
        //    {
        //        List<Proc_AddLocationsResult> LocationResult = ObjEmergeDALDataContext.AddLocations(CompanyId
        //                                                , ObjtblLocation.LocationCode
        //                                                , string.Empty
        //                                                , string.Empty
        //                                                , ObjtblLocation.Address1
        //                                                , string.Empty
        //                                                , ObjtblLocation.City
        //                                                , ObjtblLocation.fkStateID
        //                                                , ObjtblLocation.ZipCode
        //                                                , ObjtblLocation.PhoneNo1
        //                                                , string.Empty
        //                                                , string.Empty
        //                                                , string.Empty
        //                                                , string.Empty
        //                                                , string.Empty
        //                                                , true
        //                                                , true
        //                                                , DateTime.Now
        //                                                , ObjtblLocation.CreatedById
        //                                                , false
        //                                                , string.Empty
        //                                                , false, false, string.Empty, string.Empty).ToList();

        //        NewLocationId = new Guid(LocationResult.First().LocationId.ToString());
        //    }
        //    return NewLocationId;
        //}

        #endregion


        public List<tblJunoInterval> CreateJunoInterval(List<tblJunoInterval> tblRowList)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                //iterate through all row to update
                foreach (var tblrow in tblRowList)
                {
                    // Attach each table entity(Row) to table
                    ObjEmergeDALDataContext.tblJunoIntervals.InsertOnSubmit(tblrow);
                }
                // Update the entities in the database
                ObjEmergeDALDataContext.SubmitChanges();
                //Return the updated entities 
                return tblRowList;
            }
        }
        public void DeleteJunoIntervalByUserId(Guid UserId)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                List<tblJunoInterval> tblRowList = ObjEmergeDALDataContext.tblJunoIntervals.Where(t => t.CreatedDate < DateTime.Now.AddHours(-24) && t.fkUserId == UserId).ToList();
                if (tblRowList.Count > 0)
                {
                    ObjEmergeDALDataContext.tblJunoIntervals.DeleteAllOnSubmit(tblRowList);
                    ObjEmergeDALDataContext.SubmitChanges();
                }

            }
        }
        public int UploadJuno(DataTable DTtemp)
        {
            int record = DTtemp.Rows.Count;
            string str = ConfigurationManager.ConnectionStrings["EmergeConnectionString"].ConnectionString;
            using (SqlConnection con = new SqlConnection(str))
            {
                DataTable dt = new DataTable();
                dt = DTtemp;
                SqlCommand cmd = new SqlCommand("Proc_UploadJuno");
                cmd.CommandTimeout = 0;
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TVPPar", dt);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return record;
            }
        }

    }

    [Serializable]
    public class TempJuno
    {
        public string JunoRequiredFields { get; set; }
        public string RegExp { get; set; }
        public bool IsRequired { get; set; }
    }
    public class JunoList
    {
        public int pkCompanyId { get; set; }
        public int LeadStatus { get; set; }
    }

    public class DeletedJunoList
    {
        public int pkCompanyId { get; set; }
    }
}
