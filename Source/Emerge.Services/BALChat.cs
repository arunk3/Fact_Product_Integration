﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;
using Emerge;
using System.Data.Common;
using System.Data;


namespace Emerge.Services
{
    public class BALChat
    {
        public proc_CheckUserUnderSameCompanyResult GetUserCompany(Guid Userid)
        {
            proc_CheckUserUnderSameCompanyResult ObjCheckUser = new proc_CheckUserUnderSameCompanyResult();
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                ObjCheckUser = dx.proc_CheckUserUnderSameCompany(Userid).FirstOrDefault();
            }
            return ObjCheckUser;
        }

        public int RemoveMangoChatWindows(string UserID, string ChatSessionId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                ObjDALDataContext.RemoveMangoChatWindows(UserID, ChatSessionId);
                return 1;

            }
        }
        public List<Proc_LoadAllAdminUsersForChatResult> LoadAllAdminUsers()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.Proc_LoadAllAdminUsersForChat(1).ToList();
            }
        }
        public List<Proc_Get_AllChatHistoryResult> GetChatHistoryforAdmin()
        {
            using (EmergeDALDataContext objDALDataContext = new EmergeDALDataContext())
            {
                return objDALDataContext.Get_AllChatHistory().ToList();
            }
        }

        public List<proc_Get_Distinct_PublishIdResult> GetChatUsersByChatid(string ClientId )
        {
            using (EmergeDALDataContext objDataContext = new EmergeDALDataContext())
            {
                return objDataContext.Get_Distinct_PublishId(ClientId).ToList();
            }
        }

        public string GetUserNamebyId(Guid fkUserId)
        {
           // string FirstName = string.Empty;
            using (EmergeDALDataContext objdal = new EmergeDALDataContext())
            {
                var record = objdal.tblCompanyUsers.Where(rec => rec.fkUserId == fkUserId).FirstOrDefault();
                return record.FirstName +" "+ record.LastName;

            }
        }
        public List<MangoService_Event> GetUsersChatbySeesionid( string ChatSessionId)
        {
            using (EmergeDALDataContext objdatbal = new EmergeDALDataContext())
            {
                return objdatbal.MangoService_Events.Where(rec => rec.ChatSession == ChatSessionId).ToList();
            }
        }

        public List<Proc_Get_ChatInfoBySessionIdForUserResult> GetChatUsersByPublishID(string ChatSession , string Userid)
        {
            using (EmergeDALDataContext objDataContext = new EmergeDALDataContext())
            {
                return objDataContext.ChatInfoBySessionIdForUser(ChatSession, Userid).ToList();
            }
        }


        public int DeleteChatbySessionID( string ChatSessionID)
        {
            using (EmergeDALDataContext ObjDataContext = new EmergeDALDataContext())
            {

                try
                {
                    ObjDataContext.Delete_ChatbySessionID(ChatSessionID);

                    return 1;
                }
                catch (Exception)
                {
                    return -1;
                }
            }
        }
        public List<Proc_GetRoleNameByCompanyUserIdResult> GetRoleName(Guid CompanyUserId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetRoleNameByCompanyUserId(CompanyUserId).ToList();
            }
        }  
    }

    public class ChatUserList
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string imageName { get; set; }
    }

}
