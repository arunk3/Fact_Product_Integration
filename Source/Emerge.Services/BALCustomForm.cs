﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;
using System.Web.Security;

namespace Emerge.Services
{
    public class BALCustomForm
    {
        public int InsertCustomFormDetails(tblCustomForm tblcustomform)
        {
            int StrResult = 0;
            try
            {
                using (EmergeDALDataContext DbContext = new EmergeDALDataContext())
                {
                    tblCustomForm objTblCustomForm = new tblCustomForm();
                    objTblCustomForm.Title = tblcustomform.Title;
                    objTblCustomForm.Description = tblcustomform.Description;
                    objTblCustomForm.Path = tblcustomform.Path;
                    objTblCustomForm.ModifiedDate = tblcustomform.ModifiedDate;
                    objTblCustomForm.IsActive = tblcustomform.IsActive;
                    objTblCustomForm.ModifiedLoggedId = tblcustomform.ModifiedLoggedId;
                    objTblCustomForm.UploadedByUserId = tblcustomform.UploadedByUserId;
                    objTblCustomForm.FkCompanyId = tblcustomform.FkCompanyId;
                    DbContext.tblCustomForms.InsertOnSubmit(objTblCustomForm);
                    DbContext.SubmitChanges();
                    StrResult = 1;
                }
            }
            catch (Exception)
            {

            }
            return StrResult;
        }

        public int InsertMyFormDetails(tblmyform tblmyform)
        {
            int StrResult = 0;
            try
            {
                using (EmergeDALDataContext DbContext = new EmergeDALDataContext())
                {

                    tblmyform ObjTblMyForm = new tblmyform();
                    ObjTblMyForm.Title = tblmyform.Title;
                    ObjTblMyForm.Description = tblmyform.Description;
                    ObjTblMyForm.Path = tblmyform.Path;
                    ObjTblMyForm.ModifiedDate = tblmyform.ModifiedDate;
                    ObjTblMyForm.IsActive = tblmyform.IsActive;
                    ObjTblMyForm.ModifiedLoggedId = tblmyform.ModifiedLoggedId;
                    DbContext.tblmyforms.InsertOnSubmit(ObjTblMyForm);
                    DbContext.SubmitChanges();
                    StrResult = 1;
                }
            }
            catch (Exception)
            {

            }
            return StrResult;
        }

        public List<CustomFormModelColl> GetCustomFormCollection()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                List<CustomFormModelColl> ObjData = new List<CustomFormModelColl>();
                ObjData = (from u in ObjDALDataContext.tblCustomForms
                           where u.IsActive == true
                           select new CustomFormModelColl 
                           {
                               PkCustomFormId = u.pkCustomFormId,
                               Title = u.Title,
                               Description = u.Description,
                               Path = u.Path,
                               ModifiedLoggedId = u.ModifiedLoggedId != null ? (Guid)u.ModifiedLoggedId : Guid.Empty,
                               UpdatedByUserId=u.UploadedByUserId!=null ? (Guid)u.UploadedByUserId : Guid.Empty,
                               ModifiedDate = u.ModifiedDate
                           }).ToList();
                return ObjData.ToList();
            }


        }

        


        public class CustomFormModelColl
        {
            public int PkCustomFormId { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
            public string Path { get; set; }
            public Guid ModifiedLoggedId { get; set; }
            public DateTime? ModifiedDate { get; set; }
            public Guid UpdatedByUserId { get; set; }
            public Guid FkCompanyId { get; set; }

        }
    }
}
