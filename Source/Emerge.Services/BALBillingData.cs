﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Emerge.Data;
using System.Data.Linq;
using System.Linq;

namespace Emerge.Services
{
    public class BALBillingData
    {
        public Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALCompanyInformation>, List<BALReportsByLocation>, List<proc_LoadOpenInvoiceByCompanyIdResult>, List<proc_GetBillingAdjustmentsResult>> GetAllData(string ApplicationId, string Month, string Year, int CompanyId, int LocationId, int UserId, Guid SalesRepId, int RefCode)
        {
            using (BALBilling ObjBALBilling = new BALBilling())
            {
                //Old process for fetching billing statement data.   
                //IMultipleResults Results = ObjBALBilling.GetBillingForOthersMVC(new Guid(ApplicationId), int.Parse(Month), int.Parse(Year),
                //    CompanyId,
                //    LocationId,
                //    UserId,
                //  SalesRepId,
                //    RefCode,
                //    1);

                //As per the new approach for billing, where single stored procedure (gives 7 result sets) divided into the 7 different store procedures.
                //Each one give us sinle own result set.

                //Result Set 1
                //List<BALBillingSummary> ObjBALBillingSummaryTotal = Results.GetResult<BALBillingSummary>().ToList();
                List<BALBillingSummary> ObjBALBillingSummaryTotal = ObjBALBilling.BillingDrugTestingFirstResultset(new Guid(ApplicationId), int.Parse(Month), int.Parse(Year),
                    CompanyId,
                    LocationId,
                    UserId,
                  SalesRepId,
                    RefCode,
                    2).GetResult<BALBillingSummary>().ToList();
                List<BALBillingSummary> ObjNotDrugTestingSummary = ObjBALBillingSummaryTotal.Where(t => t.pkReportCategoryId != 10 && t.pkReportCategoryId != (byte)ReportCategories.DrugTesting).ToList();
                List<BALBillingSummary> ObjDrugTestingSummary = ObjBALBillingSummaryTotal.Where(t => t.pkReportCategoryId != 10 && t.pkReportCategoryId == (byte)ReportCategories.DrugTesting).ToList();

                List<proc_GetBillingAdjustmentsResult> ObjBALBillingSummaryAdjustment = GetBillingAdjustment(new Guid(ApplicationId), int.Parse(Month), int.Parse(Year), CompanyId).ToList();

                //Result Set 2
                //List<BALCompanyInformation> ObjBALCompanyInformation = Results.GetResult<BALCompanyInformation>().ToList();
                List<BALCompanyInformation> ObjBALCompanyInformation = ObjBALBilling.BillingCompanyInfoSecondResultset(new Guid(ApplicationId), int.Parse(Month), int.Parse(Year),
                    CompanyId,
                    LocationId,
                    UserId,
                  SalesRepId,
                    RefCode,
                    2).GetResult<BALCompanyInformation>().ToList();

                //Result Set 3
                //List<BALCompanyLocationsForOthers> ObjBALCompanyLocationsForOthers = Results.GetResult<BALCompanyLocationsForOthers>().ToList();
                List<BALCompanyLocationsForOthers> ObjBALCompanyLocationsForOthers = ObjBALBilling.BillingCompanyLocationInfoThirdResultset(new Guid(ApplicationId), int.Parse(Month), int.Parse(Year),
                    CompanyId,
                    LocationId,
                    UserId,
                  SalesRepId,
                    RefCode,
                    2).GetResult<BALCompanyLocationsForOthers>().ToList();

                //Result Set 4
                //List<BALProductsPerLocation> ObjBALProductsPerLocation = Results.GetResult<BALProductsPerLocation>().ToList();
                List<BALProductsPerLocation> ObjBALProductsPerLocation = ObjBALBilling.BillingCompanyProdctInfoForthdResultset(new Guid(ApplicationId), int.Parse(Month), int.Parse(Year),
                    CompanyId,
                    LocationId,
                    UserId,
                  SalesRepId,
                    RefCode,
                    2).GetResult<BALProductsPerLocation>().ToList();

                //Result Set 5
                //List<BALPackagesPrice> ObjBalPackagePrice = Results.GetResult<BALPackagesPrice>().ToList();
                List<BALPackagesPrice> ObjBalPackagePrice = ObjBALBilling.BillingCompanyAdditionalFeesInfoFivthResultset(new Guid(ApplicationId), int.Parse(Month), int.Parse(Year),
                    CompanyId,
                    LocationId,
                    UserId,
                  SalesRepId,
                    RefCode,
                    2).GetResult<BALPackagesPrice>().ToList();

                //Result Set 6
                //List<BALPackageReport> ObjPackageReport = Results.GetResult<BALPackageReport>().ToList();
                List<BALPackageReport> ObjPackageReport = ObjBALBilling.BillingCompanyCCRAdditionlInfoSixthResultset(new Guid(ApplicationId), int.Parse(Month), int.Parse(Year),
                    CompanyId,
                    LocationId,
                    UserId,
                  SalesRepId,
                    RefCode,
                    2).GetResult<BALPackageReport>().ToList();

                //Result Set 7
                //List<BALReportsFee> ObjReportFee = Results.GetResult<BALReportsFee>().ToList();
                List<BALReportsFee> ObjReportFee = ObjBALBilling.BillingCompanyEMV_EDVSeventhhResultset(new Guid(ApplicationId), int.Parse(Month), int.Parse(Year),
                    CompanyId,
                    LocationId,
                    UserId,
                  SalesRepId,
                    RefCode,
                    2).GetResult<BALReportsFee>().ToList();

                List<BALReportsByLocation> ObjBALReportsByLocation = GetReportsByLocation(ObjBALCompanyLocationsForOthers, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode);
                List<proc_LoadOpenInvoiceByCompanyIdResult> ObjOpenInvoices = LoadOpenInvoiceByCompanyId(CompanyId);

                Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALCompanyInformation>, List<BALReportsByLocation>, List<proc_LoadOpenInvoiceByCompanyIdResult>, List<proc_GetBillingAdjustmentsResult>> collection = new Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALCompanyInformation>, List<BALReportsByLocation>, List<proc_LoadOpenInvoiceByCompanyIdResult>, List<proc_GetBillingAdjustmentsResult>>(ObjNotDrugTestingSummary, ObjDrugTestingSummary, ObjBALCompanyInformation, ObjBALReportsByLocation, ObjOpenInvoices, ObjBALBillingSummaryAdjustment);
                return collection;
            }
        }

        public Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALReportsByLocation>, List<proc_GetBillingAdjustmentsResult>> GetAllDataForApi(string ApplicationId, string Month, string Year, int CompanyId, int LocationId, int UserId, Guid SalesRepId, int RefCode)
        {
            using (BALBilling ObjBALBilling = new BALBilling())
            {
                //new Guid("7dc87d77-6bb5-4bb3-9837-fa861f36f281")
                IMultipleResults Results = ObjBALBilling.GetBillingForOthersMVC(new Guid(ApplicationId), int.Parse(Month), int.Parse(Year),
                    CompanyId,
                    LocationId,
                    UserId,
                  SalesRepId,
                    RefCode,
                    2);
                List<BALBillingSummary> ObjBALBillingSummaryTotal = Results.GetResult<BALBillingSummary>().ToList();
                List<BALBillingSummary> ObjBALBillingSummary = ObjBALBillingSummaryTotal.Where(d => d.pkReportCategoryId != 10).ToList();
                List<BALBillingSummary> ObjNotDrugTestingSummary = ObjBALBillingSummaryTotal.Where(t => t.pkReportCategoryId != 10 && t.pkReportCategoryId != (byte)ReportCategories.DrugTesting).ToList();
                List<BALBillingSummary> ObjDrugTestingSummary = ObjBALBillingSummaryTotal.Where(t => t.pkReportCategoryId != 10 && t.pkReportCategoryId == (byte)ReportCategories.DrugTesting).ToList();
                List<proc_GetBillingAdjustmentsResult> ObjBALBillingSummaryAdjustment = GetBillingAdjustment(new Guid(ApplicationId), int.Parse(Month), int.Parse(Year), CompanyId).ToList();

                List<BALCompanyInformation> ObjBALCompanyInformation = Results.GetResult<BALCompanyInformation>().ToList();
                List<BALCompanyLocationsForOthers> ObjBALCompanyLocationsForOthers = Results.GetResult<BALCompanyLocationsForOthers>().ToList();
                List<BALProductsPerLocation> ObjBALProductsPerLocation = Results.GetResult<BALProductsPerLocation>().ToList();

                List<BALPackagesPrice> ObjBalPackagePrice = Results.GetResult<BALPackagesPrice>().ToList();
                List<BALPackageReport> ObjPackageReport = Results.GetResult<BALPackageReport>().ToList();
                List<BALReportsFee> ObjReportFee = Results.GetResult<BALReportsFee>().ToList();
                List<BALReportsByLocation> ObjBALReportsByLocation = GetReportsByLocation(ObjBALCompanyLocationsForOthers, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode);

                Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALReportsByLocation>, List<proc_GetBillingAdjustmentsResult>> collection = new Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALReportsByLocation>, List<proc_GetBillingAdjustmentsResult>>(ObjNotDrugTestingSummary, ObjDrugTestingSummary, ObjBALReportsByLocation, ObjBALBillingSummaryAdjustment);
                return collection;
            }
        }

        public List<BALReportsByLocation> GetReportsByLocation(List<BALCompanyLocationsForOthers> ObjBALCompanyLocationsForOthers, List<BALProductsPerLocation> ObjBALProductsPerLocation, List<BALPackagesPrice> ObjBalPackagePrice, List<BALPackageReport> ObjPackageReport, List<BALReportsFee> ObjReportFee, int Month, int Year, int RefCode)
        {
            List<BALReportsByLocation> ObjBALReportsByLocation = new List<BALReportsByLocation>();
            int iTotalReports = 0;
            decimal dMVRAmt = 0;
            //  Guid CCR1 = new Guid("2bbfdabc-df06-41d1-8e3d-8b7689b5b60e");
            int CCR1 = 39;
            //Guid SCR = new Guid("3649ef63-5410-492e-b967-fcf8774a016a");
            int SCR = 8;
            // Guid FCR = new Guid("06a0c3b7-fc38-4fe4-b080-01ceb67452a7");
            int FCR = 47;
            if (ObjBALCompanyLocationsForOthers.Count > 0)
            {
                for (int iRow = 0; iRow < ObjBALCompanyLocationsForOthers.Count; iRow++)
                {
                    BALReportsByLocation Obj = new BALReportsByLocation();
                    Obj.ProductDisplayName = ObjBALCompanyLocationsForOthers.ElementAt(iRow).LocationCity + ", " + ObjBALCompanyLocationsForOthers.ElementAt(iRow).CompanyState;
                    if (ObjBALCompanyLocationsForOthers.ElementAt(iRow).IsHome == true)
                    {
                        //if (ObjBALCompanyLocationsForOthers.ElementAt(iRow).pkCompanyId == new Guid("7DC87D77-6BB5-4BB3-9837-FA861F36F281"))

                        if (ObjBALCompanyLocationsForOthers.ElementAt(iRow).pkCompanyId == 9514)
                        {
                            Obj.IsCompanyIcon = true;
                        }
                        else
                        {
                            Obj.IsHomeIcon = true;
                        }
                    }
                    //Obj.IsHome = ObjBALCompanyLocationsForOthers.ElementAt(iRow).IsHome;
                    Obj.IsCompany = true;
                    ObjBALReportsByLocation.Add(Obj);
                    List<BALProductsPerLocation> ObjProducts = ObjBALProductsPerLocation.Where(t => t.fkLocationId == ObjBALCompanyLocationsForOthers.ElementAt(iRow).pkLocationId && t.fkReportCategoryId != (byte)ReportCategories.DrugTesting).ToList();

                    List<BALPackagesPrice> ObjPackagePrice = ObjBalPackagePrice.Where(data => data.fkLocationId == ObjBALCompanyLocationsForOthers.ElementAt(iRow).pkLocationId).ToList();
                    #region Reports For Company Locations
                    if (ObjProducts.Count > 0)
                    {
                        // string TotalEarnLocationWise = "";
                        for (int iRowLvl = 0; iRowLvl < ObjProducts.Count; iRowLvl++)
                        {
                            BALReportsByLocation Obj2 = new BALReportsByLocation();
                            decimal TotalReportAmount = 0;
                            int CurrentTotalReports = 0;
                            if (ObjProducts.ElementAt(iRowLvl).IsManualReport == true)
                            {
                                iTotalReports += ObjProducts.ElementAt(iRowLvl).ProductQty;
                                TotalReportAmount = ObjProducts.ElementAt(iRowLvl).ProductQty * ObjProducts.ElementAt(iRowLvl).ReportAmount;
                                CurrentTotalReports = ObjProducts.ElementAt(iRowLvl).ProductQty;
                            }
                            else
                            {
                                iTotalReports += ObjProducts.ElementAt(iRowLvl).TotalReports;
                                TotalReportAmount = ObjProducts.ElementAt(iRowLvl).TotalReports * ObjProducts.ElementAt(iRowLvl).ReportAmount;
                                CurrentTotalReports = ObjProducts.ElementAt(iRowLvl).TotalReports;
                            }

                            Obj2.TotalOrderedReports = ObjProducts.ElementAt(iRowLvl).TotalReports;
                            Obj2.ProductDisplayName = ObjProducts.ElementAt(iRowLvl).ProductDisplayName;
                            Obj2.ProductQty = short.Parse(CurrentTotalReports.ToString());
                            Obj2.ReportAmount = TotalReportAmount;
                            Obj2.IsProduct = true;
                            ObjBALReportsByLocation.Add(Obj2);

                            if (ObjProducts.ElementAt(iRowLvl).ProductCode.ToLower() == "mvr")
                            {
                                List<BALReportsFee> ReportFee = ObjReportFee.Where(t => t.fkLocationId == ObjProducts.ElementAt(iRowLvl).fkLocationId && t.ProductCode.ToLower() == "mvr" && t.ReportAmount == ObjProducts.ElementAt(iRowLvl).ReportAmount).ToList();
                                BALReportsByLocation Obj3 = new BALReportsByLocation();
                                string ReportName_AddionalFee = "";
                                int Reports_Total_Count = 0;
                                decimal Report_AdditionalFee = 0.00m;
                                for (int iRowLvl_ = 0; iRowLvl_ < ReportFee.Count; iRowLvl_++)
                                {
                                    dMVRAmt += ReportFee.ElementAt(iRowLvl_).AdditionalFee;
                                    ReportName_AddionalFee = "MVR State Fee"; //ReportFee.ElementAt(iRowLvl_).MVRStateCode;
                                    if (ReportFee.ElementAt(iRowLvl_).ReportFee > 0)
                                    {
                                        Reports_Total_Count += ReportFee.ElementAt(iRowLvl_).TotalReports;
                                        Report_AdditionalFee += ReportFee.ElementAt(iRowLvl_).AdditionalFee * ReportFee.ElementAt(iRowLvl_).TotalReports;
                                    }
                                }
                                if (ReportFee.Count > 0 && Report_AdditionalFee > 0.00m)
                                {
                                    Obj3.ProductDisplayName = ReportName_AddionalFee;
                                    Obj3.ProductQty = short.Parse(Reports_Total_Count.ToString());
                                    Obj3.ReportAmount = Report_AdditionalFee;
                                    Obj3.IsSubProduct = true;
                                    ObjBALReportsByLocation.Add(Obj3);

                                    // ObjPdfTblLocation.InsertTable(Get3rd_PartyFee_Table(ReportName_AddionalFee, Reports_Total_Count.ToString(), Report_AdditionalFee.ToString()));
                                }
                            }
                            if (ObjProducts.ElementAt(iRowLvl).ProductCode.ToLower() == "scr")
                            {
                                List<BALReportsFee> ReportFee = ObjReportFee.Where(t => t.fkLocationId == ObjProducts.ElementAt(iRowLvl).fkLocationId && t.ProductCode.ToLower() == "scr" && t.ReportAmount == ObjProducts.ElementAt(iRowLvl).ReportAmount).ToList();
                                BALReportsByLocation Obj3 = new BALReportsByLocation();
                                string ReportName_AddionalFee = "";
                                int Reports_Total_Count = 0;
                                decimal Report_AdditionalFee = 0.00m;
                                for (int iRowLvl_ = 0; iRowLvl_ < ReportFee.Count; iRowLvl_++)
                                {
                                    dMVRAmt += ReportFee.ElementAt(iRowLvl_).AdditionalFee;
                                    ReportName_AddionalFee = "SCR State Fee"; //ReportFee.ElementAt(iRowLvl_).SCRStateCode;
                                    if (ReportFee.ElementAt(iRowLvl_).ReportFee > 0)
                                    {
                                        Reports_Total_Count += ReportFee.ElementAt(iRowLvl_).TotalReports;
                                        Report_AdditionalFee += ReportFee.ElementAt(iRowLvl_).AdditionalFee * ReportFee.ElementAt(iRowLvl_).TotalReports;
                                    }
                                }
                                if (ReportFee.Count > 0 && Report_AdditionalFee > 0.00m)
                                {
                                    Obj3.ProductDisplayName = ReportName_AddionalFee;
                                    Obj3.ProductQty = short.Parse(Reports_Total_Count.ToString());
                                    Obj3.ReportAmount = Report_AdditionalFee;
                                    Obj3.IsSubProduct = true;
                                    ObjBALReportsByLocation.Add(Obj3);
                                    //ObjPdfTblLocation.InsertTable(Get3rd_PartyFee_Table(ReportName_AddionalFee, Reports_Total_Count.ToString(), Report_AdditionalFee.ToString()));
                                }
                            }
                            if (ObjProducts.ElementAt(iRowLvl).ProductCode.ToLower() == "ccr1")
                            {
                                List<BALReportsFee> ReportFee = ObjReportFee.Where(t => t.fkLocationId == ObjProducts.ElementAt(iRowLvl).fkLocationId && t.ProductCode.ToLower() == "ccr1" && t.ReportAmount == ObjProducts.ElementAt(iRowLvl).ReportAmount).ToList();
                                BALReportsByLocation Obj3 = new BALReportsByLocation();
                                string ReportName_AddionalFee = "";
                                int Reports_Total_Count = 0;
                                decimal Report_AdditionalFee = 0.00m;
                                for (int iRowLvl_ = 0; iRowLvl_ < ReportFee.Count; iRowLvl_++)
                                {
                                    dMVRAmt += ReportFee.ElementAt(iRowLvl_).AdditionalFee;
                                    ReportName_AddionalFee = "CCR1 County Fee"; //ReportFee.ElementAt(iRowLvl_).CCR1CountyCode;
                                    if (ReportFee.ElementAt(iRowLvl_).ReportFee > 0)
                                    {
                                        Reports_Total_Count += ReportFee.ElementAt(iRowLvl_).TotalReports;
                                        Report_AdditionalFee += ReportFee.ElementAt(iRowLvl_).AdditionalFee * ReportFee.ElementAt(iRowLvl_).TotalReports;
                                    }
                                }
                                if (ReportFee.Count > 0 && Report_AdditionalFee > 0.00m)
                                {
                                    Obj3.ProductDisplayName = ReportName_AddionalFee;
                                    Obj3.ProductQty = short.Parse(Reports_Total_Count.ToString());
                                    Obj3.ReportAmount = Report_AdditionalFee;
                                    Obj3.IsSubProduct = true;
                                    ObjBALReportsByLocation.Add(Obj3);
                                    //ObjPdfTblLocation.InsertTable(Get3rd_PartyFee_Table(ReportName_AddionalFee, Reports_Total_Count.ToString(), Report_AdditionalFee.ToString()));
                                }
                            }
                            if (ObjProducts.ElementAt(iRowLvl).ProductCode.ToLower() == "ccr2")
                            {
                                List<BALReportsFee> ReportFee = ObjReportFee.Where(t => t.fkLocationId == ObjProducts.ElementAt(iRowLvl).fkLocationId && t.ProductCode.ToLower() == "ccr2" && t.ReportAmount == ObjProducts.ElementAt(iRowLvl).ReportAmount).ToList();
                                BALReportsByLocation Obj3 = new BALReportsByLocation();
                                string ReportName_AddionalFee = "";
                                int Reports_Total_Count = 0;
                                decimal Report_AdditionalFee = 0.00m;
                                for (int iRowLvl_ = 0; iRowLvl_ < ReportFee.Count; iRowLvl_++)
                                {
                                    dMVRAmt += ReportFee.ElementAt(iRowLvl_).AdditionalFee;
                                    ReportName_AddionalFee = "CCR2 County Fee"; //ReportFee.ElementAt(iRowLvl_).CCR2CountyCode;
                                    if (ReportFee.ElementAt(iRowLvl_).ReportFee > 0)
                                    {
                                        Reports_Total_Count += ReportFee.ElementAt(iRowLvl_).TotalReports;
                                        Report_AdditionalFee += ReportFee.ElementAt(iRowLvl_).AdditionalFee * ReportFee.ElementAt(iRowLvl_).TotalReports;
                                    }
                                }
                                if (ReportFee.Count > 0 && Report_AdditionalFee > 0.00m)
                                {
                                    Obj3.ProductDisplayName = ReportName_AddionalFee;
                                    Obj3.ProductQty = short.Parse(Reports_Total_Count.ToString());
                                    Obj3.ReportAmount = Report_AdditionalFee;
                                    Obj3.IsSubProduct = true;
                                    ObjBALReportsByLocation.Add(Obj3);
                                    // ObjPdfTblLocation.InsertTable(Get3rd_PartyFee_Table(ReportName_AddionalFee, Reports_Total_Count.ToString(), Report_AdditionalFee.ToString()));
                                }
                            }
                            if (ObjProducts.ElementAt(iRowLvl).ProductCode.ToLower() == "rcx")
                            {
                                List<BALReportsFee> ReportFee = ObjReportFee.Where(t => t.fkLocationId == ObjProducts.ElementAt(iRowLvl).fkLocationId && t.ProductCode.ToLower() == "rcx" && t.ReportAmount == ObjProducts.ElementAt(iRowLvl).ReportAmount).ToList();
                                BALReportsByLocation Obj3 = new BALReportsByLocation();
                                string ReportName_AddionalFee = "";
                                int Reports_Total_Count = 0;
                                decimal Report_AdditionalFee = 0.00m;
                                for (int iRowLvl_ = 0; iRowLvl_ < ReportFee.Count; iRowLvl_++)
                                {
                                    dMVRAmt += ReportFee.ElementAt(iRowLvl_).AdditionalFee;
                                    ReportName_AddionalFee = "RCX County Fee"; //ReportFee.ElementAt(iRowLvl_).RCXCountyCode;
                                    if (ReportFee.ElementAt(iRowLvl_).ReportFee > 0)
                                    {
                                        Reports_Total_Count += ReportFee.ElementAt(iRowLvl_).TotalReports;
                                        Report_AdditionalFee += ReportFee.ElementAt(iRowLvl_).AdditionalFee * ReportFee.ElementAt(iRowLvl_).TotalReports;
                                    }
                                }
                                if (ReportFee.Count > 0 && Report_AdditionalFee > 0.00m)
                                {
                                    Obj3.ProductDisplayName = ReportName_AddionalFee;
                                    Obj3.ProductQty = short.Parse(Reports_Total_Count.ToString());
                                    Obj3.ReportAmount = Report_AdditionalFee;
                                    Obj3.IsSubProduct = true;
                                    ObjBALReportsByLocation.Add(Obj3);
                                    //ObjPdfTblLocation.InsertTable(Get3rd_PartyFee_Table(ReportName_AddionalFee, Reports_Total_Count.ToString(), Report_AdditionalFee.ToString()));
                                }

                            }
                            if (ObjProducts.ElementAt(iRowLvl).ProductCode.ToLower() == "fcr")
                            {
                                List<BALReportsFee> ReportFee = ObjReportFee.Where(t => t.fkLocationId == ObjProducts.ElementAt(iRowLvl).fkLocationId && t.ProductCode.ToLower() == "fcr" && t.ReportAmount == ObjProducts.ElementAt(iRowLvl).ReportAmount).ToList();
                                BALReportsByLocation Obj3 = new BALReportsByLocation();

                                string ReportName_AddionalFee = "";
                                int Reports_Total_Count = 0;
                                decimal Report_AdditionalFee = 0.00m;
                                for (int iRowLvl_ = 0; iRowLvl_ < ReportFee.Count; iRowLvl_++)
                                {
                                    dMVRAmt += ReportFee.ElementAt(iRowLvl_).AdditionalFee;
                                    ReportName_AddionalFee = "Jurisdiction Fee"; //ReportFee.ElementAt(iRowLvl_).FCRCountyCode;
                                    {
                                        Reports_Total_Count += ReportFee.ElementAt(iRowLvl_).TotalReports;
                                        Report_AdditionalFee += ReportFee.ElementAt(iRowLvl_).AdditionalFee * ReportFee.ElementAt(iRowLvl_).TotalReports;
                                    }
                                }
                                if (ReportFee.Count > 0 && Report_AdditionalFee > 0.00m)
                                {
                                    Obj3.ProductDisplayName = ReportName_AddionalFee;
                                    Obj3.ProductQty = short.Parse(Reports_Total_Count.ToString());
                                    Obj3.ReportAmount = Report_AdditionalFee;
                                    Obj3.IsSubProduct = true;
                                    ObjBALReportsByLocation.Add(Obj3);
                                    // ObjPdfTblLocation.InsertTable(Get3rd_PartyFee_Table(ReportName_AddionalFee, Reports_Total_Count.ToString(), Report_AdditionalFee.ToString()));
                                }
                            }
                            if (ObjProducts.ElementAt(iRowLvl).ProductCode.ToLower() == "edv" || ObjProducts.ElementAt(iRowLvl).ProductCode.ToLower() == "emv" || ObjProducts.ElementAt(iRowLvl).ProductCode.ToLower() == "plv" || ObjProducts.ElementAt(iRowLvl).ProductCode.ToLower() == "wcv" || ObjProducts.ElementAt(iRowLvl).ProductCode.ToLower() == "prv")
                            {
                                string PCode = ObjProducts.ElementAt(iRowLvl).ProductCode.ToLower();
                                List<BALReportsFee> ReportFee = ObjReportFee.Where(t => t.fkLocationId == ObjProducts.ElementAt(iRowLvl).fkLocationId && t.ProductCode.ToLower() == PCode && t.ReportAmount == ObjProducts.ElementAt(iRowLvl).ReportAmount).ToList();
                                BALReportsByLocation Obj3 = new BALReportsByLocation();
                                string ReportName_AddionalFee = "";
                                int Reports_Total_Count = 0;
                                decimal Report_AdditionalFee = 0.00m;
                                for (int iRowLvl_ = 0; iRowLvl_ < ReportFee.Count; iRowLvl_++)
                                {
                                    dMVRAmt += ReportFee.ElementAt(iRowLvl_).AdditionalFee;
                                    ReportName_AddionalFee = "Third Party Fee";
                                    if (ReportFee.ElementAt(iRowLvl_).ReportFee > 0)
                                    {
                                        Reports_Total_Count += ReportFee.ElementAt(iRowLvl_).TotalReports;
                                        Report_AdditionalFee += ReportFee.ElementAt(iRowLvl_).AdditionalFee * ReportFee.ElementAt(iRowLvl_).TotalReports;
                                    }
                                }
                                if (ReportFee.Count > 0 && Report_AdditionalFee > 0.00m)
                                {
                                    Obj3.ProductDisplayName = ReportName_AddionalFee;
                                    Obj3.ProductQty = short.Parse(Reports_Total_Count.ToString());
                                    Obj3.ReportAmount = Report_AdditionalFee;
                                    Obj3.IsSubProduct = true;
                                    ObjBALReportsByLocation.Add(Obj3);
                                    //ObjPdfTblLocation.InsertTable(Get3rd_PartyFee_Table(ReportName_AddionalFee, Reports_Total_Count.ToString(), Report_AdditionalFee.ToString()));
                                }
                            }
                        }
                    }
                    #endregion

                    #region Package Details For Current Company
                    if (ObjPackagePrice.Count > 0)
                    {
                        for (int iRowLvl = 0; iRowLvl < ObjPackagePrice.Count; iRowLvl++)
                        {
                            BALReportsByLocation ObjPackage = new BALReportsByLocation();

                            ObjPackage.ProductDisplayName = ObjPackagePrice.ElementAt(iRowLvl).PackageName;
                            ObjPackage.ProductQty = short.Parse(ObjPackagePrice.ElementAt(iRowLvl).TotalPackages.ToString());
                            ObjPackage.ReportAmount = (ObjPackagePrice.ElementAt(iRowLvl).TotalPackages * ObjPackagePrice.ElementAt(iRowLvl).ReportAmount);
                            ObjPackage.TotalOrderedReports = ObjPackagePrice.ElementAt(iRowLvl).TotalPackages;
                            ObjPackage.IsPackage = true;
                            ObjPackage.IsSubProduct = false;
                            ObjBALReportsByLocation.Add(ObjPackage);
                            bool IsTieredStatus = false;
                            using (EmergeDALDataContext DX = new EmergeDALDataContext())
                            {
                                //var IsTieredStatus1 = DX.tblProductPackages.Join(DX.tblTieredPackages, PP => PP.pkPackageId, TP => TP.fkPackageId,
                                //                                                   (PP, TP) => new
                                //                                                   {
                                //                                                       PP,
                                //                                                       TP
                                //                                                   }).Where(n => n.PP.pkPackageId == ObjPackagePrice.ElementAt(iRowLvl).fkPackageId && n.TP.Tired2Price > 0).ToList();//Applied a check for the Tired2Price Data if Tired2Price>0 INT-110
                                //IsTieredStatus = IsTieredStatus1.Count > 0 ? true : IsTieredStatus;
                                var IsTieredStatus1 = DX.tblProductPackages.Join(DX.tblTieredPackages, PP => PP.pkPackageId, TP => TP.fkPackageId,
                                                                                   (PP, TP) => new
                                                                                   {
                                                                                       PP,
                                                                                       TP
                                                                                   }).Where(n => n.PP.pkPackageId == ObjPackagePrice.ElementAt(iRowLvl).fkPackageId).ToList();
                                IsTieredStatus = IsTieredStatus1.Count > 0 ? true : IsTieredStatus;







                            }

                            if (IsTieredStatus)
                            {
                                bool Is6Month = false;
                                DateTime OrderDate = DateTime.Parse(Month + "/1/" + Year + " 0:00:00 PM");
                                DateTime SixMonthOldDate = DateTime.Now.AddMonths(-6);
                                DateTime startOfMonth = new DateTime(SixMonthOldDate.Year, SixMonthOldDate.Month, 1);
                                bool MVRProcessed = false;
                                if (OrderDate < startOfMonth)
                                {
                                    Is6Month = true;
                                }
                                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                                {
                                    if (Is6Month == false)
                                    {
                                        //var Tiered2Reports = (DX.tblOrderDetails.Where(db => db.fkPackageId == ObjPackagePrice.ElementAt(iRowLvl).fkPackageId && db.IsTieredReport== true && Convert.ToDateTime(db.OrderDt).Month == Month && Convert.ToDateTime(db.OrderDt).Year == Year)).ToList();


                                        List<Billing_Tiered2Reports_procResult> Tiered2Reports = null;
                                        using (EmergeDALDataContext DX1 = new EmergeDALDataContext())
                                        {

                                            Tiered2Reports = DX1.Billing_Tiered2Reports_proc(ObjPackagePrice.ElementAt(iRowLvl).fkPackageId, Month, Year, Convert.ToInt32(ObjBALCompanyLocationsForOthers[iRow].pkLocationId)).ToList();

                                        }




                                        List<Billing_TieredOptionalReports_procResult> TieredOptionalReports = null;
                                        using (EmergeDALDataContext DX2 = new EmergeDALDataContext())
                                        {

                                            TieredOptionalReports = DX2.Billing_TieredOptionalReports_proc(ObjPackagePrice.ElementAt(iRowLvl).fkPackageId, Month, Year, Convert.ToInt32(ObjBALCompanyLocationsForOthers[iRow].pkLocationId)).ToList();

                                        }




                                        //  var TieredOptionalReports = DX.tblOrderDetails.Where(db => db.fkPackageId == ObjPackagePrice.ElementAt(iRowLvl).fkPackageId && db.IsOptionalReport == true && Convert.ToDateTime(db.OrderDt).Month == Month && Convert.ToDateTime(db.OrderDt).Year == Year).ToList();
                                        if (Tiered2Reports != null && Tiered2Reports.Count() > 0)
                                        {
                                            BALReportsByLocation ObjTieredProduct = new BALReportsByLocation();
                                            ObjTieredProduct.TotalReports = Tiered2Reports.Count();
                                            if (Tiered2Reports.ElementAt(0).fkproductapplicationid == CCR1)
                                            {
                                                ObjTieredProduct.ProductDisplayName = "Tier2 CCR1";
                                                ObjTieredProduct.ProductQty = short.Parse(Tiered2Reports.Where(x => x.fkproductapplicationid == 39).Select(x => x.fkorderid).Count().ToString());

                                            }
                                            else if (Tiered2Reports.ElementAt(0).fkproductapplicationid == SCR)
                                            {
                                                ObjTieredProduct.ProductDisplayName = "Tier2 SCR";
                                                ObjTieredProduct.ProductQty = short.Parse(Tiered2Reports.Where(x => x.fkproductapplicationid == 44).Select(x => x.fkorderid).Count().ToString());
                                            }
                                            else if (Tiered2Reports.ElementAt(0).fkproductapplicationid == FCR)
                                            {
                                                ObjTieredProduct.ProductDisplayName = "Tier2 FCR";
                                                ObjTieredProduct.ProductQty = short.Parse(Tiered2Reports.Where(x => x.fkproductapplicationid == 47).Select(x => x.fkorderid).Count().ToString());
                                            }
                                            else
                                            {
                                                ObjTieredProduct.ProductDisplayName = "Tier2";
                                                ObjTieredProduct.ProductQty = short.Parse(Tiered2Reports.Count().ToString());
                                            }


                                            ObjTieredProduct.ReportAmount = Tiered2Reports.Count() * (Tiered2Reports.ElementAt(0).ReportAmount == null ? decimal.Parse("0") : decimal.Parse(Tiered2Reports.ElementAt(0).ReportAmount.ToString()));
                                            //ObjTieredProduct.TotalReportAmount = Tiered2Reports.Count() * (Tiered2Reports.ElementAt(0).ReportAmount == null ? decimal.Parse("0") : decimal.Parse(Tiered2Reports.ElementAt(0).ReportAmount.ToString()));
                                            ObjTieredProduct.IsSubProduct = true;
                                            ObjTieredProduct.IsTieredIcon = false;
                                            ObjBALReportsByLocation.Add(ObjTieredProduct);

                                            List<BillingStatment_AdditionalFeesValueResult> listProductWithFee = null;
                                            using (EmergeDALDataContext DX1 = new EmergeDALDataContext())
                                            {


                                                listProductWithFee = DX1.BillingStatment_AdditionalFeesValue(Tiered2Reports.ElementAt(0).fkcompanyid.Value, Tiered2Reports.ElementAt(0).OrderDt.Value.Month, Tiered2Reports.ElementAt(0).OrderDt.Value.Year, Tiered2Reports.ElementAt(0).fkpackageid, Tiered2Reports.ElementAt(0).fklocationid.Value, Tiered2Reports.ElementAt(0).fkproductapplicationid.Value, RefCode, 0).ToList();

                                            }


                                            //var listProductWithFee = Tiered2Reports.GroupBy(db => new { db.CountyCode, db.StateCode, db.AdditionalFee, db.fkProductApplicationId }).Select(group =>
                                            //                                                                            new
                                            //                                                                            {
                                            //                                                                                CountyCode = group.Key.CountyCode,
                                            //                                                                                StateCode = group.Key.StateCode,
                                            //                                                                                AdditionalFee = group.Key.AdditionalFee,
                                            //                                                                                fkProductApplicationId = group.Key.fkProductApplicationId,
                                            //                                                                                Count = group.Count()
                                            //                                                                            });

                                            int PKGTotalReports = 0;
                                            decimal PKGAdditionalFee = 0.00m;
                                            bool IsMVRCheck = false;
                                            BALReportsByLocation ObjSubProductMVR = new BALReportsByLocation();
                                            for (int i = 0; i < listProductWithFee.Count(); i++)
                                            {
                                                if (Convert.ToDecimal(listProductWithFee[i].additionalfee.Value) > 0.00m)
                                                {
                                                    BALReportsByLocation ObjSubProduct = new BALReportsByLocation();

                                                    string ProductAddionalFee = "";
                                                    if (listProductWithFee.ElementAt(i).fkproductapplicationid == CCR1)
                                                    {
                                                        IsMVRCheck = false;
                                                        ProductAddionalFee = "CCR1 County Fee (" + listProductWithFee[i].countycode + ")";
                                                    }
                                                    if (listProductWithFee.ElementAt(i).fkproductapplicationid == SCR)
                                                    {
                                                        IsMVRCheck = false;
                                                        ProductAddionalFee = "SCR State Fee (" + listProductWithFee[i].statecode + ")";
                                                    }
                                                    if (listProductWithFee.ElementAt(i).fkproductapplicationid == FCR)
                                                    {
                                                        IsMVRCheck = false;
                                                        ProductAddionalFee = "Jurisdiction Fee (" + listProductWithFee[i].countycode + ")";
                                                    }
                                                    if (listProductWithFee.ElementAt(i).fkproductapplicationid == 27)
                                                    {
                                                        ProductAddionalFee = "MVR State Fee";
                                                        IsMVRCheck = true;
                                                        ObjSubProductMVR.TotalReports = 0;
                                                        PKGTotalReports += short.Parse(listProductWithFee[i].count.ToString());
                                                        PKGAdditionalFee += Convert.ToInt32(listProductWithFee[i].count) * Convert.ToDecimal(listProductWithFee[i].additionalfee.Value);
                                                        ObjSubProductMVR.IsSubProduct = true;
                                                        ObjSubProductMVR.IsTieredIcon = false;
                                                    }

                                                    if (!IsMVRCheck)
                                                    {
                                                        // changes ND-37
                                                        ObjSubProduct.ProductDisplayName = ProductAddionalFee;
                                                        ObjSubProduct.TotalReports = Convert.ToInt32(listProductWithFee[i].count);
                                                        ObjSubProduct.ProductQty = short.Parse(listProductWithFee[i].count.ToString());
                                                        ObjSubProduct.ReportAmount = Convert.ToInt32(listProductWithFee[i].count) * Convert.ToDecimal(listProductWithFee[i].additionalfee.Value);
                                                        //ObjSubProduct.ReportAmount = 0.00m;
                                                        //ObjSubProduct.TotalReportAmount = listProductWithFee.ElementAt(i).Count * listProductWithFee.ElementAt(i).AdditionalFee;
                                                        ObjSubProduct.IsSubProduct = true;
                                                        ObjSubProduct.IsTieredIcon = false;
                                                        ObjBALReportsByLocation.Add(ObjSubProduct);
                                                    }
                                                }
                                            }
                                            if (IsMVRCheck)
                                            {
                                                ObjSubProductMVR.ProductDisplayName = "MVR State Fee";
                                                ObjSubProductMVR.ProductQty = short.Parse(PKGTotalReports.ToString());
                                                ObjSubProductMVR.ReportAmount = PKGAdditionalFee;
                                                ObjBALReportsByLocation.Add(ObjSubProductMVR);
                                                IsMVRCheck = false;
                                                MVRProcessed = true;

                                            }
                                        }
                                        if (TieredOptionalReports != null && TieredOptionalReports.Count() > 0)
                                        {
                                            BALReportsByLocation ObjTieredOptionalProduct = new BALReportsByLocation();
                                            if (TieredOptionalReports.ElementAt(0).fkproductapplicationid == CCR1)
                                            {
                                                ObjTieredOptionalProduct.ProductDisplayName = "Optional CCR1";
                                            }
                                            else if (TieredOptionalReports.ElementAt(0).fkproductapplicationid == SCR)
                                            {
                                                ObjTieredOptionalProduct.ProductDisplayName = "Optional SCR";
                                            }
                                            else if (TieredOptionalReports.ElementAt(0).fkproductapplicationid == FCR)
                                            {
                                                ObjTieredOptionalProduct.ProductDisplayName = "Optional FCR";
                                            }
                                            else
                                            {
                                                ObjTieredOptionalProduct.ProductDisplayName = "Optional";
                                            }
                                            ObjTieredOptionalProduct.TotalReports = TieredOptionalReports.Count();
                                            ObjTieredOptionalProduct.ProductQty = short.Parse(TieredOptionalReports.Count().ToString());
                                            ObjTieredOptionalProduct.ReportAmount = TieredOptionalReports.Count() * (TieredOptionalReports.ElementAt(0).ReportAmount == null ? decimal.Parse("0") : decimal.Parse(TieredOptionalReports.ElementAt(0).ReportAmount.ToString()));
                                            //ObjTieredOptionalProduct.TotalReportAmount = TieredOptionalReports.Count() * (TieredOptionalReports.ElementAt(0).ReportAmount == null ? decimal.Parse("0") : decimal.Parse(TieredOptionalReports.ElementAt(0).ReportAmount.ToString()));
                                            ObjTieredOptionalProduct.IsSubProduct = true;
                                            ObjTieredOptionalProduct.IsTieredIcon = false;
                                            ObjBALReportsByLocation.Add(ObjTieredOptionalProduct);
                                            List<BillingStatment_AdditionalFeesValueResult> listProductWithFee = null;
                                            using (EmergeDALDataContext DX1 = new EmergeDALDataContext())
                                            {

                                                listProductWithFee = DX1.BillingStatment_AdditionalFeesValue(Tiered2Reports.ElementAt(0).fkcompanyid.Value, Tiered2Reports.ElementAt(0).OrderDt.Value.Month, Tiered2Reports.ElementAt(0).OrderDt.Value.Year, Tiered2Reports.ElementAt(0).fkpackageid, Tiered2Reports.ElementAt(0).fklocationid.Value, Tiered2Reports.ElementAt(0).fkproductapplicationid.Value, RefCode, 1).ToList();

                                            }

                                            //var listProductWithFee = TieredOptionalReports.GroupBy(db => new { db.CountyCode, db.StateCode, db.AdditionalFee, db.fkProductApplicationId }).Select(group =>
                                            //                                                                            new
                                            //                                                                            {
                                            //                                                                                CountyCode = group.Key.CountyCode,
                                            //                                                                                StateCode = group.Key.StateCode,
                                            //                                                                                AdditionalFee = group.Key.AdditionalFee,
                                            //                                                                                fkProductApplicationId = group.Key.fkProductApplicationId,
                                            //                                                                                Count = group.Count()
                                            //                                                                            });
                                            for (int i = 0; i < listProductWithFee.Count(); i++)
                                            {
                                                if (Convert.ToDecimal(listProductWithFee[i].additionalfee.Value) > 0.00m)
                                                {
                                                    BALReportsByLocation ObjSubProduct = new BALReportsByLocation();

                                                    string ProductAddionalFee = "";
                                                    if (listProductWithFee.ElementAt(i).fkproductapplicationid == CCR1)
                                                    {
                                                        ProductAddionalFee = "CCR1 County Fee (" + listProductWithFee[i].countycode + ")";
                                                    }
                                                    if (listProductWithFee.ElementAt(i).fkproductapplicationid == SCR)
                                                    {
                                                        ProductAddionalFee = "SCR State Fee (" + listProductWithFee[i].statecode + ")";
                                                    }
                                                    if (listProductWithFee.ElementAt(i).fkproductapplicationid == FCR)
                                                    {
                                                        ProductAddionalFee = "Jurisdiction Fee (" + listProductWithFee[i].countycode + ")";
                                                    }

                                                    ObjSubProduct.ProductDisplayName = ProductAddionalFee;
                                                    ObjSubProduct.TotalReports = Convert.ToInt32(listProductWithFee[i].count);
                                                    ObjSubProduct.ProductQty = short.Parse(listProductWithFee[i].count.ToString());
                                                    ObjSubProduct.ReportAmount = Convert.ToInt32(listProductWithFee[i].count) * Convert.ToDecimal(listProductWithFee[i].additionalfee.Value);//ObjSubProduct.TotalReportAmount = listProductWithFee.ElementAt(i).Count * listProductWithFee.ElementAt(i).AdditionalFee;
                                                    ObjSubProduct.IsSubProduct = true;
                                                    ObjSubProduct.IsTieredIcon = false;
                                                    ObjBALReportsByLocation.Add(ObjSubProduct);
                                                }
                                            }
                                        }
                                        if (!MVRProcessed)
                                        {
                                            List<BillingStatment_AdditionalFeesValueResult> listProductWithFeeMVR = null;
                                            using (EmergeDALDataContext DX1 = new EmergeDALDataContext())
                                            {
                                                listProductWithFeeMVR = DX1.BillingStatment_AdditionalFeesValue(Convert.ToInt32(ObjBALCompanyLocationsForOthers[iRow].pkCompanyId), Month, Year, ObjPackagePrice.ElementAt(iRowLvl).fkPackageId, Convert.ToInt32(ObjBALCompanyLocationsForOthers[iRow].pkLocationId), 27, RefCode, 0).ToList();
                                                if (listProductWithFeeMVR.Count() > 0)
                                                {
                                                    int PKGTotalReportsNonTiered = 0;
                                                    decimal PKGAdditionalFeeNonTiered = 0.00m;
                                                    BALReportsByLocation ObjSubProductNonTieredMVR = new BALReportsByLocation();
                                                    for (int i = 0; i < listProductWithFeeMVR.Count(); i++)
                                                    {
                                                        if (listProductWithFeeMVR.ElementAt(i).fkproductapplicationid == 27)
                                                        {

                                                            ObjSubProductNonTieredMVR.TotalReports = 0;
                                                            PKGTotalReportsNonTiered += short.Parse(listProductWithFeeMVR[i].count.ToString());
                                                            PKGAdditionalFeeNonTiered += Convert.ToInt32(listProductWithFeeMVR[i].count) * Convert.ToDecimal(listProductWithFeeMVR[i].additionalfee.Value);

                                                        }
                                                    }
                                                    if (listProductWithFeeMVR.Count() > 0 && PKGAdditionalFeeNonTiered > 0.00m)
                                                    {
                                                        ObjSubProductNonTieredMVR.ProductDisplayName = "MVR State Fee";
                                                        ObjSubProductNonTieredMVR.ProductQty = short.Parse(PKGTotalReportsNonTiered.ToString());
                                                        ObjSubProductNonTieredMVR.TotalOrderedReports = 0;
                                                        ObjSubProductNonTieredMVR.IsPackage = true;
                                                        ObjSubProductNonTieredMVR.ReportAmount = PKGAdditionalFeeNonTiered;
                                                        ObjSubProductNonTieredMVR.IsSubProduct = true;
                                                        ObjBALReportsByLocation.Add(ObjSubProductNonTieredMVR);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        var Tiered2Reports = DX.tblOrderDetails_Archieves.Where(db => db.fkPackageId == ObjPackagePrice.ElementAt(iRowLvl).fkPackageId && db.IsTieredReport == true && Convert.ToDateTime(db.OrderDt).Month == Month && Convert.ToDateTime(db.OrderDt).Year == Year).ToList();
                                        var TieredOptionalReports = DX.tblOrderDetails_Archieves.Where(db => db.fkPackageId == ObjPackagePrice.ElementAt(iRowLvl).fkPackageId && db.IsOptionalReport == true && Convert.ToDateTime(db.OrderDt).Month == Month && Convert.ToDateTime(db.OrderDt).Year == Year).ToList();
                                        if (Tiered2Reports != null && Tiered2Reports.Count() > 0)
                                        {
                                            BALReportsByLocation ObjTieredProduct = new BALReportsByLocation();
                                            if (Tiered2Reports.ElementAt(0).fkProductApplicationId == CCR1)
                                            {
                                                ObjTieredProduct.ProductDisplayName = "Tier2 CCR1";
                                            }
                                            else if (Tiered2Reports.ElementAt(0).fkProductApplicationId == SCR)
                                            {
                                                ObjTieredProduct.ProductDisplayName = "Tier2 SCR";
                                            }
                                            else if (Tiered2Reports.ElementAt(0).fkProductApplicationId == FCR)
                                            {
                                                ObjTieredProduct.ProductDisplayName = "Tier2 FCR";
                                            }
                                            else
                                            {
                                                ObjTieredProduct.ProductDisplayName = "Tier2";
                                            }
                                            ObjTieredProduct.TotalReports = Tiered2Reports.Count();
                                            ObjTieredProduct.ProductQty = short.Parse(Tiered2Reports.Count().ToString());
                                            ObjTieredProduct.ReportAmount = Tiered2Reports.Count() * (Tiered2Reports.ElementAt(0).ReportAmount == null ? decimal.Parse("0") : decimal.Parse(Tiered2Reports.ElementAt(0).ReportAmount.ToString()));
                                            //ObjTieredProduct.TotalReportAmount = Tiered2Reports.Count() * (Tiered2Reports.ElementAt(0).ReportAmount == null ? decimal.Parse("0") : decimal.Parse(Tiered2Reports.ElementAt(0).ReportAmount.ToString()));
                                            ObjTieredProduct.IsSubProduct = true;
                                            ObjTieredProduct.IsTieredIcon = false;
                                            ObjBALReportsByLocation.Add(ObjTieredProduct);
                                            var listProductWithFee = Tiered2Reports.GroupBy(db => new { db.CountyCode, db.StateCode, db.AdditionalFee, db.fkProductApplicationId }).Select(group =>
                                                                                                                        new
                                                                                                                        {
                                                                                                                            CountyCode = group.Key.CountyCode,
                                                                                                                            StateCode = group.Key.StateCode,
                                                                                                                            AdditionalFee = group.Key.AdditionalFee,
                                                                                                                            fkProductApplicationId = group.Key.fkProductApplicationId,
                                                                                                                            Count = group.Count()
                                                                                                                        });
                                            for (int i = 0; i < listProductWithFee.Count(); i++)
                                            {
                                                if (Convert.ToDecimal(listProductWithFee.ElementAt(i).AdditionalFee) > 0.00m)
                                                {
                                                    BALReportsByLocation ObjSubProduct = new BALReportsByLocation();

                                                    string ProductAddionalFee = "";
                                                    if (listProductWithFee.ElementAt(i).fkProductApplicationId == CCR1)
                                                    {
                                                        ProductAddionalFee = "CCR1 County Fee (" + listProductWithFee.ElementAt(i).CountyCode + ")";
                                                    }
                                                    if (listProductWithFee.ElementAt(i).fkProductApplicationId == SCR)
                                                    {
                                                        ProductAddionalFee = "SCR State Fee (" + listProductWithFee.ElementAt(i).StateCode + ")";
                                                    }
                                                    if (listProductWithFee.ElementAt(i).fkProductApplicationId == FCR)
                                                    {
                                                        ProductAddionalFee = "Jurisdiction Fee (" + listProductWithFee.ElementAt(i).CountyCode + ")";
                                                    }

                                                    ObjSubProduct.ProductDisplayName = ProductAddionalFee;
                                                    ObjSubProduct.TotalReports = listProductWithFee.ElementAt(i).Count;
                                                    ObjSubProduct.ProductQty = short.Parse(listProductWithFee.ElementAt(i).Count.ToString());
                                                    ObjSubProduct.ReportAmount = listProductWithFee.ElementAt(i).Count * listProductWithFee.ElementAt(i).AdditionalFee;
                                                    //ObjSubProduct.TotalReportAmount = listProductWithFee.ElementAt(i).Count * listProductWithFee.ElementAt(i).AdditionalFee;
                                                    ObjSubProduct.IsSubProduct = true;
                                                    ObjSubProduct.IsTieredIcon = false;
                                                    ObjBALReportsByLocation.Add(ObjSubProduct);
                                                }
                                            }
                                        }
                                        if (TieredOptionalReports != null && TieredOptionalReports.Count() > 0)
                                        {
                                            BALReportsByLocation ObjTieredOptionalProduct = new BALReportsByLocation();
                                            if (TieredOptionalReports.ElementAt(0).fkProductApplicationId == CCR1)
                                            {
                                                ObjTieredOptionalProduct.ProductDisplayName = "Optional CCR1";
                                            }
                                            else if (TieredOptionalReports.ElementAt(0).fkProductApplicationId == SCR)
                                            {
                                                ObjTieredOptionalProduct.ProductDisplayName = "Optional SCR";
                                            }
                                            else if (TieredOptionalReports.ElementAt(0).fkProductApplicationId == FCR)
                                            {
                                                ObjTieredOptionalProduct.ProductDisplayName = "Optional FCR";
                                            }
                                            else
                                            {
                                                ObjTieredOptionalProduct.ProductDisplayName = "Optional";
                                            }
                                            ObjTieredOptionalProduct.TotalReports = TieredOptionalReports.Count();
                                            ObjTieredOptionalProduct.ProductQty = short.Parse(TieredOptionalReports.Count().ToString());
                                            ObjTieredOptionalProduct.ReportAmount = TieredOptionalReports.Count() * (TieredOptionalReports.ElementAt(0).ReportAmount == null ? decimal.Parse("0") : decimal.Parse(TieredOptionalReports.ElementAt(0).ReportAmount.ToString()));
                                            //ObjTieredOptionalProduct.TotalReportAmount = TieredOptionalReports.Count() * (TieredOptionalReports.ElementAt(0).ReportAmount == null ? decimal.Parse("0") : decimal.Parse(TieredOptionalReports.ElementAt(0).ReportAmount.ToString()));
                                            ObjTieredOptionalProduct.IsSubProduct = true;
                                            ObjTieredOptionalProduct.IsTieredIcon = false;
                                            ObjBALReportsByLocation.Add(ObjTieredOptionalProduct);
                                            var listProductWithFee = TieredOptionalReports.GroupBy(db => new { db.CountyCode, db.StateCode, db.AdditionalFee, db.fkProductApplicationId }).Select(group =>
                                                                                                                        new
                                                                                                                        {
                                                                                                                            CountyCode = group.Key.CountyCode,
                                                                                                                            StateCode = group.Key.StateCode,
                                                                                                                            AdditionalFee = group.Key.AdditionalFee,
                                                                                                                            fkProductApplicationId = group.Key.fkProductApplicationId,
                                                                                                                            Count = group.Count()
                                                                                                                        });
                                            for (int i = 0; i < listProductWithFee.Count(); i++)
                                            {
                                                if (Convert.ToDecimal(listProductWithFee.ElementAt(i).AdditionalFee) > 0.00m)
                                                {
                                                    BALReportsByLocation ObjSubProduct = new BALReportsByLocation();

                                                    string ProductAddionalFee = "";
                                                    if (listProductWithFee.ElementAt(i).fkProductApplicationId == CCR1)
                                                    {
                                                        ProductAddionalFee = "CCR1 County Fee (" + listProductWithFee.ElementAt(i).CountyCode + ")";
                                                    }
                                                    if (listProductWithFee.ElementAt(i).fkProductApplicationId == SCR)
                                                    {
                                                        ProductAddionalFee = "SCR State Fee (" + listProductWithFee.ElementAt(i).StateCode + ")";
                                                    }
                                                    if (listProductWithFee.ElementAt(i).fkProductApplicationId == FCR)
                                                    {
                                                        ProductAddionalFee = "Jurisdiction Fee (" + listProductWithFee.ElementAt(i).CountyCode + ")";
                                                    }

                                                    ObjSubProduct.ProductDisplayName = ProductAddionalFee;
                                                    ObjSubProduct.TotalReports = listProductWithFee.ElementAt(i).Count;
                                                    ObjSubProduct.ProductQty = short.Parse(listProductWithFee.ElementAt(i).Count.ToString());
                                                    ObjSubProduct.ReportAmount = listProductWithFee.ElementAt(i).Count * listProductWithFee.ElementAt(i).AdditionalFee;
                                                    //ObjSubProduct.TotalReportAmount = listProductWithFee.ElementAt(i).Count * listProductWithFee.ElementAt(i).AdditionalFee;
                                                    ObjSubProduct.IsSubProduct = true;
                                                    ObjSubProduct.IsTieredIcon = false;
                                                    ObjBALReportsByLocation.Add(ObjSubProduct);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (IsTieredStatus == false)
                            {
                                string ProductsInPackage = ObjPackagePrice.ElementAt(iRowLvl).PackageInclude;
                                string[] TotalProducts = ProductsInPackage.Split(',');
                                foreach (string ProductCode in TotalProducts)
                                {
                                    BALReportsByLocation ObjSubPackage = new BALReportsByLocation();
                                    if (ProductCode.ToLower().Trim() == "edv" || ProductCode.ToLower().Trim() == "emv" || ProductCode.ToLower().Trim() == "mvr" || ProductCode.ToLower().Trim() == "scr" || ProductCode.ToLower().Trim() == "fcr" || ProductCode.ToLower().Trim() == "rcx" || ProductCode.ToLower().Trim() == "ccr1" || ProductCode.ToLower().Trim() == "ccr2")
                                    {
                                        List<BALPackageReport> ObjPReports = ObjPackageReport.Where(d => d.ProductCode == ProductCode.Trim() && d.fkLocationId == ObjBALCompanyLocationsForOthers.ElementAt(iRow).pkLocationId && d.fkPackageId == ObjPackagePrice.ElementAt(iRowLvl).fkPackageId && d.PackageInclude == ObjPackagePrice.ElementAt(iRowLvl).PackageInclude).ToList();
                                        int PKGTotalReports = 0;
                                        decimal PKGAdditionalFee = 0.00m;
                                        string ProductAddionalFee = "";
                                        for (int iCol = 0; iCol < ObjPReports.Count(); iCol++)
                                        {
                                            if (ProductCode.ToLower().Trim() == "mvr")
                                            {
                                                ProductAddionalFee = "MVR State Fee";//ProductAddionalFee = ObjPReports.ElementAt(iCol).MVRStateCode;
                                            }
                                            if (ProductCode.ToLower().Trim() == "scr")
                                            {
                                                ProductAddionalFee = "SCR State Fee";// ProductAddionalFee = ObjPReports.ElementAt(iCol).SCRStateCode;
                                            }
                                            if (ProductCode.ToLower().Trim() == "fcr")
                                            {
                                                ProductAddionalFee = "Jurisdiction Fee";//ProductAddionalFee = ObjPReports.ElementAt(iCol).FCRCountyCode;
                                            }
                                            if (ProductCode.ToLower().Trim() == "ccr1")
                                            {
                                                ProductAddionalFee = "CCR1 County Fee"; // ProductAddionalFee = ObjPReports.ElementAt(iCol).CCR1CountyCode;
                                            }
                                            if (ProductCode.ToLower().Trim() == "ccr2")
                                            {
                                                ProductAddionalFee = "CCR2 County Fee"; //ProductAddionalFee = ObjPReports.ElementAt(iCol).CCR2CountyCode;  
                                            }
                                            if (ProductCode.ToLower().Trim() == "edv")
                                            {
                                                ProductAddionalFee = "Third Party Fee(edv)";
                                            }
                                            if (ProductCode.ToLower().Trim() == "emv")
                                            {
                                                ProductAddionalFee = "Third Party Fee(emv)";
                                            }
                                            if (Convert.ToDecimal(ObjPReports.ElementAt(iCol).TotalReports * ObjPReports.ElementAt(iCol).AdditionalFee) > 0.00m)
                                            {
                                                PKGTotalReports += ObjPReports.ElementAt(iCol).TotalReports;
                                                PKGAdditionalFee += (ObjPReports.ElementAt(iCol).AdditionalFee * ObjPReports.ElementAt(iCol).TotalReports);
                                            }
                                        }
                                        if (ObjPReports.Count() > 0 && PKGAdditionalFee > 0.00m)
                                        {
                                            ObjSubPackage.ProductDisplayName = ProductAddionalFee;
                                            ObjSubPackage.ProductQty = short.Parse(PKGTotalReports.ToString());
                                            ObjSubPackage.TotalOrderedReports = 0;
                                            ObjSubPackage.IsPackage = true;
                                            ObjSubPackage.ReportAmount = PKGAdditionalFee;
                                            ObjSubPackage.IsSubProduct = true;
                                            ObjBALReportsByLocation.Add(ObjSubPackage);
                                        }
                                    }

                                }
                            }

                        }
                    }
                    #endregion

                    #region Drug Testing Products For Company Locations
                    ObjProducts = ObjBALProductsPerLocation.Where(t => t.fkLocationId == ObjBALCompanyLocationsForOthers.ElementAt(iRow).pkLocationId && t.fkReportCategoryId == (byte)ReportCategories.DrugTesting).ToList();

                    if (ObjProducts.Count > 0)
                    {

                        for (int iRowLvl = 0; iRowLvl < ObjProducts.Count; iRowLvl++)
                        {
                            BALReportsByLocation ObjDrugTesting = new BALReportsByLocation();
                            decimal TotalReportAmount = ObjProducts.ElementAt(iRowLvl).TotalReports * ObjProducts.ElementAt(iRowLvl).ReportAmount;

                            ObjDrugTesting.ProductDisplayName = ObjProducts.ElementAt(iRowLvl).ProductDisplayName;
                            ObjDrugTesting.ProductQty = short.Parse(ObjProducts.ElementAt(iRowLvl).TotalReports.ToString());
                            ObjDrugTesting.ReportAmount = TotalReportAmount;
                            ObjDrugTesting.IsDrugTestingProducts = true;
                            ObjDrugTesting.TotalOrderedReports = ObjProducts.ElementAt(iRowLvl).TotalReports;
                            ObjBALReportsByLocation.Add(ObjDrugTesting);

                            string PCode = ObjProducts.ElementAt(iRowLvl).ProductCode.ToLower();
                            List<BALReportsFee> ReportFee = ObjReportFee.Where(t => t.fkLocationId == ObjProducts.ElementAt(iRowLvl).fkLocationId && t.ProductCode.ToLower() == PCode && t.ReportAmount == ObjProducts.ElementAt(iRowLvl).ReportAmount).ToList();
                            BALReportsByLocation Obj3 = new BALReportsByLocation();
                            string ReportName_AddionalFee = "";
                            int Reports_Total_Count = 0;
                            decimal Report_AdditionalFee = 0.00m;

                            for (int iRowLvl_ = 0; iRowLvl_ < ReportFee.Count; iRowLvl_++)
                            {
                                if (ReportFee.ElementAt(iRowLvl_).AdditionalFee > 0)
                                {
                                    dMVRAmt += ReportFee.ElementAt(iRowLvl_).AdditionalFee;
                                    ReportName_AddionalFee = "Shipping Cost";
                                    Reports_Total_Count += ReportFee.ElementAt(iRowLvl_).TotalReports;
                                    Report_AdditionalFee += ReportFee.ElementAt(iRowLvl_).AdditionalFee;
                                }
                            }
                            if (Report_AdditionalFee > 0)
                            {
                                Obj3.ProductDisplayName = ReportName_AddionalFee;
                                Obj3.ProductQty = short.Parse(Reports_Total_Count.ToString());
                                Obj3.ReportAmount = Report_AdditionalFee;
                                Obj3.IsSubProduct = true;
                                ObjBALReportsByLocation.Add(Obj3);
                            }
                            //ObjPdfTblLocation.InsertTable(Get3rd_PartyFee_Table(ReportName_AddionalFee, Reports_Total_Count.ToString(), Report_AdditionalFee.ToString()));

                        }
                    }
                    #endregion
                    #region discountorder test
                    List<BALProductsPerLocation> ObjProducts_Discount = ObjBALProductsPerLocation.Where(t => t.fkLocationId == ObjBALCompanyLocationsForOthers.ElementAt(iRow).pkLocationId && t.fkReportCategoryId != (byte)ReportCategories.DrugTesting).ToList();


                    if (ObjProducts_Discount.Count > 0)
                    {
                        using (EmergeDALDataContext DX = new EmergeDALDataContext())
                        {

                            var InvoiceDiscount = DX.InvoiceDiscountOrAdjustmentAmount(Convert.ToInt32(ObjBALCompanyLocationsForOthers.ElementAt(iRow).pkCompanyId), Month, Year, 0, "Discount").FirstOrDefault();

                            if (InvoiceDiscount.DiscountAmount != null)
                            {
                                BALReportsByLocation Obj3 = new BALReportsByLocation();
                                string ReportName_AddionalFee = "Discount";
                                int Reports_Total_Count = 1;
                                decimal Report_AdditionalFee = Convert.ToDecimal("-" + InvoiceDiscount.DiscountAmount);

                                Obj3.ProductDisplayName = ReportName_AddionalFee;
                                Obj3.ProductQty = short.Parse(Reports_Total_Count.ToString());
                                Obj3.ReportAmount = Report_AdditionalFee;
                                Obj3.IsSubProduct = true;
                                ObjBALReportsByLocation.Add(Obj3);


                            }


                        }
                        using (EmergeDALDataContext DX = new EmergeDALDataContext())
                        {
                            //return DX.Get_CompanyLocationsByCompanyId(CompanyId, ApplicationId).ToList<Proc_Get_CompanyLocationsByCompanyIdResult>();


                            var InvoiceAdjustment = DX.InvoiceDiscountOrAdjustmentAmount(Convert.ToInt32(ObjBALCompanyLocationsForOthers.ElementAt(iRow).pkCompanyId), Month, Year, 0, "Adjustment").FirstOrDefault();
                            if (InvoiceAdjustment.DiscountAmount != null)
                            {
                                BALReportsByLocation Obj3 = new BALReportsByLocation();
                                string ReportName_AddionalFee = "Adjustment";
                                int Reports_Total_Count = 1;
                                decimal Report_AdditionalFee = Convert.ToDecimal(InvoiceAdjustment.DiscountAmount);


                                Obj3.ProductDisplayName = ReportName_AddionalFee;
                                Obj3.ProductQty = short.Parse(Reports_Total_Count.ToString());
                                Obj3.ReportAmount = Report_AdditionalFee;
                                Obj3.IsSubProduct = true;
                                ObjBALReportsByLocation.Add(Obj3);

                            }



                        }


                    }



                    #endregion









                }
            }
            if (ObjBALReportsByLocation.Count == 0)
            {

                BALReportsByLocation Obj3 = new BALReportsByLocation();
                Obj3.ProductDisplayName = null;
                Obj3.ProductQty = -1;
                Obj3.ReportAmount = 0;
                ObjBALReportsByLocation.Add(Obj3);
            }
            return ObjBALReportsByLocation;
        }





        public List<clsSavedReports> GetSavedReports_Billing(int ProductId, int CompanyUserId, int CompanyId,
                                                                      int LocationId, string SortingColumn, string SortingDirection,
                                                                      bool AllowPaging, int PageNum, int PageSize,
                                                                      string FromDate, string ToDate, string SearchKeyword, byte OrderStatus, string ReportStatus, int RefCode)
        {
            List<clsSavedReports> listclsSavedReports = new List<clsSavedReports>();
            using (BALBilling DX = new BALBilling())
            {

                var data = DX.GetSavedReports_Billing(ProductId, CompanyUserId, CompanyId,
                                                                       LocationId, SortingColumn, SortingDirection,
                                                                       AllowPaging, PageNum, PageSize,
                                                                       FromDate, ToDate, SearchKeyword, OrderStatus, ReportStatus, RefCode).ToList();
                //try
                //{
                if (data.Count > 0)
                {
                    //string TotalOrderAmount = "";
                    //string TotalAdditionalFee = "";
                    decimal? TotalReportAmount = 0;
                    decimal? TotalAdditionalFeeAmount = 0;
                    decimal? TotalOrderCost = 0;

                    var ObjtblOrderDetailColl = DX.tblOrderDetails;
                    foreach (var obj in data)
                    {
                        clsSavedReports ObjclsSavedReports = new clsSavedReports();

                        ObjclsSavedReports.TotalRec = (obj.TotalRec != null) ? Convert.ToInt32(obj.TotalRec) : 0;

                        ObjclsSavedReports.RowNo = (obj.RowNo != null) ? Convert.ToInt32(obj.RowNo) : 0;

                        ObjclsSavedReports.ApplicantName = (obj.ApplicantName != null) ? obj.ApplicantName : "";

                        ObjclsSavedReports.LastName = (obj.LastName != null) ? obj.LastName : "";

                        ObjclsSavedReports.FirstName = (obj.FirstName != null) ? obj.FirstName : "";

                        ObjclsSavedReports.OrderNo = (obj.OrderNo != null) ? obj.OrderNo : "";

                        ObjclsSavedReports.ReportIncludes = (obj.ReportIncludes != null) ? obj.ReportIncludes : "";

                        ObjclsSavedReports.CompanyName = (obj.CompanyName != null) ? obj.CompanyName : "";

                        ObjclsSavedReports.Location = (obj.Location != null) ? obj.Location : "";

                        ObjclsSavedReports.UserEmail = (obj.UserEmail != null) ? obj.UserEmail : "";
                        if (obj.OrderDt != null)
                        {
                            ObjclsSavedReports.OrderDt = obj.OrderDt;
                        }
                        ObjclsSavedReports.Days = (obj.Days != null) ? Convert.ToInt32(obj.Days) : 0;

                        ObjclsSavedReports.OrderStatus = (obj.OrderStatus != null) ? byte.Parse(obj.OrderStatus.ToString()) : byte.Parse("0");

                        ObjclsSavedReports.OrderStatusDisplay = (obj.OrderStatusDisplay != null) ? obj.OrderStatusDisplay : "";

                        ObjclsSavedReports.OrderId = (obj.OrderId != 0) ? Convert.ToInt32(obj.OrderId) : 0;

                        ObjclsSavedReports.StateCode = (obj.StateCode != null) ? obj.StateCode : "";

                        ObjclsSavedReports.IsDrugTestingOrder = (obj.IsDrugTestingOrder != null) ? Convert.ToBoolean(obj.IsDrugTestingOrder) : false;

                        ObjclsSavedReports.ReportsWithStatus = (obj.ReportsWithStatus != null) ? obj.ReportsWithStatus : "";

                        ObjclsSavedReports.PackageAmount = (obj.PackageAmount != null) ? Convert.ToDecimal(obj.PackageAmount) : 0;

                        ObjclsSavedReports.ReportAmount = (obj.ReportAmount != null) ? Convert.ToDecimal(obj.ReportAmount) : 0;

                        ObjclsSavedReports.AdditionalFee = (obj.AdditionalFee != null) ? Convert.ToDecimal(obj.AdditionalFee) : 0;

                        ObjclsSavedReports.IsSyncpod = (obj.IsSyncpod != null) ? Convert.ToBoolean(obj.IsSyncpod) : false;

                        ObjclsSavedReports.IsTiered = (obj.IsTiered != null) ? Convert.ToBoolean(obj.IsTiered) : false;

                        ObjclsSavedReports.ReferenceCode = (obj.ReferenceCode != null) ? obj.ReferenceCode : ""; // ND-29 on 11 aug 2015

                        if (ObjclsSavedReports.ReferenceCode == "")
                        {

                            ObjclsSavedReports.ReferenceCode = "No Reference Code";
                        }
                        ObjclsSavedReports.Locationid = LocationId;
                        if (RefCode == -1)
                        {
                            ObjclsSavedReports.Rcode = 1;
                        }// change on 10 aug 2015 for nd -29
                        else
                        {
                            ObjclsSavedReports.Rcode = RefCode;
                        }

                        // ObjclsSavedReports.Rcode = RefCode;







                        if (CompanyUserId == -1)
                        {
                            ObjclsSavedReports.UCode = -1;
                            // int 230 add user wise filteration process 
                        }// change on 10 aug 2015 for nd -29
                        else
                        {
                            ObjclsSavedReports.UCode = CompanyUserId;
                        }





                        if (obj.PackageName != "")
                        {
                            ObjclsSavedReports.PackageName = obj.PackageName;
                            ObjclsSavedReports.packagenamedisplay = 0;
                        }
                        else
                        {
                            ObjclsSavedReports.PackageName = obj.PackageName;
                            ObjclsSavedReports.packagenamedisplay = 1;
                        }
                        if (obj.OrderType != null)
                        {
                            if (byte.Parse(obj.OrderType.ToString()) == 1)
                            {
                                ObjclsSavedReports.OrderType = false;
                            }
                            else
                            {
                                ObjclsSavedReports.OrderType = true;
                            }
                        }

                        TotalReportAmount = 0.00m;
                        TotalAdditionalFeeAmount = 0.00m;
                        TotalOrderCost = 0.00m;
                        decimal TotalPackageAmount = 0;
                        if (obj.IsDrugTestingOrder != null && Convert.ToBoolean(obj.IsDrugTestingOrder))
                        {
                            //List<tblOrderDetail> ObjOrderDetails = DX.tblOrderDetails.Where(db => db.fkOrderId == obj.OrderId && (db.ReportStatus == 1 || db.ReportStatus == 2 || db.ReportStatus == 5)).ToList();
                            List<tblOrderDetail> ObjOrderDetails = ObjtblOrderDetailColl.Where(db => db.fkOrderId == obj.OrderId && (db.ReportStatus == 1 || db.ReportStatus == 2 || db.ReportStatus == 5 || db.ReportStatus == 3)).ToList();
                            if (ObjOrderDetails != null)
                            {
                                foreach (tblOrderDetail ObjtblOrderDetail in ObjOrderDetails)
                                {
                                    TotalReportAmount = TotalOrderCost + ((ObjtblOrderDetail.ProductQty != null) ? ObjtblOrderDetail.ProductQty : 0) * ((ObjtblOrderDetail.ReportAmount != null) ? ObjtblOrderDetail.ReportAmount : 0);

                                    TotalAdditionalFeeAmount = ObjOrderDetails.Sum(db => db.AdditionalFee);
                                    TotalOrderCost = TotalReportAmount + TotalAdditionalFeeAmount;
                                }
                            }
                        }
                        else
                        {
                            TotalPackageAmount = Convert.ToDecimal(obj.PackageAmount);
                            TotalReportAmount = TotalPackageAmount + obj.ReportAmount;
                            TotalAdditionalFeeAmount = obj.AdditionalFee;
                            TotalOrderCost = TotalReportAmount + TotalAdditionalFeeAmount;
                        }
                        ObjclsSavedReports.Price = (TotalReportAmount != null) ? ("$" + String.Format("{0:0.00}", TotalReportAmount)) : "$0.00";
                        ObjclsSavedReports.Fee = (TotalAdditionalFeeAmount == null || TotalAdditionalFeeAmount == 0) ? "" : ("$" + String.Format("{0:0.00}", TotalAdditionalFeeAmount));
                        ObjclsSavedReports.Total = Convert.ToDecimal(TotalOrderCost); // ND-29

                        listclsSavedReports.Add(ObjclsSavedReports);

                    }


                    #region discountorder test

                    DateTime Monthdata = Convert.ToDateTime(FromDate);
                    DateTime yeardata = Convert.ToDateTime(FromDate);

                    int Month = Convert.ToInt32(Monthdata.Month);
                    int Year = Convert.ToInt32(yeardata.Year);

                    if (ObjtblOrderDetailColl.Count() > 0)
                    {
                        decimal TotalPackageAmount = 0;
                        using (EmergeDALDataContext DX1 = new EmergeDALDataContext())
                        {



                            var InvoiceDiscount = DX.InvoiceDiscountOrAdjustmentAmount(Convert.ToInt32(CompanyId), Month, Year, 0, "Discount").FirstOrDefault();

                            if (InvoiceDiscount.DiscountAmount != null)
                            {
                                clsSavedReports ObjclsSavedReports = new clsSavedReports();
                                ObjclsSavedReports.TotalRec = 0;

                                ObjclsSavedReports.RowNo = 0;

                                ObjclsSavedReports.ApplicantName = "";

                                ObjclsSavedReports.LastName = "";

                                ObjclsSavedReports.FirstName = "";

                                ObjclsSavedReports.OrderNo = "Discount";

                                //ObjclsSavedReports.ReportIncludes = "Discount";

                                ObjclsSavedReports.CompanyName = "";

                                ObjclsSavedReports.Location = "";

                                ObjclsSavedReports.UserEmail = "";

                                //ObjclsSavedReports.OrderDt = System.DateTime.Now;

                                ObjclsSavedReports.Days = 0;

                                ObjclsSavedReports.OrderStatus = 2;
                                ObjclsSavedReports.packagenamedisplay = 1;
                                ObjclsSavedReports.OrderStatusDisplay = "";

                                //ObjclsSavedReports.OrderId =  0;

                                ObjclsSavedReports.StateCode = "";

                                ObjclsSavedReports.IsDrugTestingOrder = false;

                                ObjclsSavedReports.ReportsWithStatus = "";

                                ObjclsSavedReports.PackageAmount = 0;

                                ObjclsSavedReports.ReportAmount = 0;

                                ObjclsSavedReports.AdditionalFee = 0;

                                ObjclsSavedReports.IsSyncpod = false;

                                ObjclsSavedReports.IsTiered = false;
                                ObjclsSavedReports.ReferenceCode = ""; // ND-29
                                ObjclsSavedReports.LastName = "Discount";
                                BALReportsByLocation Obj3 = new BALReportsByLocation();
                                string ReportName_AddionalFee = "Discount";
                                int Reports_Total_Count = 1;
                                decimal Report_AdditionalFee = Convert.ToDecimal("-" + InvoiceDiscount.DiscountAmount);

                                Obj3.ProductDisplayName = ReportName_AddionalFee;
                                Obj3.ProductQty = short.Parse(Reports_Total_Count.ToString());
                                Obj3.ReportAmount = Report_AdditionalFee;
                                Obj3.IsSubProduct = true;

                                TotalReportAmount = 0.00m;
                                TotalAdditionalFeeAmount = 0.00m;
                                TotalOrderCost = 0.00m;
                                TotalPackageAmount = 0;

                                TotalPackageAmount = Convert.ToDecimal(InvoiceDiscount.DiscountAmount);
                                TotalReportAmount = TotalPackageAmount;
                                //TotalAdditionalFeeAmount = obj.AdditionalFee;
                                TotalOrderCost = Convert.ToDecimal("-" + InvoiceDiscount.DiscountAmount);
                                ObjclsSavedReports.Total = Convert.ToDecimal("-" + InvoiceDiscount.DiscountAmount);

                                listclsSavedReports.Add(ObjclsSavedReports);


                            }


                        }
                        using (EmergeDALDataContext DX1 = new EmergeDALDataContext())
                        {
                            //return DX.Get_CompanyLocationsByCompanyId(CompanyId, ApplicationId).ToList<Proc_Get_CompanyLocationsByCompanyIdResult>();


                            var InvoiceAdjustment = DX.InvoiceDiscountOrAdjustmentAmount(Convert.ToInt32(CompanyId), Month, Year, 0, "Adjustment").FirstOrDefault();
                            if (InvoiceAdjustment.DiscountAmount != null)
                            {

                                clsSavedReports ObjclsSavedReports = new clsSavedReports();
                                ObjclsSavedReports.TotalRec = 0;

                                ObjclsSavedReports.RowNo = 0;

                                ObjclsSavedReports.ApplicantName = "";

                                ObjclsSavedReports.LastName = "";

                                ObjclsSavedReports.FirstName = "";

                                ObjclsSavedReports.OrderNo = "Adjustment";
                                ObjclsSavedReports.packagenamedisplay = 1;
                                ObjclsSavedReports.ReportIncludes = "Adjustment";

                                ObjclsSavedReports.CompanyName = "";

                                ObjclsSavedReports.Location = "";

                                ObjclsSavedReports.UserEmail = "";

                                //ObjclsSavedReports.OrderDt = System.DateTime.Now;

                                ObjclsSavedReports.Days = 0;

                                ObjclsSavedReports.OrderStatus = 2;

                                //ObjclsSavedReports.OrderStatusDisplay = "";

                                ObjclsSavedReports.OrderId = 0;

                                ObjclsSavedReports.StateCode = "";

                                ObjclsSavedReports.IsDrugTestingOrder = false;

                                ObjclsSavedReports.ReportsWithStatus = "";

                                ObjclsSavedReports.PackageAmount = 0;

                                ObjclsSavedReports.ReportAmount = 0;

                                ObjclsSavedReports.AdditionalFee = 0;

                                ObjclsSavedReports.IsSyncpod = false;

                                ObjclsSavedReports.IsTiered = false;
                                ObjclsSavedReports.ReferenceCode = ""; // ND-29



                                ObjclsSavedReports.ApplicantName = "Adjustment";

                                ObjclsSavedReports.LastName = "Adjustment";
                                BALReportsByLocation Obj3 = new BALReportsByLocation();
                                string ReportName_AddionalFee = "Adjustment";
                                int Reports_Total_Count = 1;
                                decimal Report_AdditionalFee = Convert.ToDecimal(InvoiceAdjustment.DiscountAmount);


                                TotalReportAmount = 0.00m;
                                TotalAdditionalFeeAmount = 0.00m;
                                TotalOrderCost = 0.00m;
                                TotalPackageAmount = 0;

                                TotalPackageAmount = Convert.ToDecimal(0);
                                TotalReportAmount = Convert.ToDecimal(InvoiceAdjustment.DiscountAmount);
                                TotalAdditionalFeeAmount = 0;
                                TotalOrderCost = Convert.ToDecimal(InvoiceAdjustment.DiscountAmount);

                                ObjclsSavedReports.Total = Convert.ToDecimal(InvoiceAdjustment.DiscountAmount);

                                listclsSavedReports.Add(ObjclsSavedReports);

                            }



                        }


                    }



                    #endregion





                }
            }
            return listclsSavedReports;

        }

        public tblCompany GetCompanyAccountNumber(int pkCompanyId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return (from db in DX.tblCompanies
                        where db.pkCompanyId == pkCompanyId
                        select db).FirstOrDefault();
            }

        }

        public List<CompanyList> GetAllCompany(Guid ApplicationId, bool Enabled, bool Deleted)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                IQueryable<CompanyList> ObjData = from c in DX.tblCompanies
                                                  join l in DX.tblLocations on c.pkCompanyId equals l.fkCompanyId
                                                  join s in DX.tblStates on l.fkStateID equals s.pkStateId
                                                  where c.fkApplicationId == ApplicationId && c.IsDeleted == Deleted && l.IsHome == true
                                                  orderby c.CompanyName ascending
                                                  select new CompanyList
                                                  {
                                                      CompanyName = c.CompanyName + "  [" + l.City + ", " + s.StateCode + "]",
                                                      pkCompanyId = c.pkCompanyId,
                                                      fkSalesRepId = c.fkSalesRepId,
                                                  };
                return ObjData.ToList();
            }
        }

        /// <summary>
        /// Function to Get All the Company by application id,Enabled,Deleted For sales managers in office
        /// </summary>
        /// <param name="ApplicationId"></param>
        /// <param name="Enabled"></param>
        /// <param name="Deleted"></param>
        /// <param name="fkSalesManagerId"></param>
        /// <returns></returns>
        public List<CompanyList> GetAllCompanyBySalesMgr(Guid ApplicationId, bool Enabled, bool Deleted, Guid fkSalesManagerId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                IQueryable<CompanyList> ObjData = from c in ObjDALDataContext.tblCompanies
                                                  join l in ObjDALDataContext.tblLocations on c.pkCompanyId equals l.fkCompanyId
                                                  join s in ObjDALDataContext.tblStates on l.fkStateID equals s.pkStateId
                                                  where c.fkApplicationId == ApplicationId && c.IsDeleted == Deleted && l.IsHome == true && c.fkSalesRepId == fkSalesManagerId
                                                  orderby c.CompanyName ascending
                                                  select new CompanyList
                                                  {
                                                      CompanyName = c.CompanyName + "  [" + l.City + ", " + s.StateCode + "]",
                                                      pkCompanyId = c.pkCompanyId
                                                  };
                if (ObjData.Count() == 0)
                {
                    Proc_Get_CompanyDetailByMemberShipIdResult ObjDet = new Proc_Get_CompanyDetailByMemberShipIdResult();
                    ObjDet = ObjDALDataContext.GetCompanyDetailByMemberShipId(fkSalesManagerId).FirstOrDefault();
                    if (ObjDet != null)
                    {
                        ObjData = from c in ObjDALDataContext.tblCompanies
                                  join l in ObjDALDataContext.tblLocations on c.pkCompanyId equals l.fkCompanyId
                                  join s in ObjDALDataContext.tblStates on l.fkStateID equals s.pkStateId
                                  where c.fkApplicationId == ApplicationId && c.IsDeleted == Deleted && l.IsHome == true && c.pkCompanyId == ObjDet.PkCompanyId
                                  orderby c.CompanyName ascending
                                  select new CompanyList
                                  {
                                      CompanyName = c.CompanyName + "  [" + l.City + ", " + s.StateCode + "]",
                                      pkCompanyId = c.pkCompanyId
                                  };

                    }
                }
                return ObjData.ToList();
            }
        }

        /// <summary>
        /// Function to Get Company Locations  By Company ID
        /// </summary>
        /// <returns></returns>
        public List<Proc_Get_CompanyLocationsByCompanyIdResult> GetLocationsByCompanyId(int CompanyId, Guid ApplicationId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return DX.Get_CompanyLocationsByCompanyId(CompanyId, ApplicationId).ToList<Proc_Get_CompanyLocationsByCompanyIdResult>();
            }
        }


        public List<tblReferenceCode> GetAllReferenceCodes(int? fkCompanyId)
        {
            //INT-10
            List<tblReferenceCode> ObjList = new List<tblReferenceCode>();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                if (fkCompanyId == 0)
                    ObjList = DX.tblReferenceCodes.OrderBy(d => d.ReferenceCode).ToList<tblReferenceCode>();
                else
                    ObjList = DX.tblReferenceCodes.Where(d => d.fkCompanyId == fkCompanyId).OrderBy(d => d.ReferenceCode).ToList<tblReferenceCode>();
            }
            return ObjList;
        }

        /// <summary>
        /// This method is used to get users according location id
        /// </summary>
        /// <param name="LocationId"></param>
        /// <returns></returns>
        public List<Proc_Get_CompanyUsersByLocationIdResult> GetCompanyUsersByLocationId(int CompanyId, int LocationId, Guid ApplicationId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return DX.GetCompanyUsersByLocationId(CompanyId, LocationId, ApplicationId).ToList<Proc_Get_CompanyUsersByLocationIdResult>();
            }
        }

        /// <summary>
        /// Function to Get Sales Rep by their role
        /// </summary>
        /// <returns></returns>
        public List<Proc_Get_SalesRepResult> GetSalesRep(string RoleName)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return DX.GetSalesRep(RoleName).ToList<Proc_Get_SalesRepResult>();
            }
        }

        /// <summary>
        /// Get order detail by orderId
        /// </summary>
        /// <param name="RoleName"></param>
        /// <returns></returns>
        public List<tblOrderDetail> GetOrderDetailByOrderId(int OrderId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return DX.tblOrderDetails.Where(db => db.fkOrderId == OrderId && (db.ReportStatus == 1 || db.ReportStatus == 2 || db.ReportStatus == 5 || db.ReportStatus == 3)).ToList<tblOrderDetail>();
            }
        }
        public List<tblOrderDetail> GetOrderDetailByOrderId()
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return DX.tblOrderDetails.ToList();
            }
        }
        /// <summary>
        /// Get AdditionalFee
        /// </summary>
        /// <param name="RoleName"></param>
        /// <returns></returns>
        public decimal GetAdditionalFee(int OrderId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                var data = DX.tblOrderDetails.Where(db => db.fkOrderId == OrderId && (db.ReportStatus == 1 || db.ReportStatus == 2 || db.ReportStatus == 5 || db.ReportStatus == 3)).Sum(db => db.AdditionalFee);
                return (data != null) ? data : 0;
            }
        }

        public List<proc_LoadOpenInvoiceByCompanyIdResult> LoadOpenInvoiceByCompanyId(int pkCompanyId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                string.Format("", "");
                return DX.LoadOpenInvoiceByCompanyId(pkCompanyId).ToList();
            }
        }


        #region Billing Data

        public delegate IMultipleResults DelegateGetBillingForAdminMVC(Guid ApplicationId, int BillingMonth, int BillingYear, int CompanyId, Guid SalesRepId, int LocationId, int UserId, int RefCodeId, short OrderType);
        public delegate List<BALReportsByLocation> DelegateGetReportsByLocation(List<BALCompanyLocations> ObjBALCompanyLocations, List<BALProductsPerLocation> ObjBALProductsPerLocation, List<BALPackagesPrice> ObjBalPackagePrice, List<BALPackageReport> ObjPackageReport, List<BALReportsFee> ObjReportFee, int Month, int Year, int RefCode);
        public Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALProductsPerLocation>, List<List<BALReportsByLocation>>, List<proc_GetBillingAdjustmentsResult>, Totals, Totals> GetBillingForAdmin(string ApplicationId, string Month, string Year, int CompanyId, int LocationId, int UserId, Guid SalesRepId, int RefCode)
        {
            using (BALBilling ObjBALBilling = new BALBilling())
            {
                //new Guid("7dc87d77-6bb5-4bb3-9837-fa861f36f281")
                DelegateGetBillingForAdminMVC delGetBillingForAdminMVC = ObjBALBilling.GetBillingForAdminMVC;
                IAsyncResult resultIAsync = delGetBillingForAdminMVC.BeginInvoke(new Guid(ApplicationId), int.Parse(Month), int.Parse(Year), CompanyId, SalesRepId, LocationId, UserId, RefCode, 1, null, null);
                //IMultipleResults Results = ObjBALBilling.GetBillingForAdminMVC(new Guid(ApplicationId), int.Parse(Month), int.Parse(Year),
                //    CompanyId,
                //  SalesRepId,
                //    LocationId,
                //    UserId,
                //    RefCode,
                //    1);
                IMultipleResults Results = delGetBillingForAdminMVC.EndInvoke(resultIAsync);

                List<BALBillingSummary> ObjBALBillingSummaryTotal = Results.GetResult<BALBillingSummary>().ToList();
                //List<BALBillingSummary> ObjBALBillingSummary = ObjBALBillingSummaryTotal.Where(d => d.pkReportCategoryId != 10).ToList();
                List<BALBillingSummary> ObjNotDrugTestingSummary = ObjBALBillingSummaryTotal.Where(t => t.pkReportCategoryId != 10 && t.pkReportCategoryId != (byte)ReportCategories.DrugTesting).ToList();
                List<BALBillingSummary> ObjDrugTestingSummary = ObjBALBillingSummaryTotal.Where(t => t.pkReportCategoryId == (byte)ReportCategories.DrugTesting).ToList();
                // List<proc_GetBillingAdjustmentsResult> ObjBALBillingSummaryAdjustment = ObjBALBillingAdjustments.GetBillingAdjustment(Utility.SiteApplicationId, BillingMonth, BillingYear, new Guid(ddlCompany.SelectedValue)).ToList<proc_GetBillingAdjustmentsResult>();
                #region categorize companies acc to status

                List<BALCompanyLocations> ObjBALAllCompanyLocations = Results.GetResult<BALCompanyLocations>().ToList();

                List<BALCompanyLocations> ObjBALActiveCompanyLocations = ObjBALAllCompanyLocations.Where(c => c.CompanyStatus == "Active").ToList();
                List<BALCompanyLocations> ObjBALOneMonthInActiveCompanyLocations = ObjBALAllCompanyLocations.Where(c => c.CompanyStatus == "InActive" && c.StatusStartDate <= DateTime.Now && c.StatusStartDate > DateTime.Now.AddMonths(-3)).ToList();
                List<BALCompanyLocations> ObjBALThreeMonthInActiveCompanyLocations = ObjBALAllCompanyLocations.Where(c => c.CompanyStatus == "InActive" && c.StatusStartDate <= DateTime.Now.AddMonths(-3) && c.StatusStartDate > DateTime.Now.AddMonths(-6)).ToList();
                List<BALCompanyLocations> ObjBALSixMonthInActiveCompanyLocations = ObjBALAllCompanyLocations.Where(c => c.CompanyStatus == "InActive" && c.StatusStartDate <= DateTime.Now.AddMonths(-6) && c.StatusStartDate > DateTime.Now.AddMonths(-12)).ToList();
                List<BALCompanyLocations> ObjBALTwelveMonthInActiveCompanyLocations = ObjBALAllCompanyLocations.Where(c => c.CompanyStatus == "InActive" && c.StatusStartDate <= DateTime.Now.AddMonths(-12)).ToList();
                List<BALCompanyLocations> ObjBALInCollectionCompanyLocations = ObjBALAllCompanyLocations.Where(c => c.CompanyStatus == "InCollection").ToList();
                List<BALCompanyLocations> ObjBALPastDueCompanyLocations = ObjBALAllCompanyLocations.Where(c => c.CompanyStatus == "PastDue").ToList();
                List<BALCompanyLocations> ObjBALLockedCompanyLocations = ObjBALAllCompanyLocations.Where(c => c.CompanyStatus == "Locked").ToList();


                //List<List<BALReportsByLocation>> ObjBALCompanyLocations = new List<List<BALReportsByLocation>>();
                //ObjBALCompanyLocations = ObjBALActiveCompanyLocations.Union(ObjBALOneMonthInActiveCompanyLocations).Union(ObjBALThreeMonthInActiveCompanyLocations).Union(ObjBALSixMonthInActiveCompanyLocations).Union(ObjBALTwelveMonthInActiveCompanyLocations).Union(ObjBALInCollectionCompanyLocations).Union(ObjBALPastDueCompanyLocations).Union(ObjBALLockedCompanyLocations).ToList();
                #endregion
                //List<BALCompanyLocations> ObjBALCompanyLocations = Results.GetResult<BALCompanyLocations>().ToList();
                List<BALProductsPerLocation> ObjBALProductsPerLocation = Results.GetResult<BALProductsPerLocation>().ToList();
                List<BALPackagesPrice> ObjBalPackagePrice = Results.GetResult<BALPackagesPrice>().ToList();
                List<BALPackageReport> ObjPackageReport = Results.GetResult<BALPackageReport>().ToList();
                List<BALReportsFee> ObjReportFee = Results.GetResult<BALReportsFee>().ToList();

                #region Asynch Call
                // List<BALReportsByLocation> ObjBALReportsByLocation = GetReportsByLocation(ObjBALActiveCompanyLocations, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode);
                //List<BALReportsByLocation> ObjBALReportsByLocationOneMonth = GetReportsByLocation(ObjBALOneMonthInActiveCompanyLocations, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode);
                //List<BALReportsByLocation> ObjBALReportsByLocationThreeMonth = GetReportsByLocation(ObjBALThreeMonthInActiveCompanyLocations, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode);
                //List<BALReportsByLocation> ObjBALReportsByLocationSixMonth = GetReportsByLocation(ObjBALSixMonthInActiveCompanyLocations, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode);
                //List<BALReportsByLocation> ObjBALReportsByLocationTwelveMonth = GetReportsByLocation(ObjBALTwelveMonthInActiveCompanyLocations, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode);
                //List<BALReportsByLocation> ObjBALReportsByLocationInCollection = GetReportsByLocation(ObjBALInCollectionCompanyLocations, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode);
                //List<BALReportsByLocation> ObjBALReportsByLocationPastDue = GetReportsByLocation(ObjBALPastDueCompanyLocations, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode);
                //List<BALReportsByLocation> ObjBALReportsByLocationLocked = GetReportsByLocation(ObjBALLockedCompanyLocations, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode);

                DelegateGetReportsByLocation objByLocationDel = GetReportsByLocation;
                IAsyncResult obj1 = objByLocationDel.BeginInvoke(ObjBALActiveCompanyLocations, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode, null, null);
                IAsyncResult obj2 = objByLocationDel.BeginInvoke(ObjBALOneMonthInActiveCompanyLocations, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode, null, null);
                IAsyncResult obj3 = objByLocationDel.BeginInvoke(ObjBALThreeMonthInActiveCompanyLocations, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode, null, null);
                IAsyncResult obj4 = objByLocationDel.BeginInvoke(ObjBALSixMonthInActiveCompanyLocations, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode, null, null);
                IAsyncResult obj5 = objByLocationDel.BeginInvoke(ObjBALTwelveMonthInActiveCompanyLocations, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode, null, null);
                IAsyncResult obj6 = objByLocationDel.BeginInvoke(ObjBALInCollectionCompanyLocations, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode, null, null);
                IAsyncResult obj7 = objByLocationDel.BeginInvoke(ObjBALPastDueCompanyLocations, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode, null, null);
                IAsyncResult obj8 = objByLocationDel.BeginInvoke(ObjBALLockedCompanyLocations, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode, null, null);

                List<List<BALReportsByLocation>> ObjBALCompanyLocations = new List<List<BALReportsByLocation>>();
                List<BALReportsByLocation> ObjBALReportsByLocation = objByLocationDel.EndInvoke(obj1);
                List<BALReportsByLocation> ObjBALReportsByLocationOneMonth = objByLocationDel.EndInvoke(obj2);
                List<BALReportsByLocation> ObjBALReportsByLocationThreeMonth = objByLocationDel.EndInvoke(obj3);
                List<BALReportsByLocation> ObjBALReportsByLocationSixMonth = objByLocationDel.EndInvoke(obj4);
                List<BALReportsByLocation> ObjBALReportsByLocationTwelveMonth = objByLocationDel.EndInvoke(obj5);
                List<BALReportsByLocation> ObjBALReportsByLocationInCollection = objByLocationDel.EndInvoke(obj6);
                List<BALReportsByLocation> ObjBALReportsByLocationPastDue = objByLocationDel.EndInvoke(obj7);
                List<BALReportsByLocation> ObjBALReportsByLocationLocked = objByLocationDel.EndInvoke(obj8);

                ObjBALCompanyLocations.Add(ObjBALReportsByLocation);
                ObjBALCompanyLocations.Add(ObjBALReportsByLocationOneMonth);
                ObjBALCompanyLocations.Add(ObjBALReportsByLocationThreeMonth);
                ObjBALCompanyLocations.Add(ObjBALReportsByLocationSixMonth);
                ObjBALCompanyLocations.Add(ObjBALReportsByLocationTwelveMonth);
                ObjBALCompanyLocations.Add(ObjBALReportsByLocationInCollection);
                ObjBALCompanyLocations.Add(ObjBALReportsByLocationPastDue);
                ObjBALCompanyLocations.Add(ObjBALReportsByLocationLocked);
                #endregion

                List<proc_GetBillingAdjustmentsResult> ObjBALBillingSummaryAdjustment = GetBillingAdjustment(new Guid(ApplicationId), int.Parse(Month), int.Parse(Year), CompanyId).ToList();
                Totals ObjSalesReport = new Totals();
                Totals ObjVolumeReport = new Totals();
                if (CompanyId != 0)
                {
                    Tuple<Totals, Totals> ObjSalesAndVolume = GetSalesAndVolume(CompanyId, int.Parse(Month), int.Parse(Year));
                    if (ObjSalesAndVolume != null)
                    {
                        ObjSalesReport = ObjSalesAndVolume.Item1;
                        ObjVolumeReport = ObjSalesAndVolume.Item2;
                    }
                }

                Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALProductsPerLocation>, List<List<BALReportsByLocation>>, List<proc_GetBillingAdjustmentsResult>, Totals, Totals> collection = new Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALProductsPerLocation>, List<List<BALReportsByLocation>>, List<proc_GetBillingAdjustmentsResult>, Totals, Totals>(ObjNotDrugTestingSummary, ObjDrugTestingSummary, ObjBALProductsPerLocation, ObjBALCompanyLocations, ObjBALBillingSummaryAdjustment, ObjSalesReport, ObjVolumeReport);
                return collection;
            }
        }

        public Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALProductsPerLocation>, List<List<BALReportsByLocation>>, List<proc_GetBillingAdjustmentsResult>, Totals, Totals> GetBillingForSales(string ApplicationId, string Month, string Year, int CompanyId, int LocationId, int UserId, string SalesRepId, int RefCode, string CompanyStatus)
        {
            using (BALBilling ObjBALBilling = new BALBilling())
            {
                //new Guid("7dc87d77-6bb5-4bb3-9837-fa861f36f281")
                if (new Guid(ApplicationId) == Guid.Empty)
                {
                    ApplicationId = "65d1e085-eb17-4f29-96d0-247b0c1ee852";
                }
                IMultipleResults Results = ObjBALBilling.GetBillingForSales(new Guid(ApplicationId), int.Parse(Month), int.Parse(Year),
                    CompanyId,
                    new Guid(SalesRepId),
                    LocationId,
                    UserId,
                    RefCode,
                    CompanyStatus
                    );
                List<BALBillingSummary> ObjBALBillingSummaryTotal = Results.GetResult<BALBillingSummary>().ToList();
                //List<BALBillingSummary> ObjBALBillingSummary = ObjBALBillingSummaryTotal.Where(d => d.pkReportCategoryId != 10).ToList();
                List<BALBillingSummary> ObjNotDrugTestingSummary = ObjBALBillingSummaryTotal.Where(t => t.pkReportCategoryId != 10 && t.pkReportCategoryId != (byte)ReportCategories.DrugTesting).ToList();
                List<BALBillingSummary> ObjDrugTestingSummary = ObjBALBillingSummaryTotal.Where(t => t.pkReportCategoryId == (byte)ReportCategories.DrugTesting).ToList();
                // List<proc_GetBillingAdjustmentsResult> ObjBALBillingSummaryAdjustment = ObjBALBillingAdjustments.GetBillingAdjustment(Utility.SiteApplicationId, BillingMonth, BillingYear, new Guid(ddlCompany.SelectedValue)).ToList<proc_GetBillingAdjustmentsResult>();

                #region categorize companies acc to status

                List<BALCompanyLocations> ObjBALAllCompanyLocations = Results.GetResult<BALCompanyLocations>().ToList();
                List<BALCompanyLocations> ObjBALActiveCompanyLocations = ObjBALAllCompanyLocations.Where(c => c.CompanyStatus == "Active").ToList();
                List<BALCompanyLocations> ObjBALOneMonthInActiveCompanyLocations = ObjBALAllCompanyLocations.Where(c => c.CompanyStatus == "InActive" && c.StatusStartDate <= DateTime.Now && c.StatusStartDate > DateTime.Now.AddMonths(-3)).ToList();
                List<BALCompanyLocations> ObjBALThreeMonthInActiveCompanyLocations = ObjBALAllCompanyLocations.Where(c => c.CompanyStatus == "InActive" && c.StatusStartDate <= DateTime.Now.AddMonths(-3) && c.StatusStartDate > DateTime.Now.AddMonths(-6)).ToList();
                List<BALCompanyLocations> ObjBALSixMonthInActiveCompanyLocations = ObjBALAllCompanyLocations.Where(c => c.CompanyStatus == "InActive" && c.StatusStartDate <= DateTime.Now.AddMonths(-6) && c.StatusStartDate > DateTime.Now.AddMonths(-12)).ToList();
                List<BALCompanyLocations> ObjBALTwelveMonthInActiveCompanyLocations = ObjBALAllCompanyLocations.Where(c => c.CompanyStatus == "InActive" && c.StatusStartDate <= DateTime.Now.AddMonths(-12)).ToList();
                List<BALCompanyLocations> ObjBALInCollectionCompanyLocations = ObjBALAllCompanyLocations.Where(c => c.CompanyStatus == "InCollection").ToList();
                List<BALCompanyLocations> ObjBALPastDueCompanyLocations = ObjBALAllCompanyLocations.Where(c => c.CompanyStatus == "PastDue").ToList();
                List<BALCompanyLocations> ObjBALLockedCompanyLocations = ObjBALAllCompanyLocations.Where(c => c.CompanyStatus == "Locked").ToList();
                List<List<BALReportsByLocation>> ObjBALCompanyLocations = new List<List<BALReportsByLocation>>();
                //ObjBALCompanyLocations = ObjBALActiveCompanyLocations.Union(ObjBALOneMonthInActiveCompanyLocations).Union(ObjBALThreeMonthInActiveCompanyLocations).Union(ObjBALSixMonthInActiveCompanyLocations).Union(ObjBALTwelveMonthInActiveCompanyLocations).Union(ObjBALInCollectionCompanyLocations).Union(ObjBALPastDueCompanyLocations).Union(ObjBALLockedCompanyLocations).ToList();
                #endregion
                //List<BALCompanyLocations> ObjBALCompanyLocations = Results.GetResult<BALCompanyLocations>().ToList();
                List<BALProductsPerLocation> ObjBALProductsPerLocation = Results.GetResult<BALProductsPerLocation>().ToList();
                List<BALPackagesPrice> ObjBalPackagePrice = Results.GetResult<BALPackagesPrice>().ToList();
                List<BALPackageReport> ObjPackageReport = Results.GetResult<BALPackageReport>().ToList();
                List<BALReportsFee> ObjReportFee = Results.GetResult<BALReportsFee>().ToList();

                List<BALReportsByLocation> ObjBALReportsByLocation = GetReportsByLocation(ObjBALActiveCompanyLocations, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode);
                List<BALReportsByLocation> ObjBALReportsByLocationOneMonth = GetReportsByLocation(ObjBALOneMonthInActiveCompanyLocations, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode);
                List<BALReportsByLocation> ObjBALReportsByLocationThreeMonth = GetReportsByLocation(ObjBALThreeMonthInActiveCompanyLocations, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode);
                List<BALReportsByLocation> ObjBALReportsByLocationSixMonth = GetReportsByLocation(ObjBALSixMonthInActiveCompanyLocations, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode);
                List<BALReportsByLocation> ObjBALReportsByLocationTwelveMonth = GetReportsByLocation(ObjBALTwelveMonthInActiveCompanyLocations, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode);
                List<BALReportsByLocation> ObjBALReportsByLocationInCollection = GetReportsByLocation(ObjBALInCollectionCompanyLocations, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode);
                List<BALReportsByLocation> ObjBALReportsByLocationPastDue = GetReportsByLocation(ObjBALPastDueCompanyLocations, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode);
                List<BALReportsByLocation> ObjBALReportsByLocationLocked = GetReportsByLocation(ObjBALLockedCompanyLocations, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode);
                ObjBALCompanyLocations.Add(ObjBALReportsByLocation);
                ObjBALCompanyLocations.Add(ObjBALReportsByLocationOneMonth);
                ObjBALCompanyLocations.Add(ObjBALReportsByLocationThreeMonth);
                ObjBALCompanyLocations.Add(ObjBALReportsByLocationSixMonth);
                ObjBALCompanyLocations.Add(ObjBALReportsByLocationTwelveMonth);
                ObjBALCompanyLocations.Add(ObjBALReportsByLocationInCollection);
                ObjBALCompanyLocations.Add(ObjBALReportsByLocationPastDue);
                ObjBALCompanyLocations.Add(ObjBALReportsByLocationLocked);
                List<proc_GetBillingAdjustmentsResult> ObjBALBillingSummaryAdjustment = GetBillingAdjustment(new Guid(ApplicationId), int.Parse(Month), int.Parse(Year), CompanyId).ToList();
                Totals ObjSalesReport = new Totals();
                Totals ObjVolumeReport = new Totals();
                if (CompanyId != 0)
                {
                    Tuple<Totals, Totals> ObjSalesAndVolume = GetSalesAndVolume(CompanyId, int.Parse(Month), int.Parse(Year));
                    if (ObjSalesAndVolume != null)
                    {
                        ObjSalesReport = ObjSalesAndVolume.Item1;
                        ObjVolumeReport = ObjSalesAndVolume.Item2;
                    }
                }
                else
                {
                    ObjSalesReport.Product = "0";
                    ObjSalesReport.ProductPercentage = 0;
                    ObjSalesReport.ProductValue = 0;
                    ObjSalesReport.Report = "";
                    ObjSalesReport.ReportPercentage = 0;
                    ObjSalesReport.ReportValue = 0;
                    ObjSalesReport.Total = "0";
                    ObjSalesReport.TotalPercentage = 0;
                    ObjSalesReport.TotalValue = 0;


                    ObjVolumeReport.Product = "0";
                    ObjVolumeReport.ProductPercentage = 0;
                    ObjVolumeReport.ProductValue = 0;
                    ObjVolumeReport.Report = "";
                    ObjVolumeReport.ReportPercentage = 0;
                    ObjVolumeReport.ReportValue = 0;
                    ObjVolumeReport.Total = "0";
                    ObjVolumeReport.TotalPercentage = 0;
                    ObjVolumeReport.TotalValue = 0;
                }
                Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALProductsPerLocation>, List<List<BALReportsByLocation>>, List<proc_GetBillingAdjustmentsResult>, Totals, Totals> collection = new Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALProductsPerLocation>, List<List<BALReportsByLocation>>, List<proc_GetBillingAdjustmentsResult>, Totals, Totals>(ObjNotDrugTestingSummary, ObjDrugTestingSummary, ObjBALProductsPerLocation, ObjBALCompanyLocations, ObjBALBillingSummaryAdjustment, ObjSalesReport, ObjVolumeReport);
                return collection;
            }
        }

        public delegate List<BALReportsByLocation> DelegateGetReportsByLocations(List<BALCompanyLocations> ObjBALCompanyLocations, List<BALProductsPerLocation> ObjBALProductsPerLocation, List<BALPackagesPrice> ObjBalPackagePrice, List<BALPackageReport> ObjPackageReport, List<BALReportsFee> ObjReportFee, int Month, int Year, int RefCode);
        public delegate List<proc_GetBillingAdjustmentsResult> DelegateGetBillingAdjustment(Guid ApplicationId, int BillingMonth, int BillingYear, int CompanyId);
        public Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALReportsByLocation>, List<proc_GetBillingAdjustmentsResult>> GetBillingForAdminForApi(string ApplicationId, string Month, string Year, int CompanyId, int LocationId, int UserId, Guid SalesRepId, int RefCode)
        {
            using (BALBilling ObjBALBilling = new BALBilling())
            {
                IMultipleResults Results = ObjBALBilling.GetBillingForAdminMVC(new Guid(ApplicationId), int.Parse(Month), int.Parse(Year),
                    CompanyId,
                  SalesRepId,
                    LocationId,
                    UserId,
                    RefCode,
                    2);
                List<BALBillingSummary> ObjBALBillingSummaryTotal = Results.GetResult<BALBillingSummary>().ToList();
                List<BALBillingSummary> ObjNotDrugTestingSummary = ObjBALBillingSummaryTotal.Where(t => t.pkReportCategoryId != 10 && t.pkReportCategoryId != (byte)ReportCategories.DrugTesting).ToList();
                List<BALBillingSummary> ObjDrugTestingSummary = ObjBALBillingSummaryTotal.Where(t => t.pkReportCategoryId == (byte)ReportCategories.DrugTesting).ToList();

                List<BALCompanyLocations> ObjBALCompanyLocations = Results.GetResult<BALCompanyLocations>().ToList();
                List<BALProductsPerLocation> ObjBALProductsPerLocation = Results.GetResult<BALProductsPerLocation>().ToList();
                List<BALPackagesPrice> ObjBalPackagePrice = Results.GetResult<BALPackagesPrice>().ToList();
                List<BALPackageReport> ObjPackageReport = Results.GetResult<BALPackageReport>().ToList();
                List<BALReportsFee> ObjReportFee = Results.GetResult<BALReportsFee>().ToList();

                #region Asynch Call
                //List<BALReportsByLocation> ObjBALReportsByLocation = GetReportsByLocation(ObjBALCompanyLocations, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode);
                //List<proc_GetBillingAdjustmentsResult> ObjBALBillingSummaryAdjustment = GetBillingAdjustment(new Guid(ApplicationId), int.Parse(Month), int.Parse(Year), CompanyId).ToList();

                DelegateGetReportsByLocations delGetReportsByLocation = GetReportsByLocation;
                DelegateGetBillingAdjustment delGetBillingAdjustment = GetBillingAdjustment;

                IAsyncResult result1 = delGetReportsByLocation.BeginInvoke(ObjBALCompanyLocations, ObjBALProductsPerLocation, ObjBalPackagePrice, ObjPackageReport, ObjReportFee, int.Parse(Month), int.Parse(Year), RefCode, null, null);
                IAsyncResult result2 = delGetBillingAdjustment.BeginInvoke(new Guid(ApplicationId), int.Parse(Month), int.Parse(Year), CompanyId, null, null);

                List<BALReportsByLocation> ObjBALReportsByLocation = delGetReportsByLocation.EndInvoke(result1);
                List<proc_GetBillingAdjustmentsResult> ObjBALBillingSummaryAdjustment = delGetBillingAdjustment.EndInvoke(result2);
                #endregion
                Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALReportsByLocation>, List<proc_GetBillingAdjustmentsResult>> collection = new Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALReportsByLocation>, List<proc_GetBillingAdjustmentsResult>>(ObjNotDrugTestingSummary, ObjDrugTestingSummary, ObjBALReportsByLocation, ObjBALBillingSummaryAdjustment);
                return collection;
            }
        }

        public List<BALReportsByLocation> GetReportsByLocation(List<BALCompanyLocations> ObjBALCompanyLocations, List<BALProductsPerLocation> ObjBALProductsPerLocation, List<BALPackagesPrice> ObjBalPackagePrice, List<BALPackageReport> ObjPackageReport, List<BALReportsFee> ObjReportFee, int Month, int Year, int RefCode)
        {

            List<BALReportsByLocation> ObjBALReportsByLocation = new List<BALReportsByLocation>();
            //string sCompany = string.Empty;
            int sCompany = 0;
            String SalesRepCompAccount = string.Empty;
            string sCompanyDisplay = string.Empty;
            string sLocation = string.Empty;

            string sLocationDisplay = string.Empty;
            decimal dMVRAmt = 0;
            //Guid CCR1 = new Guid("2bbfdabc-df06-41d1-8e3d-8b7689b5b60e");
            //Guid SCR = new Guid("3649ef63-5410-492e-b967-fcf8774a016a");
            //Guid FCR = new Guid("06a0c3b7-fc38-4fe4-b080-01ceb67452a7");

            //int CCR1 = 1;
            //int SCR = 2;
            //int FCR = 3;

            //  Guid CCR1 = new Guid("2bbfdabc-df06-41d1-8e3d-8b7689b5b60e");
            int CCR1 = 39;
            //Guid SCR = new Guid("3649ef63-5410-492e-b967-fcf8774a016a");
            int SCR = 8;
            // Guid FCR = new Guid("06a0c3b7-fc38-4fe4-b080-01ceb67452a7");
            int FCR = 47;
            #region Get All Company Accounts
            List<Proc_GetCompACWithSalesRepResult> objAllCompanyAccounts;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                objAllCompanyAccounts = DX.GetCompACWithSalesRep(0).ToList();
            }
            #endregion
            for (int iRow = 0; iRow < ObjBALCompanyLocations.Count; iRow++)
            {
                //if (sCompany != ObjBALCompanyLocations.ElementAt(iRow).CompanyName.Trim())
                // BALProductsPerLocation ObjProduct = ObjBALProductsPerLocation.Where(t => t.fkLocationId == ObjBALCompanyLocations.ElementAt(iRow).pkCompanyId).FirstOrDefault();
                //if (ObjProduct.TotalReports > 0 && ObjProduct.FirstOrDefault().IsDeleted)
                //if (ObjProduct != null)
                //{
                if (sCompany != ObjBALCompanyLocations.ElementAt(iRow).pkCompanyId)
                {
                    //BALReportsByLocation ObjMainHeading = new BALReportsByLocation();
                    sCompany = ObjBALCompanyLocations.ElementAt(iRow).pkCompanyId;
                    sCompanyDisplay = ObjBALCompanyLocations.ElementAt(iRow).CompanyName.Trim() + " (" + ObjBALCompanyLocations.ElementAt(iRow).CompanyCity + ", " + ObjBALCompanyLocations.ElementAt(iRow).CompanyState + ")";

                    #region Company Account Number
                    int pkCompanyId = ObjBALCompanyLocations.ElementAt(iRow).pkCompanyId;
                    //List<Proc_GetCompACWithSalesRepResult> ObjtblCompany = new List<Proc_GetCompACWithSalesRepResult>();
                    //using (EmergeDALDataContext DX = new EmergeDALDataContext())
                    //{
                    //  ObjtblCompany = DX.GetCompACWithSalesRep(pkCompanyId).ToList();
                    //}
                    var ObjtblCompany = objAllCompanyAccounts.Where(x => x.pkCompanyId == pkCompanyId).ToList();
                    string AccountNumber = string.Empty, FirstName = string.Empty, LastName = string.Empty, FormattedAccountNumber = string.Empty;
                    if (ObjtblCompany != null)
                    {
                        AccountNumber = ObjtblCompany.FirstOrDefault().CompanyAccountNumber;
                        FirstName = ObjtblCompany.FirstOrDefault().FirstName;
                        LastName = ObjtblCompany.FirstOrDefault().LastName;
                        FormattedAccountNumber = " " + AccountNumber;
                    }
                    #endregion

                    #region CompanyInfo


                    if (FirstName != "" && LastName != "" || AccountNumber != "")
                    {
                        SalesRepCompAccount = "Sales Rep: " + FirstName + " " + LastName + " " + " - Account# " + AccountNumber;
                    }
                    //ObjMainHeading.CompanyDisplay = sCompanyDisplay;
                    //ObjMainHeading.IsMainHeading = true;
                    //ObjMainHeading.CompanyName = SalesRepCompAccount;
                    //ObjBALReportsByLocation.Add(ObjMainHeading);
                }
                if (sLocation != ObjBALCompanyLocations.ElementAt(iRow).LocationCity.Trim())
                {
                    BALReportsByLocation ObjCompany = new BALReportsByLocation();

                    sLocation = ObjBALCompanyLocations.ElementAt(iRow).LocationCity.Trim();
                    sLocationDisplay = ObjBALCompanyLocations.ElementAt(iRow).LocationCity + " (phone: " +
                                       ObjBALCompanyLocations.ElementAt(iRow).PhoneNo1 + " | email: " +
                                       ObjBALCompanyLocations.ElementAt(iRow).MainEmailId + " | member since: " +
                                       ObjBALCompanyLocations.ElementAt(iRow).LocationCreatedDate + ")";

                    if (ObjBALCompanyLocations.ElementAt(iRow).IsHome == true)
                    {
                        //if (ObjBALCompanyLocations.ElementAt(iRow).pkCompanyId == new Guid("7DC87D77-6BB5-4BB3-9837-FA861F36F281"))

                        if (ObjBALCompanyLocations.ElementAt(iRow).pkCompanyId == 9514)
                        {
                            ObjCompany.IsCompanyIcon = true;
                        }
                        else
                        {
                            ObjCompany.IsHomeIcon = true;
                        }
                    }
                    ObjCompany.CompanyDisplay = sCompanyDisplay;
                    ObjCompany.CompanyName = SalesRepCompAccount;
                    ObjCompany.LocationDisplayName = sLocationDisplay;
                    ObjCompany.IsCompany = true;
                    ObjBALReportsByLocation.Add(ObjCompany);
                }
                else
                {
                    BALReportsByLocation ObjCompany = new BALReportsByLocation();

                    sLocationDisplay = ObjBALCompanyLocations.ElementAt(iRow).LocationCity + " (phone: " +
                                       ObjBALCompanyLocations.ElementAt(iRow).PhoneNo1 + " | email: " +
                                       ObjBALCompanyLocations.ElementAt(iRow).MainEmailId + " | member since: " +
                                       ObjBALCompanyLocations.ElementAt(iRow).LocationCreatedDate + ")";


                    if (ObjBALCompanyLocations.ElementAt(iRow).IsHome == true)
                    {
                        //if (ObjBALCompanyLocations.ElementAt(iRow).pkCompanyId == new Guid("7DC87D77-6BB5-4BB3-9837-FA861F36F281"))
                        if (ObjBALCompanyLocations.ElementAt(iRow).pkCompanyId == 9514)
                        {
                            ObjCompany.IsCompanyIcon = true;
                        }
                        else
                        {
                            ObjCompany.IsHomeIcon = true;
                        }
                    }
                    ObjCompany.CompanyDisplay = sCompanyDisplay;
                    ObjCompany.CompanyName = SalesRepCompAccount;
                    ObjCompany.LocationDisplayName = sLocationDisplay;
                    ObjCompany.IsCompany = true;
                    ObjBALReportsByLocation.Add(ObjCompany);
                }
                    #endregion CompanyInfo
                List<BALProductsPerLocation> ObjProducts = ObjBALProductsPerLocation.Where(t => t.fkLocationId == ObjBALCompanyLocations.ElementAt(iRow).pkLocationId && t.fkReportCategoryId != (byte)ReportCategories.DrugTesting && t.IsManualReport == false).ToList();
                List<BALPackagesPrice> ObjPackagePrice = ObjBalPackagePrice.Where(data => data.fkLocationId == ObjBALCompanyLocations.ElementAt(iRow).pkLocationId).ToList();

                #region Reports Details for current company

                if (ObjProducts.Count > 0)
                {
                    for (int iRowLvl = 0; iRowLvl < ObjProducts.Count; iRowLvl++)
                    {
                        BALReportsByLocation ObjProductData = new BALReportsByLocation();
                        decimal TotalReportAmount = ObjProducts.ElementAt(iRowLvl).TotalReports * ObjProducts.ElementAt(iRowLvl).ReportAmount;

                        ObjProductData.CompanyDisplay = sCompanyDisplay;
                        ObjProductData.CompanyName = SalesRepCompAccount;
                        ObjProductData.ProductDisplayName = ObjProducts.ElementAt(iRowLvl).ProductDisplayName;
                        ObjProductData.TotalReports = ObjProducts.ElementAt(iRowLvl).TotalReports;
                        ObjProductData.TotalOrderedReports = ObjProducts.ElementAt(iRowLvl).TotalReports;
                        ObjProductData.ReportAmount = ObjProducts.ElementAt(iRowLvl).ReportAmount;
                        ObjProductData.TotalReportAmount = TotalReportAmount;
                        ObjProductData.IsProduct = true;
                        ObjProductData.IsTieredIcon = false;
                        ObjProductData.LocationDisplayName = sLocationDisplay;
                        ObjBALReportsByLocation.Add(ObjProductData);


                        if (ObjProducts.ElementAt(iRowLvl).ProductCode.ToLower() == "mvr")
                        {
                            List<BALReportsFee> ReportFee = ObjReportFee.Where(t => t.fkLocationId == ObjProducts.ElementAt(iRowLvl).fkLocationId && t.ProductCode.ToLower() == "mvr" && t.ReportAmount == ObjProducts.ElementAt(iRowLvl).ReportAmount).ToList();

                            for (int iRowLvl_ = 0; iRowLvl_ < ReportFee.Count; iRowLvl_++)
                            {
                                BALReportsByLocation ObjSubProduct = new BALReportsByLocation();
                                dMVRAmt += ReportFee.ElementAt(iRowLvl_).ReportFee;
                                if (ReportFee.ElementAt(iRowLvl_).ReportFee > 0)
                                {
                                    ObjSubProduct.CompanyDisplay = sCompanyDisplay;
                                    ObjSubProduct.CompanyName = SalesRepCompAccount;
                                    ObjSubProduct.ProductDisplayName = ReportFee.ElementAt(iRowLvl_).MVRStateCode;
                                    ObjSubProduct.TotalReports = ReportFee.ElementAt(iRowLvl_).TotalReports;
                                    ObjSubProduct.ReportAmount = ReportFee.ElementAt(iRowLvl_).AdditionalFee;
                                    ObjSubProduct.TotalReportAmount = ReportFee.ElementAt(iRowLvl_).ReportFee;
                                    ObjSubProduct.IsSubProduct = true;
                                    ObjSubProduct.IsTieredIcon = false;
                                    ObjSubProduct.LocationDisplayName = sLocationDisplay;
                                    ObjBALReportsByLocation.Add(ObjSubProduct);
                                }

                            }
                        }
                        if (ObjProducts.ElementAt(iRowLvl).ProductCode.ToLower() == "scr")
                        {
                            List<BALReportsFee> ReportFee = ObjReportFee.Where(t => t.fkLocationId == ObjProducts.ElementAt(iRowLvl).fkLocationId && t.ProductCode.ToLower() == "scr" && t.ReportAmount == ObjProducts.ElementAt(iRowLvl).ReportAmount).ToList();

                            for (int iRowLvl_ = 0; iRowLvl_ < ReportFee.Count; iRowLvl_++)
                            {
                                BALReportsByLocation ObjSubProduct = new BALReportsByLocation();
                                dMVRAmt += ReportFee.ElementAt(iRowLvl_).ReportFee;
                                if (ReportFee.ElementAt(iRowLvl_).ReportFee > 0)
                                {
                                    ObjSubProduct.CompanyDisplay = sCompanyDisplay;
                                    ObjSubProduct.CompanyName = SalesRepCompAccount;
                                    ObjSubProduct.ProductDisplayName = ReportFee.ElementAt(iRowLvl_).SCRStateCode;
                                    ObjSubProduct.TotalReports = ReportFee.ElementAt(iRowLvl_).TotalReports;
                                    ObjSubProduct.ReportAmount = ReportFee.ElementAt(iRowLvl_).AdditionalFee;
                                    ObjSubProduct.TotalReportAmount = ReportFee.ElementAt(iRowLvl_).ReportFee;
                                    ObjSubProduct.IsSubProduct = true;
                                    ObjSubProduct.IsTieredIcon = false;
                                    ObjSubProduct.LocationDisplayName = sLocationDisplay;
                                    ObjBALReportsByLocation.Add(ObjSubProduct);
                                }
                            }
                        }

                        if (ObjProducts.ElementAt(iRowLvl).ProductCode.ToLower() == "ccr1")
                        {
                            List<BALReportsFee> ReportFee = ObjReportFee.Where(t => t.fkLocationId == ObjProducts.ElementAt(iRowLvl).fkLocationId && t.ProductCode.ToLower() == "ccr1" && t.ReportAmount == ObjProducts.ElementAt(iRowLvl).ReportAmount).ToList();

                            for (int iRowLvl_ = 0; iRowLvl_ < ReportFee.Count; iRowLvl_++)
                            {
                                BALReportsByLocation ObjSubProduct = new BALReportsByLocation();
                                dMVRAmt += ReportFee.ElementAt(iRowLvl_).ReportFee;
                                if (ReportFee.ElementAt(iRowLvl_).ReportFee > 0)
                                {
                                    ObjSubProduct.CompanyDisplay = sCompanyDisplay;
                                    ObjSubProduct.CompanyName = SalesRepCompAccount;
                                    ObjSubProduct.ProductDisplayName = ReportFee.ElementAt(iRowLvl_).CCR1CountyCode;
                                    ObjSubProduct.TotalReports = ReportFee.ElementAt(iRowLvl_).TotalReports;
                                    ObjSubProduct.ReportAmount = ReportFee.ElementAt(iRowLvl_).AdditionalFee;
                                    ObjSubProduct.TotalReportAmount = ReportFee.ElementAt(iRowLvl_).ReportFee;
                                    ObjSubProduct.IsSubProduct = true;
                                    ObjSubProduct.IsTieredIcon = false;
                                    ObjSubProduct.LocationDisplayName = sLocationDisplay;
                                    ObjBALReportsByLocation.Add(ObjSubProduct);
                                }
                            }
                        }

                        if (ObjProducts.ElementAt(iRowLvl).ProductCode.ToLower() == "ccr2")
                        {
                            List<BALReportsFee> ReportFee = ObjReportFee.Where(t => t.fkLocationId == ObjProducts.ElementAt(iRowLvl).fkLocationId && t.ProductCode.ToLower() == "ccr2" && t.ReportAmount == ObjProducts.ElementAt(iRowLvl).ReportAmount).ToList();

                            for (int iRowLvl_ = 0; iRowLvl_ < ReportFee.Count; iRowLvl_++)
                            {
                                BALReportsByLocation ObjSubProduct = new BALReportsByLocation();
                                //  dMVRAmt += ReportFee.ElementAt(iRowLvl_).ReportFee;
                                if (ReportFee.ElementAt(iRowLvl_).ReportFee > 0)
                                {
                                    ObjSubProduct.CompanyDisplay = sCompanyDisplay;
                                    ObjSubProduct.CompanyName = SalesRepCompAccount;
                                    ObjSubProduct.ProductDisplayName = ReportFee.ElementAt(iRowLvl_).CCR2CountyCode;
                                    ObjSubProduct.TotalReports = ReportFee.ElementAt(iRowLvl_).TotalReports;
                                    ObjSubProduct.ReportAmount = ReportFee.ElementAt(iRowLvl_).AdditionalFee;
                                    ObjSubProduct.TotalReportAmount = ReportFee.ElementAt(iRowLvl_).ReportFee;
                                    ObjSubProduct.IsSubProduct = true;
                                    ObjSubProduct.IsTieredIcon = false;
                                    ObjSubProduct.LocationDisplayName = sLocationDisplay;
                                    ObjBALReportsByLocation.Add(ObjSubProduct);
                                }
                            }
                        }
                        if (ObjProducts.ElementAt(iRowLvl).ProductCode.ToLower() == "rcx")
                        {
                            List<BALReportsFee> ReportFee = ObjReportFee.Where(t => t.fkLocationId == ObjProducts.ElementAt(iRowLvl).fkLocationId && t.ProductCode.ToLower() == "rcx" && t.ReportAmount == ObjProducts.ElementAt(iRowLvl).ReportAmount).ToList();

                            for (int iRowLvl_ = 0; iRowLvl_ < ReportFee.Count; iRowLvl_++)
                            {
                                BALReportsByLocation ObjSubProduct = new BALReportsByLocation();
                                //dMVRAmt += ReportFee.ElementAt(iRowLvl_).ReportFee;
                                if (ReportFee.ElementAt(iRowLvl_).ReportFee > 0)
                                {
                                    ObjSubProduct.CompanyDisplay = sCompanyDisplay;
                                    ObjSubProduct.CompanyName = SalesRepCompAccount;
                                    ObjSubProduct.ProductDisplayName = ReportFee.ElementAt(iRowLvl_).RCXCountyCode;
                                    ObjSubProduct.TotalReports = ReportFee.ElementAt(iRowLvl_).TotalReports;
                                    ObjSubProduct.ReportAmount = ReportFee.ElementAt(iRowLvl_).AdditionalFee;
                                    ObjSubProduct.TotalReportAmount = ReportFee.ElementAt(iRowLvl_).ReportFee;
                                    ObjSubProduct.IsSubProduct = true;
                                    ObjSubProduct.IsTieredIcon = false;
                                    ObjSubProduct.LocationDisplayName = sLocationDisplay;
                                    ObjBALReportsByLocation.Add(ObjSubProduct);
                                }
                            }
                        }
                        if (ObjProducts.ElementAt(iRowLvl).ProductCode.ToLower() == "fcr")
                        {
                            List<BALReportsFee> ReportFee = ObjReportFee.Where(t => t.fkLocationId == ObjProducts.ElementAt(iRowLvl).fkLocationId && t.ProductCode.ToLower() == "fcr" && t.ReportAmount == ObjProducts.ElementAt(iRowLvl).ReportAmount).ToList();

                            for (int iRowLvl_ = 0; iRowLvl_ < ReportFee.Count; iRowLvl_++)
                            {
                                BALReportsByLocation ObjSubProduct = new BALReportsByLocation();
                                //dMVRAmt += ReportFee.ElementAt(iRowLvl_).ReportFee;
                                if (ReportFee.ElementAt(iRowLvl_).ReportFee > 0)
                                {
                                    ObjSubProduct.CompanyDisplay = sCompanyDisplay;
                                    ObjSubProduct.CompanyName = SalesRepCompAccount;
                                    ObjSubProduct.ProductDisplayName = ReportFee.ElementAt(iRowLvl_).FCRCountyCode;
                                    ObjSubProduct.TotalReports = ReportFee.ElementAt(iRowLvl_).TotalReports;
                                    ObjSubProduct.ReportAmount = ReportFee.ElementAt(iRowLvl_).AdditionalFee;
                                    ObjSubProduct.TotalReportAmount = ReportFee.ElementAt(iRowLvl_).ReportFee;
                                    ObjSubProduct.IsSubProduct = true;
                                    ObjSubProduct.IsTieredIcon = false;
                                    ObjSubProduct.LocationDisplayName = sLocationDisplay;
                                    ObjBALReportsByLocation.Add(ObjSubProduct);
                                }
                            }
                        }
                        if (ObjProducts.ElementAt(iRowLvl).ProductCode.ToLower() == "edv" || ObjProducts.ElementAt(iRowLvl).ProductCode.ToLower() == "emv" || ObjProducts.ElementAt(iRowLvl).ProductCode.ToLower() == "plv" || ObjProducts.ElementAt(iRowLvl).ProductCode.ToLower() == "wcv" || ObjProducts.ElementAt(iRowLvl).ProductCode.ToLower() == "prv")
                        {
                            string PCode = ObjProducts.ElementAt(iRowLvl).ProductCode.ToLower();
                            List<BALReportsFee> ReportFee = ObjReportFee.Where(t => t.fkLocationId == ObjProducts.ElementAt(iRowLvl).fkLocationId && t.ProductCode.ToLower() == PCode && t.ReportAmount == ObjProducts.ElementAt(iRowLvl).ReportAmount).ToList();

                            for (int iRowLvl_ = 0; iRowLvl_ < ReportFee.Count; iRowLvl_++)
                            {
                                if (ReportFee.ElementAt(iRowLvl_).AdditionalFee > 0)
                                {
                                    BALReportsByLocation ObjSubProduct = new BALReportsByLocation();
                                    //dMVRAmt += ReportFee.ElementAt(iRowLvl_).ReportFee;
                                    //if (ReportFee.ElementAt(iRowLvl_).ReportFee > 0)
                                    //{
                                    ObjSubProduct.CompanyDisplay = sCompanyDisplay;
                                    ObjSubProduct.CompanyName = SalesRepCompAccount;
                                    ObjSubProduct.ProductDisplayName = "Third Party Fee";
                                    ObjSubProduct.TotalReports = ReportFee.ElementAt(iRowLvl_).TotalReports;
                                    ObjSubProduct.ReportAmount = ReportFee.ElementAt(iRowLvl_).AdditionalFee;
                                    ObjSubProduct.TotalReportAmount = ReportFee.ElementAt(iRowLvl_).ReportFee;
                                    ObjSubProduct.IsSubProduct = true;
                                    ObjSubProduct.IsTieredIcon = false;
                                    ObjSubProduct.LocationDisplayName = sLocationDisplay;
                                    ObjBALReportsByLocation.Add(ObjSubProduct);
                                    //}
                                }
                            }
                        }
                    }
                }
                #endregion

                #region Package Details For Current Company

                if (ObjPackagePrice.Count > 0)
                {
                    decimal TotalPackageAmount = 0;
                    for (int iRowLvl = 0; iRowLvl < ObjPackagePrice.Count; iRowLvl++)
                    {
                        BALReportsByLocation ObjCurrentCompany = new BALReportsByLocation();

                        TotalPackageAmount = ObjPackagePrice.ElementAt(iRowLvl).TotalPackages * ObjPackagePrice.ElementAt(iRowLvl).ReportAmount;

                        ObjCurrentCompany.CompanyDisplay = sCompanyDisplay;
                        ObjCurrentCompany.CompanyName = SalesRepCompAccount;
                        ObjCurrentCompany.ProductDisplayName = ObjPackagePrice.ElementAt(iRowLvl).PackageName + " [" + ObjPackagePrice.ElementAt(iRowLvl).PackageInclude + "]";
                        bool IsTieredStatus = false;
                        using (EmergeDALDataContext DX = new EmergeDALDataContext())
                        {
                            //var IsTieredStatus1 = DX.tblProductPackages.Join(DX.tblTieredPackages, PP => PP.pkPackageId, TP => TP.fkPackageId,
                            //                                                   (PP, TP) => new
                            //                                                   {
                            //                                                       PP,
                            //                                                       TP
                            //                                                   }).Where(n => n.PP.pkPackageId == ObjPackagePrice.ElementAt(iRowLvl).fkPackageId && n.TP.Tired2Price > 0).ToList();//Applied a check for the Tired2Price Data if Tired2Price>0 INT-110
                            //IsTieredStatus = IsTieredStatus1.Count > 0 ? true : IsTieredStatus;

                            var IsTieredStatus1 = DX.tblProductPackages.Join(DX.tblTieredPackages, PP => PP.pkPackageId, TP => TP.fkPackageId,
                                                                            (PP, TP) => new
                                                                            {
                                                                                PP,
                                                                                TP
                                                                            }).Where(n => n.PP.pkPackageId == ObjPackagePrice.ElementAt(iRowLvl).fkPackageId).ToList();
                            IsTieredStatus = IsTieredStatus1.Count > 0 ? true : IsTieredStatus;
                        }
                        ObjCurrentCompany.IsTieredIcon = IsTieredStatus;
                        ObjCurrentCompany.TotalReports = ObjPackagePrice.ElementAt(iRowLvl).TotalPackages;
                        ObjCurrentCompany.ReportAmount = ObjPackagePrice.ElementAt(iRowLvl).ReportAmount;
                        ObjCurrentCompany.TotalReportAmount = TotalPackageAmount;
                        ObjCurrentCompany.IsProduct = true;
                        ObjCurrentCompany.LocationDisplayName = sLocationDisplay;
                        ObjCurrentCompany.TotalOrderedReports = ObjPackagePrice.ElementAt(iRowLvl).TotalPackages;
                        ObjBALReportsByLocation.Add(ObjCurrentCompany);
                        if (IsTieredStatus)
                        {
                            bool Is6Month = false;
                            DateTime OrderDate = DateTime.Parse(Month + "/1/" + Year + " 0:00:00 PM");
                            DateTime SixMonthOldDate = DateTime.Now.AddMonths(-6);
                            DateTime startOfMonth = new DateTime(SixMonthOldDate.Year, SixMonthOldDate.Month, 1);
                            bool MVRProcessed = false;
                            if (OrderDate < startOfMonth)
                            {
                                Is6Month = true;
                            }
                            using (EmergeDALDataContext DX = new EmergeDALDataContext())
                            {
                                if (Is6Month == false)
                                {
                                    //var Tiered2Reports = DX.tblOrderDetails.Where(db => db.fkPackageId == ObjPackagePrice.ElementAt(iRowLvl).fkPackageId && db.IsTieredReport == true && Convert.ToDateTime(db.OrderDt).Month == Month && Convert.ToDateTime(db.OrderDt).Year == Year).ToList();


                                    List<Billing_Tiered2Reports_procResult> Tiered2Reports = null;
                                    using (EmergeDALDataContext DX1 = new EmergeDALDataContext())
                                    {

                                        Tiered2Reports = DX1.Billing_Tiered2Reports_proc(ObjPackagePrice.ElementAt(iRowLvl).fkPackageId, Month, Year, Convert.ToInt32(ObjBALCompanyLocations[iRow].pkLocationId)).ToList();

                                    }




                                    List<Billing_TieredOptionalReports_procResult> TieredOptionalReports = null;
                                    using (EmergeDALDataContext DX2 = new EmergeDALDataContext())
                                    {

                                        TieredOptionalReports = DX2.Billing_TieredOptionalReports_proc(ObjPackagePrice.ElementAt(iRowLvl).fkPackageId, Month, Year, Convert.ToInt32(ObjBALCompanyLocations[iRow].pkLocationId)).ToList();

                                    }







                                    // var TieredOptionalReports = DX.tblOrderDetails.Where(db => db.fkPackageId == ObjPackagePrice.ElementAt(iRowLvl).fkPackageId && db.IsOptionalReport == true && Convert.ToDateTime(db.OrderDt).Month == Month && Convert.ToDateTime(db.OrderDt).Year == Year).ToList();







                                    if (Tiered2Reports != null && Tiered2Reports.Count() > 0)
                                    {
                                        BALReportsByLocation ObjTieredProduct = new BALReportsByLocation();
                                        ObjTieredProduct.CompanyDisplay = sCompanyDisplay;
                                        ObjTieredProduct.CompanyName = SalesRepCompAccount;
                                        if (Tiered2Reports.ElementAt(0).fkproductapplicationid == CCR1)
                                        {
                                            ObjTieredProduct.ProductDisplayName = "Tier2 CCR1";
                                        }
                                        else if (Tiered2Reports.ElementAt(0).fkproductapplicationid == SCR)
                                        {
                                            ObjTieredProduct.ProductDisplayName = "Tier2 SCR";
                                        }
                                        else if (Tiered2Reports.ElementAt(0).fkproductapplicationid == FCR)
                                        {
                                            ObjTieredProduct.ProductDisplayName = "Tier2 FCR";
                                        }
                                        else
                                        {
                                            ObjTieredProduct.ProductDisplayName = "Tier2";
                                        }
                                        ObjTieredProduct.TotalReports = Tiered2Reports.Count();
                                        ObjTieredProduct.ReportAmount = Tiered2Reports.ElementAt(0).ReportAmount == null ? decimal.Parse("0") : decimal.Parse(Tiered2Reports.ElementAt(0).ReportAmount.ToString());
                                        ObjTieredProduct.TotalReportAmount = Tiered2Reports.Count() * (Tiered2Reports.ElementAt(0).ReportAmount == null ? decimal.Parse("0") : decimal.Parse(Tiered2Reports.ElementAt(0).ReportAmount.ToString()));
                                        ObjTieredProduct.IsSubProduct = true;
                                        ObjTieredProduct.IsTieredIcon = false;
                                        ObjTieredProduct.LocationDisplayName = sLocationDisplay;
                                        ObjBALReportsByLocation.Add(ObjTieredProduct);
                                        //var listProductWithFee = Tiered2Reports.GroupBy(db => new { db.CountyCode, db.StateCode, db.AdditionalFee, db.fkProductApplicationId }).Select(group =>
                                        //                                                                            new
                                        //                                                                            {
                                        //                                                                                CountyCode = group.Key.CountyCode,
                                        //                                                                                StateCode = group.Key.StateCode,
                                        //                                                                                AdditionalFee = group.Key.AdditionalFee,
                                        //                                                                                fkProductApplicationId = group.Key.fkProductApplicationId,
                                        //                                                                                Count = group.Count()
                                        //                                                                            });

                                        List<BillingStatment_AdditionalFeesValueResult> listProductWithFee = null;
                                        using (EmergeDALDataContext DX1 = new EmergeDALDataContext())
                                        {

                                            //listProductWithFee = DX1.BillingStatment_AdditionalFeesValue(Tiered2Reports.ElementAt(0).fkCompanyId.Value, Tiered2Reports.ElementAt(0).OrderDt.Value.Month, Tiered2Reports.ElementAt(0).OrderDt.Value.Year, Tiered2Reports.ElementAt(0).fkPackageId, Tiered2Reports.ElementAt(0).fkLocationId.Value, Tiered2Reports.ElementAt(0).fkProductApplicationId.Value, 0).ToList();
                                            listProductWithFee = DX1.BillingStatment_AdditionalFeesValue(Tiered2Reports.ElementAt(0).fkcompanyid.Value, Tiered2Reports.ElementAt(0).OrderDt.Value.Month, Tiered2Reports.ElementAt(0).OrderDt.Value.Year, Tiered2Reports.ElementAt(0).fkpackageid, Tiered2Reports.ElementAt(0).fklocationid.Value, Tiered2Reports.ElementAt(0).fkproductapplicationid.Value, RefCode, 0).ToList();



                                        }


                                        int PKGTotalReports = 0;
                                        decimal PKGAdditionalFee = 0.00m;
                                        bool IsMVRCheck = false;
                                        BALReportsByLocation ObjSubProductMVR = new BALReportsByLocation();
                                        string ProductAddionalFee = "";
                                        for (int i = 0; i < listProductWithFee.Count(); i++)
                                        {
                                            if (Convert.ToDecimal(listProductWithFee[i].additionalfee) > 0.00m)
                                            {
                                                BALReportsByLocation ObjSubProduct = new BALReportsByLocation();

                                                ProductAddionalFee = "";
                                                if (listProductWithFee.ElementAt(i).fkproductapplicationid == CCR1)
                                                {
                                                    IsMVRCheck = false;
                                                    ProductAddionalFee = "CCR1 County Fee (" + listProductWithFee[i].countycode + ")";
                                                }
                                                if (listProductWithFee.ElementAt(i).fkproductapplicationid == SCR)
                                                {
                                                    IsMVRCheck = false;
                                                    ProductAddionalFee = "SCR State Fee (" + listProductWithFee[i].statecode + ")";
                                                }
                                                if (listProductWithFee.ElementAt(i).fkproductapplicationid == FCR)
                                                {
                                                    IsMVRCheck = false;
                                                    ProductAddionalFee = "Jurisdiction Fee (" + listProductWithFee[i].countycode + ")";
                                                }
                                                if (listProductWithFee.ElementAt(i).fkproductapplicationid == 27)
                                                {
                                                    ProductAddionalFee = "MVR State Fee";
                                                    IsMVRCheck = true;
                                                    ObjSubProductMVR.TotalReports = 0;
                                                    PKGTotalReports += short.Parse(listProductWithFee[i].count.ToString());
                                                    PKGAdditionalFee += Convert.ToInt32(listProductWithFee[i].count) * Convert.ToDecimal(listProductWithFee[i].additionalfee.Value);
                                                    ObjSubProductMVR.IsSubProduct = true;
                                                    ObjSubProductMVR.IsTieredIcon = false;
                                                }
                                                if (!IsMVRCheck)
                                                {
                                                    ObjSubProduct.CompanyDisplay = sCompanyDisplay;
                                                    ObjSubProduct.CompanyName = SalesRepCompAccount;
                                                    ObjSubProduct.ProductDisplayName = ProductAddionalFee;
                                                    ObjSubProduct.TotalReports = Convert.ToInt32(listProductWithFee[i].count);
                                                    ObjSubProduct.ReportAmount = Convert.ToDecimal(listProductWithFee[i].additionalfee);
                                                    ObjSubProduct.TotalReportAmount = Convert.ToDecimal(listProductWithFee[i].count * listProductWithFee[i].additionalfee);
                                                    ObjSubProduct.IsSubProduct = true;
                                                    ObjSubProduct.IsTieredIcon = false;
                                                    ObjSubProduct.LocationDisplayName = sLocationDisplay;
                                                    ObjBALReportsByLocation.Add(ObjSubProduct);
                                                }
                                            }
                                        }
                                        if (IsMVRCheck)
                                        {
                                            ObjSubProductMVR.CompanyDisplay = sCompanyDisplay;
                                            ObjSubProductMVR.CompanyName = SalesRepCompAccount;
                                            ObjSubProductMVR.ProductDisplayName = ProductAddionalFee;

                                            if (PKGTotalReports == 0 && PKGAdditionalFee > 0)
                                            {
                                                ObjSubProductMVR.TotalReports = 1;
                                                ObjSubProductMVR.TotalReportAmount = Convert.ToDecimal(1 * PKGAdditionalFee);
                                            }
                                            else
                                            {
                                                ObjSubProductMVR.TotalReports = PKGTotalReports;
                                                ObjSubProductMVR.TotalReportAmount = Convert.ToDecimal(PKGAdditionalFee);
                                            }
                                            ObjSubProductMVR.ProductQty = short.Parse(PKGTotalReports.ToString());
                                            ObjSubProductMVR.ReportAmount = PKGAdditionalFee;
                                            ObjSubProductMVR.IsSubProduct = true;
                                            ObjSubProductMVR.IsTieredIcon = false;
                                            ObjSubProductMVR.LocationDisplayName = sLocationDisplay;
                                            ObjBALReportsByLocation.Add(ObjSubProductMVR);
                                            IsMVRCheck = false;
                                            MVRProcessed = true;
                                            //ObjSubProductMVR.ProductDisplayName = "MVR State Fee";
                                            //ObjSubProductMVR.ProductQty = short.Parse(PKGTotalReports.ToString());
                                            //ObjSubProductMVR.ReportAmount = PKGAdditionalFee;
                                            //ObjBALReportsByLocation.Add(ObjSubProductMVR);
                                            //IsMVRCheck = false;
                                            //MVRProcessed = true;

                                        }
                                    }
                                    if (TieredOptionalReports != null && TieredOptionalReports.Count() > 0)
                                    {
                                        BALReportsByLocation ObjTieredOptionalProduct = new BALReportsByLocation();
                                        ObjTieredOptionalProduct.CompanyDisplay = sCompanyDisplay;
                                        ObjTieredOptionalProduct.CompanyName = SalesRepCompAccount;
                                        if (TieredOptionalReports.ElementAt(0).fkproductapplicationid == CCR1)
                                        {
                                            ObjTieredOptionalProduct.ProductDisplayName = "Optional CCR1";
                                        }
                                        else if (TieredOptionalReports.ElementAt(0).fkproductapplicationid == SCR)
                                        {
                                            ObjTieredOptionalProduct.ProductDisplayName = "Optional SCR";
                                        }
                                        else if (TieredOptionalReports.ElementAt(0).fkproductapplicationid == FCR)
                                        {
                                            ObjTieredOptionalProduct.ProductDisplayName = "Optional FCR";
                                        }
                                        else
                                        {
                                            ObjTieredOptionalProduct.ProductDisplayName = "Optional";
                                        }
                                        ObjTieredOptionalProduct.TotalReports = TieredOptionalReports.Count();
                                        ObjTieredOptionalProduct.ReportAmount = TieredOptionalReports.ElementAt(0).ReportAmount == null ? decimal.Parse("0") : decimal.Parse(TieredOptionalReports.ElementAt(0).ReportAmount.ToString());
                                        ObjTieredOptionalProduct.TotalReportAmount = TieredOptionalReports.Count() * (TieredOptionalReports.ElementAt(0).ReportAmount == null ? decimal.Parse("0") : decimal.Parse(TieredOptionalReports.ElementAt(0).ReportAmount.ToString()));
                                        ObjTieredOptionalProduct.IsSubProduct = true;
                                        ObjTieredOptionalProduct.IsTieredIcon = false;
                                        ObjTieredOptionalProduct.LocationDisplayName = sLocationDisplay;
                                        ObjBALReportsByLocation.Add(ObjTieredOptionalProduct);


                                        //var listProductWithFee = TieredOptionalReports.GroupBy(db => new { db.CountyCode, db.StateCode, db.AdditionalFee, db.fkProductApplicationId }).Select(group =>
                                        //                                                                            new
                                        //                                                                            {
                                        //                                                                                CountyCode = group.Key.CountyCode,
                                        //                                                                                StateCode = group.Key.StateCode,
                                        //                                                                                AdditionalFee = group.Key.AdditionalFee,
                                        //                                                                                fkProductApplicationId = group.Key.fkProductApplicationId,
                                        //                                                                                Count = group.Count()
                                        //                                                                            });


                                        List<BillingStatment_AdditionalFeesValueResult> listProductWithFee = null;
                                        using (EmergeDALDataContext DX1 = new EmergeDALDataContext())
                                        {

                                            //changes for 22 08 2015
                                            listProductWithFee = DX1.BillingStatment_AdditionalFeesValue(Tiered2Reports.ElementAt(0).fkcompanyid.Value, Tiered2Reports.ElementAt(0).OrderDt.Value.Month, Tiered2Reports.ElementAt(0).OrderDt.Value.Year, Tiered2Reports.ElementAt(0).fkpackageid, Tiered2Reports.ElementAt(0).fklocationid.Value, Tiered2Reports.ElementAt(0).fkproductapplicationid.Value, RefCode, 1).ToList();


                                        }

                                        for (int i = 0; i < listProductWithFee.Count(); i++)
                                        {
                                            if (Convert.ToDecimal(listProductWithFee[i].additionalfee) > 0.00m)
                                            {
                                                BALReportsByLocation ObjSubProduct = new BALReportsByLocation();

                                                string ProductAddionalFee = "";
                                                if (listProductWithFee.ElementAt(i).fkproductapplicationid == CCR1)
                                                {
                                                    ProductAddionalFee = "CCR1 County Fee (" + listProductWithFee[i].countycode + ")";
                                                }
                                                if (listProductWithFee.ElementAt(i).fkproductapplicationid == SCR)
                                                {
                                                    ProductAddionalFee = "SCR State Fee (" + listProductWithFee[i].statecode + ")";
                                                }
                                                if (listProductWithFee.ElementAt(i).fkproductapplicationid == FCR)
                                                {
                                                    ProductAddionalFee = "Jurisdiction Fee (" + listProductWithFee[i].countycode + ")";
                                                }

                                                ObjSubProduct.CompanyDisplay = sCompanyDisplay;
                                                ObjSubProduct.CompanyName = SalesRepCompAccount;
                                                ObjSubProduct.ProductDisplayName = ProductAddionalFee;
                                                ObjSubProduct.TotalReports = Convert.ToInt32(listProductWithFee[i].count);
                                                ObjSubProduct.ReportAmount = Convert.ToDecimal(listProductWithFee[i].additionalfee);
                                                ObjSubProduct.TotalReportAmount = Convert.ToDecimal(listProductWithFee[i].count * listProductWithFee[i].additionalfee);
                                                ObjSubProduct.IsSubProduct = true;
                                                ObjSubProduct.IsTieredIcon = false;
                                                ObjSubProduct.LocationDisplayName = sLocationDisplay;
                                                ObjBALReportsByLocation.Add(ObjSubProduct);
                                            }
                                        }
                                    }
                                    if (!MVRProcessed)
                                    {
                                        List<BillingStatment_AdditionalFeesValueResult> listProductWithFeeMVR = null;
                                        using (EmergeDALDataContext DX1 = new EmergeDALDataContext())
                                        {
                                            listProductWithFeeMVR = DX1.BillingStatment_AdditionalFeesValue(Convert.ToInt32(ObjBALCompanyLocations.ElementAt(iRow).pkCompanyId), Month, Year, ObjPackagePrice.ElementAt(iRowLvl).fkPackageId, Convert.ToInt32(ObjBALCompanyLocations.ElementAt(iRow).pkLocationId), 27, RefCode, 0).ToList();
                                            if (listProductWithFeeMVR.Count() > 0)
                                            {
                                                int PKGTotalReportsNonTiered = 0;
                                                decimal PKGAdditionalFeeNonTiered = 0.00m;
                                                BALReportsByLocation ObjSubProductNonTieredMVR = new BALReportsByLocation();
                                                for (int i = 0; i < listProductWithFeeMVR.Count(); i++)
                                                {
                                                    if (listProductWithFeeMVR.ElementAt(i).fkproductapplicationid == 27)
                                                    {

                                                        ObjSubProductNonTieredMVR.TotalReports = 1;
                                                        PKGTotalReportsNonTiered += short.Parse(listProductWithFeeMVR[i].count.ToString());
                                                        if (Convert.ToInt32(listProductWithFeeMVR[i].count)==0)
                                                        {
                                                            PKGAdditionalFeeNonTiered += 1 * Convert.ToDecimal(listProductWithFeeMVR[i].additionalfee.Value);
                                                        }
                                                        else
                                                        { PKGAdditionalFeeNonTiered += Convert.ToInt32(listProductWithFeeMVR[i].count) * Convert.ToDecimal(listProductWithFeeMVR[i].additionalfee.Value); 
                                                        }

                                                        

                                                    }
                                                }
                                                if (listProductWithFeeMVR.Count() > 0 && PKGAdditionalFeeNonTiered > 0.00m)
                                                {
                                                    ObjSubProductNonTieredMVR.CompanyDisplay = sCompanyDisplay;
                                                    ObjSubProductNonTieredMVR.CompanyName = SalesRepCompAccount;
                                                    ObjSubProductNonTieredMVR.ProductDisplayName = "MVR State Fee";
                                                    ObjSubProductNonTieredMVR.TotalReports = short.Parse(PKGTotalReportsNonTiered.ToString());
                                                    ObjSubProductNonTieredMVR.TotalOrderedReports = 1;
                                                    ObjSubProductNonTieredMVR.IsPackage = true;
                                                    ObjSubProductNonTieredMVR.TotalReportAmount = PKGAdditionalFeeNonTiered;
                                                    ObjSubProductNonTieredMVR.ReportAmount = PKGAdditionalFeeNonTiered;
                                                    ObjSubProductNonTieredMVR.LocationDisplayName = sLocationDisplay;
                                                    ObjSubProductNonTieredMVR.IsSubProduct = true;
                                                    ObjSubProductNonTieredMVR.IsTieredIcon = false;
                                                    ObjBALReportsByLocation.Add(ObjSubProductNonTieredMVR);
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    var Tiered2Reports = DX.tblOrderDetails_Archieves.Where(db => db.fkPackageId == ObjPackagePrice.ElementAt(iRowLvl).fkPackageId && db.IsTieredReport == true && Convert.ToDateTime(db.OrderDt).Month == Month && Convert.ToDateTime(db.OrderDt).Year == Year).ToList();
                                    var TieredOptionalReports = DX.tblOrderDetails_Archieves.Where(db => db.fkPackageId == ObjPackagePrice.ElementAt(iRowLvl).fkPackageId && db.IsOptionalReport == true && Convert.ToDateTime(db.OrderDt).Month == Month && Convert.ToDateTime(db.OrderDt).Year == Year).ToList();
                                    if (Tiered2Reports != null && Tiered2Reports.Count() > 0)
                                    {
                                        BALReportsByLocation ObjTieredProduct = new BALReportsByLocation();
                                        ObjTieredProduct.CompanyDisplay = sCompanyDisplay;
                                        ObjTieredProduct.CompanyName = SalesRepCompAccount;
                                        if (Tiered2Reports.ElementAt(0).fkProductApplicationId == CCR1)
                                        {
                                            ObjTieredProduct.ProductDisplayName = "Tier2 CCR1";
                                        }
                                        else if (Tiered2Reports.ElementAt(0).fkProductApplicationId == SCR)
                                        {
                                            ObjTieredProduct.ProductDisplayName = "Tier2 SCR";
                                        }
                                        else if (Tiered2Reports.ElementAt(0).fkProductApplicationId == FCR)
                                        {
                                            ObjTieredProduct.ProductDisplayName = "Tier2 FCR";
                                        }
                                        else
                                        {
                                            ObjTieredProduct.ProductDisplayName = "Tier2";
                                        }
                                        ObjTieredProduct.TotalReports = Tiered2Reports.Count();
                                        ObjTieredProduct.ReportAmount = Tiered2Reports.ElementAt(0).ReportAmount == null ? decimal.Parse("0") : decimal.Parse(Tiered2Reports.ElementAt(0).ReportAmount.ToString());
                                        ObjTieredProduct.TotalReportAmount = Tiered2Reports.Count() * (Tiered2Reports.ElementAt(0).ReportAmount == null ? decimal.Parse("0") : decimal.Parse(Tiered2Reports.ElementAt(0).ReportAmount.ToString()));
                                        ObjTieredProduct.IsSubProduct = true;
                                        ObjTieredProduct.IsTieredIcon = false;
                                        ObjTieredProduct.LocationDisplayName = sLocationDisplay;
                                        ObjBALReportsByLocation.Add(ObjTieredProduct);
                                        var listProductWithFee = Tiered2Reports.GroupBy(db => new { db.CountyCode, db.StateCode, db.AdditionalFee, db.fkProductApplicationId }).Select(group =>
                                                                                                                    new
                                                                                                                    {
                                                                                                                        CountyCode = group.Key.CountyCode,
                                                                                                                        StateCode = group.Key.StateCode,
                                                                                                                        AdditionalFee = group.Key.AdditionalFee,
                                                                                                                        fkProductApplicationId = group.Key.fkProductApplicationId,
                                                                                                                        Count = group.Count()
                                                                                                                    });
                                        for (int i = 0; i < listProductWithFee.Count(); i++)
                                        {
                                            if (Convert.ToDecimal(listProductWithFee.ElementAt(i).AdditionalFee) > 0.00m)
                                            {
                                                BALReportsByLocation ObjSubProduct = new BALReportsByLocation();

                                                string ProductAddionalFee = "";
                                                if (listProductWithFee.ElementAt(i).fkProductApplicationId == CCR1)
                                                {
                                                    ProductAddionalFee = "CCR1 County Fee (" + listProductWithFee.ElementAt(i).CountyCode + ")";
                                                }
                                                if (listProductWithFee.ElementAt(i).fkProductApplicationId == SCR)
                                                {
                                                    ProductAddionalFee = "SCR State Fee (" + listProductWithFee.ElementAt(i).StateCode + ")";
                                                }
                                                if (listProductWithFee.ElementAt(i).fkProductApplicationId == FCR)
                                                {
                                                    ProductAddionalFee = "Jurisdiction Fee (" + listProductWithFee.ElementAt(i).CountyCode + ")";
                                                }

                                                ObjSubProduct.CompanyDisplay = sCompanyDisplay;
                                                ObjSubProduct.CompanyName = SalesRepCompAccount;
                                                ObjSubProduct.ProductDisplayName = ProductAddionalFee;
                                                ObjSubProduct.TotalReports = listProductWithFee.ElementAt(i).Count;
                                                ObjSubProduct.ReportAmount = listProductWithFee.ElementAt(i).AdditionalFee;
                                                ObjSubProduct.TotalReportAmount = listProductWithFee.ElementAt(i).Count * listProductWithFee.ElementAt(i).AdditionalFee;
                                                ObjSubProduct.IsSubProduct = true;
                                                ObjSubProduct.IsTieredIcon = false;
                                                ObjSubProduct.LocationDisplayName = sLocationDisplay;
                                                ObjBALReportsByLocation.Add(ObjSubProduct);
                                            }
                                        }
                                    }
                                    if (TieredOptionalReports != null && TieredOptionalReports.Count() > 0)
                                    {
                                        BALReportsByLocation ObjTieredOptionalProduct = new BALReportsByLocation();
                                        ObjTieredOptionalProduct.CompanyDisplay = sCompanyDisplay;
                                        ObjTieredOptionalProduct.CompanyName = SalesRepCompAccount;
                                        if (TieredOptionalReports.ElementAt(0).fkProductApplicationId == CCR1)
                                        {
                                            ObjTieredOptionalProduct.ProductDisplayName = "Optional CCR1";
                                        }
                                        else if (TieredOptionalReports.ElementAt(0).fkProductApplicationId == SCR)
                                        {
                                            ObjTieredOptionalProduct.ProductDisplayName = "Optional SCR";
                                        }
                                        else if (TieredOptionalReports.ElementAt(0).fkProductApplicationId == FCR)
                                        {
                                            ObjTieredOptionalProduct.ProductDisplayName = "Optional FCR";
                                        }
                                        else
                                        {
                                            ObjTieredOptionalProduct.ProductDisplayName = "Optional";
                                        }
                                        ObjTieredOptionalProduct.TotalReports = TieredOptionalReports.Count();
                                        ObjTieredOptionalProduct.ReportAmount = TieredOptionalReports.ElementAt(0).ReportAmount == null ? decimal.Parse("0") : decimal.Parse(TieredOptionalReports.ElementAt(0).ReportAmount.ToString());
                                        ObjTieredOptionalProduct.TotalReportAmount = TieredOptionalReports.Count() * (TieredOptionalReports.ElementAt(0).ReportAmount == null ? decimal.Parse("0") : decimal.Parse(TieredOptionalReports.ElementAt(0).ReportAmount.ToString()));
                                        ObjTieredOptionalProduct.IsSubProduct = true;
                                        ObjTieredOptionalProduct.IsTieredIcon = false;
                                        ObjTieredOptionalProduct.LocationDisplayName = sLocationDisplay;
                                        ObjBALReportsByLocation.Add(ObjTieredOptionalProduct);
                                        var listProductWithFee = TieredOptionalReports.GroupBy(db => new { db.CountyCode, db.StateCode, db.AdditionalFee, db.fkProductApplicationId }).Select(group =>
                                                                                                                    new
                                                                                                                    {
                                                                                                                        CountyCode = group.Key.CountyCode,
                                                                                                                        StateCode = group.Key.StateCode,
                                                                                                                        AdditionalFee = group.Key.AdditionalFee,
                                                                                                                        fkProductApplicationId = group.Key.fkProductApplicationId,
                                                                                                                        Count = group.Count()
                                                                                                                    });
                                        for (int i = 0; i < listProductWithFee.Count(); i++)
                                        {
                                            if (Convert.ToDecimal(listProductWithFee.ElementAt(i).AdditionalFee) > 0.00m)
                                            {
                                                BALReportsByLocation ObjSubProduct = new BALReportsByLocation();

                                                string ProductAddionalFee = "";
                                                if (listProductWithFee.ElementAt(i).fkProductApplicationId == CCR1)
                                                {
                                                    ProductAddionalFee = "CCR1 County Fee (" + listProductWithFee.ElementAt(i).CountyCode + ")";
                                                }
                                                if (listProductWithFee.ElementAt(i).fkProductApplicationId == SCR)
                                                {
                                                    ProductAddionalFee = "SCR State Fee (" + listProductWithFee.ElementAt(i).StateCode + ")";
                                                }
                                                if (listProductWithFee.ElementAt(i).fkProductApplicationId == FCR)
                                                {
                                                    ProductAddionalFee = "Jurisdiction Fee (" + listProductWithFee.ElementAt(i).CountyCode + ")";
                                                }

                                                ObjSubProduct.CompanyDisplay = sCompanyDisplay;
                                                ObjSubProduct.CompanyName = SalesRepCompAccount;
                                                ObjSubProduct.ProductDisplayName = ProductAddionalFee;
                                                ObjSubProduct.TotalReports = listProductWithFee.ElementAt(i).Count;
                                                ObjSubProduct.ReportAmount = listProductWithFee.ElementAt(i).AdditionalFee;
                                                ObjSubProduct.TotalReportAmount = listProductWithFee.ElementAt(i).Count * listProductWithFee.ElementAt(i).AdditionalFee;
                                                ObjSubProduct.IsSubProduct = true;
                                                ObjSubProduct.IsTieredIcon = false;
                                                ObjSubProduct.LocationDisplayName = sLocationDisplay;
                                                ObjBALReportsByLocation.Add(ObjSubProduct);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (IsTieredStatus == false)
                        {
                            string ProductsInPackage = ObjPackagePrice.ElementAt(iRowLvl).PackageInclude;
                            //string TieredReportProductCode=DX.tblTieredPackages
                            string[] TotalProducts = ProductsInPackage.Split(',');
                            foreach (string ProductCode in TotalProducts)
                            {
                                if (ProductCode.ToLower().Trim() == "edv" || ProductCode.ToLower().Trim() == "emv" || ProductCode.ToLower().Trim() == "mvr" || ProductCode.ToLower().Trim() == "scr" || ProductCode.ToLower().Trim() == "fcr" || ProductCode.ToLower().Trim() == "rcx" || ProductCode.ToLower().Trim() == "ccr1" || ProductCode.ToLower().Trim() == "ccr2")
                                {
                                    // CHANGES ON 04 JULY 2016


                                    List<BALPackageReport> ObjPReports = ObjPackageReport.Where(
                                            d => d.ProductCode == ProductCode.Trim() &&
                                            d.fkLocationId == ObjBALCompanyLocations.ElementAt(iRow).pkLocationId &&
                                            d.fkPackageId == ObjPackagePrice.ElementAt(iRowLvl).fkPackageId &&
                                            d.PackageInclude == ObjPackagePrice.ElementAt(iRowLvl).PackageInclude).ToList();// &&
                                    foreach (BALPackageReport ProductWithFee in ObjPReports)
                                    {
                                        if (Convert.ToDecimal(ProductWithFee.TotalReports * ProductWithFee.AdditionalFee) > 0.00m)
                                        {
                                            BALReportsByLocation ObjSubProduct = new BALReportsByLocation();

                                            string ProductAddionalFee = "";
                                            if (ProductCode.ToLower().Trim() == "mvr")
                                            {
                                                ProductAddionalFee = ProductWithFee.MVRStateCode;
                                            }
                                            if (ProductCode.ToLower().Trim() == "scr")
                                            {
                                                ProductAddionalFee = ProductWithFee.SCRStateCode;
                                            }
                                            if (ProductCode.ToLower().Trim() == "fcr")
                                            {
                                                ProductAddionalFee = ProductWithFee.FCRCountyCode;
                                            }
                                            if (ProductCode.ToLower().Trim() == "ccr1")
                                            {
                                                ProductAddionalFee = ProductWithFee.CCR1CountyCode;
                                            }
                                            if (ProductCode.ToLower().Trim() == "ccr2")
                                            {
                                                ProductAddionalFee = ProductWithFee.CCR2CountyCode;
                                            }
                                            if (ProductCode.ToLower().Trim() == "rcx")
                                            {
                                                ProductAddionalFee = ProductWithFee.RCXCountyCode;
                                            }

                                            if (ProductCode.ToLower().Trim() == "edv")
                                            {
                                                ProductAddionalFee = "Third Party Fee(edv)";
                                            }
                                            if (ProductCode.ToLower().Trim() == "emv")
                                            {
                                                ProductAddionalFee = "Third Party Fee(emv)";
                                            }


                                            ObjSubProduct.CompanyDisplay = sCompanyDisplay;
                                            ObjSubProduct.CompanyName = SalesRepCompAccount;
                                            ObjSubProduct.ProductDisplayName = ProductAddionalFee;
                                            ObjSubProduct.TotalReports = ProductWithFee.TotalReports;
                                            ObjSubProduct.ReportAmount = ProductWithFee.AdditionalFee;
                                            ObjSubProduct.TotalReportAmount = ProductWithFee.TotalReports * ProductWithFee.AdditionalFee;
                                            ObjSubProduct.IsSubProduct = true;
                                            ObjSubProduct.IsTieredIcon = false;
                                            ObjSubProduct.LocationDisplayName = sLocationDisplay;
                                            ObjBALReportsByLocation.Add(ObjSubProduct);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                #region Drug Testing Products Details for current company
                ObjProducts = ObjBALProductsPerLocation.Where(t => t.fkLocationId == ObjBALCompanyLocations.ElementAt(iRow).pkLocationId && t.fkReportCategoryId == (byte)ReportCategories.DrugTesting).ToList();

                if (ObjProducts.Count > 0)
                {
                    for (int iRowLvl = 0; iRowLvl < ObjProducts.Count; iRowLvl++)
                    {
                        BALReportsByLocation ObjDrugTesting = new BALReportsByLocation();

                        decimal TotalReportAmount = ObjProducts.ElementAt(iRowLvl).TotalReports * ObjProducts.ElementAt(iRowLvl).ReportAmount;
                        ObjDrugTesting.CompanyDisplay = sCompanyDisplay;
                        ObjDrugTesting.CompanyName = SalesRepCompAccount;
                        ObjDrugTesting.ProductDisplayName = ObjProducts.ElementAt(iRowLvl).ProductDisplayName;
                        ObjDrugTesting.TotalReports = ObjProducts.ElementAt(iRowLvl).TotalReports;
                        ObjDrugTesting.ReportAmount = ObjProducts.ElementAt(iRowLvl).ReportAmount;
                        ObjDrugTesting.TotalOrderedReports = ObjProducts.ElementAt(iRowLvl).TotalReports;
                        ObjDrugTesting.TotalReportAmount = TotalReportAmount;
                        ObjDrugTesting.IsProduct = true;
                        ObjDrugTesting.LocationDisplayName = sLocationDisplay;
                        ObjBALReportsByLocation.Add(ObjDrugTesting);


                        string PCode = ObjProducts.ElementAt(iRowLvl).ProductCode.ToLower();
                        List<BALReportsFee> ReportFee = ObjReportFee.Where(t => t.fkLocationId == ObjProducts.ElementAt(iRowLvl).fkLocationId && t.ProductCode.ToLower() == PCode && t.ReportAmount == ObjProducts.ElementAt(iRowLvl).ReportAmount).ToList();

                        for (int iRowLvl_ = 0; iRowLvl_ < ReportFee.Count; iRowLvl_++)
                        {
                            if (ReportFee.ElementAt(iRowLvl_).AdditionalFee > 0)
                            {
                                BALReportsByLocation ObjSubProduct = new BALReportsByLocation();
                                ObjSubProduct.CompanyDisplay = sCompanyDisplay;
                                ObjSubProduct.CompanyName = SalesRepCompAccount;
                                ObjSubProduct.ProductDisplayName = "Shipping Cost";
                                ObjSubProduct.TotalReports = ReportFee.ElementAt(iRowLvl_).TotalReports;
                                ObjSubProduct.ReportAmount = ReportFee.ElementAt(iRowLvl_).AdditionalFee;
                                ObjSubProduct.TotalReportAmount = ReportFee.ElementAt(iRowLvl_).AdditionalFee;
                                ObjSubProduct.IsSubProduct = true;
                                ObjSubProduct.LocationDisplayName = sLocationDisplay;
                                ObjBALReportsByLocation.Add(ObjSubProduct);
                                //}
                            }
                        }
                    }
                }
                #endregion

                #region Adjustment Section for current company
                List<BALProductsPerLocation> ObjProductAdjust = ObjBALProductsPerLocation.Where(t => t.fkLocationId == ObjBALCompanyLocations.ElementAt(iRow).pkLocationId && t.IsManualReport == true).ToList();

                if (ObjProductAdjust.Count > 0)
                {
                    for (int iRowLvl = 0; iRowLvl < ObjProductAdjust.Count; iRowLvl++)
                    {
                        BALReportsByLocation ObjAdjustments = new BALReportsByLocation();
                        decimal TotalReportAmount = ObjProductAdjust.ElementAt(iRowLvl).ProductQty * ObjProductAdjust.ElementAt(iRowLvl).ReportAmount;

                        ObjAdjustments.CompanyDisplay = sCompanyDisplay;
                        ObjAdjustments.CompanyName = SalesRepCompAccount;
                        ObjAdjustments.ProductDisplayName = ObjProductAdjust.ElementAt(iRowLvl).ProductDisplayName;
                        ObjAdjustments.TotalReports = ObjProductAdjust.ElementAt(iRowLvl).ProductQty;
                        ObjAdjustments.TotalOrderedReports = ObjProductAdjust.ElementAt(iRowLvl).TotalReports;
                        ObjAdjustments.ReportAmount = ObjProductAdjust.ElementAt(iRowLvl).ReportAmount;
                        ObjAdjustments.TotalReportAmount = TotalReportAmount;
                        ObjAdjustments.IsProduct = true;
                        ObjAdjustments.LocationDisplayName = sLocationDisplay;
                        ObjBALReportsByLocation.Add(ObjAdjustments);
                    }

                }
                #endregion Adjustment Section for current company



            }
            return ObjBALReportsByLocation;
        }

        public List<proc_GetBillingAdjustmentsResult> GetBillingAdjustment(Guid ApplicationId, int BillingMonth, int BillingYear, int CompanyId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return DX.GetBillingAdjustments(ApplicationId, BillingMonth, BillingYear, CompanyId).ToList<proc_GetBillingAdjustmentsResult>();
            }
        }

        public List<proc_GetSalesValuesforGraphResult> GetSalesValuesforGraph(int pkCompanyId, int Month, int Year)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.GetSalesValuesforGraph(pkCompanyId, Month, Year).ToList();
            }
        }

        public List<proc_GetVolumeValuesforGraphResult> GetVolumeValuesforGraph(int pkCompanyId, int Month, int Year)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.GetVolumeValuesforGraph(pkCompanyId, Month, Year).ToList();
            }
        }

        public Tuple<Totals, Totals> GetSalesAndVolume(int pkCompanyId, int Month, int Year)
        {
            string strMonthYear = Month.ToString() + "/" + Year.ToString();
            DateTime MonthYear = Convert.ToDateTime(strMonthYear);

            DateTime ObjDateTime = MonthYear.AddMonths(-11);
            //string XvalueText = "|";
            //decimal ProductMaxValue = 0;
            //decimal CurrentProductMaxValue = 0;
            //decimal ReportMaxValue = 0;
            //decimal CurrentReportMaxValue = 0;
            //decimal TotalMaxValue = 0;
            //decimal CurrentTotalMaxValue = 0;

            //string ProductOldData = "";
            //string ReportOldData = "";
            //string TotalOldData = "";

            decimal TotalSales = 0, ReportSales = 0, ProductSales = 0;
            decimal TotalVolume = 0, ReportVolume = 0, ProductVolume = 0;

            decimal PreviousTotalSales = 0, PreviousReportSales = 0, PreviousProductSales = 0;
            decimal PreviousTotalVolume = 0, PreviousReportVolume = 0, PreviousProductVolume = 0;

            decimal TotalSalesPercentage = 0, ReportSalesPercentage = 0, ProductSalesPercentage = 0;
            decimal TotalVolumePercentage = 0, ReportVolumePercentage = 0, ProductVolumePercentage = 0;

            BALCompany ObjBALCompany = new BALCompany();
            var SalesData = ObjBALCompany.GetSalesValuesforGraphforPdf(pkCompanyId, Convert.ToInt16(Month), Convert.ToInt16(Year));
            for (int i = 0; i < 12; i++)
            {
                if (i == 10)
                {
                    var Obj = SalesData.Where(x => x.OrderMonth == Convert.ToInt16(ObjDateTime.AddMonths(i).Month) && x.OrderYear == Convert.ToInt16(ObjDateTime.AddMonths(i).Year));
                    var ObjPreviousVolumeData = ObjBALCompany.GetVolumeValuesforGraph(pkCompanyId, Convert.ToInt16(ObjDateTime.AddMonths(i).Month), Convert.ToInt16(ObjDateTime.AddMonths(i).Year));
                    PreviousTotalSales = Convert.ToDecimal(Obj.ElementAt(0).TotalAmount);
                    PreviousReportSales = Convert.ToDecimal(Obj.ElementAt(0).ReportAmount);
                    PreviousProductSales = Convert.ToDecimal(Obj.ElementAt(0).ProductAmount);

                    PreviousTotalVolume = Convert.ToDecimal(ObjPreviousVolumeData.ElementAt(0).TotalAmount);
                    PreviousReportVolume = Convert.ToDecimal(ObjPreviousVolumeData.ElementAt(0).ReportAmount);
                    PreviousProductVolume = Convert.ToDecimal(ObjPreviousVolumeData.ElementAt(0).ProductAmount);
                }
                if (i == 11)
                {
                    var Obj = SalesData.Where(x => x.OrderMonth == Convert.ToInt16(ObjDateTime.AddMonths(i).Month) && x.OrderYear == Convert.ToInt16(ObjDateTime.AddMonths(i).Year));
                    var ObjVolume = ObjBALCompany.GetVolumeValuesforGraph(pkCompanyId, Convert.ToInt16(ObjDateTime.AddMonths(i).Month), Convert.ToInt16(ObjDateTime.AddMonths(i).Year));
                    TotalSales = Convert.ToDecimal(Obj.ElementAt(0).TotalAmount);
                    ReportSales = Convert.ToDecimal(Obj.ElementAt(0).ReportAmount);
                    ProductSales = Convert.ToDecimal(Obj.ElementAt(0).ProductAmount);

                    TotalVolume = Convert.ToDecimal(ObjVolume.ElementAt(0).TotalAmount);
                    ReportVolume = Convert.ToDecimal(ObjVolume.ElementAt(0).ReportAmount);
                    ProductVolume = Convert.ToDecimal(ObjVolume.ElementAt(0).ProductAmount);

                    TotalSalesPercentage = 0;
                    ReportSalesPercentage = 0;
                    ProductSalesPercentage = 0;
                    TotalVolumePercentage = 0;
                    ReportVolumePercentage = 0;
                    ProductVolumePercentage = 0;

                    if (PreviousTotalSales > 0)
                    {
                        TotalSalesPercentage = ((Convert.ToDecimal(Obj.ElementAt(0).TotalAmount) - PreviousTotalSales) / PreviousTotalSales) * 100;
                    }
                    if (PreviousReportSales > 0)
                    {
                        ReportSalesPercentage = ((Convert.ToDecimal(Obj.ElementAt(0).ReportAmount) - PreviousReportSales) / PreviousReportSales) * 100;
                    }
                    if (PreviousProductSales > 0)
                    {
                        ProductSalesPercentage = ((Convert.ToDecimal(Obj.ElementAt(0).ProductAmount) - PreviousProductSales) / PreviousProductSales) * 100;
                    }


                    if (PreviousTotalVolume > 0)
                    {
                        TotalVolumePercentage = ((Convert.ToDecimal(ObjVolume.ElementAt(0).TotalAmount) - PreviousTotalVolume) / PreviousTotalVolume) * 100;
                    }
                    if (PreviousReportVolume > 0)
                    {
                        ReportVolumePercentage = ((Convert.ToDecimal(ObjVolume.ElementAt(0).ReportAmount) - PreviousReportVolume) / PreviousReportVolume) * 100;
                    }
                    if (PreviousProductVolume > 0)
                    {
                        ProductVolumePercentage = ((Convert.ToDecimal(ObjVolume.ElementAt(0).ProductAmount) - PreviousProductVolume) / PreviousProductVolume) * 100;
                    }
                }
            }

            Totals ObjSalesReport = new Totals();
            ObjSalesReport.Total = "Total";
            ObjSalesReport.TotalValue = TotalSales;
            ObjSalesReport.TotalPercentage = (TotalSalesPercentage / 100);
            ObjSalesReport.Report = "Reports";
            ObjSalesReport.ReportValue = ReportSales;
            ObjSalesReport.ReportPercentage = (ReportSalesPercentage / 100);
            ObjSalesReport.Product = "Products";
            ObjSalesReport.ProductValue = ProductSales;
            ObjSalesReport.ProductPercentage = (ProductSalesPercentage / 100);

            Totals ObjVolumeReport = new Totals();
            ObjVolumeReport.Total = "Total";
            ObjVolumeReport.TotalValue = TotalVolume;
            ObjVolumeReport.TotalPercentage = (TotalVolumePercentage / 100);
            ObjVolumeReport.Report = "Reports";
            ObjVolumeReport.ReportValue = ReportVolume;
            ObjVolumeReport.ReportPercentage = (ReportVolumePercentage / 100);
            ObjVolumeReport.Product = "Products";
            ObjVolumeReport.ProductValue = ProductVolume;
            ObjVolumeReport.ProductPercentage = (ProductVolumePercentage / 100);

            Tuple<Totals, Totals> collection = new Tuple<Totals, Totals>(ObjSalesReport, ObjVolumeReport);
            return collection;
        }

        public List<tblVendor> GetVendorDetails()
        {
            using (EmergeDALDataContext ObjUSAIntelDataDataContext = new EmergeDALDataContext())
            {
                try
                {
                    List<tblVendor> VendordDetails = ObjUSAIntelDataDataContext.tblVendors.ToList();
                    if (VendordDetails.Count != 0)
                    {
                        return VendordDetails;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
        }

        public List<VendorReports> GetReportsByVendorId(int VendorId)
        {
            List<VendorReports> lst = new List<VendorReports>();
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                lst = (from data in ObjDALDataContext.tblProducts where data.fkVendorId == VendorId select new VendorReports { pkReportId = data.pkProductId, ReportName = data.ProductName, ProductCode = data.ProductCode }).ToList();
            }
            return lst;
        }

        public List<Proc_Get_VendorReportForAdminResult> GetVendorReport(Guid ApplicationId, int BillingMonth, int BillingYear, byte VendorId, int ProductId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetVendorReportForAdmin(ApplicationId, BillingMonth, BillingYear, VendorId, ProductId).ToList<Proc_Get_VendorReportForAdminResult>();
            }
        }

        public Tuple<List<clsVendorReport>, List<clsDuplicateTransactions>> GetVendorReportData(Guid ApplicationId, int BillingMonth, int BillingYear, byte VendorId, int ProductId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                List<Proc_Get_VendorReportForAdminResult> ObjListVendorSummary = GetVendorReport(ApplicationId, BillingMonth, BillingYear, VendorId, ProductId).ToList<Proc_Get_VendorReportForAdminResult>();
                List<tblVendor> lstVendors = GetVendorDetails().OrderBy(d => d.pkVendorId).ToList();

                #region VendorReports
                List<clsVendorReport> listVendorReports = new List<clsVendorReport>();
                if (lstVendors.Count > 0)
                {
                    foreach (var Vendors in lstVendors)
                    {

                        int CurrentVendorId = Vendors.pkVendorId;

                        List<VendorReports> lstReports = GetReportsByVendorId(CurrentVendorId).ToList<VendorReports>();

                        for (int rep = 0; rep < lstReports.Count; rep++)
                        {
                            string CurrentReportName = lstReports.ElementAt(rep).ReportName;

                            List<Proc_Get_VendorReportForAdminResult> ObjListVendorSummaryByVendorId = ObjListVendorSummary.Where(d => d.ProductName == CurrentReportName).ToList();
                            if (ObjListVendorSummaryByVendorId.Count > 0)
                            {
                                for (int iRow = 0; iRow < ObjListVendorSummaryByVendorId.Count; iRow++)
                                {
                                    #region Summary Vendor Wise
                                    clsVendorReport ObjclsVendorReport = new clsVendorReport();
                                    ObjclsVendorReport.VendorName = Vendors.VendorName;
                                    ObjclsVendorReport.ReportType = ObjListVendorSummaryByVendorId.ElementAt(iRow).ProductName;
                                    ObjclsVendorReport.DataSource = ObjListVendorSummaryByVendorId.ElementAt(iRow).ProductCode;
                                    ObjclsVendorReport.Quantity = ObjListVendorSummaryByVendorId.ElementAt(iRow).VendorReportsCount;
                                    ObjclsVendorReport.Cost = Convert.ToDecimal(ObjListVendorSummaryByVendorId.ElementAt(iRow).VendorPrice);
                                    ObjclsVendorReport.TotalCost = Convert.ToDecimal(ObjListVendorSummaryByVendorId.ElementAt(iRow).TotalCost);
                                    ObjclsVendorReport.AveragePrice = Convert.ToDecimal(ObjListVendorSummaryByVendorId.ElementAt(iRow).AverageSellingPrice);
                                    ObjclsVendorReport.TotalPrice = Convert.ToDecimal(ObjListVendorSummaryByVendorId.ElementAt(iRow).TotalSellingPrice);
                                    ObjclsVendorReport.Profit = Convert.ToDecimal(ObjListVendorSummaryByVendorId.ElementAt(iRow).Profit);
                                    listVendorReports.Add(ObjclsVendorReport);
                                    #endregion Summary Vendor Wise
                                }
                            }
                            else
                            {
                                #region If No Records as per Reports
                                clsVendorReport ObjclsVendorReport = new clsVendorReport();
                                ObjclsVendorReport.VendorName = Vendors.VendorName;
                                ObjclsVendorReport.ReportType = CurrentReportName;
                                ObjclsVendorReport.DataSource = lstReports.ElementAt(rep).ProductCode;
                                ObjclsVendorReport.Quantity = 0;
                                ObjclsVendorReport.Cost = 0;
                                ObjclsVendorReport.TotalCost = 0;
                                ObjclsVendorReport.AveragePrice = 0;
                                ObjclsVendorReport.TotalPrice = 0;
                                ObjclsVendorReport.Profit = 0;
                                listVendorReports.Add(ObjclsVendorReport);
                                #endregion If No Records as per Reports
                            }
                        }

                    }
                }
                #endregion

                #region Vendor Duplicates
                List<clsDuplicateTransactions> listDuplicateTransactions = new List<clsDuplicateTransactions>();
                if (lstVendors.Count > 0)
                {
                    foreach (var Vendors in lstVendors)
                    {
                        int CurrentVendorId = Vendors.pkVendorId;
                        List<proc_GetDuplicateVendorRecordsResult> ObjListVendorDuplicates = GetDuplicateVendorRecords(BillingMonth, BillingYear).Where(d => d.VendorId == CurrentVendorId).ToList<proc_GetDuplicateVendorRecordsResult>();
                        if (ObjListVendorDuplicates.Count > 0)
                        {
                            for (int iRow = 0; iRow < ObjListVendorDuplicates.Count; iRow++)
                            {
                                #region Vendor Duplicates - Per Vendor
                                clsDuplicateTransactions ObjDuplicate = new clsDuplicateTransactions();
                                ObjDuplicate.VendorName = Vendors.VendorName;
                                ObjDuplicate.ReportID = ObjListVendorDuplicates.ElementAt(iRow).OrderNumber.ToString();
                                ObjDuplicate.ReportType = ObjListVendorDuplicates.ElementAt(iRow).ProductName;
                                ObjDuplicate.ApplicantName = ObjListVendorDuplicates.ElementAt(iRow).SearchedFullName.Replace(" ", "");
                                ObjDuplicate.Company = ObjListVendorDuplicates.ElementAt(iRow).CompanyName;
                                ObjDuplicate.ReportStatus = ObjListVendorDuplicates.ElementAt(iRow).ReportStatus.ToString();
                                ObjDuplicate.Orderdate = ObjListVendorDuplicates.ElementAt(iRow).OrderDate;
                                listDuplicateTransactions.Add(ObjDuplicate);
                                #endregion Vendor Duplicates - Per Vendor
                            }
                        }
                        else
                        {
                            #region Add empty record
                            clsDuplicateTransactions ObjDuplicate = new clsDuplicateTransactions();
                            ObjDuplicate.VendorName = Vendors.VendorName;
                            ObjDuplicate.IsEmpty = true;
                            listDuplicateTransactions.Add(ObjDuplicate);
                            #endregion Add empty record
                        }


                    }

                }
                #endregion Vendor Duplicates

                Tuple<List<clsVendorReport>, List<clsDuplicateTransactions>> collection = new Tuple<List<clsVendorReport>, List<clsDuplicateTransactions>>(listVendorReports, listDuplicateTransactions);
                return collection;
            }

        }

        public List<proc_GetDuplicateVendorRecordsResult> GetDuplicateVendorRecords(int BillingMonth, int BillingYear)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.proc_GetDuplicateVendorRecords(BillingMonth, BillingYear).ToList<proc_GetDuplicateVendorRecordsResult>();
            }
        }

        public List<proc_GetSalesValuesforGraphMVCResult> GetSalesValuesforGraphMVC(int pkCompanyId, int NumberOfMonth)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetSalesValuesforGraphMVC(pkCompanyId, NumberOfMonth).ToList();
            }
        }

        public List<proc_GetSalesValuesforBillingGraphResult> GetSalesValuesforBillingGraph(int pkCompanyId, int NumberOfMonth, DateTime Date)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {

                return ObjDALDataContext.GetSalesValuesforBillingGraph(pkCompanyId, NumberOfMonth, Date).ToList();
            }
        }

        #endregion

        public List<Proc_GetSavedReports_BillingResult> GetSavedReports(int ProductId, int CompanyUserId, int CompanyId,
                                                                     int LocationId, string SortingColumn, string SortingDirection,
                                                                     bool AllowPaging, int PageNum, int PageSize,
                                                                     string FromDate, string ToDate, string SearchKeyword, byte OrderStatus, string ReportStatus)
        {
            //List<clsSavedReports> listclsSavedReports = new List<clsSavedReports>();
            using (BALBilling DX = new BALBilling())
            {
                List<Proc_GetSavedReports_BillingResult> data = DX.GetSavedReports_Billing(ProductId, CompanyUserId, CompanyId,
                                                                       LocationId, SortingColumn, SortingDirection,
                                                                       AllowPaging, PageNum, PageSize,
                                                                       FromDate, ToDate, SearchKeyword, OrderStatus, ReportStatus, 0).ToList();

                return data;
            }
        }

        public Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALProductsPerLocation>, List<List<BALReportsByLocation>>, List<proc_GetBillingAdjustmentsResult>, Totals, Totals> GetBillingReportData(string ApplicationId, string Month, string Year, int CompanyId, int LocationId, int UserId, Guid SalesRepId, int RefCode)
        {
            using (BALBilling ObjBALBilling = new BALBilling())
            {
                IMultipleResults Results = ObjBALBilling.GetBillingReportData(new Guid(ApplicationId), int.Parse(Month), int.Parse(Year), CompanyId, SalesRepId, LocationId, UserId, RefCode);
                List<BALBillingSummary> ObjNotDrugTestingSummary = Results.GetResult<BALBillingSummary>().ToList();
                List<BALBillingSummary> ObjDrugTestingSummary = Results.GetResult<BALBillingSummary2>().Select(x => new BALBillingSummary
                {
                    CategoryName = x.CategoryName,
                    pkProductApplicationId = x.pkProductApplicationId,
                    ProductDisplayName = x.ProductDisplayName,
                    ProductCode = x.ProductCode,
                    TotalReports = x.TotalReports,
                    pkReportCategoryId = x.pkReportCategoryId,
                    TotalEarn = x.TotalEarn
                }).ToList();

                List<BALProductsPerLocation> ObjBALProductsPerLocation = Results.GetResult<BALProductsPerLocation>().ToList();

                List<BALReportsByLocation> ObjBALCompanyLocationsList = Results.GetResult<BALReportsByLocation>().ToList();
                List<List<BALReportsByLocation>> ObjBALCompanyLocations = new List<List<BALReportsByLocation>>();
                ObjBALCompanyLocations.Add(ObjBALCompanyLocationsList.Where(x => x.SerialNumber == 1).ToList());
                ObjBALCompanyLocations.Add(ObjBALCompanyLocationsList.Where(x => x.SerialNumber == 2).ToList());
                ObjBALCompanyLocations.Add(ObjBALCompanyLocationsList.Where(x => x.SerialNumber == 3).ToList());
                ObjBALCompanyLocations.Add(ObjBALCompanyLocationsList.Where(x => x.SerialNumber == 4).ToList());
                ObjBALCompanyLocations.Add(ObjBALCompanyLocationsList.Where(x => x.SerialNumber == 5).ToList());
                ObjBALCompanyLocations.Add(ObjBALCompanyLocationsList.Where(x => x.SerialNumber == 6).ToList());
                ObjBALCompanyLocations.Add(ObjBALCompanyLocationsList.Where(x => x.SerialNumber == 7).ToList());
                ObjBALCompanyLocations.Add(ObjBALCompanyLocationsList.Where(x => x.SerialNumber == 8).ToList());

                List<proc_GetBillingAdjustmentsResult> ObjBALBillingSummaryAdjustment = Results.GetResult<proc_GetBillingAdjustmentsResult>().ToList();
                Totals ObjSalesReport = Results.GetResult<Totals>().FirstOrDefault();

                Totals ObjVolumeReport = Results.GetResult<Totals2>().Select(x => new Totals
                {
                    Total = x.Total,
                    TotalValue = x.TotalValue,
                    TotalPercentage = x.TotalPercentage,
                    Report = x.Report,
                    ReportValue = x.ReportValue,
                    ReportPercentage = x.ReportPercentage,
                    Product = x.Product,
                    ProductValue = x.ProductValue,
                    ProductPercentage = x.ProductPercentage
                }).FirstOrDefault();

                Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALProductsPerLocation>, List<List<BALReportsByLocation>>, List<proc_GetBillingAdjustmentsResult>, Totals, Totals> collection = new Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALProductsPerLocation>, List<List<BALReportsByLocation>>, List<proc_GetBillingAdjustmentsResult>, Totals, Totals>(ObjNotDrugTestingSummary, ObjDrugTestingSummary, ObjBALProductsPerLocation, ObjBALCompanyLocations, ObjBALBillingSummaryAdjustment, ObjSalesReport, ObjVolumeReport);
                return collection;
            }
        }
        public Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALReportsByLocation>, List<proc_GetBillingAdjustmentsResult>> GetBillingReportData_API(string ApplicationId, string Month, string Year, int CompanyId, int LocationId, int UserId, Guid SalesRepId, int RefCode)
        {
            BALBilling ObjBALBilling = new BALBilling();
            IMultipleResults Results = ObjBALBilling.GetBillingReportData_API(new Guid(ApplicationId), int.Parse(Month), int.Parse(Year), CompanyId, SalesRepId, LocationId, UserId, RefCode);

            List<BALBillingSummary> ObjNotDrugTestingSummary = Results.GetResult<BALBillingSummary>().ToList();
            List<BALBillingSummary> ObjDrugTestingSummary = Results.GetResult<BALBillingSummary2>().AsEnumerable().Select(x => new BALBillingSummary
            {
                CategoryName = x.CategoryName,
                pkProductApplicationId = x.pkProductApplicationId,
                ProductDisplayName = x.ProductDisplayName,
                ProductCode = x.ProductCode,
                TotalReports = x.TotalReports,
                pkReportCategoryId = x.pkReportCategoryId,
                TotalEarn = x.TotalEarn
            }).ToList();

            List<BALReportsByLocation> ObjBALReportsByLocation = Results.GetResult<BALReportsByLocation>().ToList();
            List<proc_GetBillingAdjustmentsResult> ObjBALBillingSummaryAdjustment = Results.GetResult<proc_GetBillingAdjustmentsResult>().ToList();

            Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALReportsByLocation>, List<proc_GetBillingAdjustmentsResult>> collection = new Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALReportsByLocation>, List<proc_GetBillingAdjustmentsResult>>(ObjNotDrugTestingSummary, ObjDrugTestingSummary, ObjBALReportsByLocation, ObjBALBillingSummaryAdjustment);
            return collection;
        }


    }
    public class Totals
    {
        public string Total { get; set; }
        public decimal TotalValue { get; set; }
        public decimal TotalPercentage { get; set; }
        public string Report { get; set; }
        public decimal ReportValue { get; set; }
        public decimal ReportPercentage { get; set; }
        public string Product { get; set; }
        public decimal ProductValue { get; set; }
        public decimal ProductPercentage { get; set; }
    }
    public class Totals2
    {
        public string Total { get; set; }
        public decimal TotalValue { get; set; }
        public decimal TotalPercentage { get; set; }
        public string Report { get; set; }
        public decimal ReportValue { get; set; }
        public decimal ReportPercentage { get; set; }
        public string Product { get; set; }
        public decimal ProductValue { get; set; }
        public decimal ProductPercentage { get; set; }
    }
    public class clsDuplicateTransactions
    {
        public string VendorName { get; set; }
        public string ReportID { get; set; }
        public string ReportType { get; set; }
        public string ApplicantName { get; set; }
        public string Company { get; set; }
        public string ReportStatus { get; set; }
        public DateTime? Orderdate { get; set; }
        public bool IsEmpty { get; set; }
    }
    public class clsVendorReport
    {
        public string ReportType { get; set; }
        public string DataSource { get; set; }
        public int? Quantity { get; set; }
        public decimal Cost { get; set; }
        public decimal TotalCost { get; set; }
        public decimal AveragePrice { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal Profit { get; set; }
        public string VendorName { get; set; }

    }

    //public class CompanyList
    //{
    //    public string CompanyName { get; set; }
    //    public Guid pkCompanyId { get; set; }
    //}

    public enum ReportCategories
    {
        DrugTesting = 9,
        OtherAdjustmentsReports = 11
    }
    public class VendorReports
    {
        public int pkReportId { get; set; }
        public string ReportName { get; set; }
        public string ProductCode { get; set; }
    }

    public partial class clsSavedReports
    {
        public int TotalRec { get; set; }
        public int RowNo { get; set; }
        public string ApplicantName { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string OrderNo { get; set; }
        public string ReportIncludes { get; set; }
        public string CompanyName { get; set; }
        public string Location { get; set; }
        public string UserEmail { get; set; }
        public DateTime? OrderDt { get; set; }
        public int Days { get; set; }
        public byte OrderStatus { get; set; }
        public string OrderStatusDisplay { get; set; }
        public int OrderId { get; set; }
        public string StateCode { get; set; }
        public bool IsDrugTestingOrder { get; set; }
        public string ReportsWithStatus { get; set; }
        public decimal PackageAmount { get; set; }
        public decimal ReportAmount { get; set; }
        public decimal AdditionalFee { get; set; }
        public bool IsSyncpod { get; set; }
        public bool OrderType { get; set; }
        public bool IsTiered { get; set; }
        public string Price { get; set; }
        public string Fee { get; set; }
        public decimal Total { get; set; }
        public string ReferenceCode { get; set; }  // ND-29
        public int UCode { get; set; }  // INT 230
        public int Rcode { get; set; }
        public string PackageName { get; set; }
        public int packagenamedisplay { get; set; }
        public int Locationid { get; set; }
    }
}

