﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;
using System.Data;
using System.Data.Common;

namespace Emerge.Services
{
    public class BALProducts
    {
        //public List<vwProductsEmerge> GetProducts()
        //{
        //    using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
        //    {
        //        IQueryable<vwProductsEmerge> ObjCollection = ObjDALDataContext.vwProductsEmerges.OrderBy(db => db.ProductName).Select(db => db);
        //        return ObjCollection.ToList();
        //    }
        //}

        //public List<Proc_Get_EmergeReportsForAdminResult> GetEmergeReportsForAdmin(byte ReportCategoryId)
        //{
        //    using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
        //    {
        //        return ObjDALDataContext.GetEmergeReportsForAdmin(ReportCategoryId).ToList();
        //    }
        //}

        public int SaveProductInfo(tblProduct ObjProduct, tblProductPerApplication ObjProductPerApplication)
        {
            #region Variable Assignment

            int iResult = -1;
            DbTransaction DbTrans = null;

            #endregion

            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    #region Connection Open and Transaction Assignment

                    ObjDALDataContext.Connection.Open(); /* Connection Open */
                    DbTrans = ObjDALDataContext.Connection.BeginTransaction(); /* Assign Transation */
                    ObjDALDataContext.Transaction = DbTrans;

                    #endregion

                    #region Update Product Info

                    IQueryable<tblProduct> ObjProductData = ObjDALDataContext.tblProducts.Where(db => db.pkProductId == ObjProduct.pkProductId);
                    if (ObjProductData.Count() == 1)
                    {
                        tblProduct ObjUpdate = ObjProductData.First();
                        ObjUpdate.VendorNotes = ObjProduct.VendorNotes;
                        ObjUpdate.IsLiveRunner = ObjProduct.IsLiveRunner;
                        ObjUpdate.IsEnableReview = ObjProduct.IsEnableReview;
                        ObjDALDataContext.SubmitChanges();
                    }

                    #endregion

                    #region Update Product Per Application Info

                    IQueryable<tblProductPerApplication> ObjProductPerApplicationData = ObjDALDataContext.tblProductPerApplications.Where(db => db.pkProductApplicationId == ObjProductPerApplication.pkProductApplicationId);
                    if (ObjProductPerApplicationData.Count() == 1)
                    {
                        tblProductPerApplication ObjUpdate = ObjProductPerApplicationData.First();
                        ObjUpdate.IsEnabled = ObjProductPerApplication.IsEnabled;
                        ObjUpdate.IsDefault = ObjProductPerApplication.IsDefault;
                        ObjUpdate.IsGlobal = ObjProductPerApplication.IsGlobal;
                        ObjUpdate.DoNotShow = ObjProductPerApplication.DoNotShow;
                        ObjUpdate.ProductPerApplicationPrice = ObjProductPerApplication.ProductPerApplicationPrice;
                        ObjUpdate.ProductDisplayName = ObjProductPerApplication.ProductDisplayName;
                        ObjDALDataContext.SubmitChanges();
                    }

                    #endregion

                    #region Transaction Committed

                    DbTrans.Commit(); /* Commit Transaction */

                    iResult = 1;

                    #endregion
                }
                catch
                {
                    #region Transaction Rollback

                    iResult = -1;
                    if (DbTrans != null)
                    {
                        DbTrans.Rollback(); /* Rollback Transaction */
                    }

                    #endregion
                }
                finally
                {
                    #region Connection and Transaction Close

                    ObjDALDataContext.Transaction = null;
                    if (ObjDALDataContext.Connection.State == ConnectionState.Open)
                    {
                        ObjDALDataContext.Connection.Close(); /* Close Connection */
                    }

                    #endregion
                }

                #region Return Result

                return iResult; /* Here we return result */

                #endregion
            }
        }

        ///// <summary>
        ///// Function to Get All the Products With Vendor .
        ///// </summary>
        ///// <returns></returns>

        public decimal GetUnitPricebyLocationIdAndfkProductApplicationId(int fkLocationId, int fkProductApplicationId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                decimal LocationProductPrice = 0;
                var list = ObjDALDataContext.tblProductAccesses.Where(rec => rec.fkLocationId == fkLocationId && rec.fkProductApplicationId == fkProductApplicationId).FirstOrDefault();
                if (list != null)
                { LocationProductPrice = list.LocationProductPrice; }
                return LocationProductPrice;
            }
        }




        public List<Proc_GetDrugTestQtybypkProductApplicationIdResult> GetDrugTestQtybypkProductApplicationId(int pkProductApplicationId, int fkLocationId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                //string DrugTest = string.Empty;
                var list = ObjDALDataContext.GetDrugTestQtybypkProductApplicationId(pkProductApplicationId, fkLocationId).ToList(); // && rec.fkReportCategoryId == '9').FirstOrDefault();
                //if (list != null)
                //{
                //     DrugTest = list.ElementAt(0).DrugTestQty;
                //}

                return list;
            }
        }
        public List<Proc_GetAllProductsWithVendorResult> GetAllProductsWithVendor()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetAllProductsWithVendor().ToList();
            }
        }

        //public decimal GetProductPrice(Guid fkCompanyId, Guid fkProductApplicationId)
        //{
        //    using (EmergeDALDataContext DX = new EmergeDALDataContext())
        //    {
        //        var ProductPrice = DX.tblCompany_Prices.Where(data => data.fkCompanyId == fkCompanyId && data.fkProductApplicationId == fkProductApplicationId).FirstOrDefault().CompanyPrice;
        //        return Convert.ToDecimal(ProductPrice);
        //    }
        //}

        //public int UpdateProductDescription(Guid ProductId, string Description)
        //{
        //    int success = 0;
        //    using (EmergeDALDataContext DX = new EmergeDALDataContext())
        //    {
        //        try
        //        {
        //            tblProduct ObjtblProduct = new tblProduct();
        //            ObjtblProduct = DX.tblProducts.Where(d => d.pkProductId == ProductId).FirstOrDefault();
        //            ObjtblProduct.ProductDescription = Description;
        //            DX.SubmitChanges();
        //            success = 1;
        //        }
        //        catch
        //        {
        //            success = -1;
        //        }
        //        return success;
        //    }
        //}

        //COMMENTING AS IT WAS NOT USED PROPERLY IN CONTROLLER
        //PLEASE SEE  METHOD SORTORDERCHANGE(STRING PKPRODUCTID, INT SORTORDER, INT ISUP, INT REPORTCATID) IN MANAGEREPORTCONTROller
        //public List<Proc_UpdateProductSortOrderResult> UpdateProductSortOrder(int reportcatid, byte SortOrder, int pkProductId, int IsUP)
        //{
        //    using (EmergeDALDataContext objdb = new EmergeDALDataContext())
        //    {
        //        return objdb.UpdateProductSortOrder(reportcatid, SortOrder, pkProductId, IsUP).ToList();

        //    }
        //}


        public int UploadCoveragePDF(int ProductId, string FilePath)
        {
            int success = 0;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                try
                {
                    tblProduct ObjtblProduct = new tblProduct();
                    ObjtblProduct = DX.tblProducts.Where(d => d.pkProductId == ProductId).FirstOrDefault();
                    ObjtblProduct.CoveragePDFPath = FilePath;
                    DX.SubmitChanges();
                    success = 1;
                }
                catch
                {
                    success = -1;
                }
                return success;
            }
        }

        public int UploadProductImage(int ProductId, string FilePath)
        {
            int success = 0;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                try
                {
                    tblProductPerApplication ObjtblProductPerApplications = new tblProductPerApplication();
                    ObjtblProductPerApplications = DX.tblProductPerApplications.Where(d => d.fkProductId == ProductId).FirstOrDefault();
                    ObjtblProductPerApplications.ProductImage = FilePath;
                    DX.SubmitChanges();
                    success = 1;
                }
                catch
                {
                    success = -1;
                }
                return success;
            }
        }

        public List<Proc_GetDrugTestingProductsResult> GetDrugTestingProducts(int LocationId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetDrugTestingProducts(LocationId).ToList();
            }
        }

        public int UpdateIsLinkedPrice(int fkProductApplicationId, int fkCompanyId)
        {
            int success = 0;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    success = ObjDALDataContext.UpdateLinkedReportId(fkProductApplicationId, fkCompanyId);
                }
                catch
                {
                    success = -1;
                }
                return success;
            }
        }

        /// <summary>
        /// To get the Product list.
        /// </summary>
        /// <returns>List of products</returns>
        public List<vwProductsEmerge> GetProducts()
        {
            List<vwProductsEmerge> lstVW = new List<vwProductsEmerge>();
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                //Ticket #185 (On Jira INT-8)
                //To get logged on user id from the session.
                string loggedOnUser = Convert.ToString(System.Web.HttpContext.Current.Session["CurrentUserMailId"]);
                //To get product list based on logged on user id.
                var ObjCollection = ObjDALDataContext.GetProductListByUserCompany(loggedOnUser.Trim()).OrderBy(db => db.ProductName).ToList();
                if (ObjCollection != null)
                {
                    foreach (var objColl in ObjCollection)
                    {
                        vwProductsEmerge vw = new vwProductsEmerge();
                        vw.ApplicationId = objColl.ApplicationId;
                        vw.ApplicationName = objColl.ApplicationName;
                        vw.CategoryName = objColl.CategoryName;
                        vw.CoveragePDFPath = objColl.CoveragePDFPath;
                        vw.CreatedDate = objColl.CreatedDate;
                        vw.DoNotShow = objColl.DoNotShow;
                        vw.DrugTestCategoryId = objColl.DrugTestCategoryId;
                        vw.DrugTestPanels = objColl.DrugTestPanels;
                        vw.DrugTestQty = objColl.DrugTestQty;
                        vw.IsAdjustmentProduct = objColl.IsAdjustmentProduct;
                        vw.IsDefault = objColl.IsDefault;
                        vw.IsDeleted = objColl.IsDeleted;
                        vw.IsEnabled = objColl.IsEnabled;
                        vw.IsEnableReview = objColl.IsEnableReview;
                        vw.IsGlobal = objColl.IsGlobal;
                        vw.IsLiveRunner = objColl.IsLiveRunner;
                        vw.IsThirdPartyFee = objColl.IsThirdPartyFee;
                        vw.LastModifiedDate = objColl.LastModifiedDate;
                        vw.LongDescription = objColl.LongDescription;
                        vw.pkProductApplicationId = objColl.pkProductApplicationId;
                        vw.pkProductId = objColl.pkProductId;
                        vw.ProductCode = objColl.ProductCode;
                        vw.ProductCreatedDate = objColl.ProductCreatedDate;
                        vw.ProductDefaultPrice = objColl.ProductDefaultPrice;
                        vw.ProductDescription = objColl.ProductDescription;
                        vw.ProductDisplayName = objColl.ProductDisplayName;
                        vw.ProductImage = objColl.ProductImage;
                        vw.ProductIsDeleted = objColl.ProductIsDeleted;
                        vw.ProductIsEnabled = objColl.ProductIsEnabled;
                        vw.ProductLastModifiedDate = objColl.ProductLastModifiedDate;
                        vw.ProductName = objColl.ProductName;
                        vw.ProductPerApplicationPrice = objColl.ProductPerApplicationPrice;
                        vw.ProductVendorPrice = objColl.ProductVendorPrice;
                        vw.ReportCategoryId = objColl.ReportCategoryId;
                        vw.ShortDescription = objColl.ShortDescription;
                        vw.SortOrder = objColl.SortOrder;
                        vw.VendorId = objColl.VendorId;
                        vw.VendorNotes = objColl.VendorNotes;
                        lstVW.Add(vw);
                        vw = null;
                    }
                }
                return lstVW.OrderBy(db => db.ProductName).ToList();
            }
        }
    }
}
