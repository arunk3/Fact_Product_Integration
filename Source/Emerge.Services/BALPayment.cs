﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;

namespace Emerge.Services
{
    public class BALPayment
    {
        /// <summary>
        /// To update the Autopay.
        /// </summary>
        /// <param name="pkCompanyId"></param>
        /// <param name="Autopay"></param>
        /// <returns></returns>
        public int UpdateAutoPay(int pkCompanyId, bool Autopay)
        {
            int Result = 0;
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    int success = 0;
                    IQueryable<tblCompany> ObjData = ObjEmergeDALDataContext.tblCompanies.Where(t => t.pkCompanyId == pkCompanyId);
                    if (ObjData.Count() == 1)
                    {
                        tblCompany ObjtblCompany = ObjData.First();
                        ObjtblCompany.AutoPay = Autopay;
                        ObjtblCompany.LastModifiedDate = DateTime.Now;
                    }
                    ObjEmergeDALDataContext.SubmitChanges();
                    success = 1;
                    return success;

                }
                catch (Exception)
                {

                    Result = 0;
                }


            }
            return Result;
        }

        /// <summary>
        /// To Get the Payment history details based on company id.
        /// </summary>
        /// <param name="pkCompanyId"></param>
        /// <returns></returns>
        public List<proc_LoadPaymentHistoryByCompanyIdResult> LoadPaymentHistoryByCompanyId(int pkCompanyId)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.LoadPaymentHistoryByCompanyId(pkCompanyId).ToList();
            }
        }
        /// <summary>
        /// To Get the Payment history details based on company id and selected date.
        /// </summary>
        /// <param name="pkCompanyId"></param>
        /// <returns></returns>
        public List<proc_LoadInvoiceHistoryByCompanyIdByDateResult1> LoadPaymentHistoryByCompanyIdByDate(int pkCompanyId, DateTime from, DateTime to)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.LoadInvoiceHistoryByCompanyIdByDate1(pkCompanyId, from, to).ToList();
            }
        }

        /// <summary>
        /// To get Prepayment information detail based on selected company id.
        /// </summary>
        /// <param name="pkCompanyId"></param>
        /// <returns></returns>
        public List<Proc_GetPrePayementHistoryResult1> LoadPrePaymentHistoryByCompanyId(int pkCompanyId)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.GetPrePayementHistory(pkCompanyId).ToList();
            }
        }

        /// <summary>
        /// To get Prepayment information detail based on selected company id.
        /// </summary>
        /// <param name="pkCompanyId"></param>
        /// <returns></returns>
        public List<Proc_GetPrePayementHistoryByDateResult1> LoadPrePaymentHistoryByCompanyIdByDate(int pkCompanyId, DateTime from, DateTime to)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.GetPrePayementHistoryByDate(pkCompanyId, from, to).ToList();
            }
        }


        /// <summary>
        /// To get Prepayment Recharge detail based on selected company id.
        /// </summary>
        /// <param name="pkCompanyId"></param>
        /// <returns></returns>
        public List<proc_GetPrePaymentRechargeOptionResult1> LoadRechargePrePaymentHistory(int pkCompanyId)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.GetPrePaymentRechargeOption(pkCompanyId).ToList();
            }
        }


        /// <summary>
        /// To get Prepayment Recharge detail based on selected company id and date filter.
        /// </summary>
        /// <param name="pkCompanyId"></param>
        /// <returns></returns>
        public List<proc_GetPrePaymentRechargeOptionByDateResult1> LoadRechargePrePaymentHistoryByDate(int pkCompanyId, DateTime from, DateTime to)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.GetPrePaymentRechargeOptionByDate(pkCompanyId, from, to).ToList();

            }
        }




        public List<proc_GetPaymentBillingInfoPerCompanyIdResult> GetPaymentBillingInfoByCompanyId(int pkCompanyId)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.GetPaymentBillingInfoPerCompanyId(pkCompanyId).ToList();
            }
        }


        public List<proc_GetPrePaymentBillingInfoPerCompanyIdResult1> GetPrePaymentBillingInformationByCompanyId(int pkCompanyId)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.GetPrePaymentBillingInfoPerCompanyId(pkCompanyId).ToList();
            }
        }

        /// <summary>
        /// To Get PrePayment Balance Based on selected Company id.
        /// </summary>
        /// <param name="pkCompanyId"></param>
        /// <returns></returns>
        public Decimal GetPrePaymentAmountByCompanyId(int pkCompanyId)
        {
            decimal retval = 0;
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                var val = ObjEmergeDALDataContext.GetPrePaymentAmountByCompanyId(pkCompanyId).FirstOrDefault();
                if (val != null)
                {
                    retval = (decimal)val.RemainingBalance;
                }
            }
            return retval;
        }


        /// <summary>
        /// To Get PrePayment Balance Based on selected Company id.
        /// </summary>
        /// <param name="pkCompanyId"></param>
        /// <returns></returns>
        public bool CheckDueOnReceiptPendingStatus(int pkCompanyId)
        {
            bool retval = false;
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {

                var val = ObjEmergeDALDataContext.CheckDueOnReceiptPendingStatus(pkCompanyId).FirstOrDefault();
                if (val != null)
                {
                    if (val.RowCount > 0)
                    {
                        retval = true;
                    }
                }

            }
            return retval;
        }



        /// <summary>
        /// To get all PrePayment Billing details
        /// </summary>
        /// <param name="pkCompanyId"></param>
        /// <returns></returns>
        public List<proc_GetAllPrePaymentDetailsResult1> GetAllPrePaymentDetails(int pkCompanyId)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.GetAllPrePaymentDetails(pkCompanyId).ToList();
            }
        }


        /// <summary>
        /// To get all PrePayment Billing details by date filter
        /// </summary>
        /// <param name="pkCompanyId"></param>
        /// <returns></returns>
        public List<proc_GetAllPrePaymentDetailsByDateResult1> GetAllPrePaymentDetailsByDate(int pkCompanyId, DateTime from, DateTime to)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.GetAllPrePaymentDetailsByDate(pkCompanyId, from, to).ToList();
            }
        }

        /// <summary>
        /// To get All Due On Receipt History based on company id and date filter(if selected)
        /// </summary>
        /// <param name="pkCompanyId"></param>
        /// <returns></returns>
        public List<Proc_GetAllDueonReceiptHistoryResult2> GetAllDueonReceiptHistoryByDate(int pkCompanyId, DateTime from, DateTime to, int reportType)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.GetAllDueonReceiptHistory(pkCompanyId, from, to, reportType).ToList();
            }
        }


        //public List<proc_LoadPaymentHistoryByCompanyUserIdResult> LoadPaymentHistoryByCompanyUserId(Guid pkCompanyUserId)
        //{
        //    using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
        //    {
        //        return ObjEmergeDALDataContext.proc_LoadPaymentHistoryByCompanyUserId(pkCompanyUserId).ToList();
        //    }
        //}
        public List<tblCompany> GetCompanyName(int CompanyId)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.tblCompanies.Where(d => d.pkCompanyId == CompanyId).ToList<tblCompany>();
            }
        }
    }
}
