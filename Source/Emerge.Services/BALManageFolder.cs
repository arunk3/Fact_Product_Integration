﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;
using System.Data;
using System.Data.Common;

namespace Emerge.Services
{
    public class BALManageFolder
    {
        //FOR INT-371

        #region(METHOD FOR GET MANAGE FOLDER RESULT)

        public List<Proc_GetManageFolderResult> GetManageFolder(int fkCompanyId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetManageFolder(fkCompanyId).ToList();
            }
        }

        #endregion

        #region(METHOD FOR ADD MANAGE FOLDER RESULT)

        public int AddManageFolder(tblManageFolder ObjtblManageFolder, int fkCompanyId)
        {
            int Result = 0;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    //var AllFolderName = ObjDALDataContext.tblManageFolders.ToList();

                    //foreach (var foldername in AllFolderName)
                    //{
                    //    if (foldername.FolderName == ObjtblManageFolder.FolderName)
                    //    {
                    //        Result = -1;
                    //        break;
                    //    }
                    //}
                    //if (Result != -1)
                    //{
                    if (ObjtblManageFolder.FolderName != "Pending" && ObjtblManageFolder.FolderName != "In Review" && ObjtblManageFolder.FolderName != "Incomplete" && ObjtblManageFolder.FolderName != "Submit" && ObjtblManageFolder.FolderName != "Complete")
                    {
                        if ((ObjDALDataContext.tblManageFolders.Where(d => d.FolderName == ObjtblManageFolder.FolderName && d.fkCompanyId == fkCompanyId).Count()) == 0)
                        {
                            ObjDALDataContext.Proc_AddManageFolder(fkCompanyId,
                            ObjtblManageFolder.FolderName,
                            ObjtblManageFolder.FolderDescription);

                            Result = 1;
                        }
                    }
                    else
                    {
                        Result = -1;
                    }
                }
                catch (Exception)
                {

                    Result = 0;
                }


            }
            return Result;
        }

        #endregion

        #region(METHOD FOR UPDATE MANAGE FOLDER RESULT)

        public int UpdateManageFolder(tblManageFolder ObjtblManageFolder)
        {
            int Result = 0;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    if (ObjtblManageFolder.FolderName != "Pending" && ObjtblManageFolder.FolderName != "In Review" && ObjtblManageFolder.FolderName != "Incomplete" && ObjtblManageFolder.FolderName != "Submit" && ObjtblManageFolder.FolderName != "Complete")
                    {
                        if ((ObjDALDataContext.tblManageFolders.Where(d => d.FolderName == ObjtblManageFolder.FolderName && d.pkManageFolderId != ObjtblManageFolder.pkManageFolderId && d.fkCompanyId == ObjtblManageFolder.fkCompanyId).Count()) == 0)
                        {
                            ObjDALDataContext.Proc_UpdateManageFolder(ObjtblManageFolder.pkManageFolderId,
                            ObjtblManageFolder.FolderName,
                            ObjtblManageFolder.FolderDescription);
                            Result = 1;
                        }
                    }
                    else
                    {
                        Result = -1;
                    }
                }
                catch (Exception)
                {

                    Result = 0;
                }


            }
            return Result;
        }

        #endregion

        #region(METHOD FOR DELETE MANAGE FOLDER RESULT)

        public int DeleteFolder(int pkManageFolderId, int fkCompanyId)
        {
            int Result = 0;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    ObjDALDataContext.Proc_DeleteManageFolder(pkManageFolderId, fkCompanyId);
                    Result = 1;
                }
                catch (Exception)
                {
                    Result = 0;
                }
            }
            return Result;
        }

        #endregion

        #region(METHOD FOR SELECT MANAGE FOLDER RESULT FOR DROP DOWN LIST)

        public List<tblManageFolder> GetAllManageFolders(int? fkCompanyId)
        {
            List<tblManageFolder> ObjList = new List<tblManageFolder>();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                if (fkCompanyId == 0)
                    ObjList = DX.tblManageFolders.OrderBy(d => d.FolderName).ToList<tblManageFolder>();
                else
                    ObjList = DX.tblManageFolders.Where(d => d.fkCompanyId == fkCompanyId).OrderBy(d => d.FolderName).ToList<tblManageFolder>();
            }
            return ObjList;
        }

        #endregion
    }
}
