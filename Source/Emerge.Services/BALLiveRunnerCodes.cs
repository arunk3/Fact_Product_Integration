﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Xml.Linq;
using Emerge.Data;
using System.Text.RegularExpressions;
using VendorServices;

namespace Emerge.Services
{
    public class BALLiveRunnerCodes
    {

        public string GetandMatchResutsfromResponseData(string strResponseData, string ProductCode)
        {
            string FinalResponse = "";
            string strUpdatedXML = string.Empty;
            // BALLiveRunnerCodes ObjBALLiveRunnerCodes = new BALLiveRunnerCodes();

            List<tblPenalCode> lstCodes = GetPenalCodes();

            XDocument UpdatedXML = GetXMLandInsertDescription(strResponseData, lstCodes, ProductCode);
            strUpdatedXML = Convert.ToString(UpdatedXML);

            int TrimedUpTo = strResponseData.IndexOf("<BackgroundReportPackage>");
            StringBuilder CompleteResponse = new StringBuilder();
            string XMLInitials = strResponseData.Substring(0, TrimedUpTo);
            CompleteResponse.Append(XMLInitials);
            CompleteResponse.Append(strUpdatedXML);
            CompleteResponse.Append("</BackgroundReports>");
            FinalResponse = Convert.ToString(CompleteResponse);
            return FinalResponse;
        }


        //int-122 for penal code 
        public List<clsPenalCodes> GetPenalCode(int fkStateId)
        {
            List<clsPenalCodes> ObjList = new List<clsPenalCodes>();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {

                ObjList = (from P in DX.tblPenalCodes
                           where P.fkStateId == fkStateId
                           select new clsPenalCodes
                           {
                               pkPenalCodeId = P.pkPenalCodeId,
                               PenalCode = P.PenalCode,
                               PenalTitle = P.PenalTitle,
                               PenalDescription = P.PenalDescription,
                               fkStateId = P.fkStateId,
                               ProductCode = P.ProductCode,
                               PenalCode_From = P.PenalCode_From,
                               PenalCode_To = P.PenalCode_To,
                               PenalCodeRangeType = P.PenalCodeRangeType
                           }).ToList<clsPenalCodes>();


            }
            return ObjList;
        }


        public List<tblPenalCode> GetPenalCodes()
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return DX.tblPenalCodes.ToList<tblPenalCode>();
            }
        }

        public int CheckPenalCode(string PenalCode, int pkPenalCodeId)
        {
            int success = 0;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                if (pkPenalCodeId != 0)
                {
                    success = DX.tblPenalCodes.Where(d => d.ProductCode == PenalCode && d.pkPenalCodeId != pkPenalCodeId).Count();
                }
                else
                {
                    success = DX.tblPenalCodes.Where(d => d.ProductCode == PenalCode).Count();
                }
            }
            return success;
        }

        public int AddPenalCode(tblPenalCode ObjtblPenalCode)
        {
            int success = 0;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                DX.tblPenalCodes.InsertOnSubmit(ObjtblPenalCode);
                DX.SubmitChanges();
                success = 1;
            }
            return success;
        }

        public int UpdatePenalCode(tblPenalCode ObjtblPenalCode)
        {
            GeneralService.WriteLog(Environment.NewLine + " update penal code in " , "penalcodeErrorLog");
                int success = 0;
                tblPenalCode _tblPenalCode = new tblPenalCode();
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    _tblPenalCode = DX.tblPenalCodes.Where(d => d.pkPenalCodeId == ObjtblPenalCode.pkPenalCodeId).FirstOrDefault();
                    if (_tblPenalCode != null)
                    {
                        _tblPenalCode.PenalCode = ObjtblPenalCode.PenalCode;
                        _tblPenalCode.PenalTitle = ObjtblPenalCode.PenalTitle;
                        _tblPenalCode.PenalDescription = ObjtblPenalCode.PenalDescription;
                        _tblPenalCode.PenalCode_From = ObjtblPenalCode.PenalCode_From;
                        _tblPenalCode.PenalCode_To = ObjtblPenalCode.PenalCode_To;
                        _tblPenalCode.PenalCodeRangeType = ObjtblPenalCode.PenalCodeRangeType;
                        DX.SubmitChanges();
                        success = 1;

                        //GeneralService.WriteLog(Environment.NewLine + "Error RunReport_ForPendingReports_" + success + "Response: (" + DateTime.Now.ToString() + "):\n", "");
                        GeneralService.WriteLog(Environment.NewLine + " update penal code success _" + success, "penalcodeErrorLog");

                    }
                    else
                    {
                        DX.tblPenalCodes.InsertOnSubmit(ObjtblPenalCode);
                        DX.SubmitChanges();
                        success = 1;

                        GeneralService.WriteLog(Environment.NewLine + " penal code InsertOnSubmit _" + success, "penalcodeErrorLog");
                    }

                }
                return success;
            

            

        }
        public int DeletePenalCode(int pkPenalCodeId)
        {
            int success = 0;
            tblPenalCode _tblPenalCode = new tblPenalCode();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                _tblPenalCode = DX.tblPenalCodes.Where(d => d.pkPenalCodeId == pkPenalCodeId).FirstOrDefault();
                if (_tblPenalCode != null)
                {
                    DX.tblPenalCodes.DeleteOnSubmit(_tblPenalCode);
                    DX.SubmitChanges();
                    success = 1;
                }
            }
            return success;
        }


        public List<clsPenalCodes> InfoPenalCodeLocation(int fkStateId)
        {
            List<clsPenalCodes> ObjList = new List<clsPenalCodes>();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {

                ObjList = (from P in DX.tblPenalCodes
                           where P.fkStateId == fkStateId
                           select new clsPenalCodes
                           {
                               pkPenalCodeId = P.pkPenalCodeId,
                               PenalCode = P.PenalCode,
                               PenalTitle = P.PenalTitle,
                               PenalDescription = P.PenalDescription,
                               fkStateId = P.fkStateId,
                               ProductCode = P.ProductCode,
                               PenalCode_From = P.PenalCode_From,
                               PenalCode_To = P.PenalCode_To,
                               PenalCodeRangeType = P.PenalCodeRangeType
                           }).ToList<clsPenalCodes>();


            }
            return ObjList;
        }







        public int GetMaxPenalCodeId()
        {
            int MaxPenalCodeId = 0;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                var v = DX.tblPenalCodes.Select(d => d.pkPenalCodeId).Max();
                if (v != null)
                {
                    MaxPenalCodeId = Convert.ToInt32(v);
                    MaxPenalCodeId++;
                }
            }
            return MaxPenalCodeId;
        }

        //public XDocument GetXMLandInsertDescription(string XMLLiveRunner, List<tblPenalCode> lstCodes, string ProductCode)
        //{
        //    // XMLLiveRunner = "";

        //    XDocument ObjXDocumentUppdatedLiveRunner = new XDocument();
        //    XDocument ObjXDocumentLiveRunner = new XDocument();
        //    string XML_LiveRunner = GetRequiredXml(XMLLiveRunner);
        //    ObjXDocumentLiveRunner = XDocument.Parse(XML_LiveRunner);

        //    #region Penal Codes Section
        //    var LRScreenings = ObjXDocumentLiveRunner.Descendants("Screenings").ToList();
        //    string strRegion = string.Empty;
        //    tblPenalCode ObjtblPenalCode = null;
        //    if (LRScreenings.Count > 0)
        //    {
        //        var Region = LRScreenings.Descendants("Region").FirstOrDefault();
        //        if (Region != null)                                                                                                     // If Region is CA (California) then we need to add description
        //        {
        //            strRegion = Region.Value.ToString();

        //            if (strRegion == "CA" || strRegion == "SC")                                  //if (strRegion == "CA") FOR SOTH CAROLIONA 
        //            {

        //                try
        //                {
        //                    #region If region is California then we need to add in xml

        //                    var LRCriminalReports = LRScreenings.Descendants("CriminalReport").FirstOrDefault();

        //                    if (LRCriminalReports != null)
        //                    {
        //                        var LRCriminalCaseslst = LRCriminalReports.Descendants("CriminalCase").ToList();
        //                        for (int iRow = 0; iRow < LRCriminalCaseslst.Count; iRow++)
        //                        {
        //                            string Desc = string.Empty;
        //                            var LRCriminalCases = LRCriminalCaseslst.ElementAt(iRow);
        //                            var LRChargelst = LRCriminalCases.Descendants("Charge").ToList();
        //                            if (LRChargelst.Count > 0)
        //                            {
        //                                for (int iCharge = 0; iCharge < LRChargelst.Count; iCharge++)
        //                                {
        //                                    var LRCharge = LRChargelst.ElementAt(iCharge);
        //                                    var LRChargeOrComplaint = LRCharge.Descendants("ChargeOrComplaint").FirstOrDefault();
        //                                    if (LRChargeOrComplaint != null)
        //                                    {
        //                                        string PenalCode = LRChargeOrComplaint.Value.ToString();
        //                                        string[] strStatute = new string[] { "STATUTE:" };
        //                                        string AfterStatute = PenalCode.Split(strStatute, StringSplitOptions.RemoveEmptyEntries).LastOrDefault();

        //                                        if (!string.IsNullOrEmpty(AfterStatute))
        //                                        {
        //                                            int DeleteBracket = AfterStatute.LastIndexOf(')');

        //                                            string strDeleteBracket = AfterStatute.Substring(0, DeleteBracket != -1 ? DeleteBracket : 0).Trim().ToLower();

        //                                            int strCodeTrimLast = 0;
        //                                            string strCodeToMatch = "";

        //                                            string strCode = "";
        //                                            bool IsMatchCode = false;

        //                                            ObjtblPenalCode = new tblPenalCode();
        //                                            ObjtblPenalCode = lstCodes.Where(d => d.PenalCode.ToLower() == strDeleteBracket && d.PenalCodeRangeType == 0).FirstOrDefault();                    // Match Exact
        //                                            if (ObjtblPenalCode != null)
        //                                            {
        //                                                IsMatchCode = true;
        //                                                XElement XChargeDesc = new XElement("ChargeDescription");
        //                                                LRCharge.Add(XChargeDesc);
        //                                                XChargeDesc.Value = ObjtblPenalCode.PenalDescription.ToString();
        //                                            }
        //                                            if (IsMatchCode == false)                                                          // Case 487(d)(1)  - It is example we have more codes like this example
        //                                            {
        //                                                strCodeTrimLast = strDeleteBracket.LastIndexOf(")");
        //                                                strCodeToMatch = strDeleteBracket.Substring(0, strCodeTrimLast != -1 ? strCodeTrimLast - 2 : 0);        //strCodeTrimLast - 2 because we need to cut(1) from the string
        //                                                strCode = strCodeToMatch != string.Empty ? strCodeToMatch : strDeleteBracket;
        //                                                ObjtblPenalCode = new tblPenalCode();

        //                                                ObjtblPenalCode = lstCodes.Where(d => d.PenalCode.ToLower() == strCodeToMatch.ToLower() && d.PenalCodeRangeType == 0).FirstOrDefault();
        //                                                if (ObjtblPenalCode != null)
        //                                                {
        //                                                    IsMatchCode = true;
        //                                                    XElement XChargeDesc = new XElement("ChargeDescription");
        //                                                    LRCharge.Add(XChargeDesc);
        //                                                    XChargeDesc.Value = ObjtblPenalCode.PenalDescription.ToString();
        //                                                }
        //                                            }
        //                                            if (IsMatchCode == false)
        //                                            {
        //                                                strCodeTrimLast = strCode.LastIndexOf(")");
        //                                                strCodeToMatch = strCode.Substring(0, strCodeTrimLast != -1 ? strCodeTrimLast - 2 : 0);  //strCodeTrimLast - 2 because we need to cut(d) from the string

        //                                                ObjtblPenalCode = new tblPenalCode();

        //                                                ObjtblPenalCode = lstCodes.Where(d => d.PenalCode.ToLower() == strCodeToMatch.ToLower() && d.PenalCodeRangeType == 0).FirstOrDefault();
        //                                                if (ObjtblPenalCode != null)
        //                                                {
        //                                                    IsMatchCode = true;
        //                                                    XElement XChargeDesc = new XElement("ChargeDescription");
        //                                                    LRCharge.Add(XChargeDesc);
        //                                                    XChargeDesc.Value = ObjtblPenalCode.PenalDescription.ToString();
        //                                                }
        //                                            }
        //                                            if (IsMatchCode == false)                                                              // Checking Numeric range
        //                                            {
        //                                                int IsNumeric = 0;
        //                                                try
        //                                                {
        //                                                    int.TryParse(strDeleteBracket, out IsNumeric);
        //                                                    if (IsNumeric > 0)
        //                                                    {
        //                                                        List<tblPenalCode> lstUpdatedCodes = new List<tblPenalCode>();
        //                                                        lstUpdatedCodes = lstCodes.Where(d => d.PenalCode_From != string.Empty && d.PenalCode_To != string.Empty).ToList<tblPenalCode>();

        //                                                        foreach (tblPenalCode item in lstUpdatedCodes)
        //                                                        {
        //                                                            int IsNumericPenalCode_From = 0; int IsNumericPenalCode_To = 0;

        //                                                            int.TryParse(item.PenalCode_From, out IsNumericPenalCode_From);

        //                                                            int.TryParse(item.PenalCode_To, out IsNumericPenalCode_To);

        //                                                            if (IsNumericPenalCode_From >= 0 && IsNumericPenalCode_To >= 0)
        //                                                            {
        //                                                                ObjtblPenalCode = new tblPenalCode();
        //                                                                ObjtblPenalCode = lstUpdatedCodes.Where(d => IsNumericPenalCode_From <= IsNumeric && IsNumericPenalCode_To <= IsNumeric && d.PenalCodeRangeType == 1).FirstOrDefault();
        //                                                                if (ObjtblPenalCode != null)
        //                                                                {
        //                                                                    IsMatchCode = true;
        //                                                                    XElement XChargeDesc = new XElement("ChargeDescription");
        //                                                                    LRCharge.Add(XChargeDesc);
        //                                                                    XChargeDesc.Value = ObjtblPenalCode.PenalDescription.ToString();
        //                                                                }
        //                                                            }
        //                                                        }
        //                                                    }
        //                                                }
        //                                                catch
        //                                                {
        //                                                }
        //                                            }
        //                                            if (IsMatchCode == false)     // If not numeric then code here        (Alphabets Range Type)
        //                                            {
        //                                                try
        //                                                {
        //                                                    string strCodeLastAlphabet = strDeleteBracket.Substring(strDeleteBracket.Length - 1, 1).ToLower();

        //                                                    strCodeTrimLast = strDeleteBracket.Length - 1;
        //                                                    strCodeToMatch = strDeleteBracket.Substring(0, strCodeTrimLast != -1 ? strCodeTrimLast : 0).ToLower();

        //                                                    ObjtblPenalCode = new tblPenalCode();
        //                                                    ObjtblPenalCode = lstCodes.Where(d => d.PenalCode.ToLower() == strCodeToMatch && d.PenalCode_From != string.Empty && d.PenalCode_To != string.Empty && d.PenalCodeRangeType == 2).FirstOrDefault();
        //                                                    if (ObjtblPenalCode != null)
        //                                                    {
        //                                                        string FromRange = ObjtblPenalCode.PenalCode_From;
        //                                                        string ToRange = ObjtblPenalCode.PenalCode_To;
        //                                                        if (FromRange.Length == 1 && ToRange.Length == 1)
        //                                                        {
        //                                                            try
        //                                                            {
        //                                                                char cfrom = Convert.ToChar(FromRange); char cTo = Convert.ToChar(ToRange); char cToMtach = Convert.ToChar(strCodeLastAlphabet);

        //                                                                int fromRg = Convert.ToInt32(cfrom);
        //                                                                int ToRg = Convert.ToInt32(cTo);
        //                                                                int iRangeToMatch = Convert.ToInt32(cToMtach);

        //                                                                if (iRangeToMatch >= fromRg && iRangeToMatch <= ToRg)
        //                                                                {
        //                                                                    IsMatchCode = true;
        //                                                                    XElement XChargeDesc = new XElement("ChargeDescription");
        //                                                                    LRCharge.Add(XChargeDesc);
        //                                                                    XChargeDesc.Value = ObjtblPenalCode.PenalDescription.ToString();
        //                                                                }
        //                                                            }
        //                                                            catch
        //                                                            {
        //                                                            }
        //                                                        }
        //                                                    }
        //                                                }
        //                                                catch
        //                                                {
        //                                                }
        //                                            }
        //                                            //else
        //                                            //{
        //                                            //    IsMatchCode = true;
        //                                            //    XElement XChargeDesc = new XElement("ChargeDescription");
        //                                            //    LRCharge.Add(XChargeDesc);
        //                                            //    XChargeDesc.Add(string.Empty);

        //                                            //}                                                
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                    #endregion If region is California then we need to add in xml
        //                }
        //                catch
        //                {
        //                    ObjXDocumentUppdatedLiveRunner = XDocument.Parse(XML_LiveRunner);
        //                }


        //            }
        //        }
        //    }
        //    #endregion Penal Codes Section

        //    ObjXDocumentUppdatedLiveRunner = ObjXDocumentLiveRunner;

        //    #region Ticket #871: Add Statue Severity Level to CCR1* Los Angeles reports   (Product Code = "CCR1Liverunner" and county should be "Los Angeles" only)

        //    XDocument ObjXDocumentSeverity = new XDocument();
        //    ObjXDocumentSeverity = ObjXDocumentUppdatedLiveRunner != null ? ObjXDocumentUppdatedLiveRunner : XDocument.Parse(XML_LiveRunner);

        //    try
        //    {
        //        //if (ProductCode.ToUpper() == "CCR1LIVERUNNER") EDITTED FOR INT-9 TO ALLOW FOR NCR REPORT

        //        if (ProductCode.ToUpper() == "CCR1LIVERUNNER")
        //        {
        //            var LRSeverityScreenings = ObjXDocumentSeverity.Descendants("Screenings").ToList();
        //            string strSeverityRegion = string.Empty;

        //            if (LRSeverityScreenings.Count > 0)
        //            {
        //                var SeverityRegion = LRSeverityScreenings.Descendants("Region").FirstOrDefault();
        //                if (SeverityRegion != null)                                                                 // If Region is CA (California) then we need to add description
        //                {
        //                    strSeverityRegion = SeverityRegion.Value.ToString();
        //                    //change for int-122
        //                    if (strSeverityRegion == "CA" || strSeverityRegion == "SC")                                  //if (strRegion == "CA") FOR SOUTH CAROLINA


        //                    //if (strRegion == "CA") county changes by ak  // add new requirement for state='south carloina'
        //                    {
        //                        #region if region is CA AND SC and county is los angeles

        //                        if (ObjXDocumentSeverity != null)
        //                        {
        //                            var LRCriminalReports = LRSeverityScreenings.Descendants("CriminalReport").FirstOrDefault();

        //                            if (LRCriminalReports != null)
        //                            {
        //                                var LRCriminalCaseslst = LRCriminalReports.Descendants("CriminalCase").ToList();
        //                                for (int iRow = 0; iRow < LRCriminalCaseslst.Count; iRow++)
        //                                {

        //                                    var LRCriminalCases = LRCriminalCaseslst.ElementAt(iRow);
        //                                    var CourtName = LRCriminalCases.Descendants("CourtName").FirstOrDefault();

        //                                    if (CourtName != null)
        //                                    {
        //                                        string strCourtName = CourtName.Value.ToString();
        //                                        if (strCourtName.ToLower().Contains("los angeles") || strCourtName.ToLower().Contains("abbeville")
        //                                            || strCourtName.ToLower().Contains("allendale")
        //                                            || strCourtName.ToLower().Contains("anderson")
        //                                            || strCourtName.ToLower().Contains("bamberg")
        //                                            || strCourtName.ToLower().Contains("barnwell")
        //                                            || strCourtName.ToLower().Contains("beaufort")
        //                                            || strCourtName.ToLower().Contains("berkeley")
        //                                            || strCourtName.ToLower().Contains("calhoun")
        //                                            || strCourtName.ToLower().Contains("charleston")
        //                                            || strCourtName.ToLower().Contains("cherokee")
        //                                            || strCourtName.ToLower().Contains("chester")
        //                                            || strCourtName.ToLower().Contains("chesterfield")
        //                                            || strCourtName.ToLower().Contains("clarendon")
        //                                            || strCourtName.ToLower().Contains("colleton")
        //                                            || strCourtName.ToLower().Contains("darlington")
        //                                            || strCourtName.ToLower().Contains("dillon")
        //                                            || strCourtName.ToLower().Contains("dorchester")
        //                                            || strCourtName.ToLower().Contains("edgefield")
        //                                            || strCourtName.ToLower().Contains("fairfield")
        //                                            || strCourtName.ToLower().Contains("georgetown")
        //                                            || strCourtName.ToLower().Contains("greenwood")
        //                                            || strCourtName.ToLower().Contains("hampton")
        //                                            || strCourtName.ToLower().Contains("horry")
        //                                            || strCourtName.ToLower().Contains("jasper")
        //                                            || strCourtName.ToLower().Contains("kershaw")
        //                                            || strCourtName.ToLower().Contains("lancaster")
        //                                            || strCourtName.ToLower().Contains("laurens")
        //                                            || strCourtName.ToLower().Contains("lee")
        //                                            || strCourtName.ToLower().Contains("lexington")
        //                                            || strCourtName.ToLower().Contains("marion")
        //                                            || strCourtName.ToLower().Contains("marlboro")
        //                                            || strCourtName.ToLower().Contains("mccormick")
        //                                            || strCourtName.ToLower().Contains("newberry")
        //                                            || strCourtName.ToLower().Contains("oconee")
        //                                            || strCourtName.ToLower().Contains("richland")
        //                                            || strCourtName.ToLower().Contains("saluda")
        //                                            || strCourtName.ToLower().Contains("sumter")
        //                                            || strCourtName.ToLower().Contains("union")
        //                                            || strCourtName.ToLower().Contains("williamsburg")
        //                                            || strCourtName.ToLower().Contains("york")

        //                                            )
        //                                        // if (strCourtName.ToLower().Contains("south carolina"))

        //                                        //FOR INT -122
        //                                        //SOUTH CAROLINA
        //                                        //  if (strCourtName.ToLower().Contains("SOUTH CAROLINA"))
        //                                        {
        //                                            var lstAgencyReference = LRCriminalCases.Descendants("AgencyReference").ToList();
        //                                            if (lstAgencyReference.Count > 0)
        //                                            {
        //                                                string strSeverity = string.Empty;

        //                                                for (int iRef = 0; iRef < lstAgencyReference.Count; iRef++)
        //                                                {
        //                                                    var ObjAgencyReference = lstAgencyReference.ElementAt(iRef);

        //                                                    string AgencyReferenceType = ObjAgencyReference.Attribute("type").Value.ToString();

        //                                                    if (AgencyReferenceType.ToLower() == "docket")
        //                                                    {
        //                                                        var IdValue = ObjAgencyReference.Descendants("IdValue").FirstOrDefault();
        //                                                        if (IdValue != null)
        //                                                        {
        //                                                            string strDocketNumber = IdValue.Value.ToString();

        //                                                            string strFirstFiveChars = strDocketNumber.Substring(0, 5);

        //                                                            if (!string.IsNullOrEmpty(strFirstFiveChars))
        //                                                            {
        //                                                                bool IsAnyNumberinString = Regex.IsMatch(strFirstFiveChars, @"\d");
        //                                                                if (IsAnyNumberinString == true)                                            // if it contains any digit/ number 
        //                                                                {
        //                                                                    strSeverity = "misdemeanor";
        //                                                                }
        //                                                                else                                             // if all are characters          
        //                                                                {
        //                                                                    strSeverity = "felony";
        //                                                                }
        //                                                            }

        //                                                        }
        //                                                    }
        //                                                }
        //                                                var LRChargelst = LRCriminalCases.Descendants("Charge").ToList();
        //                                                if (LRChargelst.Count > 0)
        //                                                {
        //                                                    for (int iCharge = 0; iCharge < LRChargelst.Count; iCharge++)
        //                                                    {
        //                                                        var LRCharge = LRChargelst.ElementAt(iCharge);
        //                                                        var LRChargeTypeClassification = LRCharge.Descendants("ChargeTypeClassification").FirstOrDefault();

        //                                                        if (LRChargeTypeClassification != null)
        //                                                        {
        //                                                            LRChargeTypeClassification.Value = strSeverity;
        //                                                        }
        //                                                    }
        //                                                }
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }

        //                        #endregion if region is CA and SA and county is los angeles
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    catch
        //    {
        //        ObjXDocumentUppdatedLiveRunner = ObjXDocumentLiveRunner;
        //    }
        //    #endregion Ticket #871: Add Statue Severity Level to CCR1* Los Angeles and state is south cartloina for all county reports   (Product Code = "CCR1Liverunner" and county should be "Los Angeles" only)

        //    ObjXDocumentUppdatedLiveRunner = ObjXDocumentSeverity;

        //    return ObjXDocumentUppdatedLiveRunner;
        //}



        public XDocument GetXMLandInsertDescription(string XMLLiveRunner, List<tblPenalCode> lstCodes, string ProductCode)
        {
            // XMLLiveRunner = "";

            XDocument ObjXDocumentUppdatedLiveRunner = new XDocument();
            XDocument ObjXDocumentLiveRunner = new XDocument();
            string XML_LiveRunner = GetRequiredXml(XMLLiveRunner);
            ObjXDocumentLiveRunner = XDocument.Parse(XML_LiveRunner);

            #region Penal Codes Section
            var LRScreenings = ObjXDocumentLiveRunner.Descendants("Screenings").ToList();
            string strRegion = string.Empty;
            tblPenalCode ObjtblPenalCode = null;
            if (LRScreenings.Count > 0)
            {
                var Region = LRScreenings.Descendants("Region").FirstOrDefault();
                if (Region != null)                                                                                                     // If Region is CA (California) then we need to add description
                {
                    strRegion = Region.Value.ToString();

                    if (strRegion == "CA" || strRegion == "SC")                                  //if (strRegion == "CA") FOR SOTH CAROLIONA 
                    {

                        try
                        {
                            #region If region is California then we need to add in xml

                            var LRCriminalReports = LRScreenings.Descendants("CriminalReport").FirstOrDefault();

                            if (LRCriminalReports != null)
                            {
                                var LRCriminalCaseslst = LRCriminalReports.Descendants("CriminalCase").ToList();
                                for (int iRow = 0; iRow < LRCriminalCaseslst.Count; iRow++)
                                {
                                   // string Desc = string.Empty;
                                    var LRCriminalCases = LRCriminalCaseslst.ElementAt(iRow);
                                    var LRChargelst = LRCriminalCases.Descendants("Charge").ToList();
                                    if (LRChargelst.Count > 0)
                                    {
                                        for (int iCharge = 0; iCharge < LRChargelst.Count; iCharge++)
                                        {
                                            var LRCharge = LRChargelst.ElementAt(iCharge);
                                            var LRChargeOrComplaint = LRCharge.Descendants("ChargeOrComplaint").FirstOrDefault();
                                            if (LRChargeOrComplaint != null)
                                            {
                                                string PenalCode = LRChargeOrComplaint.Value.ToString();
                                                string[] strStatute = new string[] { "STATUTE:" };
                                                string AfterStatute = PenalCode.Split(strStatute, StringSplitOptions.RemoveEmptyEntries).LastOrDefault();

                                                if (!string.IsNullOrEmpty(AfterStatute))
                                                {
                                                    int DeleteBracket = AfterStatute.LastIndexOf(')');

                                                    string strDeleteBracket = AfterStatute.Substring(0, DeleteBracket != -1 ? DeleteBracket : 0).Trim().ToLower();

                                                    int strCodeTrimLast = 0;
                                                    string strCodeToMatch = "";

                                                    string strCode = "";
                                                    bool IsMatchCode = false;

                                                    ObjtblPenalCode = new tblPenalCode();
                                                    if (strRegion == "SC") // south carolina filteration  on 1 april
                                                    {

                                                        ObjtblPenalCode = lstCodes.Where(d => d.PenalCode.ToLower() == strDeleteBracket && d.PenalCodeRangeType == 0 && d.fkStateId == 34).FirstOrDefault();                    // Match Exact

                                                    }

                                                    else
                                                    {
                                                        ObjtblPenalCode = lstCodes.Where(d => d.PenalCode.ToLower() == strDeleteBracket && d.PenalCodeRangeType == 0 && d.fkStateId == 5).FirstOrDefault();                    // Match Exact
                                                    }
                                                    if (ObjtblPenalCode != null)
                                                    {
                                                        IsMatchCode = true;
                                                        XElement XChargeDesc = new XElement("ChargeDescription");
                                                        LRCharge.Add(XChargeDesc);
                                                        XChargeDesc.Value = ObjtblPenalCode.PenalDescription.ToString();
                                                    }
                                                    if (IsMatchCode == false)                                                          // Case 487(d)(1)  - It is example we have more codes like this example
                                                    {
                                                        strCodeTrimLast = strDeleteBracket.LastIndexOf(")");
                                                        strCodeToMatch = strDeleteBracket.Substring(0, strCodeTrimLast != -1 ? strCodeTrimLast - 2 : 0);        //strCodeTrimLast - 2 because we need to cut(1) from the string
                                                        strCode = strCodeToMatch != string.Empty ? strCodeToMatch : strDeleteBracket;
                                                        ObjtblPenalCode = new tblPenalCode();

                                                        if (strRegion == "SC") // south carolina filteration  on 1 april
                                                        {


                                                            ObjtblPenalCode = lstCodes.Where(d => d.PenalCode.ToLower() == strCodeToMatch.ToLower() && d.PenalCodeRangeType == 0 && d.fkStateId == 34).FirstOrDefault();
                                                        }
                                                        else
                                                        {
                                                            ObjtblPenalCode = lstCodes.Where(d => d.PenalCode.ToLower() == strCodeToMatch.ToLower() && d.PenalCodeRangeType == 0 && d.fkStateId == 5).FirstOrDefault();
                                                        }
                                                        if (ObjtblPenalCode != null)
                                                        {
                                                            IsMatchCode = true;
                                                            XElement XChargeDesc = new XElement("ChargeDescription");
                                                            LRCharge.Add(XChargeDesc);
                                                            XChargeDesc.Value = ObjtblPenalCode.PenalDescription.ToString();
                                                        }
                                                    }
                                                    if (IsMatchCode == false)
                                                    {
                                                        strCodeTrimLast = strCode.LastIndexOf(")");
                                                        strCodeToMatch = strCode.Substring(0, strCodeTrimLast != -1 ? strCodeTrimLast - 2 : 0);  //strCodeTrimLast - 2 because we need to cut(d) from the string

                                                        ObjtblPenalCode = new tblPenalCode();

                                                        if (strRegion == "SC") // south carolina filteration  on 1 april
                                                        {

                                                            ObjtblPenalCode = lstCodes.Where(d => d.PenalCode.ToLower() == strCodeToMatch.ToLower() && d.PenalCodeRangeType == 0 && d.fkStateId == 34).FirstOrDefault();

                                                        }

                                                        else
                                                        {
                                                            ObjtblPenalCode = lstCodes.Where(d => d.PenalCode.ToLower() == strCodeToMatch.ToLower() && d.PenalCodeRangeType == 0 && d.fkStateId == 5).FirstOrDefault();
                                                        }
                                                        if (ObjtblPenalCode != null)
                                                        {
                                                            IsMatchCode = true;
                                                            XElement XChargeDesc = new XElement("ChargeDescription");
                                                            LRCharge.Add(XChargeDesc);
                                                            XChargeDesc.Value = ObjtblPenalCode.PenalDescription.ToString();
                                                        }
                                                    }
                                                    if (IsMatchCode == false)                                                              // Checking Numeric range
                                                    {
                                                        int IsNumeric = 0;
                                                        try
                                                        {
                                                            int.TryParse(strDeleteBracket, out IsNumeric);
                                                            if (IsNumeric > 0)
                                                            {
                                                                List<tblPenalCode> lstUpdatedCodes = new List<tblPenalCode>();
                                                                if (strRegion == "SC") // south carolina filteration  on 1 april
                                                                {

                                                                    lstUpdatedCodes = lstCodes.Where(d => d.PenalCode_From != string.Empty && d.PenalCode_To != string.Empty && d.fkStateId == 34).ToList<tblPenalCode>();
                                                                }


                                                                else
                                                                {
                                                                    lstUpdatedCodes = lstCodes.Where(d => d.PenalCode_From != string.Empty && d.PenalCode_To != string.Empty && d.fkStateId == 5).ToList<tblPenalCode>();
                                                                }

                                                                foreach (tblPenalCode item in lstUpdatedCodes)
                                                                {
                                                                    int IsNumericPenalCode_From = 0; int IsNumericPenalCode_To = 0;

                                                                    int.TryParse(item.PenalCode_From, out IsNumericPenalCode_From);

                                                                    int.TryParse(item.PenalCode_To, out IsNumericPenalCode_To);

                                                                    if (IsNumericPenalCode_From >= 0 && IsNumericPenalCode_To >= 0)
                                                                    {
                                                                        ObjtblPenalCode = new tblPenalCode();
                                                                        if (strRegion == "SC") // south carolina filteration  on 1 april
                                                                        {
                                                                            ObjtblPenalCode = lstUpdatedCodes.Where(d => IsNumericPenalCode_From <= IsNumeric && IsNumericPenalCode_To <= IsNumeric && d.PenalCodeRangeType == 1 && d.fkStateId == 34).FirstOrDefault();

                                                                        }
                                                                        else
                                                                        {
                                                                            ObjtblPenalCode = lstUpdatedCodes.Where(d => IsNumericPenalCode_From <= IsNumeric && IsNumericPenalCode_To <= IsNumeric && d.PenalCodeRangeType == 1 && d.fkStateId == 5).FirstOrDefault();
                                                                        }
                                                                        if (ObjtblPenalCode != null)
                                                                        {
                                                                            IsMatchCode = true;
                                                                            XElement XChargeDesc = new XElement("ChargeDescription");
                                                                            LRCharge.Add(XChargeDesc);
                                                                            XChargeDesc.Value = ObjtblPenalCode.PenalDescription.ToString();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        catch
                                                        {
                                                        }
                                                    }
                                                    if (IsMatchCode == false)     // If not numeric then code here        (Alphabets Range Type)
                                                    {
                                                        try
                                                        {
                                                            string strCodeLastAlphabet = strDeleteBracket.Substring(strDeleteBracket.Length - 1, 1).ToLower();

                                                            strCodeTrimLast = strDeleteBracket.Length - 1;
                                                            strCodeToMatch = strDeleteBracket.Substring(0, strCodeTrimLast != -1 ? strCodeTrimLast : 0).ToLower();

                                                            ObjtblPenalCode = new tblPenalCode();
                                                            if (strRegion == "SC") // south carolina filteration  on 1 april
                                                            {

                                                                ObjtblPenalCode = lstCodes.Where(d => d.PenalCode.ToLower() == strCodeToMatch && d.PenalCode_From != string.Empty && d.PenalCode_To != string.Empty && d.PenalCodeRangeType == 2 && d.fkStateId == 34).FirstOrDefault();

                                                            }

                                                            else
                                                            {
                                                                ObjtblPenalCode = lstCodes.Where(d => d.PenalCode.ToLower() == strCodeToMatch && d.PenalCode_From != string.Empty && d.PenalCode_To != string.Empty && d.PenalCodeRangeType == 2 && d.fkStateId == 5).FirstOrDefault();

                                                            }
                                                            if (ObjtblPenalCode != null)
                                                            {
                                                                string FromRange = ObjtblPenalCode.PenalCode_From;
                                                                string ToRange = ObjtblPenalCode.PenalCode_To;
                                                                if (FromRange.Length == 1 && ToRange.Length == 1)
                                                                {
                                                                    try
                                                                    {
                                                                        char cfrom = Convert.ToChar(FromRange); char cTo = Convert.ToChar(ToRange); char cToMtach = Convert.ToChar(strCodeLastAlphabet);

                                                                        int fromRg = Convert.ToInt32(cfrom);
                                                                        int ToRg = Convert.ToInt32(cTo);
                                                                        int iRangeToMatch = Convert.ToInt32(cToMtach);

                                                                        if (iRangeToMatch >= fromRg && iRangeToMatch <= ToRg)
                                                                        {
                                                                            IsMatchCode = true;
                                                                            XElement XChargeDesc = new XElement("ChargeDescription");
                                                                            LRCharge.Add(XChargeDesc);
                                                                            XChargeDesc.Value = ObjtblPenalCode.PenalDescription.ToString();
                                                                        }
                                                                    }
                                                                    catch
                                                                    {
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        catch
                                                        {
                                                        }
                                                    }
                                                    //else
                                                    //{
                                                    //    IsMatchCode = true;
                                                    //    XElement XChargeDesc = new XElement("ChargeDescription");
                                                    //    LRCharge.Add(XChargeDesc);
                                                    //    XChargeDesc.Add(string.Empty);

                                                    //}                                                
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion If region is California then we need to add in xml
                        }
                        catch
                        {
                            ObjXDocumentUppdatedLiveRunner = XDocument.Parse(XML_LiveRunner);
                        }


                    }
                }
            }
            #endregion Penal Codes Section

            ObjXDocumentUppdatedLiveRunner = ObjXDocumentLiveRunner;

            #region Ticket #871: Add Statue Severity Level to CCR1* Los Angeles reports   (Product Code = "CCR1Liverunner" and county should be "Los Angeles" only)

            XDocument ObjXDocumentSeverity = new XDocument();
            ObjXDocumentSeverity = ObjXDocumentUppdatedLiveRunner != null ? ObjXDocumentUppdatedLiveRunner : XDocument.Parse(XML_LiveRunner);

            try
            {
                //if (ProductCode.ToUpper() == "CCR1LIVERUNNER") EDITTED FOR INT-9 TO ALLOW FOR NCR REPORT

                if (ProductCode.ToUpper() == "CCR1LIVERUNNER")
                {
                    var LRSeverityScreenings = ObjXDocumentSeverity.Descendants("Screenings").ToList();
                    string strSeverityRegion = string.Empty;

                    if (LRSeverityScreenings.Count > 0)
                    {
                        var SeverityRegion = LRSeverityScreenings.Descendants("Region").FirstOrDefault();
                        if (SeverityRegion != null)                                                                 // If Region is CA (California) then we need to add description
                        {
                            strSeverityRegion = SeverityRegion.Value.ToString();
                            //change for int-122
                            if (strSeverityRegion == "CA")                                  // CHANGE ON 02 APRIL 2015 FOR misdemeanor ISSUE GIEVN BY CLEINT FOR sOTH CAROLINA
                            //if (strSeverityRegion == "CA" || strSeverityRegion == "SC")                                  //if (strRegion == "CA") FOR SOUTH CAROLINA

                            //if (strRegion == "CA") county changes by ak  // add new requirement for state='south carloina'
                            {
                                #region if region is CA AND SC and county is los angeles

                                if (ObjXDocumentSeverity != null)
                                {
                                    var LRCriminalReports = LRSeverityScreenings.Descendants("CriminalReport").FirstOrDefault();

                                    if (LRCriminalReports != null)
                                    {
                                        var LRCriminalCaseslst = LRCriminalReports.Descendants("CriminalCase").ToList();
                                        for (int iRow = 0; iRow < LRCriminalCaseslst.Count; iRow++)
                                        {

                                            var LRCriminalCases = LRCriminalCaseslst.ElementAt(iRow);
                                            var CourtName = LRCriminalCases.Descendants("CourtName").FirstOrDefault();

                                            if (CourtName != null)
                                            {
                                                string strCourtName = CourtName.Value.ToString();
                                                //if (strCourtName.ToLower().Contains("los angeles") || strCourtName.ToLower().Contains("abbeville")
                                                //    || strCourtName.ToLower().Contains("allendale")
                                                //    || strCourtName.ToLower().Contains("anderson")
                                                //    || strCourtName.ToLower().Contains("bamberg")
                                                //    || strCourtName.ToLower().Contains("barnwell")
                                                //    || strCourtName.ToLower().Contains("beaufort")
                                                //    || strCourtName.ToLower().Contains("berkeley")
                                                //    || strCourtName.ToLower().Contains("calhoun")
                                                //    || strCourtName.ToLower().Contains("charleston")
                                                //    || strCourtName.ToLower().Contains("cherokee")
                                                //    || strCourtName.ToLower().Contains("chester")
                                                //    || strCourtName.ToLower().Contains("chesterfield")
                                                //    || strCourtName.ToLower().Contains("clarendon")
                                                //    || strCourtName.ToLower().Contains("colleton")
                                                //    || strCourtName.ToLower().Contains("darlington")
                                                //    || strCourtName.ToLower().Contains("dillon")
                                                //    || strCourtName.ToLower().Contains("dorchester")
                                                //    || strCourtName.ToLower().Contains("edgefield")
                                                //    || strCourtName.ToLower().Contains("fairfield")
                                                //    || strCourtName.ToLower().Contains("georgetown")
                                                //    || strCourtName.ToLower().Contains("greenwood")
                                                //    || strCourtName.ToLower().Contains("hampton")
                                                //    || strCourtName.ToLower().Contains("horry")
                                                //    || strCourtName.ToLower().Contains("jasper")
                                                //    || strCourtName.ToLower().Contains("kershaw")
                                                //    || strCourtName.ToLower().Contains("lancaster")
                                                //    || strCourtName.ToLower().Contains("laurens")
                                                //    || strCourtName.ToLower().Contains("lee")
                                                //    || strCourtName.ToLower().Contains("lexington")
                                                //    || strCourtName.ToLower().Contains("marion")
                                                //    || strCourtName.ToLower().Contains("marlboro")
                                                //    || strCourtName.ToLower().Contains("mccormick")
                                                //    || strCourtName.ToLower().Contains("newberry")
                                                //    || strCourtName.ToLower().Contains("oconee")
                                                //    || strCourtName.ToLower().Contains("richland")
                                                //    || strCourtName.ToLower().Contains("saluda")
                                                //    || strCourtName.ToLower().Contains("sumter")
                                                //    || strCourtName.ToLower().Contains("union")
                                                //    || strCourtName.ToLower().Contains("williamsburg")
                                                //    || strCourtName.ToLower().Contains("york")

                                                //    )


                                                if (strCourtName.ToLower().Contains("los angeles")) // CHANGE ON 02 APRIL 2015 FOR misdemeanor ISSUE GIEVN BY CLEINT FOR sOTH CAROLINA

                                                // if (strCourtName.ToLower().Contains("south carolina"))

                                                //FOR INT -122
                                                //SOUTH CAROLINA
                                                //  if (strCourtName.ToLower().Contains("SOUTH CAROLINA"))
                                                {
                                                    var lstAgencyReference = LRCriminalCases.Descendants("AgencyReference").ToList();
                                                    if (lstAgencyReference.Count > 0)
                                                    {
                                                        string strSeverity = string.Empty;

                                                        for (int iRef = 0; iRef < lstAgencyReference.Count; iRef++)
                                                        {
                                                            var ObjAgencyReference = lstAgencyReference.ElementAt(iRef);

                                                            string AgencyReferenceType = ObjAgencyReference.Attribute("type").Value.ToString();

                                                            if (AgencyReferenceType.ToLower() == "docket")
                                                            {
                                                                var IdValue = ObjAgencyReference.Descendants("IdValue").FirstOrDefault();
                                                                if (IdValue != null)
                                                                {
                                                                    string strDocketNumber = IdValue.Value.ToString();

                                                                    string strFirstFiveChars = strDocketNumber.Substring(0, 5);

                                                                    if (!string.IsNullOrEmpty(strFirstFiveChars))
                                                                    {
                                                                        bool IsAnyNumberinString = Regex.IsMatch(strFirstFiveChars, @"\d");
                                                                        if (IsAnyNumberinString == true)                                            // if it contains any digit/ number 
                                                                        {
                                                                            strSeverity = "misdemeanor";
                                                                        }
                                                                        else                                             // if all are characters          
                                                                        {
                                                                            strSeverity = "felony";
                                                                        }
                                                                    }

                                                                }
                                                            }
                                                        }
                                                        var LRChargelst = LRCriminalCases.Descendants("Charge").ToList();
                                                        if (LRChargelst.Count > 0)
                                                        {
                                                            for (int iCharge = 0; iCharge < LRChargelst.Count; iCharge++)
                                                            {
                                                                var LRCharge = LRChargelst.ElementAt(iCharge);
                                                                var LRChargeTypeClassification = LRCharge.Descendants("ChargeTypeClassification").FirstOrDefault();

                                                                if (LRChargeTypeClassification != null)
                                                                {
                                                                    LRChargeTypeClassification.Value = strSeverity;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                #endregion if region is CA and SA and county is los angeles
                            }
                        }
                    }
                }
            }

            catch
            {
                ObjXDocumentUppdatedLiveRunner = ObjXDocumentLiveRunner;
            }
            #endregion Ticket #871: Add Statue Severity Level to CCR1* Los Angeles and state is south cartloina for all county reports   (Product Code = "CCR1Liverunner" and county should be "Los Angeles" only)

            ObjXDocumentUppdatedLiveRunner = ObjXDocumentSeverity;

            return ObjXDocumentUppdatedLiveRunner;
        }
        public static string GetRequiredXml(string XmlResponse)
        {
            string XmlResponse_Local = XmlResponse;
            XmlResponse_Local = XmlResponse_Local.Replace("</BackgroundReports>", "");
            int xmlstartindex = XmlResponse_Local.IndexOf("<BackgroundReportPackage>");
            XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
            return XmlResponse_Local;
        }
    }

    public struct clsPenalCodes
    {
        public int pkPenalCodeId { get; set; }
        public string PenalCode { get; set; }
        public string PenalTitle { get; set; }
        public string PenalDescription { get; set; }
        public int fkStateId { get; set; }
        public string ProductCode { get; set; }
        public string PenalCode_From { get; set; }
        public string PenalCode_To { get; set; }
        public byte PenalCodeRangeType { get; set; }
    }
}
