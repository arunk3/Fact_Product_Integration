﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Emerge.Data;
using System.Web.Security;
using System.Web.UI;
using System.Web;
using System.Data.Common;
using System.Xml;
using System.Net.Mail;
using System.Xml.Linq;
using VendorServices;
using System.Globalization;
using System.IO;
using Emerge.Services.Helper;
using System.Configuration;
using System.Net;



namespace Emerge.Services
{
    public class BALOrders
    {
        private int? _mCompanyUserId;
        public BALOrders() { }
        public string _rcxfilepath;
        public string RCXFilePath
        {
            get { return _rcxfilepath; }
            set { _rcxfilepath = value; }
        }
        public string _rcxRequestpath;
        public string RCXRequestPath
        {
            get { return _rcxRequestpath; }
            set { _rcxRequestpath = value; }
        }
        public BALOrders(int CompanyUserId)
        {
            _mCompanyUserId = CompanyUserId;
        }

        public List<Proc_GetSavedReportsForMVCResult> GetSavedReportsMVC(int ProductId, int CompanyUserId, int CompanyId,
                                                               int LocationId, string SortingColumn, string SortingDirection,
                                                               bool AllowPaging, int PageNum, int PageSize,
                                                               string FromDate, string ToDate, string SearchKeyword, byte OrderStatus, string ReportStatus, bool ShowIncomplete, string ReferenceCode, bool SavedReport6Month, string SearchTblName)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {

                return ObjDALDataContext.GetSavedReportsForMVC(ProductId, CompanyUserId, CompanyId,
                                                                       LocationId, SortingColumn, SortingDirection,
                                                                       AllowPaging, PageNum, PageSize,
                                                                       FromDate, ToDate, SearchKeyword, OrderStatus, ReportStatus, ShowIncomplete, ReferenceCode, SavedReport6Month, SearchTblName).ToList();

            }
        }


        public List<Proc_GetNoticeForAdminDashboardResult> GetNoticeForAdminDashboard(string SortingColm, string SortingDir, bool IsPaging, int pagenum, int pagesize, int ReportType, bool IsNoticeEmail, int fkOrderId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetNoticeForAdminDashboard(SortingColm, SortingDir, IsPaging, pagenum, pagesize, ReportType, IsNoticeEmail, fkOrderId).ToList();
            }
        }


        public List<proc_GetSavedReportsCountResult> GetSavedReportsCount(int ProductId, int CompanyUserId, int CompanyId,
                                                            int LocationId,
                                                            string FromDate, string ToDate, string SearchKeyword, byte OrderStatus, string ReportStatus, bool ShowIncomplete, bool SavedReport6Month)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetSavedReportsCount(ProductId, CompanyUserId, CompanyId,
                                                                       LocationId, FromDate, ToDate, SearchKeyword, OrderStatus, ReportStatus, ShowIncomplete, SavedReport6Month).ToList();

            }
        }

        public tblCompany GetPkCompanyIdByComapnyName(string CompnayName)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                tblCompany ObjRspnData = DX.tblCompanies.Where(db => db.CompanyName == CompnayName).FirstOrDefault();
                return ObjRspnData;
            }
        }

        public List<ProcGetAutoCompleteSearchRecordsResult> GetAutoCompleteSearchRecords(string keyword, Guid fkUserId, bool IsAdmin)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return DX.ProcGetAutoCompleteSearchRecords(keyword, fkUserId, IsAdmin).ToList<ProcGetAutoCompleteSearchRecordsResult>();
            }
        }

        public List<Proc_GetAutoCompleteForOfficeResult> GetAutoCompleteTop10Records(string keyword, Guid CurrentUserId, bool IsAdmin)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return DX.GetAutoCompleteForOffice(keyword, CurrentUserId, IsAdmin).ToList<Proc_GetAutoCompleteForOfficeResult>();
            }
        }

        public List<Proc_GetCallLogCompanyListforAutoCompleteResult> GetCallLogTop10ComapnyRecords(string keyword, Guid CurrentUserId, bool IsAdmin, string PageType)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return DX.GetCallLogCompanyListforAutoComplete(keyword, CurrentUserId, IsAdmin, PageType).ToList<Proc_GetCallLogCompanyListforAutoCompleteResult>();

            }
        }

        private bool IsDuplicateOrder(string OrderNo)
        {
            List<tblOrder> ObjtblOrder = new List<tblOrder>();
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                ObjtblOrder = ObjDALDataContext.tblOrders.Where(data => data.OrderNo == OrderNo.Trim()).ToList<tblOrder>();

            }
            return (ObjtblOrder.Count == 0 ? false : true);
        }
        /// <summary>
        /// This method is used to add drug test order
        /// </summary>
        /// <param name="ObjtblOrder"></param>
        /// <param name="ObjCollOrderDetail"></param>
        /// <param name="ObjtblOrderSearchedData"></param>
        /// <param name="ApplicationId"></param>
        /// <returns></returns>
        public int? AddDrugTestOrders(tblOrder ObjtblOrder, List<tblOrderDetail> ObjCollOrderDetail, tblOrderSearchedData ObjtblOrderSearchedData, Guid ApplicationId)
        {
            #region Variable Assignment

            int? OrderId = 0;
            DbTransaction DbTrans = null;
            bool IsTieredOrder = false;
            #endregion

            #region Check is current process Duplicate Or not

            if (IsDuplicateOrder(ObjtblOrder.OrderNo.Trim()))
            {
                return OrderId;
            }

            #endregion

            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    #region Connection Open and Transaction Assignment

                    ObjDALDataContext.Connection.Open(); /* Connection Open */
                    DbTrans = ObjDALDataContext.Connection.BeginTransaction(); /* Assign Transation */
                    ObjDALDataContext.Transaction = DbTrans;

                    #endregion

                    #region Insert in Order Table

                    /* Here data insert in main order table and after successfully insertion orderid will be returned  */

                    int? LocationId = ObjtblOrder.fkLocationId;
                    DateTime OrderedDate = ObjtblOrder.OrderDt;

                    List<Proc_AddEmergeOrdersResult> ObjAddOrderResult = ObjDALDataContext.AddEmergeOrders(LocationId,
                                                                                                    ObjtblOrder.fkCompanyUserId,
                                                                                                    ObjtblOrder.OrderNo,
                                                                                                    OrderedDate,
                                                                                                    ObjtblOrder.OrderStatus,
                                                                                                    ObjtblOrder.ReportIncludes,
                                                                                                    ObjtblOrder.CreatedDate,
                                                                                                    ObjtblOrder.CreatedById,
                                                                                                    ObjtblOrder.IsDrugTestingOrder, 0, false, string.Empty, string.Empty, 1, string.Empty, null, false, ObjtblOrder.SearchPersonHTML, IsTieredOrder).ToList();

                    if (ObjAddOrderResult.First().ReturnType > 0) /* If insertinon is successfully done then returntype must be greater than zero  */
                    {
                        OrderId = ObjAddOrderResult.First().OrderId;
                    }

                    if (OrderId == 0) /* If insertion in order table is not successfull then data is rollback */
                    {
                        if (DbTrans != null)
                        {
                            DbTrans.Rollback(); /* Rollback Transaction  */
                        }
                        return OrderId; /* An empty orderid wlll be returned */
                    }

                    #endregion

                    #region Insert in Order Search Table

                    /* In this following code all searched data inserted */
                    // 1 is saved as a county Id temporary
                    int iOrderSearchedData = ObjDALDataContext.AddOrderSearchedData(OrderId,
                                                                                    ObjtblOrderSearchedData.SearchedLastName,
                                                                                    ObjtblOrderSearchedData.SearchedFirstName,
                                                                                    ObjtblOrderSearchedData.SearchedMiddleInitial,
                                                                                    ObjtblOrderSearchedData.SearchedSuffix,
                                                                                    ObjtblOrderSearchedData.SearchedSsn,
                                                                                    ObjtblOrderSearchedData.SearchedDob,
                                                                                    ObjtblOrderSearchedData.SearchedPhoneNumber,
                                                                                    ObjtblOrderSearchedData.SearchedStreetAddress,
                                                                                    ObjtblOrderSearchedData.SearchedDirection,
                                                                                    ObjtblOrderSearchedData.SearchedStreetName,
                                                                                    ObjtblOrderSearchedData.SearchedStreetType,
                                                                                    ObjtblOrderSearchedData.SearchedApt,
                                                                                    ObjtblOrderSearchedData.SearchedCity,
                                                                                    ObjtblOrderSearchedData.SearchedStateId,
                                                                                    ObjtblOrderSearchedData.SearchedZipCode,
                                                                                    ObjtblOrderSearchedData.SearchedCountyId,
                                                                                    ObjtblOrderSearchedData.SearchedDriverLicense,
                                                                                    ObjtblOrderSearchedData.SearchedVehicleVin,
                                                                                    ObjtblOrderSearchedData.SearchedDLStateId,
                                                                                    ObjtblOrderSearchedData.SearchedSex,
                                                                                    ObjtblOrderSearchedData.SearchedBusinessName,
                                                                                    ObjtblOrderSearchedData.SearchedBusinessCity,
                                                                                    ObjtblOrderSearchedData.SearchedBusinessStateId,
                                                                                    ObjtblOrderSearchedData.SearchedTrackingRef,
                                                                                    ObjtblOrderSearchedData.SearchedTrackingRefId,
                                                                                    ObjtblOrderSearchedData.SearchedTrackingNotes,
                                                                                    ObjtblOrderSearchedData.SearchedApplicantEmail,
                                                                                    ObjtblOrderSearchedData.SearchedPermissibleId//ND-5                                                                                   
                                                                                    );

                    if (iOrderSearchedData < 0)
                    {
                        OrderId = 0;
                        if (DbTrans != null)
                        {
                            DbTrans.Rollback(); /* Rollback Transaction */
                        }
                        return OrderId;
                    }

                    #endregion

                    #region Insert in Order Detail Table

                    for (int iProduct = 0; iProduct < ObjCollOrderDetail.Count; iProduct++) /* This is for searched product  */
                    {
                        int? OrderDetailId = 0;
                        List<Proc_AddEmergeDrugTestOrderDetailResult> ObjAddOrderDetailResult = ObjDALDataContext.AddEmergeDrugTestOrderDetail(
                                                                                                    OrderId,
                                                                                                    ObjCollOrderDetail.ElementAt(iProduct).fkProductApplicationId,
                                                                                                    ApplicationId,
                                                                                                    LocationId,
                                                                                                    OrderedDate,
                                                                                                    ObjCollOrderDetail.ElementAt(iProduct).ProductQty,
                                                                                                    ObjCollOrderDetail.ElementAt(iProduct).ProductPanel,
                                                                                                    ObjCollOrderDetail.ElementAt(iProduct).ReportAmount).ToList();
                        if (ObjAddOrderDetailResult.First().ReturnType > 0)
                        {
                            OrderDetailId = ObjAddOrderDetailResult.First().OrderDetailId;
                        }

                        if (OrderDetailId == 0) /*  If order detailed id is returned empty then data will be rollbacked  */
                        {
                            OrderId = 0;
                            if (DbTrans != null)
                            {
                                DbTrans.Rollback(); /* Rollback Transaction */
                            }
                            return OrderId;
                        }
                    }

                    #endregion

                    #region Transaction Committed

                    DbTrans.Commit(); /* Commit Transaction */

                    #endregion
                }
                catch (Exception ex)
                {
                    #region Transaction Rollback

                    OrderId = 0;
                    if (DbTrans != null)
                    {
                        DbTrans.Rollback(); /* Rollback Transaction */
                    }

                    //Added log for exception handling.
                    VendorServices.GeneralService.WriteLog("Some Error Occured in AddDrugTestOrders(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

                    #endregion
                }
                finally
                {
                    #region Connection and Transaction Close

                    ObjDALDataContext.Transaction = null;
                    if (ObjDALDataContext.Connection.State == ConnectionState.Open)
                    {
                        ObjDALDataContext.Connection.Close(); /* Close Connection */
                    }

                    #endregion
                }

                #region Return OrderId

                return OrderId; /* Here we return successfully inserted order id */

                #endregion
            }
        }

        public List<Proc_Get_RequestAndResponseResult> GetRequestAndResponseByOrderId(int? PkOrderId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetRequestAndResponse(PkOrderId).ToList<Proc_Get_RequestAndResponseResult>();
            }
        }

        /// <summary>
        /// This method is used to get saved reports
        /// </summary>
        /// <param name="ProductId"></param>
        /// <param name="CompanyUserId"></param>
        /// <param name="CompanyId"></param>
        /// <param name="LocationId"></param>
        /// <param name="SortingColumn"></param>
        /// <param name="SortingDirection"></param>
        /// <param name="AllowPaging"></param>
        /// <param name="PageNum"></param>
        /// <param name="PageSize"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="SearchKeyword"></param>
        /// <param name="OrderStatus"></param>
        /// <returns></returns>
        public List<Proc_GetSavedReportsResult> GetSavedReports(int ProductId, int CompanyUserId, int CompanyId,
                                                                int LocationId, string SortingColumn, string SortingDirection,
                                                                bool AllowPaging, int PageNum, int PageSize,
                                                                string FromDate, string ToDate, string SearchKeyword, byte OrderStatus, string ReportStatus)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetSavedReports(ProductId, CompanyUserId, CompanyId,
                                                                       LocationId, SortingColumn, SortingDirection,
                                                                       AllowPaging, PageNum, PageSize,
                                                                       FromDate, ToDate, SearchKeyword, OrderStatus, ReportStatus).ToList();

            }
        }

        public int UpdateConsentInfo(List<tblOrderDetail> lstOrderDetail)
        {
            int iResult = -1;
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    foreach (tblOrderDetail Obj in lstOrderDetail)
                    {
                        tblOrderDetail ObjOrderDetail = DX.tblOrderDetails.Where(d => d.pkOrderDetailId == Obj.pkOrderDetailId).FirstOrDefault();
                        if (ObjOrderDetail != null)
                        {
                            ObjOrderDetail.IsConsentRequired = Obj.IsConsentRequired;
                            ObjOrderDetail.ConsentFileName = Obj.ConsentFileName;
                            ObjOrderDetail.IsConsentEmailorFax = Obj.IsConsentEmailorFax;
                            DX.SubmitChanges();
                            iResult = 1;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateConsentInfo(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            finally
            { }
            return iResult;
        }
        public void UpdateOrderResponse(int? OrderDetailId, string vendor_Res)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {

                if (DX.tblOrderResponseDatas.Where(db => db.fkOrderDetailId == OrderDetailId).Count() > 0)
                {
                    //INT-48 //Something is broken with MVR integration (Softech)
                    tblOrderResponseData ObjRspnData = DX.tblOrderResponseDatas.Where(db => db.fkOrderDetailId == OrderDetailId).FirstOrDefault();
                    if (ObjRspnData != null)
                    {
                        //To check if selected orderDetailId releted to NCR1 then no update applied.
                        var byPassNCRUpdate = DX.tblOrderDetails.Where(o => o.pkOrderDetailId == OrderDetailId && o.fkProductApplicationId == 30).FirstOrDefault();
                        if (byPassNCRUpdate == null)//This process will update other product exceept NCR1
                        {
                            DX.UpdateOrderResponse(ObjRspnData.pkOrderResponseId, vendor_Res);
                        }
                    }
                }
                else
                {
                    tblOrderResponseData_Archieve ObjRspnData = DX.tblOrderResponseData_Archieves.Where(db => db.fkOrderDetailId == OrderDetailId).FirstOrDefault();
                    if (ObjRspnData != null)
                    {
                        ObjRspnData.ResponseData = vendor_Res;
                        DX.SubmitChanges();
                    }
                }

            }
        }


        /// <summary>
        /// To update response 
        /// </summary>
        /// <param name="OrderDetailId"></param>
        /// <param name="vendor_Res"></param>
        public void UpdateOrderResponseOnUpdateStatus(int? OrderDetailId, string vendor_Res)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {

                if (DX.tblOrderResponseDatas.Where(db => db.fkOrderDetailId == OrderDetailId).Count() > 0)
                {
                    //INT-48 //Something is broken with MVR integration (Softech)
                    tblOrderResponseData ObjRspnData = DX.tblOrderResponseDatas.Where(db => db.fkOrderDetailId == OrderDetailId).FirstOrDefault();
                    if (ObjRspnData != null)
                    {
                        //To check if selected orderDetailId releted to NCR1 then no update applied.
                        var byPassNCRUpdate = DX.tblOrderDetails.Where(o => o.pkOrderDetailId == OrderDetailId && o.fkProductApplicationId == 30).FirstOrDefault();
                        if (byPassNCRUpdate != null && (ObjRspnData.ResponseData.Trim()).Length == 0)
                        {
                            DX.UpdateOrderResponse(ObjRspnData.pkOrderResponseId, vendor_Res);
                        }
                        else if (byPassNCRUpdate == null)//This process will update other product exceept NCR1
                        {
                            DX.UpdateOrderResponse(ObjRspnData.pkOrderResponseId, vendor_Res);
                        }
                    }
                }
                else
                {
                    tblOrderResponseData_Archieve ObjRspnData = DX.tblOrderResponseData_Archieves.Where(db => db.fkOrderDetailId == OrderDetailId).FirstOrDefault();
                    if (ObjRspnData != null)
                    {
                        ObjRspnData.ResponseData = vendor_Res;
                        DX.SubmitChanges();
                    }
                }

            }
        }



        public void UpdateIsEmailNotice(int pkOrderDetailId, bool IsNCRHit, bool IsNoticeEmail)
        {
            using (EmergeDALDataContext ObjdataContext = new EmergeDALDataContext())
            {
                tblOrderDetail ObjOrderDetail = ObjdataContext.tblOrderDetails.Where(db => db.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                ObjOrderDetail.IsNCRHit = IsNCRHit;
                ObjOrderDetail.IsNoticeEmail = IsNoticeEmail;
                ObjdataContext.SubmitChanges();

            }
        }

        public int IsSendNoticeEmail(int pkOrderDetailId)
        {
            using (EmergeDALDataContext ObjdataContext = new EmergeDALDataContext())
            {
                if (pkOrderDetailId != 0)
                {
                    tblOrderDetail ObjOrderDetail = ObjdataContext.tblOrderDetails.Where(db => db.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                    if (ObjOrderDetail.IsNoticeEmail == true)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 2;
                }
            }

        }
        public int UpdateOrderStatus(int? OrderId)
        {
            int iResult = -1;
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    tblOrder ObjtblOrder = new tblOrder();
                    ObjtblOrder = DX.tblOrders.Where(d => d.pkOrderId == OrderId).FirstOrDefault();
                    if (ObjtblOrder != null)
                    {
                        ObjtblOrder.OrderStatus = 1;
                        DX.SubmitChanges();
                        iResult = 1;

                    }
                    else
                    {
                        tblOrders_Archieve ObjtblOrders_Archieve = new tblOrders_Archieve();
                        ObjtblOrders_Archieve = DX.tblOrders_Archieves.Where(d => d.pkOrderId == OrderId).FirstOrDefault();
                        if (ObjtblOrders_Archieve != null)
                        {
                            ObjtblOrders_Archieve.OrderStatus = 1;
                            DX.SubmitChanges();
                            iResult = 1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateOrderStatus(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            return iResult;
        }
        public int UpdateOrderNoteByOrderId(int OrderId, string OrderNote)
        {
            int iResult = -1;
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    tblOrder ObjtblOrder = new tblOrder();
                    ObjtblOrder = DX.tblOrders.Where(d => d.pkOrderId == OrderId).FirstOrDefault();
                    if (ObjtblOrder != null)
                    {
                        ObjtblOrder.PendingOrderUpdateNote = OrderNote;
                        DX.SubmitChanges();
                        iResult = 1;

                    }
                    else
                    {
                        tblOrders_Archieve ObjtblOrders_Archieve = new tblOrders_Archieve();
                        ObjtblOrders_Archieve = DX.tblOrders_Archieves.Where(d => d.pkOrderId == OrderId).FirstOrDefault();
                        if (ObjtblOrders_Archieve != null)
                        {
                            ObjtblOrders_Archieve.PendingOrderUpdateNote = OrderNote;
                            DX.SubmitChanges();
                            iResult = 1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateOrderNoteByOrderId(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            return iResult;
        }

        public bool UpdateRequest(int RequestId, string Request)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                if (ObjDALDataContext.UpdateRequest(RequestId, Request) == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public tblOrderResponseData GetResponseByOrderDetailId(int pkOrderDetailId)
        {
            tblOrderResponseData ObjtblOrderResponseData = new tblOrderResponseData();

            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                ObjtblOrderResponseData = DX.tblOrderResponseDatas.Where(d => d.fkOrderDetailId == pkOrderDetailId).FirstOrDefault();
            }

            return ObjtblOrderResponseData;
        }
        public tblOrder GetOrderDetailByPkorderId(int? pkOrderId)
        {
            tblOrder ObjtblOrderData = new tblOrder();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                ObjtblOrderData = DX.tblOrders.Where(d => d.pkOrderId == pkOrderId).FirstOrDefault();
            }
            return ObjtblOrderData;
        }

        public tblOrder GetDrugtestbypkid(int? pkOrderId)
        {
            tblOrder records = new tblOrder();
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                records = Dx.tblOrders.Where(rec => rec.pkOrderId == pkOrderId).FirstOrDefault();
            }
            return records;
        }
        public List<Proc_Get_DrugTestOrderDetailsResult> GetDrugTestOrderDetails(int OrderId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return DX.GetDrugTestOrderDetails(OrderId).ToList();
            }
        }
        public string TaleogenrateOrderNo(string OrderNo)
        {
            if (HttpContext.Current.Session["loginfrom"] != null)
            {
                // string ouptut = string.Empty;
                string TaleoStatus = string.Empty;
                if (Convert.ToString(HttpContext.Current.Session["loginfrom"]) == "taleo")
                {
                    string xmlResponseCookie = Convert.ToString(HttpContext.Current.Session["xmlResponseCookie"]);


                    string Candidateurl = Convert.ToString(System.Web.HttpContext.Current.Session["TaleoDispatcherUrlHit"]) + "object/candidate/" + Convert.ToInt32(HttpContext.Current.Session["TaleoCandidateId"]);

                    HttpWebRequest req1 = (HttpWebRequest)WebRequest.Create(Candidateurl);

                    req1.Method = "put";
                    req1.ContentType = "application/json";
                    req1.Headers.Add("Cookie: authToken=" + xmlResponseCookie.ToString());

                    //Make API call

                    try
                    {
                        using (var streamWriter = new StreamWriter(req1.GetRequestStream()))
                        {

                            string json = string.Empty;


                            using (EmergeDALDataContext ObjDX = new EmergeDALDataContext())
                            {
                                try
                                {
                                    var Obj = (from t1 in ObjDX.tblOrders where t1.OrderNo == OrderNo select new { t1 }).First();
                                    if (Obj != null)
                                    {
                                        switch (Obj.t1.OrderStatus)
                                        {

                                            case 1:
                                                TaleoStatus = "Pending";
                                                break;
                                            case 2:
                                                TaleoStatus = "Complete";
                                                break;
                                            case 3:
                                                TaleoStatus = "In Complete";
                                                break;
                                            case 4:
                                                TaleoStatus = "In Review";
                                                break;
                                            default:
                                                TaleoStatus = "";
                                                break;


                                        }

                                    }

                                }
                                catch (Exception ex)
                                {
                                    //Added log for exception handling.
                                    VendorServices.GeneralService.WriteLog("Some Error Occured in TaleogenrateOrderNo(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                                }
                            }

                            if (Convert.ToString(HttpContext.Current.Session["Emerge_Order_Id"]) == "")
                            {
                                json = "{\"candidate\":{\"Emerge_Order_Id\":\"" + OrderNo + "\",\"Emerge_Report_Status\":\"" + TaleoStatus + "\" }}";
                            }
                            else
                            {
                                json = "{\"candidate\":{\"Emerge_Order_Id\":\"" + OrderNo + "\",\"Emerge_Report_Status\":\"" + TaleoStatus + "\" }}";
                            }
                            streamWriter.Write(json);
                        }
                        var httpResponse = (HttpWebResponse)req1.GetResponse();
                        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                        {
                            //var responseText = 
                            streamReader.ReadToEnd();


                        }
                        httpResponse.Close();

                    }
                    catch (Exception ex)
                    {
                        //Added log for exception handling.
                        VendorServices.GeneralService.WriteLog("Some Error Occured in TaleogenrateOrderNo(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    }



                    finally
                    {

                        string SuccessResponse = taleologOut();
                        if (SuccessResponse == "sucess")
                        {

                        }
                        else
                        {

                        }


                    }
                }
            }

            return "success";
        }
        public static string taleologOut()
        {


            string xmlResponseCookie = Convert.ToString(System.Web.HttpContext.Current.Session["xmlResponseCookie"]);
            //string logoutURL = "https://chk.tbe.taleo.net/chk02/ats/api/logout";

            string str = Convert.ToString(System.Web.HttpContext.Current.Session["TaleoDispatcherUrlHit"]);
            string logoutstring = str.Substring(0, str.Length - 3);
            string logoutURL = Convert.ToString(logoutstring) + "logout";

            HttpWebRequest req2 = (HttpWebRequest)WebRequest.Create(logoutURL);
            req2.Method = "post";


            req2.ContentType = "application/json";
            req2.Headers.Add("Cookie: authToken=" + xmlResponseCookie);

            //req.Headers.Add("Cookie", authToken);

            //Make API call
            try
            {
                HttpWebResponse response = (HttpWebResponse)req2.GetResponse();

                //string pageContent = new StreamReader(response.GetResponseStream()).ReadToEnd().ToString();

                //string xmlResponse2 = null;
                using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                {
                    //xmlResponse2 = 
                    sr.ReadToEnd();
                }

                response.Close();
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in taleologOut(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                //return pageContent;
            }

            return "sucess";
        }





        //START INT-371

        public int AddOrderWiseFolderStatus(List<MarkAs> lstMarkAs, int fkcompanyid)
        {
            int iResult = -1;
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    foreach (MarkAs ObjMark in lstMarkAs)
                    {
                        tblOrderWiseFolderStatus ObjtbltblOrderWiseFolderStatus = new tblOrderWiseFolderStatus();
                        ObjtbltblOrderWiseFolderStatus = DX.tblOrderWiseFolderStatus.Where(d => d.fkOrderId == ObjMark.OrderId).FirstOrDefault();
                        if (ObjtbltblOrderWiseFolderStatus != null)
                        {
                            ObjtbltblOrderWiseFolderStatus.fkFolderId = ObjMark.FolderId;
                            DX.SubmitChanges();
                        }
                        else if (ObjtbltblOrderWiseFolderStatus == null)
                        {
                            tblOrderWiseFolderStatus ObjtbltblOrderWiseFolderStatus1 = new tblOrderWiseFolderStatus();
                            ObjtbltblOrderWiseFolderStatus1.fkCompanyId = fkcompanyid;
                            ObjtbltblOrderWiseFolderStatus1.fkFolderId = ObjMark.FolderId;
                            ObjtbltblOrderWiseFolderStatus1.fkOrderId = ObjMark.OrderId;
                            DX.tblOrderWiseFolderStatus.InsertOnSubmit(ObjtbltblOrderWiseFolderStatus1);
                            DX.SubmitChanges();
                            iResult = 1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in MarkReadUnRead(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            finally
            {
            }
            return iResult;
        }

        //END INT-371



        public int MarkReadUnRead(List<MarkAs> lstMarkAs)
        {
            int iResult = -1;
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    foreach (MarkAs ObjMark in lstMarkAs)
                    {
                        tblOrder ObjtblOrder = new tblOrder();
                        ObjtblOrder = DX.tblOrders.Where(d => d.pkOrderId == ObjMark.OrderId).FirstOrDefault();
                        if (ObjtblOrder != null)
                        {
                            ObjtblOrder.IsViewed = ObjMark.IsRead;
                            DX.SubmitChanges();
                            iResult = 1;
                        }
                        else
                        {
                            tblOrders_Archieve ObjtblOrders_Archieve = new tblOrders_Archieve();
                            ObjtblOrders_Archieve = DX.tblOrders_Archieves.Where(d => d.pkOrderId == ObjMark.OrderId).FirstOrDefault();
                            if (ObjtblOrders_Archieve != null)
                            {
                                ObjtblOrders_Archieve.IsViewed = ObjMark.IsRead;
                                DX.SubmitChanges();
                                iResult = 1;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in MarkReadUnRead(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            finally
            {
            }
            return iResult;
        }














        public int DeleteOrders(List<tblOrder> objTblOrdsrs)
        {
            int iResult = -1;

            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                try
                {
                    foreach (tblOrder objTblOrderIndex in objTblOrdsrs)
                    {
                        tblOrder ObjtblOrder = new tblOrder();
                        ObjtblOrder = DX.tblOrders.Where(d => d.pkOrderId == objTblOrderIndex.pkOrderId).FirstOrDefault();

                        if (ObjtblOrder != null)
                        {
                            ObjtblOrder.IsDeleted = true;
                            ObjtblOrder.DeletedBy = objTblOrderIndex.DeletedBy;
                            ObjtblOrder.DeletedDate = objTblOrderIndex.DeletedDate;
                            DX.SubmitChanges();
                            iResult = 1;
                        }
                    }

                }
                catch (Exception ex)
                {
                    //Added log for exception handling.
                    VendorServices.GeneralService.WriteLog("Some Error Occured in DeleteOrders(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                }
                finally
                {

                }
            }
            return iResult;
        }

        public List<proc_GetConsentInforByOrderIdResult> GetConsentPendingFormsByOrderId(int PKOrderId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.proc_GetConsentInforByOrderId(PKOrderId).ToList<proc_GetConsentInforByOrderIdResult>();
            }
        }
        public List<Proc_GetAllProductsWithVendorResult> GetAllProductsWithVendor()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetAllProductsWithVendor().ToList();
            }
        }

        private bool IsInstantProduct(string p)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                var Result = ObjDALDataContext.tblProducts.Where(data => data.ProductCode == p && data.IsInstantReport == true).ToList();
                if (Result.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


        public EmergeReport AddOrders(tblOrder ObjtblOrder, List<string> ObjCollProducts, tblOrderSearchedData ObjtblOrderSearchedData,
                             List<tblOrderSearchedEmploymentInfo> ObjCollOrderSearchedEmploymentInfo,
                             List<tblOrderSearchedEducationInfo> ObjCollOrderSearchedEducationInfo,
                             List<tblOrderSearchedPersonalInfo> ObjCollOrderSearchedPersonalInfo,
                             List<tblOrderSearchedProfessionalInfo> ObjCollOrderSearchedProfessionalInfo,
                             List<tblOrderPersonalQtnInfo> ObjCollOrderPersonalQtnInfo,
                             List<tblOrderSearchedLicenseInfo> ObjCollOrderLicenseInfo,
                             List<tblOrderSearchedAliasPersonalInfo> ObjCollOrderSearchedAliasPersonalInfo,
                             List<tblOrderSearchedAliasLocationInfo> ObjCollOrderSearchedAliasLocationInfo,
                             List<tblOrderSearchedAliasWorkInfo> ObjCollOrderSearchedAliasWorkInfo,
                             Dictionary<string, Guid> ObjCompnyProductInfo,
                             List<tblOrderSearchedLiveRunnerInfo> ObjColltblOrderSearchedLiveRunnerInfo,
                             List<CLSConsentInfo> ObjCollCLSConsentInfo,
                             Guid ApplicationId, int IsNCR1NullDOB, bool IsLiveRunnerState, tblTieredPackage ObjtblTieredPackage, OrderInfo ObjOrderInfo, string CompanyAccountNo, string OrderCompanyName, string PermissiblePurpose, List<tblOrderSearchedDataJurdictionInfo> ObjJurisdictionList)        /*, int AuthorizedPurpose*/
        {


            int IsNullDOB = IsNCR1NullDOB;
            #region Variable Assignment
            OrderCompanyName = OrderCompanyName.Replace("?", "").Replace(".", "").Replace("&", "").Replace("%", "").Replace("#", "").Replace("'", "");
            int? OrderId = 0;
            //DbTransaction DbTrans = null;

            bool IsPackageReport = false;
            string PackageInclude = string.Empty;
            int fkPackageId = 0;
            bool Is7YearFeaturesEnable = false;
            #endregion

            #region Return Variable
            EmergeReport ObjEmergeReport = new EmergeReport();
            ObjEmergeReport.IsLiveRunnerState = IsLiveRunnerState;
            ObjEmergeReport.IsNullDOB = IsNCR1NullDOB;
            ObjEmergeReport.ObjCollCLSConsentInfo = ObjCollCLSConsentInfo;
            ObjEmergeReport.ObjCollOrderSearchedEducationInfo = ObjCollOrderSearchedEducationInfo;
            ObjEmergeReport.ObjCollOrderSearchedEmploymentInfo = ObjCollOrderSearchedEmploymentInfo;
            ObjEmergeReport.ObjCollOrderSearchedLicenseInfo = ObjCollOrderLicenseInfo;
            ObjEmergeReport.ObjCollOrderSearchedPersonalInfo = ObjCollOrderSearchedPersonalInfo;
            ObjEmergeReport.ObjCollOrderSearchedProfessionalInfo = ObjCollOrderSearchedProfessionalInfo;
            ObjEmergeReport.ObjCollProducts = ObjCollProducts;
            ObjEmergeReport.objColltblOrderPersonalQtnInfo = ObjCollOrderPersonalQtnInfo;
            ObjEmergeReport.ObjColltblOrderSearchedLiveRunnerInfo = ObjColltblOrderSearchedLiveRunnerInfo;
            ObjEmergeReport.ObjOrderInfo = ObjOrderInfo;
            ObjEmergeReport.ObjtblOrder = ObjtblOrder;
            ObjEmergeReport.ObjtblOrderSearchedData = ObjtblOrderSearchedData;
            ObjEmergeReport.ObjTieredPackage = ObjtblTieredPackage;
            ObjEmergeReport.CompanyAccountNo = CompanyAccountNo;
            ObjEmergeReport.OrderCompanyName = OrderCompanyName;
            ObjEmergeReport.PermissiblePurpose = PermissiblePurpose;
            Is7YearFeaturesEnable = ObjOrderInfo.Is7YearFilterApplyForCriminal;
            #endregion
            #region  Check is current process Duplicate Or not
            if (IsDuplicateOrder(ObjtblOrder.OrderNo.Trim()))
            {
                ObjEmergeReport.ObjOrderInfo.OrderId = 0;
                return ObjEmergeReport;
            }

            #endregion
            bool IsTieredOrder = false;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                List<Proc_GetAllProductsWithVendorResult> ObjProc_GetAllProductsWithVendorResult;

                try
                {
                    #region Insert in Order Table
                    /* Here data insert in main order table and after successfully insertion orderid will be returned  */
                    string ReportIncludes = string.Empty;
                    int Packageid = 0;
                    foreach (string ProductCodes in ObjCollProducts)
                    {
                        if (ProductCodes.Contains("_"))
                        {
                            string[] Pcode = ProductCodes.Split('_');
                            int.TryParse(Pcode[1], out Packageid);

                            if (Packageid != 0)
                            {
                                tblProductPackage ObjtblProductPackages = new tblProductPackage();
                                ObjtblProductPackages = ObjDALDataContext.tblProductPackages.Where(d => d.pkPackageId == Packageid).FirstOrDefault();
                                if (ObjtblProductPackages != null)
                                {
                                    if (!ReportIncludes.Contains(ObjtblProductPackages.PackageCode.ToString().ToUpper()))
                                    {
                                        ReportIncludes += ObjtblProductPackages.PackageCode.ToString().ToUpper() + ", ";
                                    }
                                }
                            }
                            else
                            {
                                ReportIncludes += Pcode[0].ToString() + ", ";
                            }
                        }
                    }
                    ReportIncludes = ReportIncludes.Substring(0, ReportIncludes.Length - 2);
                    #region Select Non Tiered Reports
                    if (ObjOrderInfo != null && ObjtblTieredPackage != null)
                    {
                        if (ObjOrderInfo.IsTiered == true && ObjOrderInfo.CheckTieredForApi == true)//This functionality used by Landing page tiered package.
                        {
                            //It should be executed when any order which is submitted by the user from the landing page Aug 18, 2015 INT-248 and INT-252 
                            IsTieredOrder = true;
                            List<tblProductPerApplication> ObjtblPPA = new List<tblProductPerApplication>();
                            using (EmergeDALDataContext dx = new EmergeDALDataContext())
                            {
                                if (ObjtblTieredPackage.Host != null)//Set Host report to returned Object
                                {
                                    ObjOrderInfo.HostProductCode = dx.tblProductPerApplications.Where(d => d.pkProductApplicationId == ObjtblTieredPackage.Host).Select(d => d.ProductCode).FirstOrDefault();
                                }

                                if (ObjtblTieredPackage.Tired2 != null)//Set Tiered 2 and removed from current running reports.
                                {
                                    ObjtblPPA = dx.tblProductPerApplications.Where(d => d.pkProductApplicationId == ObjtblTieredPackage.Tired2).ToList<tblProductPerApplication>();
                                    if (ObjtblPPA.Count() > 0)
                                    {
                                        string sRemovedElement = "";
                                        foreach (string sReport in ObjCollProducts)
                                        {
                                            if (sReport.Split('_').First().ToLower() == ObjtblPPA.First().ProductCode.ToLower())
                                            {
                                                ObjOrderInfo.Tiered2ProductCode = sReport;
                                                sRemovedElement = sReport;
                                            }
                                        }

                                    }

                                }
                                if (ObjtblTieredPackage.Tired3 != null)//Set Tiered 3 and removed from current running reports.
                                {
                                    ObjtblPPA = dx.tblProductPerApplications.Where(d => d.pkProductApplicationId == ObjtblTieredPackage.Tired3).ToList<tblProductPerApplication>();
                                    if (ObjtblPPA.Count() > 0)
                                    {
                                        string sRemovedElement = "";
                                        foreach (string sReport in ObjCollProducts)
                                        {
                                            if (sReport.Split('_').First().ToLower() == ObjtblPPA.First().ProductCode.ToLower())
                                            {
                                                ObjOrderInfo.Tiered3ProductCode = sReport;
                                                sRemovedElement = sReport;
                                            }
                                        }

                                    }

                                }
                                if (ObjtblTieredPackage.TiredOptional != null)//Set Optional Tiered and removed from current running reports.
                                {
                                    ObjtblPPA = dx.tblProductPerApplications.Where(d => d.pkProductApplicationId == ObjtblTieredPackage.TiredOptional).ToList<tblProductPerApplication>();
                                    if (ObjtblPPA.Count() > 0)
                                    {
                                        string sRemovedElement = "";
                                        foreach (string sReport in ObjCollProducts)
                                        {
                                            if (sReport.Split('_').First().ToLower() == ObjtblPPA.First().ProductCode.ToLower())
                                            {
                                                ObjOrderInfo.OptionalProductCode = sReport;
                                                sRemovedElement = sReport;
                                            }
                                        }

                                    }

                                }

                            }
                        }
                        else if (ObjOrderInfo.IsTiered == true && ObjOrderInfo.CheckTieredForApi == false)//This functionality used by Normal Tiered package.
                        {
                            //It should be executed when any order which is submitted by the user from the normal new report page.
                            IsTieredOrder = true;
                            List<tblProductPerApplication> ObjtblPPA = new List<tblProductPerApplication>();
                            using (EmergeDALDataContext dx = new EmergeDALDataContext())
                            {
                                if (ObjtblTieredPackage.Host != null)//Set Host report to returned Object
                                {
                                    ObjOrderInfo.HostProductCode = dx.tblProductPerApplications.Where(d => d.pkProductApplicationId == ObjtblTieredPackage.Host).Select(d => d.ProductCode).FirstOrDefault();
                                }

                                if (ObjtblTieredPackage.Tired2 != null)//Set Tiered 2 and removed from current running reports.
                                {
                                    ObjtblPPA = dx.tblProductPerApplications.Where(d => d.pkProductApplicationId == ObjtblTieredPackage.Tired2).ToList<tblProductPerApplication>();
                                    if (ObjtblPPA.Count() > 0)
                                    {
                                        string sRemovedElement = "";
                                        foreach (string sReport in ObjCollProducts)
                                        {
                                            if (sReport.Split('_').First().ToLower() == ObjtblPPA.First().ProductCode.ToLower())
                                            {
                                                ObjOrderInfo.Tiered2ProductCode = sReport;
                                                sRemovedElement = sReport;
                                            }
                                        }
                                        ObjCollProducts.Remove(sRemovedElement);
                                    }
                                    if (ObjOrderInfo.Tiered2ProductCode == null)
                                    {
                                        ObjOrderInfo.Tiered2ProductCode = dx.tblProductPerApplications.Where(d => d.pkProductApplicationId == ObjEmergeReport.ObjTieredPackage.Tired2).Select(d => d.ProductCode).FirstOrDefault() + "_" + ObjEmergeReport.ObjTieredPackage.fkPackageId;
                                    }
                                }


                                if (ObjtblTieredPackage.Tired3 != null)//Set Tiered 3 and removed from current running reports.
                                {
                                    ObjtblPPA = dx.tblProductPerApplications.Where(d => d.pkProductApplicationId == ObjtblTieredPackage.Tired3).ToList<tblProductPerApplication>();
                                    if (ObjtblPPA.Count() > 0)
                                    {
                                        string sRemovedElement = "";
                                        foreach (string sReport in ObjCollProducts)
                                        {
                                            if (sReport.Split('_').First().ToLower() == ObjtblPPA.First().ProductCode.ToLower())
                                            {
                                                ObjOrderInfo.Tiered3ProductCode = sReport;
                                                sRemovedElement = sReport;
                                            }
                                        }
                                        ObjCollProducts.Remove(sRemovedElement);
                                    }

                                }
                                if (ObjtblTieredPackage.TiredOptional != null)//Set Optional Tiered and removed from current running reports.
                                {
                                    ObjtblPPA = dx.tblProductPerApplications.Where(d => d.pkProductApplicationId == ObjtblTieredPackage.TiredOptional).ToList<tblProductPerApplication>();
                                    if (ObjtblPPA.Count() > 0)
                                    {
                                        string sRemovedElement = "";
                                        foreach (string sReport in ObjCollProducts)
                                        {
                                            if (sReport.Split('_').First().ToLower() == ObjtblPPA.First().ProductCode.ToLower())
                                            {
                                                ObjOrderInfo.OptionalProductCode = sReport;
                                                sRemovedElement = sReport;
                                            }
                                        }
                                        ObjCollProducts.Remove(sRemovedElement);
                                    }
                                    if (ObjOrderInfo.OptionalProductCode == null)
                                    {
                                        ObjOrderInfo.OptionalProductCode = dx.tblProductPerApplications.Where(d => d.pkProductApplicationId == ObjEmergeReport.ObjTieredPackage.TiredOptional).Select(d => d.ProductCode).FirstOrDefault() + "_" + ObjEmergeReport.ObjTieredPackage.fkPackageId;
                                    }
                                }

                            }
                        }
                        if (ObjEmergeReport.ObjOrderInfo.NormalReport == null)
                            ObjEmergeReport.ObjOrderInfo.NormalReport = new List<string>();
                        foreach (string ReportCode in ObjCollProducts)
                        {
                            ObjEmergeReport.ObjOrderInfo.NormalReport.Add(ReportCode);
                        }
                    }

                    #endregion

                    int? LocationId = ObjtblOrder.fkLocationId;
                    DateTime OrderedDate = ObjtblOrder.OrderDt;
                    if (ObjtblOrder.fkBatchId == null)
                        ObjtblOrder.fkBatchId = 0;

                    List<Proc_AddEmergeOrdersResult> ObjAddOrderResult = ObjDALDataContext.AddEmergeOrders(LocationId,
                                                                                                    ObjtblOrder.fkCompanyUserId,
                                                                                                    ObjtblOrder.OrderNo,
                                                                                                    OrderedDate,
                                                                                                    ObjtblOrder.OrderStatus,
                                                                                                    ReportIncludes,
                                                                                                    ObjtblOrder.CreatedDate,
                                                                                                    ObjtblOrder.CreatedById,
                                                                                                    false, ObjtblOrder.fkBatchId, (ObjtblOrder.IsSyncpod == null) ? false : ObjtblOrder.IsSyncpod, (ObjtblOrder.SyncpodImage == null) ? "" : ObjtblOrder.SyncpodImage,
                                                                                                     (ObjtblOrder.FaceImage == null) ? "" : ObjtblOrder.FaceImage, ObjtblOrder.OrderType, ObjtblOrder.ReferenceCode ?? string.Empty, ObjtblOrder.ParentOrderId, false, ObjtblOrder.SearchPersonHTML, IsTieredOrder).ToList();

                    if (ObjAddOrderResult.First().ReturnType > 0) /* If insertinon is successfully done then returntype must be greater than zero  */
                    {
                        OrderId = ObjAddOrderResult.First().OrderId;
                        ObjEmergeReport.ObjOrderInfo.OrderId = OrderId;
                        #region taleo work
                        if (HttpContext.Current.Session["loginfrom"] != null)
                        {
                            // string ouptut = string.Empty;
                            if (Convert.ToString(HttpContext.Current.Session["loginfrom"]) == "taleo")
                            {

                                TaleogenrateOrderNo(Convert.ToString(ObjtblOrder.OrderNo));
                            }
                        }
                        #endregion



                    }
                    if (OrderId == 0) /* If insertion in order table is not successfull then data is rollback */
                    {
                        ObjEmergeReport.ObjOrderInfo.OrderId = OrderId;
                        return ObjEmergeReport; /* An empty orderid wlll be returned */
                    }
                    #endregion

                    #region Insert in Order Search Table
                    /* In this following code all searched data inserted */
                    // 1 is saved as a county Id temporary
                    int iOrderSearchedData = ObjDALDataContext.AddOrderSearchedData(OrderId,
                                                                                    ObjtblOrderSearchedData.SearchedLastName,
                                                                                    ObjtblOrderSearchedData.SearchedFirstName,
                                                                                    ObjtblOrderSearchedData.SearchedMiddleInitial,
                                                                                    ObjtblOrderSearchedData.SearchedSuffix,
                                                                                    ObjtblOrderSearchedData.SearchedSsn == "___-__-____" ? "" : ObjtblOrderSearchedData.SearchedSsn,
                                                                                    ObjtblOrderSearchedData.SearchedDob,
                                                                                    ObjtblOrderSearchedData.SearchedPhoneNumber == "(___) ___-____" ? "" : ObjtblOrderSearchedData.SearchedPhoneNumber,
                                                                                    ObjtblOrderSearchedData.SearchedStreetAddress,
                                                                                    ObjtblOrderSearchedData.SearchedDirection,
                                                                                    ObjtblOrderSearchedData.SearchedStreetName,
                                                                                    ObjtblOrderSearchedData.SearchedStreetType,
                                                                                    ObjtblOrderSearchedData.SearchedApt,
                                                                                    ObjtblOrderSearchedData.SearchedCity,
                                                                                    ObjtblOrderSearchedData.SearchedStateId,
                                                                                    ObjtblOrderSearchedData.SearchedZipCode,
                                                                                    ObjtblOrderSearchedData.SearchedCountyId,
                                                                                    ObjtblOrderSearchedData.SearchedDriverLicense,
                                                                                    ObjtblOrderSearchedData.SearchedVehicleVin,
                                                                                    ObjtblOrderSearchedData.SearchedDLStateId,
                                                                                    ObjtblOrderSearchedData.SearchedSex,
                                                                                    ObjtblOrderSearchedData.SearchedBusinessName,
                                                                                    ObjtblOrderSearchedData.SearchedBusinessCity,
                                                                                    ObjtblOrderSearchedData.SearchedBusinessStateId,
                                                                                    ObjtblOrderSearchedData.SearchedTrackingRef,
                                                                                    ObjtblOrderSearchedData.SearchedTrackingRefId,
                                                                                    ObjtblOrderSearchedData.SearchedTrackingNotes,
                                                                                    ObjtblOrderSearchedData.SearchedApplicantEmail,
                                                                                    ObjtblOrderSearchedData.SearchedPermissibleId//ND-5 
                                                                                    );

                    if (iOrderSearchedData < 0)
                    {
                        OrderId = 0;
                        ObjEmergeReport.ObjOrderInfo.OrderId = OrderId;
                        return ObjEmergeReport;
                    }
                    #endregion

                    #region Insert in Searched Employment Table
                    if (ObjCollOrderSearchedEmploymentInfo.Count > 0) /* This is for employment search  */
                    {
                        foreach (tblOrderSearchedEmploymentInfo ObjtblOrderSearchedEmploymentInfo in ObjCollOrderSearchedEmploymentInfo)
                        {
                            int iOrderSearchedEmploymentInfo = ObjDALDataContext.AddOrderSearchedEmploymentInfo(OrderId,
                                                                                                                ObjtblOrderSearchedEmploymentInfo.SearchedContact,
                                                                                                                ObjtblOrderSearchedEmploymentInfo.SearchedName,
                                                                                                                ObjtblOrderSearchedEmploymentInfo.SearchedPhone,
                                                                                                                ObjtblOrderSearchedEmploymentInfo.SearchedStarted,
                                                                                                                ObjtblOrderSearchedEmploymentInfo.SearchedTitleStart,
                                                                                                                 ObjtblOrderSearchedEmploymentInfo.SearchedTitleEnd,
                                                                                                                ObjtblOrderSearchedEmploymentInfo.SearchedCity,
                                                                                                                ObjtblOrderSearchedEmploymentInfo.SearchedState,
                                                                                                                ObjtblOrderSearchedEmploymentInfo.SearchedAlias,
                                                                                                                ObjtblOrderSearchedEmploymentInfo.SearchedEnded,
                                                                                                                ObjtblOrderSearchedEmploymentInfo.SearchedReason,
                                                                                                                ObjtblOrderSearchedEmploymentInfo.SearchedEmail,
                                                                                                                ObjtblOrderSearchedEmploymentInfo.SearchedSupervisorContact);

                            if (iOrderSearchedEmploymentInfo < 0)
                            {
                                OrderId = 0;
                                ObjEmergeReport.ObjOrderInfo.OrderId = OrderId;
                                return ObjEmergeReport;
                            }
                        }
                    }

                    #endregion

                    #region Insert in Searched Education Table

                    if (ObjCollOrderSearchedEducationInfo.Count > 0) /* This is for educational search  */
                    {
                        foreach (tblOrderSearchedEducationInfo ObjtblOrderSearchedEducationInfo in ObjCollOrderSearchedEducationInfo)
                        {
                            int iOrderSearchedEducationInfo = ObjDALDataContext.AddOrderSearchedEducationInfo(OrderId,
                                                                                                              ObjtblOrderSearchedEducationInfo.SearchedSchoolName,
                                                                                                              ObjtblOrderSearchedEducationInfo.SearchedCity,
                                                                                                              ObjtblOrderSearchedEducationInfo.SearchedPhone,
                                                                                                              ObjtblOrderSearchedEducationInfo.SearchedStarted,
                                                                                                              ObjtblOrderSearchedEducationInfo.SearchedDegree,
                                                                                                              ObjtblOrderSearchedEducationInfo.SearchedAlias,
                                                                                                              ObjtblOrderSearchedEducationInfo.SearchedState,
                                                                                                              ObjtblOrderSearchedEducationInfo.SearchedGraduationDt,
                                                                                                              ObjtblOrderSearchedEducationInfo.SearchedEnded,
                                                                                                              ObjtblOrderSearchedEducationInfo.SearchedMajor,
                                                                                                              ObjtblOrderSearchedEducationInfo.SearchedFax,
                                                                                                              ObjtblOrderSearchedEducationInfo.SearchedGrade,
                                                                                                              ObjtblOrderSearchedEducationInfo.SearchedCredit,
                                                                                                              ObjtblOrderSearchedEducationInfo.SearchedEmail);


                            if (iOrderSearchedEducationInfo < 0)
                            {
                                OrderId = 0;
                                ObjEmergeReport.ObjOrderInfo.OrderId = OrderId;
                                return ObjEmergeReport;
                            }
                        }
                    }

                    #endregion

                    #region Insert in Searched Personal Table

                    if (ObjCollOrderSearchedPersonalInfo.Count > 0)
                    {
                        foreach (tblOrderSearchedPersonalInfo ObjtblOrderSearchedPersonalInfo in ObjCollOrderSearchedPersonalInfo)
                        {
                            int iOrderSearchedPersonalInfo = ObjDALDataContext.AddOrderSearchedPersonalInfo(ObjtblOrderSearchedPersonalInfo.pkOrderSearchedPersnlId,
                                                                                                           OrderId,
                                                                                                           ObjtblOrderSearchedPersonalInfo.SearchedName,
                                                                                                           ObjtblOrderSearchedPersonalInfo.SearchedCity,
                                                                                                           ObjtblOrderSearchedPersonalInfo.SearchedState,
                                                                                                           ObjtblOrderSearchedPersonalInfo.SearchedPhone,
                                                                                                           ObjtblOrderSearchedPersonalInfo.SearchedAltPhone,
                                                                                                           ObjtblOrderSearchedPersonalInfo.SearchedEmail,
                                                                                                           ObjtblOrderSearchedPersonalInfo.SearchedRelation,
                                                                                                           ObjtblOrderSearchedPersonalInfo.SearchedTitle,
                                                                                                           ObjtblOrderSearchedPersonalInfo.SearchedSalary,
                                                                                                           ObjtblOrderSearchedPersonalInfo.SearchedAlias);
                            if (iOrderSearchedPersonalInfo < 0)
                            {
                                OrderId = 0;
                                ObjEmergeReport.ObjOrderInfo.OrderId = OrderId;
                                return ObjEmergeReport;
                            }
                        }
                    }
                    #endregion

                    #region Insert in Searched Professional Table //INT-336

                    if (ObjCollOrderSearchedProfessionalInfo.Count > 0)
                    {
                        foreach (tblOrderSearchedProfessionalInfo ObjtblOrderSearchedProfessionalInfo in ObjCollOrderSearchedProfessionalInfo)
                        {
                            int iOrderSearchedProfessionalInfo = ObjDALDataContext.AddOrderSearchedProfessionalInfo(ObjtblOrderSearchedProfessionalInfo.pkOrderSearchedProfessnalId,
                                                                                                           OrderId,
                                                                                                           ObjtblOrderSearchedProfessionalInfo.SearchedName,
                                                                                                           ObjtblOrderSearchedProfessionalInfo.SearchedCity,
                                                                                                           ObjtblOrderSearchedProfessionalInfo.SearchedState,
                                                                                                           ObjtblOrderSearchedProfessionalInfo.SearchedPhone,
                                                                                                           ObjtblOrderSearchedProfessionalInfo.SearchedAltPhone,
                                                                                                           ObjtblOrderSearchedProfessionalInfo.SearchedEmail,
                                                                                                           ObjtblOrderSearchedProfessionalInfo.SearchedRelation,
                                                                                                           ObjtblOrderSearchedProfessionalInfo.SearchedTitle,
                                                                                                           ObjtblOrderSearchedProfessionalInfo.SearchedSalary,
                                                                                                           ObjtblOrderSearchedProfessionalInfo.SearchedAlias);
                            if (iOrderSearchedProfessionalInfo < 0)
                            {
                                OrderId = 0;
                                ObjEmergeReport.ObjOrderInfo.OrderId = OrderId;
                                return ObjEmergeReport;
                            }
                        }
                    }
                    #endregion




                    #region Insert in Searched License Table

                    if (ObjCollOrderLicenseInfo.Count > 0)
                    {
                        foreach (tblOrderSearchedLicenseInfo objtblOrderSearchedLicenseInfo in ObjCollOrderLicenseInfo)
                        {
                            int iOrderSearchedLicenseInfo = ObjDALDataContext.AddOrderSearchedLicenseInfo(
                                                                                                        OrderId,
                                                                                                        objtblOrderSearchedLicenseInfo.SearchedLicenseType,
                                                                                                        objtblOrderSearchedLicenseInfo.SearchedLicenseName,
                                                                                                        objtblOrderSearchedLicenseInfo.SearchedIssuingState,
                                                                                                        objtblOrderSearchedLicenseInfo.SearchedAuthority,
                                                                                                        objtblOrderSearchedLicenseInfo.SearchedPhone,
                                                                                                        objtblOrderSearchedLicenseInfo.SearchedLicenseNo,
                                                                                                        objtblOrderSearchedLicenseInfo.SearchedIssueDate,
                                                                                                        objtblOrderSearchedLicenseInfo.SearchedExpiryDate,
                                                                                                        objtblOrderSearchedLicenseInfo.SearchedStatus, objtblOrderSearchedLicenseInfo.SearchedAlias);
                            if (iOrderSearchedLicenseInfo < 0)
                            {
                                OrderId = 0;
                                ObjEmergeReport.ObjOrderInfo.OrderId = OrderId;
                                return ObjEmergeReport;
                            }
                        }
                    }
                    #endregion

                    #region Insert in Order Detail Table

                    string Vendor_Request = string.Empty;
                    //string Vendor_Response = string.Empty;
                    /*Code To Fetch Whole Vendor and State At one Db call.*/
                    List<tblVendor> ObjtblVendor_List = ObjDALDataContext.tblVendors.ToList();
                    List<tblState> ObjtblState_List = ObjDALDataContext.tblStates.ToList();
                    //List<tblCounty> ObjtblCounty_List = ObjDALDataContext.tblCounties.ToList();
                    BALGeneral ObjBALGeneral = new BALGeneral();
                    ObjProc_GetAllProductsWithVendorResult = GetAllProductsWithVendor();
                    string SearchedStateCode = (!string.IsNullOrEmpty(ObjtblOrderSearchedData.SearchedStateId.ToString()) ? ObjtblState_List.Where(data => data.pkStateId == ObjtblOrderSearchedData.SearchedStateId).First().StateCode : "");
                    string SearchedDlStateCode = (!string.IsNullOrEmpty(ObjtblOrderSearchedData.SearchedDLStateId.ToString()) ? ObjtblState_List.Where(data => data.pkStateId == ObjtblOrderSearchedData.SearchedDLStateId).First().StateCode : "");
                    string SearchedCountyName = "";
                    string SearchedCountyState = "";
                    int SearchedCountyId = 0;
                    Dictionary<int?, string> objdic = new Dictionary<int?, string>();

                    /*
                    * This is barry : I have change all the productcode with PName variable,
                    * now PName contains the values of current product code. And i replace all 
                    * "ObjCollProducts.ElementAt(iProduct)" & "ObjCollProducts[iProduct]" with PName
                    */
                    List<string> reportCodes = new List<string>();
                    //List<string> PckIncColl = new List<string>();
                    string PckInc = string.Empty;
                    bool IsIfAnyConsentEmailOrFax = false;
                    string PDFAttachFilePath = string.Empty;
                    bool isVerificationReport = false;
                    for (int iProduct = 0; iProduct < ObjCollProducts.Count; iProduct++) /* This is for searched product  */
                    {
                        string PName = string.Empty;
                        int fkProductApplicationId = 0;
                        if (ObjCollProducts.ElementAt(iProduct).Contains("_"))/*This condition is used to split report code & package code*/
                        {
                            string[] Pcode = ObjCollProducts.ElementAt(iProduct).Split('_');
                            PName = Pcode[0].ToString();
                            reportCodes.Add(Pcode[0].ToString());
                            if (Pcode[1] != "0")
                            {
                                IsPackageReport = true;
                                fkPackageId = Convert.ToInt32(Pcode[1].ToString());
                                PckInc = string.Join(",", ObjCollProducts.Where(d => d.Contains(Pcode[1].ToString())).Select(d => d.ToString()).ToArray());
                                PckInc = PckInc.Replace("_" + Pcode[1].ToString(), "");
                                PackageInclude = PckInc;

                                #region Add Order Package amount in new table
                                //int PackageResult =
                                ObjDALDataContext.AddOrderPackages(OrderId, fkPackageId, LocationId);
                                #endregion
                            }
                            else
                            {
                                IsPackageReport = !true;
                                fkPackageId = 0;
                                PackageInclude = string.Empty;
                            }
                        }
                        using (EmergeDALDataContext dx = new EmergeDALDataContext())
                        {
                            fkProductApplicationId = dx.tblProductPerApplications.Where(d => d.ProductCode.ToLower() == PName).Select(d => d.pkProductApplicationId).FirstOrDefault();
                        }
                        int? OrderDetailId = 0;
                        bool IsLiveRunner = false;
                        bool IsInstant = false;
                        bool IsConsentDB = false;
                        IsInstant = IsInstantProduct(PName);
                        CLSConsentInfo ObjCLSConsentInfo = new CLSConsentInfo();
                        if (ObjCollCLSConsentInfo != null)
                        {
                            if (ObjCollCLSConsentInfo.Count > 0)
                            {
                                if (ObjCollCLSConsentInfo.Any(d => d.IsConsentEmailOrFax == true))
                                {
                                    IsIfAnyConsentEmailOrFax = true;
                                }
                                if (ObjCollCLSConsentInfo.Any(d => d.ConsentReportType == PName))
                                {
                                    for (int iRow = 0; iRow < ObjCollCLSConsentInfo.Count; iRow++)
                                    {

                                        if (ObjCollCLSConsentInfo.ElementAt(iRow).ConsentReportType == PName && IsConsentDB == false)
                                        {
                                            IsConsentDB = true;
                                            ObjCLSConsentInfo.ConsentFileName = ObjCollCLSConsentInfo.ElementAt(iRow).ConsentFileName;
                                            ObjCLSConsentInfo.IsConsentEmailOrFax = ObjCollCLSConsentInfo.ElementAt(iRow).IsConsentEmailOrFax;
                                            ObjCLSConsentInfo.IsConsentRequired = ObjCollCLSConsentInfo.ElementAt(iRow).IsConsentRequired;
                                            ObjCLSConsentInfo.ConsentState = ObjCollCLSConsentInfo.ElementAt(iRow).ConsentState;
                                            PDFAttachFilePath = ObjCollCLSConsentInfo.ElementAt(iRow).PDFAttachFilePath;
                                        }
                                    }
                                }
                                else if (IsConsentDB == false)
                                {
                                    IsConsentDB = true;
                                    ObjCLSConsentInfo.ConsentFileName = string.Empty;
                                    ObjCLSConsentInfo.IsConsentEmailOrFax = false;
                                    ObjCLSConsentInfo.IsConsentRequired = false;
                                    ObjCLSConsentInfo.ConsentState = string.Empty;
                                }
                            }
                            else
                            {
                                ObjCLSConsentInfo.ConsentFileName = string.Empty;
                                ObjCLSConsentInfo.IsConsentEmailOrFax = false;
                                ObjCLSConsentInfo.IsConsentRequired = false;
                                ObjCLSConsentInfo.ConsentState = string.Empty;
                            }
                        }
                        string SelectedProduct = string.Empty;
                        SelectedProduct = PName;

                        #region apply for multiple jurisdiction for FCR report.

                        if (PName == "FCR")
                        {
                            SelectedProduct = PName;
                            int JurisdictionListCount = ObjJurisdictionList.Count;
                            if (JurisdictionListCount > 0)
                            {
                                for (int rowIndex = 0; rowIndex < JurisdictionListCount; rowIndex++)
                                {
                                    SearchedCountyName = ObjJurisdictionList.ElementAt(rowIndex).SearchedCountyName;
                                    var ProductQty = string.Empty;
                                    List<Proc_AddEmergeOrderDetailResult> ObjAddOrderDetailResultForFCR = ObjDALDataContext.Proc_AddEmergeOrderDetail(
                                                                                                        OrderId,
                                                                                                        PName,
                                                                                                        ApplicationId,
                                                                                                        LocationId,
                                                                                                        OrderedDate,
                                                                                                        IsInstant,
                                                                                                        IsLiveRunner,
                                                                                                        IsPackageReport,
                                                                                                        PackageInclude,
                                                                                                        fkPackageId,
                                                                                                        (ObjCLSConsentInfo.ConsentFileName != null) ? ObjCLSConsentInfo.ConsentFileName : "",
                                                                                                        ObjCLSConsentInfo.IsConsentEmailOrFax,
                                                                                                        ObjCLSConsentInfo.IsConsentRequired, ObjCLSConsentInfo.ConsentState, ProductQty, string.Empty).ToList();/*ObjCollProducts[iProduct]*/

                                    if (ObjAddOrderDetailResultForFCR.First().ReturnType > 0)
                                    {
                                        OrderDetailId = ObjAddOrderDetailResultForFCR.First().OrderDetailId;
                                        IsLiveRunner = (bool)ObjAddOrderDetailResultForFCR.First().IsLiveRunner;
                                        int OrderSearchedJurisdictionData = ObjDALDataContext.AddOrderSearchedJurisdictionInfo(OrderDetailId, OrderId, ObjJurisdictionList.ElementAt(rowIndex).SearchedCountyId, ObjJurisdictionList.ElementAt(rowIndex).SearchedCountyName);
                                        ObjDALDataContext.SetCCRFeeInOderDetailsTable(OrderDetailId, PName);
                                        if (OrderSearchedJurisdictionData < 0)
                                        {
                                            OrderId = 0;
                                            ObjEmergeReport.ObjOrderInfo.OrderId = OrderId;
                                            return ObjEmergeReport;
                                        }
                                    }
                                    if (OrderDetailId == 0) /*  If order detailed id is returned empty then data will be rollbacked  */
                                    {
                                        OrderId = 0;
                                        ObjEmergeReport.ObjOrderInfo.OrderId = OrderId;
                                        return ObjEmergeReport;
                                    }
                                    Vendor_Request = CreateXMLRequest(ObjtblOrder, SelectedProduct, ObjtblOrderSearchedData,
                                    ObjCollOrderSearchedEmploymentInfo,
                                    ObjCollOrderSearchedEducationInfo,
                                    ObjCollOrderSearchedPersonalInfo,
                                    ObjCollOrderSearchedProfessionalInfo,
                                        ObjCollOrderPersonalQtnInfo,
                                        ObjCollOrderLicenseInfo,
                                        ObjCollOrderSearchedAliasPersonalInfo,
                                        ObjCollOrderSearchedAliasLocationInfo,
                                        ObjCollOrderSearchedAliasWorkInfo,
                                        ApplicationId,
                                        OrderId,
                                        OrderDetailId,
                                        ObjtblVendor_List,
                                        ObjtblState_List,
                                        SearchedStateCode,
                                        SearchedDlStateCode,
                                        SearchedCountyName,
                                        ObjProc_GetAllProductsWithVendorResult,
                                        IsNullDOB, CompanyAccountNo, OrderCompanyName, PermissiblePurpose);
                                    //  Mcall method to create the XML String .
                                    int? Obj_pkOrderRequestForFcr = 0;
                                    int iResultRequestForFcr = ObjDALDataContext.AddOrderRequestData(OrderDetailId, Vendor_Request, ref Obj_pkOrderRequestForFcr); /* Add entry in order request table */
                                    if (iResultRequestForFcr < 0)
                                    {
                                        OrderId = 0;
                                        ObjEmergeReport.ObjOrderInfo.OrderId = OrderId;
                                        return ObjEmergeReport;
                                    }
                                    string CocatRequestForFcr = SelectedProduct + "^" + Vendor_Request + "^" + Obj_pkOrderRequestForFcr + "^" + IsInstant;
                                    objdic.Add(OrderDetailId, CocatRequestForFcr);/*ObjCollProducts.ElementAt(iProduct)*/

                                    #region //INT-9
                                    //#186  todo optimize code here
                                    using (EmergeDALDataContext myDB = new EmergeDALDataContext())
                                    {
                                        var orderid = (from i in myDB.tblOrderDetails
                                                       join c in myDB.tblCompanies on i.fkCompanyId equals c.pkCompanyId

                                                       where i.AdditionalFee > 0 && i.fkProductApplicationId == 47 && c.isfcrjurisdictionfee == true && i.pkOrderDetailId == OrderDetailId
                                                       orderby i.OrderDt descending
                                                       select new { i.pkOrderDetailId, i.AdditionalFee, i.fkOrderId }).ToList().FirstOrDefault();

                                        if (orderid != null)
                                        {
                                            var orderdetail = myDB.tblOrderDetails.Where(x => x.fkOrderId == orderid.fkOrderId);
                                            if (orderdetail != null)
                                            {
                                                foreach (var od in orderdetail)
                                                {
                                                    if (od.AdditionalFee > 0)
                                                    {
                                                        od.AdditionalFee = 0.00m;
                                                        myDB.SubmitChanges();
                                                    }
                                                }
                                            }
                                        }
                                    #endregion
                                    }
                                }
                                continue;
                            }
                        }
                        #endregion

                        #region Live Runner CCR's

                        if (PName == "CCR1" || PName == "CCR2" || PName == "RCX")
                        {
                            //int PkCompanyId = 0; ;
                            //int pkProductApplicationId = 0;
                            int grdRows = ObjColltblOrderSearchedLiveRunnerInfo.Count;
                            for (int rowIndex = 0; rowIndex < grdRows; rowIndex++)
                            {
                                SearchedStateCode = (!string.IsNullOrEmpty(ObjColltblOrderSearchedLiveRunnerInfo.ElementAt(rowIndex).LiveRunnerState.ToString()) ? ObjtblState_List.Where(data => data.pkStateId == Convert.ToInt32(ObjColltblOrderSearchedLiveRunnerInfo.ElementAt(rowIndex).LiveRunnerState)).First().StateCode : "");
                                string[] CountyName_IsLR = ObjColltblOrderSearchedLiveRunnerInfo.ElementAt(rowIndex).LiveRunnerCounty.Split('_');
                                SearchedCountyName = CountyName_IsLR[0];
                                SearchedCountyState = ObjColltblOrderSearchedLiveRunnerInfo.ElementAt(rowIndex).LiveRunnerState;
                                SearchedCountyId = ObjColltblOrderSearchedLiveRunnerInfo.ElementAt(rowIndex).SearchedCountyId;
                                IsLiveRunner = Convert.ToBoolean(CountyName_IsLR[1].ToLower());

                                #region 805: liverunner activation issues
                                if (IsLiveRunner && fkProductApplicationId != 0)
                                {
                                    using (EmergeDALDataContext dx = new EmergeDALDataContext())//805: lliverunner activation issues
                                    {
                                        try
                                        {
                                            IsLiveRunner = dx.tblProductAccesses.Where(d => d.fkProductApplicationId == fkProductApplicationId && d.fkLocationId == LocationId).Select(d => d.IsLiveRunner).FirstOrDefault();
                                        }
                                        catch (Exception ex)
                                        {
                                            //Added log for exception handling.
                                            VendorServices.GeneralService.WriteLog("Some Error Occured in AddOrders(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                                        }
                                    }
                                }
                                #endregion
                                var ProductQty = string.Empty;
                                List<Proc_AddEmergeOrderDetailResult> ObjAddOrderDetailResult1 = ObjDALDataContext.Proc_AddEmergeOrderDetail(
                                                                                                        OrderId,
                                                                                                        PName,
                                                                                                        ApplicationId,
                                                                                                        LocationId,
                                                                                                        OrderedDate,
                                                                                                        IsInstant,
                                                                                                        IsLiveRunner,
                                                                                                        IsPackageReport,
                                                                                                        PackageInclude,
                                                                                                        fkPackageId,
                                                                                                        (ObjCLSConsentInfo.ConsentFileName != null) ? ObjCLSConsentInfo.ConsentFileName : "",
                                                                                                    ObjCLSConsentInfo.IsConsentEmailOrFax,
                                                                                                    ObjCLSConsentInfo.IsConsentRequired, ObjCLSConsentInfo.ConsentState, ProductQty, string.Empty).ToList();



                                if (ObjAddOrderDetailResult1.First().ReturnType > 0)
                                {
                                    OrderDetailId = ObjAddOrderDetailResult1.First().OrderDetailId;
                                    IsLiveRunner = (bool)ObjAddOrderDetailResult1.First().IsLiveRunner;
                                    int iOrderSearchedLiveRunnerInfo = ObjDALDataContext.AddOrderSearchedLiverRunnerInfo(OrderId, OrderDetailId, SearchedCountyName, SearchedCountyState, SearchedCountyId);
                                    ObjDALDataContext.SetCCRFeeInOderDetailsTable(OrderDetailId, PName);/*This set the CCR report fees in the Order Detail Table*/
                                    if (iOrderSearchedLiveRunnerInfo < 0)
                                    {
                                        OrderId = 0;
                                        ObjEmergeReport.ObjOrderInfo.OrderId = OrderId;
                                        return ObjEmergeReport;
                                    }
                                }
                                if (OrderDetailId == 0) /*  If order detailed id is returned empty then data will be rollbacked  */
                                {
                                    OrderId = 0;
                                    ObjEmergeReport.ObjOrderInfo.OrderId = OrderId;
                                    return ObjEmergeReport;
                                }
                                string ProductCodeSelected;
                                if (IsLiveRunner)
                                {
                                    ProductCodeSelected = "RCX";
                                }
                                else
                                    ProductCodeSelected = PName;

                                Vendor_Request = CreateXMLRequest(ObjtblOrder, ProductCodeSelected, ObjtblOrderSearchedData,
                                                                                                ObjCollOrderSearchedEmploymentInfo,
                                                                                                ObjCollOrderSearchedEducationInfo,
                                                                                                ObjCollOrderSearchedPersonalInfo,
                                                                                                ObjCollOrderSearchedProfessionalInfo,
                                                                                                    ObjCollOrderPersonalQtnInfo,
                                                                                                    ObjCollOrderLicenseInfo,
                                                                                                    ObjCollOrderSearchedAliasPersonalInfo,
                                                                                                    ObjCollOrderSearchedAliasLocationInfo,
                                                                                                    ObjCollOrderSearchedAliasWorkInfo,
                                                                                                    ApplicationId,
                                                                                                    OrderId,
                                                                                                    OrderDetailId,
                                                                                                    ObjtblVendor_List,
                                                                                                    ObjtblState_List,
                                                                                                    SearchedStateCode,
                                                                                                    SearchedDlStateCode,
                                                                                                    SearchedCountyName,
                                                                                                    ObjProc_GetAllProductsWithVendorResult,
                                                                                                    IsNullDOB, CompanyAccountNo, OrderCompanyName, PermissiblePurpose);
                                int? Obj_pkOrderRequest1 = 0;
                                int iResultRequest1 = ObjDALDataContext.AddOrderRequestData(OrderDetailId, Vendor_Request, ref Obj_pkOrderRequest1);
                                if (iResultRequest1 < 0)
                                {
                                    OrderId = 0;
                                    ObjEmergeReport.ObjOrderInfo.OrderId = OrderId;
                                    return ObjEmergeReport;
                                }
                                string CocatRequest1 = ProductCodeSelected + "^" + Vendor_Request + "^" + Obj_pkOrderRequest1 + "^" + IsInstant;
                                objdic.Add(OrderDetailId, CocatRequest1);
                            }
                            continue;
                        }
                        #endregion

                        if (PName == "SCR") //If Selected State Is Live Runner
                        {

                            if (IsLiveRunnerState)//for liverunner
                            {
                                IsLiveRunner = true;

                                #region 805: liverunner activation issues
                                if (IsLiveRunner && fkProductApplicationId != 0)
                                {
                                    using (EmergeDALDataContext dx = new EmergeDALDataContext())//805: lliverunner activation issues
                                    {
                                        try
                                        {
                                            IsLiveRunner = dx.tblProductAccesses.Where(d => d.fkProductApplicationId == fkProductApplicationId && d.fkLocationId == LocationId).Select(d => d.IsLiveRunner).FirstOrDefault();
                                        }
                                        catch (Exception ex)
                                        {
                                            //Added log for exception handling.
                                            VendorServices.GeneralService.WriteLog("Some Error Occured in AddOrders(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                                        }
                                    }
                                }
                                #endregion

                                SelectedProduct = "RCX";
                                SearchedCountyName = "";
                            }
                            else//for non liverunner
                            {
                                SearchedStateCode = ObjDALDataContext.tblStates.Where(st => st.pkStateId.Equals(ObjtblOrderSearchedData.SearchedStateId)).Select(st => st.StateCode).FirstOrDefault();

                            }
                        }
                        var ProductQty1 = string.Empty;
                        //int? fkOrderId = OrderId;
                        if (PName == "EDV")
                        {
                            isVerificationReport = true;
                            ProductQty1 = "1";
                        }
                        else if (PName == "EMV")
                        {
                            isVerificationReport = true;
                            ProductQty1 = "1";
                        }
                        else if (PName == "PLV")
                        {
                            isVerificationReport = true;
                            ProductQty1 = "1";
                        }
                        else if (PName == "PRV")
                        {
                            isVerificationReport = true;
                            ProductQty1 = "1";
                        }
                        else if (PName == "PRV2")
                        {
                            isVerificationReport = true;
                            ProductQty1 = "1";
                        }

                        else if (PName == "WCV")
                        {
                            isVerificationReport = true;
                            ProductQty1 = "1";
                        }

                        List<Proc_AddEmergeOrderDetailResult> ObjAddOrderDetailResult = ObjDALDataContext.Proc_AddEmergeOrderDetail(
                                                                                                    OrderId,
                                                                                                    PName,
                                                                                                    ApplicationId,
                                                                                                    LocationId,
                                                                                                    OrderedDate,
                                                                                                    IsInstant,
                                                                                                    IsLiveRunner,
                                                                                                    IsPackageReport,
                                                                                                    PackageInclude,
                                                                                                    fkPackageId,
                                                                                                    (ObjCLSConsentInfo.ConsentFileName != null) ? ObjCLSConsentInfo.ConsentFileName : "",
                                                                                                    ObjCLSConsentInfo.IsConsentEmailOrFax,
                                                                                                    ObjCLSConsentInfo.IsConsentRequired, ObjCLSConsentInfo.ConsentState, ProductQty1, string.Empty).ToList();/*ObjCollProducts[iProduct]*/



                        if (ObjAddOrderDetailResult.First().ReturnType > 0)
                        {
                            OrderDetailId = ObjAddOrderDetailResult.First().OrderDetailId;
                            IsLiveRunner = (bool)ObjAddOrderDetailResult.First().IsLiveRunner;
                        }

                        if (OrderDetailId == 0) /*  If order detailed id is returned empty then data will be rollbacked  */
                        {
                            OrderId = 0;
                            ObjEmergeReport.ObjOrderInfo.OrderId = OrderId;
                            return ObjEmergeReport;
                        }

                        SearchedStateCode = (!string.IsNullOrEmpty(ObjtblOrderSearchedData.SearchedStateId.ToString()) ? ObjtblState_List.Where(data => data.pkStateId == ObjtblOrderSearchedData.SearchedStateId).First().StateCode : "");
                        if (PName == "BPS" || PName == "BPS" || PName == "UCCR" || PName == "UCC" || PName == "EBP")
                        {
                            SearchedStateCode = (!string.IsNullOrEmpty(ObjtblOrderSearchedData.SearchedBusinessStateId.ToString()) ? ObjtblState_List.Where(data => data.pkStateId == ObjtblOrderSearchedData.SearchedBusinessStateId).First().StateCode : "");
                        }
                        //  Create the Request in First Phase

                        // change for INT 328 BILLING ISSue on 10 march 2016
                        //According to greg ,we have to find add new amount in BILLING AND IN ITEMISED REPPORT SECTION 

                        if (PName == "EDV" || PName == "EMV" || PName == "PLV" || PName == "PRV" || PName == "PRV2" || PName == "WCV")
                        {

                            switch (PName)
                            {
                                case "EDV":
                                    foreach (tblOrderSearchedEducationInfo ObjSearchedEducationInfo in ObjCollOrderSearchedEducationInfo)
                                    {
                                        List<tblOrderSearchedEducationInfo> lstEDV = new List<tblOrderSearchedEducationInfo>();
                                        lstEDV.Add(ObjSearchedEducationInfo);
                                        Vendor_Request = CreateXMLRequest(ObjtblOrder, SelectedProduct, ObjtblOrderSearchedData,
                                                                   ObjCollOrderSearchedEmploymentInfo,
                                                                   lstEDV,
                                                                   ObjCollOrderSearchedPersonalInfo,
                                                                   ObjCollOrderSearchedProfessionalInfo,
                                                                       ObjCollOrderPersonalQtnInfo,
                                                                       ObjCollOrderLicenseInfo,
                                                                       ObjCollOrderSearchedAliasPersonalInfo,
                                                                       ObjCollOrderSearchedAliasLocationInfo,
                                                                       ObjCollOrderSearchedAliasWorkInfo,
                                                                       ApplicationId,
                                                                       OrderId,
                                                                       OrderDetailId,
                                                                       ObjtblVendor_List,
                                                                       ObjtblState_List,
                                                                       SearchedStateCode,
                                                                       SearchedDlStateCode,
                                                                       SearchedCountyName,
                                                                       ObjProc_GetAllProductsWithVendorResult,
                                                                       IsNullDOB, CompanyAccountNo, OrderCompanyName, PermissiblePurpose);/*ObjCollProducts.ElementAt(iProduct)*/

                                        break;

                                    }

                                    ObjCollOrderSearchedEducationInfo.RemoveAt(0);

                                    break;

                                case "EMV":
                                    foreach (tblOrderSearchedEmploymentInfo ObjSearchedEducationInfo in ObjCollOrderSearchedEmploymentInfo)
                                    {
                                        List<tblOrderSearchedEmploymentInfo> lstEMV = new List<tblOrderSearchedEmploymentInfo>();
                                        lstEMV.Add(ObjSearchedEducationInfo);
                                        Vendor_Request = CreateXMLRequest(ObjtblOrder, SelectedProduct, ObjtblOrderSearchedData,
                                                                   lstEMV,
                                                                   ObjCollOrderSearchedEducationInfo,
                                                                   ObjCollOrderSearchedPersonalInfo,
                                                                   ObjCollOrderSearchedProfessionalInfo,
                                                                       ObjCollOrderPersonalQtnInfo,
                                                                       ObjCollOrderLicenseInfo,
                                                                       ObjCollOrderSearchedAliasPersonalInfo,
                                                                       ObjCollOrderSearchedAliasLocationInfo,
                                                                       ObjCollOrderSearchedAliasWorkInfo,
                                                                       ApplicationId,
                                                                       OrderId,
                                                                       OrderDetailId,
                                                                       ObjtblVendor_List,
                                                                       ObjtblState_List,
                                                                       SearchedStateCode,
                                                                       SearchedDlStateCode,
                                                                       SearchedCountyName,
                                                                       ObjProc_GetAllProductsWithVendorResult,
                                                                       IsNullDOB, CompanyAccountNo, OrderCompanyName, PermissiblePurpose);/*ObjCollProducts.ElementAt(iProduct)*/

                                        break;

                                    }

                                    ObjCollOrderSearchedEmploymentInfo.RemoveAt(0);
                                    break;


                                case "PLV":
                                    foreach (tblOrderSearchedLicenseInfo ObjSearchedEducationInfo in ObjCollOrderLicenseInfo)
                                    {
                                        List<tblOrderSearchedLicenseInfo> lstPLV = new List<tblOrderSearchedLicenseInfo>();
                                        lstPLV.Add(ObjSearchedEducationInfo);
                                        Vendor_Request = CreateXMLRequest(ObjtblOrder, SelectedProduct, ObjtblOrderSearchedData,

                                                                       ObjCollOrderSearchedEmploymentInfo,
                                                                       ObjCollOrderSearchedEducationInfo,
                                                                       ObjCollOrderSearchedPersonalInfo,
                                                                       ObjCollOrderSearchedProfessionalInfo,
                                                                       ObjCollOrderPersonalQtnInfo,
                                                                       lstPLV,
                                                                       ObjCollOrderSearchedAliasPersonalInfo,
                                                                       ObjCollOrderSearchedAliasLocationInfo,
                                                                       ObjCollOrderSearchedAliasWorkInfo,
                                                                       ApplicationId,
                                                                       OrderId,
                                                                       OrderDetailId,
                                                                       ObjtblVendor_List,
                                                                       ObjtblState_List,
                                                                       SearchedStateCode,
                                                                       SearchedDlStateCode,
                                                                       SearchedCountyName,
                                                                       ObjProc_GetAllProductsWithVendorResult,
                                                                       IsNullDOB, CompanyAccountNo, OrderCompanyName, PermissiblePurpose);/*ObjCollProducts.ElementAt(iProduct)*/

                                        break;

                                    }

                                    ObjCollOrderLicenseInfo.RemoveAt(0);
                                    break;
                                case "PRV":
                                    foreach (tblOrderSearchedPersonalInfo ObjSearchedEducationInfo in ObjCollOrderSearchedPersonalInfo)
                                    {
                                        List<tblOrderSearchedPersonalInfo> lstPRV = new List<tblOrderSearchedPersonalInfo>();
                                        lstPRV.Add(ObjSearchedEducationInfo);
                                        Vendor_Request = CreateXMLRequest(ObjtblOrder, SelectedProduct, ObjtblOrderSearchedData,

                                                                   ObjCollOrderSearchedEmploymentInfo,
                                                                   ObjCollOrderSearchedEducationInfo,
                                                                   lstPRV,
                                                                   ObjCollOrderSearchedProfessionalInfo,
                                                                       ObjCollOrderPersonalQtnInfo,
                                                                       ObjCollOrderLicenseInfo,
                                                                       ObjCollOrderSearchedAliasPersonalInfo,
                                                                       ObjCollOrderSearchedAliasLocationInfo,
                                                                       ObjCollOrderSearchedAliasWorkInfo,
                                                                       ApplicationId,
                                                                       OrderId,
                                                                       OrderDetailId,
                                                                       ObjtblVendor_List,
                                                                       ObjtblState_List,
                                                                       SearchedStateCode,
                                                                       SearchedDlStateCode,
                                                                       SearchedCountyName,
                                                                       ObjProc_GetAllProductsWithVendorResult,
                                                                       IsNullDOB, CompanyAccountNo, OrderCompanyName, PermissiblePurpose);/*ObjCollProducts.ElementAt(iProduct)*/

                                        break;

                                    }

                                    ObjCollOrderSearchedPersonalInfo.RemoveAt(0);
                                    break;
                                case "PRV2":
                                    foreach (tblOrderSearchedProfessionalInfo ObjSearchedProfessionalInfo in ObjCollOrderSearchedProfessionalInfo)
                                    {
                                        List<tblOrderSearchedProfessionalInfo> lstPRV2 = new List<tblOrderSearchedProfessionalInfo>();
                                        lstPRV2.Add(ObjSearchedProfessionalInfo);
                                        Vendor_Request = CreateXMLRequest(ObjtblOrder, SelectedProduct, ObjtblOrderSearchedData,

                                                                   ObjCollOrderSearchedEmploymentInfo,
                                                                   ObjCollOrderSearchedEducationInfo,
                                                                  ObjCollOrderSearchedPersonalInfo,
                                                                       lstPRV2,
                                                                       ObjCollOrderPersonalQtnInfo,
                                                                       ObjCollOrderLicenseInfo,
                                                                       ObjCollOrderSearchedAliasPersonalInfo,
                                                                       ObjCollOrderSearchedAliasLocationInfo,
                                                                       ObjCollOrderSearchedAliasWorkInfo,
                                                                       ApplicationId,
                                                                       OrderId,
                                                                       OrderDetailId,
                                                                       ObjtblVendor_List,
                                                                       ObjtblState_List,
                                                                       SearchedStateCode,
                                                                       SearchedDlStateCode,
                                                                       SearchedCountyName,
                                                                       ObjProc_GetAllProductsWithVendorResult,
                                                                       IsNullDOB, CompanyAccountNo, OrderCompanyName, PermissiblePurpose);/*ObjCollProducts.ElementAt(iProduct)*/

                                        break;

                                    }

                                    ObjCollOrderSearchedProfessionalInfo.RemoveAt(0);
                                    break;
                                case "WCV":
                                    foreach (tblOrderSearchedLicenseInfo ObjSearchedEducationInfo in ObjCollOrderLicenseInfo)
                                    {
                                        List<tblOrderSearchedLicenseInfo> lstWCV = new List<tblOrderSearchedLicenseInfo>();
                                        lstWCV.Add(ObjSearchedEducationInfo);
                                        Vendor_Request = CreateXMLRequest(
                                                                          ObjtblOrder,
                                                                          SelectedProduct,
                                                                          ObjtblOrderSearchedData,
                                                                       ObjCollOrderSearchedEmploymentInfo,
                                                                       ObjCollOrderSearchedEducationInfo,
                                                                       ObjCollOrderSearchedPersonalInfo,
                                                                        ObjCollOrderSearchedProfessionalInfo,
                                                                         ObjCollOrderPersonalQtnInfo,
                                                                         lstWCV,
                                                                       ObjCollOrderSearchedAliasPersonalInfo,
                                                                       ObjCollOrderSearchedAliasLocationInfo,
                                                                       ObjCollOrderSearchedAliasWorkInfo,
                                                                       ApplicationId,
                                                                       OrderId,
                                                                        OrderDetailId,
                                                                       ObjtblVendor_List,
                                                                       ObjtblState_List,
                                                                       SearchedStateCode,
                                                                       SearchedDlStateCode,
                                                                       SearchedCountyName,
                                                                       ObjProc_GetAllProductsWithVendorResult,
                                                                       IsNullDOB, CompanyAccountNo, OrderCompanyName, PermissiblePurpose);/*ObjCollProducts.ElementAt(iProduct)*/

                                        break;

                                    }

                                    ObjCollOrderLicenseInfo.RemoveAt(0);

                                    break;
                            }
                        }
                        else
                        {
                            Vendor_Request = CreateXMLRequest(ObjtblOrder, SelectedProduct, ObjtblOrderSearchedData,
                            ObjCollOrderSearchedEmploymentInfo,
                            ObjCollOrderSearchedEducationInfo,
                            ObjCollOrderSearchedPersonalInfo,
                            ObjCollOrderSearchedProfessionalInfo,
                                ObjCollOrderPersonalQtnInfo,
                                ObjCollOrderLicenseInfo,
                                ObjCollOrderSearchedAliasPersonalInfo,
                                ObjCollOrderSearchedAliasLocationInfo,
                                ObjCollOrderSearchedAliasWorkInfo,
                                ApplicationId,
                                OrderId,
                                OrderDetailId,
                                ObjtblVendor_List,
                                ObjtblState_List,
                                SearchedStateCode,
                                SearchedDlStateCode,
                                SearchedCountyName,
                                ObjProc_GetAllProductsWithVendorResult,
                                IsNullDOB, CompanyAccountNo, OrderCompanyName, PermissiblePurpose);/*ObjCollProducts.ElementAt(iProduct)*/
                        }
                        //  Mcall method to create the XML String .
                        int? Obj_pkOrderRequest = 0;
                        int iResultRequest = ObjDALDataContext.AddOrderRequestData(OrderDetailId, Vendor_Request, ref Obj_pkOrderRequest); /* Add entry in order request table */
                        if (iResultRequest < 0)
                        {
                            OrderId = 0;
                            ObjEmergeReport.ObjOrderInfo.OrderId = OrderId;
                            return ObjEmergeReport;
                        }
                        string CocatRequest = SelectedProduct + "^" + Vendor_Request + "^" + Obj_pkOrderRequest + "^" + IsInstant;
                        objdic.Add(OrderDetailId, CocatRequest);
                    }
                    #endregion
                    if (ObjtblOrder.OrderType != 2)
                    {
                        #region Vendor Call

                        //string strmail = "";
                        string vendor_Res = "";
                        string Vendor_Req = "";
                        bool IsMVR = false;
                        bool IsSCR = false;
                        // for INT 173  START 04 MAY 2015 
                        bool IsEEI = false;
                        bool IsECR = false;
                        // for INT 173  START 04 MAY 2015 END
                        foreach (KeyValuePair<int?, string> data in objdic)
                        {
                            string[] concatRequest = data.Value.Split('^');
                            string product_Code = concatRequest[0];
                            string vendor_Req = concatRequest[1];
                            int pkOrderRequestId = Convert.ToInt32(concatRequest[2]);
                            bool IsInstantReport = Convert.ToBoolean(concatRequest[3]);
                            if (ObjtblOrderSearchedData.SearchedStateId == 32 && product_Code.ToLower() == "scr")//SCR for out side Usa state in ticket #132
                            {
                                //OUT SIDE USA NOT SEND REQUEST TO VENDOR
                            }
                            else
                                vendor_Res = GetXMLResponse(product_Code, ObjProc_GetAllProductsWithVendorResult, vendor_Req, OrderId, data.Key, IsInstantReport);

                            // Here is Set Host report Response for tiered other reports. 
                            #region Host Tiered Response for next report.

                            if (product_Code == ObjEmergeReport.ObjOrderInfo.HostProductCode)//product_Code == "PS"&&
                            {
                                ObjEmergeReport.ObjOrderInfo.Tiered2Response = vendor_Res;

                            }

                            #endregion

                            if (vendor_Res.Contains("<XML_INTERFACE>ZZZ</XML_INTERFACE>"))
                            {
                                string[] Vendor_Response_Parts = vendor_Res.Split('#');
                                vendor_Res = Vendor_Response_Parts[1].ToString();
                                Vendor_Req = Vendor_Response_Parts[0].ToString();
                                UpdateRequest(pkOrderRequestId, Vendor_Req);
                            }
                            //Guid? pkresponseid = new Guid();
                            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                            {
                                Dx.AddOrderResponse(data.Key, vendor_Res);
                            }
                            if (ObjtblOrderSearchedData.SearchedDLStateId == 47 && product_Code.ToLower() == "mvr")// 47 Pennsylvania	PA
                            {
                                IsMVR = true;
                                CheckResponseandUpdateStatus(product_Code, OrderId, vendor_Res, data.Key, IsInstantReport, CompanyAccountNo, OrderCompanyName, PermissiblePurpose, Is7YearFeaturesEnable);
                                UpdateReportStatus(OrderId, data.Key, Convert.ToByte("1"));
                            }
                            else if (ObjtblOrderSearchedData.SearchedStateId == 32 && product_Code.ToLower() == "scr")// 32 OutSide USA	OT
                            {
                                IsSCR = true;
                                CheckResponseandUpdateStatus(product_Code, OrderId, vendor_Res, data.Key, IsInstantReport, CompanyAccountNo, OrderCompanyName, PermissiblePurpose, Is7YearFeaturesEnable);
                                UpdateReportStatus(OrderId, data.Key, Convert.ToByte("1"));
                            }
                            // FOR INT 173 04 MAY 2015 WORK AS MVR FOR EEI AND ECR REPORT START 
                            else if (product_Code.ToLower() == "eei" || product_Code.ToLower() == "ecr")// CHECK WHICH REPORT IS EXIST FOR INT 147
                            {
                                if (product_Code.ToLower() == "eei")
                                {
                                    IsEEI = true;
                                }
                                else
                                {
                                    IsECR = true;
                                }


                                CheckResponseandUpdateStatus(product_Code, OrderId, vendor_Res, data.Key, IsInstantReport, CompanyAccountNo, OrderCompanyName, PermissiblePurpose, Is7YearFeaturesEnable);
                                UpdateReportStatus(OrderId, data.Key, Convert.ToByte("1"));
                            }
                            // FOR INT 173 04 MAY 2015 WORK AS MVR FOR EEI AND ECR REPORT END 

                            else
                            {
                                CheckResponseandUpdateStatus(product_Code, OrderId, vendor_Res, data.Key, IsInstantReport, CompanyAccountNo, OrderCompanyName, PermissiblePurpose, Is7YearFeaturesEnable);
                            }

                            vendor_Res = "";
                            Vendor_Req = "";

                        }

                        #endregion

                        #region Sending  E-Mail's

                        // code to Send an email For Testing Purpose .
                        BALCompany ObjCom = new BALCompany();
                        ObjBALGeneral = new BALGeneral();
                        int PkTemplateId = 29;
                        string emailText = "";
                        string Subject = "";
                        string ApplicantName = "";
                        string ReportCodes = "";
                        string UserEmail = "";
                        //string UserEmailOne = string.Empty;
                        //string UserEmailTwo = string.Empty;
                        //string MailToAddress = "";
                        int? fkCompanyUserId = ObjtblOrder.fkCompanyUserId;
                        var INFO = ObjCom.GetUserAndCompanyDetails(fkCompanyUserId).ToList();

                        var emailContent = ObjBALGeneral.GetEmailTemplate(ApplicationId, PkTemplateId);
                        emailText = emailContent.TemplateContent.ToString();
                        Subject = emailContent.TemplateSubject;

                        #region For New Style SystemTemplate
                        string Bookmarkfile = string.Empty;
                        string bookmarkfilepath = string.Empty;
                        BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                        Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                        Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                        Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                        emailText = Bookmarkfile;
                        #endregion

                        if (reportCodes.Count > 1)
                        {
                            for (int i = 0; i < reportCodes.Count; i++)
                            {
                                ReportCodes += reportCodes.ElementAt(i).ToString();

                                if (i != reportCodes.Count - 1)
                                {
                                    ReportCodes += ", ";
                                }
                            }
                        }
                        else
                        {
                            ReportCodes = reportCodes.ElementAt(0).ToString();
                        }
                        ApplicantName = ObjtblOrderSearchedData.SearchedFirstName + " " + ObjtblOrderSearchedData.SearchedLastName;
                        if (INFO.Count > 0)
                        {
                            UserEmail = INFO.ElementAt(0).Email;
                            //MailToAddress = INFO.ElementAt(0).ReportEmailAddress;
                            if (INFO.ElementAt(0).IsReportEmailActivated_Location == true)
                            {
                                if (INFO.ElementAt(0).LocationEmail != "")
                                {
                                    /* This condition is set for sending mail to COMPANY LOCATION */

                                    //For email status//
                                    BALGeneral objbalgenral = new BALGeneral();
                                    int pktemplateid = PkTemplateId;
                                    Guid user_ID = Guid.Empty;
                                    int? fkcompanyuserid = fkCompanyUserId;
                                    //bool email_status = 
                                    objbalgenral.UserEmailStatusCheck(user_ID, pktemplateid, fkcompanyuserid);
                                }
                            }
                        }
                        if (IsMVR)
                        {
                            var emailContent_Mvr = ObjBALGeneral.GetEmailTemplate(ApplicationId, 36);
                            emailText = emailContent_Mvr.TemplateContent.ToString();
                            #region For New Style SystemTemplate
                            string Bookmarkfile1 = string.Empty;

                            string bookmarkfilepath1 = string.Empty;
                            Bookmarkfile1 = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath1);
                            Bookmarkfile1 = Bookmarkfile1.Replace("%EmailHeader%", emailContent_Mvr.TemplateSubject);
                            Bookmarkfile1 = Bookmarkfile1.Replace("%EmailBody%", emailContent_Mvr.TemplateContent);
                            emailText = Bookmarkfile1;
                            #endregion


                            Subject = emailContent_Mvr.TemplateSubject.Replace("%OrderNumber%", ObjtblOrder.OrderNo).Replace("%ApplicantName%", ApplicantName);
                            SendEmailToAdminForMVR(ObjtblOrder.OrderNo, UserEmail, ApplicantName, ObjtblOrderSearchedData.SearchedDob, ObjtblOrderSearchedData.SearchedSsn, ObjtblOrderSearchedData.SearchedDriverLicense, "MVR", emailText, Subject);
                        }
                        if (IsSCR)
                        {
                            var emailContent_Mvr = ObjBALGeneral.GetEmailTemplate(ApplicationId, 36);
                            emailText = emailContent_Mvr.TemplateContent.ToString().Replace("<tr style=\"\"> <td align=\"right\"> DL# : </td> <td align=\"left\" style=\"padding-left: 5px;\"> %DLNumber% </td> </tr>", "");
                            #region For New Style SystemTemplate
                            string Bookmarkfile1 = string.Empty;

                            string bookmarkfilepath1 = string.Empty;
                            Bookmarkfile1 = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath1);
                            Bookmarkfile1 = Bookmarkfile1.Replace("%EmailHeader%", emailContent_Mvr.TemplateSubject);
                            Bookmarkfile1 = Bookmarkfile1.Replace("%EmailBody%", emailContent_Mvr.TemplateContent);
                            emailText = Bookmarkfile1;
                            #endregion


                            Subject = emailContent_Mvr.TemplateSubject.Replace("%OrderNumber%", ObjtblOrder.OrderNo).Replace("%ApplicantName%", ApplicantName);
                            SendEmailToAdminForSCR(ObjtblOrder.OrderNo, UserEmail, ApplicantName, ObjtblOrderSearchedData.SearchedDob, ObjtblOrderSearchedData.SearchedSsn, "SCR", emailText, Subject);
                        }


                        // for int 174 04 may 2015 start to send email as mvr report start

                        if (IsEEI)
                        {
                            var emailContent_Mvr = ObjBALGeneral.GetEmailTemplate(ApplicationId, 36);
                            emailText = emailContent_Mvr.TemplateContent.ToString().Replace("<tr style=\"\"> <td align=\"right\"> DL# : </td> <td align=\"left\" style=\"padding-left: 5px;\"> %DLNumber% </td> </tr>", "");
                            #region For New Style SystemTemplate
                            string Bookmarkfile1 = string.Empty;

                            string bookmarkfilepath1 = string.Empty;
                            Bookmarkfile1 = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath1);
                            Bookmarkfile1 = Bookmarkfile1.Replace("%EmailHeader%", emailContent_Mvr.TemplateSubject);
                            Bookmarkfile1 = Bookmarkfile1.Replace("%EmailBody%", emailContent_Mvr.TemplateContent);
                            emailText = Bookmarkfile1;
                            #endregion


                            Subject = emailContent_Mvr.TemplateSubject.Replace("%OrderNumber%", ObjtblOrder.OrderNo).Replace("%ApplicantName%", ApplicantName);
                            SendEmailToAdminForEEI(ObjtblOrder.OrderNo, UserEmail, ApplicantName, ObjtblOrderSearchedData.SearchedDob, ObjtblOrderSearchedData.SearchedSsn, "EEI", emailText, Subject);
                        }


                        if (IsECR)
                        {
                            var emailContent_Mvr = ObjBALGeneral.GetEmailTemplate(ApplicationId, 36);
                            emailText = emailContent_Mvr.TemplateContent.ToString().Replace("<tr style=\"\"> <td align=\"right\"> DL# : </td> <td align=\"left\" style=\"padding-left: 5px;\"> %DLNumber% </td> </tr>", "");
                            #region For New Style SystemTemplate
                            string Bookmarkfile1 = string.Empty;

                            string bookmarkfilepath1 = string.Empty;
                            Bookmarkfile1 = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath1);
                            Bookmarkfile1 = Bookmarkfile1.Replace("%EmailHeader%", emailContent_Mvr.TemplateSubject);
                            Bookmarkfile1 = Bookmarkfile1.Replace("%EmailBody%", emailContent_Mvr.TemplateContent);
                            emailText = Bookmarkfile1;
                            #endregion


                            Subject = emailContent_Mvr.TemplateSubject.Replace("%OrderNumber%", ObjtblOrder.OrderNo).Replace("%ApplicantName%", ApplicantName);
                            SendEmailToAdminForECR(ObjtblOrder.OrderNo, UserEmail, ApplicantName, ObjtblOrderSearchedData.SearchedDob, ObjtblOrderSearchedData.SearchedSsn, "ECR", emailText, Subject);
                        }


                        // for int 174 04 may 2015 start to send email as mvr report end
                        if (IsIfAnyConsentEmailOrFax == true)
                        {
                            var emailContent_CONSENT = ObjBALGeneral.GetEmailTemplate(ApplicationId, 39);           // PKTEMPLATEID = 39 for Consent Form
                            emailText = emailContent_CONSENT.TemplateContent.ToString();

                            #region For New Style SystemTemplate
                            string Bookmarkfile2 = string.Empty;
                            string bookmarkfilepath1 = string.Empty;

                            Bookmarkfile2 = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath1);
                            Bookmarkfile2 = Bookmarkfile2.Replace("%EmailHeader%", emailContent_CONSENT.TemplateSubject);
                            Bookmarkfile2 = Bookmarkfile2.Replace("%EmailBody%", emailContent_CONSENT.TemplateContent);
                            emailText = Bookmarkfile2;
                            #endregion

                            Subject = emailContent_CONSENT.TemplateSubject.Replace("%ApplicantName%", ApplicantName);
                        }
                        #endregion

                    }
                    //INT-316 Point#2 Jan 28, 2016
                    //If verification products are selected for report than mail will be sent.
                    if (isVerificationReport)
                    {
                        #region---Disabled WCV mail notification.
                        /*#region  Verification notification service,
                        //Greg need to disabled this functionality on Live.INT-336
                        int? fkCompanyUserId = ObjtblOrder.fkCompanyUserId;
                        var INFO = new BALCompany().GetUserAndCompanyDetails(fkCompanyUserId).ToList();
                        string ApplicantName = ObjtblOrderSearchedData.SearchedFirstName + " " + ObjtblOrderSearchedData.SearchedLastName;
                        string DOB = ObjtblOrderSearchedData.SearchedDob;
                        string SSN = ObjtblOrderSearchedData.SearchedSsn;
                        string DLNumber = ObjtblOrderSearchedData.SearchedDriverLicense;
                        string state = Convert.ToString(ObjtblOrderSearchedData.search_state);
                        string TemplateSelection = string.Empty;
                        string EduInstName = string.Empty;
                        string EduAliasname = string.Empty;
                        string Educity = string.Empty;
                        string EdusearchedState = string.Empty;
                        string EmpCompName = string.Empty;
                        string EmpStatename = string.Empty;
                        string EmpCityname = string.Empty;
                        string EmpPhonename = string.Empty;
                        string EmpAliasname = string.Empty;
                        string persoanlrename = string.Empty;
                        string persoanlrePhone = string.Empty;
                        string persoanlAlias = string.Empty;
                        string PLLicenseInfo = string.Empty;
                        string PLstateInfo = string.Empty;
                        string plAlias = string.Empty;
                        string WLLicenseInfo = string.Empty;
                        string emailText = string.Empty;
                        string UserEmail = string.Empty;
                        if (INFO.Count > 0)
                        {
                            UserEmail = INFO.ElementAt(0).Email;
                        }
                        string ReportCodes = string.Empty;
                        int? orderid = ObjDALDataContext.tblOrders.Where(rec => rec.OrderNo == ObjtblOrder.OrderNo).FirstOrDefault().pkOrderId;
                        VendorServices.GeneralService.WriteLog("Start process in -----------------Add Orders-------------------------------------------", "VerificationEmailSystem");
                        VendorServices.GeneralService.WriteLog("Start process in Add order for orderid : " + orderid.ToString() + DateTime.Now.ToString(), "VerificationEmailSystem");
                        VendorServices.GeneralService.WriteLog("Start process in Add order for verification process : " + DateTime.Now.ToString(), "VerificationEmailSystem");
                        if (reportCodes.Count > 1)
                        {
                            VendorServices.GeneralService.WriteLog("Start process reportCodes.Count > 1 : " + reportCodes.Count.ToString() + DateTime.Now.ToString(), "VerificationEmailSystem");

                            for (int i = 0; i < reportCodes.Count; i++)
                            {
                                ReportCodes = reportCodes.ElementAt(i).ToString();
                                if (ReportCodes == "WCV")
                                {
                                    VendorServices.GeneralService.WriteLog("Start process ReportCodess : " + ReportCodes.ToString() + DateTime.Now.ToString(), "VerificationEmailSystem");
                                    switch (ReportCodes)
                                    {
                                        case "WCV": TemplateSelection = Convert.ToString(ConfigurationManager.AppSettings["WCVTemplateSelection"]);

                                            VendorServices.GeneralService.WriteLog("Start process TemplateSelection : " + TemplateSelection.ToString() + DateTime.Now.ToString(), "VerificationEmailSystem");
                                            var WLLicenseInfodata = ObjDALDataContext.tblOrderSearchedLicenseInfos.Where(rec => rec.fkOrderId == orderid).FirstOrDefault();
                                            WLLicenseInfo = Convert.ToString(WLLicenseInfodata.SearchedLicenseType);
                                            var emailContent_Mvr = ObjBALGeneral.GetEmailTemplate(ApplicationId, Convert.ToInt32(TemplateSelection));
                                            emailText = emailContent_Mvr.TemplateContent.ToString();
                                            VendorServices.GeneralService.WriteLog("emailText is ok " + DateTime.Now.ToString(), "VerificationEmailSystem");
                                            //edit by ak 01 feb 2016 for adding order information in mail section 
                                            string Subject = emailContent_Mvr.TemplateSubject
                                                .Replace("%OrderNumber%", ObjtblOrder.OrderNo)
                                                .Replace("%ApplicantName%", ApplicantName) + " Verification.";
                                            emailText = emailText.Replace("%OrderNumber%", ObjtblOrder.OrderNo)
                                               .Replace("%ApplicantName%", ApplicantName).Replace("%ReportCode%", ReportCodes)
                                               .Replace("%DOB%", DOB)
                                                .Replace("%SSN%", SSN)
                                                .Replace("%UserEmail%", ObjtblOrderSearchedData.SearchedApplicantEmail)
                                                .Replace("%WLLicenseInfo%", WLLicenseInfo)
                                                .Replace("%DLNumber%", DLNumber);
                                            SendEmailToAdminForMVR(ObjtblOrder.OrderNo, UserEmail, ApplicantName, ObjtblOrderSearchedData.SearchedDob, ObjtblOrderSearchedData.SearchedSsn, ObjtblOrderSearchedData.SearchedDriverLicense, ReportCodes, emailText, Subject);
                                            VendorServices.GeneralService.WriteLog("SendEmailToAdminForMVR is success " + DateTime.Now.ToString(), "VerificationEmailSystem");
                                            break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            VendorServices.GeneralService.WriteLog("Start process in -----------------------------------------", "VerificationEmailSystem");
                            VendorServices.GeneralService.WriteLog("Start process in -------Report Includes<1-  ---------------", "VerificationEmailSystem");

                            ReportCodes = reportCodes.ElementAt(0).ToString();
                            switch (ReportCodes)
                            {
                                case "WCV": TemplateSelection = Convert.ToString(ConfigurationManager.AppSettings["WCVTemplateSelection"]);

                                    VendorServices.GeneralService.WriteLog("Start process TemplateSelection : " + TemplateSelection.ToString() + DateTime.Now.ToString(), "VerificationEmailSystem");
                                    var WLLicenseInfodata = ObjDALDataContext.tblOrderSearchedLicenseInfos.Where(rec => rec.fkOrderId == orderid).FirstOrDefault();
                                    WLLicenseInfo = Convert.ToString(WLLicenseInfodata.SearchedLicenseType);
                                    var emailContent_Mvr = ObjBALGeneral.GetEmailTemplate(ApplicationId, Convert.ToInt32(TemplateSelection));
                                    emailText = emailContent_Mvr.TemplateContent.ToString();
                                    VendorServices.GeneralService.WriteLog("emailText is ok " + DateTime.Now.ToString(), "VerificationEmailSystem");
                                    //edit by ak 01 feb 2016 for adding order information in mail section 
                                    string Subject = emailContent_Mvr.TemplateSubject
                                        .Replace("%OrderNumber%", ObjtblOrder.OrderNo)
                                        .Replace("%ApplicantName%", ApplicantName) + " Verification.";
                                    emailText = emailText.Replace("%OrderNumber%", ObjtblOrder.OrderNo)
                                       .Replace("%ApplicantName%", ApplicantName).Replace("%ReportCode%", ReportCodes)
                                       .Replace("%DOB%", DOB)
                                        .Replace("%SSN%", SSN)
                                        .Replace("%UserEmail%", ObjtblOrderSearchedData.SearchedApplicantEmail)
                                        .Replace("%WLLicenseInfo%", WLLicenseInfo)
                                        .Replace("%DLNumber%", DLNumber);
                                    SendEmailToAdminForMVR(ObjtblOrder.OrderNo, UserEmail, ApplicantName, ObjtblOrderSearchedData.SearchedDob, ObjtblOrderSearchedData.SearchedSsn, ObjtblOrderSearchedData.SearchedDriverLicense, ReportCodes, emailText, Subject);
                                    VendorServices.GeneralService.WriteLog("SendEmailToAdminForMVR is success " + DateTime.Now.ToString(), "VerificationEmailSystem");
                                    break;
                            }
                        }
                        #endregion */
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    #region Transaction Rollback
                    OrderId = 0;
                    #endregion
                    //Added log for exception handling.
                    VendorServices.GeneralService.WriteLog("Some Error Occured in AddOrders(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                }
                finally
                {
                    #region Connection and Transaction Close

                    ObjDALDataContext.Transaction = null;
                    if (ObjDALDataContext.Connection.State == ConnectionState.Open)
                    {
                        ObjDALDataContext.Connection.Close(); /* Close Connection */
                    }

                    #endregion
                }

                #region Return OrderId

                return ObjEmergeReport; /* Here we return successfully inserted order id */

                #endregion
            }
        }
        public string AddOrdersEscreen(tblOrder ObjtblOrder, List<string> ObjCollProducts, tblOrderSearchedData ObjtblOrderSearchedData, List<tblOrderDetail> ObjCollOrderDetail, string eSProductCode, string Request)        /*, int AuthorizedPurpose*/
        {
            #region Variable Assignment

            int? OrderId = 0;
            DbTransaction DbTrans = null;
            //string PackageInclude = string.Empty;
            string response = "", responseurl = "", Error = "";
            bool IsTieredOrder = false;
            Dictionary<string, string> Response_Dic = new Dictionary<string, string>();
            string logfilename = "Escreen/" + DateTime.Now.Ticks + "_1";
            #endregion
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {

                    #region Connection Open and Transaction Assignment

                    ObjDALDataContext.Connection.Open(); /* Connection Open */
                    DbTrans = ObjDALDataContext.Connection.BeginTransaction(); /* Assign Transation */
                    ObjDALDataContext.Transaction = DbTrans;

                    #endregion

                    #region Insert in Order Table

                    /* Here data insert in main order table and after successfully insertion orderid will be returned  */
                    int? LocationId = ObjtblOrder.fkLocationId;
                    DateTime OrderedDate = ObjtblOrder.OrderDt;
                    if (ObjtblOrder.fkBatchId == null)
                        ObjtblOrder.fkBatchId = 0;
                    //return Guid.Empty;
                    //first time eScreen order will be in-complete.
                    ObjtblOrder.OrderStatus = 3;
                    List<Proc_AddEmergeOrdersResult> ObjAddOrderResult = ObjDALDataContext.AddEmergeOrders(LocationId,
                                                                                                    ObjtblOrder.fkCompanyUserId,
                                                                                                    ObjtblOrder.OrderNo,
                                                                                                    OrderedDate,
                                                                                                    ObjtblOrder.OrderStatus,
                                                                                                    eSProductCode,
                                                                                                    ObjtblOrder.CreatedDate,
                                                                                                    ObjtblOrder.CreatedById,
                                                                                                    false, 0, false, string.Empty, string.Empty, 1, string.Empty, null, true, ObjtblOrder.SearchPersonHTML, IsTieredOrder).ToList();
                    if (ObjAddOrderResult.First().ReturnType > 0) /* If insertinon is successfully done then returntype must be greater than zero  */
                    {
                        OrderId = ObjAddOrderResult.First().OrderId;
                        Request = Request.Replace("%CurrentOrderId%", OrderId.ToString());
                    }

                    if (OrderId == 0) /* If insertion in order table is not successfull then data is rollback */
                    {
                        if (DbTrans != null)
                        {
                            DbTrans.Rollback(); /* Rollback Transaction  */
                        }
                        /* An empty orderid wlll be returned */
                    }

                    #endregion

                    #region Insert in Order Search Table

                    /* In this following code all searched data inserted */
                    // 1 is saved as a county Id temporary
                    int iOrderSearchedData = ObjDALDataContext.AddOrderSearchedData(OrderId,
                                                                                    ObjtblOrderSearchedData.SearchedLastName,
                                                                                    ObjtblOrderSearchedData.SearchedFirstName,
                                                                                    ObjtblOrderSearchedData.SearchedMiddleInitial,
                                                                                    ObjtblOrderSearchedData.SearchedSuffix,
                                                                                    ObjtblOrderSearchedData.SearchedSsn == "___-__-____" ? "" : ObjtblOrderSearchedData.SearchedSsn,
                                                                                    ObjtblOrderSearchedData.SearchedDob,
                                                                                    ObjtblOrderSearchedData.SearchedPhoneNumber == "(___) ___-____" ? "" : ObjtblOrderSearchedData.SearchedPhoneNumber,
                                                                                    ObjtblOrderSearchedData.SearchedStreetAddress,
                                                                                    ObjtblOrderSearchedData.SearchedDirection,
                                                                                    ObjtblOrderSearchedData.SearchedStreetName,
                                                                                    ObjtblOrderSearchedData.SearchedStreetType,
                                                                                    ObjtblOrderSearchedData.SearchedApt,
                                                                                    ObjtblOrderSearchedData.SearchedCity,
                                                                                    ObjtblOrderSearchedData.SearchedStateId,
                                                                                    ObjtblOrderSearchedData.SearchedZipCode,
                                                                                    ObjtblOrderSearchedData.SearchedCountyId,
                                                                                    ObjtblOrderSearchedData.SearchedDriverLicense,
                                                                                    ObjtblOrderSearchedData.SearchedVehicleVin,
                                                                                    ObjtblOrderSearchedData.SearchedDLStateId,
                                                                                    ObjtblOrderSearchedData.SearchedSex,
                                                                                    ObjtblOrderSearchedData.SearchedBusinessName,
                                                                                    ObjtblOrderSearchedData.SearchedBusinessCity,
                                                                                    ObjtblOrderSearchedData.SearchedBusinessStateId,
                                                                                    ObjtblOrderSearchedData.SearchedTrackingRef,
                                                                                    ObjtblOrderSearchedData.SearchedTrackingRefId,
                                                                                    ObjtblOrderSearchedData.SearchedTrackingNotes,
                                                                                    ObjtblOrderSearchedData.SearchedApplicantEmail,
                                                                                    ObjtblOrderSearchedData.SearchedPermissibleId//ND-5 
                                                                                    );


                    if (iOrderSearchedData < 0)
                    {

                        if (DbTrans != null)
                        {
                            DbTrans.Rollback(); /* Rollback Transaction */
                        }

                    }
                    #endregion
                    string Xmlerormessage = string.Empty;
                    int flageror1 = 1;
                    int flageror2 = 1;
                    #region Insert in Order Detail Table
                    Guid ApplicationId = new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852");

                    for (int iProduct = 0; iProduct < ObjCollOrderDetail.Count; iProduct++) /* This is for searched product  */
                    {
                        int? OrderDetailId = 0;
                        string Pname = eSProductCode.ToLower();
                        int fkProductApplicationId = 0;
                        using (EmergeDALDataContext dx = new EmergeDALDataContext())
                        {
                            fkProductApplicationId = dx.tblProductPerApplications.Where(d => d.ProductCode.ToLower() == Pname).Select(d => d.pkProductApplicationId).FirstOrDefault();
                        }

                        int? LocationIds = ObjtblOrder.fkLocationId;
                        decimal ReportAmount = GetReportAmountByProductApplicationId(Pname, fkProductApplicationId, LocationIds);
                        List<Proc_AddEmergeDrugTestOrderDetailResult> ObjAddOrderDetailResult = ObjDALDataContext.AddEmergeDrugTestOrderDetail(
                                                                                                 OrderId,
                                                                                                 fkProductApplicationId,
                                                                                                 ApplicationId,
                                                                                                 LocationIds,
                                                                                                 OrderedDate,
                                                                                                 1,
                                                                                                 "",
                                                                                                 ReportAmount).ToList();

                        if (ObjAddOrderDetailResult.First().ReturnType > 0)
                        {
                            OrderDetailId = ObjAddOrderDetailResult.First().OrderDetailId;
                            EscreenService ObjEscreenService = new EscreenService();
                            try
                            {
                                Response_Dic = ObjEscreenService.RunReport(Request.ToString());
                                response = Response_Dic["response"];
                                Error = Response_Dic["error"];
                                XDocument ObjXDocumentresponse = new XDocument();
                                if (string.IsNullOrEmpty(response) == false)
                                {
                                    ObjXDocumentresponse = XDocument.Parse(response);
                                    var TicketResponse = ObjXDocumentresponse.Descendants("TicketResponse").ToList();
                                    if (TicketResponse.Count > 0)
                                    {
                                        var Result = TicketResponse.Descendants("Result").FirstOrDefault();
                                        if (Result == null)
                                        {
                                            var Errors = TicketResponse.Descendants("Errors").FirstOrDefault();
                                            if (Errors != null)
                                            {
                                                XElement Description = Errors.Descendants("Description").FirstOrDefault();
                                                Xmlerormessage = Description.Value.ToString();
                                            }


                                        }
                                        if (Result != null)
                                        {
                                            var Action = Result.Descendants("Action").FirstOrDefault();
                                            if (Action != null)
                                            {
                                                responseurl = Action.Value.ToString();
                                            }
                                        }
                                    }

                                }
                            }
                            catch (Exception ex)
                            {
                                //Added log for exception handling.
                                VendorServices.GeneralService.WriteLog("Some Error Occured in AddOrdersEscreen(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                            }
                            finally
                            {
                                ObjEscreenService = null;
                            }


                            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                            {

                                try
                                {

                                    GeneralService.WriteLog("\ntop ", logfilename);
                                    int? Obj_pkOrderRequest = 0;
                                    //int iResultRequest = 
                                    Dx.AddOrderRequestData(OrderDetailId, Request, ref Obj_pkOrderRequest);
                                    GeneralService.WriteLog("\n successfullyyes: " + Obj_pkOrderRequest + " :" + OrderDetailId, logfilename);
                                }
                                catch (Exception ex)
                                {
                                    GeneralService.WriteLog("\n inner exception: " + ex.ToString(), logfilename);

                                    //Added log for exception handling.
                                    VendorServices.GeneralService.WriteLog("Some Error Occured in AddOrdersEscreen(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                                }

                            }

                            if (OrderDetailId == 0) /*  If order detailed id is returned empty then data will be rollbacked  */
                            {

                                if (DbTrans != null)
                                {
                                    DbTrans.Rollback(); /* Rollback Transaction */
                                }

                            }
                        }
                    #endregion
                        #region Transaction Committed

                        DbTrans.Commit(); /* Commit Transaction */
                        using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                        {
                            if (string.IsNullOrEmpty(response) == false)
                            {
                                if (string.IsNullOrEmpty(Xmlerormessage) == false)
                                {

                                    GeneralService.WriteLog("\n errormessageyes: " + OrderDetailId + " :" + response, logfilename);

                                    UpdateOrderResponseData(OrderDetailId, response);
                                    UpdateOrderDetail(OrderDetailId, response);

                                    GeneralService.WriteLog("\n errormessageyes-success: " + OrderDetailId + " :" + response, logfilename);

                                    flageror1 = 2;
                                }
                                else
                                {
                                    GeneralService.WriteLog("\n else-errormessageyes: " + OrderDetailId + " :" + response, logfilename);
                                    Dx.AddOrderResponse(OrderDetailId, responseurl);
                                }
                            }

                            if (string.IsNullOrEmpty(response) == true)
                            {
                                GeneralService.WriteLog("\n empty-errormessage: " + OrderDetailId + " :" + response, logfilename);
                                UpdateOrderResponseData(OrderDetailId, Error);
                                UpdateOrderDetail(OrderDetailId, Error);
                                flageror2 = 2;
                                GeneralService.WriteLog("\n empty-errormessageyes: " + OrderDetailId + " :" + response, logfilename);
                            }
                        }
                        if (flageror1 == 2) { responseurl = "popup" + Xmlerormessage; }
                        if (flageror2 == 2) { responseurl = "popup" + Error; }
                    }
                        #endregion
                }
                catch (Exception ex)
                {
                    #region Transaction Rollback

                    OrderId = 0;
                    if (DbTrans != null)
                    {
                        DbTrans.Rollback(); /* Rollback Transaction */
                    }
                    GeneralService.WriteLog("\n inner exception: " + ex.ToString(), logfilename);
                    #endregion


                    //Added log for exception handling.
                    VendorServices.GeneralService.WriteLog("Some Error Occured in AddOrdersEscreen(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                }
                finally
                {
                    #region Connection and Transaction Close

                    ObjDALDataContext.Transaction = null;
                    if (ObjDALDataContext.Connection.State == ConnectionState.Open)
                    {
                        ObjDALDataContext.Connection.Close(); /* Close Connection */
                    }

                    #endregion
                }
                return responseurl;
            }
        }

        public void UpdateOrderResponseData(int? OrderDetailId, string responseurl)
        {
            using (EmergeDALDataContext ObjDX = new EmergeDALDataContext())
            {
                string Emptyvariable = string.Empty;
                tblOrderResponseData tbl = new tblOrderResponseData();
                tbl.fkOrderDetailId = OrderDetailId;
                tbl.ResponseData = string.Empty;
                tbl.EmergeReviewNote = Emptyvariable;
                tbl.ErrorResponseData = responseurl;
                tbl.ManualResponse = Emptyvariable;
                tbl.ResponseFileName = Emptyvariable;
                tbl.FilteredResponseData = string.Empty;
                ObjDX.tblOrderResponseDatas.InsertOnSubmit(tbl);
                ObjDX.SubmitChanges();
            }

        }
        public void UpdateOrderDetail(int? OrderDetailId, string responseurl)
        {
            using (EmergeDALDataContext ObjDX = new EmergeDALDataContext())
            {
                try
                {
                    var Obj = (from t1 in ObjDX.tblOrderDetails where t1.pkOrderDetailId == OrderDetailId select new { t1 }).First();
                    if (Obj != null) { Obj.t1.ErrorLog = responseurl; ObjDX.SubmitChanges(); }

                }
                catch (Exception ex)
                {
                    //Added log for exception handling.
                    VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateOrderDetail(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                }
            }
        }


        public int UpdateEscreenResponse(int fkOrderId, string EventId)
        {
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    int Status = -1;
                    int OrderDetailId = DX.tblOrderDetails.Where(db => db.fkOrderId == fkOrderId).Select(db => db.pkOrderDetailId).First();
                    tblOrderResponseData ObjRsData = DX.tblOrderResponseDatas.Where(db => db.fkOrderDetailId == OrderDetailId).First();
                    if (ObjRsData != null)
                    {
                        ObjRsData.ManualResponse = EventId;
                        DX.SubmitChanges();
                        Status = 1;
                    }
                    return Status;
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateEscreenResponse(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

                throw;
            }
        }

        public int UpdateOrderStausforEscreen(int fkOrderId)
        {
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    int Status = -1;

                    var Obj = (from t1 in DX.tblOrders
                               join t2 in DX.tblOrderDetails on t1.pkOrderId equals t2.fkOrderId
                               where t1.pkOrderId == fkOrderId
                               select new { t1, t2 }).First();


                    if (Obj != null)
                    {
                        Obj.t1.OrderStatus = 2;
                        Obj.t2.ReportStatus = 2;
                        DX.SubmitChanges();
                        //To Create Invoice based on the selected Order.
                        new BALInvoice().CreateInvoice(fkOrderId);
                        Status = 1;


                    }
                    return Status;
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateOrderStausforEscreen(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

                throw;
            }

        }

        /// <summary>
        /// To incomplete Order status for Escreen if event id is missing in response.
        /// </summary>
        /// <param name="fkOrderId"></param>
        /// <returns></returns>
        public int UpdateOrderIncompleteStausforEscreen(int fkOrderId)
        {
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    int Status = -1;
                    //To getting the refernces of gein order from tblOrders and tblOrderDetails.
                    var Obj = (from t1 in DX.tblOrders
                               join t2 in DX.tblOrderDetails on t1.pkOrderId equals t2.fkOrderId
                               where t1.pkOrderId == fkOrderId
                               select new { t1, t2 }).First();


                    if (Obj != null)
                    {
                        Obj.t1.OrderStatus = 3;
                        Obj.t2.ReportStatus = 3;
                        DX.SubmitChanges();
                        //here if order has incompleted in status then no invoice will be created.
                        Status = 1;


                    }
                    return Status;
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateOrderStausforEscreen(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

                throw;
            }

        }

        public string GetEscreenOrder(int fkOrderId, string Request)        /*, int AuthorizedPurpose*/
        {
            #region Variable Assignment

            string response = "", responseurl = "", Error = "";
            Dictionary<string, string> Response_Dic = new Dictionary<string, string>();
            #endregion
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {

                int OrderDetailId = DX.tblOrderDetails.Where(db => db.fkOrderId == fkOrderId).Select(db => db.pkOrderDetailId).First();
                string EventID = DX.tblOrderResponseDatas.Where(db => db.fkOrderDetailId == OrderDetailId).Select(db => db.ManualResponse).First();
                Request = Request.Replace("%CurrentEventID%", EventID);
                EscreenService ObjEscreenService = new EscreenService();
                try
                {
                    Response_Dic = ObjEscreenService.RunReport(Request.ToString());
                    response = Response_Dic["response"];
                    Error = Response_Dic["error"];

                    XDocument ObjXDocumentresponse = new XDocument();
                    ObjXDocumentresponse = XDocument.Parse(response);
                    var TicketResponse = ObjXDocumentresponse.Descendants("TicketResponse").ToList();
                    if (TicketResponse.Count > 0)
                    {
                        var Result = TicketResponse.Descendants("Result").FirstOrDefault();
                        var Action = Result.Descendants("Action").FirstOrDefault();
                        if (Action != null)
                        {
                            responseurl = Action.Value.ToString();
                        }
                    }

                }
                catch (Exception ex)
                {
                    //Added log for exception handling.
                    VendorServices.GeneralService.WriteLog("Some Error Occured in GetEscreenOrder(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                }

                finally
                {
                    ObjEscreenService = null;
                }

                return responseurl;
            }
        }
        #region 1ticket#440

        public int UpdateOrders4Tiered(tblOrder ObjtblOrder, List<string> ObjCollProducts, tblOrderSearchedData ObjtblOrderSearchedData,
                    List<tblOrderSearchedEmploymentInfo> ObjCollOrderSearchedEmploymentInfo,
                    List<tblOrderSearchedEducationInfo> ObjCollOrderSearchedEducationInfo,
                    List<tblOrderSearchedPersonalInfo> ObjCollOrderSearchedPersonalInfo,
            List<tblOrderSearchedProfessionalInfo> ObjCollOrderSearchedProfessionalInfo,
                    List<tblOrderPersonalQtnInfo> ObjCollOrderPersonalQtnInfo,
                    List<tblOrderSearchedLicenseInfo> ObjCollOrderLicenseInfo,
                    List<tblOrderSearchedLiveRunnerInfo> ObjColltblOrderSearchedLiveRunnerInfo,
                    List<CLSConsentInfo> ObjCollCLSConsentInfo,
                    Guid ApplicationId, int IsNCR1NullDOB, bool IsLiveRunnerState,
            //Dictionary<string, string> DictStateCode,
            List<TiredSearchedData> DictStateCode, List<TiredSearchedDataJurisdiction> DicJurisdictionColl, string CompanyAccountNo, string OrderCompanyName, string PermissiblePurpose)        /*, int AuthorizedPurpose*/
        {
            int IsNullDOB = IsNCR1NullDOB;
            OrderCompanyName = OrderCompanyName.Replace("?", "").Replace(".", "").Replace("&", "").Replace("%", "").Replace("#", "").Replace("'", "");
            #region Variable Assignment

            int OrderId = 0;
            OrderId = ObjtblOrder.pkOrderId;
            DbTransaction DbTrans = null;
            bool IsPackageReport = false;
            string PackageInclude = string.Empty;
            int fkPackageId = 0;

            #endregion

            List<tblOrderSearchedAliasPersonalInfo> ObjCollOrderSearchedAliasPersonalInfo = new List<tblOrderSearchedAliasPersonalInfo>();
            List<tblOrderSearchedAliasLocationInfo> ObjCollOrderSearchedAliasLocationInfo = new List<tblOrderSearchedAliasLocationInfo>();
            List<tblOrderSearchedAliasWorkInfo> ObjCollOrderSearchedAliasWorkInfo = new List<tblOrderSearchedAliasWorkInfo>();
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                List<Proc_GetAllProductsWithVendorResult> ObjProc_GetAllProductsWithVendorResult;

                try
                {

                    #region Connection Open and Transaction Assignment
                    ObjDALDataContext.Connection.Open(); /* Connection Open */
                    #endregion

                    string ReportIncludes = string.Empty;
                    int Packageid = 0;
                    foreach (string ProductCodes in ObjCollProducts)
                    {
                        if (ProductCodes.Contains("_"))
                        {
                            string[] Pcode = ProductCodes.Split('_');
                            int.TryParse(Pcode[1], out Packageid);
                            if (Packageid != 0)
                            {
                                tblProductPackage ObjtblProductPackages = new tblProductPackage();
                                ObjtblProductPackages = ObjDALDataContext.tblProductPackages.Where(d => d.pkPackageId == Packageid).FirstOrDefault();
                                if (ObjtblProductPackages != null)
                                {
                                    if (!ReportIncludes.Contains(ObjtblProductPackages.PackageCode))
                                    {
                                        ReportIncludes += ObjtblProductPackages.PackageCode.ToString() + ", ";
                                    }
                                }
                            }
                            else
                            {
                                ReportIncludes += Pcode[0].ToString() + ", ";
                            }
                        }
                    }
                    ReportIncludes = ReportIncludes.Substring(0, ReportIncludes.Length - 2);

                    int? LocationId = ObjtblOrder.fkLocationId;
                    DateTime OrderedDate = ObjtblOrder.OrderDt;
                    if (ObjtblOrder.fkBatchId == null)
                        ObjtblOrder.fkBatchId = 0;



                    #region Update tblOrderSearchedData
                    ///If user change the selection of statecode for tiered report.
                    try
                    {
                        tblOrderSearchedData _objtblOrderSearchedData = new tblOrderSearchedData();
                        _objtblOrderSearchedData = ObjDALDataContext.tblOrderSearchedDatas.Where(d => d.pkOrderSearchedId == ObjtblOrderSearchedData.pkOrderSearchedId).FirstOrDefault();
                        if (_objtblOrderSearchedData != null)
                        {
                            if (DictStateCode.Count > 0)
                            {
                                var vSearchedStateId = ObjDALDataContext.tblStates.Where(d => d.StateCode == DictStateCode.First().state).Select(d => new { d.pkStateId }).FirstOrDefault();
                                if (vSearchedStateId != null)
                                {
                                    _objtblOrderSearchedData.SearchedStateId = vSearchedStateId.pkStateId;
                                    ObjDALDataContext.SubmitChanges();
                                    ObjtblOrderSearchedData.SearchedStateId = vSearchedStateId.pkStateId;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        VendorServices.GeneralService.WriteLog("UpdateOrders4Tiered : " + DateTime.Now.ToString(), "TieredPackageThread");
                        VendorServices.GeneralService.WriteLog("StackTrace : " + ex.StackTrace, "TieredPackageThread");
                        VendorServices.GeneralService.WriteLog("InnerException : " + ex.InnerException, "TieredPackageThread");


                        //Added log for exception handling.
                        VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateOrders4Tiered(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    }


                    #endregion


                    #region Insert in Order Detail Table

                    string Vendor_Request = "";
                    //string Vendor_Response = "";

                    /*Code To Fetch Whole Vendor and State At one Db call.*/

                    List<tblVendor> ObjtblVendor_List = ObjDALDataContext.tblVendors.ToList();
                    List<tblState> ObjtblState_List = ObjDALDataContext.tblStates.ToList();
                    List<tblCounty> ObjtblCounty_List = ObjDALDataContext.tblCounties.ToList();
                    BALProducts ObjBALProducts = new BALProducts();
                    BALGeneral ObjBALGeneral = new BALGeneral();
                    ObjProc_GetAllProductsWithVendorResult = ObjBALProducts.GetAllProductsWithVendor();
                    string SearchedStateCode = (!string.IsNullOrEmpty(ObjtblOrderSearchedData.SearchedStateId.ToString()) ? ObjtblState_List.Where(data => data.pkStateId == ObjtblOrderSearchedData.SearchedStateId).First().StateCode : "");
                    string SearchedDlStateCode = (!string.IsNullOrEmpty(ObjtblOrderSearchedData.SearchedDLStateId.ToString()) ? ObjtblState_List.Where(data => data.pkStateId == ObjtblOrderSearchedData.SearchedDLStateId).First().StateCode : "");
                    string SearchedCountyName = "";
                    string SearchedCountyState = "";
                    int SearchedCountyId = 0;
                    Dictionary<int?, string> objdic = new Dictionary<int?, string>();

                    /*
                    * This is barry : I have change all the productcode with PName variable,
                    * now PName contains the values of current product code. And i replace all 
                    * "ObjCollProducts.ElementAt(iProduct)" & "ObjCollProducts[iProduct]" with PName
                    */
                    List<string> reportCodes = new List<string>();
                    //List<string> PckIncColl = new List<string>();
                    string PckInc = string.Empty;
                    bool IsIfAnyConsentEmailOrFax = false;
                    string PDFAttachFilePath = string.Empty;
                    string StateCode = "";
                    for (int iProduct = 0; iProduct < ObjCollProducts.Count; iProduct++) /* This is for searched product  */
                    {

                        string PName = string.Empty;
                        bool IsLiveRunnerForEachState = false;
                        int fkProductApplicationId = 0;
                    #endregion

                        if (ObjCollProducts.ElementAt(iProduct).Contains("_"))/*This condition is used to split report code & package code*/
                        {
                            string[] Pcode = ObjCollProducts.ElementAt(iProduct).Split('_');
                            PName = Pcode[0].ToString();
                            reportCodes.Add(Pcode[0].ToString());
                            if (Pcode[1] != "0")
                            {
                                IsPackageReport = true;
                                fkPackageId = Convert.ToInt32(Pcode[1].ToString());
                                PckInc = string.Join(",", ObjCollProducts.Where(d => d.Contains(Pcode[1].ToString())).Select(d => d.ToString()).ToArray());
                                PckInc = PckInc.Replace("_" + Pcode[1].ToString(), "");
                                PackageInclude = PckInc; //;ReportIncludes                                
                            }
                            else
                            {
                                IsPackageReport = !true;
                                fkPackageId = 0;
                                PackageInclude = string.Empty;
                            }
                        }

                        using (EmergeDALDataContext dx = new EmergeDALDataContext())
                        {
                            fkProductApplicationId = dx.tblProductPerApplications.Where(d => d.ProductCode.ToLower() == PName).Select(d => d.pkProductApplicationId).FirstOrDefault();
                        }

                        if (PName == "FCR")
                        {
                            SearchedCountyName = ObjtblOrderSearchedData.search_county;
                        }

                        int? OrderDetailId = 0;
                        bool IsLiveRunner = false;
                        bool IsInstant = false;
                        bool IsConsentDB = false;
                        IsInstant = IsInstantProduct(PName);
                        #region Consent Region

                        CLSConsentInfo ObjCLSConsentInfo = new CLSConsentInfo();
                        if (ObjCollCLSConsentInfo != null)
                        {
                            if (ObjCollCLSConsentInfo.Count > 0)
                            {
                                if (ObjCollCLSConsentInfo.Any(d => d.IsConsentEmailOrFax == true))
                                {
                                    IsIfAnyConsentEmailOrFax = true;
                                }
                                if (ObjCollCLSConsentInfo.Any(d => d.ConsentReportType == PName))
                                {
                                    for (int iRow = 0; iRow < ObjCollCLSConsentInfo.Count; iRow++)
                                    {

                                        if (ObjCollCLSConsentInfo.ElementAt(iRow).ConsentReportType == PName && IsConsentDB == false)
                                        {
                                            IsConsentDB = true;
                                            ObjCLSConsentInfo.ConsentFileName = ObjCollCLSConsentInfo.ElementAt(iRow).ConsentFileName;
                                            ObjCLSConsentInfo.IsConsentEmailOrFax = ObjCollCLSConsentInfo.ElementAt(iRow).IsConsentEmailOrFax;
                                            ObjCLSConsentInfo.IsConsentRequired = ObjCollCLSConsentInfo.ElementAt(iRow).IsConsentRequired;
                                            ObjCLSConsentInfo.ConsentState = ObjCollCLSConsentInfo.ElementAt(iRow).ConsentState;
                                            PDFAttachFilePath = ObjCollCLSConsentInfo.ElementAt(iRow).PDFAttachFilePath;
                                        }
                                    }
                                }
                                else if (IsConsentDB == false)
                                {
                                    IsConsentDB = true;
                                    ObjCLSConsentInfo.ConsentFileName = string.Empty;
                                    ObjCLSConsentInfo.IsConsentEmailOrFax = false;
                                    ObjCLSConsentInfo.IsConsentRequired = false;
                                    ObjCLSConsentInfo.ConsentState = string.Empty;
                                }
                            }
                            else
                            {
                                ObjCLSConsentInfo.ConsentFileName = string.Empty;
                                ObjCLSConsentInfo.IsConsentEmailOrFax = false;
                                ObjCLSConsentInfo.IsConsentRequired = false;
                                ObjCLSConsentInfo.ConsentState = string.Empty;
                            }
                        }

                        #endregion

                        #region If Report name is SCR
                        if (PName == "SCR")
                        {
                            foreach (var item in DictStateCode)
                            {
                                //StateCode = item.Key;
                                StateCode = item.state;
                                using (EmergeDALDataContext dx = new EmergeDALDataContext())
                                {
                                    var vIsLivRun = dx.tblStates.Where(d => d.StateCode == StateCode).Select(d => new { d.IsStateLiveRunner }).FirstOrDefault();
                                    if (vIsLivRun != null)
                                    {
                                        IsLiveRunnerForEachState = vIsLivRun.IsStateLiveRunner;
                                    }
                                }
                                PlaceTieredSCR(ObjtblOrder, ObjCollProducts,
                                                 ObjtblOrderSearchedData,
                                                 ObjCollOrderSearchedEmploymentInfo,
                                                 ObjCollOrderSearchedEducationInfo,
                                                 ObjCollOrderSearchedPersonalInfo,
                                                 ObjCollOrderSearchedProfessionalInfo,
                                                 ObjCollOrderPersonalQtnInfo,
                                                 ObjCollOrderLicenseInfo,
                                                 ObjColltblOrderSearchedLiveRunnerInfo,
                                                 ObjCollCLSConsentInfo,
                                                 ObjCollOrderSearchedAliasPersonalInfo,
                                                 ObjCollOrderSearchedAliasLocationInfo,
                                                 ObjCollOrderSearchedAliasWorkInfo,
                                                 ObjtblVendor_List, ObjtblState_List, ObjtblCounty_List,
                                                 ObjProc_GetAllProductsWithVendorResult,
                                                 ApplicationId, IsNullDOB, StateCode, SearchedStateCode,
                                                 SearchedCountyName, IsLiveRunnerForEachState, SearchedCountyState, SearchedCountyId,
                                                 ObjDALDataContext, OrderId, PName, LocationId,
                                                 OrderedDate, IsInstant, IsPackageReport, PackageInclude,
                                                 fkPackageId, ObjCLSConsentInfo, objdic, DbTrans, CompanyAccountNo, OrderCompanyName, PermissiblePurpose, item.fName, item.mName, item.lName);
                            }
                            continue;
                        }
                        #endregion

                        #region If Report name is FCR
                        if (PName == "FCR")
                        {
                            foreach (var item in DicJurisdictionColl)
                            {
                                string SearchedCountyNameforfcr = item.Jurisdiction;
                                PlaceTieredFCR(ObjtblOrder, ObjCollProducts,
                                                 ObjtblOrderSearchedData,
                                                 ObjCollOrderSearchedEmploymentInfo,
                                                 ObjCollOrderSearchedEducationInfo,
                                                 ObjCollOrderSearchedPersonalInfo,
                                                 ObjCollOrderSearchedProfessionalInfo,
                                                 ObjCollOrderPersonalQtnInfo,
                                                 ObjCollOrderLicenseInfo,
                                                 ObjColltblOrderSearchedLiveRunnerInfo,
                                                 ObjCollCLSConsentInfo,
                                                 ObjCollOrderSearchedAliasPersonalInfo,
                                                 ObjCollOrderSearchedAliasLocationInfo,
                                                 ObjCollOrderSearchedAliasWorkInfo,
                                                 ObjtblVendor_List, ObjtblState_List, ObjtblCounty_List,
                                                 ObjProc_GetAllProductsWithVendorResult,
                                                 ApplicationId, IsNullDOB, StateCode, SearchedDlStateCode,
                                                 SearchedCountyNameforfcr, IsLiveRunnerForEachState, SearchedCountyState, SearchedCountyId,
                                                 ObjDALDataContext, OrderId, PName, LocationId,
                                                 OrderedDate, IsInstant, IsPackageReport, PackageInclude,
                                                 fkPackageId, ObjCLSConsentInfo, objdic, DbTrans, CompanyAccountNo, OrderCompanyName, PermissiblePurpose, item.fName, item.mName, item.lName);
                            }
                            continue;
                        }
                        #endregion
                        #region If Report name is NCR1
                        if (PName == "NCR1" && DictStateCode.Count == 0)
                        {
                            continue;
                        }
                        #endregion

                        #region Live Runner CCR's

                        if (PName == "CCR1" || PName == "CCR2" || PName == "RCX")/*ObjCollProducts.ElementAt(iProduct)*/
                        {
                            //int PkCompanyId = 0; ;
                            //int pkProductApplicationId = 0;
                            int grdRows = ObjColltblOrderSearchedLiveRunnerInfo.Count;
                            if (grdRows > 0)
                            {
                                for (int rowIndex = 0; rowIndex < grdRows; rowIndex++)
                                {
                                    SearchedStateCode = (!string.IsNullOrEmpty(ObjColltblOrderSearchedLiveRunnerInfo.ElementAt(rowIndex).LiveRunnerState.ToString()) ? ObjtblState_List.Where(data => data.pkStateId == Convert.ToInt32(ObjColltblOrderSearchedLiveRunnerInfo.ElementAt(rowIndex).LiveRunnerState)).First().StateCode : "");
                                    string[] CountyName_IsLR = ObjColltblOrderSearchedLiveRunnerInfo.ElementAt(rowIndex).LiveRunnerCounty.Split('_');
                                    SearchedCountyName = CountyName_IsLR[0];
                                    SearchedCountyState = ObjColltblOrderSearchedLiveRunnerInfo.ElementAt(rowIndex).LiveRunnerState;
                                    SearchedCountyId = ObjColltblOrderSearchedLiveRunnerInfo.ElementAt(rowIndex).SearchedCountyId;
                                    string fName = ObjColltblOrderSearchedLiveRunnerInfo.ElementAt(rowIndex).SearchedFirstName;
                                    string mName = ObjColltblOrderSearchedLiveRunnerInfo.ElementAt(rowIndex).SearchedMiddleName;
                                    string lName = ObjColltblOrderSearchedLiveRunnerInfo.ElementAt(rowIndex).SearchedLastName;
                                    IsLiveRunner = Convert.ToBoolean(CountyName_IsLR[1].ToLower());

                                    #region 805: liverunner activation issues
                                    if (IsLiveRunner && fkProductApplicationId != 0)
                                    {
                                        using (EmergeDALDataContext dx = new EmergeDALDataContext())//805: lliverunner activation issues
                                        {
                                            try
                                            {
                                                IsLiveRunner = dx.tblProductAccesses.Where(d => d.fkProductApplicationId == fkProductApplicationId && d.fkLocationId == LocationId).Select(d => d.IsLiveRunner).FirstOrDefault();
                                            }
                                            catch (Exception ex)
                                            {
                                                //Added log for exception handling.
                                                VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateOrders4Tiered(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                                            }
                                        }
                                    }
                                    #endregion

                                    List<Proc_AddEmergeOrderDetailForTieredResult> ObjAddOrderDetailResult1 = ObjDALDataContext.AddEmergeOrderDetailForTiered(
                                                                                                            OrderId,
                                                                                                            PName,
                                                                                                            ApplicationId,
                                                                                                            LocationId,
                                                                                                            OrderedDate,
                                                                                                            IsInstant,
                                                                                                            IsLiveRunner,
                                                                                                            IsPackageReport,
                                                                                                            PackageInclude,
                                                                                                            fkPackageId,
                                                                                                            (ObjCLSConsentInfo.ConsentFileName != null) ? ObjCLSConsentInfo.ConsentFileName : "",
                                                                                                        ObjCLSConsentInfo.IsConsentEmailOrFax,
                                                                                                        ObjCLSConsentInfo.IsConsentRequired, ObjCLSConsentInfo.ConsentState, SearchedCountyState, SearchedCountyName).ToList();



                                    if (ObjAddOrderDetailResult1.First().ReturnType > 0)
                                    {
                                        OrderDetailId = ObjAddOrderDetailResult1.First().OrderDetailId;
                                        IsLiveRunner = (bool)ObjAddOrderDetailResult1.First().IsLiveRunner;
                                        int iOrderSearchedLiveRunnerInfo = ObjDALDataContext.AddOrderSearchedLiverRunnerInfo(OrderId, OrderDetailId, SearchedCountyName, SearchedCountyState, SearchedCountyId);
                                        if (iOrderSearchedLiveRunnerInfo < 0)
                                        {
                                            OrderId = 0;
                                            if (DbTrans != null)
                                            {
                                                DbTrans.Rollback(); /* Rollback Transaction  */
                                            }
                                            return OrderId;
                                        }
                                    }

                                    if (OrderDetailId == 0) /*  If order detailed id is returned empty then data will be rollbacked  */
                                    {
                                        OrderId = 0;
                                        if (DbTrans != null)
                                        {
                                            DbTrans.Rollback(); /* Rollback Transaction */
                                        }
                                        return OrderId;
                                    }
                                    string ProductCodeSelected;
                                    if (IsLiveRunner)
                                    {
                                        ProductCodeSelected = "RCX";
                                    }
                                    else
                                        ProductCodeSelected = PName; /*ObjCollProducts.ElementAt(iProduct);*/

                                    Vendor_Request = CreateXMLRequest(ObjtblOrder, ProductCodeSelected, ObjtblOrderSearchedData,
                                                                                                    ObjCollOrderSearchedEmploymentInfo,
                                                                                                    ObjCollOrderSearchedEducationInfo,
                                                                                                    ObjCollOrderSearchedPersonalInfo,
                                                                                                    ObjCollOrderSearchedProfessionalInfo,
                                                                                                        ObjCollOrderPersonalQtnInfo,
                                                                                                        ObjCollOrderLicenseInfo,
                                                                                                        ObjCollOrderSearchedAliasPersonalInfo,
                                                                                                        ObjCollOrderSearchedAliasLocationInfo,
                                                                                                        ObjCollOrderSearchedAliasWorkInfo,
                                                                                                        ApplicationId,
                                                                                                        OrderId,
                                                                                                        OrderDetailId,
                                                                                                        ObjtblVendor_List,
                                                                                                        ObjtblState_List,
                                                                                                        SearchedStateCode,
                                                                                                        SearchedDlStateCode,
                                                                                                        SearchedCountyName,
                                                                                                        ObjProc_GetAllProductsWithVendorResult,
                                                                                                        IsNullDOB, CompanyAccountNo, OrderCompanyName, PermissiblePurpose, fName, lName, mName);
                                    int? Obj_pkOrderRequest1 = 0;
                                    int iResultRequest1 = ObjDALDataContext.AddOrderRequestData(OrderDetailId, Vendor_Request, ref Obj_pkOrderRequest1); /* Add entry in order request table */
                                    if (iResultRequest1 < 0)
                                    {
                                        OrderId = 0;
                                        if (DbTrans != null)
                                        {
                                            DbTrans.Rollback(); /* Rollback Transaction */
                                        }
                                        return OrderId;
                                    }


                                    string CocatRequest1 = ProductCodeSelected + "^" + Vendor_Request + "^" + Obj_pkOrderRequest1 + "^" + IsInstant;
                                    objdic.Add(OrderDetailId, CocatRequest1);
                                }
                            }
                            else
                            {

                            }
                            continue;
                        }

                        #endregion

                        #region If Report name is SCR and Live Runner Then report Set to RCX
                        string SelectedProduct = PName;
                        #endregion

                        #region Insert into tblorderdetails table for each report

                        List<Proc_AddEmergeOrderDetailForTieredResult> ObjAddOrderDetailResult = ObjDALDataContext.AddEmergeOrderDetailForTiered(
                                                                                                                         OrderId,
                                                                                                                         PName,
                                                                                                                         ApplicationId,
                                                                                                                         LocationId,
                                                                                                                         OrderedDate,
                                                                                                                         IsInstant,
                                                                                                                         IsLiveRunner,
                                                                                                                         IsPackageReport,
                                                                                                                         PackageInclude,
                                                                                                                         fkPackageId,
                                                                                                                         (ObjCLSConsentInfo.ConsentFileName != null) ? ObjCLSConsentInfo.ConsentFileName : "",
                                                                                                                         ObjCLSConsentInfo.IsConsentEmailOrFax,
                                                                                                                         ObjCLSConsentInfo.IsConsentRequired, ObjCLSConsentInfo.ConsentState, string.Empty, string.Empty).ToList();/*ObjCollProducts[iProduct]*/



                        if (ObjAddOrderDetailResult.First().ReturnType > 0)
                        {
                            OrderDetailId = ObjAddOrderDetailResult.First().OrderDetailId;
                            IsLiveRunner = (bool)ObjAddOrderDetailResult.First().IsLiveRunner;
                        }

                        if (OrderDetailId == 0) /*  If order detailed id is returned empty then data will be rollbacked  */
                        {
                            OrderId = 0;
                            if (DbTrans != null)
                            {
                                DbTrans.Rollback(); /* Rollback Transaction */
                            }
                            return OrderId;
                        }

                        #endregion

                        #region Generate Vendor Request and store into tblorderRequestData
                        //  Create the Request in First Phase
                        Vendor_Request = CreateXMLRequest(ObjtblOrder, SelectedProduct, ObjtblOrderSearchedData,
                        ObjCollOrderSearchedEmploymentInfo, ObjCollOrderSearchedEducationInfo, ObjCollOrderSearchedPersonalInfo,
                        ObjCollOrderSearchedProfessionalInfo, ObjCollOrderPersonalQtnInfo, ObjCollOrderLicenseInfo, ObjCollOrderSearchedAliasPersonalInfo, ObjCollOrderSearchedAliasLocationInfo,
                          ObjCollOrderSearchedAliasWorkInfo,
                            ApplicationId, OrderId, OrderDetailId, ObjtblVendor_List, ObjtblState_List, SearchedStateCode, SearchedDlStateCode,
                            SearchedCountyName, ObjProc_GetAllProductsWithVendorResult, IsNullDOB, CompanyAccountNo, OrderCompanyName, PermissiblePurpose);/*ObjCollProducts.ElementAt(iProduct)*/

                        //  Mcall method to create the XML String .
                        int? Obj_pkOrderRequest = 0;
                        int iResultRequest = ObjDALDataContext.AddOrderRequestData(OrderDetailId, Vendor_Request, ref Obj_pkOrderRequest); /* Add entry in order request table */
                        if (iResultRequest < 0)
                        {
                            OrderId = 0;
                            if (DbTrans != null)
                            {
                                DbTrans.Rollback(); /* Rollback Transaction */
                            }
                            return OrderId;
                        }

                        #endregion

                        string CocatRequest = SelectedProduct + "^" + Vendor_Request + "^" + Obj_pkOrderRequest + "^" + IsInstant;
                        objdic.Add(OrderDetailId, CocatRequest);/*ObjCollProducts.ElementAt(iProduct)*/

                    }
                    #region Vendor Call

                    //string strmail = "";
                    string vendor_Res = "";
                    string Vendor_Req = "";
                    bool IsMVR = false;
                    foreach (KeyValuePair<int?, string> data in objdic)
                    {

                        string[] concatRequest = data.Value.Split('^');
                        string product_Code = concatRequest[0];
                        string vendor_Req = concatRequest[1];
                        int pkOrderRequestId = Convert.ToInt32(concatRequest[2]);
                        bool IsInstantReport = Convert.ToBoolean(concatRequest[3]);

                        #region commented Acc. to new changes of #103 ticket

                        vendor_Res = GetXMLResponse(product_Code, ObjProc_GetAllProductsWithVendorResult, vendor_Req, OrderId, data.Key, IsInstantReport);

                        #endregion

                        if (vendor_Res.Contains("<XML_INTERFACE>ZZZ</XML_INTERFACE>"))
                        {
                            string[] Vendor_Response_Parts = vendor_Res.Split('#');
                            vendor_Res = Vendor_Response_Parts[1].ToString();
                            Vendor_Req = Vendor_Response_Parts[0].ToString();
                            UpdateRequest(pkOrderRequestId, Vendor_Req);
                        }

                        //strmail += vendor_Res + "---------------------";

                        Guid? pkresponseid = new Guid();

                        using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                        {
                            Dx.AddOrderResponse(data.Key, vendor_Res);
                        }
                        if (ObjtblOrderSearchedData.SearchedDLStateId == 47 && product_Code.ToLower() == "mvr")// 47 Pennsylvania	PA
                        {
                            IsMVR = true;
                            CheckResponseandUpdateStatus(product_Code, OrderId, vendor_Res, data.Key, IsInstantReport, CompanyAccountNo, OrderCompanyName, PermissiblePurpose, false);
                            UpdateReportStatus(OrderId, data.Key, Convert.ToByte("1"));

                        }
                        else
                        {
                            CheckResponseandUpdateStatus(product_Code, OrderId, vendor_Res, data.Key, IsInstantReport, CompanyAccountNo, OrderCompanyName, PermissiblePurpose, false);
                        }

                        vendor_Res = "";
                        Vendor_Req = "";

                    }

                    #endregion

                    #region Sending  E-Mail's

                    // code to Send an email For Testing Purpose .
                    BALCompany ObjCom = new BALCompany();
                    ObjBALGeneral = new BALGeneral();
                    int PkTemplateId = 29;
                    string emailText = "";
                    string Subject = "";
                    string ApplicantName = "";
                    string ReportCodes = "";
                    string UserEmail = "";
                    //string MailToAddress = "";
                    int? fkCompanyUserId = ObjtblOrder.fkCompanyUserId;
                    var INFO = ObjCom.GetUserAndCompanyDetails(fkCompanyUserId).ToList();

                    var emailContent = ObjBALGeneral.GetEmailTemplate(ApplicationId, PkTemplateId);

                    #region For New Style SystemTemplate
                    string Bookmarkfile = string.Empty;
                    //string bookmarkfilepath = string.Empty;
                    BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                    Bookmarkfile = objTemplateBookmarks.getBookMarkFile("~/Resources/Templates/BookMarkTemplate.htm");
                    Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                    Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                    emailText = Bookmarkfile;
                    #endregion

                    emailText = emailContent.TemplateContent.ToString();
                    Subject = emailContent.TemplateSubject;
                    if (reportCodes.Count > 1)
                    {
                        for (int i = 0; i < reportCodes.Count; i++)
                        {
                            ReportCodes += reportCodes.ElementAt(i).ToString();

                            if (i != reportCodes.Count - 1)
                            {
                                ReportCodes += ", ";
                            }
                        }
                    }
                    else
                    {
                        ReportCodes = reportCodes.ElementAt(0).ToString();
                    }
                    ApplicantName = ObjtblOrderSearchedData.SearchedFirstName + " " + ObjtblOrderSearchedData.SearchedLastName;
                    if (INFO.Count > 0)
                    {
                        UserEmail = INFO.ElementAt(0).Email;
                        //MailToAddress = INFO.ElementAt(0).ReportEmailAddress;
                        if (INFO.ElementAt(0).IsReportEmailActivated_Location == true)
                        {
                            //Do nothing.
                        }
                    }
                    if (IsMVR)
                    {
                        var emailContent_Mvr = ObjBALGeneral.GetEmailTemplate(ApplicationId, 36);
                        emailText = emailContent_Mvr.TemplateContent.ToString();
                        Subject = emailContent_Mvr.TemplateSubject.Replace("%OrderNumber%", ObjtblOrder.OrderNo).Replace("%ApplicantName%", ApplicantName);
                        SendEmailToAdminForMVR(ObjtblOrder.OrderNo, UserEmail, ApplicantName, ObjtblOrderSearchedData.SearchedDob, ObjtblOrderSearchedData.SearchedSsn, ObjtblOrderSearchedData.SearchedDriverLicense, "MVR", emailText, Subject);
                    }
                    if (IsIfAnyConsentEmailOrFax == true)
                    {
                        var emailContent_CONSENT = ObjBALGeneral.GetEmailTemplate(ApplicationId, 39);           // PKTEMPLATEID = 39 for Consent Form
                        emailText = emailContent_CONSENT.TemplateContent.ToString();
                        Subject = emailContent_CONSENT.TemplateSubject.Replace("%ApplicantName%", ApplicantName);
                    }
                    #endregion

                }
                catch (Exception ex)
                {
                    #region Transaction Rollback

                    OrderId = 0;
                    VendorServices.GeneralService.WriteLog("StackTrace : " + ex.StackTrace, "TieredPackageThread");
                    VendorServices.GeneralService.WriteLog("InnerException : " + ex.InnerException, "TieredPackageThread");
                    #endregion
                    //Added log for exception handling.
                    VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateOrders4Tiered(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                }
                finally
                {
                    #region Connection and Transaction Close

                    // ObjDALDataContext.Transaction = null;
                    if (ObjDALDataContext.Connection.State == ConnectionState.Open)
                    {
                        ObjDALDataContext.Connection.Close(); /* Close Connection */
                    }

                    #endregion
                }

                #region Return OrderId

                return OrderId; /* Here we return successfully inserted order id */

                #endregion
            }
        }


        #endregion

        #region 2ticket#440
        public int PlaceTieredFCR(tblOrder ObjtblOrder, List<string> ObjCollProducts,
                    tblOrderSearchedData ObjtblOrderSearchedData,
                    List<tblOrderSearchedEmploymentInfo> ObjCollOrderSearchedEmploymentInfo,
                    List<tblOrderSearchedEducationInfo> ObjCollOrderSearchedEducationInfo,
                    List<tblOrderSearchedPersonalInfo> ObjCollOrderSearchedPersonalInfo,
                            List<tblOrderSearchedProfessionalInfo> ObjCollOrderSearchedProfessionalInfo,
                    List<tblOrderPersonalQtnInfo> ObjCollOrderPersonalQtnInfo,
                    List<tblOrderSearchedLicenseInfo> ObjCollOrderLicenseInfo,
                    List<tblOrderSearchedLiveRunnerInfo> ObjColltblOrderSearchedLiveRunnerInfo,
                    List<CLSConsentInfo> ObjCollCLSConsentInfo,
                    List<tblOrderSearchedAliasPersonalInfo> ObjCollOrderSearchedAliasPersonalInfo,
                    List<tblOrderSearchedAliasLocationInfo> ObjCollOrderSearchedAliasLocationInfo,
                    List<tblOrderSearchedAliasWorkInfo> ObjCollOrderSearchedAliasWorkInfo,
                    List<tblVendor> ObjtblVendor_List, List<tblState> ObjtblState_List, List<tblCounty> ObjtblCounty_List,
                    List<Proc_GetAllProductsWithVendorResult> ObjProc_GetAllProductsWithVendorResult,
                    Guid ApplicationId, int IsNullDOB, string SearchedStateCode, string SearchedDlStateCode,
                    string SearchedCountyName, bool IsLiveRunner, string SearchedCountyState, int SearchedCountyId,
                    EmergeDALDataContext ObjDALDataContext, int OrderId, string PName, int? LocationId,
                    DateTime OrderedDate, bool IsInstant, bool IsPackageReport, string PackageInclude,
                    int fkPackageId, CLSConsentInfo ObjCLSConsentInfo, Dictionary<int?, string> objdic, DbTransaction DbTrans, string CompanyAccountNo, string OrderCompanyName, string PermissiblePurpose,
                     string fName = "", string mName = "", string lName = "")
        {
            //int PkCompanyId = 0;
            //int pkProductApplicationId = 0;
            int? OrderDetailId = 0;
            try
            {

                List<Proc_AddEmergeOrderDetailForTieredResult> ObjAddOrderDetailResult1 = ObjDALDataContext.AddEmergeOrderDetailForTiered(
                                                                                        OrderId,
                                                                                        PName,
                                                                                        ApplicationId,
                                                                                        LocationId,
                                                                                        OrderedDate,
                                                                                        IsInstant,
                                                                                        IsLiveRunner,
                                                                                        IsPackageReport,
                                                                                        PackageInclude,
                                                                                        fkPackageId,
                                                                                        (ObjCLSConsentInfo.ConsentFileName != null) ? ObjCLSConsentInfo.ConsentFileName : "",
                                                                                    ObjCLSConsentInfo.IsConsentEmailOrFax,
                                                                                    ObjCLSConsentInfo.IsConsentRequired, ObjCLSConsentInfo.ConsentState, SearchedStateCode, SearchedCountyName).ToList();



                if (ObjAddOrderDetailResult1.First().ReturnType > 0)
                {
                    OrderDetailId = ObjAddOrderDetailResult1.First().OrderDetailId;
                    IsLiveRunner = (bool)ObjAddOrderDetailResult1.First().IsLiveRunner;
                    int iOrderSearchedLiveRunnerInfo = ObjDALDataContext.AddOrderSearchedLiverRunnerInfo(OrderId, OrderDetailId, SearchedCountyName, SearchedCountyState, SearchedCountyId);
                    ObjDALDataContext.SetCCRFeeInOderDetailsTable(OrderDetailId, PName);/*This set the CCR report fees in the Order Detail Table*/
                    if (iOrderSearchedLiveRunnerInfo < 0)
                    {
                        OrderId = 0;
                        if (DbTrans != null)
                        {
                            DbTrans.Rollback(); /* Rollback Transaction  */
                        }
                        return OrderId;
                    }
                }

                if (OrderDetailId == 0) /*  If order detailed id is returned empty then data will be rollbacked  */
                {
                    OrderId = 0;
                    if (DbTrans != null)
                    {
                        DbTrans.Rollback(); /* Rollback Transaction */
                    }
                    return OrderId;
                }
                string ProductCodeSelected;
                if (IsLiveRunner)
                {
                    ProductCodeSelected = "RCX";
                }
                else
                    ProductCodeSelected = PName; /*ObjCollProducts.ElementAt(iProduct);*/

                string Vendor_Request = CreateXMLRequest(ObjtblOrder, ProductCodeSelected, ObjtblOrderSearchedData,
                                                                                ObjCollOrderSearchedEmploymentInfo,
                                                                                ObjCollOrderSearchedEducationInfo,
                                                                                ObjCollOrderSearchedPersonalInfo,
                                                                                ObjCollOrderSearchedProfessionalInfo,
                                                                                    ObjCollOrderPersonalQtnInfo,
                                                                                    ObjCollOrderLicenseInfo,
                                                                                    ObjCollOrderSearchedAliasPersonalInfo,
                                                                                    ObjCollOrderSearchedAliasLocationInfo,
                                                                                    ObjCollOrderSearchedAliasWorkInfo,
                                                                                    ApplicationId,
                                                                                    OrderId,
                                                                                    OrderDetailId,
                                                                                    ObjtblVendor_List,
                                                                                    ObjtblState_List,
                                                                                    SearchedStateCode,
                                                                                    SearchedDlStateCode,
                                                                                    SearchedCountyName,
                                                                                    ObjProc_GetAllProductsWithVendorResult,
                                                                                    IsNullDOB, CompanyAccountNo, OrderCompanyName, PermissiblePurpose, fName, lName, mName);
                int? Obj_pkOrderRequest1 = 0;
                int iResultRequest1 = ObjDALDataContext.AddOrderRequestData(OrderDetailId, Vendor_Request, ref Obj_pkOrderRequest1); /* Add entry in order request table */
                if (iResultRequest1 < 0)
                {
                    OrderId = 0;
                    if (DbTrans != null)
                    {
                        DbTrans.Rollback(); /* Rollback Transaction */
                    }
                    return OrderId;
                }


                string CocatRequest1 = ProductCodeSelected + "^" + Vendor_Request + "^" + Obj_pkOrderRequest1 + "^" + IsInstant;
                objdic.Add(OrderDetailId, CocatRequest1);
            }
            catch (Exception ex)
            {
                OrderId = 0;

                if (DbTrans != null)
                {
                    DbTrans.Rollback(); /* Rollback Transaction */
                }

                VendorServices.GeneralService.WriteLog("StackTrace : " + ex.StackTrace, "TieredPackageThread");
                VendorServices.GeneralService.WriteLog("InnerException : " + ex.InnerException, "TieredPackageThread");


                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in PlaceTieredFCR(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

            }
            return OrderId;
        }


        #endregion

        public int PlaceTieredSCR(tblOrder ObjtblOrder, List<string> ObjCollProducts,
                     tblOrderSearchedData ObjtblOrderSearchedData,
                     List<tblOrderSearchedEmploymentInfo> ObjCollOrderSearchedEmploymentInfo,
                     List<tblOrderSearchedEducationInfo> ObjCollOrderSearchedEducationInfo,
                     List<tblOrderSearchedPersonalInfo> ObjCollOrderSearchedPersonalInfo,
            List<tblOrderSearchedProfessionalInfo> ObjCollOrderSearchedProfessionalInfo,
                     List<tblOrderPersonalQtnInfo> ObjCollOrderPersonalQtnInfo,
                     List<tblOrderSearchedLicenseInfo> ObjCollOrderLicenseInfo,
                     List<tblOrderSearchedLiveRunnerInfo> ObjColltblOrderSearchedLiveRunnerInfo,
                     List<CLSConsentInfo> ObjCollCLSConsentInfo,
                     List<tblOrderSearchedAliasPersonalInfo> ObjCollOrderSearchedAliasPersonalInfo,
                     List<tblOrderSearchedAliasLocationInfo> ObjCollOrderSearchedAliasLocationInfo,
                     List<tblOrderSearchedAliasWorkInfo> ObjCollOrderSearchedAliasWorkInfo,
                     List<tblVendor> ObjtblVendor_List, List<tblState> ObjtblState_List, List<tblCounty> ObjtblCounty_List,
                     List<Proc_GetAllProductsWithVendorResult> ObjProc_GetAllProductsWithVendorResult,
                     Guid ApplicationId, int IsNullDOB, string SearchedStateCode, string SearchedDlStateCode,
                     string SearchedCountyName, bool IsLiveRunner, string SearchedCountyState, int SearchedCountyId,
                     EmergeDALDataContext ObjDALDataContext, int OrderId, string PName, int? LocationId,
                     DateTime OrderedDate, bool IsInstant, bool IsPackageReport, string PackageInclude,
                     int fkPackageId, CLSConsentInfo ObjCLSConsentInfo, Dictionary<int?, string> objdic, DbTransaction DbTrans, string CompanyAccountNo, string OrderCompanyName, string PermissiblePurpose,
                      string fName = "", string mName = "", string lName = "")
        {
            //int PkCompanyId = 0; ;
            //int pkProductApplicationId = 0;
            int? OrderDetailId = 0;

            List<Proc_AddEmergeOrderDetailForTieredResult> ObjAddOrderDetailResult1 = ObjDALDataContext.AddEmergeOrderDetailForTiered(
                                                                                    OrderId,
                                                                                    PName,
                                                                                    ApplicationId,
                                                                                    LocationId,
                                                                                    OrderedDate,
                                                                                    IsInstant,
                                                                                    IsLiveRunner,
                                                                                    IsPackageReport,
                                                                                    PackageInclude,
                                                                                    fkPackageId,
                                                                                    (ObjCLSConsentInfo.ConsentFileName != null) ? ObjCLSConsentInfo.ConsentFileName : "",
                                                                                ObjCLSConsentInfo.IsConsentEmailOrFax,
                                                                                ObjCLSConsentInfo.IsConsentRequired, ObjCLSConsentInfo.ConsentState, SearchedStateCode, string.Empty).ToList();



            if (ObjAddOrderDetailResult1.First().ReturnType > 0)
            {
                OrderDetailId = ObjAddOrderDetailResult1.First().OrderDetailId;
                IsLiveRunner = (bool)ObjAddOrderDetailResult1.First().IsLiveRunner;
                int iOrderSearchedLiveRunnerInfo = ObjDALDataContext.AddOrderSearchedLiverRunnerInfo(OrderId, OrderDetailId, SearchedCountyName, SearchedCountyState, SearchedCountyId);
                ObjDALDataContext.SetCCRFeeInOderDetailsTable(OrderDetailId, PName);/*This set the CCR report fees in the Order Detail Table*/
                //ObjDALDataContext.AddOrderDetailId(OrderDetailId, OrderId);
                if (iOrderSearchedLiveRunnerInfo < 0)
                {
                    OrderId = 0;
                    if (DbTrans != null)
                    {
                        DbTrans.Rollback(); /* Rollback Transaction  */
                    }
                    return OrderId;
                }
            }

            if (OrderDetailId == 0) /*  If order detailed id is returned empty then data will be rollbacked  */
            {
                OrderId = 0;
                if (DbTrans != null)
                {
                    DbTrans.Rollback(); /* Rollback Transaction */
                }
                return OrderId;
            }
            string ProductCodeSelected;
            if (IsLiveRunner)
            {
                ProductCodeSelected = "RCX";
            }
            else
                ProductCodeSelected = PName; /*ObjCollProducts.ElementAt(iProduct);*/

            string Vendor_Request = CreateXMLRequest(ObjtblOrder, ProductCodeSelected, ObjtblOrderSearchedData,
                                                                            ObjCollOrderSearchedEmploymentInfo,
                                                                            ObjCollOrderSearchedEducationInfo,
                                                                            ObjCollOrderSearchedPersonalInfo,
                                                                            ObjCollOrderSearchedProfessionalInfo,
                                                                                ObjCollOrderPersonalQtnInfo,
                                                                                ObjCollOrderLicenseInfo,
                                                                                ObjCollOrderSearchedAliasPersonalInfo,
                                                                                ObjCollOrderSearchedAliasLocationInfo,
                                                                                ObjCollOrderSearchedAliasWorkInfo,
                                                                                ApplicationId,
                                                                                OrderId,
                                                                                OrderDetailId,
                                                                                ObjtblVendor_List,
                                                                                ObjtblState_List,
                                                                                SearchedStateCode,
                                                                                SearchedDlStateCode,
                                                                                SearchedCountyName,
                                                                                ObjProc_GetAllProductsWithVendorResult,
                                                                                IsNullDOB, CompanyAccountNo, OrderCompanyName, PermissiblePurpose, fName, lName, mName);
            int? Obj_pkOrderRequest1 = 0;
            int iResultRequest1 = ObjDALDataContext.AddOrderRequestData(OrderDetailId, Vendor_Request, ref Obj_pkOrderRequest1); /* Add entry in order request table */
            if (iResultRequest1 < 0)
            {
                OrderId = 0;
                if (DbTrans != null)
                {
                    DbTrans.Rollback(); /* Rollback Transaction */
                }
                return OrderId;
            }


            string CocatRequest1 = ProductCodeSelected + "^" + Vendor_Request + "^" + Obj_pkOrderRequest1 + "^" + IsInstant;
            objdic.Add(OrderDetailId, CocatRequest1);
            return OrderId;
        }


        /// <summary>
        /// To update the submission order date of selected order that has created from the landing page.
        /// As per INT-248 ramining items (point 2) Billing/Order Date Issue – when the client clicks the “Submit” button from within Archives, 
        /// the reports submitted must keep the order ID but update the order DATE to the current date so it reflects properly on billing
        /// </summary>
        /// <param name="pkOrderId"></param>
        public void updateOrderDtForLandingPageOrders(int pkOrderId, int CompanyUserId)
        {
            try
            {

                using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                {
                    var order = Dx.tblOrders.Where(x => x.pkOrderId == pkOrderId).FirstOrDefault();
                    if (order != null)
                    {
                        if (order.OrderType == 2)//Created from the Landing page.
                        {
                            order.OrderDt = DateTime.Now;//To set current datetime.
                            order.fkCompanyUserId = CompanyUserId;//Set submitted user id.
                            order.CreatedById = CompanyUserId;//Set submitted user id.
                            var orderDetail = Dx.tblOrderDetails.Where(x => x.fkOrderId == pkOrderId).ToList();
                            if (orderDetail != null)
                            {
                                foreach (tblOrderDetail oDetail in orderDetail)
                                {
                                    oDetail.OrderDt = DateTime.Now;//To set current datetime.
                                }
                            }
                            Dx.SubmitChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                VendorServices.GeneralService.WriteLog("Order date updation for landing page submission of '" + Convert.ToString(pkOrderId) + "'\n" + ex.InnerException + "\n" + ex.StackTrace, "TieredPackageThread");
            }


        }


        public void SendEmailToAdminForMVR(string OrderNumber, string UserName, string ApplicantName, string dob, string ssn, string DLno, string ReportCode, string emailText, string Subject)
        {
            string MessageBody = "";
            StringBuilder Comments = new StringBuilder();
            try
            {
                string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/emerge-logo--no-shadows-(small).gif";
                BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                Bookmarks objBookmark = new Bookmarks();
                string MailToAddress = ConfigurationManager.AppSettings["BccAdmin"].ToString();
                objBookmark.EmailContent = emailText;

                objBookmark.Subject = Subject;
                objBookmark.ApplicantName = ApplicantName;
                objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                objBookmark.OrderNumber = OrderNumber;
                objBookmark.ReportCode = ReportCode;
                objBookmark.UserEmail = UserName;
                objBookmark.DOB = dob;
                objBookmark.SSN = ssn;
                objBookmark.DLNumber = DLno;
                MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Guid.Empty);
                string MailFrom = ConfigurationManager.AppSettings["ReportReviewFromAddress"].ToString();
                string success = BALEmergeReview.SendMail(MailToAddress, string.Empty, string.Empty, MailFrom, MessageBody, Subject);
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in SendEmailToAdminForMVR(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
        }

        public void SendEmailToAdminForSCR(string OrderNumber, string UserName, string ApplicantName, string dob, string ssn, string ReportCode, string emailText, string Subject)
        {
            string MessageBody = "";
            //StringBuilder Comments = new StringBuilder();
            try
            {
                string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/emerge-logo--no-shadows-(small).gif";
                BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                Bookmarks objBookmark = new Bookmarks();
                string MailToAddress = ConfigurationManager.AppSettings["BccAdmin"].ToString();
                objBookmark.EmailContent = emailText;

                objBookmark.Subject = Subject;
                objBookmark.ApplicantName = ApplicantName;
                objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                objBookmark.OrderNumber = OrderNumber;
                objBookmark.ReportCode = ReportCode;
                objBookmark.UserEmail = UserName;
                objBookmark.DOB = dob;
                objBookmark.SSN = ssn;
                MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Guid.Empty);
                string MailFrom = ConfigurationManager.AppSettings["ReportReviewFromAddress"].ToString();
                string success = BALEmergeReview.SendMail(MailToAddress, string.Empty, string.Empty, MailFrom, MessageBody, Subject);
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in SendEmailToAdminForSCR(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
        }

        // INT 174 MAY 04 MAY 2015 WORK AS MVR REPORT START 


        public void SendEmailToAdminForEEI(string OrderNumber, string UserName, string ApplicantName, string dob, string ssn, string ReportCode, string emailText, string Subject)
        {
            string MessageBody = "";
            //StringBuilder Comments = new StringBuilder();
            try
            {
                string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/emerge-logo--no-shadows-(small).gif";
                BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                Bookmarks objBookmark = new Bookmarks();
                string MailToAddress = ConfigurationManager.AppSettings["BccAdmin"].ToString();
                objBookmark.EmailContent = emailText;

                objBookmark.Subject = Subject;
                objBookmark.ApplicantName = ApplicantName;
                objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                objBookmark.OrderNumber = OrderNumber;
                objBookmark.ReportCode = ReportCode;
                objBookmark.UserEmail = UserName;
                objBookmark.DOB = dob;
                objBookmark.SSN = ssn;
                MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Guid.Empty);
                string MailFrom = ConfigurationManager.AppSettings["ReportReviewFromAddress"].ToString();
                string success = BALEmergeReview.SendMail(MailToAddress, string.Empty, string.Empty, MailFrom, MessageBody, Subject);
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in SendEmailToAdminForEEI(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
        }

        public void SendEmailToAdminForECR(string OrderNumber, string UserName, string ApplicantName, string dob, string ssn, string ReportCode, string emailText, string Subject)
        {
            string MessageBody = "";
            //StringBuilder Comments = new StringBuilder();
            try
            {
                string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/emerge-logo--no-shadows-(small).gif";
                BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                Bookmarks objBookmark = new Bookmarks();
                string MailToAddress = ConfigurationManager.AppSettings["BccAdmin"].ToString();
                objBookmark.EmailContent = emailText;

                objBookmark.Subject = Subject;
                objBookmark.ApplicantName = ApplicantName;
                objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                objBookmark.OrderNumber = OrderNumber;
                objBookmark.ReportCode = ReportCode;
                objBookmark.UserEmail = UserName;
                objBookmark.DOB = dob;
                objBookmark.SSN = ssn;
                MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Guid.Empty);
                string MailFrom = ConfigurationManager.AppSettings["ReportReviewFromAddress"].ToString();
                //string success = 
                BALEmergeReview.SendMail(MailToAddress, string.Empty, string.Empty, MailFrom, MessageBody, Subject);
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in SendEmailToAdminForECR(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
        }
        public bool IsValidXML(string XML)
        {
            try
            {
                XmlDocument ObjXmlDocument = new XmlDocument();
                ObjXmlDocument.LoadXml(XML);
                return true;
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in IsValidXML(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                return false;
            }
        }


        public void CheckResponseandUpdateStatus(string ObjCollProducts, int? OrderId, string Vendor_Response, int? OrderDetailId, bool IsInstantReport, string CompanyAccountNo, string OrderCompanyName, string PermissiblePurpose, bool Is7YearFilterForCriminal)
        {
            try
            {
                using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                {
                    if (IsValidXML(Vendor_Response) == true)
                    {
                        #region Best Address Saved In DB
                        try
                        {
                            if (ObjCollProducts == "NCR1")
                            {
                                XDocument ObjXDocument = new XDocument();
                                tblOrderDetail GetResponseforPS = new tblOrderDetail();
                                //Guid PkProductApplicationId = Guid.Parse("197DA3FC-FAFD-455C-A60A-8F1901A530FD");
                                int PkProductApplicationId = 0;
                                GetResponseforPS = Dx.tblOrderDetails.Where(rec => rec.fkOrderId == OrderId && rec.fkProductApplicationId == PkProductApplicationId).FirstOrDefault();
                                if (GetResponseforPS != null)
                                {
                                    int pkOrderDetailId = GetResponseforPS.pkOrderDetailId;
                                    tblOrderResponseData ObjResponseData = new tblOrderResponseData();
                                    ObjResponseData = Dx.tblOrderResponseDatas.Where(rec => rec.fkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                                    tblOrderSearchedData ObjtblOrderSearchedData = new tblOrderSearchedData();
                                    //XDocument ObjXmlDoc = new XDocument();
                                    string XML_Value = ObjResponseData.ResponseData;
                                    XML_Value = BALEmergeReview.GetRequiredXmlConditional(XML_Value, "PS");
                                    ObjXDocument = XDocument.Parse(XML_Value);
                                    var lstScreenings = ObjXDocument.Descendants("records").ToList();
                                    if (lstScreenings.Count > 0)
                                    {
                                       // List<XElement> CreditFile = lstScreenings.Descendants("records").ToList();
                                        //if (CreditFile != null && CreditFile.Count > 0)
                                        //{
                                        List<XElement> PersonalData = lstScreenings.Descendants("record").ToList();

                                            if (PersonalData != null && PersonalData.Count > 0)
                                            {
                                                bool IsAddressUptd = false;
                                                XElement Addresses = PersonalData.Descendants("addresses").FirstOrDefault();
                                                XElement PostalAddress = Addresses.Descendants("address").FirstOrDefault();
                                                if (PostalAddress != null)
                                                {
                                                    // string pkStateId = string.Empty;
                                                    string pkStateId1 = string.Empty;
                                                    XElement Region = PersonalData.Descendants("state").FirstOrDefault();
                                                    if (Region != null)
                                                    {
                                                        pkStateId1 = Region.Value.ToString();
                                                    }
                                                    //string strCounty = string.Empty;
                                                    string strCounty1 = string.Empty;
                                                    XElement County = PersonalData.Descendants("county").FirstOrDefault();
                                                    if (County != null)
                                                    {
                                                        strCounty1 = County.Value.ToString();
                                                    }

                                                    /*string strPersonName = string.Empty;
                                                    XElement PersonName = PersonalData.Descendants("PersonName").FirstOrDefault();
                                                    if (PersonName != null)
                                                    {
                                                        strPersonName = PersonName.Value.ToString();
                                                    }*/


                                                    XElement AddressLine = PersonalData.Descendants("fullstreet").FirstOrDefault();
                                                    string strAddressLine = string.Empty;
                                                    if (AddressLine != null)
                                                    {
                                                        strAddressLine = AddressLine.Value.ToString();
                                                    }

                                                    XElement StreetName = PersonalData.Descendants("street-name").FirstOrDefault();

                                                    string strStreetName = string.Empty;
                                                    if (StreetName != null)
                                                    {
                                                        strStreetName = StreetName.Value.ToString();
                                                    }
                                                    XElement PostalCode = PersonalData.Descendants("zip").FirstOrDefault();
                                                    string strPostalCode = string.Empty;
                                                    if (PostalCode != null)
                                                    {
                                                        strPostalCode = PostalCode.Value.ToString();
                                                        if (strPostalCode.Length > 5)
                                                        {
                                                            strPostalCode = strPostalCode.Substring(0, 5);
                                                        }
                                                    }

                                                    XElement Municipality = PersonalData.Descendants("city").FirstOrDefault();

                                                    string strMunicipality = string.Empty;
                                                    if (Municipality != null)
                                                    {
                                                        strMunicipality = Municipality.Value.ToString();
                                                    }
                                                    ObjtblOrderSearchedData = Dx.tblOrderSearchedDatas.Where(rec => rec.fkOrderId == OrderId).FirstOrDefault();
                                                    //tblOrderSearchedData ObjtblOrderSearchedDataVal = new tblOrderSearchedData();
                                                    if (ObjtblOrderSearchedData.search_county == null || ObjtblOrderSearchedData.search_county == string.Empty || ObjtblOrderSearchedData.search_county == "")
                                                    {
                                                        ObjtblOrderSearchedData.best_search_county = strCounty1;
                                                        IsAddressUptd = true;
                                                    }
                                                    if (ObjtblOrderSearchedData.search_state == null || ObjtblOrderSearchedData.search_state == string.Empty || ObjtblOrderSearchedData.search_state == "")
                                                    {
                                                        ObjtblOrderSearchedData.best_search_state = pkStateId1;
                                                        IsAddressUptd = true;
                                                    }
                                                    if (ObjtblOrderSearchedData.SearchedStreetAddress == null || ObjtblOrderSearchedData.SearchedStreetAddress == string.Empty || ObjtblOrderSearchedData.SearchedStreetAddress == "")
                                                    {
                                                        ObjtblOrderSearchedData.best_SearchedStreetAddress = strAddressLine;
                                                        IsAddressUptd = true;
                                                    }
                                                    if (ObjtblOrderSearchedData.SearchedStreetName == null || ObjtblOrderSearchedData.SearchedStreetName == string.Empty || ObjtblOrderSearchedData.SearchedStreetName == "")
                                                    {
                                                        ObjtblOrderSearchedData.best_SearchedStreetName = strStreetName;
                                                        IsAddressUptd = true;
                                                    }
                                                    if (ObjtblOrderSearchedData.SearchedZipCode == null || ObjtblOrderSearchedData.SearchedZipCode == string.Empty || ObjtblOrderSearchedData.SearchedZipCode == "")
                                                    {
                                                        ObjtblOrderSearchedData.best_SearchedZipCode = strPostalCode;
                                                        IsAddressUptd = true;
                                                    }
                                                    if (ObjtblOrderSearchedData.SearchedCity == null || ObjtblOrderSearchedData.SearchedCity == string.Empty || ObjtblOrderSearchedData.SearchedCity == "")
                                                    {
                                                        ObjtblOrderSearchedData.best_SearchedCity = strMunicipality;
                                                        IsAddressUptd = true;
                                                    }
                                                    ObjtblOrderSearchedData.IsAddressUpdated = IsAddressUptd;
                                                    Dx.SubmitChanges();
                                                }

                                            }   
                                    //}
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //Added log for exception handling.
                            VendorServices.GeneralService.WriteLog("Some Error Occured in CheckResponseandUpdateStatus(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                        }

                        #endregion Best Address
                        byte ReportStatus = System.Convert.ToByte(BALGeneral.CheckReportStatus(Vendor_Response, ObjCollProducts, IsInstantReport, OrderDetailId, OrderId, false));
                        if ((ObjCollProducts == "EDV" || ObjCollProducts == "EMV" || ObjCollProducts == "PRV" || ObjCollProducts == "PLV" || ObjCollProducts == "PRV2") && ReportStatus == 2)
                        {
                            AddVerificationFee(OrderDetailId, Vendor_Response, ObjCollProducts);
                        }
                        UpdateReportStatus(OrderId, OrderDetailId, ReportStatus);
                        //update 7year filter for all states for ticket #85
                        Update7YearFilterForAllStates(1, Is7YearFilterForCriminal);
                        //Send An Auto Mail For Report Review
                        if (ReportStatus == 2)
                        {
                            SendAutoMailForReview(OrderId, OrderDetailId, Vendor_Response, false);
                            BALGeneral ObjBALGeneral = new BALGeneral();
                            string fiteredResponse = Vendor_Response;
                            var ObjOrderDetail = Dx.tblOrderDetails.Where(db => db.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                            if (ObjCollProducts == "NCR4" || ObjCollProducts == "NCR1" || ObjCollProducts == "NCR+" || ObjCollProducts == "NCR2")
                            {

                                fiteredResponse = ObjBALGeneral.GetFilteredXMLDuringOrderPlace(ObjCollProducts, Vendor_Response, OrderDetailId, DateTime.Now, false, false);
                                using (EmergeDALDataContext Dx2 = new EmergeDALDataContext())
                                {
                                    var ObjOrderDetailnew = Dx2.tblOrderDetails.Where(db => db.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                                    if (ObjOrderDetailnew.ReviewStatus != 1)
                                    {
                                        ObjBALGeneral.CheckIsNCRHit(OrderId, OrderDetailId, ObjCollProducts, fiteredResponse, false);
                                    }
                                }
                            }
                            if (ObjCollProducts == "RCX" && ReportStatus == 2)
                            {
                                bool IsRCXHit = GetRCXHitstatus(Vendor_Response);
                                if (IsRCXHit == true)
                                {

                                    string PCode = (from d in Dx.tblProducts
                                                    join g in Dx.tblProductPerApplications
                                                    on d.pkProductId equals g.fkProductId
                                                    where (g.pkProductApplicationId == ObjOrderDetail.fkProductApplicationId)
                                                    select d.ProductCode).FirstOrDefault().ToString();
                                    PlaceCCR1orderForRCXHitReport(OrderId, OrderDetailId, PCode, CompanyAccountNo, OrderCompanyName, PermissiblePurpose);
                                }
                                if (this.RCXFilePath != "")
                                {
                                    bool isFileExists = false;
                                    //string responseString = 
                                    BALGeneral.GetRCXResponsefromFile(OrderDetailId, out isFileExists);
                                    if (isFileExists)
                                    {
                                        RenameRCXFile(OrderDetailId);
                                    }
                                }
                            }
                            //To Add the data in tblInvoice table
                            new BALInvoice().CreateInvoice(OrderId);
                        }

                        #region If the Live Runner Reports(CCR's) are cancelled By Vendor

                        if (ObjCollProducts == "RCX" && ReportStatus == 5)
                        {
                            #region Save Response Having Error In Live Runner Report
                            SaveLiveRunnerErrorReponse(OrderDetailId, Vendor_Response);

                            #endregion

                            var OrderDetail = Dx.tblOrderDetails.Where(db => db.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                            if (OrderDetail.IsLiveRunner == true)
                            {
                                string ProductCode = (from d in Dx.tblProducts
                                                      join g in Dx.tblProductPerApplications
                                                      on d.pkProductId equals g.fkProductId
                                                      where (g.pkProductApplicationId == OrderDetail.fkProductApplicationId)
                                                      select d.ProductCode).FirstOrDefault().ToString();
                                #region Update IsLiveRunner To false
                                OrderDetail.IsLiveRunner = false;
                                Dx.SubmitChanges();
                                #endregion
                                if (ProductCode.ToLower() != "rcx")
                                {
                                    PlaceNewOrderForLiveRunnerCancelledReport(OrderId, OrderDetailId, ProductCode, CompanyAccountNo, OrderCompanyName, PermissiblePurpose);
                                }
                            }
                            else
                                ReportStatus = 4;
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in CheckResponseandUpdateStatus(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
        }
        /// <summary>
        /// To saving the verification fees in the selected order.

        public void AddVerificationFee(int? OrderDetailId, string Vendor_Response, string ProductCode)
        {
            decimal VerificationFee = 0;
            try
            {
                //Original response
                XDocument ObjXDocument = XDocument.Parse(Vendor_Response);
                //To get the multiple fee items tag.
                var feeLines = ObjXDocument.Descendants("FeeLineItem").ToList();
                if (feeLines != null && feeLines.Count() > 0)
                {
                    foreach (XElement item in feeLines)
                    {
                        var feeType = item.Descendants("FeeType").FirstOrDefault();
                        if (feeType != null)
                        {
                            if (feeType.Value.ToUpper().Trim() == "SURCHARGE")//As per client meeting on May 10, 2016 client updated that Surcharge will be considered as third party fee.
                            {
                                //To get report amount, considered as Additonal Fee amount in emerge.
                                VerificationFee = VerificationFee + Convert.ToDecimal(item.Descendants("Amount").FirstOrDefault().Value);
                            }
                        }
                    }
                }
                if (VerificationFee > -1)
                {
                    //Saving into the database.
                    using (EmergeDALDataContext DX = new EmergeDALDataContext())
                    {
                        tblOrderDetail ObjtblOrderDetail = DX.tblOrderDetails.Where(db => db.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                        if (ObjtblOrderDetail != null)
                        {
                            ObjtblOrderDetail.AdditionalFee = VerificationFee;
                            DX.SubmitChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in AddVerificationFee(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
        }


        private tblVendor PushVendorDetails(string ProductCode, List<Proc_GetAllProductsWithVendorResult> ObjProc_GetAllProductsWithVendorResult)
        {
            tblVendor ObjtblVendor = new tblVendor();

            var ResultProducAndVendor = ObjProc_GetAllProductsWithVendorResult.Where(DataAccessLayer => DataAccessLayer.ProductCode == ProductCode).FirstOrDefault();
            if (ResultProducAndVendor != null)
            {
                ObjtblVendor.AdditionalParams = ResultProducAndVendor.AdditionalParams;
                ObjtblVendor.ServicePassword = ResultProducAndVendor.ServicePassword;
                ObjtblVendor.ServiceUrl = ResultProducAndVendor.ServiceUrl;
                ObjtblVendor.ServiceUserId = ResultProducAndVendor.ServiceUserId;
                ObjtblVendor.VendorName = ResultProducAndVendor.VendorName;
            }
            return ObjtblVendor;
        }

        /// <summary>
        /// Function to Update Response 
        /// </summary>
        /// <param name="ResponseId"></param>
        /// <param name="Response"></param>
        /// <returns></returns>
        public bool UpdateResponse(int ResponseId, string Response)
        {

            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                if (ObjEmergeDALDataContext.UpdateResponse(ResponseId, Response) == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }

        public int UpdateReportStatus_Ret(int? pkOrderId, int pkOrderDetailId, byte? ReportStatus)
        {
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                return dx.UpdateOrderStatus(pkOrderId, pkOrderDetailId, ReportStatus);
            }
        }

        public Proc_GetStateCountyPerCCROrderResult GetStateCountyPerOrderDetail(int fkOrderDetailId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return DX.GetStateCountyPerCCROrder(fkOrderDetailId).FirstOrDefault();
            }
        }

        private string BuildXMLWithoutPeopleSearch(int? pkOrderId, tblOrderSearchedData ObjtblOrderSearchedData, string ProductCode, tblVendor ObjtblVendor_single, string SearchedStateCode)
        {
            StringBuilder ObjXML = new StringBuilder();

            string username = "";
            string password = "";

            if (ProductCode == "ECR")
            {
                XmlDocument Obj_ECR = new XmlDocument();

                Obj_ECR.LoadXml(ObjtblVendor_single.AdditionalParams.ToString());

                XmlNode ObjXmlNode = Obj_ECR.DocumentElement;

                username = ObjXmlNode.SelectSingleNode("ecr_memberid").ChildNodes[0].Value;

                password = ObjXmlNode.SelectSingleNode("ecr_password").ChildNodes[0].Value;
            }
            else
            {
                username = ObjtblVendor_single.ServiceUserId.ToString();

                password = ObjtblVendor_single.ServicePassword.ToString();
            }

            string Bureau = GetBureauForType(ProductCode);
            string Product = GetTypeId(ProductCode);

            ObjXML.Append(ObjtblVendor_single.ServiceUrl + "?");
            if ("ebp" == ProductCode.ToLower() || "db" == ProductCode.ToLower())
            {
                ObjXML.Append("Member_ID=" + username + "&Password=" + password + "");
            }
            else
            {
                ObjXML.Append("Member_ID=" + username + "&Password=" + password + "&Bureau=" + Bureau + "&Product=" + Product + "&HIDESSN=true");
            }

            string Ssn_Length = "";

            if (ObjtblOrderSearchedData.SearchedSsn != "___-__-____" && ObjtblOrderSearchedData.SearchedSsn != "")
            {
                Ssn_Length = ObjtblOrderSearchedData.SearchedSsn.ToString().Replace("-", "");
            }

            string Dob = "";
            BALGeneral ObjBALGeneral = new BALGeneral();

            if (ObjBALGeneral.isValidDateTime(ObjtblOrderSearchedData.SearchedDob) == true)
            {

                DateTime Now = System.DateTime.Now;

                DateTime ObjDateTime = ObjtblOrderSearchedData.SearchedDob != "" ? Convert.ToDateTime(ObjtblOrderSearchedData.SearchedDob) : Now;

                if (ObjDateTime != Now)
                {
                    Dob = ObjDateTime.ToString("MM/dd/yyyy");

                }
            }

            switch (ProductCode.ToLower())
            {
                case "pl":
                    ObjXML.Append("&LastName=" + ObjtblOrderSearchedData.SearchedLastName.ToString().ToUpper());
                    ObjXML.Append("&FirstName=" + ObjtblOrderSearchedData.SearchedFirstName.ToString().ToUpper());
                    ObjXML.Append("&State=" + SearchedStateCode.ToUpper());
                    break;


                case "ofac":
                    ObjXML.Append("&LastName=" + ObjtblOrderSearchedData.SearchedLastName.ToString().ToUpper());
                    break;

                case "tdr":
                    ObjXML.Append("&LastName=" + ObjtblOrderSearchedData.SearchedLastName.ToString().ToUpper());
                    ObjXML.Append("&FirstName=" + ObjtblOrderSearchedData.SearchedFirstName.ToString().ToUpper());
                    ObjXML.Append("&State=" + SearchedStateCode.ToUpper());
                    ObjXML.Append("&SSN=" + Ssn_Length);
                    break;

                case "br":
                    ObjXML.Append("&LastName=" + ObjtblOrderSearchedData.SearchedLastName.ToString().ToUpper());
                    ObjXML.Append("&FirstName=" + ObjtblOrderSearchedData.SearchedFirstName.ToString().ToUpper());
                    ObjXML.Append("&State=" + SearchedStateCode.ToUpper());
                    ObjXML.Append("&SSN=" + Ssn_Length);
                    break;

                case "ecr":
                    ObjXML.Append("&LastName=" + ObjtblOrderSearchedData.SearchedLastName.ToString().ToUpper());
                    ObjXML.Append("&FirstName=" + ObjtblOrderSearchedData.SearchedFirstName.ToString().ToUpper());
                    ObjXML.Append("&State=" + SearchedStateCode.ToUpper());
                    ObjXML.Append("&SSN=" + Ssn_Length);
                    ObjXML.Append("&DOB=" + Dob);
                    ObjXML.Append("&City=" + ObjtblOrderSearchedData.SearchedCity.ToString().ToUpper());
                    ObjXML.Append("&StreetNum=" + ObjtblOrderSearchedData.SearchedStreetAddress.ToString().ToUpper());
                    ObjXML.Append("&StreetName=" + ObjtblOrderSearchedData.SearchedStreetName.ToString().ToUpper());
                    ObjXML.Append("&StreetType=" + ObjtblOrderSearchedData.SearchedStreetType.ToString().ToUpper());
                    break;

                case "eei":
                    ObjXML.Append("&LastName=" + ObjtblOrderSearchedData.SearchedLastName.ToString().ToUpper());
                    ObjXML.Append("&FirstName=" + ObjtblOrderSearchedData.SearchedFirstName.ToString().ToUpper());
                    ObjXML.Append("&State=" + SearchedStateCode.ToUpper());
                    ObjXML.Append("&SSN=" + Ssn_Length);
                    ObjXML.Append("&DOB=" + Dob);
                    ObjXML.Append("&StreetNum=" + ObjtblOrderSearchedData.SearchedStreetAddress.ToString().ToUpper());
                    ObjXML.Append("&City=" + ObjtblOrderSearchedData.SearchedCity.ToString().ToUpper());
                    ObjXML.Append("&StreetName=" + ObjtblOrderSearchedData.SearchedStreetName.ToString().ToUpper());
                    ObjXML.Append("&StreetType=" + ObjtblOrderSearchedData.SearchedStreetType.ToString().ToUpper());
                    break;

                case "ncr4":
                    ObjXML.Append("&LastName=" + ObjtblOrderSearchedData.SearchedLastName.ToString().ToUpper());
                    ObjXML.Append("&FirstName=" + ObjtblOrderSearchedData.SearchedFirstName.ToString().ToUpper());
                    ObjXML.Append("&DOB=" + Dob);
                    break;

                case "mvs":
                    ObjXML.Append("&UserName=usaintel&DLPurpose=3&VIN=" + ObjtblOrderSearchedData.SearchedVehicleVin.ToString().ToUpper());
                    break;

                case "ssr":
                    ObjXML.Append("&SSN=" + Ssn_Length);
                    break;

                case "er":
                    ObjXML.Append("&LastName=" + ObjtblOrderSearchedData.SearchedLastName.ToString().ToUpper());
                    ObjXML.Append("&FirstName=" + ObjtblOrderSearchedData.SearchedFirstName.ToString().ToUpper());
                    ObjXML.Append("&State=" + SearchedStateCode.ToUpper());
                    break;

                case "rpl":
                    if (ObjtblOrderSearchedData.SearchedPhoneNumber != "") ObjXML.Append("&PhoneNum=" + ObjtblOrderSearchedData.SearchedPhoneNumber.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").ToString());
                    break;

                case "ral":
                    ObjXML.Append("&City=" + ObjtblOrderSearchedData.SearchedCity.ToString().ToUpper());
                    ObjXML.Append("&StreetNum=" + ObjtblOrderSearchedData.SearchedStreetAddress.ToString().ToUpper());
                    ObjXML.Append("&StreetName=" + ObjtblOrderSearchedData.SearchedStreetName.ToString().ToUpper());
                    ObjXML.Append("&StreetType=" + ObjtblOrderSearchedData.SearchedStreetType.ToString().ToUpper());
                    ObjXML.Append("&State=" + SearchedStateCode.ToUpper());
                    break;

                case "ps2":
                    ObjXML.Append("&LastName=" + ObjtblOrderSearchedData.SearchedLastName.ToString().ToUpper());
                    ObjXML.Append("&FirstName=" + ObjtblOrderSearchedData.SearchedFirstName.ToString().ToUpper());
                    ObjXML.Append("&State=" + SearchedStateCode.ToUpper());
                    ObjXML.Append("&SSN=" + Ssn_Length);
                    break;
                case "bps":
                    ObjXML.Append("&Bureau=EB");
                    ObjXML.Append("&Product=PS");
                    ObjXML.Append("&BusName=" + ObjtblOrderSearchedData.SearchedBusinessName.ToUpper());
                    ObjXML.Append("&State=" + SearchedStateCode.ToUpper());
                    ObjXML.Append("&City=" + ObjtblOrderSearchedData.SearchedBusinessCity.ToString().ToUpper());
                    break;
                case "ebp":
                    ObjXML.Append("&Bureau=EB");
                    ObjXML.Append("&Product=P");
                    ObjXML.Append("&BusName=" + ObjtblOrderSearchedData.SearchedBusinessName.ToUpper());
                    ObjXML.Append("&State=" + SearchedStateCode.ToUpper());
                    ObjXML.Append("&City=" + ObjtblOrderSearchedData.SearchedBusinessCity.ToString().ToUpper());
                    break;
                case "db":
                    ObjXML.Append("&Bureau=DB");
                    ObjXML.Append("&Product=DB1AC");
                    ObjXML.Append("&BusName=" + ObjtblOrderSearchedData.SearchedBusinessName.ToUpper());
                    ObjXML.Append("&State=" + SearchedStateCode.ToUpper());
                    break;
                case "ucc":
                    ObjXML.Append("&Bureau=EB");
                    ObjXML.Append("&Product=UCC");
                    ObjXML.Append("&BusName=" + ObjtblOrderSearchedData.SearchedBusinessName.ToUpper());
                    ObjXML.Append("&State=" + SearchedStateCode.ToUpper());
                    ObjXML.Append("&City=" + ObjtblOrderSearchedData.SearchedBusinessCity.ToString().ToUpper());
                    break;
                case "uccr":
                    ObjXML.Append("&Bureau=DS");
                    ObjXML.Append("&Product=UCCR");
                    ObjXML.Append("&BusName=" + ObjtblOrderSearchedData.SearchedBusinessName.ToUpper());
                    ObjXML.Append("&State=" + SearchedStateCode.ToUpper());
                    ObjXML.Append("&City=" + ObjtblOrderSearchedData.SearchedBusinessCity.ToString().ToUpper());
                    break;

            }


            ObjXML.Append("&AddrType=S");

            return ObjXML.ToString();
        }

        /// <summary>
        /// To creating the XML request for different vendors.
        /// </summary>        
        private string CreateXMLRequest(tblOrder ObjtblOrder, string ProductCode, tblOrderSearchedData ObjtblOrderSearchedData,
                           List<tblOrderSearchedEmploymentInfo> ObjCollOrderSearchedEmploymentInfo,
                           List<tblOrderSearchedEducationInfo> ObjCollOrderSearchedEducationInfo,
                           List<tblOrderSearchedPersonalInfo> ObjCollOrderSearchedPersonalInfo,
                           List<tblOrderSearchedProfessionalInfo> ObjCollOrderSearchedProfessionalInfo,
                           List<tblOrderPersonalQtnInfo> ObjCollOrderPersonalQtnInfo,
                           List<tblOrderSearchedLicenseInfo> ObjCollOrderLicenseInfo,
                           List<tblOrderSearchedAliasPersonalInfo> ObjCollOrderSearchedAliasPersonalInfo,
                           List<tblOrderSearchedAliasLocationInfo> ObjCollOrderSearchedAliasLocationInfo,
                           List<tblOrderSearchedAliasWorkInfo> ObjCollOrderSearchedAliasWorkInfo,
                           Guid ApplicationId,
                           int? pkOrderId,
                           int? pkOrderDetailId,
                           List<tblVendor> ObjtblVendor, List<tblState> ObjtblState_List,
                           string SearchedStateCode,
                           string SearchedDlStateCode,
                           string SearchedCountyName,
                           List<Proc_GetAllProductsWithVendorResult> ObjProc_GetAllProductsWithVendorResult,
                           int IsNullDOB, string CompanyAccountNo, string OrderCompanyName, string PermissiblePurpose, string fName = "", string lName = "", string mName = "")/*,
                              int AuthorizedPurpose, int SearchType*/
        {
            //BALVendor ObjBALVendor = new BALVendor();
            string Request = "";
            try
            {
                tblVendor ObjtblVendor_single = PushVendorDetails(ProductCode, ObjProc_GetAllProductsWithVendorResult);
                if (ProductCode == "TDR" || ProductCode == "OFAC" || ProductCode == "BR" || ProductCode == "NCR4" || ProductCode == "MVS" ||
                ProductCode == "SSR" || ProductCode == "ER" || ProductCode == "RPL" || ProductCode == "RAL" || ProductCode == "ECR" || ProductCode == "EEI" || ProductCode == "PL" || ProductCode == "BPS" || ProductCode == "UCCR" || ProductCode == "UCC" || ProductCode == "EBP" || ProductCode == "DB")
                {
                    Request = BuildXMLWithoutPeopleSearch(pkOrderId, ObjtblOrderSearchedData, ProductCode, ObjtblVendor_single, SearchedStateCode);
                }
                if (ProductCode == "PS2" || ProductCode == "PS")
                {
                    Request = BuildXMLForPeopleSearch(pkOrderId, ObjtblOrderSearchedData, ProductCode, ObjtblVendor_single, SearchedStateCode);
                }
                if (ProductCode == "NCR1" || ProductCode == "RCX" || ProductCode == "FCR" || ProductCode == "NCR+")
                {
                    if (ProductCode == "RCX")
                    {
                        Request = GenerateRAPIDCOURTRequest(pkOrderId, ObjtblOrderSearchedData, ObjCollOrderLicenseInfo, ObjCollOrderPersonalQtnInfo, ObjCollOrderSearchedEducationInfo, ObjCollOrderSearchedEmploymentInfo, ProductCode, SearchedStateCode, SearchedCountyName, IsNullDOB, fName, lName, mName);
                    }
                    else
                    {
                        Request = GenerateRAPIDCOURTRequest(pkOrderId, ObjtblOrderSearchedData, ObjCollOrderLicenseInfo, ObjCollOrderPersonalQtnInfo, ObjCollOrderSearchedEducationInfo, ObjCollOrderSearchedEmploymentInfo, ProductCode, SearchedStateCode, SearchedCountyName, IsNullDOB);
                    }
                }
                if (ProductCode == "SOR" || ProductCode == "NCR3" || ProductCode == "CCR2")
                {
                    Request = GenerateBGCRequest(pkOrderId, ObjtblOrderSearchedData, ObjCollOrderLicenseInfo, ObjCollOrderPersonalQtnInfo, ObjCollOrderSearchedEducationInfo, ObjCollOrderSearchedEmploymentInfo, ObjtblVendor_single, ProductCode, SearchedStateCode, SearchedCountyName, CompanyAccountNo, OrderCompanyName, PermissiblePurpose);
                }
                if (ProductCode == "MVR" || ProductCode == "CDLIS" || ProductCode == "PSP")
                {
                    Request = GenerateSOFTECHRequest(pkOrderId, pkOrderDetailId, ObjtblOrderSearchedData, ObjCollOrderLicenseInfo, ObjCollOrderPersonalQtnInfo, ObjCollOrderSearchedEducationInfo, ObjCollOrderSearchedEmploymentInfo, ObjtblVendor_single, ProductCode, SearchedStateCode, SearchedDlStateCode, CompanyAccountNo, OrderCompanyName, PermissiblePurpose);
                }
                if (ProductCode == "NCR2")
                {
                    Request = GenerateXPEDITERequest(pkOrderId, ObjtblOrderSearchedData, ObjCollOrderLicenseInfo, ObjCollOrderPersonalQtnInfo, ObjCollOrderSearchedEducationInfo, ObjCollOrderSearchedEmploymentInfo, ObjtblVendor_single, ProductCode, SearchedStateCode, SearchedCountyName, CompanyAccountNo, OrderCompanyName, PermissiblePurpose);
                }
                if (ProductCode == "CCR1" || ProductCode == "SCR")
                {
                    Request = GenerateXPEDITERequest(pkOrderId, ObjtblOrderSearchedData, ObjCollOrderLicenseInfo, ObjCollOrderPersonalQtnInfo, ObjCollOrderSearchedEducationInfo, ObjCollOrderSearchedEmploymentInfo, ObjtblVendor_single, ProductCode, SearchedStateCode, SearchedCountyName, CompanyAccountNo, OrderCompanyName, PermissiblePurpose, fName, lName, mName);
                }
                if (ProductCode == "EDV" || ProductCode == "EMV" || ProductCode == "PRV" || ProductCode == "PLV" || ProductCode == "PRV2" || ProductCode == "WCV")
                {
                    Request = GenerateFRSRequest(pkOrderId, ObjtblOrderSearchedData, ObjCollOrderLicenseInfo, ObjCollOrderPersonalQtnInfo, ObjCollOrderSearchedEducationInfo, ObjCollOrderSearchedEmploymentInfo, ObjtblVendor_single, ProductCode, SearchedStateCode, ObjCollOrderSearchedPersonalInfo, ObjCollOrderSearchedProfessionalInfo);
                }
                if (ProductCode == "SNS")
                {
                    Request = BuilRequestForSocialDiliGence(pkOrderId, ProductCode, ObjtblOrderSearchedData, ObjCollOrderSearchedAliasPersonalInfo, ObjCollOrderSearchedAliasLocationInfo, ObjCollOrderSearchedAliasWorkInfo, ObjtblState_List, ObjtblVendor_single, SearchedStateCode);
                }
            }
            catch (Exception ex)
            {
                Request = ex.Message.ToString();
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in CreateXMLRequest(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            finally
            {
                //ObjBALVendor = null;
            }
            return Request;
        }

        public List<tblOrderSearchedEducationInfo> GetOrderSearchedEducationInfo(int fkOrderId)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                return Dx.tblOrderSearchedEducationInfos.Where(db => db.fkOrderId == fkOrderId).ToList();
            }
        }

        public List<tblOrderSearchedEmploymentInfo> GetOrderSearchedEmploymentInfo(int fkOrderId)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                return Dx.tblOrderSearchedEmploymentInfos.Where(db => db.fkOrderId == fkOrderId).ToList();
            }
        }
        public List<tblOrderSearchedLicenseInfo> GetOrderSearchedLicenseInfo(int fkOrderId)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                return Dx.tblOrderSearchedLicenseInfos.Where(db => db.fkOrderId == fkOrderId).ToList();
            }
        }


        public List<tblOrderSearchedPersonalInfo> GetOrderSearchedPersonalInfo(int fkOrderId)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                return Dx.tblOrderSearchedPersonalInfos.Where(db => db.fkOrderId == fkOrderId).ToList();
            }
        }


        public List<tblOrderPersonalQtnInfo> GetOrderSearchedPersonalQtnInfo(int fkOrderId)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                return Dx.tblOrderPersonalQtnInfos.Where(db => db.fkOrderId == fkOrderId).ToList();
            }
        }
        public tblOrder GetOrder(int fkOrderId)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                return Dx.tblOrders.Where(db => db.pkOrderId == fkOrderId).FirstOrDefault();
            }
        }
        public tblOrderSearchedData GetOrderSearchedData(int fkOrderId)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                tblOrderSearchedData objtblOrderSearchedData = new tblOrderSearchedData();
                Proc_GetOrderSearchedDataResult dataResult = Dx.Proc_GetOrderSearchedData(fkOrderId).FirstOrDefault();
                objtblOrderSearchedData.best_search_county = dataResult.best_search_county;

                objtblOrderSearchedData.best_search_state = dataResult.best_search_state;

                objtblOrderSearchedData.best_SearchedCity = dataResult.best_SearchedCity;

                objtblOrderSearchedData.best_SearchedStreetAddress = dataResult.best_SearchedStreetAddress;

                objtblOrderSearchedData.best_SearchedStreetName = dataResult.best_SearchedStreetName;

                objtblOrderSearchedData.best_SearchedZipCode = dataResult.best_SearchedZipCode;

                objtblOrderSearchedData.fkOrderId = dataResult.fkOrderId;

                objtblOrderSearchedData.IsAddressUpdated = dataResult.IsAddressUpdated;

                objtblOrderSearchedData.pkOrderSearchedId = dataResult.pkOrderSearchedId;

                objtblOrderSearchedData.search_county = dataResult.search_county;

                objtblOrderSearchedData.search_state = dataResult.search_state;

                objtblOrderSearchedData.SearchedApplicantEmail = dataResult.SearchedApplicantEmail;

                objtblOrderSearchedData.SearchedApt = dataResult.SearchedApt;

                objtblOrderSearchedData.SearchedBusinessCity = dataResult.SearchedBusinessCity;

                objtblOrderSearchedData.SearchedBusinessName = dataResult.SearchedBusinessName;

                objtblOrderSearchedData.SearchedBusinessStateId = dataResult.SearchedBusinessStateId;

                objtblOrderSearchedData.SearchedCity = dataResult.SearchedCity;

                objtblOrderSearchedData.SearchedCountyId = dataResult.SearchedCountyId;

                objtblOrderSearchedData.SearchedDirection = dataResult.SearchedDirection;

                objtblOrderSearchedData.SearchedDLStateId = dataResult.SearchedDLStateId;

                objtblOrderSearchedData.SearchedDob = dataResult.SearchedDob;

                objtblOrderSearchedData.SearchedDriverLicense = dataResult.SearchedDriverLicense;

                objtblOrderSearchedData.SearchedFirstName = dataResult.SearchedFirstName;

                objtblOrderSearchedData.SearchedLastName = dataResult.SearchedLastName;

                objtblOrderSearchedData.SearchedMiddleInitial = dataResult.SearchedMiddleInitial;

                objtblOrderSearchedData.SearchedPermissibleId = dataResult.SearchedPermissibleId;

                objtblOrderSearchedData.SearchedPhoneNumber = dataResult.SearchedPhoneNumber;

                objtblOrderSearchedData.SearchedSex = dataResult.SearchedSex;

                objtblOrderSearchedData.SearchedSsn = dataResult.SearchedSsn;

                objtblOrderSearchedData.SearchedStateId = dataResult.SearchedStateId;

                objtblOrderSearchedData.SearchedStreetAddress = dataResult.SearchedStreetAddress;

                objtblOrderSearchedData.SearchedStreetName = dataResult.SearchedStreetName;

                objtblOrderSearchedData.SearchedStreetType = dataResult.SearchedStreetType;

                objtblOrderSearchedData.SearchedSuffix = dataResult.SearchedSuffix;

                objtblOrderSearchedData.SearchedTrackingNotes = dataResult.SearchedTrackingNotes;

                objtblOrderSearchedData.SearchedTrackingRef = dataResult.SearchedTrackingRef;

                objtblOrderSearchedData.SearchedTrackingRefId = dataResult.SearchedTrackingRefId;

                objtblOrderSearchedData.SearchedVehicleVin = dataResult.SearchedVehicleVin;

                objtblOrderSearchedData.SearchedZipCode = dataResult.SearchedZipCode;

                return objtblOrderSearchedData;
            }
        }

        public List<tblOrderSearchedLiveRunnerInfo> GetOrderSearchedLiveRunnerInfo(int fkOrderId)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                return Dx.tblOrderSearchedLiveRunnerInfos.Where(db => db.fkOrderId == fkOrderId).ToList();
            }
        }
        private string GetXMLResponse(string ProductCode, List<Proc_GetAllProductsWithVendorResult> ObjProc_GetAllProductsWithVendorResult, string Vendor_Request, int? pkOrderId, int? OrderDetailId, bool IsInstantReport)
        {
            string[] ProductCode_Parts = ProductCode.Split(',');
            ProductCode = ProductCode_Parts[0].ToString().Replace('[', ' ').Trim();
            BALGeneral ObjBALGeneral = new BALGeneral();
            tblReportErrorLog ObjtblReportErrorLog;
            string Response = "", Error = "";

            Dictionary<string, string> Response_Dic = new Dictionary<string, string>();

            try
            {
                if (ProductCode == "TDR" || ProductCode == "OFAC" || ProductCode == "BR" || ProductCode == "NCR4" || ProductCode == "MVS" ||
                ProductCode == "SSR" || ProductCode == "ER" || ProductCode == "RPL" || ProductCode == "RAL" || ProductCode == "PS2" || ProductCode == "ECR" || ProductCode == "EEI" || ProductCode == "PL"
                || ProductCode == "BPS" || ProductCode == "UCCR" || ProductCode == "UCC" || ProductCode == "EBP" || ProductCode == "DB")
                {
                    MicrobiltService ObjMicrobiltService = new MicrobiltService();
                    try
                    {
                        //INT-173 this is not hitting the vendor URL
                        if (ProductCode == "ECR" || ProductCode == "EEI")
                        {

                            Response_Dic["response"] = "";
                            Response_Dic["error"] = "";
                            Response = Response_Dic["response"];
                            Error = Response_Dic["error"];

                            bool IsAdmin1 = false;
                            MembershipUser ObjMembershipUser = Membership.GetUser(Utilitys.GetUID(HttpContext.Current.User.Identity.Name));
                            if (ObjMembershipUser != null)
                            {
                                if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SystemAdmin"))
                                {
                                    IsAdmin1 = true;
                                }
                                if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SupportManager"))
                                {
                                    IsAdmin1 = true;
                                }
                                if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SupportTechnician"))
                                {
                                    IsAdmin1 = true;
                                }
                            }

                            if (!IsAdmin1)
                            {
                                string slastname = "";
                                string sfirstname = "";
                                string smiddlename = "";
                                tblOrderSearchedData obj_search = new tblOrderSearchedData();
                                using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                                {
                                    int pkid = Convert.ToInt32(pkOrderId);
                                    obj_search = Dx.tblOrderSearchedDatas.FirstOrDefault(x => x.fkOrderId == pkid);
                                    if (obj_search != null)
                                    {
                                        slastname = obj_search.SearchedLastName;
                                        sfirstname = obj_search.SearchedFirstName;
                                        smiddlename = obj_search.SearchedMiddleInitial;
                                    }
                                }

                                tblOrder obj_order = GetOrder(Convert.ToInt32(pkOrderId));
                                string orderno = obj_order.OrderNo;
                                string productinfo_name = ProductCode;
                                var productinfo = ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First();
                                if (productinfo != null)
                                {
                                    productinfo_name = productinfo.ProductName;
                                }

                                ObjBALGeneral.SendEMEditMailRRforECI(Convert.ToInt32(OrderDetailId), "Please check", "false", "1", "Emerge Review|" + slastname + sfirstname, productinfo_name, orderno, slastname + "_" + sfirstname, smiddlename);
                            }

                        }
                        else
                        {
                            Response_Dic = ObjMicrobiltService.RunReport(PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, pkOrderId.ToString(), OrderDetailId.ToString()));
                            Response = Response_Dic["response"];
                            Error = Response_Dic["error"];
                            if (Response == "" && IsInstantReport)
                            {
                                UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("3"));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //Added log for exception handling.
                        VendorServices.GeneralService.WriteLog("Some Error Occured in GetXMLResponse(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    }
                    finally
                    {
                        ObjMicrobiltService = null;
                    }
                }

                if (ProductCode == "NCR1" || ProductCode == "PS" || ProductCode == "RCX" || ProductCode == "FCR" || ProductCode == "NCR+")
                {
                        RapidCourtService ObjRapidCourtService = new RapidCourtService();
                        IDSFactProductService objIDSFactProductService = new IDSFactProductService();
                   
                    tblOrderDetail objtblOrderDetail = new tblOrderDetail();
                    try
                    {
                        using (EmergeDALDataContext dx = new EmergeDALDataContext())
                        {
                            objtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                            objtblOrderDetail.MaximumHitsAllowed = 1;
                            objtblOrderDetail.ErrorLog = Error;
                            dx.SubmitChanges();
                        }

                        //Changed By Himesh Kumar
                        if (ProductCode != "PS")
                        {
                            Response_Dic = ObjRapidCourtService.RunReport(PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, pkOrderId.ToString(), OrderDetailId.ToString()));
                        }
                        else if(ProductCode=="PS")
                        {
                            Response_Dic = objIDSFactProductService.RunReport(PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, pkOrderId.ToString(), OrderDetailId.ToString()));
                        }
                        Response = Response_Dic["response"];
                        Error = Response_Dic["error"];

                        if (ProductCode == "NCR1" || ProductCode == "PS")
                        {
                            if (Response == "" || Error.ToLower().Contains("the operation has timed out"))
                            {
                                UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("1"));
                                using (EmergeDALDataContext dx = new EmergeDALDataContext())
                                {
                                    objtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                                    objtblOrderDetail.MaximumHitsAllowed = 1;
                                    objtblOrderDetail.ErrorLog = Error;
                                    dx.SubmitChanges();
                                }
                            }
                        }

                        #region Insert All information to error log column for CCR1 Live runner Issue

                        using (EmergeDALDataContext dx = new EmergeDALDataContext())
                        {
                            try
                            {
                                objtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                                string ErrorConCat = string.Empty;
                                if (objtblOrderDetail != null)
                                {
                                    ErrorConCat = objtblOrderDetail.ErrorLog.ToString() + " ==>  (" + DateTime.Now.ToString() + ") ==> Error ==> " + Error + " ==> Response ==> " + Response;
                                    objtblOrderDetail.ErrorLog = ErrorConCat;
                                    dx.SubmitChanges();
                                }
                            }
                            catch (Exception ex)
                            {
                                //Added log for exception handling.
                                VendorServices.GeneralService.WriteLog("Some Error Occured in GetXMLResponse(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                            }
                        }

                        #endregion
                    }
                    catch (Exception ex)
                    {
                        //Added log for exception handling.
                        VendorServices.GeneralService.WriteLog("Some Error Occured in GetXMLResponse(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

                        using (EmergeDALDataContext dx = new EmergeDALDataContext())
                        {
                            try
                            {
                                objtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                                string ErrorConCat = string.Empty;
                                if (objtblOrderDetail != null)
                                {
                                    ErrorConCat = objtblOrderDetail.ErrorLog.ToString() + " ==>  (" + DateTime.Now.ToString() + ") ==> Error from catch ==> " + ex.ToString();
                                    objtblOrderDetail.ErrorLog = ErrorConCat;
                                    dx.SubmitChanges();
                                }
                            }
                            catch (Exception ex2)
                            {
                                //Added log for exception handling.
                                VendorServices.GeneralService.WriteLog("Some Error Occured in GetXMLResponse(): " + DateTime.Now.ToString() + "\n Error Message :" + ex2.Message + "\n Stack Trace :" + ex2.StackTrace, "ManageProduct");
                            }
                        }
                    }
                    finally
                    {
                        ObjRapidCourtService = null;
                        objtblOrderDetail = null;
                    }
                }
                if (ProductCode == "SOR" || ProductCode == "NCR3" || ProductCode == "CCR2")
                {
                    BackgroundChecksService ObjBackgroundChecksService = new BackgroundChecksService();
                    try
                    {
                        Response_Dic = ObjBackgroundChecksService.RunReport(PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, pkOrderId.ToString(), OrderDetailId.ToString()));
                        Response = Response_Dic["response"];
                        Error = Response_Dic["error"];
                        string[] RequestAndResponse = Response.Split('^');
                        UpdateRefSecureKey(OrderDetailId, RequestAndResponse[1]);
                        Response = RequestAndResponse[0];

                        tblOrderDetail objtblOrderDetail = new tblOrderDetail();
                        if (Response == "" && IsInstantReport)
                        {
                            UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("3"));
                        }
                        else if (Response == "" && IsInstantReport == false)
                        {
                            if (ProductCode == "SCR")//Ticket #636
                            {
                                if (Response != "" || Error.ToLower().Contains("the operation has timed out"))//Ticket #639: Some SCR report is not completing
                                {
                                    UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("1"));
                                    using (EmergeDALDataContext dx = new EmergeDALDataContext())
                                    {
                                        objtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                                        objtblOrderDetail.MaximumHitsAllowed = 1;
                                        objtblOrderDetail.ErrorLog = Error;
                                        dx.SubmitChanges();
                                    }
                                }
                                else
                                {
                                    UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("3"));
                                    using (EmergeDALDataContext dx = new EmergeDALDataContext())
                                    {
                                        objtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                                        objtblOrderDetail.MaximumHitsAllowed = 1;
                                        objtblOrderDetail.ErrorLog = Error;
                                        dx.SubmitChanges();
                                    }
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        //Added log for exception handling.
                        VendorServices.GeneralService.WriteLog("Some Error Occured in GetXMLResponse(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    }
                    finally
                    {
                        ObjBackgroundChecksService = null;
                    }
                }
                if (ProductCode == "MVR" || ProductCode == "CDLIS")
                {
                    SoftechService ObjSoftechService = new SoftechService();
                    try
                    {
                        Response_Dic = ObjSoftechService.RunReport(PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, pkOrderId.ToString(), OrderDetailId.ToString()));
                        Response = Response_Dic["response"];
                        Error = Response_Dic["error"];

                        if (Response_Dic["ispending"] != "")
                        {
                            UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("1"));
                        }

                        if (Response.Contains('^'))
                        {
                            string[] RequestAndResponse = Response.Split('^');
                            UpdateRefSecureKey(OrderDetailId, RequestAndResponse[1]);
                            Response = RequestAndResponse[0];
                        }

                        if (Response == "" && IsInstantReport)
                        {
                            UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("3"));
                        }
                    }
                    catch (Exception ex)
                    {
                        //Added log for exception handling.
                        VendorServices.GeneralService.WriteLog("Some Error Occured in GetXMLResponse(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    }
                    finally
                    {
                        ObjSoftechService = null;
                    }
                }
                #region PSPRegion

                if (ProductCode == "PSP")
                {
                    PSPServiceWebService objPSPService = new PSPServiceWebService();
                    try
                    {
                        Dictionary<string, string> Response_Dic1 = new Dictionary<string, string>();
                        Response_Dic1["response"] = "";
                        Response_Dic1["error"] = "";
                        Response_Dic1["ispending"] = "";
                        Response_Dic = objPSPService.RunReport(PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, pkOrderId.ToString(), OrderDetailId.ToString()));
                        //code comment for esceen recorde int 116
                        Response = Response_Dic["response"];
                        Error = Response_Dic["error"];

                        if (Response_Dic["ispending"] != "")
                        {
                            UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("1"));
                        }

                        if (Response.Contains('^'))
                        {
                            string[] RequestAndResponse = Response.Split('^');
                            UpdateRefSecureKey(OrderDetailId, RequestAndResponse[1]);
                            Response = RequestAndResponse[0];
                        }

                        if (Response == "" && IsInstantReport)
                        {
                            UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("3"));
                        }

                        if (Response == "")
                        {
                            UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("3"));
                        }
                    }
                    catch (Exception ex)
                    {
                        //Added log for exception handling.
                        VendorServices.GeneralService.WriteLog("Some Error Occured in GetXMLResponse(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    }
                    finally
                    {
                        objPSPService = null;
                    }
                }

                #endregion


                if (ProductCode == "NCR2" || ProductCode == "CCR1" || ProductCode == "SCR")
                {
                    XpediteService ObjXpediteService = new XpediteService();
                    try
                    {
                        Response_Dic = ObjXpediteService.RunReport(PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, pkOrderId.ToString(), OrderDetailId.ToString()));


                        Response = Response_Dic["response"];
                        Error = Response_Dic["error"];
                        string[] RequestAndResponse = Response.Split('^');
                        UpdateRefSecureKey(OrderDetailId, RequestAndResponse[1]);
                        Response = RequestAndResponse[0].Replace("utf-16", "utf-8");

                        tblOrderDetail objtblOrderDetail = new tblOrderDetail();
                        if (Response == "" && IsInstantReport)
                        {
                            if (ProductCode == "NCR2")
                            {
                                using (EmergeDALDataContext dx = new EmergeDALDataContext())
                                {
                                    objtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                                    if (objtblOrderDetail != null)
                                    {
                                        //This condition is done in vendor dll method, Please check : ObjXpediteService.RunReport
                                        objtblOrderDetail.MaximumHitsAllowed = 12;
                                        objtblOrderDetail.ErrorLog = Error;
                                        dx.SubmitChanges();
                                    }
                                }
                                UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("3"));
                            }
                        }
                        else if (Response == "" && IsInstantReport == false)
                        {
                            if (ProductCode == "CCR1")//Ticket #636
                            {
                                if (Error.ToLower().Contains("the operation has timed out"))
                                {
                                    //Change report status pending
                                    UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("1"));
                                    //Set hit status
                                    using (EmergeDALDataContext dx = new EmergeDALDataContext())
                                    {
                                        objtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                                        objtblOrderDetail.MaximumHitsAllowed = 1;
                                        objtblOrderDetail.ErrorLog = Error;
                                        dx.SubmitChanges();
                                    }
                                }
                                else if (Response == "" && Error.ToLower().Contains("server was unable to process request"))
                                {
                                    //INT-330
                                    //As per greg, he needs a alert, when any error occurred in Xpedite service then a mail will shot to support@intelifi.com
                                    //  Code to Send an email to Test
                                    string mailSubject = Convert.ToString(ConfigurationManager.AppSettings["CCR1ErrorMailSubject"]);
                                    int SendingStatus = -1;
                                    MomentumInfotech.EmailProvider.MailProvider ObjMailProvider = new MomentumInfotech.EmailProvider.MailProvider();
                                    ObjMailProvider.MailTo.Add(Convert.ToString(ConfigurationManager.AppSettings["CCR1ErrorMailTo"]));
                                    ObjMailProvider.MailFrom = Convert.ToString(ConfigurationManager.AppSettings["CCR1ErrorMailFrom"]);
                                    ObjMailProvider.Subject = mailSubject.Replace("%OrderNo%", GetOrderNoformail(pkOrderId));
                                    ObjMailProvider.Message = Response;
                                    ObjMailProvider.SendMail(out SendingStatus);
                                }
                                else
                                {

                                    //To check Order pending duration and makred as in-compkete.     
                                    new BALGeneral().checkOrderPeriod(pkOrderId, OrderDetailId, ProductCode, Error);
                                    //Set hit status
                                    using (EmergeDALDataContext dx = new EmergeDALDataContext())
                                    {
                                        objtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                                        objtblOrderDetail.MaximumHitsAllowed = 1;
                                        objtblOrderDetail.ErrorLog = Error;
                                        dx.SubmitChanges();
                                    }
                                }
                            }
                        }

                        #region Insert All information to error log column for CCR1 Live runner Issue

                        using (EmergeDALDataContext dx = new EmergeDALDataContext())
                        {
                            try
                            {
                                objtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                                string ErrorConCat = string.Empty;
                                if (objtblOrderDetail != null)
                                {
                                    ErrorConCat = objtblOrderDetail.ErrorLog.ToString() + " ==>  (" + DateTime.Now.ToString() + ") ==> Error ==> " + Error + " ==> Response ==> " + Response;
                                    objtblOrderDetail.ErrorLog = ErrorConCat;
                                    dx.SubmitChanges();
                                }
                            }
                            catch (Exception ex)
                            {
                                //Added log for exception handling.
                                VendorServices.GeneralService.WriteLog("Some Error Occured in GetXMLResponse(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                            }
                        }

                        #endregion
                    }
                    catch (Exception ex)
                    {
                        //Added log for exception handling.
                        VendorServices.GeneralService.WriteLog("Some Error Occured in GetXMLResponse(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    }
                    finally
                    {
                        ObjXpediteService = null;
                    }
                }
                if (ProductCode == "EDV" || ProductCode == "EMV" || ProductCode == "PRV" || ProductCode == "PLV" || ProductCode == "PRV2")
                {
                    //INT-15
                    //New integration of verification reports.
                    FRSService ObjFRSService = new FRSService();
                    try
                    {
                        Vendor_Request = ObjFRSService.getFinalRequest(PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, pkOrderId.ToString(), OrderDetailId.ToString()));
                        int iResultRequest = UpdateOrderRequestData(OrderDetailId, Vendor_Request);//Updated the Reuest data.
                        //To send first time response.
                        Response_Dic = ObjFRSService.RunReport(PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, pkOrderId.ToString(), OrderDetailId.ToString()));
                        XmlDocument xDoc = new XmlDocument();
                        xDoc.InnerXml = Response_Dic["response"];
                        Response = Response_Dic["response"];
                        string OrderStatusVerification = string.Empty;
                        if (xDoc.GetElementsByTagName("RPOrderSet").Count > 0)
                        {
                            OrderStatusVerification = xDoc.SelectSingleNode("RPOrderSet/RPB2BOrder/OrderStatus").FirstChild.Value;
                        }
                        else
                        {
                            OrderStatusVerification = xDoc.SelectSingleNode("RPB2BOrder/OrderStatus").FirstChild.Value;
                        }
                        if ((OrderStatusVerification.Trim().ToUpper() == "PENDING-NEW") || (OrderStatusVerification.Trim().ToUpper() == "IN PROGRESS") || (OrderStatusVerification.Trim().ToUpper() == "REVIEW"))
                        {
                            UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("1"));//in-Progress if any error found.
                        }
                        else if ((OrderStatusVerification.Trim().ToUpper() == "COMPLETE") || (OrderStatusVerification.Trim().ToUpper() == "NO LISTING") || (OrderStatusVerification.Trim().ToUpper() == "NO RECORD FOUND") || (OrderStatusVerification.Trim().ToUpper() == "NO RESPONSE") || (OrderStatusVerification.Trim().ToUpper() == "NO REF POLICY"))
                        {
                            UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("2"));//Complete if any error found.
                        }
                        else
                        {
                            UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("2"));//INT336 As per Greg requirment, he needs if order status other than Pending application will considered as Complete order status.
                        }
                    }
                    catch (Exception ex)
                    {
                        //Added log for exception handling.
                        VendorServices.GeneralService.WriteLog("Some Error Occured in GetXMLResponse(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    }
                    finally
                    {
                        ObjFRSService = null;
                    }
                }
                if (ProductCode == "WCV")
                {
                    //INT-15
                    //New integration of verification reports.
                    FRSServiceWCV ObjFRSService = new FRSServiceWCV();
                    try
                    {
                        //To send first time response.
                        Response_Dic = ObjFRSService.RunReport(PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, pkOrderId.ToString(), OrderDetailId.ToString()));
                        XmlDocument xDoc = new XmlDocument();
                        xDoc.InnerXml = Response_Dic["response"];
                        if (xDoc.GetElementsByTagName("error").Count > 0)//If any error found in up coming response.
                        {
                            Response_Dic["error"] = xDoc.GetElementsByTagName("error").Item(0).Value;//If any error found in response.
                            UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("3"));//in-complete if any error found.
                        }
                        else if (xDoc.GetElementsByTagName("order").Count > 0)
                        {
                            string strOrderId = xDoc.GetElementsByTagName("order").Item(0).Attributes.Item(0).Value;
                            Vendor_Request = GetFRSOrderDetailByVendor(strOrderId);//To get responses based on generated ids.
                            int iResultRequest = UpdateOrderRequestData(OrderDetailId, Vendor_Request);//Updated the Reuest data.
                            //To calling again based on newly generated order ids.
                            Response_Dic = ObjFRSService.RunReport(PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, pkOrderId.ToString(), OrderDetailId.ToString()));
                        }
                        Response = Response_Dic["response"];
                        xDoc.InnerXml = Response_Dic["response"];
                        if (xDoc.GetElementsByTagName("error").Count > 0)//If any error found in up coming response.
                        {
                            Response_Dic["error"] = xDoc.GetElementsByTagName("error").Item(0).Value;//If any error found in response.
                            UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("3"));//in-complete if any error found.
                        }
                    }
                    catch (Exception ex)
                    {
                        //Added log for exception handling.
                        VendorServices.GeneralService.WriteLog("Some Error Occured in GetXMLResponse(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    }
                    finally
                    {
                        ObjFRSService = null;
                    }
                }
                if (ProductCode == "SNS")
                {
                    SocialDiligenceService ObjSocialDiligenceService = new SocialDiligenceService();
                    try
                    {
                        Response_Dic = ObjSocialDiligenceService.RunReport(PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, pkOrderId.ToString(), OrderDetailId.ToString()));
                        Response = Response_Dic["response"];
                        Error = Response_Dic["error"];
                        if (Response == "" && IsInstantReport)
                        {
                            UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("3"));
                        }
                    }
                    catch (Exception ex)
                    {
                        //Added log for exception handling.
                        VendorServices.GeneralService.WriteLog("Some Error Occured in GetXMLResponse(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    }
                    finally
                    {
                        ObjSocialDiligenceService = null;
                    }
                }

                #region Add new reports PS3,TDR2, EMV2, PL2

                if (ProductCode == "PS3" || ProductCode == "TDR2" || ProductCode == "EMV2" || ProductCode == "PL2")
                {
                    MerlinData ObjMerlinData = new MerlinData();
                    try
                    {
                        Response_Dic = ObjMerlinData.RunReport(PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, pkOrderId.ToString(), OrderDetailId.ToString()));
                        Response = Response_Dic["response"];
                        Error = Response_Dic["error"];
                        if (Response == "" && IsInstantReport)
                        {
                            UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("3"));
                        }
                    }
                    catch (Exception ex)
                    {
                        //Added log for exception handling.
                        VendorServices.GeneralService.WriteLog("Some Error Occured in GetXMLResponse(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    }
                    finally
                    {
                        ObjMerlinData = null;
                    }
                }

                #endregion

                if (Error != "")
                {
                    BALGeneral.LogError(OrderDetailId, Error);
                }

                #region Send Email For Incomplete Reports
                if (Response == "" && !Error.ToLower().Contains("timed out"))
                {
                    //   SendErrorEmail(pkOrderId, Error, ProductCode, 3, OrderDetailId);
                }
                #endregion
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in GetXMLResponse(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

                ObjtblReportErrorLog = new tblReportErrorLog();
                ObjtblReportErrorLog.fkProductId = 0;
                ObjtblReportErrorLog.fkCompanyUserId = _mCompanyUserId;
                ObjtblReportErrorLog.ReportError = ex.Message.ToString();
                ObjtblReportErrorLog.fkOrderId = pkOrderId;
                Response = ex.Message.ToString();
                ObjBALGeneral.AddReportErrorToErrorLog(ObjtblReportErrorLog);
            }
            finally
            {
                ObjBALGeneral = null;
                ObjtblReportErrorLog = null;
            }
            return Response;
        }

        /// <summary>
        /// To get the Order detail by Order Id //INT-15
        /// </summary>
        /// <param name="OrderId"></param>
        /// <returns></returns>
        public string GetFRSOrderDetailByVendor(string OrderId)
        {
            StringBuilder ObjStringBuilder_rootNode = new StringBuilder();
            try
            {
                ObjStringBuilder_rootNode.Append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
                ObjStringBuilder_rootNode.Append("<XML>");
                ObjStringBuilder_rootNode.Append("<login>");
                ObjStringBuilder_rootNode.Append("<account>INTSCR</account>");
                ObjStringBuilder_rootNode.Append("<username>admin</username>");
                ObjStringBuilder_rootNode.Append("<password>Intelifi123</password>");
                ObjStringBuilder_rootNode.Append("</login>");
                ObjStringBuilder_rootNode.Append("<mode>PROD</mode>");
                ObjStringBuilder_rootNode.Append("<getOrderResults orderID='" + OrderId + "'/>");
                ObjStringBuilder_rootNode.Append("</XML>");
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in GetFRSOrderDetailByVendor(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                throw;
            }

            return Convert.ToString(ObjStringBuilder_rootNode);
        }




        /// <summary>
        /// Function To Pass the Parameters To the General Class Object.
        /// </summary>
        /// <param name="ObjProc_Get_OrderSearchedDataResult"></param>
        /// <returns></returns>
        public GeneralService PassParams(Proc_GetAllProductsWithVendorResult ObjtblVendor, string Vendor_Request, string ProductCode, string pkOrderId, string pkOrderDetailId)
        {
            GeneralService ObjGeneralService = new GeneralService();

            string AdditionalParam = "";

            if (ObjtblVendor.AdditionalParams != null)
            {
                AdditionalParam = ObjtblVendor.AdditionalParams.ToString();
            }
            ObjGeneralService.ProductCode = ProductCode;
            ObjGeneralService.Request = Vendor_Request;
            ObjGeneralService.AdditionalParams = AdditionalParam;
            ObjGeneralService.ServicePassword = ObjtblVendor.ServicePassword;
            ObjGeneralService.ServiceUrl = ObjtblVendor.ServiceUrl;
            ObjGeneralService.ServiceUserId = ObjtblVendor.ServiceUserId;
            ObjGeneralService.OrderId = pkOrderId;
            ObjGeneralService.OrderDetailId = pkOrderDetailId;
            return ObjGeneralService;
        }

        /// <summary>
        /// To update the Order status.
        /// </summary>
        /// <param name="pkOrderId"></param>
        /// <param name="pkOrderDetailId"></param>
        /// <param name="ReportStatus"></param>
        public void UpdateReportStatus(int? pkOrderId, int? pkOrderDetailId, byte ReportStatus)
        {
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                //To update the Order status.
                dx.UpdateOrderStatus(pkOrderId, pkOrderDetailId, ReportStatus);
                //Send complete mail. 
                var OrderStatus_status = dx.tblOrders.Where(o => o.pkOrderId == pkOrderId).FirstOrDefault();
                //if order exists.
                if (OrderStatus_status != null)
                {
                    //if has completed.
                    if (OrderStatus_status.OrderStatus == 2)
                    {
                        //To call send mail process if order has completed.
                        SendMail_AfterCompletionOfOrder(pkOrderId, pkOrderDetailId);
                    }
                }
            }
        }
        /// <summary>
        /// To send mail after completion of order.
        /// </summary>
        /// <param name="pkOrderId"></param>
        /// <param name="pkOrderDetailId"></param>
        public void SendMail_AfterCompletionOfOrder(int? pkOrderId, int? pkOrderDetailId)
        {
            string strLog = string.Empty;
            string fileNameforLog = "CompleteOrderMailLogs_" + DateTime.Now.ToString("ddMMyyyy");
            strLog = DateTime.Now.ToString() + " **********************Start SendMail_AfterCompletionOfOrder********************\n";
            strLog = strLog + DateTime.Now.ToString() + " OrderId : " + Convert.ToString(pkOrderId) + "\n";
            try
            {
                #region System can send mail to user
                bool isSend = true;
                int? fkCompanyUserId = 0;
                // this condition is set if user off email notification from preferences 
                using (EmergeDALDataContext dx = new EmergeDALDataContext())
                {
                    fkCompanyUserId = dx.tblOrders.Where(d => d.pkOrderId == pkOrderId).Select(d => d.fkCompanyUserId).FirstOrDefault();
                    strLog = strLog + DateTime.Now.ToString() + " Company User Id : " + Convert.ToString(fkCompanyUserId) + "\n";
                    if (dx.tblEmailNotificationAlerts.Where(d => d.fkCompanyUserId == fkCompanyUserId).Count() > 0)
                    {
                        //If user enabled for notification.
                        isSend = dx.tblEmailNotificationAlerts.Where(d => d.fkCompanyUserId == fkCompanyUserId && d.fkTemplateId == 74).Select(d => d.IsSendEmail).FirstOrDefault();
                    }
                }
                #endregion
                strLog = strLog + DateTime.Now.ToString() + " user enabled for notification : " + Convert.ToString(isSend) + "\n";
                if (isSend)//If user enabled for notification.
                {
                    Proc_GetCompleteOrderByOrderIdResult ObjReport = new Proc_GetCompleteOrderByOrderIdResult();
                    string Subject = "";
                    //To get the complete order details by order id.
                    using (EmergeDALDataContext dx = new EmergeDALDataContext())
                    {
                        ObjReport = dx.Proc_GetCompleteOrderByOrderId(pkOrderId).FirstOrDefault();
                    }
                    //To get the Logged on user roles based on user id.
                    string RoleName = BLLMembership.GetUserRoleByUsername(ObjReport.UserName);
                    strLog = strLog + DateTime.Now.ToString() + " RoleName : " + Convert.ToString(RoleName) + "\n";
                    string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + BALGeneral.GetGeneralSettings().WebSiteLogo;
                    int pkTemplateId = 74;
                    var emailContent = new BALGeneral().GetEmailTemplate(new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852"), pkTemplateId);//"Complete Report"
                    string emailText = emailContent.TemplateContent;
                    #region New way Bookmark
                    string MessageBody = string.Empty;
                    CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
                    TextInfo textInfo = cultureInfo.TextInfo;
                    string firstname = ObjReport.SearchedFirstName != null ? textInfo.ToTitleCase(ObjReport.SearchedFirstName) : string.Empty;
                    string lastname = ObjReport.SearchedLastName != null ? textInfo.ToTitleCase(ObjReport.SearchedLastName) : string.Empty;
                    #endregion
                    Subject = emailContent.TemplateSubject;
                    MessageBody = emailText.Replace("%ApplicantName%", firstname + " " + lastname);
                    string Rolename = "";
                    Rolename = BLLMembership.GetUserRoleByUsername(ObjReport.Email);
                    string UserEmails = string.Empty;
                    if (!string.IsNullOrEmpty(ObjReport.Email))
                    {
                        UserEmails = new BALGeneral().GetUserOtherMailIds(ObjReport.Email);
                        //if no other mail exists than default user mail will be emergereview@usaintel.com.
                        if (UserEmails == string.Empty)
                        {
                            UserEmails = "emergereview@usaintel.com";
                        }
                        strLog = strLog + DateTime.Now.ToString() + " To email Id : " + Convert.ToString(UserEmails) + "\n";
                    }
                    strLog = strLog + DateTime.Now.ToString() + " To email Id : " + Convert.ToString(ObjReport.Email) + "\n";
                    strLog = strLog + DateTime.Now.ToString() + " SendMail started...... \n";
                    //Call send mail process.
                    SendEmailForCompletion(ObjReport.Email, UserEmails, MessageBody, Subject, Convert.ToString(pkOrderId));
                    strLog = strLog + DateTime.Now.ToString() + " SendMail  ended \nEnd SendMail_AfterCompletionOfOrder...... \n";
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                strLog = DateTime.Now.ToString() + " Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace + "\n";
                BALGeneral.WriteLogForDeletion(strLog, fileNameforLog);
            }
            finally
            {
                BALGeneral.WriteLogForDeletion(strLog, fileNameforLog);
            }
        }
        /// <summary>
        /// update 7-year filter for all state for ticket #85
        /// </summary>
        /// <param name="filter_id"></param>
        /// <param name="IsEnabledForAllState"></param>
        public void Update7YearFilterForAllStates(int filter_id, bool IsEnabledForAllState)
        {
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                tblFilter ObjTblFilter = new tblFilter();
                ObjTblFilter = dx.tblFilters.Where(f => f.pkFilterId == filter_id).FirstOrDefault();
                if (ObjTblFilter != null)
                {
                    ObjTblFilter.IsEnableForAllState = IsEnabledForAllState;
                    dx.SubmitChanges();
                }
            }
        }

        public void PlaceNewOrderForLiveRunnerCancelledReport(int? OrderId, int? OrderDetailId, string ProductCode, string CompanyAccountNo, string OrderCompanyName, string PermissiblePurpose)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                try
                {
                    /*DbTransaction DbTrans = null;
                    DbTrans = DX.Connection.BeginTransaction();*/

                    #region Get The Saved Searched Info to place a new Order

                    tblOrder ObjtblOrder = DX.tblOrders.Where(db => db.pkOrderId == OrderId).FirstOrDefault();
                    tblOrderSearchedData ObjtblOrderSearchedData = DX.tblOrderSearchedDatas.Where(db => db.fkOrderId == OrderId).FirstOrDefault();
                    List<tblOrderSearchedEmploymentInfo> ObjCollOrderSearchedEmploymentInfo = DX.tblOrderSearchedEmploymentInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    List<tblOrderSearchedEducationInfo> ObjCollOrderSearchedEducationInfo = DX.tblOrderSearchedEducationInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    List<tblOrderPersonalQtnInfo> ObjCollOrderPersonalQtnInfo = DX.tblOrderPersonalQtnInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    List<tblOrderSearchedPersonalInfo> ObjCollOrderSearchedPersonalInfo = DX.tblOrderSearchedPersonalInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    List<tblOrderSearchedProfessionalInfo> ObjCollOrderSearchedProfessionalInfo = DX.tblOrderSearchedProfessionalInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    List<tblOrderSearchedLicenseInfo> ObjCollOrderLicenseInfo = DX.tblOrderSearchedLicenseInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    List<tblOrderSearchedAliasPersonalInfo> ObjCollOrderSearchedAliasPersonalInfo = DX.tblOrderSearchedAliasPersonalInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    List<tblOrderSearchedAliasLocationInfo> ObjCollOrderSearchedAliasLocationInfo = DX.tblOrderSearchedAliasLocationInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    List<tblOrderSearchedAliasWorkInfo> ObjCollOrderSearchedAliasWorkInfo = DX.tblOrderSearchedAliasWorkInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    tblOrderSearchedLiveRunnerInfo ObjOrderSearchedLiveRunnerInfo = DX.tblOrderSearchedLiveRunnerInfos.Where(db => db.fkOrderId == OrderId && db.fkOrderDetailId == OrderDetailId).FirstOrDefault();
                    List<tblVendor> ObjtblVendor_List = DX.tblVendors.ToList();
                    List<tblState> ObjtblState_List = DX.tblStates.ToList();

                    #endregion

                    #region Creating New Request
                    //BALProducts ObjBALProducts = new BALProducts();
                    int StateId = 0;
                    string LiveRunnerCounty = "";
                    if (!ProductCode.ToLower().Contains("ccr"))
                    {
                        StateId = Convert.ToInt32(ObjtblOrderSearchedData.SearchedStateId) == null ? 0 : Convert.ToInt32(ObjtblOrderSearchedData.SearchedStateId);
                    }
                    else
                    {
                        if (ObjOrderSearchedLiveRunnerInfo != null)
                        {
                            LiveRunnerCounty = ObjOrderSearchedLiveRunnerInfo.LiveRunnerCounty;
                        }
                        StateId = Convert.ToInt32(ObjOrderSearchedLiveRunnerInfo.LiveRunnerState);
                    }
                    string StateCode = DX.tblStates.Where(db => db.pkStateId == StateId).FirstOrDefault().StateCode;

                    List<Proc_GetAllProductsWithVendorResult> ObjProc_GetAllProductsWithVendorResult = GetAllProductsWithVendor();

                    //string SearchedStateCode = (!string.IsNullOrEmpty(ObjtblOrderSearchedData.SearchedStateId.ToString()) ? ObjtblState_List.Where(data => data.pkStateId == ObjtblOrderSearchedData.SearchedStateId).First().StateCode : "");

                    string SearchedDlStateCode = (!string.IsNullOrEmpty(ObjtblOrderSearchedData.SearchedDLStateId.ToString()) ? ObjtblState_List.Where(data => data.pkStateId == ObjtblOrderSearchedData.SearchedDLStateId).First().StateCode : "");

                    Guid SiteApplicationId = new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852");
                    string Vendor_Request = "";
                    Vendor_Request = CreateXMLRequest(ObjtblOrder, ProductCode, ObjtblOrderSearchedData, ObjCollOrderSearchedEmploymentInfo,
                                                                                                    ObjCollOrderSearchedEducationInfo,
                                                                                                    ObjCollOrderSearchedPersonalInfo,
                                                                                                    ObjCollOrderSearchedProfessionalInfo,
                                                                                                        ObjCollOrderPersonalQtnInfo,
                                                                                                        ObjCollOrderLicenseInfo,
                                                                                                        ObjCollOrderSearchedAliasPersonalInfo,
                                                                                                        ObjCollOrderSearchedAliasLocationInfo,
                                                                                                        ObjCollOrderSearchedAliasWorkInfo,
                                                                                                        SiteApplicationId,
                                                                                                        OrderId,
                                                                                                        OrderDetailId,
                                                                                                        ObjtblVendor_List,
                                                                                                        ObjtblState_List,
                                                                                                        StateCode,
                                                                                                        SearchedDlStateCode,
                                                                                                        LiveRunnerCounty,
                                                                                                        ObjProc_GetAllProductsWithVendorResult,
                                                                                                        0, CompanyAccountNo, OrderCompanyName, PermissiblePurpose);
                    int? Obj_pkOrderRequest = 0;
                    //int iResultRequest = 
                    UpdateOrderRequestData(OrderDetailId, Vendor_Request); /* Add entry in order request table */

                    // string CocatRequest = ProductCode + "^" + Vendor_Request + "^" + Obj_pkOrderRequest;

                    #endregion

                    #region Getting Response


                    string vendor_Res = "";
                    string Vendor_Req = "";

                    vendor_Res = GetXMLResponse(ProductCode, ObjProc_GetAllProductsWithVendorResult, Vendor_Request, OrderId, OrderDetailId, false);
                    if (vendor_Res.Contains("<XML_INTERFACE>ZZZ</XML_INTERFACE>"))
                    {
                        string[] Vendor_Response_Parts = vendor_Res.Split('#');
                        vendor_Res = Vendor_Response_Parts[1].ToString();
                        Vendor_Req = Vendor_Response_Parts[0].ToString();

                        UpdateRequestLRCancelled(OrderDetailId, Vendor_Req);
                    }

                    //strmail += vendor_Res + "---------------------";


                    using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                    {
                        UpdateOrderResponse(OrderDetailId, vendor_Res);
                    }

                    CheckResponseandUpdateStatus(ProductCode, OrderId, vendor_Res, OrderDetailId, false, CompanyAccountNo, OrderCompanyName, PermissiblePurpose, false);
                    vendor_Res = "";
                    Vendor_Req = "";

                    #endregion

                }
                catch (Exception ex)
                {
                    //Added log for exception handling.
                    VendorServices.GeneralService.WriteLog("Some Error Occured in PlaceNewOrderForLiveRunnerCancelledReport(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    throw;
                }
            }
        }

        public int UpdateOrderRequestData(int? OrderDetailId, string Vendor_Request)
        {
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    int Status = -1;
                    tblOrderRequestData ObjRqData = DX.tblOrderRequestDatas.Where(db => db.fkOrderDetailId == OrderDetailId).First();
                    if (ObjRqData != null)
                    {
                        ObjRqData.RequestData = Vendor_Request;
                        DX.SubmitChanges();
                        Status = 1;
                    }
                    return Status;
                }

            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateOrderRequestData(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                throw;
            }
        }

        public void UpdateRequestLRCancelled(int? OrderDetailId, string Vendor_Req)
        {
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    tblOrderRequestData ObjRqData = DX.tblOrderRequestDatas.Where(db => db.fkOrderDetailId == OrderDetailId).FirstOrDefault();
                    if (ObjRqData != null)
                    {
                        ObjRqData.RequestData = Vendor_Req + "$#$#";
                        DX.SubmitChanges();
                    }
                }

            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateRequestLRCancelled(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                throw;
            }
        }

        /// <summary>
        /// Function to Update Secure Key in case of Manual Reports .
        /// </summary>
        /// <param name="OrderDetailId"></param>
        /// <param name="p"></param>
        public void UpdateRefSecureKey(int? OrderDetailId, string RefSecureKey)
        {
            using (EmergeDALDataContext ObjUSAIntelDataDataContext = new EmergeDALDataContext())
            {
                var Result = ObjUSAIntelDataDataContext.tblOrderDetails.Single(data => data.pkOrderDetailId == OrderDetailId);
                Result.RefSecureKey = RefSecureKey;
                ObjUSAIntelDataDataContext.SubmitChanges();
            }
        }


        private void SaveLiveRunnerErrorReponse(int? OrderDetailId, string Vendor_Response)
        {
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    if (DX.tblOrderResponseDatas.Where(db => db.fkOrderDetailId == OrderDetailId).Count() > 0)
                    {
                        tblOrderResponseData ObjResponseData = DX.tblOrderResponseDatas.Where(db => db.fkOrderDetailId == OrderDetailId).FirstOrDefault();
                        if (ObjResponseData != null)
                        {
                            ObjResponseData.ErrorResponseData = Vendor_Response;
                            DX.SubmitChanges();
                        }
                    }
                    else
                    {
                        tblOrderResponseData_Archieve ObjResponseData = DX.tblOrderResponseData_Archieves.Where(db => db.fkOrderDetailId == OrderDetailId).FirstOrDefault();
                        if (ObjResponseData != null)
                        {
                            ObjResponseData.ErrorResponseData = Vendor_Response;
                            DX.SubmitChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in SaveLiveRunnerErrorReponse(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
        }


        /// <summary>
        /// Create XML Request For Microbilt For People Search
        /// </summary>
        /// <param name="ObjGetOrderSearchedDataResult"></param>
        /// <returns></returns>
        //public string BuildXMLForPeopleSearch(int? pkOrderId, tblOrderSearchedData ObjtblOrderSearchedData, string ProductCode, tblVendor ObjtblVendor_single, string SearchedStateCode)
        //{

        //    BALGeneral ObjBALGeneral = new BALGeneral();

        //    /*if (ObjBALGeneral.isValidDateTime(ObjtblOrderSearchedData.SearchedDob) == true)
        //    {

        //        DateTime DOB = DateTime.Now;
        //        if (ObjtblOrderSearchedData.SearchedDob != "")
        //        {
        //            DOB = Convert.ToDateTime(ObjtblOrderSearchedData.SearchedDob);
        //        }

        //    }*/
        //    StringBuilder ObjXML = new StringBuilder();

        //    string username = ObjtblVendor_single.ServiceUserId.ToString();
        //    string password = ObjtblVendor_single.ServicePassword.ToString();

        //    ObjXML.Append(ObjtblVendor_single.ServiceUrl + "?");
        //    ObjXML.Append("Member_ID=" + username + "&Password=" + password + "&Bureau=DS&Product=SBPSR");


        //    string Ssn_Length = (ObjtblOrderSearchedData.SearchedSsn != "___-__-____" && ObjtblOrderSearchedData.SearchedSsn != "") ? ObjtblOrderSearchedData.SearchedSsn.ToString().Replace("-", "") : "";

        //    ObjXML.Append("&LastName=" + ObjtblOrderSearchedData.SearchedLastName.ToString().ToUpper());
        //    ObjXML.Append("&FirstName=" + ObjtblOrderSearchedData.SearchedFirstName.ToString().ToUpper());
        //    ObjXML.Append("&State=" + SearchedStateCode.ToUpper());
        //    ObjXML.Append("&SSN=" + Ssn_Length);

        //    return ObjXML.ToString();
        //}


        public string BuildXMLForPeopleSearch(int? pkOrderId, tblOrderSearchedData ObjtblOrderSearchedData, string ProductCode, tblVendor ObjtblVendor_single, string SearchedStateCode)
        {

            BALGeneral ObjBALGeneral = new BALGeneral();

            /*if (ObjBALGeneral.isValidDateTime(ObjtblOrderSearchedData.SearchedDob) == true)
            {

                DateTime DOB = DateTime.Now;
                if (ObjtblOrderSearchedData.SearchedDob != "")
                {
                    DOB = Convert.ToDateTime(ObjtblOrderSearchedData.SearchedDob);
                }

            }*/
            StringBuilder ObjXML = new StringBuilder();

            string username = ObjtblVendor_single.ServiceUserId.ToString();
            string password = ObjtblVendor_single.ServicePassword.ToString();

            if (ProductCode.ToLower() != "ps")
            {
                ObjXML.Append(ObjtblVendor_single.ServiceUrl + "?");
                ObjXML.Append("Member_ID=" + username + "&Password=" + password + "&Bureau=DS&Product=SBPSR");


                string Ssn_Length = (ObjtblOrderSearchedData.SearchedSsn != "___-__-____" && ObjtblOrderSearchedData.SearchedSsn != "") ? ObjtblOrderSearchedData.SearchedSsn.ToString().Replace("-", "") : "";

                ObjXML.Append("&LastName=" + ObjtblOrderSearchedData.SearchedLastName.ToString().ToUpper());
                ObjXML.Append("&FirstName=" + ObjtblOrderSearchedData.SearchedFirstName.ToString().ToUpper());
                ObjXML.Append("&State=" + SearchedStateCode.ToUpper());
                ObjXML.Append("&SSN=" + Ssn_Length);
            }
            else
            {
                //ObjXML.Append("Login=" + username + "&Password=" + password + "&Bureau=DS&Product=SBPSR");


                string Ssn_Length = (ObjtblOrderSearchedData.SearchedSsn != "___-__-____" && ObjtblOrderSearchedData.SearchedSsn != "") ? ObjtblOrderSearchedData.SearchedSsn.ToString().Replace("-", "") : "";

                ObjXML.Append("<?xml version='1.0'?>");
                ObjXML.Append("<ieirequest>");
                ObjXML.Append("<order>");
                ObjXML.Append("<quoteback>PLACE ORDER test FACT</quoteback>");
                ObjXML.Append("<subject>");
                ObjXML.Append("<firstname>" + ObjtblOrderSearchedData.SearchedFirstName.ToString().ToUpper() + "</firstname>");
                ObjXML.Append("<middlename>" + ObjtblOrderSearchedData.SearchedMiddleInitial.ToString().ToUpper() + "</middlename>");
                ObjXML.Append("<lastname>" + ObjtblOrderSearchedData.SearchedLastName.ToString().ToUpper() + "</lastname>");
                ObjXML.Append("<ssn>" + Ssn_Length + "</ssn>");
                ObjXML.Append("<dob>" + ObjtblOrderSearchedData.SearchedDob + "</dob>");
                ObjXML.Append("<state>" + SearchedStateCode.ToUpper() + "</state>");
                ObjXML.Append("</subject>");
                ObjXML.Append("<product>");
                ObjXML.Append("<fact/>");
                ObjXML.Append("</product>");
                ObjXML.Append("</order>");
                ObjXML.Append("</ieirequest>");


            }

            return ObjXML.ToString();
        }


        /// <summary>
        /// Function to Generate Rapid Court Request .
        /// </summary>
        /// <param name="pkOrderId"></param>
        /// <param name="ObjGetOrderSearchedDataResult"></param>
        /// <param name="ObjCollOrderLicenseInfo"></param>
        /// <param name="ObjCollOrderPersonalQtnInfo"></param>
        /// <param name="ObjCollOrderSearchedEducationInfo"></param>
        /// <param name="ObjCollOrderSearchedEmploymentInfo"></param>
        /// <param name="ProductCode"></param>
        /// <param name="SearchedStateCode"></param>
        /// <returns></returns>
        private string GenerateRAPIDCOURTRequest(int? pkOrderId, tblOrderSearchedData ObjGetOrderSearchedDataResult, List<tblOrderSearchedLicenseInfo> ObjCollOrderLicenseInfo, List<tblOrderPersonalQtnInfo> ObjCollOrderPersonalQtnInfo, List<tblOrderSearchedEducationInfo> ObjCollOrderSearchedEducationInfo, List<tblOrderSearchedEmploymentInfo> ObjCollOrderSearchedEmploymentInfo, string ProductCode, string SearchedStateCode, string SearchedCountyCode, int IsNullDOB, string fName = "", string lName = "", string mName = "")
        {
            string ObjXML = string.Empty;
            bool ServiceInTestMode = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["ServiceInTestMode"]);
            if (!ServiceInTestMode)
            {
                ObjXML = Convert.ToString(GetRapidCourtLiveCode(pkOrderId, ObjGetOrderSearchedDataResult, ObjCollOrderLicenseInfo, ObjCollOrderPersonalQtnInfo, ObjCollOrderSearchedEducationInfo, ObjCollOrderSearchedEmploymentInfo, ProductCode, SearchedStateCode, SearchedCountyCode, IsNullDOB, fName = "", lName = "", mName = ""));
            }
            else
            {
                ObjXML = Convert.ToString(GetRapidCourtTestEnvironmentCode(pkOrderId, ObjGetOrderSearchedDataResult, ObjCollOrderLicenseInfo, ObjCollOrderPersonalQtnInfo, ObjCollOrderSearchedEducationInfo, ObjCollOrderSearchedEmploymentInfo, ProductCode, SearchedStateCode, SearchedCountyCode, IsNullDOB, fName = "", lName = "", mName = ""));
            }
            return ObjXML;
        }

        /// <summary>
        /// To get live environment code 
        /// </summary>
        public string GetRapidCourtLiveCode(int? pkOrderId, tblOrderSearchedData ObjGetOrderSearchedDataResult, List<tblOrderSearchedLicenseInfo> ObjCollOrderLicenseInfo, List<tblOrderPersonalQtnInfo> ObjCollOrderPersonalQtnInfo, List<tblOrderSearchedEducationInfo> ObjCollOrderSearchedEducationInfo, List<tblOrderSearchedEmploymentInfo> ObjCollOrderSearchedEmploymentInfo, string ProductCode, string SearchedStateCode, string SearchedCountyCode, int IsNullDOB, string fName = "", string lName = "", string mName = "")
        {
            string SearchedSSN = ObjGetOrderSearchedDataResult.SearchedSsn;
            Random randNum = new Random();
            int Randnum = randNum.Next(1000);
            //string ReferenceId = pkOrderId.ToString().Substring(0, 8) + Randnum.ToString() + "_" + ProductCode;
            string ReferenceId = pkOrderId.ToString() + Randnum.ToString() + "_" + ProductCode;

            StringBuilder ObjXML = new StringBuilder();

            ObjXML.Append("<?xml version='1.0' encoding='UTF-8'?>");
            if (ProductCode == "RCX" || ProductCode == "FCR")
            {
                ObjXML.Append("<BackgroundCheck>");
            }
            else
            {
                ObjXML.Append("<BackgroundCheck	xmlns=\"http://ns.hr-xml.org/2004-08-02\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://ns.hr-xml.org/2004-08-02 BackgroundCheck.xsd\">");
            }
            ObjXML.Append("<BackgroundSearchPackage>");

            ObjXML.Append("<PersonalData>");
            ObjXML.Append("<PersonName>");
            if (fName != "" || lName != "")
            {
                ObjXML.Append("<GivenName>" + fName + "</GivenName>");   //John
                ObjXML.Append("<MiddleName>" + mName + "</MiddleName>"); //D
                ObjXML.Append("<FamilyName>" + lName + "</FamilyName>");   //Doe
            }
            else
            {
                ObjXML.Append("<GivenName>" + ObjGetOrderSearchedDataResult.SearchedFirstName + "</GivenName>");   //John
                ObjXML.Append("<MiddleName>" + ObjGetOrderSearchedDataResult.SearchedMiddleInitial + "</MiddleName>"); //D
                ObjXML.Append("<FamilyName>" + ObjGetOrderSearchedDataResult.SearchedLastName + "</FamilyName>");   //Doe
            }
            ObjXML.Append("</PersonName>");
            ObjXML.Append("<DemographicDetail>");

            BALGeneral ObjBALGeneral = new BALGeneral();

            if (ObjBALGeneral.isValidDateTime(ObjGetOrderSearchedDataResult.SearchedDob) == true)
            {

                DateTime Now = System.DateTime.Now;

                DateTime ObjDateTime = ObjGetOrderSearchedDataResult.SearchedDob != "" ? Convert.ToDateTime(ObjGetOrderSearchedDataResult.SearchedDob) : Now;

                if (ObjDateTime != Now)
                {
                    ObjXML.Append("<DateOfBirth>" + ObjDateTime.ToString("yyyy-MM-dd") + "</DateOfBirth>");   //1955-01-31

                }
            }

            if (SearchedSSN != "___-__-____" && SearchedSSN != "")
            {
                string Country = "\"US\"";
                string SSN = "\"SSN\"";
                if (ProductCode == "RCX" || ProductCode == "FCR")
                    ObjXML.Append("<GovernmentId countryCode=" + Country + "  issuingAuthority=" + SSN + ">" + (SearchedSSN.Contains("-") ? SearchedSSN.Replace("-", "") : SearchedSSN) + "</GovernmentId>");
                else
                    ObjXML.Append("<GovernmentId countryCode='US' issuingAuthority='SSN'>" + (SearchedSSN.Contains("-") ? SearchedSSN.Replace("-", "") : SearchedSSN) + "</GovernmentId>"); //123456789
            }
            ObjXML.Append("</DemographicDetail>");
            ObjXML.Append("</PersonalData>");


            ObjXML.Append("<Screenings>");

            //Commented By Himesh Kumar
            //if (ProductCode == "PS")
            //{
            //    ObjXML.Append("<Screening type='x:addresstrace'>");
            //    ObjXML.Append("<ReferenceId><IdValue>" + ReferenceId + "</IdValue></ReferenceId>");
            //    ObjXML.Append("<Region>" + SearchedStateCode + "</Region>");     //NC
            //    ObjXML.Append("</Screening>");
            //}

            if (ProductCode == "NCR1")
            {
                ObjXML.Append("<Screening type='criminal'>");
                ObjXML.Append("<ReferenceId><IdValue>" + ReferenceId + "</IdValue></ReferenceId>");
                ObjXML.Append("<Region>COMP</Region>");     //NC
                if (IsNullDOB == 1)
                {
                    ObjXML.Append("<AdditionalItems type='dobnulls' >");
                    ObjXML.Append("<Text>True</Text>");
                    ObjXML.Append("</AdditionalItems>");
                }
                else
                {
                    ObjXML.Append("<AdditionalItems type='dobnulls' >");
                    ObjXML.Append("<Text>False</Text>");
                    ObjXML.Append("</AdditionalItems>");
                }
                ObjXML.Append("</Screening>");

            }

            if (ProductCode == "NCR+")
            {
                ObjXML.Append("<Screening type='criminal'>");
                ObjXML.Append("<ReferenceId><IdValue>" + ReferenceId + "</IdValue></ReferenceId>");
                ObjXML.Append("<Region>COMP_PLUS</Region>");     //NC
                ObjXML.Append("</Screening>");
            }

            if (ProductCode == "RCX" || ProductCode == "FCR")
            {
                string criminal = "\"criminal\"";
                string qualifier = "";
                string Source = "\"source\"";
                if (ProductCode == "FCR")
                {
                    qualifier = "\"county\"";
                    SearchedStateCode = "FS";
                }
                else
                    qualifier = '"' + SearchedCountyCode + '"';
                ObjXML.Append("<Screening type=" + criminal + " qualifier=" + qualifier + ">");
                ObjXML.Append("<Region>" + SearchedStateCode + "</Region>");
                ObjXML.Append("<SearchCriminal>");
                ObjXML.Append("<County>" + SearchedCountyCode + "</County>");
                ObjXML.Append("</SearchCriminal>");
                ObjXML.Append("<AdditionalItems type=" + Source + ">");
                ObjXML.Append("<Text>RCX</Text>");
                ObjXML.Append("</AdditionalItems>");
                ObjXML.Append("<ReferenceId>");
                ObjXML.Append("<IdValue>" + ReferenceId + "</IdValue>");
                ObjXML.Append("</ReferenceId>");
                ObjXML.Append("</Screening>");
            }

            ObjXML.Append("</Screenings>");

            ObjXML.Append("</BackgroundSearchPackage></BackgroundCheck>");

            return ObjXML.ToString();
        }

        /// <summary>
        /// To get rapid court test environment Code //INT70
        /// </summary>
        public string GetRapidCourtTestEnvironmentCode(int? pkOrderId, tblOrderSearchedData ObjGetOrderSearchedDataResult, List<tblOrderSearchedLicenseInfo> ObjCollOrderLicenseInfo, List<tblOrderPersonalQtnInfo> ObjCollOrderPersonalQtnInfo, List<tblOrderSearchedEducationInfo> ObjCollOrderSearchedEducationInfo, List<tblOrderSearchedEmploymentInfo> ObjCollOrderSearchedEmploymentInfo, string ProductCode, string SearchedStateCode, string SearchedCountyCode, int IsNullDOB, string fName = "", string lName = "", string mName = "")
        {
            string SearchedSSN = ObjGetOrderSearchedDataResult.SearchedSsn;
            Random randNum = new Random();
            int Randnum = randNum.Next(1000);
            //string ReferenceId = pkOrderId.ToString().Substring(0, 8) + Randnum.ToString() + "_" + ProductCode;
            string ReferenceId = pkOrderId.ToString() + Randnum.ToString() + "_" + ProductCode;

            StringBuilder ObjXML = new StringBuilder();

            ObjXML.Append("<?xml version='1.0' encoding='UTF-8'?>");
            if (ProductCode == "RCX")
            {
                ObjXML.Append("<BackgroundCheck>");
            }
            else if (ProductCode == "FCR")
            {
                // ObjXML.Append("<BackgroundCheck>");

                //INT-23
                ObjXML.Append("<BackgroundCheck	xmlns=\"http://ns.hr-xml.org/2004-08-02\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://ns.hr-xml.org/2004-08-02 BackgroundCheck.xsd\">");
                //INT-23
            }
            else
            {
                ObjXML.Append("<BackgroundCheck	xmlns=\"http://ns.hr-xml.org/2004-08-02\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://ns.hr-xml.org/2004-08-02 BackgroundCheck.xsd\">");
            }
            ObjXML.Append("<BackgroundSearchPackage>");

            ObjXML.Append("<PersonalData>");
            ObjXML.Append("<PersonName>");
            if (fName != "" || lName != "")
            {
                if (ProductCode == "RCX")
                {
                    ObjXML.Append("<GivenName>" + fName + "</GivenName>");   //John
                    ObjXML.Append("<MiddleName/>");
                    ObjXML.Append("<FamilyName>" + lName + "</FamilyName>");   //Doe
                }
                else
                {
                    ObjXML.Append("<GivenName>" + fName + "</GivenName>");   //John
                    ObjXML.Append("<MiddleName>" + mName + "</MiddleName>"); //D
                    ObjXML.Append("<FamilyName>" + lName + "</FamilyName>");   //Doe
                }
            }
            else
            {
                if (ProductCode == "RCX")
                {
                    ObjXML.Append("<GivenName>" + ObjGetOrderSearchedDataResult.SearchedFirstName + "</GivenName>");   //John
                    ObjXML.Append("<MiddleName/>"); //D
                    ObjXML.Append("<FamilyName>" + ObjGetOrderSearchedDataResult.SearchedLastName + "</FamilyName>");   //Doe
                }
                else
                {
                    ObjXML.Append("<GivenName>" + ObjGetOrderSearchedDataResult.SearchedFirstName + "</GivenName>");   //John
                    ObjXML.Append("<MiddleName>" + ObjGetOrderSearchedDataResult.SearchedMiddleInitial + "</MiddleName>"); //D
                    ObjXML.Append("<FamilyName>" + ObjGetOrderSearchedDataResult.SearchedLastName + "</FamilyName>");   //Doe
                }
            }
            ObjXML.Append("</PersonName>");
            ObjXML.Append("<DemographicDetail>");

            BALGeneral ObjBALGeneral = new BALGeneral();

            if (ObjBALGeneral.isValidDateTime(ObjGetOrderSearchedDataResult.SearchedDob) == true)
            {

                DateTime Now = System.DateTime.Now;

                DateTime ObjDateTime = ObjGetOrderSearchedDataResult.SearchedDob != "" ? Convert.ToDateTime(ObjGetOrderSearchedDataResult.SearchedDob) : Now;

                if (ObjDateTime != Now)
                {
                    ObjXML.Append("<DateOfBirth>" + ObjDateTime.ToString("yyyy-MM-dd") + "</DateOfBirth>");   //1955-01-31

                }
            }

            if (SearchedSSN != "___-__-____" && SearchedSSN != "")
            {
                string Country = "\"US\"";
                string SSN = "\"SSN\"";
                if (ProductCode == "RCX" || ProductCode == "FCR")
                    ObjXML.Append("<GovernmentId countryCode=" + Country + "  issuingAuthority=" + SSN + ">" + (SearchedSSN.Contains("-") ? SearchedSSN.Replace("-", "") : SearchedSSN) + "</GovernmentId>");
                else
                    ObjXML.Append("<GovernmentId countryCode='US' issuingAuthority='SSN'>" + (SearchedSSN.Contains("-") ? SearchedSSN.Replace("-", "") : SearchedSSN) + "</GovernmentId>"); //123456789
            }
            ObjXML.Append("</DemographicDetail>");
            ObjXML.Append("</PersonalData>");


            ObjXML.Append("<Screenings>");


            //Commented By Himesh Kumar
            //if (ProductCode == "PS")
            //{
            //    ObjXML.Append("<Screening type='x:addresstrace'>");
            //    ObjXML.Append("<ReferenceId><IdValue>" + ReferenceId + "</IdValue></ReferenceId>");
            //    ObjXML.Append("<Region>" + SearchedStateCode + "</Region>");     //NC
            //    ObjXML.Append("</Screening>");
            //}

            if (ProductCode == "NCR1")
            {
                ObjXML.Append("<Screening type='criminal'>");
                ObjXML.Append("<ReferenceId><IdValue>" + ReferenceId + "</IdValue></ReferenceId>");
                ObjXML.Append("<Region>COMP</Region>");     //NC
                if (IsNullDOB == 1)
                {
                    ObjXML.Append("<AdditionalItems type='dobnulls' >");
                    ObjXML.Append("<Text>True</Text>");
                    ObjXML.Append("</AdditionalItems>");
                }
                else
                {
                    ObjXML.Append("<AdditionalItems type='dobnulls' >");
                    ObjXML.Append("<Text>False</Text>");
                    ObjXML.Append("</AdditionalItems>");
                }
                ObjXML.Append("</Screening>");

            }

            if (ProductCode == "NCR+")
            {
                ObjXML.Append("<Screening type='criminal'>");
                ObjXML.Append("<ReferenceId><IdValue>" + ReferenceId + "</IdValue></ReferenceId>");
                ObjXML.Append("<Region>COMP_PLUS</Region>");     //NC
                ObjXML.Append("</Screening>");
            }

            if (ProductCode == "RCX" || ProductCode == "FCR")
            {
                string criminal = "\"criminal\"";
                string qualifier = "";
                string Source = "\"source\"";
                if (ProductCode == "FCR")
                {
                    qualifier = "\"county\"";
                    SearchedStateCode = "FS";
                }
                else
                    qualifier = '"' + SearchedCountyCode + '"';
                ObjXML.Append("<Screening type=" + criminal + " qualifier=" + qualifier + ">");
                ObjXML.Append("<Region>" + SearchedStateCode + "</Region>");
                ObjXML.Append("<SearchCriminal>");
                ObjXML.Append("<County>" + SearchedCountyCode + "</County>");
                ObjXML.Append("</SearchCriminal>");
                ObjXML.Append("<AdditionalItems type=" + Source + ">");
                ObjXML.Append("<Text>RCX</Text>");
                ObjXML.Append("</AdditionalItems>");
                ObjXML.Append("<ReferenceId>");
                ObjXML.Append("<IdValue>" + ReferenceId + "</IdValue>");
                ObjXML.Append("</ReferenceId>");
                ObjXML.Append("</Screening>");
            }

            ObjXML.Append("</Screenings>");

            ObjXML.Append("</BackgroundSearchPackage></BackgroundCheck>");

            return ObjXML.ToString();
        }



        /// <summary>
        /// Function To Generate Xpedite Request .
        /// </summary>
        /// <param name="pkOrderId"></param>
        /// <param name="ObjGetOrderSearchedDataResult"></param>
        /// <param name="ObjCollOrderLicenseInfo"></param>
        /// <param name="ObjCollOrderPersonalQtnInfo"></param>
        /// <param name="ObjCollOrderSearchedEducationInfo"></param>
        /// <param name="ObjCollOrderSearchedEmploymentInfo"></param>
        /// <param name="ObjtblVendor_single"></param>
        /// <param name="ProductCode"></param>
        /// <param name="SearchedStateCode"></param>
        /// <param name="SearchedCountyName"></param>
        /// <returns></returns>
        private string GenerateXPEDITERequest(int? pkOrderId, tblOrderSearchedData ObjGetOrderSearchedDataResult, List<tblOrderSearchedLicenseInfo> ObjCollOrderLicenseInfo, List<tblOrderPersonalQtnInfo> ObjCollOrderPersonalQtnInfo, List<tblOrderSearchedEducationInfo> ObjCollOrderSearchedEducationInfo, List<tblOrderSearchedEmploymentInfo> ObjCollOrderSearchedEmploymentInfo, tblVendor ObjtblVendor_single, string ProductCode, string SearchedStateCode, string SearchedCountyName, string CompanyAccountNo, string OrderCompanyName, string PermissiblePurpose, string fName = "", string lName = "", string mName = "")
        {

            StringBuilder ObjXML = new StringBuilder();

            ObjXML.Append("<?xml version='1.0' encoding='utf-8'?>");
            ObjXML.Append("<FastraxRequests>");
            ObjXML.Append("<request>");
            ObjXML.Append("<subject-info>");
            if (lName != "" || fName != "")
            {
                ObjXML.Append("<first-name>" + fName + "</first-name>");
                ObjXML.Append("<last-name>" + lName + "</last-name>");
            }
            else
            {
                ObjXML.Append("<first-name>" + ObjGetOrderSearchedDataResult.SearchedFirstName + "</first-name>");
                ObjXML.Append("<last-name>" + ObjGetOrderSearchedDataResult.SearchedLastName + "</last-name>");
            }
            BALGeneral ObjBALGeneral = new BALGeneral();

            if (ObjBALGeneral.isValidDateTime(ObjGetOrderSearchedDataResult.SearchedDob) == true)
            {

                DateTime Now = System.DateTime.Now;

                DateTime ObjDateTime = ObjGetOrderSearchedDataResult.SearchedDob != "" ? Convert.ToDateTime(ObjGetOrderSearchedDataResult.SearchedDob) : Now;

                if (ObjDateTime != Now)
                {
                    ObjXML.Append("<dob>" + ObjDateTime.ToString("yyyy-MM-dd") + "</dob>");    //1970-10-15

                }
            }


            if (ObjGetOrderSearchedDataResult.SearchedSsn != "___-__-____" && ObjGetOrderSearchedDataResult.SearchedSsn != "")
            {
                if (ObjGetOrderSearchedDataResult.SearchedSsn.Length == 9)
                {
                    ObjGetOrderSearchedDataResult.SearchedSsn = ObjGetOrderSearchedDataResult.SearchedSsn.Insert(3, "-").Insert(6, "-");
                }
                ObjXML.Append("<ssn>" + ObjGetOrderSearchedDataResult.SearchedSsn + "</ssn>");   //445-97-9990
            }
            if (ObjGetOrderSearchedDataResult.SearchedSsn == "")
            {
                ObjXML.Append("<ssn>111-11-1111</ssn>");// default value of ssn = 111-11-1111 (ticket #937: NCR2 report is getting error message)
            }

            ObjGetOrderSearchedDataResult.SearchedSex = ObjGetOrderSearchedDataResult.SearchedSex.ToLower() == "male" ? "M" : "F";

            ObjXML.Append("<sex>U</sex>");  //M  

            //Added note field for ticket #10
            ObjXML.Append("<notes>Minimum of 10 Year Search Scope.</notes>");
            ObjXML.Append("</subject-info>");
            ObjXML.Append("<report-info>");
            if (ProductCode == "CCR1")
            {
                ObjXML.Append("<individual-report ind-report-id='4'>");
                ObjXML.Append("<criminalcounty>");
                ObjXML.Append("<location-info>");
                ObjXML.Append("<state>" + SearchedStateCode.ToString() + "</state>");   //GA
                ObjXML.Append("<county>" + SearchedCountyName + "</county>");  //Dekalb
                ObjXML.Append("</location-info>");
                ObjXML.Append("</criminalcounty>");
                ObjXML.Append("</individual-report>");
            }
            else if (ProductCode == "SCR")
            {
                ObjXML.Append("<individual-report ind-report-id='3'>");
                ObjXML.Append("<criminalstate>");
                ObjXML.Append("<location-info>");
                ObjXML.Append("<state>" + SearchedStateCode.ToString() + "</state>");   //GA
                ObjXML.Append("</location-info>");
                ObjXML.Append("</criminalstate>");
                ObjXML.Append("</individual-report>");
            }

            else if (ProductCode == "NCR2")
            {
                ObjXML.Append("<individual-report ind-report-id='24'>");
                ObjXML.Append("<searchamerica></searchamerica>");
                ObjXML.Append("</individual-report>");
            }

            ObjXML.Append("</report-info>");

            #region Permmisible purpose

            ObjXML.Append("<client-field><companyname>" + OrderCompanyName + "</companyname><permissiblepurpose>" + PermissiblePurpose + "</permissiblepurpose></client-field>");

            #endregion

            ObjXML.Append("</request>");
            ObjXML.Append("</FastraxRequests>");

            return ObjXML.ToString();
        }

        /// <summary>
        /// Function to Generate Softech Request.
        /// </summary>
        /// <param name="pkOrderId"></param>
        /// <param name="ObjGetOrderSearchedDataResult"></param>
        /// <param name="ObjCollOrderLicenseInfo"></param>
        /// <param name="ObjCollOrderPersonalQtnInfo"></param>
        /// <param name="ObjCollOrderSearchedEducationInfo"></param>
        /// <param name="ObjCollOrderSearchedEmploymentInfo"></param>
        /// <param name="ObjtblVendor_single"></param>
        /// <param name="ProductCode"></param>
        /// <param name="SearchedStateCode"></param>
        /// <returns></returns>
        private string GenerateSOFTECHRequest(int? pkOrderId, int? pkOrderDetailId, tblOrderSearchedData ObjGetOrderSearchedDataResult, List<tblOrderSearchedLicenseInfo> ObjCollOrderLicenseInfo, List<tblOrderPersonalQtnInfo> ObjCollOrderPersonalQtnInfo, List<tblOrderSearchedEducationInfo> ObjCollOrderSearchedEducationInfo, List<tblOrderSearchedEmploymentInfo> ObjCollOrderSearchedEmploymentInfo, tblVendor ObjtblVendor_single, string ProductCode, string SearchedStateCode, string SearchedDlStateCode, string CompanyAccountNo, string OrderCompanyName, string PermissiblePurpose)
        {
            // StringBuilder ObjXML = new StringBuilder();
            BALGeneral ObjBALGeneral = new BALGeneral();
            //  string OrderState = (SearchedStateCode != "" ? SearchedStateCode : SearchedDlStateCode);
            string OrderState = SearchedDlStateCode;
            #region Code To fetch Account Credentials
            string username = ObjtblVendor_single.ServiceUserId.ToString();
            string password = ObjtblVendor_single.ServicePassword.ToString();
            string url = string.Empty;
            //This functionality only for some states.
            //These states does not provide instant response service.
            if (OrderState == "PA" || OrderState == "HI" || OrderState == "OR" || OrderState == "AK")//MVR_Non_Instant
            {
                url = ObjtblVendor_single.ServiceUrl.ToString() + "/SendOrders";//MVR_Non_Instant

            }
            else
            {
                url = ObjtblVendor_single.ServiceUrl.ToString() + "/OrderInteractive";
            }
            #endregion
            string lQueryString = string.Empty;

            string vendorProductCodeMVR = string.Empty;
            //string vendorProductCodeCDLIS = string.Empty;
            string firstName = string.Empty; ;
            string middleName = string.Empty;
            string lastName = string.Empty;
            string licenseNo = string.Empty;
            string ssn = "";
            string DpaCode = "";
            string dob = string.Empty;
            string gender = string.Empty;
            string state = string.Empty;
            string policyRef = string.Empty;
            int Chistory1 = 0;
            if (ProductCode == "MVR" || ProductCode == "CDLIS")
            {
                if (ProductCode == "MVR")
                {
                    vendorProductCodeMVR = "MVRUSA";
                    policyRef = Convert.ToString(pkOrderId) + "_MVR_" + Convert.ToString(pkOrderDetailId);
                }
                else
                {
                    vendorProductCodeMVR = "CDLUSA";
                    policyRef = Convert.ToString(pkOrderId) + "_CDLIS_" + Convert.ToString(pkOrderDetailId);
                }
                if (ObjGetOrderSearchedDataResult.SearchedFirstName != "")
                {
                    firstName = ObjGetOrderSearchedDataResult.SearchedFirstName;
                }
                if (ObjGetOrderSearchedDataResult.SearchedMiddleInitial != "")
                {
                    middleName = ObjGetOrderSearchedDataResult.SearchedMiddleInitial;
                }
                if (ObjGetOrderSearchedDataResult.SearchedLastName != "")
                {
                    lastName = ObjGetOrderSearchedDataResult.SearchedLastName;
                }
                if (ObjGetOrderSearchedDataResult.SearchedDriverLicense != "")
                {
                    licenseNo = ObjGetOrderSearchedDataResult.SearchedDriverLicense;
                }
                if (ObjGetOrderSearchedDataResult.SearchedSsn != "___-__-____" && ObjGetOrderSearchedDataResult.SearchedSsn != "")
                {
                    ssn = ObjGetOrderSearchedDataResult.SearchedSsn.Contains("-") ? ObjGetOrderSearchedDataResult.SearchedSsn.Replace("-", "") : ObjGetOrderSearchedDataResult.SearchedSsn;
                }
                if (ObjBALGeneral.isValidDateTime(ObjGetOrderSearchedDataResult.SearchedDob) == true)
                {
                    DateTime Now = System.DateTime.Now;
                    DateTime ObjDateTime = ObjGetOrderSearchedDataResult.SearchedDob != "" ? Convert.ToDateTime(ObjGetOrderSearchedDataResult.SearchedDob) : Now;
                    if (ObjDateTime != Now)
                    {
                        dob = ObjDateTime.ToString("MMddyy");
                    }
                }
                if (ObjGetOrderSearchedDataResult.SearchedSex.ToLower() == "male")
                {
                    gender = "M";
                }
                else if (ObjGetOrderSearchedDataResult.SearchedSex.ToLower() == "female")
                {
                    gender = "F";
                }


                if (OrderState == "PA") { Chistory1 = 10; }
                else if (OrderState == "FL") { Chistory1 = 7; } //change acc. to Ticket #880: Need to change for FL MVR report 
                else if (OrderState == "AR") { Chistory1 = 2; } //change acc. to Ticket #913: Need to change for AR MVR report 
                else if (OrderState == "TC" && ProductCode == "MVR") { Chistory1 = 5; OrderState = "TX"; } //change acc. to Ticket #331: If the user selects "Texas + CDLIS" it should send 5 Year to vendor request
                else { Chistory1 = 3; }
                if (OrderState == "OK" || OrderState == "SD")
                {
                    DpaCode = "03";
                }
                else if (OrderState == "UT")
                {
                    DpaCode = "13";
                }
                else
                {
                    DpaCode = "09";
                }

                state = OrderState;
                lQueryString = string.Format("PROCESS=NEWREQ&CUSER={0}&CCODE={1}&CPASS={2}&CUSTOMOUTPUTTYPE={3}&ORDERSTATE={4}&CLICENSE1={5}&CSOCIAL1={6}&CFNAME1={7}&CMIDDLE1={8}&CLNAME1={9}&CDATE1={10}&CSEX1={11}&CPOLICY1={12}&DPPACODE={13}&CHISTORY1={14}&NAIC={15}&MVRMODE={16}&CDLIS={17}&XMLVERSION={18}&EMBEDDEDREPORTS={19}&SCORETYPE={20}", username, "D1266", password, "3", state, licenseNo, ssn, firstName, middleName, lastName, dob, gender, policyRef, DpaCode, Chistory1, "", vendorProductCodeMVR, "", "2", "", "");
            }
            else
            {

                //ObjXML = 
                GeneratePSPRequest(username, password, ObjGetOrderSearchedDataResult.SearchedFirstName, ObjGetOrderSearchedDataResult.SearchedLastName, ObjGetOrderSearchedDataResult.SearchedDob, ObjGetOrderSearchedDataResult.SearchedDriverLicense, SearchedDlStateCode, pkOrderId.ToString(), Convert.ToString(DpaCode));
            }

            return (url + "?" + lQueryString);
        }

        public StringBuilder GeneratePSPRequest(string userName, string password, string searchdriverFname, string searchdriverLname, string searchDOB, string searchDLNum, string searchDLState, string searchOrderId, string searchdppacode)
        {
            StringBuilder ObjXML = new StringBuilder();


            #region New Code for generate PSP request for INT-116
            string customdob = Convert.ToDateTime(searchDOB).ToString("MM/dd/yy");
            string m = customdob.Substring(0, 2);
            string d = customdob.Substring(3, 2);
            string y = customdob.Substring(6, 2);
            string searchfinaldob = m + d + y;

            string website = Convert.ToString(ConfigurationManager.AppSettings["psp_website"]);
            string usercode = Convert.ToString(ConfigurationManager.AppSettings["psp_usercode"]);
            string username = Convert.ToString(ConfigurationManager.AppSettings["psp_username"]);
            string userpass = Convert.ToString(ConfigurationManager.AppSettings["psp_userpassword"]);
            string driverFname = searchdriverFname;
            string driverLname = searchdriverLname;
            string DLState = searchDLState;
            string OrderId = searchOrderId;
            string dppacode = " ";
            string DLNum = searchDLNum;
            // searchfinaldob = searchfinaldob;
            //------------------------------
            string xmlrequest = String.Concat(
            "<?xml version=\"1.0\" encoding=\"utf-8\"?>",
            "<soap:Envelope",
            "      xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"",
            "      xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"",
            "      xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">",
            "  <soap:Body>",
            "    <getReport xmlns=\"https://webservices.softechinternational.com/\">",

            " <PSP_Request>",
            "<website>" + website + "</website>",
            "<userCode>" + usercode + "</userCode>",
            "<userName>" + username + "</userName>",
            "<userPass>" + userpass + "</userPass>",
            "<DLNumber>" + DLNum + "</DLNumber>",
            "<DLLastName>" + driverLname + "</DLLastName>",
            "<DLState>" + DLState + "</DLState>",
            "<DriverDOB>" + searchfinaldob + "</DriverDOB>",
            "<InternalRefID>" + OrderId + "</InternalRefID>",
            "<DriverConsent>1</DriverConsent>",
            "<DriverCurrentFirstName>" + driverFname + "</DriverCurrentFirstName>",
            "<DriverCurrentLastName>" + driverLname + "</DriverCurrentLastName>",
            "<DPPACode>" + dppacode + "</DPPACode>",
            "<UserIP></UserIP>",
            " </PSP_Request>",
            "    </getReport>",
            "  </soap:Body>",
            "</soap:Envelope>");
            ObjXML.Append(xmlrequest);
            #endregion
            return ObjXML;
        }

        /// <summary>
        /// Function To Create BackGround Check Request .
        /// </summary>
        /// <param name="pkOrderId"></param>
        /// <param name="ObjGetOrderSearchedDataResult"></param>
        /// <param name="ObjCollOrderLicenseInfo"></param>
        /// <param name="ObjCollOrderPersonalQtnInfo"></param>
        /// <param name="ObjCollOrderSearchedEducationInfo"></param>
        /// <param name="ObjCollOrderSearchedEmploymentInfo"></param>
        /// <param name="ObjtblVendor_single"></param>
        /// <param name="ProductCode"></param>
        /// <param name="SearchedStateCode"></param>
        /// <param name="SearchedCountyName"></param>
        /// <returns></returns>
        private string GenerateBGCRequest(int? pkOrderId, tblOrderSearchedData ObjGetOrderSearchedDataResult, List<tblOrderSearchedLicenseInfo> ObjCollOrderLicenseInfo, List<tblOrderPersonalQtnInfo> ObjCollOrderPersonalQtnInfo, List<tblOrderSearchedEducationInfo> ObjCollOrderSearchedEducationInfo, List<tblOrderSearchedEmploymentInfo> ObjCollOrderSearchedEmploymentInfo, tblVendor ObjtblVendor_single, string ProductCode, string SearchedStateCode, string SearchedCountyName, string CompanyAccountNo, string OrderCompanyName, string PermissiblePurpose, string fName = "", string lName = "", string mName = "")
        {
            StringBuilder ObjXML = new StringBuilder();
            string PCode = ProductCode;
            string Gender = ObjGetOrderSearchedDataResult.SearchedSex.ToString() != "" ? ObjGetOrderSearchedDataResult.SearchedSex.ToString().ToUpper() : "MALE";

            /* string SSN = "";
             if (ObjGetOrderSearchedDataResult.SearchedSsn != "___-__-____" && ObjGetOrderSearchedDataResult.SearchedSsn != "")
             {
                 SSN = ObjGetOrderSearchedDataResult.SearchedSsn.Contains("-") ? ObjGetOrderSearchedDataResult.SearchedSsn.Replace("-", "") : ObjGetOrderSearchedDataResult.SearchedSsn;
             }*/


            string username = ObjtblVendor_single.ServiceUserId.ToString();
            string password = ObjtblVendor_single.ServicePassword.ToString();

            XmlDocument ObjXmlDocument = new XmlDocument();

            ObjXmlDocument.LoadXml(ObjtblVendor_single.AdditionalParams.ToString());

            XmlNode ObjXmlNode = ObjXmlDocument.DocumentElement;

            string accountid = ObjXmlNode.SelectSingleNode("accountId").ChildNodes[0].Value;

            /*  ------Code To fetch Account Credentials-----------------*/

            ObjXML.Append("<?xml version='1.0' encoding='UTF-8'?>");

            ObjXML.Append("<BGC version='1.4'><login><user>" + username + "</user><password>" + password + "</password><account>" + accountid + "</account></login><product><");

            string request = "";

            request = GetRequestType(PCode);

            ObjXML.Append(request);

            ObjXML.Append(" version='1'>");

            if (PCode == "SCR" || PCode == "CCR2")
            {
                ObjXML.Append("<initiate>");
            }

            if (fName != "" || lName != "")
            {
                ObjXML.Append("<order><firstName>" + fName + "</firstName><middleName>" + mName + "</middleName><lastName>" + lName + "</lastName>");

            }
            else
            {
                ObjXML.Append("<order><firstName>" + ObjGetOrderSearchedDataResult.SearchedFirstName + "</firstName><middleName>" + ObjGetOrderSearchedDataResult.SearchedMiddleInitial + "</middleName><lastName>" + ObjGetOrderSearchedDataResult.SearchedLastName + "</lastName>");
            }
            BALGeneral ObjBALGeneral = new BALGeneral();

            if (ObjBALGeneral.isValidDateTime(ObjGetOrderSearchedDataResult.SearchedDob) == true)
            {

                DateTime Now = System.DateTime.Now;

                DateTime ObjDateTime = ObjGetOrderSearchedDataResult.SearchedDob != "" ? Convert.ToDateTime(ObjGetOrderSearchedDataResult.SearchedDob) : Now;

                if (ObjDateTime != Now)
                {
                    ObjXML.Append("<DOB><month>" + ObjDateTime.Month + "</month>");
                    ObjXML.Append("<day>" + ObjDateTime.Day + "</day>");
                    ObjXML.Append("<year>" + ObjDateTime.Year + "</year></DOB>");
                }
            }
            if (PCode == "SCR" || PCode == "CCR2")
            {
                if (Gender.ToUpper() == "M")
                    Gender = "MALE";
                else if (Gender.ToUpper() == "F")
                    Gender = "FEMALE";
                ObjXML.Append("<state>" + SearchedStateCode + "</state>");
                ObjXML.Append("<gender>" + Gender + "</gender>");
            }

            if (PCode == "CCR2")
            {
                ObjXML.Append("<county>" + SearchedCountyName + "</county>");
            }

            if ((PCode == "SCR" || PCode == "CCR2") && (ObjGetOrderSearchedDataResult.SearchedSsn != "___-__-____" && ObjGetOrderSearchedDataResult.SearchedSsn != ""))
            {
                ObjXML.Append("<SSN>" + ObjGetOrderSearchedDataResult.SearchedSsn.Replace("-", "") + "</SSN>");
            }

            if (PCode == "SCR")
            {
                ObjXML.Append("<address><street>" + ObjGetOrderSearchedDataResult.SearchedStreetName + " " + ObjGetOrderSearchedDataResult.SearchedStreetAddress + "</street>");
                ObjXML.Append("<city>" + ObjGetOrderSearchedDataResult.SearchedCity + "</city>");
                ObjXML.Append("<state>" + SearchedStateCode + "</state>");
                ObjXML.Append("<postalCode>" + ObjGetOrderSearchedDataResult.SearchedZipCode + "</postalCode></address>");

            }
            ObjXML.Append("</order>");

            if (PCode == "SCR" || PCode == "CCR2")
            {
                ObjXML.Append("</initiate>");
            }

            if (PCode != "SCR" && PCode != "CCR2")
            {
                ObjXML.Append("<custom><options><noSummary>YES</noSummary></options><filters /></custom>");
            }

            ObjXML.Append("</");

            ObjXML.Append(request);

            ObjXML.Append("></product></BGC>");

            return ObjXML.ToString();
        }

        /// <summary>
        /// Function To get the Resquest Type According to the Prodcut Code Passed
        /// </summary>
        /// <param name="PCode"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        private static string GetRequestType(string PCode)
        {
            string request = "";
            switch (PCode)
            {
                case "SOR":
                    request = "USOneSearchSexOffender";
                    break;
                case "NCR3":
                    request = "USOneSearch";
                    break;
                case "SCR":
                    request = "StatewideCriminalSearch";
                    break;
                case "CCR2":
                    request = "CountyCriminalSearch";
                    break;
            }
            return request;
        }

        /// <summary>
        /// Function which will generate the FRS Request.
        /// </summary>
        /// <param name="pkOrderId"></param>
        /// <param name="ObjCollOrderLicenseInfo"></param>
        /// <param name="ObjCollOrderPersonalQtnInfo"></param>
        /// <param name="ObjCollOrderSearchedEducationInfo"></param>
        /// <param name="ObjCollOrderSearchedEmploymentInfo"></param>
        private string GenerateFRSRequest(int? pkOrderId, tblOrderSearchedData ObjtblOrderSearchedData, List<tblOrderSearchedLicenseInfo> ObjCollOrderLicenseInfo, List<tblOrderPersonalQtnInfo> ObjCollOrderPersonalQtnInfo, List<tblOrderSearchedEducationInfo> ObjCollOrderSearchedEducationInfo, List<tblOrderSearchedEmploymentInfo> ObjCollOrderSearchedEmploymentInfo, tblVendor ObjtblVendor, string ProductCode, string SearchedStateCode, List<tblOrderSearchedPersonalInfo> ObjtblOrderSearchedPersonalInfo, List<tblOrderSearchedProfessionalInfo> ObjtblOrderSearchedProfessionalInfo)
        {
            //INT336
            BALGeneral ObjBALGeneral = new BALGeneral();
            StringBuilder ObjStringBuilder_rootNode = new StringBuilder();
            StringBuilder ObjStringBuilder_subOrder = new StringBuilder();
            StringBuilder ObjStringBuilder_Alias = new StringBuilder();
            StringBuilder ObjReq = new StringBuilder();
            StringBuilder objSubject = new StringBuilder();//To Creating the Subject INT-15
            string dOBString = string.Empty;
            tblVendor vendor = getFRSVendorCredential();
            //To split SSN based on "-" and creating a new continue string.  
            string SearchedSSN = (ObjtblOrderSearchedData.SearchedSsn != "___-__-____" && ObjtblOrderSearchedData.SearchedSsn != "") ? ObjtblOrderSearchedData.SearchedSsn : "";
            //To creating the DOB date format to continue string
            if (ObjBALGeneral.isValidDateTime(ObjtblOrderSearchedData.SearchedDob) == true)
            {
                DateTime Now = System.DateTime.Now;
                DateTime ObjDateTime = ObjtblOrderSearchedData.SearchedDob != "" ? Convert.ToDateTime(ObjtblOrderSearchedData.SearchedDob) : Now;
                if (ObjDateTime != Now)
                {
                    dOBString = ObjDateTime.ToString("MM/dd/yyyy");
                }
            }
            switch (ProductCode)
            {
                case "EMV":
                    foreach (tblOrderSearchedEmploymentInfo ObjtblOrderSearchedEmploymentInfo in ObjCollOrderSearchedEmploymentInfo)
                    {
                        ObjStringBuilder_rootNode.Append("<RPB2BOrder>");
                        ObjStringBuilder_rootNode.Append("<AR_EndDate>Ongoing</AR_EndDate>");
                        ObjStringBuilder_rootNode.Append("<AR_JobTitle></AR_JobTitle>");
                        ObjStringBuilder_rootNode.Append("<AR_Salary></AR_Salary>");
                        ObjStringBuilder_rootNode.Append("<AR_StartDate></AR_StartDate>");
                        ObjStringBuilder_rootNode.Append("<AR_SupervisorName></AR_SupervisorName>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantAddress1></AR_ApplicantAddress1>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantAddress2></AR_ApplicantAddress2>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantBirthDate>" + dOBString + "</AR_ApplicantBirthDate>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantCity>" + ObjtblOrderSearchedEmploymentInfo.SearchedCity + "</AR_ApplicantCity>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantFirstName>" + ObjtblOrderSearchedData.SearchedFirstName + "</AR_ApplicantFirstName>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantMiddleName>" + ObjtblOrderSearchedData.SearchedMiddleInitial + "</AR_ApplicantMiddleName>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantLastName>" + ObjtblOrderSearchedData.SearchedLastName + "</AR_ApplicantLastName>");
                        if (!string.IsNullOrEmpty(ObjtblOrderSearchedEmploymentInfo.SearchedPhone))
                        {
                            ObjStringBuilder_subOrder.Append("<AR_ApplicantPhoneNumber>" + ObjtblOrderSearchedEmploymentInfo.SearchedPhone + "</AR_ApplicantPhoneNumber>");
                        }
                        else
                        {
                            ObjStringBuilder_subOrder.Append("<AR_ApplicantPhoneNumber>none</AR_ApplicantPhoneNumber>");
                        }

                        if (ObjtblOrderSearchedEmploymentInfo.SearchedAlias == string.Empty)
                        {
                            ObjStringBuilder_rootNode.Append("<AR_ApplicantPreviousNames>none</AR_ApplicantPreviousNames>");
                        }
                        else
                        {
                            ObjStringBuilder_rootNode.Append("<AR_ApplicantPreviousNames>" + ObjtblOrderSearchedEmploymentInfo.SearchedAlias + "</AR_ApplicantPreviousNames>");
                        }
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantSSN>" + SearchedSSN + "</AR_ApplicantSSN>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantState>" + GetStateCode(Convert.ToInt32(ObjtblOrderSearchedData.SearchedStateId)) + "</AR_ApplicantState>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantSuffix></AR_ApplicantSuffix>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantZipCode></AR_ApplicantZipCode>");
                        ObjStringBuilder_rootNode.Append("<ClientOrderID>" + pkOrderId + "</ClientOrderID>");
                        ObjStringBuilder_rootNode.Append("<ClientOrderSpecifics>none</ClientOrderSpecifics>");
                        if (!string.IsNullOrEmpty(ObjtblOrderSearchedEmploymentInfo.SearchedCity))
                        {
                            ObjStringBuilder_rootNode.Append("<AR_ReferenceCity>" + ObjtblOrderSearchedEmploymentInfo.SearchedCity + "</AR_ReferenceCity>");
                        }
                        else
                        {
                            ObjStringBuilder_rootNode.Append("<AR_ReferenceCity></AR_ReferenceCity>");
                        }
                        if (!string.IsNullOrEmpty(ObjtblOrderSearchedEmploymentInfo.SearchedName))
                        {
                            if (ObjtblOrderSearchedEmploymentInfo.SearchedName.Contains("&"))
                            {
                                ObjStringBuilder_rootNode.Append("<AR_ReferenceCompany><![CDATA[" + ObjtblOrderSearchedEmploymentInfo.SearchedName + "]]></AR_ReferenceCompany>");
                            }
                            else
                            {
                                ObjStringBuilder_rootNode.Append("<AR_ReferenceCompany>" + ObjtblOrderSearchedEmploymentInfo.SearchedName + "</AR_ReferenceCompany>");
                            }
                        }
                        ObjStringBuilder_rootNode.Append("<AR_ReferenceFaxNumber></AR_ReferenceFaxNumber>");
                        ObjStringBuilder_rootNode.Append("<AR_SupervisorName></AR_SupervisorName>");
                        if (!string.IsNullOrEmpty(ObjtblOrderSearchedEmploymentInfo.SearchedPhone))
                        {
                            ObjStringBuilder_rootNode.Append("<AR_ReferencePhoneNumber>" + ObjtblOrderSearchedEmploymentInfo.SearchedPhone + "</AR_ReferencePhoneNumber>");
                        }
                        else
                        {
                            ObjStringBuilder_rootNode.Append("<AR_ReferencePhoneNumber>none</AR_ReferencePhoneNumber>");
                        }
                        ObjStringBuilder_rootNode.Append("<AR_ReferenceState>" + GetStateCode(Convert.ToInt32(ObjtblOrderSearchedEmploymentInfo.SearchedState)) + "</AR_ReferenceState>");
                        ObjStringBuilder_rootNode.Append("<ServiceID>testServiceID</ServiceID>");
                        ObjStringBuilder_rootNode.Append("<ServiceDescr>06:Verification</ServiceDescr>");
                        ObjStringBuilder_rootNode.Append("<SpecificInstructions>testSpecificInstructions</SpecificInstructions>");
                        ObjStringBuilder_rootNode.Append("<requestDownloadConfirmation>TRUE</requestDownloadConfirmation>");
                        ObjStringBuilder_rootNode.Append("<EndUserName>Intelifi Inc.</EndUserName>");
                        ObjStringBuilder_rootNode.Append("</RPB2BOrder>");
                    }
                    break;
                case "EDV":
                    foreach (tblOrderSearchedEducationInfo ObjtblOrderSearchedEducationInfo in ObjCollOrderSearchedEducationInfo)
                    {
                        ObjStringBuilder_rootNode.Append("<RPB2BOrder>");
                        ObjStringBuilder_rootNode.Append("<AR_DatesAttended></AR_DatesAttended>");
                        ObjStringBuilder_rootNode.Append("<AR_DegreeClaimed></AR_DegreeClaimed>");
                        ObjStringBuilder_rootNode.Append("<AR_DateOfGraduation>" + ObjtblOrderSearchedEducationInfo.SearchedGraduationDt + "</AR_DateOfGraduation>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantBirthDate>" + dOBString + "</AR_ApplicantBirthDate>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantCity>" + ObjtblOrderSearchedEducationInfo.SearchedCity + "</AR_ApplicantCity>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantFirstName>" + ObjtblOrderSearchedData.SearchedFirstName + "</AR_ApplicantFirstName>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantMiddleName>" + ObjtblOrderSearchedData.SearchedMiddleInitial + "</AR_ApplicantMiddleName>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantLastName>" + ObjtblOrderSearchedData.SearchedLastName + "</AR_ApplicantLastName>");
                        if (ObjtblOrderSearchedEducationInfo.SearchedAlias == string.Empty)
                        {
                            ObjStringBuilder_rootNode.Append("<AR_ApplicantPreviousNames>none</AR_ApplicantPreviousNames>");
                        }
                        else
                        {
                            ObjStringBuilder_rootNode.Append("<AR_ApplicantPreviousNames>" + ObjtblOrderSearchedEducationInfo.SearchedAlias + "</AR_ApplicantPreviousNames>");
                        }
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantSSN>" + SearchedSSN + "</AR_ApplicantSSN>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantState>" + GetStateCode(Convert.ToInt32(ObjtblOrderSearchedData.SearchedStateId)) + "</AR_ApplicantState>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantZipCode></AR_ApplicantZipCode>");
                        ObjStringBuilder_rootNode.Append("<ClientOrderID>" + pkOrderId + "</ClientOrderID>");
                        if (!string.IsNullOrEmpty(ObjtblOrderSearchedEducationInfo.SearchedCity))
                        {
                            ObjStringBuilder_rootNode.Append("<AR_SchoolCity>" + ObjtblOrderSearchedEducationInfo.SearchedCity + "</AR_SchoolCity>");
                        }
                        else
                        {
                            ObjStringBuilder_subOrder.Append("<AR_SchoolCity></AR_SchoolCity>");
                        }
                        if (!string.IsNullOrEmpty(ObjtblOrderSearchedEducationInfo.SearchedSchoolName))
                        {
                            if (ObjtblOrderSearchedEducationInfo.SearchedSchoolName.Contains("&"))
                            {
                                ObjStringBuilder_rootNode.Append("<AR_SchoolName><![CDATA[" + ObjtblOrderSearchedEducationInfo.SearchedSchoolName + "]]></AR_SchoolName>");
                            }
                            else
                            {
                                ObjStringBuilder_rootNode.Append("<AR_SchoolName>" + ObjtblOrderSearchedEducationInfo.SearchedSchoolName + "</AR_SchoolName>");
                            }
                        }
                        if (!string.IsNullOrEmpty(ObjtblOrderSearchedEducationInfo.SearchedPhone))
                        {
                            ObjStringBuilder_rootNode.Append("<AR_SchoolPhoneNumber>" + ObjtblOrderSearchedEducationInfo.SearchedPhone + "</AR_SchoolPhoneNumber>");
                        }

                        ObjStringBuilder_rootNode.Append("<AR_SchoolState>" + GetStateCode(Convert.ToInt32(ObjtblOrderSearchedEducationInfo.SearchedState)) + "</AR_SchoolState>");
                        ObjStringBuilder_rootNode.Append("<ServiceID>testServiceID</ServiceID>");
                        ObjStringBuilder_rootNode.Append("<ServiceDescr>06:Education</ServiceDescr>");
                        ObjStringBuilder_rootNode.Append("</RPB2BOrder>");
                    }
                    break;
                case "PLV":
                    foreach (tblOrderSearchedLicenseInfo ObjtblOrderSearchedLicenseInfo in ObjCollOrderLicenseInfo)
                    {
                        ObjStringBuilder_rootNode.Append("<RPB2BOrder>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantBirthDate>" + dOBString + "</AR_ApplicantBirthDate>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantFirstName>" + ObjtblOrderSearchedData.SearchedFirstName + "</AR_ApplicantFirstName>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantMiddleName>" + ObjtblOrderSearchedData.SearchedMiddleInitial + "</AR_ApplicantMiddleName>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantLastName>" + ObjtblOrderSearchedData.SearchedLastName + "</AR_ApplicantLastName>");
                        if (ObjtblOrderSearchedLicenseInfo.SearchedAlias == string.Empty)
                        {
                            ObjStringBuilder_rootNode.Append("<AR_ApplicantPreviousNames>none</AR_ApplicantPreviousNames>");
                        }
                        else
                        {
                            ObjStringBuilder_rootNode.Append("<AR_ApplicantPreviousNames>" + ObjtblOrderSearchedLicenseInfo.SearchedAlias + "</AR_ApplicantPreviousNames>");
                        }
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantSSN>" + SearchedSSN + "</AR_ApplicantSSN>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantState>" + GetStateCode(Convert.ToInt32(ObjtblOrderSearchedData.SearchedStateId)) + "</AR_ApplicantState>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantZipCode></AR_ApplicantZipCode>");
                        ObjStringBuilder_rootNode.Append("<ClientOrderID>" + pkOrderId + "</ClientOrderID>");
                        ObjStringBuilder_rootNode.Append("<AR_LicenseNumber>" + ObjtblOrderSearchedLicenseInfo.SearchedLicenseNo + "</AR_LicenseNumber>");
                        ObjStringBuilder_rootNode.Append("<AR_LicenseType>" + ObjtblOrderSearchedLicenseInfo.SearchedLicenseType + "</AR_LicenseType>");
                        ObjStringBuilder_rootNode.Append("<AR_CR_IssuingStateJurisdiction>" + GetStateCode(Convert.ToInt32(ObjtblOrderSearchedLicenseInfo.SearchedIssuingState)) + "</AR_CR_IssuingStateJurisdiction>");
                        ObjStringBuilder_rootNode.Append("<ServiceID>testServiceID</ServiceID>");
                        ObjStringBuilder_rootNode.Append("<ServiceDescr>06:Credentials</ServiceDescr>");
                        ObjStringBuilder_rootNode.Append("</RPB2BOrder>");
                    }
                    break;
                case "PRV":
                    foreach (tblOrderSearchedPersonalInfo ObjtblOrderSearchedPersonalInfo_single in ObjtblOrderSearchedPersonalInfo)
                    {

                        ObjStringBuilder_rootNode.Append("<RPB2BOrder>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantBirthDate>" + dOBString + "</AR_ApplicantBirthDate>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantFirstName>" + ObjtblOrderSearchedData.SearchedFirstName + "</AR_ApplicantFirstName>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantMiddleName>" + ObjtblOrderSearchedData.SearchedMiddleInitial + "</AR_ApplicantMiddleName>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantLastName>" + ObjtblOrderSearchedData.SearchedLastName + "</AR_ApplicantLastName>");
                        if (ObjtblOrderSearchedPersonalInfo_single.SearchedAlias == string.Empty)
                        {
                            ObjStringBuilder_rootNode.Append("<AR_ApplicantPreviousNames>none</AR_ApplicantPreviousNames>");
                        }
                        else
                        {
                            ObjStringBuilder_rootNode.Append("<AR_ApplicantPreviousNames>" + ObjtblOrderSearchedPersonalInfo_single.SearchedAlias + "</AR_ApplicantPreviousNames>");
                        }
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantSSN>" + SearchedSSN + "</AR_ApplicantSSN>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantState>" + GetStateCode(Convert.ToInt32(ObjtblOrderSearchedData.SearchedStateId)) + "</AR_ApplicantState>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantZipCode></AR_ApplicantZipCode>");
                        ObjStringBuilder_rootNode.Append("<ClientOrderID>" + pkOrderId + "</ClientOrderID>");
                        ObjStringBuilder_rootNode.Append("<AR_ReferenceCity>none</AR_ReferenceCity>");

                        ObjStringBuilder_rootNode.Append("<AR_ReferenceName>" + ObjtblOrderSearchedPersonalInfo_single.SearchedName + "</AR_ReferenceName>");
                        ObjStringBuilder_rootNode.Append("<AR_ReferencePhoneNumber>" + ObjtblOrderSearchedPersonalInfo_single.SearchedPhone + "</AR_ReferencePhoneNumber>");

                        ObjStringBuilder_rootNode.Append("<ServiceID>testServiceID</ServiceID>");
                        ObjStringBuilder_rootNode.Append("<ServiceDescr>06:Personal Reference</ServiceDescr>");
                        ObjStringBuilder_rootNode.Append("</RPB2BOrder>");
                    }
                    break;
                case "PRV2":
                    foreach (tblOrderSearchedProfessionalInfo ObjtblOrderSearchedProfessionalInfo_single in ObjtblOrderSearchedProfessionalInfo)
                    {
                        ObjStringBuilder_rootNode.Append("<RPB2BOrder>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantBirthDate>" + dOBString + "</AR_ApplicantBirthDate>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantFirstName>" + ObjtblOrderSearchedData.SearchedFirstName + "</AR_ApplicantFirstName>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantMiddleName>" + ObjtblOrderSearchedData.SearchedMiddleInitial + "</AR_ApplicantMiddleName>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantLastName>" + ObjtblOrderSearchedData.SearchedLastName + "</AR_ApplicantLastName>");
                        if (ObjtblOrderSearchedProfessionalInfo_single.SearchedAlias == string.Empty)
                        {
                            ObjStringBuilder_rootNode.Append("<AR_ApplicantPreviousNames>none</AR_ApplicantPreviousNames>");
                        }
                        else
                        {
                            ObjStringBuilder_rootNode.Append("<AR_ApplicantPreviousNames>" + ObjtblOrderSearchedProfessionalInfo_single.SearchedAlias + "</AR_ApplicantPreviousNames>");
                        }
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantSSN>" + SearchedSSN + "</AR_ApplicantSSN>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantState>" + GetStateCode(Convert.ToInt32(ObjtblOrderSearchedData.SearchedStateId)) + "</AR_ApplicantState>");
                        ObjStringBuilder_rootNode.Append("<AR_ApplicantZipCode></AR_ApplicantZipCode>");
                        ObjStringBuilder_rootNode.Append("<ClientOrderID>" + pkOrderId + "</ClientOrderID>");
                        ObjStringBuilder_rootNode.Append("<AR_ReferenceCity>none</AR_ReferenceCity>");

                        ObjStringBuilder_rootNode.Append("<AR_ReferenceName>" + ObjtblOrderSearchedProfessionalInfo_single.SearchedName + "</AR_ReferenceName>");
                        ObjStringBuilder_rootNode.Append("<AR_ReferencePhoneNumber>" + ObjtblOrderSearchedProfessionalInfo_single.SearchedPhone + "</AR_ReferencePhoneNumber>");

                        ObjStringBuilder_rootNode.Append("<ServiceID>testServiceID</ServiceID>");
                        ObjStringBuilder_rootNode.Append("<ServiceDescr>06:Professional Reference</ServiceDescr>");
                        ObjStringBuilder_rootNode.Append("</RPB2BOrder>");
                    }
                    break;
                case "WCV":
                    ObjStringBuilder_rootNode.Append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
                    ObjStringBuilder_rootNode.Append("<New_Order>");
                    ObjStringBuilder_rootNode.Append("<login>");
                    ObjStringBuilder_rootNode.Append("<account>INTSCR</account>");
                    ObjStringBuilder_rootNode.Append("<username>" + vendor.ServiceUserId + "</username>");
                    ObjStringBuilder_rootNode.Append("<password>" + vendor.ServicePassword + "</password>");
                    ObjStringBuilder_rootNode.Append("</login>");
                    ObjStringBuilder_rootNode.Append("<mode>PROD</mode>");
                    ObjStringBuilder_rootNode.Append("<placeOrder number=\"" + pkOrderId + "\">");
                    ObjStringBuilder_rootNode.Append("<orderInfo/>");

                    objSubject.Append("<subject>");
                    objSubject.Append("<name_first>" + ObjtblOrderSearchedData.SearchedFirstName + "</name_first>");
                    objSubject.Append("<name_middle>" + ObjtblOrderSearchedData.SearchedMiddleInitial + "</name_middle>");
                    objSubject.Append("<name_last>" + ObjtblOrderSearchedData.SearchedLastName + "</name_last>");
                    objSubject.Append("<alias>PutAlias");
                    objSubject.Append("</alias>");
                    objSubject.Append("<ssn>" + SearchedSSN + "</ssn>");
                    objSubject.Append("<address></address>");
                    objSubject.Append("<city></city>");
                    objSubject.Append("<state>" + GetStateCode(Convert.ToInt32(ObjtblOrderSearchedData.SearchedStateId)) + "</state>");
                    objSubject.Append("<zip></zip>");
                    objSubject.Append("<FCRAPurpose>Employment by Hire or Contract </FCRAPurpose>");
                    objSubject.Append("<dob>" + dOBString + "</dob>");
                    objSubject.Append("<email>" + ObjtblOrderSearchedData.SearchedApplicantEmail + "</email>");
                    objSubject.Append("<race>U</race>");
                    objSubject.Append("<gender></gender>");
                    objSubject.Append("<citizenship_status>Citizen</citizenship_status>");
                    objSubject.Append("<intlgovtid/>");
                    objSubject.Append("<intlgovtid_descr/>");
                    objSubject.Append("<remote_ordernumber/>");
                    objSubject.Append("</subject>");
                    foreach (tblOrderSearchedLicenseInfo ObjtblOrderSearchedLicenseInfo in ObjCollOrderLicenseInfo)
                    {
                        ObjStringBuilder_subOrder.Append("<subOrder type=\"Workers Compensation\">");
                        if (ObjtblOrderSearchedLicenseInfo.SearchedIssuingState != string.Empty)
                        {
                            ObjStringBuilder_subOrder.Append("<state>" + ObjtblOrderSearchedLicenseInfo.SearchedIssuingState + "</state>");
                        }
                        else
                        {
                            ObjStringBuilder_subOrder.Append("<state>" + GetStateCode(Convert.ToInt32(ObjtblOrderSearchedData.SearchedStateId)) + "</state>");
                        }
                        ObjStringBuilder_subOrder.Append("</subOrder>");
                        if (!string.IsNullOrEmpty(ObjtblOrderSearchedLicenseInfo.SearchedAlias))
                        {
                            if (ObjtblOrderSearchedLicenseInfo.SearchedAlias.Contains(" "))
                            {
                                string[] SearchedAliasArr = ObjtblOrderSearchedLicenseInfo.SearchedAlias.Split(' ');
                                if (SearchedAliasArr.Count() > 2)
                                {
                                    ObjStringBuilder_Alias.Append("<name_first>" + SearchedAliasArr[0] + "</name_first>");
                                    ObjStringBuilder_Alias.Append("<name_middle>" + SearchedAliasArr[1] + "</name_middle>");
                                    ObjStringBuilder_Alias.Append("<name_last>" + SearchedAliasArr[2] + "</name_last>");
                                    ObjStringBuilder_Alias.Append("<name_suffix></name_suffix>");
                                }
                                else//changes based on INT102 above code, below is previous code (else part) 
                                {
                                    ObjStringBuilder_Alias.Append("<name_first>" + SearchedAliasArr[0] + "</name_first>");
                                    ObjStringBuilder_Alias.Append("<name_middle></name_middle>");
                                    ObjStringBuilder_Alias.Append("<name_last>" + SearchedAliasArr[1] + "</name_last>");
                                    ObjStringBuilder_Alias.Append("<name_suffix></name_suffix>");
                                }
                            }
                            else
                            {
                                ObjStringBuilder_Alias.Append("<name_first>" + ObjtblOrderSearchedLicenseInfo.SearchedAlias + "</name_first>");
                                ObjStringBuilder_Alias.Append("<name_middle></name_middle>");
                                ObjStringBuilder_Alias.Append("<name_last></name_last>");
                                ObjStringBuilder_Alias.Append("<name_suffix></name_suffix>");
                            }
                        }
                    }
                    objSubject.Replace("PutAlias", ObjStringBuilder_Alias.ToString());
                    ObjStringBuilder_rootNode.Append(objSubject);
                    ObjStringBuilder_rootNode.Append(ObjStringBuilder_subOrder);
                    ObjStringBuilder_rootNode.Append("</placeOrder>");
                    ObjStringBuilder_rootNode.Append("</New_Order>");
                    break;

            }
            ObjReq = ObjStringBuilder_rootNode;
            return ObjReq.ToString();
        }




        private string BuilRequestForSocialDiliGence(int? pkOrderId, string ProductCode, tblOrderSearchedData ObjtblOrderSearchedData,
                                             List<tblOrderSearchedAliasPersonalInfo> ObjCollOrderSearchedAliasPersonalInfo,
                                             List<tblOrderSearchedAliasLocationInfo> ObjCollOrderSearchedAliasLocationInfo,
                                             List<tblOrderSearchedAliasWorkInfo> ObjCollOrderSearchedAliasWorkInfo,
                                             List<tblState> ObjtblState_List, tblVendor ObjtblVendor_single, string SearchedStateCode)
        {

            string Username = ObjtblVendor_single.ServiceUserId;
            string Password = ObjtblVendor_single.ServicePassword;

            StringBuilder sb = new StringBuilder();

            sb.Append(ObjtblVendor_single.ServiceUrl + "?");
            sb.Append("namesake_results=1");
            sb.Append("&FirstName=" + ObjtblOrderSearchedData.SearchedFirstName);
            sb.Append("&MiddleName=" + ObjtblOrderSearchedData.SearchedMiddleInitial);
            sb.Append("&LastName=" + ObjtblOrderSearchedData.SearchedLastName);
            sb.Append("&Address1_State=" + SearchedStateCode);


            sb.Append("&partner_name=" + Username);
            sb.Append("&viewer=xml");
            sb.Append("&search=start");
            sb.Append("&partner_passwd=" + Password);

            return sb.ToString();

        }

        /// <summary>
        /// Function To Get The StateCode in case if we Provide StateID
        /// </summary>
        /// <param name="StateId"></param>
        /// <returns></returns>
        private string GetStateCode(int StateId)
        {
            string statCode = "";
            BALGeneral objBALGeneral = new BALGeneral();
            List<Proc_Get_StateByStateIdResult> state = objBALGeneral.GetStateByStateId(StateId);
            if (state.Count > 0)
            {
                statCode = state.ElementAt(0).StateCode;
            }
            return statCode;
        }

        public string GetBureauForType(string type)
        {
            string bureau = "";
            switch (type)
            {
                case "ECR":
                    bureau = "EX";
                    break;
                case "EEI":
                    bureau = "EX";
                    break;
                case "RAL":
                    bureau = "QS";
                    break;
                case "Driver License Search":
                    bureau = "DS";
                    break;
                case "RPL":
                    bureau = "QS";
                    break;
                case "PS2":
                    bureau = "DS";
                    break;
                case "OFAC":
                    bureau = "DS";
                    break;
                case "TDR":
                    bureau = "SS";
                    break;
                case "SSR":
                    bureau = "DS";
                    break;
                case "NCR4":
                    bureau = "RA";
                    break;
                case "PL":
                    bureau = "SS";
                    break;
                case "Sex Offender":
                    bureau = "DS";
                    break;
                case "NCR_UNKNOWN":
                    bureau = "NSCR";
                    break;
                case "ER":
                    bureau = "DS";
                    break;
                case "BR":
                    bureau = "DS";
                    break;
                case "MVS":
                    bureau = "SS";
                    break;
            }
            return bureau;
        }

        public string GetTypeId(string type)
        {
            string typeId = "";

            switch (type)
            {
                case "ECR":
                    typeId = "STD";
                    break;
                case "EEI":
                    typeId = "EINS";
                    break;
                case "RAL":
                    typeId = "QREVA";
                    break;
                case "Driver License Search":
                    typeId = "DLDL";
                    break;
                case "RPL":
                    typeId = "QREVP";
                    break;
                case "PS2":
                    typeId = "SBPSR";
                    break;
                case "OFAC":
                    typeId = "OFAC";
                    break;
                case "TDR":
                    typeId = "LOCATOR";
                    break;
                case "SSR":
                    typeId = "SSNVAL";
                    break;
                case "NCR4":
                    typeId = "RAPSHEET";
                    break;
                case "PL":
                    typeId = "PROPR";
                    break;
                case "SOR":
                    typeId = "NSOR";
                    break;
                case "NCR_UNKNOWN":
                    typeId = "NSCR";
                    break;
                case "ER":
                    typeId = "EVICTIONS";
                    break;
                case "BR":
                    typeId = "BLJ";
                    break;
                case "MVS":
                    typeId = "MV";
                    break;
            }
            return typeId;
        }


        public CustomOrderCollection GetOrderDetailsByOrderId(int OrderId)
        {
            CustomOrderCollection ObjCustomOrderCollection = new CustomOrderCollection();

            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                tblOrder ObjtblOrder = DX.tblOrders.Where(d => d.pkOrderId == OrderId).FirstOrDefault();
                if (ObjtblOrder != null)
                {
                    #region Get Order
                    bool? IsTieredOrder = ObjtblOrder.IsTieredOrder == null ? false : ObjtblOrder.IsTieredOrder;
                    ObjCustomOrderCollection.ObjOrder = ObjtblOrder;

                    #endregion Get Order

                    #region Get Order Detail
                    try
                    {
                        List<tblOrderDetail> lstOrderDetail = DX.tblOrderDetails.Where(d => d.fkOrderId == ObjtblOrder.pkOrderId && d.fkProductApplicationId > 0).ToList<tblOrderDetail>();
                        ObjCustomOrderCollection.ListOrderDetail = lstOrderDetail;
                        if (lstOrderDetail != null)
                        {
                            #region Get Order Request Data and Response Data
                            try
                            {
                                List<tblOrderRequestData> lstOrderRequestData = new List<tblOrderRequestData>();
                                List<tblOrderResponseData> lstOrderResponseData = new List<tblOrderResponseData>();
                                List<string> lstOrderReports = new List<string>();
                                Dictionary<string, int?> ObjCompanyProductInfo = new Dictionary<string, int?>();
                                foreach (tblOrderDetail ObjOrderDetail in lstOrderDetail)
                                {
                                    tblOrderRequestData ObjOrderRequestData = DX.tblOrderRequestDatas.Where(d => d.fkOrderDetailId == ObjOrderDetail.pkOrderDetailId).FirstOrDefault();
                                    if (ObjOrderRequestData != null)
                                    {
                                        lstOrderRequestData.Add(ObjOrderRequestData);
                                    }
                                    tblOrderResponseData ObjOrderResponseData = DX.tblOrderResponseDatas.Where(d => d.fkOrderDetailId == ObjOrderDetail.pkOrderDetailId).FirstOrDefault();
                                    if (ObjOrderResponseData != null)
                                    {
                                        lstOrderResponseData.Add(ObjOrderResponseData);
                                    }
                                    tblProduct ObjProductCode = (from p in DX.tblProductPerApplications
                                                                 join s in DX.tblProducts on
                                                                  p.fkProductId equals s.pkProductId
                                                                 where p.pkProductApplicationId == ObjOrderDetail.fkProductApplicationId
                                                                 select s).FirstOrDefault();
                                    if (ObjProductCode != null)
                                    {
                                        lstOrderReports.Add(ObjProductCode.ProductCode + "_");
                                        if (ObjCompanyProductInfo.Any(d => d.Key != (ObjProductCode.ProductCode + "_" + Convert.ToString(ObjOrderDetail.fkCompanyId))))
                                        {
                                            if (ObjOrderDetail.fkCompanyId != null)
                                            {
                                                ObjCompanyProductInfo.Add((ObjProductCode.ProductCode + "_" + Convert.ToString(ObjOrderDetail.fkCompanyId)), ObjOrderDetail.fkProductApplicationId);
                                            }
                                            else
                                            {
                                                ObjCompanyProductInfo.Add((ObjProductCode.ProductCode + "_" + Convert.ToString(Guid.Empty)), ObjOrderDetail.fkProductApplicationId);
                                            }
                                        }
                                    }
                                }
                                ObjCustomOrderCollection.ListOrderRequestData = lstOrderRequestData;
                                ObjCustomOrderCollection.ListOrderResponseData = lstOrderResponseData;
                                ObjCustomOrderCollection.ListOrderReports = lstOrderReports;
                                ObjCustomOrderCollection.CompanyProductInfo = ObjCompanyProductInfo;
                            }
                            catch (Exception ex)
                            {
                                //Added log for exception handling.
                                VendorServices.GeneralService.WriteLog("Some Error Occured in GetOrderDetailsByOrderId(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                            }
                            #endregion Get Order Request Data and Response Data
                        }
                    }
                    catch (Exception ex)
                    {
                        //Added log for exception handling.
                        VendorServices.GeneralService.WriteLog("Some Error Occured in GetOrderDetailsByOrderId(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    }
                    #endregion Get Order Detail

                    #region Get Order Searched Data

                    tblOrderSearchedData ObjtblOrderSearchedData = DX.tblOrderSearchedDatas.Where(d => d.fkOrderId == ObjtblOrder.pkOrderId).FirstOrDefault();
                    if (ObjtblOrderSearchedData != null)
                    {
                        ObjCustomOrderCollection.ObjOrderSearchedData = ObjtblOrderSearchedData;
                    }
                    #endregion Get Order Searched Data

                    #region Get Order PersonalQtnInfo

                    tblOrderPersonalQtnInfo ObjOrderPersonalQtnInfo = DX.tblOrderPersonalQtnInfos.Where(d => d.fkOrderId == ObjtblOrder.pkOrderId).FirstOrDefault();
                    if (ObjOrderPersonalQtnInfo != null)
                    {
                        ObjCustomOrderCollection.ObjOrderPersonalQtnInfo = ObjOrderPersonalQtnInfo;
                    }
                    #endregion Get Order PersonalQtnInfo
                    #region Get Order Searched Education Info
                    try
                    {
                        List<tblOrderSearchedEducationInfo> lsttblOrderSearchedEducationInfo = DX.tblOrderSearchedEducationInfos.Where(d => d.fkOrderId == ObjtblOrder.pkOrderId).ToList<tblOrderSearchedEducationInfo>();
                        ObjCustomOrderCollection.ListOrderSearchedEducationInfo = lsttblOrderSearchedEducationInfo;
                    }
                    catch (Exception ex)
                    {
                        //Added log for exception handling.
                        VendorServices.GeneralService.WriteLog("Some Error Occured in GetOrderDetailsByOrderId(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    }
                    #endregion Get Order Searched Education Info

                    #region Get Order Searched Employment Info
                    try
                    {
                        List<tblOrderSearchedEmploymentInfo> lsttblOrderSearchedEmploymentInfo = DX.tblOrderSearchedEmploymentInfos.Where(d => d.fkOrderId == ObjtblOrder.pkOrderId).ToList<tblOrderSearchedEmploymentInfo>();
                        ObjCustomOrderCollection.ListOrderSearchedEmploymentInfo = lsttblOrderSearchedEmploymentInfo;
                    }
                    catch (Exception ex)
                    {
                        //Added log for exception handling.
                        VendorServices.GeneralService.WriteLog("Some Error Occured in GetOrderDetailsByOrderId(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    }
                    #endregion Get Order Searched Employment Info

                    #region Get Order Searched License Info

                    try
                    {
                        List<tblOrderSearchedLicenseInfo> lsttblOrderSearchedLicenseInfo = DX.tblOrderSearchedLicenseInfos.Where(d => d.fkOrderId == ObjtblOrder.pkOrderId).ToList<tblOrderSearchedLicenseInfo>();
                        ObjCustomOrderCollection.ListOrderSearchedLicenseInfo = lsttblOrderSearchedLicenseInfo;
                    }
                    catch (Exception ex)
                    {
                        //Added log for exception handling.
                        VendorServices.GeneralService.WriteLog("Some Error Occured in GetOrderDetailsByOrderId(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    }

                    #endregion Get Order Searched License Info

                    #region Get Order Searched Live Runner Info (County/State Info)
                    try
                    {
                        List<tblOrderSearchedLiveRunnerInfo> lsttblOrderSearchedLiveRunnerInfo = DX.tblOrderSearchedLiveRunnerInfos.Where(d => d.fkOrderId == ObjtblOrder.pkOrderId).ToList<tblOrderSearchedLiveRunnerInfo>();
                        if (IsTieredOrder == true)
                        {
                            //List<tblOrderDetail> lstOrderDetail_FCR = DX.tblOrderDetails.Where(d => d.fkOrderId == ObjtblOrder.pkOrderId && d.fkProductApplicationId == Guid.Parse("06a0c3b7-fc38-4fe4-b080-01ceb67452a7")).ToList<tblOrderDetail>();
                            List<tblOrderDetail> lstOrderDetail_FCR = DX.tblOrderDetails.Where(d => d.fkOrderId == ObjtblOrder.pkOrderId && d.fkProductApplicationId == 0).ToList<tblOrderDetail>();
                            if (lstOrderDetail_FCR != null && lsttblOrderSearchedLiveRunnerInfo != null)
                            {

                                foreach (tblOrderDetail objOrderDetail_FCR in lstOrderDetail_FCR)
                                {
                                    int fkOrderDetailId = objOrderDetail_FCR.pkOrderDetailId;
                                    var RemoveEntry = lsttblOrderSearchedLiveRunnerInfo.Where(p => p.fkOrderDetailId == fkOrderDetailId).FirstOrDefault();
                                    lsttblOrderSearchedLiveRunnerInfo.Remove(RemoveEntry);
                                }

                            }
                        }

                        ObjCustomOrderCollection.ListOrderSearchedLiveRunnerInfo = lsttblOrderSearchedLiveRunnerInfo;
                    }
                    catch (Exception ex)
                    {
                        //Added log for exception handling.
                        VendorServices.GeneralService.WriteLog("Some Error Occured in GetOrderDetailsByOrderId(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    }

                    #endregion Get Order Searched Live Runner Info (County/State Info)

                    #region Get Order Searched Personal Info
                    try
                    {
                        List<tblOrderSearchedPersonalInfo> lsttblOrderSearchedPersonalInfo = DX.tblOrderSearchedPersonalInfos.Where(d => d.fkOrderId == ObjtblOrder.pkOrderId).ToList<tblOrderSearchedPersonalInfo>();
                        ObjCustomOrderCollection.ListOrderSearchedPersonalInfo = lsttblOrderSearchedPersonalInfo;
                    }
                    catch (Exception ex)
                    {
                        //Added log for exception handling.
                        VendorServices.GeneralService.WriteLog("Some Error Occured in GetOrderDetailsByOrderId(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    }
                    #endregion Get Order Searched Personal Info

                    #region Get Order Searched Tracking Info
                    try
                    {
                        tblReferenceCode ObjtblReferenceCode = DX.tblReferenceCodes.Join(DX.tblOrderSearchedDatas, r => r.ReferenceCode, ordSearch => ordSearch.SearchedTrackingRef, (r, ordSearch) => new { tblReferenceCode = r, tblOrderSearchedData = ordSearch }).Where(d => d.tblReferenceCode.ReferenceCode == d.tblOrderSearchedData.SearchedTrackingRef && d.tblOrderSearchedData.fkOrderId == ObjtblOrder.pkOrderId).FirstOrDefault().tblReferenceCode;
                        ObjCustomOrderCollection.ObjReferenceCode = ObjtblReferenceCode;
                    }
                    catch (Exception ex)
                    {
                        //Added log for exception handling.
                        VendorServices.GeneralService.WriteLog("Some Error Occured in GetOrderDetailsByOrderId(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    }
                    #endregion Get Order Searched Tracking Info


                }

            }
            return ObjCustomOrderCollection;
        }

        public int UpdateManualResponseByOrderDetailId(int pkOrderDetailId, string strManualResponse)
        {
            int iResult = -1;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                DX.proc_UpdateManualResponseAndStatus(pkOrderDetailId, strManualResponse);
                iResult = 1;
            }

            return iResult;
        }

        public int UpdateResponseAttachByOrderDetailId(int pkOrderDetailId, string ResponseFileName)
        {
            int iResult = -1;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                //INT-59 Upload Document
                tblOrderResponseData tblODData = DX.tblOrderResponseDatas.Where(db => db.fkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                if (tblODData != null)
                {
                    DX.UpdateResponseAttachDocByOrderDetailId(pkOrderDetailId, ResponseFileName);
                    iResult = 1;
                }
            }
            return iResult;
        }

        public void SendPendingMailReminder(int? pkOrderId, DateTime OrderDt, string Response, int ReportStatus, int OrderDetailId, string ProductCode)
        {
            try
            {
                DateTime OrderDate = OrderDt;
                DateTime currentTime = DateTime.Now;
                TimeSpan timeDiffernce = currentTime.Subtract(OrderDate);
                int SendingStatus = -1;
                int MinutesDiff = Convert.ToInt32(timeDiffernce.TotalMinutes);
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    tblOrderDetail tblODData = DX.tblOrderDetails.Where(db => db.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                    string Error = "";
                    XDocument ObjXDocument = XDocument.Parse(Response);
                    XElement ErrorElement = ObjXDocument.Descendants("errors").FirstOrDefault();
                    if (ErrorElement != null)
                    {
                        Error = Convert.ToString(ErrorElement.Value);
                    }
                    if (tblODData != null)
                    {
                        if (MinutesDiff > 10080 && tblODData.IsPendingReminderSent == -1)//-1 mail Not Sent and 1 if mail sent
                        {
                            SendingStatus = SendErrorEmail(pkOrderId, Error, ProductCode, ReportStatus, OrderDetailId);
                            if (SendingStatus > 0)
                            {
                                tblODData.IsPendingReminderSent = 1;
                                DX.SubmitChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in SendPendingMailReminder(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }

        }

        public int SendErrorEmail(int? pkOrderId, string Error, string ProductCode, int Status, int OrderDetailId)
        {
            BALGeneral ObjBALGeneral = new BALGeneral();
            try
            {
                string emailText = "";
                string MessageBody = "";
                string ReportStatus = "";
                // string WebsiteLogo = "https://www.intelifi.com/emerge/Resources/Upload/Images/emerge-logo--no-shadows-(small).gif";
                string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/emerge-logo--no-shadows-(small).gif";
                int SendingStatus = -1;
                if (Status == 1)
                {
                    ReportStatus = "Pending";
                }
                else if (Status == 3)
                    ReportStatus = "Incomplete";
                List<Proc_GetUserNameFromOrderIdResult> ObjReportInfo = ObjBALGeneral.GetUserNameFromOrderId(pkOrderId).ToList<Proc_GetUserNameFromOrderIdResult>();
                //INT334
                //As per greg need, we have changed code and getting values from the web.config files.
                int templateId = Convert.ToInt32(ConfigurationManager.AppSettings["Pending_IncompleteReportReminder"]);
                var emailContent = ObjBALGeneral.GetEmailTemplate(new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852"), templateId);//"New User Registration"
                emailText = emailContent.TemplateContent;
                BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                #region New way Bookmark
                #region For New Style SystemTemplate
                string Bookmarkfile = string.Empty;
                string bookmarkfilepath = string.Empty;

                Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                emailText = Bookmarkfile;
                #endregion

                Bookmarks objBookmark = new Bookmarks();
                objBookmark.EmailContent = emailText;
                objBookmark.OrderType = ReportStatus;
                objBookmark.UserEmail = ObjReportInfo.FirstOrDefault().UserName;
                objBookmark.OrderNumber = ObjReportInfo.FirstOrDefault().OrderNo;
                objBookmark.ReportName = ProductCode;
                objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";

                if (!string.IsNullOrEmpty(Error))
                {
                    objBookmark.Error = Error;
                }
                if (!string.IsNullOrEmpty(Error))
                {
                    MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Guid.Empty);
                }
                else
                {
                    MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Guid.Empty);
                    MessageBody = MessageBody.Replace("Error :", "");
                }

                #endregion
                MomentumInfotech.EmailProvider.MailProvider ObjMailProvider = new MomentumInfotech.EmailProvider.MailProvider();
                ObjMailProvider.MailTo.Add("support@intelifi.com");
                ObjMailProvider.MailFrom = "ReportReminder@intelifi.com";
                ObjMailProvider.Subject = "Reminder For Emerge " + ReportStatus + " Report";
                ObjMailProvider.Message = MessageBody;
                ObjMailProvider.SendMail(out SendingStatus);

                #region Send Incomplete Report Email to client with different template

                try
                {
                    SendErrorEmail_ToClient(pkOrderId, Error, ProductCode, Status, OrderDetailId);
                }
                catch (Exception ex)
                {
                    //Added log for exception handling.
                    VendorServices.GeneralService.WriteLog("Some Error Occured in SendErrorEmail(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                }

                #endregion

                return SendingStatus;
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in SendErrorEmail(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                throw ex;
            }
        }

        public int SendErrorEmail_ToClient(int? pkOrderId, string Error, string ProductCode, int Status, int OrderDetailId)
        {
            BALGeneral ObjBALGeneral = new BALGeneral();
            BALCompany ObjCom = new BALCompany();
            try
            {
                //int? fkCompanyUserId = 0;
                //string ClientMailTo = "";
                string Applicantname = "";
                string ErrorMessage = "";
                string emailText = "";
                string MessageBody = "";
                string ReportStatus = "";
                //string WebsiteLogo = "https://www.intelifi.com/emerge/Resources/Upload/Images/emerge-logo--no-shadows-(small).gif";
                string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/emerge-logo--no-shadows-(small).gif";
                int SendingStatus = -1;
                if (Status == 1)
                {
                    ReportStatus = "Pending";
                }
                else if (Status == 3)
                    ReportStatus = "Incomplete";
                List<Proc_GetUserNameFromOrderIdResult> ObjReportInfo = ObjBALGeneral.GetUserNameFromOrderId(pkOrderId).ToList<Proc_GetUserNameFromOrderIdResult>();
                var emailContent = ObjBALGeneral.GetEmailTemplate(new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852"), 30);//"Incomplete Report"
                emailText = emailContent.TemplateContent;
                BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                #region For New Style SystemTemplate
                string Bookmarkfile = string.Empty;
                string bookmarkfilepath = string.Empty;

                Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                emailText = Bookmarkfile;
                #endregion

                try
                {
                    using (EmergeDALDataContext dx = new EmergeDALDataContext())
                    {
                        var GetApplicantName = dx.tblOrderSearchedDatas.Where(d => d.fkOrderId == pkOrderId).FirstOrDefault();
                        if (GetApplicantName != null)
                        {
                            Applicantname = GetApplicantName.SearchedLastName + " " + GetApplicantName.SearchedLastName;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //Added log for exception handling.
                    VendorServices.GeneralService.WriteLog("Some Error Occured in SendErrorEmail_ToClient(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                }
                ErrorMessage = "Your order is taking longer than expected to complete and is still being processed.<br />"
                    + "Please contact support for more information on this matter.<br />"
                    + "Emerge Support Team | Support@intelifi.com | 888.409.1819";
                #region New way Bookmark
                Bookmarks objBookmark = new Bookmarks();
                objBookmark.ApplicantName = Applicantname;
                objBookmark.EmailContent = emailText;
                objBookmark.OrderType = ReportStatus;
                objBookmark.UserEmail = ObjReportInfo.FirstOrDefault().UserName;
                objBookmark.OrderNumber = ObjReportInfo.FirstOrDefault().OrderNo;
                objBookmark.ReportName = ProductCode;
                objBookmark.Error = ErrorMessage;
                objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Guid.Empty);
                #endregion
                //For email status//
                BALGeneral objbalgenral = new BALGeneral();
                //March 09, 2016
                //INT-334 Incomplete report email going to user and support
                int pktemplateid = Convert.ToInt32(ConfigurationManager.AppSettings["IncompleteReportReminder"]);
                Guid user_ID = Guid.Parse(ObjReportInfo.FirstOrDefault().fkUserId.ToString());
                int? fkcompanyuserid = 0;
                bool email_status = objbalgenral.UserEmailStatusCheck(user_ID, pktemplateid, fkcompanyuserid);
                if (email_status == true)
                {
                    MomentumInfotech.EmailProvider.MailProvider ObjMailProvider = new MomentumInfotech.EmailProvider.MailProvider();
                    ObjMailProvider.MailTo.Add("support@intelifi.com");
                    //March 09, 2016
                    //INT-334 Incomplete report email going to user and support
                    //As per greg, he needs to update code and user email will not be sent.
                    //ObjMailProvider.MailTo.Add(ObjReportInfo.FirstOrDefault().UserName);
                    ObjMailProvider.MailFrom = "ReportReminder@intelifi.com";
                    ObjMailProvider.Subject = "Reminder For Emerge " + ReportStatus + " Report";
                    ObjMailProvider.Message = MessageBody;
                    ObjMailProvider.SendMail(out SendingStatus);
                }
                //
                return SendingStatus;


            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in SendErrorEmail_ToClient(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                throw ex;
            }

        }

        #region Methods to Run CCR1 report If no criminal data is returned but ResultStatus is ‘Hit’ for RCX report

        private void RenameRCXFile(int? pkOrderDetailId)
        {
            try
            {
                if (this.RCXRequestPath != "")
                {
                    string Directory = this.RCXRequestPath;

                    var directory = new DirectoryInfo(Directory);

                    var OldFile = directory.GetFiles().Where(d => d.Name == pkOrderDetailId + ".txt");
                    string OldFileName = this.RCXRequestPath + OldFile.ElementAt(0).Name;
                    string NewFileName = OldFileName.Replace(".txt", "") + "_C.txt";
                    if (!File.Exists(NewFileName))
                    {
                        File.Move(OldFileName, NewFileName);
                    }
                }

            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in RenameRCXFile(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
        }
        public bool GetRCXHitstatus(string responsedata)
        {
            bool IsHitRCX = false;
            try
            {
                XDocument ObjXDocumentRCX = new XDocument();
                string XML_ValueRCX = BALEmergeReview.GetRequiredXml(responsedata);
                ObjXDocumentRCX = XDocument.Parse(XML_ValueRCX);

                var RCXxelementCriminalReport = ObjXDocumentRCX.Descendants("CriminalReport").ToList();
                if (RCXxelementCriminalReport.Count == 0)
                {
                    var RCXxelementScreeningStatus = ObjXDocumentRCX.Descendants("ScreeningStatus").ToList();
                    if (RCXxelementScreeningStatus.Count > 0)
                    {
                        var RCXxelementOrderStatus = RCXxelementScreeningStatus.Descendants("OrderStatus").FirstOrDefault();
                        var RCXxelementResultStatus = RCXxelementScreeningStatus.Descendants("ResultStatus").FirstOrDefault();
                        if (RCXxelementOrderStatus.Value.ToString().ToLower() == "completed" && RCXxelementResultStatus.Value.ToString().ToLower() == "hit")
                        {
                            IsHitRCX = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in GetRCXHitstatus(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            return IsHitRCX;
        }

        public void PlaceCCR1orderForRCXHitReport(int? OrderId, int? OrderDetailId, string ProductCode, string CompanyAccountNo, string OrderCompanyName, string PermissiblePurpose)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                try
                {
                    #region Get The Saved Searched Info to place a new CCR1 Order
                    tblOrder ObjtblOrder = DX.tblOrders.Where(db => db.pkOrderId == OrderId).FirstOrDefault();
                    tblOrderSearchedData ObjtblOrderSearchedData = DX.tblOrderSearchedDatas.Where(db => db.fkOrderId == OrderId).FirstOrDefault();
                    List<tblOrderSearchedEmploymentInfo> ObjCollOrderSearchedEmploymentInfo = DX.tblOrderSearchedEmploymentInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    List<tblOrderSearchedEducationInfo> ObjCollOrderSearchedEducationInfo = DX.tblOrderSearchedEducationInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    List<tblOrderPersonalQtnInfo> ObjCollOrderPersonalQtnInfo = DX.tblOrderPersonalQtnInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    List<tblOrderSearchedPersonalInfo> ObjCollOrderSearchedPersonalInfo = DX.tblOrderSearchedPersonalInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    List<tblOrderSearchedProfessionalInfo> ObjCollOrderSearchedProfessionalInfo = DX.tblOrderSearchedProfessionalInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    List<tblOrderSearchedLicenseInfo> ObjCollOrderLicenseInfo = DX.tblOrderSearchedLicenseInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    List<tblOrderSearchedAliasPersonalInfo> ObjCollOrderSearchedAliasPersonalInfo = DX.tblOrderSearchedAliasPersonalInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    List<tblOrderSearchedAliasLocationInfo> ObjCollOrderSearchedAliasLocationInfo = DX.tblOrderSearchedAliasLocationInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    List<tblOrderSearchedAliasWorkInfo> ObjCollOrderSearchedAliasWorkInfo = DX.tblOrderSearchedAliasWorkInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    tblOrderSearchedLiveRunnerInfo ObjOrderSearchedLiveRunnerInfo = DX.tblOrderSearchedLiveRunnerInfos.Where(db => db.fkOrderId == OrderId && db.fkOrderDetailId == OrderDetailId).FirstOrDefault();
                    tblOrderDetail ObjtblOrderDetail = DX.tblOrderDetails.Where(db => db.fkOrderId == OrderId && db.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                    List<tblVendor> ObjtblVendor_List = DX.tblVendors.ToList();
                    List<tblState> ObjtblState_List = DX.tblStates.ToList();

                    #endregion

                    #region Creating New Request
                    BALProducts ObjBALProducts = new BALProducts();
                    int StateId = 0;
                    string LiveRunnerCounty = "";
                    if (!ProductCode.ToLower().Contains("scr"))
                    {
                        if (ObjOrderSearchedLiveRunnerInfo != null)
                        {
                            LiveRunnerCounty = ObjOrderSearchedLiveRunnerInfo.LiveRunnerCounty;
                        }
                        StateId = Convert.ToInt32(ObjOrderSearchedLiveRunnerInfo.LiveRunnerState);
                        string StateCode = DX.tblStates.Where(db => db.pkStateId == StateId).FirstOrDefault().StateCode;

                        List<Proc_GetAllProductsWithVendorResult> ObjProc_GetAllProductsWithVendorResult = ObjBALProducts.GetAllProductsWithVendor();

                        string SearchedStateCode = (!string.IsNullOrEmpty(ObjtblOrderSearchedData.SearchedStateId.ToString()) ? ObjtblState_List.Where(data => data.pkStateId == ObjtblOrderSearchedData.SearchedStateId).First().StateCode : "");

                        string SearchedDlStateCode = (!string.IsNullOrEmpty(ObjtblOrderSearchedData.SearchedDLStateId.ToString()) ? ObjtblState_List.Where(data => data.pkStateId == ObjtblOrderSearchedData.SearchedDLStateId).First().StateCode : "");

                        Guid SiteApplicationId = new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852");
                        var ProductQty2 = string.Empty;
                        List<Proc_AddEmergeOrderDetailResult> ObjAddOrderDetailResult = DX.Proc_AddEmergeOrderDetail(
                                                                                                        OrderId,
                                                                                                        "CCR1",
                                                                                                        SiteApplicationId,
                                                                                                        ObjtblOrder.fkLocationId,
                                                                                                        ObjtblOrder.OrderDt,
                                                                                                        IsInstantProduct("CCR1"),
                                                                                                        false,
                                                                                                        ObjtblOrderDetail.IsPackageReport,
                                                                                                        ObjtblOrderDetail.PackageInclude,
                                                                                                        ObjtblOrderDetail.fkPackageId,
                                                                                                        ObjtblOrderDetail.ConsentFileName,
                                                                                                        ObjtblOrderDetail.IsConsentEmailorFax,
                                                                                                        ObjtblOrderDetail.IsConsentRequired, ObjtblOrderDetail.ConsentState, ProductQty2, string.Empty).ToList();

                        if (ObjAddOrderDetailResult.First().ReturnType > 0)
                        {
                            int? NewCCR1OrderDetailId = ObjAddOrderDetailResult.First().OrderDetailId;
                            DX.SetCCRFeeInOderDetailsTable(NewCCR1OrderDetailId, "CCR1");/*This set the CCR report fees in the Order Detail Table*/
                            //int iOrderSearchedLiveRunnerInfo = 
                            DX.AddOrderSearchedLiverRunnerInfo(OrderId, NewCCR1OrderDetailId, ObjtblOrderDetail.CountyCode, StateId.ToString(), ObjtblOrderDetail.fkCountyId);
                            //int iOrderDetailCountyinfo = 
                            UpdateCCR1CountyInfo(NewCCR1OrderDetailId, ObjtblOrderDetail.CountyCode, ObjtblOrderDetail.StateCode, ObjtblOrderDetail.fkCountyId);
                            string Vendor_Request = "";
                            Vendor_Request = CreateXMLRequest(ObjtblOrder, "CCR1", ObjtblOrderSearchedData, ObjCollOrderSearchedEmploymentInfo,
                                                                                                            ObjCollOrderSearchedEducationInfo,
                                                                                                            ObjCollOrderSearchedPersonalInfo,
                                                                                                            ObjCollOrderSearchedProfessionalInfo,
                                                                                                                ObjCollOrderPersonalQtnInfo,
                                                                                                                ObjCollOrderLicenseInfo,
                                                                                                                ObjCollOrderSearchedAliasPersonalInfo,
                                                                                                                ObjCollOrderSearchedAliasLocationInfo,
                                                                                                                ObjCollOrderSearchedAliasWorkInfo,
                                                                                                                SiteApplicationId,
                                                                                                                OrderId,
                                                                                                                OrderDetailId,
                                                                                                                ObjtblVendor_List,
                                                                                                                ObjtblState_List,
                                                                                                                StateCode,
                                                                                                                SearchedDlStateCode,
                                                                                                                LiveRunnerCounty,
                                                                                                                ObjProc_GetAllProductsWithVendorResult,
                                                                                                                0, CompanyAccountNo, OrderCompanyName, PermissiblePurpose);
                            //int iResultRequest =
                            InsertOrderRequestData(NewCCR1OrderDetailId, Vendor_Request); /* Add entry in order request table */
                            /* Update ReportIncludes in order table */
                            //int statusReportIncludes = 
                            UpdateReportsIncludes(OrderId);
                    #endregion

                            #region Getting Response
                            string vendor_Res = "";
                            // string Vendor_Req = "";

                            vendor_Res = GetXMLResponse("CCR1", ObjProc_GetAllProductsWithVendorResult, Vendor_Request, OrderId, NewCCR1OrderDetailId, false);


                            if (vendor_Res.Contains("<XML_INTERFACE>ZZZ</XML_INTERFACE>"))
                            {
                                string[] Vendor_Response_Parts = vendor_Res.Split('#');
                                vendor_Res = Vendor_Response_Parts[1].ToString();
                                //Vendor_Req = Vendor_Response_Parts[0].ToString();
                            }
                            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                            {
                                InsertOrderResponse(NewCCR1OrderDetailId, vendor_Res);
                            }
                            CheckResponseandUpdateStatus("CCR1", OrderId, vendor_Res, NewCCR1OrderDetailId, false, CompanyAccountNo, OrderCompanyName, PermissiblePurpose, false);
                            vendor_Res = "";
                            //Vendor_Req = "";
                            #endregion
                        }
                    }
                }
                catch (Exception ex)
                {
                    //Added log for exception handling.
                    VendorServices.GeneralService.WriteLog("Some Error Occured in PlaceCCR1orderForRCXHitReport(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    throw;
                }
            }
        }

        public int UpdateCCR1CountyInfo(int? OrderDetailId, string CountyCode, string StateCode, int fkCountyId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                int Status = -1;
                tblOrderDetail ObjtblOrderDetail = DX.tblOrderDetails.Where(db => db.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                if (ObjtblOrderDetail != null)
                {
                    ObjtblOrderDetail.CountyCode = CountyCode;
                    ObjtblOrderDetail.StateCode = StateCode;
                    ObjtblOrderDetail.fkCountyId = fkCountyId;
                    DX.SubmitChanges();
                    Status = 1;
                }
                return Status;
            }
        }

        public void InsertOrderResponse(int? OrderDetailId, string vendor_Res)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                tblOrderResponseData ObjResponse = new tblOrderResponseData();
                ObjResponse.fkOrderDetailId = OrderDetailId;
                ObjResponse.ResponseData = vendor_Res;
                ObjResponse.EmergeReviewNote = string.Empty;
                ObjResponse.ErrorResponseData = "";
                ObjResponse.IsMatchMeter = false;
                ObjResponse.ManualResponse = string.Empty;
                ObjResponse.ResponseFileName = string.Empty;
                ObjResponse.FilteredResponseData = string.Empty;
                DX.tblOrderResponseDatas.InsertOnSubmit(ObjResponse);
                DX.SubmitChanges();
            }
        }

        public int InsertOrderRequestData(int? OrderDetailId, string Vendor_Request)
        {
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    int Status = -1;
                    tblOrderRequestData ObjRequest = new tblOrderRequestData();
                    ObjRequest.fkOrderDetailId = OrderDetailId;
                    ObjRequest.RequestData = Vendor_Request;
                    DX.tblOrderRequestDatas.InsertOnSubmit(ObjRequest);
                    DX.SubmitChanges();
                    Status = 1;
                    return Status;
                }

            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in InsertOrderRequestData(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                throw;
            }
        }

        public int UpdateReportsIncludes(int? OrderId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                int Status = -1;
                tblOrder ObjOrder = DX.tblOrders.Where(db => db.pkOrderId == OrderId).FirstOrDefault();
                string ReportIncludes = ObjOrder.ReportIncludes;
                ReportIncludes = ReportIncludes + ", CCR1";
                if (ObjOrder != null)
                {
                    ObjOrder.ReportIncludes = ReportIncludes;
                    DX.SubmitChanges();
                    Status = 1;
                }
                return Status;
            }
        }

        #endregion

        # region method will Run PS2/NCR2 report If PS/NCR1 fails to get response.

        /// <summary>
        /// This method will Run PS2/NCR2 report If PS/NCR1 fails to get response.
        /// </summary>
        /// <param name="OrderId"></param>
        /// <param name="OrderDetailId"></param>
        /// <param name="ProductCode"></param>
        public string PlacePS2NCR2orderForPSNCR1Failure(int? OrderId, int? OrderDetailId, string ProductCode, string CompanyAccountNo, string OrderCompanyName, string PermissiblePurpose)
        {
            string vendor_Res = "";
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                try
                {
                    /*DbTransaction DbTrans = null;
                    DbTrans = DX.Connection.BeginTransaction();*/

                    #region Get The Saved Searched Info to place a new PS2/NCR2 Order

                    tblOrder ObjtblOrder = DX.tblOrders.Where(db => db.pkOrderId == OrderId).FirstOrDefault();
                    tblOrderSearchedData ObjtblOrderSearchedData = DX.tblOrderSearchedDatas.Where(db => db.fkOrderId == OrderId).FirstOrDefault();
                    List<tblOrderSearchedEmploymentInfo> ObjCollOrderSearchedEmploymentInfo = DX.tblOrderSearchedEmploymentInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    List<tblOrderSearchedEducationInfo> ObjCollOrderSearchedEducationInfo = DX.tblOrderSearchedEducationInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    List<tblOrderPersonalQtnInfo> ObjCollOrderPersonalQtnInfo = DX.tblOrderPersonalQtnInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    List<tblOrderSearchedPersonalInfo> ObjCollOrderSearchedPersonalInfo = DX.tblOrderSearchedPersonalInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    List<tblOrderSearchedProfessionalInfo> ObjCollOrderSearchedProfessionalInfo = DX.tblOrderSearchedProfessionalInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    List<tblOrderSearchedLicenseInfo> ObjCollOrderLicenseInfo = DX.tblOrderSearchedLicenseInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    List<tblOrderSearchedAliasPersonalInfo> ObjCollOrderSearchedAliasPersonalInfo = DX.tblOrderSearchedAliasPersonalInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    List<tblOrderSearchedAliasLocationInfo> ObjCollOrderSearchedAliasLocationInfo = DX.tblOrderSearchedAliasLocationInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    List<tblOrderSearchedAliasWorkInfo> ObjCollOrderSearchedAliasWorkInfo = DX.tblOrderSearchedAliasWorkInfos.Where(db => db.fkOrderId == OrderId).ToList();
                    //tblOrderSearchedLiveRunnerInfo ObjOrderSearchedLiveRunnerInfo =DX.tblOrderSearchedLiveRunnerInfos.Where(db => db.fkOrderId == OrderId && db.fkOrderDetailId == OrderDetailId).FirstOrDefault();
                    //tblOrderDetail ObjtblOrderDetail = DX.tblOrderDetails.Where(db => db.fkOrderId == OrderId && db.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                    List<tblVendor> ObjtblVendor_List = DX.tblVendors.ToList();
                    List<tblState> ObjtblState_List = DX.tblStates.ToList();

                    #endregion

                    #region Creating New Request
                    BALProducts ObjBALProducts = new BALProducts();
                    string LiveRunnerCounty = "";
                    string Pname = "";

                    List<Proc_GetAllProductsWithVendorResult> ObjProc_GetAllProductsWithVendorResult = ObjBALProducts.GetAllProductsWithVendor();

                    string SearchedStateCode = (!string.IsNullOrEmpty(ObjtblOrderSearchedData.SearchedStateId.ToString()) ? ObjtblState_List.Where(data => data.pkStateId == ObjtblOrderSearchedData.SearchedStateId).First().StateCode : "");

                    string SearchedDlStateCode = (!string.IsNullOrEmpty(ObjtblOrderSearchedData.SearchedDLStateId.ToString()) ? ObjtblState_List.Where(data => data.pkStateId == ObjtblOrderSearchedData.SearchedDLStateId).First().StateCode : "");

                    Guid SiteApplicationId = new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852");
                    if (ProductCode.ToLower() == "ncr1")
                    {
                        Pname = "NCR2";
                    }
                    if (ProductCode.ToLower() == "ps")
                    {
                        Pname = "PS2";
                    }
                    int ProductApplicationId = GetProductApplicationIdByProductCode(Pname);
                    decimal ReportAmount = GetReportAmountByProductApplicationId(Pname, ProductApplicationId, ObjtblOrder.fkLocationId);
                    int result = UpdateProductApplicationIdByOrderDetailId(OrderDetailId, ProductApplicationId, ReportAmount);

                    if (result > 0)
                    {
                        string Vendor_Request = "";
                        Vendor_Request = CreateXMLRequest(ObjtblOrder, Pname, ObjtblOrderSearchedData, ObjCollOrderSearchedEmploymentInfo,
                                                                                                        ObjCollOrderSearchedEducationInfo,
                                                                                                        ObjCollOrderSearchedPersonalInfo,
                                                                                                        ObjCollOrderSearchedProfessionalInfo,
                                                                                                            ObjCollOrderPersonalQtnInfo,
                                                                                                            ObjCollOrderLicenseInfo,
                                                                                                            ObjCollOrderSearchedAliasPersonalInfo,
                                                                                                            ObjCollOrderSearchedAliasLocationInfo,
                                                                                                            ObjCollOrderSearchedAliasWorkInfo,
                                                                                                            SiteApplicationId,
                                                                                                            OrderId,
                                                                                                            OrderDetailId,
                                                                                                            ObjtblVendor_List,
                                                                                                            ObjtblState_List,
                                                                                                            SearchedStateCode,
                                                                                                            SearchedDlStateCode,
                                                                                                            LiveRunnerCounty,
                                                                                                            ObjProc_GetAllProductsWithVendorResult,
                                                                                                            0, CompanyAccountNo, OrderCompanyName, PermissiblePurpose);
                        //int iResultRequest = 
                        UpdateRequestByOrderDetailId(OrderDetailId, Vendor_Request); /* Update entry in order request table */
                    #endregion
                        #region Getting Response
                        #region Transaction Committed
                        //DbTrans.Commit(); /* Commit Transaction */
                        #endregion
                        //string Vendor_Req = "";
                        vendor_Res = GetXMLResponse(Pname, ObjProc_GetAllProductsWithVendorResult, Vendor_Request, OrderId, OrderDetailId, IsInstantProduct(Pname));
                        if (vendor_Res.Contains("<XML_INTERFACE>ZZZ</XML_INTERFACE>"))
                        {
                            string[] Vendor_Response_Parts = vendor_Res.Split('#');
                            vendor_Res = Vendor_Response_Parts[1].ToString();
                            //Vendor_Req = Vendor_Response_Parts[0].ToString();
                        }
                        using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                        {
                            UpdateResponseByOrderDetailId(OrderDetailId, vendor_Res);
                        }

                        CheckResponseandUpdateStatus(Pname, OrderId, vendor_Res, OrderDetailId, false, CompanyAccountNo, OrderCompanyName, PermissiblePurpose, false);
                        //Vendor_Req = "";

                    }
                        #endregion

                }
                catch (Exception ex)
                {
                    //Added log for exception handling.
                    VendorServices.GeneralService.WriteLog("Some Error Occured in PlacePS2NCR2orderForPSNCR1Failure(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    throw;
                }
            }
            return vendor_Res; //changes for INT-213 
        }

        public int GetProductApplicationIdByProductCode(string Pname)
        {
            int ProductPerApplication = 0;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                ProductPerApplication = DX.tblProductPerApplications.Where(db => db.ProductCode == Pname).Select(d => d.pkProductApplicationId).FirstOrDefault();
            }
            return ProductPerApplication;
        }
        public decimal GetReportAmountByProductApplicationId(string Pname, int ProductApplicationId, int? LocationId)
        {
            decimal ReportAmount = 0;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                ReportAmount = DX.tblProductAccesses.Where(db => db.fkLocationId == LocationId && db.fkProductApplicationId == ProductApplicationId).Select(d => d.LocationProductPrice).FirstOrDefault();
                if (ReportAmount == null || ReportAmount == 0)
                {
                    ReportAmount = DX.tblProductPerApplications.Where(db => db.ProductCode == Pname && db.pkProductApplicationId == ProductApplicationId).Select(d => d.ProductPerApplicationPrice).FirstOrDefault();
                }

            }

            return ReportAmount;
        }



        public int UpdateProductApplicationIdByOrderDetailId(int? pkOrderDetailId, int? ProductApplicationId, decimal ReportAmount)
        {
            int iResult = -1;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                tblOrderDetail tblODetail = DX.tblOrderDetails.Where(db => db.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                if (tblODetail != null)
                {
                    tblODetail.fkProductApplicationId = ProductApplicationId;
                    tblODetail.ReportAmount = ReportAmount;
                    tblODetail.fkPackageId = 0;
                    DX.SubmitChanges();
                    iResult = 1;
                }
            }
            return iResult;
        }

        public int UpdateRequestByOrderDetailId(int? pkOrderDetailId, string Vendor_Request)
        {
            int iResult = -1;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                tblOrderRequestData tblORequestData = DX.tblOrderRequestDatas.Where(db => db.fkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                if (tblORequestData != null)
                {
                    tblORequestData.RequestData = Vendor_Request;
                    DX.SubmitChanges();
                    iResult = 1;
                }
            }
            return iResult;
        }

        public int UpdateResponseByOrderDetailId(int? pkOrderDetailId, string vendor_Res)
        {
            int iResult = -1;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                tblOrderResponseData tblOResponseData = DX.tblOrderResponseDatas.Where(db => db.fkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                if (tblOResponseData != null)
                {
                    //updated response
                    DX.UpdateOrderResponse(tblOResponseData.pkOrderResponseId, vendor_Res);
                    iResult = 1;
                }
            }
            return iResult;
        }

        # endregion

        #region SendAutoMailForReview for Test Site

        public void SendAutoMailForReview(int? OrderId, int? OrderDetailId, string ResponseData, bool IsPendingOrder)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                Proc_GetReportInfoForAutoReviewMailResult ReportInfo = DX.GetReportInfoForAutoReviewMail(OrderId, OrderDetailId).FirstOrDefault();
                string emailText = "";
                string MessageBody = "";
                int PkTemplateId = 0;
                int Updatestatus = 0;
                //int Mailsentstatus = 0;
                bool DoSendMail = false;
                int DOBVariation = 0;
                StringBuilder Comments = new StringBuilder();
                try
                {
                    if (ReportInfo != null)
                    {
                        #region Check If there are records in Vendor Response
                        int NUllDOB = 0;
                        DoSendMail = CheckChargesInResponse(ReportInfo.ProductCode, ResponseData, ReportInfo.IsLiveRunner, out NUllDOB);
                        #endregion
                        BALCompany ObjBALCompany = new BALCompany();
                        bool IsAdmin = false;
                        bool UserCheck = false;
                        bool ReportExist = false;
                        bool Count_Max = false;
                        bool Count_Min = false;
                        // bool Max_Check = false;
                        //bool Min_Check = false;
                        bool IsthisPackageReport = false;
                        int CountMax = 0;
                        int CountMin = 0;
                        string FilterId = "2";//Auto Emerge review for DOB variations
                        var Collection = ObjBALCompany.GetFiltersAccordingFilterId(FilterId).FirstOrDefault();
                        var ObjData = (from u in DX.tblOrderDetails
                                       where u.pkOrderDetailId == OrderDetailId
                                       select u).FirstOrDefault();
                        if (IsPendingOrder == false)
                        {
                            MembershipUser ObjMembershipUser = Membership.GetUser(Utilitys.GetUID(HttpContext.Current.User.Identity.Name));
                            if (ObjMembershipUser != null)
                            {
                                if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SystemAdmin"))
                                {
                                    IsAdmin = true;
                                }
                                if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SupportManager"))
                                {
                                    IsAdmin = true;
                                }
                                if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SupportTechnician"))
                                {
                                    IsAdmin = true;
                                }

                                if (ObjData.IsPackageReport == true)
                                {
                                    if (Collection.Packages == true)
                                    {
                                        IsthisPackageReport = false;
                                    }
                                    else
                                    {
                                        IsthisPackageReport = true;
                                    }
                                }
                                else
                                {
                                    IsthisPackageReport = false;
                                }

                                DOBVariation = Collection.Count == null ? Convert.ToInt32(ConfigurationManager.AppSettings["DOBVariation"].ToString()) : Convert.ToInt32(Collection.Count);
                                if (string.IsNullOrEmpty(Collection.CountMax.ToString()) == false)
                                {
                                    CountMax = int.Parse(Collection.CountMax.ToString());
                                    Count_Max = true;
                                }
                                if (string.IsNullOrEmpty(Collection.CountMin.ToString()) == false)
                                {
                                    CountMin = int.Parse(Collection.CountMin.ToString());
                                    Count_Min = true;
                                }
                                var Report_Type = Collection.ReportType;
                                var CollProduct = Report_Type.Split(',');
                                string Coll = ReportInfo.ProductCode.ToLower();
                                if (CollProduct.Length > 0)
                                {
                                    for (int m = 0; m < CollProduct.Length; m++)
                                    {
                                        if (Coll == CollProduct[m].ToLower())
                                        {
                                            ReportExist = true;
                                        }
                                    }
                                }


                                #region For Auto Emerge review for DOB variations

                                if (Collection.Enabled == true)
                                {
                                    if (Collection.Users == 1) { UserCheck = true; }
                                    if (Collection.Users == 2)
                                    {
                                        if (IsAdmin == true)
                                        {
                                            UserCheck = true;
                                        }
                                    }
                                    if (Collection.Users == 3)
                                    {
                                        if (IsAdmin == false)
                                        {
                                            UserCheck = true;
                                        }
                                    }
                                    if (UserCheck == true)
                                    {
                                        if (IsthisPackageReport == false)
                                        {
                                            if (NUllDOB >= DOBVariation && ReportExist == true)
                                            {
                                                if (Collection.IsEmergeReview == true)
                                                {
                                                    BALGeneral ObjBALGeneral = new BALGeneral();
                                                    BALEmergeReview ObjReview = new BALEmergeReview();
                                                    // XMLFilteration XMLFilteration = new XMLFilteration();
                                                    bool Is6MonthOld = false;
                                                    Is6MonthOld = ObjBALGeneral.Is6MonthOld(OrderId);
                                                    Updatestatus = ObjReview.UpdateReportReviewStatus(OrderDetailId, 1, string.Empty, Is6MonthOld);
                                                    if (Updatestatus == 1)
                                                    {
                                                        Comments.Append(" <table width=\"97%\"><tr><td>Report ID : </td><td>" + ReportInfo.OrderNo + "</td>"
                                                            + "</tr> <tr><td>Report Name : </td><td>" + ReportInfo.ProductDisplayName + "</td></tr>"
                                                   + "<tr><td>User : </td><td>" + ReportInfo.LastName + " " + ReportInfo.FirstName + " (<b>UserName : </b>" + ReportInfo.UserName + ") " + "</td></tr>"
                                                   + " <tr><td valign=\"top\"  style=\"width:150px;\">Additional Comments :</td><td> Auto request for " + ReportInfo.ProductCode + " report review.</td></tr>  </table>");
                                                        string BoxHeaderText = string.Empty;


                                                        Guid ApplicationId = new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852");
                                                        string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/emerge-logo--no-shadows-(small).gif";
                                                        PkTemplateId = 27;
                                                        var emailContent = ObjBALGeneral.GetEmailTemplate(ApplicationId, PkTemplateId);// "Emerge Suggestions"
                                                        emailText = emailContent.TemplateContent;
                                                        CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
                                                        TextInfo textInfo = cultureInfo.TextInfo;
                                                        string Searchedfirstname = ReportInfo.SearchedFirstName != null ? textInfo.ToTitleCase(ReportInfo.SearchedFirstName) : string.Empty;
                                                        string Searchedlastname = ReportInfo.SearchedLastName != null ? textInfo.ToTitleCase(ReportInfo.SearchedLastName) : string.Empty;

                                                        #region New way Bookmark

                                                        BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                                                        Bookmarks objBookmark = new Bookmarks();
                                                        objBookmark.EmailContent = emailText;

                                                        #region For New Style SystemTemplate
                                                        string Bookmarkfile = string.Empty;
                                                        string bookmarkfilepath = string.Empty;

                                                        Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                                                        Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                                                        Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                                                        emailText = Bookmarkfile;
                                                        #endregion

                                                        BoxHeaderText = emailContent.TemplateSubject.Replace("%ApplicantName%", Searchedlastname + " " + Searchedfirstname).Replace("%ReportCode%", ReportInfo.ProductCode);
                                                        objBookmark.Subject = BoxHeaderText;
                                                        objBookmark.ApplicantName = Searchedlastname + " " + Searchedfirstname;
                                                        objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                                                        objBookmark.UserCompany = "";
                                                        objBookmark.UserEmail = ReportInfo.LastName + " " + ReportInfo.FirstName + " (<b>UserName : </b>" + ReportInfo.UserName + ") ";
                                                        objBookmark.EmergeReviewComment = Comments.ToString();
                                                        MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, ReportInfo.UserId);
                                                        #endregion
                                                        string successAdmin = string.Empty;
                                                        if (IsPendingOrder == true)
                                                        {
                                                            successAdmin = BALEmergeReview.SendMail(ConfigurationManager.AppSettings["ReportReviewMail"].ToString(), ReportInfo.UserName.ToLower(), string.Empty, ConfigurationManager.AppSettings["ReportReviewFromAddress"].ToString(), MessageBody, BoxHeaderText);
                                                            if (successAdmin == "Success")
                                                            {
                                                                //Mailsentstatus = 1;
                                                                string SucessMsg = "Email Successfully Sent for Order No:-" + ReportInfo.OrderNo;
                                                                ObjReview.WriteReviewStatusInTextFile(SucessMsg, ConfigurationManager.AppSettings["ReportReviewMail"].ToString());
                                                            }
                                                            else
                                                            {
                                                                //Mailsentstatus = 0;
                                                                string UnSucessMsg = "Email Sending Failed for Order No:-" + ReportInfo.OrderNo;
                                                                UnSucessMsg = UnSucessMsg + " Error:" + successAdmin;
                                                                ObjReview.WriteReviewStatusInTextFile(UnSucessMsg, ConfigurationManager.AppSettings["ReportReviewMail"].ToString());
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            if (ObjData.IsPackageReport == true)
                            {
                                if (Collection.Packages == true)
                                {
                                    IsthisPackageReport = false;
                                }
                                else
                                {
                                    IsthisPackageReport = true;
                                }
                            }
                            else
                            {
                                IsthisPackageReport = false;
                            }

                            DOBVariation = Collection.Count == null ? Convert.ToInt32(ConfigurationManager.AppSettings["DOBVariation"].ToString()) : Convert.ToInt32(Collection.Count);
                            if (string.IsNullOrEmpty(Collection.CountMax.ToString()) == false)
                            {
                                CountMax = int.Parse(Collection.CountMax.ToString());
                                Count_Max = true;
                            }
                            if (string.IsNullOrEmpty(Collection.CountMin.ToString()) == false)
                            {
                                CountMin = int.Parse(Collection.CountMin.ToString());
                                Count_Min = true;
                            }
                            var Report_Type = Collection.ReportType;
                            var CollProduct = Report_Type.Split(',');
                            string Coll = ReportInfo.ProductCode.ToLower();
                            if (CollProduct.Length > 0)
                            {
                                for (int m = 0; m < CollProduct.Length; m++)
                                {
                                    if (Coll == CollProduct[m].ToLower())
                                    {
                                        ReportExist = true;
                                    }
                                }
                            }

                            #region ForAuto Emerge review for DOB variations

                            if (Collection.Enabled == true)
                            {

                                if (IsthisPackageReport == false)
                                {
                                    if (NUllDOB >= DOBVariation && ReportExist == true)
                                    {
                                        if (Collection.IsEmergeReview == true)
                                        {
                                            BALGeneral ObjBALGeneral = new BALGeneral();
                                            BALEmergeReview ObjReview = new BALEmergeReview();
                                            //XMLFilteration XMLFilteration = new XMLFilteration();
                                            bool Is6MonthOld = false;
                                            Is6MonthOld = ObjBALGeneral.Is6MonthOld(OrderId);
                                            Updatestatus = ObjReview.UpdateReportReviewStatus(OrderDetailId, 1, string.Empty, Is6MonthOld);
                                            if (Updatestatus == 1)
                                            {
                                                //XMLFilteration.UpdateOrderHitStatus(OrderId, 1);
                                                Comments.Append(" <table width=\"97%\"><tr><td>Report ID : </td><td>" + ReportInfo.OrderNo + "</td>"
                                                    + "</tr> <tr><td>Report Name : </td><td>" + ReportInfo.ProductDisplayName + "</td></tr>"
                                           + "<tr><td>User : </td><td>" + ReportInfo.LastName + " " + ReportInfo.FirstName + " (<b>UserName : </b>" + ReportInfo.UserName + ") " + "</td></tr>"
                                           + " <tr><td valign=\"top\"  style=\"width:150px;\">Additional Comments :</td><td> Auto request for " + ReportInfo.ProductCode + " report review.</td></tr>  </table>");
                                                string BoxHeaderText = string.Empty;


                                                Guid ApplicationId = new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852");
                                                string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/emerge-logo--no-shadows-(small).gif";
                                                PkTemplateId = 27;
                                                var emailContent = ObjBALGeneral.GetEmailTemplate(ApplicationId, PkTemplateId);// "Emerge Suggestions"
                                                emailText = emailContent.TemplateContent;
                                                CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
                                                TextInfo textInfo = cultureInfo.TextInfo;
                                                string Searchedfirstname = ReportInfo.SearchedFirstName != null ? textInfo.ToTitleCase(ReportInfo.SearchedFirstName) : string.Empty;
                                                string Searchedlastname = ReportInfo.SearchedLastName != null ? textInfo.ToTitleCase(ReportInfo.SearchedLastName) : string.Empty;

                                                #region New way Bookmark

                                                BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                                                Bookmarks objBookmark = new Bookmarks();
                                                objBookmark.EmailContent = emailText;

                                                #region For New Style SystemTemplate
                                                string Bookmarkfile = string.Empty;
                                                string bookmarkfilepath = string.Empty;

                                                Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                                                Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                                                Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                                                emailText = Bookmarkfile;
                                                #endregion

                                                BoxHeaderText = emailContent.TemplateSubject.Replace("%ApplicantName%", Searchedlastname + " " + Searchedfirstname).Replace("%ReportCode%", ReportInfo.ProductCode);
                                                objBookmark.Subject = BoxHeaderText;
                                                objBookmark.ApplicantName = Searchedlastname + " " + Searchedfirstname;
                                                objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                                                objBookmark.UserCompany = "";
                                                objBookmark.UserEmail = ReportInfo.LastName + " " + ReportInfo.FirstName + " (<b>UserName : </b>" + ReportInfo.UserName + ") ";
                                                objBookmark.EmergeReviewComment = Comments.ToString();
                                                MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, ReportInfo.UserId);
                                                #endregion
                                                string successAdmin = string.Empty;
                                                if (IsPendingOrder == true)
                                                {
                                                    successAdmin = BALEmergeReview.SendMail(ConfigurationManager.AppSettings["ReportReviewMail"].ToString(), ReportInfo.UserName.ToLower(), string.Empty, ConfigurationManager.AppSettings["ReportReviewFromAddress"].ToString(), MessageBody, BoxHeaderText);
                                                    if (successAdmin == "Success")
                                                    {
                                                        //Mailsentstatus = 1;
                                                        string SucessMsg = "Email Successfully Sent for Order No:-" + ReportInfo.OrderNo;
                                                        ObjReview.WriteReviewStatusInTextFile(SucessMsg, ConfigurationManager.AppSettings["ReportReviewMail"].ToString());
                                                    }
                                                    else
                                                    {
                                                        //Mailsentstatus = 0;
                                                        string UnSucessMsg = "Email Sending Failed for Order No:-" + ReportInfo.OrderNo;
                                                        UnSucessMsg = UnSucessMsg + " Error:" + successAdmin;
                                                        ObjReview.WriteReviewStatusInTextFile(UnSucessMsg, ConfigurationManager.AppSettings["ReportReviewMail"].ToString());
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion
                        }

                        #region one another method For Auto Emerge review except filters and dob variation for ticket #83
                        string LogFilename = "ManuallyEmergeReview_" + DateTime.Now.ToString("yyMMdd");
                        //INT-202 User lavel Autoreview
                        bool AutoReviewUser = false;
                        string strMReview = string.Empty;
                        BALGeneral ObjBALGenerals = new BALGeneral();
                        BALEmergeReview ObjReviews = new BALEmergeReview();
                        bool Is6MonthOlds = false;
                        Is6MonthOlds = ObjBALGenerals.Is6MonthOld(OrderId);
                        bool v = CheckChargesInResponseNCR(ReportInfo.ProductCode, ResponseData);
                        using (EmergeDALDataContext _EmergeDALDataContext = new EmergeDALDataContext())
                        {

                            var AutoEmergeReview = (from tbluser in _EmergeDALDataContext.tblCompanyUsers
                                                    join tblorder in _EmergeDALDataContext.tblOrders on tbluser.pkCompanyUserId equals tblorder.fkCompanyUserId
                                                    where tblorder.pkOrderId == OrderId
                                                    select tbluser.AutoEmergeReview).FirstOrDefault();
                            AutoReviewUser = AutoEmergeReview;
                        }

                        #region changes done for INT 23 FOR AUTO MATIC EMERGE REVIEW FOR SELECTED SSN NO STORED IN LIST AS COMPLAINCE LIST OF SSN
                        //change for int 23 
                        using (EmergeDALDataContext _EmergeDALDataContext = new EmergeDALDataContext())
                        {
                            // make auto emerge review true for ssn data that are present in data base 
                            var ResultAutomaticEmergeUser = from c in _EmergeDALDataContext.tblSSNComplainceReviewsMains
                                                            join o in _EmergeDALDataContext.tblOrderSearchedDatas on c.SSN_NO equals o.SearchedSsn
                                                            where o.fkOrderId == OrderId && c.IsEnabled == true && c.SSN_NO != string.Empty
                                                            select new { Count = c.SSN_NO };
                            if (ResultAutomaticEmergeUser != null)
                            {
                                if (ResultAutomaticEmergeUser.Count() >= 1)
                                {

                                    AutoReviewUser = true;
                                }
                            }
                        }
                        //change for int 23 
                        #endregion

                        strMReview = "AutoReviewUser : " + Convert.ToString(AutoReviewUser) + "---IsEnableReview : " + Convert.ToString(ReportInfo.IsEnableReview) + "---IsAutoReview : " + Convert.ToString(ReportInfo.IsAutoReview);
                        BALGeneral.WriteLogForDeletion(strMReview, LogFilename);

                        if (AutoReviewUser && ReportInfo.IsEnableReview && ReportInfo.IsAutoReview)//User has enabled for auto review process.
                        {
                            if (DoSendMail == true)//Send a mail if CCR1 or SCR applicable for auto review process.
                            {
                                //To check auto review process.
                                SetAutoReviewAndSendMail(Updatestatus, ObjReviews, OrderDetailId, Is6MonthOlds, Comments, ReportInfo, IsPendingOrder, ObjBALGenerals);
                                strMReview = "SendAutoMailForReview--- CCR or SCR if (AutoReviewUser && ReportInfo.IsEnableReview && ReportInfo.IsAutoReview)---SetAutoReviewAndSendMail--OrderDetailId : " + Convert.ToString(OrderDetailId) + "----ReviewStatus : " + Convert.ToString(Updatestatus) + "\n\n";
                                BALGeneral.WriteLogForDeletion(strMReview, LogFilename);
                            }
                            else
                            {
                                if (ReportInfo.ProductCode != "CCR1" && ReportInfo.ProductCode != "SCR")
                                {
                                    //Other than CCR1 or SCR
                                    //Set review-status completed to review.
                                    if (ReportInfo.ProductCode == "NCR1")
                                    {
                                        if (v)//If no record found in NCR1
                                        {
                                            Updatestatus = ObjReviews.UpdateReportReviewStatus(OrderDetailId, 1, string.Empty, Is6MonthOlds);
                                            strMReview = "SendAutoMailForReview---Other than CCR1 or SCR---if (ReportInfo.ProductCode != CCR1 || ReportInfo.ProductCode != SCR)--OrderDetailId : " + Convert.ToString(OrderDetailId) + "----ReviewStatus : " + Convert.ToString(Updatestatus) + "\n\n";
                                            BALGeneral.WriteLogForDeletion(strMReview, LogFilename);
                                        }
                                    }
                                    else
                                    {
                                        Updatestatus = ObjReviews.UpdateReportReviewStatus(OrderDetailId, 1, string.Empty, Is6MonthOlds);
                                        strMReview = "SendAutoMailForReview---Other than CCR1 or SCR---if (ReportInfo.ProductCode != CCR1 || ReportInfo.ProductCode != SCR)--OrderDetailId : " + Convert.ToString(OrderDetailId) + "----ReviewStatus : " + Convert.ToString(Updatestatus) + "\n\n";
                                        BALGeneral.WriteLogForDeletion(strMReview, LogFilename);
                                    }
                                }
                                else
                                {
                                    strMReview = "1 . SendAutoMailForReview OrderDetailId : " + Convert.ToString(OrderDetailId) + "---Product Code : " + ReportInfo.ProductCode;
                                    BALGeneral.WriteLogForDeletion(strMReview, LogFilename);
                                }
                            }
                        }
                        else
                        {
                            if (ReportInfo.IsEnableReview && ReportInfo.IsAutoReview && ReportInfo.ProductReviewStatus == 2)
                            {
                                if (DoSendMail == true)
                                {
                                    //To check auto review process.
                                    SetAutoReviewAndSendMail(Updatestatus, ObjReviews, OrderDetailId, Is6MonthOlds, Comments, ReportInfo, IsPendingOrder, ObjBALGenerals);
                                    strMReview = "SendAutoMailForReview---SCR or CCR if (ReportInfo.IsEnableReview && ReportInfo.IsAutoReview && ReportInfo.ProductReviewStatus == 2) and  if (DoSendMail == true)---SetAutoReviewAndSendMail--OrderDetailId : " + Convert.ToString(OrderDetailId) + "----ReviewStatus : " + Convert.ToString(Updatestatus) + "\n\n";
                                    BALGeneral.WriteLogForDeletion(strMReview, LogFilename);
                                }
                                else
                                {
                                    if (ReportInfo.ProductCode != "CCR1" && ReportInfo.ProductCode != "SCR")
                                    {
                                        //Other than CCR1 or SCR
                                        //Set review-status completed to review.
                                        if (ReportInfo.ProductCode == "NCR1")
                                        {
                                            if (v)//If no record found in NCR1
                                            {
                                                Updatestatus = ObjReviews.UpdateReportReviewStatus(OrderDetailId, 1, string.Empty, Is6MonthOlds);
                                                strMReview = "SendAutoMailForReview---Other than CCR1 or SCR---if (ReportInfo.ProductCode != CCR1 || ReportInfo.ProductCode != SCR)--OrderDetailId : " + Convert.ToString(OrderDetailId) + "----ReviewStatus : " + Convert.ToString(Updatestatus) + "\n\n";
                                                BALGeneral.WriteLogForDeletion(strMReview, LogFilename);
                                            }
                                        }
                                        else
                                        {
                                            Updatestatus = ObjReviews.UpdateReportReviewStatus(OrderDetailId, 1, string.Empty, Is6MonthOlds);
                                            strMReview = "SendAutoMailForReview---Other than CCR1 or SCR---if (ReportInfo.ProductCode != CCR1 || ReportInfo.ProductCode != SCR)--OrderDetailId : " + Convert.ToString(OrderDetailId) + "----ReviewStatus : " + Convert.ToString(Updatestatus) + "\n\n";
                                            BALGeneral.WriteLogForDeletion(strMReview, LogFilename);
                                        }
                                    }
                                    else
                                    {
                                        strMReview = "1 . SendAutoMailForReview OrderDetailId : " + Convert.ToString(OrderDetailId) + "---Product Code : " + ReportInfo.ProductCode;
                                        BALGeneral.WriteLogForDeletion(strMReview, LogFilename);
                                    }
                                }
                            }
                            else
                            {
                                //If DOBVeriation found and in filters "Count" value changed then it will be executed. INT-269
                                if (NUllDOB >= DOBVariation && ReportExist == true)
                                {
                                    if (ReportInfo.ProductCode == "NCR1" && DoSendMail == true)
                                    {
                                        if (v)//If no record found in NCR1
                                        {
                                            //To check auto review process.
                                            SetAutoReviewAndSendMail(Updatestatus, ObjReviews, OrderDetailId, Is6MonthOlds, Comments, ReportInfo, IsPendingOrder, ObjBALGenerals);
                                            strMReview = "SendAutoMailForReview DOBVeriation found and in filters Count value has changed OrderDetailId : " + Convert.ToString(OrderDetailId) + "----ReviewStatus : " + Convert.ToString(Updatestatus) + "\n";
                                            BALGeneral.WriteLogForDeletion(strMReview, LogFilename);
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    //Added log for exception handling.
                    VendorServices.GeneralService.WriteLog("Some Error Occured in SendAutoMailForReview(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                }
            }
        }

        /// <summary>
        /// To send auto review process based on user setting or compnay edit page setting.
        /// </summary>
        public void SetAutoReviewAndSendMail(int Updatestatus, BALEmergeReview ObjReviews, int? OrderDetailId, bool Is6MonthOlds, StringBuilder Comments, Proc_GetReportInfoForAutoReviewMailResult ReportInfo, bool IsPendingOrder, BALGeneral ObjBALGenerals)
        {
            //Set review-status complete to in-review.
            Updatestatus = ObjReviews.UpdateReportReviewStatus(OrderDetailId, 1, string.Empty, Is6MonthOlds);
            if (Updatestatus == 1)
            {
                Comments.Append(" <table width=\"97%\"><tr><td>Report ID : </td><td>" + ReportInfo.OrderNo + "</td>"
                    + "</tr> <tr><td>Report Name : </td><td>" + ReportInfo.ProductDisplayName + "</td></tr>"
           + "<tr><td>User : </td><td>" + ReportInfo.LastName + " " + ReportInfo.FirstName + " (<b>UserName : </b>" + ReportInfo.UserName + ") " + "</td></tr>"
           + " <tr><td valign=\"top\"  style=\"width:150px;\">Additional Comments :</td><td> Auto request for " + ReportInfo.ProductCode + " report review.</td></tr>  </table>");
                string BoxHeaderText = string.Empty;

                Guid ApplicationId = new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852");
                string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/emerge-logo--no-shadows-(small).gif";
                int PkTemplateId = 27;
                var emailContent = ObjBALGenerals.GetEmailTemplate(ApplicationId, PkTemplateId);// "Emerge Suggestions"
                string emailText = emailContent.TemplateContent;

                CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
                TextInfo textInfo = cultureInfo.TextInfo;
                string Searchedfirstname = ReportInfo.SearchedFirstName != null ? textInfo.ToTitleCase(ReportInfo.SearchedFirstName) : string.Empty;
                string Searchedlastname = ReportInfo.SearchedLastName != null ? textInfo.ToTitleCase(ReportInfo.SearchedLastName) : string.Empty;

                #region New way Bookmark

                BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                Bookmarks objBookmark = new Bookmarks();

                objBookmark.EmailContent = emailText;

                #region For New Style SystemTemplate
                string Bookmarkfile = string.Empty;
                string bookmarkfilepath = string.Empty;

                Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                emailText = Bookmarkfile;
                #endregion

                BoxHeaderText = emailContent.TemplateSubject.Replace("%ApplicantName%", Searchedlastname + " " + Searchedfirstname).Replace("%ReportCode%", ReportInfo.ProductCode);

                objBookmark.Subject = BoxHeaderText;
                objBookmark.ApplicantName = Searchedlastname + " " + Searchedfirstname;
                objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                objBookmark.UserCompany = "";
                objBookmark.UserEmail = ReportInfo.LastName + " " + ReportInfo.FirstName + " (<b>UserName : </b>" + ReportInfo.UserName + ") ";
                objBookmark.EmergeReviewComment = Comments.ToString();

                string MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, ReportInfo.UserId);
                string MailTo = "support@intelifi.com";//"support@dotnetoutsourcing.com"
                #endregion
                string successAdmin = string.Empty;
                if (IsPendingOrder == true)
                {
                    successAdmin = BALEmergeReview.SendMail(ConfigurationManager.AppSettings["ReportReviewMail"].ToString(), ReportInfo.UserName.ToLower(), string.Empty, ConfigurationManager.AppSettings["ReportReviewFromAddress"].ToString(), MessageBody, BoxHeaderText);
                    if (successAdmin == "Success")
                    {
                        string SucessMsg = "Email Successfully Sent for Order No:-" + ReportInfo.OrderNo;
                        ObjReviews.WriteReviewStatusInTextFile(SucessMsg, ConfigurationManager.AppSettings["ReportReviewMail"].ToString());
                    }
                    else
                    {
                        string UnSucessMsg = "Email Sending Failed for Order No:-" + ReportInfo.OrderNo;
                        UnSucessMsg = UnSucessMsg + " Error:" + successAdmin;
                        ObjReviews.WriteReviewStatusInTextFile(UnSucessMsg, ConfigurationManager.AppSettings["ReportReviewMail"].ToString());
                    }
                }
            }
        }
        #endregion

        /// <summary>
        // To send mail after completed the order.
        /// </summary>
        /// <param name="MailTo"></param>
        /// <param name="MailFrom"></param>
        /// <param name="EmailBody"></param>
        /// <param name="Subject"></param>
        /// <param name="pkOrderId"></param>
        /// <returns></returns>
        public bool SendEmailForCompletion(string MailTo, string MailFrom, string EmailBody, string Subject, string pkOrderId)
        {
            int SendingStatus = 0;
            bool _IsBodyHtml = true;

            using (MailMessage objMailMessage = new MailMessage())
            {
                try
                {
                    SmtpClient objSmtpClient = new SmtpClient();
                    MailFrom = "emerge@intelifi.com";
                    #region //Add Sender Address
                    if (MailFrom != null)

                        objMailMessage.From = new MailAddress(MailFrom);
                    #endregion

                    #region //Add Receipient Address (TO)

                    objMailMessage.To.Add(MailTo);
                    #endregion
                    #region //Add Subject
                    objMailMessage.Subject = Subject;
                    #endregion
                    #region //Message Body Html or Text
                    objMailMessage.IsBodyHtml = _IsBodyHtml;
                    #endregion
                    #region //Message Priority
                    objMailMessage.Priority = MailPriority.High;
                    #endregion
                    #region // Message Delivery Notification
                    objMailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                    #endregion
                    #region //======== Message Body ==========
                    if (_IsBodyHtml == true)
                    {
                        #region // This way prevent message to be SPAM
                        objMailMessage.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                        AlternateView plainView = AlternateView.CreateAlternateViewFromString
                            (System.Text.RegularExpressions.Regex.Replace(EmailBody, @"<(.|\n)*?>", string.Empty),
                            null, "text/plain");
                        AlternateView htmlView = AlternateView.CreateAlternateViewFromString(EmailBody, null, "text/html");

                        objMailMessage.AlternateViews.Add(plainView);
                        objMailMessage.AlternateViews.Add(htmlView);
                        #endregion
                    }
                    else
                        objMailMessage.Body = EmailBody;
                    #endregion
                    #region //Message Encoding
                    objMailMessage.BodyEncoding = System.Text.Encoding.UTF8;
                    #endregion
                    #region
                    // Message Sent
                    objSmtpClient.Send(objMailMessage);
                    #endregion
                    SendingStatus = 1;

                }
                catch //(Exception ex)
                {
                    SendingStatus = 0;

                }
            }
            return (SendingStatus == 1 ? true : false);
        }
        /// <summary>
        /// send mail to users for ticket #101
        /// </summary>
        /// <param name="MailTo"></param>
        /// <param name="MailFrom"></param>
        /// <param name="EmailBody"></param>
        /// <param name="Subject"></param>
        /// <returns></returns>
        public bool SendEmail(string MailTo, string CC, string MailFrom, string EmailBody, string Subject)
        {
            int SendingStatus = 0;
            try
            {
                MomentumInfotech.EmailProvider.MailProvider ObjMailProvider = new MomentumInfotech.EmailProvider.MailProvider();

                List<string> BCC_List = new List<string>();
                List<string> CC_List = new List<string>();
                if (!string.IsNullOrEmpty(CC))
                {
                    CC_List.Add(CC);
                }
                ObjMailProvider.MailTo.Add(MailTo);
                ObjMailProvider.MailFrom = MailFrom;
                ObjMailProvider.MailCC = CC_List;
                ObjMailProvider.Message = EmailBody;
                ObjMailProvider.MailBCC = BCC_List;
                ObjMailProvider.Subject = Subject;
                ObjMailProvider.IsBodyHtml = true;

                ObjMailProvider.Priority = MailPriority.High;
                ObjMailProvider.SendMail(out SendingStatus);
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in SendEmail(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }

            return (SendingStatus == 1 ? true : false);
        }
        private bool CheckChargesInResponse(string ProductCode, string ResponseData, bool IsLiveRunner, out int NUllDOB)
        {
            int RemainingNUllDOB = 0;
            bool IsHavingRecords = false;
            try
            {
                if ((ProductCode == "CCR1" && IsLiveRunner == true) || (ProductCode == "SCR" && IsLiveRunner == true) || ProductCode == "NCR1" || ProductCode == "RCX") //INT-140 (reports not triggering automatic emerge review when it is set in company price page)
                {
                    string XML_ValueCharges = GetRequiredXml_ForRapidCourt(ResponseData);
                    XDocument ObjXDocument = XDocument.Parse(XML_ValueCharges);
                    var List_Charges = ObjXDocument.Descendants("CriminalCase").ToList();
                    if (List_Charges.Count > 0)
                    {
                        foreach (XElement user in ObjXDocument.Descendants("CriminalCase"))
                        {
                            foreach (XElement contact in user.Descendants("DemographicDetail"))
                            {
                                var dob = user.Descendants("DateOfBirth");
                                if (dob.Count() == 0)
                                { RemainingNUllDOB++; }
                                else { }
                            }
                        }
                        IsHavingRecords = true;
                    }
                }
                else if ((ProductCode == "CCR1" && IsLiveRunner == false) || (ProductCode == "SCR" && IsLiveRunner == false)) // check auto review for CCR1 and SCR through Xpedite
                {
                    XDocument ObjXDocument = XDocument.Parse(ResponseData);
                    var xElementresponse = ObjXDocument.Descendants("report-detail-header").ToList();
                    if (xElementresponse != null && xElementresponse.Count > 0)
                    {
                        var response_clear_record = ObjXDocument.Descendants("clear-record").ToList();
                        if (response_clear_record != null && response_clear_record.FirstOrDefault().Value.ToString() == "0")
                        {
                            IsHavingRecords = true;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in CheckChargesInResponse(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            NUllDOB = RemainingNUllDOB;
            return IsHavingRecords;
        }
        private bool CheckChargesInResponseNCR(string ProductCode, string ResponseData)
        {
            bool IsHavingRecords = false;
            try
            {
                if (ProductCode == "NCR1")
                {
                    string XML_ValueCharges = GetRequiredXml_ForRapidCourt(ResponseData);
                    XDocument ObjXDocument = XDocument.Parse(XML_ValueCharges);
                    var List_Charges = ObjXDocument.Descendants("CriminalCase").ToList();
                    if (List_Charges.Count > 0)
                    {
                        IsHavingRecords = true;
                    }
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in CheckChargesInResponseNCR(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            return IsHavingRecords;
        }
        public static string GetRequiredXml_ForRapidCourt(string XmlResponse)
        {
            string XmlResponse_Local = XmlResponse;
            XmlResponse_Local = XmlResponse_Local.Replace("</BackgroundReports>", "");
            int xmlstartindex = XmlResponse_Local.IndexOf("<BackgroundReportPackage>");
            XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
            return XmlResponse_Local;
        }

        public void SendBatchProcessNotification(string ClientEmail, string firstName, string lastname, string date, string emailText, string Subject)
        {
            string MessageBody = "";
            //StringBuilder Comments = new StringBuilder();
            try
            {
                string MailToAddress = "";
                MailToAddress = ClientEmail;
                string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/emerge-logo--no-shadows-(small).gif";
                BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                Bookmarks objBookmark = new Bookmarks();
                objBookmark.EmailContent = emailText;
                objBookmark.Subject = Subject;
                objBookmark.UserFirstName = firstName;
                objBookmark.UserLastName = lastname;
                objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                objBookmark.Date = date;
                MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Guid.Empty);
                string MailFrom = System.Configuration.ConfigurationManager.AppSettings["FromAddress"].ToString();
                BALGeneral ObjBALGeneral = new BALGeneral();
                string UserEmails = string.Empty;
                if (!string.IsNullOrEmpty(MailToAddress))
                {
                    UserEmails = ObjBALGeneral.GetUserOtherMailIds(MailToAddress);
                }
                // bool success = 
                SendEmail(MailToAddress, UserEmails, MailFrom, MessageBody, Subject);
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in SendBatchProcessNotification(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
        }

        public int UpdateOrderIsViewed(bool ISViewed, int OrderId)
        {
            int iResult = -1;
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    tblOrder ObjtblOrder = new tblOrder();
                    ObjtblOrder = DX.tblOrders.Where(d => d.pkOrderId == OrderId).FirstOrDefault();
                    if (ObjtblOrder != null)
                    {
                        if (ObjtblOrder.OrderStatus == 2)
                        {
                            ObjtblOrder.IsViewed = true;
                            DX.SubmitChanges();
                            iResult = 1;
                        }
                        else
                        {
                            ObjtblOrder.IsViewed = false;
                            DX.SubmitChanges();
                            iResult = 1;
                        }
                    }
                    else
                    {
                        tblOrders_Archieve ObjtblOrders_Archieve = new tblOrders_Archieve();
                        ObjtblOrders_Archieve = DX.tblOrders_Archieves.Where(d => d.pkOrderId == OrderId).FirstOrDefault();
                        if (ObjtblOrders_Archieve != null)
                        {
                            ObjtblOrders_Archieve.IsViewed = true;
                            DX.SubmitChanges();
                            iResult = 1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateOrderIsViewed(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            return iResult;
        }

        public EmergeReport AddOrdersApi(tblOrder ObjtblOrder, List<string> ObjCollProducts, tblOrderSearchedData ObjtblOrderSearchedData,
                    List<tblOrderSearchedEmploymentInfo> ObjCollOrderSearchedEmploymentInfo,
                    List<tblOrderSearchedEducationInfo> ObjCollOrderSearchedEducationInfo,
                    List<tblOrderSearchedPersonalInfo> ObjCollOrderSearchedPersonalInfo,
            List<tblOrderSearchedProfessionalInfo> ObjCollOrderSearchedProfessionalInfo,
                    List<tblOrderPersonalQtnInfo> ObjCollOrderPersonalQtnInfo,
                    List<tblOrderSearchedLicenseInfo> ObjCollOrderLicenseInfo,
                    List<tblOrderSearchedAliasPersonalInfo> ObjCollOrderSearchedAliasPersonalInfo,
                    List<tblOrderSearchedAliasLocationInfo> ObjCollOrderSearchedAliasLocationInfo,
                    List<tblOrderSearchedAliasWorkInfo> ObjCollOrderSearchedAliasWorkInfo,
                    Dictionary<string, Guid> ObjCompnyProductInfo,
                    List<tblOrderSearchedLiveRunnerInfo> ObjColltblOrderSearchedLiveRunnerInfo,
                    List<CLSConsentInfo> ObjCollCLSConsentInfo,
                    Guid ApplicationId, int IsNCR1NullDOB, bool IsLiveRunnerState, tblTieredPackage ObjtblTieredPackage, OrderInfo ObjOrderInfo, string CompanyAccountNo, string OrderCompanyName, string PermissiblePurpose)        /*, int AuthorizedPurpose*/
        {
            //int IsNullDOB = IsNCR1NullDOB;
            OrderCompanyName = OrderCompanyName.Replace("?", "").Replace(".", "").Replace("&", "").Replace("%", "").Replace("#", "").Replace("'", "");
            #region Variable Assignment
            int? OrderId = 0;
            DbTransaction DbTrans = null;
            if (ObjtblOrder.pkOrderId == 0)
            {
                using (EmergeDALDataContext ddcontaxt = new EmergeDALDataContext())
                {
                    ObjtblOrder.pkOrderId = ddcontaxt.tblOrders.Where(o => o.OrderNo == ObjtblOrder.OrderNo).Select(o => o.pkOrderId).FirstOrDefault();
                }
            }
            OrderId = ObjtblOrder.pkOrderId;
            // bool IsPackageReport = false;
            // string PackageInclude = string.Empty;
            //int fkPackageId = 0;
            bool IsTieredOrder = false;
            EmergeReport ObjEmergeReport = new EmergeReport();
            bool Is7YearFeaturesEnable = false;
            #endregion
            #region Return Variable
            BALProducts ObjBALProducts = new BALProducts();
            List<Proc_GetAllProductsWithVendorResult> ObjProc_GetAllProductsWithVendorResult = ObjBALProducts.GetAllProductsWithVendor();
            ObjEmergeReport.IsLiveRunnerState = IsLiveRunnerState;
            ObjEmergeReport.IsNullDOB = IsNCR1NullDOB;
            ObjEmergeReport.ObjCollCLSConsentInfo = ObjCollCLSConsentInfo;
            ObjEmergeReport.ObjCollOrderSearchedEducationInfo = ObjCollOrderSearchedEducationInfo;
            ObjEmergeReport.ObjCollOrderSearchedEmploymentInfo = ObjCollOrderSearchedEmploymentInfo;
            ObjEmergeReport.ObjCollOrderSearchedLicenseInfo = ObjCollOrderLicenseInfo;
            ObjEmergeReport.ObjCollOrderSearchedPersonalInfo = ObjCollOrderSearchedPersonalInfo;
            ObjEmergeReport.ObjCollOrderSearchedProfessionalInfo = ObjCollOrderSearchedProfessionalInfo;
            ObjEmergeReport.ObjCollProducts = ObjCollProducts;
            ObjEmergeReport.objColltblOrderPersonalQtnInfo = ObjCollOrderPersonalQtnInfo;
            ObjEmergeReport.ObjColltblOrderSearchedLiveRunnerInfo = ObjColltblOrderSearchedLiveRunnerInfo;
            ObjEmergeReport.ObjOrderInfo = ObjOrderInfo;
            ObjEmergeReport.ObjtblOrder = ObjtblOrder;
            ObjEmergeReport.ObjtblOrderSearchedData = ObjtblOrderSearchedData;
            ObjEmergeReport.ObjTieredPackage = ObjtblTieredPackage;
            ObjEmergeReport.CompanyAccountNo = CompanyAccountNo;
            ObjEmergeReport.OrderCompanyName = OrderCompanyName;
            ObjEmergeReport.PermissiblePurpose = PermissiblePurpose;
            int Order_Id = ObjtblOrder.pkOrderId;
            Is7YearFeaturesEnable = ObjOrderInfo.Is7YearFilterApplyForCriminal;

            #region select non multitiered

            if (ObjOrderInfo != null && ObjtblTieredPackage != null)
            {
                if (ObjOrderInfo.IsTiered)
                {
                    IsTieredOrder = true;
                    List<tblProductPerApplication> ObjtblPPA = new List<tblProductPerApplication>();
                    using (EmergeDALDataContext dx = new EmergeDALDataContext())
                    {
                        if (ObjtblTieredPackage.Host != null)//Set Host report to returned Object
                        {
                            ObjOrderInfo.HostProductCode = dx.tblProductPerApplications.Where(d => d.pkProductApplicationId == ObjtblTieredPackage.Host).Select(d => d.ProductCode).FirstOrDefault();
                        }
                        if (ObjtblTieredPackage.Tired2 != null)//Set Tiered 2 and removed from current running reports.
                        {
                            ObjtblPPA = dx.tblProductPerApplications.Where(d => d.pkProductApplicationId == ObjtblTieredPackage.Tired2).ToList<tblProductPerApplication>();
                            if (ObjtblPPA.Count() > 0)
                            {
                                string sRemovedElement = "";
                                foreach (string sReport in ObjCollProducts)
                                {
                                    if (sReport.Split('_').First().ToLower() == ObjtblPPA.First().ProductCode.ToLower())
                                    {
                                        ObjOrderInfo.Tiered2ProductCode = sReport;
                                        sRemovedElement = sReport;
                                    }
                                }
                                ObjCollProducts.Remove(sRemovedElement);
                            }
                            if (ObjOrderInfo.Tiered2ProductCode == null)
                            {
                                ObjOrderInfo.Tiered2ProductCode = dx.tblProductPerApplications.Where(d => d.pkProductApplicationId == ObjEmergeReport.ObjTieredPackage.Tired2).Select(d => d.ProductCode).FirstOrDefault() + "_" + ObjEmergeReport.ObjTieredPackage.fkPackageId;
                            }
                        }
                        if (ObjtblTieredPackage.Tired3 != null)//Set Tiered 3 and removed from current running reports.
                        {
                            ObjtblPPA = dx.tblProductPerApplications.Where(d => d.pkProductApplicationId == ObjtblTieredPackage.Tired3).ToList<tblProductPerApplication>();
                            if (ObjtblPPA.Count() > 0)
                            {
                                string sRemovedElement = "";
                                foreach (string sReport in ObjCollProducts)
                                {
                                    if (sReport.Split('_').First().ToLower() == ObjtblPPA.First().ProductCode.ToLower())
                                    {
                                        ObjOrderInfo.Tiered3ProductCode = sReport;
                                        sRemovedElement = sReport;
                                    }
                                }
                                ObjCollProducts.Remove(sRemovedElement);
                            }
                            if (ObjOrderInfo.Tiered3ProductCode == null)
                            {
                                ObjOrderInfo.Tiered3ProductCode = dx.tblProductPerApplications.Where(d => d.pkProductApplicationId == ObjEmergeReport.ObjTieredPackage.Tired3).Select(d => d.ProductCode).FirstOrDefault() + "_" + ObjEmergeReport.ObjTieredPackage.fkPackageId;
                            }
                        }
                        if (ObjtblTieredPackage.TiredOptional != null)//Set Optional Tiered and removed from current running reports.
                        {
                            ObjtblPPA = dx.tblProductPerApplications.Where(d => d.pkProductApplicationId == ObjtblTieredPackage.TiredOptional).ToList<tblProductPerApplication>();
                            if (ObjtblPPA.Count() > 0)
                            {
                                string sRemovedElement = "";
                                foreach (string sReport in ObjCollProducts)
                                {
                                    if (sReport.Split('_').First().ToLower() == ObjtblPPA.First().ProductCode.ToLower())
                                    {
                                        ObjOrderInfo.OptionalProductCode = sReport;
                                        sRemovedElement = sReport;
                                    }
                                }
                                ObjCollProducts.Remove(sRemovedElement);
                            }
                            if (ObjOrderInfo.OptionalProductCode == null)
                            {
                                ObjOrderInfo.OptionalProductCode = dx.tblProductPerApplications.Where(d => d.pkProductApplicationId == ObjEmergeReport.ObjTieredPackage.TiredOptional).Select(d => d.ProductCode).FirstOrDefault() + "_" + ObjEmergeReport.ObjTieredPackage.fkPackageId;
                            }
                        }

                    }
                }
                if (ObjEmergeReport.ObjOrderInfo.NormalReport == null)
                {
                    ObjEmergeReport.ObjOrderInfo.NormalReport = new List<string>();
                }
                foreach (string ReportCode in ObjCollProducts)
                {
                    ObjEmergeReport.ObjOrderInfo.NormalReport.Add(ReportCode);
                }
            }
            #endregion
            #endregion
            string PDFAttachFilePath = string.Empty;
            Dictionary<int, string> objdic = new Dictionary<int, string>();
            List<string> reportCodes = new List<string>();
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    bool IsInstant = false;
                    bool IsIfAnyConsentEmailOrFax = false;

                    for (int i = 0; i < ObjCollProducts.Count; i++)
                    {
                        string PName = string.Empty;
                        if (ObjCollProducts.ElementAt(i).Contains("_"))/*This condition is used to split report code & package code*/
                        {
                            string[] Pcode = ObjCollProducts.ElementAt(i).Split('_');
                            PName = Pcode[0].ToString();
                            IsInstant = IsInstantProduct(PName);
                            reportCodes.Add(Pcode[0].ToString());
                        }
                        string[] arr = ObjCollProducts.ElementAt(i).Split('_');
                        string ProductCode = arr[0];
                        int OrderDetailId = Convert.ToInt32(arr[2]);
                        BALCompanyType ObjBALCompanyType = new BALCompanyType();
                        var query = ObjBALCompanyType.GetRequestData(OrderDetailId);
                        string CocatRequest1 = ProductCode + "^" + query.Item1 + "^" + query.Item2 + "^" + IsInstant;
                        objdic.Add(OrderDetailId, CocatRequest1);

                        bool IsConsentDB = false;
                        CLSConsentInfo ObjCLSConsentInfo = new CLSConsentInfo();
                        if (ObjCollCLSConsentInfo != null)
                        {
                            if (ObjCollCLSConsentInfo.Count > 0)
                            {
                                if (ObjCollCLSConsentInfo.Any(d => d.IsConsentEmailOrFax == true))
                                {
                                    IsIfAnyConsentEmailOrFax = true;
                                }
                                if (ObjCollCLSConsentInfo.Any(d => d.ConsentReportType == PName))
                                {
                                    for (int iRow = 0; iRow < ObjCollCLSConsentInfo.Count; iRow++)
                                    {

                                        if (ObjCollCLSConsentInfo.ElementAt(iRow).ConsentReportType == PName && IsConsentDB == false)
                                        {
                                            IsConsentDB = true;
                                            ObjCLSConsentInfo.ConsentFileName = ObjCollCLSConsentInfo.ElementAt(iRow).ConsentFileName;
                                            ObjCLSConsentInfo.IsConsentEmailOrFax = ObjCollCLSConsentInfo.ElementAt(iRow).IsConsentEmailOrFax;
                                            ObjCLSConsentInfo.IsConsentRequired = ObjCollCLSConsentInfo.ElementAt(iRow).IsConsentRequired;
                                            ObjCLSConsentInfo.ConsentState = ObjCollCLSConsentInfo.ElementAt(iRow).ConsentState;
                                            PDFAttachFilePath = ObjCollCLSConsentInfo.ElementAt(iRow).PDFAttachFilePath;
                                        }
                                    }
                                }
                                else if (IsConsentDB == false)
                                {
                                    IsConsentDB = true;
                                    ObjCLSConsentInfo.ConsentFileName = string.Empty;
                                    ObjCLSConsentInfo.IsConsentEmailOrFax = false;
                                    ObjCLSConsentInfo.IsConsentRequired = false;
                                    ObjCLSConsentInfo.ConsentState = string.Empty;
                                }
                            }
                            else
                            {
                                ObjCLSConsentInfo.ConsentFileName = string.Empty;
                                ObjCLSConsentInfo.IsConsentEmailOrFax = false;
                                ObjCLSConsentInfo.IsConsentRequired = false;
                                ObjCLSConsentInfo.ConsentState = string.Empty;
                            }
                        }
                    }
                    #region Vendor Call
                    //string strmail = "";
                    string vendor_Res = "";
                    string Vendor_Req = "";
                    bool IsMVR = false;
                    bool IsSCR = false;
                    // for INT 173  START 04 MAY 2015 
                    bool IsEEI = false;
                    bool IsECR = false;
                    foreach (KeyValuePair<int, string> data in objdic)
                    {
                        string[] concatRequest = data.Value.Split('^');
                        string product_Code = concatRequest[0];
                        string vendor_Req = concatRequest[1];
                        int pkOrderRequestId = Convert.ToInt32(concatRequest[2]);
                        bool IsInstantReport = Convert.ToBoolean(concatRequest[3]);
                        if (ObjtblOrderSearchedData.SearchedStateId == 32 && product_Code.ToLower() == "scr")//SCR for out side Usa state in ticket #132
                        {
                            //OUT SIDE USA NOT SEND REQUEST TO VENDOR
                        }
                        else
                            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                            {
                                bool flagRCX = Dx.tblOrderDetails.Where(d => d.pkOrderDetailId == data.Key).Select(d => d.IsLiveRunner).FirstOrDefault();
                                if (product_Code == "SCR" && flagRCX == true)
                                {
                                    product_Code = "RCX";
                                }
                            }
                        vendor_Res = GetXMLResponse(product_Code, ObjProc_GetAllProductsWithVendorResult, vendor_Req, OrderId, data.Key, IsInstantReport);
                        // Here is Set Host report Response for tiered other reports. 
                        #region Host Tiered Response for next report.

                        if (product_Code == ObjEmergeReport.ObjOrderInfo.HostProductCode)//product_Code == "PS"&&
                        {
                            ObjEmergeReport.ObjOrderInfo.Tiered2Response = vendor_Res;

                        }
                        #endregion
                        if (vendor_Res.Contains("<XML_INTERFACE>ZZZ</XML_INTERFACE>"))
                        {
                            string[] Vendor_Response_Parts = vendor_Res.Split('#');
                            vendor_Res = Vendor_Response_Parts[1].ToString();
                            Vendor_Req = Vendor_Response_Parts[0].ToString();
                            UpdateRequest(pkOrderRequestId, Vendor_Req);
                        }
                        using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                        {
                            Dx.AddOrderResponse(data.Key, vendor_Res);
                        }
                        if (ObjtblOrderSearchedData.SearchedDLStateId == 47 && product_Code.ToLower() == "mvr")// 47 Pennsylvania	PA
                        {
                            //IsMVR = true; fo INT 263 OFF MAIL FOR ADMIN SCETION on 17 sept 2015
                            IsMVR = false;

                            CheckResponseandUpdateStatus(product_Code, OrderId, vendor_Res, data.Key, IsInstantReport, CompanyAccountNo, OrderCompanyName, PermissiblePurpose, Is7YearFeaturesEnable);
                            UpdateReportStatus(OrderId, data.Key, Convert.ToByte("1"));
                        }
                        else if (ObjtblOrderSearchedData.SearchedStateId == 32 && product_Code.ToLower() == "scr")// 32 OutSide USA	OT
                        {
                            IsSCR = true;
                            CheckResponseandUpdateStatus(product_Code, OrderId, vendor_Res, data.Key, IsInstantReport, CompanyAccountNo, OrderCompanyName, PermissiblePurpose, Is7YearFeaturesEnable);
                            UpdateReportStatus(OrderId, data.Key, Convert.ToByte("1"));
                        }
                        // FOR INT 173 04 MAY 2015 WORK AS MVR FOR EEI AND ECR REPORT START 
                        else if (product_Code.ToLower() == "eei" || product_Code.ToLower() == "ecr")// CHECK WHICH REPORT IS EXIST FOR INT 147
                        {
                            if (product_Code.ToLower() == "eei")
                            {
                                IsEEI = true;
                            }
                            else
                            {
                                IsECR = true;
                            }
                            CheckResponseandUpdateStatus(product_Code, OrderId, vendor_Res, data.Key, IsInstantReport, CompanyAccountNo, OrderCompanyName, PermissiblePurpose, Is7YearFeaturesEnable);
                            UpdateReportStatus(OrderId, data.Key, Convert.ToByte("1"));
                        }
                        else// FOR INT 173 04 MAY 2015 WORK AS MVR FOR EEI AND ECR REPORT END 
                        {
                            CheckResponseandUpdateStatus(product_Code, OrderId, vendor_Res, data.Key, IsInstantReport, CompanyAccountNo, OrderCompanyName, PermissiblePurpose, Is7YearFeaturesEnable);
                        }

                        vendor_Res = "";
                        Vendor_Req = "";
                    }
                    using (EmergeDALDataContext DX = new EmergeDALDataContext())
                    {
                        var query = from c_output in DX.tblOrders
                                    where c_output.pkOrderId == Order_Id
                                    select c_output;
                        if (query.Count() > 0)
                        {
                            query.First().IsSubmitted = true;
                            DX.SubmitChanges();

                        }
                    }
                    #endregion

                    #region Sending  E-Mail's
                    // code to Send an email For Testing Purpose .
                    BALCompany ObjCom = new BALCompany();
                    BALGeneral ObjBALGeneral = new BALGeneral();
                    int PkTemplateId = 29;
                    string emailText = "";
                    string Subject = "";
                    string ApplicantName = "";
                    string ReportCodes = "";
                    string UserEmail = "";
                    string MailToAddress = "";
                    int? fkCompanyUserId = ObjtblOrder.fkCompanyUserId;
                    var INFO = ObjCom.GetUserAndCompanyDetails(fkCompanyUserId).ToList();

                    var emailContent = ObjBALGeneral.GetEmailTemplate(ApplicationId, PkTemplateId);
                    emailText = emailContent.TemplateContent.ToString();
                    Subject = emailContent.TemplateSubject;

                    #region For New Style SystemTemplate
                    string Bookmarkfile = string.Empty;
                    string bookmarkfilepath = string.Empty;
                    BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                    Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                    Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                    Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                    emailText = Bookmarkfile;
                    #endregion

                    if (reportCodes.Count > 1)
                    {
                        for (int i = 0; i < reportCodes.Count; i++)
                        {
                            ReportCodes += reportCodes.ElementAt(i).ToString();

                            if (i != reportCodes.Count - 1)
                            {
                                ReportCodes += ", ";
                            }
                        }
                    }
                    else
                    {
                        ReportCodes = reportCodes.ElementAt(0).ToString();
                    }
                    ApplicantName = ObjtblOrderSearchedData.SearchedFirstName + " " + ObjtblOrderSearchedData.SearchedLastName;
                    if (INFO.Count > 0)
                    {
                        UserEmail = INFO.ElementAt(0).Email;
                        MailToAddress = INFO.ElementAt(0).ReportEmailAddress;
                        if (INFO.ElementAt(0).IsReportEmailActivated_Location == true)
                        {
                            if (INFO.ElementAt(0).LocationEmail != "")
                            {
                                /* This condition is set for sending mail to COMPANY LOCATION */

                                //For email status//
                                BALGeneral objbalgenral = new BALGeneral();
                                int pktemplateid = PkTemplateId;
                                Guid user_ID = Guid.Empty;
                                int? fkcompanyuserid = fkCompanyUserId;
                                bool email_status = objbalgenral.UserEmailStatusCheck(user_ID, pktemplateid, fkcompanyuserid);
                                if (email_status == true)
                                {
                                    //Do nothing.
                                }
                            }
                        }
                    }
                    if (IsMVR)
                    {
                        var emailContent_Mvr = ObjBALGeneral.GetEmailTemplate(ApplicationId, 36);
                        emailText = emailContent_Mvr.TemplateContent.ToString();
                        #region For New Style SystemTemplate
                        string Bookmarkfile1 = string.Empty;

                        string bookmarkfilepath1 = string.Empty;
                        Bookmarkfile1 = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath1);
                        Bookmarkfile1 = Bookmarkfile1.Replace("%EmailHeader%", emailContent_Mvr.TemplateSubject);
                        Bookmarkfile1 = Bookmarkfile1.Replace("%EmailBody%", emailContent_Mvr.TemplateContent);
                        emailText = Bookmarkfile1;
                        #endregion


                        Subject = emailContent_Mvr.TemplateSubject.Replace("%OrderNumber%", ObjtblOrder.OrderNo).Replace("%ApplicantName%", ApplicantName);
                        SendEmailToAdminForMVR(ObjtblOrder.OrderNo, UserEmail, ApplicantName, ObjtblOrderSearchedData.SearchedDob, ObjtblOrderSearchedData.SearchedSsn, ObjtblOrderSearchedData.SearchedDriverLicense, "MVR", emailText, Subject);
                    }
                    if (IsIfAnyConsentEmailOrFax == true)
                    {
                        var emailContent_CONSENT = ObjBALGeneral.GetEmailTemplate(ApplicationId, 39);           // PKTEMPLATEID = 39 for Consent Form
                        emailText = emailContent_CONSENT.TemplateContent.ToString();

                        #region For New Style SystemTemplate
                        string Bookmarkfile2 = string.Empty;
                        string bookmarkfilepath1 = string.Empty;

                        Bookmarkfile2 = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath1);
                        Bookmarkfile2 = Bookmarkfile2.Replace("%EmailHeader%", emailContent_CONSENT.TemplateSubject);
                        Bookmarkfile2 = Bookmarkfile2.Replace("%EmailBody%", emailContent_CONSENT.TemplateContent);
                        emailText = Bookmarkfile2;
                        #endregion

                        Subject = emailContent_CONSENT.TemplateSubject.Replace("%ApplicantName%", ApplicantName);
                    }
                    #endregion


                }
                catch (Exception ex)
                {
                    #region Transaction Rollback

                    OrderId = 0;
                    if (DbTrans != null)
                    {
                        DbTrans.Rollback(); /* Rollback Transaction */
                    }

                    #endregion

                    //Added log for exception handling.
                    VendorServices.GeneralService.WriteLog("Some Error Occured in AddOrdersApi(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                }
                finally
                {
                    #region Connection and Transaction Close

                    ObjDALDataContext.Transaction = null;
                    if (ObjDALDataContext.Connection.State == ConnectionState.Open)
                    {
                        ObjDALDataContext.Connection.Close(); /* Close Connection */
                    }

                    #endregion
                }

                #region Return OrderId
                ObjEmergeReport.ObjOrderInfo.OrderId = ObjtblOrder.pkOrderId;
                return ObjEmergeReport; /* Here we return successfully inserted order id */

                #endregion
            }
        }
        public void setIsSubmitted(int Order_Id)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                var query = from c_output in DX.tblOrders
                            where c_output.pkOrderId == Order_Id
                            select c_output;
                if (query.Count() > 0)
                {
                    query.First().IsSubmitted = true;
                    DX.SubmitChanges();
                }
            }
        }
        /// <summary>
        /// To check Order's Company.
        /// </summary>
        /// <param name="OrderId"></param>
        /// <param name="CompanyId"></param>
        public bool CheckOrderCompanyByOrderId(int OrderId, int CompanyId)
        {
            //INT67
            bool flag = false;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                var checkordercompanyByOrderId = ObjDALDataContext.tblOrderDetails.Where(ods => ods.fkOrderId == OrderId && ods.fkCompanyId == CompanyId).Select(ods => ods.fkCompanyId).Distinct().FirstOrDefault();
                if (checkordercompanyByOrderId == null)
                {
                    var checkorderArchievescompanyByOrderId = ObjDALDataContext.tblOrderDetails_Archieves.Where(ods => ods.fkOrderId == OrderId && ods.fkCompanyId == CompanyId).Select(ods => ods.fkCompanyId).Distinct().FirstOrDefault();
                    if (checkorderArchievescompanyByOrderId != null)
                    {
                        flag = true;
                    }
                }
                if (checkordercompanyByOrderId != null)
                {
                    flag = true;
                }
            }
            return flag;
        }
        /// <summary>
        /// To get the selected Order hit status
        /// </summary>
        /// <param name="pkOrderId"></param>
        /// <returns></returns>
        public int GetOrderHitStatusManually(int pkOrderId)
        {
            int orderHitStatus = 0;
            bool flag = false;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                var values = ObjDALDataContext.tblOrders.Where(x => x.pkOrderId == pkOrderId).FirstOrDefault();
                string[] reportforHit = new string[] { "NCR1", "NCR+", "RCX", "CCR2", "NCR2", "SOR", "OFAC", "SCR", "CCR1", "NCR4", "SA" };
                string[] str = values.ReportsWithStatus.Split(',');
                //string sval = string.Empty;
                foreach (string s in str)
                {
                    string s1 = (s.Split('_'))[0];
                    if (reportforHit.Contains(s1))
                    {
                        flag = true;
                        break;
                    }
                }
                if (flag)
                {
                    orderHitStatus = values.HitStatus;
                }
            }
            return orderHitStatus;
        }
        /// <summary>
        /// To get the FRS vendor service credential. INT-15
        /// </summary>
        /// <returns></returns>
        public tblVendor getFRSVendorCredential()
        {
            tblVendor vendor = new tblVendor();
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                vendor = ObjDALDataContext.tblVendors.Where(v => v.pkVendorId == 7).FirstOrDefault();
            }
            return vendor;
        }
        /// <summary>
        /// To get the order details and submitted by order user id by selected order id.
        /// </summary>
        public Dictionary<string, int> getOrderDetialsWithUserDetailsByOrderId(int OrderId)
        {
            Dictionary<string, int> dic = new Dictionary<string, int>();
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                //To get order details by selected order id.
                var orderValues = (from or in (ObjDALDataContext.tblOrders.Where(o => o.pkOrderId == OrderId))
                                   join ord in ObjDALDataContext.tblOrderDetails
                                   on or.pkOrderId equals ord.fkOrderId
                                   select new { or.pkOrderId, or.fkCompanyUserId, or.fkLocationId, ord.fkCompanyId }).FirstOrDefault();
                if (orderValues != null)
                {
                    dic["fkCompanyId"] = Convert.ToInt32(orderValues.fkCompanyId);
                    dic["fkLocationId"] = Convert.ToInt32(orderValues.fkLocationId);
                    dic["fkCompanyUserId"] = Convert.ToInt32(orderValues.fkCompanyUserId);
                }
            }
            return dic;
        }
        /// <summary>
        /// To get the Order number based on Order Id.
        /// </summary>
        public string GetOrderNoformail(int? OrderID)
        {
            string orderNo = string.Empty;
            using (EmergeDALDataContext dal = new EmergeDALDataContext())
            {
                var order = dal.tblOrders.Where(o => o.pkOrderId == OrderID).Select(o => o.OrderNo).FirstOrDefault();
                if (order != null)
                {
                    orderNo = Convert.ToString(order);
                }
                else
                {
                    order = dal.tblOrders_Archieves.Where(o => o.pkOrderId == OrderID).Select(o => o.OrderNo).FirstOrDefault();
                    if (order != null)
                    {
                        orderNo = Convert.ToString(order);
                    }
                }
            }
            return orderNo;
        }



        #region--------------108 Categories NCR1 section State and Disposition Date wise
        /// <summary>
        /// To add new tags based on the Court Name and disposition Date.
        /// </summary>        
        public string AddNewTagsInNCRResponses(string xmlResponse)
        {
            List<string> strState = new List<string>();
            string strUpdatedXML = string.Empty;
            //string strXML = string.Empty;
            string FinalResponse = string.Empty;
            bool flag = false;
            XmlDocument doc = new XmlDocument();
            using (EmergeDALDataContext context = new EmergeDALDataContext())
            {
                strState = context.tblStates.Select(s => s.StateName.ToUpper()).ToList();
            }
            doc.InnerXml = xmlResponse;
            XDocument xDoc = new XDocument();
            string XML_ResponseNCR1 = GetRequiredXmlForNCR(doc.OuterXml);
            xDoc = XDocument.Parse(XML_ResponseNCR1);
            var lstScreenings = xDoc.Descendants("Screenings").ToList();
            var lstCriminalReports = lstScreenings.Descendants("CriminalReport").FirstOrDefault();
            if (lstScreenings.Count > 0 && lstCriminalReports != null)
            {
                var orderedtabs = lstCriminalReports.Descendants("CriminalCase").ToList();
                if (orderedtabs != null)
                {
                    // int RemainingNUllDOB = 0;
                    List<XElement> lst = new List<XElement>();
                    int i = 1;
                    foreach (XElement item in orderedtabs)
                    {
                        var ch = item.Elements("CourtName").FirstOrDefault().Value.Replace("-", "");
                        var itemExists = item.Elements("CourtState").FirstOrDefault();
                        var itemExistMatches = item.Elements("PossibleMatch").FirstOrDefault();
                        if (item.Attribute("CRID") == null)
                        {
                            item.Add(new XAttribute("CRID", i));
                        }
                        if (itemExistMatches == null)
                        {
                            var dob = item.Descendants("DateOfBirth");
                            XElement IsAdded = item.Descendants("DemographicDetail").Where(x => x.HasAttributes && x.Attribute("IsAdded").Value == "true").FirstOrDefault();
                            if (dob.Count() == 0 && IsAdded == null)
                            {
                                //RemainingNUllDOB++;
                                item.Add(new XElement("PossibleMatch", 0));
                            }
                            else
                            {
                                item.Add(new XElement("PossibleMatch", 1));

                            }
                        }
                        if (itemExists == null)
                        {
                            foreach (var state in strState)
                            {

                                if (ch.Contains(state))
                                {
                                    item.Add(new XElement("CourtState", state));
                                    flag = true;
                                    break;
                                }

                            }

                            if (flag)
                            {
                                lst.Add(item);
                                flag = false;
                            }
                            else
                            {
                                item.Add(new XElement("CourtState", "ZZZZ"));
                                lst.Add(item);
                            }
                        }
                        else
                        {
                            lst.Add(item);
                            flag = false;

                        }
                        i++;
                    }

                    foreach (XElement item in orderedtabs)
                    {
                        var itemExists = item.Elements("DispDate").FirstOrDefault();
                        if (itemExists == null)
                        {
                            #region---Disposition date
                            var d1 = item.Descendants("DispositionDate").FirstOrDefault();
                            if (d1 != null)
                            {
                                item.Add(new XElement("DispDate", (d1.Value.Split('T'))[0]));
                            }
                            else
                            {
                                d1 = item.Descendants("ChargeDate").FirstOrDefault();
                                if (d1 != null)
                                {
                                    item.Add(new XElement("DispDate", (d1.Value.Split('T'))[0]));
                                }
                                else
                                {
                                    d1 = item.Descendants("OffenseDate").FirstOrDefault();
                                    if (d1 != null)
                                    {
                                        item.Add(new XElement("DispDate", (d1.Value.Split('T'))[0]));
                                    }
                                    else
                                    {
                                        item.Add(new XElement("DispDate", "1900-01-01"));//If disposition date is missing.
                                    }
                                }
                            }
                            #endregion
                        }
                    }

                    xDoc.Descendants("Screenings").Descendants("CriminalReport").Descendants("CriminalCase").Remove();
                    foreach (var item in lst)
                    {
                        xDoc.Descendants("Screenings").Descendants("CriminalReport").FirstOrDefault().Add(item);
                    }

                    XDocument UpdatedXML = sortXMLForNCR1(xDoc.ToString());
                    strUpdatedXML = Convert.ToString(UpdatedXML);
                    string strMain = "<?xml version='1.0' encoding='ISO-8859-1'?><BackgroundReports xmlns='http://ns.hr-xml.org/2004-08-02' account='' password='' userId=''>";
                    strUpdatedXML = strMain + strUpdatedXML;
                    int TrimedUpTo = XML_ResponseNCR1.IndexOf("<BackgroundReportPackage>");
                    StringBuilder CompleteResponse = new StringBuilder();
                    string XMLInitials = XML_ResponseNCR1.Substring(0, TrimedUpTo);
                    CompleteResponse.Append(XMLInitials);
                    CompleteResponse.Append(strUpdatedXML);
                    CompleteResponse.Append("</BackgroundReports>");
                    FinalResponse = Convert.ToString(CompleteResponse);
                }
            }
            return FinalResponse;
        }

        public string GetRequiredXmlForNCR(string XmlResponse)
        {
            string XmlResponse_Local = XmlResponse;
            XmlResponse_Local = XmlResponse_Local.Replace("</BackgroundReports>", "");
            int xmlstartindex = XmlResponse_Local.IndexOf("<BackgroundReportPackage>");
            XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
            return XmlResponse_Local;
        }
        /// <summary>
        /// To sort original XML response based on the CourtName and Dispostion Date.
        /// </summary>        
        public XDocument sortXMLForNCR1(string XmlResponse)
        {
            var xDoc = XDocument.Parse(XmlResponse);
            List<XElement> lst = new List<XElement>();
            List<string> states = new List<string>();

            var collection = xDoc.Descendants("CriminalCase");
            foreach (var item in collection)
            {
                //Get only distinct CourtName.
                if (item != null)
                {
                    if (item.Elements("CourtState").FirstOrDefault() != null)
                    {
                        if (!states.Contains(item.Elements("CourtState").FirstOrDefault().Value))
                            states.Add(item.Elements("CourtState").FirstOrDefault().Value);
                    }
                }
            }
            states.Sort();//Sort distinct Court Name.

            List<XElement> lstExactMatches = new List<XElement>();
            List<XElement> lstPossibleMatches = new List<XElement>();


            //For Exact Matches
            var sortExactMatch = xDoc.Descendants("CriminalCase").Where(x => x.Elements("PossibleMatch").FirstOrDefault().Value == "0").ToList();
            if (sortExactMatch.Count() > 0)
            {
                lstExactMatches = sortExactMatch.OrderBy(x => x.Elements("CourtState").FirstOrDefault().Value).ToList();
            }

            //For Possible Matches.
            var sortPossibleMatch = xDoc.Descendants("CriminalCase").Where(x => x.Elements("PossibleMatch").FirstOrDefault().Value == "1").ToList();
            if (sortPossibleMatch.Count() > 0)
            {
                lstPossibleMatches = sortPossibleMatch.OrderBy(x => x.Elements("CourtState").FirstOrDefault().Value).ToList();
            }

            //Exact Matches.
            foreach (var item in states)
            {
                //To sort Criminal Case based on disposition Date.
                var kk = lstExactMatches.Where(x => x.Elements("CourtState").FirstOrDefault().Value == item)
                       .OrderByDescending(x => x.Elements("DispDate").FirstOrDefault().Value)
                       .ToList();
                foreach (var item1 in kk)
                {
                    lst.Add(item1);
                }
            }

            //Possible.
            foreach (var item in states)
            {
                //To sort Criminal Case based on disposition Date.
                var kk = lstPossibleMatches.Where(x => x.Elements("CourtState").FirstOrDefault().Value == item)
                       .OrderByDescending(x => x.Elements("DispDate").FirstOrDefault().Value)
                       .ToList();
                foreach (var item1 in kk)
                {
                    lst.Add(item1);
                }
            }




            var data = xDoc.Descendants("CriminalCase");
            if (lst.Count() > 0)
            {
                data.Remove();//Remove all Criminal case. 
                var document1 = xDoc.Descendants("CriminalReport").FirstOrDefault();
                foreach (var item2 in lst)
                {
                    document1.Add(new XElement(item2));//Add sorted criminal cases.
                }
            }
            return xDoc;
        }

        /// <summary>
        /// To check if order already sorted.
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        public int CheckOrderInNCRXML(string orderNo)
        {
            int retVal = 0;
            using (EmergeDALDataContext dal = new EmergeDALDataContext())
            {
                var ch = dal.tblNCRXMLSorts.Where(x => x.OrderNo == orderNo).FirstOrDefault();
                if (ch != null)
                {
                    retVal = 1;
                }
            }
            return retVal;
        }

        /// <summary>
        /// To Insert order number if any order has been sorted.
        /// </summary>        
        public void InsertOrderInNCRXML(string orderNo)
        {
            using (EmergeDALDataContext dal = new EmergeDALDataContext())
            {
                tblNCRXMLSort tbl = new tblNCRXMLSort();
                tbl.OrderNo = orderNo;
                dal.tblNCRXMLSorts.InsertOnSubmit(tbl);
                dal.SubmitChanges();
            }
        }
        #endregion

        /// <summary>
        /// Verify the package if package name comtains FACIS in name then emerge will send a 
        /// notification to support@intelifi.com
        /// </summary>        
        public void verifyPackageFACISAndSendNotification(int pkOrderId)
        {
            using (EmergeDALDataContext context = new EmergeDALDataContext())
            {
                //Get package id based on order.
                var isPackage = context.tblOrderDetails.Where(x => x.fkOrderId == pkOrderId).Select(x => x.fkPackageId).FirstOrDefault();
                if (isPackage != null && isPackage > 0)
                {
                    //To check if selected package name FACIS.
                    var facisExists = context.tblProductPackages.Where(x => x.pkPackageId == isPackage && x.PackageName.Contains("FACIS")).FirstOrDefault();
                    if (facisExists != null)
                    {
                        //To get order number.
                        string orderNo = context.tblOrders.Where(x => x.pkOrderId == pkOrderId).Select(x => x.OrderNo).FirstOrDefault();

                        //string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/emerge-logo--no-shadows-(small).gif";
                        //BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();

                        string MailToAddress = "support@intelifi.com";

                        string MessageBody = ConfigurationManager.AppSettings["FacisTieredBody"].ToString() + orderNo;


                        string MailFrom = ConfigurationManager.AppSettings["ReportReviewFromAddress"].ToString();
                        string Subject = ConfigurationManager.AppSettings["FacisTieredSubject"].ToString();
                        //string success = 
                        BALEmergeReview.SendMail(MailToAddress, string.Empty, string.Empty, MailFrom, MessageBody, Subject);
                    }
                }
            }
        }

        //added on 15 july 2016 for  INT-250
        public List<Proc_SelectNotice613PDFStatusResult> SelectNotice613PDFStatus(string from, string to)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                return Dx.SelectNotice613PDFStatus(from, to).ToList();
            }
        }
        //added on 15 july 2016 for  INT-250
        public List<Proc_SelectNotice613EmailStatusResult> SelectNotice613EmailStatus(string from, string to)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                return Dx.SelectNotice613EmailStatus(from, to).ToList();
            }
        }


        //added on 15 july 2016 for  INT-250


    }
    public class MarkAs
    {
        public int OrderId { get; set; }
        public bool IsRead { get; set; }

        public int FolderId { get; set; }

    }
    public class OrderInfo
    {
        public int? OrderId { get; set; }
        public bool IsTiered { get; set; }
        public bool IsAutomatic { get; set; }
        public bool CheckTieredForApi { get; set; }
        public string HostProductCode { get; set; }
        /// <summary>
        /// Set Html for Tiered 2 report.
        /// </summary>
        public string Tiered2ProductCode { get; set; }
        public string Tiered3ProductCode { get; set; }
        /// <summary>
        /// Set Html for Tiered 2 Optional report.
        /// </summary>
        public string OptionalProductCode { get; set; }
        public string Tiered2Response { get; set; }
        public string Tiered3Response { get; set; }
        public string OptionalResponse { get; set; }
        public string TieredTopInfo { get; set; }
        public List<string> NormalReport { get; set; }
        /// <summary>
        /// This is tiered report variable, that we use in json after retuning object.
        /// <para>County State</para><value>1</value>
        /// <para>State</para><value>2</value>
        /// <para>Any other variable</para><value>So on</value>
        /// </summary>
        public string Variable { get; set; }
        // This is for My preferences check if user needs to return back to new report page after placing an order (if it is checked)
        public bool IsDataEntry_Enable { get; set; }
        public bool Is7YearFilterApplyForCriminal { get; set; }
    }

    public class CLSConsentInfo
    {
        public int pkConsentId { get; set; }
        public string ConsentFileName { get; set; }
        public bool IsConsentEmailOrFax { get; set; }
        public bool IsConsentRequired { get; set; }
        public string ConsentReportType { get; set; }
        public string ConsentState { get; set; }
        public string PDFAttachFilePath { get; set; }
    }

    public class EmergeReport
    {
        public tblOrder ObjtblOrder { get; set; }
        public List<string> ObjCollProducts { get; set; }
        public tblOrderSearchedData ObjtblOrderSearchedData { get; set; }
        public List<tblOrderSearchedEmploymentInfo> ObjCollOrderSearchedEmploymentInfo { get; set; }
        public List<CLSConsentInfo> ObjCollCLSConsentInfo { get; set; }
        public List<tblOrderSearchedEducationInfo> ObjCollOrderSearchedEducationInfo { get; set; }
        public List<tblOrderSearchedPersonalInfo> ObjCollOrderSearchedPersonalInfo { get; set; }
        public List<tblOrderPersonalQtnInfo> objColltblOrderPersonalQtnInfo { get; set; }
        public List<tblOrderSearchedLicenseInfo> ObjCollOrderSearchedLicenseInfo { get; set; }
        public List<tblOrderSearchedLiveRunnerInfo> ObjColltblOrderSearchedLiveRunnerInfo { get; set; }
        public List<tblOrderSearchedDataJurdictionInfo> ObjJurisdictionList { get; set; }
        public List<tblOrderSearchedProfessionalInfo> ObjCollOrderSearchedProfessionalInfo { get; set; }//INT336
        public int IsNullDOB { get; set; }
        public bool IsLiveRunnerState { get; set; }
        public int AuthorizedPurpose { get; set; }
        public tblTieredPackage ObjTieredPackage { get; set; }
        public List<tblTieredPackage> objTieredPackageList { get; set; }
        public OrderInfo ObjOrderInfo { get; set; }
        public string PermissiblePurpose { get; set; }
        public string OrderCompanyName { get; set; }
        public string CompanyAccountNo { get; set; }
        public bool IsEnable7YearFilter { get; set; }
    }

    public class EmergeReports
    {
        public string ReferenceCode { get; set; }
        public List<string> PackageInfo { get; set; }
        public List<string> ReportInfo { get; set; }
        public GeneralInformation GeneralInfo { get; set; }
        public List<County> CountyInfo { get; set; }
        public Automotive AutomotiveInfo { get; set; }
        public List<Employment> EmploymentInfo { get; set; }
        public List<Education> EducationInfo { get; set; }
        public List<Personal> PersonalInfo { get; set; }
        public List<License> LicenseInfo { get; set; }
        public Business BusinessInfo { get; set; }
        public Tracking TrackingInfo { get; set; }
        public int IsNullDOB { get; set; }
        public bool IsLiveRunnerState { get; set; }
        public OrderInfo ObjOrderInfo { get; set; }
        public string PermissiblePurpose { get; set; }
        public string OrderCompanyName { get; set; }
        public string CompanyAccountNo { get; set; }
    }

    #region API CLASSES
    public class GeneralInformation
    {
        public Guid pkOrderSearchedId { get; set; }
        public int fkOrderId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MI { get; set; }
        public string Suffix { get; set; }
        public string Social { get; set; }
        public string DOB { get; set; }
        public string Phone { get; set; }
        public string StreetAddress { get; set; }
        public string Direction { get; set; }
        public string StreetName { get; set; }
        public string StreetType { get; set; }
        public string Apt { get; set; }
        public string City { get; set; }
        public int? StateId { get; set; }
        public string Zip { get; set; }
        public int? JurisdictionId { get; set; }
        public string search_county { get; set; }
        public string search_state { get; set; }
    }

    public class County
    {
        public string CIStateId { get; set; }
        public int CountyId { get; set; }
    }
    public class Automotive
    {
        public int? DLStateId { get; set; }
        public string DriverLicense { get; set; }
        public string VehicleVin { get; set; }
        public string Sex { get; set; }
    }
    public class Employment
    {
        public string CompanyName { get; set; }
        public string CompanyState { get; set; }
        public string CompanyCity { get; set; }
        public string CompanyPhone { get; set; }
        public string Alias { get; set; }
    }
    public class Education
    {
        public string InstitutionName { get; set; }
        public string InstitutionState { get; set; }
        public string InstitutionCity { get; set; }
        public string InstitutionPhone { get; set; }
        public string Alias { get; set; }
    }
    public class Personal
    {
        public string ReferenceName { get; set; }
        public string ReferencePhone { get; set; }
    }
    public class License
    {
        public string LicenseType { get; set; }
        public string LicenseState { get; set; }
        public string LicenseNo { get; set; }
        public string LicenseDate { get; set; }
        public string Alias { get; set; }
    }
    public class Business
    {
        public string BusinessName { get; set; }
        public string BusinessCity { get; set; }
        public int? BusinessStateId { get; set; }
    }
    public class Tracking
    {
        public string TrackingRef { get; set; }
        public int? TrackingRefId { get; set; }
        public string TrackingNotes { get; set; }
    }
    #endregion

    public class Report
    {
        public string ReportCode { get; set; }
    }
    public class Consent
    {
        public string ReportType { get; set; }
        public string StateCounty { get; set; }
    }
    public struct CustomOrderCollection
    {
        public tblOrder ObjOrder { get; set; }
        public List<tblOrderDetail> ListOrderDetail { get; set; }
        public List<tblOrderRequestData> ListOrderRequestData { get; set; }
        public List<tblOrderResponseData> ListOrderResponseData { get; set; }
        public tblOrderSearchedData ObjOrderSearchedData { get; set; }
        public tblOrderPersonalQtnInfo ObjOrderPersonalQtnInfo { get; set; }
        public tblOrderSearchedAliasLocationInfo ObjOrderSearchedAliasLocationInfo { get; set; }
        public List<tblOrderSearchedAliasPersonalInfo> ListOrderSearchedAliasPersonalInfo { get; set; }
        public tblOrderSearchedAliasWorkInfo ObjOrderSearchedAliasWorkInfo { get; set; }
        public List<tblOrderSearchedEducationInfo> ListOrderSearchedEducationInfo { get; set; }
        public List<tblOrderSearchedEmploymentInfo> ListOrderSearchedEmploymentInfo { get; set; }
        public List<tblOrderSearchedLicenseInfo> ListOrderSearchedLicenseInfo { get; set; }
        public List<tblOrderSearchedLiveRunnerInfo> ListOrderSearchedLiveRunnerInfo { get; set; }
        public List<tblOrderSearchedPersonalInfo> ListOrderSearchedPersonalInfo { get; set; }
        public List<string> ListOrderReports { get; set; }
        public Dictionary<string, int?> CompanyProductInfo { get; set; }
        public tblReferenceCode ObjReferenceCode { get; set; }
    }
    public class TiredSearchedData
    {
        public string county { get; set; }
        public string state { get; set; }
        public string fName { get; set; }
        public string mName { get; set; }
        public string lName { get; set; }
    }
    #region 3ticket#440
    public class TiredSearchedDataJurisdiction
    {
        public string county { get; set; }
        public string Jurisdiction { get; set; }
        public string fName { get; set; }
        public string mName { get; set; }
        public string lName { get; set; }
    }
    #endregion
}
