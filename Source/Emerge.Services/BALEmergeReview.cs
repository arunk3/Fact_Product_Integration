﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Xml.Linq;
using Emerge.Data;
using System.Globalization;
using System.Xml;
using Emerge.Services.Helper;
using System.Net.Mail;
using System.IO;
using System.Web;
using System.Configuration;
using MomentumInfotech.EmailProvider;
namespace Emerge.Services
{
    public class BALEmergeReview
    {
        /// <summary>
        /// This function is used to revert changes to the Response XML made for editing purpose
        /// </summary>
        /// <param name="ResponseData"></param>
        /// <param name="UpdatedResponse"></param>
        /// <param name="pkOrderResponseId"></param>
        /// <param name="ProductCode"></param>
        public string CompleteFinalResponse(string ResponseData, string UpdatedResponse, int pkOrderResponseId, string ProductCode)
        {
            string FinalResponse = "";
            try
            {
                //Changed By Himesh Kumar- Removed PS product condition
                if (ProductCode.ToUpper().Trim() == "NCR1" || ProductCode.ToUpper().Trim() == "RCX" || ProductCode.ToUpper().Trim() == "FCR" || ProductCode.ToUpper().Trim() == "NCR+")
                {
                    int TrimedUpTo = ResponseData.IndexOf("<BackgroundReportPackage>");
                    StringBuilder CompleteResponse = new StringBuilder();
                    string XMLInitials = ResponseData.Substring(0, TrimedUpTo);
                    CompleteResponse.Append(XMLInitials);
                    CompleteResponse.Append(UpdatedResponse);
                    CompleteResponse.Append("</BackgroundReports>");
                    if (ProductCode.ToUpper().Trim() == "NCR1" || ProductCode.ToUpper().Trim() == "RCX" || ProductCode.ToUpper().Trim() == "NCR+")
                    {
                        FinalResponse = Convert.ToString(CompleteResponse);
                    }
                    else
                    {

                        FinalResponse = Convert.ToString(CompleteResponse).Replace("&lt;", "<").Replace("&gt;", ">");
                    }
                }
                else
                    FinalResponse = UpdatedResponse;
                UpdateFinalResponse(pkOrderResponseId, FinalResponse);
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in CompleteFinalResponse(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            return FinalResponse;
        }

        public string CompleteFinalResponseOptimized(string ResponseData, string UpdatedResponse, int pkOrderResponseId, string ProductCode)
        {
            string FinalResponse = "";
            try
            {
                //CHanged By Himesh Kumar-- Removed PS product condition.
                if (ProductCode.ToUpper().Trim() == "NCR1" || ProductCode.ToUpper().Trim() == "RCX" || ProductCode.ToUpper().Trim() == "FCR" || ProductCode.ToUpper().Trim() == "NCR+")
                {
                    int TrimedUpTo = ResponseData.IndexOf("<BackgroundReportPackage>");
                    StringBuilder CompleteResponse = new StringBuilder();
                    string XMLInitials = ResponseData.Substring(0, TrimedUpTo);
                    CompleteResponse.Append(XMLInitials);
                    CompleteResponse.Append(UpdatedResponse);
                    CompleteResponse.Append("</BackgroundReports>");
                    if (ProductCode.ToUpper().Trim() == "NCR1" || ProductCode.ToUpper().Trim() == "RCX" || ProductCode.ToUpper().Trim() == "NCR+")
                    {
                        FinalResponse = Convert.ToString(CompleteResponse);
                    }
                    else
                    {

                        FinalResponse = Convert.ToString(CompleteResponse).Replace("&lt;", "<").Replace("&gt;", ">");
                    }
                }
                else
                {
                    FinalResponse = UpdatedResponse;
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in CompleteFinalResponseOptimized(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            return FinalResponse;
        }

        public void UpdateFinalResponse(int pkOrderResponseId, string FinalResponse)
        {

            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                if (DX.tblOrderResponseDatas.Where(db => db.pkOrderResponseId == pkOrderResponseId).Count() > 0)
                {
                    tblOrderResponseData ObjResponseData = DX.tblOrderResponseDatas.Where(db => db.pkOrderResponseId == pkOrderResponseId).FirstOrDefault();
                    if (ObjResponseData != null)
                    {
                        //To check if selected orderDetailId releted to NCR1 then no update applied.
                        var byPassNCRUpdate = DX.tblOrderDetails.Where(o => o.pkOrderDetailId == ObjResponseData.fkOrderDetailId && o.fkProductApplicationId == 30).FirstOrDefault();
                        if (byPassNCRUpdate != null)
                        {
                            if (FinalResponse.Contains("Deleted"))//if deleted tag is exists in response for NCR1 then it will be saved other wise it will not save response.
                            {
                                DX.UpdateOrderResponse(pkOrderResponseId, FinalResponse);
                            }
                        }
                        else
                        {
                            DX.UpdateOrderResponse(pkOrderResponseId, FinalResponse);
                        }
                    }
                }
                else
                {
                    tblOrderResponseData_Archieve ObjResponseData = DX.tblOrderResponseData_Archieves.Where(db => db.pkOrderResponseId == pkOrderResponseId).FirstOrDefault();
                    if (ObjResponseData != null)
                    {
                        DX.UpdateResponseDataInArchieve(pkOrderResponseId, FinalResponse);
                    }
                }
            }
        }


        public XDocument SelectNodeToUpdate(int ReviewedNode, string ProductCode, int pkOrderResponseId, string ResponseData, string IsDeleted, string ReviewNote, string ElementToEdit, string cRIDs, string possibleMatches)
        {
            string LogFilename = "XMLCaseDeletionLog_" + DateTime.Now.ToString("yyMMdd");

            BALGeneral.WriteLogForDeletion("Method started : SelectNodeToUpdate", LogFilename);
            //XmlDocument docResponse = new XmlDocument();
            XDocument ObjXDocument = new XDocument();
            switch (ProductCode.ToUpper())
            {
                case "NCR1":
                    ObjXDocument = ModifyRapidCourtReports(ReviewedNode, ProductCode, pkOrderResponseId, ResponseData, IsDeleted, ReviewNote, cRIDs, possibleMatches);
                    break;
                case "PS":
                    ObjXDocument = ModifyRapidCourtReports(ReviewedNode, ProductCode, pkOrderResponseId, ResponseData, IsDeleted, ReviewNote, "0", "0");
                    break;
                case "NCR+":
                    ObjXDocument = ModifyRapidCourtReports(ReviewedNode, ProductCode, pkOrderResponseId, ResponseData, IsDeleted, ReviewNote, "0", "0");
                    break;
                case "FCR":
                    ObjXDocument = ModifyRapidCourtReports(ReviewedNode, ProductCode, pkOrderResponseId, ResponseData, IsDeleted, ReviewNote, "0", "0");
                    break;
                case "RCX":
                    ObjXDocument = ModifyRapidCourtReports(ReviewedNode, ProductCode, pkOrderResponseId, ResponseData, IsDeleted, ReviewNote, "0", "0");
                    break;
                case "SCR":
                    ObjXDocument = ModifyReport(ReviewedNode, ProductCode, ResponseData, IsDeleted, ReviewNote, "report-incident");
                    break;
                case "CCR1":
                    ObjXDocument = ModifyReport(ReviewedNode, ProductCode, ResponseData, IsDeleted, ReviewNote, "report-incident");
                    break;
                case "CCR2":
                    ObjXDocument = ModifyReport(ReviewedNode, ProductCode, ResponseData, IsDeleted, ReviewNote, "detail");
                    break;
                case "OFAC":
                    ObjXDocument = ModifyReport(ReviewedNode, ProductCode, ResponseData, IsDeleted, ReviewNote, "response_row");
                    break;
                case "TDR":
                    ObjXDocument = ModifyTDRReport(ReviewedNode, ProductCode, ResponseData, IsDeleted, ReviewNote, ElementToEdit);
                    break;
                case "PS2":
                    ObjXDocument = ModifyReport(ReviewedNode, ProductCode, ResponseData, IsDeleted, ReviewNote, "response_row");
                    break;
                case "SOR":
                    ObjXDocument = ModifyReport(ReviewedNode, ProductCode, ResponseData, IsDeleted, ReviewNote, "offender");
                    break;
                case "SSR":
                    ObjXDocument = ModifyReport(ReviewedNode, ProductCode, ResponseData, IsDeleted, ReviewNote, "response_row");
                    break;
                default:
                    ObjXDocument = XDocument.Parse(ResponseData);
                    break;

            }
            BALGeneral.WriteLogForDeletion("Method End : SelectNodeToUpdate", LogFilename);
            return ObjXDocument;
        }

        public XDocument ModifyTDRReport(int ReviewedNode, string ProductCode, string ResponseData, string IsDeleted, string ReviewNote, string ElementToEdit)
        {
            string LogFilename = "XMLCaseDeletionLog_+" + DateTime.Now.ToString("yyMMdd");
            string strLog = string.Empty;//For Report Log.
            BALGeneral.WriteLogForDeletion("Method Started : ModifyTDRReport", LogFilename);
            XDocument ObjXDocument = XDocument.Parse(ResponseData);
            string RootElement = "";
            string FirstElement = "";
            string SecondElement = "";
            string ThirdElement = "";
            if (ElementToEdit == "IA")
            {
                RootElement = "response";
                FirstElement = "individual";
                SecondElement = "addresses";
                ThirdElement = "address";
            }
            if (ElementToEdit == "IAI")
            {
                RootElement = "response";
                FirstElement = "individual";
                SecondElement = "akas";
                ThirdElement = "identity";
            }
            if (ElementToEdit == "NEIGH")
            {
                RootElement = "response";
                FirstElement = "individual";
                SecondElement = "neighbors";
                ThirdElement = "neighborhood";
            }
            var RootXElement = ObjXDocument.Descendants(RootElement);
            var xelement = RootXElement.Descendants(FirstElement);
            var xelementChild = xelement.Descendants(SecondElement);
            var xRequiredElement = xelementChild.Descendants(ThirdElement).ElementAt(ReviewedNode - 1);
            if (xRequiredElement.Attribute("Deleted") == null)
            {
                xRequiredElement.Add(new XAttribute("Deleted", "" + IsDeleted + ""));
                XElement xelementNote = new XElement("ReviewNote", ReviewNote);
                xRequiredElement.Add(xelementNote);
                strLog = "Product code : " + ProductCode + "--------created attribute IsDeleted : " + IsDeleted;
            }
            else
            {
                xRequiredElement.Attribute("Deleted").Value = IsDeleted;
                XElement ReviewElement = xRequiredElement.Descendants("ReviewNote").FirstOrDefault();
                ReviewElement.Value = ReviewNote;
                strLog = "Product code : " + ProductCode + "--------updated value of IsDeleted : " + IsDeleted;
            }
            //Adding the log when any item is deleted by Admin.
            BALGeneral.WriteLogForDeletion(strLog, LogFilename);
            BALGeneral.WriteLogForDeletion("Method End : ModifyTDRReport", LogFilename);
            return ObjXDocument;
        }

        public XDocument ModifyReport(int ReviewedNode, string ProductCode, string ResponseData, string IsDeleted, string ReviewNote, string XMLElement)
        {
            string LogFilename = "XMLCaseDeletionLog_+" + DateTime.Now.ToString("yyMMdd");
            string strLog = string.Empty;//For Report Log.
            BALGeneral.WriteLogForDeletion("Method Started : ModifyReport", LogFilename);
            XDocument ObjXDocument = XDocument.Parse(ResponseData);
            var xelement = ObjXDocument.Descendants(XMLElement.Trim()).ElementAt(ReviewedNode - 1);
            if (xelement.Attribute("Deleted") == null)
            {
                xelement.Add(new XAttribute("Deleted", "" + IsDeleted + ""));
                XElement xelementNote = new XElement("ReviewNote", ReviewNote);
                xelement.Add(xelementNote);
                strLog = "Product code : " + ProductCode + "----------created attribute IsDeleted : " + IsDeleted;
            }
            else
            {
                xelement.Attribute("Deleted").Value = IsDeleted;
                XElement ReviewElement = xelement.Descendants("ReviewNote").FirstOrDefault();
                ReviewElement.Value = ReviewNote;
                strLog = "Product code : " + ProductCode + "-----updated value of IsDeleted : " + IsDeleted;
            }
            //Adding the log when any item is deleted by Admin.
            BALGeneral.WriteLogForDeletion(strLog, LogFilename);
            BALGeneral.WriteLogForDeletion("Method End : ModifyReport", LogFilename);
            return ObjXDocument;
        }

        public static string GetRequiredXml_ForRapidCourt(string XmlResponse)
        {
            string XmlResponse_Local = XmlResponse;
            XmlResponse_Local = XmlResponse_Local.Replace("</BackgroundReports>", "");
            int xmlstartindex = XmlResponse_Local.IndexOf("<BackgroundReportPackage>");
            XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
            return XmlResponse_Local;
        }


        public XDocument ModifyRapidCourtReports(int ReviewedNode, string ProductCode, int pkOrderResponseId, string ResponseData, string IsDeleted, string ReviewNote, string cRIDs, string possibleMatches)
        {
            string LogFilename = "XMLCaseDeletionLog_+" + DateTime.Now.ToString("yyMMdd");
            BALGeneral.WriteLogForDeletion("Method Started : ModifyRapidCourtReports", LogFilename);
           // XmlDocument docResponse = new XmlDocument();
            XDocument ObjXDocument = new XDocument();
            string strLog = string.Empty;//For Report Log.
            string XML_Value = GetRequiredXml_ForRapidCourt(ResponseData);
            ObjXDocument = XDocument.Parse(XML_Value);
            if (ProductCode.Trim().ToUpper() == "NCR1")
            {
                //var xelement = ObjXDocument.Descendants("CriminalCase").ElementAt(ReviewedNode - 1);
                var xelement = ObjXDocument.Descendants("CriminalCase").Where(x => x.Attribute("CRID").Value == cRIDs && x.Element("PossibleMatch").Value == possibleMatches).FirstOrDefault();
                if (xelement.Attribute("Deleted") == null)
                {
                    xelement.Add(new XAttribute("Deleted", "" + IsDeleted + ""));
                    XElement xelementNote = new XElement("ReviewNote", ReviewNote);
                    xelement.Add(xelementNote);
                    strLog = "Product code : " + ProductCode + "-----OrderResponseId : " + pkOrderResponseId + "-----------created attribute IsDeleted : " + IsDeleted;
                }
                else
                {
                    xelement.Attribute("Deleted").Value = IsDeleted;
                    XElement ReviewElement = xelement.Descendants("ReviewNote").FirstOrDefault();
                    ReviewElement.Value = ReviewNote;
                    strLog = "Product code : " + ProductCode + "-----OrderResponseId : " + pkOrderResponseId + "-----------updated value of IsDeleted : " + IsDeleted;

                }
            }
            if (ProductCode.Trim().ToUpper() == "PS")
            {
                // Changed By Himesh Kumar
                //var PSxelement = ObjXDocument.Descendants("Screenings").FirstOrDefault();
                //var xelement = PSxelement.Descendants("PersonalData").ElementAt(ReviewedNode - 1);

                var PSxelement = ObjXDocument.Descendants("records").FirstOrDefault();
                var xelement = PSxelement.Descendants("record").ElementAt(ReviewedNode - 1);
                if (xelement.Attribute("Deleted") == null)
                {
                    xelement.Add(new XAttribute("Deleted", "" + IsDeleted + ""));
                    XElement xelementNote = new XElement("ReviewNote", ReviewNote);
                    xelement.Add(xelementNote);
                    strLog = "Product code : " + ProductCode + "-----OrderResponseId : " + pkOrderResponseId + "-----------created attribute IsDeleted : " + IsDeleted;
                }
                else
                {
                    xelement.Attribute("Deleted").Value = IsDeleted;
                    XElement ReviewElement = xelement.Descendants("ReviewNote").FirstOrDefault();
                    ReviewElement.Value = ReviewNote;
                    strLog = "Product code : " + ProductCode + "-----OrderResponseId : " + pkOrderResponseId + "-----------updated value of IsDeleted : " + IsDeleted;
                }
            }
            if (ProductCode.Trim().ToUpper() == "RCX")
            {
                try
                {
                    var xelement = ObjXDocument.Descendants("CriminalCase").ElementAt(ReviewedNode - 1);
                    if (xelement.Attribute("Deleted") == null)
                    {
                        xelement.Add(new XAttribute("Deleted", "" + IsDeleted + ""));
                        XElement xelementNote = new XElement("ReviewNote", ReviewNote);
                        xelement.Add(xelementNote);
                        strLog = "Product code : " + ProductCode + "-----OrderResponseId : " + pkOrderResponseId + "-----------created attribute IsDeleted : " + IsDeleted;
                    }
                    else
                    {
                        xelement.Attribute("Deleted").Value = IsDeleted;
                        XElement ReviewElement = xelement.Descendants("ReviewNote").FirstOrDefault();
                        ReviewElement.Value = ReviewNote;
                        strLog = "Product code : " + ProductCode + "-----OrderResponseId : " + pkOrderResponseId + "-----------updated value of IsDeleted : " + IsDeleted;
                    }
                }
                catch (Exception ex)
                {
                    //Added log for exception handling.
                    VendorServices.GeneralService.WriteLog("Some Error Occured in ModifyRapidCourtReports(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                }
            }
            if (ProductCode.Trim().ToUpper() == "FCR")
            {
                try
                {
                    var xelement = ObjXDocument.Descendants("CriminalCase").ElementAt(ReviewedNode - 1);
                    if (xelement.Attribute("Deleted") == null)
                    {
                        xelement.Add(new XAttribute("Deleted", "" + IsDeleted + ""));
                        XElement xelementNote = new XElement("ReviewNote", ReviewNote);
                        xelement.Add(xelementNote);
                        strLog = "Product code : " + ProductCode + "-----OrderResponseId : " + pkOrderResponseId + "-----------created attribute IsDeleted : " + IsDeleted;
                    }
                    else
                    {
                        xelement.Attribute("Deleted").Value = IsDeleted;
                        XElement ReviewElement = xelement.Descendants("ReviewNote").FirstOrDefault();
                        ReviewElement.Value = ReviewNote;
                        strLog = "Product code : " + ProductCode + "-----OrderResponseId : " + pkOrderResponseId + "-----------updated value of IsDeleted : " + IsDeleted;
                    }
                }
                catch (Exception ex)
                {
                    //Added log for exception handling.
                    VendorServices.GeneralService.WriteLog("Some Error Occured in ModifyRapidCourtReports(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                }
            }
            if (ProductCode.Trim().ToUpper() == "NCR+")
            {
                try
                {
                    var xelement = ObjXDocument.Descendants("CriminalCase").ElementAt(ReviewedNode - 1);
                    if (xelement.Attribute("Deleted") == null)
                    {
                        xelement.Add(new XAttribute("Deleted", "" + IsDeleted + ""));
                        XElement xelementNote = new XElement("ReviewNote", ReviewNote);
                        xelement.Add(xelementNote);
                        strLog = "Product code : " + ProductCode + "-----OrderResponseId : " + pkOrderResponseId + "-----------created attribute IsDeleted : " + IsDeleted;
                    }
                    else
                    {
                        xelement.Attribute("Deleted").Value = IsDeleted;
                        XElement ReviewElement = xelement.Descendants("ReviewNote").FirstOrDefault();
                        ReviewElement.Value = ReviewNote;
                        strLog = "Product code : " + ProductCode + "-----OrderResponseId : " + pkOrderResponseId + "-----------updated value of IsDeleted : " + IsDeleted;
                    }
                }
                catch (Exception ex)
                {
                    //Added log for exception handling.
                    VendorServices.GeneralService.WriteLog("Some Error Occured in ModifyRapidCourtReports(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                }
            }
            //Adding the log when any item is deleted by Admin.
            BALGeneral.WriteLogForDeletion(strLog, LogFilename);
            BALGeneral.WriteLogForDeletion("Method End : ModifyRapidCourtReports", LogFilename);
            return ObjXDocument;
        }



        ///// <summary>
        ///// This method is used to get saved reports
        ///// </summary>
        ///// <param name="ProductId"></param>
        ///// <param name="CompanyUserId"></param>
        ///// <param name="CompanyId"></param>
        ///// <param name="LocationId"></param>
        ///// <param name="SortingColumn"></param>
        ///// <param name="SortingDirection"></param>
        ///// <param name="AllowPaging"></param>
        ///// <param name="PageNum"></param>
        ///// <param name="PageSize"></param>
        ///// <param name="FromDate"></param>
        ///// <param name="ToDate"></param>
        ///// <param name="SearchKeyword"></param>
        ///// <param name="OrderStatus"></param>
        ///// <returns></returns>
        public List<Proc_GetReportsForReviewResult> GetReportsForReview(int ProductId, int CompanyUserId, int CompanyId,
                                                                int LocationId, string SortingColumn, string SortingDirection,
                                                                bool AllowPaging, int PageNum, int PageSize,
                                                                string FromDate, string ToDate, string SearchKeyword, byte OrderStatus, int ReviewType)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {

                return ObjDALDataContext.Proc_GetReportsForReview(ProductId, CompanyUserId, CompanyId,
                                                         LocationId, SortingColumn, SortingDirection,
                                                         AllowPaging, PageNum, PageSize,
                                                         FromDate, ToDate, SearchKeyword, OrderStatus, ReviewType).ToList();
            }
        }

        public List<Proc_GetReportsForWatchListResult> GetReportsForWatchList(int ProductId, int CompanyUserId, int CompanyId,
                                                                int LocationId, string SortingColumn, string SortingDirection,
                                                                bool AllowPaging, int PageNum, int PageSize,
                                                                string FromDate, string ToDate, string SearchKeyword, byte OrderStatus, byte WatchListStatus)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {

                return ObjDALDataContext.GetReportsForWatchList(ProductId, CompanyUserId, CompanyId,
                                                         LocationId, SortingColumn, SortingDirection,
                                                         AllowPaging, PageNum, PageSize,
                                                         FromDate, ToDate, SearchKeyword, OrderStatus, WatchListStatus).ToList();
            }
        }

        public List<Proc_GetConsentPendingReportsResult> GetConsentPendingReports(int ProductId, int CompanyUserId, int CompanyId,
                                                                int LocationId, string SortingColumn, string SortingDirection,
                                                                bool AllowPaging, int PageNum, int PageSize,
                                                              string FromDate, string ToDate, string SearchKeyword, byte OrderStatus, int ReviewType)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {

                return ObjDALDataContext.GetConsentPendingReports(ProductId, CompanyUserId, CompanyId,
                                                         LocationId, SortingColumn, SortingDirection,
                                                         AllowPaging, PageNum, PageSize,
                                                         FromDate, ToDate, SearchKeyword, OrderStatus, ReviewType).ToList();
            }
        }

        public List<Proc_GetReportsForHelpResult> GetReportsForHelp(int ProductId, int CompanyUserId, int CompanyId,
                                                                      int LocationId, string SortingColumn, string SortingDirection,
                                                                      bool AllowPaging, int PageNum, int PageSize,
                                                                      string FromDate, string ToDate, string SearchKeyword, byte OrderStatus, int ReviewType)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {

                return ObjDALDataContext.GetReportsForHelp(ProductId, CompanyUserId, CompanyId,
                                                         LocationId, SortingColumn, SortingDirection,
                                                         AllowPaging, PageNum, PageSize,
                                                         FromDate, ToDate, SearchKeyword, OrderStatus, ReviewType).ToList();
            }
        }

        public int UpdateReportReviewStatus(int? pkOrderDetailId, int ReviewStatus, string ReviewComments, bool Is6MonthOld)
        {
            int status = 0;
            //try
            //{
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                if (!Is6MonthOld)
                {
                    tblOrderDetail ObjOrderDetails = DX.tblOrderDetails.Where(db => db.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                    if (ObjOrderDetails != null)
                    {
                        if(ObjOrderDetails.fkProductApplicationId!=25)
                        {
                            tblOrder objtblOrder = DX.tblOrders.Where(db => db.pkOrderId == ObjOrderDetails.fkOrderId).FirstOrDefault();
                            if (objtblOrder != null)
                            {
                                objtblOrder.IsReviewed = true;
                                DX.SubmitChanges();
                            }
                            ObjOrderDetails.ReviewStatus = ReviewStatus;
                            ObjOrderDetails.ReviewComments = ReviewComments;
                            DX.SubmitChanges();
                            status = 1;
                            UpdateHitStatus(objtblOrder.pkOrderId, Is6MonthOld);
                        }
                    }
                }
                else
                {
                    tblOrderDetails_Archieve ObjOrderDetails_Archieve = DX.tblOrderDetails_Archieves.Where(db => db.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                    if (ObjOrderDetails_Archieve != null)
                    {
                        tblOrders_Archieve objtblOrder = DX.tblOrders_Archieves.Where(db => db.pkOrderId == ObjOrderDetails_Archieve.fkOrderId).FirstOrDefault();
                        if (objtblOrder != null)
                        {
                            objtblOrder.IsReviewed = true;
                            DX.SubmitChanges();
                        }
                        ObjOrderDetails_Archieve.ReviewStatus = ReviewStatus;
                        ObjOrderDetails_Archieve.ReviewComments = ReviewComments;
                        DX.SubmitChanges();
                        status = 1;
                        UpdateHitStatus(objtblOrder.pkOrderId, Is6MonthOld);
                    }
                }
            }
            return status;
        }

        public void UpdateHitStatus(int orderId, bool Is6MonthOld = false)
        {
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    #region Main


                    List<OrderList> objOrderList = new List<OrderList>();
                    if (!Is6MonthOld)
                    {

                        objOrderList = (from O in DX.tblOrders
                                        join OD in DX.tblOrderDetails on O.pkOrderId equals OD.fkOrderId
                                        join OR in DX.tblOrderResponseDatas on OD.pkOrderDetailId equals OR.fkOrderDetailId
                                        join P in DX.tblProductPerApplications on OD.fkProductApplicationId equals P.pkProductApplicationId
                                        where O.pkOrderId == orderId && P.fkReportCategoryId != 9 && (P.ProductCode == "SCR" || P.ProductCode == "SOR" || P.ProductCode == "OFAC" || P.ProductCode == "NCR1" || P.ProductCode == "NCR2" || P.ProductCode == "NCR+" || P.ProductCode == "NCR4" || P.ProductCode == "CCR1" || P.ProductCode == "CCR2" || P.ProductCode == "RCX")
                                        select new OrderList
                                        {
                                            pkOrderId = O.pkOrderId,
                                            pkOrderDetailId = OD.pkOrderDetailId,
                                            ResponseData = OR.ResponseData,
                                            ProductCode = P.ProductCode,
                                            IsLiveRunner = OD.IsLiveRunner
                                        }).ToList();
                    }

                    else
                    {
                        objOrderList = (from O in DX.tblOrders_Archieves
                                        join OD in DX.tblOrderDetails_Archieves on O.pkOrderId equals OD.fkOrderId
                                        join OR in DX.tblOrderResponseData_Archieves on OD.pkOrderDetailId equals OR.fkOrderDetailId
                                        join P in DX.tblProductPerApplications on OD.fkProductApplicationId equals P.pkProductApplicationId
                                        where O.pkOrderId == orderId && P.fkReportCategoryId != 9 && (P.ProductCode == "SCR" || P.ProductCode == "SOR" || P.ProductCode == "OFAC" || P.ProductCode == "NCR1" || P.ProductCode == "NCR2" || P.ProductCode == "NCR+" || P.ProductCode == "NCR4" || P.ProductCode == "CCR1" || P.ProductCode == "CCR2" || P.ProductCode == "RCX")
                                        select new OrderList
                                        {
                                            pkOrderId = O.pkOrderId,
                                            pkOrderDetailId = OD.pkOrderDetailId,
                                            ResponseData = OR.ResponseData,
                                            ProductCode = P.ProductCode,
                                            IsLiveRunner = OD.IsLiveRunner
                                        }).ToList();
                    }


                    List<OrderList> TempOrderList;
                    List<int?> OrderId = objOrderList.Select(db => db.pkOrderId).Distinct().ToList();
                    foreach (int Oid in OrderId)
                    {
                        byte HitCount = 0;
                        TempOrderList = objOrderList.Where(db => db.pkOrderId == Oid).ToList();
                        foreach (OrderList Odr in TempOrderList)
                        {
                            bool Objdynamic = GetResponse(Odr.ResponseData);
                            if (Objdynamic == true)
                            {
                                if (Odr.IsLiveRunner == true)
                                {
                                    Odr.ProductCode = Odr.ProductCode + "LiveRunner";
                                }
                                string FinalResponse = ""; string UpdatedResponse = "";
                                bool IsShowRawData;
                                byte WatchListStatus = 0;
                                XMLFilteration objXMLFilteration = new XMLFilteration();
                                FinalResponse = Odr.ResponseData;
                                UpdatedResponse = Odr.ResponseData;


                                #region Dismissed Charge Response

                                if (Odr.ProductCode.ToUpper() == "NCR1" || Odr.ProductCode.ToUpper() == "CCR1LIVERUNNER" || Odr.ProductCode.ToUpper() == "RCX" || Odr.ProductCode.ToUpper() == "SCRLIVERUNNER")
                                {
                                    try
                                    {
                                        string UpdatedDismissedChargeResponse = objXMLFilteration.GetRemovedDismissedChargeXMLFilter(UpdatedResponse, Odr.ProductCode, Odr.pkOrderDetailId, Odr.IsLiveRunner);
                                        UpdatedResponse = UpdatedDismissedChargeResponse;
                                        FinalResponse = UpdatedDismissedChargeResponse;
                                    }
                                    catch (Exception ex)
                                    {
                                        FinalResponse = UpdatedResponse;
                                        //Added log for exception handling.
                                        VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateHitStatus(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                                    }

                                }
                                #endregion Dismissed Charge Response


                                if (Odr.ProductCode == "NCR1" || Odr.ProductCode == "SCR" || Odr.ProductCode == "NCR+" || Odr.ProductCode == "RCX" || Odr.ProductCode == "NCR4" || Odr.ProductCode == "CCR1LiveRunner" || Odr.ProductCode == "CCR2LiveRunner")
                                {
                                    #region Ticket #800: 7 YEAR STATES
                                    try
                                    {
                                        string Updated7YearResponse = HideSevenYear(UpdatedResponse, Odr.pkOrderDetailId, WatchListStatus, Odr.ProductCode, DateTime.Now, out IsShowRawData);

                                        UpdatedResponse = Updated7YearResponse;

                                        FinalResponse = Updated7YearResponse;
                                    }
                                    catch (Exception ex)
                                    {
                                        FinalResponse = UpdatedResponse;
                                        //Added log for exception handling.
                                        VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateHitStatus(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                                    }

                                    #endregion
                                }
                                if (Odr.ProductCode == "OFAC" || Odr.ProductCode == "NCR1" || Odr.ProductCode == "NCR2" || Odr.ProductCode == "NCR+" || Odr.ProductCode == "NCR4" || Odr.ProductCode == "SCR" || Odr.ProductCode == "CCR1" || Odr.ProductCode == "CCR2" || Odr.ProductCode == "SOR" || Odr.ProductCode == "RCX" || Odr.ProductCode == "SCRLiveRunner" || Odr.ProductCode == "CCR1LiveRunner" || Odr.ProductCode == "CCR2LiveRunner")
                                {
                                    XDocument ObjXDocument = XDocument.Parse(FinalResponse);
                                    HitCount = CheckIsHit((int)Odr.pkOrderId, Odr.ProductCode, ObjXDocument);
                                    if (HitCount == 1)
                                    {
                                        HitCount = 1;
                                        break;
                                    }
                                }
                            }
                        }
                        UpdateOrderHitStatus(Oid, HitCount);//Update(HitCOUT);
                    }

                    #endregion

                    #region Archive

                    List<OrderList> objOrderArchList = (from O in DX.tblOrders_Archieves
                                                        join OD in DX.tblOrderDetails_Archieves on O.pkOrderId equals OD.fkOrderId
                                                        join OR in DX.tblOrderResponseData_Archieves on OD.pkOrderDetailId equals OR.fkOrderDetailId
                                                        join P in DX.tblProductPerApplications on OD.fkProductApplicationId equals P.pkProductApplicationId
                                                        where O.pkOrderId == orderId && P.fkReportCategoryId != 9 && (P.ProductCode == "SCR" || P.ProductCode == "SOR" || P.ProductCode == "OFAC" || P.ProductCode == "NCR1" || P.ProductCode == "NCR2" || P.ProductCode == "NCR+" || P.ProductCode == "NCR4" || P.ProductCode == "CCR1" || P.ProductCode == "CCR2" || P.ProductCode == "RCX")
                                                        orderby O.CreatedDate descending
                                                        select new OrderList
                                                        {
                                                            pkOrderId = O.pkOrderId,
                                                            pkOrderDetailId = OD.pkOrderDetailId,
                                                            ResponseData = OR.ResponseData,
                                                            ProductCode = P.ProductCode,
                                                            IsLiveRunner = OD.IsLiveRunner
                                                        }).ToList();

                    List<OrderList> TempOrderArchList;
                    List<int?> ArchOrderId = objOrderArchList.Select(db => db.pkOrderId).Distinct().ToList();
                    foreach (int Oid in ArchOrderId)
                    {
                        byte HitCount = 0;
                        TempOrderArchList = objOrderList.Where(db => db.pkOrderId == Oid).ToList();
                        foreach (OrderList Odr in TempOrderArchList)
                        {
                            bool Objdynamic = GetResponse(Odr.ResponseData);
                            if (Objdynamic == true)
                            {
                                if (Odr.IsLiveRunner == true)
                                {
                                    Odr.ProductCode = Odr.ProductCode + "LiveRunner";
                                }
                                string FinalResponse = ""; string UpdatedResponse = "";
                                bool IsShowRawData;
                                byte WatchListStatus = 0;
                                XMLFilteration objXMLFilteration = new XMLFilteration();
                                UpdatedResponse = Odr.ResponseData;


                                #region Dismissed Charge Response

                                if (Odr.ProductCode.ToUpper() == "NCR1" || Odr.ProductCode.ToUpper() == "CCR1LIVERUNNER" || Odr.ProductCode.ToUpper() == "RCX" || Odr.ProductCode.ToUpper() == "SCRLIVERUNNER")
                                {
                                    try
                                    {
                                        string UpdatedDismissedChargeResponse = objXMLFilteration.GetRemovedDismissedChargeXMLFilter(UpdatedResponse, Odr.ProductCode, Odr.pkOrderDetailId, Odr.IsLiveRunner);
                                        UpdatedResponse = UpdatedDismissedChargeResponse;
                                        FinalResponse = UpdatedDismissedChargeResponse;
                                    }
                                    catch (Exception ex)
                                    {
                                        FinalResponse = UpdatedResponse;
                                        //Added log for exception handling.
                                        VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateHitStatus(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                                    }

                                }
                                #endregion Dismissed Charge Response


                                if (Odr.ProductCode == "NCR1" || Odr.ProductCode == "SCR" || Odr.ProductCode == "NCR+" || Odr.ProductCode == "RCX" || Odr.ProductCode == "NCR4" || Odr.ProductCode == "CCR1LiveRunner" || Odr.ProductCode == "CCR2LiveRunner")
                                {
                                    #region Ticket #800: 7 YEAR STATES
                                    try
                                    {
                                        string Updated7YearResponse = HideSevenYear(UpdatedResponse, Odr.pkOrderDetailId, WatchListStatus, Odr.ProductCode, DateTime.Now, out IsShowRawData);

                                        UpdatedResponse = Updated7YearResponse;

                                        FinalResponse = Updated7YearResponse;
                                    }
                                    catch (Exception ex)
                                    {
                                        FinalResponse = UpdatedResponse;
                                        //Added log for exception handling.
                                        VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateHitStatus(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                                    }

                                    #endregion
                                }
                                if (Odr.ProductCode == "OFAC" || Odr.ProductCode == "NCR1" || Odr.ProductCode == "NCR2" || Odr.ProductCode == "NCR+" || Odr.ProductCode == "NCR4" || Odr.ProductCode == "SCR" || Odr.ProductCode == "CCR1" || Odr.ProductCode == "CCR2" || Odr.ProductCode == "SOR" || Odr.ProductCode == "RCX" || Odr.ProductCode == "SCRLiveRunner" || Odr.ProductCode == "CCR1LiveRunner" || Odr.ProductCode == "CCR2LiveRunner")
                                {
                                    XDocument ObjXDocument = XDocument.Parse(FinalResponse);
                                    HitCount = CheckIsHit((int)Odr.pkOrderId, Odr.ProductCode, ObjXDocument);
                                    if (HitCount == 1)
                                    {
                                        HitCount = 1;
                                        break;
                                    }
                                }
                            }
                        }
                        UpdateOrderHitStatus(Oid, HitCount);//Update(HitCOUT);
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateHitStatus(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
        }

        private bool GetResponse(string p)
        {
            XmlDocument ObjXmlDocument = new XmlDocument();
            try
            {
                ObjXmlDocument.LoadXml(p);
                return true;
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in GetResponse(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                return false;
            }

        }

        public byte CheckIsHit(int OrderId, string ProductCode, XDocument ObjXDocument)
        {
            byte IsHit = 0;
            int RemainingNodes = 0;
            int Totalcount = 0;
            try
            {
                if (ProductCode == "NCR1" || ProductCode == "NCR+" || ProductCode == "RCX" || ProductCode == "SCRLiveRunner" || ProductCode == "CCR1LiveRunner" || ProductCode == "CCR2LiveRunner")
                {
                    string XML_Value = BALGeneral.GetRequiredXml_ForRapidCourt(ObjXDocument.ToString());
                    XDocument ObjXDocument1 = XDocument.Parse(XML_Value);
                    var lstScreenings = ObjXDocument1.Descendants("Screenings").ToList();
                    if (lstScreenings.Count > 0)
                    {
                        var lstCriminalReports = lstScreenings.Descendants("CriminalReport").FirstOrDefault();

                        if (lstCriminalReports != null)
                        {
                            var lstCriminalCases = lstCriminalReports.Descendants("CriminalCase").ToList();
                            if (lstCriminalCases != null && lstCriminalCases.Count > 0)
                            {
                                Totalcount = lstCriminalCases.Count;
                                foreach (XElement user in lstCriminalCases)
                                {
                                    if (user.HasAttributes && user.Attribute("Deleted").Value == "1")
                                    {
                                        RemainingNodes++;
                                    }
                                    else
                                    {
                                    }
                                }

                                if (Totalcount == RemainingNodes)
                                {
                                    IsHit = 2;
                                }
                                else
                                {
                                    IsHit = 1;
                                }
                            }
                            else
                            {
                                IsHit = 2;
                            }
                        }
                        else
                        {
                            IsHit = 2;
                        }

                    }
                }
                if (ProductCode == "NCR2")
                {
                    var lstreportdetailheader = ObjXDocument.Descendants("report-detail-header").ToList();
                    if (lstreportdetailheader.Count > 0)
                    {
                        var lstreportdata = lstreportdetailheader.Descendants("report-data").FirstOrDefault();

                        if (lstreportdata != null)
                        {
                            string reportdataValue = lstreportdata.Value.ToString();
                            if (!string.IsNullOrEmpty(reportdataValue))
                            {
                                IsHit = 1;
                            }
                            else
                            {
                                IsHit = 2;
                            }
                        }
                        else
                        {
                            IsHit = 2;
                        }
                    }
                }
                if (ProductCode == "NCR4")
                {
                    var lstResponse = ObjXDocument.Descendants("Response").ToList();
                    if (lstResponse.Count > 0)
                    {
                        var lstCriminalSearchResults = lstResponse.Descendants("CriminalSearchResults").FirstOrDefault();

                        if (lstCriminalSearchResults != null)
                        {
                            var lstJurisdictionResults = lstCriminalSearchResults.Descendants("JurisdictionResults").ToList();
                            if (lstJurisdictionResults != null && lstJurisdictionResults.Count > 0)
                            {
                                IsHit = 1;
                            }
                            else
                            {
                                IsHit = 2;
                            }
                        }
                        else
                        {
                            IsHit = 2;
                        }
                    }
                }
                if (ProductCode == "SOR")
                {
                    var xElementresponse = ObjXDocument.Descendants("response").ToList();
                    if (xElementresponse != null)
                    {
                        var detail = ObjXDocument.Descendants("detail").FirstOrDefault();
                        if (detail != null)
                        {
                            var offenders = ObjXDocument.Descendants("offenders").FirstOrDefault();
                            if (offenders != null && offenders.Attribute("qtyFound").Value != "0")
                            {
                                var lstoffender = offenders.Descendants("offender").ToList();
                                if (lstoffender != null && lstoffender.Count > 0)
                                {
                                    Totalcount = lstoffender.Count;
                                    foreach (XElement user in lstoffender)
                                    {
                                        if (user.HasAttributes && user.Attribute("Deleted").Value == "1")
                                        {
                                            RemainingNodes++;
                                        }
                                        else
                                        {
                                        }
                                    }
                                    if (Totalcount == RemainingNodes)
                                    {
                                        IsHit = 2;
                                    }
                                    else
                                    {
                                        IsHit = 1;
                                    }
                                }
                            }
                            else
                            {
                                IsHit = 2;
                            }
                        }

                        else
                        {
                            IsHit = 2;
                        }
                    }
                    else
                    {
                        IsHit = 2;
                    }
                }
                if (ProductCode == "OFAC")
                {
                    var xElementresponse = ObjXDocument.Descendants("response_data").ToList();
                    if (xElementresponse != null)
                    {
                        var response_rowColl = ObjXDocument.Descendants("response_row").ToList();
                        if (response_rowColl != null && response_rowColl.Count > 0)
                        {
                            Totalcount = response_rowColl.Count;
                            foreach (XElement user in response_rowColl)
                            {
                                if (user.HasAttributes && user.Attribute("Deleted").Value == "1")
                                {
                                    RemainingNodes++;
                                }
                                else
                                {
                                }
                            }
                            if (Totalcount == RemainingNodes)
                            {
                                IsHit = 2;
                            }
                            else
                            {
                                IsHit = 1;
                            }
                        }
                    }
                    else
                    {
                        IsHit = 2;
                    }
                }
                if (ProductCode == "CCR1" || ProductCode == "SCR")
                {
                    var xElementresponse = ObjXDocument.Descendants("report-detail-header").ToList();
                    if (xElementresponse != null)
                    {
                        var response_clear_record = ObjXDocument.Descendants("clear-record").ToList();
                        if (response_clear_record != null && response_clear_record.FirstOrDefault().Value.ToString() == "0")
                        {
                            var report_incident = xElementresponse.Descendants("report-incident").ToList();
                            if (report_incident != null && report_incident.Count > 0)
                            {
                                Totalcount = report_incident.Count;
                                foreach (XElement user in report_incident)
                                {
                                    if (user.HasAttributes && user.Attribute("Deleted").Value == "1")
                                    {
                                        RemainingNodes++;
                                    }
                                    else
                                    {
                                    }
                                }
                                if (Totalcount == RemainingNodes)
                                {
                                    IsHit = 2;
                                }
                                else
                                {
                                    IsHit = 1;
                                }
                            }
                        }
                    }
                    else
                    {
                        IsHit = 2;
                    }
                }
            }
            catch (Exception ex)
            {                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in CheckIsHit(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

            }
            return IsHit;
        }

        public void UpdateOrderHitStatus(int pkOrderId, byte HitStatus)
        {
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    if (DX.tblOrders.Where(db => db.pkOrderId == pkOrderId).Count() > 0)
                    {
                        tblOrder ObjtblOrder = DX.tblOrders.Where(db => db.pkOrderId == pkOrderId).FirstOrDefault();
                        if (ObjtblOrder != null)
                        {
                            ObjtblOrder.HitStatus = HitStatus;
                            DX.SubmitChanges();
                        }
                    }
                    else
                    {
                        tblOrders_Archieve ObjtblOrder_Archieve = DX.tblOrders_Archieves.Where(db => db.pkOrderId == pkOrderId).FirstOrDefault();
                        if (ObjtblOrder_Archieve != null)
                        {
                            ObjtblOrder_Archieve.HitStatus = HitStatus;
                            DX.SubmitChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateOrderHitStatus(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

            }
        }

        public int UpdateHelpWithOrder(int pkOrderDetailId, int HelpWithOrderStatus, string HelpWithOrderComments, bool Is6MonthOld)
        {
            int status = 0;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                if (!Is6MonthOld)
                {
                    tblOrderDetail ObjOrderDetails = DX.tblOrderDetails.Where(db => db.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                    if (ObjOrderDetails != null)
                    {
                        ObjOrderDetails.HelpWithOrderStatus = HelpWithOrderStatus;
                        ObjOrderDetails.HelpWithOrderComments = HelpWithOrderComments;
                        DX.SubmitChanges();
                        status = 1;
                    }
                }
                else
                {
                    tblOrderDetails_Archieve ObjOrderDetails_Archieve = DX.tblOrderDetails_Archieves.Where(db => db.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                    if (ObjOrderDetails_Archieve != null)
                    {
                        ObjOrderDetails_Archieve.HelpWithOrderStatus = HelpWithOrderStatus;
                        ObjOrderDetails_Archieve.HelpWithOrderComments = HelpWithOrderComments;
                        DX.SubmitChanges();
                        status = 1;
                    }
                }
            }
            return status;
        }

        ///// <summary>
        ///// Function to update reviewed status and send mail to user who have requested the report review.
        ///// </summary>
        ///// <param name="pkOrderDetailId"></param>
        ///// <param name="ReviewStatus"></param>
        ///// <returns></returns>

        public int SetNoResponseStatus(int pkOrderId, bool IsEnable)
        {
            int status = -1;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {

                tblOrder objtblOrder = DX.tblOrders.Where(db => db.pkOrderId == pkOrderId).FirstOrDefault();
                if (objtblOrder != null)
                {
                    objtblOrder.IsNoResponse = IsEnable;
                    DX.SubmitChanges();
                    status = 1;
                }
            }
            return status;
        }

        public string GetStatusLogDescrptionById(int pklogId)
        {
            string Des = string.Empty;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                var record = DX.tblCustomReviewMessages.Where(rec => rec.pkLogId == pklogId).FirstOrDefault();
                Des = record.Descrption;
            }
            return Des;

        }

        public int UpdateReviewStatusToNoResponse(int pkOrderId)
        {
            int result = -1;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {

                tblOrder objtblOrder = DX.tblOrders.Where(db => db.pkOrderId == pkOrderId).FirstOrDefault();
                if (objtblOrder != null)
                {
                    // For Main table
                    // Set Order Status =4 for No Response According to 282 Ticket.
                    objtblOrder.OrderStatus = 4;
                    objtblOrder.IsNoResponse = false;
                    DX.SubmitChanges();
                    List<tblOrderDetail> ObjOrderDetails = DX.tblOrderDetails.Where(db => db.fkOrderId == pkOrderId).ToList();
                    if (ObjOrderDetails.Count > 0)
                    {
                        foreach (tblOrderDetail Obj in ObjOrderDetails)
                        {
                            SendEmailNoResponseReview(pkOrderId, Obj.pkOrderDetailId);
                        }
                        result = 1;
                    }
                }
                else
                {
                    // For  Archive Section
                    tblOrders_Archieve objtblOrderArchive = DX.tblOrders_Archieves.Where(db => db.pkOrderId == pkOrderId).FirstOrDefault();
                    objtblOrderArchive.OrderStatus = 4;

                    //objtblOrderArchive.isno = false;
                    DX.SubmitChanges();
                    List<tblOrderDetails_Archieve> ObjOrderDetails = DX.tblOrderDetails_Archieves.Where(db => db.fkOrderId == pkOrderId).ToList();
                    if (ObjOrderDetails.Count > 0)
                    {
                        foreach (tblOrderDetails_Archieve Obj in ObjOrderDetails)
                        {
                            SendEmailNoResponseReview(pkOrderId, Obj.pkOrderDetailId);
                        }
                        result = 1;
                    }

                }
            }
            return result;
        }

        public void SendNoResponseEmailAfterTwoDay()
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                // List<tblOrder> ObjOrderList = DX.tblOrders.Where(rec => rec.IsNoResponse == true).ToList();
                List<Proc_GetNoResponseOrderListResult> ObjOrderList = DX.GetNoResponseOrderList().ToList();

                if (ObjOrderList.Count() > 0)
                {
                    for (int i = 0; i <= ObjOrderList.Count(); i++)
                    {
                        SendEmailNoResponseReview(ObjOrderList.ElementAt(i).pkOrderId, ObjOrderList.ElementAt(i).pkOrderDetailId);
                        i++;
                    }

                }



            }
        }

        private void SendEmailNoResponseReview(int pkOrderId, int pkOrderDetailId)
        {
            try
            {

                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    Proc_GetUserInfoByReportResult ObjReviewUserInfo = DX.GetUserInfoByReport(pkOrderId, pkOrderDetailId).FirstOrDefault(); ;
                    string UserEmail = string.Empty;
                    //string ReportName = string.Empty;
                    string Subject = "";//"Your Report on " + ObjReviewUserInfo.SearchedFirstName + " " + ObjReviewUserInfo.SearchedLastName + " after review is Ready on Emerge"; //"Your Report after review is Ready";
                    if (ObjReviewUserInfo != null)
                    {
                        CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
                        TextInfo textInfo = cultureInfo.TextInfo;
                        string firstname = ObjReviewUserInfo.SearchedFirstName != null ? textInfo.ToTitleCase(ObjReviewUserInfo.SearchedFirstName) : string.Empty;
                        string lastname = ObjReviewUserInfo.SearchedLastName != null ? textInfo.ToTitleCase(ObjReviewUserInfo.SearchedLastName) : string.Empty;
                        UserEmail = ObjReviewUserInfo.UserName;
                        //string Body = CreateEmailBody(ObjReviewUserInfo);
                        //string Linkis = GetViewReportLink(ObjReviewUserInfo.UserName, ObjReviewUserInfo.pkOrderId);

                        // string WebsiteLogo = "https://www.intelfi.com/emerge/Resources/Upload/Images/" + BALGeneral.GetGeneralSettings().WebSiteLogo;
                        string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + BALGeneral.GetGeneralSettings().WebSiteLogo;

                        BALGeneral ObjBALGeneral = new BALGeneral();
                        int pkTemplateId = 71;
                        var emailContent = ObjBALGeneral.GetEmailTemplate(new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852"), pkTemplateId);//"Emerge review Report"


                        BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();

                        string emailText = emailContent.TemplateContent;

                        #region For New Style SystemTemplate
                        string Bookmarkfile = string.Empty;
                        string bookmarkfilepath = string.Empty;
                        Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                        Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                        Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                        emailText = Bookmarkfile;
                        #endregion
                        Subject = emailContent.TemplateSubject.Replace("%ApplicantName%", firstname + " " + lastname);
                        #region New way Bookmark


                        Bookmarks objBookmark = new Bookmarks();
                        string MessageBody = string.Empty;
                        objBookmark.EmailContent = emailText;
                        objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                        objBookmark.ApplicantName = "Emerge";
                        // objBookmark.ViewReportLink = Linkis;
                        objBookmark.ViewReportLink = string.Empty;
                        objBookmark.ProductName = ObjReviewUserInfo.ProductDisplayName;
                        objBookmark.ApplicantName = firstname + "," + lastname;
                        objBookmark.ReportCode = ObjReviewUserInfo.ProductCode;
                        objBookmark.OrderNumber = ObjReviewUserInfo.OrderNo;
                        MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Guid.Empty);

                        #endregion

                        SendEmail(UserEmail, MessageBody, Subject);

                    }
                }
            }
            catch (Exception ex)
            {                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in SendEmailNoResponseReview(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

            }
        }




        public int UpdateWatchListStatusIndb(int? pkOrderId, int WatchlistStatus)
        {
            int status = 1;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                if (DX.tblOrderDetails.Where(db => db.fkOrderId == pkOrderId).Count() > 0)
                {
                    List<tblOrderDetail> ObjOrderDetails = DX.tblOrderDetails.Where(db => db.fkOrderId == pkOrderId && db.WatchListStatus == 1).ToList();
                    if (ObjOrderDetails.Count > 0)
                    {
                        foreach (tblOrderDetail Obj in ObjOrderDetails)
                        {
                            Obj.WatchListStatus = 2;
                            DX.SubmitChanges();
                        }
                        status = 2;
                    }
                }
                else
                {
                    List<tblOrderDetails_Archieve> ObjOrderDetails = DX.tblOrderDetails_Archieves.Where(
                            d => d.fkOrderId == pkOrderId).ToList();
                    if (ObjOrderDetails.Count > 0)
                    {
                        foreach (tblOrderDetails_Archieve Obj in ObjOrderDetails)
                        {
                            Obj.WatchListStatus = 2;
                            DX.SubmitChanges();
                        }
                        status = 2;
                    }
                }
            }

            return status;

        }

        public int UpdateReportReviewStatus(int? pkOrderDetailId, int ReviewStatus)
        {
            int status = 0;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                if (DX.tblOrderDetails.Where(d => d.fkOrderId == pkOrderDetailId).Count() > 0)
                {
                    #region Main Tables
                    if (ReviewStatus != 5)
                    {

                        List<tblOrderDetail> ObjOrderDetails = DX.tblOrderDetails.Where(
                            d => d.fkOrderId == pkOrderDetailId).ToList<tblOrderDetail>();
                        if (ObjOrderDetails.Count != 0)
                        {
                            int pkOrderDetailIdOfReviewedReport = 0;
                            try
                            {
                                pkOrderDetailIdOfReviewedReport = ObjOrderDetails.Where(d => d.ReviewStatus == 1).Select(d => d.pkOrderDetailId).FirstOrDefault();
                            }
                            catch (Exception ex)
                            {                //Added log for exception handling.
                                VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateReportReviewStatus(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

                            }
                            if (pkOrderDetailIdOfReviewedReport != 0)
                            {
                                if (ObjOrderDetails.Any(db => db.ReviewStatus == 1))
                                {
                                    SendReviewCompletedEmail(ObjOrderDetails.FirstOrDefault().fkOrderId, pkOrderDetailIdOfReviewedReport);
                                }
                                foreach (tblOrderDetail Obj in ObjOrderDetails)
                                {
                                    Obj.ReviewStatus = ReviewStatus;
                                    DX.SubmitChanges();
                                }
                            }
                            status = 1;
                        }
                    }
                    if (ReviewStatus == 5)   //came from Pending Review Reports to complete the Review status of whole Order.
                    {
                        int pkOrderId = (int)pkOrderDetailId;
                        tblOrder objtblOrder = DX.tblOrders.Where(db => db.pkOrderId == pkOrderId).FirstOrDefault();
                        if (objtblOrder != null)
                        {
                            objtblOrder.IsReviewed = true;
                            objtblOrder.IsNoResponse = false;
                            if (objtblOrder.OrderStatus == 4)
                            { // For Change Order Status No Response to Complete 
                                objtblOrder.OrderStatus = 2;
                            }
                            DX.SubmitChanges();
                        }
                        List<tblOrderDetail> ObjOrderDetails = DX.tblOrderDetails.Where(db => db.fkOrderId == pkOrderId).ToList();
                        if (ObjOrderDetails.Count > 0)
                        {
                            foreach (tblOrderDetail Obj in ObjOrderDetails)
                            {
                                Obj.ReviewStatus = 2;
                                DX.SubmitChanges();
                            }
                            Proc_GetUserInfoByReportResult ObjReviewUserInfo = DX.GetUserInfoByReport(pkOrderId, ObjOrderDetails.FirstOrDefault().pkOrderDetailId).FirstOrDefault();
                            BALEmailSystemTemplates ObjEmailTemplate = new BALEmailSystemTemplates();
                            tblAlert ObjtblAlert = new tblAlert();
                            ObjtblAlert.AlertTypeId = pkOrderId.ToString();
                            ObjtblAlert.AlertType = 4;  //4 for Emerge Review Completed
                            ObjtblAlert.AlertSentDate = DateTime.Now;
                            if (ObjReviewUserInfo != null)
                            {
                                ObjtblAlert.fkUserId = new Guid(ObjReviewUserInfo.UserId.ToString());
                                //int AlertId = 
                                ObjEmailTemplate.InsertAlerts(ObjtblAlert);
                            }
                        }

                    }
                    #endregion
                }
                else
                {
                    #region Archieve Tables

                    if (ReviewStatus != 5)
                    {
                        //List<tblOrderDetails_Archieve> ObjOrderDetails = DX.tblOrderDetails_Archieves.Where(
                        //    d => d.fkOrderId == (DX.tblOrderDetails_Archieves.Where(
                        //        t => t.pkOrderDetailId == pkOrderDetailId).Select(
                        //        t => t.fkOrderId).FirstOrDefault())).ToList();
                        List<tblOrderDetails_Archieve> ObjOrderDetails = DX.tblOrderDetails_Archieves.Where(
                             d => d.fkOrderId == pkOrderDetailId).ToList<tblOrderDetails_Archieve>();

                        if (ObjOrderDetails.Count != 0)
                        {
                            int pkOrderDetailIdOfReviewedReport = 0;
                            try
                            {
                                pkOrderDetailIdOfReviewedReport = ObjOrderDetails.Where(d => d.ReviewStatus == 1).Select(d => d.pkOrderDetailId).FirstOrDefault();
                            }
                            catch (Exception ex)
                            {                //Added log for exception handling.
                                VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateReportReviewStatus(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

                            }
                            if (pkOrderDetailIdOfReviewedReport != 0)
                            {
                                if (ObjOrderDetails.Any(db => db.ReviewStatus == 1))
                                {
                                    SendReviewCompletedEmail(ObjOrderDetails.FirstOrDefault().fkOrderId, pkOrderDetailIdOfReviewedReport);
                                }
                                foreach (tblOrderDetails_Archieve Obj in ObjOrderDetails)
                                {
                                    Obj.ReviewStatus = ReviewStatus;
                                    DX.SubmitChanges();
                                }
                            }
                            status = 1;
                        }
                    }
                    if (ReviewStatus == 5)   //came from Pending Review Reports to complete the Review status of whole Order.
                    {
                        int pkOrderId = (int)pkOrderDetailId;
                        tblOrder objtblOrder = DX.tblOrders.Where(db => db.pkOrderId == pkOrderId).FirstOrDefault();
                        if (objtblOrder != null)
                        {
                            objtblOrder.IsReviewed = false;
                            DX.SubmitChanges();
                        }
                        List<tblOrderDetails_Archieve> ObjOrderDetails = DX.tblOrderDetails_Archieves.Where(db => db.fkOrderId == pkOrderId).ToList();
                        if (ObjOrderDetails.Count > 0)
                        {
                            foreach (tblOrderDetails_Archieve Obj in ObjOrderDetails)
                            {
                                Obj.ReviewStatus = 2;
                                DX.SubmitChanges();
                            }
                            Proc_GetUserInfoByReportResult ObjReviewUserInfo = DX.GetUserInfoByReport(pkOrderId, ObjOrderDetails.FirstOrDefault().pkOrderDetailId).FirstOrDefault();
                            BALEmailSystemTemplates ObjEmailTemplate = new BALEmailSystemTemplates();
                            tblAlert ObjtblAlert = new tblAlert();
                            ObjtblAlert.AlertTypeId = pkOrderId.ToString();
                            ObjtblAlert.AlertType = 4;  //4 for Emerge Review Completed
                            ObjtblAlert.AlertSentDate = DateTime.Now;
                            if (ObjReviewUserInfo != null)
                            {
                                ObjtblAlert.fkUserId = new Guid(ObjReviewUserInfo.UserId.ToString());
                                int AlertId = ObjEmailTemplate.InsertAlerts(ObjtblAlert);
                            }
                        }
                    }

                    #endregion
                }
            }
            return status;
        }

        public int UpdateHelpWithOrderStatus(int? pkOrderDetailId, int ReviewStatus)
        {
            int status = 0;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                int? pkOrderId = pkOrderDetailId;
                if (DX.tblOrderDetails.Where(db => db.fkOrderId == pkOrderId).Count() > 0)
                {
                    List<tblOrderDetail> ObjOrderDetails = DX.tblOrderDetails.Where(db => db.fkOrderId == pkOrderId).ToList();
                    if (ObjOrderDetails.Count > 0)
                    {
                        foreach (tblOrderDetail Obj in ObjOrderDetails)
                        {
                            if (Obj.HelpWithOrderStatus == 1)
                            {
                                Obj.HelpWithOrderStatus = 2;
                                DX.SubmitChanges();
                            }
                        }
                    }
                }
                else
                {
                    List<tblOrderDetails_Archieve> ObjOrderDetails = DX.tblOrderDetails_Archieves.Where(db => db.fkOrderId == pkOrderId).ToList();
                    if (ObjOrderDetails.Count > 0)
                    {
                        foreach (tblOrderDetails_Archieve Obj in ObjOrderDetails)
                        {
                            if (Obj.HelpWithOrderStatus == 1)
                            {
                                Obj.HelpWithOrderStatus = 2;
                                DX.SubmitChanges();
                            }
                        }
                    }
                }

            }
            return status;
        }
        public int UpdateDKTStatus(int? pkOrderDetailId, byte DTKOrderStatus)
        {
            int status = 0;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                try
                {
                    int? pkOrderId = pkOrderDetailId;
                    tblOrder ObjtblOrder = DX.tblOrders.Where(db => db.pkOrderId == pkOrderId).FirstOrDefault();
                    if (ObjtblOrder != null)
                    {
                        ObjtblOrder.OrderStatus = DTKOrderStatus;
                        DX.SubmitChanges();
                    }
                    status = 1;
                }
                catch (Exception ex)
                {
                    status = 0;
                    //Added log for exception handling.
                    VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateDKTStatus(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

                }
            }
            return status;
        }
        private void checkcount()
        {
            int fkOrderId = 0;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                //var count = 
                DX.tblOrderSearchedEducationInfos.Where(rec => rec.fkOrderId == fkOrderId).Count();
            }
        }

        public int UpdateOtherReviewedReport(int pkOrderResponseId, string ReviewedNote)
        {
            int Status = 0;
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    int? pkOrderDetailId = 0;
                    if (DX.tblOrderResponseDatas.Where(db => db.pkOrderResponseId == pkOrderResponseId).Count() > 0)
                    {
                        tblOrderResponseData ObjResponse = DX.tblOrderResponseDatas.Where(db => db.pkOrderResponseId == pkOrderResponseId).FirstOrDefault();
                        if (ObjResponse != null)
                        {
                            pkOrderDetailId = ObjResponse.fkOrderDetailId;
                            ObjResponse.EmergeReviewNote = ReviewedNote;
                            DX.SubmitChanges();
                            if (pkOrderDetailId != 0)
                                UpdateReportReviewStatus(pkOrderDetailId, 2);
                            Status = 1;
                        }
                    }
                    else
                    {
                        tblOrderResponseData_Archieve ObjResponse = DX.tblOrderResponseData_Archieves.Where(db => db.pkOrderResponseId == pkOrderResponseId).FirstOrDefault();
                        if (ObjResponse != null)
                        {
                            pkOrderDetailId = ObjResponse.fkOrderDetailId;
                            ObjResponse.EmergeReviewNote = ReviewedNote;
                            DX.SubmitChanges();
                            if (pkOrderDetailId != 0)
                                UpdateReportReviewStatus(pkOrderDetailId, 2);
                            Status = 1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateOtherReviewedReport(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

            }
            return Status;
        }


        //#region Email When Report Get Reviewed
        private void SendReviewCompletedEmail(int? pkOrderId, int? pkOrderDetailId)
        {
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    Proc_GetUserInfoByReportResult ObjReviewUserInfo = DX.GetUserInfoByReport(pkOrderId, pkOrderDetailId).FirstOrDefault(); ;
                    string UserEmail = string.Empty;
                    //string ReportName = string.Empty;
                    string Subject = "";//"Your Report on " + ObjReviewUserInfo.SearchedFirstName + " " + ObjReviewUserInfo.SearchedLastName + " after review is Ready on Emerge"; //"Your Report after review is Ready";
                    if (ObjReviewUserInfo != null)
                    {
                        CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
                        TextInfo textInfo = cultureInfo.TextInfo;
                        string firstname = ObjReviewUserInfo.SearchedFirstName != null ? textInfo.ToTitleCase(ObjReviewUserInfo.SearchedFirstName) : string.Empty;
                        string lastname = ObjReviewUserInfo.SearchedLastName != null ? textInfo.ToTitleCase(ObjReviewUserInfo.SearchedLastName) : string.Empty;
                        UserEmail = ObjReviewUserInfo.UserName;
                        //string Body = CreateEmailBody(ObjReviewUserInfo);
                        //string Linkis = GetViewReportLink(ObjReviewUserInfo.UserName, ObjReviewUserInfo.pkOrderId);

                        // string WebsiteLogo = "https://www.intelifi.com/emerge/Resources/Upload/Images/" + BALGeneral.GetGeneralSettings().WebSiteLogo;
                        string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + BALGeneral.GetGeneralSettings().WebSiteLogo;

                        BALGeneral ObjBALGeneral = new BALGeneral();
                        int pkTemplateId = 43;
                        var emailContent = ObjBALGeneral.GetEmailTemplate(new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852"), pkTemplateId);//"Emerge review Report"


                        BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();

                        string emailText = emailContent.TemplateContent;

                        #region For New Style SystemTemplate
                        string Bookmarkfile = string.Empty;
                        string bookmarkfilepath = string.Empty;
                        Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                        Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                        Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                        emailText = Bookmarkfile;
                        #endregion


                        Subject = emailContent.TemplateSubject.Replace("%ApplicantName%", firstname + " " + lastname);
                        #region New way Bookmark


                        Bookmarks objBookmark = new Bookmarks();
                        string MessageBody = string.Empty;
                        objBookmark.EmailContent = emailText;
                        objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                        objBookmark.ApplicantName = "Emerge";
                        // objBookmark.ViewReportLink = Linkis;
                        objBookmark.ViewReportLink = string.Empty;
                        objBookmark.ProductName = ObjReviewUserInfo.ProductDisplayName;
                        objBookmark.ApplicantName = firstname + " " + lastname;
                        objBookmark.ReportCode = ObjReviewUserInfo.ProductCode;
                        MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Guid.Empty);

                        #endregion
                        SendEmail(UserEmail, MessageBody, Subject);
                    }
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in SendReviewCompletedEmail(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

            }
        }
        private string GetViewReportLink(string UserName, int? pkOrderId)
        {
            string ViewReportLink = "";
            //#19
            string pkOrderIdEncrypt = EncryptDecrypts.Encrypt(pkOrderId.ToString());


            string RoleName = BLLMembership.GetUserRoleByUsername(UserName);
            if (RoleName.Contains("branch"))
            {
                ViewReportLink = "<a href='https://emerge.intelifi.com/Corporate/ViewReports?_OId='" + pkOrderIdEncrypt + ">Click here</a>";
            }
            if (RoleName.Contains("basic"))
            {
                ViewReportLink = "<a href='https://emerge.intelifi.com/Corporate/ViewReports?_OId='" + pkOrderIdEncrypt + ">Click here</a>";
            }
            if (RoleName.Contains("admin"))
            {
                ViewReportLink = "<a href='https://emerge.intelifi.com/Corporate/ViewReports?_OId='" + pkOrderIdEncrypt + ">Click here</a>";
            }
            if (RoleName.Contains("corporate"))
            {
                ViewReportLink = "<a href='https://emerge.intelifi.com/Corporate/ViewReports?_OId='" + pkOrderIdEncrypt + ">Click here</a>";
            }
            return ViewReportLink;
        }
        ///// <summary>
        ///// Code To Send an Email
        ///// </summary>
        ///// <param name="OrderId"></param>
        ///// <param name="UserHtml"></param>
        ///// <param name="Username"></param>
        public bool SendEmail(string emailAddress, string EmailBody, string Subject)
        {
            int SendingStatus = 0;
            try
            {
                //MomentumInfotech.EmailProvider.MailProvider ObjMailProvider = new MomentumInfotech.EmailProvider.MailProvider();

                //List<string> BCC_List = new List<string>();
                //BCC_List.Add(BALGeneral.GetGeneralSettings().SupportEmailId);
                ////BCC_List.Add("momentum-support@hotmail.com");
                ////BCC_List.Add("support@intelifi.com");
                //ObjMailProvider.Message = EmailBody;
                //ObjMailProvider.MailTo.Add(emailAddress);
                //ObjMailProvider.MailFrom = BALGeneral.GetGeneralSettings().SupportEmailId;
                //ObjMailProvider.MailBCC = BCC_List;
                //ObjMailProvider.Subject = Subject;
                //ObjMailProvider.IsBodyHtml = true;
                //ObjMailProvider.Priority = MailPriority.High;
                //ObjMailProvider.SendMail(out SendingStatus);
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in SendEmail(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

            }

            return (SendingStatus == 1 ? true : false);
        }
        //#endregion

        public Dictionary<string, string> GetProductsForFormatting(int? OrderId, string FormattingFor)
        {
            Dictionary<string, string> ObjReviewProduct = new Dictionary<string, string>();
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    List<Proc_GetProductRequestedForReviewResult> ObjReviewRequestedProduct = DX.GetProductRequestedForReview(OrderId).ToList();
                    if (ObjReviewRequestedProduct.Count != 0)
                    {
                        for (int ICount = 0; ICount < ObjReviewRequestedProduct.Count; ICount++)
                        {
                            if (FormattingFor.ToLower() == "review")
                            {
                                ObjReviewProduct.Add(Convert.ToString(ObjReviewRequestedProduct.ElementAt(ICount).ReviewStatus) + "_" + ICount, ObjReviewRequestedProduct.ElementAt(ICount).ProductCode);
                            }
                            else if (FormattingFor.ToLower() == "liverunner")
                            {
                                ObjReviewProduct.Add(Convert.ToString(ObjReviewRequestedProduct.ElementAt(ICount).IsLiveRunner) + "_" + ICount, ObjReviewRequestedProduct.ElementAt(ICount).ProductCode);
                            }
                            else if (FormattingFor.ToLower() == "help")
                            {
                                ObjReviewProduct.Add(Convert.ToString(ObjReviewRequestedProduct.ElementAt(ICount).HelpWithOrderStatus) + "_" + ICount, ObjReviewRequestedProduct.ElementAt(ICount).ProductCode);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in GetProductsForFormatting(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

            }
            return ObjReviewProduct;

        }

        public byte GetProductReviewStatusPerLocation(int fkLocationId, int fkProductApplicationId)
        {
            byte ProductReviewStatus = 0;
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    ProductReviewStatus = DX.tblProductAccesses.Where(db => db.fkLocationId == fkLocationId && db.fkProductApplicationId == fkProductApplicationId).FirstOrDefault().ProductReviewStatus;
                }

            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in GetProductReviewStatusPerLocation(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

            }
            return ProductReviewStatus;
        }

        //#region Add Null DOB reports in NCR1

        public int AddNullDOBReport(List<int> SelectedNodes, string IsAdded, string ProductCode, int? pkOrderDetailId)
        {
            int success = 0;
            bool IsShowRawData = true;
            byte WatchListStatus = 0;
            XMLFilteration ObjXMLFilteration = new XMLFilteration();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                try
                {
                    if (DX.tblOrderResponseDatas.Where(db => db.fkOrderDetailId == pkOrderDetailId).Count() > 0)
                    {
                        tblOrderResponseData ObjResponse = DX.tblOrderResponseDatas.Where(db => db.fkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                        if (ObjResponse != null)
                        {
                            string FilteredResponse = ObjXMLFilteration.GetFilteredXML(ProductCode, ProductCode, ObjResponse.ResponseData, pkOrderDetailId, WatchListStatus, DateTime.Now, false, out IsShowRawData);
                            XDocument UpdatedXML = SelectNodeToUpdate_4NCR1(SelectedNodes, ProductCode, pkOrderDetailId, FilteredResponse, IsAdded);
                            string UpdatedResponse = Convert.ToString(UpdatedXML);
                            CompleteFinalResponse_4NCR1(ObjResponse.ResponseData, UpdatedResponse, pkOrderDetailId, ProductCode);
                            //UpdateReportReviewStatus(pkOrderDetailId, 2);

                        }
                        success = 1;
                    }
                    else
                    {
                        tblOrderResponseData_Archieve ObjResponse = DX.tblOrderResponseData_Archieves.Where(db => db.fkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                        if (ObjResponse != null)
                        {
                            string FilteredResponse = ObjXMLFilteration.GetFilteredXML(ProductCode, ProductCode, ObjResponse.ResponseData, pkOrderDetailId, WatchListStatus, DateTime.Now, false, out IsShowRawData);
                            XDocument UpdatedXML = SelectNodeToUpdate_4NCR1(SelectedNodes, ProductCode, pkOrderDetailId, FilteredResponse, IsAdded);
                            string UpdatedResponse = Convert.ToString(UpdatedXML);
                            CompleteFinalResponse_4NCR1(ObjResponse.ResponseData, UpdatedResponse, pkOrderDetailId, ProductCode);
                            //UpdateReportReviewStatus(pkOrderDetailId, 2);

                        }
                        success = 1;
                    }
                }
                catch (Exception ex)
                {
                    //Added log for exception handling.
                    VendorServices.GeneralService.WriteLog("Some Error Occured in AddNullDOBReport(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

                }
            }
            return success;
        }

        public XDocument SelectNodeToUpdate_4NCR1(List<int> SelectedNodes, string ProductCode, int? pkOrderDetailId, string ResponseData, string IsAdded)
        {
            //XmlDocument docResponse = new XmlDocument();
            XDocument ObjXDocument = new XDocument();
            switch (ProductCode.ToUpper())
            {
                case "NCR1":
                    ObjXDocument = ModifyRapidCourtReports_4NCR1(SelectedNodes, ProductCode, pkOrderDetailId, ResponseData, IsAdded);
                    break;

                default:
                    ObjXDocument = XDocument.Parse(ResponseData);
                    break;

            }
            return ObjXDocument;
        }

        public XDocument ModifyRapidCourtReports_4NCR1(List<int> SelectedNodes, string ProductCode, int? pkOrderDetailId, string ResponseData, string IsAdded)
        {
            //XmlDocument docResponse = new XmlDocument();
            XDocument ObjXDocument = new XDocument();
            string XML_Value = GetRequiredXml_ForRapidCourt(ResponseData);
            ObjXDocument = XDocument.Parse(XML_Value);
            if (ProductCode.Trim().ToUpper() == "NCR1")
            {
                foreach (int SingleNode in SelectedNodes)
                {
                    var xelement = ObjXDocument.Descendants("DemographicDetail").ElementAt(SingleNode);
                    if (xelement.Attribute("IsAdded") == null)
                    {
                        xelement.Add(new XAttribute("IsAdded", "" + IsAdded + ""));
                    }
                    else
                    {
                        xelement.Attribute("IsAdded").Value = IsAdded;
                    }
                }
            }
            return ObjXDocument;
        }

        public string CompleteFinalResponse_4NCR1(string ResponseData, string UpdatedResponse, int? pkOrderDetailId, string ProductCode)
        {
            string FinalResponse = "";
            try
            {
                if (ProductCode.ToUpper().Trim() == "NCR1")
                {
                    int TrimedUpTo = ResponseData.IndexOf("<BackgroundReportPackage>");
                    StringBuilder CompleteResponse = new StringBuilder();
                    string XMLInitials = ResponseData.Substring(0, TrimedUpTo);
                    CompleteResponse.Append(XMLInitials);
                    CompleteResponse.Append(UpdatedResponse);
                    CompleteResponse.Append("</BackgroundReports>");
                    FinalResponse = Convert.ToString(CompleteResponse);//.Replace("&lt;", "<").Replace("&gt;", ">");
                }
                else
                    FinalResponse = UpdatedResponse;
                UpdateFinalResponse_4NCR1(pkOrderDetailId, FinalResponse);
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in CompleteFinalResponse_4NCR1(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

            }
            return FinalResponse;
        }

        public void UpdateFinalResponse_4NCR1(int? pkOrderDetailId, string FinalResponse)
        {
            //try
            //{
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                if (DX.tblOrderResponseDatas.Where(db => db.fkOrderDetailId == pkOrderDetailId).Count() > 0)
                {
                    tblOrderResponseData ObjResponseData = DX.tblOrderResponseDatas.Where(db => db.fkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                    if (ObjResponseData != null)
                    {
                        ObjResponseData.ResponseData = FinalResponse;
                        DX.SubmitChanges();
                    }
                }
                else
                {
                    tblOrderResponseData_Archieve ObjResponseData = DX.tblOrderResponseData_Archieves.Where(db => db.fkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                    if (ObjResponseData != null)
                    {
                        ObjResponseData.ResponseData = FinalResponse;
                        DX.SubmitChanges();
                    }
                }
            }
            //}
            //catch
            //{
            //}
        }

        #region old match metter

        public string matchmeter(string sFirstname, string sLastname, string sMidname, string sDOB, string PSResponseData, string NCR1ResponseData, int pkOrderResponseId, string ProductCode, int OrderDetailId, int OrderId, bool IsAdmin, string spostalcode, string sregion, string smunicipality, string saddressline, bool IsAddressUpdated)
        {
            string FinalResponse = "";
            XDocument UpdatedXML = GetMatchmeterforNCR(sFirstname, sLastname, sMidname, sDOB, PSResponseData, NCR1ResponseData, OrderDetailId, OrderId, IsAdmin, spostalcode, sregion, smunicipality, saddressline, IsAddressUpdated);
            string UpdatedResponse = Convert.ToString(UpdatedXML);
            CompleteNCR1FinalResponse(NCR1ResponseData, UpdatedResponse, pkOrderResponseId, ProductCode);
            int TrimedUpTo = NCR1ResponseData.IndexOf("<BackgroundReportPackage>");
            StringBuilder CompleteResponse = new StringBuilder();
            string XMLInitials = NCR1ResponseData.Substring(0, TrimedUpTo);
            CompleteResponse.Append(XMLInitials);
            CompleteResponse.Append(UpdatedResponse);
            CompleteResponse.Append("</BackgroundReports>");
            FinalResponse = Convert.ToString(CompleteResponse);
            return FinalResponse;
        }

        public string NCR1matchmeter(string sFirstname, string sLastname, string sMidname, string sDOB, string spostalcode, string sregion, string smunicipality, string saddressline, string NCR1ResponseData, int pkOrderResponseId, string ProductCode, int OrderDetailId, int OrderId, bool IsAdmin)
        {
            string FinalResponse = "";
            XDocument UpdatedXML = GetNCRMatchmeter(sFirstname, sLastname, sMidname, sDOB, spostalcode, sregion, smunicipality, saddressline, NCR1ResponseData, OrderDetailId, OrderId, IsAdmin);
            string UpdatedResponse = Convert.ToString(UpdatedXML);
            CompleteNCR1FinalResponse(NCR1ResponseData, UpdatedResponse, pkOrderResponseId, ProductCode);
            int TrimedUpTo = NCR1ResponseData.IndexOf("<BackgroundReportPackage>");
            StringBuilder CompleteResponse = new StringBuilder();
            string XMLInitials = NCR1ResponseData.Substring(0, TrimedUpTo);
            CompleteResponse.Append(XMLInitials);
            CompleteResponse.Append(UpdatedResponse);
            CompleteResponse.Append("</BackgroundReports>");
            FinalResponse = Convert.ToString(CompleteResponse);
            return FinalResponse;
        }

        public XDocument GetNCRMatchmeter(string sFirstname, string sLastname, string sMidname, string sDOB, string spostalcode, string sregion, string smunicipality, string saddressline, string XMLNCR1resp, int OrderDetailId, int OrderId, bool IsAdmin)
        {
            XDocument ObjXDocumentupdated = new XDocument();
            XDocument ObjXDocumentNCR1 = new XDocument();
            string XML_ValueNCR1 = GetRequiredXml(XMLNCR1resp);
            ObjXDocumentNCR1 = XDocument.Parse(XML_ValueNCR1);
            try
            {
                var NCR1xelemenReport = ObjXDocumentNCR1.Descendants("CriminalReport").ToList();
                if (NCR1xelemenReport.Count > 0)
                {
                    var NCR1xelementList = NCR1xelemenReport.Descendants("CriminalCase").ToList();
                    if (NCR1xelementList.Count > 0)
                    {
                        using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
                        {
                            tblOrderDetail ObjOrderDetails = ObjEmergeDALDataContext.tblOrderDetails.Where(db => db.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                            tblOrder objtblOrder = ObjEmergeDALDataContext.tblOrders.Where(db => db.pkOrderId == OrderId).FirstOrDefault();
                            if (ObjOrderDetails.ReviewStatus == 2 && objtblOrder.IsReviewed == true)
                            {
                                for (int j = 0; j <= NCR1xelementList.Count - 1; j++)
                                {
                                    var NCR1xelement = NCR1xelementList.ElementAt(j);
                                    var FindxelementMatchMeter = NCR1xelement.Descendants("MatchMeter").ToList();
                                    if (FindxelementMatchMeter.Count > 0)
                                    {
                                        FindxelementMatchMeter.FirstOrDefault().Elements("Name").FirstOrDefault().Value = "2";
                                        FindxelementMatchMeter.FirstOrDefault().Elements("Address").FirstOrDefault().Value = "2";
                                        FindxelementMatchMeter.FirstOrDefault().Elements("DOB").FirstOrDefault().Value = "2";
                                    }
                                    else
                                    {
                                        XElement xelementMatchMeter = new XElement("MatchMeter");
                                        XElement xelementName = new XElement("Name");
                                        XElement xelementAddress = new XElement("Address");
                                        XElement xelementDOB = new XElement("DOB");
                                        NCR1xelement.Add(xelementMatchMeter);
                                        xelementMatchMeter.Add(xelementName);
                                        xelementMatchMeter.Add(xelementAddress);
                                        xelementMatchMeter.Add(xelementDOB);
                                        xelementName.Value = "2";
                                        xelementAddress.Value = "2";
                                        xelementDOB.Value = "2";
                                    }
                                }
                            }
                            else
                            {
                                int EnteredAge = 0;
                                string sYear;
                                string sMonth;
                                string sDate;
                                int[] NameMatch = new int[NCR1xelementList.Count];
                                int[] AddMatch = new int[NCR1xelementList.Count];
                                int[] DOBMatch = new int[NCR1xelementList.Count];
                                if (sDOB != null)
                                {
                                    string[] psdob = sDOB.Split('/');
                                    sYear = psdob[2];
                                    sMonth = psdob[0];
                                    sDate = psdob[1];
                                    DateTime sFullDate = Convert.ToDateTime(sMonth + "/" + sDate + "/" + sYear);
                                    EnteredAge = DateTime.Now.Year - sFullDate.Year;
                                    if (DateTime.Now.DayOfYear < sFullDate.DayOfYear)
                                    {
                                        EnteredAge = EnteredAge - 1;
                                    }
                                }
                                else
                                {
                                    sYear = string.Empty;
                                    sMonth = string.Empty;
                                    sDate = string.Empty;
                                }
                                for (int j = 0; j <= NCR1xelementList.Count - 1; j++)
                                {
                                    bool IsmatchGName = false;
                                    bool IsmatchMName = false;
                                    bool IsmatchFName = false;
                                    bool IsmatchPostCode = false;
                                    bool IsmatchRegion = false;
                                    bool IsmatchMunc = false;
                                    bool IsmatchAddressLine = false;
                                    bool IsmatchYear = false;
                                    bool IsmatchMonth = false;
                                    bool IsmatchDate = false;
                                    bool IsAddressEmpty = false;
                                    string ncr1givenname;
                                    string ncr1middlename;
                                    string ncr1familyname;
                                    string ncr1postalcode;
                                    string ncr1region;
                                    string ncr1municipality;
                                    string ncr1deliveryaddress;
                                    string ncr1Year;
                                    string ncr1Month;
                                    string ncr1Date;
                                    int ncr1Age = -1;
                                    var NCR1xelement = NCR1xelementList.ElementAt(j);
                                    var NCR1PersonNameList = NCR1xelement.Descendants("PersonName").ToList();
                                    if (NCR1PersonNameList.Count > 0)
                                    {
                                        if (NCR1PersonNameList.Count == 1)
                                        {
                                            for (int k = 0; k <= NCR1PersonNameList.Count - 1; k++)
                                            {
                                                var NCR1PersonName = NCR1PersonNameList.ElementAt(k); //NCR1xelement.Descendants("PersonName").FirstOrDefault();
                                                if (NCR1PersonName != null)
                                                {
                                                    var NCR1GivenName = NCR1PersonName.Descendants("GivenName").FirstOrDefault();
                                                    if (NCR1GivenName != null)
                                                    {
                                                        ncr1givenname = NCR1GivenName.Value.ToString();
                                                    }
                                                    else
                                                    {
                                                        ncr1givenname = string.Empty;
                                                    }
                                                    var NCR1MiddleName = NCR1PersonName.Descendants("MiddleName").FirstOrDefault();
                                                    if (NCR1MiddleName != null)
                                                    {
                                                        ncr1middlename = NCR1MiddleName.Value.ToString();
                                                    }
                                                    else
                                                    {
                                                        ncr1middlename = string.Empty;
                                                    }
                                                    var NCR1FamilyName = NCR1PersonName.Descendants("FamilyName").FirstOrDefault();
                                                    if (NCR1FamilyName != null)
                                                    {
                                                        ncr1familyname = NCR1FamilyName.Value.ToString();
                                                    }
                                                    else
                                                    {
                                                        ncr1familyname = string.Empty;
                                                    }
                                                }
                                                else
                                                {
                                                    ncr1givenname = string.Empty;
                                                    ncr1middlename = string.Empty;
                                                    ncr1familyname = string.Empty;
                                                }

                                                if (sFirstname.ToLower() == ncr1givenname.ToLower())
                                                {
                                                    IsmatchGName = true;
                                                }
                                                if (sMidname.Length == 1 && ncr1middlename.Length > 0)
                                                {
                                                    if (sMidname.ToLower() == ncr1middlename.ToLower().Substring(0, 1))
                                                    {
                                                        IsmatchMName = true;
                                                    }
                                                }
                                                else
                                                {
                                                    if (sMidname.ToLower() == ncr1middlename.ToLower())
                                                    {
                                                        IsmatchMName = true;
                                                    }
                                                }
                                                if (sLastname.ToLower() == ncr1familyname.ToLower())
                                                {
                                                    IsmatchFName = true;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            for (int k = 0; k <= NCR1PersonNameList.Count - 1; k++)
                                            {
                                                var NCR1PersonName = NCR1PersonNameList.ElementAt(k); //NCR1xelement.Descendants("PersonName").FirstOrDefault();
                                                if (NCR1PersonName != null)
                                                {
                                                    var NCR1GivenName = NCR1PersonName.Descendants("GivenName").FirstOrDefault();
                                                    if (NCR1GivenName != null)
                                                    {
                                                        ncr1givenname = NCR1GivenName.Value.ToString();
                                                    }
                                                    else
                                                    {
                                                        ncr1givenname = string.Empty;
                                                    }
                                                    var NCR1MiddleName = NCR1PersonName.Descendants("MiddleName").FirstOrDefault();
                                                    if (NCR1MiddleName != null)
                                                    {
                                                        ncr1middlename = string.Empty; //NCR1MiddleName.Value.ToString();#216
                                                    }
                                                    else
                                                    {
                                                        ncr1middlename = string.Empty;
                                                    }
                                                    var NCR1FamilyName = NCR1PersonName.Descendants("FamilyName").FirstOrDefault();
                                                    if (NCR1FamilyName != null)
                                                    {
                                                        ncr1familyname = NCR1FamilyName.Value.ToString();
                                                    }
                                                    else
                                                    {
                                                        ncr1familyname = string.Empty;
                                                    }
                                                }
                                                else
                                                {
                                                    ncr1givenname = string.Empty;
                                                    ncr1middlename = string.Empty;
                                                    ncr1familyname = string.Empty;
                                                }

                                                if (sFirstname.ToLower() == ncr1givenname.ToLower())
                                                {
                                                    IsmatchGName = true;
                                                }
                                                if (sMidname.Length == 1 && ncr1middlename.Length > 0)
                                                {
                                                    if (sMidname.ToLower() == ncr1middlename.ToLower().Substring(0, 1))
                                                    {
                                                        IsmatchMName = true;
                                                    }
                                                }
                                                else
                                                {
                                                    if (sMidname.ToLower() == ncr1middlename.ToLower())
                                                    {
                                                        IsmatchMName = true;
                                                    }
                                                }
                                                if (sLastname.ToLower() == ncr1familyname.ToLower())
                                                {
                                                    IsmatchFName = true;
                                                }
                                            }
                                        }
                                    }
                                    var NCR1PostalAddress = NCR1xelement.Descendants("PostalAddress").FirstOrDefault();
                                    if (NCR1PostalAddress != null)
                                    {
                                        var NCR1PostalCode = NCR1PostalAddress.Descendants("PostalCode").FirstOrDefault();
                                        if (NCR1PostalCode != null)
                                        {
                                            if (NCR1PostalCode.Value.Length > 5)
                                            {
                                                ncr1postalcode = NCR1PostalCode.Value.ToString().Substring(0, 5);
                                            }
                                            else
                                            {
                                                ncr1postalcode = NCR1PostalCode.Value.ToString();
                                            }
                                        }
                                        else
                                        {
                                            ncr1postalcode = string.Empty;
                                        }
                                        var NCR1Region = NCR1PostalAddress.Descendants("Region").FirstOrDefault();
                                        if (NCR1Region != null)
                                        {
                                            ncr1region = NCR1Region.Value.ToString();
                                        }
                                        else
                                        {
                                            ncr1region = string.Empty;
                                        }
                                        var NCR1Municipality = NCR1PostalAddress.Descendants("Municipality").FirstOrDefault();
                                        if (NCR1Municipality != null)
                                        {
                                            ncr1municipality = NCR1Municipality.Value.ToString();
                                        }
                                        else
                                        {
                                            ncr1municipality = string.Empty;
                                        }
                                        var NCR1DeliveryAddress = NCR1PostalAddress.Descendants("DeliveryAddress").FirstOrDefault();
                                        if (NCR1DeliveryAddress != null)
                                        {
                                            ncr1deliveryaddress = NCR1DeliveryAddress.Value.ToString();
                                        }
                                        else
                                        {
                                            ncr1deliveryaddress = string.Empty;
                                        }
                                        if (ncr1postalcode == string.Empty && ncr1region == string.Empty && ncr1municipality == string.Empty && ncr1deliveryaddress == string.Empty)
                                        {
                                            IsAddressEmpty = true;
                                        }
                                    }
                                    else
                                    {
                                        ncr1postalcode = string.Empty;
                                        ncr1region = string.Empty;
                                        ncr1municipality = string.Empty;
                                        ncr1deliveryaddress = string.Empty;
                                        IsAddressEmpty = true;
                                    }
                                    var NCR1DemographicDetail = NCR1xelement.Descendants("DemographicDetail").FirstOrDefault();
                                    if (NCR1DemographicDetail != null)
                                    {
                                        var NCR1DateOfBirth = NCR1DemographicDetail.Descendants("DateOfBirth").FirstOrDefault();
                                        if (NCR1DateOfBirth != null)
                                        {
                                            string[] ncr1dob = NCR1DateOfBirth.Value.Split('-');
                                            ncr1Year = ncr1dob[0];
                                            ncr1Month = ncr1dob[1];
                                            ncr1Date = ncr1dob[2].Substring(0, 2);
                                            ncr1Age = -1;
                                        }
                                        else
                                        {
                                            ncr1Year = string.Empty;
                                            ncr1Month = string.Empty;
                                            ncr1Date = string.Empty;
                                            ncr1Age = 0;
                                            var NCR1YearOfBirth = NCR1DemographicDetail.Descendants("Other").FirstOrDefault();
                                            if (NCR1YearOfBirth != null)
                                            {
                                                if (NCR1YearOfBirth.HasAttributes && NCR1YearOfBirth.Attribute("type").Value == "Year of Birth")
                                                {
                                                    ncr1Year = NCR1YearOfBirth.Value;
                                                }
                                            }
                                            var NCR1Age = NCR1DemographicDetail.Descendants("Age").FirstOrDefault();
                                            if (NCR1Age != null)
                                            {
                                                ncr1Age = Convert.ToInt32(NCR1Age.Value);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ncr1Year = string.Empty;
                                        ncr1Month = string.Empty;
                                        ncr1Date = string.Empty;
                                        ncr1Age = -1;
                                    }

                                    //if (sFirstname.ToLower() == ncr1givenname.ToLower())
                                    //{
                                    //    IsmatchGName = true;
                                    //}
                                    //if (sMidname.Length == 1 && ncr1middlename.Length > 0)
                                    //{
                                    //    if (sMidname.ToLower() == ncr1middlename.ToLower().Substring(0, 1))
                                    //    {
                                    //        IsmatchMName = true;
                                    //    }
                                    //}
                                    //else
                                    //{
                                    //    if (sMidname.ToLower() == ncr1middlename.ToLower())
                                    //    {
                                    //        IsmatchMName = true;
                                    //    }
                                    //}
                                    //if (sLastname.ToLower() == ncr1familyname.ToLower())
                                    //{
                                    //    IsmatchFName = true;
                                    //}
                                    if (spostalcode == ncr1postalcode)
                                    {
                                        IsmatchPostCode = true;
                                    }
                                    if (sregion == ncr1region)
                                    {
                                        IsmatchRegion = true;
                                    }
                                    if (smunicipality == ncr1municipality)
                                    {
                                        IsmatchMunc = true;
                                    }
                                    if (ncr1deliveryaddress.ToLower().Contains(saddressline.ToLower()) && saddressline.ToLower() != " ")
                                    {
                                        IsmatchAddressLine = true;
                                    }
                                    if (sYear == ncr1Year)
                                    {
                                        IsmatchYear = true;
                                    }
                                    if (sMonth == ncr1Month)
                                    {
                                        IsmatchMonth = true;
                                    }
                                    if (sDate == ncr1Date)
                                    {
                                        IsmatchDate = true;
                                    }
                                    if (IsmatchGName == true && IsmatchMName == true && IsmatchFName == true)
                                    {
                                        NameMatch[j] = 2;
                                    }
                                    else if ((IsmatchGName == true && IsmatchMName == true) || (IsmatchGName == true && IsmatchFName == true) || (IsmatchMName == true && IsmatchFName == true))
                                    {
                                        NameMatch[j] = 1;
                                    }
                                    else
                                    {
                                        NameMatch[j] = 0;
                                    }
                                    if (IsmatchPostCode == true && IsmatchRegion == true && IsmatchMunc == true && IsmatchAddressLine == true)
                                    {
                                        AddMatch[j] = 2;
                                    }
                                    else if (IsmatchPostCode == true || IsmatchRegion == true || IsmatchMunc == true || IsmatchAddressLine == true)
                                    {
                                        AddMatch[j] = 1;
                                    }
                                    else
                                    {
                                        AddMatch[j] = 0;
                                    }
                                    if (IsmatchYear == true && IsmatchMonth == true && IsmatchDate == true)
                                    {
                                        DOBMatch[j] = 2;
                                    }
                                    else if (IsmatchYear == true || IsmatchMonth == true || IsmatchDate == true || EnteredAge == ncr1Age)
                                    {
                                        DOBMatch[j] = 1;
                                    }
                                    else
                                    {
                                        DOBMatch[j] = 0;
                                    }

                                    var FindxelementMatchMeter = NCR1xelement.Descendants("MatchMeter").ToList();


                                    if (FindxelementMatchMeter.Count > 0)
                                    {
                                        if (Convert.ToInt32(FindxelementMatchMeter.FirstOrDefault().Elements("Name").FirstOrDefault().Value) < NameMatch[j])
                                        {
                                            FindxelementMatchMeter.FirstOrDefault().Elements("Name").FirstOrDefault().Value = NameMatch[j].ToString();
                                        }
                                        if (Convert.ToInt32(FindxelementMatchMeter.FirstOrDefault().Elements("Address").FirstOrDefault().Value) < AddMatch[j] && IsAddressEmpty == false)
                                        {
                                            FindxelementMatchMeter.FirstOrDefault().Elements("Address").FirstOrDefault().Value = AddMatch[j].ToString();
                                        }
                                        if (Convert.ToInt32(FindxelementMatchMeter.FirstOrDefault().Elements("DOB").FirstOrDefault().Value) < DOBMatch[j])
                                        {
                                            FindxelementMatchMeter.FirstOrDefault().Elements("DOB").FirstOrDefault().Value = DOBMatch[j].ToString();
                                        }
                                    }
                                    else
                                    {
                                        XElement xelementMatchMeter = new XElement("MatchMeter");
                                        XElement xelementName = new XElement("Name");
                                        XElement xelementAddress = new XElement("Address");
                                        XElement xelementDOB = new XElement("DOB");
                                        NCR1xelement.Add(xelementMatchMeter);
                                        xelementMatchMeter.Add(xelementName);
                                        xelementMatchMeter.Add(xelementAddress);
                                        xelementMatchMeter.Add(xelementDOB);
                                        xelementName.Value = NameMatch[j].ToString();
                                        if (IsAddressEmpty == false)
                                        {
                                            xelementAddress.Value = AddMatch[j].ToString();
                                        }
                                        else
                                        {
                                            xelementAddress.Value = "-1";
                                        }
                                        xelementDOB.Value = DOBMatch[j].ToString();

                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ObjXDocumentupdated = XDocument.Parse(XML_ValueNCR1);
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in GetNCRMatchmeter(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

            }

            ObjXDocumentupdated = ObjXDocumentNCR1;
            return ObjXDocumentupdated;
        }

        public XDocument GetMatchmeterforNCR(string sFirstname, string sLastname, string sMidname, string sDOB, string XMLPSresp, string XMLNCR1resp, int OrderDetailId, int OrderId, bool IsAdmin, string spostalcode, string sregion, string smunicipality, string saddressline, bool IsAddressUpdated)
        {
            XDocument ObjXDocumentupdated = new XDocument();
            XDocument ObjXDocumentPS = new XDocument();
            XDocument ObjXDocumentNCR1 = new XDocument();
            string XML_ValuePS = GetRequiredXml(XMLPSresp);
            string XML_ValueNCR1 = GetRequiredXml(XMLNCR1resp);
            ObjXDocumentPS = XDocument.Parse(XML_ValuePS);
            ObjXDocumentNCR1 = XDocument.Parse(XML_ValueNCR1);
            try
            {
                var PSxelementCredit = ObjXDocumentPS.Descendants("CreditFile").ToList();
                if (PSxelementCredit.Count > 0)
                {
                    var PSxelementList = PSxelementCredit.Descendants("PersonalData").ToList();
                    if (PSxelementList.Count > 0)
                    {
                        using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
                        {
                            tblOrderDetail ObjOrderDetails = ObjEmergeDALDataContext.tblOrderDetails.Where(db => db.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                            tblOrder objtblOrder = ObjEmergeDALDataContext.tblOrders.Where(db => db.pkOrderId == OrderId).FirstOrDefault();
                            var NCR1xelemenReport = ObjXDocumentNCR1.Descendants("CriminalReport").ToList();
                            if (NCR1xelemenReport.Count > 0)
                            {
                                var NCR1xelementList = NCR1xelemenReport.Descendants("CriminalCase").ToList();
                                if (NCR1xelementList.Count > 0)
                                {
                                    if (ObjOrderDetails.ReviewStatus == 2 && objtblOrder.IsReviewed == true)
                                    {
                                        for (int j = 0; j <= NCR1xelementList.Count - 1; j++)
                                        {
                                            var NCR1xelement = NCR1xelementList.ElementAt(j);
                                            var FindxelementMatchMeter = NCR1xelement.Descendants("MatchMeter").ToList();
                                            if (FindxelementMatchMeter.Count > 0)
                                            {
                                                FindxelementMatchMeter.FirstOrDefault().Elements("Name").FirstOrDefault().Value = "2";
                                                FindxelementMatchMeter.FirstOrDefault().Elements("Address").FirstOrDefault().Value = "2";
                                                FindxelementMatchMeter.FirstOrDefault().Elements("DOB").FirstOrDefault().Value = "2";
                                            }
                                            else
                                            {
                                                XElement xelementMatchMeter = new XElement("MatchMeter");
                                                XElement xelementName = new XElement("Name");
                                                XElement xelementAddress = new XElement("Address");
                                                XElement xelementDOB = new XElement("DOB");
                                                NCR1xelement.Add(xelementMatchMeter);
                                                xelementMatchMeter.Add(xelementName);
                                                xelementMatchMeter.Add(xelementAddress);
                                                xelementMatchMeter.Add(xelementDOB);
                                                xelementName.Value = "2";
                                                xelementAddress.Value = "2";
                                                xelementDOB.Value = "2";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        int ActualEnteredAge = 0;
                                        string sYear;
                                        string sMonth;
                                        string sDate;
                                        if (sDOB != null)
                                        {
                                            string[] psdob = sDOB.Split('/');
                                            sYear = psdob[2];
                                            sMonth = psdob[0];
                                            sDate = psdob[1];
                                            DateTime sFullDate = Convert.ToDateTime(sMonth + "/" + sDate + "/" + sYear);
                                            ActualEnteredAge = DateTime.Now.Year - sFullDate.Year;
                                            if (DateTime.Now.DayOfYear < sFullDate.DayOfYear)
                                            {
                                                ActualEnteredAge = ActualEnteredAge - 1;
                                            }
                                        }
                                        else
                                        {
                                            sYear = string.Empty;
                                            sMonth = string.Empty;
                                            sDate = string.Empty;
                                        }
                                        for (int i = 0; i <= PSxelementList.Count - 1; i++)
                                        {
                                            string psgivenname;
                                            string psmiddlename;
                                            string psfamilyname;
                                            string pspostalcode;
                                            string psregion;
                                            string psmunicipality;
                                            string psaddressline;
                                            string psYear;
                                            string psMonth;
                                            string psDate;
                                            int EnteredAge = 0;
                                            int[] NameMatch = new int[NCR1xelementList.Count];
                                            int[] AddMatch = new int[NCR1xelementList.Count];
                                            int[] DOBMatch = new int[NCR1xelementList.Count];
                                            var PSxelement = PSxelementList.ElementAt(i);
                                            var PSPersonName = PSxelement.Descendants("PersonName").FirstOrDefault();
                                            if (PSPersonName != null)
                                            {
                                                var PSGivenName = PSPersonName.Descendants("GivenName").FirstOrDefault();
                                                if (PSGivenName != null)
                                                {
                                                    psgivenname = PSGivenName.Value.ToString();
                                                }
                                                else
                                                {
                                                    psgivenname = string.Empty;
                                                }
                                                var PSMiddleName = PSPersonName.Descendants("MiddleName").FirstOrDefault();
                                                if (PSMiddleName != null)
                                                {
                                                    psmiddlename = PSMiddleName.Value.ToString();
                                                }
                                                else
                                                {
                                                    psmiddlename = string.Empty;
                                                }
                                                var PSFamilyName = PSPersonName.Descendants("FamilyName").FirstOrDefault();
                                                if (PSFamilyName != null)
                                                {
                                                    psfamilyname = PSFamilyName.Value.ToString();
                                                }
                                                else
                                                {
                                                    psfamilyname = string.Empty;
                                                }
                                            }
                                            else
                                            {
                                                psgivenname = string.Empty;
                                                psmiddlename = string.Empty;
                                                psfamilyname = string.Empty;
                                            }

                                            var PSPostalAddress = PSxelement.Descendants("PostalAddress").FirstOrDefault();
                                            if (PSPostalAddress != null)
                                            {
                                                var PSPostalCode = PSPostalAddress.Descendants("PostalCode").FirstOrDefault();
                                                if (PSPostalCode != null)
                                                {
                                                    if (PSPostalCode.Value.Length > 5)
                                                    {
                                                        pspostalcode = PSPostalCode.Value.ToString().Substring(0, 5);
                                                    }
                                                    else
                                                    {
                                                        pspostalcode = PSPostalCode.Value.ToString();
                                                    }
                                                }
                                                else
                                                {
                                                    pspostalcode = string.Empty;
                                                }
                                                var PSRegion = PSPostalAddress.Descendants("Region").FirstOrDefault();
                                                if (PSRegion != null)
                                                {
                                                    psregion = PSRegion.Value.ToString();
                                                }
                                                else
                                                {
                                                    psregion = string.Empty;
                                                }
                                                var PSMunicipality = PSPostalAddress.Descendants("Municipality").FirstOrDefault();
                                                if (PSMunicipality != null)
                                                {
                                                    psmunicipality = PSMunicipality.Value.ToString();
                                                }
                                                else
                                                {
                                                    psmunicipality = string.Empty;
                                                }
                                                var PSDeliveryAddress = PSPostalAddress.Descendants("DeliveryAddress").FirstOrDefault();
                                                if (PSDeliveryAddress != null)
                                                {
                                                    var PSAddressLine = PSDeliveryAddress.Descendants("AddressLine").FirstOrDefault();
                                                    if (PSAddressLine != null)
                                                    {
                                                        psaddressline = PSAddressLine.Value.ToString();
                                                    }
                                                    else
                                                    {
                                                        psaddressline = string.Empty;
                                                    }
                                                }
                                                else
                                                {
                                                    psaddressline = string.Empty;
                                                }
                                            }
                                            else
                                            {
                                                pspostalcode = string.Empty;
                                                psregion = string.Empty;
                                                psmunicipality = string.Empty;
                                                psaddressline = string.Empty;
                                            }
                                            var PsDemographicDetail = PSxelement.Descendants("DemographicDetail").FirstOrDefault();
                                            if (PsDemographicDetail != null)
                                            {
                                                var PSDateOfBirth = PsDemographicDetail.Descendants("DateOfBirth").FirstOrDefault();
                                                if (PSDateOfBirth != null)
                                                {
                                                    string[] psdob = PSDateOfBirth.Value.Split('-');
                                                    psYear = psdob[0];
                                                    psMonth = psdob[1];
                                                    psDate = psdob[2];
                                                    if (psDate != string.Empty && psYear != string.Empty)
                                                    {
                                                        DateTime psFullDate = Convert.ToDateTime(psMonth + "/" + psDate + "/" + psYear);
                                                        EnteredAge = DateTime.Now.Year - psFullDate.Year;
                                                        if (DateTime.Now.DayOfYear < psFullDate.DayOfYear)
                                                        {
                                                            EnteredAge = EnteredAge - 1;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    psYear = string.Empty;
                                                    psMonth = string.Empty;
                                                    psDate = string.Empty;
                                                }
                                            }
                                            else
                                            {
                                                psYear = string.Empty;
                                                psMonth = string.Empty;
                                                psDate = string.Empty;
                                            }
                                            for (int j = 0; j <= NCR1xelementList.Count - 1; j++)
                                            {
                                                bool IsmatchGName = false;
                                                bool IsmatchMName = false;
                                                bool IsmatchFName = false;
                                                bool IsmatchPostCode = false;
                                                bool IsmatchRegion = false;
                                                bool Ismatch_PostCode = false;
                                                bool Ismatch_Region = false;
                                                bool Ismatch_AddressLine = false;
                                                bool Ismatch_Munc = false;
                                                bool IsmatchMunc = false;
                                                bool IsmatchAddressLine = false;
                                                bool IsmatchYear = false;
                                                bool IsmatchMonth = false;
                                                bool IsmatchDate = false;
                                                bool IsAddressEmpty = false;
                                                string ncr1givenname = string.Empty;
                                                string ncr1middlename = string.Empty;
                                                string ncr1familyname = string.Empty;
                                                string ncr1postalcode;
                                                string ncr1region;
                                                string ncr1municipality;
                                                string ncr1deliveryaddress;
                                                string ncr1Year;
                                                string ncr1Month;
                                                string ncr1Date;
                                                int ncr1Age = -1;
                                                var NCR1xelement = NCR1xelementList.ElementAt(j);
                                                var NCR1PersonNameList = NCR1xelement.Descendants("PersonName").ToList();
                                                if (NCR1PersonNameList.Count > 0)
                                                {
                                                    if (NCR1PersonNameList.Count == 1)
                                                    {
                                                        for (int k = 0; k <= NCR1PersonNameList.Count - 1; k++)
                                                        {
                                                            var NCR1PersonName = NCR1PersonNameList.ElementAt(k); //NCR1xelement.Descendants("PersonName").FirstOrDefault();
                                                            if (NCR1PersonName != null)
                                                            {
                                                                var NCR1GivenName = NCR1PersonName.Descendants("GivenName").FirstOrDefault();
                                                                if (NCR1GivenName != null)
                                                                {
                                                                    ncr1givenname = NCR1GivenName.Value.ToString();
                                                                }
                                                                else
                                                                {
                                                                    ncr1givenname = string.Empty;
                                                                }
                                                                var NCR1MiddleName = NCR1PersonName.Descendants("MiddleName").FirstOrDefault();
                                                                if (NCR1MiddleName != null)
                                                                {
                                                                    ncr1middlename = NCR1MiddleName.Value.ToString();
                                                                }
                                                                else
                                                                {
                                                                    ncr1middlename = string.Empty;
                                                                }
                                                                var NCR1FamilyName = NCR1PersonName.Descendants("FamilyName").FirstOrDefault();
                                                                if (NCR1FamilyName != null)
                                                                {
                                                                    ncr1familyname = NCR1FamilyName.Value.ToString();
                                                                }
                                                                else
                                                                {
                                                                    ncr1familyname = string.Empty;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                ncr1givenname = string.Empty;
                                                                ncr1middlename = string.Empty;
                                                                ncr1familyname = string.Empty;
                                                            }
                                                            if (psgivenname == ncr1givenname)
                                                            {
                                                                IsmatchGName = true;
                                                            }
                                                            if (sMidname.Length == 1 && ncr1middlename.Length > 0 && psmiddlename.Length > 0)
                                                            {
                                                                if (sMidname.ToLower() == ncr1middlename.ToLower().Substring(0, 1) && ncr1middlename.ToLower().Substring(0, 1) == psmiddlename.ToLower().Substring(0, 1))
                                                                {
                                                                    IsmatchMName = true;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (psmiddlename == ncr1middlename)
                                                                {
                                                                    IsmatchMName = true;
                                                                }
                                                            }
                                                            if (psfamilyname == ncr1familyname)
                                                            {
                                                                IsmatchFName = true;
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        for (int k = 0; k <= NCR1PersonNameList.Count - 1; k++)
                                                        {
                                                            var NCR1PersonName = NCR1PersonNameList.ElementAt(k); //NCR1xelement.Descendants("PersonName").FirstOrDefault();
                                                            if (NCR1PersonName != null)
                                                            {
                                                                var NCR1GivenName = NCR1PersonName.Descendants("GivenName").FirstOrDefault();
                                                                if (NCR1GivenName != null)
                                                                {
                                                                    ncr1givenname = NCR1GivenName.Value.ToString();
                                                                }
                                                                else
                                                                {
                                                                    ncr1givenname = string.Empty;
                                                                }
                                                                var NCR1MiddleName = NCR1PersonName.Descendants("MiddleName").FirstOrDefault();
                                                                if (NCR1MiddleName != null)
                                                                {
                                                                    ncr1middlename = string.Empty; //NCR1MiddleName.Value.ToString();#216
                                                                }
                                                                else
                                                                {
                                                                    ncr1middlename = string.Empty;
                                                                }
                                                                var NCR1FamilyName = NCR1PersonName.Descendants("FamilyName").FirstOrDefault();
                                                                if (NCR1FamilyName != null)
                                                                {
                                                                    ncr1familyname = NCR1FamilyName.Value.ToString();
                                                                }
                                                                else
                                                                {
                                                                    ncr1familyname = string.Empty;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                ncr1givenname = string.Empty;
                                                                ncr1middlename = string.Empty;
                                                                ncr1familyname = string.Empty;
                                                            }
                                                            if (psgivenname == ncr1givenname)
                                                            {
                                                                IsmatchGName = true;
                                                            }
                                                            if (sMidname.Length == 1 && ncr1middlename.Length > 0 && psmiddlename.Length > 0)
                                                            {
                                                                if (sMidname.ToLower() == ncr1middlename.ToLower().Substring(0, 1) && ncr1middlename.ToLower().Substring(0, 1) == psmiddlename.ToLower().Substring(0, 1))
                                                                {
                                                                    IsmatchMName = true;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (psmiddlename == ncr1middlename)
                                                                {
                                                                    IsmatchMName = true;
                                                                }
                                                            }
                                                            if (psfamilyname == ncr1familyname)
                                                            {
                                                                IsmatchFName = true;
                                                            }
                                                        }
                                                    }
                                                }

                                                var NCR1PostalAddress = NCR1xelement.Descendants("PostalAddress").FirstOrDefault();
                                                if (NCR1PostalAddress != null)
                                                {
                                                    var NCR1PostalCode = NCR1PostalAddress.Descendants("PostalCode").FirstOrDefault();
                                                    if (NCR1PostalCode != null)
                                                    {
                                                        if (NCR1PostalCode.Value.Length > 5)
                                                        {
                                                            ncr1postalcode = NCR1PostalCode.Value.ToString().Substring(0, 5);
                                                        }
                                                        else
                                                        {
                                                            ncr1postalcode = NCR1PostalCode.Value.ToString();
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ncr1postalcode = string.Empty;
                                                    }
                                                    var NCR1Region = NCR1PostalAddress.Descendants("Region").FirstOrDefault();
                                                    if (NCR1Region != null)
                                                    {
                                                        ncr1region = NCR1Region.Value.ToString();
                                                    }
                                                    else
                                                    {
                                                        ncr1region = string.Empty;
                                                    }
                                                    var NCR1Municipality = NCR1PostalAddress.Descendants("Municipality").FirstOrDefault();
                                                    if (NCR1Municipality != null)
                                                    {
                                                        ncr1municipality = NCR1Municipality.Value.ToString();
                                                    }
                                                    else
                                                    {
                                                        ncr1municipality = string.Empty;
                                                    }
                                                    var NCR1DeliveryAddress = NCR1PostalAddress.Descendants("DeliveryAddress").FirstOrDefault();
                                                    if (NCR1DeliveryAddress != null)
                                                    {
                                                        ncr1deliveryaddress = NCR1DeliveryAddress.Value.ToString();
                                                    }
                                                    else
                                                    {
                                                        ncr1deliveryaddress = string.Empty;
                                                    }
                                                    if (ncr1postalcode == string.Empty && ncr1region == string.Empty && ncr1municipality == string.Empty && ncr1deliveryaddress == string.Empty)
                                                    {
                                                        IsAddressEmpty = true;
                                                    }
                                                }
                                                else
                                                {
                                                    ncr1postalcode = string.Empty;
                                                    ncr1region = string.Empty;
                                                    ncr1municipality = string.Empty;
                                                    ncr1deliveryaddress = string.Empty;
                                                    IsAddressEmpty = true;
                                                }
                                                var NCR1DemographicDetail = NCR1xelement.Descendants("DemographicDetail").FirstOrDefault();
                                                if (NCR1DemographicDetail != null)
                                                {
                                                    var NCR1DateOfBirth = NCR1DemographicDetail.Descendants("DateOfBirth").FirstOrDefault();
                                                    if (NCR1DateOfBirth != null)
                                                    {
                                                        string[] ncr1dob = NCR1DateOfBirth.Value.Split('-');
                                                        ncr1Year = ncr1dob[0];
                                                        ncr1Month = ncr1dob[1];
                                                        ncr1Date = ncr1dob[2].Substring(0, 2);
                                                    }
                                                    else
                                                    {
                                                        ncr1Year = string.Empty;
                                                        ncr1Month = string.Empty;
                                                        ncr1Date = string.Empty;
                                                        ncr1Age = 0;
                                                        var NCR1YearOfBirth = NCR1DemographicDetail.Descendants("Other").FirstOrDefault();
                                                        if (NCR1YearOfBirth != null)
                                                        {
                                                            if (NCR1YearOfBirth.HasAttributes && NCR1YearOfBirth.Attribute("type").Value == "Year of Birth")
                                                            {
                                                                ncr1Year = NCR1YearOfBirth.Value;
                                                            }
                                                        }
                                                        var NCR1Age = NCR1DemographicDetail.Descendants("Age").FirstOrDefault();
                                                        if (NCR1Age != null)
                                                        {
                                                            ncr1Age = Convert.ToInt32(NCR1Age.Value);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    ncr1Year = string.Empty;
                                                    ncr1Month = string.Empty;
                                                    ncr1Date = string.Empty;
                                                    ncr1Age = -1;
                                                }

                                                //if (pspostalcode == ncr1postalcode)
                                                //{
                                                if (string.IsNullOrEmpty(spostalcode) == false && IsAddressUpdated == true)
                                                {
                                                    if (spostalcode.Length > 5)
                                                    {
                                                        spostalcode = spostalcode.ToString().Substring(0, 5);
                                                    }

                                                    if (spostalcode.ToLower() == ncr1postalcode.ToLower())
                                                    {
                                                        IsmatchPostCode = true;
                                                        Ismatch_PostCode = false;
                                                    }
                                                }
                                                else if (pspostalcode == ncr1postalcode)
                                                {
                                                    IsmatchPostCode = false;
                                                    Ismatch_PostCode = true;
                                                }

                                                //}
                                                //if (psregion == ncr1region)
                                                //{
                                                if (string.IsNullOrEmpty(sregion) == false && IsAddressUpdated == true)
                                                {
                                                    if (sregion.ToLower() == ncr1region.ToLower())
                                                    {
                                                        IsmatchRegion = true;
                                                        Ismatch_Region = false;
                                                    }
                                                }
                                                else if (psregion == ncr1region)
                                                {
                                                    IsmatchRegion = false;
                                                    Ismatch_Region = true;
                                                }
                                                //}
                                                //if (psmunicipality == ncr1municipality)
                                                //{
                                                if (string.IsNullOrEmpty(smunicipality) == false && IsAddressUpdated == true)
                                                {
                                                    if (smunicipality.ToLower() == ncr1municipality.ToLower())
                                                    {
                                                        IsmatchMunc = true;
                                                        Ismatch_Munc = false;
                                                    }
                                                }
                                                else if (psmunicipality == ncr1municipality)
                                                {
                                                    IsmatchMunc = false;
                                                    Ismatch_Munc = true;
                                                }

                                                //}
                                                //if (ncr1deliveryaddress.ToLower().Contains(psaddressline.ToLower()))
                                                //{
                                                if (string.IsNullOrEmpty(saddressline) == false && IsAddressUpdated == true)
                                                {
                                                    if (ncr1deliveryaddress.ToLower().Contains(saddressline.ToLower()))
                                                    {
                                                        IsmatchAddressLine = true;
                                                        Ismatch_AddressLine = false;
                                                    }
                                                }
                                                else if (ncr1deliveryaddress.ToLower().Contains(psaddressline.ToLower()))
                                                {
                                                    IsmatchAddressLine = false;
                                                    Ismatch_AddressLine = true;
                                                }
                                                //}
                                                if (psYear != string.Empty)
                                                {
                                                    if (psYear == ncr1Year)
                                                    {
                                                        IsmatchYear = true;
                                                    }
                                                }
                                                if (psMonth != string.Empty)
                                                {
                                                    if (psMonth == ncr1Month)
                                                    {
                                                        IsmatchMonth = true;
                                                    }
                                                }
                                                if (psDate != string.Empty)
                                                {
                                                    if (psDate == ncr1Date)
                                                    {
                                                        IsmatchDate = true;
                                                    }
                                                }
                                                if (IsmatchGName == true && IsmatchMName == true && IsmatchFName == true)
                                                {
                                                    NameMatch[j] = 2;
                                                }
                                                else if ((IsmatchGName == true && IsmatchMName == true) || (IsmatchGName == true && IsmatchFName == true) || (IsmatchMName == true && IsmatchFName == true))
                                                {
                                                    if (sFirstname.ToLower() == ncr1givenname.ToLower())
                                                    {
                                                        IsmatchGName = true;
                                                    }
                                                    if (sMidname.Length == 1 && ncr1middlename.Length > 0)
                                                    {
                                                        if (sMidname.ToLower() == ncr1middlename.ToLower().Substring(0, 1))
                                                        {
                                                            IsmatchMName = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (sMidname.ToLower() == ncr1middlename.ToLower())
                                                        {
                                                            IsmatchMName = true;
                                                        }
                                                    }
                                                    if (sLastname.ToLower() == ncr1familyname.ToLower())
                                                    {
                                                        IsmatchFName = true;
                                                    }
                                                    if (IsmatchGName == true && IsmatchMName == true && IsmatchFName == true)
                                                    {
                                                        NameMatch[j] = 2;
                                                    }
                                                    else if ((IsmatchGName == true && IsmatchMName == true) || (IsmatchGName == true && IsmatchFName == true) || (IsmatchMName == true && IsmatchFName == true))
                                                    {
                                                        NameMatch[j] = 1;
                                                    }
                                                    else
                                                    {
                                                        NameMatch[j] = 0;
                                                    }
                                                }
                                                else
                                                {
                                                    if (sFirstname.ToLower() == ncr1givenname.ToLower())
                                                    {
                                                        IsmatchGName = true;
                                                    }
                                                    if (sMidname.Length == 1 && ncr1middlename.Length > 0)
                                                    {
                                                        if (sMidname.ToLower() == ncr1middlename.ToLower().Substring(0, 1))
                                                        {
                                                            IsmatchMName = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (sMidname.ToLower() == ncr1middlename.ToLower())
                                                        {
                                                            IsmatchMName = true;
                                                        }
                                                    }
                                                    if (sLastname.ToLower() == ncr1familyname.ToLower())
                                                    {
                                                        IsmatchFName = true;
                                                    }
                                                    if (IsmatchGName == true && IsmatchMName == true && IsmatchFName == true)
                                                    {
                                                        NameMatch[j] = 2;
                                                    }
                                                    else if ((IsmatchGName == true && IsmatchMName == true) || (IsmatchGName == true && IsmatchFName == true) || (IsmatchMName == true && IsmatchFName == true))
                                                    {
                                                        NameMatch[j] = 1;
                                                    }
                                                    else
                                                    {
                                                        NameMatch[j] = 0;
                                                    }
                                                }
                                                if (IsmatchPostCode == true && IsmatchRegion == true && IsmatchMunc == true && IsmatchAddressLine == true && IsAddressUpdated == true)
                                                {
                                                    AddMatch[j] = 2;
                                                }
                                                else if (Ismatch_PostCode == true && Ismatch_Region == true && Ismatch_Munc == true && Ismatch_AddressLine == true)
                                                {
                                                    AddMatch[j] = 2;
                                                }
                                                else if (Ismatch_PostCode == true || Ismatch_Region == true || Ismatch_Munc == true || Ismatch_AddressLine == true)
                                                {
                                                    AddMatch[j] = 4;
                                                }
                                                else if ((IsmatchPostCode == true || IsmatchRegion == true || IsmatchMunc == true || IsmatchAddressLine == true) && IsAddressUpdated == true)
                                                {
                                                    AddMatch[j] = 1;
                                                }
                                                else
                                                {
                                                    AddMatch[j] = 0;
                                                }
                                                if (IsmatchYear == true && IsmatchMonth == true && IsmatchDate == true)
                                                {
                                                    DOBMatch[j] = 2;
                                                }
                                                else if (IsmatchYear == true || IsmatchMonth == true || IsmatchDate == true || EnteredAge == ncr1Age)
                                                {
                                                    if (sYear == ncr1Year)
                                                    {
                                                        IsmatchYear = true;
                                                    }
                                                    if (sMonth == ncr1Month)
                                                    {
                                                        IsmatchMonth = true;
                                                    }
                                                    if (sDate == ncr1Date)
                                                    {
                                                        IsmatchDate = true;
                                                    }
                                                    if (IsmatchYear == true && IsmatchMonth == true && IsmatchDate == true)
                                                    {
                                                        DOBMatch[j] = 2;
                                                    }
                                                    else if (IsmatchYear == true || IsmatchMonth == true || IsmatchDate == true || ActualEnteredAge == ncr1Age)
                                                    {
                                                        DOBMatch[j] = 1;
                                                    }
                                                    else
                                                    {
                                                        DOBMatch[j] = 0;
                                                    }
                                                }
                                                else
                                                {
                                                    if (sYear == ncr1Year)
                                                    {
                                                        IsmatchYear = true;
                                                    }
                                                    if (sMonth == ncr1Month)
                                                    {
                                                        IsmatchMonth = true;
                                                    }
                                                    if (sDate == ncr1Date)
                                                    {
                                                        IsmatchDate = true;
                                                    }
                                                    if (IsmatchYear == true && IsmatchMonth == true && IsmatchDate == true)
                                                    {
                                                        DOBMatch[j] = 2;
                                                    }
                                                    else if (IsmatchYear == true || IsmatchMonth == true || IsmatchDate == true || ActualEnteredAge == ncr1Age)
                                                    {
                                                        DOBMatch[j] = 1;
                                                    }
                                                    else
                                                    {
                                                        DOBMatch[j] = 0;
                                                    }
                                                }
                                                var FindxelementMatchMeter = NCR1xelement.Descendants("MatchMeter").ToList();


                                                if (FindxelementMatchMeter.Count > 0)
                                                {
                                                    if (Convert.ToInt32(FindxelementMatchMeter.FirstOrDefault().Elements("Name").FirstOrDefault().Value) < NameMatch[j])
                                                    {
                                                        FindxelementMatchMeter.FirstOrDefault().Elements("Name").FirstOrDefault().Value = NameMatch[j].ToString();
                                                    }
                                                    if (Convert.ToInt32(FindxelementMatchMeter.FirstOrDefault().Elements("Address").FirstOrDefault().Value) < AddMatch[j] && IsAddressEmpty == false)
                                                    {
                                                        FindxelementMatchMeter.FirstOrDefault().Elements("Address").FirstOrDefault().Value = AddMatch[j].ToString();
                                                    }
                                                    if (Convert.ToInt32(FindxelementMatchMeter.FirstOrDefault().Elements("DOB").FirstOrDefault().Value) < DOBMatch[j])
                                                    {
                                                        FindxelementMatchMeter.FirstOrDefault().Elements("DOB").FirstOrDefault().Value = DOBMatch[j].ToString();
                                                    }
                                                }
                                                else
                                                {
                                                    XElement xelementMatchMeter = new XElement("MatchMeter");
                                                    XElement xelementName = new XElement("Name");
                                                    XElement xelementAddress = new XElement("Address");
                                                    XElement xelementDOB = new XElement("DOB");
                                                    NCR1xelement.Add(xelementMatchMeter);
                                                    xelementMatchMeter.Add(xelementName);
                                                    xelementMatchMeter.Add(xelementAddress);
                                                    xelementMatchMeter.Add(xelementDOB);
                                                    xelementName.Value = NameMatch[j].ToString();
                                                    if (IsAddressEmpty == false)
                                                    {
                                                        xelementAddress.Value = AddMatch[j].ToString();
                                                    }
                                                    else
                                                    {
                                                        xelementAddress.Value = "-1";
                                                    }
                                                    xelementDOB.Value = DOBMatch[j].ToString();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ObjXDocumentupdated = XDocument.Parse(XML_ValueNCR1);
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in GetMatchmeterforNCR(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

            }

            ObjXDocumentupdated = ObjXDocumentNCR1;
            return ObjXDocumentupdated;
        }

        #endregion

        public static string GetRequiredXml(string XmlResponse)
        {
            string XmlResponse_Local = XmlResponse;
            XmlResponse_Local = XmlResponse_Local.Replace("</BackgroundReports>", "");
            int xmlstartindex = XmlResponse_Local.IndexOf("<BackgroundReportPackage>");
            XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
            return XmlResponse_Local;
        }

        public static string GetRequiredXmlConditional(string XmlResponse, string ProductCode)
        {
            string XmlResponse_Local = XmlResponse;
            switch (ProductCode)
            {
                    //Changed By Himesh Kumar- removed PS product case
                case "NCR1":
                case "NCR+":
                case "RCX":
                    {
                        XmlResponse_Local = XmlResponse_Local.Replace("</BackgroundReports>", "");
                        int xmlstartindex = XmlResponse_Local.IndexOf("<BackgroundReportPackage>");
                        XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex); break;
                    }
                case "SCR":
                    {
                        if (XmlResponse_Local.IndexOf("<BackgroundReportPackage>") != -1)
                        {
                            XmlResponse_Local = XmlResponse_Local.Replace("</BackgroundReports>", "");
                            int xmlstartindex = XmlResponse_Local.IndexOf("<BackgroundReportPackage>");
                            XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                        } break;
                    }
                default:
                    break;
            }


            return XmlResponse_Local;
        }

        public string CompleteNCR1FinalResponse(string ResponseData, string UpdatedResponse, int pkOrderResponseId, string ProductCode)
        {
            string FinalResponse = "";
            try
            {
                if (ProductCode.ToUpper().Trim() == "NCR1")
                {
                    int TrimedUpTo = ResponseData.IndexOf("<BackgroundReportPackage>");
                    StringBuilder CompleteResponse = new StringBuilder();
                    string XMLInitials = ResponseData.Substring(0, TrimedUpTo);
                    CompleteResponse.Append(XMLInitials);
                    CompleteResponse.Append(UpdatedResponse);
                    CompleteResponse.Append("</BackgroundReports>");
                    if (ProductCode.ToUpper().Trim() == "NCR1")
                    {
                        FinalResponse = Convert.ToString(CompleteResponse);
                    }
                    else
                    {
                        FinalResponse = Convert.ToString(CompleteResponse).Replace("&lt;", "<").Replace("&gt;", ">");
                    }
                }
                else
                    FinalResponse = UpdatedResponse;
                //UpdateNCR1FinalResponse(pkOrderResponseId, FinalResponse);
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in CompleteNCR1FinalResponse(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

            }
            return FinalResponse;
        }

        public string CompleteSCRFinalResponse(string ResponseData, string UpdatedResponse, int pkOrderResponseId, string ProductCode)
        {
            string FinalResponse = "";
            try
            {
                if (ProductCode.ToUpper().Trim() == "SCR")
                {
                    int TrimedUpTo = ResponseData.IndexOf("<product>");
                    StringBuilder CompleteResponse = new StringBuilder();
                    string XMLInitials = ResponseData.Substring(0, TrimedUpTo);
                    CompleteResponse.Append(XMLInitials);
                    CompleteResponse.Append(UpdatedResponse);
                    CompleteResponse.Append("</BGC>");
                    if (ProductCode.ToUpper().Trim() == "SCR")
                    {
                        FinalResponse = Convert.ToString(CompleteResponse);
                    }
                    else
                    {
                        FinalResponse = Convert.ToString(CompleteResponse).Replace("&lt;", "<").Replace("&gt;", ">");
                    }
                }
                else
                    FinalResponse = UpdatedResponse;
                //UpdateNCR1FinalResponse(pkOrderResponseId, FinalResponse);
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in CompleteSCRFinalResponse(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

            }
            return FinalResponse;
        }

        public static string GetRequiredXmlforSor(string XmlResponse)
        {
            string XmlResponse_Local = XmlResponse;
            XmlResponse_Local = XmlResponse_Local.Replace("</BGC>", "");
            int xmlstartindex = XmlResponse_Local.IndexOf("<product>");
            XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
            return XmlResponse_Local;
        }
        public static string GetRequiredXmlforCCR1(string XmlResponse)
        {

            string XmlResponse_Local = XmlResponse;
            XmlResponse_Local = XmlResponse_Local.Replace("</FastraxNetwork>", "");
            int xmlstartindex = XmlResponse_Local.IndexOf("<report>");
            XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
            return XmlResponse_Local;
        }


        #region HideSevenYear for Test Site

        public string HideSevenYear(string ResponseData, int? pkOrderDetailId, byte WatchListStatus, string ProductCode, DateTime orderdt, out bool IsShowRawData)
        {
            string FinalResponse = "";
            bool _IsShowRawData = true;
            XDocument UpdatedXML = GetHidedResponse(ResponseData, pkOrderDetailId, WatchListStatus, ProductCode, orderdt, out _IsShowRawData);
            string UpdatedResponse = Convert.ToString(UpdatedXML);
            if (ProductCode == "NCR1" || ProductCode == "NCR+" || ProductCode == "RCX")
            {
                //CompleteNCR1FinalResponse(ResponseData, UpdatedResponse, pkOrderResponseId, ProductCode);
                int TrimedUpTo = ResponseData.IndexOf("<BackgroundReportPackage>");
                StringBuilder CompleteResponse = new StringBuilder();
                string XMLInitials = ResponseData.Substring(0, TrimedUpTo);
                CompleteResponse.Append(XMLInitials);
                CompleteResponse.Append(UpdatedResponse);
                CompleteResponse.Append("</BackgroundReports>");
                FinalResponse = Convert.ToString(CompleteResponse);
            }
            else
            {
                FinalResponse = UpdatedResponse;
            }
            IsShowRawData = _IsShowRawData;
            return FinalResponse;
        }

        #region-----Updated code after INT-174 changes
        //public XDocument GetHidedResponse(string XMLresp, int? pkOrderDetailId, byte WatchListStatus, string ProductCode, DateTime orderdt, out bool IsShowRawData)
        //{

        //    string strLog = string.Empty;//For Report Log.
        //    string LogFilename = "XMLFilteration_XMLCaseDeletionLog_" + DateTime.Now.ToString("yyMMdd");
        //    strLog += DateTime.Now.ToString() + " Method Started : GetHidedResponse for OrderDetailId " + Convert.ToString(pkOrderDetailId);
        //    //lststates.Add("CA");
        //    //lststates.Add("KS");
        //    //lststates.Add("ME");
        //    //lststates.Add("MD");
        //    //lststates.Add("MA");
        //    //lststates.Add("MT");
        //    //lststates.Add("NY");
        //    //lststates.Add("NH");
        //    //lststates.Add("NM");
        //    //lststates.Add("NY");
        //    //lststates.Add("WA");
        //    string Filter_Id = "1";
        //    List<string> s = new List<string>();
        //    BALCompany ObjBALCompany = new BALCompany();
        //    List<tblState> ObjtblState = new List<tblState>();
        //    var Collection = ObjBALCompany.GetFiltersAccordingFilterId(Filter_Id).FirstOrDefault();//Getting filter rules 7 Year Filter.
        //    //bool MaxDate = false;
        //    //bool MinDate = false;
        //    bool flagforLiveRunnerCounty = false;//used for LOS ANGELES county --INT-287 as per Greg comments on Nov 27, 2015.
        //    string Year = Collection.Year.ToString();
        //    string MaxNewYear = Collection.TimeFrameFrom.ToString();
        //    string MinNewYear = Collection.TimeFrameIn.ToString();
        //    int CheckCase = 0;
        //    int Year_new = 0;
        //    int? tempflagYear = null;
        //    bool additionalFilterforNewYork = false;
        //    DateTime? Max_new_Year = null;
        //    DateTime? Min_new_Year = null;
        //    if (string.IsNullOrEmpty(Year) == false) { Year_new = int.Parse(Year); }
        //    if (string.IsNullOrEmpty(MaxNewYear) == false) { Max_new_Year = DateTime.Parse(MaxNewYear); }
        //    if (string.IsNullOrEmpty(MinNewYear) == false) { Min_new_Year = DateTime.Parse(MinNewYear); }
        //    if (Collection.Year != null) { Year_new = int.Parse(Collection.Year.ToString()); tempflagYear = 1; }
        //    //Case: 1 When Year is present but MaxYear and MinYear both null
        //    if (tempflagYear != null && Max_new_Year == null && Min_new_Year == null)
        //    {
        //        CheckCase = 1;
        //    }
        //    //Case: 2 When Year is present but One is Present from MaxYear and MinYear 
        //    if (tempflagYear != null && Max_new_Year != null && Min_new_Year == null)
        //    {
        //        CheckCase = 2;
        //    }
        //    //Case: 3 When Year, MaxYear and MinYear all are present
        //    if (tempflagYear != null && Max_new_Year != null && Min_new_Year != null)
        //    {
        //        CheckCase = 3;
        //    }
        //    if (Collection != null)
        //    {
        //        if (string.IsNullOrEmpty(Collection.Keywords) == false)
        //        {
        //            var coll = Collection.Keywords.ToString();
        //            var ArrayCollection = coll.Split(',');
        //            if (ArrayCollection.Count() > 0)
        //            {
        //                for (int j = 0; j < ArrayCollection.Count(); j++)
        //                {
        //                    s.Add(ArrayCollection[j].Trim().ToUpper().Replace(" ", ""));//Adding all keywords in List<string> s = new List<string>();
        //                }
        //            }
        //        }
        //    }
        //    var ListState = new List<StateName>();
        //    if (Collection != null)
        //    {

        //        #region---Used to 7 year auto filters from company edit page.
        //        Collection.IsEnableForAllState = get7yearAutoFiltersFromCompanyEditPage(Convert.ToInt32(HttpContext.Current.Session["pkCompanyId"]));
        //        #endregion
        //        if (string.IsNullOrEmpty(Collection.StateId) == false && Collection.IsEnableForAllState == false)
        //        {
        //            string FilterId = Collection.StateId;
        //            List<tblState> State = new List<tblState>();
        //            var Coll = FilterId.Split(',');
        //            if (Coll.Length > 0)
        //            {
        //                for (int r = 0; r < Coll.Length; r++)
        //                {
        //                    if (string.IsNullOrEmpty(Coll[r]) == false)
        //                    {
        //                        State.Add(new tblState { pkStateId = int.Parse(Coll[r]) });
        //                    }
        //                }
        //            }
        //            ListState = ObjBALCompany.GetStateName(State); //Getting Collection of State According to the 7 Year Filter rule. 
        //        }
        //        else
        //        {
        //            ListState = ObjBALCompany.GetStateNameForEnable7YearFilter(); // for ticket #85 if 7 Year filter enable, get all state
        //        }
        //    }
        //    if (ListState.Count > 0)
        //    {
        //        for (int d = 0; d < ListState.Count(); d++)
        //        {
        //            ObjtblState.Add(new tblState { StateCode = ListState.ElementAt(d).StateCode, StateName = ListState.ElementAt(d).State_Name, pkStateId = ListState.ElementAt(0).pkStateId });
        //        }
        //    }
        //    bool _IsShowRawData = true;
        //    List<string> StateCodeColl = new List<string>();
        //    List<string> StateNameColl = new List<string>();
        //    List<tblState> ObjStateColl = new List<tblState>();
        //    List<tblCounty> ObjCountyColl = new List<tblCounty>();

        //    XDocument ObjXDocumentupdated = new XDocument();
        //    XDocument ObjXDocument = new XDocument();

        //    try
        //    {
        //        using (EmergeDALDataContext DX = new EmergeDALDataContext())
        //        {
        //            StateCodeColl = ObjtblState.Select(x => x.StateCode).ToList(); //Assigning StateCode According to the 7 Year Filter rule. 
        //            StateNameColl = ObjtblState.Select(x => x.StateName).ToList(); //Assigning StateName According to the 7 Year Filter rule. 
        //            ObjStateColl = ObjtblState.ToList<tblState>(); //Assigning StateName According to the 7 Year Filter rule. 


        //            ObjCountyColl = DX.tblCounties.ToList<tblCounty>();
        //            bool IsEmergeReviewStatus = false;
        //            if (ProductCode.Trim().ToUpper() == "CCR1LiveRunner".Trim().ToUpper())
        //            {
        //                ProductCode = "RCX";
        //            }
        //            string XML_Value = GetRequiredXmlConditional(XMLresp, ProductCode.Trim().ToUpper());
        //            ObjXDocument = XDocument.Parse(XML_Value);
        //            if (ProductCode.Trim().ToUpper() == "NCR1" || ProductCode.Trim().ToUpper() == "NCR+" || ProductCode.Trim().ToUpper() == "RCX")
        //            {

        //                #region NCR1, NCR+, RCX
        //                List<XElement> xelementCriminalReport = ObjXDocument.Descendants("CriminalReport").ToList();
        //                strLog += DateTime.Now.ToString() + "  Line No ----------------------11846 CriminalReport records : " + Convert.ToString(xelementCriminalReport.Count()) + "\n";
        //                if (xelementCriminalReport != null)
        //                {
        //                    if (xelementCriminalReport.Count > 0)
        //                    {
        //                        List<XElement> CriminalCaseColl, CriminalCaseCollRm;
        //                        CriminalCaseColl = ObjXDocument.Descendants("CriminalCase").ToList();
        //                        CriminalCaseCollRm = ObjXDocument.Descendants("CriminalCase").ToList();

        //                        strLog += DateTime.Now.ToString() + "  Line No ----------------------11855 CriminalCase : " + Convert.ToString(CriminalCaseColl.Count()) + "\n";
        //                        XElement CriminalCaseSingle = null;
        //                        if (CriminalCaseColl != null && CriminalCaseColl.Count > 0)
        //                        {

        //                            for (int i = 0; i < CriminalCaseColl.Count; i++)
        //                            {
        //                                bool IsCourtTag = false;
        //                                CriminalCaseSingle = CriminalCaseColl.ElementAt(i);

        //                                List<XElement> RegionColl;
        //                                RegionColl = CriminalCaseSingle.Descendants("CourtName").ToList<XElement>();
        //                                var CaseFileDate = CriminalCaseSingle.Descendants("CaseFileDate").FirstOrDefault();
        //                                IsCourtTag = true;

        //                                if (RegionColl == null || RegionColl.Count == 0)
        //                                {
        //                                    RegionColl = CriminalCaseSingle.Descendants("Region").ToList<XElement>();
        //                                    IsCourtTag = false;
        //                                }
        //                                if (RegionColl != null && RegionColl.Count > 0)
        //                                {
        //                                    bool isStateValid = false;
        //                                    string StateCode = "";
        //                                    string CourtName = "";
        //                                    if (IsCourtTag)
        //                                    {
        //                                        CourtName = RegionColl.ElementAt(0).Value;
        //                                        //As per the Greg comments  in JIRA on Nov 27, 2015
        //                                        //If los angeles county is comming and no charge found in the response.
        //                                        //Then records are not being filtered out from the report.
        //                                        if (CourtName.ToLower().Contains("los angeles"))
        //                                        {

        //                                            string str = string.Empty;
        //                                            int flag = 0;
        //                                            DateTime dt;
        //                                            if (CaseFileDate != null)
        //                                            {
        //                                                str = CaseFileDate.Value;
        //                                                str = (str != "" && str.Length > 9) ? str.Substring(0, 10) : DateTime.Now.ToShortDateString();
        //                                                DateTime.TryParse(str, out dt);
        //                                                int UpdateYear = Year_new * (-1);
        //                                                DateTime NewDate = DateTime.Now.AddYears(UpdateYear);
        //                                                if (CheckCase == 1)
        //                                                {
        //                                                    if (dt < DateTime.Now.AddYears(UpdateYear))
        //                                                    {
        //                                                        #region #800: 7 YEAR STATES, Comment #31
        //                                                        if (flag == 2)
        //                                                        {
        //                                                            IsEmergeReviewStatus = true;
        //                                                        }

        //                                                        #endregion
        //                                                        if (flag != 2)
        //                                                        {
        //                                                            if (Collection.IsRemoveCharges == true)
        //                                                            {

        //                                                                flagforLiveRunnerCounty = false;
        //                                                            }
        //                                                            else
        //                                                            {
        //                                                                flagforLiveRunnerCounty = true;//Only for Live runner.
        //                                                            }
        //                                                        }
        //                                                    }
        //                                                    else
        //                                                    {
        //                                                        flagforLiveRunnerCounty = true;//Only for Live runner.
        //                                                    }
        //                                                }
        //                                                if (CheckCase == 2)
        //                                                {
        //                                                    if (Max_new_Year <= NewDate)
        //                                                    {

        //                                                        if (dt < Max_new_Year)
        //                                                        {
        //                                                            #region #800: 7 YEAR STATES, Comment #31
        //                                                            if (flag == 2)
        //                                                            {
        //                                                                IsEmergeReviewStatus = true;
        //                                                            }

        //                                                            #endregion
        //                                                            if (flag != 2)
        //                                                            {
        //                                                                if (Collection.IsRemoveCharges == true)
        //                                                                {

        //                                                                    flagforLiveRunnerCounty = false;
        //                                                                }
        //                                                                else
        //                                                                {
        //                                                                    flagforLiveRunnerCounty = true;//Only for Live runner.
        //                                                                }
        //                                                            }
        //                                                        }
        //                                                    }
        //                                                    else
        //                                                    {
        //                                                        if (dt < DateTime.Now.AddYears(UpdateYear))
        //                                                        {
        //                                                            #region #800: 7 YEAR STATES, Comment #31
        //                                                            if (flag == 2)
        //                                                            {
        //                                                                IsEmergeReviewStatus = true;
        //                                                            }


        //                                                            #endregion
        //                                                            if (flag != 2)
        //                                                            {
        //                                                                if (Collection.IsRemoveCharges == true)
        //                                                                {

        //                                                                    flagforLiveRunnerCounty = false;
        //                                                                }
        //                                                                else
        //                                                                {
        //                                                                    flagforLiveRunnerCounty = true;//Only for Live runner.
        //                                                                }
        //                                                            }
        //                                                        }
        //                                                        else
        //                                                        {
        //                                                            flagforLiveRunnerCounty = true;//Only for Live runner.
        //                                                        }
        //                                                    }
        //                                                }
        //                                                if (CheckCase == 3)
        //                                                {
        //                                                    if (Min_new_Year <= dt && dt <= Max_new_Year)
        //                                                    {
        //                                                        #region #800: 7 YEAR STATES, Comment #31
        //                                                        if (flag == 2)
        //                                                        {
        //                                                            IsEmergeReviewStatus = true;
        //                                                        }

        //                                                        #endregion
        //                                                        if (flag != 2)
        //                                                        {
        //                                                            if (Collection.IsRemoveCharges == true)
        //                                                            {

        //                                                                flagforLiveRunnerCounty = false;
        //                                                            }
        //                                                            else
        //                                                            {
        //                                                                flagforLiveRunnerCounty = true;//Only for Live runner.
        //                                                            }
        //                                                        }
        //                                                    }
        //                                                    else
        //                                                    {
        //                                                        flagforLiveRunnerCounty = true;//Only for Live runner.
        //                                                    }
        //                                                }
        //                                            }
        //                                            else
        //                                            {
        //                                                flagforLiveRunnerCounty = true;//Only for Live runner.
        //                                            }
        //                                        }
        //                                        if (CourtName.Contains("-"))
        //                                        {
        //                                            string[] Getstatename = CourtName.Split('-');
        //                                            if (Getstatename.Count() > 1)
        //                                            {
        //                                                if (!GetPackageFor7YearFilter(pkOrderDetailId))
        //                                                {
        //                                                    if (!GetPackageFor10YearFilter(pkOrderDetailId))
        //                                                    {
        //                                                        //INT-256 as per Greg comments on JIRA We need to create the following logic: 
        //                                                        //IF record SOURCE = New York (any record in New York).
        //                                                        //THEN it should consider the "ADDITIONALEVENTS" tag for the 7-year filter.
        //                                                        //It should look at the most recent date to calculate.
        //                                                        if (Getstatename[1].Trim().ToUpper() == "NEW YORK")
        //                                                        {
        //                                                            isStateValid = true;//It should me implicitly filter state as per additional item date.
        //                                                            additionalFilterforNewYork = true;
        //                                                            strLog += DateTime.Now.ToString() + "  Line No ----------------------11900 New York found \n";
        //                                                        }
        //                                                        else
        //                                                        {
        //                                                            isStateValid = (ObjStateColl.Where(d => d.StateName.ToLower() == Getstatename[1].Trim().ToLower()).Count() > 0) ? true : false;
        //                                                            additionalFilterforNewYork = false;
        //                                                            strLog += DateTime.Now.ToString() + "  Line No ----------------------11906 New York Not found \n";
        //                                                        }
        //                                                    }
        //                                                }
        //                                            }

        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        foreach (XElement Region in RegionColl)
        //                                        {
        //                                            StateCode = Region.Value;
        //                                            if (!GetPackageFor7YearFilter(pkOrderDetailId))
        //                                            {
        //                                                if (!GetPackageFor10YearFilter(pkOrderDetailId))
        //                                                {
        //                                                    isStateValid = (StateCodeColl.Where(d => d.ToLower() == StateCode.ToLower()).Count() > 0) ? true : false;
        //                                                }
        //                                            }
        //                                            else
        //                                            {
        //                                                isStateValid = false;
        //                                                break;
        //                                            }
        //                                            if (isStateValid)
        //                                                break;
        //                                        }
        //                                    }
        //                                    if (isStateValid)
        //                                    {
        //                                        List<XElement> ChargeColl, ChargeCollRm, AdditionalItemsColl;
        //                                        ChargeColl = CriminalCaseSingle.Descendants("Charge").ToList<XElement>();
        //                                        ChargeCollRm = CriminalCaseSingle.Descendants("Charge").ToList<XElement>();
        //                                        AdditionalItemsColl = CriminalCaseSingle.Descendants("AdditionalItems").ToList<XElement>();
        //                                        XElement ChargeSingle = null;
        //                                        if (ChargeColl != null && ChargeColl.Count > 0)
        //                                        {
        //                                            foreach (XElement xe in ChargeColl)
        //                                            {
        //                                                ChargeSingle = xe;
        //                                                string strDisposition = string.Empty;
        //                                                XElement OffenseDate = ChargeSingle.Descendants("OffenseDate").FirstOrDefault();
        //                                                XElement DispositionDate = ChargeSingle.Descendants("DispositionDate").FirstOrDefault();
        //                                                XElement DispDate = ChargeSingle.Descendants("DispDate").FirstOrDefault();
        //                                                XElement Disposition = ChargeSingle.Descendants("Disposition").FirstOrDefault();
        //                                                XElement SentenceDate = ChargeSingle.Descendants("Sentence").FirstOrDefault();
        //                                                int flag = 1;
        //                                                if (Disposition != null)
        //                                                {
        //                                                    string Despostioncheck = string.Empty;
        //                                                    Despostioncheck = Disposition.Value;
        //                                                    if (!string.IsNullOrEmpty(Despostioncheck))
        //                                                    {
        //                                                        string Desposition = Despostioncheck.ToLower();
        //                                                        while (Desposition.IndexOf(" ") >= 0)
        //                                                        {
        //                                                            Desposition = Desposition.Replace(" ", "");
        //                                                        }
        //                                                        for (int r = 0; r < s.Count(); r++)
        //                                                        {
        //                                                            if (string.IsNullOrEmpty(s[r]) == false)
        //                                                            {
        //                                                                if (Desposition.ToLower().Contains(s[r].ToLower()))
        //                                                                {
        //                                                                    flag = 2;
        //                                                                }
        //                                                            }
        //                                                        }
        //                                                    }
        //                                                    else
        //                                                    {
        //                                                        flag = 2;
        //                                                    }
        //                                                }
        //                                                //}

        //                                                string str = ""; DateTime dt;
        //                                                if (OffenseDate != null)
        //                                                {
        //                                                    str = OffenseDate.Value;
        //                                                }
        //                                                else if (DispositionDate != null)
        //                                                {
        //                                                    str = DispositionDate.Value;
        //                                                }
        //                                                else if (CaseFileDate != null)
        //                                                {
        //                                                    str = CaseFileDate.Value;
        //                                                }
        //                                                else if (DispDate != null)
        //                                                {
        //                                                    str = DispDate.Value;//20070625
        //                                                    if (str != "" && str.Length > 7)
        //                                                    {
        //                                                        str = str.Substring(0, 4) + "/" + str.Substring(4, 2) + "/" + str.Substring(6, 2);
        //                                                    }
        //                                                    else
        //                                                    {
        //                                                        str = DateTime.Now.ToShortDateString();
        //                                                    }
        //                                                }
        //                                                else if (SentenceDate != null)
        //                                                { // changes regarding ticket 418
        //                                                    string SentenceDatevalue = SentenceDate.Value;
        //                                                    if (SentenceDatevalue.Contains("SENTENCE DATE"))
        //                                                    {
        //                                                        string[] splitdatevalue = SentenceDatevalue.Split(':');
        //                                                        var Completedatevalue = splitdatevalue[1];
        //                                                        str = Completedatevalue.Substring(0, 11).Replace("/", "");
        //                                                        string str1 = str.Substring(5, 4);
        //                                                        string str2 = str.Substring(1, 2);
        //                                                        string str3 = str.Substring(3, 2);
        //                                                        str = str1 + "/" + str2 + "/" + str3;
        //                                                    }
        //                                                }
        //                                                str = (str != "" && str.Length > 9) ? str.Substring(0, 10) : DateTime.Now.ToShortDateString();
        //                                                DateTime.TryParse(str, out dt);
        //                                                int UpdateYear = Year_new * (-1);
        //                                                DateTime NewDate = DateTime.Now.AddYears(UpdateYear);
        //                                                if (CheckCase == 1)
        //                                                {
        //                                                    if (dt < DateTime.Now.AddYears(UpdateYear))
        //                                                    {
        //                                                        #region #800: 7 YEAR STATES, Comment #31
        //                                                        if (flag == 2)
        //                                                        {
        //                                                            IsEmergeReviewStatus = true;
        //                                                        }
        //                                                        if (_IsShowRawData)
        //                                                            _IsShowRawData = false;

        //                                                        #endregion
        //                                                        if (flag != 2)
        //                                                        {
        //                                                            if (Collection.IsRemoveCharges == true)
        //                                                            {

        //                                                                ChargeCollRm.Remove(ChargeSingle);
        //                                                            }
        //                                                        }
        //                                                    }
        //                                                }
        //                                                if (CheckCase == 2)
        //                                                {
        //                                                    if (Max_new_Year <= NewDate)
        //                                                    {

        //                                                        if (dt < Max_new_Year)
        //                                                        {
        //                                                            #region #800: 7 YEAR STATES, Comment #31
        //                                                            if (flag == 2)
        //                                                            {
        //                                                                IsEmergeReviewStatus = true;
        //                                                            }
        //                                                            if (_IsShowRawData)
        //                                                                _IsShowRawData = false;

        //                                                            #endregion
        //                                                            if (flag != 2)
        //                                                            {
        //                                                                if (Collection.IsRemoveCharges == true)
        //                                                                {

        //                                                                    ChargeCollRm.Remove(ChargeSingle);
        //                                                                }
        //                                                            }
        //                                                        }
        //                                                    }
        //                                                    else
        //                                                    {
        //                                                        if (dt < DateTime.Now.AddYears(UpdateYear))
        //                                                        {
        //                                                            #region #800: 7 YEAR STATES, Comment #31
        //                                                            if (flag == 2)
        //                                                            {
        //                                                                IsEmergeReviewStatus = true;
        //                                                            }
        //                                                            if (_IsShowRawData)
        //                                                                _IsShowRawData = false;

        //                                                            #endregion
        //                                                            if (flag != 2)
        //                                                            {
        //                                                                if (Collection.IsRemoveCharges == true)
        //                                                                {

        //                                                                    ChargeCollRm.Remove(ChargeSingle);
        //                                                                }
        //                                                            }
        //                                                        }
        //                                                    }
        //                                                }
        //                                                if (CheckCase == 3)
        //                                                {
        //                                                    if (Min_new_Year <= dt && dt <= Max_new_Year)
        //                                                    {
        //                                                        #region #800: 7 YEAR STATES, Comment #31
        //                                                        if (flag == 2)
        //                                                        {
        //                                                            IsEmergeReviewStatus = true;
        //                                                        }
        //                                                        if (_IsShowRawData)
        //                                                            _IsShowRawData = false;

        //                                                        #endregion
        //                                                        if (flag != 2)
        //                                                        {
        //                                                            if (Collection.IsRemoveCharges == true)
        //                                                            {

        //                                                                ChargeCollRm.Remove(ChargeSingle);
        //                                                            }
        //                                                        }
        //                                                    }
        //                                                }
        //                                            }
        //                                        }
        //                                        else
        //                                        {
        //                                            //INT-287 as per Greg comments in JIRA.
        //                                            //If any county belongs to Los angeles and no charges are cmoing in the response.
        //                                            //then response will be displayed on view report page.
        //                                            if (ProductCode == "RCX")
        //                                            {
        //                                                if (!flagforLiveRunnerCounty)//If Los angeles county does not exists.
        //                                                {
        //                                                    CriminalCaseCollRm.Remove(CriminalCaseSingle);
        //                                                }
        //                                            }
        //                                            else
        //                                            {
        //                                                CriminalCaseCollRm.Remove(CriminalCaseSingle);
        //                                            }
        //                                        }

        //                                        if (AdditionalItemsColl != null && AdditionalItemsColl.Count() > 0)//INT256 New implementation.
        //                                        {
        //                                            if (additionalFilterforNewYork)
        //                                            {
        //                                                foreach (XElement xe in AdditionalItemsColl)
        //                                                {
        //                                                    ChargeSingle = xe;
        //                                                    DateTime dtNewYork;
        //                                                    string strDisposition = string.Empty;
        //                                                    //to get the Additional items date field entries.
        //                                                    XElement anyDate = ChargeSingle.Descendants("EffectiveDate").Descendants("StartDate").Descendants("AnyDate").FirstOrDefault();
        //                                                    if (anyDate != null)
        //                                                    {
        //                                                        string[] splitdatevalueNewYork = anyDate.Value.Split('T');//To split date part from the string like <AnyDate>2000-03-18T00:00:00</AnyDate>. 
        //                                                        //var CompletedatevalueNewYork = splitdatevalueNewYork[0];//get only date part from the string array.
        //                                                        DateTime.TryParse(splitdatevalueNewYork[0], out dtNewYork);//To verify the date format.
        //                                                        int UpdateYear = Year_new * (-1);//To creating the less than 7 year date from the current date.
        //                                                        if (dtNewYork < DateTime.Now.AddYears(UpdateYear))//to check date if coming date less than 7 year.
        //                                                        {
        //                                                            //7 YEAR STATES, Comment
        //                                                            strLog += DateTime.Now.ToString() + "  Line No ----------------------12149 New York found and remove based on case \n";
        //                                                            CriminalCaseCollRm.Remove(CriminalCaseSingle);//If coming date is less than 7 year than it will remove Single CriminalCaseSingle section. 
        //                                                        }
        //                                                    }
        //                                                }
        //                                            }
        //                                        }
        //                                        if (Collection.IsRemoveCharges == true)
        //                                        {
        //                                            #region Removing/Adding Charges to Current criminal Case
        //                                            if (ChargeCollRm.Count > 0)
        //                                            {
        //                                                strLog += DateTime.Now.ToString() + "  Line No ----------------------12161 Removing/Adding Charges to Current criminal Case \n";
        //                                                strLog += DateTime.Now.ToString() + "  Line No ----------------------12162 Charges added : " + Convert.ToString(ChargeCollRm.Count) + "\n";
        //                                                CriminalCaseSingle.Descendants("Charge").Remove();
        //                                                foreach (XElement rm in ChargeCollRm)
        //                                                {
        //                                                    CriminalCaseSingle.Add(rm);
        //                                                }
        //                                            }
        //                                            #endregion

        //                                            #region Logic to remove CriminalCase if all chares are removed
        //                                            if (ChargeCollRm.Count == 0)
        //                                            {
        //                                                //INT-287 as per Greg comments in JIRA.
        //                                                //If any county belongs to Los angeles and no charges are cmoing in the response.
        //                                                //then response will be displayed on view report page.
        //                                                if (ProductCode == "RCX") //If Los angeles county does not exists.
        //                                                {
        //                                                    if (!flagforLiveRunnerCounty)
        //                                                    {
        //                                                        CriminalCaseCollRm.Remove(CriminalCaseSingle);
        //                                                    }
        //                                                }
        //                                                else
        //                                                {
        //                                                    CriminalCaseCollRm.Remove(CriminalCaseSingle);
        //                                                }
        //                                            }
        //                                            #endregion
        //                                        }
        //                                    }
        //                                    else//Package Filter functionality based on Jira ticket INT-174
        //                                    {
        //                                        #region 7 Year Filter
        //                                        if (GetPackageFor7YearFilter(pkOrderDetailId))
        //                                        {
        //                                            strLog += DateTime.Now.ToString() + "  Line No ----------------------12184 GetPackageFor7YearFilter \n";
        //                                            List<XElement> ChargeColl, ChargeCollRm, AdditionalItemsColl;
        //                                            ChargeColl = CriminalCaseSingle.Descendants("Charge").ToList<XElement>();
        //                                            ChargeCollRm = CriminalCaseSingle.Descendants("Charge").ToList<XElement>();
        //                                            AdditionalItemsColl = CriminalCaseSingle.Descendants("AdditionalItems").ToList<XElement>();
        //                                            XElement ChargeSingle = null;
        //                                            if (ChargeColl != null && ChargeColl.Count > 0)
        //                                            {
        //                                                foreach (XElement xe in ChargeColl)
        //                                                {
        //                                                    ChargeSingle = xe;
        //                                                    string strDisposition = string.Empty;
        //                                                    XElement OffenseDate = ChargeSingle.Descendants("OffenseDate").FirstOrDefault();
        //                                                    XElement DispositionDate = ChargeSingle.Descendants("DispositionDate").FirstOrDefault();
        //                                                    XElement DispDate = ChargeSingle.Descendants("DispDate").FirstOrDefault();
        //                                                    XElement Disposition = ChargeSingle.Descendants("Disposition").FirstOrDefault();
        //                                                    XElement SentenceDate = ChargeSingle.Descendants("Sentence").FirstOrDefault();


        //                                                    int flag = 1;

        //                                                    if (Disposition != null)
        //                                                    {
        //                                                        string Despostioncheck = string.Empty;
        //                                                        Despostioncheck = Disposition.Value;
        //                                                        if (!string.IsNullOrEmpty(Despostioncheck))
        //                                                        {
        //                                                            string Desposition = Despostioncheck.ToLower();
        //                                                            while (Desposition.IndexOf(" ") >= 0)
        //                                                            {
        //                                                                Desposition = Desposition.Replace(" ", "");
        //                                                            }
        //                                                            for (int r = 0; r < s.Count(); r++)
        //                                                            {
        //                                                                if (string.IsNullOrEmpty(s[r]) == false)
        //                                                                {
        //                                                                    if (Desposition.ToLower().Contains(s[r].ToLower()))
        //                                                                    {
        //                                                                        flag = 2;
        //                                                                    }
        //                                                                }
        //                                                            }
        //                                                        }
        //                                                        else
        //                                                        {
        //                                                            flag = 2;
        //                                                        }
        //                                                    }
        //                                                    string str = ""; DateTime dt;
        //                                                    if (OffenseDate != null)
        //                                                    {
        //                                                        str = OffenseDate.Value;
        //                                                    }
        //                                                    else if (DispositionDate != null)
        //                                                    {
        //                                                        str = DispositionDate.Value;
        //                                                    }
        //                                                    else if (CaseFileDate != null)
        //                                                    {
        //                                                        str = CaseFileDate.Value;
        //                                                    }
        //                                                    else if (DispDate != null)
        //                                                    {
        //                                                        str = DispDate.Value;//20070625
        //                                                        if (str != "" && str.Length > 7)
        //                                                        {
        //                                                            str = str.Substring(0, 4) + "/" + str.Substring(4, 2) + "/" + str.Substring(6, 2);
        //                                                        }
        //                                                        else
        //                                                        {
        //                                                            str = DateTime.Now.ToShortDateString();
        //                                                        }
        //                                                    }
        //                                                    else if (SentenceDate != null)
        //                                                    { // changes regarding ticket 418
        //                                                        string SentenceDatevalue = SentenceDate.Value;
        //                                                        if (SentenceDatevalue.Contains("SENTENCE DATE"))
        //                                                        {
        //                                                            string[] splitdatevalue = SentenceDatevalue.Split(':');
        //                                                            var Completedatevalue = splitdatevalue[1];
        //                                                            str = Completedatevalue.Substring(0, 11).Replace("/", "");
        //                                                            string str1 = str.Substring(5, 4);
        //                                                            string str2 = str.Substring(1, 2);
        //                                                            string str3 = str.Substring(3, 2);
        //                                                            str = str1 + "/" + str2 + "/" + str3;
        //                                                        }
        //                                                    }
        //                                                    str = (str != "" && str.Length > 9) ? str.Substring(0, 10) : DateTime.Now.ToShortDateString();
        //                                                    DateTime.TryParse(str, out dt);
        //                                                    int UpdateYear = Year_new * (-1);
        //                                                    DateTime NewDate = DateTime.Now.AddYears(UpdateYear);
        //                                                    if (CheckCase == 1)
        //                                                    {
        //                                                        if (dt < DateTime.Now.AddYears(UpdateYear))
        //                                                        {
        //                                                            #region #800: 7 YEAR STATES, Comment #31
        //                                                            if (flag == 2)
        //                                                            {
        //                                                                IsEmergeReviewStatus = true;
        //                                                            }
        //                                                            if (_IsShowRawData)
        //                                                                _IsShowRawData = false;

        //                                                            #endregion
        //                                                            if (flag != 2)
        //                                                            {
        //                                                                if (Collection.IsRemoveCharges == true)
        //                                                                {
        //                                                                    if (dt < DateTime.Now.AddYears(UpdateYear))
        //                                                                    {
        //                                                                        ChargeCollRm.Remove(ChargeSingle);
        //                                                                    }
        //                                                                }
        //                                                            }
        //                                                        }
        //                                                    }

        //                                                    if (CheckCase == 2)
        //                                                    {
        //                                                        if (Max_new_Year <= NewDate)
        //                                                        {

        //                                                            if (dt < Max_new_Year)
        //                                                            {
        //                                                                #region #800: 7 YEAR STATES, Comment #31
        //                                                                if (flag == 2)
        //                                                                {
        //                                                                    IsEmergeReviewStatus = true;
        //                                                                }
        //                                                                if (_IsShowRawData)
        //                                                                    _IsShowRawData = false;

        //                                                                #endregion
        //                                                                if (flag != 2)
        //                                                                {
        //                                                                    if (Collection.IsRemoveCharges == true)
        //                                                                    {
        //                                                                        if (dt < Max_new_Year)
        //                                                                        {
        //                                                                            ChargeCollRm.Remove(ChargeSingle);
        //                                                                        }

        //                                                                    }
        //                                                                }
        //                                                            }
        //                                                            else
        //                                                            {
        //                                                                ChargeCollRm.Remove(ChargeSingle);
        //                                                            }
        //                                                        }
        //                                                        else
        //                                                        {
        //                                                            if (dt < DateTime.Now.AddYears(UpdateYear))
        //                                                            {
        //                                                                #region #800: 7 YEAR STATES, Comment #31
        //                                                                if (flag == 2)
        //                                                                {
        //                                                                    IsEmergeReviewStatus = true;
        //                                                                }
        //                                                                if (_IsShowRawData)
        //                                                                    _IsShowRawData = false;

        //                                                                #endregion
        //                                                                if (flag != 2)
        //                                                                {
        //                                                                    if (Collection.IsRemoveCharges == true)
        //                                                                    {
        //                                                                        if (dt < DateTime.Now.AddYears(UpdateYear))
        //                                                                        {
        //                                                                            ChargeCollRm.Remove(ChargeSingle);
        //                                                                        }
        //                                                                    }
        //                                                                }
        //                                                            }
        //                                                        }

        //                                                    }
        //                                                    if (CheckCase == 3)
        //                                                    {
        //                                                        if (Min_new_Year <= dt && dt <= Max_new_Year)
        //                                                        {
        //                                                            #region #800: 7 YEAR STATES, Comment #31
        //                                                            if (flag == 2)
        //                                                            {
        //                                                                IsEmergeReviewStatus = true;
        //                                                            }
        //                                                            if (_IsShowRawData)
        //                                                                _IsShowRawData = false;

        //                                                            #endregion
        //                                                            if (flag != 2)
        //                                                            {
        //                                                                if (Collection.IsRemoveCharges == true)
        //                                                                {
        //                                                                    ChargeCollRm.Remove(ChargeSingle);
        //                                                                }
        //                                                            }
        //                                                        }
        //                                                    }
        //                                                }
        //                                            }
        //                                            else
        //                                            {

        //                                                CriminalCaseCollRm.Remove(CriminalCaseSingle);

        //                                            }
        //                                            if (Collection.IsRemoveCharges == true)
        //                                            {
        //                                                #region Removing/Adding Charges to Current criminal Case
        //                                                if (ChargeCollRm.Count > 0)
        //                                                {
        //                                                    strLog += DateTime.Now.ToString() + "  Line No ----------------------12396 Charges added : " + Convert.ToString(ChargeCollRm.Count) + "\n";
        //                                                    CriminalCaseSingle.Descendants("Charge").Remove();
        //                                                    foreach (XElement rm in ChargeCollRm)
        //                                                    {
        //                                                        CriminalCaseSingle.Add(rm);
        //                                                    }
        //                                                }
        //                                                #endregion

        //                                                #region Logic to remove CriminalCase if all chares are removed
        //                                                if (ChargeCollRm.Count == 0)
        //                                                {
        //                                                    CriminalCaseCollRm.Remove(CriminalCaseSingle);
        //                                                }
        //                                                #endregion
        //                                            }
        //                                        }
        //                                        #endregion

        //                                        #region 10 Year Filter
        //                                        if (GetPackageFor10YearFilter(pkOrderDetailId))//10 Year Filter
        //                                        {
        //                                            strLog += DateTime.Now.ToString() + "  Line No ----------------------12184 GetPackageFor10YearFilter \n";
        //                                            Year_new = 10;
        //                                            List<XElement> ChargeColl, ChargeCollRm, AdditionalItemsColl;
        //                                            ChargeColl = CriminalCaseSingle.Descendants("Charge").ToList<XElement>();
        //                                            ChargeCollRm = CriminalCaseSingle.Descendants("Charge").ToList<XElement>();
        //                                            AdditionalItemsColl = CriminalCaseSingle.Descendants("AdditionalItems").ToList<XElement>();
        //                                            XElement ChargeSingle = null;
        //                                            if (ChargeColl != null && ChargeColl.Count > 0)
        //                                            {
        //                                                foreach (XElement xe in ChargeColl)
        //                                                {
        //                                                    ChargeSingle = xe;
        //                                                    string strDisposition = string.Empty;
        //                                                    XElement OffenseDate = ChargeSingle.Descendants("OffenseDate").FirstOrDefault();
        //                                                    XElement DispositionDate = ChargeSingle.Descendants("DispositionDate").FirstOrDefault();
        //                                                    XElement DispDate = ChargeSingle.Descendants("DispDate").FirstOrDefault();
        //                                                    XElement Disposition = ChargeSingle.Descendants("Disposition").FirstOrDefault();
        //                                                    XElement SentenceDate = ChargeSingle.Descendants("Sentence").FirstOrDefault();


        //                                                    int flag = 1;

        //                                                    if (Disposition != null)
        //                                                    {
        //                                                        string Despostioncheck = string.Empty;
        //                                                        Despostioncheck = Disposition.Value;
        //                                                        if (!string.IsNullOrEmpty(Despostioncheck))
        //                                                        {
        //                                                            string Desposition = Despostioncheck.ToLower();
        //                                                            while (Desposition.IndexOf(" ") >= 0)
        //                                                            {
        //                                                                Desposition = Desposition.Replace(" ", "");
        //                                                            }
        //                                                            for (int r = 0; r < s.Count(); r++)
        //                                                            {
        //                                                                if (string.IsNullOrEmpty(s[r]) == false)
        //                                                                {
        //                                                                    if (Desposition.ToLower().Contains(s[r].ToLower()))
        //                                                                    {
        //                                                                        flag = 2;
        //                                                                    }
        //                                                                }
        //                                                            }
        //                                                        }
        //                                                        else
        //                                                        {
        //                                                            flag = 2;
        //                                                        }
        //                                                    }

        //                                                    string str = ""; DateTime dt;
        //                                                    if (OffenseDate != null)
        //                                                    {
        //                                                        str = OffenseDate.Value;
        //                                                    }
        //                                                    else if (DispositionDate != null)
        //                                                    {
        //                                                        str = DispositionDate.Value;
        //                                                    }
        //                                                    else if (CaseFileDate != null)
        //                                                    {
        //                                                        str = CaseFileDate.Value;
        //                                                    }
        //                                                    else if (DispDate != null)
        //                                                    {
        //                                                        str = DispDate.Value;//20070625
        //                                                        if (str != "" && str.Length > 7)
        //                                                        {
        //                                                            str = str.Substring(0, 4) + "/" + str.Substring(4, 2) + "/" + str.Substring(6, 2);
        //                                                        }
        //                                                        else
        //                                                        {
        //                                                            str = DateTime.Now.ToShortDateString();
        //                                                        }
        //                                                    }
        //                                                    else if (SentenceDate != null)
        //                                                    { // changes regarding ticket 418
        //                                                        string SentenceDatevalue = SentenceDate.Value;
        //                                                        if (SentenceDatevalue.Contains("SENTENCE DATE"))
        //                                                        {
        //                                                            string[] splitdatevalue = SentenceDatevalue.Split(':');
        //                                                            var Completedatevalue = splitdatevalue[1];
        //                                                            str = Completedatevalue.Substring(0, 11).Replace("/", "");
        //                                                            string str1 = str.Substring(5, 4);
        //                                                            string str2 = str.Substring(1, 2);
        //                                                            string str3 = str.Substring(3, 2);
        //                                                            str = str1 + "/" + str2 + "/" + str3;

        //                                                        }
        //                                                    }
        //                                                    str = (str != "" && str.Length > 9) ? str.Substring(0, 10) : DateTime.Now.ToShortDateString();
        //                                                    DateTime.TryParse(str, out dt);


        //                                                    int UpdateYear = Year_new * (-1);
        //                                                    DateTime NewDate = DateTime.Now.AddYears(UpdateYear);

        //                                                    if (CheckCase == 1)
        //                                                    {
        //                                                        if (dt < DateTime.Now.AddYears(UpdateYear))
        //                                                        {
        //                                                            #region #800: 7 YEAR STATES, Comment #31
        //                                                            if (flag == 2)
        //                                                            {
        //                                                                IsEmergeReviewStatus = true;
        //                                                            }
        //                                                            if (_IsShowRawData)
        //                                                                _IsShowRawData = false;

        //                                                            #endregion
        //                                                            if (flag != 2)
        //                                                            {
        //                                                                if (Collection.IsRemoveCharges == true)
        //                                                                {
        //                                                                    if (dt < DateTime.Now.AddYears(UpdateYear))
        //                                                                    {
        //                                                                        ChargeCollRm.Remove(ChargeSingle);
        //                                                                    }
        //                                                                }
        //                                                            }
        //                                                        }
        //                                                    }

        //                                                    if (CheckCase == 2)
        //                                                    {
        //                                                        if (Max_new_Year <= NewDate)
        //                                                        {

        //                                                            if (dt < Max_new_Year)
        //                                                            {
        //                                                                #region #800: 7 YEAR STATES, Comment #31
        //                                                                if (flag == 2)
        //                                                                {
        //                                                                    IsEmergeReviewStatus = true;
        //                                                                }
        //                                                                if (_IsShowRawData)
        //                                                                    _IsShowRawData = false;

        //                                                                #endregion
        //                                                                if (flag != 2)
        //                                                                {
        //                                                                    if (Collection.IsRemoveCharges == true)
        //                                                                    {
        //                                                                        if (dt < Max_new_Year)
        //                                                                        {
        //                                                                            ChargeCollRm.Remove(ChargeSingle);
        //                                                                        }

        //                                                                    }
        //                                                                }
        //                                                            }
        //                                                            else
        //                                                            {
        //                                                                ChargeCollRm.Remove(ChargeSingle);
        //                                                            }
        //                                                        }
        //                                                        else
        //                                                        {
        //                                                            if (dt < DateTime.Now.AddYears(UpdateYear))
        //                                                            {
        //                                                                #region #800: 7 YEAR STATES, Comment #31
        //                                                                if (flag == 2)
        //                                                                {
        //                                                                    IsEmergeReviewStatus = true;
        //                                                                }
        //                                                                if (_IsShowRawData)
        //                                                                    _IsShowRawData = false;

        //                                                                #endregion
        //                                                                if (flag != 2)
        //                                                                {
        //                                                                    if (Collection.IsRemoveCharges == true)
        //                                                                    {
        //                                                                        if (dt < DateTime.Now.AddYears(UpdateYear))
        //                                                                        {
        //                                                                            ChargeCollRm.Remove(ChargeSingle);
        //                                                                        }
        //                                                                    }
        //                                                                }
        //                                                            }
        //                                                        }

        //                                                    }
        //                                                    if (CheckCase == 3)
        //                                                    {
        //                                                        if (Min_new_Year <= dt && dt <= Max_new_Year)
        //                                                        {
        //                                                            #region #800: 7 YEAR STATES, Comment #31
        //                                                            if (flag == 2)
        //                                                            {
        //                                                                IsEmergeReviewStatus = true;
        //                                                            }
        //                                                            if (_IsShowRawData)
        //                                                                _IsShowRawData = false;

        //                                                            #endregion
        //                                                            if (flag != 2)
        //                                                            {
        //                                                                if (Collection.IsRemoveCharges == true)
        //                                                                {
        //                                                                    ChargeCollRm.Remove(ChargeSingle);
        //                                                                }
        //                                                            }
        //                                                        }
        //                                                    }
        //                                                }
        //                                            }
        //                                            else
        //                                            {

        //                                                CriminalCaseCollRm.Remove(CriminalCaseSingle);

        //                                            }
        //                                            if (Collection.IsRemoveCharges == true)
        //                                            {
        //                                                #region Removing/Adding Charges to Current criminal Case
        //                                                if (ChargeCollRm.Count > 0)
        //                                                {
        //                                                    strLog += DateTime.Now.ToString() + "  Line No ----------------------12636 Charges added : " + Convert.ToString(ChargeCollRm.Count) + "\n";
        //                                                    CriminalCaseSingle.Descendants("Charge").Remove();
        //                                                    foreach (XElement rm in ChargeCollRm)
        //                                                    {
        //                                                        CriminalCaseSingle.Add(rm);
        //                                                    }
        //                                                }
        //                                                #endregion

        //                                                #region Logic to remove CriminalCase if all chares are removed
        //                                                if (ChargeCollRm.Count == 0)
        //                                                {
        //                                                    CriminalCaseCollRm.Remove(CriminalCaseSingle);
        //                                                }
        //                                                #endregion
        //                                            }
        //                                        }
        //                                        #endregion
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        ///
        //                        /// Here i have removed all empty CriminalCases
        //                        if (Collection.IsRemoveCharges == true)
        //                        {
        //                            strLog += DateTime.Now.ToString() + "  Line No ----------------------12664 Charges added : " + Convert.ToString(CriminalCaseCollRm.Count) + "\n";
        //                            xelementCriminalReport.First().RemoveAll();
        //                            foreach (XElement rm in CriminalCaseCollRm)
        //                            {
        //                                xelementCriminalReport.First().Add(rm);
        //                            }
        //                        }
        //                        // INT-456 Auto review, to count Criminal case 
        //                        HttpContext.Current.Session["CriminalCaseCountResponse"] = 0;
        //                        HttpContext.Current.Session["CriminalCaseCountResponse"] = CriminalCaseCollRm.Count;
        //                    }
        //                }
        //                #endregion
        //            }
        //            else if (ProductCode.Trim().ToUpper() == "SCR")
        //            {
        //                #region SCR
        //                ///
        //                /// In SCR we have 2 type of tags.<reslut>/<CriminalCase>
        //                /// First I have applied check for <CriminalCase>
        //                ///
        //                List<XElement> xelementCriminalReport = ObjXDocument.Descendants("CriminalReport").ToList();
        //                List<XElement> xElementresults = ObjXDocument.Descendants("results").ToList<XElement>();
        //                if (xelementCriminalReport != null)
        //                {
        //                    if (xelementCriminalReport.Count > 0)
        //                    {
        //                    }
        //                    ///
        //                    /// If Response contain result tag
        //                    ///
        //                    else if (xElementresults != null && xElementresults.Count > 0)
        //                    {
        //                        List<XElement> resultColl, resultCollRm;
        //                        resultColl = ObjXDocument.Descendants("result").ToList();
        //                        resultCollRm = ObjXDocument.Descendants("result").ToList();

        //                        XElement CriminalCaseSingle = null;
        //                        if (resultColl != null && resultColl.Count > 0)
        //                        {
        //                            for (int i = 0; i < resultColl.Count; i++)
        //                            {
        //                                CriminalCaseSingle = resultColl.ElementAt(i);
        //                                List<XElement> RegionColl = CriminalCaseSingle.Descendants("state").ToList<XElement>();
        //                                if (RegionColl != null && RegionColl.Count > 0)
        //                                {
        //                                    bool isStateValid = false;
        //                                    string StateName = "";
        //                                    foreach (XElement Region in RegionColl)
        //                                    {
        //                                        StateName = Region.Value;
        //                                        isStateValid = (StateNameColl.Where(d => d.ToLower() == StateName.ToLower()).Count() > 0) ? true : false;
        //                                        if (isStateValid)
        //                                            break;
        //                                    }
        //                                    if (isStateValid)
        //                                    {
        //                                        string strDisposition = string.Empty;
        //                                        int flag = 1;
        //                                        var dispositionlst = CriminalCaseSingle.Descendants("disposition").ToList();

        //                                        if (dispositionlst.Count > 0)
        //                                        {
        //                                            for (int iComment = 0; iComment < dispositionlst.Count; iComment++)
        //                                            {
        //                                                //string disposition = dispositionlst.ElementAt(iComment).Value;
        //                                                strDisposition = dispositionlst.ElementAt(iComment).Value;
        //                                                //using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
        //                                                //{
        //                                                if (!string.IsNullOrEmpty(strDisposition))
        //                                                {
        //                                                    string Desposition = strDisposition.ToLower();
        //                                                    while (Desposition.IndexOf(" ") >= 0)
        //                                                    {
        //                                                        Desposition = Desposition.Replace(" ", "");
        //                                                    }

        //                                                    for (int r = 0; r < s.Count(); r++)
        //                                                    {
        //                                                        if (string.IsNullOrEmpty(s[r]) == false)
        //                                                        {
        //                                                            if (Desposition.ToLower().Contains(s[r].ToLower()))
        //                                                            {
        //                                                                flag = 2;
        //                                                            }
        //                                                        }
        //                                                    }
        //                                                }
        //                                                else
        //                                                {
        //                                                    flag = 2;
        //                                                }
        //                                                //}
        //                                            }
        //                                        }
        //                                        List<XElement> ChargeColl;
        //                                        ChargeColl = CriminalCaseSingle.Descendants("dispositionDate").ToList<XElement>();
        //                                        if (ChargeColl != null && ChargeColl.Count > 0)
        //                                        {
        //                                            foreach (XElement xe in ChargeColl)
        //                                            {
        //                                                string str = xe.Value;
        //                                                DateTime dt;
        //                                                str = (str != "" && str.Length > 9) ? str.Substring(0, 10) : DateTime.Now.ToShortDateString();
        //                                                DateTime.TryParse(str, out dt);


        //                                                int UpdateYear = Year_new * (-1);
        //                                                DateTime NewDate = DateTime.Now.AddYears(UpdateYear);


        //                                                //if (Max_new_Year != null)
        //                                                //{
        //                                                //    if (Max_new_Year >= NewDate) { MaxDate = true; }
        //                                                //    else { MaxDate = false; }
        //                                                //}
        //                                                //else { MaxDate = true; }
        //                                                //if (Min_new_Year != null)
        //                                                //{
        //                                                //    if (Min_new_Year <= NewDate) { MinDate = true; }
        //                                                //    else { MinDate = false; }
        //                                                //}
        //                                                //else
        //                                                //{
        //                                                //    MinDate = true;
        //                                                //}

        //                                                if (CheckCase == 1)
        //                                                {
        //                                                    if (dt < DateTime.Now.AddYears(UpdateYear))
        //                                                    {
        //                                                        if (flag == 2)
        //                                                        {
        //                                                            IsEmergeReviewStatus = true;
        //                                                        }
        //                                                        if (flag != 2)
        //                                                        {
        //                                                            if (Collection.IsRemoveCharges == true)
        //                                                            {
        //                                                                resultCollRm.Remove(CriminalCaseSingle);
        //                                                                break;
        //                                                            }
        //                                                        }
        //                                                    }
        //                                                }


        //                                                if (CheckCase == 2)
        //                                                {
        //                                                    if (Max_new_Year <= NewDate)
        //                                                    {

        //                                                        if (dt < Max_new_Year)
        //                                                        {
        //                                                            if (flag == 2)
        //                                                            {
        //                                                                IsEmergeReviewStatus = true;
        //                                                            }
        //                                                            if (flag != 2)
        //                                                            {
        //                                                                if (Collection.IsRemoveCharges == true)
        //                                                                {
        //                                                                    resultCollRm.Remove(CriminalCaseSingle);
        //                                                                    break;
        //                                                                }
        //                                                            }
        //                                                        }
        //                                                    }
        //                                                    else
        //                                                    {
        //                                                        if (dt < DateTime.Now.AddYears(UpdateYear))
        //                                                        {
        //                                                            if (flag == 2)
        //                                                            {
        //                                                                IsEmergeReviewStatus = true;
        //                                                            }
        //                                                            if (flag != 2)
        //                                                            {
        //                                                                if (Collection.IsRemoveCharges == true)
        //                                                                {
        //                                                                    resultCollRm.Remove(CriminalCaseSingle);
        //                                                                    break;
        //                                                                }
        //                                                            }
        //                                                        }

        //                                                    }
        //                                                }

        //                                                if (CheckCase == 3)
        //                                                {
        //                                                    if (Min_new_Year <= dt && dt <= Max_new_Year)
        //                                                    {
        //                                                        if (flag == 2)
        //                                                        {
        //                                                            IsEmergeReviewStatus = true;
        //                                                        }
        //                                                        if (flag != 2)
        //                                                        {
        //                                                            if (Collection.IsRemoveCharges == true)
        //                                                            {
        //                                                                resultCollRm.Remove(CriminalCaseSingle);
        //                                                                break;
        //                                                            }
        //                                                        }
        //                                                    }
        //                                                }



        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        ///
        //                        /// Here i have removed all empty CriminalCases
        //                        ///
        //                        if (Collection.IsRemoveCharges == true)
        //                        {
        //                            xElementresults.First().RemoveAll();
        //                            foreach (XElement rm in resultCollRm)
        //                            {
        //                                xElementresults.First().Add(rm);
        //                            }
        //                        }

        //                    }
        //                }

        //                #endregion
        //            }
        //            else if (ProductCode.Trim().ToUpper() == "NCR4")
        //            {
        //                #region NCR4

        //                List<XElement> XECriminalSearchResults = ObjXDocument.Descendants("CriminalSearchResults").ToList<XElement>();
        //                if (XECriminalSearchResults != null)
        //                {
        //                    if (XECriminalSearchResults.Count > 0)
        //                    {
        //                        List<XElement> JurisdictionColl, JurisdictionCollRm;
        //                        JurisdictionColl = ObjXDocument.Descendants("JurisdictionResults").ToList();
        //                        JurisdictionCollRm = ObjXDocument.Descendants("JurisdictionResults").ToList();

        //                        XElement JurisdictionSingle = null;
        //                        if (JurisdictionColl != null && JurisdictionColl.Count > 0)
        //                        {

        //                            for (int i = 0; i < JurisdictionColl.Count; i++)
        //                            {
        //                                JurisdictionSingle = JurisdictionColl.ElementAt(i);
        //                                string strDisposition = string.Empty;
        //                                List<XElement> ChargeColl = JurisdictionSingle.Descendants("Charge").ToList<XElement>();
        //                                if (ChargeColl != null && ChargeColl.Count > 0)
        //                                {
        //                                    bool isStateValid = false;
        //                                    string CountyName = "";

        //                                    foreach (XElement XECharge in ChargeColl)
        //                                    {
        //                                        XElement XEChrgCountyName = XECharge.Descendants("ChrgCountyName").FirstOrDefault();
        //                                        if (XEChrgCountyName != null)
        //                                        {
        //                                            CountyName = XEChrgCountyName.Value;
        //                                            ///
        //                                            /// We have county name and then apply check for statename/statecode. 
        //                                            ///
        //                                            int stateId = 0;
        //                                            if (CountyName != "")
        //                                            {
        //                                                stateId = ObjCountyColl.Where(d => d.CountyName.ToLower() == CountyName.ToLower()).Select(d => d.fkStateId).FirstOrDefault();
        //                                                if (stateId != 0)
        //                                                {
        //                                                    isStateValid = (ObjStateColl.Where(d => d.pkStateId == stateId).Count() > 0) ? true : false;
        //                                                }
        //                                            }
        //                                            if (isStateValid)
        //                                                break;
        //                                        }
        //                                    }
        //                                    if (isStateValid)
        //                                    {
        //                                        XElement XEDispDate = JurisdictionSingle.Descendants("DispDate").FirstOrDefault();
        //                                        XElement Disposition = JurisdictionSingle.Descendants("Disposition").FirstOrDefault();
        //                                        int flag = 1;
        //                                        //using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
        //                                        //{
        //                                        if (Disposition != null)
        //                                        {
        //                                            string Despostioncheck = string.Empty;
        //                                            Despostioncheck = Disposition.Value;
        //                                            if (!string.IsNullOrEmpty(Despostioncheck))
        //                                            {
        //                                                string Desposition = Despostioncheck.ToLower();
        //                                                while (Desposition.IndexOf(" ") >= 0)
        //                                                {
        //                                                    Desposition = Desposition.Replace(" ", "");
        //                                                }

        //                                                for (int r = 0; r < s.Count(); r++)
        //                                                {
        //                                                    if (string.IsNullOrEmpty(s[r]) == false)
        //                                                    {
        //                                                        if (Desposition.ToLower().Contains(s[r].ToLower()))
        //                                                        {
        //                                                            flag = 2;
        //                                                        }
        //                                                    }
        //                                                }
        //                                            }
        //                                            else
        //                                            {
        //                                                flag = 2;
        //                                            }
        //                                        }
        //                                        //}
        //                                        if (XEDispDate != null)
        //                                        {
        //                                            string str = XEDispDate.Value;
        //                                            DateTime dt;//20070625
        //                                            if (str != "" && str.Length > 7)
        //                                            {
        //                                                str = str.Substring(0, 4) + "/" + str.Substring(4, 2) + "/" + str.Substring(6, 2);
        //                                            }
        //                                            else
        //                                            {
        //                                                str = DateTime.Now.ToShortDateString();
        //                                            }
        //                                            DateTime.TryParse(str, out dt);

        //                                            int UpdateYear = Year_new * (-1);
        //                                            DateTime NewDate = DateTime.Now.AddYears(UpdateYear);


        //                                            //if (Max_new_Year != null)
        //                                            //{
        //                                            //    if (Max_new_Year >= NewDate) { MaxDate = true; }
        //                                            //    else { MaxDate = false; }
        //                                            //}
        //                                            //else { MaxDate = true; }
        //                                            //if (Min_new_Year != null)
        //                                            //{
        //                                            //    if (Min_new_Year <= NewDate) { MinDate = true; }
        //                                            //    else { MinDate = false; }
        //                                            //}
        //                                            //else
        //                                            //{
        //                                            //    MinDate = true;
        //                                            //}

        //                                            if (CheckCase == 1)
        //                                            {
        //                                                if (dt < DateTime.Now.AddYears(UpdateYear))
        //                                                {

        //                                                    if (flag == 2)
        //                                                    {
        //                                                        IsEmergeReviewStatus = true;
        //                                                    }
        //                                                    if (flag != 2)
        //                                                    {
        //                                                        if (Collection.IsRemoveCharges == true)
        //                                                        {
        //                                                            JurisdictionCollRm.Remove(JurisdictionSingle);
        //                                                        }
        //                                                    }

        //                                                }
        //                                            }

        //                                            if (CheckCase == 2)
        //                                            {
        //                                                if (Max_new_Year <= NewDate)
        //                                                {

        //                                                    if (dt < Max_new_Year)
        //                                                    {
        //                                                        if (flag == 2)
        //                                                        {
        //                                                            IsEmergeReviewStatus = true;
        //                                                        }
        //                                                        if (flag != 2)
        //                                                        {
        //                                                            if (Collection.IsRemoveCharges == true)
        //                                                            {
        //                                                                JurisdictionCollRm.Remove(JurisdictionSingle);
        //                                                            }
        //                                                        }

        //                                                    }
        //                                                }
        //                                                else
        //                                                {
        //                                                    if (dt < DateTime.Now.AddYears(UpdateYear))
        //                                                    {
        //                                                        if (flag == 2)
        //                                                        {
        //                                                            IsEmergeReviewStatus = true;
        //                                                        }
        //                                                        if (flag != 2)
        //                                                        {
        //                                                            if (Collection.IsRemoveCharges == true)
        //                                                            {
        //                                                                JurisdictionCollRm.Remove(JurisdictionSingle);
        //                                                            }
        //                                                        }

        //                                                    }

        //                                                }
        //                                            }


        //                                            if (CheckCase == 3)
        //                                            {
        //                                                if (Min_new_Year <= dt && dt <= Max_new_Year)
        //                                                {
        //                                                    if (flag == 2)
        //                                                    {
        //                                                        IsEmergeReviewStatus = true;
        //                                                    }
        //                                                    if (flag != 2)
        //                                                    {
        //                                                        if (Collection.IsRemoveCharges == true)
        //                                                        {
        //                                                            JurisdictionCollRm.Remove(JurisdictionSingle);
        //                                                        }
        //                                                    }

        //                                                }
        //                                            }
        //                                        }
        //                                        else
        //                                        {
        //                                            if (Collection.IsRemoveCharges == true)
        //                                            {
        //                                                JurisdictionCollRm.Remove(JurisdictionSingle);
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        ///
        //                        /// Here i have removed all empty CriminalCases
        //                        ///
        //                        XElement XESearchResultCount = XECriminalSearchResults.First().Descendants("SearchResultCount").FirstOrDefault();
        //                        if (XESearchResultCount != null)
        //                        {
        //                            XESearchResultCount.Value = JurisdictionCollRm.Count.ToString();
        //                        }
        //                        if (Collection.IsRemoveCharges == true)
        //                        {
        //                            XECriminalSearchResults.First().RemoveAll();
        //                            if (XESearchResultCount != null)
        //                            {
        //                                XECriminalSearchResults.First().Add(XESearchResultCount);
        //                            }
        //                            foreach (XElement rm in JurisdictionCollRm)
        //                            {
        //                                XECriminalSearchResults.First().Add(rm);
        //                            }
        //                        }
        //                    }
        //                }

        //                #endregion
        //            }
        //            else if (ProductCode.Trim().ToUpper() == "PS2")
        //            {
        //                ///
        //                /// Date tag not found.
        //                ///
        //            }
        //            else if (ProductCode.Trim().ToUpper() == "CCR1")
        //            {
        //                #region CCR1 NON- LIVERUNNER #482
        //                //XElement ChargeDate = null;
        //                //XElement FileDate = null;
        //                //XElement DispDate = null;
        //                //foreach (XElement element in ObjXDocument.Descendants("report-detail-header").ToList())
        //                //{
        //                //    bool IS_State_Valid = false;
        //                //    XElement StateCode = element.Descendants("state").FirstOrDefault();
        //                //    if (StateCode != null)
        //                //    {
        //                //        IS_State_Valid = (StateCodeColl.Where(d => d.ToLower() == StateCode.Value.ToLower()).Count() > 0) ? true : false;
        //                //    }
        //                //    if (IS_State_Valid != false)
        //                //    {
        //                //        foreach (XElement element_rpt in element.Descendants("report-incident").ToList())
        //                //        {
        //                //            int Flag_c = 1;
        //                //            string strDisposition = string.Empty;
        //                //            int flag = 1;
        //                //            var dispositionlst = element_rpt.Descendants("disposition").ToList();

        //                //            if (dispositionlst.Count > 0)
        //                //            {
        //                //                for (int iComment = 0; iComment < dispositionlst.Count; iComment++)
        //                //                {
        //                //                    string disposition = dispositionlst.ElementAt(iComment).Value;
        //                //                    strDisposition = dispositionlst.ElementAt(iComment).Value;
        //                //                    if (!string.IsNullOrEmpty(strDisposition))
        //                //                    {
        //                //                        string Desposition = strDisposition.ToLower();
        //                //                        while (Desposition.IndexOf(" ") >= 0)
        //                //                        {
        //                //                            Desposition = Desposition.Replace(" ", "");
        //                //                        }

        //                //                        for (int r = 0; r < s.Count(); r++)
        //                //                        {
        //                //                            if (string.IsNullOrEmpty(s[r]) == false)
        //                //                            {
        //                //                                if (Desposition.ToLower().Contains(s[r].ToLower()))
        //                //                                {
        //                //                                    flag = 2;
        //                //                                }
        //                //                            }
        //                //                        }
        //                //                    }
        //                //                    else
        //                //                    {
        //                //                        flag = 2;
        //                //                    }
        //                //                }
        //                //            }
        //                //            #region charge-date

        //                //            foreach (XElement element_charge in element_rpt.Descendants("charge-date").ToList())
        //                //            {
        //                //                ChargeDate = element_charge;
        //                //                if (ChargeDate != null)
        //                //                {
        //                //                    string str = ChargeDate.Value;
        //                //                    DateTime dt;
        //                //                    if (str != "" && str.Length > 7)
        //                //                    {
        //                //                        str = str.Substring(0, 4) + "/" + str.Substring(4, 2) + "/" + str.Substring(6, 2);
        //                //                    }
        //                //                    else
        //                //                    {
        //                //                        str = DateTime.Now.ToShortDateString();
        //                //                    }
        //                //                    DateTime.TryParse(str, out dt);

        //                //                    int UpdateYear = Year_new * (-1);
        //                //                    DateTime NewDate = DateTime.Now.AddYears(UpdateYear);

        //                //                    if (CheckCase == 1)
        //                //                    {
        //                //                        if (dt < DateTime.Now.AddYears(UpdateYear))
        //                //                        {
        //                //                            Flag_c = 2;
        //                //                        }
        //                //                    }
        //                //                    if (CheckCase == 2)
        //                //                    {
        //                //                        if (Max_new_Year <= NewDate)
        //                //                        {
        //                //                            if (dt < Max_new_Year)
        //                //                            {
        //                //                                Flag_c = 2;
        //                //                            }
        //                //                        }
        //                //                        else
        //                //                        {
        //                //                            if (dt < DateTime.Now.AddYears(UpdateYear))
        //                //                            {
        //                //                                Flag_c = 2;
        //                //                            }
        //                //                        }
        //                //                    }
        //                //                    if (CheckCase == 3)
        //                //                    {
        //                //                        if (Min_new_Year <= dt && dt <= Max_new_Year)
        //                //                        {
        //                //                            Flag_c = 2;
        //                //                        }
        //                //                    }
        //                //                }
        //                //            }
        //                //            #endregion
        //                //            #region file-date
        //                //            foreach (XElement element_file in element_rpt.Descendants("file-date").ToList())
        //                //            {
        //                //                FileDate = element_file;
        //                //                if (FileDate != null)
        //                //                {
        //                //                    string str = FileDate.Value;
        //                //                    DateTime dt;
        //                //                    if (str != "" && str.Length > 7)
        //                //                    {
        //                //                        str = str.Substring(0, 4) + "/" + str.Substring(4, 2) + "/" + str.Substring(6, 2);
        //                //                    }
        //                //                    else
        //                //                    {
        //                //                        str = DateTime.Now.ToShortDateString();
        //                //                    }
        //                //                    DateTime.TryParse(str, out dt);
        //                //                    int UpdateYear = Year_new * (-1);
        //                //                    DateTime NewDate = DateTime.Now.AddYears(UpdateYear);
        //                //                    if (CheckCase == 1)
        //                //                    {
        //                //                        if (dt < DateTime.Now.AddYears(UpdateYear))
        //                //                        {
        //                //                            Flag_c = 2;
        //                //                        }
        //                //                    }
        //                //                    if (CheckCase == 2)
        //                //                    {
        //                //                        if (Max_new_Year <= NewDate)
        //                //                        {
        //                //                            if (dt < Max_new_Year)
        //                //                            {
        //                //                                Flag_c = 2;
        //                //                            }
        //                //                        }
        //                //                        else
        //                //                        {
        //                //                            if (dt < DateTime.Now.AddYears(UpdateYear))
        //                //                            {
        //                //                                Flag_c = 2;
        //                //                            }
        //                //                        }
        //                //                    }
        //                //                    if (CheckCase == 3)
        //                //                    {
        //                //                        if (Min_new_Year <= dt && dt <= Max_new_Year)
        //                //                        {
        //                //                            Flag_c = 2;
        //                //                        }
        //                //                    }
        //                //                }
        //                //            }
        //                //            #endregion
        //                //            #region disposition-date
        //                //            foreach (XElement element_disp in element_rpt.Descendants("disposition-date").ToList())
        //                //            {
        //                //                DispDate = element_disp;
        //                //                if (DispDate != null)
        //                //                {
        //                //                    string str = DispDate.Value;
        //                //                    DateTime dt;
        //                //                    if (str != "" && str.Length > 7)
        //                //                    {
        //                //                        str = str.Substring(0, 4) + "/" + str.Substring(4, 2) + "/" + str.Substring(6, 2);
        //                //                    }
        //                //                    else
        //                //                    {
        //                //                        str = DateTime.Now.ToShortDateString();
        //                //                    }
        //                //                    DateTime.TryParse(str, out dt);
        //                //                    int UpdateYear = Year_new * (-1);
        //                //                    DateTime NewDate = DateTime.Now.AddYears(UpdateYear);
        //                //                    if (CheckCase == 1)
        //                //                    {
        //                //                        if (dt < DateTime.Now.AddYears(UpdateYear))
        //                //                        {
        //                //                            Flag_c = 2;
        //                //                        }
        //                //                    }
        //                //                    if (CheckCase == 2)
        //                //                    {
        //                //                        if (Max_new_Year <= NewDate)
        //                //                        {
        //                //                            if (dt < Max_new_Year)
        //                //                            {
        //                //                                Flag_c = 2;
        //                //                            }
        //                //                        }
        //                //                        else
        //                //                        {
        //                //                            if (dt < DateTime.Now.AddYears(UpdateYear))
        //                //                            {
        //                //                                Flag_c = 2;
        //                //                            }
        //                //                        }
        //                //                    }
        //                //                    if (CheckCase == 3)
        //                //                    {
        //                //                        if (Min_new_Year <= dt && dt <= Max_new_Year)
        //                //                        {
        //                //                            Flag_c = 2;
        //                //                        }
        //                //                    }
        //                //                }
        //                //            }
        //                //            #endregion
        //                //            if (Flag_c == 2)
        //                //            {
        //                //                //element.Remove();
        //                //                //IsEmergeReviewStatus = true;
        //                //                if (flag == 2)
        //                //                {
        //                //                    IsEmergeReviewStatus = true;
        //                //                }
        //                //                if (flag != 2)
        //                //                {
        //                //                    if (Collection.IsRemoveCharges == true)
        //                //                    {
        //                //                        element.Remove();
        //                //                        break;
        //                //                    }
        //                //                }
        //                //            }
        //                //        }
        //                //    }
        //                //}
        //                #endregion
        //            }
        //            else if (ProductCode.Trim().ToUpper() == "NCR2")
        //            {
        //                ///
        //                /// xslt shows html CDATA.
        //                ///
        //            }
        //            if (Collection.IsEmergeReview == true)
        //            {
        //                if (IsEmergeReviewStatus == true)
        //                {

        //                    using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
        //                    {
        //                        tblOrderDetail ObjOrderDetails = ObjEmergeDALDataContext.tblOrderDetails.Where(db => db.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
        //                        if (ObjOrderDetails != null)
        //                        {
        //                            tblOrder objtblOrder = ObjEmergeDALDataContext.tblOrders.Where(db => db.pkOrderId == ObjOrderDetails.fkOrderId).FirstOrDefault();
        //                            if (objtblOrder.IsReviewed == false && ObjOrderDetails.ReviewStatus == 0)
        //                            {
        //                                bool Is6MonthOld = false;
        //                                BALGeneral ObjBALGeneral = new BALGeneral();
        //                                Is6MonthOld = ObjBALGeneral.Is6MonthOld(ObjOrderDetails.fkOrderId);
        //                                int? OrderId = ObjOrderDetails.fkOrderId;
        //                                int? pkOrderDetail_Id = pkOrderDetailId;
        //                                Email_send(OrderId, pkOrderDetail_Id);
        //                                UpdateReportReviewStatus(pkOrderDetail_Id, 1, string.Empty, Is6MonthOld);
        //                            }
        //                        }
        //                    }

        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //Added log for exception handling.
        //        VendorServices.GeneralService.WriteLog("Some Error Occured in GetHidedResponse(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
        //        BALGeneral.WriteLogForDeletion(" Some Error Occured in GetHidedResponse(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, LogFilename);
        //    }
        //    strLog += DateTime.Now.ToString() + "  Line No ----------------------13416 End GetHidedResponse\n";
        //    BALGeneral.WriteLogForDeletion(strLog, LogFilename);
        //    ObjXDocumentupdated = ObjXDocument;
        //    IsShowRawData = _IsShowRawData;
        //    return ObjXDocumentupdated;
        //}

        public XDocument GetHidedResponse(string XMLresp, int? pkOrderDetailId, byte WatchListStatus, string ProductCode, DateTime orderdt, out bool IsShowRawData)
        {

            string strLog = string.Empty;//For Report Log.
            string LogFilename = "XMLFilteration_XMLCaseDeletionLog_" + DateTime.Now.ToString("yyMMdd");
            strLog += DateTime.Now.ToString() + " Method Started : GetHidedResponse for OrderDetailId " + Convert.ToString(pkOrderDetailId);
            //lststates.Add("CA");
            //lststates.Add("KS");
            //lststates.Add("ME");
            //lststates.Add("MD");
            //lststates.Add("MA");
            //lststates.Add("MT");
            //lststates.Add("NY");
            //lststates.Add("NH");
            //lststates.Add("NM");
            //lststates.Add("NY");
            //lststates.Add("WA");
            bool isValidFlag = false;
            string Filter_Id = "1";
            List<string> s = new List<string>();
            BALCompany ObjBALCompany = new BALCompany();
            List<tblState> ObjtblState = new List<tblState>();
            bool Is7YearFilterApplyForAllCriminalResult = Convert.ToBoolean(HttpContext.Current.Session["Is7YearFilterApplyForAllCriminalResult"]);//getting the status of Enable 7-year filter on all criminal results checkbox at customer end side
            Dictionary<string, bool> Find7YearStatus = ObjBALCompany.Get7YearFilterStatus(Convert.ToInt32(HttpContext.Current.Session["pkCompanyId"]),pkOrderDetailId);//getting status of 7yearFilter and 7YearFilterAuto 
            bool Auto7YearFilter = Find7YearStatus.FirstOrDefault(x => x.Key == "Auto").Value;
            bool Mannul7YearFilter = Find7YearStatus.FirstOrDefault(x => x.Key == "Manual").Value;
            if (Auto7YearFilter)
            {
                isValidFlag = true;
            }
            else
            {
                if (Mannul7YearFilter && Is7YearFilterApplyForAllCriminalResult)
                {
                    isValidFlag = true;
                }
                else
                {
                    isValidFlag = false;
                }
            }
            var Collection = ObjBALCompany.GetFiltersAccordingFilterId(Filter_Id).FirstOrDefault();//Getting filter rules 7 Year Filter.
            //bool MaxDate = false;
            //bool MinDate = false;
            bool flagforLiveRunnerCounty = false;//used for LOS ANGELES county --INT-287 as per Greg comments on Nov 27, 2015.
            string Year = Collection.Year.ToString();
            string MaxNewYear = Collection.TimeFrameFrom.ToString();
            string MinNewYear = Collection.TimeFrameIn.ToString();
            int CheckCase = 0;
            int Year_new = 0;
            int? tempflagYear = null;
            bool additionalFilterforNewYork = false;
            DateTime? Max_new_Year = null;
            DateTime? Min_new_Year = null;
            if (string.IsNullOrEmpty(Year) == false) { Year_new = int.Parse(Year); }
            if (string.IsNullOrEmpty(MaxNewYear) == false) { Max_new_Year = DateTime.Parse(MaxNewYear); }
            if (string.IsNullOrEmpty(MinNewYear) == false) { Min_new_Year = DateTime.Parse(MinNewYear); }
            if (Collection.Year != null) { Year_new = int.Parse(Collection.Year.ToString()); tempflagYear = 1; }
            //Case: 1 When Year is present but MaxYear and MinYear both null
            if (tempflagYear != null && Max_new_Year == null && Min_new_Year == null)
            {
                CheckCase = 1;
            }
            //Case: 2 When Year is present but One is Present from MaxYear and MinYear 
            if (tempflagYear != null && Max_new_Year != null && Min_new_Year == null)
            {
                CheckCase = 2;
            }
            //Case: 3 When Year, MaxYear and MinYear all are present
            if (tempflagYear != null && Max_new_Year != null && Min_new_Year != null)
            {
                CheckCase = 3;
            }
            if (Collection != null)
            {
                if (string.IsNullOrEmpty(Collection.Keywords) == false)
                {
                    var coll = Collection.Keywords.ToString();
                    var ArrayCollection = coll.Split(',');
                    if (ArrayCollection.Count() > 0)
                    {
                        for (int j = 0; j < ArrayCollection.Count(); j++)
                        {
                            s.Add(ArrayCollection[j].Trim().ToUpper().Replace(" ", ""));//Adding all keywords in List<string> s = new List<string>();
                        }
                    }
                }
            }
            var ListState = new List<StateName>();
            if (Collection != null)
            {

                #region---Used to 7 year auto filters from company edit page.
                Collection.IsEnableForAllState = get7yearAutoFiltersFromCompanyEditPage(Convert.ToInt32(HttpContext.Current.Session["pkCompanyId"]));
                #endregion
                if (string.IsNullOrEmpty(Collection.StateId) == false && Collection.IsEnableForAllState == false)
                {
                    string FilterId = Collection.StateId;
                    List<tblState> State = new List<tblState>();
                    var Coll = FilterId.Split(',');
                    if (Coll.Length > 0)
                    {
                        for (int r = 0; r < Coll.Length; r++)
                        {
                            if (string.IsNullOrEmpty(Coll[r]) == false)
                            {
                                State.Add(new tblState { pkStateId = int.Parse(Coll[r]) });
                            }
                        }
                    }
                    ListState = ObjBALCompany.GetStateName(State); //Getting Collection of State According to the 7 Year Filter rule. 
                }
                else
                {
                    ListState = ObjBALCompany.GetStateNameForEnable7YearFilter(); // for ticket #85 if 7 Year filter enable, get all state
                }
            }
            if (ListState.Count > 0)
            {
                for (int d = 0; d < ListState.Count(); d++)
                {
                    ObjtblState.Add(new tblState { StateCode = ListState.ElementAt(d).StateCode, StateName = ListState.ElementAt(d).State_Name, pkStateId = ListState.ElementAt(0).pkStateId });
                }
            }
            bool _IsShowRawData = true;
            List<string> StateCodeColl = new List<string>();
            List<string> StateNameColl = new List<string>();
            List<tblState> ObjStateColl = new List<tblState>();
            List<tblCounty> ObjCountyColl = new List<tblCounty>();

            XDocument ObjXDocumentupdated = new XDocument();
            XDocument ObjXDocument = new XDocument();

            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    StateCodeColl = ObjtblState.Select(x => x.StateCode).ToList(); //Assigning StateCode According to the 7 Year Filter rule. 
                    StateNameColl = ObjtblState.Select(x => x.StateName).ToList(); //Assigning StateName According to the 7 Year Filter rule. 
                    ObjStateColl = ObjtblState.ToList<tblState>(); //Assigning StateName According to the 7 Year Filter rule. 


                    ObjCountyColl = DX.tblCounties.ToList<tblCounty>();
                    bool IsEmergeReviewStatus = false;
                    if (ProductCode.Trim().ToUpper() == "CCR1LiveRunner".Trim().ToUpper())
                    {
                        ProductCode = "RCX";
                    }
                    string XML_Value = GetRequiredXmlConditional(XMLresp, ProductCode.Trim().ToUpper());
                    ObjXDocument = XDocument.Parse(XML_Value);
                    if (ProductCode.Trim().ToUpper() == "NCR1" || ProductCode.Trim().ToUpper() == "NCR+" || ProductCode.Trim().ToUpper() == "RCX")
                    {

                        #region NCR1, NCR+, RCX
                        List<XElement> xelementCriminalReport = ObjXDocument.Descendants("CriminalReport").ToList();
                        strLog += DateTime.Now.ToString() + "  Line No ----------------------11846 CriminalReport records : " + Convert.ToString(xelementCriminalReport.Count()) + "\n";
                        if (xelementCriminalReport != null)
                        {
                            if (xelementCriminalReport.Count > 0)
                            {
                                List<XElement> CriminalCaseColl, CriminalCaseCollRm;
                                CriminalCaseColl = ObjXDocument.Descendants("CriminalCase").ToList();
                                CriminalCaseCollRm = ObjXDocument.Descendants("CriminalCase").ToList();

                                strLog += DateTime.Now.ToString() + "  Line No ----------------------11855 CriminalCase : " + Convert.ToString(CriminalCaseColl.Count()) + "\n";
                                XElement CriminalCaseSingle = null;
                                if (CriminalCaseColl != null && CriminalCaseColl.Count > 0)
                                {

                                    for (int i = 0; i < CriminalCaseColl.Count; i++)
                                    {
                                        bool IsCourtTag = false;
                                        CriminalCaseSingle = CriminalCaseColl.ElementAt(i);

                                        List<XElement> RegionColl;
                                        RegionColl = CriminalCaseSingle.Descendants("CourtName").ToList<XElement>();
                                        var CaseFileDate = CriminalCaseSingle.Descendants("CaseFileDate").FirstOrDefault();
                                        IsCourtTag = true;

                                        if (RegionColl == null || RegionColl.Count == 0)
                                        {
                                            RegionColl = CriminalCaseSingle.Descendants("Region").ToList<XElement>();
                                            IsCourtTag = false;
                                        }
                                        if (RegionColl != null && RegionColl.Count > 0)
                                        {
                                            bool isStateValid = false;
                                            string StateCode = "";
                                            string CourtName = "";
                                            if (IsCourtTag)
                                            {
                                                CourtName = RegionColl.ElementAt(0).Value;
                                                //As per the Greg comments  in JIRA on Nov 27, 2015
                                                //If los angeles county is comming and no charge found in the response.
                                                //Then records are not being filtered out from the report.
                                                if (CourtName.ToLower().Contains("los angeles"))
                                                {

                                                    string str = string.Empty;
                                                    int flag = 0;
                                                    DateTime dt;
                                                    if (CaseFileDate != null)
                                                    {
                                                        str = CaseFileDate.Value;
                                                        str = (str != "" && str.Length > 9) ? str.Substring(0, 10) : DateTime.Now.ToShortDateString();
                                                        DateTime.TryParse(str, out dt);
                                                        int UpdateYear = Year_new * (-1);
                                                        DateTime NewDate = DateTime.Now.AddYears(UpdateYear);
                                                        if (CheckCase == 1)
                                                        {
                                                            if (dt < DateTime.Now.AddYears(UpdateYear))
                                                            {
                                                                #region #800: 7 YEAR STATES, Comment #31
                                                                if (flag == 2)
                                                                {
                                                                    IsEmergeReviewStatus = true;
                                                                }

                                                                #endregion
                                                                if (flag != 2)
                                                                {
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {

                                                                        flagforLiveRunnerCounty = false;
                                                                    }
                                                                    else
                                                                    {
                                                                        flagforLiveRunnerCounty = true;//Only for Live runner.
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                flagforLiveRunnerCounty = true;//Only for Live runner.
                                                            }
                                                        }
                                                        if (CheckCase == 2)
                                                        {
                                                            if (Max_new_Year <= NewDate)
                                                            {

                                                                if (dt < Max_new_Year)
                                                                {
                                                                    #region #800: 7 YEAR STATES, Comment #31
                                                                    if (flag == 2)
                                                                    {
                                                                        IsEmergeReviewStatus = true;
                                                                    }

                                                                    #endregion
                                                                    if (flag != 2)
                                                                    {
                                                                        if (Collection.IsRemoveCharges == true)
                                                                        {

                                                                            flagforLiveRunnerCounty = false;
                                                                        }
                                                                        else
                                                                        {
                                                                            flagforLiveRunnerCounty = true;//Only for Live runner.
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (dt < DateTime.Now.AddYears(UpdateYear))
                                                                {
                                                                    #region #800: 7 YEAR STATES, Comment #31
                                                                    if (flag == 2)
                                                                    {
                                                                        IsEmergeReviewStatus = true;
                                                                    }


                                                                    #endregion
                                                                    if (flag != 2)
                                                                    {
                                                                        if (Collection.IsRemoveCharges == true)
                                                                        {

                                                                            flagforLiveRunnerCounty = false;
                                                                        }
                                                                        else
                                                                        {
                                                                            flagforLiveRunnerCounty = true;//Only for Live runner.
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    flagforLiveRunnerCounty = true;//Only for Live runner.
                                                                }
                                                            }
                                                        }
                                                        if (CheckCase == 3)
                                                        {
                                                            if (Min_new_Year <= dt && dt <= Max_new_Year)
                                                            {
                                                                #region #800: 7 YEAR STATES, Comment #31
                                                                if (flag == 2)
                                                                {
                                                                    IsEmergeReviewStatus = true;
                                                                }

                                                                #endregion
                                                                if (flag != 2)
                                                                {
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {

                                                                        flagforLiveRunnerCounty = false;
                                                                    }
                                                                    else
                                                                    {
                                                                        flagforLiveRunnerCounty = true;//Only for Live runner.
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                flagforLiveRunnerCounty = true;//Only for Live runner.
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        flagforLiveRunnerCounty = true;//Only for Live runner.
                                                    }
                                                }
                                                if (CourtName.Contains("-"))
                                                {
                                                    string[] Getstatename = CourtName.Split('-');
                                                    if (Getstatename.Count() > 1)
                                                    {
                                                        if (!GetPackageFor7YearFilter(pkOrderDetailId))
                                                        {
                                                            if (!GetPackageFor10YearFilter(pkOrderDetailId))
                                                            {
                                                                //INT-256 as per Greg comments on JIRA We need to create the following logic: 
                                                                //IF record SOURCE = New York (any record in New York).
                                                                //THEN it should consider the "ADDITIONALEVENTS" tag for the 7-year filter.
                                                                //It should look at the most recent date to calculate.
                                                                if (Getstatename[1].Trim().ToUpper() == "NEW YORK")
                                                                {
                                                                    isStateValid = true;//It should me implicitly filter state as per additional item date.
                                                                    additionalFilterforNewYork = true;
                                                                    strLog += DateTime.Now.ToString() + "  Line No ----------------------11900 New York found \n";
                                                                }
                                                                else
                                                                {
                                                                    isStateValid = (ObjStateColl.Where(d => d.StateName.ToLower() == Getstatename[1].Trim().ToLower()).Count() > 0) ? true : false;
                                                                    additionalFilterforNewYork = false;
                                                                    strLog += DateTime.Now.ToString() + "  Line No ----------------------11906 New York Not found \n";
                                                                }
                                                            }
                                                        }
                                                    }

                                                }
                                                //===========================
                                                #region It filter When Is7Yearfilter or Is7YearAuto checked status is true
                                                if (isValidFlag)  //It filter When Is7Yearfilter or Is7YearAuto checked status is true
                                                {
                                                    #region When Is7Yearfilter or Is7YearAuto checked status is true for INT-221

                                                    List<XElement> ChargeColl, ChargeCollRm, AdditionalItemsColl;
                                                    ChargeColl = CriminalCaseSingle.Descendants("Charge").ToList<XElement>();
                                                    ChargeCollRm = CriminalCaseSingle.Descendants("Charge").ToList<XElement>();
                                                    AdditionalItemsColl = CriminalCaseSingle.Descendants("AdditionalItems").ToList<XElement>();
                                                    XElement ChargeSingle = null;
                                                    if (ChargeColl != null && ChargeColl.Count > 0)
                                                    {
                                                        foreach (XElement xe in ChargeColl)
                                                        {
                                                            ChargeSingle = xe;
                                                            string strDisposition = string.Empty;
                                                            XElement OffenseDate = ChargeSingle.Descendants("OffenseDate").FirstOrDefault();
                                                            XElement DispositionDate = ChargeSingle.Descendants("DispositionDate").FirstOrDefault();
                                                            XElement DispDate = ChargeSingle.Descendants("DispDate").FirstOrDefault();
                                                            XElement Disposition = ChargeSingle.Descendants("Disposition").FirstOrDefault();
                                                            XElement SentenceDate = ChargeSingle.Descendants("Sentence").FirstOrDefault();
                                                            int flag = 1;
                                                            if (Disposition != null)
                                                            {
                                                                string Despostioncheck = string.Empty;
                                                                Despostioncheck = Disposition.Value;
                                                                if (!string.IsNullOrEmpty(Despostioncheck))
                                                                {
                                                                    string Desposition = Despostioncheck.ToLower();
                                                                    while (Desposition.IndexOf(" ") >= 0)
                                                                    {
                                                                        Desposition = Desposition.Replace(" ", "");
                                                                    }
                                                                    for (int r = 0; r < s.Count(); r++)
                                                                    {
                                                                        if (string.IsNullOrEmpty(s[r]) == false)
                                                                        {
                                                                            if (Desposition.ToLower().Contains(s[r].ToLower()))
                                                                            {
                                                                                flag = 2;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    flag = 2;
                                                                }
                                                            }
                                                            string str = ""; DateTime dt;
                                                            if (OffenseDate != null)
                                                            {
                                                                str = OffenseDate.Value;
                                                            }
                                                            else if (DispositionDate != null)
                                                            {
                                                                str = DispositionDate.Value;
                                                            }
                                                            else if (CaseFileDate != null)
                                                            {
                                                                str = CaseFileDate.Value;
                                                            }
                                                            else if (DispDate != null)
                                                            {
                                                                str = DispDate.Value;//20070625
                                                                if (str != "" && str.Length > 7)
                                                                {
                                                                    str = str.Substring(0, 4) + "/" + str.Substring(4, 2) + "/" + str.Substring(6, 2);
                                                                }
                                                                else
                                                                {
                                                                    str = DateTime.Now.ToShortDateString();
                                                                }
                                                            }
                                                            else if (SentenceDate != null)
                                                            { // changes regarding ticket 418
                                                                string SentenceDatevalue = SentenceDate.Value;
                                                                if (SentenceDatevalue.Contains("SENTENCE DATE"))
                                                                {
                                                                    string[] splitdatevalue = SentenceDatevalue.Split(':');
                                                                    var Completedatevalue = splitdatevalue[1];
                                                                    str = Completedatevalue.Substring(0, 11).Replace("/", "");
                                                                    string str1 = str.Substring(5, 4);
                                                                    string str2 = str.Substring(1, 2);
                                                                    string str3 = str.Substring(3, 2);
                                                                    str = str1 + "/" + str2 + "/" + str3;
                                                                }
                                                            }
                                                            str = (str != "" && str.Length > 9) ? str.Substring(0, 10) : DateTime.Now.ToShortDateString();
                                                            DateTime.TryParse(str, out dt);
                                                            int UpdateYear = Year_new * (-1);
                                                            DateTime NewDate = DateTime.Now.AddYears(UpdateYear);
                                                            if (CheckCase == 1)
                                                            {
                                                                if (dt < DateTime.Now.AddYears(UpdateYear))
                                                                {
                                                                    #region #800: 7 YEAR STATES, Comment #31
                                                                    if (flag == 2)
                                                                    {
                                                                        IsEmergeReviewStatus = true;
                                                                    }
                                                                    if (_IsShowRawData)
                                                                        _IsShowRawData = false;

                                                                    #endregion
                                                                    if (flag != 2)
                                                                    {
                                                                        if (Collection.IsRemoveCharges == true)
                                                                        {

                                                                            ChargeCollRm.Remove(ChargeSingle);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            if (CheckCase == 2)
                                                            {
                                                                if (Max_new_Year <= NewDate)
                                                                {

                                                                    if (dt < Max_new_Year)
                                                                    {
                                                                        #region #800: 7 YEAR STATES, Comment #31
                                                                        if (flag == 2)
                                                                        {
                                                                            IsEmergeReviewStatus = true;
                                                                        }
                                                                        if (_IsShowRawData)
                                                                            _IsShowRawData = false;

                                                                        #endregion
                                                                        if (flag != 2)
                                                                        {
                                                                            if (Collection.IsRemoveCharges == true)
                                                                            {

                                                                                ChargeCollRm.Remove(ChargeSingle);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    if (dt < DateTime.Now.AddYears(UpdateYear))
                                                                    {
                                                                        #region #800: 7 YEAR STATES, Comment #31
                                                                        if (flag == 2)
                                                                        {
                                                                            IsEmergeReviewStatus = true;
                                                                        }
                                                                        if (_IsShowRawData)
                                                                            _IsShowRawData = false;

                                                                        #endregion
                                                                        if (flag != 2)
                                                                        {
                                                                            if (Collection.IsRemoveCharges == true)
                                                                            {

                                                                                ChargeCollRm.Remove(ChargeSingle);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            if (CheckCase == 3)
                                                            {
                                                                if (Min_new_Year <= dt && dt <= Max_new_Year)
                                                                {
                                                                    #region #800: 7 YEAR STATES, Comment #31
                                                                    if (flag == 2)
                                                                    {
                                                                        IsEmergeReviewStatus = true;
                                                                    }
                                                                    if (_IsShowRawData)
                                                                        _IsShowRawData = false;

                                                                    #endregion
                                                                    if (flag != 2)
                                                                    {
                                                                        if (Collection.IsRemoveCharges == true)
                                                                        {

                                                                            ChargeCollRm.Remove(ChargeSingle);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        //INT-287 as per Greg comments in JIRA.
                                                        //If any county belongs to Los angeles and no charges are cmoing in the response.
                                                        //then response will be displayed on view report page.
                                                        if (ProductCode == "RCX")
                                                        {
                                                            if (!flagforLiveRunnerCounty)//If Los angeles county does not exists.
                                                            {
                                                                CriminalCaseCollRm.Remove(CriminalCaseSingle);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            CriminalCaseCollRm.Remove(CriminalCaseSingle);
                                                        }
                                                    }

                                                    if (AdditionalItemsColl != null && AdditionalItemsColl.Count() > 0)//INT256 New implementation.
                                                    {
                                                        if (additionalFilterforNewYork)
                                                        {
                                                            foreach (XElement xe in AdditionalItemsColl)
                                                            {
                                                                ChargeSingle = xe;
                                                                DateTime dtNewYork;
                                                                string strDisposition = string.Empty;
                                                                //to get the Additional items date field entries.
                                                                XElement anyDate = ChargeSingle.Descendants("EffectiveDate").Descendants("StartDate").Descendants("AnyDate").FirstOrDefault();
                                                                if (anyDate != null)
                                                                {
                                                                    string[] splitdatevalueNewYork = anyDate.Value.Split('T');//To split date part from the string like <AnyDate>2000-03-18T00:00:00</AnyDate>. 
                                                                    var CompletedatevalueNewYork = splitdatevalueNewYork[0];//get only date part from the string array.
                                                                    DateTime.TryParse(splitdatevalueNewYork[0], out dtNewYork);//To verify the date format.
                                                                    int UpdateYear = Year_new * (-1);//To creating the less than 7 year date from the current date.
                                                                    if (dtNewYork < DateTime.Now.AddYears(UpdateYear))//to check date if coming date less than 7 year.
                                                                    {
                                                                        //7 YEAR STATES, Comment
                                                                        strLog += DateTime.Now.ToString() + "  Line No ----------------------12149 New York found and remove based on case \n";
                                                                        CriminalCaseCollRm.Remove(CriminalCaseSingle);//If coming date is less than 7 year than it will remove Single CriminalCaseSingle section. 
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    if (Collection.IsRemoveCharges == true)
                                                    {
                                                        #region Removing/Adding Charges to Current criminal Case
                                                        if (ChargeCollRm.Count > 0)
                                                        {
                                                            strLog += DateTime.Now.ToString() + "  Line No ----------------------12161 Removing/Adding Charges to Current criminal Case \n";
                                                            strLog += DateTime.Now.ToString() + "  Line No ----------------------12162 Charges added : " + Convert.ToString(ChargeCollRm.Count) + "\n";
                                                            CriminalCaseSingle.Descendants("Charge").Remove();
                                                            foreach (XElement rm in ChargeCollRm)
                                                            {
                                                                CriminalCaseSingle.Add(rm);
                                                            }
                                                        }
                                                        #endregion

                                                        #region Logic to remove CriminalCase if all chares are removed
                                                        if (ChargeCollRm.Count == 0)
                                                        {
                                                            //INT-287 as per Greg comments in JIRA.
                                                            //If any county belongs to Los angeles and no charges are cmoing in the response.
                                                            //then response will be displayed on view report page.
                                                            if (ProductCode == "RCX") //If Los angeles county does not exists.
                                                            {
                                                                if (!flagforLiveRunnerCounty)
                                                                {
                                                                    CriminalCaseCollRm.Remove(CriminalCaseSingle);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                CriminalCaseCollRm.Remove(CriminalCaseSingle);
                                                            }
                                                        }
                                                        #endregion
                                                    }
                                                    #endregion
                                                    #region INT221 New implementation for TrialDate.
                                                    if (AdditionalItemsColl != null && AdditionalItemsColl.Count() > 0 && CriminalCaseCollRm.Count > 0)//INT221 New implementation for TrialDate.
                                                    {

                                                        foreach (XElement xe in AdditionalItemsColl)
                                                        {
                                                            ChargeSingle = xe;
                                                            DateTime dtTrialDate;
                                                            //string strDisposition = string.Empty;
                                                            //to get the Additional items date field entries.
                                                            string AdditionalType;
                                                            if (ChargeSingle.Attribute("type") != null)
                                                            {
                                                                AdditionalType = ChargeSingle.Attribute("type").Value;

                                                                if (AdditionalType == "TrialDate")
                                                                {
                                                                    XElement TrialDate = ChargeSingle.Descendants("Text").FirstOrDefault();

                                                                    string[] splitdatevalueTrialDate = TrialDate.Value.Split('T');//To split date part from the string like <Text>2000-03-18T00:00:00</Text>. 
                                                                    var CompletedatevalueNewYorkTrialDate = splitdatevalueTrialDate[0];//get only date part from the string array.
                                                                    DateTime.TryParse(splitdatevalueTrialDate[0], out dtTrialDate);//To verify the date format.
                                                                    int UpdateYear = Year_new * (-1);//To creating the less than 7 year date from the current date.
                                                                    if (dtTrialDate < DateTime.Now.AddYears(UpdateYear))//to check date if coming date less than 7 year.
                                                                    {
                                                                        //7 YEAR STATES, Comment                                                                      
                                                                        CriminalCaseCollRm.Remove(CriminalCaseSingle);//If coming date is less than 7 year than it will remove Single CriminalCaseSingle section. 
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion

                                                }
                                                else
                                                {
                                                    #region 7 Year Filter
                                                    if (GetPackageFor7YearFilter(pkOrderDetailId))
                                                    {
                                                        strLog += DateTime.Now.ToString() + "  Line No ----------------------12184 GetPackageFor7YearFilter \n";
                                                        List<XElement> ChargeColl, ChargeCollRm, AdditionalItemsColl;
                                                        ChargeColl = CriminalCaseSingle.Descendants("Charge").ToList<XElement>();
                                                        ChargeCollRm = CriminalCaseSingle.Descendants("Charge").ToList<XElement>();
                                                        AdditionalItemsColl = CriminalCaseSingle.Descendants("AdditionalItems").ToList<XElement>();
                                                        XElement ChargeSingle = null;
                                                        if (ChargeColl != null && ChargeColl.Count > 0)
                                                        {
                                                            foreach (XElement xe in ChargeColl)
                                                            {
                                                                ChargeSingle = xe;
                                                                string strDisposition = string.Empty;
                                                                XElement OffenseDate = ChargeSingle.Descendants("OffenseDate").FirstOrDefault();
                                                                XElement DispositionDate = ChargeSingle.Descendants("DispositionDate").FirstOrDefault();
                                                                XElement DispDate = ChargeSingle.Descendants("DispDate").FirstOrDefault();
                                                                XElement Disposition = ChargeSingle.Descendants("Disposition").FirstOrDefault();
                                                                XElement SentenceDate = ChargeSingle.Descendants("Sentence").FirstOrDefault();


                                                                int flag = 1;

                                                                if (Disposition != null)
                                                                {
                                                                    string Despostioncheck = string.Empty;
                                                                    Despostioncheck = Disposition.Value;
                                                                    if (!string.IsNullOrEmpty(Despostioncheck))
                                                                    {
                                                                        string Desposition = Despostioncheck.ToLower();
                                                                        while (Desposition.IndexOf(" ") >= 0)
                                                                        {
                                                                            Desposition = Desposition.Replace(" ", "");
                                                                        }
                                                                        for (int r = 0; r < s.Count(); r++)
                                                                        {
                                                                            if (string.IsNullOrEmpty(s[r]) == false)
                                                                            {
                                                                                if (Desposition.ToLower().Contains(s[r].ToLower()))
                                                                                {
                                                                                    flag = 2;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        flag = 2;
                                                                    }
                                                                }
                                                                string str = ""; DateTime dt;
                                                                if (OffenseDate != null)
                                                                {
                                                                    str = OffenseDate.Value;
                                                                }
                                                                else if (DispositionDate != null)
                                                                {
                                                                    str = DispositionDate.Value;
                                                                }
                                                                else if (CaseFileDate != null)
                                                                {
                                                                    str = CaseFileDate.Value;
                                                                }
                                                                else if (DispDate != null)
                                                                {
                                                                    str = DispDate.Value;//20070625
                                                                    if (str != "" && str.Length > 7)
                                                                    {
                                                                        str = str.Substring(0, 4) + "/" + str.Substring(4, 2) + "/" + str.Substring(6, 2);
                                                                    }
                                                                    else
                                                                    {
                                                                        str = DateTime.Now.ToShortDateString();
                                                                    }
                                                                }
                                                                else if (SentenceDate != null)
                                                                { // changes regarding ticket 418
                                                                    string SentenceDatevalue = SentenceDate.Value;
                                                                    if (SentenceDatevalue.Contains("SENTENCE DATE"))
                                                                    {
                                                                        string[] splitdatevalue = SentenceDatevalue.Split(':');
                                                                        var Completedatevalue = splitdatevalue[1];
                                                                        str = Completedatevalue.Substring(0, 11).Replace("/", "");
                                                                        string str1 = str.Substring(5, 4);
                                                                        string str2 = str.Substring(1, 2);
                                                                        string str3 = str.Substring(3, 2);
                                                                        str = str1 + "/" + str2 + "/" + str3;
                                                                    }
                                                                }
                                                                str = (str != "" && str.Length > 9) ? str.Substring(0, 10) : DateTime.Now.ToShortDateString();
                                                                DateTime.TryParse(str, out dt);
                                                                int UpdateYear = Year_new * (-1);
                                                                DateTime NewDate = DateTime.Now.AddYears(UpdateYear);
                                                                if (CheckCase == 1)
                                                                {
                                                                    if (dt < DateTime.Now.AddYears(UpdateYear))
                                                                    {
                                                                        #region #800: 7 YEAR STATES, Comment #31
                                                                        if (flag == 2)
                                                                        {
                                                                            IsEmergeReviewStatus = true;
                                                                        }
                                                                        if (_IsShowRawData)
                                                                            _IsShowRawData = false;

                                                                        #endregion
                                                                        if (flag != 2)
                                                                        {
                                                                            if (Collection.IsRemoveCharges == true)
                                                                            {
                                                                                if (dt < DateTime.Now.AddYears(UpdateYear))
                                                                                {
                                                                                    ChargeCollRm.Remove(ChargeSingle);
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                                if (CheckCase == 2)
                                                                {
                                                                    if (Max_new_Year <= NewDate)
                                                                    {

                                                                        if (dt < Max_new_Year)
                                                                        {
                                                                            #region #800: 7 YEAR STATES, Comment #31
                                                                            if (flag == 2)
                                                                            {
                                                                                IsEmergeReviewStatus = true;
                                                                            }
                                                                            if (_IsShowRawData)
                                                                                _IsShowRawData = false;

                                                                            #endregion
                                                                            if (flag != 2)
                                                                            {
                                                                                if (Collection.IsRemoveCharges == true)
                                                                                {
                                                                                    if (dt < Max_new_Year)
                                                                                    {
                                                                                        ChargeCollRm.Remove(ChargeSingle);
                                                                                    }

                                                                                }
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            ChargeCollRm.Remove(ChargeSingle);
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        if (dt < DateTime.Now.AddYears(UpdateYear))
                                                                        {
                                                                            #region #800: 7 YEAR STATES, Comment #31
                                                                            if (flag == 2)
                                                                            {
                                                                                IsEmergeReviewStatus = true;
                                                                            }
                                                                            if (_IsShowRawData)
                                                                                _IsShowRawData = false;

                                                                            #endregion
                                                                            if (flag != 2)
                                                                            {
                                                                                if (Collection.IsRemoveCharges == true)
                                                                                {
                                                                                    if (dt < DateTime.Now.AddYears(UpdateYear))
                                                                                    {
                                                                                        ChargeCollRm.Remove(ChargeSingle);
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }

                                                                }
                                                                if (CheckCase == 3)
                                                                {
                                                                    if (Min_new_Year <= dt && dt <= Max_new_Year)
                                                                    {
                                                                        #region #800: 7 YEAR STATES, Comment #31
                                                                        if (flag == 2)
                                                                        {
                                                                            IsEmergeReviewStatus = true;
                                                                        }
                                                                        if (_IsShowRawData)
                                                                            _IsShowRawData = false;

                                                                        #endregion
                                                                        if (flag != 2)
                                                                        {
                                                                            if (Collection.IsRemoveCharges == true)
                                                                            {
                                                                                ChargeCollRm.Remove(ChargeSingle);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {

                                                            CriminalCaseCollRm.Remove(CriminalCaseSingle);

                                                        }
                                                        if (Collection.IsRemoveCharges == true)
                                                        {
                                                            #region Removing/Adding Charges to Current criminal Case
                                                            if (ChargeCollRm.Count > 0)
                                                            {
                                                                strLog += DateTime.Now.ToString() + "  Line No ----------------------12396 Charges added : " + Convert.ToString(ChargeCollRm.Count) + "\n";
                                                                CriminalCaseSingle.Descendants("Charge").Remove();
                                                                foreach (XElement rm in ChargeCollRm)
                                                                {
                                                                    CriminalCaseSingle.Add(rm);
                                                                }
                                                            }
                                                            #endregion

                                                            #region Logic to remove CriminalCase if all chares are removed
                                                            if (ChargeCollRm.Count == 0)
                                                            {
                                                                CriminalCaseCollRm.Remove(CriminalCaseSingle);
                                                            }
                                                            #endregion
                                                        }
                                                    }
                                                    #endregion

                                                    #region 10 Year Filter
                                                    if (GetPackageFor10YearFilter(pkOrderDetailId))//10 Year Filter
                                                    {
                                                        strLog += DateTime.Now.ToString() + "  Line No ----------------------12184 GetPackageFor10YearFilter \n";
                                                        Year_new = 10;
                                                        List<XElement> ChargeColl, ChargeCollRm, AdditionalItemsColl;
                                                        ChargeColl = CriminalCaseSingle.Descendants("Charge").ToList<XElement>();
                                                        ChargeCollRm = CriminalCaseSingle.Descendants("Charge").ToList<XElement>();
                                                        AdditionalItemsColl = CriminalCaseSingle.Descendants("AdditionalItems").ToList<XElement>();
                                                        XElement ChargeSingle = null;
                                                        if (ChargeColl != null && ChargeColl.Count > 0)
                                                        {
                                                            foreach (XElement xe in ChargeColl)
                                                            {
                                                                ChargeSingle = xe;
                                                                string strDisposition = string.Empty;
                                                                XElement OffenseDate = ChargeSingle.Descendants("OffenseDate").FirstOrDefault();
                                                                XElement DispositionDate = ChargeSingle.Descendants("DispositionDate").FirstOrDefault();
                                                                XElement DispDate = ChargeSingle.Descendants("DispDate").FirstOrDefault();
                                                                XElement Disposition = ChargeSingle.Descendants("Disposition").FirstOrDefault();
                                                                XElement SentenceDate = ChargeSingle.Descendants("Sentence").FirstOrDefault();


                                                                int flag = 1;

                                                                if (Disposition != null)
                                                                {
                                                                    string Despostioncheck = string.Empty;
                                                                    Despostioncheck = Disposition.Value;
                                                                    if (!string.IsNullOrEmpty(Despostioncheck))
                                                                    {
                                                                        string Desposition = Despostioncheck.ToLower();
                                                                        while (Desposition.IndexOf(" ") >= 0)
                                                                        {
                                                                            Desposition = Desposition.Replace(" ", "");
                                                                        }
                                                                        for (int r = 0; r < s.Count(); r++)
                                                                        {
                                                                            if (string.IsNullOrEmpty(s[r]) == false)
                                                                            {
                                                                                if (Desposition.ToLower().Contains(s[r].ToLower()))
                                                                                {
                                                                                    flag = 2;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        flag = 2;
                                                                    }
                                                                }

                                                                string str = ""; DateTime dt;
                                                                if (OffenseDate != null)
                                                                {
                                                                    str = OffenseDate.Value;
                                                                }
                                                                else if (DispositionDate != null)
                                                                {
                                                                    str = DispositionDate.Value;
                                                                }
                                                                else if (CaseFileDate != null)
                                                                {
                                                                    str = CaseFileDate.Value;
                                                                }
                                                                else if (DispDate != null)
                                                                {
                                                                    str = DispDate.Value;//20070625
                                                                    if (str != "" && str.Length > 7)
                                                                    {
                                                                        str = str.Substring(0, 4) + "/" + str.Substring(4, 2) + "/" + str.Substring(6, 2);
                                                                    }
                                                                    else
                                                                    {
                                                                        str = DateTime.Now.ToShortDateString();
                                                                    }
                                                                }
                                                                else if (SentenceDate != null)
                                                                { // changes regarding ticket 418
                                                                    string SentenceDatevalue = SentenceDate.Value;
                                                                    if (SentenceDatevalue.Contains("SENTENCE DATE"))
                                                                    {
                                                                        string[] splitdatevalue = SentenceDatevalue.Split(':');
                                                                        var Completedatevalue = splitdatevalue[1];
                                                                        str = Completedatevalue.Substring(0, 11).Replace("/", "");
                                                                        string str1 = str.Substring(5, 4);
                                                                        string str2 = str.Substring(1, 2);
                                                                        string str3 = str.Substring(3, 2);
                                                                        str = str1 + "/" + str2 + "/" + str3;

                                                                    }
                                                                }
                                                                str = (str != "" && str.Length > 9) ? str.Substring(0, 10) : DateTime.Now.ToShortDateString();
                                                                DateTime.TryParse(str, out dt);


                                                                int UpdateYear = Year_new * (-1);
                                                                DateTime NewDate = DateTime.Now.AddYears(UpdateYear);

                                                                if (CheckCase == 1)
                                                                {
                                                                    if (dt < DateTime.Now.AddYears(UpdateYear))
                                                                    {
                                                                        #region #800: 7 YEAR STATES, Comment #31
                                                                        if (flag == 2)
                                                                        {
                                                                            IsEmergeReviewStatus = true;
                                                                        }
                                                                        if (_IsShowRawData)
                                                                            _IsShowRawData = false;

                                                                        #endregion
                                                                        if (flag != 2)
                                                                        {
                                                                            if (Collection.IsRemoveCharges == true)
                                                                            {
                                                                                if (dt < DateTime.Now.AddYears(UpdateYear))
                                                                                {
                                                                                    ChargeCollRm.Remove(ChargeSingle);
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                                if (CheckCase == 2)
                                                                {
                                                                    if (Max_new_Year <= NewDate)
                                                                    {

                                                                        if (dt < Max_new_Year)
                                                                        {
                                                                            #region #800: 7 YEAR STATES, Comment #31
                                                                            if (flag == 2)
                                                                            {
                                                                                IsEmergeReviewStatus = true;
                                                                            }
                                                                            if (_IsShowRawData)
                                                                                _IsShowRawData = false;

                                                                            #endregion
                                                                            if (flag != 2)
                                                                            {
                                                                                if (Collection.IsRemoveCharges == true)
                                                                                {
                                                                                    if (dt < Max_new_Year)
                                                                                    {
                                                                                        ChargeCollRm.Remove(ChargeSingle);
                                                                                    }

                                                                                }
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            ChargeCollRm.Remove(ChargeSingle);
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        if (dt < DateTime.Now.AddYears(UpdateYear))
                                                                        {
                                                                            #region #800: 7 YEAR STATES, Comment #31
                                                                            if (flag == 2)
                                                                            {
                                                                                IsEmergeReviewStatus = true;
                                                                            }
                                                                            if (_IsShowRawData)
                                                                                _IsShowRawData = false;

                                                                            #endregion
                                                                            if (flag != 2)
                                                                            {
                                                                                if (Collection.IsRemoveCharges == true)
                                                                                {
                                                                                    if (dt < DateTime.Now.AddYears(UpdateYear))
                                                                                    {
                                                                                        ChargeCollRm.Remove(ChargeSingle);
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }

                                                                }
                                                                if (CheckCase == 3)
                                                                {
                                                                    if (Min_new_Year <= dt && dt <= Max_new_Year)
                                                                    {
                                                                        #region #800: 7 YEAR STATES, Comment #31
                                                                        if (flag == 2)
                                                                        {
                                                                            IsEmergeReviewStatus = true;
                                                                        }
                                                                        if (_IsShowRawData)
                                                                            _IsShowRawData = false;

                                                                        #endregion
                                                                        if (flag != 2)
                                                                        {
                                                                            if (Collection.IsRemoveCharges == true)
                                                                            {
                                                                                ChargeCollRm.Remove(ChargeSingle);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {

                                                            CriminalCaseCollRm.Remove(CriminalCaseSingle);

                                                        }
                                                        if (Collection.IsRemoveCharges == true)
                                                        {
                                                            #region Removing/Adding Charges to Current criminal Case
                                                            if (ChargeCollRm.Count > 0)
                                                            {
                                                                strLog += DateTime.Now.ToString() + "  Line No ----------------------12636 Charges added : " + Convert.ToString(ChargeCollRm.Count) + "\n";
                                                                CriminalCaseSingle.Descendants("Charge").Remove();
                                                                foreach (XElement rm in ChargeCollRm)
                                                                {
                                                                    CriminalCaseSingle.Add(rm);
                                                                }
                                                            }
                                                            #endregion

                                                            #region Logic to remove CriminalCase if all chares are removed
                                                            if (ChargeCollRm.Count == 0)
                                                            {
                                                                CriminalCaseCollRm.Remove(CriminalCaseSingle);
                                                            }
                                                            #endregion
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                foreach (XElement Region in RegionColl)
                                                {
                                                    StateCode = Region.Value;
                                                    if (!GetPackageFor7YearFilter(pkOrderDetailId))
                                                    {
                                                        if (!GetPackageFor10YearFilter(pkOrderDetailId))
                                                        {
                                                            isStateValid = (StateCodeColl.Where(d => d.ToLower() == StateCode.ToLower()).Count() > 0) ? true : false;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        isStateValid = false;
                                                        break;
                                                    }
                                                    if (isStateValid)
                                                        break;
                                                }
                                            }
                                            if (isStateValid)
                                            {
                                                List<XElement> ChargeColl, ChargeCollRm, AdditionalItemsColl;
                                                ChargeColl = CriminalCaseSingle.Descendants("Charge").ToList<XElement>();
                                                ChargeCollRm = CriminalCaseSingle.Descendants("Charge").ToList<XElement>();
                                                AdditionalItemsColl = CriminalCaseSingle.Descendants("AdditionalItems").ToList<XElement>();
                                                XElement ChargeSingle = null;
                                                if (ChargeColl != null && ChargeColl.Count > 0)
                                                {
                                                    foreach (XElement xe in ChargeColl)
                                                    {
                                                        ChargeSingle = xe;
                                                        string strDisposition = string.Empty;
                                                        XElement OffenseDate = ChargeSingle.Descendants("OffenseDate").FirstOrDefault();
                                                        XElement DispositionDate = ChargeSingle.Descendants("DispositionDate").FirstOrDefault();
                                                        XElement DispDate = ChargeSingle.Descendants("DispDate").FirstOrDefault();
                                                        XElement Disposition = ChargeSingle.Descendants("Disposition").FirstOrDefault();
                                                        XElement SentenceDate = ChargeSingle.Descendants("Sentence").FirstOrDefault();
                                                        int flag = 1;
                                                        if (Disposition != null)
                                                        {
                                                            string Despostioncheck = string.Empty;
                                                            Despostioncheck = Disposition.Value;
                                                            if (!string.IsNullOrEmpty(Despostioncheck))
                                                            {
                                                                string Desposition = Despostioncheck.ToLower();
                                                                while (Desposition.IndexOf(" ") >= 0)
                                                                {
                                                                    Desposition = Desposition.Replace(" ", "");
                                                                }
                                                                for (int r = 0; r < s.Count(); r++)
                                                                {
                                                                    if (string.IsNullOrEmpty(s[r]) == false)
                                                                    {
                                                                        if (Desposition.ToLower().Contains(s[r].ToLower()))
                                                                        {
                                                                            flag = 2;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                flag = 2;
                                                            }
                                                        }
                                                        //}

                                                        string str = ""; DateTime dt;
                                                        if (OffenseDate != null)
                                                        {
                                                            str = OffenseDate.Value;
                                                        }
                                                        else if (DispositionDate != null)
                                                        {
                                                            str = DispositionDate.Value;
                                                        }
                                                        else if (CaseFileDate != null)
                                                        {
                                                            str = CaseFileDate.Value;
                                                        }
                                                        else if (DispDate != null)
                                                        {
                                                            str = DispDate.Value;//20070625
                                                            if (str != "" && str.Length > 7)
                                                            {
                                                                str = str.Substring(0, 4) + "/" + str.Substring(4, 2) + "/" + str.Substring(6, 2);
                                                            }
                                                            else
                                                            {
                                                                str = DateTime.Now.ToShortDateString();
                                                            }
                                                        }
                                                        else if (SentenceDate != null)
                                                        { // changes regarding ticket 418
                                                            string SentenceDatevalue = SentenceDate.Value;
                                                            if (SentenceDatevalue.Contains("SENTENCE DATE"))
                                                            {
                                                                string[] splitdatevalue = SentenceDatevalue.Split(':');
                                                                var Completedatevalue = splitdatevalue[1];
                                                                str = Completedatevalue.Substring(0, 11).Replace("/", "");
                                                                string str1 = str.Substring(5, 4);
                                                                string str2 = str.Substring(1, 2);
                                                                string str3 = str.Substring(3, 2);
                                                                str = str1 + "/" + str2 + "/" + str3;
                                                            }
                                                        }
                                                        str = (str != "" && str.Length > 9) ? str.Substring(0, 10) : DateTime.Now.ToShortDateString();
                                                        DateTime.TryParse(str, out dt);
                                                        int UpdateYear = Year_new * (-1);
                                                        DateTime NewDate = DateTime.Now.AddYears(UpdateYear);
                                                        if (CheckCase == 1)
                                                        {
                                                            if (dt < DateTime.Now.AddYears(UpdateYear))
                                                            {
                                                                #region #800: 7 YEAR STATES, Comment #31
                                                                if (flag == 2)
                                                                {
                                                                    IsEmergeReviewStatus = true;
                                                                }
                                                                if (_IsShowRawData)
                                                                    _IsShowRawData = false;

                                                                #endregion
                                                                if (flag != 2)
                                                                {
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {

                                                                        ChargeCollRm.Remove(ChargeSingle);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        if (CheckCase == 2)
                                                        {
                                                            if (Max_new_Year <= NewDate)
                                                            {

                                                                if (dt < Max_new_Year)
                                                                {
                                                                    #region #800: 7 YEAR STATES, Comment #31
                                                                    if (flag == 2)
                                                                    {
                                                                        IsEmergeReviewStatus = true;
                                                                    }
                                                                    if (_IsShowRawData)
                                                                        _IsShowRawData = false;

                                                                    #endregion
                                                                    if (flag != 2)
                                                                    {
                                                                        if (Collection.IsRemoveCharges == true)
                                                                        {

                                                                            ChargeCollRm.Remove(ChargeSingle);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (dt < DateTime.Now.AddYears(UpdateYear))
                                                                {
                                                                    #region #800: 7 YEAR STATES, Comment #31
                                                                    if (flag == 2)
                                                                    {
                                                                        IsEmergeReviewStatus = true;
                                                                    }
                                                                    if (_IsShowRawData)
                                                                        _IsShowRawData = false;

                                                                    #endregion
                                                                    if (flag != 2)
                                                                    {
                                                                        if (Collection.IsRemoveCharges == true)
                                                                        {

                                                                            ChargeCollRm.Remove(ChargeSingle);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        if (CheckCase == 3)
                                                        {
                                                            if (Min_new_Year <= dt && dt <= Max_new_Year)
                                                            {
                                                                #region #800: 7 YEAR STATES, Comment #31
                                                                if (flag == 2)
                                                                {
                                                                    IsEmergeReviewStatus = true;
                                                                }
                                                                if (_IsShowRawData)
                                                                    _IsShowRawData = false;

                                                                #endregion
                                                                if (flag != 2)
                                                                {
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {

                                                                        ChargeCollRm.Remove(ChargeSingle);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    //INT-287 as per Greg comments in JIRA.
                                                    //If any county belongs to Los angeles and no charges are cmoing in the response.
                                                    //then response will be displayed on view report page.
                                                    if (ProductCode == "RCX")
                                                    {
                                                        if (!flagforLiveRunnerCounty)//If Los angeles county does not exists.
                                                        {
                                                            CriminalCaseCollRm.Remove(CriminalCaseSingle);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        CriminalCaseCollRm.Remove(CriminalCaseSingle);
                                                    }
                                                }

                                                if (AdditionalItemsColl != null && AdditionalItemsColl.Count() > 0)//INT256 New implementation.
                                                {
                                                    if (additionalFilterforNewYork)
                                                    {
                                                        foreach (XElement xe in AdditionalItemsColl)
                                                        {
                                                            ChargeSingle = xe;
                                                            DateTime dtNewYork;
                                                            string strDisposition = string.Empty;
                                                            //to get the Additional items date field entries.
                                                            XElement anyDate = ChargeSingle.Descendants("EffectiveDate").Descendants("StartDate").Descendants("AnyDate").FirstOrDefault();
                                                            if (anyDate != null)
                                                            {
                                                                string[] splitdatevalueNewYork = anyDate.Value.Split('T');//To split date part from the string like <AnyDate>2000-03-18T00:00:00</AnyDate>. 
                                                                //var CompletedatevalueNewYork = splitdatevalueNewYork[0];//get only date part from the string array.
                                                                DateTime.TryParse(splitdatevalueNewYork[0], out dtNewYork);//To verify the date format.
                                                                int UpdateYear = Year_new * (-1);//To creating the less than 7 year date from the current date.
                                                                if (dtNewYork < DateTime.Now.AddYears(UpdateYear))//to check date if coming date less than 7 year.
                                                                {
                                                                    //7 YEAR STATES, Comment
                                                                    strLog += DateTime.Now.ToString() + "  Line No ----------------------12149 New York found and remove based on case \n";
                                                                    CriminalCaseCollRm.Remove(CriminalCaseSingle);//If coming date is less than 7 year than it will remove Single CriminalCaseSingle section. 
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                if (Collection.IsRemoveCharges == true)
                                                {
                                                    #region Removing/Adding Charges to Current criminal Case
                                                    if (ChargeCollRm.Count > 0)
                                                    {
                                                        strLog += DateTime.Now.ToString() + "  Line No ----------------------12161 Removing/Adding Charges to Current criminal Case \n";
                                                        strLog += DateTime.Now.ToString() + "  Line No ----------------------12162 Charges added : " + Convert.ToString(ChargeCollRm.Count) + "\n";
                                                        CriminalCaseSingle.Descendants("Charge").Remove();
                                                        foreach (XElement rm in ChargeCollRm)
                                                        {
                                                            CriminalCaseSingle.Add(rm);
                                                        }
                                                    }
                                                    #endregion

                                                    #region Logic to remove CriminalCase if all chares are removed
                                                    if (ChargeCollRm.Count == 0)
                                                    {
                                                        //INT-287 as per Greg comments in JIRA.
                                                        //If any county belongs to Los angeles and no charges are cmoing in the response.
                                                        //then response will be displayed on view report page.
                                                        if (ProductCode == "RCX") //If Los angeles county does not exists.
                                                        {
                                                            if (!flagforLiveRunnerCounty)
                                                            {
                                                                CriminalCaseCollRm.Remove(CriminalCaseSingle);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            CriminalCaseCollRm.Remove(CriminalCaseSingle);
                                                        }
                                                    }
                                                    #endregion
                                                }
                                            }
                                            else//Package Filter functionality based on Jira ticket INT-174
                                            {
                                                #region 7 Year Filter
                                                if (GetPackageFor7YearFilter(pkOrderDetailId))
                                                {
                                                    strLog += DateTime.Now.ToString() + "  Line No ----------------------12184 GetPackageFor7YearFilter \n";
                                                    List<XElement> ChargeColl, ChargeCollRm, AdditionalItemsColl;
                                                    ChargeColl = CriminalCaseSingle.Descendants("Charge").ToList<XElement>();
                                                    ChargeCollRm = CriminalCaseSingle.Descendants("Charge").ToList<XElement>();
                                                    AdditionalItemsColl = CriminalCaseSingle.Descendants("AdditionalItems").ToList<XElement>();
                                                    XElement ChargeSingle = null;
                                                    if (ChargeColl != null && ChargeColl.Count > 0)
                                                    {
                                                        foreach (XElement xe in ChargeColl)
                                                        {
                                                            ChargeSingle = xe;
                                                            string strDisposition = string.Empty;
                                                            XElement OffenseDate = ChargeSingle.Descendants("OffenseDate").FirstOrDefault();
                                                            XElement DispositionDate = ChargeSingle.Descendants("DispositionDate").FirstOrDefault();
                                                            XElement DispDate = ChargeSingle.Descendants("DispDate").FirstOrDefault();
                                                            XElement Disposition = ChargeSingle.Descendants("Disposition").FirstOrDefault();
                                                            XElement SentenceDate = ChargeSingle.Descendants("Sentence").FirstOrDefault();


                                                            int flag = 1;

                                                            if (Disposition != null)
                                                            {
                                                                string Despostioncheck = string.Empty;
                                                                Despostioncheck = Disposition.Value;
                                                                if (!string.IsNullOrEmpty(Despostioncheck))
                                                                {
                                                                    string Desposition = Despostioncheck.ToLower();
                                                                    while (Desposition.IndexOf(" ") >= 0)
                                                                    {
                                                                        Desposition = Desposition.Replace(" ", "");
                                                                    }
                                                                    for (int r = 0; r < s.Count(); r++)
                                                                    {
                                                                        if (string.IsNullOrEmpty(s[r]) == false)
                                                                        {
                                                                            if (Desposition.ToLower().Contains(s[r].ToLower()))
                                                                            {
                                                                                flag = 2;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    flag = 2;
                                                                }
                                                            }
                                                            string str = ""; DateTime dt;
                                                            if (OffenseDate != null)
                                                            {
                                                                str = OffenseDate.Value;
                                                            }
                                                            else if (DispositionDate != null)
                                                            {
                                                                str = DispositionDate.Value;
                                                            }
                                                            else if (CaseFileDate != null)
                                                            {
                                                                str = CaseFileDate.Value;
                                                            }
                                                            else if (DispDate != null)
                                                            {
                                                                str = DispDate.Value;//20070625
                                                                if (str != "" && str.Length > 7)
                                                                {
                                                                    str = str.Substring(0, 4) + "/" + str.Substring(4, 2) + "/" + str.Substring(6, 2);
                                                                }
                                                                else
                                                                {
                                                                    str = DateTime.Now.ToShortDateString();
                                                                }
                                                            }
                                                            else if (SentenceDate != null)
                                                            { // changes regarding ticket 418
                                                                string SentenceDatevalue = SentenceDate.Value;
                                                                if (SentenceDatevalue.Contains("SENTENCE DATE"))
                                                                {
                                                                    string[] splitdatevalue = SentenceDatevalue.Split(':');
                                                                    var Completedatevalue = splitdatevalue[1];
                                                                    str = Completedatevalue.Substring(0, 11).Replace("/", "");
                                                                    string str1 = str.Substring(5, 4);
                                                                    string str2 = str.Substring(1, 2);
                                                                    string str3 = str.Substring(3, 2);
                                                                    str = str1 + "/" + str2 + "/" + str3;
                                                                }
                                                            }
                                                            str = (str != "" && str.Length > 9) ? str.Substring(0, 10) : DateTime.Now.ToShortDateString();
                                                            DateTime.TryParse(str, out dt);
                                                            int UpdateYear = Year_new * (-1);
                                                            DateTime NewDate = DateTime.Now.AddYears(UpdateYear);
                                                            if (CheckCase == 1)
                                                            {
                                                                if (dt < DateTime.Now.AddYears(UpdateYear))
                                                                {
                                                                    #region #800: 7 YEAR STATES, Comment #31
                                                                    if (flag == 2)
                                                                    {
                                                                        IsEmergeReviewStatus = true;
                                                                    }
                                                                    if (_IsShowRawData)
                                                                        _IsShowRawData = false;

                                                                    #endregion
                                                                    if (flag != 2)
                                                                    {
                                                                        if (Collection.IsRemoveCharges == true)
                                                                        {
                                                                            if (dt < DateTime.Now.AddYears(UpdateYear))
                                                                            {
                                                                                ChargeCollRm.Remove(ChargeSingle);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            if (CheckCase == 2)
                                                            {
                                                                if (Max_new_Year <= NewDate)
                                                                {

                                                                    if (dt < Max_new_Year)
                                                                    {
                                                                        #region #800: 7 YEAR STATES, Comment #31
                                                                        if (flag == 2)
                                                                        {
                                                                            IsEmergeReviewStatus = true;
                                                                        }
                                                                        if (_IsShowRawData)
                                                                            _IsShowRawData = false;

                                                                        #endregion
                                                                        if (flag != 2)
                                                                        {
                                                                            if (Collection.IsRemoveCharges == true)
                                                                            {
                                                                                if (dt < Max_new_Year)
                                                                                {
                                                                                    ChargeCollRm.Remove(ChargeSingle);
                                                                                }

                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        ChargeCollRm.Remove(ChargeSingle);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    if (dt < DateTime.Now.AddYears(UpdateYear))
                                                                    {
                                                                        #region #800: 7 YEAR STATES, Comment #31
                                                                        if (flag == 2)
                                                                        {
                                                                            IsEmergeReviewStatus = true;
                                                                        }
                                                                        if (_IsShowRawData)
                                                                            _IsShowRawData = false;

                                                                        #endregion
                                                                        if (flag != 2)
                                                                        {
                                                                            if (Collection.IsRemoveCharges == true)
                                                                            {
                                                                                if (dt < DateTime.Now.AddYears(UpdateYear))
                                                                                {
                                                                                    ChargeCollRm.Remove(ChargeSingle);
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                            }
                                                            if (CheckCase == 3)
                                                            {
                                                                if (Min_new_Year <= dt && dt <= Max_new_Year)
                                                                {
                                                                    #region #800: 7 YEAR STATES, Comment #31
                                                                    if (flag == 2)
                                                                    {
                                                                        IsEmergeReviewStatus = true;
                                                                    }
                                                                    if (_IsShowRawData)
                                                                        _IsShowRawData = false;

                                                                    #endregion
                                                                    if (flag != 2)
                                                                    {
                                                                        if (Collection.IsRemoveCharges == true)
                                                                        {
                                                                            ChargeCollRm.Remove(ChargeSingle);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {

                                                        CriminalCaseCollRm.Remove(CriminalCaseSingle);

                                                    }
                                                    if (Collection.IsRemoveCharges == true)
                                                    {
                                                        #region Removing/Adding Charges to Current criminal Case
                                                        if (ChargeCollRm.Count > 0)
                                                        {
                                                            strLog += DateTime.Now.ToString() + "  Line No ----------------------12396 Charges added : " + Convert.ToString(ChargeCollRm.Count) + "\n";
                                                            CriminalCaseSingle.Descendants("Charge").Remove();
                                                            foreach (XElement rm in ChargeCollRm)
                                                            {
                                                                CriminalCaseSingle.Add(rm);
                                                            }
                                                        }
                                                        #endregion

                                                        #region Logic to remove CriminalCase if all chares are removed
                                                        if (ChargeCollRm.Count == 0)
                                                        {
                                                            CriminalCaseCollRm.Remove(CriminalCaseSingle);
                                                        }
                                                        #endregion
                                                    }
                                                }
                                                #endregion

                                                #region 10 Year Filter
                                                if (GetPackageFor10YearFilter(pkOrderDetailId))//10 Year Filter
                                                {
                                                    strLog += DateTime.Now.ToString() + "  Line No ----------------------12184 GetPackageFor10YearFilter \n";
                                                    Year_new = 10;
                                                    List<XElement> ChargeColl, ChargeCollRm, AdditionalItemsColl;
                                                    ChargeColl = CriminalCaseSingle.Descendants("Charge").ToList<XElement>();
                                                    ChargeCollRm = CriminalCaseSingle.Descendants("Charge").ToList<XElement>();
                                                    AdditionalItemsColl = CriminalCaseSingle.Descendants("AdditionalItems").ToList<XElement>();
                                                    XElement ChargeSingle = null;
                                                    if (ChargeColl != null && ChargeColl.Count > 0)
                                                    {
                                                        foreach (XElement xe in ChargeColl)
                                                        {
                                                            ChargeSingle = xe;
                                                            string strDisposition = string.Empty;
                                                            XElement OffenseDate = ChargeSingle.Descendants("OffenseDate").FirstOrDefault();
                                                            XElement DispositionDate = ChargeSingle.Descendants("DispositionDate").FirstOrDefault();
                                                            XElement DispDate = ChargeSingle.Descendants("DispDate").FirstOrDefault();
                                                            XElement Disposition = ChargeSingle.Descendants("Disposition").FirstOrDefault();
                                                            XElement SentenceDate = ChargeSingle.Descendants("Sentence").FirstOrDefault();


                                                            int flag = 1;

                                                            if (Disposition != null)
                                                            {
                                                                string Despostioncheck = string.Empty;
                                                                Despostioncheck = Disposition.Value;
                                                                if (!string.IsNullOrEmpty(Despostioncheck))
                                                                {
                                                                    string Desposition = Despostioncheck.ToLower();
                                                                    while (Desposition.IndexOf(" ") >= 0)
                                                                    {
                                                                        Desposition = Desposition.Replace(" ", "");
                                                                    }
                                                                    for (int r = 0; r < s.Count(); r++)
                                                                    {
                                                                        if (string.IsNullOrEmpty(s[r]) == false)
                                                                        {
                                                                            if (Desposition.ToLower().Contains(s[r].ToLower()))
                                                                            {
                                                                                flag = 2;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    flag = 2;
                                                                }
                                                            }

                                                            string str = ""; DateTime dt;
                                                            if (OffenseDate != null)
                                                            {
                                                                str = OffenseDate.Value;
                                                            }
                                                            else if (DispositionDate != null)
                                                            {
                                                                str = DispositionDate.Value;
                                                            }
                                                            else if (CaseFileDate != null)
                                                            {
                                                                str = CaseFileDate.Value;
                                                            }
                                                            else if (DispDate != null)
                                                            {
                                                                str = DispDate.Value;//20070625
                                                                if (str != "" && str.Length > 7)
                                                                {
                                                                    str = str.Substring(0, 4) + "/" + str.Substring(4, 2) + "/" + str.Substring(6, 2);
                                                                }
                                                                else
                                                                {
                                                                    str = DateTime.Now.ToShortDateString();
                                                                }
                                                            }
                                                            else if (SentenceDate != null)
                                                            { // changes regarding ticket 418
                                                                string SentenceDatevalue = SentenceDate.Value;
                                                                if (SentenceDatevalue.Contains("SENTENCE DATE"))
                                                                {
                                                                    string[] splitdatevalue = SentenceDatevalue.Split(':');
                                                                    var Completedatevalue = splitdatevalue[1];
                                                                    str = Completedatevalue.Substring(0, 11).Replace("/", "");
                                                                    string str1 = str.Substring(5, 4);
                                                                    string str2 = str.Substring(1, 2);
                                                                    string str3 = str.Substring(3, 2);
                                                                    str = str1 + "/" + str2 + "/" + str3;

                                                                }
                                                            }
                                                            str = (str != "" && str.Length > 9) ? str.Substring(0, 10) : DateTime.Now.ToShortDateString();
                                                            DateTime.TryParse(str, out dt);


                                                            int UpdateYear = Year_new * (-1);
                                                            DateTime NewDate = DateTime.Now.AddYears(UpdateYear);

                                                            if (CheckCase == 1)
                                                            {
                                                                if (dt < DateTime.Now.AddYears(UpdateYear))
                                                                {
                                                                    #region #800: 7 YEAR STATES, Comment #31
                                                                    if (flag == 2)
                                                                    {
                                                                        IsEmergeReviewStatus = true;
                                                                    }
                                                                    if (_IsShowRawData)
                                                                        _IsShowRawData = false;

                                                                    #endregion
                                                                    if (flag != 2)
                                                                    {
                                                                        if (Collection.IsRemoveCharges == true)
                                                                        {
                                                                            if (dt < DateTime.Now.AddYears(UpdateYear))
                                                                            {
                                                                                ChargeCollRm.Remove(ChargeSingle);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            if (CheckCase == 2)
                                                            {
                                                                if (Max_new_Year <= NewDate)
                                                                {

                                                                    if (dt < Max_new_Year)
                                                                    {
                                                                        #region #800: 7 YEAR STATES, Comment #31
                                                                        if (flag == 2)
                                                                        {
                                                                            IsEmergeReviewStatus = true;
                                                                        }
                                                                        if (_IsShowRawData)
                                                                            _IsShowRawData = false;

                                                                        #endregion
                                                                        if (flag != 2)
                                                                        {
                                                                            if (Collection.IsRemoveCharges == true)
                                                                            {
                                                                                if (dt < Max_new_Year)
                                                                                {
                                                                                    ChargeCollRm.Remove(ChargeSingle);
                                                                                }

                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        ChargeCollRm.Remove(ChargeSingle);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    if (dt < DateTime.Now.AddYears(UpdateYear))
                                                                    {
                                                                        #region #800: 7 YEAR STATES, Comment #31
                                                                        if (flag == 2)
                                                                        {
                                                                            IsEmergeReviewStatus = true;
                                                                        }
                                                                        if (_IsShowRawData)
                                                                            _IsShowRawData = false;

                                                                        #endregion
                                                                        if (flag != 2)
                                                                        {
                                                                            if (Collection.IsRemoveCharges == true)
                                                                            {
                                                                                if (dt < DateTime.Now.AddYears(UpdateYear))
                                                                                {
                                                                                    ChargeCollRm.Remove(ChargeSingle);
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                            }
                                                            if (CheckCase == 3)
                                                            {
                                                                if (Min_new_Year <= dt && dt <= Max_new_Year)
                                                                {
                                                                    #region #800: 7 YEAR STATES, Comment #31
                                                                    if (flag == 2)
                                                                    {
                                                                        IsEmergeReviewStatus = true;
                                                                    }
                                                                    if (_IsShowRawData)
                                                                        _IsShowRawData = false;

                                                                    #endregion
                                                                    if (flag != 2)
                                                                    {
                                                                        if (Collection.IsRemoveCharges == true)
                                                                        {
                                                                            ChargeCollRm.Remove(ChargeSingle);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {

                                                        CriminalCaseCollRm.Remove(CriminalCaseSingle);

                                                    }
                                                    if (Collection.IsRemoveCharges == true)
                                                    {
                                                        #region Removing/Adding Charges to Current criminal Case
                                                        if (ChargeCollRm.Count > 0)
                                                        {
                                                            strLog += DateTime.Now.ToString() + "  Line No ----------------------12636 Charges added : " + Convert.ToString(ChargeCollRm.Count) + "\n";
                                                            CriminalCaseSingle.Descendants("Charge").Remove();
                                                            foreach (XElement rm in ChargeCollRm)
                                                            {
                                                                CriminalCaseSingle.Add(rm);
                                                            }
                                                        }
                                                        #endregion

                                                        #region Logic to remove CriminalCase if all chares are removed
                                                        if (ChargeCollRm.Count == 0)
                                                        {
                                                            CriminalCaseCollRm.Remove(CriminalCaseSingle);
                                                        }
                                                        #endregion
                                                    }
                                                }
                                                #endregion
                                            }
                                        }
                                    }
                                }
                                ///
                                /// Here i have removed all empty CriminalCases
                                if (Collection.IsRemoveCharges == true)
                                {
                                    strLog += DateTime.Now.ToString() + "  Line No ----------------------12664 Charges added : " + Convert.ToString(CriminalCaseCollRm.Count) + "\n";
                                    xelementCriminalReport.First().RemoveAll();
                                    foreach (XElement rm in CriminalCaseCollRm)
                                    {
                                        xelementCriminalReport.First().Add(rm);
                                    }
                                }
                                // INT-456 Auto review, to count Criminal case 
                           HttpContext.Current.Session["CriminalCaseCountResponse"] = 0;
                                HttpContext.Current.Session["CriminalCaseCountResponse"] = CriminalCaseCollRm.Count;
                                if(CriminalCaseCollRm.Count==0)
                                {
                                    tblOrderDetail ObjOrderDetails = DX.tblOrderDetails.Where(db => db.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                                    if (ObjOrderDetails != null)
                                    {
                                        tblOrder objtblOrder = DX.tblOrders.Where(db => db.pkOrderId == ObjOrderDetails.fkOrderId).FirstOrDefault();
                                        if (objtblOrder != null)
                                        {
                                            if (Convert.ToInt32(HttpContext.Current.Session["CriminalCaseCountResponse"]) == 0)
                                            {
                                                objtblOrder.IsReviewed = false;
                                                ObjOrderDetails.ReviewStatus = 0;
                                                ObjOrderDetails.ReviewComments = "";
                                            }
                                            DX.SubmitChanges();
                                        }                                        
                                    }
                                }
                            }

                        }
                        #endregion
                    }
                    else if (ProductCode.Trim().ToUpper() == "SCR")
                    {
                        #region SCR
                        ///
                        /// In SCR we have 2 type of tags.<reslut>/<CriminalCase>
                        /// First I have applied check for <CriminalCase>
                        ///
                        List<XElement> xelementCriminalReport = ObjXDocument.Descendants("CriminalReport").ToList();
                        List<XElement> xElementresults = ObjXDocument.Descendants("results").ToList<XElement>();
                        if (xelementCriminalReport != null)
                        {
                            if (xelementCriminalReport.Count > 0)
                            {
                            }
                            ///
                            /// If Response contain result tag
                            ///
                            else if (xElementresults != null && xElementresults.Count > 0)
                            {
                                List<XElement> resultColl, resultCollRm;
                                resultColl = ObjXDocument.Descendants("result").ToList();
                                resultCollRm = ObjXDocument.Descendants("result").ToList();

                                XElement CriminalCaseSingle = null;
                                if (resultColl != null && resultColl.Count > 0)
                                {
                                    for (int i = 0; i < resultColl.Count; i++)
                                    {
                                        CriminalCaseSingle = resultColl.ElementAt(i);
                                        List<XElement> RegionColl = CriminalCaseSingle.Descendants("state").ToList<XElement>();
                                        if (RegionColl != null && RegionColl.Count > 0)
                                        {
                                            bool isStateValid = false;
                                            string StateName = "";
                                            foreach (XElement Region in RegionColl)
                                            {
                                                StateName = Region.Value;
                                                isStateValid = (StateNameColl.Where(d => d.ToLower() == StateName.ToLower()).Count() > 0) ? true : false;
                                                if (isStateValid)
                                                    break;
                                            }
                                            if (isStateValid)
                                            {
                                                string strDisposition = string.Empty;
                                                int flag = 1;
                                                var dispositionlst = CriminalCaseSingle.Descendants("disposition").ToList();

                                                if (dispositionlst.Count > 0)
                                                {
                                                    for (int iComment = 0; iComment < dispositionlst.Count; iComment++)
                                                    {
                                                        //string disposition = dispositionlst.ElementAt(iComment).Value;
                                                        strDisposition = dispositionlst.ElementAt(iComment).Value;
                                                        //using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
                                                        //{
                                                        if (!string.IsNullOrEmpty(strDisposition))
                                                        {
                                                            string Desposition = strDisposition.ToLower();
                                                            while (Desposition.IndexOf(" ") >= 0)
                                                            {
                                                                Desposition = Desposition.Replace(" ", "");
                                                            }

                                                            for (int r = 0; r < s.Count(); r++)
                                                            {
                                                                if (string.IsNullOrEmpty(s[r]) == false)
                                                                {
                                                                    if (Desposition.ToLower().Contains(s[r].ToLower()))
                                                                    {
                                                                        flag = 2;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            flag = 2;
                                                        }
                                                        //}
                                                    }
                                                }
                                                List<XElement> ChargeColl;
                                                ChargeColl = CriminalCaseSingle.Descendants("dispositionDate").ToList<XElement>();
                                                if (ChargeColl != null && ChargeColl.Count > 0)
                                                {
                                                    foreach (XElement xe in ChargeColl)
                                                    {
                                                        string str = xe.Value;
                                                        DateTime dt;
                                                        str = (str != "" && str.Length > 9) ? str.Substring(0, 10) : DateTime.Now.ToShortDateString();
                                                        DateTime.TryParse(str, out dt);


                                                        int UpdateYear = Year_new * (-1);
                                                        DateTime NewDate = DateTime.Now.AddYears(UpdateYear);

                                                        if (CheckCase == 1)
                                                        {
                                                            if (dt < DateTime.Now.AddYears(UpdateYear))
                                                            {
                                                                if (flag == 2)
                                                                {
                                                                    IsEmergeReviewStatus = true;
                                                                }
                                                                if (flag != 2)
                                                                {
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {
                                                                        resultCollRm.Remove(CriminalCaseSingle);
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }


                                                        if (CheckCase == 2)
                                                        {
                                                            if (Max_new_Year <= NewDate)
                                                            {

                                                                if (dt < Max_new_Year)
                                                                {
                                                                    if (flag == 2)
                                                                    {
                                                                        IsEmergeReviewStatus = true;
                                                                    }
                                                                    if (flag != 2)
                                                                    {
                                                                        if (Collection.IsRemoveCharges == true)
                                                                        {
                                                                            resultCollRm.Remove(CriminalCaseSingle);
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (dt < DateTime.Now.AddYears(UpdateYear))
                                                                {
                                                                    if (flag == 2)
                                                                    {
                                                                        IsEmergeReviewStatus = true;
                                                                    }
                                                                    if (flag != 2)
                                                                    {
                                                                        if (Collection.IsRemoveCharges == true)
                                                                        {
                                                                            resultCollRm.Remove(CriminalCaseSingle);
                                                                            break;
                                                                        }
                                                                    }
                                                                }

                                                            }
                                                        }

                                                        if (CheckCase == 3)
                                                        {
                                                            if (Min_new_Year <= dt && dt <= Max_new_Year)
                                                            {
                                                                if (flag == 2)
                                                                {
                                                                    IsEmergeReviewStatus = true;
                                                                }
                                                                if (flag != 2)
                                                                {
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {
                                                                        resultCollRm.Remove(CriminalCaseSingle);
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }



                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                ///
                                /// Here i have removed all empty CriminalCases
                                ///
                                if (Collection.IsRemoveCharges == true)
                                {
                                    xElementresults.First().RemoveAll();
                                    foreach (XElement rm in resultCollRm)
                                    {
                                        xElementresults.First().Add(rm);
                                    }
                                }

                            }
                        }

                        #endregion
                    }
                    else if (ProductCode.Trim().ToUpper() == "NCR4")
                    {
                        #region NCR4

                        List<XElement> XECriminalSearchResults = ObjXDocument.Descendants("CriminalSearchResults").ToList<XElement>();
                        if (XECriminalSearchResults != null)
                        {
                            if (XECriminalSearchResults.Count > 0)
                            {
                                List<XElement> JurisdictionColl, JurisdictionCollRm;
                                JurisdictionColl = ObjXDocument.Descendants("JurisdictionResults").ToList();
                                JurisdictionCollRm = ObjXDocument.Descendants("JurisdictionResults").ToList();

                                XElement JurisdictionSingle = null;
                                if (JurisdictionColl != null && JurisdictionColl.Count > 0)
                                {

                                    for (int i = 0; i < JurisdictionColl.Count; i++)
                                    {
                                        JurisdictionSingle = JurisdictionColl.ElementAt(i);
                                        string strDisposition = string.Empty;
                                        List<XElement> ChargeColl = JurisdictionSingle.Descendants("Charge").ToList<XElement>();
                                        if (ChargeColl != null && ChargeColl.Count > 0)
                                        {
                                            bool isStateValid = false;
                                            string CountyName = "";

                                            foreach (XElement XECharge in ChargeColl)
                                            {
                                                XElement XEChrgCountyName = XECharge.Descendants("ChrgCountyName").FirstOrDefault();
                                                if (XEChrgCountyName != null)
                                                {
                                                    CountyName = XEChrgCountyName.Value;
                                                    ///
                                                    /// We have county name and then apply check for statename/statecode. 
                                                    ///
                                                    int stateId = 0;
                                                    if (CountyName != "")
                                                    {
                                                        stateId = ObjCountyColl.Where(d => d.CountyName.ToLower() == CountyName.ToLower()).Select(d => d.fkStateId).FirstOrDefault();
                                                        if (stateId != 0)
                                                        {
                                                            isStateValid = (ObjStateColl.Where(d => d.pkStateId == stateId).Count() > 0) ? true : false;
                                                        }
                                                    }
                                                    if (isStateValid)
                                                        break;
                                                }
                                            }
                                            if (isStateValid)
                                            {
                                                XElement XEDispDate = JurisdictionSingle.Descendants("DispDate").FirstOrDefault();
                                                XElement Disposition = JurisdictionSingle.Descendants("Disposition").FirstOrDefault();
                                                int flag = 1;
                                                //using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
                                                //{
                                                if (Disposition != null)
                                                {
                                                    string Despostioncheck = string.Empty;
                                                    Despostioncheck = Disposition.Value;
                                                    if (!string.IsNullOrEmpty(Despostioncheck))
                                                    {
                                                        string Desposition = Despostioncheck.ToLower();
                                                        while (Desposition.IndexOf(" ") >= 0)
                                                        {
                                                            Desposition = Desposition.Replace(" ", "");
                                                        }

                                                        for (int r = 0; r < s.Count(); r++)
                                                        {
                                                            if (string.IsNullOrEmpty(s[r]) == false)
                                                            {
                                                                if (Desposition.ToLower().Contains(s[r].ToLower()))
                                                                {
                                                                    flag = 2;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        flag = 2;
                                                    }
                                                }
                                                //}
                                                if (XEDispDate != null)
                                                {
                                                    string str = XEDispDate.Value;
                                                    DateTime dt;//20070625
                                                    if (str != "" && str.Length > 7)
                                                    {
                                                        str = str.Substring(0, 4) + "/" + str.Substring(4, 2) + "/" + str.Substring(6, 2);
                                                    }
                                                    else
                                                    {
                                                        str = DateTime.Now.ToShortDateString();
                                                    }
                                                    DateTime.TryParse(str, out dt);

                                                    int UpdateYear = Year_new * (-1);
                                                    DateTime NewDate = DateTime.Now.AddYears(UpdateYear);


                                                    if (CheckCase == 1)
                                                    {
                                                        if (dt < DateTime.Now.AddYears(UpdateYear))
                                                        {

                                                            if (flag == 2)
                                                            {
                                                                IsEmergeReviewStatus = true;
                                                            }
                                                            if (flag != 2)
                                                            {
                                                                if (Collection.IsRemoveCharges == true)
                                                                {
                                                                    JurisdictionCollRm.Remove(JurisdictionSingle);
                                                                }
                                                            }

                                                        }
                                                    }

                                                    if (CheckCase == 2)
                                                    {
                                                        if (Max_new_Year <= NewDate)
                                                        {

                                                            if (dt < Max_new_Year)
                                                            {
                                                                if (flag == 2)
                                                                {
                                                                    IsEmergeReviewStatus = true;
                                                                }
                                                                if (flag != 2)
                                                                {
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {
                                                                        JurisdictionCollRm.Remove(JurisdictionSingle);
                                                                    }
                                                                }

                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (dt < DateTime.Now.AddYears(UpdateYear))
                                                            {
                                                                if (flag == 2)
                                                                {
                                                                    IsEmergeReviewStatus = true;
                                                                }
                                                                if (flag != 2)
                                                                {
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {
                                                                        JurisdictionCollRm.Remove(JurisdictionSingle);
                                                                    }
                                                                }

                                                            }

                                                        }
                                                    }


                                                    if (CheckCase == 3)
                                                    {
                                                        if (Min_new_Year <= dt && dt <= Max_new_Year)
                                                        {
                                                            if (flag == 2)
                                                            {
                                                                IsEmergeReviewStatus = true;
                                                            }
                                                            if (flag != 2)
                                                            {
                                                                if (Collection.IsRemoveCharges == true)
                                                                {
                                                                    JurisdictionCollRm.Remove(JurisdictionSingle);
                                                                }
                                                            }

                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (Collection.IsRemoveCharges == true)
                                                    {
                                                        JurisdictionCollRm.Remove(JurisdictionSingle);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                ///
                                /// Here i have removed all empty CriminalCases
                                ///
                                XElement XESearchResultCount = XECriminalSearchResults.First().Descendants("SearchResultCount").FirstOrDefault();
                                if (XESearchResultCount != null)
                                {
                                    XESearchResultCount.Value = JurisdictionCollRm.Count.ToString();
                                }
                                if (Collection.IsRemoveCharges == true)
                                {
                                    XECriminalSearchResults.First().RemoveAll();
                                    if (XESearchResultCount != null)
                                    {
                                        XECriminalSearchResults.First().Add(XESearchResultCount);
                                    }
                                    foreach (XElement rm in JurisdictionCollRm)
                                    {
                                        XECriminalSearchResults.First().Add(rm);
                                    }
                                }
                            }
                        }

                        #endregion
                    }                    
                    if (Collection.IsEmergeReview == true)
                    {
                        if (IsEmergeReviewStatus == true)
                        {

                            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
                            {
                                tblOrderDetail ObjOrderDetails = ObjEmergeDALDataContext.tblOrderDetails.Where(db => db.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                                if (ObjOrderDetails != null)
                                {
                                    tblOrder objtblOrder = ObjEmergeDALDataContext.tblOrders.Where(db => db.pkOrderId == ObjOrderDetails.fkOrderId).FirstOrDefault();
                                    if (objtblOrder.IsReviewed == false && ObjOrderDetails.ReviewStatus == 0)
                                    {
                                        bool Is6MonthOld = false;
                                        BALGeneral ObjBALGeneral = new BALGeneral();
                                        Is6MonthOld = ObjBALGeneral.Is6MonthOld(ObjOrderDetails.fkOrderId);
                                        int? OrderId = ObjOrderDetails.fkOrderId;
                                        int? pkOrderDetail_Id = pkOrderDetailId;
                                        Email_send(OrderId, pkOrderDetail_Id);
                                        UpdateReportReviewStatus(pkOrderDetail_Id, 1, string.Empty, Is6MonthOld);
                                    }
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in GetHidedResponse(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                BALGeneral.WriteLogForDeletion(" Some Error Occured in GetHidedResponse(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, LogFilename);
            }

            strLog += DateTime.Now.ToString() + "  Line No ----------------------13416 End GetHidedResponse\n";
            BALGeneral.WriteLogForDeletion(strLog, LogFilename);
            ObjXDocumentupdated = ObjXDocument;
            IsShowRawData = _IsShowRawData;

            return ObjXDocumentupdated;
        }




        #endregion

        public void AutoReviewByKeywords(string XMLresp, int? pkOrderDetailId, string ProductCode)
        {
            string Filter_Id = "4";
            List<string> arrKeywords = new List<string>();
            BALCompany ObjBALCompany = new BALCompany();
            var Collection = ObjBALCompany.GetFiltersAccordingFilterId(Filter_Id).FirstOrDefault();//Getting filter rules 7 Year Filter.
            if (Collection != null)
            {
                if (string.IsNullOrEmpty(Collection.Keywords) == false)
                {
                    var coll = Collection.Keywords.ToString();
                    var ArrayCollection = coll.Split(',');
                    if (ArrayCollection.Count() > 0)
                    {
                        for (int j = 0; j < ArrayCollection.Count(); j++)
                        {
                            arrKeywords.Add(ArrayCollection[j].Trim().ToUpper());//Adding all keywords in List<string> s = new List<string>();

                        }
                    }
                }
            }

            //XDocument ObjXDocumentupdated = new XDocument();
            XDocument ObjXDocument = new XDocument();

            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    bool IsEmergeReviewStatus = false;
                    string XML_Value = GetRequiredXmlConditional(XMLresp, ProductCode.Trim().ToUpper());
                    ObjXDocument = XDocument.Parse(XML_Value);
                    if (ProductCode.Trim().ToUpper() == "NCR1" || ProductCode.Trim().ToUpper() == "NCR+" || ProductCode.Trim().ToUpper() == "RCX")
                    {
                        #region NCR1, NCR+, RCX
                        List<XElement> xelementCriminalReport = ObjXDocument.Descendants("CriminalReport").ToList();
                        if (xelementCriminalReport != null)
                        {
                            if (xelementCriminalReport.Count > 0)
                            {
                                List<XElement> CriminalCaseColl, CriminalCaseCollRm;
                                CriminalCaseColl = ObjXDocument.Descendants("CriminalCase").ToList();
                                CriminalCaseCollRm = ObjXDocument.Descendants("CriminalCase").ToList();

                                XElement CriminalCaseSingle = null;
                                if (CriminalCaseColl != null && CriminalCaseColl.Count > 0)
                                {

                                    for (int i = 0; i < CriminalCaseColl.Count; i++)
                                    {
                                        CriminalCaseSingle = CriminalCaseColl.ElementAt(i);
                                        List<XElement> ChargeColl, ChargeCollRm, AdditionalItemsColl;
                                        ChargeColl = CriminalCaseSingle.Descendants("Charge").ToList<XElement>();
                                        ChargeCollRm = CriminalCaseSingle.Descendants("Charge").ToList<XElement>();
                                        AdditionalItemsColl = CriminalCaseSingle.Descendants("AdditionalItems").ToList<XElement>();
                                        XElement AdditionalItemSingle = null;
                                        if (AdditionalItemsColl != null && AdditionalItemsColl.Count > 0)
                                        {
                                            foreach (XElement xe in AdditionalItemsColl)
                                            {
                                                AdditionalItemSingle = xe;
                                                XElement Text = AdditionalItemSingle.Descendants("Text").FirstOrDefault();
                                                if (Text != null)
                                                {
                                                    if (arrKeywords != null)
                                                    {
                                                        if (arrKeywords.Count() > 0)
                                                        {
                                                            for (int arr = 0; arr < arrKeywords.Count(); arr++)
                                                            {
                                                                if (Text.Value.ToLower().Contains(arrKeywords.ElementAt(arr).ToLower().ToString()))
                                                                {
                                                                    IsEmergeReviewStatus = true;
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {
                                                                        ChargeCollRm.Remove(AdditionalItemSingle);
                                                                    }
                                                                    //WatchListStatus = 1;
                                                                    //if (CriminalCaseSingle.Attribute("WatchList") == null)
                                                                    //{
                                                                    //    CriminalCaseSingle.Add(new XAttribute("WatchList", "" + 1 + ""));
                                                                    //}
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else { CriminalCaseCollRm.Remove(CriminalCaseSingle); }
                                        if (Collection.IsRemoveCharges == true)
                                        {
                                            #region Removing/Adding Charges to Current criminal Case

                                            CriminalCaseSingle.Descendants("Charge").Remove();
                                            //CriminalCaseSingle.RemoveAll();
                                            foreach (XElement rm in ChargeCollRm)
                                            {
                                                CriminalCaseSingle.Add(rm);
                                            }
                                            #endregion

                                            #region Logic to remove CriminalCase if all chares are removed
                                            if (ChargeCollRm.Count == 0)
                                            {
                                                CriminalCaseCollRm.Remove(CriminalCaseSingle);
                                            }
                                            #endregion
                                        }
                                        XElement ChargeSingle = null;
                                        if (ChargeColl != null && ChargeColl.Count > 0)
                                        {
                                            foreach (XElement xe in ChargeColl)
                                            {
                                                ChargeSingle = xe;
                                                string strDisposition = string.Empty;
                                                XElement Disposition = ChargeSingle.Descendants("Disposition").FirstOrDefault();
                                                if (Disposition != null)
                                                {
                                                    strDisposition = Disposition.Value;
                                                    if (strDisposition.ToLower() != "dismissed")
                                                    {
                                                        if (arrKeywords != null)
                                                        {
                                                            if (arrKeywords.Count() > 0)
                                                            {
                                                                for (int arr = 0; arr < arrKeywords.Count(); arr++)
                                                                {
                                                                    if (Disposition.Value.ToLower().Contains(arrKeywords.ElementAt(arr).ToLower().ToString()))
                                                                    {
                                                                        IsEmergeReviewStatus = true;
                                                                        if (Collection.IsRemoveCharges == true)
                                                                        {
                                                                            ChargeCollRm.Remove(AdditionalItemSingle);
                                                                        }
                                                                        //WatchListStatus = 1;
                                                                        //if (CriminalCaseSingle.Attribute("WatchList") == null)
                                                                        //{
                                                                        //    CriminalCaseSingle.Add(new XAttribute("WatchList", "" + 1 + ""));
                                                                        //}
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                if (strDisposition.ToLower() != "dismissed")
                                                {
                                                    var ChargeTypeClassification = ChargeSingle.Descendants("ChargeTypeClassification").FirstOrDefault();
                                                    if (ChargeTypeClassification != null)
                                                    {
                                                        if (arrKeywords != null)
                                                        {
                                                            if (arrKeywords.Count() > 0)
                                                            {
                                                                for (int arr = 0; arr < arrKeywords.Count(); arr++)
                                                                {
                                                                    if (ChargeTypeClassification.Value.ToLower().Contains(arrKeywords.ElementAt(arr).ToLower().ToString()))
                                                                    {
                                                                        IsEmergeReviewStatus = true;
                                                                        if (Collection.IsRemoveCharges == true)
                                                                        {
                                                                            ChargeCollRm.Remove(AdditionalItemSingle);
                                                                        }
                                                                        //WatchListStatus = 1;
                                                                        //if (CriminalCaseSingle.Attribute("WatchList") == null)
                                                                        //{
                                                                        //    CriminalCaseSingle.Add(new XAttribute("WatchList", "" + 1 + ""));
                                                                        //}
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                    var ChargeOrComplaint = ChargeSingle.Descendants("ChargeOrComplaint").FirstOrDefault();
                                                    if (ChargeOrComplaint != null)
                                                    {
                                                        if (arrKeywords != null)
                                                        {
                                                            if (arrKeywords.Count() > 0)
                                                            {
                                                                for (int arr = 0; arr < arrKeywords.Count(); arr++)
                                                                {
                                                                    if (ChargeOrComplaint.Value.ToLower().Contains(arrKeywords.ElementAt(arr).ToLower().ToString()))
                                                                    {
                                                                        IsEmergeReviewStatus = true;
                                                                        if (Collection.IsRemoveCharges == true)
                                                                        {
                                                                            ChargeCollRm.Remove(AdditionalItemSingle);
                                                                        }
                                                                        //WatchListStatus = 1;
                                                                        //if (CriminalCaseSingle.Attribute("WatchList") == null)
                                                                        //{
                                                                        //    CriminalCaseSingle.Add(new XAttribute("WatchList", "" + 1 + ""));
                                                                        //}
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                    var Textlst = ChargeSingle.Descendants("Text").ToList();

                                                    if (Textlst.Count > 0)
                                                    {
                                                        for (int iText = 0; iText < Textlst.Count; iText++)
                                                        {
                                                            string text = Textlst.ElementAt(iText).Value;
                                                            if (arrKeywords != null)
                                                            {
                                                                if (arrKeywords.Count() > 0)
                                                                {
                                                                    for (int arr = 0; arr < arrKeywords.Count(); arr++)
                                                                    {
                                                                        if (text.ToLower().Contains(arrKeywords.ElementAt(arr).ToLower().ToString()))
                                                                        {
                                                                            IsEmergeReviewStatus = true;
                                                                            if (Collection.IsRemoveCharges == true)
                                                                            {
                                                                                ChargeCollRm.Remove(AdditionalItemSingle);
                                                                            }
                                                                            //WatchListStatus = 1;
                                                                            //if (CriminalCaseSingle.Attribute("WatchList") == null)
                                                                            //{
                                                                            //    CriminalCaseSingle.Add(new XAttribute("WatchList", "" + 1 + ""));
                                                                            //}
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                    var Sentencelst = ChargeSingle.Descendants("Sentence").ToList();

                                                    if (Sentencelst.Count > 0)
                                                    {
                                                        for (int iSentence = 0; iSentence < Sentencelst.Count; iSentence++)
                                                        {
                                                            string sentence = Sentencelst.ElementAt(iSentence).Value;
                                                            if (arrKeywords != null)
                                                            {
                                                                if (arrKeywords.Count() > 0)
                                                                {
                                                                    for (int arr = 0; arr < arrKeywords.Count(); arr++)
                                                                    {
                                                                        if (sentence.ToLower().Contains(arrKeywords.ElementAt(arr).ToLower().ToString()))
                                                                        {
                                                                            IsEmergeReviewStatus = true;
                                                                            if (Collection.IsRemoveCharges == true)
                                                                            {
                                                                                ChargeCollRm.Remove(AdditionalItemSingle);
                                                                            }
                                                                            //WatchListStatus = 1;
                                                                            //if (CriminalCaseSingle.Attribute("WatchList") == null)
                                                                            //{
                                                                            //    CriminalCaseSingle.Add(new XAttribute("WatchList", "" + 1 + ""));
                                                                            //}
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    var Commentlst = ChargeSingle.Descendants("Comment").ToList();

                                                    if (Commentlst.Count > 0)
                                                    {
                                                        for (int iComment = 0; iComment < Commentlst.Count; iComment++)
                                                        {
                                                            string comment = Commentlst.ElementAt(iComment).Value;
                                                            if (arrKeywords != null)
                                                            {
                                                                if (arrKeywords.Count() > 0)
                                                                {
                                                                    for (int arr = 0; arr < arrKeywords.Count(); arr++)
                                                                    {
                                                                        if (comment.ToLower().Contains(arrKeywords.ElementAt(arr).ToLower().ToString()))
                                                                        {
                                                                            IsEmergeReviewStatus = true;
                                                                            if (Collection.IsRemoveCharges == true)
                                                                            {
                                                                                ChargeCollRm.Remove(AdditionalItemSingle);
                                                                            }
                                                                            //WatchListStatus = 1;
                                                                            //if (CriminalCaseSingle.Attribute("WatchList") == null)
                                                                            //{
                                                                            //    CriminalCaseSingle.Add(new XAttribute("WatchList", "" + 1 + ""));
                                                                            //}
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else { CriminalCaseCollRm.Remove(CriminalCaseSingle); }
                                        if (Collection.IsRemoveCharges == true)
                                        {
                                            #region Removing/Adding Charges to Current criminal Case

                                            CriminalCaseSingle.Descendants("Charge").Remove();
                                            //CriminalCaseSingle.RemoveAll();
                                            foreach (XElement rm in ChargeCollRm)
                                            {
                                                CriminalCaseSingle.Add(rm);
                                            }
                                            #endregion

                                            #region Logic to remove CriminalCase if all chares are removed
                                            if (ChargeCollRm.Count == 0)
                                            {
                                                CriminalCaseCollRm.Remove(CriminalCaseSingle);
                                            }
                                            #endregion
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                    }
                    else if (ProductCode.Trim().ToUpper() == "SCR")
                    {
                        #region SCR
                        ///
                        /// In SCR we have 2 type of tags.<reslut>/<CriminalCase>
                        /// First I have applied check for <CriminalCase>
                        ///
                        List<XElement> xelementCriminalReport = ObjXDocument.Descendants("CriminalReport").ToList();
                        List<XElement> xElementresults = ObjXDocument.Descendants("results").ToList<XElement>();
                        if (xelementCriminalReport != null)
                        {
                            if (xelementCriminalReport.Count > 0)
                            {

                            }
                            ///
                            /// If Response contain result tag
                            ///
                            else if (xElementresults != null)
                            {
                                List<XElement> resultColl, resultCollRm;
                                resultColl = ObjXDocument.Descendants("result").ToList();
                                resultCollRm = ObjXDocument.Descendants("result").ToList();

                                XElement CriminalCaseSingle = null;
                                if (resultColl != null && resultColl.Count > 0)
                                {
                                    for (int i = 0; i < resultColl.Count; i++)
                                    {
                                        CriminalCaseSingle = resultColl.ElementAt(i);
                                        string strDisposition = string.Empty;
                                        var dispositionlst = CriminalCaseSingle.Descendants("disposition").ToList();

                                        if (dispositionlst.Count > 0)
                                        {
                                            for (int iComment = 0; iComment < dispositionlst.Count; iComment++)
                                            {
                                                string disposition = dispositionlst.ElementAt(iComment).Value;
                                                strDisposition = dispositionlst.ElementAt(iComment).Value;
                                                if (strDisposition.ToLower() != "dismissed")
                                                {
                                                    if (arrKeywords != null)
                                                    {
                                                        if (arrKeywords.Count() > 0)
                                                        {
                                                            for (int arr = 0; arr < arrKeywords.Count(); arr++)
                                                            {
                                                                if (disposition.ToLower().Contains(arrKeywords.ElementAt(arr).ToLower().ToString()))
                                                                {
                                                                    IsEmergeReviewStatus = true;
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {
                                                                        resultCollRm.Remove(CriminalCaseSingle);
                                                                        break;
                                                                    }
                                                                    //WatchListStatus = 1;
                                                                    //if (CriminalCaseSingle.Attribute("WatchList") == null)
                                                                    //{
                                                                    //    CriminalCaseSingle.Add(new XAttribute("WatchList", "" + 1 + ""));
                                                                    //}
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (strDisposition.ToLower() != "dismissed")
                                        {
                                            var crimeDetailslst = CriminalCaseSingle.Descendants("crimeDetails").ToList();

                                            if (crimeDetailslst.Count > 0)
                                            {
                                                for (int iText = 0; iText < crimeDetailslst.Count; iText++)
                                                {
                                                    string crimeDetails = crimeDetailslst.ElementAt(iText).Value;
                                                    if (arrKeywords != null)
                                                    {
                                                        if (arrKeywords.Count() > 0)
                                                        {
                                                            for (int arr = 0; arr < arrKeywords.Count(); arr++)
                                                            {
                                                                if (crimeDetails.ToLower().Contains(arrKeywords.ElementAt(arr).ToLower().ToString()))
                                                                {
                                                                    IsEmergeReviewStatus = true;
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {
                                                                        resultCollRm.Remove(CriminalCaseSingle);
                                                                        break;
                                                                    }
                                                                    //WatchListStatus = 1;
                                                                    //if (CriminalCaseSingle.Attribute("WatchList") == null)
                                                                    //{
                                                                    //    CriminalCaseSingle.Add(new XAttribute("WatchList", "" + 1 + ""));
                                                                    //}
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            var crimeTypelst = CriminalCaseSingle.Descendants("crimeType").ToList();

                                            if (crimeTypelst.Count > 0)
                                            {
                                                for (int iSentence = 0; iSentence < crimeTypelst.Count; iSentence++)
                                                {
                                                    string crimeType = crimeTypelst.ElementAt(iSentence).Value;
                                                    if (arrKeywords != null)
                                                    {
                                                        if (arrKeywords.Count() > 0)
                                                        {
                                                            for (int arr = 0; arr < arrKeywords.Count(); arr++)
                                                            {
                                                                if (crimeType.ToLower().Contains(arrKeywords.ElementAt(arr).ToLower().ToString()))
                                                                {
                                                                    IsEmergeReviewStatus = true;
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {
                                                                        resultCollRm.Remove(CriminalCaseSingle);
                                                                        break;
                                                                    }
                                                                    //WatchListStatus = 1;
                                                                    //if (CriminalCaseSingle.Attribute("WatchList") == null)
                                                                    //{
                                                                    //    CriminalCaseSingle.Add(new XAttribute("WatchList", "" + 1 + ""));
                                                                    //}
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if (Collection.IsRemoveCharges == true)
                                {
                                    xElementresults.First().RemoveAll();
                                    foreach (XElement rm in resultCollRm)
                                    {
                                        xElementresults.First().Add(rm);
                                    }
                                }
                            }
                        }

                        #endregion
                    }
                    else if (ProductCode.Trim().ToUpper() == "NCR4")
                    {
                        #region NCR4

                        List<XElement> XECriminalSearchResults = ObjXDocument.Descendants("CriminalSearchResults").ToList<XElement>();
                        if (XECriminalSearchResults != null)
                        {
                            if (XECriminalSearchResults.Count > 0)
                            {
                                List<XElement> JurisdictionColl, JurisdictionCollRm;
                                JurisdictionColl = ObjXDocument.Descendants("JurisdictionResults").ToList();
                                JurisdictionCollRm = ObjXDocument.Descendants("JurisdictionResults").ToList();

                                XElement JurisdictionSingle = null;
                                if (JurisdictionColl != null && JurisdictionColl.Count > 0)
                                {
                                    for (int i = 0; i < JurisdictionColl.Count; i++)
                                    {
                                        JurisdictionSingle = JurisdictionColl.ElementAt(i);
                                        string strDisposition = string.Empty;
                                        List<XElement> ChargeColl = JurisdictionSingle.Descendants("Charge").ToList<XElement>();
                                        if (ChargeColl != null && ChargeColl.Count > 0)
                                        {
                                            XElement Disposition = JurisdictionSingle.Descendants("Disposition").FirstOrDefault();
                                            if (Disposition != null)
                                            {
                                                strDisposition = Disposition.Value;
                                                if (strDisposition.ToLower() != "dismissed")
                                                {
                                                    if (arrKeywords != null)
                                                    {
                                                        if (arrKeywords.Count() > 0)
                                                        {
                                                            for (int arr = 0; arr < arrKeywords.Count(); arr++)
                                                            {
                                                                if (Disposition.Value.ToLower().Contains(arrKeywords.ElementAt(arr).ToLower().ToString()))
                                                                {
                                                                    IsEmergeReviewStatus = true;
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {
                                                                        JurisdictionCollRm.Remove(JurisdictionSingle);
                                                                    }
                                                                    //WatchListStatus = 1;
                                                                    //if (JurisdictionSingle.Attribute("WatchList") == null)
                                                                    //{
                                                                    //    JurisdictionSingle.Add(new XAttribute("WatchList", "" + 1 + ""));
                                                                    //}
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                ///
                                /// Here i have removed all empty CriminalCases
                                ///
                                XElement XESearchResultCount = XECriminalSearchResults.First().Descendants("SearchResultCount").FirstOrDefault();
                                if (XESearchResultCount != null)
                                {
                                    XESearchResultCount.Value = JurisdictionCollRm.Count.ToString();
                                }
                                if (Collection.IsRemoveCharges == true)
                                {
                                    XECriminalSearchResults.First().RemoveAll();
                                    if (XESearchResultCount != null)
                                    {
                                        XECriminalSearchResults.First().Add(XESearchResultCount);
                                    }
                                    foreach (XElement rm in JurisdictionCollRm)
                                    {
                                        XECriminalSearchResults.First().Add(rm);
                                    }
                                }
                            }
                        }

                        #endregion
                    }
                    else if (ProductCode.Trim().ToUpper() == "PS2")
                    {
                        ///
                        /// Date tag not found.
                        ///
                    }
                    else if (ProductCode.Trim().ToUpper() == "CCR1")
                    {
                        #region CCR1 NON- LIVERUNNER #481
                        //foreach (XElement element in ObjXDocument.Descendants("report-detail-header").ToList())
                        //{
                        //    foreach (XElement element_rpt in element.Descendants("report-incident").ToList())
                        //    {
                        //        string strDisposition = string.Empty;
                        //        var dispositionlst = element_rpt.Descendants("disposition").ToList();

                        //        if (dispositionlst.Count > 0)
                        //        {
                        //            for (int iComment = 0; iComment < dispositionlst.Count; iComment++)
                        //            {
                        //                string disposition = dispositionlst.ElementAt(iComment).Value;
                        //                strDisposition = dispositionlst.ElementAt(iComment).Value;
                        //                if (!string.IsNullOrEmpty(strDisposition))
                        //                {
                        //                    string Desposition = strDisposition.ToLower();
                        //                    while (Desposition.IndexOf(" ") >= 0)
                        //                    {
                        //                        Desposition = Desposition.Replace(" ", "");
                        //                    }

                        //                    if (arrKeywords != null)
                        //                    {
                        //                        if (arrKeywords.Count() > 0)
                        //                        {
                        //                            for (int arr = 0; arr < arrKeywords.Count(); arr++)
                        //                            {
                        //                                if (Desposition.ToLower().Contains(arrKeywords.ElementAt(arr).ToLower().ToString()))
                        //                                {
                        //                                    IsEmergeReviewStatus = true;
                        //                                    if (Collection.IsRemoveCharges == true)
                        //                                    {
                        //                                        element.Remove();
                        //                                    }
                        //                                }
                        //                            }
                        //                        }
                        //                    }
                        //                }
                        //            }
                        //        }
                        //    }
                        //}
                        #endregion

                    }
                    else if (ProductCode.Trim().ToUpper() == "NCR2")
                    {
                        ///
                        /// xslt shows html CDATA.
                        ///
                    }
                    if (Collection.IsEmergeReview == true)
                    {
                        if (IsEmergeReviewStatus == true)
                        {

                            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
                            {
                                tblOrderDetail ObjOrderDetails = ObjEmergeDALDataContext.tblOrderDetails.Where(db => db.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                                if (ObjOrderDetails != null)
                                {
                                    tblOrder objtblOrder = ObjEmergeDALDataContext.tblOrders.Where(db => db.pkOrderId == ObjOrderDetails.fkOrderId).FirstOrDefault();
                                    if (objtblOrder.IsReviewed == false && ObjOrderDetails.ReviewStatus == 0)
                                    {
                                        bool Is6MonthOld = false;
                                        int Updatestatus = 0;
                                        BALGeneral ObjBALGeneral = new BALGeneral();
                                        Is6MonthOld = ObjBALGeneral.Is6MonthOld(ObjOrderDetails.fkOrderId);
                                        int? OrderId = ObjOrderDetails.fkOrderId;
                                        int? pkOrderDetail_Id = pkOrderDetailId;
                                        Updatestatus = UpdateReportReviewStatus(pkOrderDetail_Id, 1, string.Empty, Is6MonthOld);
                                        if (Updatestatus == 1)
                                        {
                                            Email_send(OrderId, pkOrderDetail_Id);
                                        }
                                    }
                                }
                            }

                        }
                    }
                    //if (Collection.IsWatchList == true)
                    //{
                    //    DX.UpdateWatchListStatus(WatchListStatus, pkOrderDetailId);
                    //}
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in AutoReviewByKeywords(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

            }
           // ObjXDocumentupdated = ObjXDocument;
        }

        #endregion

        public bool IsLiveRunnerReport(int? OrderDetailId)
        {
            bool IsLiveRunner = false;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                IsLiveRunner = (from a in DX.tblOrderDetails
                                where a.pkOrderDetailId == OrderDetailId
                                select a.IsLiveRunner).FirstOrDefault();

            }
            return IsLiveRunner;
        }

        public bool SendEmail(string MailTo, string MailFrom, string EmailBody, string Subject)
        {
            int SendingStatus = 0;
            try
            {
                MailProvider ObjMailProvider = new MailProvider();

                List<string> BCC_List = new List<string>();
                ObjMailProvider.MailTo.Add(MailTo);
                ObjMailProvider.MailFrom = MailFrom;
                ObjMailProvider.Message = EmailBody;
                ObjMailProvider.MailBCC = BCC_List;
                ObjMailProvider.Subject = Subject;
                ObjMailProvider.IsBodyHtml = true;

                ObjMailProvider.Priority = MailPriority.High;
                ObjMailProvider.SendMail(out SendingStatus);
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in SendEmail(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

            }

            return (SendingStatus == 1 ? true : false);
        }

        public void Email_send(int? OrderId, int? pkOrderDetail_Id)
        {
            string fileName = "Reviewmail_" + DateTime.Now.ToString("yyMMdd");
            string sentMailLog = "Email_send for OrderId " + Convert.ToString(OrderId);
            BALGeneral.WriteLogForDeletion(sentMailLog, fileName);

            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                BALGeneral ObjBALGeneral = new BALGeneral();
                StringBuilder Comments = new StringBuilder();
                int PkTemplateId = 0;
                string emailText = "";
                string MessageBody = "";
                Proc_GetReportInfoForAutoReviewMailResult ReportInfo = DX.GetReportInfoForAutoReviewMail(OrderId, pkOrderDetail_Id).FirstOrDefault();
                string MsgBeforeEmail = "Emerge Reviewed for Order No:-" + ReportInfo.OrderNo;
                string UserIdentityName = ReportInfo.UserName.ToLower();
                WriteReviewStatusInTextFile(MsgBeforeEmail, UserIdentityName);
                Comments.Append(" <table width=\"97%\"><tr><td>Report ID : </td><td>" + ReportInfo.OrderNo + "</td>"
    + "</tr> <tr><td>Report Name : </td><td>" + ReportInfo.ProductDisplayName + "</td></tr>"
    + "<tr><td>User : </td><td>" + ReportInfo.LastName + " " + ReportInfo.FirstName + " (<b>UserName : </b>" + ReportInfo.UserName + ") " + "</td></tr>"
    + " <tr><td valign=\"top\"  style=\"width:150px;\">Additional Comments :</td><td> Auto request for " + ReportInfo.ProductCode + " report review.</td></tr>  </table>");

                string BoxHeaderText = ReportInfo.ProductCode + " Auto-Review Request - " + ReportInfo.LastName + " " + ReportInfo.FirstName;//string.Empty;

                Guid ApplicationId = new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852");
                string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/emerge-logo--no-shadows-(small).gif";
                PkTemplateId = 27;
                var emailContent = ObjBALGeneral.GetEmailTemplate(ApplicationId, PkTemplateId);// "Emerge Suggestions"
                emailText = emailContent.TemplateContent;

                CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
                TextInfo textInfo = cultureInfo.TextInfo;
                string Searchedfirstname = ReportInfo.SearchedFirstName != null ? textInfo.ToTitleCase(ReportInfo.SearchedFirstName) : string.Empty;
                string Searchedlastname = ReportInfo.SearchedLastName != null ? textInfo.ToTitleCase(ReportInfo.SearchedLastName) : string.Empty;

                #region New way Bookmark

                BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                #region  For New Style SystemTemplate
                string Bookmarkfile = string.Empty;
                string bookmarkfilepath = string.Empty;

                Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                emailText = Bookmarkfile;
                #endregion
                Bookmarks objBookmark = new Bookmarks();

                objBookmark.EmailContent = emailText;



                BoxHeaderText = emailContent.TemplateSubject.Replace("%ApplicantName%", Searchedlastname + " " + Searchedfirstname).Replace("%ReportCode%", ReportInfo.ProductCode);

                objBookmark.Subject = BoxHeaderText;
                objBookmark.ApplicantName = Searchedlastname + " " + Searchedfirstname;
                objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                objBookmark.UserCompany = "";
                objBookmark.UserEmail = ReportInfo.LastName + " " + ReportInfo.FirstName + " (<b>UserName : </b>" + ReportInfo.UserName + ") ";
                objBookmark.EmergeReviewComment = Comments.ToString();

                MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, ReportInfo.UserId);
                #endregion
                string successAdmin = string.Empty;
                sentMailLog = "Email_send for OrderNo " + Convert.ToString(OrderId);
                BALGeneral.WriteLogForDeletion(sentMailLog, fileName);
                successAdmin = BALEmergeReview.SendMail(ConfigurationManager.AppSettings["ReportReviewMail"].ToString(), UserIdentityName, string.Empty, ConfigurationManager.AppSettings["ReportReviewFromAddress"].ToString(), MessageBody, BoxHeaderText);

                if (successAdmin == "Success")
                {
                    sentMailLog = "Email_send Success for OrderNo " + ReportInfo.OrderNo;
                    BALGeneral.WriteLogForDeletion(sentMailLog, fileName);
                    string SucessMsg = "Email Successfully Sent for Order No:-" + ReportInfo.OrderNo;
                    WriteReviewStatusInTextFile(SucessMsg, ConfigurationManager.AppSettings["ReportReviewMail"].ToString());
                }
                else
                {
                    sentMailLog = "Email_send Failed for OrderNo " + ReportInfo.OrderNo;
                    BALGeneral.WriteLogForDeletion(sentMailLog, fileName);
                    string UnSucessMsg = "Email Sending Failed for Order No:-" + ReportInfo.OrderNo;
                    UnSucessMsg = UnSucessMsg + " Error:" + successAdmin;
                    WriteReviewStatusInTextFile(UnSucessMsg, ConfigurationManager.AppSettings["ReportReviewMail"].ToString());
                }
            }
        }

        public static string SendMail(string TO, string CC, string BCC, string FROM, string MESSAGE, string SUBJECT)
        {
            string fileName = "Reviewmail_" + DateTime.Now.ToString("yyMMdd");
            string sentMailLog = "SendMail method call started";
            BALGeneral.WriteLogForDeletion(sentMailLog, fileName);

            string SendingStatus = string.Empty;
            string SMTPHost = ConfigurationManager.AppSettings["ReviewSMTPHost"].ToString();
            int SMTPPort = Convert.ToInt32(ConfigurationManager.AppSettings["ReviewMailSMTPPort"].ToString());
            string UserName = ConfigurationManager.AppSettings["ReviewMailUserName"].ToString();
            string Password = ConfigurationManager.AppSettings["ReviewMailPassword"].ToString();


            sentMailLog = "SMTPHost : " + SMTPHost + " UserName : " + UserName + "" + " Password  " + Password;
            BALGeneral.WriteLogForDeletion(sentMailLog, fileName);
            #region----Testing purpose
            //TO = "arunk3@chetu.com";
            //CC = "arunk3@chetu.com";
            //BCC = "";
            //FROM = "arunk3@chetu.com";
            //string SMTPHost = "mail01.chetu.com";
            //int SMTPPort = 25;
            //string UserName = "arunk3@chetu.com";
            //string Password = "july@2015";
            #endregion
            MailProvider ObjMailProvider = new MailProvider(SMTPHost, SMTPPort, UserName, Password);

            ObjMailProvider.Message = MESSAGE;
            ObjMailProvider.MailTo.Add(TO);

            if (CC != "")
            {
                string[] CCInfo = CC.Split(',');
                ObjMailProvider.MailCC = CCInfo.ToList<string>();
            }

            if (BCC != "")
            {
                string[] BCCInfo = BCC.Split(',');
                ObjMailProvider.MailBCC = BCCInfo.ToList<string>();
            }
            ObjMailProvider.MailFrom = FROM;
            ObjMailProvider.Subject = SUBJECT;
            ObjMailProvider.IsBodyHtml = true;
            ObjMailProvider.Priority = MailPriority.High;
            ObjMailProvider.SendMail(out SendingStatus);
            sentMailLog = "SendingStatus: " + Convert.ToString(SendingStatus);
            BALGeneral.WriteLogForDeletion(sentMailLog, fileName);
            return (SendingStatus);
        }

        public static string SendMailWithHtml(string TO, string CC, string BCC, string FROM, string MESSAGE, string SUBJECT, Attachment ATTACHMENT)
        {
            string SendingStatus = string.Empty;
            string SMTPHost = ConfigurationManager.AppSettings["ReviewSMTPHost"].ToString();
            int SMTPPort = Convert.ToInt32(ConfigurationManager.AppSettings["ReviewMailSMTPPort"].ToString());
            string UserName = ConfigurationManager.AppSettings["ReviewMailUserName"].ToString();
            string Password = ConfigurationManager.AppSettings["ReviewMailPassword"].ToString();

            MailProvider ObjMailProvider = new MailProvider(SMTPHost, SMTPPort, UserName, Password);

            ObjMailProvider.Message = MESSAGE;
            ObjMailProvider.MailTo.Add(TO);

            if (CC != "")
            {
                string[] CCInfo = CC.Split(',');
                ObjMailProvider.MailCC = CCInfo.ToList<string>();
            }

            if (BCC != "")
            {
                string[] BCCInfo = BCC.Split(',');
                ObjMailProvider.MailBCC = BCCInfo.ToList<string>();
            }
            ObjMailProvider.MailAttachments.Add(ATTACHMENT);

            ObjMailProvider.MailFrom = FROM;
            ObjMailProvider.Subject = SUBJECT;
            ObjMailProvider.IsBodyHtml = true;
            ObjMailProvider.Priority = MailPriority.High;
            ObjMailProvider.SendMail(out SendingStatus);
            return (SendingStatus);
        }

        public void WriteReviewStatusInTextFile(string ReviewMsg, string UserIdentityName)
        {
            try
            {
               // List<string> strlst = new List<string>();
                StreamWriter SW;
                string savedPath = string.Empty;
                string fileName = string.Empty;
                DateTime dt = DateTime.Now;
                fileName = dt.ToString("yyMMdd");
                savedPath = HttpContext.Current.Server.MapPath("~/Resources/Upload/EmergeReviewLog/" + fileName + ".txt");
                if (!System.IO.File.Exists(savedPath))
                {
                    savedPath = HttpContext.Current.Server.MapPath("~/Resources/Upload/EmergeReviewLog/" + fileName + ".txt");
                    SW = System.IO.File.CreateText(savedPath);
                }
                else
                {
                    SW = System.IO.File.AppendText(savedPath);
                }
                SW.WriteLine("\n");
                SW.WriteLine("****************************************");
                SW.WriteLine(ReviewMsg);
                SW.WriteLine("Emerge Review Order Identity:- " + UserIdentityName);
                SW.WriteLine("Time of Event :- " + DateTime.Now.ToString());
                SW.WriteLine("****************************************");
                SW.Close();

            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in WriteReviewStatusInTextFile(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

            }
        }

        /// <summary>
        /// To get the 7 Year functionality for package.
        /// </summary>
        /// <param name="pkPackageId"></param>
        /// <param name="fkCompanyId"></param>
        /// <returns></returns>
        public bool GetPackageFor7YearFilter(int? pkOrderDetailId)
        {
            bool pass7Year = false;
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    int appliedCompanyId = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["7YearFilterCompanyId"]);
                    //To getting the product detail is7YearFilter applied for selected package
                    var Inner7yrStatus = (from a in DX.tblProductPackages
                                          join b in DX.tblOrderDetails on a.pkPackageId equals b.fkPackageId
                                          where b.pkOrderDetailId == pkOrderDetailId && b.fkCompanyId == appliedCompanyId && a.PackageName.Contains("A")
                                          select new { a.Is7YearFilter, b.fkCompanyId }).FirstOrDefault();
                    //To getting the company detail is7YearFilter applied for selected selected
                    var company = DX.tblCompanies.Where(c => c.pkCompanyId == appliedCompanyId).FirstOrDefault();
                    if (Inner7yrStatus != null && company != null)
                    {
                        //if 7Yearfilter not applied for company
                        if (!company.Is7YearFilter)
                        {
                            //If 7YearFilter applied for both
                            if (Inner7yrStatus.Is7YearFilter)
                            {
                                pass7Year = true;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in GetPackageFor7YearFilter(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

            }
            return pass7Year;
        }

        /// <summary>
        /// To get the 10 Year functionality for package.
        /// </summary>
        /// <param name="pkPackageId"></param>
        /// <param name="fkCompanyId"></param>
        /// <returns></returns>
        public bool GetPackageFor10YearFilter(int? pkOrderDetailId)
        {
            bool pass10Year = false;
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    //To getting the product detail is10YearFilter applied for selected package
                    var Inner10yrStatus = (from a in DX.tblProductPackages
                                           join b in DX.tblOrderDetails on a.pkPackageId equals b.fkPackageId
                                           where b.pkOrderDetailId == pkOrderDetailId
                                           select new { a.Is10YearFilter, b.fkCompanyId }).FirstOrDefault();

                    if (Inner10yrStatus != null)
                    {
                        //If 10YearFilter applied for both
                        if (Inner10yrStatus.Is10YearFilter)
                        {
                            pass10Year = true;
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in GetPackageFor10YearFilter(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

            }
            return pass10Year;
        }



        /// <summary>
        /// To get user auto 7 year filteration enabled check from company edit page.
        /// </summary>
        /// <param name="pkUserId"></param>
        /// <returns></returns>
        public bool get7yearAutoFiltersFromCompanyEditPage(int pkCompanyId)
        {
            //Is7YearAutoCheck is disabled from the company edit page.
            bool isSevenYearAuto = false;
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    //if Is7YearAutoCheck is enabled from the company edit page.
                    isSevenYearAuto = DX.tblCompanies.Where(x => x.pkCompanyId == pkCompanyId).Select(x => x.Is7YearAutoCheck).FirstOrDefault();
                }
            }
            catch (Exception)
            {
            }
            return isSevenYearAuto;
        }
    }

    public class OrderList
    {
        public int? pkOrderId { get; set; }
        public int? pkOrderDetailId { get; set; }
        public string ResponseData { get; set; }
        public string ProductCode { get; set; }
        public bool IsLiveRunner { get; set; }
    }
}
