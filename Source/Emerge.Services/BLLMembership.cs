﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;

namespace Emerge.Services
{
    public class BLLMembership
    {
        Guid SiteApplicationId = new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852");
        public static string GetUserRoleByUsername(string UserName)
        {
            string RoleName = "";
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                var Results = from a_u in ObjDALDataContext.aspnet_Users
                              join u_r in ObjDALDataContext.aspnet_UsersInRoles
                                  on a_u.UserId equals u_r.UserId
                              join r in ObjDALDataContext.aspnet_Roles
                                  on u_r.RoleId equals r.RoleId
                              where a_u.UserName.ToLower() == UserName.ToLower()
                              select new
                              {
                                  r.RoleName
                              };
                RoleName = Results.FirstOrDefault().RoleName.ToLower();
            }
            return RoleName;
        }

        public void SignOut(string UserName, DateTime LastActivityDate)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                aspnet_User User = ObjDALDataContext.aspnet_Users.Where(t => t.UserName == UserName && t.ApplicationId == SiteApplicationId).SingleOrDefault();
                if (User != null)
                {
                    User.LastActivityDate = LastActivityDate;
                    ObjDALDataContext.SubmitChanges();
                }
            }
        }

        /// <summary>
        /// Check if user login is Enabled and is not deleted
        /// </summary>
        /// <param name="LoggedUserId"></param>
        /// <returns></returns>
        public int CheckUserIsValid(Guid LoggedUserId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                int? outValid = 0;
                ObjDALDataContext.Check_User_Valid(LoggedUserId, ref outValid);
                return (int)outValid;
            }
        }


        //add on 03 feb 2016 for cheking status in Billing section 
        public int BillingCheckUserIsValid(Guid LoggedUserId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                int? outValid = 0;
                ObjDALDataContext.proc_BillingLocked_User(LoggedUserId, ref outValid);
                return (int)outValid;
            }
        }


        public static UserInfo GetUserInformation(Guid guid)
        {
            UserInfo RetInfo = new UserInfo();
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    RetInfo = (from cu in DX.tblCompanyUsers
                               join m in DX.aspnet_Users
                               on cu.fkUserId equals m.UserId
                               where m.UserId == guid
                               select new UserInfo
                                   {
                                       Firstname = cu.FirstName,
                                       Lastname = cu.LastName,
                                       Email = m.UserName
                                   }).FirstOrDefault();
                }
            }
            catch { }
            return RetInfo;
        }
        /// <summary>
        /// This method is used to update user name
        /// </summary>
        /// <param name="OldUserName"></param>
        /// <param name="NewUserName"></param>
        /// <returns></returns>
        public int UpdateUserName(string OldUserName, string NewUserName)
        {
            OldUserName = OldUserName.ToLower();

            int iResult = -1;

            try
            {
                using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
                {

                    if (IsExistUserName(OldUserName, NewUserName))
                    {
                        iResult = 2;
                        return iResult;
                    }

                    IQueryable<aspnet_User> ObjData = ObjDALDataContext.aspnet_Users.Where(data => data.UserName.ToLower() == OldUserName && data.ApplicationId == SiteApplicationId);
                    IQueryable<aspnet_Membership> ObjData2 = ObjDALDataContext.aspnet_Memberships.Where(data => data.Email.ToLower() == OldUserName && data.ApplicationId == SiteApplicationId);
                    if (ObjData.Count() == 1)
                    {
                        aspnet_User ObjUpdate = ObjData.First();
                        ObjUpdate.UserName = NewUserName;
                        ObjUpdate.LoweredUserName = NewUserName.ToLower();
                    }
                    if (ObjData2.Count() == 1)
                    {
                        aspnet_Membership ObjUpdate = ObjData2.First();
                        ObjUpdate.Email = NewUserName;
                        ObjUpdate.LoweredEmail = NewUserName;

                    }


                    ObjDALDataContext.SubmitChanges();
                    iResult = 1;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return iResult;
        }
        /// <summary>
        /// This method is used to check existing username
        /// </summary>
        /// <param name="OldUserName"></param>
        /// <param name="NewUserName"></param>
        /// <returns></returns>
        public bool IsExistUserName(string OldUserName, string NewUserName)
        {
            OldUserName = OldUserName.ToLower();
            NewUserName = NewUserName.ToLower();

            try
            {
                using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
                {
                    var ObjData = ObjDALDataContext.aspnet_Users.Select(db => db);

                    ObjData = ObjData.Where(db => db.UserName.ToLower() == NewUserName && db.UserName.ToLower() != OldUserName && db.ApplicationId == SiteApplicationId);

                    if (ObjData.Count() > 0)
                    {
                        return true;
                    }
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return false;
        }

    }

    public struct UserInfo
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
    }

}
