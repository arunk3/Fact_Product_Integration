﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;
using System.Data;
using System.Data.Common;

namespace Emerge.Services
{
    public class BALReferenceCode
    {
        public int AddReference(tblReferenceCode ObjtblReferenceCode, int fkCompanyId, bool Justoverloading)
        {
            int Result =0;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    if ((ObjDALDataContext.tblReferenceCodes.Where(d => d.ReferenceCode == ObjtblReferenceCode.ReferenceCode && d.fkCompanyId == fkCompanyId).Count()) == 0)
                    {
                       var Result1 = ObjDALDataContext.AddReference(fkCompanyId,
                           ObjtblReferenceCode.ReferenceCode,
                           ObjtblReferenceCode.ReferenceNote);
                       if (Convert.ToInt16(Result1) > 0)
                       {
                           Result = Convert.ToInt16(Result1);
                       }
                    }
                    else
                    {
                        var v = ObjDALDataContext.tblReferenceCodes.Where(d => d.ReferenceCode == ObjtblReferenceCode.ReferenceCode && d.fkCompanyId == fkCompanyId).Select(d => new { d.pkReferenceCodeId }).FirstOrDefault();
                        if (v != null)
                        {
                            Result = v.pkReferenceCodeId;
                        }
                        else
                        {
                            Result = -1;
                        }
                    }
                }
                catch (Exception)
                {
                    Result = 0;
                }
            }
            return Result;
        }

        public int AddReference(tblReferenceCode ObjtblReferenceCode, int fkCompanyId)
        {
            int Result = 0;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    if ((ObjDALDataContext.tblReferenceCodes.Where(d => d.ReferenceCode == ObjtblReferenceCode.ReferenceCode && d.fkCompanyId == fkCompanyId).Count()) == 0)
                    {
                        ObjDALDataContext.AddReference(fkCompanyId,
                        ObjtblReferenceCode.ReferenceCode,
                        ObjtblReferenceCode.ReferenceNote);

                        Result = 1;
                    }
                    else
                    {
                        Result = -1;
                    }
                }
                catch (Exception)
                {

                    Result = 0;
                }


            }
            return Result;
        }

        public int UpdateRefenceByOrderId(int fkOrderId, string RefCode, string PkRererenceId, string ReferenceNote,int PermissibleId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                ObjDALDataContext.UpdateReferenceByOrderId(fkOrderId, RefCode, PkRererenceId, ReferenceNote, PermissibleId);
                return 1;
            }
        }
        public int UpdateReference(tblReferenceCode ObjtblReferenceCode)
        {
            int Result = 0;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    if ((ObjDALDataContext.tblReferenceCodes.Where(d => d.ReferenceCode == ObjtblReferenceCode.ReferenceCode && d.pkReferenceCodeId != ObjtblReferenceCode.pkReferenceCodeId && d.fkCompanyId == ObjtblReferenceCode.fkCompanyId).Count()) == 0)
                    {
                        ObjDALDataContext.UpdateReference(ObjtblReferenceCode.pkReferenceCodeId,
                        ObjtblReferenceCode.ReferenceCode,
                        ObjtblReferenceCode.ReferenceNote);
                        Result = 1;
                    }
                    else
                    {
                        Result = -1;
                    }
                }
                catch (Exception)
                {

                    Result = 0;
                }


            }
            return Result;
        }

        public int AddEditReference(List<tblReferenceCode> listReferenceCode)
        {
            int Result = 0;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {

                if (listReferenceCode.Count > 0)
                {
                    foreach (var obj in listReferenceCode)
                    {
                        var objtbl = ObjDALDataContext.tblReferenceCodes.Where(d => d.pkReferenceCodeId == obj.pkReferenceCodeId).FirstOrDefault();

                        if (objtbl != null)
                        {
                            if (ObjDALDataContext.tblReferenceCodes.Where(d => d.ReferenceCode == obj.ReferenceCode && d.pkReferenceCodeId != obj.pkReferenceCodeId && d.fkCompanyId == obj.fkCompanyId).FirstOrDefault() == null)
                            {
                                ObjDALDataContext.UpdateReference(obj.pkReferenceCodeId,
                                obj.ReferenceCode,
                                obj.ReferenceNote);
                            }
                        }
                        else
                        {
                            if (ObjDALDataContext.tblReferenceCodes.Where(d => d.ReferenceCode == obj.ReferenceCode && d.fkCompanyId == obj.fkCompanyId).FirstOrDefault()==null)
                            {
                                ObjDALDataContext.AddReference(obj.fkCompanyId,
                                obj.ReferenceCode,
                                obj.ReferenceNote);
                            }
                        }
                    }
                    Result = 1;
                }
            }
            return Result;
        }

        public int DeleteReference(int pkReferenceCodeId, int fkCompanyId)
        {
            int Result = 0;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    ObjDALDataContext.DeleteReference(pkReferenceCodeId, fkCompanyId);
                    Result = 1;

                }
                catch (Exception)
                {
                    Result = 0;
                }
            }
            return Result;
        }

        public List<Proc_GetReferencesResult> GetReferences(int fkCompanyId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetReferences(fkCompanyId).ToList();
            }
        }

        public List<Proc_GetEmailAlertsResult> GetEmailAlerts(int fkCompanyUserId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetEmailAlerts(fkCompanyUserId).ToList();
            }
        }

        public void InsertEmailAlerts(tblEmailNotificationAlert ObjtblEmailAlert)
        {
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                if (dx.tblEmailNotificationAlerts.Where(d => d.fkCompanyUserId == ObjtblEmailAlert.fkCompanyUserId).Count() == 0)
                {
                    tblEmailNotificationAlert objtblEmailNotificationAlert = new tblEmailNotificationAlert();
                    objtblEmailNotificationAlert.fkTemplateId = ObjtblEmailAlert.fkTemplateId;
                    objtblEmailNotificationAlert.fkCompanyUserId = ObjtblEmailAlert.fkCompanyUserId;
                    objtblEmailNotificationAlert.IsSendEmail = ObjtblEmailAlert.IsSendEmail;
                    objtblEmailNotificationAlert.IsSendAlert = ObjtblEmailAlert.IsSendAlert;
                    dx.tblEmailNotificationAlerts.InsertOnSubmit(objtblEmailNotificationAlert);
                    dx.SubmitChanges();
                }
            }
        }


        public int UpdateEmailAlertInfo(tblEmailNotificationAlert ObjtblEmailAlert)
        {
            int iResult = -1;

            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    IQueryable<tblEmailNotificationAlert> ObjtblEmailAlertData = ObjDALDataContext.tblEmailNotificationAlerts.Where(db => db.pkEmailNotificationAlertId == ObjtblEmailAlert.pkEmailNotificationAlertId && db.fkCompanyUserId == ObjtblEmailAlert.fkCompanyUserId && db.fkTemplateId == ObjtblEmailAlert.fkTemplateId);
                    if (ObjtblEmailAlertData.Count() ==1)
                    {
                        tblEmailNotificationAlert objtblEmailNotificationAlert = ObjtblEmailAlertData.First();
                        objtblEmailNotificationAlert.IsSendEmail = ObjtblEmailAlert.IsSendEmail;
                        objtblEmailNotificationAlert.IsSendAlert = ObjtblEmailAlert.IsSendAlert;
                        ObjDALDataContext.SubmitChanges();
                    }
                    else
                    {
                       
                        tblEmailNotificationAlert objtblEmailNotificationAlert = new tblEmailNotificationAlert();
                        objtblEmailNotificationAlert.IsSendEmail = ObjtblEmailAlert.IsSendEmail;
                        objtblEmailNotificationAlert.IsSendAlert = ObjtblEmailAlert.IsSendAlert;
                        objtblEmailNotificationAlert.fkTemplateId = ObjtblEmailAlert.fkTemplateId;
                        objtblEmailNotificationAlert.fkCompanyUserId = ObjtblEmailAlert.fkCompanyUserId;
                        ObjDALDataContext.tblEmailNotificationAlerts.InsertOnSubmit(objtblEmailNotificationAlert);
                        ObjDALDataContext.SubmitChanges();
                    
                    
                    
                    }
                    iResult = 1;
                }
                catch
                {
                    iResult = -1;
                }

                return iResult; /* Here we return result */
            }
        }
    }
}
