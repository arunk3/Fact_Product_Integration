﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;
using System.Web;
using System.Web.Security;
namespace Emerge.Services
{
    public class BALInvoice
    {
        //public List<proc_LoadOpenInvoiceByCompanyIdResult> LoadOpenInvoiceByCompanyId(Guid pkCompanyId)
        //{
        //    using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
        //    {
        //        return ObjEmergeDALDataContext.proc_LoadOpenInvoiceByCompanyId(pkCompanyId).ToList();
        //    }
        //}
        //public List<Proc_LoadInvoiceAlertsResult> LoadInvoiceAlertsByCompanyId(Guid pkCompanyId)
        //{
        //    using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
        //    {
        //        return ObjEmergeDALDataContext.Proc_LoadInvoiceAlerts(pkCompanyId).ToList();
        //    }
        //}


        //To update the invoice table after Gegenration of alert. 
        public int UpdateInvoiceForAlerts(long pkInvoiceId)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                var tblInvoice = ObjEmergeDALDataContext.tblInvoices.Single(p => p.pkInvoiceId == pkInvoiceId);
                tblInvoice.IsViewed = true;
                ObjEmergeDALDataContext.SubmitChanges();
                return 1;
            }
        }
        //To getting all unpaind company list.
        public List<proc_Get_AllUnpaidCompaniesResult> GetAllUnpaidCompanyList()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetAllUnpaidCompanies().ToList<proc_Get_AllUnpaidCompaniesResult>();
            }
        }
        //to Load Invoice History By selected Company Id.
        public List<proc_LoadInvoiceHistoryByCompanyIdResult> LoadInvoiceHistoryByCompanyId(int pkCompanyId)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.LoadInvoiceHistoryByCompanyId(pkCompanyId).ToList();
            }
        }

        //to Load Invoice History By selected Company Id.
        public List<proc_LoadInvoiceHistoryByCompanyIdByDateResult1> LoadInvoiceHistoryByCompanyIdByDate(int pkCompanyId, DateTime from, DateTime to)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.LoadInvoiceHistoryByCompanyIdByDate1(pkCompanyId, from, to).ToList();
            }
        }



        //to Get the Maximum Transaction id.
        public long GetMaxTransactionId()
        {
            long MaxTransactionId = 0;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                var v = DX.tblPaymentHistories.Select(d => d.TransactionId).Max();
                if (v != null)
                {
                    MaxTransactionId = Convert.ToInt64(v);
                    MaxTransactionId++;
                }
            }
            return MaxTransactionId;
        }

        //to Get the Account History Details.
        public List<proc_LoadAllAccountHistoryResult> LoadAccountHistory(int pkCompanyId)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.LoadAllAccountHistory(pkCompanyId).ToList();
            }
        }


        //to Get the Account History Details by selected company id and selected date .
        public List<proc_LoadAllAccountHistoryByDateResult2> LoadAccountHistoryByDate(int pkCompanyId, DateTime from, DateTime to)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.LoadAllAccountHistoryByDate(pkCompanyId, from, to).ToList();
            }
        }




        public int AddPaymentHistoryDetails(tblPaymentHistory ObjtblPaymentHistory, bool IsAutomaticPay, bool IsPayAllInvoice)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {

                List<proc_AddInvoicePaymentHistoryResult> ObjAddPaymentResult = ObjEmergeDALDataContext.AddInvoicePaymentHistory(
                    ObjtblPaymentHistory.fkCompanyId, ObjtblPaymentHistory.fkInvoiceId, ObjtblPaymentHistory.fkCompanyUserId, ObjtblPaymentHistory.PaidAmount,
                    ObjtblPaymentHistory.PaidDate, ObjtblPaymentHistory.TransactionId,
                    ObjtblPaymentHistory.CardNumber, ObjtblPaymentHistory.CardExpiryMonth,
                    ObjtblPaymentHistory.CardExpiryYear, ObjtblPaymentHistory.FirstName,
                    ObjtblPaymentHistory.LastName, ObjtblPaymentHistory.CompanyName,
                    ObjtblPaymentHistory.Address, ObjtblPaymentHistory.City,
                    ObjtblPaymentHistory.fkStateId, ObjtblPaymentHistory.Zipcode,
                    ObjtblPaymentHistory.fkCountryId, ObjtblPaymentHistory.Email,
                    ObjtblPaymentHistory.CreatedDateTime, IsAutomaticPay, IsPayAllInvoice, false
                    ).ToList();
                return ObjAddPaymentResult.First().ReturnType;
            }
        }
        // BY ARUN
        public List<proc_GetOrderDetailsByOrderNoResult1> getOrderDetailsByOrderNo(string orderNo, string AdjustmentType)
        {
            List<proc_GetOrderDetailsByOrderNoResult1> lst = new List<proc_GetOrderDetailsByOrderNoResult1>();
            using (EmergeDALDataContext Dx2 = new EmergeDALDataContext())
            {
                lst = Dx2.GetOrderDetailsByOrderNo(orderNo, AdjustmentType).ToList();
            }
            return lst;
        }




        public int AddPrePaymentHistoryDetails(tblPaymentHistory ObjtblPaymentHistory)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {

                List<proc_AddPrePaymentHistoryResult1> ObjAddPrePaymentHistoryResult = ObjEmergeDALDataContext.AddPrePaymentHistory(
                    ObjtblPaymentHistory.fkCompanyId, ObjtblPaymentHistory.fkCompanyUserId, ObjtblPaymentHistory.PaidAmount,
                    ObjtblPaymentHistory.PaidDate, ObjtblPaymentHistory.TransactionId,
                    ObjtblPaymentHistory.CardNumber, ObjtblPaymentHistory.CardExpiryMonth,
                    ObjtblPaymentHistory.CardExpiryYear, ObjtblPaymentHistory.FirstName,
                    ObjtblPaymentHistory.LastName, ObjtblPaymentHistory.CompanyName,
                    ObjtblPaymentHistory.Address, ObjtblPaymentHistory.City,
                    ObjtblPaymentHistory.fkStateId, ObjtblPaymentHistory.Zipcode,
                    ObjtblPaymentHistory.fkCountryId, ObjtblPaymentHistory.Email,
                    ObjtblPaymentHistory.CreatedDateTime).ToList();
                return ObjAddPrePaymentHistoryResult.First().ReturnType;

            }
        }


        public List<Proc_LoadInvoiceNumberByCompanyIdDataResult> LoadInvoiceNumberByCompanyId(int PkCompanyId)
        {

            var tblInvoice = new List<Proc_LoadInvoiceNumberByCompanyIdDataResult>();


            using (EmergeDALDataContext dbObj = new EmergeDALDataContext())
            {

                tblInvoice = dbObj.Proc_LoadInvoiceNumberByCompanyIdData(Convert.ToInt32(PkCompanyId)).ToList();

            }

            return tblInvoice.ToList();
        }


        public List<LoadInvoiceNumberbyFillInvoicesByCompanyIdforPaymentResult> LoadInvoiceNumberbyFillInvoicesByCompanyIdforPayment(int PkCompanyId)
        {

            var tblInvoice = new List<LoadInvoiceNumberbyFillInvoicesByCompanyIdforPaymentResult>();


            using (EmergeDALDataContext dbObj = new EmergeDALDataContext())
            {

                tblInvoice = dbObj.LoadInvoiceNumberbyFillInvoicesByCompanyIdforPayment(Convert.ToInt32(PkCompanyId)).ToList();

            }

            return tblInvoice.ToList();

            // }
        }









        public List<tblCompany_Price> GetCompanyPrice(int pkProductApplicationid, int pkCompanyId)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                var tblCompany_Price = from p in ObjEmergeDALDataContext.tblCompany_Prices
                                       where p.fkCompanyId == pkCompanyId && p.fkProductApplicationId == pkProductApplicationid
                                       select p;
                return tblCompany_Price.ToList();
            }
        }


        public void SaveAdjustmentBasedonProduct(int companyid, long invoice, int PaymentProcess, int adjustmentProductId, int Qty, decimal AdjustmentAmount, decimal DiscountAmount, Guid User, string DiscountDes)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                ObjEmergeDALDataContext.Proc_AddInvoicePayment(companyid, invoice, PaymentProcess, adjustmentProductId, Qty, AdjustmentAmount, DiscountAmount, DiscountDes, User);
            }
        }




        //To update the Invoice Discount.
        public int UpdateInvoiceDiscount(long pkInvoiceId, decimal DiscountAmount, string DiscountDesc, DateTime CreatedDateTime)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                List<Proc_UpdateInvoiceDiscountResult> ObjUpdateInvoiceDiscountResult = ObjEmergeDALDataContext.UpdateInvoiceDiscount(
                    pkInvoiceId, DiscountAmount, DiscountDesc, CreatedDateTime
                    ).ToList();

                return ObjUpdateInvoiceDiscountResult.First().ReturnType;
            }
        }

        /// <summary>
        /// To Getting the Calulated Remaining Amount based on selected Invoice.
        /// </summary>
        /// <param name="pkInvoiceId"></param>
        /// <returns></returns>
        public decimal GetSelectedInvoiceAmount(long pkInvoiceId)
        {
            decimal retVal = 0;

            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                var ret = ObjEmergeDALDataContext.Proc_GetInvoiceAmount(pkInvoiceId).FirstOrDefault();
                if (ret != null)
                {
                    retVal = Convert.ToDecimal(ret.RetAmt);
                }
                return retVal;
            }
        }

        //To get the default price of selected product.
        public decimal GetDefaultPrice(int companyId, int productID, long pkInvoiceId)
        {
            decimal retVal = -1;
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                retVal = ObjEmergeDALDataContext.Proc_GetDefaultPrice(companyId, productID, pkInvoiceId);
            }
            return retVal;
        }
        public decimal UpdateCustomPricePerInvoice(long companyId, decimal priceValue, long pkInvoiceId, long pkorderdetailid, string Notes, int check, string AdjustmentType)
        {
            //var retVal = -1;
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
               // var retVal = 
                    ObjEmergeDALDataContext.UpdateCustomPricePerInvoice(companyId, priceValue, pkInvoiceId, pkorderdetailid, Notes, check, AdjustmentType);
            }
            return 1;
        }




        //To get the Remaining quantity of selected product.
        public int GetRemainingQtyofProduct(int companyId, int productID, long pkInvoiceId)
        {
            int retVal = -1;
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                retVal = Convert.ToInt32(ObjEmergeDALDataContext.GetAdjustmentQuantity(companyId, pkInvoiceId, productID));
            }
            return retVal;
        }



        //To Get the discount of selected invoice.
        public List<tblInvoice> GetInvoiceDiscountDetails(long pkInvoiceId)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                var tblInvoice = from p in ObjEmergeDALDataContext.tblInvoices
                                 where p.pkInvoiceId == pkInvoiceId
                                 select p;
                return tblInvoice.ToList();
            }
        }
        //To Get the Invoice Month Year based in selected invoice id.
        public List<tblInvoice> GetInvoiceMonthYear(long pkInvoiceId)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                var tblInvoice = from p in ObjEmergeDALDataContext.tblInvoices
                                 where p.pkInvoiceId == pkInvoiceId
                                 select p;
                return tblInvoice.ToList();
            }
        }
        //To get the Invoice Balance based on selected Invoice Id
        public List<tblInvoice> GetInvoiceBalanceAmount(long pkInvoiceId)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                var tblInvoice = from p in ObjEmergeDALDataContext.tblInvoices
                                 where p.pkInvoiceId == pkInvoiceId
                                 select p;
                return tblInvoice.ToList();
            }
        }

        //To Get the Invoice number.
        public List<tblInvoice> GetInvoiceNumber(long pkInvoiceId)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                var tblInvoice = from p in ObjEmergeDALDataContext.tblInvoices
                                 where p.pkInvoiceId == pkInvoiceId
                                 select p;
                return tblInvoice.ToList();
            }
        }
        /// <summary>
        /// To Lock user account based on Due Payment.
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public bool LockUserAccountBasedOnPayment(int companyId)
        {
            bool flagReturn = false;
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                var retVal = ObjEmergeDALDataContext.tblCompanies.Where(cmp => cmp.pkCompanyId == companyId).Select(cmp => cmp.NonPaymentLockDate).FirstOrDefault();
                if (retVal != null)
                {
                    flagReturn = true;
                }
            }
            return flagReturn;
        }

        /// <summary>
        /// To Get the Due Payment of any company.
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<string> GetDuePaymentDetails(int companyId)
        {
            List<string> lstDuePayment = new List<string>();
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                var retVal = ObjEmergeDALDataContext.DuePaymentDetails(companyId).FirstOrDefault();
                if (retVal != null)
                {
                    //Add Due Date.
                    lstDuePayment.Add(Convert.ToDateTime(retVal.DueDate).ToShortDateString());
                    //Add Pending Balanace.
                    lstDuePayment.Add(Convert.ToString(retVal.PendingAmount));

                }
            }
            return lstDuePayment;
        }

        /// <summary>
        /// To Create Invoice based on the selected Order.
        /// </summary>
        /// <param name="fkorderId"></param>

        public void CreateInvoice(int? fkorderId)
        {
            //To Add the data in tblInvoice table
            using (EmergeDALDataContext Dx2 = new EmergeDALDataContext())
            {
                //Get Logged in user id.
                Guid UserId = Guid.Empty;
                //   int companyId = 411;

                // var checkTestCompany = Dx2.BillingTestCompany(companyId).FirstOrDefault();

                MembershipUser Obj = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                if (Obj != null)
                {
                    UserId = new Guid(Obj.ProviderUserKey.ToString());
                    var checkCompany = Dx2.GetCompanyDetailByMemberShipId(UserId).FirstOrDefault();

                    var checkTestCompany = Dx2.BillingTestCompany(checkCompany.PkCompanyId).FirstOrDefault();

                    if (checkCompany != null)
                    {
                        if (checkCompany.PkCompanyId == checkTestCompany.pkcompanyid)
                        {
                            //To Add tblInvoice Data.
                            int? invoiceid = Convert.ToInt32(Dx2.AddInTblInvoice(fkorderId).ReturnValue);



                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  CreateInvoice Is Created Suceesfully :" + "True", "Quikbook");


                            if (invoiceid > 0)
                            {

                                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  CreateInvoice Is Created Suceesfully :" + invoiceid, "Quikbook");
                                //For Adding the new Alert after Generation of New invoice
                                var pInvoiceId = Dx2.tblInvoices.Where(db => db.pkInvoiceId == invoiceid).FirstOrDefault();
                                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  CreateInvoice Is pInvoiceId :" + pInvoiceId, "Quikbook");
                                //Add alerts in tblAlert table.
                                tblAlert ObjtblAlert = new tblAlert();
                                ObjtblAlert.AlertTypeId = pInvoiceId.pkInvoiceId.ToString();
                                ObjtblAlert.AlertType = 5;  //4 for Emerge Review Completed
                                ObjtblAlert.AlertSentDate = DateTime.Now;
                                ObjtblAlert.fkUserId = UserId;
                                new BALEmailSystemTemplates().InsertAlerts(ObjtblAlert);
                            }
                        }
                    }
                }
            }

        }

        /// <summary>
        /// To Create PrePayment Invoice based on the selected Order.
        /// </summary>
        /// <param name="fkorderId"></param>
        public void CreateInvoicePrePayment(int fkorderId)
        {
            //To Add the data in tblInvoice table
            using (EmergeDALDataContext Dx2 = new EmergeDALDataContext())
            {
                //Get Logged in user id.
                Guid UserId = Guid.Empty;
                int companyId1 = 411;
                MembershipUser Obj = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                if (Obj != null)
                {
                    UserId = new Guid(Obj.ProviderUserKey.ToString());
                    var checkCompany = Dx2.GetCompanyDetailByMemberShipId(UserId).FirstOrDefault();
                    if (checkCompany != null)
                    {
                        if (checkCompany.PkCompanyId == companyId1)
                        {
                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\nCreateInvoicePrePayment Is Created Suceesfully :" + "true", "Quikbook");


                            //To Add tblInvoice Data.
                            int invoiceid = Dx2.AddInTblInvoicePrePayment(fkorderId);

                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\nCreateInvoicePrePayment Is Created Suceesfully :" + invoiceid, "Quikbook");

                            #region Remove Alert option from the Prepayment
                            //if (invoiceid > 0)
                            //{
                            //    //For Adding the new Alert after Generation of New invoice
                            //    var pInvoiceId = Dx2.tblInvoices.Where(db => db.pkInvoiceId == invoiceid).FirstOrDefault();
                            //    //Add alerts in tblAlert table.
                            //    tblAlert ObjtblAlert = new tblAlert();
                            //    ObjtblAlert.AlertTypeId = pInvoiceId.pkInvoiceId.ToString();
                            //    ObjtblAlert.AlertType = 5;  //4 for Emerge Review Completed
                            //    ObjtblAlert.AlertSentDate = DateTime.Now;
                            //    ObjtblAlert.fkUserId = UserId;
                            //    new BALEmailSystemTemplates().InsertAlerts(ObjtblAlert);
                            //}
                            #endregion
                        }
                    }
                }
            }
        }


    }
}
