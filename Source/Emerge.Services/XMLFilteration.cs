﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Xml.Linq;
using Emerge.Data;
using Emerge.Services.Helper;
using System.Web.Security;
using System.Web.UI;
using System.Web;
using System.Text.RegularExpressions;


namespace Emerge.Services
{
    public class XMLFilteration
    {

        /// <summary>
        /// To set value if criminal cases are removed from the collection.
        /// This variable call only one time.
        /// </summary>
        int recursiveCallFilterResponse=0;
        /// <summary>
        /// To set value if any CONVICTED value found in dispostion tag. 
        /// </summary>
        bool flagConvicted = false;
        

        #region XMLFilteration for TEST Site

        public string GetFilteredXML(string ProductCode, string ModifiedProductCode, string strResponseData, int? pkOrderDetailId, byte WatchListStatus, DateTime CurrentDate, bool IsLiveRunner, out bool IsShowRawData)
        {
            string FinalResponse = ""; string UpdatedResponse = "";
            bool _IsShowRawData = true;
            using (EmergeDALDataContext Obj = new EmergeDALDataContext())
            {
                UpdatedResponse = strResponseData;
                string FilterId = "3";
                bool IsAdmin = false;
                bool UserCheck = false;
                bool ReportExist = false;
                bool IsthisPackageReport = false;

                BALCompany ObjBALCompany = new BALCompany();
                var ObjData = (dynamic)null;

                ObjData = (from u in Obj.tblOrderDetails
                           where u.pkOrderDetailId == pkOrderDetailId
                           select u).FirstOrDefault();
                if (ObjData == null)
                {
                    ObjData = (from u in Obj.tblOrderDetails_Archieves
                               where u.pkOrderDetailId == pkOrderDetailId
                               select u).FirstOrDefault();
                }
                var Collection = ObjBALCompany.GetFiltersAccordingFilterId(FilterId).FirstOrDefault(); //Getting filter rules for Watch list FilterOR Dismissed Filter
               // string Keywords = Collection.Keywords;
                MembershipUser ObjMembershipUser = Membership.GetUser(Utilitys.GetUID(HttpContext.Current.User.Identity.Name));
                if (ObjMembershipUser != null)
                {
                    if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SystemAdmin")) { IsAdmin = true; } //Checking Current user is admin or not.
                    if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SupportManager")) { IsAdmin = true; }
                    if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SupportTechnician")) { IsAdmin = true; }

                    if (Collection != null)
                    {
                        if (ObjData != null)
                        {
                            if (ObjData.IsPackageReport == true)//Checking Is this PackageReport  or not.
                            {
                                if (Collection.Packages == true) { IsthisPackageReport = false; }
                                else { IsthisPackageReport = true; }
                            }
                            else
                            {
                                IsthisPackageReport = false;
                            }
                            if (string.IsNullOrEmpty(Collection.ReportType) == false) //Checking Report type exist   or not.
                            {
                                var Report_Type = Collection.ReportType;
                                var CollProduct = Report_Type.Split(',');
                                //string Coll = ProductCode.ToUpper();
                                string Coll_1 = ModifiedProductCode.ToUpper();
                                if (CollProduct.Length > 0)
                                {
                                    for (int m = 0; m < CollProduct.Length; m++)
                                    {
                                        if (Coll_1 == CollProduct[m].ToUpper())
                                        {
                                            ReportExist = true;
                                            break;
                                        }
                                    }
                                }
                            }

                            if (Collection.Enabled == true) //Checking Watch list FilterOR DismissedFilter  Active or not.
                            {
                                if (Collection.Users == 1) { UserCheck = true; }
                                if (Collection.Users == 2) { if (IsAdmin == true) { UserCheck = true; } }
                                if (Collection.Users == 3) { if (IsAdmin == false) { UserCheck = true; } }
                                if (UserCheck == true)
                                {
                                    if (IsthisPackageReport == false)
                                    {
                                        if (ReportExist == true)
                                        {
                                            #region Dismissed Charge Response
                                            try
                                            {
                                                string UpdatedDismissedChargeResponse = GetRemovedDismissedChargeXMLFilter(UpdatedResponse, ProductCode, pkOrderDetailId, IsLiveRunner);
                                                UpdatedResponse = UpdatedDismissedChargeResponse;
                                                FinalResponse = UpdatedDismissedChargeResponse;
                                            }
                                            catch (Exception ex)
                                            {
                                                FinalResponse = UpdatedResponse;
                                                //Added log for exception handling.
                                                VendorServices.GeneralService.WriteLog("Some Error Occured in GetFilteredXML(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                                            }
                                            #endregion Dismissed Charge Response
                                        }
                                    }
                                }
                            }
                        }
                    }


                    BALEmergeReview objBALEmergeReview = new BALEmergeReview();

                    string Filter_Id4 = "4";
                    bool Is_Admin4 = false;
                    bool User_Check4 = false;
                    bool Report_Exist4 = false;
                    bool Isthis_PackageReport4 = false;
                    var Collection_4 = ObjBALCompany.GetFiltersAccordingFilterId(Filter_Id4).FirstOrDefault(); //Getting filter rules 7 Year Filter.
                    if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SystemAdmin")) { Is_Admin4 = true; }      //Checking Current user is admin or not.
                    if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SupportManager")) { Is_Admin4 = true; }
                    if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SupportTechnician")) { Is_Admin4 = true; }
                    if (Collection_4 != null)
                    {
                        if (ObjData != null)
                        {
                            if (ObjData.IsPackageReport == true) //Checking Is this PackageReport  or not.
                            {
                                if (Collection_4.Packages == true) { Isthis_PackageReport4 = false; }
                                else { Isthis_PackageReport4 = true; }
                            }
                            else { Isthis_PackageReport4 = false; }
                            if (string.IsNullOrEmpty(Collection_4.ReportType) == false) //Checking Report type exist   or not.
                            {
                                var Report_Type = Collection_4.ReportType;
                                var CollProduct = Report_Type.Split(',');
                                //string Coll = ProductCode.ToUpper();
                                string Coll_1 = ModifiedProductCode.ToUpper();
                                if (CollProduct.Length > 0)
                                {
                                    for (int m = 0; m < CollProduct.Length; m++)
                                    {
                                        if (Coll_1 == CollProduct[m].ToUpper())
                                        {
                                            Report_Exist4 = true;
                                            break;
                                        }
                                    }
                                }
                            }

                            if (Collection_4.Enabled == true) //Checking Report type exist   or not.
                            {
                                if (Collection_4.Users == 1) { User_Check4 = true; }
                                if (Collection_4.Users == 2) { if (Is_Admin4 == true) { User_Check4 = true; } }
                                if (Collection_4.Users == 3) { if (Is_Admin4 == false) { User_Check4 = true; } }
                                if (User_Check4 == true)
                                {
                                    if (Isthis_PackageReport4 == false)
                                    {
                                        if (Report_Exist4 == true)
                                        {
                                            #region Ticket #173:
                                            try
                                            {
                                                objBALEmergeReview.AutoReviewByKeywords(UpdatedResponse, pkOrderDetailId, ModifiedProductCode);
                                                //objBALEmergeReview.AutoReviewByKeywords(strResponseData, pkOrderDetailId, ModifiedProductCode);
                                            }
                                            catch (Exception ex)
                                            {
                                                FinalResponse = UpdatedResponse;
                                                //Added log for exception handling.
                                                VendorServices.GeneralService.WriteLog("Some Error Occured in GetFilteredXML(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                                            }
                                            #endregion
                                        }
                                    }
                                }
                            }
                        }
                    }
                    string Filter_Id = "1";
                    bool Is_Admin = false;
                    bool User_Check = false;
                    bool Report_Exist = false;
                    bool Isthis_PackageReport = false;
                    var Collection_seven = ObjBALCompany.GetFiltersAccordingFilterId(Filter_Id).FirstOrDefault(); //Getting filter rules 7 Year Filter.
                    if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SystemAdmin")) { Is_Admin = true; }      //Checking Current user is admin or not.
                    if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SupportManager")) { Is_Admin = true; }
                    if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SupportTechnician")) { Is_Admin = true; }
                    if (Collection_seven != null)
                    {
                        if (ObjData != null)
                        {
                            if (ObjData.IsPackageReport == true) //Checking Is this PackageReport  or not.
                            {
                                if (Collection_seven.Packages == true) { Isthis_PackageReport = false; }
                                else { Isthis_PackageReport = true; }
                            }
                            else { Isthis_PackageReport = false; }
                            if (string.IsNullOrEmpty(Collection_seven.ReportType) == false) //Checking Report type exist   or not.
                            {
                                var Report_Type = Collection_seven.ReportType;
                                var CollProduct = Report_Type.Split(',');
                               // string Coll = ProductCode.ToUpper();
                                string Coll_1 = ModifiedProductCode.ToUpper();
                                if (CollProduct.Length > 0)
                                {
                                    for (int m = 0; m < CollProduct.Length; m++)
                                    {
                                        if (Coll_1 == CollProduct[m].ToUpper())
                                        {
                                            Report_Exist = true;
                                            break;
                                        }
                                    }
                                }
                            }

                            if (Collection_seven.Enabled == true) //Checking Report type exist   or not.
                            {
                                if (Collection_seven.Users == 1) { User_Check = true; }
                                if (Collection_seven.Users == 2) { if (Is_Admin == true) { User_Check = true; } }
                                if (Collection_seven.Users == 3) { if (Is_Admin == false) { User_Check = true; } }
                                if (User_Check == true)
                                {
                                    if (Isthis_PackageReport == false)
                                    {
                                        if (Report_Exist == true)
                                        {
                                            #region Ticket #800: 7 YEAR STATES
                                            try
                                            {
                                                string Updated7YearResponse = objBALEmergeReview.HideSevenYear(UpdatedResponse, pkOrderDetailId, WatchListStatus, ModifiedProductCode, CurrentDate, out IsShowRawData);
                                                //string Updated7YearResponse = objBALEmergeReview.HideSevenYear(strResponseData, pkOrderDetailId, WatchListStatus, ModifiedProductCode, CurrentDate, out IsShowRawData);
                                                UpdatedResponse = Updated7YearResponse;
                                                FinalResponse = Updated7YearResponse;
                                            }
                                            catch (Exception ex)
                                            {
                                                FinalResponse = UpdatedResponse;
                                                //Added log for exception handling.
                                                VendorServices.GeneralService.WriteLog("Some Error Occured in GetFilteredXML(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                                            }
                                            #endregion
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


                #region LiveRunner Codes - (Penal Codes)

                if (ProductCode.ToUpper() == "CCR1LIVERUNNER" || ProductCode.ToUpper() == "CCR2LIVERUNNER" || ProductCode.ToUpper() == "RCX" || ProductCode.ToUpper() == "NCR1")
                //if (ProductCode.ToUpper() == "CCR1LIVERUNNER" || ProductCode.ToUpper() == "CCR2LIVERUNNER" || ProductCode.ToUpper() == "RCX" || ProductCode.ToUpper() == "NCR1LIVERUNNER" || ProductCode.ToUpper() == "NCR1")//INT122
                {
                    try
                    {
                        //if (IsLiveRunner == true) // for int 122
                        //{
                        BALLiveRunnerCodes ObjLiveRunnerCodes = new BALLiveRunnerCodes();
                        string UpdatedPenalCodesResponse = ObjLiveRunnerCodes.GetandMatchResutsfromResponseData(UpdatedResponse, ProductCode);

                        UpdatedResponse = UpdatedPenalCodesResponse;
                        FinalResponse = UpdatedPenalCodesResponse;
                        //}
                    }
                    catch (Exception ex)
                    {
                        FinalResponse = UpdatedResponse;
                        //Added log for exception handling.
                        VendorServices.GeneralService.WriteLog("Some Error Occured in GetFilteredXML(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    }
                }

                #endregion LiveRunner Codes - (Penal Codes)

                IsShowRawData = _IsShowRawData;
                if (FinalResponse == string.Empty)
                {
                    FinalResponse = strResponseData;
                }
                if (ProductCode == "OFAC" || ProductCode == "NCR1" || ProductCode == "NCR2" || ProductCode == "NCR+" || ProductCode == "NCR4" || ProductCode == "SCR" || ProductCode == "CCR1" || ProductCode == "CCR2" || ProductCode == "SOR" || ProductCode == "RCX" || ProductCode == "SCRLiveRunner" || ProductCode == "CCR1LiveRunner" || ProductCode == "CCR2LiveRunner")
                {
                    XDocument ObjXDocument = XDocument.Parse(FinalResponse);
                    using (EmergeDALDataContext DX = new EmergeDALDataContext())
                    {
                        if (DX.tblOrderDetails.Where(db => db.pkOrderDetailId == pkOrderDetailId).Count() > 0)
                        {
                            tblOrderDetail ObjOrderDetails = DX.tblOrderDetails.Where(db => db.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                            if (ObjOrderDetails != null)
                            {
                                //tblOrder objtblOrder = DX.tblOrders.Where(db => db.pkOrderId == ObjOrderDetails.fkOrderId).FirstOrDefault(); ;
                                //if (objtblOrder.HitStatus == 0)INT28
                                {
                                    CheckIsHit(ObjOrderDetails.fkOrderId, ProductCode, ObjXDocument);
                                }
                            }
                        }
                        else
                        {
                            tblOrderDetails_Archieve ObjtblOrderDetails_Archieve = DX.tblOrderDetails_Archieves.Where(db => db.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                            if (ObjtblOrderDetails_Archieve != null)
                            {
                                tblOrders_Archieve objtblOrder = DX.tblOrders_Archieves.Where(db => db.pkOrderId == ObjtblOrderDetails_Archieve.fkOrderId).FirstOrDefault(); ;
                                if (objtblOrder.HitStatus == 0)
                                {
                                    CheckIsHit(ObjtblOrderDetails_Archieve.fkOrderId, ProductCode, ObjXDocument);
                                }
                            }
                        }
                    }
                }
            }
            return FinalResponse;
        }

        public string GetRemovedDismissedChargeXML(string strResponseData, string ProductCode, int? pkOrderDetailId)
        {
            string FinalResponse = "";
            string strUpdatedXML = string.Empty;
            if (ProductCode.ToUpper() == "NCR1")
            {
                XDocument UpdatedXML = GetFilteredResponseforNCR1(strResponseData, pkOrderDetailId);
                strUpdatedXML = Convert.ToString(UpdatedXML);
                int TrimedUpTo = strResponseData.IndexOf("<BackgroundReportPackage>");
                StringBuilder CompleteResponse = new StringBuilder();
                string XMLInitials = strResponseData.Substring(0, TrimedUpTo);
                CompleteResponse.Append(XMLInitials);
                CompleteResponse.Append(strUpdatedXML);
                CompleteResponse.Append("</BackgroundReports>");
                FinalResponse = Convert.ToString(CompleteResponse);
            }
            else if (ProductCode.ToUpper() == "NCR+")
            {
                strUpdatedXML = strResponseData;
                FinalResponse = strUpdatedXML;
            }
            else if (ProductCode.ToUpper() == "RCX")
            {
                XDocument UpdatedXML = GetDismissedChargeResponse(strResponseData, pkOrderDetailId);
                strUpdatedXML = Convert.ToString(UpdatedXML);
                int TrimedUpTo = strResponseData.IndexOf("<BackgroundReportPackage>");
                StringBuilder CompleteResponse = new StringBuilder();
                string XMLInitials = strResponseData.Substring(0, TrimedUpTo);
                CompleteResponse.Append(XMLInitials);
                CompleteResponse.Append(strUpdatedXML);
                CompleteResponse.Append("</BackgroundReports>");
                FinalResponse = Convert.ToString(CompleteResponse);
            }
            else
            {
                strUpdatedXML = strResponseData;
                FinalResponse = strUpdatedXML;
            }

            return FinalResponse;
        }

        public string GetRemovedDismissedChargeXMLFilter(string strResponseData, string ProductCode, int? pkOrderDetailId, bool IsLiveRunner)
        {
            string FinalResponse = "";
            string strUpdatedXML = string.Empty;
            if (ProductCode.ToUpper() == "NCR1")
            {
                XDocument UpdatedXML = GetFilteredResponseforNCR1(strResponseData, pkOrderDetailId);
                strUpdatedXML = Convert.ToString(UpdatedXML);
                int TrimedUpTo = strResponseData.IndexOf("<BackgroundReportPackage>");
                StringBuilder CompleteResponse = new StringBuilder();
                string XMLInitials = strResponseData.Substring(0, TrimedUpTo);
                CompleteResponse.Append(XMLInitials);
                CompleteResponse.Append(strUpdatedXML);
                CompleteResponse.Append("</BackgroundReports>");
                FinalResponse = Convert.ToString(CompleteResponse);
            }
            else if (ProductCode.ToUpper() == "NCR+")
            {
                strUpdatedXML = strResponseData;
                int TrimedUpTo = strResponseData.IndexOf("<BackgroundReportPackage>");
                StringBuilder CompleteResponse = new StringBuilder();
                string XMLInitials = strResponseData.Substring(0, TrimedUpTo);
                CompleteResponse.Append(XMLInitials);
                CompleteResponse.Append(strUpdatedXML);
                CompleteResponse.Append("</BackgroundReports>");
                FinalResponse = Convert.ToString(CompleteResponse);
            }
            else if (ProductCode.ToUpper() == "CCR1" && IsLiveRunner == false)
            {
                //strUpdatedXML = strResponseData;
                //FinalResponse = strUpdatedXML;
                XDocument UpdatedXML = GetRemovedDismissedChargeXMLFilterforCCR1(strResponseData, pkOrderDetailId);
                strUpdatedXML = Convert.ToString(UpdatedXML);
                int TrimedUpTo = strResponseData.IndexOf("<FastraxNetwork");
                StringBuilder CompleteResponse = new StringBuilder();
                string XMLInitials = strResponseData.Substring(0, TrimedUpTo);
                CompleteResponse.Append(XMLInitials);
                CompleteResponse.Append(strUpdatedXML);
                FinalResponse = Convert.ToString(CompleteResponse);
            }
            else if (ProductCode.ToUpper() == "SCR" && IsLiveRunner == false)
            {
                XDocument UpdatedXML = GetRemovedDismissedChargeXMLFilterforSCR(strResponseData, pkOrderDetailId);
                strUpdatedXML = Convert.ToString(UpdatedXML);
                FinalResponse = strUpdatedXML;
            }
            else
            {
                XDocument UpdatedXML = GetDismissedChargeResponse(strResponseData, pkOrderDetailId);
                strUpdatedXML = Convert.ToString(UpdatedXML);
                int TrimedUpTo = strResponseData.IndexOf("<BackgroundReportPackage>");
                StringBuilder CompleteResponse = new StringBuilder();
                string XMLInitials = strResponseData.Substring(0, TrimedUpTo);
                CompleteResponse.Append(XMLInitials);
                CompleteResponse.Append(strUpdatedXML);
                CompleteResponse.Append("</BackgroundReports>");
                FinalResponse = Convert.ToString(CompleteResponse);
            }
            return FinalResponse;
        }

        public XDocument GetRemovedDismissedChargeXMLFilterforCCR1(string strResponseData, int? pkOrderDetailId)
        {
            XDocument ObjXDocumentUppdated = new XDocument();
            XDocument ObjXDocument = new XDocument();
            ObjXDocument = XDocument.Parse(strResponseData);
            BALCompany ObjBALCompany = new BALCompany();
            List<string> s = new List<string>();
            string FilterId = "3";
            var Collection = ObjBALCompany.GetFiltersAccordingFilterId(FilterId).FirstOrDefault();//Getting filter rules for Watch list FilterOR Dismissed Filter
            if (Collection != null)
            {
                if (string.IsNullOrEmpty(Collection.Keywords) == false)
                {
                    var coll = Collection.Keywords.ToString();
                    var ArrayCollection = coll.Split(',');
                    if (ArrayCollection.Count() > 0)
                    {
                        for (int j = 0; j < ArrayCollection.Count(); j++)
                        {

                            if (string.IsNullOrEmpty(ArrayCollection[j]) == false)
                            {
                                s.Add(ArrayCollection[j].Trim().ToUpper()); //Adding all keywords in List<string> s = new List<string>();
                            }
                        }

                    }

                }
            }

            try
            {
                var lstScreenings = ObjXDocument.Descendants("report-requested").ToList();

                if (lstScreenings.Count > 0)
                {
                    var lstCriminalReports = lstScreenings.Descendants("report").FirstOrDefault();

                    if (lstCriminalReports != null)
                    {
                        var lstCriminalCases = lstCriminalReports.Descendants("report-detail-header").ToList();
                        var lstRMCriminalCaseslst = lstCriminalReports.Descendants("report-detail-header").ToList();

                        if (lstCriminalCases != null && lstCriminalCases.Count > 0)
                        {
                            #region criminal cases
                            for (int iRow = 0; iRow < lstCriminalCases.Count; iRow++)
                            {
                                bool ISDismissed = false;
                                var CriminalCasesSingle = lstCriminalCases.ElementAt(iRow);

                                List<XElement> lstCharge = CriminalCasesSingle.Descendants("report-incident").ToList<XElement>();
                                List<XElement> lstRMCharge = CriminalCasesSingle.Descendants("report-incident").ToList<XElement>();
                                if (lstCharge.Count > 0)
                                {
                                    for (int iCharge = 0; iCharge < lstCharge.Count; iCharge++)
                                    {
                                        var ChargeSingle = lstCharge.ElementAt(iCharge);

                                        var Disposition = ChargeSingle.Descendants("disposition").FirstOrDefault();

                                        if (Disposition != null)
                                        {
                                            string DispositionValue = Disposition.Value.ToString();

                                            for (int r = 0; r < s.Count(); r++)
                                            {
                                                if (string.IsNullOrEmpty(s[r]) == false)
                                                {
                                                    if (DispositionValue.ToUpper().Contains(s[r]))
                                                    {
                                                        #region ticket 481
                                                        FilterId = "5";
                                                        var NewCollection = ObjBALCompany.GetFiltersAccordingFilterId(FilterId).FirstOrDefault();
                                                        if (NewCollection.Enabled == true)
                                                        {
                                                            List<string> NewKeyword = new List<string>();
                                                            if (NewCollection != null)
                                                            {
                                                                if (string.IsNullOrEmpty(Collection.Keywords) == false)
                                                                {
                                                                    var coll = NewCollection.Keywords.ToString();
                                                                    var ArrayCollection = coll.Split(',');
                                                                    if (ArrayCollection.Count() > 0)
                                                                    {
                                                                        for (int j = 0; j < ArrayCollection.Count(); j++)
                                                                        {

                                                                            if (string.IsNullOrEmpty(ArrayCollection[j]) == false)
                                                                            {
                                                                                NewKeyword.Add(ArrayCollection[j].Trim().ToUpper()); //Adding all keywords in List<string> s = new List<string>();
                                                                            }
                                                                        }

                                                                    }

                                                                }
                                                            }

                                                            for (int p = 0; p < NewKeyword.Count(); p++)
                                                            {
                                                                if (string.IsNullOrEmpty(NewKeyword[p]) == false)
                                                                {
                                                                    var fileterKeword = NewKeyword[p].ToUpper();
                                                                    var NonfileterKeword = "";

                                                                    for (int l = 0; l < s.Count(); l++)
                                                                    {
                                                                        if (string.IsNullOrEmpty(s[l]) == false)
                                                                        {
                                                                            //if ((s[l]).Contains(fileterKeword))
                                                                            //{
                                                                            //    NonfileterKeword = s[l].ToUpper();
                                                                            //}
                                                                            if (s[l] == fileterKeword)//update ticket #145
                                                                            {
                                                                                NonfileterKeword = s[l].ToUpper();
                                                                            }
                                                                        }
                                                                    }

                                                                    if (DispositionValue.ToUpper().Contains(NewKeyword[p].ToUpper()))
                                                                    {
                                                                        if (Regex.Matches(DispositionValue.ToUpper(), fileterKeword).Count == 0)
                                                                        {
                                                                            ISDismissed = true;
                                                                            if (Collection.IsRemoveCharges == true)
                                                                            {
                                                                                lstRMCharge.Remove(ChargeSingle);
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            if (Regex.Matches(DispositionValue.ToUpper(), NonfileterKeword).Count != Regex.Matches(DispositionValue.ToUpper(), fileterKeword).Count)
                                                                            {

                                                                            }
                                                                            else
                                                                            {
                                                                                ISDismissed = true;
                                                                                if (Collection.IsRemoveCharges == true)
                                                                                {
                                                                                    lstRMCharge.Remove(ChargeSingle);
                                                                                }
                                                                            }
                                                                        }
                                                                    }

                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            ISDismissed = true;
                                                            if (Collection.IsRemoveCharges == true)
                                                            {
                                                                lstRMCharge.Remove(ChargeSingle);
                                                            }
                                                        }

                                                        #endregion
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                                else
                                {
                                    // lstRMCriminalCaseslst.Remove(CriminalCasesSingle);
                                }

                                #region Removing/Adding Charges to Current criminal Case
                                if (Collection.IsRemoveCharges == true)
                                {
                                    CriminalCasesSingle.Descendants("report-incident").Remove();

                                    foreach (XElement rm in lstRMCharge)
                                    {
                                        CriminalCasesSingle.Add(rm);
                                    }
                                }
                                #endregion

                                #region Logic to remove CriminalCase if all charges are removed

                                if (lstRMCharge.Count == 0 && ISDismissed == true)
                                {
                                    if (Collection.IsRemoveCharges == true)
                                    {
                                        lstRMCriminalCaseslst.Remove(CriminalCasesSingle);

                                    }
                                }
                                #endregion
                            }

                            #endregion criminal cases
                        }
                        if (Collection.IsRemoveCharges == true)
                        {
                            lstCriminalReports.RemoveAll();

                            foreach (XElement rm in lstRMCriminalCaseslst)
                            {
                                lstCriminalReports.Add(rm);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ObjXDocumentUppdated = XDocument.Parse(strResponseData);
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in GetRemovedDismissedChargeXMLFilterforCCR1(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }

            if (Collection.IsEmergeReview == true)
            {
                BALEmergeReview ObjBALEmergeReview = new BALEmergeReview();

                using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
                {
                    tblOrderDetail ObjOrderDetails = ObjEmergeDALDataContext.tblOrderDetails.Where(db => db.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                    if (ObjOrderDetails != null)
                    {
                        tblOrder objtblOrder = ObjEmergeDALDataContext.tblOrders.Where(db => db.pkOrderId == ObjOrderDetails.fkOrderId).FirstOrDefault();
                        if (objtblOrder.IsReviewed == false && ObjOrderDetails.ReviewStatus == 0)
                        {
                            bool Is6MonthOld = false;
                            BALGeneral ObjBALGeneral = new BALGeneral();
                            Is6MonthOld = ObjBALGeneral.Is6MonthOld(ObjOrderDetails.fkOrderId);
                            int? OrderId = ObjOrderDetails.fkOrderId;
                            int? pkOrderDetail_Id = pkOrderDetailId;
                            ObjBALEmergeReview.Email_send(OrderId, pkOrderDetail_Id);
                            ObjBALEmergeReview.UpdateReportReviewStatus(pkOrderDetail_Id, 1, string.Empty, Is6MonthOld);
                        }
                    }
                }


            }

            ObjXDocumentUppdated = ObjXDocument;
            return ObjXDocumentUppdated;
        }

        public XDocument GetRemovedDismissedChargeXMLFilterforSCR(string strResponseData, int? pkOrderDetailId)
        {
            XDocument ObjXDocumentUppdated = new XDocument();
            XDocument ObjXDocument = new XDocument();
            ObjXDocument = XDocument.Parse(strResponseData);
            BALCompany ObjBALCompany = new BALCompany();
            List<string> s = new List<string>();
            string FilterId = "3";
            var Collection = ObjBALCompany.GetFiltersAccordingFilterId(FilterId).FirstOrDefault();//Getting filter rules for Watch list FilterOR Dismissed Filter
            if (Collection != null)
            {
                if (string.IsNullOrEmpty(Collection.Keywords) == false)
                {
                    var coll = Collection.Keywords.ToString();
                    var ArrayCollection = coll.Split(',');
                    if (ArrayCollection.Count() > 0)
                    {
                        for (int j = 0; j < ArrayCollection.Count(); j++)
                        {

                            if (string.IsNullOrEmpty(ArrayCollection[j]) == false)
                            {
                                s.Add(ArrayCollection[j].Trim().ToUpper()); //Adding all keywords in List<string> s = new List<string>();
                            }
                        }

                    }

                }
            }

            try
            {
                var lstScreenings = ObjXDocument.Descendants("product").ToList();

                if (lstScreenings.Count > 0)
                {
                    var lstCriminalReports = lstScreenings.Descendants("results").FirstOrDefault();

                    if (lstCriminalReports != null)
                    {
                        var lstCriminalCases = lstCriminalReports.Descendants("result").ToList();
                        var lstRMCriminalCaseslst = lstCriminalReports.Descendants("result").ToList();

                        if (lstCriminalCases != null && lstCriminalCases.Count > 0)
                        {
                            #region criminal cases
                            for (int iRow = 0; iRow < lstCriminalCases.Count; iRow++)
                            {
                                bool ISDismissed = false;
                                var CriminalCasesSingle = lstCriminalCases.ElementAt(iRow);
                                var CriminalcountsSingle = CriminalCasesSingle.Descendants("counts").FirstOrDefault();
                                List<XElement> lstCharge = CriminalcountsSingle.Descendants("count").ToList<XElement>();
                                List<XElement> lstRMCharge = CriminalcountsSingle.Descendants("count").ToList<XElement>();
                                if (lstCharge.Count > 0)
                                {
                                    for (int iCharge = 0; iCharge < lstCharge.Count; iCharge++)
                                    {
                                        var ChargeSingle = lstCharge.ElementAt(iCharge);

                                        var Disposition = ChargeSingle.Descendants("disposition").FirstOrDefault();

                                        if (Disposition != null)
                                        {
                                            string DispositionValue = Disposition.Value.ToString();

                                            for (int r = 0; r < s.Count(); r++)
                                            {
                                                if (string.IsNullOrEmpty(s[r]) == false)
                                                {
                                                    if (DispositionValue.ToUpper().Contains(s[r]))
                                                    {
                                                        #region ticket 467
                                                        ISDismissed = true;
                                                        if (Collection.IsRemoveCharges == true)
                                                        {
                                                            lstRMCharge.Remove(ChargeSingle);
                                                        }
                                                        #endregion
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                                else
                                {
                                    // lstRMCriminalCaseslst.Remove(CriminalCasesSingle);
                                }

                                #region Removing/Adding Charges to Current criminal Case
                                if (Collection.IsRemoveCharges == true)
                                {
                                    CriminalcountsSingle.Descendants("count").Remove();

                                    foreach (XElement rm in lstRMCharge)
                                    {
                                        CriminalcountsSingle.Add(rm);
                                    }
                                }
                                #endregion

                                #region Logic to remove CriminalCase if all charges are removed

                                if (lstRMCharge.Count == 0 && ISDismissed == true)
                                {
                                    if (Collection.IsRemoveCharges == true)
                                    {
                                        lstRMCriminalCaseslst.Remove(CriminalCasesSingle);

                                    }
                                }
                                #endregion
                            }

                            #endregion criminal cases
                        }
                        if (Collection.IsRemoveCharges == true)
                        {
                            lstCriminalReports.RemoveAll();

                            foreach (XElement rm in lstRMCriminalCaseslst)
                            {
                                lstCriminalReports.Add(rm);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ObjXDocumentUppdated = XDocument.Parse(strResponseData);
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in GetRemovedDismissedChargeXMLFilterforSCR(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }

            if (Collection.IsEmergeReview == true)
            {
                BALEmergeReview ObjBALEmergeReview = new BALEmergeReview();

                using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
                {
                    tblOrderDetail ObjOrderDetails = ObjEmergeDALDataContext.tblOrderDetails.Where(db => db.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                    if (ObjOrderDetails != null)
                    {
                        tblOrder objtblOrder = ObjEmergeDALDataContext.tblOrders.Where(db => db.pkOrderId == ObjOrderDetails.fkOrderId).FirstOrDefault();
                        if (objtblOrder.IsReviewed == false && ObjOrderDetails.ReviewStatus == 0)
                        {
                            bool Is6MonthOld = false;
                            BALGeneral ObjBALGeneral = new BALGeneral();
                            Is6MonthOld = ObjBALGeneral.Is6MonthOld(ObjOrderDetails.fkOrderId);
                            int? OrderId = ObjOrderDetails.fkOrderId;
                            int? pkOrderDetail_Id = pkOrderDetailId; ;
                            ObjBALEmergeReview.Email_send(OrderId, pkOrderDetail_Id);
                            ObjBALEmergeReview.UpdateReportReviewStatus(pkOrderDetail_Id, 1, string.Empty, Is6MonthOld);
                        }
                    }
                }


            }

            ObjXDocumentUppdated = ObjXDocument;
            return ObjXDocumentUppdated;
        }

        public XDocument GetDismissedChargeResponse(string XMLLiveRunner, int? pkOrderDetailId)
        {
            string LogFilename = "XMLFilteration_XMLCaseDeletionLog_" + DateTime.Now.ToString("yyMMdd");
            string strLog = string.Empty;//For Report Log.
            BALGeneral.WriteLogForDeletion(DateTime.Now.ToString() + " Method Started : GetDismissedChargeResponse for OrderDetailId '" + Convert.ToString(pkOrderDetailId) + "'******************************", LogFilename);
            XDocument ObjXDocumentUppdatedLiveRunner = new XDocument();
            XDocument ObjXDocumentLiveRunner = new XDocument();
            string XML_LiveRunner = GetRequiredXml(XMLLiveRunner);
            ObjXDocumentLiveRunner = XDocument.Parse(XML_LiveRunner);
            BALCompany ObjBALCompany = new BALCompany();
           //bool flagforPLEA = false;
            List<string> s = new List<string>();
            string FilterId = "3";
            bool flagGuilty = false;
            var Collection = ObjBALCompany.GetFiltersAccordingFilterId(FilterId).FirstOrDefault();//Getting filter rules for Watch list FilterOR Dismissed Filter
            if (Collection != null)
            {
                if (string.IsNullOrEmpty(Collection.Keywords) == false)
                {
                    var coll = Collection.Keywords.ToString();
                    var ArrayCollection = coll.Split(',');
                    if (ArrayCollection.Count() > 0)
                    {
                        for (int j = 0; j < ArrayCollection.Count(); j++)
                        {

                            if (string.IsNullOrEmpty(ArrayCollection[j]) == false)
                            {
                                s.Add(ArrayCollection[j].Trim().ToUpper()); //Adding all keywords in List<string> s = new List<string>();
                            }
                        }

                    }

                }
            }

            try
            {
                var lstScreenings = ObjXDocumentLiveRunner.Descendants("Screenings").ToList();
                //string strRegion = string.Empty;

                if (lstScreenings.Count > 0)
                {
                    var lstCriminalReports = lstScreenings.Descendants("CriminalReport").FirstOrDefault();

                    if (lstCriminalReports != null)
                    {
                        var lstCriminalCases = lstCriminalReports.Descendants("CriminalCase").ToList();
                        var lstRMCriminalCaseslst = lstCriminalReports.Descendants("CriminalCase").ToList();

                        if (lstCriminalCases != null && lstCriminalCases.Count > 0)
                        {
                            #region criminal cases
                            for (int iRow = 0; iRow < lstCriminalCases.Count; iRow++)
                            {
                                bool ISDismissed = false;
                                var CriminalCasesSingle = lstCriminalCases.ElementAt(iRow);

                                List<XElement> lstCharge = CriminalCasesSingle.Descendants("Charge").ToList<XElement>();
                                List<XElement> lstRMCharge = CriminalCasesSingle.Descendants("Charge").ToList<XElement>();
                                if (lstCharge.Count > 0)
                                {
                                    for (int iCharge = 0; iCharge < lstCharge.Count; iCharge++)
                                    {
                                        var ChargeSingle = lstCharge.ElementAt(iCharge);

                                        var Disposition = ChargeSingle.Descendants("Disposition").FirstOrDefault();

                                        if (Disposition != null)
                                        {
                                            string DispositionValue = Disposition.Value.ToString();

                                            for (int r = 0; r < s.Count(); r++)
                                            {
                                                if (string.IsNullOrEmpty(s[r]) == false)
                                                {
                                                    //Based on June 04, 2015 client meeting, Greg said, if <Disposion> contains GUILTY in free text than it will be displayed and also bypass the <Sentance> check.
                                                    if (!DispositionValue.ToUpper().Contains("NOT GUILTY"))
                                                    {
                                                        if (DispositionValue.ToUpper().Contains("GUILTY"))//Based on June 04, 2015 client meeting
                                                        {
                                                            flagGuilty = true;
                                                            break;
                                                        }
                                                        else
                                                        {
                                                            //Original Code
                                                            if (DispositionValue.ToUpper().Contains(s[r]))
                                                            {
                                                                #region ticket 467

                                                                ISDismissed = true;
                                                                if (Collection.IsRemoveCharges == true)
                                                                {
                                                                    strLog = DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " Removed tag from disposition section line number 942\n";
                                                                    BALGeneral.WriteLogForDeletion(strLog, LogFilename);
                                                                    lstRMCharge.Remove(ChargeSingle);
                                                                }

                                                                #endregion
                                                            }
                                                        }
                                                    }
                                                    else if (DispositionValue.ToUpper().Contains("NOT GUILTY BENCH TRIAL"))//Based  on INT-276 Oct 28, 2015 Greg agreed with this condition  
                                                    {
                                                        string strDispositionValue = "NOT GUILTY BENCH TRIAL";//exists in Disposition tag in response.
                                                        //Original Code
                                                        if (strDispositionValue==(s[r]))//if dismissed filteres contains "NOT GUILTY BENCH TRIAL"
                                                        {
                                                            ISDismissed = true;
                                                            if (Collection.IsRemoveCharges == true)
                                                            {
                                                                lstRMCharge.Remove(ChargeSingle);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        var ChargeTypeClassification = ChargeSingle.Descendants("ChargeTypeClassification").FirstOrDefault();
                                        //filteration for tag <ChargeTypeClassification>infraction</ChargeTypeClassification>.

                                        if (ChargeTypeClassification != null)
                                        {
                                            string ChargeTypeClassificationValue = ChargeTypeClassification.Value.ToString();

                                            for (int r = 0; r < s.Count(); r++)
                                            {
                                                if (string.IsNullOrEmpty(s[r]) == false)
                                                {
                                                    if (ChargeTypeClassificationValue.ToUpper().Contains(s[r]))
                                                    {
                                                        #region ticket 467
                                                        ISDismissed = true;
                                                        if (Collection.IsRemoveCharges == true)
                                                        {
                                                            lstRMCharge.Remove(ChargeSingle);
                                                        }

                                                        #endregion
                                                    }
                                                }
                                            }
                                        }
                                        var Textlst = ChargeSingle.Descendants("Text").ToList();
                                        //filteration for tag <Text>NOTE: 04-22-1999 - NOLLE PROS</Text>.

                                        if (Textlst.Count > 0)
                                        {
                                            for (int iText = 0; iText < Textlst.Count; iText++)
                                            {
                                                string text = Textlst.ElementAt(iText).Value;
                                                for (int r = 0; r < s.Count(); r++)
                                                {
                                                    if (string.IsNullOrEmpty(s[r]) == false)
                                                    {
                                                        if (text.ToUpper().Contains(s[r]))
                                                        {
                                                            #region ticket 467

                                                            ISDismissed = true;
                                                            if (Collection.IsRemoveCharges == true)
                                                            {
                                                                lstRMCharge.Remove(ChargeSingle);
                                                            }

                                                            #endregion
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        var Sentencelst = ChargeSingle.Descendants("Sentence").ToList();
                                        //filteration for tag <Sentence>EXPUNGED PROBATION: 1Y</Sentence>.

                                        if (Sentencelst.Count > 0)
                                        {
                                            if (!flagGuilty)//Based on June 04, 2015 client meeting
                                            {
                                                for (int iSentence = 0; iSentence < Sentencelst.Count; iSentence++)
                                                {
                                                    string sentence = Sentencelst.ElementAt(iSentence).Value;
                                                    for (int r = 0; r < s.Count(); r++)
                                                    {
                                                        if (string.IsNullOrEmpty(s[r]) == false)
                                                        {
                                                            if (sentence.ToUpper().Contains(s[r]))// if (sentence.ToUpper().Trim() == (s[r]).ToUpper().Trim())
                                                            {
                                                                #region ticket 467

                                                                ISDismissed = true;
                                                                if (Collection.IsRemoveCharges == true)
                                                                {
                                                                    lstRMCharge.Remove(ChargeSingle);
                                                                }

                                                                #endregion
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        var Commentlst = ChargeSingle.Descendants("Comment").ToList();
                                        //filteration for tag 1.<Comment>5-6MO.SENT.ACT&#39;D;DART;PTC 108 UNCONDITIONAL DISCHARGE</Comment>.
                                        //2. CASE NOTE: DISMISSED

                                        if (Commentlst.Count > 0)
                                        {
                                            for (int iComment = 0; iComment < Commentlst.Count; iComment++)
                                            {
                                                string comment = Commentlst.ElementAt(iComment).Value;
                                                for (int r = 0; r < s.Count(); r++)
                                                {
                                                    if (string.IsNullOrEmpty(s[r]) == false)
                                                    {
                                                        if (comment.ToUpper().Contains(s[r]))
                                                        {
                                                            #region INT105 ******PLEA : NOT GUILTY
                                                            //To getting the splited by : both side value. 
                                                            string[] commentsArr = comment.Split(':');
                                                            if (commentsArr.Count() > 0)
                                                            {
                                                                //To check if Comments tag text have "PLEA : NOT GUILT"
                                                                //As per Greg comments --INT105
                                                                if (commentsArr[0].ToUpper().Trim() == "PLEA".Trim() && commentsArr[1].Contains(s[r]))
                                                                {
                                                                    //Skip removing process.
                                                                    continue;
                                                                }


                                                                //else
                                                                {
                                                                    #region ticket 467
                                                                    //Removing the Charges, if any one found.
                                                                    ISDismissed = true;
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {
                                                                        //  lstRMCharge.Remove(ChargeSingle);
                                                                    }
                                                                    #endregion
                                                                }
                                                            }
                                                            #endregion
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                                else
                                {
                                    // lstRMCriminalCaseslst.Remove(CriminalCasesSingle);
                                }

                                #region Removing/Adding Charges to Current criminal Case
                                if (Collection.IsRemoveCharges == true)
                                {
                                    CriminalCasesSingle.Descendants("Charge").Remove();
                                    strLog = DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " Removed  all charges section line number 1097\n";
                                    BALGeneral.WriteLogForDeletion(strLog, LogFilename);

                                    strLog = DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " Records added after remove all : '" + Convert.ToString(lstRMCharge.Count()) + "' line number 1100\n";
                                    BALGeneral.WriteLogForDeletion(strLog, LogFilename);
                                    foreach (XElement rm in lstRMCharge)
                                    {
                                        CriminalCasesSingle.Add(rm);
                                    }
                                }
                                #endregion

                                #region Logic to remove CriminalCase if all charges are removed

                                if (lstRMCharge.Count == 0 && ISDismissed == true)
                                {
                                    if (Collection.IsRemoveCharges == true)
                                    {
                                        strLog = DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " Logic to remove CriminalCase if all charges are removed line number 1115\n";
                                        BALGeneral.WriteLogForDeletion(strLog, LogFilename);
                                        lstRMCriminalCaseslst.Remove(CriminalCasesSingle);

                                    }
                                }
                                #endregion
                            }

                            #endregion criminal cases
                        }
                        if (Collection.IsRemoveCharges == true)
                        {
                            lstCriminalReports.RemoveAll();
                            strLog = DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " Removed  all charges section line number 1129\n";
                            BALGeneral.WriteLogForDeletion(strLog, LogFilename);

                            strLog = DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " Records added after remove all : '" + Convert.ToString(lstRMCriminalCaseslst.Count()) + "' line number 1132\n";
                            BALGeneral.WriteLogForDeletion(strLog, LogFilename);

                            foreach (XElement rm in lstRMCriminalCaseslst)
                            {
                                lstCriminalReports.Add(rm);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ObjXDocumentUppdatedLiveRunner = XDocument.Parse(XML_LiveRunner);
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog(DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + "Some Error Occured in GetDismissedChargeResponse(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }

            if (Collection.IsEmergeReview == true)
            {
                BALEmergeReview ObjBALEmergeReview = new BALEmergeReview();

                using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
                {
                    tblOrderDetail ObjOrderDetails = ObjEmergeDALDataContext.tblOrderDetails.Where(db => db.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                    if (ObjOrderDetails != null)
                    {
                        tblOrder objtblOrder = ObjEmergeDALDataContext.tblOrders.Where(db => db.pkOrderId == ObjOrderDetails.fkOrderId).FirstOrDefault();
                        if (objtblOrder.IsReviewed == false && ObjOrderDetails.ReviewStatus == 0)
                        {
                            bool Is6MonthOld = false;
                            BALGeneral ObjBALGeneral = new BALGeneral();
                            Is6MonthOld = ObjBALGeneral.Is6MonthOld(ObjOrderDetails.fkOrderId);
                            int? OrderId = ObjOrderDetails.fkOrderId;
                            int? pkOrderDetail_Id = pkOrderDetailId;
                            ObjBALEmergeReview.Email_send(OrderId, pkOrderDetail_Id);
                            ObjBALEmergeReview.UpdateReportReviewStatus(pkOrderDetail_Id, 1, string.Empty, Is6MonthOld);
                            strLog = DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " UpdateReportReviewStatus line number 1785";
                            BALGeneral.WriteLogForDeletion(strLog, LogFilename);
                        }
                    }
                }


            }
            BALGeneral.WriteLogForDeletion(DateTime.Now.ToString() + " Method End : GetDismissedChargeResponse for  OrderDetailId : '" + Convert.ToString(pkOrderDetailId) +"'*******************************\n", LogFilename);
            ObjXDocumentUppdatedLiveRunner = ObjXDocumentLiveRunner;
            return ObjXDocumentUppdatedLiveRunner;
        }


        public XDocument GetFilteredResponseforNCR1(string strResponseNCR1, int? pkOrderDetailId)
        {
            string LogFilename = "XMLFilteration_XMLCaseDeletionLog_" + DateTime.Now.ToString("yyMMdd");
            string strLog = string.Empty;//For Report Log.
            BALGeneral.WriteLogForDeletion(DateTime.Now.ToString() + " Method Started : GetFilteredResponseforNCR1 for OrderDetailId '"+Convert.ToString(pkOrderDetailId)+"'", LogFilename);
            XDocument ObjXDocumentUpdatedFilteredNCR1 = new XDocument();
            XDocument ObjXDocumentFilteredNCR1 = new XDocument();
            string XML_ResponseNCR1 = GetRequiredXml(strResponseNCR1);
            ObjXDocumentFilteredNCR1 = XDocument.Parse(XML_ResponseNCR1);
            BALCompany ObjBALCompany = new BALCompany();
            BALGlobalFilters objGlobleFilter = new BALGlobalFilters();
            List<string> s = new List<string>();
            //List<string> objList = new List<string>();
            //bool globalISDismissed = false;
            bool flagGuilty = false;
            bool IsFindDocketNumber = false;
            string FilterId = "3";
            var Collection = ObjBALCompany.GetFiltersAccordingFilterId(FilterId).FirstOrDefault();  //Getting filter rules for Watch list FilterOR Dismissed Filter
            var GlobleFilterData = objGlobleFilter.GetGlobleFiltersForRemoveRowCourtHouseData();//coded by ticket #105            
            if (Collection != null)
            {
                if (string.IsNullOrEmpty(Collection.Keywords) == false)
                {
                    var coll = Collection.Keywords.ToString();
                    var ArrayCollection = coll.Split(',');
                    if (ArrayCollection.Count() > 0)
                    {
                        for (int j = 0; j < ArrayCollection.Count(); j++)
                        {
                            if (string.IsNullOrEmpty(ArrayCollection[j]) == false)
                            {
                                s.Add(ArrayCollection[j].Trim().ToUpper()); //Adding all keywords in List<string> s = new List<string>();

                            }
                        }
                    }
                }
            }

            try
            {
                var lstScreenings = ObjXDocumentFilteredNCR1.Descendants("Screenings").ToList();
                //string strRegion = string.Empty;
                if (lstScreenings.Count > 0)
                {
                    var lstCriminalReports = lstScreenings.Descendants("CriminalReport").FirstOrDefault();
                    if (lstCriminalReports != null)
                    {
                        var lstCriminalCases = lstCriminalReports.Descendants("CriminalCase").ToList();
                        var lstRMCriminalCaseslst = lstCriminalReports.Descendants("CriminalCase").ToList();//coded by ticket #105
                        strLog =strLog+ DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " Found Criminal Cases : " + Convert.ToString(lstCriminalCases.Count() + "\n");                        
                        if (lstCriminalCases != null && lstCriminalCases.Count > 0)
                        {
                            #region criminal cases
                            for (int iRow = 0; iRow < lstCriminalCases.Count; iRow++)
                            {
                                bool ISDismissed = false;
                                var CriminalCasesSingle = lstCriminalCases.ElementAt(iRow);
                                #region CourtName
                                string strCourtName = string.Empty;
                                var CourtName = CriminalCasesSingle.Descendants("CourtName").FirstOrDefault();
                                if (CourtName != null)
                                {
                                    strCourtName = CourtName.Value.ToString();
                                }
                                #endregion
                                List<XElement> lstCharge = CriminalCasesSingle.Descendants("Charge").ToList<XElement>();
                                List<XElement> lstRMCharge = CriminalCasesSingle.Descendants("Charge").ToList<XElement>();
                                if (lstCharge.Count > 0)
                                {
                                    for (int iCharge = 0; iCharge < lstCharge.Count; iCharge++)
                                    {
                                        var ChargeSingle = lstCharge.ElementAt(iCharge);
                                        var Disposition = ChargeSingle.Descendants("Disposition").FirstOrDefault();
                                        //filteration for tags 1. <Disposition>OTHER DISMISSAL WITH PREJUDICE</Disposition>.
                                        //2. <Disposition>DISMISSED W/O LEAVE</Disposition>.
                                        //3. <Disposition>OTHER DISMISSAL WITHOUT PREJUDICE</Disposition>.
                                        //4. <Disposition>NOLLE PROSEQUI</Disposition>.
                                        //5. <Disposition>NOLLE PROSSE</Disposition>.
                                        //6. DSMD-OTHER
                                        //7. DSMD-DEF CONV OTHR
                                        //8. DSMD-DEFR ADJUD
                                        //9. NOT GUILTY
                                        //10. DISMISSED BY JUDGE
                                        //11. 208 - DISMISS
                                        //12. 209 - DISMISS/STATE MOTION
                                        //13. <Disposition>NOT CONVICTED</Disposition>
                                        //14. COMPL DISMISSED BY CTY ATTY
                                        //15. COURT DISMISSAL
                                        //16. DISMISSAL W/O LEAVE
                                        //17. ADMINISTRATIVELY DISMISSED
                                        //18. DISM
                                        //19. NOLP
                                        if (Disposition != null)
                                        {
                                            string DispositionValue = Disposition.Value.ToString();
                                            for (int r = 0; r < s.Count(); r++)
                                            {
                                                if (string.IsNullOrEmpty(s[r]) == false)
                                                {
                                                    //Based on June 04, 2015 client meeting, Greg said, if <Disposion> contains GUILTY in free text than it will be displayed and also bypass the <Sentance> check.
                                                    if (!DispositionValue.ToUpper().Contains("NOT GUILTY"))
                                                    {
                                                        if (DispositionValue.ToUpper().Contains("GUILTY"))//Based on June 04, 2015 client meeting
                                                        {
                                                            strLog = strLog + DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " Found GUILTY in disposition so it will bypass santance block\n";
                                                            flagGuilty = true;
                                                            break;
                                                        }
                                                        else
                                                        {
                                                            //Original Code
                                                            if (DispositionValue.ToUpper().Contains(s[r]))
                                                            {
                                                                #region ticket 467

                                                                ISDismissed = true;
                                                                if (Collection.IsRemoveCharges == true)
                                                                {
                                                                    strLog = strLog + DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " Removed tag from disposition section line number 1309\n";
                                                                    lstRMCharge.Remove(ChargeSingle);
                                                                }

                                                                #endregion
                                                            }
                                                        }
                                                    }
                                                    else if (DispositionValue.ToUpper().Contains("NOT GUILTY BENCH TRIAL"))//Based  on INT-276 Oct 28, 2015 Greg agreed with this condition  
                                                    {
                                                        string strDispositionValue = "NOT GUILTY BENCH TRIAL";//exists in Disposition tag in response.
                                                       //Original Code
                                                        if (strDispositionValue==(s[r]))//if dismissed filteres contains "NOT GUILTY BENCH TRIAL"
                                                        {
                                                            ISDismissed = true;
                                                            if (Collection.IsRemoveCharges == true)
                                                            {
                                                                lstRMCharge.Remove(ChargeSingle);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            // if (DispositionValue.ToUpper().Contains("NOT CONVICTED"))
                                            if (DispositionValue.ToUpper().Trim() == "NOT CONVICTED".Trim())
                                            {
                                                if (strCourtName.ToUpper().Contains("SAN BERNARDINO") || strCourtName.ToUpper().Contains("RIVERSIDE"))//#975 NCR1 for San Bernardino filtering out "Not convicted" but we need an exception for San Bernardino and Riverside'
                                                {
                                                    ISDismissed = false;

                                                }
                                                else
                                                {
                                                    #region ticket 467

                                                    ISDismissed = true;
                                                   // globalISDismissed = true;
                                                    if (Collection.IsRemoveCharges == true)
                                                    {
                                                       strLog = strLog + DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " Removed tag from disposition section line number 1336\n";
                                                       lstRMCharge.Remove(ChargeSingle);
                                                    }

                                                    #endregion
                                                }
                                            }
                                            //else if (DispositionValue.ToUpper().Trim() == "CONVICTED".Trim())
                                            //{
                                            //    //If any convicted value found in dispostion tag.
                                            //    flagConvicted=true;
                                            //}
                                        }

                                        var ChargeTypeClassification = ChargeSingle.Descendants("ChargeTypeClassification").FirstOrDefault();
                                        //filteration for tag <ChargeTypeClassification>infraction</ChargeTypeClassification>.

                                        if (ChargeTypeClassification != null)
                                        {
                                            string ChargeTypeClassificationValue = ChargeTypeClassification.Value.ToString();

                                            for (int r = 0; r < s.Count(); r++)
                                            {
                                                if (string.IsNullOrEmpty(s[r]) == false)
                                                {
                                                    if (ChargeTypeClassificationValue.ToUpper().Contains(s[r]))
                                                    {
                                                        #region ticket 467

                                                        ISDismissed = true;
                                                        if (Collection.IsRemoveCharges == true)
                                                        {
                                                            strLog += DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " Removed tag from ChargeTypeClassification section line number 1364\n";
                                                            lstRMCharge.Remove(ChargeSingle);
                                                        }
                                                        #endregion
                                                    }
                                                }
                                            }
                                        }

                                        var Textlst = ChargeSingle.Descendants("Text").ToList();
                                        //filteration for tag <Text>NOTE: 04-22-1999 - NOLLE PROS</Text>.

                                        if (Textlst.Count > 0)
                                        {
                                            for (int iText = 0; iText < Textlst.Count; iText++)
                                            {
                                                string text = Textlst.ElementAt(iText).Value;

                                                for (int r = 0; r < s.Count(); r++)
                                                {
                                                    if (string.IsNullOrEmpty(s[r]) == false)
                                                    {
                                                        if (text.ToUpper().Contains(s[r]))
                                                        {
                                                            ISDismissed = true;
                                                            if (Collection.IsRemoveCharges == true)
                                                            {
                                                                strLog += DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " Removed tag from Text section line number 1393\n";
                                                                lstRMCharge.Remove(ChargeSingle);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        var Sentencelst = ChargeSingle.Descendants("Sentence").ToList();
                                        //filteration for tag <Sentence>EXPUNGED PROBATION: 1Y</Sentence>.

                                        if (Sentencelst.Count > 0)
                                        {
                                            if (!flagGuilty)//Based on June 04, 2015 client meeting
                                            {
                                                for (int iSentence = 0; iSentence < Sentencelst.Count; iSentence++)
                                                {
                                                    string sentence = Sentencelst.ElementAt(iSentence).Value;

                                                    for (int r = 0; r < s.Count(); r++)
                                                    {
                                                        if (string.IsNullOrEmpty(s[r]) == false)
                                                        {
                                                            //if (sentence.ToUpper().Contains(s[r]))
                                                            if (sentence.ToUpper() == s[r].ToUpper())
                                                            {
                                                                #region ticket 467

                                                                ISDismissed = true;
                                                                if (Collection.IsRemoveCharges == true)
                                                                {
                                                                    strLog += DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " Removed tag from Text Santance line number 1425\n";
                                                                    lstRMCharge.Remove(ChargeSingle);
                                                                }
                                                                #endregion
                                                            }
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                        var Commentlst = ChargeSingle.Descendants("Comment").ToList();
                                        //filteration for tag 1.<Comment>5-6MO.SENT.ACT&#39;D;DART;PTC 108 UNCONDITIONAL DISCHARGE</Comment>.
                                        //2. CASE NOTE: DISMISSED

                                        if (Commentlst.Count > 0)
                                        {
                                            for (int iComment = 0; iComment < Commentlst.Count; iComment++)
                                            {
                                                string comment = Commentlst.ElementAt(iComment).Value;

                                                for (int r = 0; r < s.Count(); r++)
                                                {
                                                    if (string.IsNullOrEmpty(s[r]) == false)
                                                    {
                                                        if (comment.ToUpper().Contains(s[r]))
                                                        {
                                                            //#region ticket 467
                                                            #region INT105 ******PLEA : NOT GUILTY
                                                            //To getting the splited by : both side value. 
                                                            string[] commentsArr = comment.Split(':');
                                                            if (commentsArr.Count() > 0)
                                                            {
                                                                //To check if Comments tag text have "PLEA : NOT GUILT"
                                                                //As per Greg comments --INT105 // change by int-131 for not guilty action
                                                                if (commentsArr[0].Contains("PLEA") && commentsArr[1].Contains(s[r]))
                                                                {
                                                                    //Skip removing process.
                                                                    // ISDismissed = false;
                                                                    strLog += DateTime.Now.ToString() +  ": OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " Removed tag from Comment and found PLEA : GUILTY section line number 1466\n";
                                                                    continue;
                                                                }
                                                                else
                                                                {
                                                                    #region ticket 467
                                                                    //Removing the Charges, if any one found.
                                                                    ISDismissed = true;
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {
                                                                        strLog += DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " Removed tag from Comment and not found PLEA : GUILTY and IsRemoveCharges section line number 1477\n";
                                                                        lstRMCharge.Remove(ChargeSingle);
                                                                    }
                                                                    #endregion
                                                                }
                                                            }
                                                            #endregion

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    // lstRMCriminalCaseslst.Remove(CriminalCasesSingle);
                                }

                                #region Removing/Adding Charges to Current criminal Case
                                if (Collection.IsRemoveCharges == true && ISDismissed == true)
                                {
                                    CriminalCasesSingle.Descendants("Charge").Remove();
                                    strLog += DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " Records added after remove all : '" + Convert.ToString(lstRMCharge.Count())+ "' line number 1507\n";
                                    foreach (XElement rm in lstRMCharge)
                                    {   
                                        CriminalCasesSingle.Add(rm);
                                    }
                                }
                                #endregion

                                #region Logic to remove CriminalCase if all charges are removed

                                if (lstRMCharge.Count == 0 && ISDismissed == true)
                                {
                                    strLog += DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " remove CriminalCase if all charges are removed line number 1520\n";
                                    if (Collection.IsRemoveCharges == true)
                                    {
                                       lstRMCriminalCaseslst.Remove(CriminalCasesSingle);
                                    }
                                }
                                #endregion
                                #region Logic to remove CriminalCase if "FILTER REQUEST: Juvenile records out of Washington."
                                if (lstRMCriminalCaseslst.Count > 0)
                                {
                                    for (int i = 0; i < lstRMCriminalCaseslst.Count; i++)
                                    {
                                        XElement lstJ = lstRMCriminalCaseslst[i];

                                        var AdditionalItemsLst = lstJ.Descendants("AdditionalItems").ToList();//INT-143
                                        if (AdditionalItemsLst.Count() > 0 && AdditionalItemsLst != null)
                                        {
                                            foreach (XElement lstJInner in AdditionalItemsLst)
                                            {
                                                if (lstJInner.Descendants("Text").FirstOrDefault() != null)
                                                {
                                                    string sCaseType = lstJInner.Descendants("Text").FirstOrDefault().Value.Trim().ToUpper();
                                                    if (!string.IsNullOrEmpty(sCaseType.Split(':')[0]))
                                                    {
                                                        string sCType = sCaseType.Split(':')[0].Trim().ToUpper();
                                                        if (sCType == "CASE TYPE")
                                                        {
                                                            if (lstJInner.Descendants("Text").FirstOrDefault() != null)
                                                            {
                                                                string sJuvenile = lstJInner.Descendants("Text").FirstOrDefault().Value.Trim().ToUpper();
                                                                if (!string.IsNullOrEmpty(sJuvenile.Split(':')[1]))
                                                                {
                                                                    string sval = sJuvenile.Split(':')[1].Trim().ToUpper();
                                                                    if (sval == "JUVENILE" || sval == "J")
                                                                    {
                                                                        strLog += DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " CASE TYPE : J or JUVENILE line number 1557";                                                                        
                                                                        lstRMCriminalCaseslst.Remove(lstJ);//INT143
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }
                                #endregion
                                #region ticket #105
                                // block record through globle filter

                                if (GlobleFilterData != null && GlobleFilterData.Count > 0)
                                {
                                    string dataSourceValue = string.Empty;
                                    string devilaryAddressValue = string.Empty;
                                    string docketValue = string.Empty;
                                    string dataSource = string.Empty;
                                    string docketNumber = string.Empty;
                                    string deliveryAddress = string.Empty;


                                    var CourtNameValue = CriminalCasesSingle.Descendants("CourtName").FirstOrDefault();
                                    if (CourtNameValue != null)
                                    {
                                        dataSourceValue = CourtNameValue.Value.ToString();
                                    }
                                    docketValue = CriminalCasesSingle.Descendants("AgencyReference").Where(x => (string)x.Attribute("type") == "Docket").Select(x => (string)x.Element("IdValue").Value).FirstOrDefault();
                                    var DevileryAddress = CriminalCasesSingle.Descendants("DeliveryAddress").FirstOrDefault();
                                    var MunicipalityAddress = CriminalCasesSingle.Descendants("Municipality").FirstOrDefault();

                                    if (DevileryAddress != null)
                                    {
                                        devilaryAddressValue = DevileryAddress.Value.ToString();
                                    }
                                    else
                                    {
                                        if (MunicipalityAddress != null)
                                        {
                                            devilaryAddressValue = MunicipalityAddress.Value.ToString();
                                        }
                                        else
                                        {
                                            devilaryAddressValue = string.Empty;
                                        }
                                    }
                                    for (int objIndex = 0; objIndex < GlobleFilterData.Count; objIndex++)
                                    {
                                        dataSource = GlobleFilterData.ElementAt(objIndex).Source;
                                        docketNumber = GlobleFilterData.ElementAt(objIndex).DocketNumber;
                                        deliveryAddress = GlobleFilterData.ElementAt(objIndex).DeliveryAddress;

                                        if (!string.IsNullOrEmpty(dataSource) && !string.IsNullOrEmpty(docketNumber) && !string.IsNullOrEmpty(deliveryAddress))
                                        {
                                            if (!string.IsNullOrEmpty(dataSourceValue) && !string.IsNullOrEmpty(docketValue) && !string.IsNullOrEmpty(devilaryAddressValue))
                                            {
                                                if (docketNumber.ToUpper() == docketValue.ToUpper() && dataSource.ToUpper() == dataSourceValue.ToUpper() && deliveryAddress.ToUpper() == devilaryAddressValue.ToUpper())
                                                {
                                                    strLog += DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " Removed by Global filter if (docketNumber.ToUpper() == docketValue.ToUpper() && dataSource.ToUpper() == dataSourceValue.ToUpper() && deliveryAddress.ToUpper() == devilaryAddressValue.ToUpper() line number 1622";
                                                    lstRMCriminalCaseslst.Remove(CriminalCasesSingle);
                                                    IsFindDocketNumber = true;
                                                }
                                            }
                                        }
                                        else if (!string.IsNullOrEmpty(dataSource) && !string.IsNullOrEmpty(docketNumber))
                                        {
                                            if (!string.IsNullOrEmpty(dataSourceValue) && !string.IsNullOrEmpty(docketValue))
                                            {
                                                if (docketNumber.ToUpper() == docketValue.ToUpper() && dataSource.ToUpper() == dataSourceValue.ToUpper())
                                                {
                                                    strLog += DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " Removed by Global filter if (docketNumber.ToUpper() == docketValue.ToUpper() && dataSource.ToUpper() == dataSourceValue.ToUpper() line number 1635";
                                                    lstRMCriminalCaseslst.Remove(CriminalCasesSingle);
                                                    IsFindDocketNumber = true;
                                                }
                                            }

                                        }
                                        else if (!string.IsNullOrEmpty(dataSource) && !string.IsNullOrEmpty(deliveryAddress))
                                        {
                                            if (!string.IsNullOrEmpty(dataSourceValue) && !string.IsNullOrEmpty(devilaryAddressValue))
                                            {
                                                if (dataSource.ToUpper() == dataSourceValue.ToUpper() && deliveryAddress.ToUpper() == devilaryAddressValue.ToUpper())
                                                {
                                                    strLog += DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " Removed by Global filter if (dataSource.ToUpper() == dataSourceValue.ToUpper() && deliveryAddress.ToUpper() == devilaryAddressValue.ToUpper() line number 1649";
                                                    lstRMCriminalCaseslst.Remove(CriminalCasesSingle);
                                                    IsFindDocketNumber = true;
                                                }
                                            }

                                        }
                                        else if (!string.IsNullOrEmpty(docketNumber) && !string.IsNullOrEmpty(deliveryAddress))
                                        {
                                            if (!string.IsNullOrEmpty(docketValue) && !string.IsNullOrEmpty(devilaryAddressValue))
                                            {
                                                if (docketNumber.ToUpper() == docketValue.ToUpper() && deliveryAddress.ToUpper() == devilaryAddressValue.ToUpper())
                                                {
                                                    strLog += DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " Removed by Global filter if (docketNumber.ToUpper() == docketValue.ToUpper() && deliveryAddress.ToUpper() == devilaryAddressValue.ToUpper() line number 1663";
                                                    lstRMCriminalCaseslst.Remove(CriminalCasesSingle);
                                                    IsFindDocketNumber = true;
                                                }
                                            }
                                        }
                                        else if (!string.IsNullOrEmpty(dataSource))
                                        {
                                            if (!string.IsNullOrEmpty(dataSourceValue))
                                            {
                                                if (dataSource.ToUpper() == dataSourceValue.ToUpper())
                                                {
                                                    strLog += DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " Removed by Global filter if (dataSource.ToUpper() == dataSourceValue.ToUpper() line number 1677";
                                                    lstRMCriminalCaseslst.Remove(CriminalCasesSingle);
                                                    IsFindDocketNumber = true;
                                                }
                                            }
                                        }
                                        else if (!string.IsNullOrEmpty(docketNumber))
                                        {
                                            if (!string.IsNullOrEmpty(docketValue))
                                            {
                                                if (docketNumber.ToUpper() == docketValue.ToUpper())
                                                {
                                                    strLog += DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " Removed by Global filter if (docketNumber.ToUpper() == docketValue.ToUpper() line number 1689";
                                                    lstRMCriminalCaseslst.Remove(CriminalCasesSingle);
                                                    IsFindDocketNumber = true;
                                                }
                                            }
                                        }
                                        else if (!string.IsNullOrEmpty(deliveryAddress))
                                        {
                                            if (!string.IsNullOrEmpty(devilaryAddressValue))
                                            {
                                                if (deliveryAddress.ToUpper() == devilaryAddressValue.ToUpper())
                                                {
                                                    strLog += DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " Removed by Global filter if (deliveryAddress.ToUpper() == devilaryAddressValue.ToUpper() line number 1702";
                                                    lstRMCriminalCaseslst.Remove(CriminalCasesSingle);
                                                    IsFindDocketNumber = true;
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                            #endregion criminal cases
                        }
                        // change for int -131

                        if (Collection.IsRemoveCharges == true)

                        //&& globalISDismissed == true change by sir int=143 on 9 march 2015
                        {
                            lstCriminalReports.RemoveAll();
                            strLog += DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " Records added after remove all : '" + Convert.ToString(lstRMCriminalCaseslst.Count()) + "' line number 1728\n";
                            foreach (XElement rm in lstRMCriminalCaseslst)
                            {
                                lstCriminalReports.Add(rm);
                            }
                        }

                        if (IsFindDocketNumber)
                        {
                            lstCriminalReports.RemoveAll();
                            strLog += DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " Records added after remove all : '" + Convert.ToString(lstRMCriminalCaseslst.Count()) + "' line number 1742\n";
                            foreach (XElement rm in lstRMCriminalCaseslst)
                            {
                                lstCriminalReports.Add(rm);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ObjXDocumentUpdatedFilteredNCR1 = XDocument.Parse(XML_ResponseNCR1);
                //Added log for exception handling.
                strLog += " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + "Some Error Occured in GetFilteredResponseforNCR1(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace;
                BALGeneral.WriteLogForDeletion(strLog, LogFilename);
            }

            if (Collection.IsEmergeReview == true)
            {
                strLog += DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " if (Collection.IsEmergeReview == true) line number 1762\n";
                BALEmergeReview ObjBALEmergeReview = new BALEmergeReview();
                using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
                {
                    tblOrderDetail ObjOrderDetails = ObjEmergeDALDataContext.tblOrderDetails.Where(db => db.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                    if (ObjOrderDetails != null)
                    {
                        tblOrder objtblOrder = ObjEmergeDALDataContext.tblOrders.Where(db => db.pkOrderId == ObjOrderDetails.fkOrderId).FirstOrDefault();
                        if (objtblOrder.IsReviewed == false && ObjOrderDetails.ReviewStatus == 0)
                        {
                            strLog += DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " if (objtblOrder.IsReviewed == false && ObjOrderDetails.ReviewStatus == 0) line number 1775";
                            bool Is6MonthOld = false;
                            BALGeneral ObjBALGeneral = new BALGeneral();
                            Is6MonthOld = ObjBALGeneral.Is6MonthOld(ObjOrderDetails.fkOrderId);
                            int? OrderId = ObjOrderDetails.fkOrderId;
                            int? pkOrderDetail_Id = pkOrderDetailId;
                            ObjBALEmergeReview.Email_send(OrderId, pkOrderDetail_Id);
                            ObjBALEmergeReview.UpdateReportReviewStatus(pkOrderDetail_Id, 1, string.Empty, Is6MonthOld);
                            strLog += DateTime.Now.ToString() + " OrderDetailId : " + Convert.ToString(pkOrderDetailId) + " UpdateReportReviewStatus line number 1785";
                        }
                    }
                }
            }
            strLog+=DateTime.Now.ToString()+ " Method End : GetFilteredResponseforNCR1 for OrderDetailId '"+Convert.ToString(pkOrderDetailId)+"'******************************\n";
            BALGeneral.WriteLogForDeletion(strLog, LogFilename);
            ObjXDocumentUpdatedFilteredNCR1 = ObjXDocumentFilteredNCR1;
            return ObjXDocumentUpdatedFilteredNCR1;
        }

        #endregion

        /// <summary>
        /// Function to Check the NCR's HIT.
        /// </summary>
        /// <param name="pkOrderDetailId"></param>
        /// <param name="ProductCode"></param>
        /// <param name="ObjXDocument"></param>
        /// <returns></returns>
        public void CheckIsHit(int? OrderId, string ProductCode, XDocument ObjXDocument)
        {
            try
            {
                if (ProductCode == "NCR1" || ProductCode == "NCR+" || ProductCode == "RCX" || ProductCode == "SCRLiveRunner" || ProductCode == "CCR1LiveRunner" || ProductCode == "CCR2LiveRunner")
                {
                    //int RemainingNUllDOB = 0;
                    //int Totalcount = 0;
                    //int checkRecordFilterOut = 0;

                    bool flagVariation = false;
                    bool flagExact = false;

                    string XML_Value = BALGeneral.GetRequiredXml_ForRapidCourt(ObjXDocument.ToString());
                    XDocument ObjXDocument1 = XDocument.Parse(XML_Value);
                    var lstScreenings = ObjXDocument1.Descendants("Screenings").ToList();
                    if (lstScreenings.Count > 0)
                    {
                        var lstCriminalReports = lstScreenings.Descendants("CriminalReport").FirstOrDefault();
                        //Check record 100% filter out in NCR1 reports for ticket #104
                        if (lstCriminalReports != null)
                        {
                            var hasDeleted = lstCriminalReports.Descendants("CriminalCase").Where(x => x.HasAttributes).FirstOrDefault();
                            if (hasDeleted == null)
                            {
                                var lstCriminalCases = lstCriminalReports.Descendants("CriminalCase").ToList();
                                if (lstCriminalCases != null && lstCriminalCases.Count > 0)
                                {
                                    foreach (XElement user in lstCriminalReports.Descendants("CriminalCase"))
                                    {
                                        var dob = user.Descendants("DateOfBirth");
                                        var IsAdded = user.Descendants("DemographicDetail").Where(x => x.HasAttributes && x.Attribute("IsAdded").Value == "true");
                                        if (dob.Count() == 0 && IsAdded.Count() == 0)
                                        {
                                            flagVariation = true;
                                        }

                                        else
                                        {
                                            flagVariation = false;
                                        }

                                        if (dob.Count() > 0 || IsAdded.Count() > 0)
                                        {
                                            flagExact = true;
                                        }
                                        else
                                        {
                                            flagExact = false;
                                        }
                                    }

                                    if (flagVariation == true || flagExact == true)
                                    {
                                        UpdateOrderHitStatus(OrderId, 1);//Ticket#184
                                    }

                                }
                                else
                                {
                                    UpdateOrderHitStatus(OrderId, 2);
                                }
                            }
                            else
                            {
                                var lstCriminalCases = lstCriminalReports.Descendants("CriminalCase").Where(x => x.Attribute("Deleted").Value == "0").ToList();
                                if (lstCriminalCases != null && lstCriminalCases.Count > 0)
                                {
                                    foreach (XElement user in lstCriminalReports.Descendants("CriminalCase"))
                                    {
                                        var dob = user.Descendants("DateOfBirth");
                                        var IsAdded = user.Descendants("DemographicDetail").Where(x => x.HasAttributes && x.Attribute("IsAdded").Value == "true");
                                        if (dob.Count() == 0 && IsAdded.Count() == 0)
                                        {
                                            flagVariation = true;
                                        }

                                        else
                                        {
                                            flagVariation = false;
                                        }

                                        if (dob.Count() > 0 || IsAdded.Count() > 0)
                                        {
                                            flagExact = true;
                                        }
                                        else
                                        {
                                            flagExact = false;
                                        }
                                    }

                                    if (flagVariation == true || flagExact == true)
                                    {
                                        UpdateOrderHitStatus(OrderId, 1);//Ticket#184
                                    }

                                }
                                else
                                {
                                    UpdateOrderHitStatus(OrderId, 2);
                                }
                            }


                        }
                        else
                        {
                            UpdateOrderHitStatus(OrderId, 2);
                        }

                    }
                }
                if (ProductCode == "NCR2")
                {
                    var lstreportdetailheader = ObjXDocument.Descendants("report-detail-header").ToList();
                    if (lstreportdetailheader.Count > 0)
                    {
                        var lstreportdata = lstreportdetailheader.Descendants("report-data").FirstOrDefault();

                        if (lstreportdata != null)
                        {
                            string reportdataValue = lstreportdata.Value.ToString();
                            if (!string.IsNullOrEmpty(reportdataValue))
                            {
                                UpdateOrderHitStatus(OrderId, 1);
                            }
                            else
                            {
                                UpdateOrderHitStatus(OrderId, 2);
                            }
                        }
                        else
                        {
                            UpdateOrderHitStatus(OrderId, 2);
                        }
                    }
                }
                if (ProductCode == "NCR4")
                {
                    var lstResponse = ObjXDocument.Descendants("Response").ToList();
                    if (lstResponse.Count > 0)
                    {
                        var lstCriminalSearchResults = lstResponse.Descendants("CriminalSearchResults").FirstOrDefault();

                        if (lstCriminalSearchResults != null)
                        {
                            var lstJurisdictionResults = lstCriminalSearchResults.Descendants("JurisdictionResults").ToList();
                            if (lstJurisdictionResults != null && lstJurisdictionResults.Count > 0)
                            {
                                UpdateOrderHitStatus(OrderId, 1);
                            }
                            else
                            {
                                UpdateOrderHitStatus(OrderId, 2);
                            }
                        }
                        else
                        {
                            UpdateOrderHitStatus(OrderId, 2);
                        }
                    }
                }
                #region-------Code Commented SCR Move to Xpedit (CCR1)
                //if (ProductCode == "SCR")
                //{
                //    var xElementresults = ObjXDocument.Descendants("results").ToList();
                //    if (xElementresults != null)
                //    {
                //        var resultColl = ObjXDocument.Descendants("result").ToList();
                //        if (resultColl != null && resultColl.Count > 0)
                //        {
                //            UpdateOrderHitStatus(OrderId, 1);
                //        }
                //        else
                //        {
                //            UpdateOrderHitStatus(OrderId, 2);
                //        }
                //    }
                //    else
                //    {
                //        UpdateOrderHitStatus(OrderId, 2);
                //    }
                //}
                #endregion

                if (ProductCode == "SOR")
                {
                    var xElementresponse = ObjXDocument.Descendants("response").ToList();
                    if (xElementresponse != null)
                    {
                        var detail = ObjXDocument.Descendants("detail").FirstOrDefault();
                        if (detail != null)
                        {
                            var offenders = ObjXDocument.Descendants("offenders").FirstOrDefault();
                            if (offenders != null && offenders.Attribute("qtyFound").Value != "0")
                            {
                                UpdateOrderHitStatus(OrderId, 1);
                            }
                            else
                            {
                                UpdateOrderHitStatus(OrderId, 2);
                            }
                        }
                        else
                        {
                            UpdateOrderHitStatus(OrderId, 2);
                        }
                    }
                }
                if (ProductCode == "OFAC")
                {
                    var xElementresponse = ObjXDocument.Descendants("response_data").ToList();
                    if (xElementresponse != null)
                    {
                        var response_rowColl = ObjXDocument.Descendants("response_row").ToList();
                        if (response_rowColl != null && response_rowColl.Count > 0)
                        {
                            UpdateOrderHitStatus(OrderId, 1);
                        }
                        else
                        {
                            UpdateOrderHitStatus(OrderId, 2);
                        }
                    }
                    else
                    {
                        UpdateOrderHitStatus(OrderId, 2);
                    }
                }
                if (ProductCode == "CCR1" || ProductCode == "SCR")
                {
                    var xElementresponse = ObjXDocument.Descendants("report-detail-header").ToList();
                    if (xElementresponse != null && xElementresponse.Count > 0)
                    {
                        var response_clear_record = ObjXDocument.Descendants("clear-record").ToList();
                        if (response_clear_record != null && response_clear_record.FirstOrDefault().Value.ToString() == "0")
                        {
                            UpdateOrderHitStatus(OrderId, 1);
                        }
                        else
                        {
                            UpdateOrderHitStatus(OrderId, 2);
                        }
                    }
                    else
                    {
                        UpdateOrderHitStatus(OrderId, 2);
                    }
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in CheckIsHit(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
        }

        public int UpdateOrderHitStatus(int? pkOrderId, byte HitStatus)
        {
            int Status = 0;
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    if (DX.tblOrders.Where(db => db.pkOrderId == pkOrderId).Count() > 0)
                    {
                        tblOrder ObjtblOrder = DX.tblOrders.Where(db => db.pkOrderId == pkOrderId).FirstOrDefault();
                        if (ObjtblOrder != null)
                        {
                            #region----Old Code
                            //if (HitStatus == 1)
                            //{
                            //    if (ObjtblOrder.HitStatus != 2)//INT28
                            //    {
                            //        ObjtblOrder.HitStatus = HitStatus;
                            //    }
                            //}
                            //else
                            //{
                            //    if (ObjtblOrder.HitStatus != 1)
                            //    {
                            //        ObjtblOrder.HitStatus = HitStatus;
                            //    }
                            //}
                            #endregion

                            #region-----New Code
                            //Red-hit status change based on comming hit status.
                            // HitValue       HitValue in table     Result
                            //    1                 1              (1) Red    
                            //    1                 2              (1) Red
                            //    2                 1              (1) Red
                            //    2                 2              (2) Green

                            if ((HitStatus == 1 && ObjtblOrder.HitStatus == 1) || (HitStatus == 1 && ObjtblOrder.HitStatus == 2) || (HitStatus == 2 && ObjtblOrder.HitStatus == 1) || (HitStatus == 1 && ObjtblOrder.HitStatus == 0))
                            {
                                ObjtblOrder.HitStatus = 1; //If record found.
                            }
                            else if (HitStatus == 2 && ObjtblOrder.HitStatus == 2 || (HitStatus == 2 && ObjtblOrder.HitStatus == 0))
                            {
                                ObjtblOrder.HitStatus = 2; //If no record found.
                            }
                            else
                            {
                                ObjtblOrder.HitStatus = 0;//If nothing is exists regarding hit status.
                            }
                            #endregion
                            DX.SubmitChanges();
                            Status = 1;
                        }
                    }
                    else
                    {
                        tblOrders_Archieve ObjtblOrder_Archieve = DX.tblOrders_Archieves.Where(db => db.pkOrderId == pkOrderId).FirstOrDefault();
                        if (ObjtblOrder_Archieve != null)
                        {
                            if (HitStatus == 1)
                            {
                                ObjtblOrder_Archieve.HitStatus = HitStatus;
                            }
                            else
                            {
                                if (ObjtblOrder_Archieve.HitStatus != 1)
                                {
                                    ObjtblOrder_Archieve.HitStatus = HitStatus;
                                }
                            }
                            DX.SubmitChanges();
                            Status = 1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateOrderHitStatus(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            return Status;
        }
        /// <summary>
        /// To update hit status manually INT28 JULY,01 2015 as per Greg comments in JIRA
        /// </summary>
        /// <param name="pkOrderId"></param>
        /// <param name="HitStatus"></param>
        /// <returns></returns>
        public int UpdateOrderHitStatusManually(int? pkOrderId, byte HitStatus)
        {
            int Status = 0;
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    if (DX.tblOrders.Where(db => db.pkOrderId == pkOrderId).Count() > 0)
                    {
                        tblOrder ObjtblOrder = DX.tblOrders.Where(db => db.pkOrderId == pkOrderId).FirstOrDefault();
                        if (ObjtblOrder != null)
                        {
                            ObjtblOrder.HitStatus = HitStatus;
                            DX.SubmitChanges();
                            Status = 1;
                        }
                    }
                    else
                    {
                        tblOrders_Archieve ObjtblOrder_Archieve = DX.tblOrders_Archieves.Where(db => db.pkOrderId == pkOrderId).FirstOrDefault();
                        if (ObjtblOrder_Archieve != null)
                        {
                            ObjtblOrder_Archieve.HitStatus = HitStatus;
                            DX.SubmitChanges();
                            Status = 1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateOrderHitStatusManually(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            return Status;
        }





        public static string GetRequiredXml(string XmlResponse)
        {
            string XmlResponse_Local = XmlResponse;
            XmlResponse_Local = XmlResponse_Local.Replace("</BackgroundReports>", "");
            int xmlstartindex = XmlResponse_Local.IndexOf("<BackgroundReportPackage>");
            XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
            return XmlResponse_Local;
        }

    }
}
