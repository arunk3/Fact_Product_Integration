﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;

namespace Emerge.Services
{
    public class BALDrug
    {

        int success = 0;

        public List<Proc_GetDrugProductsResult> GetDrugProducts()
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                return Dx.GetDrugProducts().ToList();
            }
        }

        public List<Proc_GetDrugProductforSortChangeResult> GetDrugProductforSortChange(byte SortOrder)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                return Dx.GetDrugProductforSortChange(SortOrder).ToList();
            }
        }

        public List<clsDrugTestingKit> GetDrugProducts(int fkReportCategoryId)
        {
            List<clsDrugTestingKit> ObjList = new List<clsDrugTestingKit>();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {

                ObjList = (from P in DX.tblProducts
                           join PPA in DX.tblProductPerApplications
                           on P.pkProductId equals PPA.fkProductId
                           where (PPA.fkReportCategoryId == fkReportCategoryId && PPA.IsDeleted == false)
                           select new clsDrugTestingKit
                           {
                               CreatedDate = PPA.CreatedDate,
                               DrugTestCategoryId = PPA.DrugTestCategoryId,
                               DrugTestCategoryName = (DX.tblDrugTestCategories.Where(d => d.DrugTestCategoryId == PPA.DrugTestCategoryId).Select(d => d.DrugTestCategoryName).FirstOrDefault().ToString()),
                               DrugTestPanels = PPA.DrugTestPanels,
                               DrugTestQty = PPA.DrugTestQty,
                               fkReportCategoryId = PPA.fkReportCategoryId,
                               IsDefault = PPA.IsDefault,
                               IsDeleted = PPA.IsDeleted,
                               IsEnabled = PPA.IsEnabled,
                               IsGlobal = PPA.IsGlobal,
                               IsNonMemberRpt = PPA.IsNonMemberRpt,
                               LongDescription = PPA.LongDescription,
                               pkProductApplicationId = PPA.pkProductApplicationId,
                               pkProductId = PPA.fkProductId,
                               ProductDisplayName = PPA.ProductDisplayName,
                               ProductImage = PPA.ProductImage.ToString(),
                               ProductPerApplicationPrice = PPA.ProductPerApplicationPrice,
                               ProductCode = P.ProductCode,
                               SortOrder = P.SortOrder

                           }).OrderBy(d => d.SortOrder).ToList<clsDrugTestingKit>();


            }
            return ObjList;
        }

        public List<clsDrugTestingKit> GetDrugProductsingle(int pkproductId)
        {
            List<clsDrugTestingKit> ObjList = new List<clsDrugTestingKit>();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {

                ObjList = (from P in DX.tblProducts
                           join PPA in DX.tblProductPerApplications
                           on P.pkProductId equals PPA.fkProductId
                           where (PPA.fkProductId == pkproductId)
                           select new clsDrugTestingKit
                           {
                               CreatedDate = PPA.CreatedDate,
                               DrugTestCategoryId = PPA.DrugTestCategoryId,
                               DrugTestPanels = PPA.DrugTestPanels,
                               DrugTestQty = PPA.DrugTestQty,
                               fkReportCategoryId = PPA.fkReportCategoryId,
                               IsDefault = PPA.IsDefault,
                               IsDeleted = PPA.IsDeleted,
                               IsEnabled = PPA.IsEnabled,
                               IsGlobal = PPA.IsGlobal,
                               IsNonMemberRpt = PPA.IsNonMemberRpt,
                               LongDescription = PPA.LongDescription,
                               pkProductApplicationId = PPA.pkProductApplicationId,
                               pkProductId = PPA.fkProductId,
                               ProductDisplayName = PPA.ProductDisplayName,
                               ProductImage = PPA.ProductImage.ToString(),
                               ProductPerApplicationPrice = PPA.ProductPerApplicationPrice,
                               ProductCode = P.ProductCode,
                               SortOrder = P.SortOrder

                           }).OrderBy(d => d.SortOrder).ToList<clsDrugTestingKit>();


            }
            return ObjList;
        }

        public int AddDrugProduct(tblProduct ObjtblProduct, tblProductPerApplication ObjtblProductPerApplication)
        {
            int? PkProductID = null;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                ObjtblProduct.SortOrder = Convert.ToByte(GetMaxSortOrderWithIncrement());
                DX.tblProducts.InsertOnSubmit(ObjtblProduct);
                DX.SubmitChanges();
                PkProductID = ObjtblProduct.pkProductId;
                if (PkProductID != null)
                {
                    ObjtblProductPerApplication.fkProductId = PkProductID;
                    DX.tblProductPerApplications.InsertOnSubmit(ObjtblProductPerApplication);
                    DX.SubmitChanges();
                    success = 1;
                }
                else
                {
                    success = -1;
                }

            }
            return success;
        }

        public int UpdateDrugProduct(tblProduct ObjtblProduct, tblProductPerApplication ObjtblProductPerApplication)
        {
            tblProduct _tblProduct = new tblProduct();
            tblProductPerApplication _tblProductPerApplication = new tblProductPerApplication();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                _tblProduct = DX.tblProducts.Where(d => d.pkProductId == ObjtblProduct.pkProductId).FirstOrDefault();
                _tblProductPerApplication = DX.tblProductPerApplications.Where(d => d.fkProductId == ObjtblProductPerApplication.fkProductId).FirstOrDefault();
                if (_tblProduct != null && _tblProductPerApplication != null)
                {
                    #region tblproduct
                    _tblProduct.IsEnabled = true; /* By default this should be true */
                    _tblProduct.LastModifiedDate = ObjtblProduct.LastModifiedDate;
                    _tblProduct.ProductCode = ObjtblProduct.ProductCode;
                    _tblProduct.ProductDefaultPrice = ObjtblProduct.ProductDefaultPrice;
                    _tblProduct.ProductDescription = ObjtblProduct.ProductDescription;
                    #endregion

                    #region tblproductperapplication
                    _tblProductPerApplication.DrugTestCategoryId = ObjtblProductPerApplication.DrugTestCategoryId;
                    _tblProductPerApplication.DrugTestPanels = ObjtblProductPerApplication.DrugTestPanels;
                    _tblProductPerApplication.DrugTestQty = ObjtblProductPerApplication.DrugTestQty;
                    _tblProductPerApplication.fkReportCategoryId = ObjtblProductPerApplication.fkReportCategoryId;
                    _tblProductPerApplication.IsDefault = ObjtblProductPerApplication.IsDefault;
                    _tblProductPerApplication.IsEnabled = ObjtblProductPerApplication.IsEnabled;
                    _tblProductPerApplication.IsGlobal = ObjtblProductPerApplication.IsGlobal;
                    _tblProductPerApplication.LastModifiedDate = ObjtblProductPerApplication.LastModifiedDate;
                    _tblProductPerApplication.LongDescription = ObjtblProductPerApplication.LongDescription;
                    _tblProductPerApplication.ProductDisplayName = ObjtblProductPerApplication.ProductDisplayName;
                    //_tblProductPerApplication.ProductImage = ObjtblProductPerApplication.ProductImage;
                    _tblProductPerApplication.ProductPerApplicationPrice = ObjtblProductPerApplication.ProductPerApplicationPrice;
                    #endregion

                    DX.SubmitChanges();
                    success = 1;
                }

            }
            return success;
        }

        public int DeleteDrugProduct(int PkProductId, out string ImageName)
        {
            string PImgName = string.Empty;
            tblProduct _tblProduct = new tblProduct();
            tblProductPerApplication _tblProductPerApplication = new tblProductPerApplication();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                _tblProduct = DX.tblProducts.Where(d => d.pkProductId == PkProductId).FirstOrDefault();
                _tblProductPerApplication = DX.tblProductPerApplications.Where(d => d.fkProductId == PkProductId).FirstOrDefault();
                if (_tblProduct != null && _tblProductPerApplication != null)
                {
                    _tblProduct.ProductCode = _tblProduct.ProductCode + "_DELETED";
                    _tblProduct.IsDeleted = true;
                  
                    //_tblProductPerApplication.ProductCode = _tblProductPerApplication.ProductCode + "_DELETED";
                    //_tblProductPerApplication.IsDeleted = true;
                    PImgName = _tblProductPerApplication.ProductImage;
                    DX.SubmitChanges();
                    success = 1;
                }
            }
            ImageName = PImgName;
            return success;
        }

        public List<tblDrugTestCategory> GetDrugTestCategories()
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return DX.tblDrugTestCategories.ToList<tblDrugTestCategory>();
            }
        }
        public proc_GetDrugTestingDetailsByOrderIDResult GetDrugTestingDetailsByOrderId(int OrderId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return DX.GetDrugTestingDetailsByOrderID(OrderId).FirstOrDefault();
            }
        }
        public int CancelDrugTestingByOrderId(int pkOrderDetailId, byte OrderStatus)
        {
            int status = 0;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                tblOrder ObjtblOrder = DX.tblOrders.Where(db => db.pkOrderId == pkOrderDetailId).FirstOrDefault();
                if (ObjtblOrder != null)
                {
                    ObjtblOrder.OrderStatus = OrderStatus;
                    DX.SubmitChanges();
                    status = 1;
                }
                tblOrderDetail Objtbl = DX.tblOrderDetails.Where(d => d.fkOrderId == pkOrderDetailId).FirstOrDefault();
                if (Objtbl != null)
                {
                    Objtbl.ReportStatus = OrderStatus;
                    DX.SubmitChanges();
                    status = 1;
                }
            }
            return status;
        }
        public int SaveDetailsForDrugTestingByOrderId(tblOrderDetail ObjtblOrderDetail, tblCompany_Price ObjtblCompany_Price, tblOrder ObjtblOrder, bool IsApplyCompanyPricing)
        {
            int iResult = -1;
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    DX.proc_SaveDetailsForDrugTestingByOrderId(ObjtblOrderDetail.pkOrderDetailId, ObjtblOrderDetail.ReportAmount, ObjtblOrderDetail.fkProductApplicationId,
                                                               ObjtblOrderDetail.ProductQty, ObjtblOrder.pkOrderId,
                                                               IsApplyCompanyPricing, ObjtblCompany_Price.fkCompanyId, ObjtblCompany_Price.CompanyPrice, ObjtblOrderDetail.AdditionalFee);
                    iResult = 1;


                }
            }
            catch
            {
                //Added log for exception handling.
                throw;
            }
            return iResult;
        }
        ////public string SaveDetailsForDrugTestingByOrderId(tblOrderDetail ObjtblOrderDetail, tblCompany_Price ObjtblCompany_Price, tblOrder ObjtblOrder, bool IsApplyCompanyPricing)
        ////{
        ////    string err = "";
        ////   // int iResult = -1;
        ////    try
        ////    {
        ////        using (EmergeDALDataContext DX = new EmergeDALDataContext())
        ////        {
        ////            tblOrderDetail Objtbl = DX.tblOrderDetails.Where(d => d.pkOrderDetailId == ObjtblOrderDetail.pkOrderDetailId).FirstOrDefault();
        ////            if (Objtbl != null)
        ////            {
        ////                Objtbl.fkProductApplicationId = ObjtblOrderDetail.fkProductApplicationId;
        ////                Objtbl.ReportAmount = ObjtblOrderDetail.ReportAmount;
        ////                Objtbl.ProductQty = ObjtblOrderDetail.ProductQty;
        ////                Objtbl.ReportStatus = ObjtblOrderDetail.ReportStatus;
        ////                //DX.SubmitChanges();
        ////                //iResult = 1;
        ////            }
        ////            tblOrder ObjtblOrd = DX.tblOrders.Where(d => d.pkOrderId == ObjtblOrder.pkOrderId).FirstOrDefault();
        ////            if (ObjtblOrd != null)
        ////            {
        ////                ObjtblOrd.OrderStatus = ObjtblOrder.OrderStatus;                       
        ////                //DX.SubmitChanges();
        ////                //iResult = 1;
        ////            }
        ////            if (IsApplyCompanyPricing == true)
        ////            {
        ////                tblCompany_Price ObjtblComp = DX.tblCompany_Prices.Where(d => d.fkCompanyId == ObjtblCompany_Price.fkCompanyId && d.fkProductApplicationId == ObjtblCompany_Price.fkProductApplicationId).FirstOrDefault();
        ////                if (ObjtblComp != null)
        ////                {
        ////                    ObjtblComp.CompanyPrice = ObjtblCompany_Price.CompanyPrice;                          
        ////                    //iResult = 1;
        ////                }
        ////            } 
        ////            DX.SubmitChanges();
        ////            err = string.Empty;

        ////        }
        ////    }
        ////    catch (Exception ex) 
        ////    {
        ////        err = ex.ToString();
        ////    }
        ////    return err;
        ////}
        public List<proc_GetDrugTestingProductsByLocationIdResult> GetDrugTestingProductsByLocationId(int LocationId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return DX.GetDrugTestingProductsByLocationId(LocationId).ToList<proc_GetDrugTestingProductsByLocationIdResult>();
            }
        }
        public List<tblProductPerApplication> GetDrugTestingProducts()
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return DX.tblProductPerApplications.Where(d => d.fkReportCategoryId == 9).ToList<tblProductPerApplication>();
            }
        }
        public int CheckProductCode(string PCode, int PkProductId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                if (PkProductId != 0)
                {
                    success = DX.tblProducts.Where(d => d.ProductCode == PCode && d.IsDeleted == false && d.pkProductId != PkProductId).Count();
                }
                else
                {
                    success = DX.tblProducts.Where(d => d.ProductCode == PCode && d.IsDeleted == false).Count();
                }
            }
            return success;
        }

        public int ChangeOrder(int pkProductIdFst, int pkProductIdSec, byte SOFst, byte SOSec)//, bool IsUpDown
        {
            int success = 0;
            tblProduct Fst = new tblProduct();
            tblProduct Sec = new tblProduct();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                Fst = DX.tblProducts.Where(d => d.pkProductId == pkProductIdFst).FirstOrDefault();
                Sec = DX.tblProducts.Where(d => d.pkProductId == pkProductIdSec).FirstOrDefault();
                if (Fst != null && Sec != null)
                {
                    //if (IsUpDown)
                    //{
                    Fst.SortOrder = Convert.ToByte(SOSec);
                    Sec.SortOrder = Convert.ToByte(SOFst);
                    //}
                    DX.SubmitChanges();
                    success = 1;
                }
            }

            return success;
        }

        public int GetMaxSortOrderWithIncrement()
        {
            int max = 0;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                max = Convert.ToInt32(DX.tblProducts.Max(d => d.SortOrder));
                max = max + 1;
            }
            return max;
        }

        public byte AddSortOrder(int PkProductId)
        {
            byte success = 0;
            tblProduct ObjtblProduct = new tblProduct();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                ObjtblProduct = DX.tblProducts.Where(d => d.pkProductId == PkProductId).FirstOrDefault();
                if (ObjtblProduct != null)
                {
                    success = Convert.ToByte(GetMaxSortOrderWithIncrement());
                    ObjtblProduct.SortOrder = success;
                    DX.SubmitChanges();
                }
            }
            return success;
        }

        public Tuple<decimal?, decimal?> GetDrugtestingkit()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                var query = (from u in ObjDALDataContext.tblDrugtestingCategories select u).ToList();
                decimal? SalivaPrice = null;
                decimal? EZcupPrice = null;
                if (query != null)
                {
                    var Temp1 = query.Where(t => t.pkDrugtestingCategoryId == 1).FirstOrDefault();
                    var Temp2 = query.Where(t => t.pkDrugtestingCategoryId == 2).FirstOrDefault();
                    if (Temp1 != null) { if (Temp1.AveragePrice != null) { SalivaPrice = Temp1.AveragePrice; } }
                    if (Temp2 != null) { if (Temp2.AveragePrice != null) { EZcupPrice = Temp2.AveragePrice; } }
                }
                return new Tuple<decimal?, decimal?>(SalivaPrice, EZcupPrice);
            }
        }
        public int UpdateDrugPrice(decimal SalivaPrice, decimal EZcupPrice)
        {
            int success = 0;

            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                try
                {
                    var Obj = (from t1 in DX.tblDrugtestingCategories select t1).ToList();
                    if (Obj != null)
                    {
                        var Temp1 = Obj.Where(t => t.pkDrugtestingCategoryId == 1).FirstOrDefault();
                        var Temp2 = Obj.Where(t => t.pkDrugtestingCategoryId == 2).FirstOrDefault();
                        if (Temp1 != null) { Temp1.AveragePrice = SalivaPrice; };
                        if (Temp2 != null) { Temp2.AveragePrice = EZcupPrice; };
                        DX.SubmitChanges();
                        success = 1;
                    }
                }
                catch
                {
                    success = -1;
                    //Added log for exception handling.
                    throw;
                }
            }
            return success;

        }


    }
    public struct clsDrugTestingKit
    {
        public int pkProductApplicationId { get; set; }
        public int? pkProductId { get; set; }
        public string ProductDisplayName { get; set; }
        public decimal ProductPerApplicationPrice { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string ProductBulletPoint { get; set; }
        public int fkReportCategoryId { get; set; }
        public bool? IsEnabled { get; set; }
        public bool? IsDefault { get; set; }
        public bool? IsGlobal { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsNonMemberRpt { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ProductImage { get; set; }
        public byte? DrugTestCategoryId { get; set; }
        public string DrugTestCategoryName { get; set; }
        public string DrugTestQty { get; set; }
        public string DrugTestPanels { get; set; }
        public string ProductCode { get; set; }
        public byte? SortOrder { get; set; }
    }

}
