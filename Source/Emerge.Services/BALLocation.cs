﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;
using System.Data.Common;
using System.Data;


namespace Emerge.Services
{
    public class BALLocation
    {
        EmergeDALDataContext ObjDALDataContext;

        /// <summary>
        /// Function To add Location of company
        /// </summary>
        /// <param name="CompanyId"></param>
        /// <param name="ObjtblLocation"></param>
        /// <returns></returns>
        public int InsertNewLocation(int? CompanyId, tblLocation ObjtblLocation)
        {
            using (ObjDALDataContext = new EmergeDALDataContext())
            {
                return (int)ObjDALDataContext.AddLocations(CompanyId
                                                        , ObjtblLocation.LocationCode
                                                        , ObjtblLocation.LocationContact
                                                        , ObjtblLocation.LocationNote
                                                        , ObjtblLocation.Address1
                                                        , ObjtblLocation.Address2
                                                        , ObjtblLocation.City
                                                        , ObjtblLocation.fkStateID
                                                        , ObjtblLocation.ZipCode
                                                        , ObjtblLocation.PhoneNo1
                                                        , ObjtblLocation.Ext1
                                                        , ObjtblLocation.PhoneNo2
                                                        , ObjtblLocation.Ext2
                                                        , ObjtblLocation.Fax
                                                        , ObjtblLocation.Fax2
                                                        , ObjtblLocation.IsEnabled
                                                        , ObjtblLocation.IsHome
                                                        , ObjtblLocation.CreatedDate
                                                        , ObjtblLocation.CreatedById
                                                        , ObjtblLocation.IsCustomCity
                                                        , ObjtblLocation.LocationEmail
                                                        , ObjtblLocation.IsReportEmailActivated
                                                        , ObjtblLocation.IsSyncpod, ObjtblLocation.SecondaryPhone, ObjtblLocation.SecondaryFax).ElementAt(0).LocationId;
            }
        }

        /// <summary>
        /// Function To add Products of Location
        /// </summary>
        /// <param name="CompanyId"></param>
        /// <param name="ObjtblLocation"></param>
        /// <returns></returns>
        public int InsertNewLocationProduct(tblProductAccess ObjtblProductAccess)
        {
            using (ObjDALDataContext = new EmergeDALDataContext())
            {
                return (int)ObjDALDataContext.AddLocationProduct(
                    ObjtblProductAccess.fkLocationId,
                    ObjtblProductAccess.fkProductApplicationId,
                    ObjtblProductAccess.IsLiveRunner,
                    ObjtblProductAccess.ProductReviewStatus, ObjtblProductAccess.IsProductEnabled, ObjtblProductAccess.LocationProductPrice
                    ).ElementAt(0).pkProductAccessId;
            }
        }

        //public int InsertUpdateLocationProduct(List<tblProductAccess> listtblProductAccess)
        //{
        //    using (ObjDALDataContext = new EmergeDALDataContext())
        //    {
        //        if (listtblProductAccess.Count > 0)
        //        {

        //            foreach (var obj in listtblProductAccess)
        //            {

        //                    var result = (Guid)ObjDALDataContext.Add_LocationProduct(
        //                        obj.fkLocationId,
        //                        obj.fkProductApplicationId,
        //                        obj.IsLiveRunner,
        //                        obj.ProductReviewStatus
        //                        ).ElementAt(0).pkProductAccessId;

        //            }

        //        }

        //        return 1;
        //    }
        //}

        public int InsertUpdateLocationProduct(List<tblProductAccess> listtblProductAccess, List<tblProductAccess> deleteList)
        {
            using (ObjDALDataContext = new EmergeDALDataContext())
            {
                if (listtblProductAccess.Count > 0)
                {

                    foreach (var obj in listtblProductAccess)
                    {
                        var record = ObjDALDataContext.tblProductAccesses.Where(x => x.fkLocationId == obj.fkLocationId && x.fkProductApplicationId == obj.fkProductApplicationId).FirstOrDefault();
                        if (record != null)
                        {
                            ObjDALDataContext.tblProductAccesses.DeleteOnSubmit(record);
                            ObjDALDataContext.SubmitChanges();
                        }
                        var result = (int)ObjDALDataContext.AddLocationProduct(
                            obj.fkLocationId,
                            obj.fkProductApplicationId,
                            obj.IsLiveRunner,
                            obj.ProductReviewStatus, obj.IsProductEnabled, obj.LocationProductPrice
                            ).ElementAt(0).pkProductAccessId;

                    }

                }
                if (deleteList.Count > 0)
                {
                    foreach (var obj in deleteList)
                    {
                        var record = ObjDALDataContext.tblProductAccesses.Where(x => x.fkLocationId == obj.fkLocationId && x.fkProductApplicationId == obj.fkProductApplicationId).FirstOrDefault();
                        if (record != null)
                        {
                            ObjDALDataContext.tblProductAccesses.DeleteOnSubmit(record);
                            ObjDALDataContext.SubmitChanges();
                        }
                        var result =(int)ObjDALDataContext.AddLocationProduct(
                            obj.fkLocationId,
                            obj.fkProductApplicationId,
                            obj.IsLiveRunner,
                            obj.ProductReviewStatus, obj.IsProductEnabled, obj.LocationProductPrice
                            ).ElementAt(0).pkProductAccessId;
                    }
                }
                return 1;
            }
        }

        //INT197

        public int InsertUpdateAllLocationProduct(List<tblProductAccess> listtblProductAccess, List<tblProductAccess> deleteList, int fkcompanyId)
        {
            List<int> lstLocations = new List<int>();
            using (ObjDALDataContext = new EmergeDALDataContext())
            {
                lstLocations = ObjDALDataContext.tblLocations.Where(x => x.fkCompanyId == fkcompanyId).Select(x => x.pkLocationId).ToList();
                if (listtblProductAccess.Count > 0)
                {

                    foreach (var obj in listtblProductAccess)
                    {


                        foreach (int loc in lstLocations)
                        {
                            var record = ObjDALDataContext.tblProductAccesses.Where(x => x.fkLocationId == loc && x.fkProductApplicationId == obj.fkProductApplicationId).FirstOrDefault();
                            if (record != null)
                            {
                                ObjDALDataContext.tblProductAccesses.DeleteOnSubmit(record);
                                ObjDALDataContext.SubmitChanges();
                            }
                            var result = (int)ObjDALDataContext.AddLocationProduct(
                                loc,
                                obj.fkProductApplicationId,
                                obj.IsLiveRunner,
                                obj.ProductReviewStatus, obj.IsProductEnabled, obj.LocationProductPrice
                                ).ElementAt(0).pkProductAccessId;
                        }

                    }

                }
                if (deleteList.Count > 0)
                {
                    foreach (var obj in deleteList)
                    {
                        foreach (int loc in lstLocations)
                        {

                            var record = ObjDALDataContext.tblProductAccesses.Where(x => x.fkLocationId == loc && x.fkProductApplicationId == obj.fkProductApplicationId).FirstOrDefault();
                            if (record != null)
                            {
                                ObjDALDataContext.tblProductAccesses.DeleteOnSubmit(record);
                                ObjDALDataContext.SubmitChanges();
                            }
                            var result = (int)ObjDALDataContext.AddLocationProduct(
                                loc,
                                obj.fkProductApplicationId,
                                obj.IsLiveRunner,
                                obj.ProductReviewStatus, obj.IsProductEnabled, obj.LocationProductPrice
                                ).ElementAt(0).pkProductAccessId;
                        }
                    }
                }
                return 1;
            }
        }






        /// <summary>
        /// Function To Update the IsDeleted Status of Single Row By ID
        /// </summary>
        /// <param name="LocationId"></param>
        /// <returns></returns>
        public int DeleteLocationById(int LocationId)
        {
            using (ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.DeleteLocation(LocationId);
            }
        }

        /// <summary>
        /// Function to Get All the Locations For Admin
        /// </summary>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <param name="IsPaging"></param>
        /// <param name="SearchString"></param>
        /// <returns></returns>
        //public List<Proc_Get_AllLocationsForAdminResult> GetAllLocationsForAdmin(int PageIndex, int PageSize, bool IsPaging, string SearchString, int LocationId, Int16 StatusType, Guid FkCompanyId, string SortingColumn, string SortingDirection)
        //{
        //    using (ObjDALDataContext = new EmergeDALDataContext())
        //    {
        //        return ObjDALDataContext.GetAllLocationsForAdmin(PageIndex, PageSize, IsPaging, SearchString, LocationId, StatusType, FkCompanyId, SortingColumn, SortingDirection).ToList<Proc_Get_AllLocationsForAdminResult>();
        //    }
        //}

        /// <summary>
        /// Function To Update Location
        /// </summary>
        /// <returns></returns>
        public int UpdateLocation(tblLocation ObjLocation, int fkCompanyId)
        {
            using (ObjDALDataContext = new EmergeDALDataContext())
            {
                int Result = ObjDALDataContext.UpdateLocations(
                                                        ObjLocation.pkLocationId,
                                                        fkCompanyId,
                                                        ObjLocation.LocationContact,
                                                        ObjLocation.LocationNote,
                                                        ObjLocation.Address1,
                                                        ObjLocation.Address2,
                                                        ObjLocation.City,
                                                        ObjLocation.fkStateID,
                                                        ObjLocation.ZipCode,
                                                        ObjLocation.PhoneNo1,
                                                        ObjLocation.Ext1,
                                                        ObjLocation.PhoneNo2,
                                                        ObjLocation.Ext2,
                                                        ObjLocation.Fax,
                                                        ObjLocation.Fax2,
                                                        ObjLocation.IsEnabled,
                                                        ObjLocation.LastModifiedDate,
                                                        ObjLocation.LastModifiedById,
                                                        ObjLocation.IsCustomCity,
                                                        ObjLocation.LocationEmail,
                                                        ObjLocation.IsReportEmailActivated,
                                                        ObjLocation.IsSyncpod, ObjLocation.SecondaryPhone, ObjLocation.SecondaryFax);
                return Result;
            }
        }

        /// <summary>
        /// Function To Get Location Details Including Its Product Which can be accessed by the Location 
        /// </summary>
        /// <param name="ObjLocationId"></param>
        /// <returns></returns>
        public List<Proc_Get_LocationDetailByLocationIdResult> GetLocationDetailByLocationId(int ObjLocationId)
        {
            using (ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.Get_LocationDetailByLocationId(ObjLocationId).ToList<Proc_Get_LocationDetailByLocationIdResult>();
            }
        }


        /// <summary>
        /// Delete All Products of Location
        /// </summary>
        /// <param name="LocationId"></param>
        /// <returns></returns>
        public int DeleteProductsOfLocation(int LocationId)
        {
            using (ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.Proc_Delete_ProductsOfLocation(LocationId);
            }
        }


        /// <summary>
        /// Function to Get All Locations of Company 
        /// </summary>
        /// <param name="ApplicationId"></param>
        /// <returns></returns>
        public List<tblLocation> GetLocationsByCompanyId(int CompanyId)
        {
            using (ObjDALDataContext = new EmergeDALDataContext())
            {
                IQueryable<tblLocation> ObjData = null;
                if (CompanyId != 0)
                {
                    ObjData = ObjDALDataContext.tblLocations.Where(Data => Data.fkCompanyId == CompanyId && Data.IsDeleted == false).OrderBy(d => d.City);
                    return ObjData.ToList();
                }
                else
                {
                    ObjData = ObjDALDataContext.tblLocations.Where(Data => Data.IsDeleted == false).OrderBy(d => d.City);
                    return ObjData.ToList();
                }
            }
        }

        /// <summary>
        /// Function to Get All Locations of Company for branch Manager //INT343
        /// </summary>
        /// <param name="ApplicationId"></param>
        /// <returns></returns>
        public List<tblLocation> GetLocationsByCompanyIdForBranchManager(string emailId)
        {
            using (ObjDALDataContext = new EmergeDALDataContext())
            {
                IQueryable<tblLocation> ObjData = null;
                List<int> locMain = new List<int>();
                IEnumerable<int> loc1 = (from user in ObjDALDataContext.aspnet_Users
                                         join cmpUser in ObjDALDataContext.tblCompanyUsers
                                         on user.UserId equals cmpUser.fkUserId
                                         join loc in ObjDALDataContext.tblLocations
                                         on cmpUser.fkLocationId equals loc.pkLocationId
                                         where user.UserName == emailId
                                         select loc.pkLocationId).ToList();
                IEnumerable<int> loc2 = (from user in ObjDALDataContext.aspnet_Users
                                         join cmpUser in ObjDALDataContext.tblCompanyUsers
                                         on user.UserId equals cmpUser.fkUserId
                                         join uloc in ObjDALDataContext.tblUsersLocations
                                         on cmpUser.pkCompanyUserId equals uloc.fkCompanyUserId
                                         join loc in ObjDALDataContext.tblLocations
                                         on uloc.fklocationId equals loc.pkLocationId
                                         where user.UserName == emailId && uloc.IsHome == true
                                         select loc.pkLocationId).ToList();

                locMain.AddRange(loc1);
                locMain.AddRange(loc2);
                ObjData = ObjDALDataContext.tblLocations.Where(Data => locMain.Contains(Data.pkLocationId) && Data.IsDeleted == false).OrderBy(d => d.City);
                return ObjData.ToList();


            }
        }

        public List<tblUsersLocation> GetLocationsByPkCompanyUserId(int fkCompanyUserId)
        {
            using (ObjDALDataContext = new EmergeDALDataContext())
            {
                IQueryable<tblUsersLocation> ObjData = null;
                if (fkCompanyUserId != 0)
                {
                    ObjData = ObjDALDataContext.tblUsersLocations.Where(Data => Data.fkCompanyUserId == fkCompanyUserId);
                    return ObjData.ToList();
                }
                else
                {

                    return ObjData.ToList();
                }
            }
        }



        //public List<proc_GetLocationByIsHomeStatusAndIdResult> GetLocationsByCompanyIdForOthr(bool IsHome, Guid CompanyId)
        //{
        //    /*using (ObjDALDataContext = new EmergeDALDataContext())
        //    {
        //        IQueryable<proc_GetLocationByIsHomeStatusAndIdResult> ObjData = null;
        //        //IQueryable<tblLocation> ObjDataNull = null;
        //        if (CompanyId != Guid.Empty)
        //        {
        //            return ObjDALDataContext.GetLocationByIsHomeStatusAndId(CompanyId, IsHome).ToList();                    
        //        }
        //        else
        //        {
        //            ObjDataNull = ObjDALDataContext.tblLocations.Where(Data => Data.IsDeleted == false).OrderBy(d => d.City).ToList<tblLocation>();
        //            return ObjData.ToList();
        //        }
        //    }*/
        //}


        /// <summary>
        ///  To make Enable/disable users and Delete them
        /// </summary>
        /// <param name="pkLocationId"></param>
        /// <param name="ActionToDo"></param>
        /// <param name="ActionValue"></param>
        /// <returns></returns>
        public int ActionOnLocations(List<tblLocation> LstLocation, byte ActionToDo, bool ActionValue)
        {
            DbTransaction DbTrans = null;
            using (ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    ObjDALDataContext.Connection.Open();
                    DbTrans = ObjDALDataContext.Connection.BeginTransaction();
                    ObjDALDataContext.Transaction = DbTrans;
                    for (int i = 0; i < LstLocation.Count; i++)
                    {
                        ObjDALDataContext.TakeActionOnLocations(LstLocation.ElementAt(i).pkLocationId, ActionToDo, ActionValue);
                    }
                    DbTrans.Commit(); // Commit Transaction
                    return (int)1;
                }
                catch (Exception ex)
                {
                    if (DbTrans != null)
                    {
                        DbTrans.Rollback(); // Rollback Transaction
                    }
                    throw ex;
                }
                finally
                {
                    ObjDALDataContext.Transaction = null;
                    if (ObjDALDataContext.Connection.State == ConnectionState.Open)
                    {
                        ObjDALDataContext.Connection.Close(); // Close Connection
                    }
                }
            }
        }

        public List<City> GetCityStatewise(int StateId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                List<City> ObjCity = new List<City>();
                ObjCity = (from db in DX.tblCities
                           where db.fkStateId == StateId
                           select new City
                           {
                               CityId = db.pkCityId,
                               CityName = db.City,
                               CityValue = db.City
                           }).OrderBy(dt => dt.CityName).ToList<City>().Distinct(new MyItemComparer()).ToList();

                return ObjCity;
            }
        }

        /// <summary>
        /// Get product access detail.
        /// </summary>
        /// <param name="productCode"></param>
        /// <param name="fkCompanyId"></param>
        /// <param name="fkLocation"></param>
        /// <returns></returns>
        public clsProductAccessDetail GetProductAccessDetail(string productCode, int? fkCompanyId, int? fkLocation)
        {
            clsProductAccessDetail obj = new clsProductAccessDetail();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                obj = (from tblpa in DX.tblProductAccesses
                       join tblpp in DX.tblProductPerApplications on tblpa.fkProductApplicationId equals tblpp.pkProductApplicationId
                       where tblpp.ProductCode == productCode
                       join tblcp in DX.tblCompany_Prices on tblpp.pkProductApplicationId equals tblcp.fkProductApplicationId
                       where (tblpa.fkLocationId == fkLocation) && (tblcp.fkCompanyId == fkCompanyId)
                       select new clsProductAccessDetail
                       {
                           _IsLiveRunner = tblpa.IsLiveRunner,
                           _ProductReviewStatus = tblpa.ProductReviewStatus,
                           _IsProductEnabled = tblpa.IsProductEnabled,
                           _LocationProductPrice = tblpa.LocationProductPrice,
                           _pkProductAccessId = tblpa.pkProductAccessId,
                           _fkLocationId = fkLocation,
                           _fkProductApplicationId = tblpa.fkProductApplicationId,
                           _CompanyPrice = tblcp.CompanyPrice,
                           _EnableReviewPerCompany = tblcp.EnableReviewPerCompany,
                           _QuantityByLead = tblcp.QuantityByLead,
                           _IsDefaultPrice = tblcp.IsDefaultPrice,
                           _MonthlyVolume = tblcp.MonthlyVolume,
                           _CurrentPrice = tblcp.CurrentPrice,
                           _MonthlySaving = tblcp.MonthlySaving,
                           _pkCompanyPriceId = tblcp.pkCompanyPriceId,
                           _fkCompanyId = fkCompanyId,
                           _LinkedReportId = tblcp.LinkedReportId
                       }).FirstOrDefault();

            }

            return obj;
        }


    }

    //INT197
    public class clsProductAccessDetail
    {
        public bool _IsLiveRunner;
        public byte _ProductReviewStatus;
        public bool _IsProductEnabled;
        public decimal _LocationProductPrice;
        public int _pkProductAccessId;
        public System.Nullable<int> _fkLocationId;
        public System.Nullable<int> _fkProductApplicationId;
        public System.Nullable<decimal> _CompanyPrice;
        public bool _EnableReviewPerCompany;
        public string _QuantityByLead;
        public bool _IsDefaultPrice;
        public System.Nullable<decimal> _MonthlyVolume;
        public System.Nullable<decimal> _CurrentPrice;
        public System.Nullable<decimal> _MonthlySaving;
        public int _pkCompanyPriceId;
        public System.Nullable<int> _fkCompanyId;
        public System.Nullable<int> _LinkedReportId;
    }

    public class City
    {
        public string CityName { get; set; }
        public string CityValue { get; set; }

        public int CityId { get; set; }
    }

    class MyItemComparer : IEqualityComparer<City>
    {
        public new bool Equals(City item1, City item2)
        {
            return item1.CityValue.Equals(item2.CityValue);
        }

        public int GetHashCode(City item)
        {
            return item.CityValue.GetHashCode();
        }
    }
}
