﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;
using Emerge.Services.Helper;
using MomentumInfotech.EmailProvider;

namespace Emerge.Services
{
    public class BALViewReport
    {
        /// <summary>
        /// To saving the email details, for content locker record.
        /// </summary>
        /// <param name="OrderId"></param>
        /// <param name="ApplicantEmail"></param>
        /// <returns></returns>
        public bool AddEmailDetailINDBForViewBlockRecords(int OrderId, string ApplicantEmail)
        {

            bool IsCheckViewReport = false;

            using (EmergeDALDataContext _dbContext = new EmergeDALDataContext())
            {
                var obj = _dbContext.tblViewReportEmailDetails.Where(tv => tv.fkOrderId == OrderId).FirstOrDefault();
                if (obj == null)
                {
                    if (!string.IsNullOrEmpty(ApplicantEmail))
                    {
                        tblViewReportEmailDetail objEmailDetail = new tblViewReportEmailDetail();
                        objEmailDetail.fkOrderId = OrderId;
                        objEmailDetail.AplicantEmail = ApplicantEmail;
                        objEmailDetail.IsCheckViewReport = true;
                        _dbContext.tblViewReportEmailDetails.InsertOnSubmit(objEmailDetail);
                        _dbContext.SubmitChanges();
                        IsCheckViewReport = true;
                        //613 Mail Notification.
                        //bool mailSucceeded = 
                            SendMailNotification(ApplicantEmail);
                    }
                }
                else
                {
                    IsCheckViewReport = obj.IsCheckViewReport;
                }

            }
            return IsCheckViewReport;
        }

        /// <summary>
        /// To send the 613 notification email.
        /// </summary>
        /// <param name="emailId"></param>
        /// <returns></returns>
        public bool SendMailNotification(string emailId)
        {
            string emailText = string.Empty;            
            try
            { 
                emailText = "Your 613 notice is ready.";
                return Utilitys.SendMail(emailId.Trim(), string.Empty, string.Empty, BALGeneral.GetGeneralSettings().SupportEmailId, emailText, "613 Notification");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
               
            }

        }

    }
}
