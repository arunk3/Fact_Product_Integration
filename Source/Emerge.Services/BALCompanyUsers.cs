﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;
using System.Data.Common;
using System.Data;
using Emerge.Data;

namespace Emerge.Services
{
    public class BALCompanyUsers
    {

        public List<Proc_Get_UserInfoByMemberShipUserIdResult> GetUserInfoByMemberShipUserId(Guid UserId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetUserInfoByMemberShipUserId(UserId).ToList();
            }
        }

        public Guid GetfkUserId(int pkCompanyUserId)
        {
//            Guid FkUserId = Guid.Empty;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.tblCompanyUsers.Where(d => d.pkCompanyUserId == pkCompanyUserId).FirstOrDefault().fkUserId;
            }

        }
        /// <summary>
        /// This method is used to get users according location id
        /// </summary>
        /// <param name="LocationId"></param>
        /// <returns></returns>
        public List<Proc_Get_CompanyUsersByLocationIdResult> GetCompanyUsersByLocationId(int CompanyId, int LocationId, Guid ApplicationId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetCompanyUsersByLocationId(CompanyId, LocationId, ApplicationId).ToList<Proc_Get_CompanyUsersByLocationIdResult>();
            }
        }

        public int UpdateWCVAcknowlwdgeInDB( int pkCompanyUserId)
        {
            using(EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                ObjDALDataContext.UpdateWCVAcknowledge(pkCompanyUserId);
                return 1;
            }
        }
        public List<Proc_Get_AllEmergeUsersForAdminResult> GetAllEmergeUsersForAdmin(int PageIndex, int PageSize, bool IsPaging, Int16 StatusType, int CompanyId, int FkLocationId, string SortingColumn, string SortingDirection, string FilterColumn, byte FilterAction, string FilterValue, out int TotalRec)
        {
            using (EmergeDALDataContext dbObj = new EmergeDALDataContext())
            {
                int? count = 0;
                List<Proc_Get_AllEmergeUsersForAdminResult> List = dbObj.GetAllEmergeUsersForAdmin(PageIndex, PageSize, IsPaging, StatusType, CompanyId, FkLocationId, SortingColumn, SortingDirection, FilterColumn, FilterAction, FilterValue, ref count).ToList();
                TotalRec = (int)count;
                return List;
            }
        }
        public int InsertDashBoardStatus(Guid PkUserId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                var list = ObjDALDataContext.tblDashboardLists.Where(rec => rec.fkUserID == PkUserId);
                //if(list
                if (list.Count() == 0)
                {
                    ObjDALDataContext.InsertDashBoardColpaseStatus(PkUserId);
                }
                return 1;
            }
        }

        public int UpdateDashBoardStaus(Guid PkcompanyUserID , string DivName, bool ColpaseStatus)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                   
                var list = ObjDALDataContext.tblDashboardLists.Where(rec => rec.fkUserID == PkcompanyUserID && rec.DivName == DivName);

                if (list.Count() == 0)
                if(true)
                {
                    // Insert Stautus for new tab
                    ObjDALDataContext.InsertDashBoardColpaseStatusForNewtab(PkcompanyUserID, DivName, ColpaseStatus);
                }
                else
                {
                   // Update tab Staus # 19
                    ObjDALDataContext.UpdateDashBoardColpaseStatus(PkcompanyUserID, DivName, ColpaseStatus);
                }
                
                return 1;
            }
        }
        public  proc_Get_CompanyUser_ByCompanyUserIdResult SelectCompanyUserByCompanyUserId(int PkCompanyUserId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetCompanyUserByCompanyUserId(PkCompanyUserId).FirstOrDefault();
                
            }
        }

        public List<tblCompanyUser> GetUserInformationByMembershipUserId(Guid UserId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                IQueryable<tblCompanyUser> ObjData = ObjDALDataContext.tblCompanyUsers.Where(db => db.fkUserId == UserId);
                return ObjData.ToList();
            }
        }

        public List<proc_GetUserCompanyInfoResult> GetUserCompanyInfo(Guid UserId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {                 
                return ObjDALDataContext.GetUserCompanyInfo(UserId).ToList();
            }
        }

        /// <summary>
        /// Function To Fetch All the Users For Corporate Manager 
        /// </summary>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <param name="IsPaging"></param>
        /// <param name="SearchString"></param>
        /// <param name="PkCompanyId"></param>
        /// <returns></returns>
        public List<Proc_Get_UsersUnderCompanyResult> GetUsersUnderCompany(int PageIndex, int PageSize, bool IsPaging, string SearchString, int PkCompanyId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetUsersUnderCompany(PageIndex, PageSize, IsPaging, SearchString, PkCompanyId).ToList<Proc_Get_UsersUnderCompanyResult>();
            }
        }
        /// <summary>
        /// Function To Fetch All the Users For Branch Manager 
        /// </summary>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <param name="IsPaging"></param>
        /// <param name="SearchString"></param>
        /// <param name="PkLocationId"></param>
        /// <returns></returns>
        /// 
        
       
        public List<Proc_Get_UsersUnderLocationResult> GetUsersUnderLocation(int PageIndex, int PageSize, bool IsPaging, string SearchString, int PkLocationId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetUsersUnderLocation(PageIndex, PageSize, IsPaging, SearchString, PkLocationId).ToList<Proc_Get_UsersUnderLocationResult>();
            }
        }

        /// <summary>
        /// Function To Get Single Record For Company User Id According to the PkCompanyUserId
        /// </summary>
        /// <param name="PkCompanyUserId"></param>
        /// <returns></returns>
        public tblCompanyUser GetCompanyUserByCompanyUserId(int PkCompanyUserId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.tblCompanyUsers.Single(data => data.pkCompanyUserId == PkCompanyUserId);
            }
        }


        public List<tblDashboardList> GetDashBoardStatusByUserId(Guid PkCompanyUserId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                List<tblDashboardList> list = new List<tblDashboardList>();
                list = ObjDALDataContext.tblDashboardLists.Where(rec => rec.fkUserID == PkCompanyUserId).ToList();
                return list;

            }
        }
        public int UpdateMyProfileinOffice(tblCompanyUser ObjCompanyUser, Guid? RoleId)
        {
            EmergeDALDataContext ObjDALDataContext = null;
            int ReturnValue = -1;
            bool? IsSignedFcra = false;
            DbTransaction DbTrans = null;
            using (ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    ObjDALDataContext.Connection.Open(); /* Connection Open */
                    DbTrans = ObjDALDataContext.Connection.BeginTransaction(); /* Assign Transation */
                    ObjDALDataContext.Transaction = DbTrans;
                    ObjCompanyUser.Initials = !string.IsNullOrEmpty(ObjCompanyUser.Initials) ? ObjCompanyUser.Initials : string.Empty;
                    if (ObjCompanyUser.IsSignedFcra == null)
                        IsSignedFcra = ObjDALDataContext.tblCompanyUsers.Where(db => db.pkCompanyUserId == ObjCompanyUser.pkCompanyUserId).FirstOrDefault().IsSignedFcra;
                    else
                        IsSignedFcra = ObjCompanyUser.IsSignedFcra;
                    int Result = ObjDALDataContext.UpdateCompanyUsers(ObjCompanyUser.pkCompanyUserId,
                                                        ObjCompanyUser.fkLocationId,
                                                        ObjCompanyUser.UserNote,
                                                        ObjCompanyUser.FirstName,
                                                        ObjCompanyUser.LastName,
                                                        ObjCompanyUser.Initials,
                                                        ObjCompanyUser.UserAddressLine1,
                                                        ObjCompanyUser.UserAddressLine2,
                                                        ObjCompanyUser.PhoneNo,
                                                        ObjCompanyUser.ZipCode,
                                                        ObjCompanyUser.UserEmailOne,
                                                        ObjCompanyUser.UserEmailTwo,
                                                        ObjCompanyUser.fkStateId,
                                                        ObjCompanyUser.fkCountyId,
                                                        ObjCompanyUser.IsEnabled,
                                                        IsSignedFcra,
                                                        ObjCompanyUser.LastModifiedDate,
                                                        ObjCompanyUser.LastModifiedById,
                                                        ObjCompanyUser.UserImage, ObjCompanyUser.LinkedInProfileUrl, ObjCompanyUser.IsLiveRunnerEnabled, ObjCompanyUser.AutoEmergeReview);
                    if (Result < 0)
                    {
                        if (DbTrans != null)
                        {
                            DbTrans.Rollback(); /* Rollback Transaction  */
                        }
                        ReturnValue = Result;
                    }
                    else
                    {
                        ReturnValue = Result;
                        DbTrans.Commit();/* Commit Transaction */
                        //int val = ObjDALDataContext.UpdateUserRole(ObjCompanyUser.fkUserId, RoleId);
                        //if (val < 0)
                        //{
                        //    if (DbTrans != null)
                        //    {
                        //        DbTrans.Rollback(); /* Rollback Transaction  */
                        //    }
                        //    ReturnValue = val;
                        //}
                        //else
                        //{
                        //    ReturnValue = val;
                        //    DbTrans.Commit();/* Commit Transaction */
                        //}
                    }
                }
                catch
                {
                    #region Transaction Rollback

                    if (DbTrans != null)
                    {
                        DbTrans.Rollback(); /* Rollback Transaction */
                        //return -1;
                    }

                    #endregion
                }
                finally
                {
                    #region Connection and Transaction Close

                    ObjDALDataContext.Transaction = null;
                    if (ObjDALDataContext.Connection.State == ConnectionState.Open)
                    {
                        ObjDALDataContext.Connection.Close(); /* Close Connection */
                    }

                    #endregion
                }
            }
            return ReturnValue;
        }

        /// <summary>
        /// Function To Update Company User  
        /// </summary>
        /// <returns></returns>
        public int UpdateCompanyUsers(tblCompanyUser ObjCompanyUser, Guid ?RoleId)
        {
            EmergeDALDataContext ObjDALDataContext = null;
            int ReturnValue = -1;
            bool? IsSignedFcra = false;
            DbTransaction DbTrans = null;
            using (ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    ObjDALDataContext.Connection.Open(); /* Connection Open */
                    DbTrans = ObjDALDataContext.Connection.BeginTransaction(); /* Assign Transation */
                    ObjDALDataContext.Transaction = DbTrans;
                    ObjCompanyUser.Initials = !string.IsNullOrEmpty(ObjCompanyUser.Initials) ? ObjCompanyUser.Initials : string.Empty;
                    if (ObjCompanyUser.IsSignedFcra == null)
                        IsSignedFcra = ObjDALDataContext.tblCompanyUsers.Where(db => db.pkCompanyUserId == ObjCompanyUser.pkCompanyUserId).FirstOrDefault().IsSignedFcra;
                    else
                        IsSignedFcra = ObjCompanyUser.IsSignedFcra;
                    int Result = ObjDALDataContext.UpdateCompanyUsers(ObjCompanyUser.pkCompanyUserId,
                                                        ObjCompanyUser.fkLocationId,
                                                        ObjCompanyUser.UserNote,
                                                        ObjCompanyUser.FirstName,
                                                        ObjCompanyUser.LastName,
                                                        ObjCompanyUser.Initials,
                                                        ObjCompanyUser.UserAddressLine1,
                                                        ObjCompanyUser.UserAddressLine2,
                                                        ObjCompanyUser.PhoneNo,
                                                        ObjCompanyUser.ZipCode,
                                                        ObjCompanyUser.UserEmailOne,
                                                        ObjCompanyUser.UserEmailTwo,
                                                        ObjCompanyUser.fkStateId,
                                                        ObjCompanyUser.fkCountyId,
                                                        ObjCompanyUser.IsEnabled,
                                                        IsSignedFcra,
                                                        ObjCompanyUser.LastModifiedDate,
                                                        ObjCompanyUser.LastModifiedById,
                                                        ObjCompanyUser.UserImage, ObjCompanyUser.LinkedInProfileUrl, ObjCompanyUser.IsLiveRunnerEnabled, ObjCompanyUser.AutoEmergeReview);
                    if (Result < 0)
                    {
                        if (DbTrans != null)
                        {
                            DbTrans.Rollback(); /* Rollback Transaction  */
                        }
                        ReturnValue = Result;
                    }
                    else
                    {
                        int val = ObjDALDataContext.UpdateUserRole(ObjCompanyUser.fkUserId, RoleId);
                        if (val < 0)
                        {
                            if (DbTrans != null)
                            {
                                DbTrans.Rollback(); /* Rollback Transaction  */
                            }
                            ReturnValue = val;
                        }
                        else
                        {
                            ReturnValue = val;
                            DbTrans.Commit();/* Commit Transaction */
                        }
                    }
                }
                catch
                {
                    #region Transaction Rollback

                    if (DbTrans != null)
                    {
                        DbTrans.Rollback(); /* Rollback Transaction */
                        //return -1;
                    }

                    #endregion
                }
                finally
                {
                    #region Connection and Transaction Close

                    ObjDALDataContext.Transaction = null;
                    if (ObjDALDataContext.Connection.State == ConnectionState.Open)
                    {
                        ObjDALDataContext.Connection.Close(); /* Close Connection */
                    }

                    #endregion
                }
            }
            return ReturnValue;
        }

        public int DeleteOtherLocation(int fkCompanyUserId)
        {
            using (EmergeDALDataContext ObjDalDataContext = new EmergeDALDataContext())
            {
                
                try
                {
                    ObjDalDataContext._Delete_OtherLocations(fkCompanyUserId);
                }
                catch
                {
                    return 0;
                }
                return 2;
            }
        }

        public int UpdateCompanyUsers(tblCompanyUser ObjCompanyUser, Guid RoleId, string OldRole)
        {
            int ReturnValue = -1;
            bool? IsSignedFcra = false;
            DbTransaction DbTrans = null;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    ObjDALDataContext.Connection.Open(); /* Connection Open */
                    DbTrans = ObjDALDataContext.Connection.BeginTransaction(); /* Assign Transation */
                    ObjDALDataContext.Transaction = DbTrans;
                    ObjCompanyUser.Initials = !string.IsNullOrEmpty(ObjCompanyUser.Initials) ? ObjCompanyUser.Initials : string.Empty;
                    if (ObjCompanyUser.IsSignedFcra == null)
                        IsSignedFcra = ObjDALDataContext.tblCompanyUsers.Where(db => db.pkCompanyUserId == ObjCompanyUser.pkCompanyUserId).FirstOrDefault().IsSignedFcra;
                    else
                        IsSignedFcra = ObjCompanyUser.IsSignedFcra;
                    int Result = ObjDALDataContext.UpdateCompanyUsers(ObjCompanyUser.pkCompanyUserId,
                                                        ObjCompanyUser.fkLocationId,
                                                        ObjCompanyUser.UserNote,
                                                        ObjCompanyUser.FirstName,
                                                        ObjCompanyUser.LastName,
                                                        ObjCompanyUser.Initials,
                                                        ObjCompanyUser.UserAddressLine1,
                                                        ObjCompanyUser.UserAddressLine2,
                                                        ObjCompanyUser.PhoneNo,
                                                        ObjCompanyUser.ZipCode,
                                                        ObjCompanyUser.UserEmailOne,
                                                        ObjCompanyUser.UserEmailTwo,
                                                        ObjCompanyUser.fkStateId,
                                                        ObjCompanyUser.fkCountyId,
                                                        ObjCompanyUser.IsEnabled,
                                                        IsSignedFcra,
                                                        ObjCompanyUser.LastModifiedDate,
                                                        ObjCompanyUser.LastModifiedById,
                                                        ObjCompanyUser.UserImage,ObjCompanyUser.LinkedInProfileUrl,ObjCompanyUser.IsLiveRunnerEnabled, ObjCompanyUser.AutoEmergeReview);
                    if (Result < 0)
                    {
                        if (DbTrans != null)
                        {
                            DbTrans.Rollback(); /* Rollback Transaction  */
                        }
                        ReturnValue = Result;
                    }
                    else
                    {
                        if (OldRole != "")
                        {
                            ReturnValue = 1;
                        }
                        else
                        {
                            int val = ObjDALDataContext.UpdateUserRole(ObjCompanyUser.fkUserId, RoleId);
                            if (val < 0)
                            {
                                if (DbTrans != null)
                                {
                                    DbTrans.Rollback(); /* Rollback Transaction  */
                                }

                                ReturnValue = val;
                            }
                           
                        }
                    } DbTrans.Commit();
                }
                catch
                {
                    #region Transaction Rollback

                    if (DbTrans != null)
                    {
                        DbTrans.Rollback(); /* Rollback Transaction */
                        //return -1;
                    }

                    #endregion
                }
                finally
                {
                    #region Connection and Transaction Close

                    ObjDALDataContext.Transaction = null;
                    if (ObjDALDataContext.Connection.State == ConnectionState.Open)
                    {
                        ObjDALDataContext.Connection.Close(); /* Close Connection */
                    }

                    #endregion
                }
            }
            return ReturnValue;
        }

        /// <summary>
        /// Function To Update My Account Information
        /// </summary>
        /// <returns></returns>
        public bool UpdateMyAccount(tblCompanyUser ObjCompanyUser)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                int Result = ObjDALDataContext.UpdateMyAccount(ObjCompanyUser.pkCompanyUserId,
                                                        ObjCompanyUser.FirstName,
                                                        ObjCompanyUser.LastName,
                                                        ObjCompanyUser.UserAddressLine1,
                                                        ObjCompanyUser.UserAddressLine2,
                                                        ObjCompanyUser.PhoneNo,
                                                        ObjCompanyUser.ZipCode,
                                                        ObjCompanyUser.UserEmailOne,
                                                        ObjCompanyUser.UserEmailTwo,
                                                        ObjCompanyUser.fkStateId,
                                                        ObjCompanyUser.fkCountyId,
                                                        ObjCompanyUser.UserImage,
                                                        ObjCompanyUser.LastModifiedDate,
                                                        ObjCompanyUser.LastModifiedById);
                if (Result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Function To Add New Company User
        /// </summary>
        /// <param name="ObjtblCompanyUser"></param>
        /// <returns></returns>
        public int InsertCompanyUser(tblCompanyUser ObjtblCompanyUser)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return (int)ObjDALDataContext.AddCompanyUsers(ObjtblCompanyUser.fkUserId,
                                                    ObjtblCompanyUser.fkLocationId,
                                                    ObjtblCompanyUser.UserCode,
                                                    ObjtblCompanyUser.UserNote,
                                                    ObjtblCompanyUser.FirstName,
                                                    ObjtblCompanyUser.LastName,
                                                    ObjtblCompanyUser.Initials,
                                                    ObjtblCompanyUser.UserAddressLine1,
                                                    ObjtblCompanyUser.UserAddressLine2,
                                                    ObjtblCompanyUser.PhoneNo,
                                                    ObjtblCompanyUser.ZipCode,
                                                    ObjtblCompanyUser.UserEmailOne,
                                                    ObjtblCompanyUser.UserEmailTwo,
                                                    ObjtblCompanyUser.fkStateId,
                                                    ObjtblCompanyUser.fkCountyId,
                                                    ObjtblCompanyUser.IsEnabled,
                                                    ObjtblCompanyUser.IsSignedFcra,
                                                    ObjtblCompanyUser.CreatedDate,
                                                    ObjtblCompanyUser.CreatedById, ObjtblCompanyUser.UserImage,"", ObjtblCompanyUser.IsLiveRunnerEnabled).ElementAt(0).CompanyUserId;

               
            }
        }

        public int InsertOtherLocationOfCompany(int PkCompanyUserId, int fklocationId, bool IsHome)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                ObjDALDataContext.InsertOtherCompanyLocation(PkCompanyUserId, fklocationId, IsHome);
                return 1;

            }
        }

        public int UpdateOtherLocationOfCompany(int PkCompanyUserId, int fklocationId, bool IsHome)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                ObjDALDataContext.UpdatetblOtherLocations(PkCompanyUserId, fklocationId, IsHome);
                return 1;

            }
        }


        public int GetPkCompanyUserId(Guid fkUserId, int fkLocationId )
        {
            //int PkCompanyUserId =0;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                tblCompanyUser ObjtblCompanyUser = ObjDALDataContext.tblCompanyUsers.Where(p => p.fkUserId == fkUserId && p.fkLocationId == fkLocationId).FirstOrDefault();
              return ObjtblCompanyUser.pkCompanyUserId;

            }
        }
        public void UpdateMIfield(int pkCompanyUserId, bool Status)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                tblCompanyUser ObjtblCompanyUser = ObjDALDataContext.tblCompanyUsers.Single(data => data.pkCompanyUserId == pkCompanyUserId);
                ObjtblCompanyUser.IsMIEnable = Status;
                ObjDALDataContext.SubmitChanges();
            }
        }



        public int ActionOnUsers(List<tblCompanyUser> listCompanyUsers, byte ActionToDo, bool ActionValue)
        {

            DbTransaction DbTrans = null;
            using (EmergeDALDataContext ObjDALDataContext= new EmergeDALDataContext())
            {
                try
                {
                    ObjDALDataContext.Connection.Open();
                    DbTrans = ObjDALDataContext.Connection.BeginTransaction();
                    ObjDALDataContext.Transaction = DbTrans;
                    for (int i = 0; i < listCompanyUsers.Count; i++)
                    {
                        ObjDALDataContext.proc_TakeActionOnUsers(listCompanyUsers.ElementAt(i).fkUserId, ActionToDo, ActionValue);
                    }
                    DbTrans.Commit(); // Commit Transaction
                    return (int)1;
                }
                catch (Exception ex)
                {
                    if (DbTrans != null)
                    {
                        DbTrans.Rollback(); // Rollback Transaction
                    }
                    throw ex;
                }
                finally
                {
                    ObjDALDataContext.Transaction = null;
                    if (ObjDALDataContext.Connection.State == ConnectionState.Open)
                    {
                        ObjDALDataContext.Connection.Close(); // Close Connection
                    }
                }
            }
        }

        /// <summary>
        /// Update User's Deleted Status To Deleted .
        /// </summary>
        /// <param name="CompanyUserId"></param>
        /// <returns></returns>
        public bool DeleteEmergeUserById(int CompanyUserId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                if (DX.DeleteCompanyUser(CompanyUserId) == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public void UpdateSignedFcraStatus(Guid FkUserId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                tblCompanyUser ObjtblCompanyUser = ObjDALDataContext.tblCompanyUsers.Single(data => data.fkUserId == FkUserId);
                ObjtblCompanyUser.IsSignedFcra = true;
                ObjDALDataContext.SubmitChanges();
            }
        }

        public bool StateCheckStatus(int pkStateId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                //tblState ObjtblState = new tblState();
                bool list = (from t in ObjDALDataContext.tblStates
                            where t.pkStateId == pkStateId
                            select t.IsEnabled).FirstOrDefault();

                  return list;
            }
        }

        public bool StateCheckStatusLiveRunner(int pkStateId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
               // tblState ObjtblState = new tblState();
                bool list = (from t in ObjDALDataContext.tblStates
                             where t.pkStateId == pkStateId
                             select t.IsStateLiveRunner).FirstOrDefault();

                return list;
            }
        }


        /// <summary>
        /// To check user company by user id
        /// </summary>
        public bool CheckCompanyByUserId(int userId, int CompanyId)
        {
            bool flagCheck = false; 
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                var userCompany = (from cmp in ObjDALDataContext.tblCompanies
                                   join loc in ObjDALDataContext.tblLocations
                                   on cmp.pkCompanyId equals loc.fkCompanyId
                                   join cmpUser in ObjDALDataContext.tblCompanyUsers
                                   on loc.pkLocationId equals cmpUser.fkLocationId
                                   where cmp.pkCompanyId == CompanyId && cmpUser.pkCompanyUserId == userId
                                   select cmpUser).FirstOrDefault();
                if (userCompany != null)
                {
                    flagCheck = true;
                }
            }
            return flagCheck;
        }



        //public List<MyFormColl> GetMyFormCollection()
        //{
        //    using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
        //    {
        //        List<MyFormColl> ObjData = new List<MyFormColl>();
        //        ObjData = (from u in ObjDALDataContext.tblmyforms
        //                   select new MyFormColl
        //                   {
        //                       pkformId = u.pkformId,
        //                       Title = u.Title,
        //                       Description = u.Description,
        //                       Path = u.Path,
        //                       ModifiedLoggedId = u.ModifiedLoggedId != null ? (Guid)u.ModifiedLoggedId : Guid.Empty,
        //                       ModifiedDate = u.ModifiedDate,
        //                   }).ToList();
        //        return ObjData.ToList();
        //    }
        
        
        //}


        //public int UpdateForms(tblmyform Objtblmyform)
        //{
        //    int Result = 0;
        //    tblmyform ObjDoc = new tblmyform();
        //    using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
        //    {
        //        try
        //        {

        //            ObjDoc = ObjDALDataContext.tblmyforms.Where(d => d.pkformId == Objtblmyform.pkformId).FirstOrDefault();
        //                if (ObjDoc != null)
        //                {
        //                    ObjDoc.Title = Objtblmyform.Title;
        //                    ObjDoc.Description = Objtblmyform.Description;
        //                    ObjDoc.Path = Objtblmyform.Path;
        //                    ObjDoc.ModifiedDate = Objtblmyform.ModifiedDate;
        //                    ObjDoc.ModifiedLoggedId = Objtblmyform.ModifiedLoggedId;
                            
        //                    ObjDALDataContext.SubmitChanges();
        //                    Result = 1;
        //                }
                   

        //        }
        //        catch (Exception)
        //        {

        //            Result = 0;
        //        }
        //    }
        //    return Result;
        //}






    }

    //public class MyFormColl
    //{
    //    public int pkformId { get; set; }
    //    public string Title { get; set; }
    //    public string Description { get; set; }
    //    public string Path { get; set; }
    //    public Guid ModifiedLoggedId { get; set; }
    //    public DateTime? ModifiedDate { get; set; }



    //} 

   

}
