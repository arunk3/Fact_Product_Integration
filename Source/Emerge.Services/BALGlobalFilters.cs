﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;
using Emerge;

namespace Emerge.Services
{
    public class BALGlobalFilters
    {

        public int InsertGlobleFilter(tblGlobleFilter tblGlobleFilter)
        {
            int Result = -1;
            string ReportType = "NCR1";
            using (EmergeDALDataContext DbContext = new EmergeDALDataContext())
            {
                tblGlobleFilter objTtblGlobleFilter = new tblGlobleFilter();
                objTtblGlobleFilter.Source = tblGlobleFilter.Source;
                objTtblGlobleFilter.DocketNumber = tblGlobleFilter.DocketNumber;
                objTtblGlobleFilter.DeliveryAddress = tblGlobleFilter.DeliveryAddress;
                objTtblGlobleFilter.IsEnabled = true;
                objTtblGlobleFilter.ReportsType = ReportType;
                DbContext.tblGlobleFilters.InsertOnSubmit(objTtblGlobleFilter);
                DbContext.SubmitChanges();
                Result = 1;
            }

            return Result;
        }

        public List<tblGlobleFilter> GetGlobleFilters()
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {

                return DX.tblGlobleFilters.ToList();
            }
        }

        public tblGlobleFilter GetGlobleFilters(int GlobleFilterId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                tblGlobleFilter ObjData = new tblGlobleFilter();
                ObjData = DX.tblGlobleFilters.Where(x => x.PkGlobleFilterId == GlobleFilterId).FirstOrDefault();
                return ObjData;
            }
        }

        public List<GlobleFilter> GetGlobleFiltersForRemoveRowCourtHouseData()
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                List<GlobleFilter> ObjData = new List<GlobleFilter>();
                ObjData = DX.tblGlobleFilters.Select(x => new GlobleFilter
                {
                    GlobleFilterId = x.PkGlobleFilterId,
                    Source=x.Source,
                    DocketNumber=x.DocketNumber!=null ? x.DocketNumber : null,
                    DeliveryAddress=x.DeliveryAddress !=null ? x.DeliveryAddress : null,
                    IsEnable = x.IsEnabled,
                    ReportType = x.ReportsType
                }).Where(x => x.IsEnable == true).ToList();
                return ObjData;
            }
        }

        public int UpdateGlobleFilters(tblGlobleFilter tblGlobleFilter)
        {
            int result = -1;
            tblGlobleFilter objTblGlobleFilter = new tblGlobleFilter();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                objTblGlobleFilter = DX.tblGlobleFilters.Where(x => x.PkGlobleFilterId == tblGlobleFilter.PkGlobleFilterId).FirstOrDefault();

                if (objTblGlobleFilter != null)
                {
                    objTblGlobleFilter.IsEnabled = tblGlobleFilter.IsEnabled;
                    objTblGlobleFilter.Source = tblGlobleFilter.Source;
                    objTblGlobleFilter.DocketNumber = tblGlobleFilter.DocketNumber;
                    objTblGlobleFilter.DeliveryAddress = tblGlobleFilter.DeliveryAddress;
                    DX.SubmitChanges();
                    result = 1;
                }
            }
            return result;
        }

        public int DeleteGlobleFilters(int GlobleFilterId)
        {
            int result = -1;
            tblGlobleFilter objTblGlobleFilter = new tblGlobleFilter();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                objTblGlobleFilter = DX.tblGlobleFilters.Where(x => x.PkGlobleFilterId == GlobleFilterId).FirstOrDefault();
                if (objTblGlobleFilter != null)
                {
                    DX.tblGlobleFilters.DeleteOnSubmit(objTblGlobleFilter);
                    DX.SubmitChanges();
                    result = 1;
                }
            }
            return result;
        }
    }

    public class GlobleFilter
    {
        public int GlobleFilterId { get; set; }
        public string ReportType { get; set; }
        public string Keywords { get; set; }
        public string Source { get; set; }
        public string DocketNumber { get; set; }
        public string DeliveryAddress { get; set; }
        public bool IsEnable { get; set; }
    }
}
