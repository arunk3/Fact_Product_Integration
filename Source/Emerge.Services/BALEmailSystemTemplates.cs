﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;

namespace Emerge.Services
{
    public class BALEmailSystemTemplates
    {
        /// <summary>
        /// This method is used to Get Email Templates
        /// </summary>
        /// <param name="TemplateId"></param>
        /// <param name="ApplicationId"></param>
        /// <returns></returns>
        public List<Proc_USA_LoadEmailTemplatesResult> GetEmailTemplates(int TemplateId, Guid ApplicationId, Boolean IsSystemTemplate, int TemplateCode)
        {
            try
            {
                using (EmergeDALDataContext ObjDataContext = new EmergeDALDataContext())
                {
                    return ObjDataContext.LoadEmailTemplates(TemplateId, ApplicationId, IsSystemTemplate, TemplateCode).ToList<Proc_USA_LoadEmailTemplatesResult>();
                }
            }
            catch// (Exception ex)
            {

                throw;
            }
        }
        /// <summary>
        /// This method is used to delete Template
        /// </summary>
        /// <param name="ObjEmailTemplate"></param>
        /// <returns></returns>
        public int DeleteEmailTemplate(int pkTemplateId)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                int val = Dx.DeleteEmailTemplate(pkTemplateId);
                return val;
            }
        }
        /// <summary>
        /// This method is used to Insert Email Template
        /// </summary>
        /// <param name="ObjEmailTemplate"></param>
        /// <returns></returns>
        public int InsertEmailTemplate(tblEmailTemplate ObjEmailTemplate)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                int val = Dx.InsertEmailTemplates(ObjEmailTemplate.fkApplicationId, ObjEmailTemplate.TemplateName,
                                                  ObjEmailTemplate.TemplateSubject,
                                                  ObjEmailTemplate.TemplateContent, ObjEmailTemplate.DefaultTemplateContent,
                                                  ObjEmailTemplate.IsSystemTemplate,
                                                  ObjEmailTemplate.CreatedDate,
                                                  ObjEmailTemplate.TemplateCode);
                return val;

            }
        }
        /// <summary>
        /// This method is used to update Email Template
        /// </summary>
        /// <param name="ObjEmailTemplate"></param>
        /// <returns></returns>
        public int UpdateEmailTemplate(tblEmailTemplate ObjEmailTemplate)
        {
            string TemplateName = ObjEmailTemplate.TemplateName;
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                int val = Dx.UpdateEmailTemplates(ObjEmailTemplate.pkTemplateId, ObjEmailTemplate.fkApplicationId,
                                                        TemplateName, ObjEmailTemplate.TemplateSubject,
                                                        ObjEmailTemplate.TemplateContent,
                                                        ObjEmailTemplate.LastModifiedDate, ObjEmailTemplate.TemplateType);
                return val;
            }
        }




        public int UpdateSystemNotification(int TemplateId, string Title, string Content, bool isEnabled)
        {

            int status = -1;
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                tblEmailTemplate ObjtblEmailTemplate = Dx.tblEmailTemplates.Where(d => d.pkTemplateId == TemplateId).FirstOrDefault();
                if (ObjtblEmailTemplate != null)
                {
                    ObjtblEmailTemplate.TemplateSubject = Title;
                    ObjtblEmailTemplate.DefaultTemplateContent = Title;
                    ObjtblEmailTemplate.TemplateContent = Content;
                    ObjtblEmailTemplate.IsEnabled = isEnabled;
                    Dx.SubmitChanges();
                    status = 1;
                }

            }
            return status;
        }
        public int UpdateSystemNotificationStatus(int TemplateId, bool isEnabled)
        {

            int status = -1;
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                tblEmailTemplate ObjtblEmailTemplate = Dx.tblEmailTemplates.Where(d => d.pkTemplateId == TemplateId).FirstOrDefault();
                if (ObjtblEmailTemplate != null)
                {
                    ObjtblEmailTemplate.IsEnabled = isEnabled;
                    ObjtblEmailTemplate.TemplateSubject = string.Empty;
                    ObjtblEmailTemplate.TemplateContent = string.Empty;
                    Dx.SubmitChanges();
                    status = 1;
                }

            }
            return status;
        }
        /// <summary>
        /// This method is used to update System Template
        /// </summary>
        /// <param name="ObjEmailTemplate"></param>
        /// <returns></returns>
        public int UpdateSystemTemplate(tblEmailTemplate ObjEmailTemplate)
        {
            int iresult = 0;
            tblEmailTemplate ObjtblEmailTemplate = new tblEmailTemplate();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                ObjtblEmailTemplate = DX.tblEmailTemplates.Where(d => d.pkTemplateId == ObjEmailTemplate.pkTemplateId).FirstOrDefault();
                if (ObjtblEmailTemplate != null)
                {
                    ObjtblEmailTemplate.fkApplicationId = ObjEmailTemplate.fkApplicationId;
                    ObjtblEmailTemplate.TemplateName = ObjEmailTemplate.TemplateName;
                    ObjtblEmailTemplate.TemplateSubject = ObjEmailTemplate.TemplateSubject;
                    ObjtblEmailTemplate.TemplateContent = ObjEmailTemplate.TemplateContent;
                    ObjtblEmailTemplate.LastModifiedDate = ObjEmailTemplate.LastModifiedDate;
                    ObjtblEmailTemplate.IsEnabled = ObjEmailTemplate.IsEnabled;
                    DX.SubmitChanges();
                    iresult = 1;
                }
            }
            return iresult;
        }


        

 public int UpdateEmailTemplateNew(tblEmailTemplate ObjEmailTemplate)
        {
            int iresult = 0;
            tblEmailTemplate ObjtblEmailTemplate = new tblEmailTemplate();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                ObjtblEmailTemplate = DX.tblEmailTemplates.Where(d => d.pkTemplateId == ObjEmailTemplate.pkTemplateId).FirstOrDefault();
                if (ObjtblEmailTemplate != null)
                {
                    //   ObjtblEmailTemplate.fkApplicationId = ObjEmailTemplate.fkApplicationId;
                    ObjtblEmailTemplate.TemplateName = ObjEmailTemplate.TemplateName;
                    ObjtblEmailTemplate.TemplateSubject = ObjEmailTemplate.TemplateSubject;
                    ObjtblEmailTemplate.TemplateContent = ObjEmailTemplate.TemplateContent;
                    ObjtblEmailTemplate.LastModifiedDate = ObjEmailTemplate.LastModifiedDate;
                    ObjtblEmailTemplate.IsNotificationTemplate = ObjEmailTemplate.IsNotificationTemplate;
                    DX.SubmitChanges();
                    iresult = 1;
                }
                else
                {
                    iresult = -1;
                }
            }
            return iresult;
        }

        public List<proc_Get_UserEmail_ByRoleIdResult> LoadUsers(int UserType, int pkcompanyId)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                return Dx.Get_UserEmail_ByRoleId(UserType, pkcompanyId).ToList();
            }
        }
        # region Emerge Alerts

        public int InsertAlerts(tblAlert ObjtblAlert)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return (int)ObjEmergeDALDataContext.AddAlerts(ObjtblAlert.AlertTypeId, ObjtblAlert.AlertType, ObjtblAlert.fkUserId, ObjtblAlert.AlertSentDate).ElementAt(0).pkAlertId;
            }
        }

        public int GetMaxFeatureId()
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return (int)ObjEmergeDALDataContext.tblFeatures.Select(d => d.FeatureId).Max();
            }
        }

        //public List<Proc_GetAllUserIdsResult> GetAllUserIds()
        //{
        //    using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
        //    {
        //        return ObjEmergeDALDataContext.GetAllUserIds().ToList();
        //    }
        //}

        public int InsertAlerts(int FeatureId, DateTime CreatedDate)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return DX.InsertAlerts(FeatureId, CreatedDate);
            }
        }

        # endregion

        //public int IsRNGEnabled(int pkTemplateId)
        //{
        //    int Status = 0;
        //    EmergeDALDataContext DX = new EmergeDALDataContext();
        //    try
        //    {
        //        if ((DX.tblEmailTemplates.Where(db => db.pkTemplateId == pkTemplateId && db.IsEnabled == true).Count()) > 0)
        //        {
        //            Status = 1;
        //        }
        //    }
        //    catch
        //    {
        //    }
        //    return Status;

        //}








        //public int DeleteMultiple_Template(List<tblEmailTemplate> lstTemplates)
        //{
        //    DbTransaction DbTrans = null;
        //    using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
        //    {
        //        try
        //        {
        //            ObjEmergeDALDataContext.Connection.Open();
        //            DbTrans = ObjEmergeDALDataContext.Connection.BeginTransaction();
        //            ObjEmergeDALDataContext.Transaction = DbTrans;
        //            for (int i = 0; i < lstTemplates.Count; i++)
        //            {
        //                ObjEmergeDALDataContext.DeleteEmailTemplate(lstTemplates.ElementAt(i).pkTemplateId);
        //            }
        //            DbTrans.Commit(); // Commit Transaction
        //            return (int)1;
        //        }
        //        catch (Exception ex)
        //        {
        //            if (DbTrans != null)
        //            {
        //                DbTrans.Rollback(); // Rollback Transaction
        //            }
        //            throw ex;
        //        }
        //        finally
        //        {
        //            ObjEmergeDALDataContext.Transaction = null;
        //            if (ObjEmergeDALDataContext.Connection.State == ConnectionState.Open)
        //            {
        //                ObjEmergeDALDataContext.Connection.Close(); // Close Connection
        //            }
        //        }
        //    }
        //}



    }
}
