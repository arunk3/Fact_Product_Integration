﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;
using System.Web;
using System.Web.Security;
using Emerge;

using System.Data.Linq.Mapping;
using System.Data.Linq;
using System.Reflection;

namespace Emerge.Services
{

    public class BALInvoiceQuickBook : EmergeDALDataContext
    {

        EmergeDALDataContext myDB = new EmergeDALDataContext();
        /// <summary>
        /// This method is used to fetch the top 1 record from the quickbooks table for inserting in quickbook
        /// </summary>
        /// <returns></returns>
        public QuickBookRequest GetRequest()
        {
            QuickBookRequest getRequest = null;
            try
            {
                getRequest = (from m in myDB.QuickBookRequests
                              where m.Status.Equals(0)
                              select m).OrderBy(m => m.ID).FirstOrDefault();
                //getRequest = (from m in myDB.QuickBookRequests
                //              where m.Status.Equals(0)
                //              select m).Take(1).OrderBy(m => m.ID).FirstOrDefault();
            }
            catch (Exception ex)
            {
                string evLogTxt = " Error in GetRequest in BALInvoiceQuickBook \r\n\r\n";
                evLogTxt = evLogTxt + "Error =" + ex.Message + " /r/n";
                logEvent(evLogTxt);

            }
            return getRequest;


        }
        /// <summary>
        /// This method is used to get record from the tblInvoice and Company for genrating a invoice.
        /// </summary>
        /// <param name="invoiceID"></param>
        /// <returns></returns>

        //public IList<InvoiceQuickbook> GetRecord(int invoiceID)
        //{

        //    try
        //    {
        //        var list = (from i in myDB.tblInvoices
        //                    join c in myDB.tblCompanies on i.fkCompanyId equals c.pkCompanyId
        //                    join l in myDB.tblLocations on c.pkCompanyId equals l.fkCompanyId
        //                    join s in myDB.tblStates on l.fkStateID equals s.pkStateId
        //                    where ((i.pkInvoiceId == invoiceID) && (l.IsHome == true))
        //                    select new InvoiceQuickbook
        //                    {
        //                        InvoiceID = i.pkInvoiceId,
        //                        CompanyName = c.CompanyName,
        //                        Address1 = l.Address1,
        //                        Address2 = l.Address2,
        //                        City = l.City,
        //                        StateName = s.StateName,
        //                        ZipCode = l.ZipCode,
        //                        InvoiceAmount = i.InvoiceAmount,
        //                        InvoiceDate = i.InvoiceDate,
        //                        InvoiceDueDate = i.InvoiceDueDate
        //                    }).ToList();
        //        return list;

        //    }
        //    catch (Exception ex)
        //    {

        //        string evLogTxt = " Error in GetRecord in BALInvoiceQuickBook \r\n\r\n";
        //        evLogTxt = evLogTxt + "Error =" + ex.Message + " /r/n";
        //        logEvent(evLogTxt);

        //        IList<InvoiceQuickbook> list = new List<InvoiceQuickbook>();
        //        return list;
        //    }

        //}

        public IList<InvoiceRecordForQuickBookResult> GetRecord(int invoiceID, int companyID)
        {

            try
            {

                using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
                {
                    return ObjDALDataContext.InvoiceRecordForQuickBook(invoiceID, companyID).ToList<InvoiceRecordForQuickBookResult>();


                }



            }
            catch (Exception ex)
            {

                string evLogTxt = " Error in GetRecord in BALInvoiceQuickBook \r\n\r\n";
                evLogTxt = evLogTxt + "Error =" + ex.Message + " /r/n";
                logEvent(evLogTxt);

                IList<InvoiceRecordForQuickBookResult> list = new List<InvoiceRecordForQuickBookResult>();
                return list;
            }

        }


        public IList<InvoiceRecordDiscoountPerOrderResult> GetRecord_discountPerOrder(int companyID, int month, int year, int invoiceID)
        {

            try
            {

                using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
                {
                    return ObjDALDataContext.InvoiceRecordDiscoountPerOrder(companyID, month, year, invoiceID).ToList<InvoiceRecordDiscoountPerOrderResult>();


                }



            }
            catch (Exception ex)
            {

                string evLogTxt = " Error in GetRecord in BALInvoiceQuickBook \r\n\r\n";
                evLogTxt = evLogTxt + "Error =" + ex.Message + " /r/n";
                logEvent(evLogTxt);

                IList<InvoiceRecordDiscoountPerOrderResult> list = new List<InvoiceRecordDiscoountPerOrderResult>();
                return list;
            }

        }




        public IList<InvoiceRecordForQuickBook_discountResult> GetRecord_discount(int invoiceID, int companyID)
        {

            try
            {

                using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
                {

                    return ObjDALDataContext.InvoiceRecordForQuickBook_discount(invoiceID, companyID).ToList<InvoiceRecordForQuickBook_discountResult>();

                }



            }
            catch (Exception ex)
            {

                string evLogTxt = " Error in GetRecord in BALInvoiceQuickBook \r\n\r\n";
                evLogTxt = evLogTxt + "Error =" + ex.Message + " /r/n";
                logEvent(evLogTxt);

                IList<InvoiceRecordForQuickBook_discountResult> list = new List<InvoiceRecordForQuickBook_discountResult>();
                return list;
            }

        }


        [Function(Name = "dbo.QuickbooksBillingReport")]

        [ResultType(typeof(BALProductsPerLocation))]
        [ResultType(typeof(BALPackagesPrice))]
        

        public IMultipleResults QBBillingReport(Guid ApplicationId, int BillingMonth, int BillingYear, int CompanyId, int LocationId, int UserId, Guid SalesRepId, int RefCodeId, short OrderType, int invoiceID)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), ApplicationId, BillingMonth, BillingYear, CompanyId, LocationId, UserId, SalesRepId, RefCodeId, OrderType, invoiceID);
            return (IMultipleResults)(result.ReturnValue);

        }


        public IList<BALProductsPerLocation> getAllRecord(int CompanyId, int BillingMonth, int BillingYear, int invoiceID)
        {

            IMultipleResults Results = QBBillingReport(new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852"), BillingMonth, BillingYear, CompanyId, 0, 0, new Guid("00000000-0000-0000-0000-000000000000"), 0, 1, invoiceID);
            //.QuickbooksBillingReport(new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852"), month, year, s.pkCompanyId, new Guid("00000000-0000-0000-0000-000000000000"), 0, 0, 0, 1);
            List<BALProductsPerLocation> ObjBALProductsPerLocation = Results.GetResult<BALProductsPerLocation>().ToList();
            List<BALPackagesPrice> ObjBalPackagePrice = Results.GetResult<BALPackagesPrice>().ToList();

            IList<BALProductsPerLocation> lst = new List<BALProductsPerLocation>();
            if (ObjBALProductsPerLocation != null)
            {
                if (ObjBALProductsPerLocation.Count > 0)
                {
                    lst = ObjBALProductsPerLocation;

                }
            }
            if (ObjBalPackagePrice != null)
            {
                if (ObjBalPackagePrice.Count > 0)
                {
                    for (int i = 0; i < ObjBalPackagePrice.Count; i++)
                    {
                        BALProductsPerLocation obj = new BALProductsPerLocation();
                        obj.fkLocationId = ObjBalPackagePrice[i].fkLocationId;
                        obj.fkProductApplicationId = 0;
                        obj.ProductDisplayName = ObjBalPackagePrice[i].PackageName;
                        obj.ProductCode = "";
                        obj.fkReportCategoryId = 0;
                        obj.TotalReports = ObjBalPackagePrice[i].TotalPackages;
                        obj.ReportAmount = ObjBalPackagePrice[i].ReportAmount;
                        obj.IsManualReport = true;
                        obj.IsAdjustmentProduct = 0;
                        obj.ProductQty = ObjBalPackagePrice[i].TotalPackages;
                        lst.Add(obj);
                    }

                }
            }


            //List<BALAdditionalFeeprocforInvoice> ObjBalAdditionalFeeforInvoice = Results.GetResult<BALAdditionalFeeprocforInvoice>().ToList();
            //if (ObjBalAdditionalFeeforInvoice != null)
            //{
            //    if (ObjBalAdditionalFeeforInvoice.Count > 0)
            //    {
            //        for (int i = 0; i < ObjBalAdditionalFeeforInvoice.Count; i++)
            //        {
            //            BALProductsPerLocation obj = new BALProductsPerLocation();
            //            obj.fkLocationId = ObjBalAdditionalFeeforInvoice[i].fkLocationId;
            //            obj.fkProductApplicationId = 0;
            //            if (Convert.ToString(ObjBalAdditionalFeeforInvoice[i].PackageDisplayName) == "")
            //            {
            //                obj.ProductDisplayName = ObjBalAdditionalFeeforInvoice[i].ReportIncludes;
            //            }

            //            else
            //            {
            //                obj.ProductDisplayName = ObjBalAdditionalFeeforInvoice[i].PackageDisplayName;
            //            }


            //            if (ObjBalAdditionalFeeforInvoice[i].ReportWithStatus.Contains("_0_0") || ObjBalAdditionalFeeforInvoice[i].ReportWithStatus.Contains("_1_0"))
            //            {
            //                ObjBalAdditionalFeeforInvoice[i].ReportWithStatus.Replace("_0_0", "");
            //                ObjBalAdditionalFeeforInvoice[i].ReportWithStatus.Replace("_1_0", "");
            //            }

            //            obj.ProductCode = ObjBalAdditionalFeeforInvoice[i].ReportWithStatus;
            //            obj.fkReportCategoryId = 0;
            //            obj.TotalReports = 1;
            //            obj.ReportAmount = ObjBalAdditionalFeeforInvoice[i].AdditionalFees;
            //            obj.IsManualReport = true;
            //            obj.IsAdjustmentProduct = 0;
            //            obj.ProductQty = 1;
            //            lst.Add(obj);
            //        }

            //    }
            //}







            return lst;
        }
        /// <summary>
        /// This method is used to update the quickbook table on the basis of functionality
        /// </summary>
        /// <param name="requestID"></param>
        /// <param name="ListID"></param>
        /// <param name="updateType"></param>
        /// <param name="status"></param>
        /// <param name="txnID"></param>
        /// <returns></returns>
        public bool updateRequestByID(int requestID, string ListID, string updateType, bool status, string txnID, string refNo)
        {
            try
            {
                QuickBookRequest item = (from m in myDB.QuickBookRequests
                                         where m.ID == requestID
                                         select m).FirstOrDefault();

                if (updateType == "ListID")
                {
                    item.ListID = ListID;
                }
                else if (updateType == "AddInvoice")
                {

                    item.Status = status;
                    item.RefNo = refNo;
                    item.TranID = txnID;
                }
                else if (updateType == "AddCustomer")
                {
                    item.ListID = ListID;
                    item.Status = status;
                }
                else if (updateType == "MakePayment")
                {

                    item.Status = status;
                }
                else if (updateType == "UpdateInvoice")
                {

                    item.Status = status;
                }

                myDB.SubmitChanges();
                return true;
            }
            catch (Exception ex)
            {
                string evLogTxt = " Error in updateRequestByID in BALInvoiceQuickBook \r\n\r\n";
                evLogTxt = evLogTxt + "Error =" + ex.Message + " /r/n";
                logEvent(evLogTxt);

                return false;
            }



        }

        private void logEvent(string logText)
        {

            try
            {
                string path = HttpContext.Current.Server.MapPath("~/LogFile.txt");
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(path, true))
                {
                    file.WriteLine(logText);
                }

                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"d:\LogFile.txt", true))
                {
                    file.WriteLine(logText);
                }

                //var baseDir = AppDomain.CurrentDomain.BaseDirectory;
                //var retFilePath = baseDir + "Log\\Log" + String.Format("{0:MM-d-yyyy}", DateTime.Now) + ".txt";
                //Log.LogFilePath = retFilePath;
                //Log.ErrorRoutine(false, null, logText);
                //evLog.WriteEntry(logText);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public IList<QuickBooksPaymentResult> GetRecordForPayment(int invoiceID, string bankTaxID, int companyID)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.QuickBooksPayment(invoiceID, bankTaxID, companyID).ToList<QuickBooksPaymentResult>();
            }
        }



        public IList<QuickBookUpdatedRecordsDataResult> QuickBookUpdatedRecordsData(string Tranid, string ListID, string companyname)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.QuickBookUpdatedRecordsData(Tranid, ListID, companyname).ToList<QuickBookUpdatedRecordsDataResult>();
            }
        }


        public IList<QuickBooksPayment_discountPerInvoiceResult> GetRecordForPayment_discount(long invoiceID, int companyID)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.QuickBooksPayment_discountPerInvoice(invoiceID, companyID).ToList<QuickBooksPayment_discountPerInvoiceResult>();
            }
        }


        public IList<QuickBooksPayment_discountResult> GetRecordForPayment_Discount(int invoiceID, string bankTaxID, int companyID)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.QuickBooksPayment_discount(invoiceID, bankTaxID, companyID).ToList<QuickBooksPayment_discountResult>();
            }
        }

        public void MakePayement(long invoiceId, string bankTranxID, int companyID)
        {
            try
            {


                using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
                {
                    //var data = ObjDALDataContext.QuickBookRequests.Where;


                    var InvoiceMonthYeardata = (from tb1 in ObjDALDataContext.tblInvoices
                                                where tb1.pkInvoiceId.Equals(invoiceId)
                                                select tb1).ToList();

                    var Companydata = (from tb1 in ObjDALDataContext.tblCompanies
                                       where tb1.pkCompanyId.Equals(companyID)
                                       select tb1).ToList();

                    QuickBookRequest objQCRequest = new QuickBookRequest();
                    objQCRequest.InvoiceID = invoiceId;
                    //objQCRequest.CompanyName = bankTranxID;   edit on 16 nov 2015

                    objQCRequest.CompanyName = Companydata.ElementAt(0).CompanyName.ToString();
                    objQCRequest.TranID = bankTranxID;
                    objQCRequest.CompanyID = Convert.ToInt32(InvoiceMonthYeardata.ElementAt(0).fkCompanyId);
                    objQCRequest.Type = "MakePayment";
                    objQCRequest.InvoiceMonth = Convert.ToInt32(InvoiceMonthYeardata.ElementAt(0).InvoiceMonth);
                    objQCRequest.InvoiceYear = Convert.ToInt32(InvoiceMonthYeardata.ElementAt(0).InvoiceYear);

                    myDB.QuickBookRequests.InsertOnSubmit(objQCRequest);
                    myDB.SubmitChanges();



                }




            }
            catch
            {

            }
        }

    }
    public class InvoiceQuickbook
    {
        public long InvoiceID { get; set; }
        public string CompanyName { get; set; }
        public string Description { get; set; }
        public string Quantity { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string StateName { get; set; }
        public string ZipCode { get; set; }
        public decimal InvoiceAmount { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public DateTime? InvoiceDueDate { get; set; }

    }
}
