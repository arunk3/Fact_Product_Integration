﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;
using VendorServices;
using System.IO;
using System.Globalization;
using System.Xml;
using System.Net;
using System.Net.Mail;
using System.Configuration;
using Emerge.Services.Helper;
using System.Xml.Linq;
using System.Web.Security;
using System.Web;

namespace Emerge.Services
{
    public class BALPendingOrders
    {
        public string _filepath;

        public string FilePath
        {
            get { return _filepath; }
            set { _filepath = value; }
        }


        public string _rcxfilepath;

        public string RCXFilePath
        {
            get { return _rcxfilepath; }
            set { _rcxfilepath = value; }
        }

        public string _rcxRequestpath;

        public string RCXRequestPath
        {
            get { return _rcxRequestpath; }
            set { _rcxRequestpath = value; }
        }

        private int? _mCompanyUserId = 0;

        string Response_data = "";
        // bool isncr1toncr2 = false;//INT-213
        List<Proc_Get_PendingReportsResult> ObjPendingReports = null;
        List<Proc_Get_TieredReportOrdersResult> ObjTieredReports = null;
        List<Proc_Get_PendingReportByVendorIdResult> ObjPendingReportByVendorId = null;
        BALGeneral ObjBALGeneral;
        BALOrders ObjBALOrders;
        XpediteService ObjXpediteService;
        MicrobiltService ObjMicrobiltService;
        BackgroundChecksService ObjBackgroundChecksService;
        RapidCourtService ObjRapidCourtService;
        SoftechService ObjSoftechService;
        FRSService ObjFRSService;
        SocialDiligenceService ObjSocialDiligenceService;
        GeneralService ObjGeneralService;
        BALProducts ObjBALProducts;
        List<Proc_GetAllProductsWithVendorResult> ObjProc_GetAllProductsWithVendorResult;
        Proc_Get_PendingReportByOrderIdResult ObjProc_Get_PendingReportByOrderIdResult;
        int Counter = 0, dc = 0;

        /// <summary>
        /// Function to Run the Pending Reports.
        /// </summary>
        public void ResendPendingOrders(out int? ErrorOrderID)
        {
            int? ErrorOrderID_FRS = 0, ErrorOrderID_OTHER = 0, _ErrorOrderID = 0;
            ObjBALOrders = new BALOrders();
            try
            {
                if (System.IO.File.Exists(System.Configuration.ConfigurationManager.AppSettings["LogPath"] + "AllPendingOrders.txt"))
                {
                    System.IO.File.WriteAllText(System.Configuration.ConfigurationManager.AppSettings["LogPath"] + "AllPendingOrders.txt", string.Empty);
                }
                else
                {
                    System.IO.File.Create(System.Configuration.ConfigurationManager.AppSettings["LogPath"] + "AllPendingOrders.txt");
                }

                using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
                {
                    ObjPendingReports = ObjDALDataContext.Get_PendingReports().ToList();
                }

                if (ObjPendingReports != null && ObjPendingReports.Count > 0)
                {
                    //  For NON FRS which are not instant
                    List<Proc_Get_PendingReportsResult> ObjProc_Get_PendingReportsResult_NON_FRS = ObjPendingReports.Where(data => data.IsInstantReport == false && (data.ProductCode != "EDV" && data.ProductCode != "EMV" && data.ProductCode != "PRV" && data.ProductCode != "PLV" && data.ProductCode != "PRV2" && data.ProductCode != "WCV")).ToList<Proc_Get_PendingReportsResult>();//WCV enabled
                    if (ObjProc_Get_PendingReportsResult_NON_FRS.Count > 0)
                    {
                        VendorServices.GeneralService.WriteLog(Environment.NewLine + "NONFrsReportOrders Started:", "AllPendingOrders");
                        foreach (Proc_Get_PendingReportsResult ObjReport in ObjProc_Get_PendingReportsResult_NON_FRS)
                        {
                            VendorServices.GeneralService.WriteLog("," + ObjReport.fkOrderId, "AllPendingOrders");
                            _ErrorOrderID = RunNonFrsReports(_ErrorOrderID, ObjReport);
                        }
                        VendorServices.GeneralService.WriteLog(Environment.NewLine + "NONFrsReportOrders Completed:", "AllPendingOrders");
                    }
                    //  For FRS Report which are not instant
                    List<Proc_Get_PendingReportsResult> ObjProc_Get_PendingReportsResult_FRS = ObjPendingReports.Where(data => data.IsInstantReport == false && (data.ProductCode == "EDV" || data.ProductCode == "EMV" || data.ProductCode == "PRV" || data.ProductCode == "PLV" || data.ProductCode == "PRV2" || data.ProductCode == "WCV")).ToList<Proc_Get_PendingReportsResult>();//WCV enabled
                    if (ObjProc_Get_PendingReportsResult_FRS.Count > 0)
                    {
                        foreach (Proc_Get_PendingReportsResult _ObjReportFRS in ObjProc_Get_PendingReportsResult_FRS)
                        {
                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "FrsReportOrders Started:", "AllPendingOrders");
                            GetResponseAndSaveResponse_For_FRS(_ObjReportFRS, out ErrorOrderID_FRS);
                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "FrsReportOrders Completed:", "AllPendingOrders");
                            _ErrorOrderID = ErrorOrderID_FRS;
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                VendorServices.GeneralService.WriteLog(Environment.NewLine + "Error at method:RunNonFrsReports" + Environment.NewLine + ex.StackTrace.ToString(), "AllPendingOrders");
            }
            ErrorOrderID = _ErrorOrderID;
        }

        public void ResendPendingOrderByVendorId(byte VendorId, out int? ErrorOrderID)
        {
            int? ErrorOrderID_FRS = 0, ErrorOrderID_OTHER = 0, _ErrorOrderID = 0;
            ObjBALOrders = new BALOrders();
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                ObjPendingReportByVendorId = ObjDALDataContext.Proc_Get_PendingReportByVendorId(VendorId).ToList();
                ObjPendingReports = new List<Proc_Get_PendingReportsResult>();

                foreach (Proc_Get_PendingReportByVendorIdResult item in ObjPendingReportByVendorId)
                {
                    ObjPendingReports.Add(new Proc_Get_PendingReportsResult
                    {
                        Email = item.Email,
                        FieldsMissing = item.FieldsMissing,
                        fkOrderId = item.fkOrderId,
                        fkProductApplicationId = item.fkProductApplicationId,
                        IsInstantReport = item.IsInstantReport,
                        IsLiveRunner = item.IsLiveRunner,
                        OrderDt = item.OrderDt,
                        OrderNo = item.OrderNo,
                        pkOrderDetailId = item.pkOrderDetailId,
                        pkOrderRequestId = item.pkOrderRequestId,
                        pkOrderResponseId = item.pkOrderResponseId,
                        pkOrderSearchedId = item.pkOrderSearchedId,
                        pkProductId = item.pkProductId,
                        ProductCode = item.ProductCode,
                        ProductDisplayName = item.ProductDisplayName,
                        RefSecureKey = item.RefSecureKey,
                        ReportStatus = item.ReportStatus,
                        RequestData = item.RequestData,
                        ResponseData = item.ResponseData,
                        search_county = item.search_county,
                        search_state = item.search_state,
                        SearchedApt = item.SearchedApt,
                        SearchedBusinessCity = item.SearchedBusinessCity,
                        SearchedBusinessName = item.SearchedBusinessName,
                        SearchedBusinessStateId = item.SearchedBusinessStateId,
                        SearchedCity = item.SearchedCity,
                        SearchedCountyId = item.SearchedCountyId,
                        SearchedDirection = item.SearchedDirection,
                        SearchedDLStateId = item.SearchedDLStateId,
                        SearchedDob = item.SearchedDob,
                        SearchedDriverLicense = item.SearchedDriverLicense,
                        SearchedFirstName = item.SearchedFirstName,
                        SearchedLastName = item.SearchedLastName,
                        SearchedMiddleInitial = item.SearchedMiddleInitial,
                        SearchedPhoneNumber = item.SearchedPhoneNumber,
                        SearchedSex = item.SearchedSex,
                        SearchedSsn = item.SearchedSsn,
                        SearchedStateCode = item.SearchedStateCode,
                        SearchedStateId = item.SearchedStateId,
                        SearchedStreetAddress = item.SearchedStreetAddress,
                        SearchedStreetName = item.SearchedStreetName,
                        SearchedStreetType = item.SearchedStreetType,
                        SearchedSuffix = item.SearchedSuffix,
                        SearchedTrackingNotes = item.SearchedTrackingNotes,
                        SearchedTrackingRef = item.SearchedTrackingRef,
                        SearchedTrackingRefId = item.SearchedTrackingRefId,
                        SearchedVehicleVin = item.SearchedVehicleVin,
                        SearchedZipCode = item.SearchedZipCode,
                        SearchPersonHTML = item.SearchPersonHTML,
                        SortOrder = item.SortOrder,
                        UserName = item.UserName
                    });
                }
            }

            if (ObjPendingReports != null && ObjPendingReports.Count > 0)
            {
                //  For NON FRS which are not instant
                List<Proc_Get_PendingReportsResult> ObjProc_Get_PendingReportsResult_NON_FRS = ObjPendingReports.Where(data => data.IsInstantReport == false && (data.ProductCode != "EDV" && data.ProductCode != "EMV" && data.ProductCode != "PRV" && data.ProductCode != "PLV" && data.ProductCode != "PRV2" && data.ProductCode != "WCV")).ToList<Proc_Get_PendingReportsResult>();//WCV enabled
                if (ObjProc_Get_PendingReportsResult_NON_FRS.Count > 0)
                {
                    foreach (Proc_Get_PendingReportsResult ObjReport in ObjProc_Get_PendingReportsResult_NON_FRS)
                    {
                        _ErrorOrderID = RunNonFrsReports(_ErrorOrderID, ObjReport);
                    }
                }

                //  For FRS Report which are not instant
                List<Proc_Get_PendingReportsResult> ObjProc_Get_PendingReportsResult_FRS = ObjPendingReports.Where(data => data.IsInstantReport == false && (data.ProductCode == "EDV" || data.ProductCode == "EMV" || data.ProductCode == "PRV" || data.ProductCode == "PLV" || data.ProductCode == "PRV2")).ToList<Proc_Get_PendingReportsResult>();
                if (ObjProc_Get_PendingReportsResult_FRS.Count > 0)
                {
                    foreach (Proc_Get_PendingReportsResult _ObjReportFRS in ObjProc_Get_PendingReportsResult_FRS)
                    {
                        GetResponseAndSaveResponse_For_FRS(_ObjReportFRS, out ErrorOrderID_FRS);
                        _ErrorOrderID = ErrorOrderID_FRS;
                    }
                }
            }
            ErrorOrderID = _ErrorOrderID;
        }

        private int? RunNonFrsReports(int? _ErrorOrderID, Proc_Get_PendingReportsResult ObjReport)
        {
            try
            {
                _ErrorOrderID = ObjReport.fkOrderId;
                GetResponseAndSaveResponse(ObjReport);
            }
            catch (Exception ex)
            {
                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\nError in RunNonFrsReports() method- Order at:" + _ErrorOrderID.ToString() + Environment.NewLine + ex.InnerException.ToString(), "WindowsService_ResendPendingAllOrders");
            }
            return _ErrorOrderID;
        }

        /// <summary>
        /// Function to Fetch the Response For FRS Report Only
        /// </summary>
        /// <param name="ObjProc_Get_PendingReportsResult_FRS"></param>
        private void GetResponseAndSaveResponse_For_FRS(Proc_Get_PendingReportsResult ObjReport_FRS, out int? ErrorOrderID)
        {
            int? _ErrorOrderID = 0, ErrorCode_FRS = 0;
            ObjBALProducts = new BALProducts();
            Proc_Get_PendingReportsResult ObjReport = ObjReport_FRS;
            VendorServices.GeneralService.WriteLog("," + ObjReport.fkOrderId, "AllPendingOrders");

            int? pkOrderId = ObjReport.fkOrderId;
            int OrderDetailId = ObjReport.pkOrderDetailId;
            ObjProc_GetAllProductsWithVendorResult = ObjBALProducts.GetAllProductsWithVendor();
            string ProductCode = ObjReport.ProductCode;
            string Vendor_Request = "";
            string Vendor_Response = ObjReport.ResponseData;

            string[] String_Separator = new string[] { "$#$#" };

            if (ObjReport.RequestData.Contains("$#$#"))
            {
                string[] Request_array = ObjReport.RequestData.Split(String_Separator, StringSplitOptions.RemoveEmptyEntries);
                Vendor_Request = Request_array.Last();
            }
            else
            {
                Vendor_Request = ObjReport.RequestData;
            }
            string RefSecureKey = ObjReport.RefSecureKey;
            ObjBALGeneral = new BALGeneral();

            //string Response = "";
               // string Error = "";
            List<FRSAttributes> Response_Dic = new List<FRSAttributes>();
            string strRequest = string.Empty;
            string reqId = string.Empty;
            try
            {

                try
                {
                    if (ObjReport_FRS.ProductCode.ToUpper() == "WCV")//WCV enabled
                    {
                        FRSServiceWCV ObjFRSService = new FRSServiceWCV();
                        using (EmergeDALDataContext dx = new EmergeDALDataContext())
                        {
                            strRequest = dx.tblOrderRequestDatas.Where(x => x.fkOrderDetailId == OrderDetailId).Select(x => x.RequestData).FirstOrDefault();
                        }

                        XmlDocument xdoc = new XmlDocument();
                        xdoc.InnerXml = strRequest;
                        reqId = Convert.ToString(xdoc.GetElementsByTagName("getOrderResults")[0].Attributes[0].Value); //To get the Order no generated by vendor service.
                        Vendor_Request = GetFRSOrderDetailByVendor(reqId);
                        //int iResultRequest = 
                            new BALOrders().UpdateOrderRequestData(OrderDetailId, Vendor_Request);//Updated the Reuest data.
                        Response_Dic = ObjFRSService.RunReport_ForPendingReports(PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, Vendor_Response, RefSecureKey, pkOrderId.ToString(), OrderDetailId.ToString()), reqId);
                    }
                    else
                    {
                        FRSService ObjFRSService = new FRSService();
                        using (EmergeDALDataContext dx = new EmergeDALDataContext())
                        {
                            strRequest = dx.tblOrderRequestDatas.Where(x => x.fkOrderDetailId == OrderDetailId).Select(x => x.RequestData).FirstOrDefault();
                        }
                        Vendor_Request = strRequest;
                        Response_Dic = ObjFRSService.RunReport_ForPendingReports(PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, Vendor_Response, RefSecureKey, pkOrderId.ToString(), OrderDetailId.ToString()), reqId);
                    }
                }
                catch //(Exception ex)
                {

                }
                finally
                {
                    ObjFRSService = null;
                }
            }
            catch //(Exception ex)
            {

            }
            finally
            {
                FRS_Final_Process(Response_Dic, ObjReport_FRS, out ErrorCode_FRS);

                ErrorOrderID = ErrorCode_FRS;
            }
        }


        /// <summary>
        /// To get the Order detail by Order Id //INT-15
        /// </summary>
        /// <param name="OrderId"></param>
        /// <returns></returns>
        public string GetFRSOrderDetailByVendor(string OrderId)
        {

            StringBuilder ObjStringBuilder_rootNode = new StringBuilder();
            try
            {
                ObjStringBuilder_rootNode.Append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
                ObjStringBuilder_rootNode.Append("<XML>");
                ObjStringBuilder_rootNode.Append("<login>");
                ObjStringBuilder_rootNode.Append("<account>INTSCR</account>");
                ObjStringBuilder_rootNode.Append("<username>admin</username>");
                ObjStringBuilder_rootNode.Append("<password>Intelifi123</password>");
                ObjStringBuilder_rootNode.Append("</login>");
                ObjStringBuilder_rootNode.Append("<mode>PROD</mode>");
                ObjStringBuilder_rootNode.Append("<getOrderResults orderID='" + OrderId + "'/>");
                ObjStringBuilder_rootNode.Append("</XML>");
            }
            catch (Exception)
            {

                throw;
            }

            return Convert.ToString(ObjStringBuilder_rootNode);
        }


        private void FRS_Final_Process(List<FRSAttributes> Response_Dic, Proc_Get_PendingReportsResult ObjReport_FRS, out int? ErrorOrderId)
        {
            int? _ErrorOrderId = 0;
            try
            {
                List<PendingLog> ObjPendingLog = new List<PendingLog>();
                Proc_Get_PendingReportsResult ObjSingle_Order = new Proc_Get_PendingReportsResult();
                List<Proc_Get_PendingReportsResult> objFRS = new List<Proc_Get_PendingReportsResult>() { ObjReport_FRS };

                foreach (FRSAttributes ObjFRSAttributes_SingleResponse in Response_Dic)
                {
                    var lstReport_FRS = from c in objFRS
                                        where c.fkOrderId.ToString().Contains(ObjFRSAttributes_SingleResponse.FRS_Order_Id)
                                        select c;
                    ObjSingle_Order = lstReport_FRS.FirstOrDefault();
                    _ErrorOrderId = CheckFrsResponse_Sub(_ErrorOrderId, ObjSingle_Order, ObjFRSAttributes_SingleResponse);
                    if (_ErrorOrderId != 0)
                        ObjPendingLog.Add(new PendingLog { Error = ObjFRSAttributes_SingleResponse.FRS_Error, pkOrderId = _ErrorOrderId, RuningRime = DateTime.Now });

                }
                WritePendingLog(ObjPendingLog);
            }
            catch //(Exception ex)
            {
                //    SendErrorMail(ex.Message.ToString(), "FRS - WCV : Error Stage 3 : (FRS_Final_Process) : " + DateTime.Now.ToString());
            }
            ErrorOrderId = _ErrorOrderId;
        }
        private int? CheckFrsResponse_Sub(int? _ErrorOrderId, Proc_Get_PendingReportsResult ObjSingle_Order, FRSAttributes ObjFRSAttributes_SingleResponse)
        {
            try
            {
                if (ObjSingle_Order.fkOrderId.ToString().Contains(ObjFRSAttributes_SingleResponse.FRS_Order_Id) && ObjSingle_Order.ProductCode.ToLower() == ObjFRSAttributes_SingleResponse.FRS_Product_Code.ToLower())
                {
                    _ErrorOrderId = ObjSingle_Order.fkOrderId;
                    UpdateStatus_And_Sendemail(ObjSingle_Order, ObjFRSAttributes_SingleResponse.FRS_Response, string.Empty, string.Empty, string.Empty);
                }
                else { }
            }
            catch //(Exception ex)
            {


            }
            return _ErrorOrderId;
        }

        /// <summary>
        /// Function to Re run Single Pening Order.
        /// </summary>
        /// <param name="pkOrderId_nullable"></param>
        public void ResendPendingOrderByOrderId(int pkOrderId_nullable, string ProductCode)
        {
            ObjBALOrders = new BALOrders();
            //string Body = "";
            int? ErrorOrderID_FRS = 0, _ErrorOrderID = 0;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                ObjProc_Get_PendingReportByOrderIdResult = ObjDALDataContext.Get_PendingReportByOrderId(pkOrderId_nullable).Where(data => data.IsInstantReport == false && (data.ProductCode.ToLower() == ProductCode.ToLower())).FirstOrDefault();
            }
            if (ObjProc_Get_PendingReportByOrderIdResult != null)
            {
                if (ProductCode == "EDV" || ProductCode == "EMV" || ProductCode == "PRV" || ProductCode == "PLV" || ProductCode == "PRV2" || ProductCode == "WCV")
                {

                    using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
                    {
                        ObjPendingReports = ObjDALDataContext.Get_PendingReports().ToList();
                    }
                    if (ObjPendingReports != null && ObjPendingReports.Count > 0)
                    {
                        List<Proc_Get_PendingReportsResult> ObjProc_Get_PendingReportsResult_FRS = ObjPendingReports.Where(data => data.IsInstantReport == false && (data.ProductCode == "EDV" || data.ProductCode == "EMV" || data.ProductCode == "PRV" || data.ProductCode == "PLV" || data.ProductCode == "PRV2")).ToList<Proc_Get_PendingReportsResult>();
                        if (ObjProc_Get_PendingReportsResult_FRS.Count > 0)
                        {
                            foreach (Proc_Get_PendingReportsResult _ObjReportFRS in ObjProc_Get_PendingReportsResult_FRS)
                            {
                                GetResponseAndSaveResponse_For_FRS(_ObjReportFRS, out ErrorOrderID_FRS);
                                _ErrorOrderID = ErrorOrderID_FRS;
                            }
                        }
                    }
                }
                else
                {
                    GetResponseAndSaveResponse(ConvertAs(ObjProc_Get_PendingReportByOrderIdResult));
                }
            }
        }

        private Proc_Get_PendingReportsResult ConvertAs(Proc_Get_PendingReportByOrderIdResult ObjProc_Get_PendingReportByOrderIdResult)
        {
            Proc_Get_PendingReportsResult ObjProc_Get_PendingReportsResult = new Proc_Get_PendingReportsResult();
            ObjProc_Get_PendingReportsResult.Email = ObjProc_Get_PendingReportByOrderIdResult.Email;
            ObjProc_Get_PendingReportsResult.IsLiveRunner = ObjProc_Get_PendingReportByOrderIdResult.IsLiveRunner;
            ObjProc_Get_PendingReportsResult.fkOrderId = ObjProc_Get_PendingReportByOrderIdResult.fkOrderId;
            ObjProc_Get_PendingReportsResult.fkProductApplicationId = ObjProc_Get_PendingReportByOrderIdResult.fkProductApplicationId;
            ObjProc_Get_PendingReportsResult.OrderDt = ObjProc_Get_PendingReportByOrderIdResult.OrderDt;
            ObjProc_Get_PendingReportsResult.OrderNo = ObjProc_Get_PendingReportByOrderIdResult.OrderNo;
            ObjProc_Get_PendingReportsResult.pkOrderDetailId = ObjProc_Get_PendingReportByOrderIdResult.pkOrderDetailId;
            ObjProc_Get_PendingReportsResult.pkOrderRequestId = ObjProc_Get_PendingReportByOrderIdResult.pkOrderRequestId;
            ObjProc_Get_PendingReportsResult.pkOrderResponseId = ObjProc_Get_PendingReportByOrderIdResult.pkOrderResponseId;
            ObjProc_Get_PendingReportsResult.pkOrderSearchedId = ObjProc_Get_PendingReportByOrderIdResult.pkOrderSearchedId;
            ObjProc_Get_PendingReportsResult.pkProductId = ObjProc_Get_PendingReportByOrderIdResult.pkProductId;
            ObjProc_Get_PendingReportsResult.ProductCode = ObjProc_Get_PendingReportByOrderIdResult.ProductCode;
            ObjProc_Get_PendingReportsResult.ProductDisplayName = ObjProc_Get_PendingReportByOrderIdResult.ProductDisplayName;
            ObjProc_Get_PendingReportsResult.RefSecureKey = ObjProc_Get_PendingReportByOrderIdResult.RefSecureKey;
            ObjProc_Get_PendingReportsResult.ReportStatus = ObjProc_Get_PendingReportByOrderIdResult.ReportStatus;
            ObjProc_Get_PendingReportsResult.RequestData = ObjProc_Get_PendingReportByOrderIdResult.RequestData;
            ObjProc_Get_PendingReportsResult.ResponseData = ObjProc_Get_PendingReportByOrderIdResult.ResponseData;
            ObjProc_Get_PendingReportsResult.SearchedApt = ObjProc_Get_PendingReportByOrderIdResult.SearchedApt;
            ObjProc_Get_PendingReportsResult.SearchedBusinessCity = ObjProc_Get_PendingReportByOrderIdResult.SearchedBusinessCity;
            ObjProc_Get_PendingReportsResult.SearchedBusinessName = ObjProc_Get_PendingReportByOrderIdResult.SearchedBusinessName;
            ObjProc_Get_PendingReportsResult.SearchedBusinessStateId = ObjProc_Get_PendingReportByOrderIdResult.SearchedBusinessStateId;
            ObjProc_Get_PendingReportsResult.SearchedCity = ObjProc_Get_PendingReportByOrderIdResult.SearchedCity;
            ObjProc_Get_PendingReportsResult.SearchedCountyId = ObjProc_Get_PendingReportByOrderIdResult.SearchedCountyId;
            ObjProc_Get_PendingReportsResult.SearchedDirection = ObjProc_Get_PendingReportByOrderIdResult.SearchedDirection;
            ObjProc_Get_PendingReportsResult.SearchedDLStateId = ObjProc_Get_PendingReportByOrderIdResult.SearchedDLStateId;
            ObjProc_Get_PendingReportsResult.SearchedDob = ObjProc_Get_PendingReportByOrderIdResult.SearchedDob;
            ObjProc_Get_PendingReportsResult.SearchedDriverLicense = ObjProc_Get_PendingReportByOrderIdResult.SearchedDriverLicense;
            ObjProc_Get_PendingReportsResult.SearchedFirstName = ObjProc_Get_PendingReportByOrderIdResult.SearchedFirstName;
            ObjProc_Get_PendingReportsResult.SearchedLastName = ObjProc_Get_PendingReportByOrderIdResult.SearchedLastName;
            ObjProc_Get_PendingReportsResult.SearchedMiddleInitial = ObjProc_Get_PendingReportByOrderIdResult.SearchedMiddleInitial;
            ObjProc_Get_PendingReportsResult.SearchedPhoneNumber = ObjProc_Get_PendingReportByOrderIdResult.SearchedPhoneNumber;
            ObjProc_Get_PendingReportsResult.SearchedSex = ObjProc_Get_PendingReportByOrderIdResult.SearchedSex;
            ObjProc_Get_PendingReportsResult.SearchedSsn = ObjProc_Get_PendingReportByOrderIdResult.SearchedSsn;
            ObjProc_Get_PendingReportsResult.SearchedStateCode = ObjProc_Get_PendingReportByOrderIdResult.SearchedStateCode;
            ObjProc_Get_PendingReportsResult.SearchedStateId = ObjProc_Get_PendingReportByOrderIdResult.SearchedStateId;
            ObjProc_Get_PendingReportsResult.SearchedStreetAddress = ObjProc_Get_PendingReportByOrderIdResult.SearchedStreetAddress;
            ObjProc_Get_PendingReportsResult.SearchedStreetName = ObjProc_Get_PendingReportByOrderIdResult.SearchedStreetName;
            ObjProc_Get_PendingReportsResult.SearchedStreetType = ObjProc_Get_PendingReportByOrderIdResult.SearchedStreetType;
            ObjProc_Get_PendingReportsResult.SearchedSuffix = ObjProc_Get_PendingReportByOrderIdResult.SearchedSuffix;
            ObjProc_Get_PendingReportsResult.SearchedTrackingNotes = ObjProc_Get_PendingReportByOrderIdResult.SearchedTrackingNotes;
            ObjProc_Get_PendingReportsResult.SearchedTrackingRef = ObjProc_Get_PendingReportByOrderIdResult.SearchedTrackingRef;
            ObjProc_Get_PendingReportsResult.SearchedVehicleVin = ObjProc_Get_PendingReportByOrderIdResult.SearchedVehicleVin;
            ObjProc_Get_PendingReportsResult.SearchedZipCode = ObjProc_Get_PendingReportByOrderIdResult.SearchedZipCode;
            ObjProc_Get_PendingReportsResult.SearchPersonHTML = ObjProc_Get_PendingReportByOrderIdResult.SearchPersonHTML;
            ObjProc_Get_PendingReportsResult.SortOrder = ObjProc_Get_PendingReportByOrderIdResult.SortOrder;
            ObjProc_Get_PendingReportsResult.UserName = ObjProc_Get_PendingReportByOrderIdResult.UserName;


            return ObjProc_Get_PendingReportsResult;
        }

        private void GetResponseAndSaveResponse(Proc_Get_PendingReportsResult ObjReport)
        {
            try
            {
                Response_data = GetXMLResponse_ForPendingReports(ObjReport);
                //  Update Response
                UpdateStatus_And_Sendemail(ObjReport, Response_data, string.Empty, string.Empty, string.Empty);
            }
            catch (Exception ex)
            {
                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\nGetResponseAndSaveResponse gave error at:" + DateTime.Now.ToString() + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace.ToString(), "WindowsService_ResendPendingAllOrders");
                //throw ex;
            }
        }

        /// <summary>
        /// Function To Update Mvr Response
        /// </summary>
        /// <param name="OrderId"></param>
        /// <param name="Response_data"></param>
        public void UpdateMvrResponse(int OrderId, string Response_data, string ProductCode, string CompanyAccountNo, string OrderCompanyName, string PermissiblePurpose)
        {
            ObjBALOrders = new BALOrders();
            //string Body = "";
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                ObjProc_Get_PendingReportByOrderIdResult = ObjDALDataContext.Get_PendingReportByOrderId(OrderId).Where(data => data.ProductCode.ToLower() == ProductCode.ToLower()).FirstOrDefault();
            }
            if (ObjProc_Get_PendingReportByOrderIdResult != null)
            {
                UpdateStatus_And_Sendemail(ConvertAs(ObjProc_Get_PendingReportByOrderIdResult), Response_data, CompanyAccountNo, OrderCompanyName, PermissiblePurpose);
            }
        }

        public void UpdateStatus_And_Sendemail(Proc_Get_PendingReportsResult ObjReport, string Response_data, string CompanyAccountNo, string OrderCompanyName, string PermissiblePurpose)
        {
            string fileName = "Reviewmail_" + DateTime.Now.ToString("yyMMdd");
            string sentMailLog = "UpdateStatus_And_Sendemail method call started : " + Convert.ToString(ObjReport.OrderNo);
            BALGeneral.WriteLogForDeletion(sentMailLog, fileName);
            try
            {
                BALOrders ObjOrder = new BALOrders();
                BALGeneral ObjBALGeneral = new BALGeneral();
                //BLLMembership ObjBLLMembership = new BLLMembership();
                string fiteredResponse = Response_data;
                using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                {
                    //string Body = "";
                    string Subject = "";
                    string ProductCode = ObjReport.ProductCode;
                    if (ObjReport.IsLiveRunner == true)
                    {
                        ProductCode = "RCX";
                    }
                    byte ReportStatus = 1;

                    if ((ProductCode == "NCR1" || ProductCode == "PS") && Response_data == "") // Incomplete NCR1, PS report if response is empty
                    {
                        ReportStatus = 3;
                    }
                    else
                    {//INT-213
                        ReportStatus = System.Convert.ToByte(BALGeneral.CheckReportStatus(Response_data, ProductCode, false, ObjReport.pkOrderDetailId, ObjReport.fkOrderId, true).ToString());
                    }

                    #region Get Verification Fee From Vendor Response
                    if ((ProductCode == "EDV" || ProductCode == "EMV" || ProductCode == "PRV" || ProductCode == "PLV" || ProductCode == "PRV2") && ReportStatus == 2)
                    {

                        ObjOrder.AddVerificationFee(ObjReport.pkOrderDetailId, Response_data, ProductCode);
                        //To Add the data in tblInvoice table
                        new BALInvoice().CreateInvoice(ObjReport.fkOrderId);
                    }
                    else if ((ProductCode == "WCV") && ReportStatus == 2)
                    {
                        ObjOrder.AddVerificationFee(ObjReport.pkOrderDetailId, Response_data, ProductCode);
                        if (ProductCode == "WCV")
                            WCVLog(DateTime.Now, "15.1 UpdateStatus_And_Sendemail : fkOrderId : " + ObjReport.fkOrderId.ToString() + " : " + ProductCode, Response_data.ToString());

                        //To Add the data in tblInvoice table
                        new BALInvoice().CreateInvoice(ObjReport.fkOrderId);
                    }

                    #endregion

                    if (ReportStatus == 1 || ReportStatus == 3)
                    {
                        DateTime OrderDate = ObjReport.OrderDt;
                        ObjOrder.SendPendingMailReminder(ObjReport.fkOrderId, OrderDate, Response_data, ReportStatus, ObjReport.pkOrderDetailId, ProductCode);
                    }
                    #region If the Live Runner Reports(CCR's) are cancelled By Vendor

                    if (ProductCode == "RCX" && ReportStatus == 5)
                    {
                        var OrderDetail = Dx.tblOrderDetails.Where(db => db.pkOrderDetailId == ObjReport.pkOrderDetailId).FirstOrDefault();
                        if (OrderDetail.IsLiveRunner == true)
                        {
                            #region Update IsLiveRunner To false
                            OrderDetail.IsLiveRunner = false;
                            Dx.SubmitChanges();
                            #endregion
                            ObjBALOrders.PlaceNewOrderForLiveRunnerCancelledReport(ObjReport.fkOrderId, ObjReport.pkOrderDetailId, ObjReport.ProductCode, string.Empty, string.Empty, string.Empty);
                        }
                        else
                            ReportStatus = 4;
                    }

                    #endregion
                    else
                    {
                        #region This Section will call the url inside the MVR response to Apply the Styles to the XML.This code from view report will now be removed.

                        if (ReportStatus == 2 && ProductCode == "MVR")
                        {
                            // GetMvrResponse(Response_data);
                            //Do nothing //MVR_Non_Instant
                        }

                        #endregion
                        if (ObjReport.pkOrderResponseId == null)
                        {
                            tblOrderResponseData ObjResponse = new tblOrderResponseData();
                            //ObjResponse.pkOrderResponseId = Guid.NewGuid();
                            ObjResponse.fkOrderDetailId = ObjReport.pkOrderDetailId;
                            bool isFileExists = false;
                            string responseString = BALGeneral.GetRCXResponsefromFile(ObjReport.pkOrderDetailId, out isFileExists);
                            if (isFileExists)
                            {
                                ObjResponse.ResponseData = responseString;
                                ReportStatus = 2;
                            }
                            else
                            {
                                ObjResponse.ResponseData = Response_data;
                            }
                            ObjResponse.ErrorResponseData = "Entry from pkOrderResponseId == null section";
                            ObjResponse.EmergeReviewNote = string.Empty;
                            Dx.tblOrderResponseDatas.InsertOnSubmit(ObjResponse);
                            Dx.SubmitChanges();
                            RenameRCXFile(ObjReport.pkOrderDetailId);
                        }
                        else
                        {
                            try
                            {
                                bool updated = false;
                                if (ProductCode == "WCV")
                                    WCVLog(DateTime.Now, "15.2 UpdateResponse : fkOrderId : " + ObjReport.fkOrderId.ToString() + " : " + ProductCode, Response_data.ToString());
                                VendorServices.GeneralService.WriteLog("," + Environment.NewLine + ObjReport.OrderNo + "Before response Insert", "AllPendingOrders");
                                //INT-48 //Something is broken with MVR integration (Softech).
                                if (ProductCode == "MVR" && ReportStatus == 2)//MVR_Non_Instant
                                {
                                    XDocument ObjXDocumentQuoteBack = XDocument.Parse(Response_data);
                                    var filename = ObjXDocumentQuoteBack.Descendants("QuoteBack").FirstOrDefault();
                                    if (filename != null)
                                    {
                                        string[] strQB = Convert.ToString(filename.Value).Split('_');
                                        ObjBALOrders.UpdateOrderResponseOnUpdateStatus(Convert.ToInt32(strQB[2]), Response_data);
                                    }
                                }
                                else
                                {
                                    ObjBALOrders.UpdateOrderResponseOnUpdateStatus(ObjReport.pkOrderDetailId, Response_data);
                                }




                                VendorServices.GeneralService.WriteLog("," + Environment.NewLine + ObjReport.OrderNo + "After response Insert", "AllPendingOrders");
                                var ObjOrderDetail = Dx.tblOrderDetails.Where(db => db.pkOrderDetailId == ObjReport.pkOrderDetailId).FirstOrDefault();
                                if (ProductCode == "RCX" && ReportStatus == 2)
                                {
                                    bool IsRCXHit = ObjBALOrders.GetRCXHitstatus(Response_data);
                                    if (IsRCXHit == true)
                                    {
                                        string PCode = (from d in Dx.tblProducts
                                                        join g in Dx.tblProductPerApplications
                                                        on d.pkProductId equals g.fkProductId
                                                        where (g.pkProductApplicationId == ObjOrderDetail.fkProductApplicationId)
                                                        select d.ProductCode).FirstOrDefault().ToString();
                                        ObjBALOrders.PlaceCCR1orderForRCXHitReport(ObjReport.fkOrderId, ObjReport.pkOrderDetailId, PCode, CompanyAccountNo, OrderCompanyName, PermissiblePurpose);
                                    }
                                    if (this.RCXFilePath != "")
                                    {
                                        bool isFileExists = false;
                                        string responseString = BALGeneral.GetRCXResponsefromFile(ObjReport.pkOrderDetailId, out isFileExists);
                                        if (isFileExists)
                                        {
                                            Response_data = responseString;
                                            ReportStatus = 2;
                                            RenameRCXFile(ObjReport.pkOrderDetailId);
                                        }
                                    }
                                }
                               // var ObjOrderDetail2 = Dx.tblOrderDetails.Where(db => db.pkOrderDetailId == ObjReport.pkOrderDetailId).FirstOrDefault();
                                if (ProductCode == "NCR4" || ProductCode == "NCR1" || ProductCode == "NCR+" || ProductCode == "NCR2")
                                {
                                    fiteredResponse = ObjBALGeneral.GetFilteredXMLDuringOrderPlace(ProductCode, Response_data, ObjReport.pkOrderDetailId, DateTime.Now, true, false);
                                    using (EmergeDALDataContext Dx2 = new EmergeDALDataContext())
                                    {

                                        var ObjOrderDetailnew = Dx2.tblOrderDetails.Where(db => db.pkOrderDetailId == ObjReport.pkOrderDetailId).FirstOrDefault();
                                        if (ObjOrderDetailnew.ReviewStatus != 1)
                                        {
                                            ObjBALGeneral.CheckIsNCRHit(ObjReport.fkOrderId, ObjReport.pkOrderDetailId, ProductCode, fiteredResponse, false);
                                        }
                                    }
                                }
                                if (ProductCode == "WCV")
                                    WCVLog(DateTime.Now, "15.3 UpdateResponse : fkOrderId : " + ObjReport.fkOrderId.ToString() + " : " + ProductCode + " : " + updated.ToString(), Response_data.ToString());


                            }
                            catch
                            {
                            }

                        }
                        if (ProductCode == "MVR" && ReportStatus == 2)//MVR_Non_Instant
                        {
                            XDocument ObjXDocumentQuoteBack1 = XDocument.Parse(Response_data);
                            var filename1 = ObjXDocumentQuoteBack1.Descendants("QuoteBack").FirstOrDefault();
                            if (filename1 != null)
                            {
                                string[] strQB = Convert.ToString(filename1.Value).Split('_');
                                ObjBALOrders.UpdateReportStatus(Convert.ToInt32(strQB[0]), Convert.ToInt32(strQB[2]), ReportStatus);
                            }
                        }
                        else
                        {
                            //  Update Report Status 
                            if (ProductCode == "WCV")
                                WCVLog(DateTime.Now, "15.4 UpdateReportStatus : fkOrderId : " + ObjReport.fkOrderId.ToString() + " : " + ProductCode, Response_data.ToString());

                            ObjBALOrders.UpdateReportStatus(ObjReport.fkOrderId, ObjReport.pkOrderDetailId, ReportStatus);
                        }
                        if (ReportStatus == 2)
                        {
                            sentMailLog = "#region Send Auto Mail For Report Review for OrderNo " + Convert.ToString(ObjReport.fkOrderId);
                            BALGeneral.WriteLogForDeletion(sentMailLog, fileName);

                            #region Send Auto Mail For Report Review
                            ObjOrder.SendAutoMailForReview(ObjReport.fkOrderId, ObjReport.pkOrderDetailId, Response_data, true);
                            #endregion

                            sentMailLog = "if Reportstatus = 2 for OrderId " + Convert.ToString(ObjReport.fkOrderId);
                            BALGeneral.WriteLogForDeletion(sentMailLog, fileName);

                            //string Linkis = GetViewReportLink(ObjReport);

                            //  string WebsiteLogo = "https://www.usaintel.com/emerge/Resources/Upload/Images/" + BALGeneral.GetGeneralSettings().WebSiteLogo;
                            string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + BALGeneral.GetGeneralSettings().WebSiteLogo;


                            int pkTemplateId = 5;
                            var emailContent = ObjBALGeneral.GetEmailTemplate(new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852"), pkTemplateId);//"Complete Report"

                            string emailText = emailContent.TemplateContent;
                            BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                            #region For New Style SystemTemplate
                            string Bookmarkfile = string.Empty;
                            string bookmarkfilepath = string.Empty;

                            Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                            Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                            Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                            emailText = Bookmarkfile;
                            #endregion

                            #region New way Bookmark

                            CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
                            TextInfo textInfo = cultureInfo.TextInfo;
                            string firstname = ObjReport.SearchedFirstName != null ? textInfo.ToTitleCase(ObjReport.SearchedFirstName) : string.Empty;
                            string lastname = ObjReport.SearchedLastName != null ? textInfo.ToTitleCase(ObjReport.SearchedLastName) : string.Empty;

                            Bookmarks objBookmark = new Bookmarks();
                            string MessageBody = string.Empty;
                            objBookmark.EmailContent = emailText;
                            objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                            objBookmark.ApplicantName = "Emerge";
                            // objBookmark.ViewReportLink = Linkis;
                            objBookmark.ViewReportLink = string.Empty;
                            objBookmark.ProductName = ObjReport.ProductDisplayName;
                            objBookmark.ApplicantName = firstname + " " + lastname;
                            objBookmark.ReportCode = ObjReport.ProductCode;
                            MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Guid.Empty);

                            #endregion

                            //Send Email
                            Subject = emailContent.TemplateSubject.Replace("%ApplicantName%", firstname + " " + lastname); //"Your Report on " + ObjReport.SearchedFirstName + " " + ObjReport.SearchedLastName + " is Ready on Emerge";
                            string Rolename = "";
                            Rolename = BLLMembership.GetUserRoleByUsername(ObjReport.Email);

                            string UserEmails = string.Empty;
                            if (IsSetTrueMailByUser(ObjReport.fkOrderId))
                            {
                                if (!string.IsNullOrEmpty(ObjReport.Email))
                                {
                                    UserEmails = ObjBALGeneral.GetUserOtherMailIds(ObjReport.Email);
                                }
                                sentMailLog = "SendEmail(ObjReport.Email, UserEmails, MessageBody, Subject) for OrderNo " + Convert.ToString(ObjReport.fkOrderId);
                                BALGeneral.WriteLogForDeletion(sentMailLog, fileName);
                                SendEmail(ObjReport.Email, UserEmails, MessageBody, Subject);
                            }
                            sentMailLog = "if Reportstatus = 2 but not sent any mail from the section for OrderId " + Convert.ToString(ObjReport.fkOrderId);
                            BALGeneral.WriteLogForDeletion(sentMailLog, fileName);

                            //To Add the data in tblInvoice table
                            new BALInvoice().CreateInvoice(ObjReport.fkOrderId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\nUpdateStatus_And_Sendemail gave error at:" + DateTime.Now.ToString() + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace.ToString(), "WindowsService_ResendPendingAllOrders");
                WCVLog(DateTime.Now, "15.6 UpdateStatus_And_Sendemail : error : " + ObjReport.fkOrderId.ToString(), ex.Message.ToString());

            }
            sentMailLog = "UpdateStatus_And_Sendemail method call ended : " + Convert.ToString(ObjReport.OrderNo);
            BALGeneral.WriteLogForDeletion(sentMailLog, fileName);
        }

        private void RenameRCXFile(int pkOrderDetailId)
        {
            try
            {
                if (this.RCXRequestPath != "")
                {
                    string Directory = this.RCXRequestPath;

                    var directory = new DirectoryInfo(Directory);

                    var OldFile = directory.GetFiles().Where(d => d.Name == pkOrderDetailId + ".txt");
                    string OldFileName = this.RCXRequestPath + OldFile.ElementAt(0).Name;
                    string NewFileName = OldFileName.Replace(".txt", "") + "_C.txt";
                    if (!File.Exists(NewFileName))
                    {
                        File.Move(OldFileName, NewFileName);
                    }
                }

            }
            catch
            {

            }
        }

        /// <summary>
        /// Function to Cut the Unwanted Code from XML
        /// </summary>
        /// <param name="XmlResponse"></param>
        /// <returns></returns>
        public static string GetRequiredXml_ForRapidCourt(string XmlResponse)
        {
            string XmlResponse_Local = XmlResponse;
            XmlResponse_Local = XmlResponse_Local.Replace("</BackgroundReports>", "");
            int xmlstartindex = XmlResponse_Local.IndexOf("<BackgroundReportPackage>");
            XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
            return XmlResponse_Local;
        }

        private bool IsSetTrueMailByUser(int? fkOrderId)
        {
            bool isSend = false;
            int? fkCompanyUserId = 0;
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                fkCompanyUserId = dx.tblOrders.Where(d => d.pkOrderId == fkOrderId).Select(d => d.fkCompanyUserId).FirstOrDefault();
                ///
                /// this condition is set if user off email notification from preferences 
                ///
                if (dx.tblEmailNotificationAlerts.Where(d => d.fkCompanyUserId == fkCompanyUserId).Count() > 0)
                {
                    isSend = dx.tblEmailNotificationAlerts.Where(d => d.fkCompanyUserId == fkCompanyUserId && d.fkTemplateId == 5).Select(d => d.IsSendEmail).FirstOrDefault();
                }
                else
                {
                    isSend = true;
                }
            }
            return isSend;
        }


        /// <summary>
        /// Get the Formatted MVR Reponse.
        /// </summary>
        /// <param name="Response_Data"></param>
        private string GetMvrResponse(string Response_Data)
        {
            string ResponseData = Response_Data.Trim();
            bool Objdynamic = GetResponse(ResponseData);
            string UriResponse = "";
            if (Objdynamic == true)
            {
                string Response = (ResponseData == null ? "" : ResponseData.ToString());

                XmlDocument ObjXmlDocument = new XmlDocument();

                ObjXmlDocument.LoadXml(Response);

                XmlNode ObjXmlNode = ObjXmlDocument.DocumentElement;

                string URL_string = ObjXmlNode.SelectSingleNode("ReportLink").ChildNodes[0].Value;

                string[] str_Uri_Content = URL_string.Replace("_$_", "&").Split('?');

                string Uri_string = str_Uri_Content[0].Replace("mvr.", "ptp.");

                string Content_string = "";

                if (str_Uri_Content[1].Contains("&amp;"))
                {
                    Content_string = str_Uri_Content[1].Replace("&amp;", "&");
                }
                else
                {
                    Content_string = str_Uri_Content[1];
                }

                UriResponse = SendRequest(Uri_string, Content_string);

                //int LengthOfResponse = UriResponse.Length;

                int FirstHalfIndex = UriResponse.IndexOf("** END OF RECORD **");

                UriResponse = UriResponse.Substring(0, FirstHalfIndex);

                int startindex = UriResponse.IndexOf("<table border=\"1\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">", 300);

                UriResponse = UriResponse.Substring(startindex);

                UriResponse = "<pre><div style=\"font-family: Arial; font-size: 12px;\">" + UriResponse + "</div></pre><br /><div style=\"font-size: 16px;\"><center>** END OF RECORD ***</center></div>";

            }
            return UriResponse;
        }

        /// <summary>
        /// Validate the XML
        /// </summary>
        /// <param name="ResponseData"></param>
        /// <returns></returns>
        private bool GetResponse(string ResponseData)
        {
            XmlDocument ObjXmlDocument = new XmlDocument();
            try
            {
                ObjXmlDocument.LoadXml(ResponseData);
                return true;
            }
            catch //(Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Return the Formatted MVR Response from the vendor
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public static string SendRequest(string uri, string content)
        {
            if (uri == "")
            {
                return "";
            }
            string Response = "";
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(uri);
            request.Timeout = 65000;
            request.ProtocolVersion = HttpVersion.Version11;
            request.Method = WebRequestMethods.Http.Post;
            request.ContentLength = content.Length;
            request.ContentType = "application/x-www-form-urlencoded";
            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(content);
            writer.Close();
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            Response = reader.ReadToEnd();
            response.Close();
            return Response;
        }

        private string CreateEmailBody(Proc_Get_PendingReportsResult ObjReport)
        {
            //string WebsiteLogo = "https://www.usaintel.com/emerge/Resources/Images/emergePoweredBy.png";

            //string Linkis = GetViewReportLink(ObjReport);

            //  string WebsiteLogo = "https://www.intelifi.com/emerge/Resources/Upload/Images/" + BALGeneral.GetGeneralSettings().WebSiteLogo;
            string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + BALGeneral.GetGeneralSettings().WebSiteLogo;


            BALGeneral ObjBALGeneral = new BALGeneral();
            int pkTemplateId = 5;
            var emailContent = ObjBALGeneral.GetEmailTemplate(new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852"), pkTemplateId);//"Pending Report"
            BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
            string emailText = emailContent.TemplateContent;
            #region For New Style SystemTemplate
            string Bookmarkfile = string.Empty;
            string bookmarkfilepath = string.Empty;

            Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
            Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
            Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
            emailText = Bookmarkfile;
            #endregion

            #region New way Bookmark


            Bookmarks objBookmark = new Bookmarks();
            string MessageBody = string.Empty;
            objBookmark.EmailContent = emailText;
            objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
            objBookmark.ApplicantName = "Emerge";
            // objBookmark.ViewReportLink = Linkis;
            objBookmark.ViewReportLink = string.Empty;
            objBookmark.ProductName = ObjReport.ProductDisplayName;
            objBookmark.ApplicantName = ObjReport.SearchedFirstName + " " + ObjReport.SearchedLastName;
            objBookmark.ReportCode = ObjReport.ProductCode;
            MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Guid.Empty);

            #endregion
            return MessageBody;
        }

        private string GetViewReportLink(Proc_Get_PendingReportsResult ObjReport)
        {
            string ViewReportLink = "";
            string RoleName = BLLMembership.GetUserRoleByUsername(ObjReport.UserName);
            //#19
            string fkOrderidEncrypt = EncryptDecrypts.Encrypt(ObjReport.fkOrderId.ToString());

            if (RoleName.Contains("branch"))
            {

                ViewReportLink = "<a href='https://emerge.intelifi.com/Corporate/ViewReports?_OId='" + fkOrderidEncrypt + "'>Click here</a>";
            }
            if (RoleName.Contains("basic"))
            {
                ViewReportLink = "<a href='https://emerge.intelifi.com/Corporate/ViewReports?_OId='" + fkOrderidEncrypt + "'>Click here</a>";
            }
            if (RoleName.Contains("admin"))
            {
                ViewReportLink = "<a href='https://emerge.intelifi.com/Corporate/ViewReports?_OId='" + fkOrderidEncrypt + "'>Click here</a>";
            }
            if (RoleName.Contains("corporate"))
            {
                ViewReportLink = "<a href='https://emerge.intelifi.com/Corporate/ViewReports?_OId='" + fkOrderidEncrypt + "'>Click here</a>";
            }
            return ViewReportLink;
        }

        /// <summary>
        /// Function To fetch XML Response From Vendor.
        /// </summary>
        /// <param name="p"></param>
        /// <param name="ObjtblVendor_List"></param>
        /// <returns></returns>
        private string GetXMLResponse_ForPendingReports(Proc_Get_PendingReportsResult ObjReport)
        {
            //isncr1toncr2 = false;//INT-213
            List<PendingLog> ObjPendingLog = new List<PendingLog>();

            if (ObjReport.RequestData == null)
            {
                return "";
            }
            BALOrders ObjOrders = new BALOrders();
            ObjBALProducts = new BALProducts();
            int? pkOrderId = ObjReport.fkOrderId;
            int OrderDetailId = ObjReport.pkOrderDetailId;
            ObjProc_GetAllProductsWithVendorResult = ObjBALProducts.GetAllProductsWithVendor();
            string ProductCode = ObjReport.ProductCode;
            if (ObjReport.IsLiveRunner == true)
            {
                ProductCode = "RCX";
            }
            string Vendor_Request = "";
            string Vendor_Response = ObjReport.ResponseData;

            string[] String_Separator = new string[] { "$#$#" };

            if (ObjReport.RequestData.Contains("$#$#"))
            {
                string[] Request_array = ObjReport.RequestData.Split(String_Separator, StringSplitOptions.RemoveEmptyEntries);
                Vendor_Request = Request_array.Last();
            }
            else
            {
                Vendor_Request = ObjReport.RequestData;
            }

            if (ProductCode == "SCR")//INT-254
            {
                if (Vendor_Request.Contains("BackgroundCheck"))
                {
                    ProductCode = "RCX";
                    using (EmergeDALDataContext dxLiveRunner = new EmergeDALDataContext())
                    {
                        tblOrderDetail orderDetailObj = dxLiveRunner.tblOrderDetails.Where(x => x.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                        if (orderDetailObj != null)
                        {
                            if (orderDetailObj.IsLiveRunner == false)
                            {
                                orderDetailObj.IsLiveRunner = true;
                                dxLiveRunner.SubmitChanges();
                            }
                        }
                    }
                }
            }
            string RefSecureKey = ObjReport.RefSecureKey;
            ObjBALGeneral = new BALGeneral();
           // tblReportErrorLog ObjtblReportErrorLog;

            string Response = "", Error = "";
            Dictionary<string, string> Response_Dic = new Dictionary<string, string>();

            try
            {
                if (ProductCode == "TDR" || ProductCode == "OFAC" || ProductCode == "BR" || ProductCode == "NCR4" || ProductCode == "MVS" ||
                ProductCode == "SSR" || ProductCode == "ER" || ProductCode == "RPL" || ProductCode == "RAL" || ProductCode == "PS2" || ProductCode == "ECR" || ProductCode == "EEI" || ProductCode == "PL")
                {
                    ObjMicrobiltService = new MicrobiltService();
                    try
                    {
                        Response_Dic = ObjMicrobiltService.RunReport_ForPendingReports(PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, Vendor_Response, RefSecureKey, pkOrderId.ToString(), OrderDetailId.ToString()));
                        Response = Response_Dic["response"];
                        Error = Response_Dic["error"];
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        ObjMicrobiltService = null;
                    }
                }
                if (ProductCode == "NCR1" || ProductCode == "PS" || ProductCode == "RCX" || ProductCode == "FCR" || ProductCode == "NCR+")
                {
                    ObjRapidCourtService = new RapidCourtService();
                    IDSFactProductService objIDSFactProductService = new IDSFactProductService();
                   
                    tblOrderDetail ObjtblOrderDetail = new tblOrderDetail();
                    try
                    {
                        if (ProductCode == "NCR1" || ProductCode == "PS")
                        {
                            using (EmergeDALDataContext dx = new EmergeDALDataContext())
                            {
                                ObjtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == ObjReport.pkOrderDetailId).FirstOrDefault();
                                if (ObjtblOrderDetail != null)
                                {
                                    ObjtblOrderDetail.MaximumHitsAllowed = 20;
                                    dx.SubmitChanges();
                                }
                            }
                        }
                        //Changed By Himesh Kumar
                        if (ProductCode != "PS")
                        {
                            Response_Dic = ObjRapidCourtService.RunReport_ForPendingReports(PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, Vendor_Response, RefSecureKey, pkOrderId.ToString(), OrderDetailId.ToString()));
                        }
                        else if(ProductCode=="PS")
                        {
                            Response_Dic = objIDSFactProductService.RunReport_ForPendingReports(PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, Vendor_Response, RefSecureKey, pkOrderId.ToString(), OrderDetailId.ToString()));
                        }
                        Response = Response_Dic["response"];
                        Error = Response_Dic["error"];
                        if (ProductCode == "NCR1" || ProductCode == "PS")
                        {

                            using (EmergeDALDataContext dx = new EmergeDALDataContext())
                            {
                                bool gSetting = dx.tblGeneralSettings.Select(x => x.IsNCR1Backupfeature).FirstOrDefault();//INT213
                                if (gSetting)
                                {
                                    if (Response == "" || Error.ToLower().Contains("the operation has timed out"))
                                    {
                                        try
                                        {
                                            int RetryHitCount = 0;

                                            ObjtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == ObjReport.pkOrderDetailId).FirstOrDefault();
                                            if (ObjtblOrderDetail != null)
                                            {
                                                RetryHitCount = ObjtblOrderDetail.MaximumHitsAllowed;
                                                if (RetryHitCount == 20)
                                                {
                                                    ObjtblOrderDetail.MaximumHitsAllowed = RetryHitCount++;
                                                    ObjtblOrderDetail.ErrorLog = Error;
                                                    dx.SubmitChanges();
                                                    ObjOrders.UpdateReportStatus(ObjReport.fkOrderId, ObjReport.pkOrderDetailId, Convert.ToByte("3"));
                                                    Response = ObjOrders.PlacePS2NCR2orderForPSNCR1Failure(ObjReport.fkOrderId, ObjReport.pkOrderDetailId, ProductCode, string.Empty, string.Empty, string.Empty); // Ticket #909: New feature for NCR1 and PS
                                                    if (ProductCode == "NCR1")
                                                    {
                                                        ObjReport.ProductCode = "NCR2";   // here we changes product code and product application id INT-213
                                                        ObjReport.fkProductApplicationId = 11;
                                                        ObjReport.ProductDisplayName = "National Criminal Backup";
                                                    }
                                                    else if (ProductCode == "PS")
                                                    {
                                                        ObjReport.ProductCode = "PS2"; // here we changes product code and product application id INT-213
                                                        ObjReport.fkProductApplicationId = 46;
                                                        ObjReport.ProductDisplayName = "People Search Backup";
                                                    }
                                                    #region auto emerge  review whene NCR1 to NCR2
                                                    //INT-213 here report automatic go in auto emerge review
                                                    if (ObjReport.ProductCode == "NCR2")
                                                    {
                                                        Proc_GetReportInfoForAutoReviewMailResult ReportInfoforNCR1Backup = dx.GetReportInfoForAutoReviewMail(ObjReport.fkOrderId, ObjReport.pkOrderDetailId).FirstOrDefault();
                                                        StringBuilder Comments1 = new StringBuilder();
                                                        string MessageBody1 = "";
                                                        int PkTemplateId1 = 0;
                                                        int Updatestatus1 = 0;
                                                        //int Mailsentstatus1 = 0;
                                                        string emailText1 = "";
                                                        BALEmergeReview ObjReview = new BALEmergeReview();
                                                        XMLFilteration XMLFilteration = new XMLFilteration();
                                                        bool Is6MonthOld = false;
                                                        Is6MonthOld = ObjBALGeneral.Is6MonthOld(ObjReport.fkOrderId);
                                                        Updatestatus1 = ObjReview.UpdateReportReviewStatus(OrderDetailId, 1, string.Empty, Is6MonthOld);
                                                        if (Updatestatus1 == 1)
                                                        {
                                                            Comments1.Append(" <table width=\"97%\"><tr><td>Report ID : </td><td>" + ReportInfoforNCR1Backup.OrderNo + "</td>"
                                                                + "</tr> <tr><td>Report Name : </td><td>" + ReportInfoforNCR1Backup.ProductDisplayName + "</td></tr>"
                                                       + "<tr><td>User : </td><td>" + ReportInfoforNCR1Backup.LastName + " " + ReportInfoforNCR1Backup.FirstName + " (<b>UserName : </b>" + ReportInfoforNCR1Backup.UserName + ") " + "</td></tr>"
                                                       + " <tr><td valign=\"top\"  style=\"width:150px;\">Additional Comments :</td><td> Auto request for " + ReportInfoforNCR1Backup.ProductCode + " report review.</td></tr>  </table>");
                                                            string BoxHeaderText = string.Empty;


                                                            Guid ApplicationId1 = new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852");
                                                            string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/emerge-logo--no-shadows-(small).gif";
                                                            PkTemplateId1 = 27;
                                                            var emailContent = ObjBALGeneral.GetEmailTemplate(ApplicationId1, PkTemplateId1);// "Emerge Suggestions"
                                                            emailText1 = emailContent.TemplateContent;
                                                            CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
                                                            TextInfo textInfo = cultureInfo.TextInfo;
                                                            string Searchedfirstname = ReportInfoforNCR1Backup.SearchedFirstName != null ? textInfo.ToTitleCase(ReportInfoforNCR1Backup.SearchedFirstName) : string.Empty;
                                                            string Searchedlastname = ReportInfoforNCR1Backup.SearchedLastName != null ? textInfo.ToTitleCase(ReportInfoforNCR1Backup.SearchedLastName) : string.Empty;

                                                            #region New way Bookmark

                                                            BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                                                            Bookmarks objBookmark = new Bookmarks();
                                                            objBookmark.EmailContent = emailText1;

                                                            #region For New Style SystemTemplate
                                                            string Bookmarkfile = string.Empty;
                                                            string bookmarkfilepath = string.Empty;

                                                            Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                                                            Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                                                            Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                                                            emailText1 = Bookmarkfile;
                                                            #endregion

                                                            BoxHeaderText = emailContent.TemplateSubject.Replace("%ApplicantName%", Searchedlastname + " " + Searchedfirstname).Replace("%ReportCode%", ObjReport.ProductCode);
                                                            objBookmark.Subject = BoxHeaderText;
                                                            objBookmark.ApplicantName = Searchedlastname + " " + Searchedfirstname;
                                                            objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                                                            objBookmark.UserCompany = "";
                                                            objBookmark.UserEmail = ReportInfoforNCR1Backup.LastName + " " + ReportInfoforNCR1Backup.FirstName + " (<b>UserName : </b>" + ReportInfoforNCR1Backup.UserName + ") ";
                                                            objBookmark.EmergeReviewComment = Comments1.ToString();
                                                            MessageBody1 = objTemplateBookmarks.ReplaceBookmarks(objBookmark, ReportInfoforNCR1Backup.UserId);
                                                            #endregion
                                                            string successAdmin = string.Empty;
                                                            //int reportstatus1 = dx.tblOrderDetails.Where(db => db.pkOrderDetailId ==ObjReport.pkOrderDetailId).FirstOrDefault().ReportStatus;
                                                            //if (reportstatus1 == 1)
                                                            //{

                                                            successAdmin = BALEmergeReview.SendMail(ConfigurationManager.AppSettings["ReportReviewMail"].ToString(), ReportInfoforNCR1Backup.UserName.ToLower(), string.Empty, ConfigurationManager.AppSettings["ReportReviewFromAddress"].ToString(), MessageBody1, BoxHeaderText);
                                                            if (successAdmin == "Success")
                                                            {
                                                               // Mailsentstatus1 = 1;
                                                                string SucessMsg = "Email Successfully Sent for Order No:-" + ReportInfoforNCR1Backup.OrderNo;
                                                                ObjReview.WriteReviewStatusInTextFile(SucessMsg, ConfigurationManager.AppSettings["ReportReviewMail"].ToString());
                                                            }
                                                            else
                                                            {
                                                                //Mailsentstatus1 = 0;
                                                                string UnSucessMsg = "Email Sending Failed for Order No:-" + ReportInfoforNCR1Backup.OrderNo;
                                                                UnSucessMsg = UnSucessMsg + " Error:" + successAdmin;
                                                                ObjReview.WriteReviewStatusInTextFile(UnSucessMsg, ConfigurationManager.AppSettings["ReportReviewMail"].ToString());
                                                            }

                                                            //}
                                                        }

                                                    }
                                                    //------------------------------
                                                    #endregion


                                                }
                                            }


                                        }
                                        catch
                                        {

                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        using (EmergeDALDataContext dx = new EmergeDALDataContext())
                        {
                            try
                            {
                                ObjtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                                string ErrorConCat = string.Empty;
                                if (ObjtblOrderDetail != null)
                                {
                                    ErrorConCat = ObjtblOrderDetail.ErrorLog.ToString() + " ==>  (" + DateTime.Now.ToString() + ") ==> Error from catch 2nd try ==> " + ex.ToString();
                                    ObjtblOrderDetail.ErrorLog = ErrorConCat;
                                    dx.SubmitChanges();
                                }
                            }
                            catch
                            {

                            }
                        }
                    }
                    finally
                    {
                        ObjRapidCourtService = null;
                        ObjtblOrderDetail = null;
                    }
                }
                if (ProductCode == "CCR2")
                {
                    ObjBackgroundChecksService = new BackgroundChecksService();
                    try
                    {
                        Response_Dic = ObjBackgroundChecksService.RunReport_ForPendingReports(PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, Vendor_Response, RefSecureKey, pkOrderId.ToString(), OrderDetailId.ToString()));
                        Response = Response_Dic["response"];
                        Error = Response_Dic["error"];
                        if (ProductCode == "SCR")
                        {
                            /*
                             * Service should be call interval of 15 minutes. otherwise use Time.Sleep()
                             */
                            if (Response == "" && Error.ToLower().Contains("the operation has timed out"))//This condition is applied due to Ticket #639: Some SCR report is not completing
                            {
                                try
                                {
                                    int RetryHitCount = 0;
                                    using (EmergeDALDataContext dx = new EmergeDALDataContext())
                                    {
                                        tblOrderDetail ObjtblOrderDetail = new tblOrderDetail();
                                        ObjtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == ObjReport.pkOrderDetailId).FirstOrDefault();
                                        if (ObjtblOrderDetail != null)
                                        {
                                            RetryHitCount = ObjtblOrderDetail.MaximumHitsAllowed;
                                            if (RetryHitCount < 4)
                                            {
                                                ObjtblOrderDetail.MaximumHitsAllowed = RetryHitCount++;
                                                ObjtblOrderDetail.ErrorLog = Error;
                                                dx.SubmitChanges();
                                            }
                                            else
                                            {
                                                ObjOrders.UpdateReportStatus(ObjReport.fkOrderId, ObjReport.pkOrderDetailId, Convert.ToByte("3"));
                                            }
                                        }
                                    }
                                }
                                catch
                                {
                                }
                            }
                            else if (Error != "")
                            {
                                using (EmergeDALDataContext dx = new EmergeDALDataContext())
                                {
                                    tblOrderDetail ObjtblOrderDetail = new tblOrderDetail();
                                    ObjtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == ObjReport.pkOrderDetailId).FirstOrDefault();
                                    if (ObjtblOrderDetail != null)
                                    {
                                        ObjtblOrderDetail.ErrorLog = Error;
                                        dx.SubmitChanges();
                                    }
                                }
                                ObjOrders.UpdateReportStatus(ObjReport.fkOrderId, ObjReport.pkOrderDetailId, Convert.ToByte("3"));
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        ObjBackgroundChecksService = null;
                    }
                }

                if (ProductCode == "MVR" || ProductCode == "CDLIS")//MVR_Non_Instant
                {
                    ObjSoftechService = new SoftechService();
                    BALOrders ObjBALOrders = new BALOrders();
                    try
                    {
                        #region INT 263  comment code for running report using softech on 17 sept 2015
                        Response_Dic = ObjSoftechService.RunReport_ForPendingReports(PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, Vendor_Response, RefSecureKey, pkOrderId.ToString(), OrderDetailId.ToString()));
                        #endregion
                        Response = Response_Dic["response"];
                        Error = Response_Dic["error"];

                        if (Response_Dic["ispending"] != "")
                        {
                            ObjBALOrders.UpdateReportStatus(pkOrderId, ObjReport.pkOrderDetailId, System.Convert.ToByte("1"));
                        }

                        if (Response.Contains('^'))//MVR_Non_Instant
                        {
                            string[] RequestAndResponse = Response.Split('^');
                            XDocument ObjXDocumentQuoteBack = XDocument.Parse(RequestAndResponse[0]);
                            var filename = ObjXDocumentQuoteBack.Descendants("QuoteBack").FirstOrDefault();
                            if (filename != null)
                            {
                                string[] strQB = Convert.ToString(filename.Value).Split('_');
                                ObjBALOrders.UpdateRefSecureKey(Convert.ToInt32(strQB[2]), RequestAndResponse[1]);
                            }
                            Response = RequestAndResponse[0];
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        ObjSoftechService = null;
                    }
                }

                if (ProductCode == "NCR2" || ProductCode == "CCR1" || ProductCode == "SCR")
                {
                    ObjXpediteService = new XpediteService();
                    try
                    {
                        Response_Dic = ObjXpediteService.RunReport_ForPendingReports(PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, Vendor_Response, RefSecureKey, pkOrderId.ToString(), OrderDetailId.ToString()));
                        Response = Response_Dic["response"];
                        Error = Response_Dic["error"];
                        if (ProductCode == "CCR1" || ProductCode == "SCR")
                        {
                            if (Response == "" && Error.ToLower().Contains("the operation has timed out"))
                            {
                                try
                                {
                                    int RetryHitCount = 0;
                                    using (EmergeDALDataContext dx = new EmergeDALDataContext())
                                    {
                                        tblOrderDetail ObjtblOrderDetail = new tblOrderDetail();
                                        ObjtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == ObjReport.pkOrderDetailId).FirstOrDefault();
                                        if (ObjtblOrderDetail != null)
                                        {
                                            RetryHitCount = ObjtblOrderDetail.MaximumHitsAllowed;
                                            if (RetryHitCount < 4)//This condition is mentioned in ticket #639 (3 times retry)
                                            {
                                                ObjtblOrderDetail.MaximumHitsAllowed = RetryHitCount++;
                                                dx.SubmitChanges();
                                            }
                                            else
                                            {
                                                new BALGeneral().checkOrderPeriod(pkOrderId, OrderDetailId, ProductCode, Error);
                                            }
                                        }
                                    }
                                }
                                catch
                                {
                                }
                            }

                            else if (Response == "" && Error.ToLower().Contains("server was unable to process request"))
                            {
                                //INT-330
                                //As per greg, he needs a alert, when any error occurred in Xpedite service then a mail will shot to support@intelifi.com
                                //  Code to Send an email to Test
                                //string mailSubject = Convert.ToString(ConfigurationManager.AppSettings["CCR1ErrorMailSubject"]);
                                //int SendingStatus = -1;
                                //MomentumInfotech.EmailProvider.MailProvider ObjMailProvider = new MomentumInfotech.EmailProvider.MailProvider();
                                //ObjMailProvider.MailTo.Add(Convert.ToString(ConfigurationManager.AppSettings["CCR1ErrorMailTo"]));
                                //ObjMailProvider.MailFrom = Convert.ToString(ConfigurationManager.AppSettings["CCR1ErrorMailFrom"]);
                                //ObjMailProvider.Subject = mailSubject.Replace("%OrderNo%", new BALOrders().GetOrderNoformail(pkOrderId));
                                //ObjMailProvider.Message = Response;
                                //ObjMailProvider.SendMail(out SendingStatus);                                  
                            }
                            else if (Error != "")
                            {
                                using (EmergeDALDataContext dx = new EmergeDALDataContext())
                                {
                                    tblOrderDetail ObjtblOrderDetail = new tblOrderDetail();
                                    ObjtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == ObjReport.pkOrderDetailId).FirstOrDefault();
                                    if (ObjtblOrderDetail != null)
                                    {
                                        ObjtblOrderDetail.ErrorLog = Error;
                                        dx.SubmitChanges();
                                    }
                                }
                                new BALGeneral().checkOrderPeriod(pkOrderId, OrderDetailId, ProductCode, Error);
                            }
                        }

                        if (ProductCode == "NCR2")
                        {
                            if (Response == "" && Error.ToLower().Contains("the operation has timed out"))
                            {
                                ObjOrders.UpdateReportStatus(ObjReport.fkOrderId, ObjReport.pkOrderDetailId, Convert.ToByte("3"));
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        ObjXpediteService = null;
                    }
                }
                if (Error != "")
                {
                    ObjPendingLog.Add(new PendingLog { Error = Error, pkOrderId = ObjReport.fkOrderId, RuningRime = DateTime.Now });
                    BALGeneral.LogError(ObjReport.pkOrderDetailId, Error);
                }


            }
            catch (Exception ex)
            {
                BALGeneral.LogError(ObjReport.pkOrderDetailId, ex.Message.ToString());
            }
            finally
            {
                ObjBALGeneral = null;
               // ObjtblReportErrorLog = null;
                WritePendingLog(ObjPendingLog);

            }

            return Response;
        }

        /// <summary>
        /// Function To Pass the Parameters To the General Class Object.
        /// </summary>
        /// <param name="ObjProc_Get_OrderSearchedDataResult"></param>
        /// <returns></returns>
        public GeneralService PassParams(Proc_GetAllProductsWithVendorResult ObjtblVendor, string Vendor_Request, string ProductCode, string Response, string RefSecureKey, string pkOrderId, string pkOrderDetailId)
        {
            ObjGeneralService = new GeneralService();

            string AdditionalParam = "";

            if (ObjtblVendor.AdditionalParams != null)
            {
                AdditionalParam = ObjtblVendor.AdditionalParams.ToString();
            }
            ObjGeneralService.Response = Response;
            ObjGeneralService.ProductCode = ProductCode;
            ObjGeneralService.Request = Vendor_Request;
            ObjGeneralService.AdditionalParams = AdditionalParam;
            ObjGeneralService.ServicePassword = ObjtblVendor.ServicePassword;
            ObjGeneralService.ServiceUrl = ObjtblVendor.ServiceUrl;
            ObjGeneralService.ServiceUserId = ObjtblVendor.ServiceUserId;
            ObjGeneralService.RefSecureKey = RefSecureKey;
            ObjGeneralService.OrderId = pkOrderId;
            ObjGeneralService.OrderDetailId = pkOrderDetailId;

            return ObjGeneralService;
        }

        /// <summary>
        /// Function to Send an Test Email
        /// </summary>
        /// <param name="Body"></param>
        public void SendTestEmail(string emailAddress, string EmailBody, string Subject)
        {
            //  Code to Send an email to Test
            int SendingStatus = 0;
            try
            {
                MomentumInfotech.EmailProvider.MailProvider ObjMailProvider = new MomentumInfotech.EmailProvider.MailProvider();

                List<string> BCC_List = new List<string>();
                //BCC_List.Add("momentum-support@hotmail.com");
                ObjMailProvider.Message = EmailBody;
                ObjMailProvider.MailTo.Add(emailAddress);
                ObjMailProvider.MailFrom = BALGeneral.GetGeneralSettings().SupportEmailId;
                ObjMailProvider.MailBCC = BCC_List;
                ObjMailProvider.Subject = Subject;
                ObjMailProvider.IsBodyHtml = true;
                ObjMailProvider.Priority = MailPriority.High;
                ObjMailProvider.SendMail(out SendingStatus);
            }
            catch
            {

            }
            //  Code to Send an email to Test        
        }

        /// <summary>
        /// Code To Send an Email
        /// </summary>
        /// <param name="OrderId"></param>
        /// <param name="UserHtml"></param>
        /// <param name="Username"></param>
        public bool SendEmail(string emailAddress, string EmailBody, string Subject)
        {
            int SendingStatus = 0;
            try
            {
                MomentumInfotech.EmailProvider.MailProvider ObjMailProvider = new MomentumInfotech.EmailProvider.MailProvider();

                List<string> BCC_List = new List<string>();
                BCC_List.Add(BALGeneral.GetGeneralSettings().SupportEmailId);
                //BCC_List.Add("momentum-support@hotmail.com");
                //BCC_List.Add("support@intelifi.com");
                ObjMailProvider.Message = EmailBody;
                ObjMailProvider.MailTo.Add(emailAddress);
                ObjMailProvider.MailFrom = BALGeneral.GetGeneralSettings().SupportEmailId;
                ObjMailProvider.MailBCC = BCC_List;
                ObjMailProvider.Subject = Subject;
                ObjMailProvider.IsBodyHtml = true;
                ObjMailProvider.Priority = MailPriority.High;
                ObjMailProvider.SendMail(out SendingStatus);
            }
            catch //(Exception ex2)
            {

            }

            return (SendingStatus == 1 ? true : false);
        }
        /// <summary>
        /// Email send with cc for ticket #101
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="CC"></param>
        /// <param name="EmailBody"></param>
        /// <param name="Subject"></param>
        /// <returns></returns>

        public bool SendEmail(string emailAddress, string CC, string EmailBody, string Subject)
        {
            int SendingStatus = 0;
            try
            {
                MomentumInfotech.EmailProvider.MailProvider ObjMailProvider = new MomentumInfotech.EmailProvider.MailProvider();
                List<string> BCC_List = new List<string>();
                List<string> CC_List = new List<string>();
                if (!string.IsNullOrEmpty(CC))
                {
                    CC_List.Add(CC);
                }
                BCC_List.Add(BALGeneral.GetGeneralSettings().SupportEmailId);
                //BCC_List.Add("momentum-support@hotmail.com");
                //BCC_List.Add("support@intelifi.com");
                ObjMailProvider.Message = EmailBody;
                ObjMailProvider.MailTo.Add(emailAddress);
                ObjMailProvider.MailCC = CC_List;
                ObjMailProvider.MailFrom = BALGeneral.GetGeneralSettings().SupportEmailId;
                ObjMailProvider.MailBCC = BCC_List;
                ObjMailProvider.Subject = Subject;
                ObjMailProvider.IsBodyHtml = true;
                ObjMailProvider.Priority = MailPriority.High;
                ObjMailProvider.SendMail(out SendingStatus);
            }
            catch //(Exception ex2)
            {

            }

            return (SendingStatus == 1 ? true : false);
        }

        public void SendErrorMail(string errormsg, string subject)
        {
            int SendingStatus = 0;
            try
            {
                MomentumInfotech.EmailProvider.MailProvider ObjMailProvider = new MomentumInfotech.EmailProvider.MailProvider();

                List<string> BCC_List = new List<string>();
                //BCC_List.Add(BALGeneral.GetGeneralSettings().SupportEmailId);
                //BCC_List.Add("momentum-support@hotmail.com");
                //BCC_List.Add("momentum_helpdesk@yahoo.com");
                ObjMailProvider.Message = errormsg;
                //ObjMailProvider.MailTo.Add("momentum-support@hotmail.com");
                ObjMailProvider.MailFrom = "emerge@intelifi.com";
                ObjMailProvider.MailBCC = BCC_List;
                ObjMailProvider.Subject = subject;
                ObjMailProvider.IsBodyHtml = true;
                ObjMailProvider.Priority = MailPriority.High;
                ObjMailProvider.SendMail(out SendingStatus);
            }
            catch //(Exception ex2)
            {

            }

        }

        public void WCVLog(DateTime CreatedDate, string name, string value)
        {
            try
            {
                using (EmergeDALDataContext dx = new EmergeDALDataContext())
                {
                    tblwcvLog obj = new tblwcvLog();
                    obj.createddate = CreatedDate;
                    obj.name = name;
                    obj.value = value;
                    dx.tblwcvLogs.InsertOnSubmit(obj);
                    dx.SubmitChanges();
                }
            }
            catch
            { }

        }

        public void WritePendingLog(List<PendingLog> ObjPendingLog)
        {
            try
            {
                if (this.FilePath != "")
                {
                    StreamWriter SW;
                    string savedPath = string.Empty;
                    //DateTime dt = DateTime.Now;
                    savedPath = FilePath;
                    if (!File.Exists(savedPath))
                    {
                        SW = File.CreateText(savedPath);
                    }
                    else
                    {
                        SW = File.AppendText(savedPath);
                    }

                    if (ObjPendingLog != null)
                    {
                        if (ObjPendingLog.Count > 0)
                        {
                            foreach (PendingLog item in ObjPendingLog)
                            {
                                string sLine = item.pkOrderId.ToString() + "," + item.RuningRime.ToString() + "," + item.Error;
                                SW.WriteLine(sLine);
                            }

                        }
                    }
                    SW.Close();
                }
            }
            catch
            {

            }
        }



        //for #103 Ticket

        #region Run Tiered Orders

        public void RunTieredOrders()
        {
            int? _ErrorOrderID = 0;
            ObjBALOrders = new BALOrders();

            //if (System.IO.File.Exists(System.Configuration.ConfigurationManager.AppSettings["LogPath"] + "AllTieredOrders.txt"))
            //{
            //    System.IO.File.WriteAllText(System.Configuration.ConfigurationManager.AppSettings["LogPath"] + "AllTieredOrders.txt", string.Empty);
            //}
            //else
            //{
            //    System.IO.File.Create(System.Configuration.ConfigurationManager.AppSettings["LogPath"] + "AllTieredOrders.txt");
            //}

            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                ObjTieredReports = ObjDALDataContext.Get_TieredReportOrders().ToList();
            }

            if (ObjTieredReports != null && ObjTieredReports.Count > 0)
            {
                List<Proc_Get_TieredReportOrdersResult> ObjProc_Get_TieredReportOrdersResult = ObjTieredReports.Where(data => data.IsInstantReport == false && (data.ProductCode != "EDV" && data.ProductCode != "EMV" && data.ProductCode != "PRV" && data.ProductCode != "PLV" && data.ProductCode != "PRV2" || data.ProductCode != "WCV")).ToList<Proc_Get_TieredReportOrdersResult>();//WCV enabled
                if (ObjProc_Get_TieredReportOrdersResult.Count > 0)
                {
                    //VendorServices.GeneralService.WriteLog(Environment.NewLine + "TieredReportOrders Started:", "AllTieredOrders");
                    foreach (Proc_Get_TieredReportOrdersResult ObjReport in ObjProc_Get_TieredReportOrdersResult)
                    {
                       // VendorServices.GeneralService.WriteLog("," + ObjReport.fkOrderId, "AllTieredOrders");
                        _ErrorOrderID = RunTieredReports(_ErrorOrderID, ObjReport);
                    }
                   // VendorServices.GeneralService.WriteLog(Environment.NewLine + "TieredReportOrders Completed:", "AllTieredOrders");
                }

            }
        }

        private int? RunTieredReports(int? _ErrorOrderID, Proc_Get_TieredReportOrdersResult ObjReport)
        {
            try
            {
                _ErrorOrderID = ObjReport.fkOrderId;
                //SendTestEmail(" Order ID NON FRS " + ObjReport.fkOrderId + " is Processed ");
                GetResponseAndSaveResponseForTiered(ObjReport);
            }
            catch (Exception ex)
            {
                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\nError in RunTieredReports() method- Order at:" + _ErrorOrderID.ToString() + Environment.NewLine + ex.InnerException.ToString(), "WindowsService_RunAllTieredOrders");
            }
            return _ErrorOrderID;
        }

        private void GetResponseAndSaveResponseForTiered(Proc_Get_TieredReportOrdersResult ObjReport)
        {
            try
            {
                Response_data = GetXMLResponse_ForTieredReports(ObjReport);

                UpdateStatus_And_SendemailForTiered(ObjReport, Response_data, string.Empty, string.Empty, string.Empty);
                UpdateIsProcessTiered(ObjReport.pkOrderDetailId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool IsInstantProduct(string p)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                var Result = ObjDALDataContext.tblProducts.Where(data => data.ProductCode == p && data.IsInstantReport == true).ToList();
                if (Result.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        private string GetXMLResponse_ForTieredReports(Proc_Get_TieredReportOrdersResult ObjReport)
        {
           // List<PendingLog> ObjPendingLog = new List<PendingLog>();

            if (ObjReport.RequestData == null)
            {
                return "";
            }
           // BALOrders ObjOrders = new BALOrders();
            ObjBALProducts = new BALProducts();
            int? pkOrderId = ObjReport.fkOrderId;
            int OrderDetailId = ObjReport.pkOrderDetailId;
            ObjProc_GetAllProductsWithVendorResult = ObjBALProducts.GetAllProductsWithVendor();
            string ProductCode = ObjReport.ProductCode;
            if (ObjReport.IsLiveRunner == true)
            {
                ProductCode = "RCX";
            }
            string Vendor_Request = "";
            string Vendor_Response = ObjReport.ResponseData;

            string[] String_Separator = new string[] { "$#$#" };

            if (ObjReport.RequestData.Contains("$#$#"))
            {
                string[] Request_array = ObjReport.RequestData.Split(String_Separator, StringSplitOptions.RemoveEmptyEntries);
                Vendor_Request = Request_array.Last();
            }
            else
            {
                Vendor_Request = ObjReport.RequestData;
            }


            string RefSecureKey = ObjReport.RefSecureKey;
            ObjBALGeneral = new BALGeneral();
            ObjBALOrders = new BALOrders();
            tblReportErrorLog ObjtblReportErrorLog;
            bool IsInstantReport = false;

            IsInstantReport = IsInstantProduct(ProductCode);

            string Response = "", Error = "";
            Dictionary<string, string> Response_Dic = new Dictionary<string, string>();

            try
            {
                if (ProductCode == "TDR" || ProductCode == "OFAC" || ProductCode == "BR" || ProductCode == "NCR4" || ProductCode == "MVS" ||
                ProductCode == "SSR" || ProductCode == "ER" || ProductCode == "RPL" || ProductCode == "RAL" || ProductCode == "PS2" || ProductCode == "ECR" || ProductCode == "EEI" || ProductCode == "PL")
                {
                    MicrobiltService ObjMicrobiltService = new MicrobiltService();
                    try
                    {
                        Response_Dic = ObjMicrobiltService.RunReport(ObjBALOrders.PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, pkOrderId.ToString(), OrderDetailId.ToString()));
                        Response = Response_Dic["response"];
                        Error = Response_Dic["error"];
                        if (Response == "" && IsInstantReport)
                        {
                            ObjBALOrders.UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("3"));
                        }
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        ObjMicrobiltService = null;
                    }
                }
                if (ProductCode == "NCR1" || ProductCode == "PS" || ProductCode == "RCX" || ProductCode == "FCR" || ProductCode == "NCR+")
                {
                    RapidCourtService ObjRapidCourtService = new RapidCourtService();
                    tblOrderDetail objtblOrderDetail = new tblOrderDetail();
                    try
                    {
                        using (EmergeDALDataContext dx = new EmergeDALDataContext())
                        {
                            objtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                            objtblOrderDetail.MaximumHitsAllowed = 1;
                            objtblOrderDetail.ErrorLog = Error;
                            dx.SubmitChanges();
                        }
                        //Response_Dic = ObjRapidCourtService.RunReport(ObjBALOrders.PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, pkOrderId.ToString(), OrderDetailId.ToString()));
                        Response_Dic = ObjRapidCourtService.RunReport_ForPendingReports(ObjBALOrders.PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, pkOrderId.ToString(), OrderDetailId.ToString()));
                        Response = Response_Dic["response"];
                        Error = Response_Dic["error"];

                        if (ProductCode == "NCR1" || ProductCode == "PS")
                        {
                            if (Response == "" || Error.ToLower().Contains("the operation has timed out"))
                            {
                                ObjBALOrders.UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("1"));
                                using (EmergeDALDataContext dx = new EmergeDALDataContext())
                                {
                                    objtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                                    objtblOrderDetail.MaximumHitsAllowed = 1;
                                    objtblOrderDetail.ErrorLog = Error;
                                    dx.SubmitChanges();
                                }
                            }
                        }

                        #region Insert All information to error log column for CCR1 Live runner Issue

                        using (EmergeDALDataContext dx = new EmergeDALDataContext())
                        {
                            try
                            {
                                objtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                                string ErrorConCat = string.Empty;
                                if (objtblOrderDetail != null)
                                {
                                    ErrorConCat = objtblOrderDetail.ErrorLog.ToString() + " ==>  (" + DateTime.Now.ToString() + ") ==> Error ==> " + Error + " ==> Response ==> " + Response;
                                    objtblOrderDetail.ErrorLog = ErrorConCat;
                                    dx.SubmitChanges();
                                }
                            }
                            catch { }
                        }

                        #endregion
                    }
                    catch (Exception ex)
                    {
                        using (EmergeDALDataContext dx = new EmergeDALDataContext())
                        {
                            try
                            {
                                objtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                                string ErrorConCat = string.Empty;
                                if (objtblOrderDetail != null)
                                {
                                    ErrorConCat = objtblOrderDetail.ErrorLog.ToString() + " ==>  (" + DateTime.Now.ToString() + ") ==> Error from catch ==> " + ex.ToString();
                                    objtblOrderDetail.ErrorLog = ErrorConCat;
                                    dx.SubmitChanges();
                                }
                            }
                            catch { }
                        }
                    }
                    finally
                    {
                        ObjRapidCourtService = null;
                        objtblOrderDetail = null;
                    }
                }
                if (ProductCode == "SOR" || ProductCode == "NCR3" || ProductCode == "CCR2")
                {
                    BackgroundChecksService ObjBackgroundChecksService = new BackgroundChecksService();
                    try
                    {
                        Response_Dic = ObjBackgroundChecksService.RunReport(ObjBALOrders.PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, pkOrderId.ToString(), OrderDetailId.ToString()));
                        Response = Response_Dic["response"];
                        Error = Response_Dic["error"];
                        string[] RequestAndResponse = Response.Split('^');
                        ObjBALOrders.UpdateRefSecureKey(OrderDetailId, RequestAndResponse[1]);
                        Response = RequestAndResponse[0];

                        tblOrderDetail objtblOrderDetail = new tblOrderDetail();
                        if (Response == "" && IsInstantReport)
                        {
                            ObjBALOrders.UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("3"));
                        }
                        else if (Response == "" && IsInstantReport == false)
                        {
                            if (ProductCode == "SCR")//Ticket #636
                            {
                                if (Response != "" || Error.ToLower().Contains("the operation has timed out"))//Ticket #639: Some SCR report is not completing
                                {
                                    ObjBALOrders.UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("1"));
                                    using (EmergeDALDataContext dx = new EmergeDALDataContext())
                                    {
                                        objtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                                        objtblOrderDetail.MaximumHitsAllowed = 1;
                                        objtblOrderDetail.ErrorLog = Error;
                                        dx.SubmitChanges();
                                    }
                                }
                                else
                                {
                                    ObjBALOrders.UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("3"));
                                    using (EmergeDALDataContext dx = new EmergeDALDataContext())
                                    {
                                        objtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                                        objtblOrderDetail.MaximumHitsAllowed = 1;
                                        objtblOrderDetail.ErrorLog = Error;
                                        dx.SubmitChanges();
                                    }
                                }
                            }
                        }

                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        ObjBackgroundChecksService = null;
                    }
                }
                if (ProductCode == "MVR" || ProductCode == "CDLIS")
                {
                    SoftechService ObjSoftechService = new SoftechService();
                    try
                    {
                        #region INT 263  comment code for running report using softech on 17 sept 2015
                        Response_Dic = ObjSoftechService.RunReport_ForPendingReports(PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, Vendor_Response, RefSecureKey, pkOrderId.ToString(), OrderDetailId.ToString()));
                        #endregion
                        Response = Response_Dic["response"];
                        Error = Response_Dic["error"];

                        if (Response_Dic["ispending"] != "")
                        {
                            ObjBALOrders.UpdateReportStatus(pkOrderId, ObjReport.pkOrderDetailId, System.Convert.ToByte("1"));
                        }

                        if (Response.Contains('^'))//MVR_Non_Instant
                        {
                            string[] RequestAndResponse = Response.Split('^');
                            XDocument ObjXDocumentQuoteBack = XDocument.Parse(RequestAndResponse[0]);
                            var filename = ObjXDocumentQuoteBack.Descendants("QuoteBack").FirstOrDefault();
                            if (filename != null)
                            {
                                string[] strQB = Convert.ToString(filename.Value).Split('_');
                                ObjBALOrders.UpdateRefSecureKey(Convert.ToInt32(strQB[2]), RequestAndResponse[1]);
                            }
                            Response = RequestAndResponse[0];
                        }
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        ObjSoftechService = null;
                    }
                }

                #region PSPRegion

                if (ProductCode == "PSP")
                {
                    //PSPServiceWebService objPSPService = new PSPServiceWebService();
                    try
                    {
                        Dictionary<string, string> Response_Dic1 = new Dictionary<string, string>();
                        Response_Dic1["response"] = "";
                        Response_Dic1["error"] = "";
                        Response_Dic1["ispending"] = "";
                        Response_Dic = Response_Dic1;// objPSPService.RunReport(ObjBALOrders.PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, pkOrderId.ToString(), OrderDetailId.ToString()));
                        Response = Response_Dic["response"];
                        Error = Response_Dic["error"];

                        if (Response_Dic["ispending"] != "")
                        {
                            ObjBALOrders.UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("1"));
                        }

                        if (Response.Contains('^'))
                        {
                            string[] RequestAndResponse = Response.Split('^');
                            ObjBALOrders.UpdateRefSecureKey(OrderDetailId, RequestAndResponse[1]);
                            Response = RequestAndResponse[0];
                        }

                        if (Response == "" && IsInstantReport)
                        {
                            ObjBALOrders.UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("3"));
                        }

                        if (Response == "")
                        {
                            ObjBALOrders.UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("3"));
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        //objPSPService = null;
                    }
                }

                #endregion

                if (ProductCode == "NCR2" || ProductCode == "CCR1" || ProductCode == "SCR")
                {
                    XpediteService ObjXpediteService = new XpediteService();
                    try
                    {
                        if (ProductCode == "CCR1" || ProductCode == "SCR")
                        {
                            Response_Dic = ObjXpediteService.RunReport_ForPendingReports(PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, Vendor_Response, RefSecureKey, pkOrderId.ToString(), OrderDetailId.ToString()));
                        }
                        else
                        {
                            Response_Dic = ObjXpediteService.RunReport(ObjBALOrders.PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, pkOrderId.ToString(), OrderDetailId.ToString()));
                        }

                        Response = Response_Dic["response"];
                        Error = Response_Dic["error"];
                        string[] RequestAndResponse = Response.Split('^');
                        ObjBALOrders.UpdateRefSecureKey(OrderDetailId, RequestAndResponse[1]);
                        Response = RequestAndResponse[0].Replace("utf-16", "utf-8");

                        tblOrderDetail objtblOrderDetail = new tblOrderDetail();
                        if (Response == "" && IsInstantReport)
                        {
                            if (ProductCode == "NCR2")
                            {
                                using (EmergeDALDataContext dx = new EmergeDALDataContext())
                                {
                                    objtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                                    if (objtblOrderDetail != null)
                                    {
                                        //This condition is done in vendor dll method, Please check : ObjXpediteService.RunReport
                                        objtblOrderDetail.MaximumHitsAllowed = 12;
                                        objtblOrderDetail.ErrorLog = Error;
                                        dx.SubmitChanges();
                                    }
                                }
                                ObjBALOrders.UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("3"));
                            }
                        }
                        else if (Response == "" && IsInstantReport == false)
                        {
                            if (ProductCode == "CCR1")//Ticket #636
                            {
                                if (Error.ToLower().Contains("the operation has timed out"))//Ticket #639: Some SCR report is not completing
                                {
                                    ObjBALOrders.UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("1"));
                                    using (EmergeDALDataContext dx = new EmergeDALDataContext())
                                    {
                                        objtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                                        objtblOrderDetail.MaximumHitsAllowed = 1;
                                        objtblOrderDetail.ErrorLog = Error;
                                        dx.SubmitChanges();
                                    }
                                }
                                else
                                {
                                    //ObjBALOrders.UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("3"));//INT196
                                    new BALGeneral().checkOrderPeriod(pkOrderId, OrderDetailId, ProductCode, Error);
                                    using (EmergeDALDataContext dx = new EmergeDALDataContext())
                                    {
                                        objtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                                        objtblOrderDetail.MaximumHitsAllowed = 1;
                                        objtblOrderDetail.ErrorLog = Error;
                                        dx.SubmitChanges();
                                    }
                                }
                            }
                            if (ProductCode == "SCR")//Ticket #636
                            {
                                if (Error.ToLower().Contains("the operation has timed out"))//Ticket #639: Some SCR report is not completing
                                {
                                    ObjBALOrders.UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("1"));
                                    using (EmergeDALDataContext dx = new EmergeDALDataContext())
                                    {
                                        objtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                                        objtblOrderDetail.MaximumHitsAllowed = 1;
                                        objtblOrderDetail.ErrorLog = Error;
                                        dx.SubmitChanges();
                                    }
                                }
                                else
                                {
                                    // ObjBALOrders.UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("3"));INT196
                                    new BALGeneral().checkOrderPeriod(pkOrderId, OrderDetailId, ProductCode, Error);
                                    using (EmergeDALDataContext dx = new EmergeDALDataContext())
                                    {
                                        objtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                                        objtblOrderDetail.MaximumHitsAllowed = 1;
                                        objtblOrderDetail.ErrorLog = Error;
                                        dx.SubmitChanges();
                                    }
                                }
                            }


                        }

                        #region Insert All information to error log column for CCR1 Live runner Issue

                        using (EmergeDALDataContext dx = new EmergeDALDataContext())
                        {
                            try
                            {
                                objtblOrderDetail = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == OrderDetailId).FirstOrDefault();
                                string ErrorConCat = string.Empty;
                                if (objtblOrderDetail != null)
                                {
                                    ErrorConCat = objtblOrderDetail.ErrorLog.ToString() + " ==>  (" + DateTime.Now.ToString() + ") ==> Error ==> " + Error + " ==> Response ==> " + Response;
                                    objtblOrderDetail.ErrorLog = ErrorConCat;
                                    dx.SubmitChanges();
                                }
                            }
                            catch { }
                        }

                        #endregion




                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        ObjXpediteService = null;
                    }
                }
                if (ProductCode == "EDV" || ProductCode == "EMV" || ProductCode == "PRV" || ProductCode == "PLV" || ProductCode == "PRV2" || ProductCode == "WCV")
                {

                    try
                    {

                        if (ProductCode == "WCV")//WCV enabled
                        {
                            FRSServiceWCV ObjFRSService = new FRSServiceWCV();
                            Response_Dic = ObjFRSService.RunReport(ObjBALOrders.PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, pkOrderId.ToString(), OrderDetailId.ToString()));
                            Response = Response_Dic["response"];
                            Error = Response_Dic["error"];
                            if (Response == "" && IsInstantReport)
                            {
                                ObjBALOrders.UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("3"));
                            }
                        }
                        else
                        {
                            FRSService ObjFRSService = new FRSService();
                            Response_Dic = ObjFRSService.RunReport(ObjBALOrders.PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, pkOrderId.ToString(), OrderDetailId.ToString()));
                            Response = Response_Dic["response"];
                            Error = Response_Dic["error"];
                            if (Response == "" && IsInstantReport)
                            {
                                ObjBALOrders.UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("3"));
                            }
                        }
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        ObjFRSService = null;
                    }
                }
                if (ProductCode == "SNS")
                {
                    SocialDiligenceService ObjSocialDiligenceService = new SocialDiligenceService();
                    try
                    {
                        Response_Dic = ObjSocialDiligenceService.RunReport(ObjBALOrders.PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, pkOrderId.ToString(), OrderDetailId.ToString()));
                        Response = Response_Dic["response"];
                        Error = Response_Dic["error"];
                        if (Response == "" && IsInstantReport)
                        {
                            ObjBALOrders.UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("3"));
                        }
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        ObjSocialDiligenceService = null;
                    }
                }

                #region Add new reports PS3,TDR2, EMV2, PL2

                if (ProductCode == "PS3" || ProductCode == "TDR2" || ProductCode == "EMV2" || ProductCode == "PL2")
                {
                    MerlinData ObjMerlinData = new MerlinData();
                    try
                    {
                        Response_Dic = ObjMerlinData.RunReport(ObjBALOrders.PassParams(ObjProc_GetAllProductsWithVendorResult.Where(data => data.ProductCode == ProductCode).First(), Vendor_Request, ProductCode, pkOrderId.ToString(), OrderDetailId.ToString()));
                        Response = Response_Dic["response"];
                        Error = Response_Dic["error"];
                        if (Response == "" && IsInstantReport)
                        {
                            ObjBALOrders.UpdateReportStatus(pkOrderId, OrderDetailId, Convert.ToByte("3"));
                        }
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        ObjMerlinData = null;
                    }
                }

                #endregion

                if (Error != "")
                {
                    BALGeneral.LogError(OrderDetailId, Error);
                }

                #region Send Email For Incomplete Reports
                if (Response == "" && !Error.ToLower().Contains("timed out"))
                {
                    //   SendErrorEmail(pkOrderId, Error, ProductCode, 3, OrderDetailId);
                }
                #endregion
            }
            catch (Exception ex)
            {
                ObjtblReportErrorLog = new tblReportErrorLog();
                ObjtblReportErrorLog.fkProductId = 0;
                ObjtblReportErrorLog.fkCompanyUserId = _mCompanyUserId;
                ObjtblReportErrorLog.ReportError = ex.Message.ToString();
                ObjtblReportErrorLog.fkOrderId = pkOrderId;
                Response = ex.Message.ToString();
                ObjBALGeneral.AddReportErrorToErrorLog(ObjtblReportErrorLog);
                BALGeneral.LogError(ObjReport.pkOrderDetailId, ex.Message.ToString());
            }
            finally
            {
                ObjBALGeneral = null;
                ObjtblReportErrorLog = null;
            }

            return Response;
        }

        public void UpdateStatus_And_SendemailForTiered(Proc_Get_TieredReportOrdersResult ObjReport, string Response_data, string CompanyAccountNo, string OrderCompanyName, string PermissiblePurpose)
        {
            try
            {
                BALOrders ObjOrder = new BALOrders();
                //BLLMembership ObjBLLMembership = new BLLMembership();

                string fiteredResponse = Response_data;
                using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                {
                    //string Body = "";
                    string Subject = "";
                    string ProductCode = ObjReport.ProductCode;
                    if (ObjReport.IsLiveRunner == true)
                    {
                        ProductCode = "RCX";
                    }
                    byte ReportStatus = 1;

                    if ((ProductCode == "NCR1" || ProductCode == "PS") && Response_data == "") // Incomplete NCR1, PS report if response is empty
                    {
                        ReportStatus = 3;
                    }
                    else
                        ReportStatus = System.Convert.ToByte(BALGeneral.CheckReportStatus(Response_data, ProductCode, false, ObjReport.pkOrderDetailId, ObjReport.fkOrderId, true).ToString());

                    #region Get Verification Fee From Vendor Response
                    if ((ProductCode == "EDV" || ProductCode == "EMV" || ProductCode == "PRV" || ProductCode == "PLV" || ProductCode == "PRV2") && ReportStatus == 2)
                    {

                        ObjOrder.AddVerificationFee(ObjReport.pkOrderDetailId, Response_data, ProductCode);
                        //To Add the data in tblInvoice table
                        new BALInvoice().CreateInvoice(ObjReport.fkOrderId);
                    }

                    #endregion

                    if (ReportStatus == 1 || ReportStatus == 3)
                    {
                        DateTime OrderDate = ObjReport.OrderDt;
                        ObjOrder.SendPendingMailReminder(ObjReport.fkOrderId, OrderDate, Response_data, ReportStatus, ObjReport.pkOrderDetailId, ProductCode);
                    }
                    #region If the Live Runner Reports(CCR's) are cancelled By Vendor

                    if (ProductCode == "RCX" && ReportStatus == 5)
                    {
                        var OrderDetail = Dx.tblOrderDetails.Where(db => db.pkOrderDetailId == ObjReport.pkOrderDetailId).FirstOrDefault();
                        if (OrderDetail.IsLiveRunner == true)
                        {
                            #region Update IsLiveRunner To false
                            OrderDetail.IsLiveRunner = false;
                            Dx.SubmitChanges();
                            #endregion
                            ObjBALOrders.PlaceNewOrderForLiveRunnerCancelledReport(ObjReport.fkOrderId, ObjReport.pkOrderDetailId, ObjReport.ProductCode, string.Empty, string.Empty, string.Empty);
                        }
                        else
                            ReportStatus = 4;
                    }

                    #endregion
                    else
                    {
                        #region This Section will call the url inside the MVR response to Apply the Styles to the XML.This code from view report will now be removed.

                        if (ReportStatus == 2 && ProductCode == "MVR")//MVR_Non_Instant
                        {
                            //Response_data = GetMvrResponse(Response_data);
                        }

                        #endregion
                        if (ObjReport.pkOrderResponseId == null)
                        {
                            tblOrderResponseData ObjResponse = new tblOrderResponseData();
                            //ObjResponse.pkOrderResponseId = Guid.NewGuid();
                            ObjResponse.fkOrderDetailId = ObjReport.pkOrderDetailId;
                            bool isFileExists = false;
                            string responseString = BALGeneral.GetRCXResponsefromFile(ObjReport.pkOrderDetailId, out isFileExists);
                            if (isFileExists)
                            {
                                ObjResponse.ResponseData = responseString;
                                ReportStatus = 2;
                            }
                            else
                            {
                                ObjResponse.ResponseData = Response_data;
                            }
                            ObjResponse.ErrorResponseData = "Entry from pkOrderResponseId == null section";
                            ObjResponse.EmergeReviewNote = string.Empty;
                            Dx.tblOrderResponseDatas.InsertOnSubmit(ObjResponse);
                            Dx.SubmitChanges();
                            RenameRCXFile(ObjReport.pkOrderDetailId);
                        }
                        else
                        {
                            try
                            {
                               // bool updated = false;

                                VendorServices.GeneralService.WriteLog("," + Environment.NewLine + ObjReport.OrderNo + "Before response Insert", "AllPendingOrders");
                                //MVR_Non_Instant
                                XDocument ObjXDocumentQuoteBack = XDocument.Parse(Response_data);
                                var filename = ObjXDocumentQuoteBack.Descendants("QuoteBack").FirstOrDefault();
                                if (filename != null)
                                {
                                    string[] strQB = Convert.ToString(filename.Value).Split('_');
                                    ObjBALOrders.UpdateOrderResponse(Convert.ToInt32(strQB[2]), Response_data);
                                }
                                else
                                {
                                    ObjBALOrders.UpdateOrderResponse(ObjReport.pkOrderDetailId, Response_data);
                                }
                                VendorServices.GeneralService.WriteLog("," + Environment.NewLine + ObjReport.OrderNo + "After response Insert", "AllPendingOrders");
                                var ObjOrderDetail = Dx.tblOrderDetails.Where(db => db.pkOrderDetailId == ObjReport.pkOrderDetailId).FirstOrDefault();
                                if (ProductCode == "RCX" && ReportStatus == 2)
                                {
                                    bool IsRCXHit = ObjBALOrders.GetRCXHitstatus(Response_data);
                                    if (IsRCXHit == true)
                                    {
                                        string PCode = (from d in Dx.tblProducts
                                                        join g in Dx.tblProductPerApplications
                                                        on d.pkProductId equals g.fkProductId
                                                        where (g.pkProductApplicationId == ObjOrderDetail.fkProductApplicationId)
                                                        select d.ProductCode).FirstOrDefault().ToString();
                                        ObjBALOrders.PlaceCCR1orderForRCXHitReport(ObjReport.fkOrderId, ObjReport.pkOrderDetailId, PCode, CompanyAccountNo, OrderCompanyName, PermissiblePurpose);
                                    }
                                    if (this.RCXFilePath != "")
                                    {
                                        bool isFileExists = false;
                                        string responseString = BALGeneral.GetRCXResponsefromFile(ObjReport.pkOrderDetailId, out isFileExists);
                                        if (isFileExists)
                                        {
                                            Response_data = responseString;
                                            ReportStatus = 2;
                                            RenameRCXFile(ObjReport.pkOrderDetailId);
                                        }
                                    }
                                }
                                BALGeneral ObjBALGeneral = new BALGeneral();
                               // var ObjOrderDetail2 = Dx.tblOrderDetails.Where(db => db.pkOrderDetailId == ObjReport.pkOrderDetailId).FirstOrDefault();
                                if (ProductCode == "NCR4" || ProductCode == "NCR1" || ProductCode == "NCR+" || ProductCode == "NCR2")
                                {
                                    fiteredResponse = ObjBALGeneral.GetFilteredXMLDuringOrderPlace(ProductCode, Response_data, ObjReport.pkOrderDetailId, DateTime.Now, true, false);
                                    using (EmergeDALDataContext Dx2 = new EmergeDALDataContext())
                                    {
                                        var ObjOrderDetailnew = Dx2.tblOrderDetails.Where(db => db.pkOrderDetailId == ObjReport.pkOrderDetailId).FirstOrDefault();
                                        if (ObjOrderDetailnew.ReviewStatus != 1)
                                        {
                                            ObjBALGeneral.CheckIsNCRHit(ObjReport.fkOrderId, ObjReport.pkOrderDetailId, ProductCode, fiteredResponse, false);

                                        }
                                    }
                                }
                            }
                            catch
                            {
                            }

                        }
                        //  Update Report Status 

                        //MVR_Non_Instant
                        if (ProductCode == "MVR" && ReportStatus == 2)//MVR_Non_Instant
                        {
                            XDocument ObjXDocumentQuoteBack1 = XDocument.Parse(Response_data);
                            var filename1 = ObjXDocumentQuoteBack1.Descendants("QuoteBack").FirstOrDefault();
                            if (filename1 != null)
                            {
                                string[] strQB = Convert.ToString(filename1.Value).Split('_');
                                ObjBALOrders.UpdateReportStatus(Convert.ToInt32(strQB[0]), Convert.ToInt32(strQB[2]), ReportStatus);
                            }
                        }
                        else
                        {
                            ObjBALOrders.UpdateReportStatus(ObjReport.fkOrderId, ObjReport.pkOrderDetailId, ReportStatus);
                        }


                        if (ReportStatus == 2)
                        {
                            if (ProductCode == "OFAC" || ProductCode == "NCR1" || ProductCode == "NCR2" || ProductCode == "NCR+" || ProductCode == "NCR4" || ProductCode == "SCR" || ProductCode == "CCR1" || ProductCode == "CCR2" || ProductCode == "SOR" || ProductCode == "RCX" || ProductCode == "SCRLiveRunner" || ProductCode == "CCR1LiveRunner" || ProductCode == "CCR2LiveRunner")
                            {
                                XDocument ObjXDocument = XDocument.Parse(fiteredResponse);
                                XMLFilteration objXMLFilteration = new XMLFilteration();
                                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                                {
                                    if (DX.tblOrderDetails.Where(db => db.pkOrderDetailId == ObjReport.pkOrderDetailId).Count() > 0)
                                    {
                                        tblOrderDetail ObjOrderDetails = DX.tblOrderDetails.Where(db => db.pkOrderDetailId == ObjReport.pkOrderDetailId).FirstOrDefault();
                                        if (ObjOrderDetails != null)
                                        {
                                            tblOrder objtblOrder = DX.tblOrders.Where(db => db.pkOrderId == ObjOrderDetails.fkOrderId).FirstOrDefault(); ;
                                            if (objtblOrder.HitStatus == 0)
                                            {
                                                objXMLFilteration.CheckIsHit(ObjOrderDetails.fkOrderId, ProductCode, ObjXDocument);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        tblOrderDetails_Archieve ObjtblOrderDetails_Archieve = DX.tblOrderDetails_Archieves.Where(db => db.pkOrderDetailId == ObjReport.pkOrderDetailId).FirstOrDefault();
                                        if (ObjtblOrderDetails_Archieve != null)
                                        {
                                            tblOrder objtblOrder = DX.tblOrders.Where(db => db.pkOrderId == ObjtblOrderDetails_Archieve.fkOrderId).FirstOrDefault(); ;
                                            if (objtblOrder.HitStatus == 0)
                                            {
                                                objXMLFilteration.CheckIsHit(ObjtblOrderDetails_Archieve.fkOrderId, ProductCode, ObjXDocument);
                                            }
                                        }
                                    }
                                }
                            }
                            #region Send Auto Mail For Report Review
                            ObjOrder.SendAutoMailForReview(ObjReport.fkOrderId, ObjReport.pkOrderDetailId, Response_data, true);
                            #endregion

                            //Send Email Here .
                            string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + BALGeneral.GetGeneralSettings().WebSiteLogo;

                            BALGeneral ObjBALGeneral = new BALGeneral();
                            int pkTemplateId = 5;
                            var emailContent = ObjBALGeneral.GetEmailTemplate(new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852"), pkTemplateId);//"Complete Report"

                            string emailText = emailContent.TemplateContent;
                            BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                            #region For New Style SystemTemplate
                            string Bookmarkfile = string.Empty;
                            string bookmarkfilepath = string.Empty;

                            Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                            Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                            Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                            emailText = Bookmarkfile;
                            #endregion

                            #region New way Bookmark

                            CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
                            TextInfo textInfo = cultureInfo.TextInfo;
                            string firstname = ObjReport.SearchedFirstName != null ? textInfo.ToTitleCase(ObjReport.SearchedFirstName) : string.Empty;
                            string lastname = ObjReport.SearchedLastName != null ? textInfo.ToTitleCase(ObjReport.SearchedLastName) : string.Empty;

                            Bookmarks objBookmark = new Bookmarks();
                            string MessageBody = string.Empty;
                            objBookmark.EmailContent = emailText;
                            objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                            objBookmark.ApplicantName = "Emerge";
                            // objBookmark.ViewReportLink = Linkis;
                            objBookmark.ViewReportLink = string.Empty;
                            objBookmark.ProductName = ObjReport.ProductDisplayName;
                            objBookmark.ApplicantName = firstname + " " + lastname;
                            objBookmark.ReportCode = ObjReport.ProductCode;
                            MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Guid.Empty);

                            #endregion

                            //Send Email
                            Subject = emailContent.TemplateSubject.Replace("%ApplicantName%", firstname + " " + lastname); //"Your Report on " + ObjReport.SearchedFirstName + " " + ObjReport.SearchedLastName + " is Ready on Emerge";
                            //string Rolename = "";
                            //Rolename = BLLMembership.GetUserRoleByUsername(ObjReport.Email);
                            //if (Rolename.ToLower() != "dataentry".ToLower())
                            //{
                            string UserEmails = string.Empty;

                            if (IsSetTrueMailByUser(ObjReport.fkOrderId))
                            {
                                //ticket #101 adding emails
                                if (!string.IsNullOrEmpty(ObjReport.Email))
                                {
                                    UserEmails = ObjBALGeneral.GetUserOtherMailIds(ObjReport.Email);
                                }
                                //Utilitys.SendMail(ObjReport.Email, UserEmails, string.Empty, string.Empty, MessageBody, Subject);
                                SendEmail(ObjReport.Email, UserEmails, MessageBody, Subject);
                            }
                            //}


                            //To Add the data in tblInvoice table
                            new BALInvoice().CreateInvoice(ObjReport.fkOrderId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WCVLog(DateTime.Now, "15.6 UpdateStatus_And_Sendemail : error : " + ObjReport.fkOrderId.ToString(), ex.Message.ToString());
            }
        }
        public void UpdateIsProcessTiered(int IsOrderDetailId)
        {
            try
            {
                using (EmergeDALDataContext ObjDb = new EmergeDALDataContext())
                {
                    var query = ObjDb.tblOrderDetails
                           .Where(pd => pd.pkOrderDetailId == IsOrderDetailId).SingleOrDefault();
                    if (query != null)
                    {
                        if (query.ReportStatus == 2)
                        {
                            query.IsTieredProcessed = true;
                        }
                        else
                        {
                            query.IsTieredProcessed = false;
                        }
                        ObjDb.SubmitChanges();
                    }
                }
            }
            catch (Exception)
            { }
        }
        #endregion
    }

    public class PendingLog
    {
        public int? pkOrderId { get; set; }
        public string Error { get; set; }
        public DateTime RuningRime { get; set; }
    }
}
