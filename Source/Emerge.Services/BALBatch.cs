﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;
using System.Xml.Serialization;
using System.Web;
using System.IO;
using System.Xml.Linq;






namespace Emerge.Services
{
    public class BALBatch
    {
        int status = 0;
        public int InsertBatchOrderFile(tblBatchOrder ObjtblBatchOrder)
        {
            try
            {
                using (EmergeDALDataContext dx = new EmergeDALDataContext())
                {
                    dx.tblBatchOrders.InsertOnSubmit(ObjtblBatchOrder);
                    dx.SubmitChanges();
                    status = 1;
                }
            }
            catch { status = -1; }
            return status;
        }

        public int? InsertSingleOrder(EmergeReport ObjEmergeReport)
        {
            Guid SiteApplicationId = new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852");
            
            EmergeReport ObjEmergeReport1 = new EmergeReport();
            int? OrderId = 0;

            List<tblOrderSearchedAliasPersonalInfo> ObjCollOrderSearchedAliasPersonalInfo = new List<tblOrderSearchedAliasPersonalInfo>();
            List<tblOrderSearchedAliasLocationInfo> ObjCollOrderSearchedAliasLocationInfo = new List<tblOrderSearchedAliasLocationInfo>();
            List<tblOrderSearchedAliasWorkInfo> ObjCollOrderSearchedAliasWorkInfo = new List<tblOrderSearchedAliasWorkInfo>();
            Dictionary<string, Guid> ObjCompnyProductInfo = new Dictionary<string, Guid>();
            BALOrders ObjBALOrders = new BALOrders();
            ObjEmergeReport1 = ObjBALOrders.AddOrders(ObjEmergeReport.ObjtblOrder,
                       ObjEmergeReport.ObjCollProducts,
                       ObjEmergeReport.ObjtblOrderSearchedData,
                       ObjEmergeReport.ObjCollOrderSearchedEmploymentInfo,
                       ObjEmergeReport.ObjCollOrderSearchedEducationInfo,
                       ObjEmergeReport.ObjCollOrderSearchedPersonalInfo,
                       ObjEmergeReport.ObjCollOrderSearchedProfessionalInfo,
                       ObjEmergeReport.objColltblOrderPersonalQtnInfo,
                       ObjEmergeReport.ObjCollOrderSearchedLicenseInfo,
                       ObjCollOrderSearchedAliasPersonalInfo,
                       ObjCollOrderSearchedAliasLocationInfo,
                       ObjCollOrderSearchedAliasWorkInfo,
                       ObjCompnyProductInfo,
                       ObjEmergeReport.ObjColltblOrderSearchedLiveRunnerInfo,
                       ObjEmergeReport.ObjCollCLSConsentInfo,
                       SiteApplicationId, ObjEmergeReport.IsNullDOB,
                       ObjEmergeReport.IsLiveRunnerState, ObjEmergeReport.ObjTieredPackage, ObjEmergeReport.ObjOrderInfo, ObjEmergeReport.CompanyAccountNo, ObjEmergeReport.OrderCompanyName, ObjEmergeReport.PermissiblePurpose, ObjEmergeReport.ObjJurisdictionList);
            OrderId = ObjEmergeReport1.ObjOrderInfo.OrderId;
            return OrderId;
        }



        #region batchinsert 29 sep

        public void getBatchFiles()
        {
            DirectoryInfo di = new DirectoryInfo(HttpContext.Current.Server.MapPath(@"~\Resources\Upload\TempBatchFiles"));

            FileSystemInfo[] fsi = di.GetFiles();
            if (di != null)
            {
                if (fsi.Length > 0)
                {
                    foreach (FileInfo fi in fsi)
                    {
                        string sExt = "";
                        sExt = Path.GetExtension(fi.Name);
                        if (sExt.ToLower() == ".xml")
                        {
                            tblBatchOrder objtblBatchOrder = new tblBatchOrder();
                            string strUserId = string.Empty;
                            Guid UserId = Guid.Empty;
                            string FilePath = HttpContext.Current.Server.MapPath(@"~\Resources\Upload\TempBatchFiles\") + fi.Name;
                            using (EmergeDALDataContext dx = new EmergeDALDataContext())
                            {
                                objtblBatchOrder = dx.tblBatchOrders.Where(d => d.BatchFileName == fi.Name).FirstOrDefault();
                                if (objtblBatchOrder != null)
                                {
                                    strUserId = fi.Name.Split('_').FirstOrDefault();
                                    UserId = new Guid(strUserId);

                                    if (!objtblBatchOrder.IsProcessed)
                                    {
                                        SendNotificationToUser(UserId, objtblBatchOrder.CreateDate);
                                        Deserialize(FilePath, fi.Name, objtblBatchOrder.pkBatchId);
                                    }

                                    if (File.Exists(FilePath))
                                    {
                                        try
                                        {
                                            // File.Delete(FilePath);
                                        }
                                        catch //(Exception)
                                        {
                                            // throw;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        public List<Proc_GetBatchOrderByPkCompanyUserIdResult> GetBatchOrderByPkCompanyUserId(Guid  PkCompanyUserId)
        {
            using (EmergeDALDataContext objdata = new EmergeDALDataContext())
            {
               return objdata.GetBatchOrderByPkCompanyUserId(PkCompanyUserId).ToList();
            }
        }
        

        public void SendNotificationToUser(Guid UserId, DateTime FileCreatedDate)
        {
            Proc_Get_UserInfoByMemberShipUserIdResult ObjUserInfo = new Proc_Get_UserInfoByMemberShipUserIdResult();
            BALGeneral ObjBALGeneral = new BALGeneral(); string emailText = ""; string Subject = "";
            BALOrders ObjBALOrders = new BALOrders();
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                ObjUserInfo = dx.GetUserInfoByMemberShipUserId(UserId).FirstOrDefault();
                if (ObjUserInfo != null)
                {
                    var emailContent_Mvr = ObjBALGeneral.GetEmailTemplate(new Guid("65D1E085-EB17-4F29-96D0-247B0C1EE852"), 40);
                    emailText = emailContent_Mvr.TemplateContent.ToString();
                    #region For New Style SystemTemplate
                    string Bookmarkfile = string.Empty;
                    string bookmarkfilepath = string.Empty;
                    BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                    Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                    Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent_Mvr.TemplateSubject);
                    Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent_Mvr.TemplateContent);
                    emailText = Bookmarkfile;
                    #endregion


                    Subject = emailContent_Mvr.TemplateSubject.Replace("%Date%", FileCreatedDate.ToString());
                      //For email status//
                    BALGeneral objbalgenral = new BALGeneral();
                    int pktemplateid = 40;
                    Guid user_ID = UserId;
                    int fkcompanyuserid = 0;
                    bool email_status = objbalgenral.UserEmailStatusCheck(user_ID, pktemplateid, fkcompanyuserid);

                  
                    if (email_status == true)
                    {
                        ObjBALOrders.SendBatchProcessNotification(ObjUserInfo.UserEmail, ObjUserInfo.FirstName, ObjUserInfo.LastName, FileCreatedDate.ToString(), emailText, Subject);

                    }

                                   
                }
            }
        }

        public void Deserialize(string spath, string sFilename, int fkbatchId)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<ApplicantInfo>));
            TextReader tr = new StreamReader(spath);
            List<ApplicantInfo> ObjApplicantColl = (List<ApplicantInfo>)serializer.Deserialize(tr);
            tr.Close();
            RunProcessAll(ObjApplicantColl, spath, sFilename, fkbatchId);
        }

        public void RunProcessAll(List<ApplicantInfo> ApplicantInfoColl, string FilePath, string sFilename, int fkbatchId)
        {
            XElement doc = XElement.Load(FilePath);
            IEnumerable<XElement> ApcantColl = (from b in doc.Elements("ApplicantInfo") where ((string)b.Attribute("IsApplicantProcessed").Value == "0") select b);
            if (ApcantColl != null)
            {
                if (ApplicantInfoColl.Count() > 0 && ApcantColl.Count() > 0)
                {
                    foreach (ApplicantInfo AppInfo in ApplicantInfoColl)
                    {
                        int? OrderId = 0;
                        string ApplicantNum = AppInfo.ApplicantNumber;

                        if (AppInfo.IsApplicantProcessed == "0")
                        {
                            OrderId = RunProcessSingle(AppInfo, fkbatchId);
                            if (OrderId != 0)
                            {
                                ///
                                /// This means current order is processed and insert into db is done.
                                ///
                                IEnumerable<XElement> Apcant = (from b in doc.Elements("ApplicantInfo")
                                                                where ((string)b.Attribute("ApplicantNumber").Value == Convert.ToString(ApplicantNum))
                                                                select b);
                                if (Apcant != null)
                                {
                                    foreach (XElement xe in Apcant)
                                    {
                                        xe.SetAttributeValue("IsApplicantProcessed", "1");
                                    }
                                    doc.Save(FilePath);
                                }
                            }
                        }
                    }
                }
                else //Means all records are processed and update status in DB. 
                {
                    using (EmergeDALDataContext dx = new EmergeDALDataContext())
                    {
                        tblBatchOrder objtblBatchOrder = new tblBatchOrder();
                        objtblBatchOrder = dx.tblBatchOrders.Where(d => d.BatchFileName == sFilename).FirstOrDefault();
                        if (objtblBatchOrder != null)
                        {
                            objtblBatchOrder.IsProcessed = true;
                            dx.SubmitChanges();
                        }
                    }
                }
            }
        }

        private int? RunProcessSingle(ApplicantInfo APPLICANTINFO, int fkBatchId)
        {
            //SetViewState();

            int? OrderId = 0;
            int LocationId = 0;
            int CompanyUserId = 0;
            //BALOrders ObjBALOrders = new BALOrders();
            OrderInfo ObjOrderInfo = new OrderInfo();
            tblTieredPackage ObjtblTieredPackage = new tblTieredPackage();
            int TieredPackageid = 0;
            List<tblState> tblStateColl = new List<tblState>();
            using (EmergeDALDataContext dx = new EmergeDALDataContext()) { tblStateColl = dx.tblStates.ToList(); }

            try
            {
                List<string> ObjCollProducts = new List<string>();
                //Dictionary<string, int> ObjCompnyProductInfo = new Dictionary<string, int>();
                //String sScript = "alert('Please select at least one report to run.');";
                // if (ViewState["vw_Panels"] == null)
                //{
                //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "scri", sScript, true);
                //    return;
                //}
                //else
                //{
                //ObjCollProducts = (List<string>)ViewState["vw_Panels"];
                ////////
                //BATCHProductColl = ObjCollProducts;
                ////////
                // ObjCompnyProductInfo = (Dictionary<string, Guid>)ViewState["vwCompanyProductInfo"];
                ///
                //BATCHCompanyProductInfo = ObjCompnyProductInfo;
                ///
                if (ObjCollProducts.Count == 0)
                {
                    // ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "scri", sScript, true);
                    // return;
                }

                ObjCollProducts = APPLICANTINFO.ProductColl; // FindControlsinSelectedReports(ObjCollProducts);
                for (int i = 0; i < ObjCollProducts.Count; i++)
                {
                    string[] ProductFullName = ObjCollProducts.ElementAt(i).Split('_');
                    int.TryParse(ProductFullName[1], out TieredPackageid);
                    if (ProductFullName[0] == "NCRPLUS")
                    {
                        ObjCollProducts.RemoveAt(i);
                        ObjCollProducts.Add("NCR+_" + ProductFullName[1]);
                    }
                    if (ProductFullName[0] == "NCR2")//Ticket #740: Incomplete report on NCR2 and error log is not shwing #17
                    {
                        if (APPLICANTINFO.Applicant.Social == "")
                        {
                            //txtSocial.Text = "999-99-9999";
                            APPLICANTINFO.Applicant.Social = "999-99-9999";
                        }
                    }
                }
                //if (ObjCollProducts.Count == 0)
                //{
                //    sScript = "alert('There are no fields in selected report(s).');";
                //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "scri", sScript, true);
                //    return;
                //}
                #region Tiered Section

                using (EmergeDALDataContext dx = new EmergeDALDataContext())
                {
                    if (TieredPackageid != 0)
                    {
                        ObjtblTieredPackage = dx.tblTieredPackages.Where(d => d.fkPackageId == TieredPackageid).FirstOrDefault();
                        if (ObjtblTieredPackage != null)
                        {
                            ObjOrderInfo.IsTiered = true;
                            ObjOrderInfo.IsAutomatic = ObjtblTieredPackage.IsAutomaticRun;
                            if (ObjtblTieredPackage.Variable == "County State")
                            {
                                ObjOrderInfo.Variable = "1";
                            }
                            else if (ObjtblTieredPackage.Variable == "State")
                            {
                                ObjOrderInfo.Variable = "2";
                            }
                        }
                    }
                }

                #endregion

                DateTime Dated = DateTime.Now;

                #region Insertion in main order table


                //if (ViewState["vwUserLocationId"] != null)
                //{
                //    LocationId = new Guid(ViewState["vwUserLocationId"].ToString());
                //    Session["LocationId"] = LocationId;
                //}
                //if (ViewState["vwCompanyUserId"] != null)
                //{
                //    CompanyUserId = new Guid(ViewState["vwCompanyUserId"].ToString());
                //    Session["CompanyUserId"] = CompanyUserId;
                //}
                if (APPLICANTINFO.OwnerInfo.LocationId != null)
                {
                    LocationId = APPLICANTINFO.OwnerInfo.LocationId;
                }
                if (APPLICANTINFO.OwnerInfo.CompanyUserId != null)
                {
                    CompanyUserId = APPLICANTINFO.OwnerInfo.CompanyUserId;
                }
                tblOrder ObjtblOrder = new tblOrder();
                ObjtblOrder.fkLocationId = LocationId;
                ObjtblOrder.fkCompanyUserId = CompanyUserId;

                //BATCHOwnerInfo.CompanyUserId = CompanyUserId.ToString();
                //BATCHOwnerInfo.LocationId = LocationId.ToString();

                string OrderNo = Dated.ToString("yyMMddHHmmssfff");
                ObjtblOrder.OrderNo = OrderNo;
                ObjtblOrder.OrderDt = Dated;
                ObjtblOrder.OrderStatus = (byte)1;
                ObjtblOrder.CreatedDate = Dated;
                ObjtblOrder.CreatedById = CompanyUserId;
                ObjtblOrder.fkBatchId = fkBatchId;
                ObjtblOrder.FaceImage = string.Empty;
                ObjtblOrder.OrderType = (byte)1;
                ObjtblOrder.ReferenceCode = string.Empty;
                ObjtblOrder.IsSyncpod = false;

                #endregion

                #region Insert new ReferenceCode for company

                int ReferenceCodeId = 0;
                string ReferenceCode = string.Empty;
                //if (Convert.ToInt32(ddlTrackingRefCode.SelectedValue) > 0)
                //{
                //    ReferenceCodeId = Convert.ToInt32(ddlTrackingRefCode.SelectedValue);
                //    ReferenceCode = ddlTrackingRefCode.SelectedItem.Text;
                //}
                //else if (ddlTrackingRefCode.SelectedValue == "-3" && txtTrackingRefCode.Text.Trim() != "")
                //{
                ReferenceCode = APPLICANTINFO.Tracking.TrackingRefCode;// txtTrackingRefCode.Text.Trim();
                tblReferenceCode ObjtblReferenceCode = new tblReferenceCode();
                BALReferenceCode ObjBALReferenceCode = new BALReferenceCode();
                //DateTime CreatedDate = DateTime.Now;
                int fkCompanyId = APPLICANTINFO.OwnerInfo.CompanyId;   //ViewState["PkCompanyId"].ToString());
                ObjtblReferenceCode.fkCompanyId = fkCompanyId;
                ObjtblReferenceCode.ReferenceCode = APPLICANTINFO.Tracking.TrackingRefCode; //txtTrackingRefCode.Text.Trim();

                ObjtblReferenceCode.ReferenceNote = APPLICANTINFO.Tracking.TrackingNotes;    //txtTrackingNotes.Text.Trim();
                int result = ObjBALReferenceCode.AddReference(ObjtblReferenceCode, fkCompanyId, true);
                if (result > 0)
                {
                    ReferenceCodeId = result;
                }
                // }

                // BATCHTracking.TrackingRefCode = ReferenceCode;
                // BATCHTracking.SearchedTrackingRefId = ReferenceCodeId.ToString();

                #endregion

                #region Insertion in order searched table

                tblOrderSearchedData ObjtblOrderSearchedData = new tblOrderSearchedData();

                ObjtblOrderSearchedData.SearchedLastName = APPLICANTINFO.Applicant.LastName; //txtLastName.Text.Trim();
                ObjtblOrderSearchedData.SearchedFirstName = APPLICANTINFO.Applicant.FirstName; //txtFirstName.Text.Trim();
                ObjtblOrderSearchedData.SearchedMiddleInitial = APPLICANTINFO.Applicant.MI; //txtMI.Text.Trim();
                ObjtblOrderSearchedData.SearchedSuffix = APPLICANTINFO.Applicant.Suffix; //(ddlSuffix.SelectedValue != "-1") ? ddlSuffix.SelectedValue : string.Empty;
                ObjtblOrderSearchedData.SearchedSsn = APPLICANTINFO.Applicant.Social; //txtSocial.Text.Trim();
                ObjtblOrderSearchedData.SearchedDob = APPLICANTINFO.Applicant.DOB; //txtDOB.Text.Trim();
                ObjtblOrderSearchedData.SearchedPhoneNumber = APPLICANTINFO.Applicant.Phone; //txtPhone.Text.Trim();
                ObjtblOrderSearchedData.SearchedStreetAddress = APPLICANTINFO.Applicant.StreetNumber; //txtAddress.Text.Trim();
                ObjtblOrderSearchedData.SearchedDirection = APPLICANTINFO.Applicant.Direction; //(ddlDirection.SelectedValue != "-1") ? ddlDirection.SelectedValue : string.Empty;
                ObjtblOrderSearchedData.SearchedStreetName = APPLICANTINFO.Applicant.StreetName; //txtStreet.Text.Trim();
                ObjtblOrderSearchedData.SearchedStreetType = APPLICANTINFO.Applicant.Type; //(ddlType.SelectedValue != "-1") ? ddlType.SelectedValue : string.Empty;
                ObjtblOrderSearchedData.SearchedApt = APPLICANTINFO.Applicant.Apt; //txtApt.Text.Trim();
                ObjtblOrderSearchedData.SearchedCity = APPLICANTINFO.Applicant.City; //txtCit.y.Text.Trim();
                ObjtblOrderSearchedData.SearchedApplicantEmail = APPLICANTINFO.Applicant.ApplicantEmail;

                //BATCHApplicant.LastName             = txtLastName.Text.Trim();
                //BATCHApplicant.FirstName            = txtFirstName.Text.Trim();
                //BATCHApplicant.MI                   = txtMI.Text.Trim();
                //BATCHApplicant.Suffix               = (ddlSuffix.SelectedValue != "-1") ? ddlSuffix.SelectedValue : string.Empty;
                //BATCHApplicant.Social           = txtSocial.Text.Trim();
                //BATCHApplicant.DOB               = txtDOB.Text.Trim();
                //BATCHApplicant.Phone             = txtPhone.Text.Trim();
                //BATCHApplicant.StreetNumber     = txtAddress.Text.Trim();
                //BATCHApplicant.Direction        = (ddlDirection.SelectedValue != "-1") ? ddlDirection.SelectedValue : string.Empty;
                //BATCHApplicant.StreetName       = txtStreet.Text.Trim();
                //BATCHApplicant.Type              = (ddlType.SelectedValue != "-1") ? ddlType.SelectedValue : string.Empty;
                //BATCHApplicant.AptNo            = txtApt.Text.Trim();
                //BATCHApplicant.City             = txtCity.Text.Trim();


                bool IsLiveRunnerState = false;
                if (!String.IsNullOrEmpty(APPLICANTINFO.Applicant.State))//ddlState.SelectedValue != "-1")
                {
                    tblState objtblState = new tblState();
                    using (EmergeDALDataContext dx = new EmergeDALDataContext())
                    {
                        objtblState = dx.tblStates.Where(d => d.StateName == APPLICANTINFO.Applicant.State).FirstOrDefault();
                        if (objtblState == null)
                        {
                            objtblState = dx.tblStates.Where(d => d.StateCode == APPLICANTINFO.Applicant.State).FirstOrDefault();
                        }
                        if (objtblState != null)
                        {
                            IsLiveRunnerState = objtblState.IsStateLiveRunner;
                            ObjtblOrderSearchedData.SearchedStateId = objtblState.pkStateId;
                            //BATCHApplicant.State = ddlState.SelectedValue;
                            //string[] SelectedStateValues = ddlState.SelectedValue.Split('_');
                            //string SelectedState = SelectedStateValues[0].Trim();
                            //if (SelectedStateValues[1].Trim().ToLower() == "true" && hdnIsSCRLiveRunnerLocation.Value == "1")
                            //{
                            //    IsLiveRunnerState = true;
                            //}
                            //ObjtblOrderSearchedData.SearchedStateId = int.Parse(SelectedState);
                        }
                    }
                }

                ObjtblOrderSearchedData.SearchedZipCode = APPLICANTINFO.Applicant.Zip;// txtZip.Text.Trim();

                //if (ddlJurisdiction.SelectedValue != "-1")
                //{
                //    ObjtblOrderSearchedData.search_county = ddlJurisdiction.SelectedItem.Text.Trim();
                //    ObjtblOrderSearchedData.SearchedCountyId = Convert.ToInt32(ddlJurisdiction.SelectedValue);
                //}
                if (!String.IsNullOrEmpty(APPLICANTINFO.Applicant.Jurisdiction))
                {
                    ObjtblOrderSearchedData.search_county = APPLICANTINFO.Applicant.Jurisdiction;
                    tblJurisdiction objtblJurisdiction = new tblJurisdiction();
                    using (EmergeDALDataContext dx = new EmergeDALDataContext())
                    {
                        objtblJurisdiction = dx.tblJurisdictions.Where(d => d.JurisdictionName == APPLICANTINFO.Applicant.Jurisdiction).FirstOrDefault();
                        if (objtblJurisdiction != null)
                        {
                            ObjtblOrderSearchedData.SearchedCountyId = objtblJurisdiction.pkJurisdictionId;
                        }
                    }
                    // ObjtblOrderSearchedData.SearchedCountyId = Convert.ToInt32(ddlJurisdiction.SelectedValue);
                }

                //ObjtblOrderSearchedData.SearchedCountyId = int.Parse(hidSelectedCounty.Value == "" ? "0" : hidSelectedCounty.Value);

                ObjtblOrderSearchedData.SearchedDriverLicense = APPLICANTINFO.Automotive.DriverLicense;// txtDriverLicense.Text.Trim();
                ObjtblOrderSearchedData.SearchedVehicleVin = APPLICANTINFO.Automotive.VehicleVin;// txtVehicleVin.Text.Trim();
                //BATCHAutomotive.DriverLicense = txtDriverLicense.Text.Trim();
                //BATCHAutomotive.VehicleVin = txtVehicleVin.Text.Trim();

                //if (ddlDLState.SelectedValue != "-1")
                //{
                //    ObjtblOrderSearchedData.SearchedDLStateId = int.Parse(ddlDLState.SelectedValue);
                //    BATCHAutomotive.DLState = ddlDLState.SelectedValue;
                //}

                if (!String.IsNullOrEmpty(APPLICANTINFO.Automotive.DLState))
                {
                    tblState objtblState = new tblState();
                    using (EmergeDALDataContext dx = new EmergeDALDataContext())
                    {
                        objtblState = dx.tblStates.Where(d => d.StateName == APPLICANTINFO.Automotive.DLState).FirstOrDefault();
                        if (objtblState == null)
                        {
                            objtblState = dx.tblStates.Where(d => d.StateCode == APPLICANTINFO.Automotive.DLState).FirstOrDefault();
                        }
                        if (objtblState != null)
                        {
                            ObjtblOrderSearchedData.SearchedDLStateId = objtblState.pkStateId;
                        }
                    }
                }
                // ObjtblOrderSearchedData.SearchedSex = (ddlSex.SelectedValue != "-1") ? ddlSex.SelectedValue : string.Empty;
                ObjtblOrderSearchedData.SearchedSex = (!string.IsNullOrEmpty(APPLICANTINFO.Automotive.Sex)) ? APPLICANTINFO.Automotive.Sex : "";
                //BATCHAutomotive.Sex = (ddlSex.SelectedValue != "-1") ? ddlSex.SelectedValue : string.Empty;
                ObjtblOrderSearchedData.SearchedBusinessName = APPLICANTINFO.Business.BusinessName;//txtBusinessName.Text.Trim();
                ObjtblOrderSearchedData.SearchedBusinessCity = APPLICANTINFO.Business.BusinessCity;//txtBusinessCity.Text.Trim();

                // BATCHBusiness.BusinessName = txtBusinessName.Text.Trim();
                //BATCHBusiness.BusinessCity = txtBusinessCity.Text.Trim();

                //if (ddlBusinessState.SelectedValue != "-1")
                //{
                //    ObjtblOrderSearchedData.SearchedBusinessStateId = int.Parse(ddlBusinessState.SelectedValue);
                //    BATCHBusiness.BusinessState = ddlBusinessState.SelectedValue;
                //}
                if (!String.IsNullOrEmpty(APPLICANTINFO.Business.BusinessState))
                {
                    tblState objtblState = new tblState();
                    using (EmergeDALDataContext dx = new EmergeDALDataContext())
                    {
                        objtblState = dx.tblStates.Where(d => d.StateName == APPLICANTINFO.Business.BusinessState).FirstOrDefault();
                        if (objtblState != null)
                        {
                            ObjtblOrderSearchedData.SearchedBusinessStateId = objtblState.pkStateId;
                        }
                    }
                }

                // ObjtblOrderSearchedData.SearchedTrackingRef = txtTrackingRefCode.Text.Trim();//ddlTrackingRefCode.SelectedValue;
                // ObjtblOrderSearchedData.SearchedTrackingRefId = Convert.ToInt32(ddlTrackingRefCode.SelectedValue);
                ObjtblOrderSearchedData.SearchedTrackingRef = ReferenceCode;
                if (ReferenceCodeId != 0)
                {
                    ObjtblOrderSearchedData.SearchedTrackingRefId = ReferenceCodeId;
                }

                // ObjtblOrderSearchedData.SearchedTrackingNotes = txtTrackingNotes.Text.Trim();
                ObjtblOrderSearchedData.SearchedTrackingNotes = (!String.IsNullOrEmpty(APPLICANTINFO.Tracking.TrackingNotes)) ? APPLICANTINFO.Tracking.TrackingNotes : "";
                ObjtblOrderSearchedData.SearchedApplicantEmail = (!String.IsNullOrEmpty(APPLICANTINFO.Applicant.ApplicantEmail)) ? APPLICANTINFO.Applicant.ApplicantEmail : "";

                #endregion

                #region Insertion in employment table

                List<tblOrderSearchedEmploymentInfo> ObjCollOrderSearchedEmploymentInfo = new List<tblOrderSearchedEmploymentInfo>();
                if (ObjCollProducts.Any(d => d.Contains("EMV")))
                {

                    //for (int iRow = 0; iRow < gvEmployment.Rows.Count; iRow++)
                    //{
                    //    BATCHEmploymentSingle = new clsBatEmployment();

                    //    TextBox txtCompanyName = (TextBox)gvEmployment.Rows[iRow].FindControl("txtCompanyName");
                    //    TextBox txtCompanyCity = (TextBox)gvEmployment.Rows[iRow].FindControl("txtCompanyCity");
                    //    TextBox txtCompanyPhone = (TextBox)gvEmployment.Rows[iRow].FindControl("txtCompanyPhone");
                    //    TextBox txtAliasName = (TextBox)gvEmployment.Rows[iRow].FindControl("txtAliasName");
                    //    DropDownList ddlCompanyState = (DropDownList)gvEmployment.Rows[iRow].FindControl("ddlCompanyState");

                    //    tblOrderSearchedEmploymentInfo ObjtblOrderSearchedEmploymentInfo = new tblOrderSearchedEmploymentInfo();
                    //    ObjtblOrderSearchedEmploymentInfo.SearchedAlias = txtAliasName.Text.Trim();
                    //    ObjtblOrderSearchedEmploymentInfo.SearchedName = txtCompanyName.Text.Trim();
                    //    ObjtblOrderSearchedEmploymentInfo.SearchedCity = txtCompanyCity.Text.Trim();
                    //    ObjtblOrderSearchedEmploymentInfo.SearchedState = ddlCompanyState.SelectedValue;
                    //    ObjtblOrderSearchedEmploymentInfo.SearchedPhone = txtCompanyPhone.Text.Trim() == "(___) ___-____" ? "" : txtCompanyPhone.Text.Trim();

                    //    BATCHEmploymentSingle.AliasName = txtAliasName.Text.Trim();
                    //    BATCHEmploymentSingle.CompanyName = txtCompanyName.Text.Trim();
                    //    BATCHEmploymentSingle.CompanyCity = txtCompanyCity.Text.Trim();
                    //    BATCHEmploymentSingle.CompanyState = ddlCompanyState.SelectedValue;
                    //    BATCHEmploymentSingle.CompanyPhone = txtCompanyPhone.Text.Trim() == "(___) ___-____" ? "" : txtCompanyPhone.Text.Trim();

                    //    ObjtblOrderSearchedEmploymentInfo.SearchedContact = string.Empty;
                    //    ObjtblOrderSearchedEmploymentInfo.SearchedStarted = string.Empty;
                    //    ObjtblOrderSearchedEmploymentInfo.SearchedEnded = string.Empty;
                    //    ObjtblOrderSearchedEmploymentInfo.SearchedEmail = string.Empty;
                    //    ObjtblOrderSearchedEmploymentInfo.SearchedTitleStart = string.Empty;
                    //    ObjtblOrderSearchedEmploymentInfo.SearchedTitleEnd = string.Empty;
                    //    ObjtblOrderSearchedEmploymentInfo.SearchedSupervisorContact = string.Empty;
                    //    ObjtblOrderSearchedEmploymentInfo.SearchedReason = string.Empty;

                    //    ObjCollOrderSearchedEmploymentInfo.Add(ObjtblOrderSearchedEmploymentInfo);
                    //    BATCHEmployment.Add(BATCHEmploymentSingle);
                    //}


                    for (int iRow = 0; iRow < APPLICANTINFO.Employment.Count; iRow++)
                    {

                        tblOrderSearchedEmploymentInfo ObjtblOrderSearchedEmploymentInfo = new tblOrderSearchedEmploymentInfo();
                        ObjtblOrderSearchedEmploymentInfo.SearchedAlias = APPLICANTINFO.Employment.ElementAt(iRow).AliasName;// txtAliasName.Text.Trim();
                        ObjtblOrderSearchedEmploymentInfo.SearchedName = APPLICANTINFO.Employment.ElementAt(iRow).CompanyName; //txtCompanyName.Text.Trim();
                        ObjtblOrderSearchedEmploymentInfo.SearchedCity = APPLICANTINFO.Employment.ElementAt(iRow).CompanyCity; //txtCompanyCity.Text.Trim();
                        var GetStateId = tblStateColl.Where(d => d.StateName == APPLICANTINFO.Employment.ElementAt(iRow).CompanyState).Select(d => new { d.pkStateId }).FirstOrDefault();
                        ObjtblOrderSearchedEmploymentInfo.SearchedState = (GetStateId != null) ? GetStateId.pkStateId.ToString() : ""; //ddlCompanyState.SelectedValue;
                        ObjtblOrderSearchedEmploymentInfo.SearchedPhone = APPLICANTINFO.Employment.ElementAt(iRow).CompanyPhone; //txtCompanyPhone.Text.Trim() == "(___) ___-____" ? "" : txtCompanyPhone.Text.Trim();

                        ObjtblOrderSearchedEmploymentInfo.SearchedContact = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedStarted = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedEnded = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedEmail = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedTitleStart = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedTitleEnd = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedSupervisorContact = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedReason = string.Empty;

                        ObjCollOrderSearchedEmploymentInfo.Add(ObjtblOrderSearchedEmploymentInfo);

                    }
                }

                #endregion

                #region Insertion in education table

                List<tblOrderSearchedEducationInfo> ObjCollOrderSearchedEducationInfo = new List<tblOrderSearchedEducationInfo>();
                if (ObjCollProducts.Any(d => d.Contains("EDV")))
                {
                    //for (int iRow = 0; iRow < gvEducation.Rows.Count; iRow++)
                    //{
                    //    BATCHEducationSingle = new clsBatEducation();

                    //    TextBox txtSchoolName = (TextBox)gvEducation.Rows[iRow].FindControl("txtSchoolName");
                    //    TextBox txtSchoolCity = (TextBox)gvEducation.Rows[iRow].FindControl("txtSchoolCity");
                    //    TextBox txtSchoolPhone = (TextBox)gvEducation.Rows[iRow].FindControl("txtSchoolPhone");
                    //    TextBox txtInstituteAlias = (TextBox)gvEducation.Rows[iRow].FindControl("txtInstituteAlias");
                    //    DropDownList ddlInstituteState = (DropDownList)gvEducation.Rows[iRow].FindControl("ddlInstituteState");

                    //    tblOrderSearchedEducationInfo ObjtblOrderSearchedEducationInfo = new tblOrderSearchedEducationInfo();
                    //    ObjtblOrderSearchedEducationInfo.SearchedSchoolName = txtSchoolName.Text.Trim();
                    //    ObjtblOrderSearchedEducationInfo.SearchedCity = txtSchoolCity.Text.Trim();
                    //    ObjtblOrderSearchedEducationInfo.SearchedPhone = txtSchoolPhone.Text.Trim() == "(___) ___-____" ? "" : txtSchoolPhone.Text.Trim();
                    //    ObjtblOrderSearchedEducationInfo.SearchedAlias = txtInstituteAlias.Text.Trim();
                    //    ObjtblOrderSearchedEducationInfo.SearchedState = ddlInstituteState.SelectedValue;

                    //    BATCHEducationSingle.SchoolName = txtSchoolName.Text.Trim();
                    //    BATCHEducationSingle.SchoolCity = txtSchoolCity.Text.Trim();
                    //    BATCHEducationSingle.SchoolPhone = txtSchoolPhone.Text.Trim() == "(___) ___-____" ? "" : txtSchoolPhone.Text.Trim();
                    //    BATCHEducationSingle.SchoolName = txtInstituteAlias.Text.Trim();
                    //    BATCHEducationSingle.InstituteState = ddlInstituteState.SelectedValue;


                    //    ObjtblOrderSearchedEducationInfo.SearchedStarted = string.Empty;
                    //    ObjtblOrderSearchedEducationInfo.SearchedDegree = string.Empty;
                    //    ObjtblOrderSearchedEducationInfo.SearchedGraduationDt = string.Empty;
                    //    ObjtblOrderSearchedEducationInfo.SearchedEnded = string.Empty;
                    //    ObjtblOrderSearchedEducationInfo.SearchedMajor = string.Empty;
                    //    ObjtblOrderSearchedEducationInfo.SearchedFax = string.Empty;
                    //    ObjtblOrderSearchedEducationInfo.SearchedGrade = string.Empty;
                    //    ObjtblOrderSearchedEducationInfo.SearchedCredit = string.Empty;
                    //    ObjtblOrderSearchedEducationInfo.SearchedEmail = string.Empty;

                    //    ObjCollOrderSearchedEducationInfo.Add(ObjtblOrderSearchedEducationInfo);
                    //    BATCHEducation.Add(BATCHEducationSingle);
                    //} 
                    for (int iRow = 0; iRow < APPLICANTINFO.Education.Count; iRow++)
                    {
                        tblOrderSearchedEducationInfo ObjtblOrderSearchedEducationInfo = new tblOrderSearchedEducationInfo();
                        ObjtblOrderSearchedEducationInfo.SearchedSchoolName = APPLICANTINFO.Education.ElementAt(iRow).SchoolName;//txtSchoolName.Text.Trim();
                        ObjtblOrderSearchedEducationInfo.SearchedCity = APPLICANTINFO.Education.ElementAt(iRow).SchoolCity;//txtSchoolCity.Text.Trim();
                        ObjtblOrderSearchedEducationInfo.SearchedPhone = APPLICANTINFO.Education.ElementAt(iRow).SchoolPhone;// txtSchoolPhone.Text.Trim() == "(___) ___-____" ? "" : txtSchoolPhone.Text.Trim();
                        ObjtblOrderSearchedEducationInfo.SearchedAlias = APPLICANTINFO.Education.ElementAt(iRow).InstituteAlias;// txtInstituteAlias.Text.Trim();

                        var GetStateId = tblStateColl.Where(d => d.StateName == APPLICANTINFO.Education.ElementAt(iRow).InstituteState).Select(d => new { d.pkStateId }).FirstOrDefault();
                        ObjtblOrderSearchedEducationInfo.SearchedState = (GetStateId != null) ? GetStateId.pkStateId.ToString() : "";// ddlInstituteState.SelectedValue;

                        ObjtblOrderSearchedEducationInfo.SearchedStarted = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedDegree = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedGraduationDt = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedEnded = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedMajor = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedFax = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedGrade = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedCredit = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedEmail = string.Empty;

                        ObjCollOrderSearchedEducationInfo.Add(ObjtblOrderSearchedEducationInfo);
                    }
                }

                #endregion

                #region Insertion in Personal table and Question table

                List<tblOrderSearchedPersonalInfo> ObjCollOrderSearchedPersonalInfo = new List<tblOrderSearchedPersonalInfo>();
                List<tblOrderPersonalQtnInfo> objColltblOrderPersonalQtnInfo = new List<tblOrderPersonalQtnInfo>();
                if (ObjCollProducts.Any(d => d.Contains("PRV")))
                {
                    // StringBuilder sb = new StringBuilder();

                    //for (int iRow = 0; iRow < gvPersonalInfo.Rows.Count; iRow++)
                    //{
                    //    BATCHPersonalSingle = new clsBatPersonal();
                    //    Guid pkOrderSearchedPersnlId = Guid.NewGuid();

                    //    #region //For other grid Controls
                    //    TextBox txtReferenceName = (TextBox)gvPersonalInfo.Rows[iRow].FindControl("txtReferenceName");
                    //    TextBox txtReferencePhone = (TextBox)gvPersonalInfo.Rows[iRow].FindControl("txtReferencePhone");

                    //    tblOrderSearchedPersonalInfo ObjtblOrderSearchedPersonalInfo = new tblOrderSearchedPersonalInfo();
                    //    ObjtblOrderSearchedPersonalInfo.SearchedName = txtReferenceName.Text.Trim();
                    //    ObjtblOrderSearchedPersonalInfo.SearchedPhone = txtReferencePhone.Text.Trim() == "(___) ___-____" ? "" : txtReferencePhone.Text.Trim();
                    //    ObjtblOrderSearchedPersonalInfo.pkOrderSearchedPersnlId = pkOrderSearchedPersnlId;

                    //    BATCHPersonalSingle.ReferenceName = txtReferenceName.Text.Trim();
                    //    BATCHPersonalSingle.ReferencePhone = txtReferencePhone.Text.Trim() == "(___) ___-____" ? "" : txtReferencePhone.Text.Trim();

                    //    ObjtblOrderSearchedPersonalInfo.SearchedCity = string.Empty;
                    //    ObjtblOrderSearchedPersonalInfo.SearchedState = string.Empty;
                    //    ObjtblOrderSearchedPersonalInfo.SearchedAltPhone = string.Empty;
                    //    ObjtblOrderSearchedPersonalInfo.SearchedRelation = string.Empty;
                    //    ObjtblOrderSearchedPersonalInfo.SearchedEmail = string.Empty;
                    //    ObjtblOrderSearchedPersonalInfo.SearchedSalary = string.Empty;
                    //    ObjtblOrderSearchedPersonalInfo.SearchedTitle = string.Empty;
                    //    ObjtblOrderSearchedPersonalInfo.SearchedAlias = string.Empty;

                    //    ObjCollOrderSearchedPersonalInfo.Add(ObjtblOrderSearchedPersonalInfo);
                    //    #endregion

                    //    #region For questions
                    //    tblOrderPersonalQtnInfo objtblOrderPersonalQtnInfo = new tblOrderPersonalQtnInfo();
                    //    objColltblOrderPersonalQtnInfo.Add(objtblOrderPersonalQtnInfo);
                    //    BATCHPersonal.Add(BATCHPersonalSingle);
                    //    #endregion
                    //} 
                    for (int iRow = 0; iRow < APPLICANTINFO.Personal.Count; iRow++)
                    {
                        int pkOrderSearchedPersnlId = 0;

                        #region For other grid Controls

                        tblOrderSearchedPersonalInfo ObjtblOrderSearchedPersonalInfo = new tblOrderSearchedPersonalInfo();
                        ObjtblOrderSearchedPersonalInfo.SearchedName = APPLICANTINFO.Personal.ElementAt(iRow).ReferenceName;// txtReferenceName.Text.Trim();
                        ObjtblOrderSearchedPersonalInfo.SearchedPhone = APPLICANTINFO.Personal.ElementAt(iRow).ReferencePhone;// txtReferencePhone.Text.Trim() == "(___) ___-____" ? "" : txtReferencePhone.Text.Trim();
                        ObjtblOrderSearchedPersonalInfo.SearchedAlias = APPLICANTINFO.Personal.ElementAt(iRow).ReferenceAlias;
                        ObjtblOrderSearchedPersonalInfo.pkOrderSearchedPersnlId = pkOrderSearchedPersnlId;

                        ObjtblOrderSearchedPersonalInfo.SearchedCity = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedState = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedAltPhone = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedRelation = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedEmail = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedSalary = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedTitle = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedAlias = string.Empty;

                        ObjCollOrderSearchedPersonalInfo.Add(ObjtblOrderSearchedPersonalInfo);

                        #endregion

                        #region For questions

                        tblOrderPersonalQtnInfo objtblOrderPersonalQtnInfo = new tblOrderPersonalQtnInfo();
                        objColltblOrderPersonalQtnInfo.Add(objtblOrderPersonalQtnInfo);

                        #endregion
                    }

                }

                #endregion

                #region Insertion in License verification table

                List<tblOrderSearchedLicenseInfo> ObjCollOrderSearchedLicenseInfo = new List<tblOrderSearchedLicenseInfo>();

                if (ObjCollProducts.Any(d => d.Contains("PLV")))
                {
                    //for (int iRow = 0; iRow < gvLicenseVerify.Rows.Count; iRow++)
                    //{
                    //    BATCHLicenseSingle = new clsBatLicense();
                    //    TextBox txtLicenseType = (TextBox)gvLicenseVerify.Rows[iRow].FindControl("txtLicenseType");
                    //    TextBox txtLicenseIssueDate = (TextBox)gvLicenseVerify.Rows[iRow].FindControl("txtLicenseIssueDate");
                    //    TextBox txtLicenseNumber = (TextBox)gvLicenseVerify.Rows[iRow].FindControl("txtLicenseNumber");
                    //    TextBox txtLicenseAlias = (TextBox)gvLicenseVerify.Rows[iRow].FindControl("txtLicenseAlias");
                    //    DropDownList ddlLicenseState = (DropDownList)gvLicenseVerify.Rows[iRow].FindControl("ddlLicenseState");

                    //    tblOrderSearchedLicenseInfo ObjtblOrderSearchedLicenseInfo = new tblOrderSearchedLicenseInfo();
                    //    ObjtblOrderSearchedLicenseInfo.pkOrderLicenseVerifyId = Guid.NewGuid();
                    //    ObjtblOrderSearchedLicenseInfo.SearchedLicenseType = txtLicenseType.Text.Trim();
                    //    ObjtblOrderSearchedLicenseInfo.SearchedIssueDate = txtLicenseIssueDate.Text.Trim();
                    //    ObjtblOrderSearchedLicenseInfo.SearchedIssuingState = ddlLicenseState.SelectedValue;
                    //    ObjtblOrderSearchedLicenseInfo.SearchedLicenseNo = txtLicenseNumber.Text.Trim();
                    //    ObjtblOrderSearchedLicenseInfo.SearchedAlias = txtLicenseAlias.Text.Trim();

                    //    BATCHLicenseSingle.LicenseType = txtLicenseType.Text.Trim();
                    //    BATCHLicenseSingle.LicenseIssueDate = txtLicenseIssueDate.Text.Trim();
                    //    BATCHLicenseSingle.LicenseState = ddlLicenseState.SelectedValue;
                    //    BATCHLicenseSingle.LicenseNumber = txtLicenseNumber.Text.Trim();
                    //    BATCHLicenseSingle.LicenseAlias = txtLicenseAlias.Text.Trim();
                    //    BATCHLicenseSingle.pkSearchedOrderId = Guid.NewGuid().ToString();

                    //    ObjtblOrderSearchedLicenseInfo.SearchedLicenseName = string.Empty;
                    //    //ObjtblOrderSearchedLicenseInfo.SearchedLicenseType = string.Empty;
                    //    ObjtblOrderSearchedLicenseInfo.SearchedAuthority = string.Empty;
                    //    ObjtblOrderSearchedLicenseInfo.SearchedPhone = string.Empty;
                    //    ObjtblOrderSearchedLicenseInfo.SearchedExpiryDate = string.Empty;
                    //    ObjtblOrderSearchedLicenseInfo.SearchedStatus = string.Empty;
                    //    ObjCollOrderSearchedLicenseInfo.Add(ObjtblOrderSearchedLicenseInfo);
                    //    BATCHLicense.Add(BATCHLicenseSingle);
                    //}
                    for (int iRow = 0; iRow < APPLICANTINFO.License.Count; iRow++)
                    {
                        tblOrderSearchedLicenseInfo ObjtblOrderSearchedLicenseInfo = new tblOrderSearchedLicenseInfo();
                       // ObjtblOrderSearchedLicenseInfo.pkOrderLicenseVerifyId = Guid.NewGuid();
                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseType = APPLICANTINFO.License.ElementAt(iRow).LicenseType;// txtLicenseType.Text.Trim();
                        ObjtblOrderSearchedLicenseInfo.SearchedIssueDate = APPLICANTINFO.License.ElementAt(iRow).LicenseIssueDate;//txtLicenseIssueDate.Text.Trim();

                        var GetStateId = tblStateColl.Where(d => d.StateName == APPLICANTINFO.License.ElementAt(iRow).LicenseState).Select(d => new { d.pkStateId }).FirstOrDefault();
                        ObjtblOrderSearchedLicenseInfo.SearchedIssuingState = (GetStateId != null) ? GetStateId.pkStateId.ToString() : "";// ddlInstituteState.SelectedValue;

                        //ObjtblOrderSearchedLicenseInfo.SearchedIssuingState = APPLICANTINFO.License.ElementAt(iRow).LicenseState;//ddlLicenseState.SelectedValue;
                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseNo = APPLICANTINFO.License.ElementAt(iRow).LicenseNumber;//txtLicenseNumber.Text.Trim();
                        ObjtblOrderSearchedLicenseInfo.SearchedAlias = APPLICANTINFO.License.ElementAt(iRow).LicenseAlias;//txtLicenseAlias.Text.Trim();

                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseName = string.Empty;
                        //ObjtblOrderSearchedLicenseInfo.SearchedLicenseType = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedAuthority = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedPhone = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedExpiryDate = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedStatus = string.Empty;
                        ObjCollOrderSearchedLicenseInfo.Add(ObjtblOrderSearchedLicenseInfo);
                    }
                }

                if (ObjCollProducts.Any(d => d.Contains("WCV")))
                {
                    for (int iRow = 0; iRow < APPLICANTINFO.License.Count(); iRow++)
                    {
                        tblOrderSearchedLicenseInfo ObjtblOrderSearchedLicenseInfo = new tblOrderSearchedLicenseInfo();
                       // ObjtblOrderSearchedLicenseInfo.pkOrderLicenseVerifyId = Guid.NewGuid();
                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseType = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedIssueDate = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedIssuingState = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseNo = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedAlias = APPLICANTINFO.License.ElementAt(iRow).LicenseAlias;

                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseName = string.Empty;
                        //ObjtblOrderSearchedLicenseInfo.SearchedLicenseType = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedAuthority = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedPhone = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedExpiryDate = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedStatus = string.Empty;
                        ObjCollOrderSearchedLicenseInfo.Add(ObjtblOrderSearchedLicenseInfo);
                    }
                }

                #endregion

                #region For Social Diligence

                #region Insertion in Alias PersonalInfo table

               // List<tblOrderSearchedAliasPersonalInfo> ObjCollOrderSearchedAliasPersonalInfo = new List<tblOrderSearchedAliasPersonalInfo>();

                if (ObjCollProducts.Any(d => d.Contains("SNS")))
                {
                    //for (int iRow = 0; iRow < GVAlsoKnownAs.Rows.Count; iRow++)
                    //{

                    //    TextBox txtAliasFname = (TextBox)GVAlsoKnownAs.Rows[iRow].FindControl("txtAliasFname");
                    //    TextBox txtAliasMname = (TextBox)GVAlsoKnownAs.Rows[iRow].FindControl("txtAliasMname");
                    //    TextBox txtAliasLname = (TextBox)GVAlsoKnownAs.Rows[iRow].FindControl("txtAliasLname");

                    //    tblOrderSearchedAliasPersonalInfo objtblOrderSearchedAliasPersonalInfo = new tblOrderSearchedAliasPersonalInfo();
                    //    objtblOrderSearchedAliasPersonalInfo.pkAliasPersonalInfoId = Guid.NewGuid();
                    //    objtblOrderSearchedAliasPersonalInfo.SearchedAliasFirstName = txtAliasFname.Text.Trim();
                    //    objtblOrderSearchedAliasPersonalInfo.SearchedAliasLastName = txtAliasLname.Text.Trim();
                    //    objtblOrderSearchedAliasPersonalInfo.SearchedAliasMiddleInitials = txtAliasMname.Text.Trim();

                    //    ObjCollOrderSearchedAliasPersonalInfo.Add(objtblOrderSearchedAliasPersonalInfo);
                    //}
                }

                #endregion

                #region Insertion in Alias Location Info table

                //List<tblOrderSearchedAliasLocationInfo> ObjCollOrderSearchedAliasLocationInfo = new List<tblOrderSearchedAliasLocationInfo>();

                if (ObjCollProducts.Any(d => d.Contains("SNS")))
                {
                    //for (int iRow = 0; iRow < GVLocations.Rows.Count; iRow++)
                    //{

                    //    TextBox txtSNSCity = (TextBox)GVLocations.Rows[iRow].FindControl("txtSNSCity");
                    //    TextBox txtSNSZip = (TextBox)GVLocations.Rows[iRow].FindControl("txtSNSZip");
                    //    DropDownList ddlLocationState = (DropDownList)GVLocations.Rows[iRow].FindControl("ddlLocationState");

                    //    tblOrderSearchedAliasLocationInfo objtblOrderSearchedAliasLocationInfo = new tblOrderSearchedAliasLocationInfo();
                    //    objtblOrderSearchedAliasLocationInfo.pkAliasLocationId = Guid.NewGuid();
                    //    objtblOrderSearchedAliasLocationInfo.SearchedAliasCity = txtSNSCity.Text.Trim();
                    //    objtblOrderSearchedAliasLocationInfo.SearchedAliasZip = txtSNSZip.Text.Trim();
                    //    objtblOrderSearchedAliasLocationInfo.SearchedLocationState = Convert.ToInt32(ddlLocationState.SelectedValue);

                    //    ObjCollOrderSearchedAliasLocationInfo.Add(objtblOrderSearchedAliasLocationInfo);
                    //}
                }

                #endregion

                #region Insertion in Alias Work Info table

               // List<tblOrderSearchedAliasWorkInfo> ObjCollOrderSearchedAliasWorkInfo = new List<tblOrderSearchedAliasWorkInfo>();

                if (ObjCollProducts.Any(d => d.Contains("SNS")))
                {
                    //for (int iRow = 0; iRow < GVWorkInfo.Rows.Count; iRow++)
                    //{

                    //    TextBox txtWIComName = (TextBox)GVWorkInfo.Rows[iRow].FindControl("txtWIComName");
                    //    TextBox txtWIJobTitle = (TextBox)GVWorkInfo.Rows[iRow].FindControl("txtWIJobTitle");
                    //    TextBox txtWICity = (TextBox)GVWorkInfo.Rows[iRow].FindControl("txtWICity");
                    //    TextBox txtWIZip = (TextBox)GVWorkInfo.Rows[iRow].FindControl("txtWIZip");
                    //    DropDownList ddlWorkInfoState = (DropDownList)GVWorkInfo.Rows[iRow].FindControl("ddlWorkInfoState");

                    //    tblOrderSearchedAliasWorkInfo objtblOrderSearchedAliasWorkInfo = new tblOrderSearchedAliasWorkInfo();
                    //    objtblOrderSearchedAliasWorkInfo.pkAliasWorkInfoId = Guid.NewGuid();
                    //    objtblOrderSearchedAliasWorkInfo.SearchedAliasCompanyName = txtWIComName.Text.Trim();
                    //    objtblOrderSearchedAliasWorkInfo.SearchedAliasJobCity = txtWICity.Text.Trim();
                    //    objtblOrderSearchedAliasWorkInfo.SearchedAliasJobTitle = txtWIJobTitle.Text.Trim();
                    //    objtblOrderSearchedAliasWorkInfo.SearchedAliasJobZip = txtWIZip.Text.Trim();
                    //    objtblOrderSearchedAliasWorkInfo.SearchedJobStateId = Convert.ToInt32(ddlWorkInfoState.SelectedValue);

                    //    ObjCollOrderSearchedAliasWorkInfo.Add(objtblOrderSearchedAliasWorkInfo);
                    //}
                }

                #endregion

                #endregion

                #region Insertion in LiveRunnerInfo table For CCR's Reports

                List<tblOrderSearchedLiveRunnerInfo> ObjColltblOrderSearchedLiveRunnerInfo = new List<tblOrderSearchedLiveRunnerInfo>();
                if (ObjCollProducts.Any(d => d.Contains("CCR1")) || ObjCollProducts.Any(d => d.Contains("CCR2")) || ObjCollProducts.Any(d => d.Contains("RCX")))
                {
                    // List of the Live Runner Products
                    // List<string> ObjCollRcxProducts = (List<string>)ViewState["ObjCollRcxProducts"];
                    //for (int iRow = 0; iRow < grdCounty.Rows.Count; iRow++)
                    //{
                    //    BATCHCountySingle = new clsBatCounty();
                    //    DropDownList ddlCountyInfoState = (DropDownList)grdCounty.Rows[iRow].FindControl("ddlCountyInfoState");
                    //    DropDownList ddlCountyInfoCounty = (DropDownList)grdCounty.Rows[iRow].FindControl("ddlCountyInfoCounty");
                    //    string[] CountyValueData = ddlCountyInfoCounty.SelectedValue.Trim().Split('_');
                    //    int CountyId = Convert.ToInt32(CountyValueData[0]);
                    //    string IsRcxCounty = CountyValueData[1];
                    //    tblOrderSearchedLiveRunnerInfo ObjBALtblOrderSearchedLiveRunnerInfo = new tblOrderSearchedLiveRunnerInfo();
                    //    ObjBALtblOrderSearchedLiveRunnerInfo.LiveRunnerCounty = ddlCountyInfoCounty.SelectedItem.Text.Trim() + "_" + IsRcxCounty;
                    //    ObjBALtblOrderSearchedLiveRunnerInfo.LiveRunnerState = ddlCountyInfoState.SelectedValue;
                    //    ObjBALtblOrderSearchedLiveRunnerInfo.SearchedCountyId = CountyId;

                    //    BATCHCountySingle.CountyInfoCounty = ddlCountyInfoCounty.SelectedValue;
                    //    BATCHCountySingle.CountyInfoState = ddlCountyInfoCounty.SelectedItem.Text.Trim();
                    //    // BATCHCountySingle.State = ddlCountyInfoState.SelectedValue;
                    //    //  BATCHCountySingle.pkCountyId = CountyId.ToString();
                    //    //  BATCHCountySingle.LiveRunnerMessage = "";

                    //    ObjColltblOrderSearchedLiveRunnerInfo.Add(ObjBALtblOrderSearchedLiveRunnerInfo);
                    //    BATCHCounty.Add(BATCHCountySingle);
                    //}
                    for (int iRow = 0; iRow < APPLICANTINFO.County.Count; iRow++)
                    {
                        tblOrderSearchedLiveRunnerInfo ObjBALtblOrderSearchedLiveRunnerInfo = new tblOrderSearchedLiveRunnerInfo();

                        tblCounty objtblCounty = new tblCounty();
                        if (!String.IsNullOrEmpty(APPLICANTINFO.County.ElementAt(iRow).CountyInfoCounty))
                        {
                            using (EmergeDALDataContext dx = new EmergeDALDataContext())
                            {
                                objtblCounty = dx.tblCounties.Where(d => d.CountyName == APPLICANTINFO.County.ElementAt(iRow).CountyInfoCounty).FirstOrDefault();
                                if (objtblCounty != null)
                                {
                                    ObjBALtblOrderSearchedLiveRunnerInfo.LiveRunnerCounty = objtblCounty.CountyName + "_" + objtblCounty.IsRcxCounty.ToString();

                                    var GetStateId = tblStateColl.Where(d => d.StateName == APPLICANTINFO.County.ElementAt(iRow).CountyInfoState).Select(d => new { d.pkStateId }).FirstOrDefault();
                                    ObjBALtblOrderSearchedLiveRunnerInfo.LiveRunnerState = (GetStateId != null) ? GetStateId.pkStateId.ToString() : "";// ddlInstituteState.SelectedValue;

                                    ObjBALtblOrderSearchedLiveRunnerInfo.SearchedCountyId = objtblCounty.pkCountyId;
                                }
                            }

                            //ObjBALtblOrderSearchedLiveRunnerInfo.LiveRunnerCounty = ddlCountyInfoCounty.SelectedItem.Text.Trim() + "_" + IsRcxCounty;
                            // ObjBALtblOrderSearchedLiveRunnerInfo.LiveRunnerState = ddlCountyInfoState.SelectedValue;
                            //ObjBALtblOrderSearchedLiveRunnerInfo.SearchedCountyId = CountyId;
                        }

                        ObjColltblOrderSearchedLiveRunnerInfo.Add(ObjBALtblOrderSearchedLiveRunnerInfo);
                    }
                }
                #endregion

                #region Live Runner Information For BAL Prcocessing
               // Dictionary<string, string> dictRcxProducts = new Dictionary<string, string>();
                #endregion

                #region Null DOB
                int IsNullDOB = 0;
                if (!String.IsNullOrEmpty(APPLICANTINFO.Applicant.IsNullDob))
                {
                    bool chkNullDob = false;
                    bool.TryParse(APPLICANTINFO.Applicant.IsNullDob, out chkNullDob);
                    if (chkNullDob)
                        IsNullDOB = 1;
                }
                //if (chkIsNullDOB.Checked)
                //    IsNullDOB = 1;
                #endregion



                #region Calling Business Layer Method for insertion in db
                List<CLSConsentInfo> CollCLSConsentInfo = new List<CLSConsentInfo>();
                CollCLSConsentInfo.Add(new CLSConsentInfo { ConsentFileName = "", ConsentReportType = "", IsConsentEmailOrFax = false, IsConsentRequired = false, });
                EmergeReport ObjEmergeReport = new EmergeReport();
                ObjEmergeReport.ObjCollCLSConsentInfo = CollCLSConsentInfo;
                //if (ObjCollOrderSearchedAliasLocationInfo.Count > 0)
                //{
                //    foreach (tblOrderSearchedAliasLocationInfo item in ObjCollOrderSearchedAliasLocationInfo)
                //    {
                //        setval4nullAnother(item);
                //    }
                //}
                //ObjEmergeReport.ObjCollOrderSearchedAliasLocationInfo = ObjCollOrderSearchedAliasLocationInfo;

                //if (ObjCollOrderSearchedAliasPersonalInfo.Count > 0)
                //{
                //    foreach (tblOrderSearchedAliasPersonalInfo item in ObjCollOrderSearchedAliasPersonalInfo)
                //    {
                //        setval4nullAnother(item);
                //    }
                //}
                //ObjEmergeReport.ObjCollOrderSearchedAliasPersonalInfo = ObjCollOrderSearchedAliasPersonalInfo;

                //if (ObjCollOrderSearchedAliasWorkInfo.Count > 0)
                //{
                //    foreach (tblOrderSearchedAliasWorkInfo item in ObjCollOrderSearchedAliasWorkInfo)
                //    {
                //        setval4nullAnother(item);
                //    }
                //}
                //ObjEmergeReport.ObjCollOrderSearchedAliasWorkInfo = ObjCollOrderSearchedAliasWorkInfo;

                if (ObjCollOrderSearchedEducationInfo.Count > 0)
                {
                    foreach (tblOrderSearchedEducationInfo item in ObjCollOrderSearchedEducationInfo)
                    {
                        setval4nullAnother(item);
                    }
                }
                ObjEmergeReport.ObjCollOrderSearchedEducationInfo = ObjCollOrderSearchedEducationInfo;

                if (ObjCollOrderSearchedEmploymentInfo.Count > 0)
                {
                    foreach (tblOrderSearchedEmploymentInfo item in ObjCollOrderSearchedEmploymentInfo)
                    {
                        setval4nullAnother(item);
                    }
                }
                ObjEmergeReport.ObjCollOrderSearchedEmploymentInfo = ObjCollOrderSearchedEmploymentInfo;
                if (ObjCollOrderSearchedLicenseInfo.Count > 0)
                {
                    foreach (tblOrderSearchedLicenseInfo item in ObjCollOrderSearchedLicenseInfo)
                    {
                        setval4nullAnother(item);
                    }
                }
                ObjEmergeReport.ObjCollOrderSearchedLicenseInfo = ObjCollOrderSearchedLicenseInfo;

                if (ObjCollOrderSearchedPersonalInfo.Count > 0)
                {
                    foreach (tblOrderSearchedPersonalInfo item in ObjCollOrderSearchedPersonalInfo)
                    {
                        setval4nullAnother(item);
                    }
                }
                ObjEmergeReport.ObjCollOrderSearchedPersonalInfo = ObjCollOrderSearchedPersonalInfo;
                ObjEmergeReport.ObjCollProducts = ObjCollProducts;

                if (objColltblOrderPersonalQtnInfo.Count > 0)
                {
                    foreach (tblOrderPersonalQtnInfo item in objColltblOrderPersonalQtnInfo)
                    {
                        setval4nullAnother(item);
                    }
                }
                ObjEmergeReport.objColltblOrderPersonalQtnInfo = objColltblOrderPersonalQtnInfo;

                setval4nullAnother(ObjtblOrder);
                ObjEmergeReport.ObjtblOrder = ObjtblOrder;

                setval4nullAnother(ObjtblOrderSearchedData);
                ObjEmergeReport.ObjtblOrderSearchedData = ObjtblOrderSearchedData;

                if (ObjColltblOrderSearchedLiveRunnerInfo.Count > 0)
                {
                    foreach (tblOrderSearchedLiveRunnerInfo item in ObjColltblOrderSearchedLiveRunnerInfo)
                    {
                        setval4nullAnother(item);
                    }
                }
                ObjEmergeReport.ObjColltblOrderSearchedLiveRunnerInfo = ObjColltblOrderSearchedLiveRunnerInfo;

                ObjEmergeReport.IsNullDOB = IsNullDOB;
                ObjEmergeReport.IsLiveRunnerState = IsLiveRunnerState;
                ObjEmergeReport.ObjTieredPackage = ObjtblTieredPackage;
                ObjEmergeReport.ObjOrderInfo = ObjOrderInfo;
                if (APPLICANTINFO.OwnerInfo.CompanyAccountNo != null)
                {
                    ObjEmergeReport.CompanyAccountNo = APPLICANTINFO.OwnerInfo.CompanyAccountNo;
                }
                else
                {
                    ObjEmergeReport.CompanyAccountNo = string.Empty;
                }
                if (APPLICANTINFO.OwnerInfo.OrderCompanyName != null)
                {
                    ObjEmergeReport.OrderCompanyName = APPLICANTINFO.OwnerInfo.OrderCompanyName;
                }
                else
                {
                    ObjEmergeReport.OrderCompanyName = string.Empty;
                }
                if (APPLICANTINFO.OwnerInfo.PermissiblePurpose != null)
                {
                    ObjEmergeReport.PermissiblePurpose = APPLICANTINFO.OwnerInfo.PermissiblePurpose;
                }
                else
                {
                    ObjEmergeReport.PermissiblePurpose = string.Empty;
                }

                OrderId = InsertSingleOrder(ObjEmergeReport);
                // Session["EmergeReport"] = ObjEmergeReport;
                // Response.Redirect("Searching.aspx", false);
                #endregion
                // }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
            return OrderId;
        }

        public int InserIntoDB(EmergeReport ObjEmergeReport)
        {
            int OrderId = 0;
            //BALOrders ObjBALOrders = new BALOrders();
            //OrderId = ObjBALOrders.AddOrders(ObjEmergeReport.ObjtblOrder,
            //            ObjEmergeReport.ObjCollProducts,
            //            ObjEmergeReport.ObjtblOrderSearchedData,
            //            ObjEmergeReport.ObjCollOrderSearchedEmploymentInfo,
            //            ObjEmergeReport.ObjCollOrderSearchedEducationInfo,
            //            ObjEmergeReport.ObjCollOrderSearchedPersonalInfo,
            //            ObjEmergeReport.objColltblOrderPersonalQtnInfo,
            //            ObjEmergeReport.ObjCollOrderSearchedLicenseInfo,
            //            ObjEmergeReport.ObjCollOrderSearchedAliasPersonalInfo,
            //            ObjEmergeReport.ObjCollOrderSearchedAliasLocationInfo,
            //            ObjEmergeReport.ObjCollOrderSearchedAliasWorkInfo,
            //            ObjEmergeReport.ObjCompnyProductInfo,
            //            ObjEmergeReport.ObjColltblOrderSearchedLiveRunnerInfo,
            //            ObjEmergeReport.ObjCollCLSConsentInfo,
            //            Utility.SiteApplicationId, ObjEmergeReport.IsNullDOB,
            //            ObjEmergeReport.IsLiveRunnerState, ObjEmergeReport.ObjTieredPackage, ObjEmergeReport.ObjOrderInfo);

            // Response.Write(OrderId.ToString());
            return OrderId;
        }


        public static string GetTextWithLength(string str, int Length)
        {
            string sContent = string.Empty;
            sContent = str;
            if (sContent.Length > Length)
            {
                sContent = sContent.Substring(0, Length);
            }
            return sContent;
        }


        public static string GeneraterRandom()
        {
            Random ObjRandom = new Random();
            string strChars = "Q,E,W,R,T,Y,U,I,O,P,A,S,D,F,G,H,J,K,L,Z,X,C,V,B,N,M";
            string strNums = "8,6,3,1,9,2,5,4,7,0";
            string[] arrNumSplit = strNums.Split(',');
            string[] arrCharSplit = strChars.Split(',');
            string strRandom = "";


            for (int i = 0; i < 1; i++)
            {
                int iRandom = ObjRandom.Next(0, arrCharSplit.Length - 1);
                strRandom += arrCharSplit[iRandom].ToString();
            }
            for (int i = 0; i < 5; i++)
            {
                int iRandom = ObjRandom.Next(0, arrNumSplit.Length - 1);
                strRandom += arrNumSplit[iRandom].ToString();
            }

            return strRandom;
        }
        public static string RandomStr()
        {
            string rStr = Path.GetRandomFileName();
            rStr = rStr.Replace(".", "");
            return rStr;
        }
        public static object setval4null(object myObject)
        {
            #region Null Properties convert to String.empty
            object anv = new object();
            foreach (var propertyInfo in myObject.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(myObject, null) == null)
                    {
                        propertyInfo.SetValue(myObject, string.Empty, null);
                    }
                }
            }
            anv = myObject;
            return anv;
            #endregion
        }
        public static void setval4nullAnother(object myObject)
        {
            #region Null Properties convert to String.empty

            foreach (var propertyInfo in myObject.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(myObject, null) == null)
                    {
                        propertyInfo.SetValue(myObject, string.Empty, null);
                    }
                }
            }

            #endregion
        }
        public static object Cell2Property(object myObject, List<string> ProertyColl, string PropertyValue)
        {
            #region Null Properties convert to String.empty
            object anv = new object();
            foreach (var propertyInfo in myObject.GetType().GetProperties())
            {
                if (ProertyColl.Any(d => d.Contains(propertyInfo.Name)))
                {
                    object prop = propertyInfo.GetValue(myObject, null);
                    propertyInfo.SetValue(prop, PropertyValue, null);
                    //if (propertyInfo.GetValue(myObject, null) == null)
                    //{
                    //    propertyInfo.SetValue(myObject, DateTime.Now.ToFileTime().ToString(), null);
                    //}
                }
            }
            anv = myObject;
            return anv;
            #endregion
        }

        #endregion




    }

    [Serializable]
    public class ApplicantInfo
    {
        [XmlAttribute("ApplicantNumber")]
        public string ApplicantNumber { get; set; }
        [XmlAttribute("IsApplicantProcessed")]
        public string IsApplicantProcessed { get; set; }
        public clsBatOwnerInfo OwnerInfo { get; set; }
        public List<string> ProductColl { get; set; }
        Dictionary<string, Guid> CompanyProductInfo { get; set; }
        public clsBatApplicant Applicant { get; set; }
        public List<clsBatCounty> County { get; set; }
        public clsBatAutomotive Automotive { get; set; }
        public List<clsBatEmployment> Employment { get; set; }
        public List<clsBatEducation> Education { get; set; }
        public List<clsBatPersonal> Personal { get; set; }
        public List<clsBatLicense> License { get; set; }
        public clsBatBusiness Business { get; set; }
        public clsBatTracking Tracking { get; set; }
    }

    [Serializable]
    public class clsBatOwnerInfo
    {
        public int CompanyUserId { get; set; }
        public int LocationId { get; set; }
        public int CompanyId { get; set; }
        public string PermissiblePurpose { get; set; }
        public string OrderCompanyName { get; set; }
        public string CompanyAccountNo { get; set; }
    }

    [Serializable]
    public class clsBatApplicant
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MI { get; set; }
        public string Suffix { get; set; }
        public string Social { get; set; }
        public string DOB { get; set; }
        public string IsNullDob { get; set; }
        public string Phone { get; set; }
        public string StreetNumber { get; set; }
        public string Direction { get; set; }
        public string StreetName { get; set; }
        public string Type { get; set; }
        public string Apt { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Jurisdiction { get; set; }
        public string ApplicantEmail { get; set; } 
    }
    [Serializable]
    public class clsBatCounty
    {       
        public string CountyInfoCounty { get; set; }
        public string CountyInfoState { get; set; }       
    }
    [Serializable]
    public class clsBatAutomotive
    {
        public string DLState { get; set; }
        public string DriverLicense { get; set; }
        public string VehicleVin { get; set; }
        public string Sex { get; set; }
    }
    [Serializable]
    public class clsBatEmployment
    {
        public string CompanyName { get; set; }
        public string CompanyState { get; set; }
        public string CompanyCity { get; set; }
        public string CompanyPhone { get; set; }
        public string AliasName { get; set; }
    }
    [Serializable]
    public class clsBatEducation
    {
        public string SchoolName { get; set; }
        public string InstituteState { get; set; }
        public string SchoolCity { get; set; }
        public string SchoolPhone { get; set; }
        public string InstituteAlias { get; set; }
    }
    [Serializable]
    public class clsBatPersonal
    {
        public string ReferenceName { get; set; }
        public string ReferencePhone { get; set; }
        public string ReferenceAlias { get; set; }
    }
    [Serializable]
    public class clsBatLicense
    {
        public string pkSearchedOrderId { get; set; }
        public string LicenseType { get; set; }
        public string LicenseNumber { get; set; }
        public string LicenseIssueDate { get; set; }
        public string LicenseState { get; set; }
        public string LicenseAlias { get; set; }
    }
    [Serializable]
    public class clsBatBusiness
    {
        public string BusinessName { get; set; }
        public string BusinessCity { get; set; }
        public string BusinessState { get; set; }
    }
    [Serializable]
    public class clsBatTracking
    {
        public string SearchedTrackingRefId { get; set; }
        public string TrackingRefCode { get; set; }
        public string TrackingNotes { get; set; }
    }
}
