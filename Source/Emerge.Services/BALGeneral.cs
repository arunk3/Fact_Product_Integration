﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Configuration;
using System.Net.Mail;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Emerge.Services.Helper;
using iTextSharp.text.html.simpleparser;
using System.Net;
using System.Web;
using MomentumInfotech.EmailProvider;
using System.Globalization;
using System.Web.Security;
using System.Text.RegularExpressions;


namespace Emerge.Services
{
    public class BALGeneral
    {
        public BALGeneral()
        {

        }
        public BALGeneral(string rcxfilepath)
        {
            RCXFilePath = rcxfilepath;
        }
        public static string RCXFilePath
        {
            get;
            set;
        }

        //INT 219 SSN duplicate report alert within 30 days.
        public int SSNReportcount(string social, int companyid)
        {
            int duplicateReportResult = 0;
            using (EmergeDALDataContext _EmergeDALDataContext = new EmergeDALDataContext())
            {
                // retrive the report count containing Specific SSN for login company user.
                bool ReportAlertStatus = Convert.ToBoolean(HttpContext.Current.Session["IsDuplicateSSNReportAlertEnabled"]);
                if (ReportAlertStatus == true)
                {
                    var ReportCountResult = _EmergeDALDataContext.GetDuplicateSSNReportCount(social, companyid).FirstOrDefault();
                    if (ReportCountResult.TotalReportCount != 0)
                    {
                        duplicateReportResult = 1;
                    }
                }
            }
            return duplicateReportResult;
        }

        public List<Proc_Get_AutoCompleteForArchiveResult> GetAutoCompleteForArchive(string keyword, Guid fkUserId, bool IsAdmin)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return DX.GetAutoCompleteForArchive(keyword, fkUserId, IsAdmin).ToList<Proc_Get_AutoCompleteForArchiveResult>();
            }
        }
        public List<tblState> GetStates()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.tblStates.Where(rec => rec.pkStateId != 60).OrderBy(db => db.StateName).ToList();
            }
        }

        public List<Proc_GetSavedEmergeReviewForAdminResult> GetSavedEmergeReviewList(int ProductId, int CompanyUserId, int CompanyId,
                                                            int LocationId, string SortingColumn, string SortingDirection,
                                                            bool AllowPaging, int PageNum, int PageSize,
                                                            string FromDate, string ToDate, string SearchKeyword, byte OrderStatus, string ReportStatus, bool ShowIncomplete, string ReferenceCode, bool SavedReport6Month)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetSavedEmergeReviewForAdmin(ProductId, CompanyUserId, CompanyId,
                                                                       LocationId, SortingColumn, SortingDirection,
                                                                       AllowPaging, PageNum, PageSize,
                                                                       FromDate, ToDate, SearchKeyword, OrderStatus, ReportStatus, ShowIncomplete, ReferenceCode, SavedReport6Month).ToList();

            }
        }



        public List<tblCity> GetCities()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                IQueryable<tblCity> ObjCollection = ObjDALDataContext.tblCities.OrderBy(db => db.City).Select(db => db);
                return ObjCollection.ToList();
            }
        }

        public List<tblCompanyType> GetCompanyTypes()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                IQueryable<tblCompanyType> ObjCollection = ObjDALDataContext.tblCompanyTypes.OrderBy(db => db.CompanyType).Select(db => db);
                return ObjCollection.ToList();
            }
        }

        public List<tblCompany> GetMainEmailId()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                IQueryable<tblCompany> ObjCollection = ObjDALDataContext.tblCompanies.OrderBy(db => db.MainEmailId).Select(db => db);
                return ObjCollection.ToList();
            }
        }

        public List<tblLocation> GetMainPhone()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                IQueryable<tblLocation> ObjCollection = ObjDALDataContext.tblLocations.OrderBy(db => db.PhoneNo1).Select(db => db);
                return ObjCollection.ToList();
            }
        }

        public List<clsStates> GetStatesNR()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                var Response = from d in ObjDALDataContext.tblStates
                               where d.pkStateId != 60

                               select new clsStates
                               {
                                   pkStateId = Convert.ToString(d.pkStateId + "_" + d.IsStateLiveRunner + "_" + d.IsConsentForm_SCR),
                                   StateName = d.StateName
                               };
                return Response.OrderBy(db => db.StateName).ToList();
            }
        }

        public List<clsStates> GetStatesSCR()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                var Response = (from d in ObjDALDataContext.GetStatesWithConsent()
                                where d.pkStateId != 60

                                select new clsStates
                                {
                                    pkStateId = Convert.ToString(d.pkStateId + "_" + d.IsStateLiveRunner + "_" + d.IsConsent),
                                    StateName = d.StateName
                                });
                return Response.OrderBy(db1 => db1.StateName).ToList();
            }
        }

        public List<clsStates> GetStatesMVR()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                var Response = from d in ObjDALDataContext.tblStates
                               join db in ObjDALDataContext.tblStateFees on
                               d.pkStateId equals db.fkStateId
                               join dbProduct in ObjDALDataContext.tblProducts on
                               db.fkProductId equals dbProduct.pkProductId
                               where dbProduct.ProductCode.ToLower() == "mvr"
                               select new clsStates
                               {
                                   pkStateId = Convert.ToString(d.pkStateId + "_" + db.IsConsent),
                                   StateName = d.StateName
                               };
                return Response.OrderBy(db => db.StateName).ToList();
            }
        }

        public List<clsStates> GetStatesNR1()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                var Response = from d in ObjDALDataContext.tblStates
                               where d.pkStateId != 60
                               select new clsStates
                               {
                                   pkStateId = Convert.ToString(d.pkStateId + "_" + d.IsConsentForm_MVR),
                                   StateName = d.StateName
                               };
                return Response.OrderBy(db => db.StateName).ToList();
            }
        }
        public List<clsCounty> GetStatesCountiesNR(int StateId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                var Response = from d in ObjDALDataContext.tblCounties
                               where d.fkStateId == StateId
                               select new clsCounty
                               {
                                   pkCountyId = Convert.ToString(d.pkCountyId + "_" + d.IsRcxCounty + "_" + d.IsConsentForm_CCR + "_" + d.IsConsentForm_RCX + "_" + d.CountyName),
                                   CountyName = d.CountyName,
                                   IsRcxCounty = d.IsRcxCounty
                               };
                return Response.OrderBy(d => d.CountyName).ToList();
            }
        }

        public List<clsCounty> GetStatesCountiesWithConsent(int StateId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {

                var Response = from d in ObjDALDataContext.GetCountyWithConsent(StateId)
                               select new clsCounty
                               {
                                   pkCountyId = Convert.ToString(d.pkCountyId + "_" + d.IsRcxCounty + "_" + d.IsConsent + d.CountyName),//d.IsConsent is the multiple values from sp
                                   CountyName = d.CountyName.Trim(),
                                   IsRcxCounty = d.IsRcxCounty
                               };
                return Response.OrderBy(d => d.CountyName).ToList();
            }
        }

        public List<clsCounty> GetStatesCounties(int StateId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                var Response = from d in ObjDALDataContext.tblCounties
                               where d.fkStateId == StateId
                               select new clsCounty
                               {
                                   pkCountyId = Convert.ToString(d.pkCountyId),
                                   CountyName = d.CountyName
                               };
                return Response.OrderBy(d => d.CountyName).ToList();
            }
        }


        public List<tblCounty> GetStatesCounty(int StateId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {

                var Response = (from d in ObjDALDataContext.tblCounties
                                where d.fkStateId == StateId
                                select d).ToList();
                return Response;
            }
        }

        public List<tblCounty> GetCountyInfo(int CountyId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {

                var Response = (from d in ObjDALDataContext.tblCounties
                                where d.pkCountyId == CountyId
                                select d).ToList();
                return Response;
            }
        }

        public List<tblState> GetStateInfo(int StateId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {

                var Response = (from d in ObjDALDataContext.tblStates
                                where d.pkStateId == StateId
                                select d).ToList();
                return Response;
            }
        }


        public int CountReferenceCode(string ReferenceCode)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return DX.tblOrders.Where(d => d.ReferenceCode == ReferenceCode).Count();
            }
        }
        public List<clsCounty> GetStatesCountiesAll()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                var Response = from d in ObjDALDataContext.tblCounties
                               select new clsCounty
                               {
                                   pkCountyId = Convert.ToString(d.pkCountyId),
                                   CountyName = d.CountyName
                               };
                return Response.OrderBy(d => d.CountyName).ToList();
            }
        }

        /// <summary>
        /// This function is used to Get Categories of Reports.
        /// </summary>
        /// <returns></returns>
        public List<Proc_Get_ReportCategoriesResult> GetReportCategories()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetReportCategories().ToList();
            }
        }
        public List<Proc_Get_NewReportsByCategoryIdResult> GetNewReportsByCategoryId(int LocationId, byte ReportCategoryId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetNewReportsByCategoryId(LocationId, ReportCategoryId).ToList<Proc_Get_NewReportsByCategoryIdResult>();
            }
        }

        public List<Proc_Get_NewReportsForLandingPageResult> GetNewReportsForLandingPage(int LocationId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetNewReportsForLandingPage(LocationId).ToList<Proc_Get_NewReportsForLandingPageResult>();
            }
        }


        public List<Proc_Get_ReportsByCategoryIdResult> GetReportsByCategoryId(int LocationId, byte ReportCategoryId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.Get_ReportsByCategoryId(LocationId, ReportCategoryId).ToList<Proc_Get_ReportsByCategoryIdResult>();
            }
        }

        /// <summary>
        /// This function is used to Get Products of Categories by Id.
        /// </summary>
        /// <param name="LocationId"></param>
        /// <param name="ReportCategoryId"></param>
        /// <param name="CompanyId"></param>
        /// <returns></returns>
        public List<Proc_GetReportProductsByIdResult> GetReportProductsById(int LocationId, byte ReportCategoryId, int CompanyId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetReportProductsById(LocationId, ReportCategoryId, CompanyId).ToList();
            }
        }
        /// <summary>
        /// Function To get State Information If StateID is Passed
        /// </summary>
        public List<Proc_Get_StateByStateIdResult> GetStateByStateId(int StateId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetStateByStateId(StateId).ToList<Proc_Get_StateByStateIdResult>();
            }
        }

        public bool IsStateHasConsentOption(int pkId, string TableName)
        {
            bool IsConsentOption = false;
            try
            {
                using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
                {
                    if (!string.IsNullOrEmpty(TableName))
                    {
                        if (TableName == "State")
                        {
                            IsConsentOption = ObjDALDataContext.tblStates.Where(d => d.pkStateId == pkId).FirstOrDefault().IsConsentForm_SCR;
                        }
                        else if (TableName == "County")
                        {
                            IsConsentOption = ObjDALDataContext.tblCounties.Where(d => d.pkCountyId == pkId).FirstOrDefault().IsConsentForm_CCR;
                        }
                        else if (TableName == "Jurisdiction")
                        {
                            IsConsentOption = ObjDALDataContext.tblJurisdictions.Where(d => d.pkJurisdictionId == pkId).FirstOrDefault().IsConsentForm;
                        }
                    }

                }

            }
            catch
            {
            }
            return IsConsentOption;
        }
        public List<Proc_Get_EmergeProductPackagesResult> GetEmergeProductPackages(int LocationId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetEmergeProductPackages(LocationId).ToList();
            }
        }

        public List<Proc_Get_EmergeProductPackageReportsResult> GetEmergeProductPackageReports(int LocationId, int PackageId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetEmergeProductPackageReports(LocationId, PackageId).ToList();
            }
        }

        public string GetEmergeProductPackageReportsNew(int LocationId, int PackageId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.Get_EmergeProductPackageReportsNew(LocationId, PackageId).ElementAt(0).PackageReports;
            }
        }
        /// <summary>
        /// Function To Add Report Error To the Error Log
        /// </summary>
        /// <param name="StateId"></param>
        /// <returns></returns>
        public Guid AddReportErrorToErrorLog(tblReportErrorLog ObjtblReportErrorLog)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return new Guid(ObjDALDataContext.AddReportError(ObjtblReportErrorLog.ReportError,
                                                                 ObjtblReportErrorLog.fkProductId,
                                                                 ObjtblReportErrorLog.fkCompanyUserId,
                                                                 ObjtblReportErrorLog.fkOrderId
                                                                 ).ToString());
            }
        }

        /// <summary>
        /// Function To Get All the Roles According To Application Id Passed
        /// </summary>
        /// <param name="ApplicationId"></param>
        /// <returns></returns>
        public List<aspnet_Role> FetchRolesExceptAdmin(Guid ApplicationId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.aspnet_Roles.Where(data => data.ApplicationId == ApplicationId && data.RoleName != "Admin" && data.RoleName != "SystemAdmin").ToList();
            }
        }

        public List<aspnet_Role> FetchRoles(Guid ApplicationId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.aspnet_Roles.Where(data => data.ApplicationId == ApplicationId && data.RoleName != "SystemAdmin").ToList();
            }
        }

        public List<aspnet_Role> FetchRolesAll(Guid ApplicationId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.aspnet_Roles.Where(data => data.ApplicationId == ApplicationId).ToList();
            }
        }
        public tblEmailTemplate GetEmailTemplate(Guid applicationID, string template_Name)
        {
            tblEmailTemplate ObjtblEmailTemplate = new tblEmailTemplate();
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                if (template_Name != "")
                {
                    ObjtblEmailTemplate = Dx.tblEmailTemplates.Where(t => t.fkApplicationId == applicationID && t.TemplateName == template_Name).FirstOrDefault();
                }
            }
            return ObjtblEmailTemplate;
        }

        public tblEmailTemplate GetEmailTemplate(Guid applicationID, int pkTemplateId)
        {
            tblEmailTemplate ObjtblEmailTemplate = new tblEmailTemplate();
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                if (pkTemplateId != 0)
                {
                    ObjtblEmailTemplate = Dx.tblEmailTemplates.Where(t => t.fkApplicationId == applicationID && t.pkTemplateId == pkTemplateId).FirstOrDefault();
                }
            }
            return ObjtblEmailTemplate;
        }
        public tblGeneralSetting GetSettings(byte settingId)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                return Dx.tblGeneralSettings.Where(t => t.pkSettingId == settingId).FirstOrDefault();
            }
        }
        public int UpdateSettings(tblGeneralSetting objSettings)
        {
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                var dbsettings = dx.tblGeneralSettings.Where(t => t.pkSettingId == 1).FirstOrDefault();
                if (dbsettings != null)
                {
                    dbsettings.PageContents = objSettings.PageContents;
                    dx.SubmitChanges();
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
        }
        public int SaveFilters_keywords(string tem_keyword, int filter_id)
        {
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                var query = from c_output in dx.tbl7YearFilters
                            where c_output.pkFilterId == 1
                            select c_output;
                if (query.Count() > 0)
                {
                    query.First().Keywords = tem_keyword;
                    dx.SubmitChanges();
                    return 2;
                }
                else
                {
                    tbl7YearFilter Objtbl7YearFilter = new tbl7YearFilter();
                    Objtbl7YearFilter.Keywords = tem_keyword;
                    Objtbl7YearFilter.pkFilterId = filter_id;
                    dx.tbl7YearFilters.InsertOnSubmit(Objtbl7YearFilter);
                    dx.SubmitChanges();
                    return 1;
                }
            }
        }
        public string Get_Filter_Keywords()
        {
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                //tbl7YearFilter Objtbl7YearFilter = new tbl7YearFilter();
                var list = (from t in dx.tbl7YearFilters
                            where t.pkFilterId == 1
                            select t.Keywords).SingleOrDefault();
                return (string)list;
            }
        }
        public List<proc_GetStateCountyResult> GetStateCountyFee(int stateId)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                return Dx.GetStateCounty(stateId).ToList();
            }
        }

        public List<proc_GetStateCountyForRCXResult> GetStateCountyForRCX(int stateId)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                return Dx.GetStateCountyForRCX(stateId).ToList();
            }
        }

        public List<proc_GetStateCountyForCCRResult> GetStateCountyForCCR(int stateId, String ProductCode)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                return Dx.GetStateCountyForCCR(stateId, ProductCode).ToList();
            }
        }

        public tblCounty GetCountyFees(int pkCountyId)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                return Dx.tblCounties.Where(t => t.pkCountyId == pkCountyId).FirstOrDefault();
            }
        }

        public int UpdateCountyFees(List<tblCounty> lstCounty)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                if (lstCounty.Count > 0)
                {
                    foreach (tblCounty obj in lstCounty)
                    {
                        DX.UpdateStateCountyFee(obj.pkCountyId, obj.CCRFee, obj.IsRcxCounty, obj.RcxFee, obj.IsConsentForm_RCX);
                    }
                    return 1;
                }
                return 0;
            }
        }

        public int UpdateStateCountyFeeRCX(List<tblCountyFee> lstCounty)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                if (lstCounty.Count > 0)
                {
                    foreach (tblCountyFee obj in lstCounty)
                    {
                        DX.UpdateStateCountyFeeRCXAndCCR(obj.pkCountyFeeId, obj.fkProductId, obj.AdditionalFee, obj.IsConsent, obj.DisclaimerData);
                    }
                    return 1;
                }
                return 0;
            }
        }

        public int UpdateCountyFees_CCR(List<tblCounty> lstCounty, List<tblCountyFee> lstCountyFee)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                if (lstCounty.Count > 0)
                {
                    foreach (tblCounty obj in lstCounty)
                    {
                        DX.proc_UpdateStateCountyFeeCCR(obj.pkCountyId, obj.CCRFee, obj.IsRcxCounty, obj.RcxFee, obj.IsConsentForm_CCR);
                    }
                    foreach (tblCountyFee obj in lstCountyFee)
                    {
                        DX.UpdateStateCountyFeeRCXAndCCR(obj.pkCountyFeeId, obj.fkProductId, obj.AdditionalFee, obj.IsConsent, obj.DisclaimerData);
                    }
                    foreach (tblCountyFee obj in lstCountyFee)
                    {
                        DX.UpdateStateCountyFeeRCXAndCCR(obj.pkCountyFeeId, 4, obj.AdditionalFee, obj.IsConsent, obj.DisclaimerData);
                    }
                    return 1;
                }
                return 0;
            }
        }
        public List<tblState> GetAllStates()
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                return Dx.tblStates.OrderBy(db => db.StateName).ToList();
            }
        }
        public List<tblState> GetDisabledStates()
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                return Dx.tblStates.Where(db => db.IsEnabled == false).OrderBy(db => db.StateName).ToList();
            }
        }

        public List<clsStateFee> GetAllStatesByCode(string Code)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                int ProductId = Dx.tblProducts.Where(x => x.ProductCode.ToLower() == Code.ToLower()).FirstOrDefault().pkProductId;
                var data = (from db in Dx.tblStates
                            join jn in Dx.tblStateFees
                            on db.pkStateId equals jn.fkStateId
                            where jn.fkProductId == ProductId
                            select new clsStateFee
                            {
                                pkStateId = db.pkStateId,
                                fkCountryId = db.fkCountryId,
                                StateName = db.StateName,
                                StateCode = db.StateCode,
                                IsStateLiveRunner = db.IsStateLiveRunner,
                                LiveRunnerFees = db.LiveRunnerFees,
                                IsEnabled = db.IsEnabled,
                                pkStateFeeId = jn.pkStateFeeId,
                                fkProductId = jn.fkProductId,
                                IsConsent = jn.IsConsent,
                                AdditionalFee = jn.AdditionalFee,
                                DisclaimerData = jn.DisclaimerData
                            }).ToList();
                return data;
            }
        }

        public int UpdateSCRFee(List<tblState> lstState, List<tblStateFee> lstStateFee)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                if (lstState.Count > 0)
                {
                    foreach (tblState obj in lstState)
                    {
                        Dx.UpdateSCRFee(obj.pkStateId, obj.SCRFee, obj.LiveRunnerFees, obj.IsStateLiveRunner, obj.IsConsentForm_SCR, obj.IsEnabled);
                    }
                    foreach (tblStateFee obj in lstStateFee)
                    {
                        Dx.proc_UpdateStateFee(obj.fkStateId, obj.fkProductId, obj.AdditionalFee, obj.IsConsent, obj.DisclaimerData);
                    }
                    return 1;
                }
                return 0;
            }
        }

        public int UpdateStateFee(List<tblStateFee> lstState)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                if (lstState.Count > 0)
                {
                    foreach (tblStateFee obj in lstState)
                    {
                        Dx.proc_UpdateStateFee(obj.fkStateId, obj.fkProductId, obj.AdditionalFee, obj.IsConsent, obj.DisclaimerData);
                    }
                    return 1;
                }
                return 0;
            }
        }

        public int UpdateMVRFee(List<tblState> lstState)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                if (lstState.Count > 0)
                {
                    foreach (tblState obj in lstState)
                    {
                        Dx.UpdateMVRFee(obj.pkStateId, obj.MVRFee, obj.IsConsentForm_MVR);
                    }
                    return 1;
                }
                return 0;
            }
        }

        /// <summary>
        /// Function to Validate the Valid DateTime Object
        /// </summary>
        /// <param name="DateTimeObject"></param>
        /// <returns></returns>
        public bool isValidDateTime(string DateTimeObject)
        {
            try
            {
                DateTime ObjDateTime = Convert.ToDateTime(DateTimeObject);
                return true;
            }
            catch //(Exception ex)
            {
                return false;
            }
        }

        public List<proc_GetComplete_ManualReportResult> GetCompletedManualReports(Guid Userid, bool IsAdmin)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                return Dx.GetComplete_ManualReport(Userid, IsAdmin).ToList();
            }
        }

        public List<Proc_GetAllAlertsByUserIdResult> GetAllAlertsByUserId(Guid Userid)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                return Dx.GetAllAlertsByUserId(Userid).ToList();
            }
        }

        /// <summary>
        /// To delete the alerts.
        /// </summary>
        /// <param name="pkAlertId"></param>
        /// <returns></returns>
        public int DeleteAlerts(int pkAlertId)
        {
            int retValue = 0;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                tblAlert ObjtblAlerts = new tblAlert();
                ObjtblAlerts = ObjDALDataContext.tblAlerts.Where(p => p.pkAlertId == pkAlertId).FirstOrDefault();
                //ObjtblAlerts = ObjDALDataContext.tblAlerts.Single(p => p.pkAlertId == pkAlertId);
                if (ObjtblAlerts != null)
                {
                    ObjDALDataContext.tblAlerts.DeleteOnSubmit(ObjtblAlerts);
                    ObjDALDataContext.SubmitChanges();
                    retValue = 1;
                }
            }
            return retValue;
        }

        public void UpdateOrderDetail_IsViewed(int pkOrderDetailId)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                try
                {
                    tblOrderDetail orderDetail = Dx.tblOrderDetails.Where(t => t.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                    if (orderDetail != null)
                    {
                        orderDetail.IsViewed = true;
                        Dx.SubmitChanges();
                    }
                }
                catch
                {
                    Dx.UpdateIsViewed(pkOrderDetailId);
                }

            }
        }

        public void UpdateOrderDetail_ReviewStatus(int pkOrderDetailId)
        {
            int Rstatus = 0;
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                try
                {
                    tblOrderDetail orderDetail = Dx.tblOrderDetails.Where(t => t.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                    if (orderDetail != null)
                    {
                        orderDetail.ReviewStatus = Rstatus;
                        Dx.SubmitChanges();
                    }
                }
                catch
                {

                }

            }
        }

        /// <summary>
        /// To Get the updated review status of selected order.
        /// </summary>
        /// <param name="pkOrderDetailId"></param>
        /// <returns></returns>
        public int GetOrderDetail_ReviewStatus(int pkOrderDetailId)
        {
            int Rstatus = 0;
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                try
                {
                    tblOrderDetail orderDetail = Dx.tblOrderDetails.Where(t => t.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                    if (orderDetail != null)
                    {
                        Rstatus = orderDetail.ReviewStatus;
                    }
                }
                catch
                {

                }

            }
            return Rstatus;
        }
        ///// <summary>
        ///// Function To Fetch all General Settings
        ///// </summary>
        ///// <returns></returns>
        public static tblGeneralSetting GetGeneralSettings()
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                return Dx.tblGeneralSettings.Where(t => t.pkSettingId == 1).FirstOrDefault();
            }
        }

        /// <summary>
        /// Function To Log Error in Reports
        /// </summary>
        /// <param name="OrderDetailId"></param>
        /// <param name="Error"></param>
        public static void LogError(int? OrderDetailId, string Error)
        {
            using (EmergeDALDataContext ObjUSAIntelDataDataContext = new EmergeDALDataContext())
            {
                tblOrderDetail ObjtblOrderDetail = ObjUSAIntelDataDataContext.tblOrderDetails.Single(data => data.pkOrderDetailId == OrderDetailId);
                ObjtblOrderDetail.ErrorLog = ObjtblOrderDetail.ErrorLog + "   " + System.DateTime.Now.ToString() + "  " + Error;
                ObjUSAIntelDataDataContext.SubmitChanges();
            }
        }

        /// <summary>
        /// Function to Check the Status of Response we Got From Vendor.
        /// </summary>
        /// <param name="Vendor_Response"></param>
        /// <param name="ProductCode"></param>
        /// <returns></returns>
        public static int CheckReportStatus(string Vendor_Response, string ProductCode, bool IsInstantReport, int? pkOrderDetailId, int? OrderId, bool IsPendingReport)
        {

            int ReportStatus = 1;
            BALGeneral ObjBALGeneral = new BALGeneral();
            string fiteredResponse = "";
            /*
             *      1   For Pending
             *      2   For Complete         
             *      3   For Incomplete
             *      4   For Cancel 
             
             */
            bool Objbool = false;
            if (IsValidXml(Vendor_Response) == true)
            {
                try
                {
                    XmlDocument ObjXmlDocument = new XmlDocument();
                    ObjXmlDocument.LoadXml(Vendor_Response);

                    #region INT-116
                    XDocument doc = XDocument.Parse(Vendor_Response);
                    string statusresultValue = string.Empty;
                    string statusdetailsvalue = string.Empty;
                    if (ProductCode == "EDV" || ProductCode == "EMV" || ProductCode == "PRV" || ProductCode == "PLV" || ProductCode == "PRV2")
                    {
                        string OrderStatusVerification = string.Empty;
                        if (ObjXmlDocument.GetElementsByTagName("RPOrderSet").Count > 0)
                        {
                            OrderStatusVerification = ObjXmlDocument.SelectSingleNode("RPOrderSet/RPB2BOrder/OrderStatus").FirstChild.Value;
                        }
                        else
                        {
                            OrderStatusVerification = ObjXmlDocument.SelectSingleNode("RPB2BOrder/OrderStatus").FirstChild.Value;
                        }
                        if ((OrderStatusVerification.Trim().ToUpper() == "PENDING-NEW") || (OrderStatusVerification.Trim().ToUpper() == "IN PROGRESS") || (OrderStatusVerification.Trim().ToUpper() == "REVIEW"))
                        {
                            ReportStatus = 1;
                        }
                        else if ((OrderStatusVerification.Trim().ToUpper() == "COMPLETE") || (OrderStatusVerification.Trim().ToUpper() == "NO LISTING") || (OrderStatusVerification.Trim().ToUpper() == "NO RECORD FOUND") || (OrderStatusVerification.Trim().ToUpper() == "NO RESPONSE") || (OrderStatusVerification.Trim().ToUpper() == "NO REF POLICY"))
                        {
                            ReportStatus = 2;
                        }
                        else
                        {
                            ReportStatus = 2;//INT336 As per Greg requirment, he needs if order status other than Pending application will considered as Complete order status. 
                        }
                    }
                    else if (ProductCode == "WCV")
                    {
                        bool verificationFlag = false;
                        var lstVerificationResult = doc.Descendants("subOrder").ToList();//To check multiple order status.
                        foreach (XElement element in lstVerificationResult)
                        {
                            XAttribute AttrfilledStatus = element.Attribute("filledStatus");
                            XAttribute Attrheld_for_review = element.Attribute("held_for_review");

                            if (AttrfilledStatus.Value.Trim().ToLower() == "filled")//If any one completed
                            {
                                if (Attrheld_for_review.Value.Trim().ToUpper() == "Y")//If filledStatus is filled and held for review is yes then it will be pending.
                                {
                                    verificationFlag = false;
                                    break;//If any one found pending, then break loop.
                                }
                                else
                                {
                                    verificationFlag = true;
                                }
                            }
                            else if (AttrfilledStatus.Value.Trim().ToLower() == "unfilled" || AttrfilledStatus.Value.Trim().ToLower() == "in progress")//If any one pending.
                            {
                                verificationFlag = false;
                                break;//If any one found pending, then break loop.
                            }
                        }
                        if (verificationFlag == true)//Completed.
                        {
                            ReportStatus = 2;
                        }
                        else if (verificationFlag == false)//Pending.
                        {
                            ReportStatus = 1;
                        }
                    }
                    else
                    {

                        if (ProductCode == "PSP")
                        {
                            statusresultValue = doc.Descendants("Status").FirstOrDefault().Value;
                            statusdetailsvalue = doc.Descendants("StatusDetail").FirstOrDefault().Value;
                            if (statusresultValue.Trim().ToLower() == "success")
                            {
                                ReportStatus = 2;
                            }
                            else if (statusresultValue.Trim().ToLower() == "failure" || statusresultValue.Trim().ToLower() == "partial")
                            {
                                if (statusdetailsvalue == "1")
                                {
                                    ReportStatus = 2;
                                }
                                else
                                {
                                    ReportStatus = 1;
                                }
                            }
                            else
                            {
                                ReportStatus = 3;
                            }
                        }
                    }

                    #endregion

                    if (ProductCode == "TDR" || ProductCode == "OFAC" || ProductCode == "BR" || ProductCode == "NCR4" || ProductCode == "MVS" ||
                        ProductCode == "SSR" || ProductCode == "ER" || ProductCode == "RPL" || ProductCode == "RAL" || ProductCode == "ECR" || ProductCode == "EEI" || ProductCode == "PL" || ProductCode == "PS2")
                    {
                        if (Vendor_Response != "<XML_INTERFACE>ZZZ</XML_INTERFACE>")
                        {
                            Objbool = true;
                            ReportStatus = 2;
                            XDocument ObjXDocument = XDocument.Parse(Vendor_Response);
                            var List_OrderStatus = ObjXDocument.Descendants("status").ToList();
                            var List_OrderError = ObjXDocument.Descendants("responseErrorMessage").ToList();

                            if (List_OrderError.Count != 0)
                            {
                                Objbool = false;
                                ReportStatus = 1;
                            }

                            if (List_OrderStatus.Count != 0 && ReportStatus == 2)
                            {
                                foreach (XElement ObjXElement in List_OrderStatus)
                                {
                                    if (ObjXElement != null)
                                    {
                                        if (ObjXElement.Value.Trim() == "-1")
                                        {
                                            Objbool = false;
                                            ReportStatus = 1;
                                        }
                                        else
                                        {
                                            Objbool = true;
                                            ReportStatus = 2;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (IsInstantReport && ReportStatus != 2)
                            {
                                ReportStatus = 3;
                            }
                        }

                    }

                    if (ProductCode == "NCR1" || ProductCode == "FCR" || ProductCode == "NCR+")
                    {
                        string XML_Value = GetRequiredXml_ForRapidCourt(Vendor_Response);
                        XDocument ObjXDocument = XDocument.Parse(XML_Value);
                        var List_OrderStatus = ObjXDocument.Descendants("OrderStatus").ToList();
                        foreach (XElement ObjXElement in List_OrderStatus)
                        {
                            if (ObjXElement != null)
                            {
                                if (ObjXElement.Value.Trim() == "Completed")
                                {
                                    Objbool = true;
                                    ReportStatus = 2;
                                    break;
                                }
                                if (ObjXElement.Value.Trim() == "INCOMPLETE")
                                {
                                    Objbool = true;
                                    ReportStatus = 3;
                                    break;
                                }
                                if (ObjXElement.Value.Trim() == "Cancelled")
                                {
                                    Objbool = true;
                                    ReportStatus = 3;
                                    break;
                                }
                            }
                        }
                    }

                    if (ProductCode == "PS")
                    {

                        // Changed By Himesh Kumar
                        //string XML_Value = GetRequiredXml_ForRapidCourt(Vendor_Response);
                        //XDocument ObjXDocument = XDocument.Parse(XML_Value);
                        //var List_OrderStatus = ObjXDocument.Descendants("OrderStatus").ToList();
                        //var xcust = List_OrderStatus.Where(d => d.Value.ToString() == "Completed").FirstOrDefault();
                        //if (xcust != null)
                        //{
                        //    ReportStatus = 2;
                        //}
                        //else
                        //{
                        //    foreach (XElement ObjXElement in List_OrderStatus)
                        //    {
                        //        if (ObjXElement != null)
                        //        {
                        //            if (ObjXElement.Value.Trim() == "INCOMPLETE" || ObjXElement.Value.Trim() == "Cancelled")
                        //            {
                        //                Objbool = true;
                        //                ReportStatus = 3;
                        //                break;
                        //            }
                        //        }
                        //    }
                        //}

                        string XML_Value = Vendor_Response;//GetRequiredXml_ForRapidCourt(Vendor_Response);
                        XDocument ObjXDocument = XDocument.Parse(XML_Value);
                        var List_OrderStatus = ObjXDocument.Descendants("requestinformation").Attributes("code").FirstOrDefault().Value;
                        if (List_OrderStatus != null)
                        {
                            if (Convert.ToInt32(List_OrderStatus) == 100 || Convert.ToInt32(List_OrderStatus) == 101)
                            {
                                ReportStatus = 2;
                            }
                            else if (Convert.ToInt32(List_OrderStatus) == 102)
                            {
                                ReportStatus = 1;
                            }
                            else
                            {
                                ReportStatus = 3;
                            }
                        }

                    }

                    if (ProductCode == "RCX")
                    {
                        string XML_Value = GetRequiredXml_ForRapidCourt(Vendor_Response);
                        XDocument ObjXDocument = XDocument.Parse(XML_Value);
                        var List_OrderStatus = ObjXDocument.Descendants("OrderStatus").ToList();
                        foreach (XElement ObjXElement in List_OrderStatus)
                        {
                            if (ObjXElement != null)
                            {
                                if (ObjXElement.Value.Trim() == "Fulfilled" || ObjXElement.Value.Trim() == "Completed")//ticket #136 add "fulfilled" status
                                {

                                    bool ReportStatusFlag = false;
                                    using (EmergeDALDataContext ObjDALDataContext_ReportStatus = new EmergeDALDataContext())
                                    {

                                        var data = ObjDALDataContext_ReportStatus.tblOrderDetails.Where(x => x.pkOrderDetailId == pkOrderDetailId).Select(x => x.ReportStatus).FirstOrDefault();
                                        if (data != null)
                                        {
                                            if (data == 2)
                                            {
                                                ReportStatusFlag = true;
                                            }


                                        }
                                    }

                                    if (!ReportStatusFlag)
                                    {
                                        Objbool = true;
                                        ReportStatus = 2;
                                        #region*********Send Email to Admin, If order Status is Fulfilled. #Ticket136
                                        if (ObjXElement.Value.Trim() == "Fulfilled")
                                        {
                                            SendMailAfterReportStatusFulFilled(OrderId);
                                        }
                                        #endregion
                                        //fiteredResponse = 
                                            ObjBALGeneral.GetFilteredXMLDuringOrderPlace(ProductCode, Vendor_Response, pkOrderDetailId, DateTime.Now, IsPendingReport, true);
                                        #region Write First time response in txt file to check orderstatus-Fulfilled
                                        try
                                        {
                                            if (RCXFilePath != "")
                                            {
                                                string FileName = RCXFilePath + pkOrderDetailId + ".txt";
                                                StringBuilder sb = new StringBuilder();
                                                StreamWriter sw;
                                                if (!File.Exists(FileName))
                                                {
                                                    sw = File.CreateText(FileName);
                                                    sb.Append(Vendor_Response);
                                                    sw.WriteLine(sb);
                                                    sw.Close();
                                                }
                                            }
                                        }
                                        catch //(Exception ex)
                                        {

                                        }
                                        finally
                                        {
                                        }

                                        #endregion
                                        break;

                                    }
                                }
                                if (ObjXElement.Value.Trim() == "Hold" || ObjXElement.Value.Trim() == "Suspended") //ticket #136 remove "fulfilled" status and add with above condition
                                {
                                    Objbool = false;
                                    ReportStatus = 1;
                                    CheckReportStatusinRCXFile(pkOrderDetailId, ref ReportStatus, ref Objbool);
                                    break;
                                }
                                if (ObjXElement.Value.Trim() == "INCOMPLETE")
                                {
                                    ReportStatus = 3;
                                    CheckReportStatusinRCXFile(pkOrderDetailId, ref ReportStatus, ref Objbool);
                                    break;
                                }
                                if (ObjXElement.Value.Trim() == "Cancelled")
                                {
                                    Objbool = true;
                                    ReportStatus = (ProductCode == "RCX") ? 5 : 3; // 5 temp status to run Normal report is cancelled by live runner
                                    break;
                                }
                            }
                        }
                    }

                    if (ProductCode == "SOR" || ProductCode == "NCR3")
                    {
                        if (Vendor_Response != "")
                        {
                            Objbool = true; // Not Know Yet  return true;
                            ReportStatus = 2;
                            //fiteredResponse =
                                ObjBALGeneral.GetFilteredXMLDuringOrderPlace(ProductCode, Vendor_Response, pkOrderDetailId, DateTime.Now, IsPendingReport, false);
                        }
                    }

                    //response for backgroungcheks services reports
                    if (ProductCode == "CCR2")
                    {
                        if (Vendor_Response != "")
                        {
                            XDocument ObjXDocument = XDocument.Parse(Vendor_Response);
                            var List_OrderStatus = ObjXDocument.Descendants("status").ToList();
                            foreach (XElement ObjXElement in List_OrderStatus)
                            {
                                if (ObjXElement != null)
                                {
                                    if (ObjXElement.Value.Trim() == "Completed")
                                    {
                                        Objbool = true;
                                        ReportStatus = 2;
                                        //fiteredResponse = 
                                            ObjBALGeneral.GetFilteredXMLDuringOrderPlace(ProductCode, Vendor_Response, pkOrderDetailId, DateTime.Now, IsPendingReport, false);
                                        break;
                                    }
                                    if (ObjXElement.Value.Trim() != "Completed" && IsInstantReport)
                                    {
                                        ReportStatus = 3;
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    if (ProductCode == "MVR")//MVR_Non_Instant
                    {
                        XDocument ObjXDocument = XDocument.Parse(Vendor_Response);
                        var List_OrderStatus = ObjXDocument.Descendants("MvrStatus").ToList();

                        if (List_OrderStatus.Count != 0)
                        {
                            foreach (XElement ObjXElement in List_OrderStatus)
                            {
                                if (ObjXElement != null)
                                {
                                    if (ObjXElement.Value.Trim() == "3" || ObjXElement.Value.Trim() == "4" || ObjXElement.Value.Trim() == "5" || ObjXElement.Value.Trim() == "6")
                                    {
                                        Objbool = true;
                                        ReportStatus = 2;
                                        break;
                                    }
                                    else
                                    {
                                        Objbool = false;
                                        ReportStatus = 1;
                                    }
                                }
                            }
                        }
                        else
                        {
                            Objbool = false;
                            ReportStatus = 1;
                        }
                    }

                    if (ProductCode == "CDLIS")
                    {
                        XDocument ObjXDocument = XDocument.Parse(Vendor_Response);
                        var List_OrderStatus = ObjXDocument.Descendants("CDLISStatus").ToList();
                        if (List_OrderStatus.Count != 0)
                        {
                            foreach (XElement ObjXElement in List_OrderStatus)
                            {
                                if (ObjXElement != null)
                                {
                                    if (ObjXElement.Value.Trim() == "3" || ObjXElement.Value.Trim() == "4" || ObjXElement.Value.Trim() == "5" || ObjXElement.Value.Trim() == "6")
                                    {
                                        Objbool = true;
                                        ReportStatus = 2;
                                        break;
                                    }
                                    else
                                    {
                                        Objbool = false;
                                        ReportStatus = 1;
                                        //break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            Objbool = false;
                            ReportStatus = 1;
                        }
                    }


                    if (ProductCode == "NCR2")
                    {
                        XDocument ObjXDocument = XDocument.Parse(Vendor_Response);
                        var List_OrderStatus = ObjXDocument.Descendants("status").ToList();
                        foreach (XElement ObjXElement in List_OrderStatus)
                        {
                            if (ObjXElement != null)
                            {
                                if (ObjXElement.Value.Trim().ToLower() == "c")
                                {
                                    Objbool = true;
                                    ReportStatus = 2;
                                    break;
                                }
                                else
                                {
                                    ReportStatus = 3;
                                    break;
                                }
                            }
                        }
                    }
                    if (ProductCode == "SCR" || ProductCode == "CCR1")
                    {
                        XDocument ObjXDocument = XDocument.Parse(Vendor_Response);
                        var List_OrderStatus = ObjXDocument.Descendants("status").ToList();
                        if (List_OrderStatus != null && List_OrderStatus.Count > 0)
                        {
                            foreach (XElement ObjXElement in List_OrderStatus)
                            {
                                if (ObjXElement != null)
                                {
                                    if (ObjXElement.Value.Trim() == "C" || ObjXElement.Value.Trim() == "D")
                                    {
                                        Objbool = true;
                                        ReportStatus = 2;
                                        //fiteredResponse = 
                                            ObjBALGeneral.GetFilteredXMLDuringOrderPlace(ProductCode, Vendor_Response, pkOrderDetailId, DateTime.Now, IsPendingReport, false);
                                        break;
                                    }
                                    else if (ObjXElement.Value.Trim() == "P")
                                    {
                                        Objbool = false;
                                        ReportStatus = 1;
                                        break;
                                    }

                                }
                            }
                        }
                    }
                    if (ProductCode == "SNS")
                    {
                        Objbool = true;
                        ReportStatus = 2;
                    }
                }
                catch { }
                finally
                {
                    ObjBALGeneral = null;
                }
            }
            return ReportStatus;
        }


        /// <summary>
        /// Function to Check the NCR's HIT.
        /// </summary>
        /// <param name="pkOrderDetailId"></param>
        /// <param name="ProductCode"></param>
        /// <param name="ObjXDocument"></param>
        /// <returns></returns>
        public void CheckIsNCRHit(int? OrderId, int? pkOrderDetailId, string ProductCode, string Vendor_Response, bool ftpStatus)
        {
            string XML_Value = BALEmergeReview.GetRequiredXmlConditional(Vendor_Response, ProductCode.Trim().ToUpper());
            XDocument ObjXDocument = XDocument.Parse(XML_Value);
            try
            {
                if (ProductCode == "NCR1" || ProductCode == "NCR+")
                {
                    var lstScreenings = ObjXDocument.Descendants("Screenings").ToList();
                    if (lstScreenings.Count > 0)
                    {
                        var lstCriminalReports = lstScreenings.Descendants("CriminalReport").FirstOrDefault();

                        VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "NoticePdf613");
                        VendorServices.GeneralService.WriteLog("lstCriminalReports In : " + DateTime.Now.ToString(), "NoticePdf613");

                        if (lstCriminalReports != null)
                        {

                            VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "NoticePdf613");
                            VendorServices.GeneralService.WriteLog("lstCriminalReports Is not null : " + DateTime.Now.ToString(), "NoticePdf613");

                            var lstCriminalCases = lstCriminalReports.Descendants("CriminalCase").ToList();
                            if (lstCriminalCases != null && lstCriminalCases.Count > 0)
                            {
                                Send_Update613Notice(OrderId, pkOrderDetailId, ftpStatus);
                            }
                        }
                    }
                }
                if (ProductCode == "NCR2")
                {
                    var lstreportdetailheader = ObjXDocument.Descendants("report-detail-header").ToList();
                    if (lstreportdetailheader.Count > 0)
                    {
                        var lstreportdata = lstreportdetailheader.Descendants("report-data").FirstOrDefault();

                        if (lstreportdata != null)
                        {
                            string reportdataValue = lstreportdata.Value.ToString();
                            if (!string.IsNullOrEmpty(reportdataValue))
                            {
                                Send_Update613Notice(OrderId, pkOrderDetailId, ftpStatus);
                            }
                        }
                    }
                }
                if (ProductCode == "NCR4")
                {
                    var lstResponse = ObjXDocument.Descendants("Response").ToList();
                    if (lstResponse.Count > 0)
                    {
                        var lstCriminalSearchResults = lstResponse.Descendants("CriminalSearchResults").FirstOrDefault();

                        if (lstCriminalSearchResults != null)
                        {
                            var lstJurisdictionResults = lstCriminalSearchResults.Descendants("JurisdictionResults").ToList();
                            if (lstJurisdictionResults != null && lstJurisdictionResults.Count > 0)
                            {
                                Send_Update613Notice(OrderId, pkOrderDetailId, ftpStatus);
                            }
                        }
                    }
                }
            }
            catch { }
        }

        public void Send_Update613Notice(int? OrderId, int? OrderDetailId, bool ftpStatus)
        {
            BALCompany ObjBALCompany = new BALCompany();
            BALOrders ObjBALOrders = new BALOrders();
            string UserName = string.Empty;
            string PdfFile = string.Empty;
            string CompanyName = string.Empty;
            string Address = string.Empty;
            string ApplicantName = string.Empty;
            string ApplicantNamepdf = string.Empty;
            string OrderNo = string.Empty;

            string ApplicantEmail = string.Empty;
            List<Proc_Get_RequestAndResponseResult> ObjProc_Get_RequestAndResponseResult = new List<Proc_Get_RequestAndResponseResult>();
            Proc_Get_RequestAndResponseResult ObjRequestAndResponseResult = new Proc_Get_RequestAndResponseResult();
            List<Proc_Get_CompanyDetailByMemberShipIdResult> ObjCompanyDetails = new List<Proc_Get_CompanyDetailByMemberShipIdResult>();
            Proc_Get_CompanyDetailByMemberShipIdResult ObjCompanyDetailByMemberShipIdResult = new Proc_Get_CompanyDetailByMemberShipIdResult();
            if (OrderId != 0)
            {
                ObjProc_Get_RequestAndResponseResult = ObjBALOrders.GetRequestAndResponseByOrderId(OrderId);
                if (ObjProc_Get_RequestAndResponseResult.Count > 0)
                {
                    ObjRequestAndResponseResult = ObjProc_Get_RequestAndResponseResult.First();
                    ObjCompanyDetails = ObjBALCompany.GetCompanyDetailByMemberShipIdResult(ObjRequestAndResponseResult.fkUserId);
                    if (ObjCompanyDetails.Count > 0)
                    {

                        VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "NoticePdf613");
                        VendorServices.GeneralService.WriteLog("Send_Update613Notice In : " + OrderId + "" + OrderDetailId, "NoticePdf613");


                        ObjCompanyDetailByMemberShipIdResult = ObjCompanyDetails.First();

                        UserName = ObjCompanyDetailByMemberShipIdResult.lastname + ", " + ObjCompanyDetailByMemberShipIdResult.firstname;
                        CompanyName = ObjCompanyDetailByMemberShipIdResult.CompanyName;
                        Address = ObjCompanyDetailByMemberShipIdResult.city + ", " + ObjCompanyDetailByMemberShipIdResult.statecode;
                        ApplicantName = GenerateApplicantName(ObjRequestAndResponseResult.SearchedLastName, ObjRequestAndResponseResult.SearchedSuffix, ObjRequestAndResponseResult.SearchedMiddleInitial, ObjRequestAndResponseResult.SearchedFirstName);
                        ApplicantNamepdf = ObjRequestAndResponseResult.SearchedLastName + "_" + ObjRequestAndResponseResult.SearchedFirstName;

                        VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "NoticePdf613");
                        VendorServices.GeneralService.WriteLog("Send_Update613Notice In : " + ApplicantNamepdf, "NoticePdf613");

                        //int-132
                        ApplicantNamepdf = ObjRequestAndResponseResult.SearchedFirstName + "_" + ObjRequestAndResponseResult.SearchedLastName;


                        ApplicantEmail = ObjRequestAndResponseResult.SearchedApplicantEmail;
                        OrderNo = ObjRequestAndResponseResult.OrderNo;
                        #region Mailing Address

                        var ObjData = new tblOrderSearchedData();
                        string stateName = "";
                        string mailingAddress = "";
                        string StreetAddress = string.Empty;
                        string StreetName = string.Empty;

                        using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                        {
                            ObjData = Dx.tblOrderSearchedDatas.FirstOrDefault(n => n.fkOrderId == OrderId);
                            if (ObjData != null)
                            {

                                VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "NoticePdf613");
                                VendorServices.GeneralService.WriteLog(" ObjData is not null" + "1", "NoticePdf613");


                                if (ObjData.search_state != null && ObjData.search_state != string.Empty)
                                {
                                    stateName = ((ObjData.search_state != "-1") && (ObjData.search_state != null)) ? Dx.tblStates.FirstOrDefault(n => n.pkStateId == Convert.ToInt32(ObjData.search_state)).StateCode : stateName;
                                }
                                else
                                {
                                    stateName = ((ObjData.SearchedStateId.ToString() != "-1") && (ObjData.SearchedStateId != null)) ? Dx.tblStates.FirstOrDefault(n => n.pkStateId == Convert.ToInt32(ObjData.SearchedStateId)).StateCode : stateName;
                                }
                                mailingAddress = ObjData.SearchedFirstName != string.Empty ? ObjData.SearchedFirstName + " " : mailingAddress;
                                mailingAddress = ObjData.SearchedLastName != string.Empty ? mailingAddress + ObjData.SearchedLastName + "\n" : mailingAddress + "\n";
                                if (ObjData.best_SearchedStreetAddress != null && ObjData.best_SearchedStreetAddress != string.Empty)
                                {
                                    mailingAddress = ObjData.best_SearchedStreetAddress != string.Empty ? mailingAddress + ObjData.best_SearchedStreetAddress + " " : mailingAddress;
                                    StreetAddress = ObjData.best_SearchedStreetAddress != string.Empty ? ObjData.best_SearchedStreetAddress : string.Empty;
                                }
                                else if (ObjData.SearchedStreetAddress != null && ObjData.SearchedStreetAddress != string.Empty)
                                {
                                    mailingAddress = ObjData.SearchedStreetAddress != string.Empty ? mailingAddress + ObjData.SearchedStreetAddress + " " : mailingAddress;
                                    StreetAddress = ObjData.SearchedStreetAddress != string.Empty ? ObjData.SearchedStreetAddress : string.Empty;
                                }
                                if (ObjData.best_SearchedStreetName != null && ObjData.best_SearchedStreetName != string.Empty)
                                {
                                    StreetName = ObjData.best_SearchedStreetAddress != string.Empty ? ObjData.best_SearchedStreetAddress : string.Empty;
                                }
                                else if (ObjData.SearchedStreetName != null && ObjData.SearchedStreetName != string.Empty)
                                {
                                    StreetName = ObjData.SearchedStreetName != string.Empty ? ObjData.SearchedStreetName : string.Empty;
                                }

                                if (StreetAddress != string.Empty && StreetName != string.Empty && StreetName != StreetAddress)
                                {
                                    if (ObjData.best_SearchedStreetName != null && ObjData.best_SearchedStreetName != string.Empty)
                                    {
                                        mailingAddress = ObjData.best_SearchedStreetName != string.Empty ? mailingAddress + ObjData.best_SearchedStreetName + " " : mailingAddress;
                                    }
                                    else if (ObjData.SearchedStreetName != null && ObjData.SearchedStreetName != string.Empty)
                                    {
                                        mailingAddress = ObjData.SearchedStreetName != string.Empty ? mailingAddress + ObjData.SearchedStreetName + " " : mailingAddress;
                                    }
                                }

                                mailingAddress = ObjData.SearchedStreetType != string.Empty ? mailingAddress + ObjData.SearchedStreetType + " " : mailingAddress;
                                mailingAddress = ObjData.SearchedApt != string.Empty ? mailingAddress + ObjData.SearchedApt + "\n" : mailingAddress + "\n";
                                //   mailingAddress = ObjData.best_SearchedCity != string.Empty ? mailingAddress + ObjData.best_SearchedCity + ", ": mailingAddress;
                                if (ObjData.best_SearchedCity != null && ObjData.best_SearchedCity != string.Empty)
                                {
                                    mailingAddress = ObjData.best_SearchedCity != string.Empty ? mailingAddress + ObjData.best_SearchedCity + ", " : mailingAddress;
                                }
                                else if (ObjData.SearchedCity != null && ObjData.SearchedCity != string.Empty)
                                {
                                    mailingAddress = ObjData.SearchedCity != string.Empty ? mailingAddress + ObjData.SearchedCity + ", " : mailingAddress;
                                }

                                //bool AddressStatus = mailingAddress != string.Empty ? true : false;
                                if (mailingAddress != string.Empty)
                                {
                                    mailingAddress = stateName != string.Empty ? mailingAddress + stateName : mailingAddress;
                                    if (ObjData.best_SearchedZipCode != null && ObjData.best_SearchedZipCode != string.Empty)
                                    {
                                        mailingAddress = ObjData.best_SearchedZipCode != string.Empty ? mailingAddress + " " + ObjData.best_SearchedZipCode : mailingAddress;
                                    }
                                    else if (ObjData.SearchedZipCode != null && ObjData.SearchedZipCode != string.Empty)
                                    {
                                        mailingAddress = ObjData.SearchedZipCode != string.Empty ? mailingAddress + " " + ObjData.SearchedZipCode : mailingAddress;
                                    }
                                }
                            }
                        }

                        #endregion
                        //INT-14 new method to generate pdf
                        VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "NoticePdf613");
                        VendorServices.GeneralService.WriteLog(" ftpStatus status" + ftpStatus, "NoticePdf613");
                        PdfFile = GeneratePdfOf613Noticeforftpuploadservie(ObjRequestAndResponseResult.SearchedFirstName + " " + ObjRequestAndResponseResult.SearchedLastName + ",", ApplicantNamepdf, UserName, CompanyName, Address, mailingAddress, OrderId, OrderNo);
                        VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "NoticePdf613");
                        VendorServices.GeneralService.WriteLog(" PdfFile" + "1", "NoticePdf613");

                        VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "NoticePdf613");
                        VendorServices.GeneralService.WriteLog(" ApplicantEmail" + ApplicantEmail, "NoticePdf613");
                        // ApplicantEmail = "";
                        if (!string.IsNullOrEmpty(ApplicantEmail))
                        {

                            VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "NoticePdf613");
                            VendorServices.GeneralService.WriteLog(" ApplicantEmail if condition in " + ApplicantEmail, "NoticePdf613");



                            //string Message = string.Empty;
                            bool EmailSent = false;
                            string MessageBody = "", emailText = "", Subject = "", Name = "";
                            string FileName = ApplicantName;
                            Name = FileName.Contains('_') ? FileName.Replace('_', ' ') : FileName;
                            try
                            {
                                //  string WebsiteLogo = "https://www.intelifi.com/emerge/Resources/Upload/Images/emerge-logo--no-shadows-%28small%29.gif";
                                string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/emerge-logo--no-shadows-(small).gif";

                                Guid SiteApplicationId = new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852");
                                var emailContent = GetEmailTemplate(SiteApplicationId, "613 Notice");//"Emailed Report Request"
                                string GetTemplateSubject = emailContent.TemplateSubject;
                                //  GetTemplateSubject = GetTemplateSubject.Replace("%%ApplicantName%%","[ "+ ObjRequestAndResponseResult.SearchedLastName + "," + ObjRequestAndResponseResult.SearchedFirstName + " ]");
                                GetTemplateSubject = GetTemplateSubject.Replace("%%ApplicantName%%", "[" + ObjRequestAndResponseResult.SearchedLastName + ", " + ObjRequestAndResponseResult.SearchedFirstName + "]");
                                Subject = GetTemplateSubject;
                                BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                                #region For New Style SystemTemplate
                                string Bookmarkfile = string.Empty;
                                string bookmarkfilepath = string.Empty;

                                Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                                Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                                Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                                emailText = Bookmarkfile;
                                #endregion

                                emailText = emailContent.TemplateContent.Replace("%ApplicantName%", ApplicantName).Replace("%UserName%", CompanyName).Replace("%WebsiteLogo%", "<img src='" + WebsiteLogo + "' />");
                                MessageBody = emailText;

                                byte[] byteArray = System.IO.File.ReadAllBytes(System.Web.HttpContext.Current.Server.MapPath("~/Resources/Upload/613NoticePdf/" + PdfFile));// Encoding.Default.GetBytes("");
                                using (MemoryStream stream = new MemoryStream(byteArray))
                                {


                                    EmailSent = SendMailWithAttachMent(ApplicantEmail, BALGeneral.GetGeneralSettings().SupportEmailId, MessageBody, Subject, new Attachment(stream, PdfFile));

                                    if (EmailSent == true)
                                    {
                                        using (EmergeDALDataContext DataContext = new EmergeDALDataContext())
                                        {

                                            try
                                            {
                                                tblNotice613EmailStatus o = new tblNotice613EmailStatus();
                                                o.isEmailSent = true;
                                                o.Orderdetailid = Convert.ToInt32(OrderDetailId);
                                                o.Orderid = Convert.ToInt32(OrderId);
                                                o.ApplicantName = Convert.ToString(ApplicantEmail);
                                                o.SentDate = DateTime.Now;
                                                DataContext.tblNotice613EmailStatus.InsertOnSubmit(o);
                                                DataContext.SubmitChanges();
                                            }
                                            catch (Exception ass)
                                            {
                                                VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "NoticePdf613");

                                                VendorServices.GeneralService.WriteLog("Exception Occur: " + ass.Message.ToString(), "NoticePdf613");

                                            }

                                        }



                                        UpdateIsEmailNotice(OrderDetailId, true, true);
                                        //Message = "Email Sent Successfully";
                                    }
                                    else
                                    {
                                        UpdateIsEmailNotice(OrderDetailId, true, false);
                                        //Message = "Some error occured,please try again";
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                //Message = "Some error occured,please try again";

                                VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "NoticePdf613");
                                VendorServices.GeneralService.WriteLog(" ApplicantEmail is not null" + ex.Message.ToString(), "NoticePdf613");

                            }
                            finally
                            {
                                if (PdfFile != null)
                                {
                                    string FilePath = System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\613NoticePdf\") + PdfFile;
                                    if (File.Exists(FilePath))
                                    {
                                        File.Delete(FilePath);
                                        VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "NoticePdf613");
                                        VendorServices.GeneralService.WriteLog(" file deleted is successfully" + "", "NoticePdf613");


                                    }
                                }
                            }
                        }
                        else
                        {
                            if (ftpStatus)
                            {
                                #region for FTP upload and email support team
                                try
                                {
                                    if (mailingAddress != string.Empty)
                                    {

                                        VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "NoticePdf613");
                                        VendorServices.GeneralService.WriteLog(" mailingAddress else condition in " + mailingAddress, "NoticePdf613");

                                        try
                                        {
                                            string filename = System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\613NoticePdf\") + PdfFile;
                                            FileInfo PdfFileobj = new FileInfo(filename);

                                            string ftpPath = ConfigurationManager.AppSettings["ftpPath"].ToString() + PdfFileobj.Name;
                                            string ftpUserName = ConfigurationManager.AppSettings["ftpUserName"].ToString();
                                            string ftpPassword = ConfigurationManager.AppSettings["ftpPassword"].ToString();

                                            string FilePath = System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\613NoticePdf\") + PdfFile;


                                            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpPath);
                                            request.Method = WebRequestMethods.Ftp.UploadFile;
                                            request.Proxy = new WebProxy();

                                            // This example assumes the FTP site uses anonymous logon.
                                            request.Credentials = new NetworkCredential(ftpUserName, ftpPassword);

                                            FileStream fs = File.OpenRead(FilePath);
                                            byte[] fileContents = new byte[fs.Length];
                                            fs.Read(fileContents, 0, fileContents.Length);
                                            fs.Close();

                                            Stream requestStream = request.GetRequestStream();
                                            requestStream.Write(fileContents, 0, fileContents.Length);
                                            requestStream.Close();


                                            VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "NoticePdf613");
                                            VendorServices.GeneralService.WriteLog(" requestStream  is done oks " + mailingAddress, "NoticePdf613");

                                        }

                                        catch (Exception s)
                                        {

                                            VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "NoticePdf613");

                                            VendorServices.GeneralService.WriteLog("Exception Occure: " + s.ToString(), "NoticePdf613");
                                        }




                                    }
                                    else
                                    {
                                        Guid SiteApplicationId = new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852");
                                        var emailContent = GetEmailTemplate(SiteApplicationId, 63);//"Emailed Report Request"
                                        string GetTemplateSubject = emailContent.TemplateSubject;
                                        GetTemplateSubject = GetTemplateSubject.Replace("%OrderId%", ObjRequestAndResponseResult.OrderNo.ToString());
                                        string Subject = GetTemplateSubject;
                                        BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                                        string emailText = emailContent.TemplateContent.Replace("%OrderId%", ObjRequestAndResponseResult.OrderNo.ToString());
                                        string MessageBody = emailText;
                                        bool EmailSent = SendMailWithoutAttachMent(BALGeneral.GetGeneralSettings().AdminEmailId, BALGeneral.GetGeneralSettings().SupportEmailId, MessageBody, Subject);
                                    }
                                    #region update table tblnotice613pdfstatus
                                    using (EmergeDALDataContext DataContext = new EmergeDALDataContext())
                                    {

                                        VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "NoticePdf613");

                                        VendorServices.GeneralService.WriteLog("DataContext Occur: " + OrderId, "NoticePdf613");

                                        try
                                        {
                                            tblNotice613PdfStatus o = new tblNotice613PdfStatus();
                                            o.IsPdfSent = true;
                                            o.OrderDetailId = OrderDetailId.ToString();
                                            o.OrderId = Convert.ToInt32(OrderId);
                                            o.PdfName = PdfFile;
                                            o.SentDate = DateTime.Now;
                                            DataContext.tblNotice613PdfStatus.InsertOnSubmit(o);
                                            DataContext.SubmitChanges();
                                        }
                                        catch (Exception ass)
                                        {
                                            VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "NoticePdf613");

                                            VendorServices.GeneralService.WriteLog("Exception Occur: " + ass.Message.ToString(), "NoticePdf613");

                                        }

                                    }
                                    #endregion
                                #endregion
                                }
                                catch (Exception ex)
                                {

                                    VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "NoticePdf613");

                                    VendorServices.GeneralService.WriteLog("Exception Occure: " + ex.ToString(), "NoticePdf613");
                                    VendorServices.GeneralService.WriteLog("Sending 613 NoticePdf End at: " + DateTime.Now.ToString(), "NoticePdf613");

                                }

                                UpdateIsEmailNotice(OrderDetailId, true, true);




                                VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "NoticePdf613");
                                VendorServices.GeneralService.WriteLog("UpdateIsEmailNotice is completed  " + DateTime.Now.ToString(), "NoticePdf613");



                            }

                        }
                    }
                }
            }
        }


        private bool CheckIfFtpFileExists(string ftpPath, string ftpUserName, string ftpPassword)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpPath);
            request.Credentials = new NetworkCredential(ftpUserName, ftpPassword);
            request.Method = WebRequestMethods.Ftp.GetFileSize;
            try
            {
                //THE FILE EXISTS
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                if (FtpStatusCode.ActionNotTakenFileUnavailable == response.StatusCode)
                {
                    //THE FILE DOES NOT EXIST
                    return false;
                }
            }
            return true;
        }

        public bool SendMailWithoutAttachMent(string TO, string FROM, string MESSAGE, string SUBJECT)
        {
            int SendingStatus = 0;
            try
            {
                MomentumInfotech.EmailProvider.MailProvider ObjMailProvider = new MomentumInfotech.EmailProvider.MailProvider();

                ObjMailProvider.Message = MESSAGE;
                ObjMailProvider.MailTo.Add(TO);
                ObjMailProvider.MailFrom = FROM;
                ObjMailProvider.Subject = SUBJECT;
                ObjMailProvider.IsBodyHtml = true;
                ObjMailProvider.Priority = MailPriority.High;
                ObjMailProvider.SendMail(out SendingStatus);
            }
            catch //(Exception ex)
            {
            }

            return (SendingStatus == 1 ? true : false);
        }

        public bool SendMailWithAttachMent(string TO, string FROM, string MESSAGE, string SUBJECT, Attachment ATTACHMENT)
        {
            int SendingStatus = 0;
            try
            {
                MomentumInfotech.EmailProvider.MailProvider ObjMailProvider = new MomentumInfotech.EmailProvider.MailProvider();

                ObjMailProvider.Message = MESSAGE;
                ObjMailProvider.MailTo.Add(TO);
                ObjMailProvider.MailAttachments.Add(ATTACHMENT);
                ObjMailProvider.MailFrom = FROM;
                ObjMailProvider.Subject = SUBJECT;
                ObjMailProvider.IsBodyHtml = true;
                ObjMailProvider.Priority = MailPriority.High;
                ObjMailProvider.SendMail(out SendingStatus);
            }
            catch //(Exception ex)
            {
            }

            return (SendingStatus == 1 ? true : false);
        }

        #region 613NoticeWriteToPdf

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "typeofelement")]
        public string GenerateAuthorizationform(string firstname, string ssn, string dob, string email, int CompanyId, string Ordered_date, string LastName)
        {
            string pdfFileName = string.Empty;
            var CompanyName = string.Empty;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                CompanyName = (from u in ObjDALDataContext.tblCompanies

                               where u.pkCompanyId == CompanyId
                               select u.CompanyName).FirstOrDefault();

            }
            if (CompanyId == 6587)
            {
                string FullName = firstname + " " + LastName;
                string Date = Ordered_date;
                iTextSharp.text.Rectangle ObjPageSize = new iTextSharp.text.Rectangle(PageSize.A4);
                Document ObjDocument = new Document(ObjPageSize, 55, 55, 15, 12);
                //float Leading = 13;
                //DateTime DateToBePrint = DateTime.Now;
                try
                {
                    pdfFileName = "axiomstaffingdisclosureandauthorization.pdf";
                    DirectoryInfo dir = null;
                    if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\613NoticePdf")))
                    {
                        Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\613NoticePdf"));
                    }
                    else
                    {

                        dir = new DirectoryInfo(System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\613NoticePdf"));
                        FileSystemInfo[] info = dir.GetFiles();
                        if (info.Length > 0)
                        {
                            foreach (FileInfo fi in info)
                            {
                                if (fi.Name.Contains(""))
                                {
                                    string FilePath = System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\613NoticePdf\") + fi.Name;
                                    if (System.IO.File.Exists(FilePath))
                                    {
                                        try
                                        {
                                            System.IO.File.Delete(FilePath);
                                        }
                                        catch //(Exception ex)
                                        {
                                            // ListErrors.Add("File Used By Another Process : " + ex.Message.ToString());
                                        }

                                    }
                                }
                            }
                        }
                    }
                    Font Verdana = FontFactory.GetFont("Verdana", 10F, Font.UNDERLINE);
                    string path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Resources\\Upload\\613NoticePdf\\" + pdfFileName;
                    PdfWriter writer = PdfWriter.GetInstance(ObjDocument, new FileStream(path, FileMode.Create));
                    ObjDocument.Open();
                    #region for blankspace
                    Chunk spaceChunk = new Chunk("                                                                                                                                                     ");
                    #endregion
                    Phrase op = new Phrase();
                    Cell ocell = new Cell(op);
                    Chunk ock = new Chunk();
                    string paragraph = string.Empty;
                    #region Main Paragraph
                    paragraph = "";
                    //StringBuilder sb = new StringBuilder();
                    var f = FontFactory.GetFont("Times New Roman", 10, Font.BOLD);
                    PdfPTable table = new PdfPTable(3);
                    PdfPCell cell = new PdfPCell(new Phrase("Authorization and Release Form", f));
                    cell.Colspan = 3;
                    cell.HorizontalAlignment = 1;
                    cell.BorderColor = iTextSharp.text.Color.WHITE;
                    table.AddCell(cell);
                    ObjDocument.Add(table);
                    StyleSheet styles = new iTextSharp.text.html.simpleparser.StyleSheet();
                    var firstparagraph = "<p align='justify' style='font-size:10'>I acknowledge receipt of the separate document entitled DISCLOSURE REGARDING BACKGROUND INVESTIGATION and A SUMMARY OF YOUR RIGHTS UNDER THE FAIR CREDIT REPORTING ACT and certify that I have read and understand both of those documents. I hereby authorize the obtaining of “consumer reports” and/or “investigative consumer reports” by Axiom Staffing Group at any time after receipt of this authorization and throughout my employment, if applicable. To this end, I hereby authorize, without reservation, any law enforcement agency, administrator, state or federal agency, institution, school or university (public or private), information service bureau, employer, or insurance company to furnish any and all background information requested by Name of company, Address, Phone number, Website and/or Axiom Staffing Group. I agree that a facsimile (“fax”), electronic or photographic copy of this Authorization shall be as valid as the original. Further, I understand that Axiom Staffing Group may provide a copy of the completed consumer report to its clients for the purpose of determining my eligibility for placement and I expressly authorize such acts.</p>";
                    var parsedHtmlElementsfirst = HTMLWorker.ParseToList(new StringReader(firstparagraph), styles);
                    foreach (var htmlElement in parsedHtmlElementsfirst)
                    {
                        string typeofelement = htmlElement.GetType().ToString();
                        ObjDocument.Add(htmlElement as IElement);

                    }
                    var secondparagraph = "<p align='justify' style='font-size:10'><b><u>New York applicants only:</b></u> Upon request, you will be informed whether or not a consumer report was requested by the Company, and if such report was requested, informed of the name and address of the consumer reporting agency that furnished the report. You have the right to inspect and receive a copy of any investigative consumer report requested by the Company by contacting the consumer reporting agency identified above directly. By signing below, you acknowledge receipt of Article 23-A of the New York Correction Law.</p><p align='justify' style='font-size:10'></br><b><u>Washington State applicants only:</b></u> You also have the right to request from the consumer reporting agency a written summary of your rights and remedies under the Washington Fair Credit Reporting Act.</p><p align='justify' style='font-size:10'><b><u>Minnesota and Oklahoma applicants only:</b></u> Please check this box if you would like to receive a copy of a consumer report if one is obtained by the Company.</p><br>";
                    var parsedHtmlElementssecond = HTMLWorker.ParseToList(new StringReader(secondparagraph), styles);

                    foreach (var htmlElement in parsedHtmlElementssecond)
                    {
                        string typeofelement = htmlElement.GetType().ToString();
                        ObjDocument.Add(htmlElement as IElement);

                    }

                    PdfPTable tabletableempty = new PdfPTable(3);
                    PdfPCell emptycell = new PdfPCell(new Phrase("", f));
                    emptycell.Colspan = 3;
                    emptycell.HorizontalAlignment = 1;
                    emptycell.BorderColor = iTextSharp.text.Color.WHITE;
                    tabletableempty.AddCell(emptycell);
                    ObjDocument.Add(tabletableempty);
                    string imageFilePath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Content\\themes\\base\\images\\box.jpg";
                    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imageFilePath);
                    image.Alignment = iTextSharp.text.Image.ALIGN_BOTTOM;
                    PdfPTable tableimg = new PdfPTable(24);
                    tableimg.DefaultCell.Border = 0;
                    tableimg.DefaultCell.BorderColor = Color.WHITE;
                    tableimg.WidthPercentage = 100;
                    PdfPCell celljpg = new PdfPCell(image);
                    celljpg.HorizontalAlignment = PdfPCell.ALIGN_TOP;
                    celljpg.BorderColor = iTextSharp.text.Color.WHITE;
                    tableimg.AddCell(celljpg);
                    PdfPCell cellimg = new PdfPCell(new Phrase("California, Minnesota and Oklahoma Applicants:Check box if you request a copy of your consumer report"));
                    cellimg.Colspan = 26;
                    ((Chunk)(cellimg.Phrase[0])).Font = new iTextSharp.text.Font(((Chunk)(cellimg.Phrase[0])).Font.Family, 9f);
                    cellimg.BorderColor = iTextSharp.text.Color.WHITE;
                    cellimg.HorizontalAlignment = PdfPCell.ALIGN_BOTTOM;
                    tableimg.AddCell(cellimg);
                    ObjDocument.Add(tableimg);
                    paragraph = "<p align='justify' style='font-size:10'> </br><b><u>California applicants only:</b></u> Under California Civil Code section 1786.22, you are entitled to find out what is in the CRA’s file on you with proper identification, as follows:<ul style=\"list-style-type:circle;font-size:10\"><li>In person, by visual inspection of your file during normal business hours and on reasonable notice. You also may request a copy of the information in person. The CRA may not charge you more than the actual copying costs for providing you with a copy of your file.</li><li>A summary of all information contained in the CRA file on you that is required to be provided by the California Civil Code will be provided to you via telephone, if you have made a written request, with proper identification, for telephone disclosure, and the toll charge, if any, for the telephone call is prepaid by or charged directly to you.</li><li>By requesting a copy be sent to a specified addressee by certified mail. CRAs complying with requests for certified mailings shall not be liable for disclosures to third parties caused by mishandling of mail after such mailings leave the CRAs.</li></ul></p><br>";
                    var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(paragraph), styles);
                    foreach (var htmlElement in parsedHtmlElements)
                    {
                        string typeofelement = htmlElement.GetType().ToString();
                        ObjDocument.Add(htmlElement as IElement);
                    }

                    var paragraph2 = "<p align='justify' style='font-size:10'>“Proper Identification” includes documents such as a valid driver’s license, social security account number, military identification card, and credit cards. Only if you cannot identify yourself with such information may the CRA require additional information concerning your employment and personal or family history in order to verify your identity. The CRA will provide trained personnel to explain any information furnished to you and will provide a written explanation of any coded information contained in files maintained on you. This written explanation will be provided whenever a file is provided to you for visual inspection. You may be accompanied by one other person of your choosing, who must furnish reasonable identification. An CRA may require you to furnish a written statement granting permission to the CRA to discuss your file in such person’s presence. Please check this box if you would like to receive a copy of an investigative consumer report or consumer credit report at no charge if one is obtained by the Company whenever you have a right to receive such a copy under California law. □</p><br>";
                    var parsedHtmlElements2 = HTMLWorker.ParseToList(new StringReader(paragraph2), styles);
                    foreach (var htmlElement in parsedHtmlElements2)
                    {
                        string typeofelement = htmlElement.GetType().ToString();
                        ObjDocument.Add(htmlElement as IElement);
                    }
                    var main6 = "<p align='justify' style='font-size:10'>Signature: " + FullName + " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date: " + Date + " </p><br>";
                    var parsedHtmlElements5 = HTMLWorker.ParseToList(new StringReader(main6), styles);
                    foreach (var htmlElement in parsedHtmlElements5)
                    {
                        string typeofelement = htmlElement.GetType().ToString();
                        ObjDocument.Add(htmlElement as IElement);
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    ObjDocument.Close();
                }
            }
            else
            {
                string FullName = firstname + " " + LastName;
                string Date = Ordered_date;

                iTextSharp.text.Rectangle ObjPageSize = new iTextSharp.text.Rectangle(PageSize.A4);
                Document ObjDocument = new Document(ObjPageSize, 55, 55, 15, 12);
               // float Leading = 13;
                DateTime DateToBePrint = DateTime.Now;
                try
                {
                    pdfFileName = "disclosureandauthorization.pdf";
                    DirectoryInfo dir = null;
                    if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\613NoticePdf")))
                    {
                        Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\613NoticePdf"));
                    }
                    else
                    {

                        dir = new DirectoryInfo(System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\613NoticePdf"));
                        FileSystemInfo[] info = dir.GetFiles();
                        if (info.Length > 0)
                        {
                            foreach (FileInfo fi in info)
                            {
                                if (fi.Name.Contains(""))
                                {
                                    string FilePath = System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\613NoticePdf\") + fi.Name;
                                    if (System.IO.File.Exists(FilePath))
                                    {
                                        try
                                        {
                                            System.IO.File.Delete(FilePath);
                                        }
                                        catch (Exception ex)
                                        {
                                            // ListErrors.Add("File Used By Another Process : " + ex.Message.ToString());
                                        }

                                    }
                                }
                            }
                        }
                    }
                    Font Verdana = FontFactory.GetFont("Verdana", 10F, Font.UNDERLINE);
                    string path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Resources\\Upload\\613NoticePdf\\" + pdfFileName;
                    PdfWriter writer = PdfWriter.GetInstance(ObjDocument, new FileStream(path, FileMode.Create));
                    ObjDocument.Open();
                    #region for blankspace
                    Chunk spaceChunk = new Chunk("                                                                                                                                                     ");
                    #endregion
                    Phrase op = new Phrase();
                    Cell ocell = new Cell(op);
                    Chunk ock = new Chunk();
                    string paragraph = string.Empty;
                    #region Main Paragraph
                    paragraph = "";
                    StringBuilder sb = new StringBuilder();
                    var f = FontFactory.GetFont("Times New Roman", 10, Font.BOLD);
                    PdfPTable table = new PdfPTable(3);
                    PdfPCell cell = new PdfPCell(new Phrase("DISCLOSURE AND AUTHORIZATION", f));
                    cell.Colspan = 3;
                    cell.HorizontalAlignment = 1;
                    cell.BorderColor = iTextSharp.text.Color.WHITE;
                    table.AddCell(cell);
                    ObjDocument.Add(table);
                    StyleSheet styles = new iTextSharp.text.html.simpleparser.StyleSheet();
                    var firstparagraph = "<p align='justify' style='font-size:10'>In connection with my application for employment (including contract for services or volunteer services) or tenancy with  <u>" + CompanyName + "</u>. These consumer reports (investigative consumer reports in California) may include the following types of information: names and dates of previous employers, salary, work experience,education, accidents, licensure, credit (except California), etc. I further understand that such reports may contain public record information such as, but not limited to: my driving record, workers’ compensation claims, judgments, bankruptcy proceedings,criminal records, etc., from federal, state and other agencies which maintain such records. In addition, investigative consumer reports as defined by the federal Fair Credit Reporting Act, gathered from personal interviews with former employers and other past or current associates of mine to gather information regarding my work  performance, character, general reputation and personal characteristics may be obtained.</p>";
                    var parsedHtmlElementsfirst = HTMLWorker.ParseToList(new StringReader(firstparagraph), styles);
                    foreach (var htmlElement in parsedHtmlElementsfirst)
                    {
                        string typeofelement = htmlElement.GetType().ToString();
                        ObjDocument.Add(htmlElement as IElement);

                    }
                    var secondparagraph = "<p align='justify' style='font-size:10'>I have the right to make a request to the consumer reporting agency: Intelifi, Inc. 8730 Wilshire Blvd, Suite 412, Beverly Hills, California 90211; telephone (888)409-1819 (“Agency”), upon proper identification, to request the nature and substance of all information in its files on me at the time of my request, including the sources of information and the agency, on our behalf, will provide a complete and accurate disclosure of the nature and scope of the investigation covered by the investigative consumer report(s); and the recipients of any reports on me which the agency has previously furnished within the two year period for employment requests, and one  year for other purposes preceding my request (California three years). I hereby consent to your obtaining the above information from the agency. You may view our privacy policy at our website: <Span style='COLOR:#3a90ff;'><u>www.intelifi.com.</u></Span> I hereby authorize procurement of consumer report(s) and investigative consumer report(s). If hired (or contracted), this authorization shall remain on file and shall serve as ongoing authorization for you to procure consumer reports at any time during my employment (or contract)  period.</p><br>";
                    var parsedHtmlElementssecond = HTMLWorker.ParseToList(new StringReader(secondparagraph), styles);

                    foreach (var htmlElement in parsedHtmlElementssecond)
                    {
                        string typeofelement = htmlElement.GetType().ToString();
                        ObjDocument.Add(htmlElement as IElement);

                    }

                    PdfPTable tabletableempty = new PdfPTable(3);
                    PdfPCell emptycell = new PdfPCell(new Phrase("", f));
                    emptycell.Colspan = 3;
                    emptycell.HorizontalAlignment = 1;
                    emptycell.BorderColor = iTextSharp.text.Color.WHITE;
                    tabletableempty.AddCell(emptycell);
                    ObjDocument.Add(tabletableempty);
                    string imageFilePath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Content\\themes\\base\\images\\box.jpg";
                    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imageFilePath);
                    image.Alignment = iTextSharp.text.Image.ALIGN_BOTTOM;
                    PdfPTable tableimg = new PdfPTable(24);
                    tableimg.DefaultCell.Border = 0;
                    tableimg.DefaultCell.BorderColor = Color.WHITE;
                    tableimg.WidthPercentage = 100;
                    PdfPCell celljpg = new PdfPCell(image);
                    celljpg.HorizontalAlignment = PdfPCell.ALIGN_TOP;
                    celljpg.BorderColor = iTextSharp.text.Color.WHITE;
                    tableimg.AddCell(celljpg);
                    PdfPCell cellimg = new PdfPCell(new Phrase("California, Minnesota and Oklahoma Applicants:Check box if you request a copy of your consumer report"));
                    cellimg.Colspan = 26;
                    ((Chunk)(cellimg.Phrase[0])).Font = new iTextSharp.text.Font(((Chunk)(cellimg.Phrase[0])).Font.Family, 9f);
                    cellimg.BorderColor = iTextSharp.text.Color.WHITE;
                    cellimg.HorizontalAlignment = PdfPCell.ALIGN_BOTTOM;
                    tableimg.AddCell(cellimg);
                    ObjDocument.Add(tableimg);
                    paragraph = "<p align='justify' style='font-size:10'><b>Notice to California Residents: </b>You have the right under Section 1786.22 of the California Civil Code to contact the Agency during reasonable hours (9:00 a.m. to 5:00 p.m. (PTZ) Monday through Friday) to obtain all information in your  file for your review. You may obtain such information as follows: 1) In person at the Agency’s offices, which address is listed above. You can have someone accompany you to the Agency’s offices. Agency may require this third party to present reasonable identification. You may be required at the time of such visit to sign an authorization for Agency to disclose to or discuss your information with this third party; 2) By certified mail, if you have previously provided identification in a written request that your file be sent to you or to a third party identified by you; 3) By telephone,if you have previously provided proper identification in writing to Agency; and  4) Agency has trained personnel to explain any information in your file to you and if the file contains any information that is coded, such will be explained to you.</p><br>";
                    var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(paragraph), styles);
                    foreach (var htmlElement in parsedHtmlElements)
                    {
                        string typeofelement = htmlElement.GetType().ToString();
                        ObjDocument.Add(htmlElement as IElement);
                    }

                    var paragraph2 = "<p align='justify' style='font-size:10'><b>Notice to New York Residents:</b>  I acknowledge receiving a copy of Article 23A of the NY Correction Law</p><br>";
                    var parsedHtmlElements2 = HTMLWorker.ParseToList(new StringReader(paragraph2), styles);
                    foreach (var htmlElement in parsedHtmlElements2)
                    {
                        string typeofelement = htmlElement.GetType().ToString();
                        ObjDocument.Add(htmlElement as IElement);
                    }

                    var main6 = "<p align='justify' style='font-size:11'><b>I AUTHORIZE, WITHOUT RESERVATION, ANY PARTY OR AGENCY CONTACTED BY THE CONSUMER REPORTING AGENCY TO FURNISH THE ABOVE-MENTIONED INFORMATION.</b></p><p align='justify' style='font-size:10'>I acknowledge that I have been provided a copy of consumer’s rights under the Fair Credit Reporting Act.</p><br>";
                    var parsedHtmlElements5 = HTMLWorker.ParseToList(new StringReader(main6), styles);
                    foreach (var htmlElement in parsedHtmlElements5)
                    {
                        string typeofelement = htmlElement.GetType().ToString();
                        ObjDocument.Add(htmlElement as IElement);
                    }


                    PdfPTable table2 = new PdfPTable(5);
                    table2.DefaultCell.Border = 0;
                    table2.DefaultCell.BorderColor = Color.WHITE;
                    table2.WidthPercentage = 100;
                    PdfPCell cell2 = new PdfPCell(new Phrase("", f));
                    cell2.Colspan = 5;
                    cell2.HorizontalAlignment = 1;
                    cell2.BorderColor = iTextSharp.text.Color.WHITE;
                    table2.AddCell(cell2);
                    PdfPCell cellfirstname = new PdfPCell(new Phrase(FullName, Verdana));
                    cellfirstname.BorderColor = iTextSharp.text.Color.WHITE;
                    table2.AddCell(cellfirstname);
                    table2.AddCell("           ");
                    table2.AddCell("");
                    PdfPCell cellssn = new PdfPCell(new Phrase(ssn, Verdana));
                    cellssn.BorderColor = iTextSharp.text.Color.WHITE;
                    table2.AddCell(cellssn);
                    PdfPCell celldob = new PdfPCell(new Phrase(dob, Verdana));
                    celldob.BorderColor = iTextSharp.text.Color.WHITE;
                    table2.AddCell(celldob);
                    PdfPCell cellprint = new PdfPCell(new Phrase("Print Name"));
                    cellprint.Colspan = 2;
                    cellprint.BorderColor = iTextSharp.text.Color.WHITE;
                    ((Chunk)(cellprint.Phrase[0])).Font = new iTextSharp.text.Font(((Chunk)(cellprint.Phrase[0])).Font.Family, 10f);
                    table2.AddCell(cellprint);
                    table2.AddCell("");
                    PdfPCell cellSecurity = new PdfPCell(new Phrase("Social Security #"));
                    cellSecurity.BorderColor = iTextSharp.text.Color.WHITE;
                    ((Chunk)(cellSecurity.Phrase[0])).Font = new iTextSharp.text.Font(((Chunk)(cellSecurity.Phrase[0])).Font.Family, 10f);
                    table2.AddCell(cellSecurity);
                    PdfPCell cellDobs = new PdfPCell(new Phrase("Date of Birth"));
                    cellDobs.BorderColor = iTextSharp.text.Color.WHITE;
                    ((Chunk)(cellDobs.Phrase[0])).Font = new iTextSharp.text.Font(((Chunk)(cellDobs.Phrase[0])).Font.Family, 10f);
                    table2.AddCell(cellDobs);
                    PdfPCell cellnew = new PdfPCell(new Phrase("", f));
                    cellnew.Colspan = 5;
                    cellnew.HorizontalAlignment = 1;
                    cellnew.BorderColor = iTextSharp.text.Color.WHITE;
                    table2.AddCell(cellnew);
                    PdfPCell cellnew1 = new PdfPCell(new Phrase("", f));
                    cellnew1.Colspan = 5;
                    cellnew1.HorizontalAlignment = 1;
                    cellnew1.BorderColor = iTextSharp.text.Color.WHITE;
                    table2.AddCell(cellnew1);
                    PdfPCell cellnew2 = new PdfPCell(new Phrase("", f));
                    cellnew2.Colspan = 5;
                    cellnew2.HorizontalAlignment = 1;
                    cellnew2.BorderColor = iTextSharp.text.Color.WHITE;
                    table2.AddCell(cellnew2);
                    PdfPCell cellfirstnameap = new PdfPCell(new Phrase(FullName, Verdana));
                    cellfirstnameap.BorderColor = iTextSharp.text.Color.WHITE;
                    table2.AddCell(cellfirstnameap);
                    table2.AddCell("           ");
                    PdfPCell cellDate = new PdfPCell(new Phrase(Date, Verdana));
                    cellDate.BorderColor = iTextSharp.text.Color.WHITE;
                    table2.AddCell(cellDate);
                    PdfPCell cell4 = new PdfPCell(new Phrase(email, Verdana));
                    cell4.Colspan = 2;
                    cell4.BorderColor = iTextSharp.text.Color.WHITE;
                    table2.AddCell(cell4);
                    PdfPCell cellApplicant = new PdfPCell(new Phrase("Applicant’s Signature"));
                    cellApplicant.Colspan = 2;
                    cellApplicant.BorderColor = iTextSharp.text.Color.WHITE;
                    ((Chunk)(cellApplicant.Phrase[0])).Font = new iTextSharp.text.Font(((Chunk)(cellApplicant.Phrase[0])).Font.Family, 10f);
                    table2.AddCell(cellApplicant);
                    PdfPCell cellDates = new PdfPCell(new Phrase("Date"));
                    cellDates.BorderColor = iTextSharp.text.Color.WHITE;
                    ((Chunk)(cellDates.Phrase[0])).Font = new iTextSharp.text.Font(((Chunk)(cellDates.Phrase[0])).Font.Family, 10f);
                    table2.AddCell(cellDates);
                    PdfPCell cell3 = new PdfPCell(new Phrase("Email(required in order to receive legal notices)"));
                    cell3.Colspan = 2;
                    cell3.BorderColor = iTextSharp.text.Color.WHITE;
                    ((Chunk)(cell3.Phrase[0])).Font = new iTextSharp.text.Font(((Chunk)(cell3.Phrase[0])).Font.Family, 9f);
                    table2.AddCell(cell3);
                    table2.DefaultCell.BorderColor = iTextSharp.text.Color.WHITE;
                    ObjDocument.Add(table2);
                    #endregion
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    ObjDocument.Close();
                }
            }
            return pdfFileName;
        }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "ocell"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "ex"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "DateToBePrint")]
        public string Confidentialform(string CompanyName, string Recipient, string Address)
        {

            string pdfFileName = string.Empty;
            iTextSharp.text.Rectangle ObjPageSize = new iTextSharp.text.Rectangle(PageSize.A4);
            Document ObjDocument = new Document(ObjPageSize, 55, 55, 15, 12);
            DateTime dt = DateTime.Now;
            string sDate = dt.ToShortDateString();
            //float Leading = 13;
            //DateTime DateToBePrint = DateTime.Now;
            try
            {
                pdfFileName = "Pre_Adverse_File" + DateTime.Now.ToFileTime().ToString() + ".pdf";
                DirectoryInfo dir = null;
                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\AdversePDF")))
                {
                    Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\AdversePDF"));
                }
                else
                {

                    dir = new DirectoryInfo(System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\AdversePDF"));
                    FileSystemInfo[] info = dir.GetFiles();
                    if (info.Length > 0)
                    {
                        foreach (FileInfo fi in info)
                        {
                            if (fi.Name.Contains(""))
                            {
                                string FilePath = System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\AdversePDF\") + fi.Name;
                                if (System.IO.File.Exists(FilePath))
                                {
                                    try
                                    {
                                        System.IO.File.Delete(FilePath);
                                    }
                                    catch //(Exception ex)
                                    {
                                        // ListErrors.Add("File Used By Another Process : " + ex.Message.ToString());
                                    }

                                }
                            }
                        }
                    }
                }
                StyleSheet styles = new iTextSharp.text.html.simpleparser.StyleSheet();
               //Font Verdana = FontFactory.GetFont("Verdana", 10F, Font.UNDERLINE);
                string path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Resources\\Upload\\AdversePDF\\" + pdfFileName;
                PdfWriter writer = PdfWriter.GetInstance(ObjDocument, new FileStream(path, FileMode.Create));
                ObjDocument.Open();
                #region for blankspace
                Chunk spaceChunk = new Chunk("                                                                                                                                                     ");
                #endregion
                Phrase op = new Phrase();
                Cell ocell = new Cell(op);
                Chunk ock = new Chunk();
                string paragraph = string.Empty;
                #region Main Paragraph
                paragraph = "";
                StringBuilder sb = new StringBuilder();
                var f = FontFactory.GetFont("Arial", 14, Font.BOLD);
                var f2 = FontFactory.GetFont("Arial", 10, Font.NORMAL);
                //string imageFilePath2 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Content\\themes\\base\\images\\usaintel_Dispute.jpg";
                //string imageFilePath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Content\\themes\\base\\images\\usaintelPowerToKnow.jpg";
                //iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imageFilePath);
                //iTextSharp.text.Image image2 = iTextSharp.text.Image.GetInstance(imageFilePath2);
                //image.Alignment = iTextSharp.text.Image.ALIGN_BOTTOM;
                //PdfPTable tableimg = new PdfPTable(5);
                //tableimg.DefaultCell.Border = 0;
                //tableimg.DefaultCell.BorderColor = Color.WHITE;
                //tableimg.WidthPercentage = 100;
                //PdfPCell celljpg = new PdfPCell(image);
                //celljpg.HorizontalAlignment = PdfPCell.ALIGN_TOP;
                //celljpg.BorderColor = iTextSharp.text.Color.WHITE;
                //tableimg.AddCell(celljpg);
                PdfPCell cellnew = new PdfPCell(new Phrase("", f));
                //cellnew.Colspan = 2;
                //cellnew.HorizontalAlignment = 1;
                //cellnew.BorderColor = iTextSharp.text.Color.WHITE;
                //tableimg.AddCell(cellnew);
                //PdfPCell cell = new PdfPCell(new Phrase("                                              ", f));
                //cell.Colspan = 2;
                //cell.HorizontalAlignment = 1;
                //cell.VerticalAlignment = 4;
                //cell.BorderColor = iTextSharp.text.Color.WHITE;
                //tableimg.AddCell(cell);
                //ObjDocument.Add(tableimg);
                //string imageFilePathhd = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Content\\themes\\base\\images\\Letter_Header.jpg";
                //iTextSharp.text.Image imageh = iTextSharp.text.Image.GetInstance(imageFilePathhd);
                //imageh.Alignment = iTextSharp.text.Image.ALIGN_BOTTOM;
                //PdfPTable tableimgh = new PdfPTable(1);
                //tableimgh.DefaultCell.Border = 0;
                //tableimgh.DefaultCell.BorderColor = Color.WHITE;
                //tableimgh.WidthPercentage = 100;
                //PdfPCell cellheader = new PdfPCell(imageh);
                //cellheader.HorizontalAlignment = PdfPCell.ALIGN_TOP;
                //cellheader.BorderColor = iTextSharp.text.Color.WHITE;
                //tableimgh.AddCell(cellheader);
                //ObjDocument.Add(tableimgh);
                var Space = "<br>";
                var parsedHtmlElements5 = HTMLWorker.ParseToList(new StringReader(Space), styles);
                foreach (var htmlElement in parsedHtmlElements5)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);
                }
                var fHeading = FontFactory.GetFont("Arial", 11, Font.BOLD);
                //var fHeading1 = FontFactory.GetFont("Arial", 11, Font.UNDERLINE);
                //var fHeading2 = FontFactory.GetFont("Arial", 10, Font.NORMAL);
                PdfPTable tableHeading = new PdfPTable(1);
                tableHeading.DefaultCell.Border = 0;
                tableHeading.DefaultCell.BorderColor = Color.WHITE;
                tableHeading.WidthPercentage = 100;
                PdfPCell cellheading = new PdfPCell(new Phrase("                                                 Pre-Adverse Action Letter", fHeading));
                cellheading.BorderColor = iTextSharp.text.Color.WHITE;
                tableHeading.AddCell(cellheading);
                ObjDocument.Add(tableHeading);
                var Space2 = "<br><br>";
                var parsedHtmlElements6 = HTMLWorker.ParseToList(new StringReader(Space2), styles);
                foreach (var htmlElement in parsedHtmlElements6)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);
                }
                var fText = FontFactory.GetFont("Arial", 11, Font.NORMAL);
                PdfPTable tableinfo = new PdfPTable(1);
                tableinfo.DefaultCell.Border = 0;
                tableinfo.DefaultCell.BorderColor = Color.WHITE;
                tableinfo.WidthPercentage = 100;
                PdfPCell celltoday = new PdfPCell(new Phrase(sDate, fText));
                celltoday.BorderColor = iTextSharp.text.Color.WHITE;
                PdfPCell cellcompanyname = new PdfPCell(new Phrase("", fText));
                cellcompanyname.BorderColor = iTextSharp.text.Color.WHITE;
                PdfPCell celladdress = new PdfPCell(new Phrase(Address, fText));
                celladdress.BorderColor = iTextSharp.text.Color.WHITE;
                tableinfo.AddCell(celltoday);
                tableinfo.AddCell(cellcompanyname);
                //tableinfo.AddCell(celladdress);
                ObjDocument.Add(tableinfo);
                string[] strCompanyName = CompanyName.Split('_');
                var Space3 = "<br>";
                var parsedHtmlElements7 = HTMLWorker.ParseToList(new StringReader(Space3), styles);
                foreach (var htmlElement in parsedHtmlElements7)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);
                }
                paragraph = "<span style='font-size:11'>Dear  " + Recipient + "</span>:<br><br><p align='justify' style='font-size:11;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This notification is being provided to advise you of our intent to take adverse employment action, e.g., not hire, terminate, etc., against you, based in whole or in part, upon information contained in a consumer report provided to us by Intelifi, Inc. (a copy of that report is attached for your reference along with a copy of your rights under the Fair Credit Reporting Act).</p><br/><p align='justify' style='font-size:11'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;If you believe that this report is incomplete or inaccurate, or wish to explain and/or provide mitigating information, please advise me immediately.  You may contact Intelifi at: (888) 409-1819, and advise them that you dispute the report if you, in fact, dispute its contents.   They will begin an investigation of your dispute.  To ensure accurate communication, have the report in hand when you call them.   We will delay taking adverse action for five (5) business days from the date of this letter.  At the conclusion of that period we will take the contemplated adverse action.  However, that does not affect your rights to dispute the contents of the report or your file with Intelifi at any time.  It is our policy to individually assess each applicant, and your input regarding the explanation of, or provide mitigating information regarding a criminal record or other adverse information is welcomed.  Contact us so we can properly evaluate your situation and determine whether or not to take the contemplated adverse action.</p><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p align='justify' style='font-size:11;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;If you have any questions, please contact our office.</p><br><br><p align='justify' style='font-size:11;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Very truly yours,</p><br><p align='justify' style='font-size:11;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + strCompanyName[0] + " | " + strCompanyName[1] + "</p>";
                var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(paragraph), styles);
                foreach (var htmlElement in parsedHtmlElements)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);
                }
                var Space4 = "<br>";
                var parsedHtmlElements9 = HTMLWorker.ParseToList(new StringReader(Space4), styles);
                foreach (var htmlElement in parsedHtmlElements9)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);
                }
                var SpaceBlank = "<br><br><br><br><br><br><br><br><br><br><br><br><br>";
                var parsedHtmlElementsBlank = HTMLWorker.ParseToList(new StringReader(SpaceBlank), styles);
                foreach (var htmlElement in parsedHtmlElementsBlank)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);
                }
                //string imageFilePathft = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Content\\themes\\base\\images\\Letter_Fotter.png";
                //iTextSharp.text.Image imageft = iTextSharp.text.Image.GetInstance(imageFilePathft);
                //imageft.Alignment = iTextSharp.text.Image.ALIGN_BOTTOM;
                //PdfPTable tableimgft = new PdfPTable(5);
                //tableimgft.DefaultCell.Border = 0;
                //tableimgft.DefaultCell.BorderColor = Color.WHITE;
                //tableimgft.WidthPercentage = 100;
                //PdfPCell cellft = new PdfPCell(imageft);
                //cellft.HorizontalAlignment = PdfPCell.ALIGN_TOP;
                //cellft.BorderColor = iTextSharp.text.Color.WHITE;
                //tableimgft.AddCell(cellft);
                //PdfPCell cellnew4 = new PdfPCell(new Phrase("", f));
                //cellnew4.Colspan = 4;
                //cellnew4.BorderColor = iTextSharp.text.Color.WHITE;
                //tableimgft.AddCell(cellnew4);
                //ObjDocument.Add(tableimgft);
                var Space_4 = "<br>";
                var parsedHtmlElements_9 = HTMLWorker.ParseToList(new StringReader(Space_4), styles);
                foreach (var htmlElement in parsedHtmlElements_9)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);
                }
                //var paragraph_new = "<p align='justify' style='font-size:10'><I><b>Para information en espanol, visite <Span style='COLOR:#0000ff;'><u>www.consumerfinance.gov/learnmore</u></Span> o escribe a la Consumer Financial Protection Bureau , 1700G Street N.W., Washington, DC 2006.</b></p><I>";
                //var parsedHtmlElementsfirst = HTMLWorker.ParseToList(new StringReader(paragraph_new), styles);
                //foreach (var htmlElement in parsedHtmlElementsfirst)
                //{
                //    string typeofelement = htmlElement.GetType().ToString();
                //    ObjDocument.Add(htmlElement as IElement);

                //}
                var Space_summary = "<br><br>";
                var parsedHtmlElements_summary = HTMLWorker.ParseToList(new StringReader(Space_summary), styles);
                foreach (var htmlElement in parsedHtmlElements_summary)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);
                }
                var fsummary = FontFactory.GetFont("Arial", 10, Font.BOLD);
                PdfPTable tablesummary = new PdfPTable(3);
                PdfPCell cellsummary = new PdfPCell(new Phrase("A Summary of Your Rights Under the Fair Credit Reporting Act", fsummary));
                cellsummary.Colspan = 3;
                cellsummary.HorizontalAlignment = 1;
                cellsummary.BorderColor = iTextSharp.text.Color.WHITE;
                tablesummary.AddCell(cellsummary);
                ObjDocument.Add(tablesummary);
                var paragraph_rights = "<p align='justify' style='font-size:11'>The federal Fair Credit Reporting Act (FCRA) promotes the accuracy, fairness, and privacy of information in the files of consumer reporting agencies. There are many types of consumer reporting agencies, including credit bureaus and specialty agencies (such as agencies that sell information about check writing histories, medical records, and rental history records). Here is a summary of your major rights under the FCRA.<b> For more information, including information about additional rights, go to <Span style='COLOR:#0000ff;'><u>www.consumerfinance.gov/learnmore</u></Span> or write to: Consumer Financial Protection Bureau, 1700 G Street N.W., Washington, D.C. 20006.<br></b></p>";
                var parsedparagraph_rights = HTMLWorker.ParseToList(new StringReader(paragraph_rights), styles);
                foreach (var htmlElement in parsedparagraph_rights)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);

                }
                var paragraph_rights2 = "<p align='justify' style='font-size:11'><b>You may have additional rights under Maine's FCRA, Me. Rev. Stat. Ann. 10, Sec 1311 etseq.</b></p><br><p align='justify' style='font-size:11'>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>You must be told if information in your file has been used against you.</b> Anyone whouses a credit report or another type of consumer report to deny your application forcredit, insurance, or employment – or to take another adverse action against you –must tell you, and must give you the name, address, and phone number of the agency that provided the information.<br><br>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>You have the right to know what is in your file.</b> You may request and obtain all the information about you in the files of a consumer reporting agency (your “filedisclosure”). You will be required to provide proper identification, which may include your Social Security number. In many cases, the disclosure will be free.You are entitled to a free file disclosure if:<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;•	A person has taken adverse action against you because of information in your credit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;report;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;•You are the victim of identify theft and place a fraud alert in your file;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;•	Your file contains inaccurate information as a result of fraud;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;•	You are on public assistance;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;•	You are unemployed but expect to apply for employment within 60 days.<br>In addition, all consumers are entitled to one free disclosure every 12 months upon request from each nationwide credit bureau and from nationwide specialty consumer reporting agencies. See <Span style='COLOR:#0000ff;'><u>www.consumerfinance.gov/learnmore</u></Span> for additional information.<br><br>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>You have the right to ask for a credit score.</b>Credit scores are numerical summaries of your credit-worthiness based on information from credit bureaus. You may request a credit score from consumer reporting agencies that create scores or distribute scores used in residential real property loans, but you will have to pay for it. In some mortgage transactions, you will receive credit score information for free from the mortgage lender.<br><br>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>You have the right to dispute incomplete or inaccurate information.</b>If you identify information in your file that is incomplete or inaccurate, and report it to the consumer reporting agency, the agency must investigate unless your dispute is frivolous. See <Span style='COLOR:#0000ff;'><u>www.consumerfinance.gov/learnmore</u></Span> for an explanation of dispute procedures.<br><br>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Consumer reporting agencies must correct or delete inaccurate, incomplete, or unverifiable information.</b> Inaccurate, incomplete or unverifiable information must be removed or corrected, usually within 30 days. However, a consumer reporting agency may continue to report information it has verified as accurate.<br><br>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Consumer reporting agencies may not report outdated negative information.</b>In most cases, a consumer reporting agency may not report negative informationthat is more than seven years old, or bankruptcies that are more than 10 years old. <br>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Access to your file is limited.</b> A consumer reporting agency may provide information about you only to people with a valid need -- usually to consider an application with a creditor, insurer, employer, landlord, or other business. The FCRA specifies those with a valid need for access.<br><br>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>You must give your consent for reports to be provided to employers.</b> A consumer reporting agency may not give out information about you to your employer, or a potential employer, without your written consent given to the employer. Written consent generally is not required in the trucking industry. For more information, go to <Span style='COLOR:#0000ff;'><u>www.consumerfinance.gov/learnmore</u></Span>.<br><br>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>You may limit “prescreened” offers of credit and insurance you get based on information in your credit report.</b> Unsolicited “prescreened” offers for credit and insurance must include a toll-free phone number you can call if you choose to remove your name and address from the lists these offers are based on. You may opt-out with the nationwide credit bureaus at 1-888-567-8688.<br><br>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>You may seek damages from violators.</b> If a consumer reporting agency, or, in some cases, a user of consumer reports or a furnisher of information to a consumer re orting agency violates the FCRA, you may be able to sue in state or federal court.<br><br>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Identity theft victims and active duty military personnel have additional rights.</b> For more information, visit <Span style='COLOR:#0000ff;'><u>www.consumerfinance.gov/learnmore</u></Span>.<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br></p><p align='justify' style='font-size:11'><b>States may enforce the FCRA, and many states have their own consumer reporting laws. In some cases, you may have more rights under state law. For more information,contact your state or local consumer protection agency or your state Attorney General. For information about your federal rights, contact:</b></p><br><br><br>";
                var parsedparagraph_rights2 = HTMLWorker.ParseToList(new StringReader(paragraph_rights2), styles);
                foreach (var htmlElement in parsedparagraph_rights2)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);

                }
                Font FontInfoBold = FontFactory.GetFont("Arial", 7F, Font.BOLD);
                Font FontInfoNormal = FontFactory.GetFont("Arial", 7F, Font.NORMAL);
                PdfPTable ObjPdfTblMainBottom = new PdfPTable(2);
                ObjPdfTblMainBottom.DefaultCell.Border = 6;
                ObjPdfTblMainBottom.DefaultCell.BorderColor = Color.BLACK;
                ObjPdfTblMainBottom.WidthPercentage = 115;
                PdfPCell cellnew1 = new PdfPCell(new Phrase("TYPE OF BUSINESS:", FontInfoBold));
                cellnew1.BorderWidth = 1;
                cellnew1.HorizontalAlignment = 1;
                cellnew1.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdfTblMainBottom.AddCell(cellnew1);
                PdfPCell cellnew2 = new PdfPCell(new Phrase("CONTACT:", FontInfoBold));
                cellnew2.BorderWidth = 1;
                cellnew2.HorizontalAlignment = 1;
                cellnew2.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdfTblMainBottom.AddCell(cellnew2);
                string strTL1 = string.Empty;
                strTL1 = "\n\n1.a. Banks, saving associations, and credit unions with total assets of over $10 billion and their"
                + " affiliates.\n\n"
                + "\nb. Such affiliates that are not banks,saving associations, or credit unions also should list, in"
                + " addition to the Bureau:\n\n";
                PdfPCell cellnew3 = new PdfPCell(new Phrase(strTL1, FontInfoNormal));
                cellnew3.BorderWidth = 1;
                cellnew3.HorizontalAlignment = Element.ALIGN_LEFT;
                cellnew3.VerticalAlignment = Element.ALIGN_CENTER;
                cellnew3.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdfTblMainBottom.AddCell(cellnew3);
                string strTR1 = string.Empty;
                strTR1 = "a. Bureau of Consumer Financial Protection"
                + "\n1700 G Street NW"
                + "\nWashington, DC 20006\n\n"
                + "\nb. Federal Trade Commission: ConsumerResponse Center-"
                + "\nFCRA"
                + "\nWashington, DC 20580"
                + "\n(877)382-4357";
                PdfPCell cellnew_4 = new PdfPCell(new Phrase(strTR1, FontInfoNormal));
                cellnew_4.BorderWidth = 1;
                cellnew_4.HorizontalAlignment = Element.ALIGN_LEFT;
                cellnew_4.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdfTblMainBottom.AddCell(cellnew_4);
                string strTL2 = string.Empty;
                strTL2 = "\n\n2. To the extent not included in item 1 above:\n\n"
                + "\na. National Banks, federal saving associations, and federal branches ad federal agencies of "
                + "foreign banks\n\n"
                + "\nb. State member banks, branches and agencies of foreign banks( other than federal branches,"
                + " federal agencies and insured state branches of foreign banks), commercial leading companies"
                + " owned or controlled by foreign banks, and organizations operating under section 25 or 25A of"
                + " the Federal Reserve Act\n\n"
                + "\nc. Nonmember Insured Banks, Insured State Branches of Foreign banks and insured state"
                + " saving associations\n\n"
                + "\nd. Federal Credit Unions\n\n";
                PdfPCell cellnew_5 = new PdfPCell(new Phrase(strTL2, FontInfoNormal));
                cellnew_5.BorderWidth = 1;
                cellnew_5.HorizontalAlignment = Element.ALIGN_LEFT;
                cellnew_5.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdfTblMainBottom.AddCell(cellnew_5);
                string strTR2 = string.Empty;
                strTR2 = "a. Office of Comptroller of the Currency"
                + "\nCustomer Assistance Group"
                + "\n1301 McKinnney Street, Suite 3450"
                + "\nHouston, TX 77010-9050\n\n"
                + "\nb. Federal Reserve Consumer Help Center"
                + "\nPO Box 1200"
                + "\nMinneapolis, MN55480\n\n"
                + "\nc. FDIC Consumer Response Center"
                + "\n1100 Walnut Street, Box #11"
                + "\nKansas City, MO 64106\n\n"
                + "\nd. National Credit Union Administrator"
                + "\nOffice of Consumer Protection(OCP)"
                + "\nDivision of Consumer Compliance and Outreach(DCCO)"
                + "\n1775 Duke Street"
                + "\nAlexandria, VA 22314";
                PdfPCell cellnew_6 = new PdfPCell(new Phrase(strTR2, FontInfoNormal));
                cellnew_6.BorderWidth = 1;
                cellnew_6.HorizontalAlignment = Element.ALIGN_LEFT;
                cellnew_6.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdfTblMainBottom.AddCell(cellnew_6);
                string strTR7 = string.Empty;
                strTR7 = "\n3. Air carriers\n\n";
                PdfPCell cellnew_7 = new PdfPCell(new Phrase(strTR7, FontInfoNormal));
                cellnew_7.BorderWidth = 1;
                cellnew_7.HorizontalAlignment = Element.ALIGN_LEFT;
                cellnew_7.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdfTblMainBottom.AddCell(cellnew_7);
                string strTR3 = string.Empty;
                strTR3 = "Asst.General Counsel for Aviation Enforcement & Proceeding"
                + "\n Department ofTransportation"
                + "\n400 Seventh Street SW"
                + "\nWashington, DC 20590";
                PdfPCell cellnew_8 = new PdfPCell(new Phrase(strTR3, FontInfoNormal));
                cellnew_8.HorizontalAlignment = Element.ALIGN_LEFT;
                cellnew_8.BorderWidth = 1;
                cellnew_8.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdfTblMainBottom.AddCell(cellnew_8);
                string strTR9 = string.Empty;
                strTR9 = "\n4. Creditors Subject to Surface Transportation Board\n\n";
                PdfPCell cellnew_9 = new PdfPCell(new Phrase(strTR9, FontInfoNormal));
                cellnew_9.BorderWidth = 1;
                cellnew_9.HorizontalAlignment = Element.ALIGN_LEFT;
                cellnew_9.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdfTblMainBottom.AddCell(cellnew_9);
                string strTR4 = string.Empty;
                strTR4 = "Office of Proceedings, Surface Transportation Board"
                + "\nDepartment of Transportation"
                + "\n1925 K Street NW"
                + "\nWashington, DC 20423";
                PdfPCell cellnew_10 = new PdfPCell(new Phrase(strTR4, FontInfoNormal));
                cellnew_10.BorderWidth = 1;
                cellnew_10.HorizontalAlignment = Element.ALIGN_LEFT;
                cellnew_10.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdfTblMainBottom.AddCell(cellnew_10);
                string strTR11 = string.Empty;
                strTR11 = "\n5. Creditors Subject to Packers and Stockyards Act\n\n";
                PdfPCell cellnew_11 = new PdfPCell(new Phrase(strTR11, FontInfoNormal));
                cellnew_11.BorderWidth = 1;
                cellnew_11.HorizontalAlignment = Element.ALIGN_LEFT;
                cellnew_11.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdfTblMainBottom.AddCell(cellnew_11);
                string strTR5 = string.Empty;
                strTR5 = "\nNearest packers and Stockyards Administration area supervisor\n\n";
                PdfPCell cellnew_12 = new PdfPCell(new Phrase(strTR5, FontInfoNormal));
                cellnew_12.BorderWidth = 1;
                cellnew_12.HorizontalAlignment = Element.ALIGN_LEFT;
                cellnew_12.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdfTblMainBottom.AddCell(cellnew_12);
                string strTR_6 = string.Empty;
                strTR_6 = "\n6. Small Business Investment Companies\n\n";
                PdfPCell cellnew_13 = new PdfPCell(new Phrase(strTR_6, FontInfoNormal));
                cellnew_13.BorderWidth = 1;
                cellnew_13.HorizontalAlignment = Element.ALIGN_LEFT;
                cellnew_13.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdfTblMainBottom.AddCell(cellnew_13);
                string strTR6 = string.Empty;
                strTR6 = "Associate Deputy Administrator for CapitalAccess"
                + " United States Small Business Administration"
                + " 406 Third Street, SW, 8TH Floor"
                + " Washington, DC 20416";
                PdfPCell cellnew_14 = new PdfPCell(new Phrase(strTR6, FontInfoNormal));
                cellnew_14.HorizontalAlignment = Element.ALIGN_LEFT;
                cellnew_14.BorderWidth = 1;
                cellnew_14.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdfTblMainBottom.AddCell(cellnew_14);
                string strTR15 = string.Empty;
                strTR15 = "\n7. Brokers and Dealers\n\n";
                PdfPCell cellnew_15 = new PdfPCell(new Phrase(strTR15, FontInfoNormal));
                cellnew_15.BorderWidth = 1;
                cellnew_15.HorizontalAlignment = Element.ALIGN_LEFT;
                cellnew_15.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdfTblMainBottom.AddCell(cellnew_15);
                string strTR_7 = string.Empty;
                strTR_7 = "Securities and Exchange Commission"
                + "\n100 F St NE"
                + "\nWashington, DC 20549";
                PdfPCell cellnew_16 = new PdfPCell(new Phrase(strTR_7, FontInfoNormal));
                cellnew_16.BorderWidth = 1;
                cellnew_16.HorizontalAlignment = Element.ALIGN_LEFT;
                cellnew_16.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdfTblMainBottom.AddCell(cellnew_16);
                string strTL8 = string.Empty;
                strTL8 = "\n8. Federal Land Banks, Federal Land Bank Associations, Federal Intermediate Credit Banks,"
                + " and production Credit Associations\n\n";
                PdfPCell cellnew_17 = new PdfPCell(new Phrase(strTL8, FontInfoNormal));
                cellnew_17.BorderWidth = 1;
                cellnew_17.HorizontalAlignment = Element.ALIGN_LEFT;
                cellnew_17.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdfTblMainBottom.AddCell(cellnew_17);
                string strTR8 = string.Empty;
                strTR8 = "Farm Credit Administration"
                + "\n1501 Farm Credit Drive"
                + "\nMcLean, VA 22102-5090";
                PdfPCell cellnew_18 = new PdfPCell(new Phrase(strTR8, FontInfoNormal));
                cellnew_18.BorderWidth = 1;
                cellnew_18.HorizontalAlignment = Element.ALIGN_LEFT;
                cellnew_18.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdfTblMainBottom.AddCell(cellnew_18);
                string strTR_9 = string.Empty;
                strTR_9 = "\n9. Retailers, Finance Companies, and All Other Creditors Not Listed Above\n\n";
                PdfPCell cellnew_19 = new PdfPCell(new Phrase(strTR_9, FontInfoNormal));
                cellnew_19.BorderWidth = 1;
                cellnew_19.HorizontalAlignment = Element.ALIGN_LEFT;
                cellnew_19.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdfTblMainBottom.AddCell(cellnew_19);
                string strTR20 = string.Empty;
                strTR20 = "FTC Regional Office for region in which the creditor operates or"
                + "\nFederal Trade Commission: Consumer Response Center- FCRA"
                + "\nWashington, DC 20580"
                + "\n(877)382-4357";
                PdfPCell cellnew_20 = new PdfPCell(new Phrase(strTR20, FontInfoNormal));
                cellnew_20.BorderWidth = 1;
                cellnew_20.HorizontalAlignment = Element.ALIGN_LEFT;
                cellnew_20.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdfTblMainBottom.AddCell(cellnew_20);
                ObjDocument.Add(ObjPdfTblMainBottom);
                var Space_Dispute = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>";
                var parsedHtmlElements_Space_Dispute = HTMLWorker.ParseToList(new StringReader(Space_Dispute), styles);
                foreach (var htmlElement in parsedHtmlElements_Space_Dispute)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);
                }
                PdfPTable tableDisputeImg = new PdfPTable(5);
                tableDisputeImg.DefaultCell.Border = 0;
                tableDisputeImg.DefaultCell.BorderColor = Color.WHITE;
                tableDisputeImg.WidthPercentage = 75;
                PdfPCell cellDisp = new PdfPCell(new Phrase("", f));
                cellDisp.Colspan = 2;
                cellDisp.HorizontalAlignment = 1;
                cellDisp.BorderColor = iTextSharp.text.Color.WHITE;
                tableDisputeImg.AddCell(cellDisp);
                PdfPCell cellDisp1 = new PdfPCell(new Phrase("", f));
                cellDisp1.Colspan = 2;
                cellDisp1.HorizontalAlignment = 1;
                cellDisp1.VerticalAlignment = 4;
                cellDisp1.BorderColor = iTextSharp.text.Color.WHITE;
                tableDisputeImg.AddCell(cellDisp1);
                //PdfPCell celljpg1 = new PdfPCell(image2);
                //celljpg1.HorizontalAlignment = PdfPCell.ALIGN_TOP;
                //celljpg1.BorderColor = iTextSharp.text.Color.WHITE;
                //tableDisputeImg.AddCell(celljpg1);
                //ObjDocument.Add(tableDisputeImg);
                PdfPTable tableDisputetxt = new PdfPTable(5);
                tableDisputetxt.DefaultCell.Border = 0;
                tableDisputetxt.DefaultCell.BorderColor = Color.WHITE;
                tableDisputetxt.WidthPercentage = 105;
                PdfPCell celltxt = new PdfPCell(new Phrase("", f));
                celltxt.Colspan = 2;
                celltxt.HorizontalAlignment = 1;
                celltxt.BorderColor = iTextSharp.text.Color.WHITE;
                tableDisputetxt.AddCell(celltxt);
                string strDisp = string.Empty;
                strDisp = "Disclosure Department "
                + "\n8730 Wilshire Blvd #412 Beverly Hills, CA 90211"
                + "\nToll Free Number: 888.409.1819\n Fax: 310.623.1820\n\n";
                PdfPCell celltxt2 = new PdfPCell(new Phrase(strDisp, f2));
                celltxt2.HorizontalAlignment = Element.ALIGN_RIGHT;
                celltxt2.Colspan = 3;
                celltxt2.BorderColor = iTextSharp.text.Color.WHITE;
                tableDisputetxt.AddCell(celltxt2);
                ObjDocument.Add(tableDisputetxt);
                var paragraph_Dispute = "<p align='center' style='font-size:10'><b><u>DISPUTE NOTIFICATION FORM</u></b></p>";
                var parsedHtmlElementsDispute = HTMLWorker.ParseToList(new StringReader(paragraph_Dispute), styles);
                foreach (var htmlElement in parsedHtmlElementsDispute)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);

                }
                var paragraph_Dis = "<p align='justify' style='font-size:10'>If you wish to dispute the accuracy of any information contained within the intelifi.com, inc. (intelifi.COM) consumer or investigative consumer report completed on you by intelifi.COM, please forward us the documents listed below and check one of the following reasons this report was conducted:</p><br>";
                var parsedHtmlElementsDis = HTMLWorker.ParseToList(new StringReader(paragraph_Dis), styles);
                foreach (var htmlElement in parsedHtmlElementsDis)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);

                }
                string imageFile_Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Content\\themes\\base\\images\\box_dis.jpg";
                iTextSharp.text.Image imagebox = iTextSharp.text.Image.GetInstance(imageFile_Path);
                imagebox.Alignment = iTextSharp.text.Image.ALIGN_BOTTOM;
                PdfPTable table_img = new PdfPTable(24);
                table_img.DefaultCell.Border = 0;
                table_img.DefaultCell.BorderColor = Color.WHITE;
                table_img.WidthPercentage = 100;
                PdfPCell cell_jpg = new PdfPCell(imagebox);
                cell_jpg.HorizontalAlignment = PdfPCell.ALIGN_TOP;
                cell_jpg.BorderColor = iTextSharp.text.Color.WHITE;
                table_img.AddCell(cell_jpg);
                PdfPCell emptycell = new PdfPCell(new Phrase("", f));
                emptycell.Colspan = 1;
                emptycell.HorizontalAlignment = 1;
                emptycell.BorderColor = iTextSharp.text.Color.WHITE;
                table_img.AddCell(emptycell);
                PdfPCell cellimg = new PdfPCell(new Phrase("Consumer Report: search conducted on yourself"));
                cellimg.Colspan = 22;
                ((Chunk)(cellimg.Phrase[0])).Font = new iTextSharp.text.Font(((Chunk)(cellimg.Phrase[0])).Font.Family, 9f);
                cellimg.BorderColor = iTextSharp.text.Color.WHITE;
                cellimg.HorizontalAlignment = PdfPCell.ALIGN_BOTTOM;
                table_img.AddCell(cellimg);
                PdfPCell cell_jpg2 = new PdfPCell(imagebox);
                cell_jpg2.HorizontalAlignment = PdfPCell.ALIGN_TOP;
                cell_jpg2.BorderColor = iTextSharp.text.Color.WHITE;
                table_img.AddCell(cell_jpg2);
                PdfPCell emptycell2 = new PdfPCell(new Phrase("", f));
                emptycell2.Colspan = 1;
                emptycell2.HorizontalAlignment = 1;
                emptycell2.BorderColor = iTextSharp.text.Color.WHITE;
                table_img.AddCell(emptycell2);
                PdfPCell cellimg2 = new PdfPCell(new Phrase("Investigative Consumer Report: search requested by (company name)_____________________ for employment purposes"));
                cellimg2.Colspan = 22;
                ((Chunk)(cellimg2.Phrase[0])).Font = new iTextSharp.text.Font(((Chunk)(cellimg2.Phrase[0])).Font.Family, 9f);
                cellimg2.BorderColor = iTextSharp.text.Color.WHITE;
                cellimg2.HorizontalAlignment = PdfPCell.ALIGN_BOTTOM;
                table_img.AddCell(cellimg2);
                PdfPCell cell_jpg3 = new PdfPCell(imagebox);
                cell_jpg3.HorizontalAlignment = PdfPCell.ALIGN_TOP;
                cell_jpg3.BorderColor = iTextSharp.text.Color.WHITE;
                table_img.AddCell(cell_jpg3);
                PdfPCell emptycell3 = new PdfPCell(new Phrase("", f));
                emptycell3.Colspan = 1;
                emptycell3.HorizontalAlignment = 1;
                emptycell3.BorderColor = iTextSharp.text.Color.WHITE;
                table_img.AddCell(emptycell3);
                PdfPCell cellimg3 = new PdfPCell(new Phrase("Other: _______________________________________________________________________"));
                cellimg3.Colspan = 22;
                ((Chunk)(cellimg3.Phrase[0])).Font = new iTextSharp.text.Font(((Chunk)(cellimg3.Phrase[0])).Font.Family, 9f);
                cellimg3.BorderColor = iTextSharp.text.Color.WHITE;
                cellimg3.HorizontalAlignment = PdfPCell.ALIGN_BOTTOM;
                table_img.AddCell(cellimg3);
                PdfPCell cellimg4 = new PdfPCell(new Phrase(" You may fax (310.623.1820) or mail (8730 Wilshire Blvd #412 Beverly Hills, CA 90211) the following information in order for the Dispute Resolution process to begin:"));
                cellimg4.Colspan = 24;
                ((Chunk)(cellimg4.Phrase[0])).Font = new iTextSharp.text.Font(((Chunk)(cellimg4.Phrase[0])).Font.Family, 9f);
                cellimg4.BorderColor = iTextSharp.text.Color.WHITE;
                cellimg4.HorizontalAlignment = PdfPCell.ALIGN_BOTTOM;
                table_img.AddCell(cellimg4);
                ObjDocument.Add(table_img);
                var paragraph_1 = "<p align='justify' style='font-size:10'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;&nbsp;&nbsp;Dispute Notification Form signed</p><p align='justify' style='font-size:10'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;&nbsp;&nbsp;Copy of a state issued picture identification</p><p align='justify' style='font-size:10'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;&nbsp;&nbsp;Court documents supporting changes being requested, if available</p>";
                var parsedparagraph_1 = HTMLWorker.ParseToList(new StringReader(paragraph_1), styles);
                foreach (var htmlElement in parsedparagraph_1)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);
                }
                var paragraph_2 = "<p align='justify' style='font-size:10'> You will be contacted by intelifi.COM following the receipt of your signed form. If you have any further questions, you may contact the intelifi.COM disclosure department by calling us toll free at 888.409.1819.</p><br><br>";
                var parsedparagraph_2 = HTMLWorker.ParseToList(new StringReader(paragraph_2), styles);
                foreach (var htmlElement in parsedparagraph_2)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);
                }
                PdfPTable ObjPdfTbl_detail = new PdfPTable(5);
                ObjPdfTbl_detail.WidthPercentage = 100;
                PdfPCell cell_detail3 = new PdfPCell(new Phrase("", FontInfoBold));
                cell_detail3.BorderColor = Color.WHITE;
                cell_detail3.BorderColorBottom = Color.BLACK;
                cell_detail3.BorderWidthBottom = 3;
                cell_detail3.Colspan = 5;
                ObjPdfTbl_detail.AddCell(cell_detail3);
                ObjDocument.Add(ObjPdfTbl_detail);
                PdfPTable ObjPdfTbldetail = new PdfPTable(5);
                ObjPdfTbldetail.WidthPercentage = 100;
                PdfPCell celldetail1 = new PdfPCell(new Phrase("LAST NAME", FontInfoBold));
                celldetail1.BorderWidth = 1;
                celldetail1.HorizontalAlignment = Element.ALIGN_LEFT;
                celldetail1.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetail.AddCell(celldetail1);
                PdfPCell celldetail2 = new PdfPCell(new Phrase("FIRST NAME", FontInfoBold));
                celldetail2.BorderWidth = 1;
                celldetail2.HorizontalAlignment = Element.ALIGN_LEFT;
                celldetail2.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetail.AddCell(celldetail2);
                PdfPCell celldetail3 = new PdfPCell(new Phrase("", FontInfoBold));
                celldetail3.BorderWidth = 1;
                celldetail3.HorizontalAlignment = Element.ALIGN_LEFT;
                celldetail3.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetail.AddCell(celldetail3);
                PdfPCell celldetail4 = new PdfPCell(new Phrase("", FontInfoBold));
                celldetail4.BorderWidth = 1;
                celldetail4.HorizontalAlignment = Element.ALIGN_LEFT;
                celldetail4.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetail.AddCell(celldetail4);
                PdfPCell celldetail5 = new PdfPCell(new Phrase("MIDDLE NAME", FontInfoBold));
                celldetail5.BorderWidth = 1;
                celldetail5.HorizontalAlignment = 1;
                celldetail5.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetail.AddCell(celldetail5);
                PdfPCell celldetail6 = new PdfPCell(new Phrase("", FontInfoBold));
                celldetail6.BorderWidth = 1;
                celldetail6.HorizontalAlignment = 1;
                celldetail6.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetail.AddCell(celldetail6);
                ObjDocument.Add(ObjPdfTbldetail);
                var Space_tbl = "<br>";
                var parsedSpace = HTMLWorker.ParseToList(new StringReader(Space_tbl), styles);
                foreach (var htmlElement in parsedSpace)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);
                }

                PdfPTable ObjPdfTbl_detail2 = new PdfPTable(5);
                ObjPdfTbl_detail2.WidthPercentage = 100;
                PdfPCell cell_detail32 = new PdfPCell(new Phrase("", FontInfoBold));
                cell_detail32.BorderColor = Color.WHITE;
                cell_detail32.BorderColorBottom = Color.BLACK;
                cell_detail32.BorderWidthBottom = 3;
                cell_detail32.Colspan = 5;
                ObjPdfTbl_detail2.AddCell(cell_detail3);
                ObjDocument.Add(ObjPdfTbl_detail2);
                PdfPTable ObjPdfTbldetail2 = new PdfPTable(5);
                ObjPdfTbldetail2.WidthPercentage = 100;
                PdfPCell celldetail11 = new PdfPCell(new Phrase("ADDRESS", FontInfoBold));
                celldetail11.BorderWidth = 1;
                celldetail11.HorizontalAlignment = Element.ALIGN_LEFT;
                celldetail11.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetail2.AddCell(celldetail11);
                PdfPCell celldetail22 = new PdfPCell(new Phrase("CITY", FontInfoBold));
                celldetail22.BorderWidth = 1;
                celldetail22.HorizontalAlignment = 1;
                celldetail22.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetail2.AddCell(celldetail22);
                PdfPCell celldetail33 = new PdfPCell(new Phrase("", FontInfoBold));
                celldetail33.BorderWidth = 1;
                celldetail33.HorizontalAlignment = 1;
                celldetail33.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetail2.AddCell(celldetail33);
                PdfPCell celldetail44 = new PdfPCell(new Phrase("STATE", FontInfoBold));
                celldetail44.BorderWidth = 1;
                celldetail44.HorizontalAlignment = 1;
                celldetail44.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetail2.AddCell(celldetail44);
                PdfPCell celldetail55 = new PdfPCell(new Phrase("ZIP CODE", FontInfoBold));
                celldetail55.BorderWidth = 1;
                celldetail55.HorizontalAlignment = 1;
                celldetail55.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetail2.AddCell(celldetail55);
                PdfPCell celldetail66 = new PdfPCell(new Phrase("", FontInfoBold));
                celldetail66.BorderWidth = 1;
                celldetail66.HorizontalAlignment = 1;
                celldetail66.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetail2.AddCell(celldetail66);
                ObjDocument.Add(ObjPdfTbldetail2);
                var Space_tbl2 = "<br>";
                var parsedSpace2 = HTMLWorker.ParseToList(new StringReader(Space_tbl2), styles);
                foreach (var htmlElement in parsedSpace2)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);
                }
                PdfPTable ObjPdfTbl_detail22 = new PdfPTable(5);
                ObjPdfTbl_detail22.WidthPercentage = 100;
                PdfPCell cell_detail322 = new PdfPCell(new Phrase("", FontInfoBold));
                cell_detail322.BorderColor = Color.WHITE;
                cell_detail322.BorderColorBottom = Color.BLACK;
                cell_detail322.BorderWidthBottom = 3;
                cell_detail322.Colspan = 5;
                ObjPdfTbl_detail22.AddCell(cell_detail322);
                ObjDocument.Add(ObjPdfTbl_detail22);
                PdfPTable ObjPdfTbldetail_2 = new PdfPTable(5);
                ObjPdfTbldetail_2.WidthPercentage = 100;
                PdfPCell celldetail1_1 = new PdfPCell(new Phrase("DAYTIME PHONE #", FontInfoBold));
                celldetail1_1.BorderWidth = 1;
                celldetail1_1.HorizontalAlignment = Element.ALIGN_LEFT;
                celldetail1_1.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetail_2.AddCell(celldetail1_1);
                PdfPCell celldetail2_2 = new PdfPCell(new Phrase("", FontInfoBold));
                celldetail2_2.BorderWidth = 1;
                celldetail2_2.HorizontalAlignment = 1;
                celldetail2_2.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetail_2.AddCell(celldetail2_2);
                PdfPCell celldetail3_3 = new PdfPCell(new Phrase("", FontInfoBold));
                celldetail3_3.BorderWidth = 1;
                celldetail3_3.HorizontalAlignment = 1;
                celldetail3_3.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetail_2.AddCell(celldetail3_3);
                PdfPCell celldetail4_4 = new PdfPCell(new Phrase("EVENING PHONE #", FontInfoBold));
                celldetail4_4.BorderWidth = 1;
                celldetail4_4.HorizontalAlignment = 1;
                celldetail4_4.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetail_2.AddCell(celldetail4_4);
                PdfPCell celldetail5_5 = new PdfPCell(new Phrase("", FontInfoBold));
                celldetail5_5.BorderWidth = 1;
                celldetail5_5.HorizontalAlignment = 1;
                celldetail5_5.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetail_2.AddCell(celldetail5_5);
                PdfPCell celldetail6_6 = new PdfPCell(new Phrase("", FontInfoBold));
                celldetail6_6.BorderWidth = 1;
                celldetail6_6.HorizontalAlignment = 1;
                celldetail6_6.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetail_2.AddCell(celldetail6_6);
                ObjDocument.Add(ObjPdfTbldetail_2);
                var Space_tbl3 = "<br>";
                var parsedSpace3 = HTMLWorker.ParseToList(new StringReader(Space_tbl3), styles);
                foreach (var htmlElement in parsedSpace3)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);
                }

                PdfPTable ObjPdfTbl_detail3 = new PdfPTable(5);
                ObjPdfTbl_detail3.WidthPercentage = 100;
                PdfPCell cell_detail_3 = new PdfPCell(new Phrase("", FontInfoBold));
                cell_detail_3.BorderColor = Color.WHITE;
                cell_detail_3.BorderColorBottom = Color.BLACK;
                cell_detail_3.BorderWidthBottom = 3;
                cell_detail_3.Colspan = 5;
                ObjPdfTbl_detail3.AddCell(cell_detail_3);
                ObjDocument.Add(ObjPdfTbl_detail3);
                PdfPTable ObjPdfTbldetai_l = new PdfPTable(6);
                ObjPdfTbldetai_l.WidthPercentage = 100;
                PdfPCell celldetai_l = new PdfPCell(new Phrase("SOCIAL SECURITY NUMBER", FontInfoBold));
                celldetai_l.BorderWidth = 1;
                celldetai_l.HorizontalAlignment = Element.ALIGN_LEFT;
                celldetai_l.Colspan = 2;
                celldetai_l.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetai_l.AddCell(celldetai_l);
                PdfPCell celldetai_l2 = new PdfPCell(new Phrase("", FontInfoBold));
                celldetai_l2.BorderWidth = 1;
                celldetai_l2.HorizontalAlignment = 1;
                celldetai_l2.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetai_l.AddCell(celldetai_l2);
                PdfPCell celldetai_l3 = new PdfPCell(new Phrase("", FontInfoBold));
                celldetai_l3.BorderWidth = 1;
                celldetai_l3.HorizontalAlignment = 1;
                celldetai_l3.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetai_l.AddCell(celldetai_l3);
                PdfPCell celldetai_l4 = new PdfPCell(new Phrase("BIRTH DATE", FontInfoBold));
                celldetai_l4.BorderWidth = 1;
                celldetai_l4.HorizontalAlignment = 1;
                celldetai_l4.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetai_l.AddCell(celldetai_l4);
                PdfPCell celldetai_l5 = new PdfPCell(new Phrase("", FontInfoBold));
                celldetai_l5.BorderWidth = 1;
                celldetai_l5.HorizontalAlignment = 1;
                celldetai_l5.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetai_l.AddCell(celldetai_l5);
                PdfPCell celldetai_l6 = new PdfPCell(new Phrase("", FontInfoBold));
                celldetai_l6.BorderWidth = 1;
                celldetai_l6.HorizontalAlignment = 1;
                celldetai_l6.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetai_l.AddCell(celldetai_l6);
                ObjDocument.Add(ObjPdfTbldetai_l);
                var Space_tbl4 = "<br>";
                var parsedSpace4 = HTMLWorker.ParseToList(new StringReader(Space_tbl4), styles);
                foreach (var htmlElement in parsedSpace4)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);
                }

                PdfPTable ObjPdfTbl_detail4 = new PdfPTable(5);
                ObjPdfTbl_detail4.WidthPercentage = 100;
                PdfPCell cell_detail_4 = new PdfPCell(new Phrase("", FontInfoBold));
                cell_detail_4.BorderColor = Color.WHITE;
                cell_detail_4.BorderColorBottom = Color.BLACK;
                cell_detail_4.BorderWidthBottom = 3;
                cell_detail_4.Colspan = 5;
                ObjPdfTbl_detail4.AddCell(cell_detail_4);
                ObjDocument.Add(ObjPdfTbl_detail4);
                PdfPTable ObjPdfTbldetai_l1 = new PdfPTable(6);
                ObjPdfTbldetai_l1.WidthPercentage = 100;
                PdfPCell celldetai_l1 = new PdfPCell(new Phrase("DRIVER'S LICENSE NUMBER", FontInfoBold));
                celldetai_l1.BorderWidth = 1;
                celldetai_l1.HorizontalAlignment = Element.ALIGN_LEFT;
                celldetai_l1.Colspan = 2;
                celldetai_l1.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetai_l1.AddCell(celldetai_l1);
                PdfPCell celldetai_l_2 = new PdfPCell(new Phrase("STATE", FontInfoBold));
                celldetai_l_2.BorderWidth = 1;
                celldetai_l_2.HorizontalAlignment = Element.ALIGN_LEFT;
                celldetai_l_2.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetai_l1.AddCell(celldetai_l_2);
                PdfPCell celldetai_l_3 = new PdfPCell(new Phrase("", FontInfoBold));
                celldetai_l_3.BorderWidth = 1;
                celldetai_l_3.HorizontalAlignment = 1;
                celldetai_l_3.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetai_l1.AddCell(celldetai_l_3);
                PdfPCell celldetai_l_4 = new PdfPCell(new Phrase("EMAIL ADDRESS", FontInfoBold));
                celldetai_l_4.BorderWidth = 1;
                celldetai_l_4.HorizontalAlignment = 1;
                celldetai_l_4.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetai_l1.AddCell(celldetai_l_4);
                PdfPCell celldetai_l_5 = new PdfPCell(new Phrase("", FontInfoBold));
                celldetai_l_5.BorderWidth = 1;
                celldetai_l_5.HorizontalAlignment = 1;
                celldetai_l_5.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetai_l1.AddCell(celldetai_l_5);
                PdfPCell celldetai_l_6 = new PdfPCell(new Phrase("", FontInfoBold));
                celldetai_l_6.BorderWidth = 1;
                celldetai_l_6.HorizontalAlignment = 1;
                celldetai_l_6.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetai_l1.AddCell(celldetai_l_6);
                ObjDocument.Add(ObjPdfTbldetai_l1);
                var para = "<br><p align='justify' style='font-size:10'> My signature below authorizes intelifi.COM to begin the disclosure process in connection with a consumer report or investigative consumer report, which was prepared on me by intelifi.COM.</p>";
                var pars = HTMLWorker.ParseToList(new StringReader(para), styles);
                foreach (var htmlElement in pars)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);

                }
                var Space_tbl5 = "<br>";
                var parsedSpace5 = HTMLWorker.ParseToList(new StringReader(Space_tbl5), styles);
                foreach (var htmlElement in parsedSpace5)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);
                }

                PdfPTable ObjPdfTbl_detail5 = new PdfPTable(5);
                ObjPdfTbl_detail5.WidthPercentage = 100;
                PdfPCell cell_detail_5 = new PdfPCell(new Phrase("", FontInfoBold));
                cell_detail_5.BorderColor = Color.WHITE;
                cell_detail_5.BorderColorBottom = Color.BLACK;
                cell_detail_5.BorderWidthBottom = 3;
                cell_detail_5.Colspan = 5;
                ObjPdfTbl_detail5.AddCell(cell_detail_5);
                ObjDocument.Add(ObjPdfTbl_detail5);
                PdfPTable ObjPdfTbldetai_l51 = new PdfPTable(5);
                ObjPdfTbldetai_l51.WidthPercentage = 100;
                PdfPCell celldetai_l51 = new PdfPCell(new Phrase("SIGNATURE", FontInfoBold));
                celldetai_l51.BorderWidth = 1;
                celldetai_l51.HorizontalAlignment = Element.ALIGN_LEFT;
                celldetai_l51.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetai_l51.AddCell(celldetai_l51);
                PdfPCell celldetai_l_52 = new PdfPCell(new Phrase("", FontInfoBold));
                celldetai_l_52.BorderWidth = 1;
                celldetai_l_52.HorizontalAlignment = 1;
                celldetai_l_52.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetai_l51.AddCell(celldetai_l_52);
                PdfPCell celldetai_l_53 = new PdfPCell(new Phrase("", FontInfoBold));
                celldetai_l_53.BorderWidth = 1;
                celldetai_l_53.HorizontalAlignment = 1;
                celldetai_l_53.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetai_l51.AddCell(celldetai_l_53);
                PdfPCell celldetai_l_42 = new PdfPCell(new Phrase("DATE", FontInfoBold));
                celldetai_l_42.BorderWidth = 1;
                celldetai_l_42.HorizontalAlignment = 1;
                celldetai_l_42.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetai_l51.AddCell(celldetai_l_42);
                PdfPCell celldetai_l_5_3 = new PdfPCell(new Phrase("", FontInfoBold));
                celldetai_l_5_3.BorderWidth = 1;
                celldetai_l_5_3.HorizontalAlignment = 1;
                celldetai_l_5_3.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetai_l51.AddCell(celldetai_l_5_3);
                PdfPCell celldetai_l_64 = new PdfPCell(new Phrase("", FontInfoBold));
                celldetai_l_64.BorderWidth = 1;
                celldetai_l_64.HorizontalAlignment = 1;
                celldetai_l_64.BorderColor = iTextSharp.text.Color.WHITE;
                ObjPdfTbldetai_l51.AddCell(celldetai_l_64);
                ObjDocument.Add(ObjPdfTbldetai_l51);
                var Space_tbl6 = "<br><p align='justify' style='font-size:10'>Please identify all information that you believe is incorrect on your intelifi.COM report:</p>";
                var parsedSpace6 = HTMLWorker.ParseToList(new StringReader(Space_tbl6), styles);
                foreach (var htmlElement in parsedSpace6)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);
                }
                var Space_tbl7 = "<br>";
                var parsedSpace7 = HTMLWorker.ParseToList(new StringReader(Space_tbl7), styles);
                foreach (var htmlElement in parsedSpace7)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);
                }

                PdfPTable ObjPdfTbl_detail7 = new PdfPTable(5);
                ObjPdfTbl_detail7.WidthPercentage = 100;
                PdfPCell cell_detail_7 = new PdfPCell(new Phrase("", FontInfoBold));
                cell_detail_7.BorderColor = Color.WHITE;
                cell_detail_7.BorderColorBottom = Color.BLACK;
                cell_detail_7.BorderWidthBottom = 3;
                cell_detail_7.Colspan = 5;
                ObjPdfTbl_detail7.AddCell(cell_detail_7);
                ObjDocument.Add(ObjPdfTbl_detail7);
                var Space_tbl8 = "<br>";
                var parsedSpace8 = HTMLWorker.ParseToList(new StringReader(Space_tbl8), styles);
                foreach (var htmlElement in parsedSpace8)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);
                }

                PdfPTable ObjPdfTbl_detail8 = new PdfPTable(5);
                ObjPdfTbl_detail8.WidthPercentage = 100;
                PdfPCell cell_detail_8 = new PdfPCell(new Phrase("", FontInfoBold));
                cell_detail_8.BorderColor = Color.WHITE;
                cell_detail_8.BorderColorBottom = Color.BLACK;
                cell_detail_8.BorderWidthBottom = 3;
                cell_detail_8.Colspan = 5;
                ObjPdfTbl_detail8.AddCell(cell_detail_8);
                ObjDocument.Add(ObjPdfTbl_detail8);
                var Space_tbl9 = "<br><br><br><br><br>";
                var parsedSpace9 = HTMLWorker.ParseToList(new StringReader(Space_tbl9), styles);
                foreach (var htmlElement in parsedSpace9)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);
                }
                PdfPTable table_copy = new PdfPTable(24);
                table_copy.DefaultCell.Border = 0;
                table_copy.DefaultCell.BorderColor = Color.WHITE;
                table_copy.WidthPercentage = 100;
                PdfPCell cellcopy = new PdfPCell(new Phrase("Copyright © intelifi.com                               Dispute Notification Form                    Created on 12/02/2008 10:09 AM"));
                ((Chunk)(cellcopy.Phrase[0])).Font = new iTextSharp.text.Font(((Chunk)(cellcopy.Phrase[0])).Font.Family, 9f);
                cellcopy.BorderColor = iTextSharp.text.Color.WHITE;
                cellcopy.Colspan = 24;
                cellcopy.HorizontalAlignment = PdfPCell.ALIGN_BOTTOM;
                table_copy.AddCell(cellcopy);
                ObjDocument.Add(table_copy);
                var paragraphftc = "<p align='justify' style='font-size:9'><b><I>Para information en espanol, visite <Span style='COLOR:#0000ff;'><u>www.ftc.gov/credit</u></Span> o escribe a la FTC Consumer Response Center, Room 130-A 600 Pennsylvania Ave. N.W., Washington, D.C. 20580&nbsp;</I><u>A Summary of Your Rights Under the Fair Credit Reporting Act</u></b></p><p align='justify' style='font-size:9'>The federal Fair Credit Reporting Act (FCRA) promotes the accuracy, fairness, and privacy of information in the files of consumer reporting agencies. There are many types of consumer reporting agencies, including credit bureaus and specialty agencies (such as agencies that sell information about check writing histories, medical records, and rental history records). Here is a summary of your major rights under the FCRA.<b>For more information, including information about additional rights, go to <Span style='COLOR:#0000ff;'><u>www.ftc.gov/credit</u></Span> or write to: Consumer Response Center, Room 130-A, Federal Trade Commission, 600 Pennsylvania Ave. N.W., Washington, D.C. 20580.</b></p><br><p align='justify' style='font-size:9'>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>You must be told if information in your file has been used against you.</b>Anyone who uses a credit report or another type of consumer report to deny your application for credit, insurance, or employment - or to take another adverse action against you - must tell you, and must give you the name, address, and phone number of the agency that provided the information.</p><p align='justify' style='font-size:9'>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>You have the right to know what is in your file.</b> You may request and obtain all the information about you in the files of a consumer reporting agency (your ''file disclosure''). You will be required to provide proper identification, which may include your Social Security number. In many cases, the disclosure will be free. You are entitled to a free file disclosure if:<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;o&nbsp;&nbsp;&nbsp;a person has taken adverse action against you because of information in your credit&nbsp;&nbsp;&nbsp;&nbsp;report;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;o&nbsp;&nbsp;&nbsp;you are the victim of identity theft and place a fraud alert in your file;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;o&nbsp;&nbsp;&nbsp;your file contains inaccurate information as a result of fraud;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;o&nbsp;&nbsp;&nbsp;you are on public assistance;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;o&nbsp;&nbsp;&nbsp;you are unemployed but expect to apply for employment within 60 days.</p><br><p align='justify' style='font-size:9'>In addition, by September 2005 all consumers will be entitled to one free disclosure every 12 months upon request from each nationwide credit bureau and from nationwide specialty consumer reporting agencies. See <Span style='COLOR:#0000ff;'><u>www.ftc.gov/credit</u></Span> for additional information.</p><br><p align='justify' style='font-size:9'>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>You have the right to ask for a credit score.</b>Credit scores are numerical summaries of your credit-worthiness based on information from credit bureaus. You may request a credit score from consumer reporting agencies that create scores or distribute scores used in residential real property loans, but you will have to pay for it. In some mortgage transactions, you will receive credit score information for free from the mortgage lender.</p><p align='justify' style='font-size:9'>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>You have the right to dispute incomplete or inaccurate information.</b>If you identify information in your file that is incomplete or inaccurate, and report it to the consumer reporting agency, the agency must investigate unless your dispute is frivolous. See <Span style='COLOR:#0000ff;'><u>www.ftc.gov/credit</u></Span> for an explanation of dispute procedures.</p><p align='justify' style='font-size:9'>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Consumer reporting agencies must correct or delete inaccurate, incomplete, or unverifiable information.</b>Inaccurate, incomplete or unverifiable information must be removed or corrected, usually within 30 days. However, a consumer reporting agency may continue to report information it has verified as accurate.</p><p align='justify' style='font-size:9'>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Consumer reporting agencies may not report outdated negative information.</b> In most cases, a consumer reporting agency may not report negative information that is more than seven years old, or bankruptcies that are more than 10 years old.</p><p align='justify' style='font-size:9'>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>You must give your consent for reports to be provided to employers.</b>A consumer reporting agency may not give out information about you to your employer, or a potential employer, without your written consent given to the employer.Written consent generally is not required in the trucking industry. For more information, go to <Span style='COLOR:#0000ff;'><u>www.ftc.gov/credit.</u></Span>Copyright © intelifi.com&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dispute Notification Form&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Created on 12/02/2008 10:09 AM</p><p align='justify' style='font-size:9'>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>You may limit ''prescreened'' offers of credit and insurance you get based on information in your credit report.</b>Unsolicited ''prescreened'' offers for credit and insurance must include a toll-free phone number you can call if you choose to remove your name and address from the lists these offers are based on. You may opt-out with the nationwide credit bureaus at <Span style='COLOR:#ff0000;'>1-888-5-OPTOUT (1-888-567-8688).</Span></p><p align='justify' style='font-size:9'>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>You may seek damages from violators.</b>If a consumer reporting agency, or, in some cases, a user of consumer reports or a furnisher of information to a consumer reporting agency violates the FCRA, you may be able to sue in state or federal court.</p><p align='justify' style='font-size:9'>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Identity theft victims and active duty military personnel have additional rights.</b>For more information, visit <Span style='COLOR:#0000ff;'><u>www.ftc.gov/credit</u></Span>.</p><br><p align='justify' style='font-size:9'>States may enforce the FCRA and many states have their own consumer reporting laws. In some cases, you may have more rights under state law. For more information, contact your state or local consumer protection agency or your state Attorney General. Federal enforcers are:</p><br><br>";
                var parsedHtmlftc = HTMLWorker.ParseToList(new StringReader(paragraphftc), styles);
                foreach (var htmlElement in parsedHtmlftc)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);

                }
                PdfPTable ObjPdf_TblMainBottom = new PdfPTable(2);
                ObjPdf_TblMainBottom.DefaultCell.Border = 6;
                ObjPdf_TblMainBottom.DefaultCell.BorderColor = Color.BLACK;
                ObjPdf_TblMainBottom.WidthPercentage = 115;
                PdfPCell cell_new1 = new PdfPCell(new Phrase("TYPE OF BUSINESS:", FontInfoNormal));
                cell_new1.BorderWidth = 1;
                cell_new1.HorizontalAlignment = Element.ALIGN_LEFT;
                cell_new1.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdf_TblMainBottom.AddCell(cellnew1);
                PdfPCell cell_new2 = new PdfPCell(new Phrase("CONTACT:", FontInfoNormal));
                cell_new2.BorderWidth = 1;
                cell_new2.HorizontalAlignment = Element.ALIGN_LEFT;
                cell_new2.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdf_TblMainBottom.AddCell(cellnew2);
                string str_TL1 = string.Empty;
                str_TL1 = "Consumer reporting agencies, creditors and others not listed below";
                PdfPCell cell_new3 = new PdfPCell(new Phrase(str_TL1, FontInfoNormal));
                cell_new3.BorderWidth = 1;
                cell_new3.HorizontalAlignment = Element.ALIGN_LEFT;
                cell_new3.VerticalAlignment = Element.ALIGN_CENTER;
                cell_new3.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdf_TblMainBottom.AddCell(cell_new3);
                string str_TR1 = string.Empty;
                str_TR1 = "Federal Trade Commission: Consumer Response Center - FCRA Washington, DC 20580 1-877-382-4357";
                PdfPCell cell_new_4 = new PdfPCell(new Phrase(str_TR1, FontInfoNormal));
                cell_new_4.BorderWidth = 1;
                cell_new_4.HorizontalAlignment = Element.ALIGN_LEFT;
                cell_new_4.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdf_TblMainBottom.AddCell(cell_new_4);
                string str_TL2 = string.Empty;
                str_TL2 = "National banks, federal branches/agencies of foreign banks (word ''National''or initials ''N.A.'' appear in or after bank's name)";
                PdfPCell cell_new_2 = new PdfPCell(new Phrase(str_TL2, FontInfoNormal));
                cell_new_2.BorderWidth = 1;
                cell_new_2.HorizontalAlignment = Element.ALIGN_LEFT;
                cell_new_2.VerticalAlignment = Element.ALIGN_CENTER;
                cell_new_2.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdf_TblMainBottom.AddCell(cell_new_2);
                string str_TR3 = string.Empty;
                str_TR3 = "Office of the Comptroller of the CurrencyCompliance Management, Mail Stop 6-6Washington, DC 20219 800-613-6743";
                PdfPCell cell_new_3 = new PdfPCell(new Phrase(str_TR3, FontInfoNormal));
                cell_new_3.BorderWidth = 1;
                cell_new_3.HorizontalAlignment = Element.ALIGN_LEFT;
                cell_new_3.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdf_TblMainBottom.AddCell(cell_new_3);
                string str_TL4 = string.Empty;
                str_TL4 = "Federal Reserve System member banks (except national banks, and federal branches/agencies of foreign banks)";
                PdfPCell cell_new_14 = new PdfPCell(new Phrase(str_TL4, FontInfoNormal));
                cell_new_14.BorderWidth = 1;
                cell_new_14.HorizontalAlignment = Element.ALIGN_LEFT;
                cell_new_14.VerticalAlignment = Element.ALIGN_CENTER;
                cell_new_14.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdf_TblMainBottom.AddCell(cell_new_14);
                string str_TR5 = string.Empty;
                str_TR5 = "Federal Reserve BoardDivision of Consumer & Community AffairsWashington, DC 20551 202-452-3693";
                PdfPCell cell_new_5 = new PdfPCell(new Phrase(str_TR5, FontInfoNormal));
                cell_new_5.BorderWidth = 1;
                cell_new_5.HorizontalAlignment = Element.ALIGN_LEFT;
                cell_new_5.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdf_TblMainBottom.AddCell(cell_new_5);
                string str_TL6 = string.Empty;
                str_TL6 = "Savings associations and federally chartered savings banks (word ''Federal'' or initials ''F.S.B.'' appear in federal institution's name)";
                PdfPCell cell_new_16 = new PdfPCell(new Phrase(str_TL6, FontInfoNormal));
                cell_new_16.BorderWidth = 1;
                cell_new_16.HorizontalAlignment = Element.ALIGN_LEFT;
                cell_new_16.VerticalAlignment = Element.ALIGN_CENTER;
                cell_new_16.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdf_TblMainBottom.AddCell(cell_new_16);
                string str_TR7 = string.Empty;
                str_TR7 = "Office of Thrift SupervisionConsumer ComplaintsWashington, DC 20552 800-842-6929";
                PdfPCell cell_new_7 = new PdfPCell(new Phrase(str_TR7, FontInfoNormal));
                cell_new_7.BorderWidth = 1;
                cell_new_7.HorizontalAlignment = Element.ALIGN_LEFT;
                cell_new_7.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdf_TblMainBottom.AddCell(cell_new_7);
                string str_TL8 = string.Empty;
                str_TL8 = "Federal credit unions (words ''Federal Credit Union appear in institution's name)''";
                PdfPCell cell_new_18 = new PdfPCell(new Phrase(str_TL8, FontInfoNormal));
                cell_new_18.BorderWidth = 1;
                cell_new_18.HorizontalAlignment = Element.ALIGN_LEFT;
                cell_new_18.VerticalAlignment = Element.ALIGN_CENTER;
                cell_new_18.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdf_TblMainBottom.AddCell(cell_new_18);
                string str_TR9 = string.Empty;
                str_TR9 = "National Credit Union Administration1775 Duke StreetAlexandria, VA 22314 703-519-4600";
                PdfPCell cell_new_9 = new PdfPCell(new Phrase(str_TR9, FontInfoNormal));
                cell_new_9.BorderWidth = 1;
                cell_new_9.HorizontalAlignment = Element.ALIGN_LEFT;
                cell_new_9.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdf_TblMainBottom.AddCell(cell_new_9);
                string str_TL12 = string.Empty;
                str_TL12 = "State-chartered banks that are not members of the Federal Reserve System";
                PdfPCell cell_new_12 = new PdfPCell(new Phrase(str_TL12, FontInfoNormal));
                cell_new_12.BorderWidth = 1;
                cell_new_12.HorizontalAlignment = Element.ALIGN_LEFT;
                cell_new_12.VerticalAlignment = Element.ALIGN_CENTER;
                cell_new_12.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdf_TblMainBottom.AddCell(cell_new_12);
                string str_TR13 = string.Empty;
                str_TR13 = "Federal Deposit Insurance CorporationConsumer Response Center, 2345 Grand Avenue, Suite 100Kansas City, Missouri 64108-2638 1-877-275-3342";
                PdfPCell cell_new_13 = new PdfPCell(new Phrase(str_TR13, FontInfoNormal));
                cell_new_13.BorderWidth = 1;
                cell_new_13.HorizontalAlignment = Element.ALIGN_LEFT;
                cell_new_13.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdf_TblMainBottom.AddCell(cell_new_13);
                string str_TL14 = string.Empty;
                str_TL14 = "Air, surface, or rail common carriers regulated by former Civil Aeronautics Board or Interstate Commerce Commission";
                PdfPCell cell_new_114 = new PdfPCell(new Phrase(str_TL14, FontInfoNormal));
                cell_new_114.BorderWidth = 1;
                cell_new_114.HorizontalAlignment = Element.ALIGN_LEFT;
                cell_new_114.VerticalAlignment = Element.ALIGN_CENTER;
                cell_new_114.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdf_TblMainBottom.AddCell(cell_new_114);
                string str_TR15 = string.Empty;
                str_TR15 = "Department of Transportation, Office of Financial ManagementWashington, DC 20590 202-366-1306";
                PdfPCell cell_new_15 = new PdfPCell(new Phrase(str_TR15, FontInfoNormal));
                cell_new_15.BorderWidth = 1;
                cell_new_15.HorizontalAlignment = Element.ALIGN_LEFT;
                cell_new_15.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdf_TblMainBottom.AddCell(cell_new_15);
                string str_TL16 = string.Empty;
                str_TL16 = "Activities subject to the Packers and Stockyards Act, 1921";
                PdfPCell cell_new_116 = new PdfPCell(new Phrase(str_TL16, FontInfoNormal));
                cell_new_116.BorderWidth = 1;
                cell_new_116.HorizontalAlignment = Element.ALIGN_LEFT;
                cell_new_116.VerticalAlignment = Element.ALIGN_CENTER;
                cell_new_116.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdf_TblMainBottom.AddCell(cell_new_116);
                string str_TR17 = string.Empty;
                str_TR17 = "Department of Agriculture Office of Deputy Administrator - GIPSA Washington, DC 20250 202-720-7051";
                PdfPCell cell_new_17 = new PdfPCell(new Phrase(str_TR17, FontInfoNormal));
                cell_new_17.BorderWidth = 1;
                cell_new_17.HorizontalAlignment = Element.ALIGN_LEFT;
                cell_new_17.BorderColor = iTextSharp.text.Color.BLACK;
                ObjPdf_TblMainBottom.AddCell(cell_new_17);
                ObjDocument.Add(ObjPdf_TblMainBottom);
                var Space_tbl_3 = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>";
                var parsedSpace_3 = HTMLWorker.ParseToList(new StringReader(Space_tbl_3), styles);
                foreach (var htmlElement in parsedSpace_3)
                {
                    string typeofelement = htmlElement.GetType().ToString();
                    ObjDocument.Add(htmlElement as IElement);
                }
                PdfPTable table_copy3 = new PdfPTable(24);
                table_copy3.DefaultCell.Border = 0;
                table_copy3.DefaultCell.BorderColor = Color.WHITE;
                table_copy3.WidthPercentage = 100;

                PdfPCell celllastcopy = new PdfPCell(new Phrase("Copyright © intelifi.com                               Dispute Notification Form                    Created on 12/02/2008 10:09 AM"));
                celllastcopy.Colspan = 24;
                ((Chunk)(celllastcopy.Phrase[0])).Font = new iTextSharp.text.Font(((Chunk)(celllastcopy.Phrase[0])).Font.Family, 9f);
                celllastcopy.BorderColor = iTextSharp.text.Color.WHITE;
                celllastcopy.HorizontalAlignment = PdfPCell.ALIGN_BOTTOM;
                table_copy3.AddCell(celllastcopy);
                ObjDocument.Add(table_copy3);

                #endregion
            }
            catch (Exception ex)
            {
            }
            finally
            {
                ObjDocument.Close();
            }
            return pdfFileName;
        }


        public string GenerateApplicantName(string sLastname, string sSufix, string sMiddle, string sFirstName)
        {
            //string applicantName = string.Empty;
            //int getstr = 0, ilen = 0;
            //applicantName = (sLastname != "") ? sLastname + ", " : "";
            //applicantName += (sSufix != "") ? sSufix + ", " : "";
            //applicantName += (sMiddle != "") ? sMiddle + ", " : "";
            //applicantName += (sFirstName != "") ? sFirstName + ", " : "";

            //ilen = applicantName.LastIndexOf(",");
            //getstr = ilen + 2;
            //if (getstr == applicantName.Length)
            //{
            //    applicantName = applicantName.Substring(0, ilen);
            //}
            //return applicantName;
            //INT-132--Based on the client requirement (FirstName + Middle Name+ Last Name)
            string applicantName = string.Empty;
            int getstr = 0, ilen = 0;
            applicantName += (sSufix != "") ? sSufix + ", " : "";
            applicantName += (sFirstName != "") ? sFirstName + " " : "";
            applicantName += (sMiddle != "") ? sMiddle + " " : "";
            applicantName += (sLastname != "") ? sLastname + ", " : "";
            ilen = applicantName.LastIndexOf(",");
            getstr = ilen + 2;
            if (getstr == applicantName.Length)
            {
                applicantName = applicantName.Substring(0, ilen + 1);
            }
            return applicantName;
        }


        public string str21()
        {
            string str = string.Empty;

            str = " ";
            return str;
        }

        public string str22()
        {
            string str = "\nThe federal Fair Credit Reporting Act (FCRA) promotes the accuracy, fairness, and "
               + "privacy of information in the files of consumer reporting agencies. There are many "
               + "types of consumer reporting agencies, including credit bureaus and specialty agencies "
               + "(such as agencies that sell information about check writing histories, medical "
               + "records, and rental history records). Here is a summary of your major "
               + "rights under the FCRA. For more information, including information about additional "
               + "rights, go to www.consumerfinance.gov/learnmore or write to: Consumer Financial Protection Bureau, "
               + "1700 G Street N.W., Washington, D.C. 20006. "
               + "You may have additional rights under Maine's FCRA, Me. Rev. Stat. Ann. 10, Sec 1311 et"
               + "seq.\n"
               +

                 // Paragraph 2

                "\n•You must be told if information in your file has been used against you. Anyone who"
                        + "uses a credit report or another type of consumer report to deny your application for"
                        + "credit, insurance, or employment – or to take another adverse action against you –"
                        + "must tell you, and must give you the name, address, and phone number of the agency"
                        + "that provided the information."
                + "\n•	You have the right to know what is in your file. You may request and obtain all "
                        + "the information about you in the files of a consumer reporting agency (your “file"
                        + "disclosure”). You will be required to provide proper identification, which may "
                        + "include your Social Security number. In many cases, the disclosure will be free."
                        + "You are entitled to a free file disclosure "
                        + "if: \n"
                        + "\t\t\t\t\t\t\t\t\t\t•	A person has taken adverse action against you because of information in your"
                + "credit report;"
                + "\n\t\t\t\t\t\t\t\t\t\t•	You are the victim of identify theft and place a fraud alert in your file;"
                + "\n\t\t\t\t\t\t\t\t\t\t•	Your file contains inaccurate information as a result of fraud;"
                + "\n\t\t\t\t\t\t\t\t\t\t•	You are on public assistance;"
                + "\n\t\t\t\t\t\t\t\t\t\t•	You are unemployed but expect to apply for employment within 60 days."
                + "\n  \t\t\t\t\t\t\tIn addition, all consumers are  entitled to one free disclosure"
                + " every 12 months upon request from each nationwide credit bureau and \n"
                + " from nationwide specialty consumer reporting agencies. See www.consumerfinance.gov/learnmore for additional "
                + " information."
                + "\n•	You have the right to ask for a credit score. Credit scores are numerical"
                + " summaries of your credit-worthiness based on information from credit bureaus."
                + " You may request a credit score from consumer reporting agencies that create scores"
                + " or distribute scores used in residential real property loans, but you will have to"
                + " pay for it. In some mortgage transactions, you will receive credit score information "
                + " for free from the mortgage lender."
                // Paragraph 4
                + "\n•	You have the right to dispute incomplete or inaccurate information. If you identify"
                + " information in your file that is incomplete or inaccurate, and report it to the"
                + " consumer reporting agency, the agency must investigate unless your dispute is "
                + " frivolous. See www.consumerfinance.gov/learnmore for an explanation of dispute procedures."
                + "\n•	Consumer reporting agencies must correct or delete inaccurate, incomplete,"
                + " or unverifiable information. Inaccurate, incomplete or unverifiable information"
                + " must be removed or corrected, usually within 30 days. However, a consumer reporting"
                + " agency may continue to report information it has verified as accurate."
                + "\n•	Consumer reporting agencies may not report outdated negative information."
                + "In most cases, a consumer reporting agency may not report negative information"
                + "that is more than seven years old, or bankruptcies that are more than 10 years old."
                + "\n•	Access to your file is limited. A consumer reporting agency may provide"
                + " information about you only to people with a valid need -- usually to consider "
                + " an application with a creditor, insurer, employer, landlord, or other business."
                + " The FCRA specifies those with a valid need for access."
                + "\n•	You must give your consent for reports to be provided to employers. A "
                + " consumer reporting agency may not give out information about you to your"
                + " employer, or a potential employer, without your written consent given to"
                + " the employer. Written consent generally is not required in the trucking industry. "
                + " For more information, go to www.consumerfinance.gov/learnmore."
                + "\n•	You may limit “prescreened” offers of credit and insurance you get based on information in "
                + " your credit report. Unsolicited “prescreened” offers for credit and insurance must"
                + " include a toll-free phone number you can call if you choose to remove your name"
                + " and address from the lists these offers are based on. You may opt-out with the"
                + " nationwide credit bureaus at 1-888-567-8688."
                + "\n•	You may seek damages from violators. If a consumer reporting agency, or,"
                + " in some cases, a user of consumer reports or a furnisher of information to "
                + " a consumer reporting agency violates the FCRA, you may be able to sue in state"
                + " or federal court.\n•	Identity theft victims and active duty military personnel have"
                + " additional rights. For more information, visit www.consumerfinance.gov/learnmore."
               ;
            return str;
        }

        public string str23()
        {
            string str = string.Empty;
            str = "•	You may seek damages from violators. If a consumer reporting agency, or,"
                + " in some cases, a user of consumer reports or a furnisher of information to "
                + " a consumer reporting agency violates the FCRA, you may be able to sue in state"
                + " or federal court.\n•	Identity theft victims and active duty military personnel have"
                + " additional rights. For more information, visit www.consumerfinance.gov/learnmore.";

            return str;
        }

        public string str3()
        {
            string str = string.Empty;
            str = "States may enforce the FCRA, and many states have their own consumer reporting "
                + "laws. In some cases, you may have more rights under state law. For more information,"
                + "contact your state or local consumer protection agency or your state Attorney"
                + "General. For information about your federal rights, contact: \n";

            return str;
        }

        public string ButtomLeft1()
        {
            string str = string.Empty;
            str = "Consumer reporting agencies, creditors and others not listed below Federal Trade"
                + "Commission: Consumer Response Center - FCRA\n"
                + "Washington, DC 20580 1-877-382-4357\n\n"
                + "National banks, federal branches/agencies of foreign banks (word \"National\" or "
                + "initials \"N.A.\" appear in or after bank's name) Office of the Comptroller of"
               + "the Currency\n"
            + "Compliance Management, Mail Stop 6-6" + "Washington, DC 20219 800-613-6743\n\n Federal Reserve System member banks (except national banks,"
            + "and federal branches/agencies of foreign banks)\nFederal Reserve Consumer Help (FRCH)\n";


            return str;
        }

        public string ButtomLeft2()
        {
            string str = string.Empty;
            str = "P O Box 1200\n "
            + "Minneapolis, MN 55480\n "
            + "Telephone: 888-851-1920\n "
            + "Website Address: www.federalreserveconsumerhelp.gov\n "
            + "Email Address: ConsumerHelp@FederalReserve.gov\n\n "
            + "Savings associations and federally chartered savings banks (word "
            + "\"Federal\" or initials \"F.S.B.\" appear in federal institution's name) "
            + "Office of Thrift Supervision "
            + "Consumer Complaints\n "
            + "Washington, DC 20552 800-842-6929\n ";

            return str;
        }

        public string ButtomRight1()
        {
            string str = string.Empty;
            str = "Federal credit unions (words \"Federal Credit Union\" appear in Institution’s name "
                + ") \nNational Credit Union Administration "
                + "1775 Duke Street "
                + "Alexandria, VA 22314 703-519-4600\n\n "
                + "State-chartered banks that are not members of the Federal Reserve\n "
                + "System "
            + "Federal Deposit Insurance Corporation "
            + "Consumer Response Center, 2345 Grand Avenue, Suite 100\n "
            + "Kansas City, Missouri 64108-2638 1-877-275-3342\n\n "
            + "Air, surface, or rail common carriers regulated by former Civil ";


            return str;
        }

        public string ButtomRight2()
        {
            string str = string.Empty;
            str = "Aeronautics Board or Interstate Commerce Commission "
            + "Department of Transportation, Office of Financial Management\n "
            + "Washington, DC 20590 202-366-1306\n\n "
            + "Activities subject to the Packers and Stockyards Act, 1921 Department of Agriculture\n\n "
            + "Office of Deputy Administrator - GIPSA "
            + "Washington, DC 20250 202-720-7051\n ";

            return str;
        }
        #endregion

        public string GeneratePdfOf613Notice(string ApplicantName, string ApplicantNamepdf, string UserName, string CompanyName, string Address, string mailingAddress, int? OrderId, string OrderNo)
        {
            string pdfFileName = string.Empty;
            iTextSharp.text.Rectangle ObjPageSize = new iTextSharp.text.Rectangle(PageSize.LETTER);
            Document ObjDocument = new Document(ObjPageSize, 55, 55, 15, 12);
            float Leading = 13;
            DateTime DateToBePrint = DateTime.Now;
            try
            {
                pdfFileName = "Intelifi_Inc_613Notice_" + ApplicantNamepdf + "_" + OrderNo + ".pdf";
                DirectoryInfo dir = null;
                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\613NoticePdf")))
                {
                    Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\613NoticePdf"));
                }
                else
                {

                    dir = new DirectoryInfo(System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\613NoticePdf"));
                    FileSystemInfo[] info = dir.GetFiles();
                    if (info.Length > 0)
                    {
                        foreach (FileInfo fi in info)
                        {
                            if (fi.Name.Contains(""))
                            {
                                string FilePath = System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\613NoticePdf\") + fi.Name;
                                if (System.IO.File.Exists(FilePath))
                                {
                                    try
                                    {
                                        System.IO.File.Delete(FilePath);
                                    }
                                    catch// (Exception ex)
                                    {
                                        // ListErrors.Add("File Used By Another Process : " + ex.Message.ToString());
                                    }

                                }
                            }
                        }
                    }
                }
                var ObjData = new tblOrderSearchedData();
                string stateName = "";
                mailingAddress = "";
                string StreetAddress = string.Empty;
                string StreetName = string.Empty;

                using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                {
                    ObjData = Dx.tblOrderSearchedDatas.FirstOrDefault(n => n.fkOrderId == OrderId);
                    if (ObjData != null)
                    {
                        if (ObjData.search_state != null && ObjData.search_state != string.Empty)
                        {
                            stateName = ((ObjData.search_state != "-1") && (ObjData.search_state != null)) ? Dx.tblStates.FirstOrDefault(n => n.pkStateId == Convert.ToInt32(ObjData.search_state)).StateCode : stateName;
                        }
                        else
                        {
                            stateName = ((ObjData.SearchedStateId.ToString() != "-1") && (ObjData.SearchedStateId != null)) ? Dx.tblStates.FirstOrDefault(n => n.pkStateId == Convert.ToInt32(ObjData.SearchedStateId)).StateCode : stateName;
                        }
                        mailingAddress = ObjData.SearchedFirstName != string.Empty ? ObjData.SearchedFirstName + " " : mailingAddress;
                        mailingAddress = ObjData.SearchedLastName != string.Empty ? mailingAddress + ObjData.SearchedLastName + "\n" : mailingAddress + "\n";
                        if (ObjData.best_SearchedStreetAddress != null && ObjData.best_SearchedStreetAddress != string.Empty)
                        {
                            mailingAddress = ObjData.best_SearchedStreetAddress != string.Empty ? mailingAddress + ObjData.best_SearchedStreetAddress + " " : mailingAddress;
                            StreetAddress = ObjData.best_SearchedStreetAddress != string.Empty ? ObjData.best_SearchedStreetAddress : string.Empty;
                        }
                        else if (ObjData.SearchedStreetAddress != null && ObjData.SearchedStreetAddress != string.Empty)
                        {
                            mailingAddress = ObjData.SearchedStreetAddress != string.Empty ? mailingAddress + ObjData.SearchedStreetAddress + " " : mailingAddress;
                            StreetAddress = ObjData.SearchedStreetAddress != string.Empty ? ObjData.SearchedStreetAddress : string.Empty;
                        }
                        if (ObjData.best_SearchedStreetName != null && ObjData.best_SearchedStreetName != string.Empty)
                        {
                            StreetName = ObjData.best_SearchedStreetAddress != string.Empty ? ObjData.best_SearchedStreetAddress : string.Empty;
                        }
                        else if (ObjData.SearchedStreetName != null && ObjData.SearchedStreetName != string.Empty)
                        {
                            StreetName = ObjData.SearchedStreetName != string.Empty ? ObjData.SearchedStreetName : string.Empty;
                        }

                        if (StreetAddress != string.Empty && StreetName != string.Empty && StreetName != StreetAddress)
                        {
                            if (ObjData.best_SearchedStreetName != null && ObjData.best_SearchedStreetName != string.Empty)
                            {
                                mailingAddress = ObjData.best_SearchedStreetName != string.Empty ? mailingAddress + ObjData.best_SearchedStreetName + " " : mailingAddress;
                            }
                            else if (ObjData.SearchedStreetName != null && ObjData.SearchedStreetName != string.Empty)
                            {
                                mailingAddress = ObjData.SearchedStreetName != string.Empty ? mailingAddress + ObjData.SearchedStreetName + " " : mailingAddress;
                            }
                        }

                        mailingAddress = ObjData.SearchedStreetType != string.Empty ? mailingAddress + ObjData.SearchedStreetType + " " : mailingAddress;
                        mailingAddress = ObjData.SearchedApt != string.Empty ? mailingAddress + ObjData.SearchedApt + "\n" : mailingAddress + "\n";
                        //   mailingAddress = ObjData.best_SearchedCity != string.Empty ? mailingAddress + ObjData.best_SearchedCity + ", ": mailingAddress;
                        if (ObjData.best_SearchedCity != null && ObjData.best_SearchedCity != string.Empty)
                        {
                            mailingAddress = ObjData.best_SearchedCity != string.Empty ? mailingAddress + ObjData.best_SearchedCity + ", " : mailingAddress;
                        }
                        else if (ObjData.SearchedCity != null && ObjData.SearchedCity != string.Empty)
                        {
                            mailingAddress = ObjData.SearchedCity != string.Empty ? mailingAddress + ObjData.SearchedCity + ", " : mailingAddress;
                        }

                       // bool AddressStatus = mailingAddress != string.Empty ? true : false;
                        if (mailingAddress != string.Empty)
                        {
                            mailingAddress = stateName != string.Empty ? mailingAddress + stateName : mailingAddress;
                            if (ObjData.best_SearchedZipCode != null && ObjData.best_SearchedZipCode != string.Empty)
                            {
                                mailingAddress = ObjData.best_SearchedZipCode != string.Empty ? mailingAddress + " " + ObjData.best_SearchedZipCode : mailingAddress;
                            }
                            else if (ObjData.SearchedZipCode != null && ObjData.SearchedZipCode != string.Empty)
                            {
                                mailingAddress = ObjData.SearchedZipCode != string.Empty ? mailingAddress + " " + ObjData.SearchedZipCode : mailingAddress;
                            }
                        }
                    }
                }

                string path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Resources\\Upload\\613NoticePdf\\" + pdfFileName;
                PdfWriter.GetInstance(ObjDocument, new FileStream(path, FileMode.Create));


                //iTextSharp.text.Font FontInfoHeader = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.BOLD, iTextSharp.text.Color.BLACK);
                iTextSharp.text.Font FontInfoNormal = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 8, iTextSharp.text.Font.NORMAL, iTextSharp.text.Color.BLACK);
               // iTextSharp.text.Font FontInfoHeadings = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 11, iTextSharp.text.Font.BOLD, iTextSharp.text.Color.BLACK);
                //iTextSharp.text.Font FontInfoTotal = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.BOLD, iTextSharp.text.Color.BLACK);
                ObjDocument.Open();
                #region for blankspace
                Chunk spaceChunk = new Chunk("                                                                                                                                                     ");
                #endregion
                HeaderFooter ObjPdfFooter = new HeaderFooter(new Phrase(" ", FontInfoNormal), new Phrase(spaceChunk + DateToBePrint.ToString("MMMM dd, yyyy hh:mm tt"), FontInfoNormal));
                //  ObjDocument.Footer = ObjPdfFooter;
                ObjPdfFooter.Alignment = Element.ALIGN_LEFT;

                string footer = "";

                #region Page 1 Table Declare
                iTextSharp.text.Table MainPageTbl = new iTextSharp.text.Table(2);
                MainPageTbl.Width = 100;
                MainPageTbl.Padding = 0;
                MainPageTbl.Cellpadding = 2;
                //MainPageTbl.Cellspacing = 1;
                MainPageTbl.DefaultCell.BackgroundColor = Color.WHITE;
                // MainPageTbl.CellsFitPage = true;
                MainPageTbl.TableFitsPage = true;
                MainPageTbl.BorderColor = Color.WHITE;
                MainPageTbl.Border = 0;
                #endregion

                #region Page 1 Address Blocks

                string usaIntelAddress = "\n Intelifi, Inc. \n 8730 Wilshire Blvd Suite 412 \n Beverly Hills, CA 90211 \n\n";
                Phrase fPhrase = new Phrase(Leading, usaIntelAddress, FontInfoNormal);
                Cell fCell = new Cell();
                fCell.BackgroundColor = new Color(255, 255, 255);
                //fCell.BorderColor = Color.BLACK;
                //fCell.BorderWidth = 1;
                //fCell.BorderWidthBottom = 1;
                //fCell.BorderWidthLeft = 1;
                //fCell.BorderWidthRight = 1;
                //fCell.BorderWidthTop = 1;
                fCell.Border = Rectangle.NO_BORDER;
                fCell.Width = 50;
                fCell.HorizontalAlignment = Element.ALIGN_LEFT;
                fCell.Add(fPhrase);
                MainPageTbl.AddCell(fCell);

                fCell = new Cell();
                fCell.BorderColor = Color.WHITE;
                fCell.Border = Rectangle.NO_BORDER;
                fCell.Width = 50;
                MainPageTbl.AddCell(fCell);


                fPhrase = new Phrase("\n\n");
                fCell = new Cell();
                fCell.Colspan = 2;
                fCell.HorizontalAlignment = Element.ALIGN_CENTER;
                fCell.BackgroundColor = new Color(255, 255, 255);
                fCell.BorderColor = Color.WHITE;
                // fCell.BorderWidthTop = 1;
                // fCell.BorderColorTop = Color.BLACK;
                fCell.Width = 100;
                fCell.Add(fPhrase);
                MainPageTbl.AddCell(fCell);
                //int-132

                //fPhrase = mailingAddress != string.Empty ? new Phrase(Leading, "\n" + mailingAddress + "\n\n", FontInfoNormal) : new Phrase(Leading, "\n\n\n", FontInfoNormal);
                //fCell = new Cell();
                //fCell.BackgroundColor = new Color(255, 255, 255);
                ////fCell.BorderColor = Color.BLACK;
                ////fCell.BorderWidth = 1;
                ////fCell.BorderWidthBottom = 1;
                ////fCell.BorderWidthLeft = 1;
                ////fCell.BorderWidthRight = 1;
                ////fCell.BorderWidthTop = 1;
                //fCell.Border = Rectangle.NO_BORDER;
                //fCell.Width = 50;
                //fCell.HorizontalAlignment = Element.ALIGN_LEFT;
                //fCell.Add(fPhrase);
                //MainPageTbl.AddCell(fCell);

                //fCell = new Cell();
                //fCell.BorderColor = Color.WHITE;
                //fCell.Border = Rectangle.NO_BORDER;
                //fCell.Width = 50;
                //MainPageTbl.AddCell(fCell);

                //fPhrase = new Phrase("\n \n");

                fCell = new Cell();
                fCell.Colspan = 2;
                fCell.HorizontalAlignment = Element.ALIGN_CENTER;
                fCell.BackgroundColor = new Color(255, 255, 255);
                fCell.Width = 100;
                fCell.BorderColor = Color.WHITE;
                fCell.BorderColorBottom = Color.BLACK;
                fCell.BorderWidthBottom = 1;
                fCell.Add(fPhrase);
                MainPageTbl.AddCell(fCell);
                #endregion

                #region page 1 Lower Table Declare
                iTextSharp.text.Table TblMain = new iTextSharp.text.Table(1);
                TblMain.Width = 100;
                TblMain.Padding = 0;
                TblMain.Cellpadding = 2;
                TblMain.Cellspacing = 1;
                TblMain.DefaultCell.Border = Rectangle.NO_BORDER;
                TblMain.Border = 2;
                TblMain.BorderColor = Color.WHITE;
                TblMain.CellsFitPage = true;
                TblMain.TableFitsPage = true;
                #endregion

                #region page 3 Table Declare
                iTextSharp.text.Table TblMain1 = new iTextSharp.text.Table(1);
                TblMain1.Width = 100;
                TblMain1.Padding = 0;
                TblMain1.Cellpadding = 2;
                TblMain1.Cellspacing = 1;
                TblMain1.DefaultCell.Border = Rectangle.NO_BORDER;
                TblMain1.CellsFitPage = true;
                TblMain1.TableFitsPage = true;
                #endregion

                #region page 1 Lower Table Data
                Phrase op = new Phrase();
                Cell ocell = new Cell(op);
               // Chunk ock = new Chunk();

                op = new Phrase(Leading, DateTime.Now.ToString("MMM-dd-yyyy"), FontInfoNormal);
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain.AddCell(ocell);


                ocell = new Cell();
                op = new Phrase(Leading, "Dear " + ApplicantName, FontInfoNormal);
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain.AddCell(ocell);

                string paragraph = string.Empty;
                #region Main Paragraph
                paragraph = "";
                paragraph = "Pursuant to FCRA 613 this is to advise you that " + CompanyName + " at " + Address
                   + " has requested that we furnish information from public records regarding you. Enclosed is "
                   + " a copy of the Summary of Rights prepared by the Consumer Financial Protection Bureau , and  "
                   + " a  document to request a copy of your report. If you have any questions or concerns  regarding "
                   + "the background report please contact Intelifi, Inc. 8730 Wilshire Blvd. Suite 412 Beverly Hills, "
                   + " CA 90211 (800) 409-1819.\n\n";

                #endregion


                iTextSharp.text.Font FontInfoNormal1 = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.NORMAL, iTextSharp.text.Color.BLACK);


                ocell = new Cell();
                op = new Phrase(Leading, paragraph, FontInfoNormal1);
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain.AddCell(ocell);

                ocell = new Cell();
                op = new Phrase(Leading, "Sincerely,", FontInfoNormal);
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain.AddCell(ocell);

                ocell = new Cell();
                op = new Phrase(Leading, "Intelifi, Inc.", FontInfoNormal);
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain.AddCell(ocell);

                ocell = new Cell();
                op = new Phrase(Leading, "Enclosures: Consumer Summary of Rights\nDocuments Request Form", FontInfoNormal);
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain.AddCell(ocell);

                ocell = new Cell();
                op = new Phrase(Leading, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", FontInfoNormal);
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain.AddCell(ocell);
                #endregion

                #region Page1 Footer
                iTextSharp.text.Table footerTbl1 = new iTextSharp.text.Table(1);
                footerTbl1.Width = 100;
                footerTbl1.Padding = 0;
                footerTbl1.Cellpadding = 2;
                footerTbl1.Cellspacing = 1;
                footerTbl1.DefaultCell.Border = Rectangle.NO_BORDER;
                footerTbl1.CellsFitPage = true;
                footerTbl1.TableFitsPage = true;
                footerTbl1.BorderColor = Color.WHITE;
                footerTbl1.Border = 0;

                Cell ocell3 = new Cell();
                Phrase op3 = new Phrase(Leading, "\n\n\n\n", FontInfoNormal);
                ocell3.HorizontalAlignment = Element.ALIGN_CENTER;
                ocell3.BackgroundColor = new Color(255, 255, 255);
                ocell3.BorderColor = Color.WHITE;
                ocell3.Border = 0;
                ocell3.Add(op3);
                footerTbl1.AddCell(ocell3);

                ocell3 = new Cell();
                footer = "1                                                                                                                              " + DateToBePrint.ToString("MMMM dd, yyyy hh:mm tt");
                op3 = new Phrase(Leading, footer, FontInfoNormal);
                ocell3.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell3.BackgroundColor = new Color(255, 255, 255);
                ocell3.BorderColor = Color.BLACK;
                ocell3.BorderWidthBottom = 1;
                ocell3.BorderWidthLeft = 0;
                ocell3.BorderWidthRight = 0;
                ocell3.BorderWidthTop = 1;
                ocell3.Add(op3);
                footerTbl1.AddCell(ocell3);
                #endregion

                #region page 3 Table Data
                //iTextSharp.text.Font FontInfoItalic = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.ITALIC, iTextSharp.text.Color.BLACK);
                iTextSharp.text.Font FontInfoBoldUnderLine = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.UNDERLINE, iTextSharp.text.Color.BLACK);
                iTextSharp.text.Font FontInfoBold = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.BOLD, iTextSharp.text.Color.BLACK);

                string paragraph2 = string.Empty;

                paragraph2 = "";
                paragraph2 = "Para information en espanol, visite www.consumerfinance.gov/learnmore  o escribe a la Consumer "
                    + " Financial Protection Bureau , 1700 G Street N.W., Washington, DC 2006.";



                ocell = new Cell();
                op = new Phrase(Leading, paragraph2, FontInfoNormal);
                ocell.HorizontalAlignment = Element.ALIGN_CENTER;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain1.AddCell(ocell);


                ocell = new Cell();
                op = new Phrase(Leading, "A Summary of Your Rights Under the Fair Credit Reporting Act", FontInfoBoldUnderLine);
                ocell.HorizontalAlignment = Element.ALIGN_CENTER;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain1.AddCell(ocell);


                //ocell = new Cell();
                //op = new Phrase(Leading, str21(), FontInfoNormal);
                //op.Leading = 13;
                //ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                //ocell.BackgroundColor = new Color(255, 255, 255);
                //ocell.Add(op);
                //TblMain1.AddCell(ocell);

                ocell = new Cell();
                op = new Phrase(Leading, str22(), FontInfoNormal);
                op.Leading = 13;
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain1.AddCell(ocell);

                //ocell = new Cell();
                //op = new Phrase(Leading, str23(), FontInfoNormal);
                //op.Leading = 13;
                //ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                //ocell.BackgroundColor = new Color(255, 255, 255);
                //ocell.Add(op);
                //TblMain1.AddCell(ocell);

                ocell = new Cell();
                op = new Phrase(Leading, str3(), FontInfoNormal);
                op.Leading = 15;
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain1.AddCell(ocell);

                //ocell = new Cell();
                //op = new Phrase(Leading, "\n\n", FontInfoNormal);
                //ocell.HorizontalAlignment = Element.ALIGN_CENTER;
                //ocell.BackgroundColor = new Color(255, 255, 255);
                //ocell.Add(op);
                //TblMain1.AddCell(ocell);
                #endregion

                #region Page3 Footer
                iTextSharp.text.Table footerTbl3 = new iTextSharp.text.Table(1);
                footerTbl3.Width = 100;
                footerTbl3.Padding = 0;
                footerTbl3.Cellpadding = 2;
                footerTbl3.Cellspacing = 1;
                footerTbl3.DefaultCell.Border = Rectangle.NO_BORDER;
                footerTbl3.CellsFitPage = true;
                footerTbl3.TableFitsPage = true;
                footerTbl3.BorderColor = Color.WHITE;
                footerTbl3.Border = 0;

                Cell ocell2 = new Cell();
                Phrase op2 = new Phrase(Leading, "\n", FontInfoNormal);
                ocell2.HorizontalAlignment = Element.ALIGN_CENTER;
                ocell2.BackgroundColor = new Color(255, 255, 255);
                ocell2.BorderColor = Color.WHITE;
                ocell2.Border = 0;
                ocell2.BorderWidthTop = 1;
                ocell2.BorderColorTop = Color.BLACK;
                ocell2.Add(op2);
                footerTbl3.AddCell(ocell2);

                ocell2 = new Cell();
                footer = "2                                                                                                                              " + DateToBePrint.ToString("MMMM dd, yyyy hh:mm tt");
                op2 = new Phrase(Leading, footer, FontInfoNormal);
                ocell2.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell2.BackgroundColor = new Color(255, 255, 255);
                ocell2.BorderColor = Color.BLACK;
                ocell2.BorderWidthBottom = 1;
                ocell2.BorderWidthLeft = 0;
                ocell2.BorderWidthRight = 0;
                ocell2.BorderWidthTop = 1;
                ocell2.Add(op2);
                footerTbl3.AddCell(ocell2);
                #endregion

                #region Page 4
                iTextSharp.text.Table ObjPdfTblMainBottom = new iTextSharp.text.Table(2);
                ObjPdfTblMainBottom.Width = 100;
                ObjPdfTblMainBottom.Padding = 0;
                ObjPdfTblMainBottom.Cellpadding = 2;
                ObjPdfTblMainBottom.Cellspacing = 1;
                //ObjPdfTblMainBottom.DefaultCell.Border = Rectangle.NO_BORDER;
                ObjPdfTblMainBottom.CellsFitPage = true;
                ObjPdfTblMainBottom.TableFitsPage = true;
                ObjPdfTblMainBottom.SetWidths(new int[2] { 60, 40 });
                ObjPdfTblMainBottom.BorderColor = Color.BLACK;
                ObjPdfTblMainBottom.Border = 1;

                Phrase TPhrase = new Phrase();
                Cell TCell = new Cell(TPhrase);

                TCell = new Cell();
                TPhrase = new Phrase(Leading, "TYPE OF BUSINESS:", FontInfoBold);
                TPhrase.Leading = 15;
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                TCell = new Cell();
                TPhrase = new Phrase(Leading, "CONTACT:", FontInfoBold);
                TPhrase.Leading = 15;
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTL1 = string.Empty;
                strTL1 = "1.a. Banks, saving associations, and credit unions with total assets of over $10 billion and their"
                + " affiliates."
                + "\nb. Such affiliates that are not banks,saving associations, or credit unions also should list, in"
                + " addition to the Bureau:";
                TCell = new Cell();
                TPhrase = new Phrase(13, strTL1, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTR1 = string.Empty;
                strTR1 = "a. Bureau of Consumer Financial Protection"
                + "\n1700 G Street NW"
                + "\nWashington, DC 20006"
                + "\nb. Federal Trade Commission: ConsumerResponse Center- FCRA"
                + "\nWashington, DC 20580"
                + "\n(877)382-4357";

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, strTR1, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTL2 = string.Empty;
                strTL2 = "2. To the extent not included in item 1 above:"
                + "\na. National Banks, federal saving associations, and federal branches ad federal agencies of"
                + "foreign banks"
                + "\nb. State member banks, branches and agencies of foreign banks( other than federal branches,"
                + " federal agencies and insured state branches of foreign banks), commercial leading companies"
                + " owned or controlled by foreign banks, and organizations operating under section 25 or 25A of"
                + " the Federal Reserve Act"
                + "\nc. Nonmember Insured Banks, Insured State Branches of Foreign banks and insured state"
                + " saving organizations"
                + "\nd. Federal Credit Unions";
                TCell = new Cell();
                TPhrase = new Phrase(13, strTL2, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTR2 = string.Empty;
                strTR2 = "a. Office of Comptroller of the Currency"
                + "\nCustomer Assistance Group"
                + "\n1301 McKinnney Street, Suite 3450"
                + "\nHouston, TX 77010-9050"
                + "\nb. Federal Reserve Consumer Help Center"
                + "\nPO Box 1200"
                + "\nMinneapolis, MN55480"
                + "\nc. FDIC Consumer Response Center"
                + "\n1100 Walnut Street, Box #11"
                + "\nKansas City, MO 64106"
                + "\nd. National Credit Union Administrator"
                + "\nOffice of Consumer Protection(OCP)"
                + "\nDivision of Consumer Compliance and Outreach(DCCO)"
                + "\n1775 Duke Street, Alexandria, VA 22314";

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, strTR2, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                TCell = new Cell();
                TPhrase = new Phrase(13, "3. Air carriers", FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTR3 = string.Empty;
                strTR3 = "Asst.General Counsel for Aviation Enforcement & Proceeding"
                + "\n Department ofTransportation"
                + "\n400 Seventh Street SW"
                + "\nWashington, DC 20590";

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, strTR3, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);


                TCell = new Cell();
                TPhrase = new Phrase(13, "4. Creditors Subject to Surface Transportation Board", FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTR4 = string.Empty;
                strTR4 = "Office of Proceedings, Surface Transportation Board"
                + "\nDepartment of Transportation"
                + "\n1925 K Street NW"
                + "\nWashington, DC 20423";

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, strTR4, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);


                TCell = new Cell();
                TPhrase = new Phrase(13, "5. Creditors Subject to Packers and Stockyards Act", FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTR5 = string.Empty;
                strTR5 = "Nearest packers and Stockyards Administration area supervisor";

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, strTR5, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                TCell = new Cell();
                TPhrase = new Phrase(13, "6. Small Business Investment Companies", FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTR6 = string.Empty;
                strTR6 = "Associate Deputy Administrator for CapitalAccess"
                + " United States Small Business Administration"
                + " 406 Third Street, SW, 8TH Floor"
                + " Washington, DC 20416";

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, strTR6, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                TCell = new Cell();
                TPhrase = new Phrase(13, "7. Brokers and Dealers", FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTR7 = string.Empty;
                strTR7 = "Securities and Exchange Commission"
                + "\n100 F St NE"
                + "\nWashington, DC 20549";

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, strTR7, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTL8 = string.Empty;
                strTL8 = "8. Federal Land Banks, Federal Land Bank Associations, Federal Intermediate Credit Banks,"
                + " and production Credit Associations";
                TCell = new Cell();
                TPhrase = new Phrase(13, strTL8, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTR8 = string.Empty;
                strTR8 = "Farm Credit Administration"
                + "\n1501 Farm Credit Drive"
                + "\nMcLean, VA 22102-5090";

                TCell = new Cell();
                TPhrase = new Phrase(13, strTR8, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, "9. Retailers, Finance Companies, and All Other Creditors Not Listed Above", FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);


                string strTR9 = string.Empty;
                strTR9 = "FTC Regional Office for region in which the creditor operates or"
                + "\nFederal Trade Commission: Consumer Response Center- FCRA"
                + "\nWashington, DC 20580"
                + "\n(877)382-4357";

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, strTR9, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                #endregion

                #region Page Num 4 Footer
                iTextSharp.text.Table footerTbl4 = new iTextSharp.text.Table(1);
                footerTbl4.Width = 100;
                footerTbl4.Padding = 0;
                footerTbl4.Cellpadding = 2;
                footerTbl4.Cellspacing = 1;
                footerTbl4.DefaultCell.Border = Rectangle.NO_BORDER;
                footerTbl4.CellsFitPage = true;
                footerTbl4.TableFitsPage = true;
                //footerTbl.BorderColor = Color.WHITE;
                footerTbl4.Border = 0;

                Cell ocell4 = new Cell();
                Phrase op4 = new Phrase(Leading, "\n", FontInfoNormal);
                ocell4.HorizontalAlignment = Element.ALIGN_CENTER;
                ocell4.BackgroundColor = new Color(255, 255, 255);
                ocell4.BorderColor = Color.WHITE;
                ocell4.Border = 0;
                ocell4.BorderWidthTop = 1;
                ocell4.BorderColorTop = Color.BLACK;
                ocell4.Add(op4);
                footerTbl4.AddCell(ocell4);

                ocell4 = new Cell();
                footer = "3                                                                                                                              " + DateToBePrint.ToString("MMMM dd, yyyy hh:mm tt");
                op4 = new Phrase(Leading, footer, FontInfoNormal);
                ocell4.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell4.BackgroundColor = new Color(255, 255, 255);
                ocell4.BorderColor = Color.BLACK;
                ocell4.BorderWidthBottom = 1;
                ocell4.BorderWidthLeft = 0;
                ocell4.BorderWidthRight = 0;
                ocell4.BorderWidthTop = 1;
                ocell4.Add(op4);
                footerTbl4.AddCell(ocell4);
                #endregion

                // New  Documents 

                #region Page 5 in Pdf

                iTextSharp.text.Table ObjPdfTblNewAddDoc = new iTextSharp.text.Table(1);

                ObjPdfTblNewAddDoc.Width = 100;
                ObjPdfTblNewAddDoc.Padding = 0;
                ObjPdfTblNewAddDoc.Cellpadding = 2;
                ObjPdfTblNewAddDoc.Cellspacing = 1;
                ObjPdfTblNewAddDoc.DefaultCell.Border = Rectangle.NO_BORDER;
                ObjPdfTblNewAddDoc.CellsFitPage = true;
                ObjPdfTblNewAddDoc.TableFitsPage = true;


                Phrase nPhrase = new Phrase();
                string paragraph3 = string.Empty;
                Cell ncell = new Cell(nPhrase);

                iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(new Uri(ApplicationPath.GetApplicationPath() + @"Content\themes\base\images\UsaintelPowerToKnow.png"));
                nPhrase = new Phrase();
                ImgLogo.ScaleAbsoluteHeight(40);
                ImgLogo.ScaleAbsoluteWidth(100);
                Chunk chk = new Chunk(ImgLogo, 0, -2);
                nPhrase.Add(chk);
                ObjPdfTblNewAddDoc.AddCell(nPhrase);


                ncell = new Cell();
                nPhrase = new Phrase(Leading, "Document Request Form", FontInfoBold);
                ncell.HorizontalAlignment = Element.ALIGN_CENTER;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);

                paragraph3 = "";
                paragraph3 = "This form is to be used to request a copy of your Consumer Background Investigative Report. Pursuant"
                + " to FCRA§ 609 and the abridged notification in the included Summary of Your Rights Under the Fair Credit Reporting"
                + " Act, “Every consumer reporting agency shall upon request and subject to 610(a)(1) [§ 1681h], clearly and "
                + " accurately disclose to the consumer all information in the consumer's file at the time of the request.” \n \n";



                ncell = new Cell();
                nPhrase = new Phrase(Leading, paragraph3, FontInfoNormal);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);


                string paragraph4 = string.Empty;

                paragraph4 = "";
                paragraph4 = "To receive a copy of your Consumer Background Investigative Report by mail, simply send the below"
                + " identifying information along with proof of your identity.  Proof of your identity can include a photocopy of "
                + " one of the following documents: a State Driver License, a State Issued ID Card, a Military ID Card or a W2 form. \n";




                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, paragraph4, FontInfoNormal);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(op);
                ObjPdfTblNewAddDoc.AddCell(nPhrase);

                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, "\nRequired Information: \n", FontInfoBold);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);

                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, "Your Social Security Number: ____ ____ ____ - ____ ____ - ____ ____ ____ ____ \n", FontInfoNormal);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);

                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, "Full Legal Name:______________________________________________________________________________ ", FontInfoNormal);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);

                ocell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, "DOB:________________________________________________________________________________________ ", FontInfoNormal);
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ocell);

                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, "Street Address (residence):____________________________________________________________________ ", FontInfoNormal);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);

                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, "City, State, Zip: _____________________________________________________________________________ ", FontInfoNormal);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);

                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, "Phone Number: (_______)____________________________________________________________________ ", FontInfoNormal);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);

                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, "E-mail Address: _____________________________________________________________________________ ", FontInfoNormal);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);


                string PleaseLeadPhrase = "\n Please read and sign the following statement. Your signature acknowledges your agreement. "
                                         + "By submitting this form, I state that all the information contained is true to the best of my knowledge.";
                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, PleaseLeadPhrase, FontInfoBold);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);



                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, "\nSignature: _________________________________________________________Date: __________________ \n", FontInfoNormal);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);



                string AfterCompletePhrase = "After completing this form, please return it by mail or fax. The mailing address and fax number are provided "
                                            + "below. Remember to include your proof of identity.";


                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, AfterCompletePhrase, FontInfoBold);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);


                string AddressInfo = "Intelifi Inc                                                                     \n"
                                    + "Attn: Compliance Department                                        \n "
                                    + "8730 Wilshire Blvd, Suite 412                                          \n"
                                    + "Beverly Hills, CA  90211                                                  \n";



                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, AddressInfo, FontInfoNormal);
                ncell.HorizontalAlignment = Element.ALIGN_CENTER;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);


                string AddressInfo2 = "Fax No. (310) 623-1821                                            \n"
                                     + "Customer Service No (888) 409-1819                             \n";
                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, AddressInfo2, FontInfoNormal);
                ncell.HorizontalAlignment = Element.ALIGN_CENTER;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);



                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, "Thank you,", FontInfoNormal);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);

                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, "Intelifi Compliance Department", FontInfoNormal);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);


                #endregion

                #region Page Num 5 Footer
                iTextSharp.text.Table footerTbl5 = new iTextSharp.text.Table(1);
                footerTbl5.Width = 100;
                footerTbl5.Padding = 0;
                footerTbl5.Cellpadding = 2;
                footerTbl5.Cellspacing = 1;
                footerTbl5.DefaultCell.Border = Rectangle.NO_BORDER;
                footerTbl5.CellsFitPage = true;
                footerTbl5.TableFitsPage = true;
                //footerTbl.BorderColor = Color.WHITE;
                footerTbl5.Border = 0;

                Cell ocell1 = new Cell();
                Phrase op1 = new Phrase(Leading, "\n\n\n\n\n\n\n\n", FontInfoNormal);
                ocell1.HorizontalAlignment = Element.ALIGN_CENTER;
                ocell1.BackgroundColor = new Color(255, 255, 255);
                ocell1.BorderColor = Color.WHITE;
                ocell1.Border = 0;
                ocell1.BorderWidthTop = 1;
                ocell1.BorderColorTop = Color.BLACK;
                ocell1.Add(op1);
                footerTbl5.AddCell(ocell1);

                ocell1 = new Cell();
                footer = "4                                                                                                                              " + DateToBePrint.ToString("MMMM dd, yyyy hh:mm tt");
                op1 = new Phrase(Leading, footer, FontInfoNormal);
                ocell1.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell1.BackgroundColor = new Color(255, 255, 255);
                ocell1.BorderColor = Color.BLACK;
                ocell1.BorderWidthBottom = 1;
                ocell1.BorderWidthLeft = 0;
                ocell1.BorderWidthRight = 0;
                ocell1.BorderWidthTop = 1;
                ocell1.Add(op1);
                footerTbl5.AddCell(ocell1);
                #endregion

                #region Blank Page Insert In PDF
                iTextSharp.text.Table blankTbl = new iTextSharp.text.Table(1);
                blankTbl.Width = 100;
                blankTbl.Padding = 0;
                blankTbl.Cellpadding = 2;
                blankTbl.Cellspacing = 1;
                blankTbl.DefaultCell.Border = Rectangle.NO_BORDER;
                blankTbl.CellsFitPage = true;
                blankTbl.TableFitsPage = true;
                #endregion

                #region Add Table in Document
                ObjDocument.Add(MainPageTbl);
                ObjDocument.Add(TblMain);
                ObjDocument.Add(footerTbl1);
                ObjDocument.NewPage();
                ObjDocument.Add(blankTbl);
                ObjDocument.NewPage();
                ObjDocument.Add(TblMain1);
                ObjDocument.Add(blankTbl);
                ObjDocument.Add(footerTbl3);
                ObjDocument.NewPage();
                ObjDocument.Add(ObjPdfTblMainBottom);
                ObjDocument.Add(blankTbl);
                ObjDocument.Add(footerTbl4);
                ObjDocument.NewPage();
                ObjDocument.Add(ObjPdfTblNewAddDoc);
                ObjDocument.Add(footerTbl5);
                ObjDocument.NewPage();
                ObjDocument.Add(blankTbl);
                ObjDocument.NewPage();
                #endregion
            }
            catch //(Exception ex)
            {

                // string err = "Error Msg : " + ex.Message.ToString() + ", " + ex.InnerException.ToString() + ", " + ex.Source.ToString();

            }
            finally
            {
                ObjDocument.Close();
            }

            // return ObjMemoryStream;
            return pdfFileName;
        }

        //INT-14 new method to generate pdf

        public string GeneratePdfOf613Noticeforftpuploadservie(string ApplicantName, string ApplicantNamepdf
            , string UserName, string CompanyName, string Address, string mailingAddress, int? OrderId, string OrderNo)
        {
            string pdfFileName = string.Empty;
            iTextSharp.text.Rectangle ObjPageSize = new iTextSharp.text.Rectangle(PageSize.LETTER);
            Document ObjDocument = new Document(ObjPageSize, 55, 55, 15, 12);
            float Leading = 13;
            DateTime DateToBePrint = DateTime.Now;
            try
            {
                pdfFileName = "Intelifi_Inc_613Notice_" + ApplicantNamepdf + "_" + OrderNo + ".pdf";
                DirectoryInfo dir = null;
                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\613NoticePdf")))
                {
                    Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\613NoticePdf"));
                }
                else
                {

                    dir = new DirectoryInfo(System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\613NoticePdf"));
                    FileSystemInfo[] info = dir.GetFiles();
                    if (info.Length > 0)
                    {
                        foreach (FileInfo fi in info)
                        {
                            if (fi.Name.Contains(""))
                            {
                                string FilePath = System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\613NoticePdf\") + fi.Name;
                                if (System.IO.File.Exists(FilePath))
                                {
                                    try
                                    {
                                        System.IO.File.Delete(FilePath);
                                    }
                                    catch (Exception ex)
                                    {
                                        // ListErrors.Add("File Used By Another Process : " + ex.Message.ToString());
                                    }

                                }
                            }
                        }
                    }
                }
                var ObjData = new tblOrderSearchedData();
                string stateName = "";
                mailingAddress = "";
                string StreetAddress = string.Empty;
                string StreetName = string.Empty;

                using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                {
                    ObjData = Dx.tblOrderSearchedDatas.FirstOrDefault(n => n.fkOrderId == OrderId);
                    if (ObjData != null)
                    {
                        if (ObjData.search_state != null && ObjData.search_state != string.Empty)
                        {
                            stateName = ((ObjData.search_state != "-1") && (ObjData.search_state != null)) ? Dx.tblStates.FirstOrDefault(n => n.pkStateId == Convert.ToInt32(ObjData.search_state)).StateCode : stateName;
                        }
                        else
                        {
                            stateName = ((ObjData.SearchedStateId.ToString() != "-1") && (ObjData.SearchedStateId != null)) ? Dx.tblStates.FirstOrDefault(n => n.pkStateId == Convert.ToInt32(ObjData.SearchedStateId)).StateCode : stateName;
                        }
                        mailingAddress = ObjData.SearchedFirstName != string.Empty ? ObjData.SearchedFirstName + " " : mailingAddress;
                        mailingAddress = ObjData.SearchedLastName != string.Empty ? mailingAddress + ObjData.SearchedLastName + "\n" : mailingAddress + "\n";
                        if (ObjData.best_SearchedStreetAddress != null && ObjData.best_SearchedStreetAddress != string.Empty)
                        {
                            mailingAddress = ObjData.best_SearchedStreetAddress != string.Empty ? mailingAddress + ObjData.best_SearchedStreetAddress + " " : mailingAddress;
                            StreetAddress = ObjData.best_SearchedStreetAddress != string.Empty ? ObjData.best_SearchedStreetAddress : string.Empty;
                        }
                        else if (ObjData.SearchedStreetAddress != null && ObjData.SearchedStreetAddress != string.Empty)
                        {
                            mailingAddress = ObjData.SearchedStreetAddress != string.Empty ? mailingAddress + ObjData.SearchedStreetAddress + " " : mailingAddress;
                            StreetAddress = ObjData.SearchedStreetAddress != string.Empty ? ObjData.SearchedStreetAddress : string.Empty;
                        }
                        if (ObjData.best_SearchedStreetName != null && ObjData.best_SearchedStreetName != string.Empty)
                        {
                            StreetName = ObjData.best_SearchedStreetAddress != string.Empty ? ObjData.best_SearchedStreetAddress : string.Empty;
                        }
                        else if (ObjData.SearchedStreetName != null && ObjData.SearchedStreetName != string.Empty)
                        {
                            StreetName = ObjData.SearchedStreetName != string.Empty ? ObjData.SearchedStreetName : string.Empty;
                        }

                        if (StreetAddress != string.Empty && StreetName != string.Empty && StreetName != StreetAddress)
                        {
                            if (ObjData.best_SearchedStreetName != null && ObjData.best_SearchedStreetName != string.Empty)
                            {
                                mailingAddress = ObjData.best_SearchedStreetName != string.Empty ? mailingAddress + ObjData.best_SearchedStreetName + " " : mailingAddress;
                            }
                            else if (ObjData.SearchedStreetName != null && ObjData.SearchedStreetName != string.Empty)
                            {
                                mailingAddress = ObjData.SearchedStreetName != string.Empty ? mailingAddress + ObjData.SearchedStreetName + " " : mailingAddress;
                            }
                        }

                        mailingAddress = ObjData.SearchedStreetType != string.Empty ? mailingAddress + ObjData.SearchedStreetType + " " : mailingAddress;
                        mailingAddress = ObjData.SearchedApt != string.Empty ? mailingAddress + ObjData.SearchedApt + "\n" : mailingAddress + "\n";
                        //   mailingAddress = ObjData.best_SearchedCity != string.Empty ? mailingAddress + ObjData.best_SearchedCity + ", ": mailingAddress;
                        if (ObjData.best_SearchedCity != null && ObjData.best_SearchedCity != string.Empty)
                        {
                            mailingAddress = ObjData.best_SearchedCity != string.Empty ? mailingAddress + ObjData.best_SearchedCity + ", " : mailingAddress;
                        }
                        else if (ObjData.SearchedCity != null && ObjData.SearchedCity != string.Empty)
                        {
                            mailingAddress = ObjData.SearchedCity != string.Empty ? mailingAddress + ObjData.SearchedCity + ", " : mailingAddress;
                        }

                        //bool AddressStatus = mailingAddress != string.Empty ? true : false;
                        if (mailingAddress != string.Empty)
                        {
                            mailingAddress = stateName != string.Empty ? mailingAddress + stateName : mailingAddress;
                            if (ObjData.best_SearchedZipCode != null && ObjData.best_SearchedZipCode != string.Empty)
                            {
                                mailingAddress = ObjData.best_SearchedZipCode != string.Empty ? mailingAddress + " " + ObjData.best_SearchedZipCode : mailingAddress;
                            }
                            else if (ObjData.SearchedZipCode != null && ObjData.SearchedZipCode != string.Empty)
                            {
                                mailingAddress = ObjData.SearchedZipCode != string.Empty ? mailingAddress + " " + ObjData.SearchedZipCode : mailingAddress;
                            }
                        }
                    }
                }

                string path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Resources\\Upload\\613NoticePdf\\" + pdfFileName;
                PdfWriter.GetInstance(ObjDocument, new FileStream(path, FileMode.Create));


                //iTextSharp.text.Font FontInfoHeader = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.BOLD, iTextSharp.text.Color.BLACK);
                iTextSharp.text.Font FontInfoNormal = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 8, iTextSharp.text.Font.NORMAL, iTextSharp.text.Color.BLACK);
                //iTextSharp.text.Font FontInfoHeadings = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 11, iTextSharp.text.Font.BOLD, iTextSharp.text.Color.BLACK);
                //iTextSharp.text.Font FontInfoTotal = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.BOLD, iTextSharp.text.Color.BLACK);
                ObjDocument.Open();
                #region for blankspace
                Chunk spaceChunk = new Chunk("                                                                                                                                                     ");
                #endregion
                HeaderFooter ObjPdfFooter = new HeaderFooter(new Phrase(" ", FontInfoNormal), new Phrase(spaceChunk + DateToBePrint.ToString("MMMM dd, yyyy hh:mm tt"), FontInfoNormal));
                //  ObjDocument.Footer = ObjPdfFooter;
                ObjPdfFooter.Alignment = Element.ALIGN_LEFT;

                string footer = "";

                #region Page 1 Table Declare
                iTextSharp.text.Table MainPageTbl = new iTextSharp.text.Table(2);
                MainPageTbl.Width = 100;
                MainPageTbl.Padding = 0;
                MainPageTbl.Cellpadding = 2;
                //MainPageTbl.Cellspacing = 1;
                MainPageTbl.DefaultCell.BackgroundColor = Color.WHITE;
                // MainPageTbl.CellsFitPage = true;
                MainPageTbl.TableFitsPage = true;
                MainPageTbl.BorderColor = Color.WHITE;
                MainPageTbl.Border = 0;
                #endregion

                #region Page 1 Address Blocks

                string usaIntelAddress = "\n Intelifi, Inc. \n 8730 Wilshire Blvd Suite 412 \n Beverly Hills, CA 90211 \n\n";
                Phrase fPhrase = new Phrase(Leading, usaIntelAddress, FontInfoNormal);
                Cell fCell = new Cell();
                fCell.BackgroundColor = new Color(255, 255, 255);
                fCell.Border = Rectangle.NO_BORDER;
                fCell.Width = 50;
                fCell.HorizontalAlignment = Element.ALIGN_LEFT;
                fCell.Add(fPhrase);
                MainPageTbl.AddCell(fCell);

                fCell = new Cell();
                fCell.BorderColor = Color.WHITE;
                fCell.Border = Rectangle.NO_BORDER;
                fCell.Width = 50;
                MainPageTbl.AddCell(fCell);


                fPhrase = new Phrase("\n\n");
                fCell = new Cell();
                fCell.Colspan = 2;
                fCell.HorizontalAlignment = Element.ALIGN_CENTER;
                fCell.BackgroundColor = new Color(255, 255, 255);
                fCell.BorderColor = Color.WHITE;
                // fCell.BorderWidthTop = 1;
                // fCell.BorderColorTop = Color.BLACK;
                fCell.Width = 100;
                fCell.Add(fPhrase);
                MainPageTbl.AddCell(fCell);
                //int-132


                fPhrase = mailingAddress != string.Empty ? new Phrase(Leading, "\n" + mailingAddress + "\n\n", FontInfoNormal) : new Phrase(Leading, "\n\n\n", FontInfoNormal);
                fCell = new Cell();
                fCell.BackgroundColor = new Color(255, 255, 255);
                fCell.BorderColor = Color.BLACK;
                fCell.BorderWidth = 1;
                fCell.BorderWidthBottom = 1;
                fCell.BorderWidthLeft = 1;
                fCell.BorderWidthRight = 1;
                fCell.BorderWidthTop = 1;
                fCell.Border = Rectangle.NO_BORDER;
                fCell.Width = 50;
                fCell.HorizontalAlignment = Element.ALIGN_LEFT;
                fCell.Add(fPhrase);
                MainPageTbl.AddCell(fCell);

                fCell = new Cell();
                fCell.BorderColor = Color.WHITE;
                fCell.Border = Rectangle.NO_BORDER;
                fCell.Width = 50;
                MainPageTbl.AddCell(fCell);

                fPhrase = new Phrase("\n \n");





                fCell = new Cell();
                fCell.Colspan = 2;
                fCell.HorizontalAlignment = Element.ALIGN_CENTER;
                fCell.BackgroundColor = new Color(255, 255, 255);
                fCell.Width = 100;
                fCell.BorderColor = Color.WHITE;
                fCell.BorderColorBottom = Color.BLACK;
                fCell.BorderWidthBottom = 1;
                fCell.Add(fPhrase);
                MainPageTbl.AddCell(fCell);
                #endregion

                #region page 1 Lower Table Declare
                iTextSharp.text.Table TblMain = new iTextSharp.text.Table(1);
                TblMain.Width = 100;
                TblMain.Padding = 0;
                TblMain.Cellpadding = 2;
                TblMain.Cellspacing = 1;
                TblMain.DefaultCell.Border = Rectangle.NO_BORDER;
                TblMain.Border = 2;
                TblMain.BorderColor = Color.WHITE;
                TblMain.CellsFitPage = true;
                TblMain.TableFitsPage = true;
                #endregion

                #region page 3 Table Declare
                iTextSharp.text.Table TblMain1 = new iTextSharp.text.Table(1);
                TblMain1.Width = 100;
                TblMain1.Padding = 0;
                TblMain1.Cellpadding = 2;
                TblMain1.Cellspacing = 1;
                TblMain1.DefaultCell.Border = Rectangle.NO_BORDER;
                TblMain1.CellsFitPage = true;
                TblMain1.TableFitsPage = true;
                #endregion

                #region page 1 Lower Table Data
                Phrase op = new Phrase();
                Cell ocell = new Cell(op);
                //Chunk ock = new Chunk();

                op = new Phrase(Leading, DateTime.Now.ToString("MMM-dd-yyyy"), FontInfoNormal);
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain.AddCell(ocell);


                ocell = new Cell();
                op = new Phrase(Leading, "Dear " + ApplicantName, FontInfoNormal);
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain.AddCell(ocell);

                string paragraph = string.Empty;
                #region Main Paragraph
                paragraph = "";
                paragraph = "Pursuant to FCRA 613 this is to advise you that " + CompanyName + " at " + Address
                   + " has requested that we furnish information from public records regarding you. Enclosed is "
                   + " a copy of the Summary of Rights prepared by the Consumer Financial Protection Bureau , and  "
                   + " a  document to request a copy of your report. If you have any questions or concerns  regarding "
                   + "the background report please contact Intelifi, Inc. 8730 Wilshire Blvd. Suite 412 Beverly Hills, "
                   + " CA 90211 (800) 409-1819.\n\n";

                #endregion


                iTextSharp.text.Font FontInfoNormal1 = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.NORMAL, iTextSharp.text.Color.BLACK);


                ocell = new Cell();
                op = new Phrase(Leading, paragraph, FontInfoNormal1);
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain.AddCell(ocell);

                ocell = new Cell();
                op = new Phrase(Leading, "Sincerely,", FontInfoNormal);
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain.AddCell(ocell);

                ocell = new Cell();
                op = new Phrase(Leading, "Intelifi, Inc.", FontInfoNormal);
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain.AddCell(ocell);

                ocell = new Cell();
                op = new Phrase(Leading, "Enclosures: Consumer Summary of Rights\nDocuments Request Form", FontInfoNormal);
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain.AddCell(ocell);

                ocell = new Cell();
                op = new Phrase(Leading, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", FontInfoNormal);
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain.AddCell(ocell);
                #endregion

                #region Page1 Footer
                iTextSharp.text.Table footerTbl1 = new iTextSharp.text.Table(1);
                footerTbl1.Width = 100;
                footerTbl1.Padding = 0;
                footerTbl1.Cellpadding = 2;
                footerTbl1.Cellspacing = 1;
                footerTbl1.DefaultCell.Border = Rectangle.NO_BORDER;
                footerTbl1.CellsFitPage = true;
                footerTbl1.TableFitsPage = true;
                footerTbl1.BorderColor = Color.WHITE;
                footerTbl1.Border = 0;

                Cell ocell3 = new Cell();
                Phrase op3 = new Phrase(Leading, "\n\n\n\n", FontInfoNormal);
                ocell3.HorizontalAlignment = Element.ALIGN_CENTER;
                ocell3.BackgroundColor = new Color(255, 255, 255);
                ocell3.BorderColor = Color.WHITE;
                ocell3.Border = 0;
                ocell3.Add(op3);
                footerTbl1.AddCell(ocell3);

                ocell3 = new Cell();
                footer = "1                                                                                                                              " + DateToBePrint.ToString("MMMM dd, yyyy hh:mm tt");
                op3 = new Phrase(Leading, footer, FontInfoNormal);
                ocell3.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell3.BackgroundColor = new Color(255, 255, 255);
                ocell3.BorderColor = Color.BLACK;
                ocell3.BorderWidthBottom = 1;
                ocell3.BorderWidthLeft = 0;
                ocell3.BorderWidthRight = 0;
                ocell3.BorderWidthTop = 1;
                ocell3.Add(op3);
                footerTbl1.AddCell(ocell3);
                #endregion

                #region page 3 Table Data
                //iTextSharp.text.Font FontInfoItalic = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.ITALIC, iTextSharp.text.Color.BLACK);
                iTextSharp.text.Font FontInfoBoldUnderLine = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.UNDERLINE, iTextSharp.text.Color.BLACK);
                iTextSharp.text.Font FontInfoBold = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.BOLD, iTextSharp.text.Color.BLACK);

                string paragraph2 = string.Empty;

                paragraph2 = "";
                paragraph2 = "Para information en espanol, visite www.consumerfinance.gov/learnmore  o escribe a la Consumer "
                    + " Financial Protection Bureau , 1700 G Street N.W., Washington, DC 2006.";



                ocell = new Cell();
                op = new Phrase(Leading, paragraph2, FontInfoNormal);
                ocell.HorizontalAlignment = Element.ALIGN_CENTER;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain1.AddCell(ocell);


                ocell = new Cell();
                op = new Phrase(Leading, "A Summary of Your Rights Under the Fair Credit Reporting Act", FontInfoBoldUnderLine);
                ocell.HorizontalAlignment = Element.ALIGN_CENTER;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain1.AddCell(ocell);
                ocell = new Cell();
                op = new Phrase(Leading, str22(), FontInfoNormal);
                op.Leading = 13;
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain1.AddCell(ocell);
                ocell = new Cell();
                op = new Phrase(Leading, str3(), FontInfoNormal);
                op.Leading = 15;
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain1.AddCell(ocell);
                #endregion

                #region Page3 Footer
                iTextSharp.text.Table footerTbl3 = new iTextSharp.text.Table(1);
                footerTbl3.Width = 100;
                footerTbl3.Padding = 0;
                footerTbl3.Cellpadding = 2;
                footerTbl3.Cellspacing = 1;
                footerTbl3.DefaultCell.Border = Rectangle.NO_BORDER;
                footerTbl3.CellsFitPage = true;
                footerTbl3.TableFitsPage = true;
                footerTbl3.BorderColor = Color.WHITE;
                footerTbl3.Border = 0;

                Cell ocell2 = new Cell();
                Phrase op2 = new Phrase(Leading, "\n", FontInfoNormal);
                ocell2.HorizontalAlignment = Element.ALIGN_CENTER;
                ocell2.BackgroundColor = new Color(255, 255, 255);
                ocell2.BorderColor = Color.WHITE;
                ocell2.Border = 0;
                ocell2.BorderWidthTop = 1;
                ocell2.BorderColorTop = Color.BLACK;
                ocell2.Add(op2);
                footerTbl3.AddCell(ocell2);

                ocell2 = new Cell();
                footer = "2                                                                                                                              " + DateToBePrint.ToString("MMMM dd, yyyy hh:mm tt");
                op2 = new Phrase(Leading, footer, FontInfoNormal);
                ocell2.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell2.BackgroundColor = new Color(255, 255, 255);
                ocell2.BorderColor = Color.BLACK;
                ocell2.BorderWidthBottom = 1;
                ocell2.BorderWidthLeft = 0;
                ocell2.BorderWidthRight = 0;
                ocell2.BorderWidthTop = 1;
                ocell2.Add(op2);
                footerTbl3.AddCell(ocell2);
                #endregion

                #region Page 4
                iTextSharp.text.Table ObjPdfTblMainBottom = new iTextSharp.text.Table(2);
                ObjPdfTblMainBottom.Width = 100;
                ObjPdfTblMainBottom.Padding = 0;
                ObjPdfTblMainBottom.Cellpadding = 2;
                ObjPdfTblMainBottom.Cellspacing = 1;
                ObjPdfTblMainBottom.CellsFitPage = true;
                ObjPdfTblMainBottom.TableFitsPage = true;
                ObjPdfTblMainBottom.SetWidths(new int[2] { 60, 40 });
                ObjPdfTblMainBottom.BorderColor = Color.BLACK;
                ObjPdfTblMainBottom.Border = 1;

                Phrase TPhrase = new Phrase();
                Cell TCell = new Cell(TPhrase);

                TCell = new Cell();
                TPhrase = new Phrase(Leading, "TYPE OF BUSINESS:", FontInfoBold);
                TPhrase.Leading = 15;
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                TCell = new Cell();
                TPhrase = new Phrase(Leading, "CONTACT:", FontInfoBold);
                TPhrase.Leading = 15;
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTL1 = string.Empty;
                strTL1 = "1.a. Banks, saving associations, and credit unions with total assets of over $10 billion and their"
                + " affiliates."
                + "\nb. Such affiliates that are not banks,saving associations, or credit unions also should list, in"
                + " addition to the Bureau:";
                TCell = new Cell();
                TPhrase = new Phrase(13, strTL1, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTR1 = string.Empty;
                strTR1 = "a. Bureau of Consumer Financial Protection"
                + "\n1700 G Street NW"
                + "\nWashington, DC 20006"
                + "\nb. Federal Trade Commission: ConsumerResponse Center- FCRA"
                + "\nWashington, DC 20580"
                + "\n(877)382-4357";

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, strTR1, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTL2 = string.Empty;
                strTL2 = "2. To the extent not included in item 1 above:"
                + "\na. National Banks, federal saving associations, and federal branches ad federal agencies of"
                + "foreign banks"
                + "\nb. State member banks, branches and agencies of foreign banks( other than federal branches,"
                + " federal agencies and insured state branches of foreign banks), commercial leading companies"
                + " owned or controlled by foreign banks, and organizations operating under section 25 or 25A of"
                + " the Federal Reserve Act"
                + "\nc. Nonmember Insured Banks, Insured State Branches of Foreign banks and insured state"
                + " saving organizations"
                + "\nd. Federal Credit Unions";
                TCell = new Cell();
                TPhrase = new Phrase(13, strTL2, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTR2 = string.Empty;
                strTR2 = "a. Office of Comptroller of the Currency"
                + "\nCustomer Assistance Group"
                + "\n1301 McKinnney Street, Suite 3450"
                + "\nHouston, TX 77010-9050"
                + "\nb. Federal Reserve Consumer Help Center"
                + "\nPO Box 1200"
                + "\nMinneapolis, MN55480"
                + "\nc. FDIC Consumer Response Center"
                + "\n1100 Walnut Street, Box #11"
                + "\nKansas City, MO 64106"
                + "\nd. National Credit Union Administrator"
                + "\nOffice of Consumer Protection(OCP)"
                + "\nDivision of Consumer Compliance and Outreach(DCCO)"
                + "\n1775 Duke Street, Alexandria, VA 22314";

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, strTR2, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                TCell = new Cell();
                TPhrase = new Phrase(13, "3. Air carriers", FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTR3 = string.Empty;
                strTR3 = "Asst.General Counsel for Aviation Enforcement & Proceeding"
                + "\n Department ofTransportation"
                + "\n400 Seventh Street SW"
                + "\nWashington, DC 20590";

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, strTR3, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);


                TCell = new Cell();
                TPhrase = new Phrase(13, "4. Creditors Subject to Surface Transportation Board", FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTR4 = string.Empty;
                strTR4 = "Office of Proceedings, Surface Transportation Board"
                + "\nDepartment of Transportation"
                + "\n1925 K Street NW"
                + "\nWashington, DC 20423";

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, strTR4, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);


                TCell = new Cell();
                TPhrase = new Phrase(13, "5. Creditors Subject to Packers and Stockyards Act", FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTR5 = string.Empty;
                strTR5 = "Nearest packers and Stockyards Administration area supervisor";

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, strTR5, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                TCell = new Cell();
                TPhrase = new Phrase(13, "6. Small Business Investment Companies", FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTR6 = string.Empty;
                strTR6 = "Associate Deputy Administrator for CapitalAccess"
                + " United States Small Business Administration"
                + " 406 Third Street, SW, 8TH Floor"
                + " Washington, DC 20416";

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, strTR6, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                TCell = new Cell();
                TPhrase = new Phrase(13, "7. Brokers and Dealers", FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTR7 = string.Empty;
                strTR7 = "Securities and Exchange Commission"
                + "\n100 F St NE"
                + "\nWashington, DC 20549";

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, strTR7, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTL8 = string.Empty;
                strTL8 = "8. Federal Land Banks, Federal Land Bank Associations, Federal Intermediate Credit Banks,"
                + " and production Credit Associations";
                TCell = new Cell();
                TPhrase = new Phrase(13, strTL8, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTR8 = string.Empty;
                strTR8 = "Farm Credit Administration"
                + "\n1501 Farm Credit Drive"
                + "\nMcLean, VA 22102-5090";

                TCell = new Cell();
                TPhrase = new Phrase(13, strTR8, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, "9. Retailers, Finance Companies, and All Other Creditors Not Listed Above", FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);


                string strTR9 = string.Empty;
                strTR9 = "FTC Regional Office for region in which the creditor operates or"
                + "\nFederal Trade Commission: Consumer Response Center- FCRA"
                + "\nWashington, DC 20580"
                + "\n(877)382-4357";

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, strTR9, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                #endregion

                #region Page Num 4 Footer
                iTextSharp.text.Table footerTbl4 = new iTextSharp.text.Table(1);
                footerTbl4.Width = 100;
                footerTbl4.Padding = 0;
                footerTbl4.Cellpadding = 2;
                footerTbl4.Cellspacing = 1;
                footerTbl4.DefaultCell.Border = Rectangle.NO_BORDER;
                footerTbl4.CellsFitPage = true;
                footerTbl4.TableFitsPage = true;
                //footerTbl.BorderColor = Color.WHITE;
                footerTbl4.Border = 0;

                Cell ocell4 = new Cell();
                Phrase op4 = new Phrase(Leading, "\n", FontInfoNormal);
                ocell4.HorizontalAlignment = Element.ALIGN_CENTER;
                ocell4.BackgroundColor = new Color(255, 255, 255);
                ocell4.BorderColor = Color.WHITE;
                ocell4.Border = 0;
                ocell4.BorderWidthTop = 1;
                ocell4.BorderColorTop = Color.BLACK;
                ocell4.Add(op4);
                footerTbl4.AddCell(ocell4);

                ocell4 = new Cell();
                footer = "3                                                                                                                              " + DateToBePrint.ToString("MMMM dd, yyyy hh:mm tt");
                op4 = new Phrase(Leading, footer, FontInfoNormal);
                ocell4.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell4.BackgroundColor = new Color(255, 255, 255);
                ocell4.BorderColor = Color.BLACK;
                ocell4.BorderWidthBottom = 1;
                ocell4.BorderWidthLeft = 0;
                ocell4.BorderWidthRight = 0;
                ocell4.BorderWidthTop = 1;
                ocell4.Add(op4);
                footerTbl4.AddCell(ocell4);
                #endregion

                // New  Documents 

                #region Page 5 in Pdf

                iTextSharp.text.Table ObjPdfTblNewAddDoc = new iTextSharp.text.Table(1);

                ObjPdfTblNewAddDoc.Width = 100;
                ObjPdfTblNewAddDoc.Padding = 0;
                ObjPdfTblNewAddDoc.Cellpadding = 2;
                ObjPdfTblNewAddDoc.Cellspacing = 1;
                ObjPdfTblNewAddDoc.DefaultCell.Border = Rectangle.NO_BORDER;
                ObjPdfTblNewAddDoc.CellsFitPage = true;
                ObjPdfTblNewAddDoc.TableFitsPage = true;


                Phrase nPhrase = new Phrase();
                string paragraph3 = string.Empty;
                Cell ncell = new Cell(nPhrase);

                iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(new Uri(ApplicationPath.GetApplicationPath() + @"Content\themes\base\images\intelifi_pdf.png"));
                nPhrase = new Phrase();
                ImgLogo.ScaleAbsoluteHeight(40);
                ImgLogo.ScaleAbsoluteWidth(100);
                Chunk chk = new Chunk(ImgLogo, 0, -2);
                nPhrase.Add(chk);
                ObjPdfTblNewAddDoc.AddCell(nPhrase);


                ncell = new Cell();
                nPhrase = new Phrase(Leading, "Document Request Form", FontInfoBold);
                ncell.HorizontalAlignment = Element.ALIGN_CENTER;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);

                paragraph3 = "";
                paragraph3 = "This form is to be used to request a copy of your Consumer Background Investigative Report. Pursuant"
                + " to FCRA§ 609 and the abridged notification in the included Summary of Your Rights Under the Fair Credit Reporting"
                + " Act, “Every consumer reporting agency shall upon request and subject to 610(a)(1) [§ 1681h], clearly and "
                + " accurately disclose to the consumer all information in the consumer's file at the time of the request.” \n \n";



                ncell = new Cell();
                nPhrase = new Phrase(Leading, paragraph3, FontInfoNormal);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);


                string paragraph4 = string.Empty;

                paragraph4 = "";
                paragraph4 = "To receive a copy of your Consumer Background Investigative Report by mail, simply send the below"
                + " identifying information along with proof of your identity.  Proof of your identity can include a photocopy of "
                + " one of the following documents: a State Driver License, a State Issued ID Card, a Military ID Card or a W2 form. \n";




                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, paragraph4, FontInfoNormal);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(op);
                ObjPdfTblNewAddDoc.AddCell(nPhrase);

                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, "\nRequired Information: \n", FontInfoBold);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);

                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, "Your Social Security Number: ____ ____ ____ - ____ ____ - ____ ____ ____ ____ \n", FontInfoNormal);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);

                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, "Full Legal Name:______________________________________________________________________________ ", FontInfoNormal);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);

                ocell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, "DOB:________________________________________________________________________________________ ", FontInfoNormal);
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ocell);

                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, "Street Address (residence):____________________________________________________________________ ", FontInfoNormal);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);

                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, "City, State, Zip: _____________________________________________________________________________ ", FontInfoNormal);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);

                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, "Phone Number: (_______)____________________________________________________________________ ", FontInfoNormal);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);

                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, "E-mail Address: _____________________________________________________________________________ ", FontInfoNormal);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);


                string PleaseLeadPhrase = "\n Please read and sign the following statement. Your signature acknowledges your agreement. "
                                         + "By submitting this form, I state that all the information contained is true to the best of my knowledge.";
                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, PleaseLeadPhrase, FontInfoBold);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);



                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, "\nSignature: _________________________________________________________Date: __________________ \n", FontInfoNormal);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);



                string AfterCompletePhrase = "After completing this form, please return it by mail or fax. The mailing address and fax number are provided "
                                            + "below. Remember to include your proof of identity.";


                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, AfterCompletePhrase, FontInfoBold);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);


                string AddressInfo = "Intelifi Inc                                                                     \n"
                                    + "Attn: Compliance Department                                        \n "
                                    + "8730 Wilshire Blvd, Suite 412                                          \n"
                                    + "Beverly Hills, CA  90211                                                  \n";



                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, AddressInfo, FontInfoNormal);
                ncell.HorizontalAlignment = Element.ALIGN_CENTER;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);


                string AddressInfo2 = "Fax No. (310) 623-1821                                                  \n"
                                     + "Customer Service No (888) 409-1819                             \n";
                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, AddressInfo2, FontInfoNormal);
                ncell.HorizontalAlignment = Element.ALIGN_CENTER;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);



                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, "Thank you,", FontInfoNormal);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);

                ncell = new Cell();
                nPhrase = new Phrase();
                nPhrase = new Phrase(Leading, "Intelifi Compliance Department", FontInfoNormal);
                ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                ncell.BackgroundColor = new Color(255, 255, 255);
                ncell.Add(nPhrase);
                ObjPdfTblNewAddDoc.AddCell(ncell);


                #endregion

                #region Page Num 5 Footer
                iTextSharp.text.Table footerTbl5 = new iTextSharp.text.Table(1);
                footerTbl5.Width = 100;
                footerTbl5.Padding = 0;
                footerTbl5.Cellpadding = 2;
                footerTbl5.Cellspacing = 1;
                footerTbl5.DefaultCell.Border = Rectangle.NO_BORDER;
                footerTbl5.CellsFitPage = true;
                footerTbl5.TableFitsPage = true;
                //footerTbl.BorderColor = Color.WHITE;
                footerTbl5.Border = 0;

                Cell ocell1 = new Cell();
                Phrase op1 = new Phrase(Leading, "\n\n\n\n\n\n\n\n", FontInfoNormal);
                ocell1.HorizontalAlignment = Element.ALIGN_CENTER;
                ocell1.BackgroundColor = new Color(255, 255, 255);
                ocell1.BorderColor = Color.WHITE;
                ocell1.Border = 0;
                ocell1.BorderWidthTop = 1;
                ocell1.BorderColorTop = Color.BLACK;
                ocell1.Add(op1);
                footerTbl5.AddCell(ocell1);

                ocell1 = new Cell();
                footer = "4                                                                                                                              " + DateToBePrint.ToString("MMMM dd, yyyy hh:mm tt");
                op1 = new Phrase(Leading, footer, FontInfoNormal);
                ocell1.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell1.BackgroundColor = new Color(255, 255, 255);
                ocell1.BorderColor = Color.BLACK;
                ocell1.BorderWidthBottom = 1;
                ocell1.BorderWidthLeft = 0;
                ocell1.BorderWidthRight = 0;
                ocell1.BorderWidthTop = 1;
                ocell1.Add(op1);
                footerTbl5.AddCell(ocell1);
                #endregion

                #region Blank Page Insert In PDF
                iTextSharp.text.Table blankTbl = new iTextSharp.text.Table(1);
                blankTbl.Width = 100;
                blankTbl.Padding = 0;
                blankTbl.Cellpadding = 2;
                blankTbl.Cellspacing = 1;
                blankTbl.DefaultCell.Border = Rectangle.NO_BORDER;
                blankTbl.CellsFitPage = true;
                blankTbl.TableFitsPage = true;
                #endregion

                #region Add Table in Document
                ObjDocument.Add(MainPageTbl);
                ObjDocument.Add(TblMain);
                ObjDocument.Add(footerTbl1);
                ObjDocument.NewPage();
                ObjDocument.Add(blankTbl);
                ObjDocument.NewPage();
                ObjDocument.Add(TblMain1);
                ObjDocument.Add(blankTbl);
                ObjDocument.Add(footerTbl3);
                ObjDocument.NewPage();
                ObjDocument.Add(ObjPdfTblMainBottom);
                ObjDocument.Add(blankTbl);
                ObjDocument.Add(footerTbl4);
                ObjDocument.NewPage();
                ObjDocument.Add(ObjPdfTblNewAddDoc);
                ObjDocument.Add(footerTbl5);
                ObjDocument.NewPage();
                ObjDocument.Add(blankTbl);
                ObjDocument.NewPage();
                #endregion
            }
            catch (Exception ex)
            {
                VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "NoticePdf613");
                VendorServices.GeneralService.WriteLog(" GeneratePdfOf613Noticeforftpuploadservie status" + ex.Message.ToString(), "NoticePdf613");
            }
            finally
            {
                ObjDocument.Close();
            }
            return pdfFileName;
        }
        //INT-14



        public void UpdateIsEmailNotice(int? pkOrderDetailId, bool IsNCRHit, bool IsNoticeEmail)
        {
            try
            {
                using (EmergeDALDataContext ObjdataContext = new EmergeDALDataContext())
                {
                    VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "NoticePdf613");
                    VendorServices.GeneralService.WriteLog(" IsNCRHit" + IsNCRHit, "NoticePdf613");
                    VendorServices.GeneralService.WriteLog(" IsNoticeEmail" + IsNoticeEmail, "NoticePdf613");
                    tblOrderDetail ObjOrderDetail = ObjdataContext.tblOrderDetails.Where(db => db.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                    ObjOrderDetail.IsNCRHit = IsNCRHit;
                    ObjOrderDetail.IsNoticeEmail = IsNoticeEmail;
                    ObjdataContext.SubmitChanges();

                }
            }
            catch (Exception sad)
            {
                VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "NoticePdf613");
                VendorServices.GeneralService.WriteLog("Exceptuion come in update is email notice " + sad.Message.ToString(), "NoticePdf613");
            }
        }

        private static void CheckReportStatusinRCXFile(int? pkOrderDetailId, ref int ReportStatus, ref bool Objbool)
        {
            if (RCXFilePath != "")
            {
                bool isFileExists = false;
                string responseString = GetRCXResponsefromFile(pkOrderDetailId, out isFileExists);
                if (isFileExists)
                {
                    string savedXML_Value = GetRequiredXml_ForRapidCourt(responseString);
                    XDocument ObjXDocumentsaved = XDocument.Parse(savedXML_Value);
                    var List_OrderStatusSaved = ObjXDocumentsaved.Descendants("OrderStatus").ToList();
                    foreach (XElement ObjXElementsaved in List_OrderStatusSaved)
                    {
                        if (ObjXElementsaved != null)
                        {
                            if (ObjXElementsaved.Value.Trim() == "Completed")
                            {
                                Objbool = true;
                                ReportStatus = 2;
                            }
                        }
                    }
                }
                else
                {
                    Objbool = true;
                }
            }
        }

        /// <summary>
        /// return RCX response from txt file
        /// </summary>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public static string GetRCXResponsefromFile(int? pkOrderDetailId, out bool IsExists)
        {
            string FileName = RCXFilePath + pkOrderDetailId + ".txt";
            string responseString = "";
            IsExists = false;
            if (File.Exists(FileName))
            {
                using (StreamReader responseFile = new StreamReader(FileName))
                {
                    responseString = responseFile.ReadToEnd();
                    responseFile.Close();
                    IsExists = true;
                }
            }
            return responseString;
        }


        /// <summary>
        /// Function To check valid XML 
        /// </summary>
        /// <param name="Vendor_Response"></param>
        /// <returns></returns>
        public static bool IsValidXml(string Vendor_Response)
        {
            try
            {
                XmlDocument ObjXmlDocument = new XmlDocument();
                ObjXmlDocument.LoadXml(Vendor_Response);
                return true;

            }
            catch (Exception)
            {

                return false;
            }
        }

        /// <summary>
        /// Function to Cut the Unwanted Code from XML
        /// </summary>
        /// <param name="XmlResponse"></param>
        /// <returns></returns>
        public static string GetRequiredXml_ForRapidCourt(string XmlResponse)
        {
            string XmlResponse_Local = XmlResponse;
            XmlResponse_Local = XmlResponse_Local.Replace("</BackgroundReports>", "");
            int xmlstartindex = XmlResponse_Local.IndexOf("<BackgroundReportPackage>");
            XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
            return XmlResponse_Local;
        }

        /// <summary>
        /// Function To Update SendPendingEmail Status
        /// </summary>
        /// <param name="OrderDetailId"></param>
        public static void UpdateEmailSentStatus(int OrderDetailId)
        {
            using (EmergeDALDataContext ObjUSAIntelDataDataContext = new EmergeDALDataContext())
            {
                tblOrderDetail ObjtblOrderDetail = ObjUSAIntelDataDataContext.tblOrderDetails.Single(data => data.pkOrderDetailId == OrderDetailId);
                ObjtblOrderDetail.IsSentPendingEmail = true;
                ObjUSAIntelDataDataContext.SubmitChanges();
            }
        }

        /// <summary>
        /// Function to Update In-Complete Email  Sent Status
        /// </summary>
        /// <param name="OrderDetailId"></param>
        public static void UpdateInCompleteEmailSentStatus(int OrderDetailId)
        {
            using (EmergeDALDataContext ObjUSAIntelDataDataContext = new EmergeDALDataContext())
            {
                tblOrderDetail ObjtblOrderDetail = ObjUSAIntelDataDataContext.tblOrderDetails.Single(data => data.pkOrderDetailId == OrderDetailId);
                ObjtblOrderDetail.IsSentInCompleteEmail = true;
                ObjUSAIntelDataDataContext.SubmitChanges();
            }
        }

        /// <summary>
        /// Function to Update First Pending Request Sent Status so that Next time if same Pending Report is being opened under View Report page then Call to vendor must not go.
        /// </summary>
        /// <param name="OrderDetailId"></param>
        public static void UpdateFirstPendingRequestStatus(int OrderDetailId)
        {
            using (EmergeDALDataContext ObjUSAIntelDataDataContext = new EmergeDALDataContext())
            {
                tblOrderDetail ObjtblOrderDetail = ObjUSAIntelDataDataContext.tblOrderDetails.Single(data => data.pkOrderDetailId == OrderDetailId);
                ObjtblOrderDetail.IsFirstPendingRequest = false;
                ObjUSAIntelDataDataContext.SubmitChanges();
            }
        }

        ///// <summary>
        ///// Method to Get UserName while User coming from Get Report Link on their mail
        ///// </summary>
        ///// <param name="OrderId"></param>
        ///// <returns></returns>
        public List<Proc_GetUserNameFromOrderIdResult> GetUserNameFromOrderId(int? OrderId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                List<Proc_GetUserNameFromOrderIdResult> ObjResult = DX.GetUserNameFromOrderId(OrderId).ToList();
                return ObjResult;
            }
        }

        public List<tblJurisdiction> GetJurisdictions()
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return DX.tblJurisdictions.OrderBy(db => db.JurisdictionName).ToList();
            }
        }

        public List<clsJurisdiction> GetJurisdictionsForNewReport()
        {

            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                var Response = from d in DX.tblJurisdictions
                               join db in DX.tblJurisdictionFees on
                               d.pkJurisdictionId equals db.fkJurisdictionId
                               select new clsJurisdiction
                               {
                                   pkJurisdictionId = Convert.ToString(d.pkJurisdictionId + "_" + db.IsConsent + "_" + d.JurisdictionName),
                                   JurisdictionName = d.JurisdictionName
                               };
                return Response.OrderBy(db => db.JurisdictionName).ToList();
                //  return DX.tblJurisdictions.ToList();
            }
        }

        public List<clsJurisdiction> GetJurisdictionsForNewReportLog()
        {

            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                var Response = from d in DX.tblJurisdictions
                               join db in DX.tblJurisdictionFees on
                               d.pkJurisdictionId equals db.fkJurisdictionId
                               select new clsJurisdiction
                               {
                                   pkJurisdictionId = Convert.ToString(d.pkJurisdictionId),
                                   JurisdictionName = d.JurisdictionName
                               };
                return Response.OrderBy(db => db.JurisdictionName).ToList();
                //  return DX.tblJurisdictions.ToList();
            }
        }

        public List<Proc_Get_DisclaimerInfoforScrMvrWcbResult> GetDisclamierInfoForScrMvrWcb(int? fkperApplicationId, int? StateId)
        {
            using (EmergeDALDataContext Objdb = new EmergeDALDataContext())
            {
                return Objdb.Get_DisclaimerInfoforScrMvrWcb(fkperApplicationId, StateId).ToList();
            }
        }

        public List<Proc_Get_DisclaimerInfoforCcr1Ccr2RcxResult> GetDisclamierInfoForCcr1Ccr2Rcx(int fkCountyId, int PkProductId)
        {
            using (EmergeDALDataContext objdb = new EmergeDALDataContext())
            {
                return objdb.Get_DisclaimerInfoforCcr1Ccr2Rcx(fkCountyId, PkProductId).ToList();
            }
        }


        public List<Proc_Get_DisclaimerInfoForFCRResult> GetDisclamierInfoForFCR(string CountyCode)
        {
            using (EmergeDALDataContext Objdb = new EmergeDALDataContext())
            {
                return Objdb.Get_DisclaimerInfoForFCR(CountyCode).ToList();
            }
        }
        public int UpdateJurisdictionfee(List<tblJurisdiction> ObjJuriFeeColl)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                int Status = 0;
                if (ObjJuriFeeColl.Count > 0)
                {
                    for (int i = 0; i < ObjJuriFeeColl.Count; i++)
                    {
                        tblJurisdiction ObjJurisdiction = DX.tblJurisdictions.Single(db => db.pkJurisdictionId == ObjJuriFeeColl.ElementAt(i).pkJurisdictionId);
                        if (ObjJurisdiction != null)
                        {
                            ObjJurisdiction.JurisdictionFee = ObjJuriFeeColl.ElementAt(i).JurisdictionFee;
                            ObjJurisdiction.DisclaimerData = ObjJuriFeeColl.ElementAt(i).DisclaimerData;

                            ObjJurisdiction.IsConsentForm = ObjJuriFeeColl.ElementAt(i).IsConsentForm;
                            DX.SubmitChanges();
                        }
                    }
                    Status = 1;
                }
                return Status;
            }
        }

        public List<tblCounty> GetAllCounties()
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                IQueryable<tblCounty> ObjCollection = DX.tblCounties.OrderBy(db => db.CountyName).Select(db => db);
                return ObjCollection.ToList();
            }
        }

        public bool Is6MonthOld(int? OrderNo)
        {
            bool RetStatus = false;
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                if (dx.tblOrders.Where(d => d.pkOrderId == OrderNo).Count() == 0)
                {
                    RetStatus = true;//Means order is older than 6 month.
                }
            }
            return RetStatus;
        }

        public int UpdateWCVDetails(List<tblState> lstState)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                if (lstState.Count > 0)
                {
                    foreach (tblState obj in lstState)
                    {
                        Dx.proc_UpdateWCVConsentDetails(obj.pkStateId, obj.IsConsentForm_WCV);
                    }
                    return 1;
                }
                return 0;
            }
        }

        public List<tblCompany> MatchAPIUsername(string Username, string Password)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return DX.tblCompanies.Where(d => d.APIUsername == Username && d.APIPassword == Password).Select(d => d).ToList();
            }
        }


        public bool UserEmailStatusCheck(Guid user_ID, int pktemplateid, int? fkcompanyuserid)
        {

            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                bool email_status = true;
                if (user_ID != null || fkcompanyuserid != null)
                {
                    var temp = DX.Check_User_Email_Status(fkcompanyuserid, user_ID, pktemplateid).FirstOrDefault();
                    if (temp != null)
                    {
                        email_status = Convert.ToBoolean(temp.IsSendEmail);
                    }
                    else
                    {
                        email_status = true;
                    }

                }
                if (email_status == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

        }

        #region Filter Response During OrderPlace Test Site

        public string GetFilteredXMLDuringOrderPlace(string ProductCode, string strResponseData, int? pkOrderDetailId, DateTime CurrentDate, bool IsPendingReport, bool IsLiveRunner)
        {
            string FinalResponse = ""; string UpdatedResponse = "";
            using (EmergeDALDataContext Obj = new EmergeDALDataContext())
            {
                UpdatedResponse = strResponseData;

                HttpContext.Current.Session["OrderDetailId"] = pkOrderDetailId;


                string FilterId = "3";
                bool IsAdmin = false;
                bool UserCheck = false;
                bool ReportExist = false;
                bool IsthisPackageReport = false;

                BALCompany ObjBALCompany = new BALCompany();


                var ObjData = (from u in Obj.tblOrderDetails
                               where u.pkOrderDetailId == pkOrderDetailId
                               select u).FirstOrDefault();
                var Collection = ObjBALCompany.GetFiltersAccordingFilterId(FilterId).FirstOrDefault(); //Getting filter rules for Watch list FilterOR Dismissed Filter
                //string Keywords = Collection.Keywords;
                if (IsPendingReport == false)
                {
                    MembershipUser ObjMembershipUser = Membership.GetUser(Utilitys.GetUID(HttpContext.Current.User.Identity.Name));
                    if (ObjMembershipUser != null)
                    {
                        if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SystemAdmin")) { IsAdmin = true; } //Checking Current user is admin or not.
                        if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SupportManager")) { IsAdmin = true; }
                        if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SupportTechnician")) { IsAdmin = true; }

                        if (Collection != null)
                        {
                            if (ObjData != null)
                            {
                                if (ObjData.IsPackageReport == true)//Checking Is this PackageReport  or not.
                                {
                                    if (Collection.Packages == true) { IsthisPackageReport = false; }
                                    else { IsthisPackageReport = true; }
                                }
                                else
                                {
                                    IsthisPackageReport = false;
                                }
                                if (string.IsNullOrEmpty(Collection.ReportType) == false) //Checking Report type exist   or not.
                                {
                                    var Report_Type = Collection.ReportType;
                                    var CollProduct = Report_Type.Split(',');
                                    string Coll = ProductCode.ToUpper();
                                    if (CollProduct.Length > 0)
                                    {
                                        for (int m = 0; m < CollProduct.Length; m++)
                                        {
                                            if (Coll == CollProduct[m].ToUpper())
                                            {
                                                ReportExist = true;
                                                break;
                                            }
                                        }
                                    }
                                }

                                if (Collection.Enabled == true) //Checking Watch list FilterOR DismissedFilter  Active or not.
                                {
                                    if (Collection.Users == 1) { UserCheck = true; }
                                    if (Collection.Users == 2) { if (IsAdmin == true) { UserCheck = true; } }
                                    if (Collection.Users == 3) { if (IsAdmin == false) { UserCheck = true; } }
                                    if (UserCheck == true)
                                    {
                                        if (IsthisPackageReport == false)
                                        {
                                            if (ReportExist == true)
                                            {
                                                #region Dismissed Charge Response
                                                try
                                                {
                                                    string UpdatedDismissedChargeResponse = GetRemovedDismissedChargeXMLFilterDuringOrderPlace(UpdatedResponse, ProductCode, IsLiveRunner);
                                                    UpdatedResponse = UpdatedDismissedChargeResponse;
                                                    FinalResponse = UpdatedDismissedChargeResponse;
                                                }
                                                catch
                                                {
                                                    FinalResponse = UpdatedResponse;
                                                }
                                                #endregion Dismissed Charge Response
                                            }
                                        }
                                    }
                                }
                            }
                        }


                        //BALEmergeReview objBALEmergeReview = new BALEmergeReview();

                        string Filter_Id4 = "4";
                        bool Is_Admin4 = false;
                        bool User_Check4 = false;
                        bool Report_Exist4 = false;
                        bool Isthis_PackageReport4 = false;
                        var Collection_4 = ObjBALCompany.GetFiltersAccordingFilterId(Filter_Id4).FirstOrDefault(); //Getting filter rules 7 Year Filter.
                        if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SystemAdmin")) { Is_Admin4 = true; }      //Checking Current user is admin or not.
                        if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SupportManager")) { Is_Admin4 = true; }
                        if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SupportTechnician")) { Is_Admin4 = true; }
                        if (Collection_4 != null)
                        {
                            if (ObjData != null)
                            {
                                if (ObjData.IsPackageReport == true) //Checking Is this PackageReport  or not.
                                {
                                    if (Collection_4.Packages == true) { Isthis_PackageReport4 = false; }
                                    else { Isthis_PackageReport4 = true; }
                                }
                                else { Isthis_PackageReport4 = false; }
                                if (string.IsNullOrEmpty(Collection_4.ReportType) == false) //Checking Report type exist   or not.
                                {
                                    var Report_Type = Collection_4.ReportType;
                                    var CollProduct = Report_Type.Split(',');
                                    string Coll = ProductCode.ToUpper();
                                    if (CollProduct.Length > 0)
                                    {
                                        for (int m = 0; m < CollProduct.Length; m++)
                                        {
                                            if (Coll == CollProduct[m].ToUpper())
                                            {
                                                Report_Exist4 = true;
                                                break;
                                            }
                                        }
                                    }
                                }

                                if (Collection_4.Enabled == true) //Checking Report type exist   or not.
                                {
                                    if (Collection_4.Users == 1) { User_Check4 = true; }
                                    if (Collection_4.Users == 2) { if (Is_Admin4 == true) { User_Check4 = true; } }
                                    if (Collection_4.Users == 3) { if (Is_Admin4 == false) { User_Check4 = true; } }
                                    if (User_Check4 == true)
                                    {
                                        if (Isthis_PackageReport4 == false)
                                        {
                                            if (Report_Exist4 == true)
                                            {
                                                #region Ticket #173:
                                                try
                                                {
                                                    AutoReviewByKeywordsDuringOrderPlace(UpdatedResponse, pkOrderDetailId, ProductCode, IsPendingReport);
                                                }
                                                catch
                                                {
                                                    FinalResponse = UpdatedResponse;
                                                }
                                                #endregion
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        string Filter_Id = "1";
                        bool Is_Admin = false;
                        bool User_Check = false;
                        bool Report_Exist = false;
                        bool Isthis_PackageReport = false;
                        var Collection_seven = ObjBALCompany.GetFiltersAccordingFilterId(Filter_Id).FirstOrDefault(); //Getting filter rules 7 Year Filter.
                        if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SystemAdmin")) { Is_Admin = true; }      //Checking Current user is admin or not.
                        if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SupportManager")) { Is_Admin = true; }
                        if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SupportTechnician")) { Is_Admin = true; }
                        if (Collection_seven != null)
                        {
                            if (ObjData != null)
                            {
                                if (ObjData.IsPackageReport == true) //Checking Is this PackageReport  or not.
                                {
                                    if (Collection_seven.Packages == true) { Isthis_PackageReport = false; }
                                    else { Isthis_PackageReport = true; }
                                }
                                else { Isthis_PackageReport = false; }
                                if (string.IsNullOrEmpty(Collection_seven.ReportType) == false) //Checking Report type exist   or not.
                                {
                                    var Report_Type = Collection_seven.ReportType;
                                    var CollProduct = Report_Type.Split(',');
                                    string Coll = ProductCode.ToUpper();
                                    if (CollProduct.Length > 0)
                                    {
                                        for (int m = 0; m < CollProduct.Length; m++)
                                        {
                                            if (Coll == CollProduct[m].ToUpper())
                                            {
                                                Report_Exist = true;
                                                break;
                                            }
                                        }
                                    }
                                }

                                if (Collection_seven.Enabled == true) //Checking Report type exist   or not.
                                {
                                    if (Collection_seven.Users == 1) { User_Check = true; }
                                    if (Collection_seven.Users == 2) { if (Is_Admin == true) { User_Check = true; } }
                                    if (Collection_seven.Users == 3) { if (Is_Admin == false) { User_Check = true; } }
                                    if (User_Check == true)
                                    {
                                        if (Isthis_PackageReport == false)
                                        {
                                            if (Report_Exist == true)
                                            {
                                                #region Ticket #800: 7 YEAR STATES
                                                try
                                                {
                                                    string Updated7YearResponse = HideSevenYearDuringOrderPlace(UpdatedResponse, ProductCode, CurrentDate, pkOrderDetailId, IsPendingReport);
                                                    UpdatedResponse = Updated7YearResponse;
                                                    FinalResponse = Updated7YearResponse;
                                                }
                                                catch
                                                {
                                                    FinalResponse = UpdatedResponse;
                                                }
                                                #endregion
                                            }
                                        }
                                    }
                                }
                            }
                        }


                    }
                }
                else
                {
                    if (Collection != null)
                    {
                        if (ObjData != null)
                        {
                            if (ObjData.IsPackageReport == true)//Checking Is this PackageReport  or not.
                            {
                                if (Collection.Packages == true) { IsthisPackageReport = false; }
                                else { IsthisPackageReport = true; }
                            }
                            else
                            {
                                IsthisPackageReport = false;
                            }
                            if (string.IsNullOrEmpty(Collection.ReportType) == false) //Checking Report type exist   or not.
                            {
                                var Report_Type = Collection.ReportType;
                                var CollProduct = Report_Type.Split(',');
                                string Coll = ProductCode.ToUpper();
                                if (CollProduct.Length > 0)
                                {
                                    for (int m = 0; m < CollProduct.Length; m++)
                                    {
                                        if (Coll == CollProduct[m].ToUpper())
                                        {
                                            ReportExist = true;
                                            break;
                                        }
                                    }
                                }
                            }

                            if (Collection.Enabled == true) //Checking Watch list FilterOR DismissedFilter  Active or not.
                            {
                                if (IsthisPackageReport == false)
                                {
                                    if (ReportExist == true)
                                    {
                                        #region Dismissed Charge Response
                                        try
                                        {
                                            string UpdatedDismissedChargeResponse = GetRemovedDismissedChargeXMLFilterDuringOrderPlace(UpdatedResponse, ProductCode, IsLiveRunner);
                                            UpdatedResponse = UpdatedDismissedChargeResponse;
                                            FinalResponse = UpdatedDismissedChargeResponse;
                                        }
                                        catch
                                        {
                                            FinalResponse = UpdatedResponse;
                                        }
                                        #endregion Dismissed Charge Response
                                    }
                                }
                            }
                        }
                    }


                    BALEmergeReview objBALEmergeReview = new BALEmergeReview();

                    string Filter_Id4 = "4";
                    bool Report_Exist4 = false;
                    bool Isthis_PackageReport4 = false;
                    var Collection_4 = ObjBALCompany.GetFiltersAccordingFilterId(Filter_Id4).FirstOrDefault(); //Getting filter rules 7 Year Filter.}
                    if (Collection_4 != null)
                    {
                        if (ObjData != null)
                        {
                            if (ObjData.IsPackageReport == true) //Checking Is this PackageReport  or not.
                            {
                                if (Collection_4.Packages == true) { Isthis_PackageReport4 = false; }
                                else { Isthis_PackageReport4 = true; }
                            }
                            else { Isthis_PackageReport4 = false; }
                            if (string.IsNullOrEmpty(Collection_4.ReportType) == false) //Checking Report type exist   or not.
                            {
                                var Report_Type = Collection_4.ReportType;
                                var CollProduct = Report_Type.Split(',');
                                string Coll = ProductCode.ToUpper();
                                if (CollProduct.Length > 0)
                                {
                                    for (int m = 0; m < CollProduct.Length; m++)
                                    {
                                        if (Coll == CollProduct[m].ToUpper())
                                        {
                                            Report_Exist4 = true;
                                            break;
                                        }
                                    }
                                }
                            }

                            if (Collection_4.Enabled == true) //Checking Report type exist   or not.
                            {
                                if (Isthis_PackageReport4 == false)
                                {
                                    if (Report_Exist4 == true)
                                    {
                                        #region Ticket #173:
                                        try
                                        {
                                            AutoReviewByKeywordsDuringOrderPlace(UpdatedResponse, pkOrderDetailId, ProductCode, IsPendingReport);
                                        }
                                        catch
                                        {
                                            FinalResponse = UpdatedResponse;
                                        }
                                        #endregion
                                    }
                                }
                            }
                        }
                    }

                    string Filter_Id = "1";
                    bool Report_Exist = false;
                    bool Isthis_PackageReport = false;
                    var Collection_seven = ObjBALCompany.GetFiltersAccordingFilterId(Filter_Id).FirstOrDefault(); //Getting filter rules 7 Year Filter.
                    if (Collection_seven != null)
                    {
                        if (ObjData != null)
                        {
                            if (ObjData.IsPackageReport == true) //Checking Is this PackageReport  or not.
                            {
                                if (Collection_seven.Packages == true) { Isthis_PackageReport = false; }
                                else { Isthis_PackageReport = true; }
                            }
                            else { Isthis_PackageReport = false; }
                            if (string.IsNullOrEmpty(Collection_seven.ReportType) == false) //Checking Report type exist   or not.
                            {
                                var Report_Type = Collection_seven.ReportType;
                                var CollProduct = Report_Type.Split(',');
                                string Coll = ProductCode.ToUpper();
                                if (CollProduct.Length > 0)
                                {
                                    for (int m = 0; m < CollProduct.Length; m++)
                                    {
                                        if (Coll == CollProduct[m].ToUpper())
                                        {
                                            Report_Exist = true;
                                            break;
                                        }
                                    }
                                }
                            }

                            if (Collection_seven.Enabled == true) //Checking Report type exist   or not.
                            {
                                if (Isthis_PackageReport == false)
                                {
                                    if (Report_Exist == true)
                                    {
                                        #region Ticket #800: 7 YEAR STATES
                                        try
                                        {
                                            string Updated7YearResponse = HideSevenYearDuringOrderPlace(UpdatedResponse, ProductCode, CurrentDate, pkOrderDetailId, IsPendingReport);
                                            UpdatedResponse = Updated7YearResponse;
                                            FinalResponse = Updated7YearResponse;
                                        }
                                        catch
                                        {
                                            FinalResponse = UpdatedResponse;
                                        }
                                        #endregion
                                    }
                                }
                            }
                        }
                    }
                }

                if (FinalResponse == string.Empty)
                {
                    FinalResponse = strResponseData;
                }
                if (ProductCode == "OFAC" || ProductCode == "NCR1" || ProductCode == "NCR2" || ProductCode == "NCR+" || ProductCode == "NCR4" || ProductCode == "SCR" || ProductCode == "CCR1" || ProductCode == "CCR2" || ProductCode == "SOR" || ProductCode == "RCX" || ProductCode == "SCRLiveRunner" || ProductCode == "CCR1LiveRunner" || ProductCode == "CCR2LiveRunner")
                {
                    XDocument ObjXDocument = XDocument.Parse(FinalResponse);
                    XMLFilteration ObjXMLFilteration = new XMLFilteration();
                    using (EmergeDALDataContext DX = new EmergeDALDataContext())
                    {
                        if (DX.tblOrderDetails.Where(db => db.pkOrderDetailId == pkOrderDetailId).Count() > 0)
                        {
                            tblOrderDetail ObjOrderDetails = DX.tblOrderDetails.Where(db => db.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                            if (ObjOrderDetails != null)
                            {
                                tblOrder objtblOrder = DX.tblOrders.Where(db => db.pkOrderId == ObjOrderDetails.fkOrderId).FirstOrDefault(); ;
                                if (objtblOrder.HitStatus == 0)
                                {
                                    ObjXMLFilteration.CheckIsHit(ObjOrderDetails.fkOrderId, ProductCode, ObjXDocument);
                                }
                            }
                        }
                        else
                        {
                            tblOrderDetails_Archieve ObjtblOrderDetails_Archieve = DX.tblOrderDetails_Archieves.Where(db => db.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                            if (ObjtblOrderDetails_Archieve != null)
                            {
                                tblOrders_Archieve objtblOrder = DX.tblOrders_Archieves.Where(db => db.pkOrderId == ObjtblOrderDetails_Archieve.fkOrderId).FirstOrDefault(); ;
                                if (objtblOrder.HitStatus == 0)
                                {
                                    ObjXMLFilteration.CheckIsHit(ObjtblOrderDetails_Archieve.fkOrderId, ProductCode, ObjXDocument);
                                }
                            }
                        }
                    }
                }
            }
            return FinalResponse;
        }

        public string GetRemovedDismissedChargeXMLFilterDuringOrderPlace(string strResponseData, string ProductCode, bool IsLiveRunner)
        {
            string FinalResponse = "";
            string strUpdatedXML = string.Empty;
            if (ProductCode.ToUpper() == "NCR1")
            {
                XDocument UpdatedXML = GetFilteredResponseforNCR1DuringOrderPlace(strResponseData);
                strUpdatedXML = Convert.ToString(UpdatedXML);
                int TrimedUpTo = strResponseData.IndexOf("<BackgroundReportPackage>");
                StringBuilder CompleteResponse = new StringBuilder();
                string XMLInitials = strResponseData.Substring(0, TrimedUpTo);
                CompleteResponse.Append(XMLInitials);
                CompleteResponse.Append(strUpdatedXML);
                CompleteResponse.Append("</BackgroundReports>");
                FinalResponse = Convert.ToString(CompleteResponse);
            }
            else if (ProductCode.ToUpper() == "NCR+")
            {
                strUpdatedXML = strResponseData;
                int TrimedUpTo = strResponseData.IndexOf("<BackgroundReportPackage>");
                StringBuilder CompleteResponse = new StringBuilder();
                string XMLInitials = strResponseData.Substring(0, TrimedUpTo);
                CompleteResponse.Append(XMLInitials);
                CompleteResponse.Append(strUpdatedXML);
                CompleteResponse.Append("</BackgroundReports>");
                FinalResponse = Convert.ToString(CompleteResponse);
            }
            else if (ProductCode.ToUpper() == "CCR1" && IsLiveRunner == false)
            {
                //strUpdatedXML = strResponseData;
                //FinalResponse = strUpdatedXML;
                XDocument UpdatedXML = GetRemovedDismissedChargeXMLFilterforCCR1DuringOrderPlace(strResponseData);
                strUpdatedXML = Convert.ToString(UpdatedXML);
                int TrimedUpTo = strResponseData.IndexOf("<FastraxNetwork");
                StringBuilder CompleteResponse = new StringBuilder();
                string XMLInitials = strResponseData.Substring(0, TrimedUpTo);
                CompleteResponse.Append(XMLInitials);
                CompleteResponse.Append(strUpdatedXML);
                FinalResponse = Convert.ToString(CompleteResponse);
            }
            else if (ProductCode.ToUpper() == "SCR" && IsLiveRunner == false)
            {
                XDocument UpdatedXML = GetRemovedDismissedChargeXMLFilterforSCRDuringOrderPlace(strResponseData);
                strUpdatedXML = Convert.ToString(UpdatedXML);
                FinalResponse = strUpdatedXML;
            }
            else
            {
                XDocument UpdatedXML = GetDismissedChargeResponseDuringOrderPlace(strResponseData);
                strUpdatedXML = Convert.ToString(UpdatedXML);
                int TrimedUpTo = strResponseData.IndexOf("<BackgroundReportPackage>");
                StringBuilder CompleteResponse = new StringBuilder();
                string XMLInitials = strResponseData.Substring(0, TrimedUpTo);
                CompleteResponse.Append(XMLInitials);
                CompleteResponse.Append(strUpdatedXML);
                CompleteResponse.Append("</BackgroundReports>");
                FinalResponse = Convert.ToString(CompleteResponse);
            }
            return FinalResponse;
        }

        public XDocument GetRemovedDismissedChargeXMLFilterforCCR1DuringOrderPlace(string strResponseData)
        {
            XDocument ObjXDocumentUppdated = new XDocument();
            XDocument ObjXDocument = new XDocument();
            ObjXDocument = XDocument.Parse(strResponseData);
            BALCompany ObjBALCompany = new BALCompany();
            List<string> s = new List<string>();
            string FilterId = "3";
            var Collection = ObjBALCompany.GetFiltersAccordingFilterId(FilterId).FirstOrDefault();//Getting filter rules for Watch list FilterOR Dismissed Filter
            if (Collection != null)
            {
                if (string.IsNullOrEmpty(Collection.Keywords) == false)
                {
                    var coll = Collection.Keywords.ToString();
                    var ArrayCollection = coll.Split(',');
                    if (ArrayCollection.Count() > 0)
                    {
                        for (int j = 0; j < ArrayCollection.Count(); j++)
                        {

                            if (string.IsNullOrEmpty(ArrayCollection[j]) == false)
                            {
                                s.Add(ArrayCollection[j].Trim().ToUpper()); //Adding all keywords in List<string> s = new List<string>();
                            }
                        }

                    }

                }
            }

            try
            {
                var lstScreenings = ObjXDocument.Descendants("report-requested").ToList();

                if (lstScreenings.Count > 0)
                {
                    var lstCriminalReports = lstScreenings.Descendants("report").FirstOrDefault();

                    if (lstCriminalReports != null)
                    {
                        var lstCriminalCases = lstCriminalReports.Descendants("report-detail-header").ToList();
                        var lstRMCriminalCaseslst = lstCriminalReports.Descendants("report-detail-header").ToList();

                        if (lstCriminalCases != null && lstCriminalCases.Count > 0)
                        {
                            #region criminal cases
                            for (int iRow = 0; iRow < lstCriminalCases.Count; iRow++)
                            {
                                bool ISDismissed = false;
                                var CriminalCasesSingle = lstCriminalCases.ElementAt(iRow);

                                List<XElement> lstCharge = CriminalCasesSingle.Descendants("report-incident").ToList<XElement>();
                                List<XElement> lstRMCharge = CriminalCasesSingle.Descendants("report-incident").ToList<XElement>();
                                if (lstCharge.Count > 0)
                                {
                                    for (int iCharge = 0; iCharge < lstCharge.Count; iCharge++)
                                    {
                                        var ChargeSingle = lstCharge.ElementAt(iCharge);

                                        var Disposition = ChargeSingle.Descendants("disposition").FirstOrDefault();

                                        if (Disposition != null)
                                        {
                                            string DispositionValue = Disposition.Value.ToString();

                                            for (int r = 0; r < s.Count(); r++)
                                            {
                                                if (string.IsNullOrEmpty(s[r]) == false)
                                                {
                                                    if (DispositionValue.ToUpper().Contains(s[r]))
                                                    {
                                                        #region ticket 481
                                                        FilterId = "5";
                                                        var NewCollection = ObjBALCompany.GetFiltersAccordingFilterId(FilterId).FirstOrDefault();
                                                        if (NewCollection.Enabled == true)
                                                        {
                                                            List<string> NewKeyword = new List<string>();
                                                            if (NewCollection != null)
                                                            {
                                                                if (string.IsNullOrEmpty(Collection.Keywords) == false)
                                                                {
                                                                    var coll = NewCollection.Keywords.ToString();
                                                                    var ArrayCollection = coll.Split(',');
                                                                    if (ArrayCollection.Count() > 0)
                                                                    {
                                                                        for (int j = 0; j < ArrayCollection.Count(); j++)
                                                                        {

                                                                            if (string.IsNullOrEmpty(ArrayCollection[j]) == false)
                                                                            {
                                                                                NewKeyword.Add(ArrayCollection[j].Trim().ToUpper()); //Adding all keywords in List<string> s = new List<string>();
                                                                            }
                                                                        }

                                                                    }

                                                                }
                                                            }

                                                            for (int p = 0; p < NewKeyword.Count(); p++)
                                                            {
                                                                if (string.IsNullOrEmpty(NewKeyword[p]) == false)
                                                                {
                                                                    var fileterKeword = NewKeyword[p].ToUpper();
                                                                    var NonfileterKeword = "";

                                                                    for (int l = 0; l < s.Count(); l++)
                                                                    {
                                                                        if (string.IsNullOrEmpty(s[l]) == false)
                                                                        {
                                                                            if (s[l] == fileterKeword)//update ticket #145
                                                                            {
                                                                                NonfileterKeword = s[l].ToUpper();
                                                                            }
                                                                        }
                                                                    }

                                                                    if (DispositionValue.ToUpper().Contains(NewKeyword[p].ToUpper()))
                                                                    {
                                                                        if (Regex.Matches(DispositionValue.ToUpper(), fileterKeword).Count == 0)
                                                                        {
                                                                            ISDismissed = true;
                                                                            if (Collection.IsRemoveCharges == true)
                                                                            {
                                                                                lstRMCharge.Remove(ChargeSingle);
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            if (Regex.Matches(DispositionValue.ToUpper(), NonfileterKeword).Count != Regex.Matches(DispositionValue.ToUpper(), fileterKeword).Count)
                                                                            {

                                                                            }
                                                                            else
                                                                            {
                                                                                ISDismissed = true;
                                                                                if (Collection.IsRemoveCharges == true)
                                                                                {
                                                                                    lstRMCharge.Remove(ChargeSingle);
                                                                                }
                                                                            }
                                                                        }
                                                                    }

                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            ISDismissed = true;
                                                            if (Collection.IsRemoveCharges == true)
                                                            {
                                                                lstRMCharge.Remove(ChargeSingle);
                                                            }
                                                        }

                                                        #endregion
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }

                                #region Removing/Adding Charges to Current criminal Case
                                if (Collection.IsRemoveCharges == true)
                                {
                                    CriminalCasesSingle.Descendants("report-incident").Remove();

                                    foreach (XElement rm in lstRMCharge)
                                    {
                                        CriminalCasesSingle.Add(rm);
                                    }
                                }
                                #endregion

                                #region Logic to remove CriminalCase if all charges are removed

                                if (lstRMCharge.Count == 0 && ISDismissed == true)
                                {
                                    if (Collection.IsRemoveCharges == true)
                                    {
                                        lstRMCriminalCaseslst.Remove(CriminalCasesSingle);

                                    }
                                }
                                #endregion
                            }

                            #endregion criminal cases
                        }
                        if (Collection.IsRemoveCharges == true)
                        {
                            lstCriminalReports.RemoveAll();

                            foreach (XElement rm in lstRMCriminalCaseslst)
                            {
                                lstCriminalReports.Add(rm);
                            }
                        }
                    }
                }
            }
            catch
            {
                ObjXDocumentUppdated = XDocument.Parse(strResponseData);
            }
            ObjXDocumentUppdated = ObjXDocument;
            return ObjXDocumentUppdated;
        }

        public XDocument GetRemovedDismissedChargeXMLFilterforSCRDuringOrderPlace(string strResponseData)
        {
            XDocument ObjXDocumentUppdated = new XDocument();
            XDocument ObjXDocument = new XDocument();
            ObjXDocument = XDocument.Parse(strResponseData);
            BALCompany ObjBALCompany = new BALCompany();
            List<string> s = new List<string>();
            string FilterId = "3";
            var Collection = ObjBALCompany.GetFiltersAccordingFilterId(FilterId).FirstOrDefault();//Getting filter rules for Watch list FilterOR Dismissed Filter
            if (Collection != null)
            {
                if (string.IsNullOrEmpty(Collection.Keywords) == false)
                {
                    var coll = Collection.Keywords.ToString();
                    var ArrayCollection = coll.Split(',');
                    if (ArrayCollection.Count() > 0)
                    {
                        for (int j = 0; j < ArrayCollection.Count(); j++)
                        {

                            if (string.IsNullOrEmpty(ArrayCollection[j]) == false)
                            {
                                s.Add(ArrayCollection[j].Trim().ToUpper()); //Adding all keywords in List<string> s = new List<string>();
                            }
                        }

                    }

                }
            }

            try
            {
                var lstScreenings = ObjXDocument.Descendants("product").ToList();

                if (lstScreenings.Count > 0)
                {
                    var lstCriminalReports = lstScreenings.Descendants("results").FirstOrDefault();

                    if (lstCriminalReports != null)
                    {
                        var lstCriminalCases = lstCriminalReports.Descendants("result").ToList();
                        var lstRMCriminalCaseslst = lstCriminalReports.Descendants("result").ToList();

                        if (lstCriminalCases != null && lstCriminalCases.Count > 0)
                        {
                            #region criminal cases
                            for (int iRow = 0; iRow < lstCriminalCases.Count; iRow++)
                            {
                                bool ISDismissed = false;
                                var CriminalCasesSingle = lstCriminalCases.ElementAt(iRow);
                                var CriminalcountsSingle = CriminalCasesSingle.Descendants("counts").FirstOrDefault();
                                List<XElement> lstCharge = CriminalcountsSingle.Descendants("count").ToList<XElement>();
                                List<XElement> lstRMCharge = CriminalcountsSingle.Descendants("count").ToList<XElement>();
                                if (lstCharge.Count > 0)
                                {
                                    for (int iCharge = 0; iCharge < lstCharge.Count; iCharge++)
                                    {
                                        var ChargeSingle = lstCharge.ElementAt(iCharge);

                                        var Disposition = ChargeSingle.Descendants("disposition").FirstOrDefault();

                                        if (Disposition != null)
                                        {
                                            string DispositionValue = Disposition.Value.ToString();

                                            for (int r = 0; r < s.Count(); r++)
                                            {
                                                if (string.IsNullOrEmpty(s[r]) == false)
                                                {
                                                    if (DispositionValue.ToUpper().Contains(s[r]))
                                                    {
                                                        #region ticket 467

                                                        ISDismissed = true;
                                                        if (Collection.IsRemoveCharges == true)
                                                        {
                                                            lstRMCharge.Remove(ChargeSingle);
                                                        }

                                                        #endregion
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #region Removing/Adding Charges to Current criminal Case
                                if (Collection.IsRemoveCharges == true)
                                {
                                    CriminalcountsSingle.Descendants("count").Remove();

                                    foreach (XElement rm in lstRMCharge)
                                    {
                                        CriminalcountsSingle.Add(rm);
                                    }
                                }
                                #endregion

                                #region Logic to remove CriminalCase if all charges are removed

                                if (lstRMCharge.Count == 0 && ISDismissed == true)
                                {
                                    if (Collection.IsRemoveCharges == true)
                                    {
                                        lstRMCriminalCaseslst.Remove(CriminalCasesSingle);

                                    }
                                }
                                #endregion
                            }

                            #endregion criminal cases
                        }
                        if (Collection.IsRemoveCharges == true)
                        {
                            lstCriminalReports.RemoveAll();

                            foreach (XElement rm in lstRMCriminalCaseslst)
                            {
                                lstCriminalReports.Add(rm);
                            }
                        }
                    }
                }
            }
            catch
            {
                ObjXDocumentUppdated = XDocument.Parse(strResponseData);
            }


            ObjXDocumentUppdated = ObjXDocument;
            return ObjXDocumentUppdated;
        }

        public XDocument GetDismissedChargeResponseDuringOrderPlace(string XMLLiveRunner)
        {
            XDocument ObjXDocumentUppdatedLiveRunner = new XDocument();
            XDocument ObjXDocumentLiveRunner = new XDocument();
            string XML_LiveRunner = XMLFilteration.GetRequiredXml(XMLLiveRunner);
            ObjXDocumentLiveRunner = XDocument.Parse(XML_LiveRunner);
            BALCompany ObjBALCompany = new BALCompany();
            List<string> s = new List<string>();
            bool flagGuilty = false;
            string FilterId = "3";
            var Collection = ObjBALCompany.GetFiltersAccordingFilterId(FilterId).FirstOrDefault();//Getting filter rules for Watch list FilterOR Dismissed Filter
            if (Collection != null)
            {
                if (string.IsNullOrEmpty(Collection.Keywords) == false)
                {
                    var coll = Collection.Keywords.ToString();
                    var ArrayCollection = coll.Split(',');
                    if (ArrayCollection.Count() > 0)
                    {
                        for (int j = 0; j < ArrayCollection.Count(); j++)
                        {
                            if (string.IsNullOrEmpty(ArrayCollection[j]) == false)
                            {
                                s.Add(ArrayCollection[j].Trim().ToUpper()); //Adding all keywords in List<string> s = new List<string>();
                            }
                        }
                    }
                }
            }
            try
            {
                var lstScreenings = ObjXDocumentLiveRunner.Descendants("Screenings").ToList();
               // string strRegion = string.Empty;

                if (lstScreenings.Count > 0)
                {
                    var lstCriminalReports = lstScreenings.Descendants("CriminalReport").FirstOrDefault();

                    if (lstCriminalReports != null)
                    {
                        var lstCriminalCases = lstCriminalReports.Descendants("CriminalCase").ToList();
                        var lstRMCriminalCaseslst = lstCriminalReports.Descendants("CriminalCase").ToList();

                        if (lstCriminalCases != null && lstCriminalCases.Count > 0)
                        {
                            #region criminal cases
                            for (int iRow = 0; iRow < lstCriminalCases.Count; iRow++)
                            {
                                bool ISDismissed = false;
                                var CriminalCasesSingle = lstCriminalCases.ElementAt(iRow);

                                List<XElement> lstCharge = CriminalCasesSingle.Descendants("Charge").ToList<XElement>();
                                List<XElement> lstRMCharge = CriminalCasesSingle.Descendants("Charge").ToList<XElement>();
                                if (lstCharge.Count > 0)
                                {
                                    for (int iCharge = 0; iCharge < lstCharge.Count; iCharge++)
                                    {
                                        var ChargeSingle = lstCharge.ElementAt(iCharge);

                                        var Disposition = ChargeSingle.Descendants("Disposition").FirstOrDefault();

                                        if (Disposition != null)
                                        {
                                            string DispositionValue = Disposition.Value.ToString();
                                            for (int r = 0; r < s.Count(); r++)
                                            {
                                                if (string.IsNullOrEmpty(s[r]) == false)
                                                {
                                                    //Based on June 04, 2015 client meeting, Greg said, if <Disposion> contains GUILTY in free text than it will be displayed and also bypass the <Sentance> check.
                                                    if (!DispositionValue.ToUpper().Contains("NOT GUILTY"))
                                                    {
                                                        if (DispositionValue.ToUpper().Contains("GUILTY"))//Based on June 04, 2015 client meeting
                                                        {
                                                            flagGuilty = true;
                                                            break;
                                                        }
                                                        else
                                                        {
                                                            //Original Code
                                                            if (DispositionValue.ToUpper().Contains(s[r]))
                                                            {
                                                                #region ticket 467

                                                                ISDismissed = true;
                                                                if (Collection.IsRemoveCharges == true)
                                                                {
                                                                    lstRMCharge.Remove(ChargeSingle);
                                                                }

                                                                #endregion
                                                            }
                                                        }
                                                    }
                                                    else if (DispositionValue.ToUpper().Contains("NOT GUILTY BENCH TRIAL"))//Based  on INT-276 Oct 28, 2015 Greg agreed with this condition  
                                                    {
                                                        string strDispositionValue = "NOT GUILTY BENCH TRIAL";//exists in Disposition tag in response.
                                                        //Original Code
                                                        if (strDispositionValue == (s[r]))//if dismissed filteres contains "NOT GUILTY BENCH TRIAL"
                                                        {
                                                            ISDismissed = true;
                                                            if (Collection.IsRemoveCharges == true)
                                                            {
                                                                lstRMCharge.Remove(ChargeSingle);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        var ChargeTypeClassification = ChargeSingle.Descendants("ChargeTypeClassification").FirstOrDefault();
                                        //filteration for tag <ChargeTypeClassification>infraction</ChargeTypeClassification>.

                                        if (ChargeTypeClassification != null)
                                        {
                                            string ChargeTypeClassificationValue = ChargeTypeClassification.Value.ToString();

                                            for (int r = 0; r < s.Count(); r++)
                                            {
                                                if (string.IsNullOrEmpty(s[r]) == false)
                                                {
                                                    if (ChargeTypeClassificationValue.ToUpper().Contains(s[r]))
                                                    {
                                                        #region ticket 467

                                                        ISDismissed = true;
                                                        if (Collection.IsRemoveCharges == true)
                                                        {
                                                            lstRMCharge.Remove(ChargeSingle);
                                                        }

                                                        #endregion
                                                    }
                                                }
                                            }
                                        }
                                        var Textlst = ChargeSingle.Descendants("Text").ToList();
                                        //filteration for tag <Text>NOTE: 04-22-1999 - NOLLE PROS</Text>.

                                        if (Textlst.Count > 0)
                                        {
                                            for (int iText = 0; iText < Textlst.Count; iText++)
                                            {
                                                string text = Textlst.ElementAt(iText).Value;
                                                for (int r = 0; r < s.Count(); r++)
                                                {
                                                    if (string.IsNullOrEmpty(s[r]) == false)
                                                    {
                                                        if (text.ToUpper().Contains(s[r]))
                                                        {
                                                            #region ticket 467

                                                            ISDismissed = true;
                                                            if (Collection.IsRemoveCharges == true)
                                                            {
                                                                lstRMCharge.Remove(ChargeSingle);
                                                            }

                                                            #endregion
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        var Sentencelst = ChargeSingle.Descendants("Sentence").ToList();
                                        //filteration for tag <Sentence>EXPUNGED PROBATION: 1Y</Sentence>.

                                        if (Sentencelst.Count > 0)
                                        {
                                            if (!flagGuilty)//Based on June 04, 2015 client meeting
                                            {
                                                for (int iSentence = 0; iSentence < Sentencelst.Count; iSentence++)
                                                {
                                                    string sentence = Sentencelst.ElementAt(iSentence).Value;
                                                    for (int r = 0; r < s.Count(); r++)
                                                    {
                                                        if (string.IsNullOrEmpty(s[r]) == false)
                                                        {
                                                            if (sentence.ToUpper().Contains(s[r]))
                                                            {
                                                                #region ticket 467

                                                                ISDismissed = true;
                                                                if (Collection.IsRemoveCharges == true)
                                                                {
                                                                    lstRMCharge.Remove(ChargeSingle);
                                                                }

                                                                #endregion
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        var Commentlst = ChargeSingle.Descendants("Comment").ToList();
                                        //filteration for tag 1.<Comment>5-6MO.SENT.ACT&#39;D;DART;PTC 108 UNCONDITIONAL DISCHARGE</Comment>.
                                        //2. CASE NOTE: DISMISSED

                                        if (Commentlst.Count > 0)
                                        {
                                            for (int iComment = 0; iComment < Commentlst.Count; iComment++)
                                            {
                                                string comment = Commentlst.ElementAt(iComment).Value;
                                                for (int r = 0; r < s.Count(); r++)
                                                {
                                                    if (string.IsNullOrEmpty(s[r]) == false)
                                                    {
                                                        if (comment.ToUpper().Contains(s[r]))
                                                        {
                                                            #region ticket 467

                                                            ISDismissed = true;
                                                            if (Collection.IsRemoveCharges == true)
                                                            {
                                                                lstRMCharge.Remove(ChargeSingle);
                                                            }

                                                            #endregion
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                                else
                                {
                                    // lstRMCriminalCaseslst.Remove(CriminalCasesSingle);
                                }

                                #region Removing/Adding Charges to Current criminal Case
                                if (Collection.IsRemoveCharges == true)
                                {
                                    CriminalCasesSingle.Descendants("Charge").Remove();

                                    foreach (XElement rm in lstRMCharge)
                                    {
                                        CriminalCasesSingle.Add(rm);
                                    }
                                }
                                #endregion

                                #region Logic to remove CriminalCase if all charges are removed

                                if (lstRMCharge.Count == 0 && ISDismissed == true)
                                {
                                    if (Collection.IsRemoveCharges == true)
                                    {
                                        lstRMCriminalCaseslst.Remove(CriminalCasesSingle);
                                    }
                                }
                                #endregion
                            }

                            #endregion criminal cases
                        }
                        if (Collection.IsRemoveCharges == true)
                        {
                            lstCriminalReports.RemoveAll();

                            foreach (XElement rm in lstRMCriminalCaseslst)
                            {
                                lstCriminalReports.Add(rm);
                            }
                        }
                    }
                }
            }
            catch
            {
                ObjXDocumentUppdatedLiveRunner = XDocument.Parse(XML_LiveRunner);
            }
            ObjXDocumentUppdatedLiveRunner = ObjXDocumentLiveRunner;
            return ObjXDocumentUppdatedLiveRunner;
        }

        public XDocument GetFilteredResponseforNCR1DuringOrderPlace(string strResponseNCR1)
        {
            #region-----Old Code
            //XDocument ObjXDocumentUpdatedFilteredNCR1 = new XDocument();
            //XDocument ObjXDocumentFilteredNCR1 = new XDocument();
            //string XML_ResponseNCR1 = XMLFilteration.GetRequiredXml(strResponseNCR1);
            //ObjXDocumentFilteredNCR1 = XDocument.Parse(XML_ResponseNCR1);
            //BALCompany ObjBALCompany = new BALCompany();
            //List<string> s = new List<string>();
            //string FilterId = "3";
            //bool flagGuilty = false;
            //var Collection = ObjBALCompany.GetFiltersAccordingFilterId(FilterId).FirstOrDefault();  //Getting filter rules for Watch list FilterOR Dismissed Filter
            //if (Collection != null)
            //{
            //    if (string.IsNullOrEmpty(Collection.Keywords) == false)
            //    {
            //        var coll = Collection.Keywords.ToString();
            //        var ArrayCollection = coll.Split(',');
            //        if (ArrayCollection.Count() > 0)
            //        {
            //            for (int j = 0; j < ArrayCollection.Count(); j++)
            //            {
            //                if (string.IsNullOrEmpty(ArrayCollection[j]) == false)
            //                {
            //                    s.Add(ArrayCollection[j].Trim().ToUpper()); //Adding all keywords in List<string> s = new List<string>();
            //                }
            //            }
            //        }

            //    }
            //}
            //try
            //{
            //    var lstScreenings = ObjXDocumentFilteredNCR1.Descendants("Screenings").ToList();
            //   // string strRegion = string.Empty;

            //    if (lstScreenings.Count > 0)
            //    {
            //        var lstCriminalReports = lstScreenings.Descendants("CriminalReport").FirstOrDefault();

            //        if (lstCriminalReports != null)
            //        {
            //            var lstCriminalCases = lstCriminalReports.Descendants("CriminalCase").ToList();
            //            var lstRMCriminalCaseslst = lstCriminalReports.Descendants("CriminalCase").ToList();

            //            if (lstCriminalCases != null && lstCriminalCases.Count > 0)
            //            {
            //                #region criminal cases
            //                for (int iRow = 0; iRow < lstCriminalCases.Count; iRow++)
            //                {
            //                    bool ISDismissed = false;
            //                    var CriminalCasesSingle = lstCriminalCases.ElementAt(iRow);

            //                    #region CourtName
            //                    string strCourtName = string.Empty;
            //                    var CourtName = CriminalCasesSingle.Descendants("CourtName").FirstOrDefault();
            //                    if (CourtName != null)
            //                    {
            //                        strCourtName = CourtName.Value.ToString();
            //                    }
            //                    #endregion

            //                    List<XElement> lstCharge = CriminalCasesSingle.Descendants("Charge").ToList<XElement>();
            //                    List<XElement> lstRMCharge = CriminalCasesSingle.Descendants("Charge").ToList<XElement>();
            //                    if (lstCharge.Count > 0)
            //                    {
            //                        for (int iCharge = 0; iCharge < lstCharge.Count; iCharge++)
            //                        {
            //                            var ChargeSingle = lstCharge.ElementAt(iCharge);

            //                            var Disposition = ChargeSingle.Descendants("Disposition").FirstOrDefault();
            //                            //filteration for tags 1. <Disposition>OTHER DISMISSAL WITH PREJUDICE</Disposition>.
            //                            //2. <Disposition>DISMISSED W/O LEAVE</Disposition>.
            //                            //3. <Disposition>OTHER DISMISSAL WITHOUT PREJUDICE</Disposition>.
            //                            //4. <Disposition>NOLLE PROSEQUI</Disposition>.
            //                            //5. <Disposition>NOLLE PROSSE</Disposition>.
            //                            //6. DSMD-OTHER
            //                            //7. DSMD-DEF CONV OTHR
            //                            //8. DSMD-DEFR ADJUD
            //                            //9. NOT GUILTY
            //                            //10. DISMISSED BY JUDGE
            //                            //11. 208 - DISMISS
            //                            //12. 209 - DISMISS/STATE MOTION
            //                            //13. <Disposition>NOT CONVICTED</Disposition>
            //                            //14. COMPL DISMISSED BY CTY ATTY
            //                            //15. COURT DISMISSAL
            //                            //16. DISMISSAL W/O LEAVE
            //                            //17. ADMINISTRATIVELY DISMISSED
            //                            //18. DISM
            //                            //19. NOLP

            //                            if (Disposition != null)
            //                            {
            //                                string DispositionValue = Disposition.Value.ToString();
            //                                for (int r = 0; r < s.Count(); r++)
            //                                {
            //                                    if (string.IsNullOrEmpty(s[r]) == false)
            //                                    {
            //                                        //Based on June 04, 2015 client meeting, Greg said, if <Disposion> contains GUILTY in free text than it will be displayed and also bypass the <Sentance> check.
            //                                        if (!DispositionValue.ToUpper().Contains("NOT GUILTY"))
            //                                        {
            //                                            if (DispositionValue.ToUpper().Contains("GUILTY"))//Based on June 04, 2015 client meeting
            //                                            {
            //                                                flagGuilty = true;
            //                                                break;
            //                                            }
            //                                            else
            //                                            {
            //                                                //Original Code
            //                                                if (DispositionValue.ToUpper().Contains(s[r]))
            //                                                {
            //                                                    #region ticket 467

            //                                                    ISDismissed = true;
            //                                                    if (Collection.IsRemoveCharges == true)
            //                                                    {
            //                                                        lstRMCharge.Remove(ChargeSingle);
            //                                                    }

            //                                                    #endregion
            //                                                }
            //                                            }
            //                                        }
            //                                        else if (DispositionValue.ToUpper().Contains("NOT GUILTY BENCH TRIAL"))//Based  on INT-276 Oct 28, 2015 Greg agreed with this condition  
            //                                        {
            //                                            string strDispositionValue = "NOT GUILTY BENCH TRIAL";//exists in Disposition tag in response.
            //                                            //Original Code
            //                                            if (strDispositionValue == (s[r]))//if dismissed filteres contains "NOT GUILTY BENCH TRIAL"
            //                                            {
            //                                                ISDismissed = true;
            //                                                if (Collection.IsRemoveCharges == true)
            //                                                {
            //                                                    lstRMCharge.Remove(ChargeSingle);
            //                                                }
            //                                            }
            //                                        }
            //                                    }
            //                                }
            //                                if (DispositionValue.ToUpper().Contains("NOT CONVICTED"))
            //                                {
            //                                    if (strCourtName.ToUpper().Contains("SAN BERNARDINO") || strCourtName.ToUpper().Contains("RIVERSIDE"))//#975 NCR1 for San Bernardino filtering out "Not convicted" but we need an exception for San Bernardino and Riverside'
            //                                    {
            //                                        ISDismissed = false;
            //                                    }
            //                                    else
            //                                    {
            //                                        #region ticket 467

            //                                        ISDismissed = true;
            //                                        if (Collection.IsRemoveCharges == true)
            //                                        {
            //                                            lstRMCharge.Remove(ChargeSingle);
            //                                        }

            //                                        #endregion
            //                                    }
            //                                }
            //                            }

            //                            var ChargeTypeClassification = ChargeSingle.Descendants("ChargeTypeClassification").FirstOrDefault();
            //                            //filteration for tag <ChargeTypeClassification>infraction</ChargeTypeClassification>.

            //                            if (ChargeTypeClassification != null)
            //                            {
            //                                string ChargeTypeClassificationValue = ChargeTypeClassification.Value.ToString();

            //                                for (int r = 0; r < s.Count(); r++)
            //                                {
            //                                    if (string.IsNullOrEmpty(s[r]) == false)
            //                                    {
            //                                        if (ChargeTypeClassificationValue.ToUpper().Contains(s[r]))
            //                                        {
            //                                            #region ticket 467

            //                                            ISDismissed = true;
            //                                            if (Collection.IsRemoveCharges == true)
            //                                            {
            //                                                lstRMCharge.Remove(ChargeSingle);
            //                                            }

            //                                            #endregion
            //                                        }
            //                                    }
            //                                }
            //                            }

            //                            var Textlst = ChargeSingle.Descendants("Text").ToList();
            //                            //filteration for tag <Text>NOTE: 04-22-1999 - NOLLE PROS</Text>.

            //                            if (Textlst.Count > 0)
            //                            {
            //                                for (int iText = 0; iText < Textlst.Count; iText++)
            //                                {
            //                                    string text = Textlst.ElementAt(iText).Value;
            //                                    for (int r = 0; r < s.Count(); r++)
            //                                    {
            //                                        if (string.IsNullOrEmpty(s[r]) == false)
            //                                        {
            //                                            if (text.ToUpper().Contains(s[r]))
            //                                            {
            //                                                #region ticket 467

            //                                                ISDismissed = true;
            //                                                if (Collection.IsRemoveCharges == true)
            //                                                {
            //                                                    lstRMCharge.Remove(ChargeSingle);
            //                                                }

            //                                                #endregion
            //                                            }
            //                                        }
            //                                    }
            //                                }
            //                            }
            //                            var Sentencelst = ChargeSingle.Descendants("Sentence").ToList();
            //                            //filteration for tag <Sentence>EXPUNGED PROBATION: 1Y</Sentence>.

            //                            if (Sentencelst.Count > 0)
            //                            {
            //                                if (!flagGuilty)//Based on June 04, 2015 client meeting
            //                                {
            //                                    for (int iSentence = 0; iSentence < Sentencelst.Count; iSentence++)
            //                                    {
            //                                        string sentence = Sentencelst.ElementAt(iSentence).Value;
            //                                        for (int r = 0; r < s.Count(); r++)
            //                                        {
            //                                            if (string.IsNullOrEmpty(s[r]) == false)
            //                                            {
            //                                                if (sentence.ToUpper() == s[r].ToUpper())
            //                                                {
            //                                                    #region ticket 467

            //                                                    ISDismissed = true;
            //                                                    if (Collection.IsRemoveCharges == true)
            //                                                    {
            //                                                        lstRMCharge.Remove(ChargeSingle);
            //                                                    }

            //                                                    #endregion
            //                                                }
            //                                            }
            //                                        }
            //                                    }
            //                                }
            //                            }
            //                            var Commentlst = ChargeSingle.Descendants("Comment").ToList();
            //                            //filteration for tag 1.<Comment>5-6MO.SENT.ACT&#39;D;DART;PTC 108 UNCONDITIONAL DISCHARGE</Comment>.
            //                            //2. CASE NOTE: DISMISSED

            //                            if (Commentlst.Count > 0)
            //                            {
            //                                for (int iComment = 0; iComment < Commentlst.Count; iComment++)
            //                                {
            //                                    string comment = Commentlst.ElementAt(iComment).Value;
            //                                    for (int r = 0; r < s.Count(); r++)
            //                                    {
            //                                        if (string.IsNullOrEmpty(s[r]) == false)
            //                                        {
            //                                            if (comment.ToUpper().Contains(s[r]))
            //                                            {
            //                                                #region ticket 467

            //                                                ISDismissed = true;
            //                                                if (Collection.IsRemoveCharges == true)
            //                                                {
            //                                                    lstRMCharge.Remove(ChargeSingle);
            //                                                }

            //                                                #endregion
            //                                            }
            //                                        }
            //                                    }
            //                                }
            //                            }
            //                        }

            //                    }
            //                    else
            //                    {
            //                        // lstRMCriminalCaseslst.Remove(CriminalCasesSingle);
            //                    }

            //                    #region Removing/Adding Charges to Current criminal Case
            //                    if (Collection.IsRemoveCharges == true)
            //                    {
            //                        CriminalCasesSingle.Descendants("Charge").Remove();

            //                        foreach (XElement rm in lstRMCharge)
            //                        {
            //                            CriminalCasesSingle.Add(rm);
            //                        }
            //                    }
            //                    #endregion

            //                    #region Logic to remove CriminalCase if all charges are removed

            //                    if (lstRMCharge.Count == 0 && ISDismissed == true)
            //                    {

            //                        if (Collection.IsRemoveCharges == true)
            //                        {
            //                            if (Collection.IsRemoveCharges == true)
            //                            {
            //                                lstRMCriminalCaseslst.Remove(CriminalCasesSingle);
            //                            }
            //                        }
            //                    }
            //                    #endregion
            //                }

            //                #endregion criminal cases
            //            }
            //            if (Collection.IsRemoveCharges == true)
            //            {
            //                lstCriminalReports.RemoveAll();

            //                foreach (XElement rm in lstRMCriminalCaseslst)
            //                {
            //                    lstCriminalReports.Add(rm);
            //                }
            //            }
            //        }
            //    }
            //}
            //catch
            //{
            //    ObjXDocumentUpdatedFilteredNCR1 = XDocument.Parse(XML_ResponseNCR1);
            //}

            //ObjXDocumentUpdatedFilteredNCR1 = ObjXDocumentFilteredNCR1;
            //return ObjXDocumentUpdatedFilteredNCR1;
            #endregion

            XDocument ObjXDocumentUpdatedFilteredNCR1 = new XDocument();
            XDocument ObjXDocumentFilteredNCR1 = new XDocument();
            string XML_ResponseNCR1 = XMLFilteration.GetRequiredXml(strResponseNCR1);
            ObjXDocumentFilteredNCR1 = XDocument.Parse(XML_ResponseNCR1);
            BALCompany ObjBALCompany = new BALCompany();
            BALGlobalFilters objGlobleFilter = new BALGlobalFilters();
            List<string> s = new List<string>();
            //List<string> objList = new List<string>();
            //bool globalISDismissed = false;
            bool flagGuilty = false;
            bool IsFindDocketNumber = false;
            string FilterId = "3";
            var Collection = ObjBALCompany.GetFiltersAccordingFilterId(FilterId).FirstOrDefault();  //Getting filter rules for Watch list FilterOR Dismissed Filter
            var GlobleFilterData = objGlobleFilter.GetGlobleFiltersForRemoveRowCourtHouseData();//coded by ticket #105            
            if (Collection != null)
            {
                if (string.IsNullOrEmpty(Collection.Keywords) == false)
                {
                    var coll = Collection.Keywords.ToString();
                    var ArrayCollection = coll.Split(',');
                    if (ArrayCollection.Count() > 0)
                    {
                        for (int j = 0; j < ArrayCollection.Count(); j++)
                        {
                            if (string.IsNullOrEmpty(ArrayCollection[j]) == false)
                            {
                                s.Add(ArrayCollection[j].Trim().ToUpper()); //Adding all keywords in List<string> s = new List<string>();

                            }
                        }
                    }
                }
            }

            try
            {
                var lstScreenings = ObjXDocumentFilteredNCR1.Descendants("Screenings").ToList();
                //string strRegion = string.Empty;
                if (lstScreenings.Count > 0)
                {
                    var lstCriminalReports = lstScreenings.Descendants("CriminalReport").FirstOrDefault();
                    if (lstCriminalReports != null)
                    {
                        var lstCriminalCases = lstCriminalReports.Descendants("CriminalCase").ToList();
                        var lstRMCriminalCaseslst = lstCriminalReports.Descendants("CriminalCase").ToList();//coded by ticket #105
                        
                        if (lstCriminalCases != null && lstCriminalCases.Count > 0)
                        {
                            #region criminal cases
                            for (int iRow = 0; iRow < lstCriminalCases.Count; iRow++)
                            {
                                bool ISDismissed = false;
                                var CriminalCasesSingle = lstCriminalCases.ElementAt(iRow);
                                #region CourtName
                                string strCourtName = string.Empty;
                                var CourtName = CriminalCasesSingle.Descendants("CourtName").FirstOrDefault();
                                if (CourtName != null)
                                {
                                    strCourtName = CourtName.Value.ToString();
                                }
                                #endregion
                                List<XElement> lstCharge = CriminalCasesSingle.Descendants("Charge").ToList<XElement>();
                                List<XElement> lstRMCharge = CriminalCasesSingle.Descendants("Charge").ToList<XElement>();
                                if (lstCharge.Count > 0)
                                {
                                    for (int iCharge = 0; iCharge < lstCharge.Count; iCharge++)
                                    {
                                        var ChargeSingle = lstCharge.ElementAt(iCharge);
                                        var Disposition = ChargeSingle.Descendants("Disposition").FirstOrDefault();
                                        //filteration for tags 1. <Disposition>OTHER DISMISSAL WITH PREJUDICE</Disposition>.
                                        //2. <Disposition>DISMISSED W/O LEAVE</Disposition>.
                                        //3. <Disposition>OTHER DISMISSAL WITHOUT PREJUDICE</Disposition>.
                                        //4. <Disposition>NOLLE PROSEQUI</Disposition>.
                                        //5. <Disposition>NOLLE PROSSE</Disposition>.
                                        //6. DSMD-OTHER
                                        //7. DSMD-DEF CONV OTHR
                                        //8. DSMD-DEFR ADJUD
                                        //9. NOT GUILTY
                                        //10. DISMISSED BY JUDGE
                                        //11. 208 - DISMISS
                                        //12. 209 - DISMISS/STATE MOTION
                                        //13. <Disposition>NOT CONVICTED</Disposition>
                                        //14. COMPL DISMISSED BY CTY ATTY
                                        //15. COURT DISMISSAL
                                        //16. DISMISSAL W/O LEAVE
                                        //17. ADMINISTRATIVELY DISMISSED
                                        //18. DISM
                                        //19. NOLP
                                        if (Disposition != null)
                                        {
                                            string DispositionValue = Disposition.Value.ToString();
                                            for (int r = 0; r < s.Count(); r++)
                                            {
                                                if (string.IsNullOrEmpty(s[r]) == false)
                                                {
                                                    //Based on June 04, 2015 client meeting, Greg said, if <Disposion> contains GUILTY in free text than it will be displayed and also bypass the <Sentance> check.
                                                    if (!DispositionValue.ToUpper().Contains("NOT GUILTY"))
                                                    {
                                                        if (DispositionValue.ToUpper().Contains("GUILTY"))//Based on June 04, 2015 client meeting
                                                        {
                                                            flagGuilty = true;
                                                            break;
                                                        }
                                                        else
                                                        {
                                                            //Original Code
                                                            if (DispositionValue.ToUpper().Contains(s[r]))
                                                            {
                                                                #region ticket 467

                                                                ISDismissed = true;
                                                                if (Collection.IsRemoveCharges == true)
                                                                {
                                                                    lstRMCharge.Remove(ChargeSingle);
                                                                }

                                                                #endregion
                                                            }
                                                        }
                                                    }
                                                    else if (DispositionValue.ToUpper().Contains("NOT GUILTY BENCH TRIAL"))//Based  on INT-276 Oct 28, 2015 Greg agreed with this condition  
                                                    {
                                                        string strDispositionValue = "NOT GUILTY BENCH TRIAL";//exists in Disposition tag in response.
                                                        //Original Code
                                                        if (strDispositionValue == (s[r]))//if dismissed filteres contains "NOT GUILTY BENCH TRIAL"
                                                        {
                                                            ISDismissed = true;
                                                            if (Collection.IsRemoveCharges == true)
                                                            {
                                                                lstRMCharge.Remove(ChargeSingle);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            // if (DispositionValue.ToUpper().Contains("NOT CONVICTED"))
                                            if (DispositionValue.ToUpper().Trim() == "NOT CONVICTED".Trim())
                                            {
                                                if (strCourtName.ToUpper().Contains("SAN BERNARDINO") || strCourtName.ToUpper().Contains("RIVERSIDE"))//#975 NCR1 for San Bernardino filtering out "Not convicted" but we need an exception for San Bernardino and Riverside'
                                                {
                                                    ISDismissed = false;

                                                }
                                                else
                                                {
                                                    #region ticket 467

                                                    ISDismissed = true;
                                                    // globalISDismissed = true;
                                                    if (Collection.IsRemoveCharges == true)
                                                    {
                                                        lstRMCharge.Remove(ChargeSingle);
                                                    }

                                                    #endregion
                                                }
                                            }                                            
                                        }

                                        var ChargeTypeClassification = ChargeSingle.Descendants("ChargeTypeClassification").FirstOrDefault();
                                        //filteration for tag <ChargeTypeClassification>infraction</ChargeTypeClassification>.

                                        if (ChargeTypeClassification != null)
                                        {
                                            string ChargeTypeClassificationValue = ChargeTypeClassification.Value.ToString();

                                            for (int r = 0; r < s.Count(); r++)
                                            {
                                                if (string.IsNullOrEmpty(s[r]) == false)
                                                {
                                                    if (ChargeTypeClassificationValue.ToUpper().Contains(s[r]))
                                                    {
                                                        #region ticket 467

                                                        ISDismissed = true;
                                                        if (Collection.IsRemoveCharges == true)
                                                        {
                                                            lstRMCharge.Remove(ChargeSingle);
                                                        }
                                                        #endregion
                                                    }
                                                }
                                            }
                                        }

                                        var Textlst = ChargeSingle.Descendants("Text").ToList();
                                        //filteration for tag <Text>NOTE: 04-22-1999 - NOLLE PROS</Text>.

                                        if (Textlst.Count > 0)
                                        {
                                            for (int iText = 0; iText < Textlst.Count; iText++)
                                            {
                                                string text = Textlst.ElementAt(iText).Value;

                                                for (int r = 0; r < s.Count(); r++)
                                                {
                                                    if (string.IsNullOrEmpty(s[r]) == false)
                                                    {
                                                        if (text.ToUpper().Contains(s[r]))
                                                        {
                                                            ISDismissed = true;
                                                            if (Collection.IsRemoveCharges == true)
                                                            {
                                                                lstRMCharge.Remove(ChargeSingle);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        var Sentencelst = ChargeSingle.Descendants("Sentence").ToList();
                                        //filteration for tag <Sentence>EXPUNGED PROBATION: 1Y</Sentence>.

                                        if (Sentencelst.Count > 0)
                                        {
                                            if (!flagGuilty)//Based on June 04, 2015 client meeting
                                            {
                                                for (int iSentence = 0; iSentence < Sentencelst.Count; iSentence++)
                                                {
                                                    string sentence = Sentencelst.ElementAt(iSentence).Value;

                                                    for (int r = 0; r < s.Count(); r++)
                                                    {
                                                        if (string.IsNullOrEmpty(s[r]) == false)
                                                        {
                                                            //if (sentence.ToUpper().Contains(s[r]))
                                                            if (sentence.ToUpper() == s[r].ToUpper())
                                                            {
                                                                #region ticket 467

                                                                ISDismissed = true;
                                                                if (Collection.IsRemoveCharges == true)
                                                                {
                                                                    lstRMCharge.Remove(ChargeSingle);
                                                                }
                                                                #endregion
                                                            }
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                        var Commentlst = ChargeSingle.Descendants("Comment").ToList();
                                        //filteration for tag 1.<Comment>5-6MO.SENT.ACT&#39;D;DART;PTC 108 UNCONDITIONAL DISCHARGE</Comment>.
                                        //2. CASE NOTE: DISMISSED

                                        if (Commentlst.Count > 0)
                                        {
                                            for (int iComment = 0; iComment < Commentlst.Count; iComment++)
                                            {
                                                string comment = Commentlst.ElementAt(iComment).Value;

                                                for (int r = 0; r < s.Count(); r++)
                                                {
                                                    if (string.IsNullOrEmpty(s[r]) == false)
                                                    {
                                                        if (comment.ToUpper().Contains(s[r]))
                                                        {
                                                            //#region ticket 467
                                                            #region INT105 ******PLEA : NOT GUILTY
                                                            //To getting the splited by : both side value. 
                                                            string[] commentsArr = comment.Split(':');
                                                            if (commentsArr.Count() > 0)
                                                            {
                                                                //To check if Comments tag text have "PLEA : NOT GUILT"
                                                                //As per Greg comments --INT105 // change by int-131 for not guilty action
                                                                if (commentsArr[0].Contains("PLEA") && commentsArr[1].Contains(s[r]))
                                                                {
                                                                    //Skip removing process.
                                                                    // ISDismissed = false;
                                                                    continue;
                                                                }
                                                                else
                                                                {
                                                                    #region ticket 467
                                                                    //Removing the Charges, if any one found.
                                                                    ISDismissed = true;
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {
                                                                        lstRMCharge.Remove(ChargeSingle);
                                                                    }
                                                                    #endregion
                                                                }
                                                            }
                                                            #endregion

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    // lstRMCriminalCaseslst.Remove(CriminalCasesSingle);
                                }

                                #region Removing/Adding Charges to Current criminal Case
                                if (Collection.IsRemoveCharges == true && ISDismissed == true)
                                {
                                    CriminalCasesSingle.Descendants("Charge").Remove();
                                    foreach (XElement rm in lstRMCharge)
                                    {
                                        CriminalCasesSingle.Add(rm);
                                    }
                                }
                                #endregion

                                #region Logic to remove CriminalCase if all charges are removed

                                if (lstRMCharge.Count == 0 && ISDismissed == true)
                                {
                                    if (Collection.IsRemoveCharges == true)
                                    {
                                        lstRMCriminalCaseslst.Remove(CriminalCasesSingle);
                                    }
                                }
                                #endregion
                                #region Logic to remove CriminalCase if "FILTER REQUEST: Juvenile records out of Washington."
                                if (lstRMCriminalCaseslst.Count > 0)
                                {
                                    for (int i = 0; i < lstRMCriminalCaseslst.Count; i++)
                                    {
                                        XElement lstJ = lstRMCriminalCaseslst[i];

                                        var AdditionalItemsLst = lstJ.Descendants("AdditionalItems").ToList();//INT-143
                                        if (AdditionalItemsLst.Count() > 0 && AdditionalItemsLst != null)
                                        {
                                            foreach (XElement lstJInner in AdditionalItemsLst)
                                            {
                                                if (lstJInner.Descendants("Text").FirstOrDefault() != null)
                                                {
                                                    string sCaseType = lstJInner.Descendants("Text").FirstOrDefault().Value.Trim().ToUpper();
                                                    if (!string.IsNullOrEmpty(sCaseType.Split(':')[0]))
                                                    {
                                                        string sCType = sCaseType.Split(':')[0].Trim().ToUpper();
                                                        if (sCType == "CASE TYPE")
                                                        {
                                                            if (lstJInner.Descendants("Text").FirstOrDefault() != null)
                                                            {
                                                                string sJuvenile = lstJInner.Descendants("Text").FirstOrDefault().Value.Trim().ToUpper();
                                                                if (!string.IsNullOrEmpty(sJuvenile.Split(':')[1]))
                                                                {
                                                                    string sval = sJuvenile.Split(':')[1].Trim().ToUpper();
                                                                    if (sval == "JUVENILE" || sval == "J")
                                                                    {
                                                                        lstRMCriminalCaseslst.Remove(lstJ);//INT143
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }
                                #endregion
                                #region ticket #105
                                // block record through globle filter

                                if (GlobleFilterData != null && GlobleFilterData.Count > 0)
                                {
                                    string dataSourceValue = string.Empty;
                                    string devilaryAddressValue = string.Empty;
                                    string docketValue = string.Empty;
                                    string dataSource = string.Empty;
                                    string docketNumber = string.Empty;
                                    string deliveryAddress = string.Empty;


                                    var CourtNameValue = CriminalCasesSingle.Descendants("CourtName").FirstOrDefault();
                                    if (CourtNameValue != null)
                                    {
                                        dataSourceValue = CourtNameValue.Value.ToString();
                                    }
                                    docketValue = CriminalCasesSingle.Descendants("AgencyReference").Where(x => (string)x.Attribute("type") == "Docket").Select(x => (string)x.Element("IdValue").Value).FirstOrDefault();
                                    var DevileryAddress = CriminalCasesSingle.Descendants("DeliveryAddress").FirstOrDefault();
                                    var MunicipalityAddress = CriminalCasesSingle.Descendants("Municipality").FirstOrDefault();

                                    if (DevileryAddress != null)
                                    {
                                        devilaryAddressValue = DevileryAddress.Value.ToString();
                                    }
                                    else
                                    {
                                        if (MunicipalityAddress != null)
                                        {
                                            devilaryAddressValue = MunicipalityAddress.Value.ToString();
                                        }
                                        else
                                        {
                                            devilaryAddressValue = string.Empty;
                                        }
                                    }
                                    for (int objIndex = 0; objIndex < GlobleFilterData.Count; objIndex++)
                                    {
                                        dataSource = GlobleFilterData.ElementAt(objIndex).Source;
                                        docketNumber = GlobleFilterData.ElementAt(objIndex).DocketNumber;
                                        deliveryAddress = GlobleFilterData.ElementAt(objIndex).DeliveryAddress;

                                        if (!string.IsNullOrEmpty(dataSource) && !string.IsNullOrEmpty(docketNumber) && !string.IsNullOrEmpty(deliveryAddress))
                                        {
                                            if (!string.IsNullOrEmpty(dataSourceValue) && !string.IsNullOrEmpty(docketValue) && !string.IsNullOrEmpty(devilaryAddressValue))
                                            {
                                                if (docketNumber.ToUpper() == docketValue.ToUpper() && dataSource.ToUpper() == dataSourceValue.ToUpper() && deliveryAddress.ToUpper() == devilaryAddressValue.ToUpper())
                                                {
                                                    lstRMCriminalCaseslst.Remove(CriminalCasesSingle);
                                                    IsFindDocketNumber = true;
                                                }
                                            }
                                        }
                                        else if (!string.IsNullOrEmpty(dataSource) && !string.IsNullOrEmpty(docketNumber))
                                        {
                                            if (!string.IsNullOrEmpty(dataSourceValue) && !string.IsNullOrEmpty(docketValue))
                                            {
                                                if (docketNumber.ToUpper() == docketValue.ToUpper() && dataSource.ToUpper() == dataSourceValue.ToUpper())
                                                {
                                                    lstRMCriminalCaseslst.Remove(CriminalCasesSingle);
                                                    IsFindDocketNumber = true;
                                                }
                                            }

                                        }
                                        else if (!string.IsNullOrEmpty(dataSource) && !string.IsNullOrEmpty(deliveryAddress))
                                        {
                                            if (!string.IsNullOrEmpty(dataSourceValue) && !string.IsNullOrEmpty(devilaryAddressValue))
                                            {
                                                if (dataSource.ToUpper() == dataSourceValue.ToUpper() && deliveryAddress.ToUpper() == devilaryAddressValue.ToUpper())
                                                {
                                                    lstRMCriminalCaseslst.Remove(CriminalCasesSingle);
                                                    IsFindDocketNumber = true;
                                                }
                                            }

                                        }
                                        else if (!string.IsNullOrEmpty(docketNumber) && !string.IsNullOrEmpty(deliveryAddress))
                                        {
                                            if (!string.IsNullOrEmpty(docketValue) && !string.IsNullOrEmpty(devilaryAddressValue))
                                            {
                                                if (docketNumber.ToUpper() == docketValue.ToUpper() && deliveryAddress.ToUpper() == devilaryAddressValue.ToUpper())
                                                {
                                                    lstRMCriminalCaseslst.Remove(CriminalCasesSingle);
                                                    IsFindDocketNumber = true;
                                                }
                                            }
                                        }
                                        else if (!string.IsNullOrEmpty(dataSource))
                                        {
                                            if (!string.IsNullOrEmpty(dataSourceValue))
                                            {
                                                if (dataSource.ToUpper() == dataSourceValue.ToUpper())
                                                {
                                                    lstRMCriminalCaseslst.Remove(CriminalCasesSingle);
                                                    IsFindDocketNumber = true;
                                                }
                                            }
                                        }
                                        else if (!string.IsNullOrEmpty(docketNumber))
                                        {
                                            if (!string.IsNullOrEmpty(docketValue))
                                            {
                                                if (docketNumber.ToUpper() == docketValue.ToUpper())
                                                {
                                                    lstRMCriminalCaseslst.Remove(CriminalCasesSingle);
                                                    IsFindDocketNumber = true;
                                                }
                                            }
                                        }
                                        else if (!string.IsNullOrEmpty(deliveryAddress))
                                        {
                                            if (!string.IsNullOrEmpty(devilaryAddressValue))
                                            {
                                                if (deliveryAddress.ToUpper() == devilaryAddressValue.ToUpper())
                                                {
                                                    lstRMCriminalCaseslst.Remove(CriminalCasesSingle);
                                                    IsFindDocketNumber = true;
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                            #endregion criminal cases
                        }
                        // change for int -131

                        if (Collection.IsRemoveCharges == true)

                        //&& globalISDismissed == true change by sir int=143 on 9 march 2015
                        {
                            lstCriminalReports.RemoveAll();
                            foreach (XElement rm in lstRMCriminalCaseslst)
                            {
                                lstCriminalReports.Add(rm);
                            }
                        }

                        if (IsFindDocketNumber)
                        {
                            lstCriminalReports.RemoveAll();
                            foreach (XElement rm in lstRMCriminalCaseslst)
                            {
                                lstCriminalReports.Add(rm);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ObjXDocumentUpdatedFilteredNCR1 = XDocument.Parse(XML_ResponseNCR1);                
            }
            ObjXDocumentUpdatedFilteredNCR1 = ObjXDocumentFilteredNCR1;
            return ObjXDocumentUpdatedFilteredNCR1;

        }

        public string HideSevenYearDuringOrderPlace(string ResponseData, string ProductCode, DateTime orderdt, int? pkOrderDetailId, bool IsPendingReport)
        {
            string FinalResponse = "";
            XDocument UpdatedXML = GetHidedResponseDuringOrderPlace(ResponseData, ProductCode, orderdt, pkOrderDetailId, IsPendingReport);
            string UpdatedResponse = Convert.ToString(UpdatedXML);
            if (ProductCode == "NCR1" || ProductCode == "NCR+" || ProductCode == "RCX")
            {
                int TrimedUpTo = ResponseData.IndexOf("<BackgroundReportPackage>");
                StringBuilder CompleteResponse = new StringBuilder();
                string XMLInitials = ResponseData.Substring(0, TrimedUpTo);
                CompleteResponse.Append(XMLInitials);
                CompleteResponse.Append(UpdatedResponse);
                CompleteResponse.Append("</BackgroundReports>");
                FinalResponse = Convert.ToString(CompleteResponse);
            }
            else
            {
                FinalResponse = UpdatedResponse;
            }
            return FinalResponse;
        }

        public XDocument GetHidedResponseDuringOrderPlace(string XMLresp, string ProductCode, DateTime orderdt, int? pkOrderDetailId, bool IsPendingReport)
        {
            string Filter_Id = "1";
            List<string> s = new List<string>();
            BALCompany ObjBALCompany = new BALCompany();
            List<tblState> ObjtblState = new List<tblState>();
            bool IsEmergeReviewStatus = false;
            var Collection = ObjBALCompany.GetFiltersAccordingFilterId(Filter_Id).FirstOrDefault();//Getting filter rules 7 Year Filter.
            bool flagforLiveRunnerCounty = false;//used for LOS ANGELES county --INT-287 as per Greg comments on Nov 27, 2015.
           // bool MaxDate = false;
            //bool MinDate = false;
            string Year = Collection.Year.ToString();
            string MaxNewYear = Collection.TimeFrameFrom.ToString();
            string MinNewYear = Collection.TimeFrameIn.ToString();
            int CheckCase = 0;
            bool additionalFilterforNewYork = false;
            int Year_new = 0;
            DateTime? Max_new_Year = null;
            DateTime? Min_new_Year = null;
            int? tempflagYear = null;
            if (string.IsNullOrEmpty(Year) == false) { Year_new = int.Parse(Year); }
            if (string.IsNullOrEmpty(MaxNewYear) == false) { Max_new_Year = DateTime.Parse(MaxNewYear); }
            if (string.IsNullOrEmpty(MinNewYear) == false) { Min_new_Year = DateTime.Parse(MinNewYear); }
            if (Collection.Year != null) { Year_new = int.Parse(Collection.Year.ToString()); tempflagYear = 1; }

            //Case: 1 When Year is present but MaxYear and MinYear both null
            if (tempflagYear != null && Max_new_Year == null && Min_new_Year == null)
            {
                CheckCase = 1;

            }

            //Case: 2 When Year is present but One is Present from MaxYear and MinYear 

            if (tempflagYear != null && Max_new_Year != null && Min_new_Year == null)
            {
                CheckCase = 2;

            }

            //Case: 3 When Year, MaxYear and MinYear all are present

            if (tempflagYear != null && Max_new_Year != null && Min_new_Year != null)
            {
                CheckCase = 3;

            }

            if (Collection != null)
            {
                if (string.IsNullOrEmpty(Collection.Keywords) == false)
                {
                    var coll = Collection.Keywords.ToString();
                    var ArrayCollection = coll.Split(',');
                    if (ArrayCollection.Count() > 0)
                    {
                        for (int j = 0; j < ArrayCollection.Count(); j++)
                        {
                            s.Add(ArrayCollection[j].Trim().ToUpper().Replace(" ", ""));//Adding all keywords in List<string> s = new List<string>();

                        }
                    }
                }
            }

            var ListState = new List<StateName>();
            if (Collection != null)
            {
                #region---Used to 7 year auto filters from company edit page.
                Collection.IsEnableForAllState = new BALEmergeReview().get7yearAutoFiltersFromCompanyEditPage(Convert.ToInt32(HttpContext.Current.Session["pkCompanyId"]));
                #endregion
                if (string.IsNullOrEmpty(Collection.StateId) == false && Collection.IsEnableForAllState == false)
                {
                    string FilterId = Collection.StateId;
                    List<tblState> State = new List<tblState>();
                    var Coll = FilterId.Split(',');
                    if (Coll.Length > 0)
                    {
                        for (int r = 0; r < Coll.Length; r++)
                        {
                            if (string.IsNullOrEmpty(Coll[r]) == false)
                            {
                                State.Add(new tblState { pkStateId = int.Parse(Coll[r]) });
                            }
                        }
                    }
                    ListState = ObjBALCompany.GetStateName(State); //Getting Collection of State According to the 7 Year Filter rule. 
                }
                else
                {
                    ListState = ObjBALCompany.GetStateNameForEnable7YearFilter(); // for ticket #85 if 7 Year filter enable, get all state
                }
            }
            if (ListState.Count > 0)
            {
                for (int d = 0; d < ListState.Count(); d++)
                {
                    ObjtblState.Add(new tblState { StateCode = ListState.ElementAt(d).StateCode, StateName = ListState.ElementAt(d).State_Name, pkStateId = ListState.ElementAt(0).pkStateId });
                }
            }
            List<string> StateCodeColl = new List<string>();
            List<string> StateNameColl = new List<string>();
            List<tblState> ObjStateColl = new List<tblState>();
            List<tblCounty> ObjCountyColl = new List<tblCounty>();

            XDocument ObjXDocumentupdated = new XDocument();
            XDocument ObjXDocument = new XDocument();

            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    StateCodeColl = ObjtblState.Select(x => x.StateCode).ToList(); //Assigning StateCode According to the 7 Year Filter rule. 
                    StateNameColl = ObjtblState.Select(x => x.StateName).ToList(); //Assigning StateName According to the 7 Year Filter rule. 
                    ObjStateColl = ObjtblState.ToList<tblState>(); //Assigning StateName According to the 7 Year Filter rule. 


                    ObjCountyColl = DX.tblCounties.ToList<tblCounty>();
                    string XML_Value = BALEmergeReview.GetRequiredXmlConditional(XMLresp, ProductCode.Trim().ToUpper());
                    ObjXDocument = XDocument.Parse(XML_Value);
                    if (ProductCode.Trim().ToUpper() == "NCR1" || ProductCode.Trim().ToUpper() == "NCR+" || ProductCode.Trim().ToUpper() == "RCX")
                    {
                        #region NCR1, NCR+, RCX
                        List<XElement> xelementCriminalReport = ObjXDocument.Descendants("CriminalReport").ToList();
                        if (xelementCriminalReport != null)
                        {
                            if (xelementCriminalReport.Count > 0)
                            {
                                List<XElement> CriminalCaseColl, CriminalCaseCollRm;
                                CriminalCaseColl = ObjXDocument.Descendants("CriminalCase").ToList();
                                CriminalCaseCollRm = ObjXDocument.Descendants("CriminalCase").ToList();

                                XElement CriminalCaseSingle = null;
                                if (CriminalCaseColl != null && CriminalCaseColl.Count > 0)
                                {

                                    for (int i = 0; i < CriminalCaseColl.Count; i++)
                                    {
                                        bool IsCourtTag = false;
                                        CriminalCaseSingle = CriminalCaseColl.ElementAt(i);

                                        List<XElement> RegionColl;
                                        RegionColl = CriminalCaseSingle.Descendants("CourtName").ToList<XElement>();
                                        var CaseFileDate = CriminalCaseSingle.Descendants("CaseFileDate").FirstOrDefault();
                                        IsCourtTag = true;

                                        if (RegionColl == null || RegionColl.Count == 0)
                                        {
                                            RegionColl = CriminalCaseSingle.Descendants("Region").ToList<XElement>();
                                            IsCourtTag = false;
                                        }
                                        if (RegionColl != null && RegionColl.Count > 0)
                                        {
                                            bool isStateValid = false;
                                            string StateCode = "";
                                            string CourtName = "";
                                            if (IsCourtTag)
                                            {
                                                CourtName = RegionColl.ElementAt(0).Value;
                                                //As per the Greg comments  in JIRA on Nov 27, 2015
                                                //If los angeles county is comming and no charge found in the response.
                                                //Then records are not being filtered out from the report.
                                                if (CourtName.ToLower().Contains("los angeles"))
                                                {
                                                    string str = string.Empty;
                                                    int flag = 0;
                                                    DateTime dt;
                                                    if (CaseFileDate != null)
                                                    {
                                                        str = CaseFileDate.Value;
                                                        str = (str != "" && str.Length > 9) ? str.Substring(0, 10) : DateTime.Now.ToShortDateString();
                                                        DateTime.TryParse(str, out dt);
                                                        int UpdateYear = Year_new * (-1);
                                                        DateTime NewDate = DateTime.Now.AddYears(UpdateYear);
                                                        if (CheckCase == 1)
                                                        {
                                                            if (dt < DateTime.Now.AddYears(UpdateYear))
                                                            {
                                                                #region #800: 7 YEAR STATES, Comment #31
                                                                if (flag == 2)
                                                                {
                                                                    IsEmergeReviewStatus = true;
                                                                }

                                                                #endregion
                                                                if (flag != 2)
                                                                {
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {

                                                                        flagforLiveRunnerCounty = false;
                                                                    }
                                                                    else
                                                                    {
                                                                        flagforLiveRunnerCounty = true;//Only for Live runner.
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                flagforLiveRunnerCounty = true;//Only for Live runner.
                                                            }
                                                        }
                                                        if (CheckCase == 2)
                                                        {
                                                            if (Max_new_Year <= NewDate)
                                                            {

                                                                if (dt < Max_new_Year)
                                                                {
                                                                    #region #800: 7 YEAR STATES, Comment #31
                                                                    if (flag == 2)
                                                                    {
                                                                        IsEmergeReviewStatus = true;
                                                                    }

                                                                    #endregion
                                                                    if (flag != 2)
                                                                    {
                                                                        if (Collection.IsRemoveCharges == true)
                                                                        {

                                                                            flagforLiveRunnerCounty = false;
                                                                        }
                                                                        else
                                                                        {
                                                                            flagforLiveRunnerCounty = true;//Only for Live runner.
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (dt < DateTime.Now.AddYears(UpdateYear))
                                                                {
                                                                    #region #800: 7 YEAR STATES, Comment #31
                                                                    if (flag == 2)
                                                                    {
                                                                        IsEmergeReviewStatus = true;
                                                                    }


                                                                    #endregion
                                                                    if (flag != 2)
                                                                    {
                                                                        if (Collection.IsRemoveCharges == true)
                                                                        {

                                                                            flagforLiveRunnerCounty = false;
                                                                        }
                                                                        else
                                                                        {
                                                                            flagforLiveRunnerCounty = true;//Only for Live runner.
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    flagforLiveRunnerCounty = true;//Only for Live runner.
                                                                }
                                                            }
                                                        }
                                                        if (CheckCase == 3)
                                                        {
                                                            if (Min_new_Year <= dt && dt <= Max_new_Year)
                                                            {
                                                                #region #800: 7 YEAR STATES, Comment #31
                                                                if (flag == 2)
                                                                {
                                                                    IsEmergeReviewStatus = true;
                                                                }

                                                                #endregion
                                                                if (flag != 2)
                                                                {
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {

                                                                        flagforLiveRunnerCounty = false;
                                                                    }
                                                                    else
                                                                    {
                                                                        flagforLiveRunnerCounty = true;//Only for Live runner.
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                flagforLiveRunnerCounty = true;//Only for Live runner.
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        flagforLiveRunnerCounty = true;//Only for Live runner.
                                                    }
                                                }
                                                if (CourtName.Contains("-"))
                                                {
                                                    string[] Getstatename = CourtName.Split('-');
                                                    //INT-256 as per Greg comments on JIRA We need to create the following logic: 
                                                    //IF record SOURCE = New York (any record in New York).
                                                    //THEN it should consider the "ADDITIONALEVENTS" tag for the 7-year filter.
                                                    //It should look at the most recent date to calculate.
                                                    if (Getstatename[1].Trim().ToUpper() == "NEW YORK")
                                                    {
                                                        isStateValid = true;//It should me implicitly filter state as per additional item date.
                                                        additionalFilterforNewYork = true;
                                                    }
                                                    else
                                                    {
                                                        isStateValid = (ObjStateColl.Where(d => d.StateName.ToLower() == Getstatename[1].Trim().ToLower()).Count() > 0) ? true : false;
                                                        additionalFilterforNewYork = false;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                foreach (XElement Region in RegionColl)
                                                {
                                                    StateCode = Region.Value;
                                                    isStateValid = (StateCodeColl.Where(d => d.ToLower() == StateCode.ToLower()).Count() > 0) ? true : false;
                                                    if (isStateValid)
                                                        break;
                                                }
                                            }
                                            if (isStateValid)
                                            {
                                                List<XElement> ChargeColl, ChargeCollRm, AdditionalItemsColl;
                                                ChargeColl = CriminalCaseSingle.Descendants("Charge").ToList<XElement>();
                                                ChargeCollRm = CriminalCaseSingle.Descendants("Charge").ToList<XElement>();
                                                AdditionalItemsColl = CriminalCaseSingle.Descendants("AdditionalItems").ToList<XElement>();

                                                XElement ChargeSingle = null;
                                                if (ChargeColl != null && ChargeColl.Count > 0)
                                                {
                                                    foreach (XElement xe in ChargeColl)
                                                    {
                                                        ChargeSingle = xe;
                                                        string strDisposition = string.Empty;
                                                        XElement OffenseDate = ChargeSingle.Descendants("OffenseDate").FirstOrDefault();
                                                        XElement DispositionDate = ChargeSingle.Descendants("DispositionDate").FirstOrDefault();
                                                        XElement DispDate = ChargeSingle.Descendants("DispDate").FirstOrDefault();
                                                        XElement Disposition = ChargeSingle.Descendants("Disposition").FirstOrDefault();
                                                        XElement SentenceDate = ChargeSingle.Descendants("Sentence").FirstOrDefault();

                                                        int flag = 1;
                                                        using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
                                                        {
                                                            if (Disposition != null)
                                                            {
                                                                string Despostioncheck = string.Empty;
                                                                Despostioncheck = Disposition.Value;
                                                                if (!string.IsNullOrEmpty(Despostioncheck))
                                                                {
                                                                    string Desposition = Despostioncheck.ToLower();
                                                                    while (Desposition.IndexOf(" ") >= 0)
                                                                    {
                                                                        Desposition = Desposition.Replace(" ", "");
                                                                    }
                                                                    for (int r = 0; r < s.Count(); r++)
                                                                    {
                                                                        if (string.IsNullOrEmpty(s[r]) == false)
                                                                        {
                                                                            if (Desposition.ToLower().Contains(s[r].ToLower()))
                                                                            {
                                                                                flag = 2;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    flag = 2;
                                                                }
                                                            }
                                                        }

                                                        string str = ""; DateTime dt;
                                                        if (OffenseDate != null)
                                                        {
                                                            str = OffenseDate.Value;
                                                        }
                                                        else if (DispositionDate != null)
                                                        {
                                                            str = DispositionDate.Value;
                                                        }
                                                        else if (CaseFileDate != null)
                                                        {
                                                            str = CaseFileDate.Value;
                                                        }
                                                        else if (DispDate != null)
                                                        {
                                                            str = DispDate.Value;//20070625
                                                            if (str != "" && str.Length > 7)
                                                            {
                                                                str = str.Substring(0, 4) + "/" + str.Substring(4, 2) + "/" + str.Substring(6, 2);
                                                            }
                                                            else
                                                            {
                                                                str = DateTime.Now.ToShortDateString();
                                                            }
                                                        }
                                                        else if (SentenceDate != null)
                                                        { // changes regarding ticket 418
                                                            string SentenceDatevalue = SentenceDate.Value;
                                                            if (SentenceDatevalue.Contains("SENTENCE DATE"))
                                                            {
                                                                string[] splitdatevalue = SentenceDatevalue.Split(':');
                                                                var Completedatevalue = splitdatevalue[1];
                                                                str = Completedatevalue.Substring(0, 11).Replace("/", "");
                                                                string str1 = str.Substring(5, 4);
                                                                string str2 = str.Substring(1, 2);
                                                                string str3 = str.Substring(3, 2);
                                                                str = str1 + "/" + str2 + "/" + str3;

                                                            }
                                                        }
                                                        str = (str != "" && str.Length > 9) ? str.Substring(0, 10) : DateTime.Now.ToShortDateString();
                                                        DateTime.TryParse(str, out dt);


                                                        int UpdateYear = Year_new * (-1);
                                                        DateTime NewDate = DateTime.Now.AddYears(UpdateYear);


                                                        //if (Max_new_Year != null)
                                                        //{
                                                        //    if (Max_new_Year >= NewDate) { MaxDate = true; }
                                                        //    else { MaxDate = false; }
                                                        //}
                                                        //else { MaxDate = true; }
                                                        //if (Min_new_Year != null)
                                                        //{
                                                        //    if (Min_new_Year <= NewDate) { MinDate = true; }
                                                        //    else { MinDate = false; }
                                                        //}
                                                        //else { MinDate = true; }
                                                        if (CheckCase == 1)
                                                        {


                                                            if (dt < DateTime.Now.AddYears(UpdateYear))
                                                            {
                                                                #region #800: 7 YEAR STATES, Comment #31
                                                                if (flag == 2)
                                                                {
                                                                    IsEmergeReviewStatus = true;
                                                                }
                                                                #endregion
                                                                if (flag != 2)
                                                                {
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {

                                                                        ChargeCollRm.Remove(ChargeSingle);
                                                                    }
                                                                }
                                                            }

                                                        }

                                                        if (CheckCase == 2)
                                                        {
                                                            if (Max_new_Year <= NewDate)
                                                            {

                                                                if (dt < Max_new_Year)
                                                                {
                                                                    #region #800: 7 YEAR STATES, Comment #31
                                                                    if (flag == 2)
                                                                    {
                                                                        IsEmergeReviewStatus = true;
                                                                    }
                                                                    #endregion
                                                                    if (flag != 2)
                                                                    {
                                                                        if (Collection.IsRemoveCharges == true)
                                                                        {

                                                                            ChargeCollRm.Remove(ChargeSingle);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (dt < DateTime.Now.AddYears(UpdateYear))
                                                                {
                                                                    #region #800: 7 YEAR STATES, Comment #31
                                                                    if (flag == 2)
                                                                    {
                                                                        IsEmergeReviewStatus = true;
                                                                    }

                                                                    #endregion
                                                                    if (flag != 2)
                                                                    {
                                                                        if (Collection.IsRemoveCharges == true)
                                                                        {

                                                                            ChargeCollRm.Remove(ChargeSingle);
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                        }



                                                        if (CheckCase == 3)
                                                        {
                                                            if (Min_new_Year <= dt && dt <= Max_new_Year)
                                                            {
                                                                #region #800: 7 YEAR STATES, Comment #31
                                                                if (flag == 2)
                                                                {
                                                                    IsEmergeReviewStatus = true;
                                                                }

                                                                #endregion
                                                                if (flag != 2)
                                                                {
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {

                                                                        ChargeCollRm.Remove(ChargeSingle);
                                                                    }
                                                                }
                                                            }
                                                        }

                                                    }
                                                }
                                                else
                                                {
                                                    //INT-287 as per Greg comments in JIRA.
                                                    //If any county belongs to Los angeles and no charges are cmoing in the response.
                                                    //then response will be displayed on view report page.
                                                    if (ProductCode == "RCX")
                                                    {
                                                        if (!flagforLiveRunnerCounty)//If Los angeles county does not exists.
                                                        {
                                                            CriminalCaseCollRm.Remove(CriminalCaseSingle);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        CriminalCaseCollRm.Remove(CriminalCaseSingle);
                                                    }
                                                }
                                                if (AdditionalItemsColl != null && AdditionalItemsColl.Count() > 0)//INT256 New implementation.
                                                {
                                                    //If <CourtName>DOC - NEW YORK</CourtName>
                                                    if (additionalFilterforNewYork)
                                                    {
                                                        foreach (XElement xe in AdditionalItemsColl)
                                                        {
                                                            ChargeSingle = xe;
                                                            DateTime dtNewYork;
                                                            string strDisposition = string.Empty;
                                                            //to get the Additional items date field entries.
                                                            XElement anyDate = ChargeSingle.Descendants("EffectiveDate").Descendants("StartDate").Descendants("AnyDate").FirstOrDefault();
                                                            if (anyDate != null)
                                                            {
                                                                string[] splitdatevalueNewYork = anyDate.Value.Split('T');//To split date part from the string like <AnyDate>2000-03-18T00:00:00</AnyDate>. 
                                                                var CompletedatevalueNewYork = splitdatevalueNewYork[0];//get only date part from the string array.
                                                                DateTime.TryParse(splitdatevalueNewYork[0], out dtNewYork);//To verify the date format.
                                                                int UpdateYear = Year_new * (-1);//To creating the less than 7 year date from the current date.
                                                                if (dtNewYork < DateTime.Now.AddYears(UpdateYear))//to check date if coming date less than 7 year.
                                                                {
                                                                    //7 YEAR STATES, Comment
                                                                    CriminalCaseCollRm.Remove(CriminalCaseSingle);//If coming date is less than 7 year than it will remove Single CriminalCaseSingle section. 
                                                                }
                                                            }
                                                        }
                                                    }
                                                }


                                                if (Collection.IsRemoveCharges == true)
                                                {
                                                    #region Removing/Adding Charges to Current criminal Case

                                                    CriminalCaseSingle.Descendants("Charge").Remove();
                                                    //CriminalCaseSingle.RemoveAll();
                                                    foreach (XElement rm in ChargeCollRm)
                                                    {
                                                        CriminalCaseSingle.Add(rm);
                                                    }
                                                    #endregion

                                                    #region Logic to remove CriminalCase if all chares are removed
                                                    if (ChargeCollRm.Count == 0)
                                                    {
                                                        //INT-287 as per Greg comments in JIRA.
                                                        //If any county belongs to Los angeles and no charges are cmoing in the response.
                                                        //then response will be displayed on view report page.
                                                        if (ProductCode == "RCX") //If Los angeles county does not exists.
                                                        {
                                                            if (!flagforLiveRunnerCounty)
                                                            {
                                                                CriminalCaseCollRm.Remove(CriminalCaseSingle);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            CriminalCaseCollRm.Remove(CriminalCaseSingle);
                                                        }
                                                    }
                                                    #endregion
                                                }
                                            }
                                        }
                                    }
                                }
                                /// Here i have removed all empty CriminalCases
                                if (Collection.IsRemoveCharges == true)
                                {  ///
                                    xelementCriminalReport.First().RemoveAll();
                                    foreach (XElement rm in CriminalCaseCollRm)
                                    {
                                        xelementCriminalReport.First().Add(rm);
                                    }
                                }
                            }
                        }
                        #endregion
                    }
                    else if (ProductCode.Trim().ToUpper() == "SCR")
                    {
                        #region SCR
                        ///
                        /// In SCR we have 2 type of tags.<reslut>/<CriminalCase>
                        /// First I have applied check for <CriminalCase>
                        ///
                        List<XElement> xelementCriminalReport = ObjXDocument.Descendants("CriminalReport").ToList();
                        List<XElement> xElementresults = ObjXDocument.Descendants("results").ToList<XElement>();
                        if (xelementCriminalReport != null)
                        {
                            if (xelementCriminalReport.Count > 0)
                            {
                            }
                            ///
                            /// If Response contain result tag
                            ///
                            else if (xElementresults != null)
                            {
                                List<XElement> resultColl, resultCollRm;
                                resultColl = ObjXDocument.Descendants("result").ToList();
                                resultCollRm = ObjXDocument.Descendants("result").ToList();

                                XElement CriminalCaseSingle = null;
                                if (resultColl != null && resultColl.Count > 0)
                                {
                                    for (int i = 0; i < resultColl.Count; i++)
                                    {
                                        CriminalCaseSingle = resultColl.ElementAt(i);
                                        List<XElement> RegionColl = CriminalCaseSingle.Descendants("state").ToList<XElement>();
                                        if (RegionColl != null && RegionColl.Count > 0)
                                        {
                                            bool isStateValid = false;
                                            string StateName = "";
                                            foreach (XElement Region in RegionColl)
                                            {
                                                StateName = Region.Value;
                                                isStateValid = (StateNameColl.Where(d => d.ToLower() == StateName.ToLower()).Count() > 0) ? true : false;
                                                if (isStateValid)
                                                    break;
                                            }
                                            if (isStateValid)
                                            {
                                                string strDisposition = string.Empty;
                                                int flag = 1;
                                                var dispositionlst = CriminalCaseSingle.Descendants("disposition").ToList();

                                                if (dispositionlst.Count > 0)
                                                {
                                                    for (int iComment = 0; iComment < dispositionlst.Count; iComment++)
                                                    {
                                                        //string disposition = dispositionlst.ElementAt(iComment).Value;
                                                        strDisposition = dispositionlst.ElementAt(iComment).Value;
                                                        using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
                                                        {
                                                            if (!string.IsNullOrEmpty(strDisposition))
                                                            {
                                                                string Desposition = strDisposition.ToLower();
                                                                while (Desposition.IndexOf(" ") >= 0)
                                                                {
                                                                    Desposition = Desposition.Replace(" ", "");
                                                                }

                                                                for (int r = 0; r < s.Count(); r++)
                                                                {
                                                                    if (string.IsNullOrEmpty(s[r]) == false)
                                                                    {
                                                                        if (Desposition.ToLower().Contains(s[r].ToLower()))
                                                                        {
                                                                            flag = 2;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                flag = 2;
                                                            }
                                                        }
                                                    }
                                                }
                                                List<XElement> ChargeColl;
                                                ChargeColl = CriminalCaseSingle.Descendants("dispositionDate").ToList<XElement>();
                                                if (ChargeColl != null && ChargeColl.Count > 0)
                                                {
                                                    foreach (XElement xe in ChargeColl)
                                                    {
                                                        string str = xe.Value;
                                                        DateTime dt;
                                                        str = (str != "" && str.Length > 9) ? str.Substring(0, 10) : DateTime.Now.ToShortDateString();
                                                        DateTime.TryParse(str, out dt);


                                                        int UpdateYear = Year_new * (-1);
                                                        DateTime NewDate = DateTime.Now.AddYears(UpdateYear);


                                                        //if (Max_new_Year != null)
                                                        //{
                                                        //    if (Max_new_Year >= NewDate) { MaxDate = true; }
                                                        //    else { MaxDate = false; }
                                                        //}
                                                        //else { MaxDate = true; }
                                                        //if (Min_new_Year != null)
                                                        //{
                                                        //    if (Min_new_Year <= NewDate) { MinDate = true; }
                                                        //    else { MinDate = false; }
                                                        //}
                                                        //else
                                                        //{
                                                        //    MinDate = true;
                                                        //}

                                                        if (CheckCase == 1)
                                                        {
                                                            if (dt < DateTime.Now.AddYears(UpdateYear))
                                                            {
                                                                if (flag == 2)
                                                                {
                                                                    IsEmergeReviewStatus = true;
                                                                }
                                                                if (flag != 2)
                                                                {
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {
                                                                        resultCollRm.Remove(CriminalCaseSingle);
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }


                                                        if (CheckCase == 2)
                                                        {
                                                            if (Max_new_Year <= NewDate)
                                                            {

                                                                if (dt < Max_new_Year)
                                                                {
                                                                    if (flag == 2)
                                                                    {
                                                                        IsEmergeReviewStatus = true;
                                                                    }
                                                                    if (flag != 2)
                                                                    {
                                                                        if (Collection.IsRemoveCharges == true)
                                                                        {
                                                                            resultCollRm.Remove(CriminalCaseSingle);
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (dt < DateTime.Now.AddYears(UpdateYear))
                                                                {
                                                                    if (flag == 2)
                                                                    {
                                                                        IsEmergeReviewStatus = true;
                                                                    }
                                                                    if (flag != 2)
                                                                    {
                                                                        if (Collection.IsRemoveCharges == true)
                                                                        {
                                                                            resultCollRm.Remove(CriminalCaseSingle);
                                                                            break;
                                                                        }
                                                                    }
                                                                }

                                                            }

                                                        }

                                                        if (CheckCase == 3)
                                                        {
                                                            if (Min_new_Year <= dt && dt <= Max_new_Year)
                                                            {
                                                                if (flag == 2)
                                                                {
                                                                    IsEmergeReviewStatus = true;
                                                                }
                                                                if (flag != 2)
                                                                {
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {
                                                                        resultCollRm.Remove(CriminalCaseSingle);
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                ///
                                /// Here i have removed all empty CriminalCases
                                ///
                                if (Collection.IsRemoveCharges == true)
                                {
                                    xElementresults.First().RemoveAll();
                                    foreach (XElement rm in resultCollRm)
                                    {
                                        xElementresults.First().Add(rm);
                                    }
                                }

                            }
                        }

                        #endregion
                    }
                    else if (ProductCode.Trim().ToUpper() == "NCR4")
                    {
                        #region NCR4

                        List<XElement> XECriminalSearchResults = ObjXDocument.Descendants("CriminalSearchResults").ToList<XElement>();
                        if (XECriminalSearchResults != null)
                        {
                            if (XECriminalSearchResults.Count > 0)
                            {
                                List<XElement> JurisdictionColl, JurisdictionCollRm;
                                JurisdictionColl = ObjXDocument.Descendants("JurisdictionResults").ToList();
                                JurisdictionCollRm = ObjXDocument.Descendants("JurisdictionResults").ToList();

                                XElement JurisdictionSingle = null;
                                if (JurisdictionColl != null && JurisdictionColl.Count > 0)
                                {

                                    for (int i = 0; i < JurisdictionColl.Count; i++)
                                    {
                                        JurisdictionSingle = JurisdictionColl.ElementAt(i);
                                        string strDisposition = string.Empty;
                                        List<XElement> ChargeColl = JurisdictionSingle.Descendants("Charge").ToList<XElement>();
                                        if (ChargeColl != null && ChargeColl.Count > 0)
                                        {
                                            bool isStateValid = false;
                                            string CountyName = "";

                                            foreach (XElement XECharge in ChargeColl)
                                            {
                                                XElement XEChrgCountyName = XECharge.Descendants("ChrgCountyName").FirstOrDefault();
                                                if (XEChrgCountyName != null)
                                                {
                                                    CountyName = XEChrgCountyName.Value;
                                                    ///
                                                    /// We have county name and then apply check for statename/statecode. 
                                                    ///
                                                    int stateId = 0;
                                                    if (CountyName != "")
                                                    {
                                                        stateId = ObjCountyColl.Where(d => d.CountyName.ToLower() == CountyName.ToLower()).Select(d => d.fkStateId).FirstOrDefault();
                                                        if (stateId != 0)
                                                        {
                                                            isStateValid = (ObjStateColl.Where(d => d.pkStateId == stateId).Count() > 0) ? true : false;
                                                        }
                                                    }
                                                    if (isStateValid)
                                                        break;
                                                }
                                            }
                                            if (isStateValid)
                                            {
                                                XElement XEDispDate = JurisdictionSingle.Descendants("DispDate").FirstOrDefault();
                                                XElement Disposition = JurisdictionSingle.Descendants("Disposition").FirstOrDefault();
                                                int flag = 1;
                                                using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
                                                {
                                                    if (Disposition != null)
                                                    {
                                                        string Despostioncheck = string.Empty;
                                                        Despostioncheck = Disposition.Value;
                                                        if (!string.IsNullOrEmpty(Despostioncheck))
                                                        {
                                                            string Desposition = Despostioncheck.ToLower();
                                                            while (Desposition.IndexOf(" ") >= 0)
                                                            {
                                                                Desposition = Desposition.Replace(" ", "");
                                                            }

                                                            for (int r = 0; r < s.Count(); r++)
                                                            {
                                                                if (string.IsNullOrEmpty(s[r]) == false)
                                                                {
                                                                    if (Desposition.ToLower().Contains(s[r].ToLower()))
                                                                    {
                                                                        flag = 2;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            flag = 2;
                                                        }
                                                    }
                                                }

                                                if (XEDispDate != null)
                                                {
                                                    string str = XEDispDate.Value;
                                                    DateTime dt;//20070625
                                                    if (str != "" && str.Length > 7)
                                                    {
                                                        str = str.Substring(0, 4) + "/" + str.Substring(4, 2) + "/" + str.Substring(6, 2);
                                                    }
                                                    else
                                                    {
                                                        str = DateTime.Now.ToShortDateString();
                                                    }
                                                    DateTime.TryParse(str, out dt);

                                                    int UpdateYear = Year_new * (-1);
                                                    DateTime NewDate = DateTime.Now.AddYears(UpdateYear);
                                                    if (CheckCase == 1)
                                                    {
                                                        if (dt < DateTime.Now.AddYears(UpdateYear))
                                                        {

                                                            if (flag == 2)
                                                            {
                                                                IsEmergeReviewStatus = true;
                                                            }
                                                            if (flag != 2)
                                                            {
                                                                if (Collection.IsRemoveCharges == true)
                                                                {
                                                                    JurisdictionCollRm.Remove(JurisdictionSingle);
                                                                }
                                                            }

                                                        }
                                                    }

                                                    if (CheckCase == 2)
                                                    {
                                                        if (Max_new_Year <= NewDate)
                                                        {

                                                            if (dt < Max_new_Year)
                                                            {
                                                                if (flag == 2)
                                                                {
                                                                    IsEmergeReviewStatus = true;
                                                                }
                                                                if (flag != 2)
                                                                {
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {
                                                                        JurisdictionCollRm.Remove(JurisdictionSingle);
                                                                    }
                                                                }

                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (dt < DateTime.Now.AddYears(UpdateYear))
                                                            {
                                                                if (flag == 2)
                                                                {
                                                                    IsEmergeReviewStatus = true;
                                                                }
                                                                if (flag != 2)
                                                                {
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {
                                                                        JurisdictionCollRm.Remove(JurisdictionSingle);
                                                                    }
                                                                }

                                                            }

                                                        }
                                                    }


                                                    if (CheckCase == 3)
                                                    {
                                                        if (Min_new_Year <= dt && dt <= Max_new_Year)
                                                        {
                                                            if (flag == 2)
                                                            {
                                                                IsEmergeReviewStatus = true;
                                                            }
                                                            if (flag != 2)
                                                            {
                                                                if (Collection.IsRemoveCharges == true)
                                                                {
                                                                    JurisdictionCollRm.Remove(JurisdictionSingle);
                                                                }
                                                            }

                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (Collection.IsRemoveCharges == true)
                                                    {
                                                        JurisdictionCollRm.Remove(JurisdictionSingle);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                ///
                                /// Here i have removed all empty CriminalCases
                                ///
                                XElement XESearchResultCount = XECriminalSearchResults.First().Descendants("SearchResultCount").FirstOrDefault();
                                if (XESearchResultCount != null)
                                {
                                    XESearchResultCount.Value = JurisdictionCollRm.Count.ToString();
                                }
                                if (Collection.IsRemoveCharges == true)
                                {
                                    XECriminalSearchResults.First().RemoveAll();
                                    if (XESearchResultCount != null)
                                    {
                                        XECriminalSearchResults.First().Add(XESearchResultCount);
                                    }
                                    foreach (XElement rm in JurisdictionCollRm)
                                    {
                                        XECriminalSearchResults.First().Add(rm);
                                    }
                                }
                            }
                        }

                        #endregion
                    }
                    else if (ProductCode.Trim().ToUpper() == "PS2")
                    {
                        ///
                        /// Date tag not found.
                        ///
                    }
                    else if (ProductCode.Trim().ToUpper() == "CCR1")
                    {
                        #region CCR1 NON- LIVERUNNER #482
                        //Do nothing.
                        #endregion
                    }
                    else if (ProductCode.Trim().ToUpper() == "NCR2")
                    {
                        ///
                        /// xslt shows html CDATA.
                        ///
                    }
                    if (Collection.IsEmergeReview == true)
                    {
                        if (IsEmergeReviewStatus == true)
                        {
                            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
                            {
                                tblOrderDetail ObjOrderDetails = ObjEmergeDALDataContext.tblOrderDetails.Where(db => db.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                                if (ObjOrderDetails != null)
                                {
                                    tblOrder objtblOrder = ObjEmergeDALDataContext.tblOrders.Where(db => db.pkOrderId == ObjOrderDetails.fkOrderId).FirstOrDefault();
                                    if (objtblOrder.IsReviewed == false && ObjOrderDetails.ReviewStatus == 0)
                                    {
                                        bool Is6monthOld = false;
                                        int Updatestatus = 0;
                                        BALEmergeReview ObjBALEmergeReview = new BALEmergeReview();
                                        Is6monthOld = Is6MonthOld(ObjOrderDetails.fkOrderId);
                                        int OrderId = Convert.ToInt32(ObjOrderDetails.fkOrderId.ToString());
                                        int pkOrderDetail_Id = Convert.ToInt32(pkOrderDetailId.ToString());
                                        Updatestatus = ObjBALEmergeReview.UpdateReportReviewStatus(pkOrderDetail_Id, 1, string.Empty, Is6monthOld);
                                        if (Updatestatus == 1 && IsPendingReport == true)
                                        {
                                            ObjBALEmergeReview.Email_send(OrderId, pkOrderDetail_Id);
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            ObjXDocumentupdated = ObjXDocument;
            return ObjXDocumentupdated;
        }

        public void AutoReviewByKeywordsDuringOrderPlace(string XMLresp, int? pkOrderDetailId, string ProductCode, bool IsPendingReport)
        {
            string Filter_Id = "4";
            List<string> arrKeywords = new List<string>();
            BALCompany ObjBALCompany = new BALCompany();
            var Collection = ObjBALCompany.GetFiltersAccordingFilterId(Filter_Id).FirstOrDefault();//Getting filter rules 7 Year Filter.
            if (Collection != null)
            {
                if (string.IsNullOrEmpty(Collection.Keywords) == false)
                {
                    var coll = Collection.Keywords.ToString();
                    var ArrayCollection = coll.Split(',');
                    if (ArrayCollection.Count() > 0)
                    {
                        for (int j = 0; j < ArrayCollection.Count(); j++)
                        {
                            arrKeywords.Add(ArrayCollection[j].Trim().ToUpper());//Adding all keywords in List<string> s = new List<string>();

                        }
                    }
                }
            }

            //XDocument ObjXDocumentupdated = new XDocument();
            XDocument ObjXDocument = new XDocument();

            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    bool IsEmergeReviewStatus = false;
                    string XML_Value = BALEmergeReview.GetRequiredXmlConditional(XMLresp, ProductCode.Trim().ToUpper());
                    ObjXDocument = XDocument.Parse(XML_Value);
                    if (ProductCode.Trim().ToUpper() == "NCR1" || ProductCode.Trim().ToUpper() == "NCR+" || ProductCode.Trim().ToUpper() == "RCX")
                    {
                        #region NCR1, NCR+, RCX
                        List<XElement> xelementCriminalReport = ObjXDocument.Descendants("CriminalReport").ToList();
                        if (xelementCriminalReport != null)
                        {
                            if (xelementCriminalReport.Count > 0)
                            {
                                List<XElement> CriminalCaseColl, CriminalCaseCollRm;
                                CriminalCaseColl = ObjXDocument.Descendants("CriminalCase").ToList();
                                CriminalCaseCollRm = ObjXDocument.Descendants("CriminalCase").ToList();

                                XElement CriminalCaseSingle = null;
                                if (CriminalCaseColl != null && CriminalCaseColl.Count > 0)
                                {

                                    for (int i = 0; i < CriminalCaseColl.Count; i++)
                                    {
                                        CriminalCaseSingle = CriminalCaseColl.ElementAt(i);
                                        List<XElement> ChargeColl, ChargeCollRm, AdditionalItemsColl;
                                        ChargeColl = CriminalCaseSingle.Descendants("Charge").ToList<XElement>();
                                        ChargeCollRm = CriminalCaseSingle.Descendants("Charge").ToList<XElement>();
                                        AdditionalItemsColl = CriminalCaseSingle.Descendants("AdditionalItems").ToList<XElement>();
                                        XElement AdditionalItemSingle = null;
                                        if (AdditionalItemsColl != null && AdditionalItemsColl.Count > 0)
                                        {
                                            foreach (XElement xe in AdditionalItemsColl)
                                            {
                                                AdditionalItemSingle = xe;
                                                XElement Text = AdditionalItemSingle.Descendants("Text").FirstOrDefault();
                                                if (Text != null)
                                                {
                                                    if (arrKeywords != null)
                                                    {
                                                        if (arrKeywords.Count() > 0)
                                                        {
                                                            for (int arr = 0; arr < arrKeywords.Count(); arr++)
                                                            {
                                                                if (Text.Value.ToLower().Contains(arrKeywords.ElementAt(arr).ToLower().ToString()))
                                                                {
                                                                    IsEmergeReviewStatus = true;
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {
                                                                        ChargeCollRm.Remove(AdditionalItemSingle);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else { CriminalCaseCollRm.Remove(CriminalCaseSingle); }
                                        if (Collection.IsRemoveCharges == true)
                                        {
                                            #region Removing/Adding Charges to Current criminal Case

                                            CriminalCaseSingle.Descendants("Charge").Remove();
                                            //CriminalCaseSingle.RemoveAll();
                                            foreach (XElement rm in ChargeCollRm)
                                            {
                                                CriminalCaseSingle.Add(rm);
                                            }
                                            #endregion

                                            #region Logic to remove CriminalCase if all chares are removed
                                            if (ChargeCollRm.Count == 0)
                                            {
                                                CriminalCaseCollRm.Remove(CriminalCaseSingle);
                                            }
                                            #endregion
                                        }
                                        XElement ChargeSingle = null;
                                        if (ChargeColl != null && ChargeColl.Count > 0)
                                        {
                                            foreach (XElement xe in ChargeColl)
                                            {
                                                ChargeSingle = xe;
                                                string strDisposition = string.Empty;
                                                XElement Disposition = ChargeSingle.Descendants("Disposition").FirstOrDefault();
                                                if (Disposition != null)
                                                {
                                                    strDisposition = Disposition.Value;
                                                    if (strDisposition.ToLower() != "dismissed")
                                                    {
                                                        if (arrKeywords != null)
                                                        {
                                                            if (arrKeywords.Count() > 0)
                                                            {
                                                                for (int arr = 0; arr < arrKeywords.Count(); arr++)
                                                                {
                                                                    if (Disposition.Value.ToLower().Contains(arrKeywords.ElementAt(arr).ToLower().ToString()))
                                                                    {
                                                                        IsEmergeReviewStatus = true;
                                                                        if (Collection.IsRemoveCharges == true)
                                                                        {
                                                                            ChargeCollRm.Remove(AdditionalItemSingle);
                                                                        }
                                                                        //WatchListStatus = 1;
                                                                        //if (CriminalCaseSingle.Attribute("WatchList") == null)
                                                                        //{
                                                                        //    CriminalCaseSingle.Add(new XAttribute("WatchList", "" + 1 + ""));
                                                                        //}
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                if (strDisposition.ToLower() != "dismissed")
                                                {
                                                    var ChargeTypeClassification = ChargeSingle.Descendants("ChargeTypeClassification").FirstOrDefault();
                                                    if (ChargeTypeClassification != null)
                                                    {
                                                        if (arrKeywords != null)
                                                        {
                                                            if (arrKeywords.Count() > 0)
                                                            {
                                                                for (int arr = 0; arr < arrKeywords.Count(); arr++)
                                                                {
                                                                    if (ChargeTypeClassification.Value.ToLower().Contains(arrKeywords.ElementAt(arr).ToLower().ToString()))
                                                                    {
                                                                        IsEmergeReviewStatus = true;
                                                                        if (Collection.IsRemoveCharges == true)
                                                                        {
                                                                            ChargeCollRm.Remove(AdditionalItemSingle);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                    var ChargeOrComplaint = ChargeSingle.Descendants("ChargeOrComplaint").FirstOrDefault();
                                                    if (ChargeOrComplaint != null)
                                                    {
                                                        if (arrKeywords != null)
                                                        {
                                                            if (arrKeywords.Count() > 0)
                                                            {
                                                                for (int arr = 0; arr < arrKeywords.Count(); arr++)
                                                                {
                                                                    if (ChargeOrComplaint.Value.ToLower().Contains(arrKeywords.ElementAt(arr).ToLower().ToString()))
                                                                    {
                                                                        IsEmergeReviewStatus = true;
                                                                        if (Collection.IsRemoveCharges == true)
                                                                        {
                                                                            ChargeCollRm.Remove(AdditionalItemSingle);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                    var Textlst = ChargeSingle.Descendants("Text").ToList();

                                                    if (Textlst.Count > 0)
                                                    {
                                                        for (int iText = 0; iText < Textlst.Count; iText++)
                                                        {
                                                            string text = Textlst.ElementAt(iText).Value;
                                                            if (arrKeywords != null)
                                                            {
                                                                if (arrKeywords.Count() > 0)
                                                                {
                                                                    for (int arr = 0; arr < arrKeywords.Count(); arr++)
                                                                    {
                                                                        if (text.ToLower().Contains(arrKeywords.ElementAt(arr).ToLower().ToString()))
                                                                        {
                                                                            IsEmergeReviewStatus = true;
                                                                            if (Collection.IsRemoveCharges == true)
                                                                            {
                                                                                ChargeCollRm.Remove(AdditionalItemSingle);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                    var Sentencelst = ChargeSingle.Descendants("Sentence").ToList();

                                                    if (Sentencelst.Count > 0)
                                                    {
                                                        for (int iSentence = 0; iSentence < Sentencelst.Count; iSentence++)
                                                        {
                                                            string sentence = Sentencelst.ElementAt(iSentence).Value;
                                                            if (arrKeywords != null)
                                                            {
                                                                if (arrKeywords.Count() > 0)
                                                                {
                                                                    for (int arr = 0; arr < arrKeywords.Count(); arr++)
                                                                    {
                                                                        if (sentence.ToLower().Contains(arrKeywords.ElementAt(arr).ToLower().ToString()))
                                                                        {
                                                                            IsEmergeReviewStatus = true;
                                                                            if (Collection.IsRemoveCharges == true)
                                                                            {
                                                                                ChargeCollRm.Remove(AdditionalItemSingle);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    var Commentlst = ChargeSingle.Descendants("Comment").ToList();

                                                    if (Commentlst.Count > 0)
                                                    {
                                                        for (int iComment = 0; iComment < Commentlst.Count; iComment++)
                                                        {
                                                            string comment = Commentlst.ElementAt(iComment).Value;
                                                            if (arrKeywords != null)
                                                            {
                                                                if (arrKeywords.Count() > 0)
                                                                {
                                                                    for (int arr = 0; arr < arrKeywords.Count(); arr++)
                                                                    {
                                                                        if (comment.ToLower().Contains(arrKeywords.ElementAt(arr).ToLower().ToString()))
                                                                        {
                                                                            IsEmergeReviewStatus = true;
                                                                            if (Collection.IsRemoveCharges == true)
                                                                            {
                                                                                ChargeCollRm.Remove(AdditionalItemSingle);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else { CriminalCaseCollRm.Remove(CriminalCaseSingle); }
                                        if (Collection.IsRemoveCharges == true)
                                        {
                                            #region Removing/Adding Charges to Current criminal Case

                                            CriminalCaseSingle.Descendants("Charge").Remove();
                                            //CriminalCaseSingle.RemoveAll();
                                            foreach (XElement rm in ChargeCollRm)
                                            {
                                                CriminalCaseSingle.Add(rm);
                                            }
                                            #endregion

                                            #region Logic to remove CriminalCase if all chares are removed
                                            if (ChargeCollRm.Count == 0)
                                            {
                                                CriminalCaseCollRm.Remove(CriminalCaseSingle);
                                            }
                                            #endregion
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                    }
                    else if (ProductCode.Trim().ToUpper() == "SCR")
                    {
                        #region SCR
                        ///
                        /// In SCR we have 2 type of tags.<reslut>/<CriminalCase>
                        /// First I have applied check for <CriminalCase>
                        ///
                        List<XElement> xelementCriminalReport = ObjXDocument.Descendants("CriminalReport").ToList();
                        List<XElement> xElementresults = ObjXDocument.Descendants("results").ToList<XElement>();
                        if (xelementCriminalReport != null)
                        {
                            if (xelementCriminalReport.Count > 0)
                            {

                            }
                            ///
                            /// If Response contain result tag
                            ///
                            else if (xElementresults != null)
                            {
                                List<XElement> resultColl, resultCollRm;
                                resultColl = ObjXDocument.Descendants("result").ToList();
                                resultCollRm = ObjXDocument.Descendants("result").ToList();

                                XElement CriminalCaseSingle = null;
                                if (resultColl != null && resultColl.Count > 0)
                                {
                                    for (int i = 0; i < resultColl.Count; i++)
                                    {
                                        CriminalCaseSingle = resultColl.ElementAt(i);
                                        string strDisposition = string.Empty;
                                        var dispositionlst = CriminalCaseSingle.Descendants("disposition").ToList();

                                        if (dispositionlst.Count > 0)
                                        {
                                            for (int iComment = 0; iComment < dispositionlst.Count; iComment++)
                                            {
                                                string disposition = dispositionlst.ElementAt(iComment).Value;
                                                strDisposition = dispositionlst.ElementAt(iComment).Value;
                                                if (strDisposition.ToLower() != "dismissed")
                                                {
                                                    if (arrKeywords != null)
                                                    {
                                                        if (arrKeywords.Count() > 0)
                                                        {
                                                            for (int arr = 0; arr < arrKeywords.Count(); arr++)
                                                            {
                                                                if (disposition.ToLower().Contains(arrKeywords.ElementAt(arr).ToLower().ToString()))
                                                                {
                                                                    IsEmergeReviewStatus = true;
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {
                                                                        resultCollRm.Remove(CriminalCaseSingle);
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (strDisposition.ToLower() != "dismissed")
                                        {
                                            var crimeDetailslst = CriminalCaseSingle.Descendants("crimeDetails").ToList();

                                            if (crimeDetailslst.Count > 0)
                                            {
                                                for (int iText = 0; iText < crimeDetailslst.Count; iText++)
                                                {
                                                    string crimeDetails = crimeDetailslst.ElementAt(iText).Value;
                                                    if (arrKeywords != null)
                                                    {
                                                        if (arrKeywords.Count() > 0)
                                                        {
                                                            for (int arr = 0; arr < arrKeywords.Count(); arr++)
                                                            {
                                                                if (crimeDetails.ToLower().Contains(arrKeywords.ElementAt(arr).ToLower().ToString()))
                                                                {
                                                                    IsEmergeReviewStatus = true;
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {
                                                                        resultCollRm.Remove(CriminalCaseSingle);
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            var crimeTypelst = CriminalCaseSingle.Descendants("crimeType").ToList();

                                            if (crimeTypelst.Count > 0)
                                            {
                                                for (int iSentence = 0; iSentence < crimeTypelst.Count; iSentence++)
                                                {
                                                    string crimeType = crimeTypelst.ElementAt(iSentence).Value;
                                                    if (arrKeywords != null)
                                                    {
                                                        if (arrKeywords.Count() > 0)
                                                        {
                                                            for (int arr = 0; arr < arrKeywords.Count(); arr++)
                                                            {
                                                                if (crimeType.ToLower().Contains(arrKeywords.ElementAt(arr).ToLower().ToString()))
                                                                {
                                                                    IsEmergeReviewStatus = true;
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {
                                                                        resultCollRm.Remove(CriminalCaseSingle);
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if (Collection.IsRemoveCharges == true)
                                {
                                    xElementresults.First().RemoveAll();
                                    foreach (XElement rm in resultCollRm)
                                    {
                                        xElementresults.First().Add(rm);
                                    }
                                }
                            }
                        }

                        #endregion
                    }
                    else if (ProductCode.Trim().ToUpper() == "NCR4")
                    {
                        #region NCR4

                        List<XElement> XECriminalSearchResults = ObjXDocument.Descendants("CriminalSearchResults").ToList<XElement>();
                        if (XECriminalSearchResults != null)
                        {
                            if (XECriminalSearchResults.Count > 0)
                            {
                                List<XElement> JurisdictionColl, JurisdictionCollRm;
                                JurisdictionColl = ObjXDocument.Descendants("JurisdictionResults").ToList();
                                JurisdictionCollRm = ObjXDocument.Descendants("JurisdictionResults").ToList();

                                XElement JurisdictionSingle = null;
                                if (JurisdictionColl != null && JurisdictionColl.Count > 0)
                                {
                                    for (int i = 0; i < JurisdictionColl.Count; i++)
                                    {
                                        JurisdictionSingle = JurisdictionColl.ElementAt(i);
                                        string strDisposition = string.Empty;
                                        List<XElement> ChargeColl = JurisdictionSingle.Descendants("Charge").ToList<XElement>();
                                        if (ChargeColl != null && ChargeColl.Count > 0)
                                        {
                                            XElement Disposition = JurisdictionSingle.Descendants("Disposition").FirstOrDefault();
                                            if (Disposition != null)
                                            {
                                                strDisposition = Disposition.Value;
                                                if (strDisposition.ToLower() != "dismissed")
                                                {
                                                    if (arrKeywords != null)
                                                    {
                                                        if (arrKeywords.Count() > 0)
                                                        {
                                                            for (int arr = 0; arr < arrKeywords.Count(); arr++)
                                                            {
                                                                if (Disposition.Value.ToLower().Contains(arrKeywords.ElementAt(arr).ToLower().ToString()))
                                                                {
                                                                    IsEmergeReviewStatus = true;
                                                                    if (Collection.IsRemoveCharges == true)
                                                                    {
                                                                        JurisdictionCollRm.Remove(JurisdictionSingle);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                ///
                                /// Here i have removed all empty CriminalCases
                                ///
                                XElement XESearchResultCount = XECriminalSearchResults.First().Descendants("SearchResultCount").FirstOrDefault();
                                if (XESearchResultCount != null)
                                {
                                    XESearchResultCount.Value = JurisdictionCollRm.Count.ToString();
                                }
                                if (Collection.IsRemoveCharges == true)
                                {
                                    XECriminalSearchResults.First().RemoveAll();
                                    if (XESearchResultCount != null)
                                    {
                                        XECriminalSearchResults.First().Add(XESearchResultCount);
                                    }
                                    foreach (XElement rm in JurisdictionCollRm)
                                    {
                                        XECriminalSearchResults.First().Add(rm);
                                    }
                                }
                            }
                        }

                        #endregion
                    }
                    else if (ProductCode.Trim().ToUpper() == "PS2")
                    {
                        ///
                        /// Date tag not found.
                        ///
                    }
                    else if (ProductCode.Trim().ToUpper() == "CCR1")
                    {
                        #region CCR1 NON- LIVERUNNER #481
                        //Do nothing.
                        #endregion

                    }
                    else if (ProductCode.Trim().ToUpper() == "NCR2")
                    {
                        ///
                        /// xslt shows html CDATA.
                        ///
                    }
                    if (Collection.IsEmergeReview == true)
                    {
                        if (IsEmergeReviewStatus == true)
                        {

                            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
                            {
                                tblOrderDetail ObjOrderDetails = ObjEmergeDALDataContext.tblOrderDetails.Where(db => db.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                                if (ObjOrderDetails != null)
                                {
                                    tblOrder objtblOrder = ObjEmergeDALDataContext.tblOrders.Where(db => db.pkOrderId == ObjOrderDetails.fkOrderId).FirstOrDefault();
                                    if (objtblOrder.IsReviewed == false && ObjOrderDetails.ReviewStatus == 0)
                                    {
                                        bool Is6monthOld = false;
                                        int Updatestatus = 0;
                                        BALEmergeReview ObjBALEmergeReview = new BALEmergeReview();
                                        Is6monthOld = Is6MonthOld(ObjOrderDetails.fkOrderId);
                                        int OrderId = Convert.ToInt32(ObjOrderDetails.fkOrderId.ToString());
                                        int pkOrderDetail_Id = Convert.ToInt32(pkOrderDetailId.ToString());
                                        Updatestatus = ObjBALEmergeReview.UpdateReportReviewStatus(pkOrderDetail_Id, 1, string.Empty, Is6monthOld);
                                        if (Updatestatus == 1 && IsPendingReport == true)
                                        {
                                            ObjBALEmergeReview.Email_send(OrderId, pkOrderDetail_Id);
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
           // ObjXDocumentupdated = ObjXDocument;
        }

        #endregion

        /// <summary>
        /// Get ShipTo Address of Users
        /// </summary>
        /// <param name="locationid"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ShipToAddress GetShipToAddressDetails(int? locationid, int? userId)
        {
           // string address = string.Empty;
            ShipToAddress objShipToAddress = new ShipToAddress();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                var valAddress = DX.GetShipToAddressofUser(locationid, userId).FirstOrDefault();
                if (valAddress != null)
                {
                    objShipToAddress.CompanyName = valAddress.CompanyName;
                    objShipToAddress.Address1 = valAddress.Address1;
                    objShipToAddress.Address2 = valAddress.Address2 != null ? valAddress.Address2 : string.Empty;
                    objShipToAddress.StateCode = valAddress.StateCode;
                    objShipToAddress.City = valAddress.City;
                    objShipToAddress.ZipCode = valAddress.ZipCode;
                }
            }
            return objShipToAddress;
        }

        /// <summary>
        /// To save the shipping address based on order id.
        /// </summary>
        /// <param name="shipToAddress"></param>
        public void SaveShipToAddressInDB(ShipToAddress shipToAddress)
        {
            tblShipToAddressDetail objTblShip = new tblShipToAddressDetail();
            using (EmergeDALDataContext dbContext = new EmergeDALDataContext())
            {
                objTblShip.fkLocationId = shipToAddress.LocationId;
                objTblShip.fkCompanyUserId = shipToAddress.CompanyUserId;
                objTblShip.CompanyName = shipToAddress.CompanyName;
                objTblShip.Address1 = shipToAddress.Address1;
                objTblShip.Address2 = shipToAddress.Address2;
                objTblShip.State_Code = shipToAddress.StateCode;
                objTblShip.City = shipToAddress.City;
                objTblShip.ZipCode = shipToAddress.ZipCode;
                objTblShip.fkOrderId = shipToAddress.fkOrderId;
                dbContext.tblShipToAddressDetails.InsertOnSubmit(objTblShip);
                dbContext.SubmitChanges();
            }
        }
        /// <summary>
        /// update ship address to database
        /// </summary>
        /// <param name="shipToAddress"></param>
        public void UpdateShipToAddressInDB(ShipToAddress shipToAddress)
        {
            tblShipToAddressDetail objTblShip = new tblShipToAddressDetail();
            using (EmergeDALDataContext dbContext = new EmergeDALDataContext())
            {
                objTblShip = dbContext.tblShipToAddressDetails.Where(x => x.fkLocationId == shipToAddress.LocationId && x.fkCompanyUserId == shipToAddress.CompanyUserId && x.fkOrderId == shipToAddress.fkOrderId).FirstOrDefault();
                if (objTblShip != null)
                {
                    objTblShip.CompanyName = shipToAddress.CompanyName;
                    objTblShip.Address1 = shipToAddress.Address1;
                    objTblShip.Address2 = shipToAddress.Address2;
                    objTblShip.State_Code = shipToAddress.StateCode;
                    objTblShip.City = shipToAddress.City;
                    objTblShip.ZipCode = shipToAddress.ZipCode;
                    dbContext.SubmitChanges();
                }
            }
        }
        /// <summary>
        /// Check Ship to address already exist or not
        /// </summary>
        /// <param name="LocationId"></param>
        /// <param name="CompanyUserId"></param>
        /// <returns></returns>
        public bool CheckCompanyExistanceForShipAddress(int LocationId, int CompanyUserId)
        {
            bool checkSatus = false;
            using (EmergeDALDataContext dbContext = new EmergeDALDataContext())
            {
                var objValue = dbContext.tblShipToAddressDetails.Where(x => x.fkLocationId == LocationId && x.fkCompanyUserId == CompanyUserId).FirstOrDefault();
                if (objValue != null)
                {
                    checkSatus = true;
                }
            }
            return checkSatus;
        }

        /// <summary>
        /// Get Updated ship to address for drug testing order
        /// </summary>
        /// <param name="LocationId"></param>
        /// <param name="CompanyUserId"></param>
        /// <returns></returns>
        public ShipToAddress GetUpdatedShipToAddress(int LocationId, int CompanyUserId, int fkOrderId)
        {
            ShipToAddress objShipToaddress = new ShipToAddress();
            using (EmergeDALDataContext dbContext = new EmergeDALDataContext())
            {
                objShipToaddress = dbContext.tblShipToAddressDetails.Where(x => x.fkCompanyUserId == CompanyUserId && x.fkLocationId == LocationId && x.fkOrderId == fkOrderId).Select(x => new ShipToAddress { CompanyName = x.CompanyName, Address1 = x.Address1, Address2 = x.Address2, StateCode = x.State_Code, City = x.City, ZipCode = x.ZipCode }).FirstOrDefault();
            }

            return objShipToaddress;

        }

        /// <summary>
        /// drug testing order update for ticket #17
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="reportAmount"></param>
        /// <param name="productQuantity"></param>
        /// <param name="additionalFee"></param>
        /// <returns></returns>
        public int UpdateDrugTestProductInDb(int orderId, decimal reportAmount, short productQuantity, decimal additionalFee)
        {
            int result = 0;
            tblOrderDetail objTblOrderDetails = new tblOrderDetail();
           // tblProductPerApplication objTblProductPerApplication = new tblProductPerApplication();
            using (EmergeDALDataContext dbContext = new EmergeDALDataContext())
            {
                objTblOrderDetails = dbContext.tblOrderDetails.Where(x => x.fkOrderId == orderId).FirstOrDefault();
                if (objTblOrderDetails != null)
                {
                    objTblOrderDetails.ReportAmount = reportAmount;
                    objTblOrderDetails.ProductQty = productQuantity;
                    objTblOrderDetails.AdditionalFee = additionalFee;
                    dbContext.SubmitChanges();
                    result = 1;
                }
            }

            return result;

        }

        /// <summary>
        /// To Get Other email ids to users.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string GetUserOtherMailIds(string userEmail)
        {
            string usersMailIds = string.Empty;
            Guid UserId = Guid.Empty;
            MembershipUser Obj = Membership.GetUser(userEmail);
            if (Obj != null)
            {
                UserId = new Guid(Obj.ProviderUserKey.ToString());
            }

            using (EmergeDALDataContext dbContext = new EmergeDALDataContext())
            {
                if (UserId != null)
                {
                    var emailsIdValues = dbContext.tblCompanyUsers.Where(c => c.fkUserId == UserId).FirstOrDefault();
                    if (emailsIdValues != null)
                    {
                        if (!string.IsNullOrEmpty(emailsIdValues.UserEmailOne))
                        {
                            usersMailIds = usersMailIds + emailsIdValues.UserEmailOne + ",";
                        }
                        if (!string.IsNullOrEmpty(emailsIdValues.UserEmailTwo))
                        {
                            usersMailIds = usersMailIds + emailsIdValues.UserEmailTwo;
                        }
                    }
                }
            }
            if (!string.IsNullOrEmpty(usersMailIds))
                return usersMailIds.TrimEnd(',');
            else
                return string.Empty;
        }



        public static void SendingProcess(string Message)
        {
            int SendingStatus = 0;
            try
            {
                MomentumInfotech.EmailProvider.MailProvider ObjMailProvider = new MomentumInfotech.EmailProvider.MailProvider();

                ObjMailProvider.Message = Message;
                //ObjMailProvider.MailTo.Add("arunk3@chetu.com");
                ObjMailProvider.MailTo.Add(BALGeneral.GetGeneralSettings().SupportEmailId);
                ObjMailProvider.MailFrom = BALGeneral.GetGeneralSettings().SupportEmailId;
                ObjMailProvider.Subject = "Check Order Status.";
                ObjMailProvider.IsBodyHtml = true;
                ObjMailProvider.Priority = MailPriority.High;
                ObjMailProvider.SendMail(out SendingStatus);
            }
            catch //(Exception ex)
            {
            }
        }


        /// <summary>
        /// To send the mail, if report status comming fulfilled in status.
        /// </summary>
        public static void SendMailAfterReportStatusFulFilled(int? OrderId)
        {
            string msg = string.Empty;
            using (EmergeDALDataContext dbContext = new EmergeDALDataContext())
            {
                var order = dbContext.tblOrders.Where(ord => ord.pkOrderId.Equals(OrderId)).FirstOrDefault();
                if (order != null)
                {
                    msg = "Please check status of OrderNo : " + order.OrderNo + ", because current status is fulfilled";
                    //Thread object for the used for the sending email process.

                    System.Threading.Thread oThreadForMail = new System.Threading.Thread(() => SendingProcess(msg));
                    oThreadForMail.Start();
                }
            }
        }


        #region 613NoticePdf XML Filteration updated
        public string GetFilteredXMLDuringSending613Notice(string ProductCode, string strResponseData, int? pkOrderDetailId, DateTime CurrentDate, bool IsPendingReport, bool IsLiveRunner)
        {
            string FinalResponse = ""; string UpdatedResponse = "";
            using (EmergeDALDataContext Obj = new EmergeDALDataContext())
            {
                UpdatedResponse = strResponseData;

                HttpContext.Current.Session["OrderDetailId"] = pkOrderDetailId;


                string FilterId = "3";
                bool IsAdmin = false;
                bool UserCheck = false;
                bool ReportExist = false;
                bool IsthisPackageReport = false;

                BALCompany ObjBALCompany = new BALCompany();


                var ObjData = (from u in Obj.tblOrderDetails
                               where u.pkOrderDetailId == pkOrderDetailId
                               select u).FirstOrDefault();
                var Collection = ObjBALCompany.GetFiltersAccordingFilterId(FilterId).FirstOrDefault(); //Getting filter rules for Watch list FilterOR Dismissed Filter
                //string Keywords = Collection.Keywords;
                if (IsPendingReport == false)
                {

                    //if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SystemAdmin")) { IsAdmin = true; } //Checking Current user is admin or not.
                    //if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SupportManager")) { IsAdmin = true; }
                    //if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SupportTechnician")) { IsAdmin = true; }
                    IsAdmin = true;
                    if (Collection != null)
                    {
                        if (ObjData != null)
                        {
                            if (ObjData.IsPackageReport == true)//Checking Is this PackageReport  or not.
                            {
                                if (Collection.Packages == true) { IsthisPackageReport = false; }
                                else { IsthisPackageReport = true; }
                            }
                            else
                            {
                                IsthisPackageReport = false;
                            }
                            if (string.IsNullOrEmpty(Collection.ReportType) == false) //Checking Report type exist   or not.
                            {
                                var Report_Type = Collection.ReportType;
                                var CollProduct = Report_Type.Split(',');
                                string Coll = ProductCode.ToUpper();
                                if (CollProduct.Length > 0)
                                {
                                    for (int m = 0; m < CollProduct.Length; m++)
                                    {
                                        if (Coll == CollProduct[m].ToUpper())
                                        {
                                            ReportExist = true;
                                            break;
                                        }
                                    }
                                }
                            }

                            if (Collection.Enabled == true) //Checking Watch list FilterOR DismissedFilter  Active or not.
                            {
                                if (Collection.Users == 1) { UserCheck = true; }
                                if (Collection.Users == 2) { if (IsAdmin == true) { UserCheck = true; } }
                                if (Collection.Users == 3) { if (IsAdmin == false) { UserCheck = true; } }
                                if (UserCheck == true)
                                {
                                    if (IsthisPackageReport == false)
                                    {
                                        if (ReportExist == true)
                                        {
                                            #region Dismissed Charge Response
                                            try
                                            {
                                                string UpdatedDismissedChargeResponse = GetRemovedDismissedChargeXMLFilterDuringOrderPlace(UpdatedResponse, ProductCode, IsLiveRunner);
                                                UpdatedResponse = UpdatedDismissedChargeResponse;
                                                FinalResponse = UpdatedDismissedChargeResponse;
                                            }
                                            catch
                                            {
                                                FinalResponse = UpdatedResponse;
                                            }
                                            #endregion Dismissed Charge Response
                                        }
                                    }
                                }
                            }
                        }
                    }


                    //BALEmergeReview objBALEmergeReview = new BALEmergeReview();

                    string Filter_Id4 = "4";
                    bool Is_Admin4 = false;
                    bool User_Check4 = false;
                    bool Report_Exist4 = false;
                    bool Isthis_PackageReport4 = false;
                    var Collection_4 = ObjBALCompany.GetFiltersAccordingFilterId(Filter_Id4).FirstOrDefault(); //Getting filter rules 7 Year Filter.
                    // if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SystemAdmin")) { Is_Admin4 = true; }      //Checking Current user is admin or not.
                    //if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SupportManager")) { Is_Admin4 = true; }
                    // if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SupportTechnician")) { Is_Admin4 = true; }
                    Is_Admin4 = true;
                    if (Collection_4 != null)
                    {
                        if (ObjData != null)
                        {
                            if (ObjData.IsPackageReport == true) //Checking Is this PackageReport  or not.
                            {
                                if (Collection_4.Packages == true) { Isthis_PackageReport4 = false; }
                                else { Isthis_PackageReport4 = true; }
                            }
                            else { Isthis_PackageReport4 = false; }
                            if (string.IsNullOrEmpty(Collection_4.ReportType) == false) //Checking Report type exist   or not.
                            {
                                var Report_Type = Collection_4.ReportType;
                                var CollProduct = Report_Type.Split(',');
                                string Coll = ProductCode.ToUpper();
                                if (CollProduct.Length > 0)
                                {
                                    for (int m = 0; m < CollProduct.Length; m++)
                                    {
                                        if (Coll == CollProduct[m].ToUpper())
                                        {
                                            Report_Exist4 = true;
                                            break;
                                        }
                                    }
                                }
                            }

                            if (Collection_4.Enabled == true) //Checking Report type exist   or not.
                            {
                                if (Collection_4.Users == 1) { User_Check4 = true; }
                                if (Collection_4.Users == 2) { if (Is_Admin4 == true) { User_Check4 = true; } }
                                if (Collection_4.Users == 3) { if (Is_Admin4 == false) { User_Check4 = true; } }
                                if (User_Check4 == true)
                                {
                                    if (Isthis_PackageReport4 == false)
                                    {
                                        if (Report_Exist4 == true)
                                        {
                                            #region Ticket #173:
                                            try
                                            {
                                                AutoReviewByKeywordsDuringOrderPlace(UpdatedResponse, pkOrderDetailId, ProductCode, IsPendingReport);
                                                //objBALEmergeReview.AutoReviewByKeywords(UpdatedResponse, pkOrderDetailId, ProductCode);
                                                //objBALEmergeReview.AutoReviewByKeywords(strResponseData, pkOrderDetailId, ProductCode);
                                            }
                                            catch
                                            {
                                                FinalResponse = UpdatedResponse;
                                            }
                                            #endregion
                                        }
                                    }
                                }
                            }
                        }
                    }

                    string Filter_Id = "1";
                    bool Is_Admin = false;
                    bool User_Check = false;
                    bool Report_Exist = false;
                    bool Isthis_PackageReport = false;
                    var Collection_seven = ObjBALCompany.GetFiltersAccordingFilterId(Filter_Id).FirstOrDefault(); //Getting filter rules 7 Year Filter.
                    //if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SystemAdmin")) { Is_Admin = true; }      //Checking Current user is admin or not.
                    //if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SupportManager")) { Is_Admin = true; }
                    //if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SupportTechnician")) { Is_Admin = true; }
                    Is_Admin = true;
                    if (Collection_seven != null)
                    {
                        if (ObjData != null)
                        {
                            if (ObjData.IsPackageReport == true) //Checking Is this PackageReport  or not.
                            {
                                if (Collection_seven.Packages == true) { Isthis_PackageReport = false; }
                                else { Isthis_PackageReport = true; }
                            }
                            else { Isthis_PackageReport = false; }
                            if (string.IsNullOrEmpty(Collection_seven.ReportType) == false) //Checking Report type exist   or not.
                            {
                                var Report_Type = Collection_seven.ReportType;
                                var CollProduct = Report_Type.Split(',');
                                string Coll = ProductCode.ToUpper();
                                if (CollProduct.Length > 0)
                                {
                                    for (int m = 0; m < CollProduct.Length; m++)
                                    {
                                        if (Coll == CollProduct[m].ToUpper())
                                        {
                                            Report_Exist = true;
                                            break;
                                        }
                                    }
                                }
                            }

                            if (Collection_seven.Enabled == true) //Checking Report type exist   or not.
                            {
                                if (Collection_seven.Users == 1) { User_Check = true; }
                                if (Collection_seven.Users == 2) { if (Is_Admin == true) { User_Check = true; } }
                                if (Collection_seven.Users == 3) { if (Is_Admin == false) { User_Check = true; } }
                                if (User_Check == true)
                                {
                                    if (Isthis_PackageReport == false)
                                    {
                                        if (Report_Exist == true)
                                        {
                                            #region Ticket #800: 7 YEAR STATES
                                            try
                                            {
                                                string Updated7YearResponse = HideSevenYearDuringOrderPlace(UpdatedResponse, ProductCode, CurrentDate, pkOrderDetailId, IsPendingReport);
                                                UpdatedResponse = Updated7YearResponse;
                                                FinalResponse = Updated7YearResponse;
                                            }
                                            catch
                                            {
                                                FinalResponse = UpdatedResponse;
                                            }
                                            #endregion
                                        }
                                    }
                                }
                            }
                        }



                    }
                }

                else
                {
                    if (Collection != null)
                    {
                        if (ObjData != null)
                        {
                            if (ObjData.IsPackageReport == true)//Checking Is this PackageReport  or not.
                            {
                                if (Collection.Packages == true) { IsthisPackageReport = false; }
                                else { IsthisPackageReport = true; }
                            }
                            else
                            {
                                IsthisPackageReport = false;
                            }
                            if (string.IsNullOrEmpty(Collection.ReportType) == false) //Checking Report type exist   or not.
                            {
                                var Report_Type = Collection.ReportType;
                                var CollProduct = Report_Type.Split(',');
                                string Coll = ProductCode.ToUpper();
                                if (CollProduct.Length > 0)
                                {
                                    for (int m = 0; m < CollProduct.Length; m++)
                                    {
                                        if (Coll == CollProduct[m].ToUpper())
                                        {
                                            ReportExist = true;
                                            break;
                                        }
                                    }
                                }
                            }

                            if (Collection.Enabled == true) //Checking Watch list FilterOR DismissedFilter  Active or not.
                            {
                                if (IsthisPackageReport == false)
                                {
                                    if (ReportExist == true)
                                    {
                                        #region Dismissed Charge Response
                                        try
                                        {
                                            string UpdatedDismissedChargeResponse = GetRemovedDismissedChargeXMLFilterDuringOrderPlace(UpdatedResponse, ProductCode, IsLiveRunner);
                                            UpdatedResponse = UpdatedDismissedChargeResponse;
                                            FinalResponse = UpdatedDismissedChargeResponse;
                                        }
                                        catch
                                        {
                                            FinalResponse = UpdatedResponse;
                                        }
                                        #endregion Dismissed Charge Response
                                    }
                                }
                            }
                        }
                    }


                   // BALEmergeReview objBALEmergeReview = new BALEmergeReview();

                    string Filter_Id4 = "4";
                    bool Report_Exist4 = false;
                    bool Isthis_PackageReport4 = false;
                    var Collection_4 = ObjBALCompany.GetFiltersAccordingFilterId(Filter_Id4).FirstOrDefault(); //Getting filter rules 7 Year Filter.}
                    if (Collection_4 != null)
                    {
                        if (ObjData != null)
                        {
                            if (ObjData.IsPackageReport == true) //Checking Is this PackageReport  or not.
                            {
                                if (Collection_4.Packages == true) { Isthis_PackageReport4 = false; }
                                else { Isthis_PackageReport4 = true; }
                            }
                            else { Isthis_PackageReport4 = false; }
                            if (string.IsNullOrEmpty(Collection_4.ReportType) == false) //Checking Report type exist   or not.
                            {
                                var Report_Type = Collection_4.ReportType;
                                var CollProduct = Report_Type.Split(',');
                                string Coll = ProductCode.ToUpper();
                                if (CollProduct.Length > 0)
                                {
                                    for (int m = 0; m < CollProduct.Length; m++)
                                    {
                                        if (Coll == CollProduct[m].ToUpper())
                                        {
                                            Report_Exist4 = true;
                                            break;
                                        }
                                    }
                                }
                            }

                            if (Collection_4.Enabled == true) //Checking Report type exist   or not.
                            {
                                if (Isthis_PackageReport4 == false)
                                {
                                    if (Report_Exist4 == true)
                                    {
                                        #region Ticket #173:
                                        try
                                        {
                                            AutoReviewByKeywordsDuringOrderPlace(UpdatedResponse, pkOrderDetailId, ProductCode, IsPendingReport);
                                            //objBALEmergeReview.AutoReviewByKeywords(UpdatedResponse, pkOrderDetailId, ProductCode);
                                            //objBALEmergeReview.AutoReviewByKeywords(strResponseData, pkOrderDetailId, ProductCode);
                                        }
                                        catch
                                        {
                                            FinalResponse = UpdatedResponse;
                                        }
                                        #endregion
                                    }
                                }
                            }
                        }
                    }

                    string Filter_Id = "1";
                    bool Report_Exist = false;
                    bool Isthis_PackageReport = false;
                    var Collection_seven = ObjBALCompany.GetFiltersAccordingFilterId(Filter_Id).FirstOrDefault(); //Getting filter rules 7 Year Filter.
                    if (Collection_seven != null)
                    {
                        if (ObjData != null)
                        {
                            if (ObjData.IsPackageReport == true) //Checking Is this PackageReport  or not.
                            {
                                if (Collection_seven.Packages == true) { Isthis_PackageReport = false; }
                                else { Isthis_PackageReport = true; }
                            }
                            else { Isthis_PackageReport = false; }
                            if (string.IsNullOrEmpty(Collection_seven.ReportType) == false) //Checking Report type exist   or not.
                            {
                                var Report_Type = Collection_seven.ReportType;
                                var CollProduct = Report_Type.Split(',');
                                string Coll = ProductCode.ToUpper();
                                if (CollProduct.Length > 0)
                                {
                                    for (int m = 0; m < CollProduct.Length; m++)
                                    {
                                        if (Coll == CollProduct[m].ToUpper())
                                        {
                                            Report_Exist = true;
                                            break;
                                        }
                                    }
                                }
                            }

                            if (Collection_seven.Enabled == true) //Checking Report type exist   or not.
                            {
                                if (Isthis_PackageReport == false)
                                {
                                    if (Report_Exist == true)
                                    {
                                        #region Ticket #800: 7 YEAR STATES
                                        try
                                        {
                                            string Updated7YearResponse = HideSevenYearDuringOrderPlace(UpdatedResponse, ProductCode, CurrentDate, pkOrderDetailId, IsPendingReport);
                                            UpdatedResponse = Updated7YearResponse;
                                            FinalResponse = Updated7YearResponse;
                                        }
                                        catch
                                        {
                                            FinalResponse = UpdatedResponse;
                                        }
                                        #endregion
                                    }
                                }
                            }
                        }
                    }
                }

                // end if

                if (FinalResponse == string.Empty)
                {
                    FinalResponse = strResponseData;
                }
                if (ProductCode == "OFAC" || ProductCode == "NCR1" || ProductCode == "NCR2" || ProductCode == "NCR+" || ProductCode == "NCR4" || ProductCode == "SCR" || ProductCode == "CCR1" || ProductCode == "CCR2" || ProductCode == "SOR" || ProductCode == "RCX" || ProductCode == "SCRLiveRunner" || ProductCode == "CCR1LiveRunner" || ProductCode == "CCR2LiveRunner")
                {
                    XDocument ObjXDocument = XDocument.Parse(FinalResponse);
                    XMLFilteration ObjXMLFilteration = new XMLFilteration();
                    using (EmergeDALDataContext DX = new EmergeDALDataContext())
                    {
                        if (DX.tblOrderDetails.Where(db => db.pkOrderDetailId == pkOrderDetailId).Count() > 0)
                        {
                            tblOrderDetail ObjOrderDetails = DX.tblOrderDetails.Where(db => db.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                            if (ObjOrderDetails != null)
                            {
                                tblOrder objtblOrder = DX.tblOrders.Where(db => db.pkOrderId == ObjOrderDetails.fkOrderId).FirstOrDefault(); ;
                                if (objtblOrder.HitStatus == 0)
                                {
                                    ObjXMLFilteration.CheckIsHit(ObjOrderDetails.fkOrderId, ProductCode, ObjXDocument);
                                }
                            }
                        }
                        else
                        {
                            tblOrderDetails_Archieve ObjtblOrderDetails_Archieve = DX.tblOrderDetails_Archieves.Where(db => db.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                            if (ObjtblOrderDetails_Archieve != null)
                            {
                                tblOrders_Archieve objtblOrder = DX.tblOrders_Archieves.Where(db => db.pkOrderId == ObjtblOrderDetails_Archieve.fkOrderId).FirstOrDefault(); ;
                                if (objtblOrder.HitStatus == 0)
                                {
                                    ObjXMLFilteration.CheckIsHit(ObjtblOrderDetails_Archieve.fkOrderId, ProductCode, ObjXDocument);
                                }
                            }
                        }
                    }
                }
            }
            return FinalResponse;
        }
        #endregion

        #region INT-173
        #region Add Review and Help Comments

        public string SendEMEditMailRRforECI(int pkOrderDetailId, string txtSuggestionsRR, string hdnIs6MonthOld, string HDNReviewDiffer,
           string HDNReviewSubject, string HDNReportName, string OrderNo, string ApplicantNameForEmail, string MiddleName)
        {
            int UpdatedStatus = 0;
            byte Status = 1;
            string Middle_Name = string.Empty;
            string UMiddle_Name = string.Empty;
            if (string.IsNullOrEmpty(MiddleName) == false)
            {
                Middle_Name = ", This applicant has a middle name or initial on file : " + MiddleName;
                UMiddle_Name = "Middle_Name: " + MiddleName;
            }
            else
            {
                Middle_Name = MiddleName;
                UMiddle_Name = MiddleName;
            }

            BALEmergeReview ObjReview = new BALEmergeReview();
            if (HDNReviewDiffer == "0")
            {
                UpdatedStatus = ObjReview.UpdateHelpWithOrder(pkOrderDetailId, 1, txtSuggestionsRR.Trim() + UMiddle_Name, Convert.ToBoolean(hdnIs6MonthOld));
                if (UpdatedStatus == 1)
                {
                    Status = SendMailRequestingReviewforECI(HDNReviewDiffer, txtSuggestionsRR + Middle_Name, HDNReviewSubject, HDNReportName, OrderNo, ApplicantNameForEmail.Replace("_", " "));
                }
            }
            else
            {
                UpdatedStatus = ObjReview.UpdateReportReviewStatus(pkOrderDetailId, 1, txtSuggestionsRR.Trim() + UMiddle_Name, Convert.ToBoolean(hdnIs6MonthOld));
                if (UpdatedStatus == 1)
                {
                    Status = SendMailRequestingReviewforECI(HDNReviewDiffer, txtSuggestionsRR + Middle_Name, HDNReviewSubject, HDNReportName, OrderNo, ApplicantNameForEmail.Replace("_", " "));
                }
            }
            string Message = string.Empty;
            if (Status == 1)
            {
                Message = "<span class='SuccessMsg'>Your Emerge Review has been submitted successfully.</span>";
            }
            else
            {
                Message = "<span class='ErrorMsg'>Some Error Occurred. Please Try Again.</span>";
            }
            return Message;
        }

        private byte SendMailRequestingReviewforECI(string HDNReviewDiffer, string txtSuggestionsRR, string HDNReviewSubject, string HDNReportName, string OrderNo, string ApplicantNameForEmail)
        {
            string emailText = "";
            string MessageBody = "";
            int PkTemplateId = 0;
            StringBuilder Comments = new StringBuilder();
            Comments.Append(" <table width=\"97%\"><tr><td>Report ID : </td><td>" + OrderNo + "</td>"
                + "</tr> <tr><td>Report Name : </td><td>" + HDNReportName + "</td></tr>"
       + "<tr><td>User : </td><td>" + System.Web.HttpContext.Current.Session["UserFullName"].ToString() + " (<b>UserName : </b>" + HttpContext.Current.User.Identity.Name + ") " + "</td></tr>"
       + " <tr><td valign=\"top\"  style=\"width:150px;\">Additional Comments :</td><td>" + txtSuggestionsRR.Trim() + "</td></tr>  </table>");
            string BoxHeaderText = string.Empty;
            BoxHeaderText = HDNReviewSubject;
            BALGeneral ObjBALGeneral = new BALGeneral();
            //  string WebsiteLogo = "https://www.intelifi.com/emerge/Resources/Upload/Images/emerge-logo--no-shadows-(small).gif";
            string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/emerge-logo--no-shadows-(small).gif";

            if (HDNReviewDiffer == "0")
            {

                PkTemplateId = Convert.ToInt32(EmailTemplates1.HelpWithOrder);
            }
            else
            {
                PkTemplateId = Convert.ToInt32(EmailTemplates1.EmergeReview);

            }
            //For email status//
           // BALGeneral objbalgenral = new BALGeneral();
           // MembershipUser ObjMembershipUser = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            //Guid UserId = Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
            //int pktemplateid = PkTemplateId;
            //Guid user_ID = UserId;
            //int fkcompanyuserid = 0;
            //bool email_status = true;//objbalgenral.UserEmailStatusCheck(user_ID, pktemplateid, fkcompanyuserid);


            var emailContent = ObjBALGeneral.GetEmailTemplate(BALGeneral.SiteApplicationId1, PkTemplateId);// "Emerge Suggestions"
            emailText = emailContent.TemplateContent;

            BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
            #region  For New Style SystemTemplate
            string Bookmarkfile = string.Empty;
            string bookmarkfilepath = string.Empty;

            Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
            Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
            Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
            emailText = Bookmarkfile;
            #endregion


            #region New way Bookmark


            Bookmarks objBookmark = new Bookmarks();

            objBookmark.EmailContent = emailText;

            objBookmark.Subject = BoxHeaderText;
            if (ApplicantNameForEmail != null)
            {
                objBookmark.ApplicantName = ApplicantNameForEmail;/* Session["UserFullName"].ToString();*/
            }
            objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
            objBookmark.UserCompany = ApplicantNameForEmail;
            objBookmark.UserEmail = System.Web.HttpContext.Current.Session["UserFullName"].ToString() + " (<b>UserName : </b>" + System.Web.HttpContext.Current.User.Identity.Name + ") ";
            objBookmark.EmergeAssistComment = Comments.ToString();
            objBookmark.EmergeReviewComment = Comments.ToString();
            objBookmark.Suggestion = Comments.ToString();

            MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, BALGeneral.GetUID1(System.Web.HttpContext.Current.User.Identity.Name.ToString()));

            #endregion
            //For email status//
            //string MsgBeforeEmail = "Emerge Reviewed for Order No:-" + OrderNo;
            string UserIdentityName = System.Web.HttpContext.Current.User.Identity.Name;
            if (HDNReviewDiffer != "0")
            {
                // WriteReviewStatusInTextFile(MsgBeforeEmail, UserIdentityName);
            }
            //string successClient = string.Empty;
            string successAdmin = string.Empty;
            //if (email_status == true)
            //{
            successAdmin = BALEmergeReview.SendMail(ConfigurationManager.AppSettings["ReportReviewMail"].ToString(), UserIdentityName, string.Empty, ConfigurationManager.AppSettings["ReportReviewFromAddress"].ToString(), MessageBody, BoxHeaderText);
            //successClient = BALEmergeReview.SendMail(User.Identity.Name, string.Empty, string.Empty, ConfigurationManager.AppSettings["ReportReviewFromAddress"].ToString(), MessageBody, BoxHeaderText);
            //}
            //     //FillPopUp();

            if (HDNReviewDiffer == "0")
            {
                if (successAdmin == "Success")
                {
                    return 1;
                }
                else
                {
                    return 2;
                }
            }
            else
            {
                if (successAdmin == "Success")
                {
                    //string SucessMsg = "Email Successfully Sent for Order No:-" + OrderNo;
                    //WriteReviewStatusInTextFile(SucessMsg, ConfigurationManager.AppSettings["ReportReviewMail"].ToString());
                    return 1;
                }
                else
                {
                    string UnSucessMsg = "Email Sending Failed for Order No:-" + OrderNo;
                    UnSucessMsg = UnSucessMsg + " Error:" + successAdmin;
                    //WriteReviewStatusInTextFile(UnSucessMsg, ConfigurationManager.AppSettings["ReportReviewMail"].ToString());
                    return 2;
                }
            }


        }

        public static Guid SiteApplicationId1 = new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852");
        public static Guid GetUID1(string Username)
        {
            Guid UserId = Guid.Empty;
            try
            {
                MembershipUser Obj = Membership.GetUser(Username);
                if (Obj != null)
                {
                    UserId = new Guid(Obj.ProviderUserKey.ToString());
                }
            }
            catch { }
            return UserId;
        }

        #endregion
        #endregion

        /// <summary>
        /// To check if any order is older than 60 days.
        /// Marked status as pending to in-complete.
        /// Send a mail to admin (support@intelifi.com).
        /// </summary>        
        public void checkOrderPeriod(int? orderId, int? OrderDetailId, string productCode, string error)
        {
            try
            {
                //Database instance
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    //To get the selected order created date.
                    var OrderDt = DX.tblOrderDetails.Where(x => x.pkOrderDetailId == OrderDetailId).Select(x => x.OrderDt).FirstOrDefault();
                    if (OrderDt != null)
                    {
                        //To check if, selected order older than 60
                        int ts = DateTime.Now.Subtract(OrderDt.Value).Days;
                        if (ts > 60)
                        {
                            //Set reportstatus pending to in-complete
                            new BALOrders().UpdateReportStatus(orderId, OrderDetailId, Convert.ToByte("3"));
                            //Send a mail to admin (support@intelifi.com), regarding in-completion.
                            new BALOrders().SendErrorEmail(orderId, error, productCode, 3, (int)OrderDetailId);
                        }
                        else
                        {
                            new BALOrders().UpdateReportStatus(orderId, OrderDetailId, Convert.ToByte("1")); // for pending int 206
                        }

                    }
                }
            }
            catch //(Exception ee)
            {

            }
        }

        /// <summary>
        /// To write the log if any criminal case deleted by admin or other criteria.
        /// </summary>
        /// <param name="Content"></param>
        /// <param name="FilePrefix"></param>
        public static void WriteLogForDeletion(string Content, string FilePrefix)
        {
            try
            {
                string path = System.Configuration.ConfigurationManager.AppSettings["LogPathForCaseDeletion"];
                string FileName = path + FilePrefix + ".txt";
                StringBuilder sb = new StringBuilder();
                StreamWriter sw;
                if (!File.Exists(FileName))
                {
                    sw = File.CreateText(FileName);
                    sb.Append(Content);
                    sw.WriteLine(sb);
                    sw.Close();
                }
                else
                {
                    sw = File.AppendText(FileName);
                    sb.Append(Content);
                    sw.WriteLine(sb);
                    sw.Close();
                }
            }
            catch //(Exception ee)
            {

            }
        }

    }


    public enum EmailTemplates1
    {
        NewUserRegistration = 1,
        PendingReports = 5,
        ReportRequest = 6,
        PackageRequest = 7,
        PasswordRecovery = 12,
        EmailedReportRequest = 13,
        EmergeEdit = 15,
        ProductOrdered = 18,
        ReviewOrder = 19,
        HelpWithOrder = 20,
        EmergeReview = 27,
        DemoSchedule = 31,
        SendDocument = 32,
        AccountStatusChange = 33,
        QualityControlSurvey = 34,
        DemoCompleted = 35,
        EmailBillingStatement = 37,
        MonthlyBillingStatement = 38,
        ReportsNotGenerating = 41,
        SyncpodActivation = 42,
        PendingReportUpdate = 45,
        AdverseLetter = 48,
        ReportReminder = 23,
        NewOrder = 29,
        IncompleteReport = 30,
        ManualOrderPlaced = 36,
        BatchProcessNotification = 40,
        EmergeReviewReport = 43,
        PendingReport = 44,
        Notice613ForPS = 63
    }



    public class clsCounty
    {
        public string CountyName { get; set; }
        public bool IsRcxCounty { get; set; }
        public string pkCountyId { get; set; }

    }
    public class clsStates
    {
        public string StateName { get; set; }
        public bool IsLiveRunnerState { get; set; }
        public string pkStateId { get; set; }

    }

    //public class clsStateOfEmployment
    //{
    //    public string StateName { get; set; }
    //    public bool IsLiveRunnerState { get; set; }
    //    public string pkStateId { get; set; }

    //}

    public class clsJurisdiction
    {
        public string JurisdictionName { get; set; }
        public string pkJurisdictionId { get; set; }


    }

    public class clsStateFee
    {
        public int pkStateFeeId { get; set; }
        public int? fkProductId { get; set; }
        public decimal AdditionalFee { get; set; }
        public bool IsConsent { get; set; }
        public bool IsEnabled { get; set; }

        public int pkStateId { get; set; }
        public int fkCountryId { get; set; }
        public string StateName { get; set; }
        public string StateCode { get; set; }
        public string DisclaimerData { get; set; }
        public bool MVRFee { get; set; }
        public bool SCRFee { get; set; }
        public bool IsStateLiveRunner { get; set; }
        public decimal LiveRunnerFees { get; set; }
        public bool CriminalFilter { get; set; }
    }

    public class ShipToAddress
    {
        public int fkOrderId { get; set; }
        public int LocationId { get; set; }
        public int CompanyUserId { get; set; }
        public string CompanyName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string StateCode { get; set; }
        public string StateName { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
    }
}
