﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Emerge.Data;

namespace Emerge.Services
{
    public class BALCompanyType
    {
     public List<tblCompanyType> GetAllCompanyType()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.tblCompanyTypes.OrderBy(db => db.CompanyType).ToList<tblCompanyType>();
            }
        }
     public List<tblPermissiblePurpose> GetPermissiblePurpose()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.tblPermissiblePurposes.ToList<tblPermissiblePurpose>();
            }
        }
     public decimal Get_County_Fee(string Countyname,string StateCode, int locationId, string productCode)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
               // Guid pkproductId = new Guid("6e1082a8-126f-41d3-90f5-9da0b79d0ed6");
                int pkproductId = 3;
                var vIsLivRun1 = ObjDALDataContext.tblCounties.Join(ObjDALDataContext.tblStates,Counties=>Counties.fkStateId,States=>States.pkStateId,
                                                                        (Counties,States) => new
                                                                        {
                                                                            CountyName = Counties.CountyName,
                                                                            StateCode = States.StateCode,
                                                                            IsRcxCounty = Counties.IsRcxCounty
                                                                        })
                                                                        .FirstOrDefault(n => n.CountyName.ToLower().Trim() == Countyname.ToLower().Trim() && n.StateCode.ToLower().Trim() == StateCode.ToLower().Trim());
                bool vIsLivRun = vIsLivRun1 != null ? vIsLivRun1.IsRcxCounty : false; 
                if (vIsLivRun)
                {
                    try
                    {
                        vIsLivRun = ObjDALDataContext.tblProductAccesses.Join(ObjDALDataContext.tblProductPerApplications, ProductAccesses => ProductAccesses.fkProductApplicationId,
                                                                                ProductPerApplications => ProductPerApplications.pkProductApplicationId,
                                                                                (ProductAccesses, ProductPerApplications) => new
                                                                                {
                                                                                    ProductCode = ProductPerApplications.ProductCode,
                                                                                    locationId = ProductAccesses.fkLocationId,
                                                                                    IsLiveRunner = ProductAccesses.IsLiveRunner
                                                                                })
                                                                                .Where(n => n.ProductCode.ToLower().Trim() == productCode.ToLower().Trim() && n.locationId == locationId).FirstOrDefault().IsLiveRunner;

                        vIsLivRun = vIsLivRun != null ? vIsLivRun : false;
                    }
                    catch //(Exception ex)
                    {
                        vIsLivRun = false;
                    }
                }
                if (vIsLivRun)
                {
                    var query = ObjDALDataContext.tblCounties.Join(ObjDALDataContext.tblCountyFees, Counties => Counties.pkCountyId, CountyFees => CountyFees.fkCountyId,
                                                                    (Counties, CountyFees) => new 
                                                                    { 
                                                                        Counties,
                                                                        CountyFees
                                                                    })
                                                              .Join(ObjDALDataContext.tblProducts, firstJoin => firstJoin.CountyFees.fkProductId, Products => Products.pkProductId,
                                                                    (firstJoin, Products) => new
                                                                    {
                                                                        firstJoin,
                                                                        Products
                                                                    })
                                                              .Join(ObjDALDataContext.tblProductPerApplications, SecondJoin => SecondJoin.Products.pkProductId, ppa => ppa.fkProductId,
                                                                    (SecondJoin, ppa) => new
                                                                    {
                                                                        SecondJoin, ppa                                                                       
                                                                    })
                                                              .Join(ObjDALDataContext.tblStates, thirdJoin=>thirdJoin.SecondJoin.firstJoin.Counties.fkStateId,States=>States.pkStateId,
                                                                    (thirdJoin,States)=> new
                                                                    {
                                                                        AdditionalFee = thirdJoin.SecondJoin.firstJoin.CountyFees.AdditionalFee != null ? thirdJoin.SecondJoin.firstJoin.CountyFees.AdditionalFee : 0,
                                                                        ProductCode = thirdJoin.ppa.ProductCode,
                                                                        CountyName = thirdJoin.SecondJoin.firstJoin.Counties.CountyName.ToLower(),
                                                                        stateCode = States.StateCode
                                                                    })
                                                              .FirstOrDefault(n =>n.CountyName.ToLower().Trim() == Countyname.ToLower().Trim() && 
                                                                                        n.ProductCode.ToLower().Trim() == ("RCX").ToLower().Trim() &&
                                                                                        n.stateCode.ToLower().Trim() == StateCode.ToLower().Trim()                                                                                                 
                                                                              );


                    var query1 = query.AdditionalFee == null ? 0 : query.AdditionalFee;
                    return query1;
                }
                else
                {
                    var query = (from u in ObjDALDataContext.tblCounties
                                 join ut in ObjDALDataContext.tblCountyFees on u.pkCountyId equals ut.fkCountyId
                                 join state in ObjDALDataContext.tblStates on u.fkStateId equals state.pkStateId
                                 where ( ut.fkProductId == pkproductId &&
                                 u.CountyName.ToLower().Trim() == Countyname.ToLower().Trim() && 
                                 state.StateCode.ToLower().Trim() == StateCode.ToLower().Trim()
                                 ) select ut.AdditionalFee).FirstOrDefault();
                    query = query == null ? 0 : query;
                    return query;
                }
            }
        }
     public string Get_State_Fee(string Statename)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                //Guid pkproductId = new Guid("83ca1bd9-0213-4da3-b1ef-24b20b7f3738");
                int pkproductId = 44;
                bool vIsLivRun = ObjDALDataContext.tblStates.Where(d => d.StateName == Statename).FirstOrDefault().IsStateLiveRunner;
                vIsLivRun = vIsLivRun == null ? false : vIsLivRun;
                if (vIsLivRun)
                {
                    var query = ObjDALDataContext.tblStates.FirstOrDefault(n => n.StateName == Statename).LiveRunnerFees;
                    return query.ToString();
                }
                else
                {
                    var query = (from u in ObjDALDataContext.tblStates
                                 join ut in ObjDALDataContext.tblStateFees on u.pkStateId equals ut.fkStateId
                                 where ut.fkProductId == pkproductId &&
                                 u.StateName == Statename
                                 orderby ut.pkStateFeeId
                                 select ut.AdditionalFee).FirstOrDefault();

                    return query.ToString();
                }
            }





        }
     public int UpdateApiInformation(tblCompany _ObjtblCompany)
        {
            int success = 0;
            tblCompany ObjtblCompany = new tblCompany();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                try
                {
                    ObjtblCompany = DX.tblCompanies.Where(d => d.pkCompanyId == _ObjtblCompany.pkCompanyId).FirstOrDefault();
                    if (ObjtblCompany != null)
                    {
                        ObjtblCompany.EnableAPI = _ObjtblCompany.EnableAPI;
                        ObjtblCompany.Integration = _ObjtblCompany.Integration;
                        ObjtblCompany.APIUsername = _ObjtblCompany.APIUsername;
                        ObjtblCompany.APIPassword = _ObjtblCompany.APIPassword;
                        ObjtblCompany.PaymentTerms = _ObjtblCompany.PaymentTerms;
                        ObjtblCompany.CompanyLogo = _ObjtblCompany.CompanyLogo;
                        //ObjtblCompany.APILocation = _ObjtblCompany.APILocation;
                        ObjtblCompany.APILocations = _ObjtblCompany.APILocations;
                        ObjtblCompany.ApiUrl = _ObjtblCompany.ApiUrl;
                       
                        DX.SubmitChanges();
                        success = 1;
                    }
                }
                catch
                {
                    success = -1;
                }
            }
            return success;
        }
     public int GetApiLocation(Guid UserId, int CompanyID, string ApiUrl)
        {
           #region-----Old code
            //using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            //{
            //    Guid User_Id = UserId;
            //    var query = (from tc in ObjDALDataContext.tblCompanyUsers
            //                 join tl in ObjDALDataContext.tblLocations on tc.fkLocationId equals tl.pkLocationId
            //                 join tblc in ObjDALDataContext.tblCompanies on tl.fkCompanyId equals tblc.pkCompanyId
            //                 where tc.fkUserId == User_Id && tblc.pkCompanyId == CompanyID && tblc.ApiUrl == ApiUrl
            //                 select tblc.APILocation).FirstOrDefault();
            //   if (query == null)  {  return 3; }
            //   else {  return 1; }

            //}
           #endregion

            #region---New code
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {

                Guid User_Id = UserId;
                //INT-106
                List<string> result = new List<string>();
                var location = (from tc in ObjDALDataContext.tblCompanies
                                where tc.pkCompanyId == CompanyID
                                select tc.APILocations).FirstOrDefault();
                if (location != null)
                {
                    result = location.Split(',').ToList();
                }

                var query = (from tc in ObjDALDataContext.tblCompanies
                             join tl in ObjDALDataContext.tblLocations on tc.pkCompanyId equals tl.fkCompanyId
                             join tcu in ObjDALDataContext.tblCompanyUsers on tl.pkLocationId equals tcu.fkLocationId
                             where tcu.fkUserId == User_Id && tc.pkCompanyId == CompanyID && tc.ApiUrl == ApiUrl && result.Contains(tcu.fkLocationId.ToString())
                             select tc.APILocations).FirstOrDefault();

                if (query == null)
                {
                    return 3;
                }
                else
                {
                    return 1;
                }
            }
            #endregion

        }
     public Tuple<string, string> GetCountyState(int Zip)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                int ZipCode = Zip;
                var query = (from u in ObjDALDataContext.tblStates
                             join ut in ObjDALDataContext.tblcountystates on u.StateCode equals ut.state
                             where ut.zip == ZipCode
                             select new Clscountystate { StateName = u.StateName, CountyName = ut.county }).FirstOrDefault();
                string state = string.Empty;
                string county = string.Empty;
                if (query != null)
                {
                    state = query.StateName;
                    county = query.CountyName;
                }
                return new Tuple<string, string>(state, county);
            }

        }
     public int Get_apiurlexist(string apiurl)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {

                var query = (from u in ObjDALDataContext.tblCompanies
                             where u.ApiUrl == apiurl

                             select u.CompanyName).FirstOrDefault();
                if (query != null)  { return 2;  }
                else { return 1; }


            }
        }
     public int Get_apiurlexistcompany(string apiurl, int pkCompanyId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {

                var query = (from u in ObjDALDataContext.tblCompanies
                             where u.ApiUrl == apiurl && u.pkCompanyId != pkCompanyId

                             select u.CompanyName).FirstOrDefault();
                if (query != null) { return 2;  }
                else  { return 1; }


            }
       }
     public string GetAlreadyexistapiurl(int pkCompanyId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                var query = (from u in ObjDALDataContext.tblCompanies
                             where u.pkCompanyId == pkCompanyId
                             select u.ApiUrl).FirstOrDefault();
                return query;

            }

        }
     public Tuple<string, int> GetRequestData(int OrderDetailId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                var query = (from u in ObjDALDataContext.tblOrderRequestDatas

                             where u.fkOrderDetailId == OrderDetailId
                             select new { u.RequestData, u.pkOrderRequestId }).FirstOrDefault();
                string RequestData = string.Empty;
                int pkOrderRequestId = 0;
                if (query != null)
                {
                    RequestData = query.RequestData;
                    pkOrderRequestId = query.pkOrderRequestId;
                }
                return new Tuple<string, int>(RequestData, pkOrderRequestId);
            }


        }
     public Tuple<int> GetCity_Id(string CityName)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                var query = (from u in ObjDALDataContext.tblCities

                             where u.City == CityName
                             select new { u.pkCityId }).FirstOrDefault();
                int City_Id = 0;
                if (query != null)
                {
                    City_Id = query.pkCityId;
                }
                return new Tuple<int>(City_Id);
            }
        }
 }

    public class Clscountystate
    {
        public string CountyName { get; set; }
        public string StateName { get; set; }
    }
    public class ClsCityName
    {
        public string CountyName { get; set; }
        public string StateName { get; set; }

    }
}
