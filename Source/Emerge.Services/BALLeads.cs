﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;
using System.Data.Linq;
using System.Data.Common;
using System.Data;
using System.Web.Security;

namespace Emerge.Services
{
    public class BALLeads
    {

        EmergeDALDataContext ObjEmergeDALDataContext;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <param name="IsPaging"></param>
        /// <param name="SearchString"></param>
        /// <param name="CompanyType"></param>
        /// <param name="StatusType"></param>
        /// <param name="SortingColumn"></param>
        /// <param name="SortingDirection"></param>
        /// <returns></returns>
        public List<Proc_Get_AllLeadsResult> GetAllLeads(int PageIndex, int PageSize, bool IsPaging, string SearchString,
            Int16 CompanyType, Int16 StatusType, string SortingColumn,
            string SortingDirection, string FromDate, string ToDate,
            byte LeadStatus, Guid SalesManagerId, Guid SalesRepId, Guid CurrentUserId, string LeadSource, byte LeadType, string filterColumn, byte filterAction, string filterValue, string LeadNextStep, string SalesAssosiate)
        {
            using (ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.GetAllLeads(PageIndex, PageSize, IsPaging, SearchString, CompanyType, StatusType,
                                                                  SortingColumn, SortingDirection, FromDate, ToDate,
                                                                  LeadStatus, SalesManagerId, SalesRepId, CurrentUserId, LeadSource, LeadType, filterColumn, filterAction, filterValue, LeadNextStep, SalesAssosiate).ToList<Proc_Get_AllLeadsResult>();
            }
        }

        public tblCompany GetCompanyIdByComapnyName(string CompnayName)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                tblCompany ObjRspnData = DX.tblCompanies.Where(db => db.CompanyName == CompnayName).FirstOrDefault();
                return ObjRspnData;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <param name="IsPaging"></param>
        /// <param name="SearchString"></param>
        /// <param name="CompanyType"></param>
        /// <param name="StatusType"></param>
        /// <param name="SortingColumn"></param>
        /// <param name="SortingDirection"></param>
        /// <returns></returns>
        public List<Proc_Get_LeadsByUserIdResult> GetLeadsByCurrentUserId(int PageIndex, int PageSize, bool IsPaging, string SearchString,
            Int16 CompanyType, Int16 StatusType, string SortingColumn,
            string SortingDirection, byte LeadStatus, Guid SalesManagerId, Guid SalesRepId, Guid CurrentUserId, string LeadSource, byte LeadType)
        {
            using (ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.GetLeadsByUserId(PageIndex, PageSize, IsPaging, SearchString, CompanyType, StatusType,
                                                                  SortingColumn, SortingDirection, LeadStatus, SalesManagerId, SalesRepId, CurrentUserId, LeadSource, LeadType).ToList<Proc_Get_LeadsByUserIdResult>();
            }
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="PageIndex"></param>
        ///// <param name="PageSize"></param>
        ///// <param name="IsPaging"></param>
        ///// <param name="SearchString"></param>
        ///// <param name="CompanyType"></param>
        ///// <param name="StatusType"></param>
        ///// <param name="SortingColumn"></param>
        ///// <param name="SortingDirection"></param>
        ///// <returns></returns>
        public List<Proc_Get_AllClientsResult> GetAllClients(int PageIndex, int PageSize, bool IsPaging, string SearchString,
            Int16 CompanyType, string StatusType, string SortingColumn,
            string SortingDirection, string FromDate, string ToDate, Guid SalesManagerId, Guid SalesRepId, Guid CurrentUserId, string LeadSource, string filterColumn, byte filterAction, string filterValue)
        {
            using (ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.Get_AllClients(PageIndex, PageSize, IsPaging, SearchString, CompanyType, StatusType,
                                                                  SortingColumn, SortingDirection, FromDate, ToDate, SalesManagerId, SalesRepId, CurrentUserId, LeadSource, filterColumn, filterAction, filterValue).ToList<Proc_Get_AllClientsResult>();
            }
        }


        public List<Proc_Get_AllClientsAndLeadResult> GetAllLeadAndClientByUserid(Guid UserId, string SerachingString, bool Isadmin)
        {
            using (ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.Get_AllClientsAndLead(SerachingString, UserId, Isadmin).ToList();
            }
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="PageIndex"></param>
        ///// <param name="PageSize"></param>
        ///// <param name="IsPaging"></param>
        ///// <param name="SearchString"></param>
        ///// <param name="CompanyType"></param>
        ///// <param name="StatusType"></param>
        ///// <param name="SortingColumn"></param>
        ///// <param name="SortingDirection"></param>
        ///// <returns></returns>

        public List<Proc_Get_ClientsByUserIdResult> GetClientsByCurrentUserId(int PageIndex, int PageSize, bool IsPaging, string SearchString,
            Int16 CompanyType, string StatusType, string SortingColumn,
            string SortingDirection, Guid SalesManagerId, Guid SalesRepId, Guid CurrentUserId, string LeadSource)
        {
            using (ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.GetClientsByUserId(PageIndex, PageSize, IsPaging, SearchString, CompanyType, StatusType,
                                                                  SortingColumn, SortingDirection, SalesManagerId, SalesRepId, CurrentUserId, LeadSource).ToList<Proc_Get_ClientsByUserIdResult>();
            }
        }


        [Serializable]
        public class cTrainee
        {
            public int pkCompanyTraineeId { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Title { get; set; }
            public string Phone { get; set; }
            public string Ext { get; set; }
            public string EmailId { get; set; }
        }

        ///// <summary>
        ///// Function to Add New Company Lead 
        ///// </summary>
        ///// <param name="ObjtblCompany"></param>
        public List<int> InsertNewCompanyLead(tblCompany ObjtblCompany, tblLocation ObjtblLocation, string LeadComments)
        {
            List<int> ObjGuid_CompLocation = new List<int>();

            BALLocation ObjBALLocation;

            ObjBALLocation = new BALLocation();
            //DbTransaction ObjDbTransaction = null;
            int NewLocationId = 0;
            int CompanyId = 0;
            try
            {
                using (ObjEmergeDALDataContext = new EmergeDALDataContext())
                {

                    var CompanyResult = ObjEmergeDALDataContext.AddCompanyLead(
                                                 ObjtblCompany.fkCompanyTypeId,
                                                 ObjtblCompany.fkApplicationId,
                                                 ObjtblCompany.CompanyCode,
                                                 ObjtblCompany.fkSalesRepId,
                                                 ObjtblCompany.fkSalesManagerId,
                                                 ObjtblCompany.CompanyAccountNumber,
                                                 ObjtblCompany.CompanyNote,
                                                 ObjtblCompany.CompanyName,
                                                 ObjtblCompany.FranchiseName,
                                                 ObjtblCompany.MainContactPersonName,
                                                 ObjtblCompany.BillingContactPersonName,
                                                 ObjtblCompany.MainEmailId,
                                                 ObjtblCompany.BillingEmailId,
                                                 ObjtblCompany.LeadSource,
                                                 ObjtblCompany.LeadNumber,
                                                 ObjtblCompany.CurrentProvider,
                                                 ObjtblCompany.NoOfLocations,
                                                 ObjtblCompany.LeadStatus,
                                                 ObjtblCompany.Priority1,
                                                 ObjtblCompany.Priority2,
                                                 ObjtblCompany.Priority3,
                                                  ObjtblCompany.IsEnabled,
                                                 ObjtblCompany.IsDeleted,
                                                 ObjtblCompany.CreatedDate,
                                                 ObjtblCompany.LastModifiedDate,
                                                 ObjtblCompany.CreatedById,
                                                 ObjtblCompany.LastModifiedById,
                                                 ObjtblCompany.IsEnableReview,
                                                 ObjtblCompany.Priority1Notes,
                                                 ObjtblCompany.Priority2Notes,
                                                 ObjtblCompany.Priority3Notes,
                                                 DateTime.Now,
                                                 ObjtblCompany.IsMainEmail,
                                                ObjtblCompany.CompanyUrl,
                                                ObjtblCompany.SecondaryMainContactPersonName,
                                                ObjtblCompany.MainTitle,
                                                ObjtblCompany.SecondaryTitle,
                                                 ObjtblCompany.SecondaryEmail,
                                                 ObjtblCompany.MonthlyVolume,
                                                 ObjtblCompany.NoofEmployees,
                                                 ObjtblCompany.LeadSourceId,
                                                 ObjtblCompany.ApptDate,
                                                 ObjtblCompany.NextStep
                                                 );




                    CompanyId = (int)CompanyResult.First().CompanyId;

                    ObjGuid_CompLocation.Add(CompanyId == 0 ? 0 : CompanyId);
                    if (LeadComments != null && LeadComments != "")
                    {
                        tblCallLog objtblCallLog = new tblCallLog();
                        objtblCallLog.CreatedDate = DateTime.Now;
                        objtblCallLog.fkCompanyId = CompanyId;
                        objtblCallLog.fkUserId = ObjtblCompany.CreatedById.Value;
                        objtblCallLog.LeadComments = LeadComments;
                        // objtblCallLog.pkCallLogId = Guid.NewGuid();
                        ObjEmergeDALDataContext.tblCallLogs.InsertOnSubmit(objtblCallLog);
                        ObjEmergeDALDataContext.SubmitChanges();
                    }
                    NewLocationId = ObjBALLocation.InsertNewLocation(CompanyId, ObjtblLocation);

                    ObjGuid_CompLocation.Add(NewLocationId == 0 ? 0 : NewLocationId);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
            return ObjGuid_CompLocation;
        }


        ///// <summary>
        ///// Function To Update Lead 
        ///// </summary>
        ///// <returns></returns>
        public int UpdateLead(tblCompany ObjtblCompany, tblLocation ObjtblLocation, string LeadComments)
        {
            DbTransaction ObjDbTransaction = null;
            BALLocation ObjBALLocation;
            using (ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                ObjEmergeDALDataContext.Connection.Open();
                ObjDbTransaction = ObjEmergeDALDataContext.Connection.BeginTransaction();
                ObjEmergeDALDataContext.Transaction = ObjDbTransaction;

                int Result = ObjEmergeDALDataContext.UpdateLead(ObjtblCompany.pkCompanyId,
                                                        ObjtblCompany.fkSalesRepId,
                                                        ObjtblCompany.CompanyAccountNumber,
                                                        ObjtblCompany.fkCompanyTypeId,
                                                        ObjtblCompany.CompanyName,
                                                        ObjtblCompany.CompanyNote,
                                                        ObjtblCompany.FranchiseName,
                                                        ObjtblCompany.MainContactPersonName,
                                                        ObjtblCompany.BillingContactPersonName,
                                                        ObjtblCompany.MainEmailId,
                                                        ObjtblCompany.BillingEmailId,
                                                        ObjtblCompany.IsEnabled,
                                                        ObjtblCompany.LastModifiedDate,
                                                        ObjtblCompany.LastModifiedById,
                                                        ObjtblCompany.IsEnableReview,
                                                        ObjtblCompany.fkSalesManagerId,
                                                        ObjtblCompany.LeadSource,
                                                        ObjtblCompany.LeadNumber,
                                                        ObjtblCompany.CurrentProvider,
                                                        ObjtblCompany.NoOfLocations,
                                                        ObjtblCompany.Priority1,
                                                        ObjtblCompany.Priority2,
                                                        ObjtblCompany.Priority3,
                                                        ObjtblCompany.LeadStatus,
                                                        ObjtblCompany.Priority1Notes,
                                                        ObjtblCompany.Priority2Notes,
                                                        ObjtblCompany.Priority3Notes,
                                                        ObjtblCompany.IsMainEmail,
                                                        ObjtblCompany.CompanyUrl,
                                                        ObjtblCompany.SecondaryMainContactPersonName,
                                                        ObjtblCompany.MainTitle,
                                                        ObjtblCompany.SecondaryTitle,
                                                        ObjtblCompany.SecondaryEmail,
                                                        ObjtblCompany.MonthlyVolume,
                                                        ObjtblCompany.NoofEmployees,
                                                        ObjtblCompany.LeadSourceId,
                                                        ObjtblCompany.ApptDate,
                                                        ObjtblCompany.NextStep
                                                           );

                if (Result < 0)
                {
                    if (ObjDbTransaction != null)
                    {
                        ObjDbTransaction.Rollback();
                    }
                    return -2;//false   //-2 error in company updating
                }
                if (LeadComments != "" && LeadComments != null)
                {
                    try
                    {
                        tblCallLog objtblCallLog = new tblCallLog();
                        objtblCallLog.CreatedDate = DateTime.Now;
                        objtblCallLog.fkCompanyId = ObjtblCompany.pkCompanyId;
                        objtblCallLog.fkUserId = ObjtblCompany.LastModifiedById.Value;
                        objtblCallLog.LeadComments = LeadComments;
                        //objtblCallLog.pkCallLogId = Guid.NewGuid();
                        ObjEmergeDALDataContext.tblCallLogs.InsertOnSubmit(objtblCallLog);
                        ObjEmergeDALDataContext.SubmitChanges();
                        //ObjEmergeDALDataContext.AddCommentsToCallLog(ObjtblCompany.pkCompanyId, ObjtblCompany.LastModifiedById, LeadComments, DateTime.Now);

                    }
                    catch //(Exception ex)
                    {
                        if (ObjDbTransaction != null)
                        {
                            ObjDbTransaction.Rollback();
                        }
                        return -7;//false   //-5 error in call Log updating
                    }
                }
                ObjBALLocation = new BALLocation();

                int LocationResult = ObjBALLocation.UpdateLocation(ObjtblLocation, ObjtblCompany.pkCompanyId);

                if (LocationResult != 1)
                {
                    if (ObjDbTransaction != null)
                    {
                        ObjDbTransaction.Rollback();
                    }
                    return -3;//LocationResult // -3 error in Location updating

                }

                ObjDbTransaction.Commit();

                return 1;//LocationResult //1 for success
            }
        }

        /// <summary>
        /// Function To Get Distinct Lead Source
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>            
        public List<Proc_GetDistinctLeadSourceResult> GetLeadSource()
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.GetDistinctLeadSource().ToList<Proc_GetDistinctLeadSourceResult>();
            }
        }
        //ND-21
        public bool GetCountByDateTime(Int16 companyId, DateTime demoDateTime)
        {

            if (companyId == 0)
                return false;
            try
            {
                using (ObjEmergeDALDataContext = new EmergeDALDataContext())
                {

                    return (ObjEmergeDALDataContext.tblDemoSchedules.Where(x => x.DemoDateTime == demoDateTime && x.fkCompanyId == companyId).Count() > 0);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }


        public List<Proc_GetSaleMangerByMainPhoneNoResult> GetSaleMangerByMainPhoneNo(string MainPhoneNo, string companyName)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.GetSaleMangerByMainPhoneNo(MainPhoneNo, companyName).ToList();
            }
        }

        ///// <summary>
        ///// Function To Add Recent Reviewed Lead 
        ///// </summary>
        ///// <returns></returns>
        //public int AddRecentReviewedLead(Guid UserId, Guid pkCompanyId)
        //{
        //    int iresult = 0;
        //    using (EmergeDALDataContext DX = new EmergeDALDataContext())
        //    {
        //        DX.AddRecentLeads(UserId, pkCompanyId);
        //        DX.SubmitChanges();
        //        iresult = 1;
        //    }
        //    return iresult;
        //}

        ///// <summary>
        ///// Function To Get Recent Reviews by UserId
        ///// </summary>
        ///// <param name="UserId"></param>
        ///// <returns></returns>            
        //public List<Proc_Get_Recent_ReviewLeadsResult> GetRecentReviewLeads(Guid UserId)
        //{
        //    using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
        //    {
        //        return ObjEmergeDALDataContext.GetRecentReviewLeads(UserId).ToList<Proc_Get_Recent_ReviewLeadsResult>();
        //    }
        //}

        ///// <summary>
        ///// Function To Add Comments to Call Log 
        ///// </summary>
        ///// <returns></returns>
        public int AddCommentsToCallLog(int pkCompanyId, Guid UserId, string LeadComments, DateTime CreatedDate)
        {
            int iresult = 0;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                DX.AddCommentsToCallLog(pkCompanyId, UserId, LeadComments, CreatedDate);
                DX.SubmitChanges();
                iresult = 1;
            }
            return iresult;
        }

        ///// <summary>
        ///// Function To Add Status to Call Log 
        ///// </summary>
        ///// <returns></returns>
        public int AddStatusToCallLog(int pkCompanyId, Guid UserId, string LeadComments, DateTime CreatedDate, bool CheckStatusSelect)
        {
            int iresult = 0;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                try
                {
                    tblCallLog ObjtblCallLog = new tblCallLog();
                    List<tblCallLog> lstcalllog = new List<tblCallLog>();
                    lstcalllog = DX.tblCallLogs.Where(db => db.fkCompanyId == pkCompanyId).ToList();
                    string[] stringSeparators = new string[] { "##" };
                    string[] statusArr = LeadComments.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                    if (statusArr.Length > 0)
                    {
                        int LeadStatus = Convert.ToInt32(statusArr[0]);

                        if (LeadStatus == 2)
                        {
                            if (!CheckStatusSelect)
                            {
                                LeadStatus = 20;
                                ObjtblCallLog = new tblCallLog();
                                //Guid pkCallLogId = Guid.NewGuid();
                                //ObjtblCallLog.pkCallLogId = pkCallLogId;
                                ObjtblCallLog.fkCompanyId = pkCompanyId;
                                ObjtblCallLog.fkUserId = UserId;
                                ObjtblCallLog.LeadComments = "##" + LeadStatus + "##";
                                ObjtblCallLog.CreatedDate = CreatedDate;
                                DX.tblCallLogs.InsertOnSubmit(ObjtblCallLog);
                                DX.SubmitChanges();
                                iresult = 1;
                            }
                            else
                            {
                                ObjtblCallLog = new tblCallLog();
                                //Guid pkCallLogId = Guid.NewGuid();
                                //ObjtblCallLog.pkCallLogId = pkCallLogId;
                                ObjtblCallLog.fkCompanyId = pkCompanyId;
                                ObjtblCallLog.fkUserId = UserId;
                                ObjtblCallLog.LeadComments = "##" + LeadStatus + "##";
                                ObjtblCallLog.CreatedDate = CreatedDate;
                                DX.tblCallLogs.InsertOnSubmit(ObjtblCallLog);
                                DX.SubmitChanges();

                                iresult = 1;
                            }
                        }
                        else
                        {
                            if (lstcalllog.Count > 0)
                            {
                                if (!lstcalllog.Any(db => db.LeadComments.Contains(("##" + LeadStatus + "##"))))
                                {
                                    for (int i = 1; i <= LeadStatus; i++)
                                    {
                                        if (!lstcalllog.Any(db => db.LeadComments.Contains(("##" + i + "##"))))
                                        {
                                            ObjtblCallLog = new tblCallLog();
                                            //Guid pkCallLogId = Guid.NewGuid();
                                            // ObjtblCallLog.pkCallLogId = pkCallLogId;
                                            ObjtblCallLog.fkCompanyId = pkCompanyId;
                                            ObjtblCallLog.fkUserId = UserId;
                                            ObjtblCallLog.LeadComments = "##" + i + "##";
                                            ObjtblCallLog.CreatedDate = CreatedDate;
                                            DX.tblCallLogs.InsertOnSubmit(ObjtblCallLog);
                                            DX.SubmitChanges();
                                        }
                                    }
                                    iresult = 1;
                                }
                                else
                                {
                                    foreach (tblCallLog objtblCallLog in lstcalllog)
                                    {
                                        if (LeadStatus == 1)
                                        {
                                            LeadStatus = LeadStatus + 1;
                                            for (int i = LeadStatus; i <= 5; i++)
                                            {
                                                if (objtblCallLog.LeadComments.Contains("##" + i + "##"))
                                                {
                                                    DX.tblCallLogs.DeleteOnSubmit(objtblCallLog);
                                                    DX.SubmitChanges();
                                                }
                                            }
                                        }
                                        else
                                        {
                                            for (int i = LeadStatus; i <= 5; i++)
                                            {
                                                if (objtblCallLog.LeadComments.Contains("##" + i + "##"))
                                                {
                                                    DX.tblCallLogs.DeleteOnSubmit(objtblCallLog);
                                                    DX.SubmitChanges();
                                                }
                                            }
                                        }
                                        iresult = 1;
                                    }
                                }
                            }
                            else
                            {
                                for (int i = 1; i <= LeadStatus; i++)
                                {
                                    ObjtblCallLog = new tblCallLog();
                                    //Guid pkCallLogId = Guid.NewGuid();
                                    //ObjtblCallLog.pkCallLogId = pkCallLogId;
                                    ObjtblCallLog.fkCompanyId = pkCompanyId;
                                    ObjtblCallLog.fkUserId = UserId;
                                    ObjtblCallLog.LeadComments = "##" + i + "##";
                                    ObjtblCallLog.CreatedDate = CreatedDate;
                                    DX.tblCallLogs.InsertOnSubmit(ObjtblCallLog);
                                    DX.SubmitChanges();
                                }
                                iresult = 1;
                            }
                        }
                    }
                }
                catch
                {
                    iresult = -1;
                }
            }
            return iresult;
        }


        ///// <summary>
        ///// Function To Get LeadComments by CompanyId
        ///// </summary>
        ///// <param name="fkCompanyId"></param>
        ///// <returns></returns>            
        public List<Proc_Get_CommentsForLeadResult> GetCommentsForLead(int fkCompanyId)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.Get_CommentsForLead(fkCompanyId).ToList<Proc_Get_CommentsForLeadResult>();
            }
        }


        public List<Proc_GetTraineeBy_DemoId_CompanyIdResult> GetTraineeByDemoIdCompanyId(int FkDemoScheduleId, int fkCompanyId)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.Proc_GetTraineeBy_DemoId_CompanyId(FkDemoScheduleId, fkCompanyId).ToList<Proc_GetTraineeBy_DemoId_CompanyIdResult>();
            }
        }

        public List<Proc_GetDocumentsBy_SentId_companyIdResult> GetDocumentsBySentIdcompanyId(int FkDocumentSentId, int fkCompanyId)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.GetDocumentsBySentIdcompanyId(FkDocumentSentId, fkCompanyId).ToList<Proc_GetDocumentsBy_SentId_companyIdResult>();
            }
        }

        //public tblOfficeDocumentSent GetDocumentsByDocId(Guid pkDocumentId)
        //{
        //    tblOfficeDocumentSent lst = new tblOfficeDocumentSent();
        //    try
        //    {
        //        using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
        //        {
        //            lst = ObjEmergeDALDataContext.tblOfficeDocumentSents.Where(d => d.pkDocumentId == pkDocumentId).FirstOrDefault();

        //        }
        //    }
        //    catch
        //    {

        //    }
        //    return lst;
        //}


        public void UpdateCallLogs(int fkCompanyId, int callLogId, string callLogText)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                var callLog = ObjEmergeDALDataContext.tblCallLogs.Where(cl => cl.fkCompanyId.Equals(fkCompanyId) && cl.pkCallLogId.Equals(callLogId)).FirstOrDefault();
                if (callLog != null)
                {
                    callLog.LeadComments = callLogText;
                    ObjEmergeDALDataContext.SubmitChanges();
                }
            }
        }




        public int UpdateLeadStatus(tblCompany _ObjtblCompany)
        {
            int success = 0;
            tblCompany ObjtblCompany = new tblCompany();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                try
                {
                    ObjtblCompany = DX.tblCompanies.Where(d => d.pkCompanyId == _ObjtblCompany.pkCompanyId).FirstOrDefault();
                    if (ObjtblCompany != null)
                    {
                        ObjtblCompany.LeadStatus = _ObjtblCompany.LeadStatus;
                        ObjtblCompany.LastModifiedById = _ObjtblCompany.LastModifiedById;
                        ObjtblCompany.LastModifiedDate = _ObjtblCompany.LastModifiedDate;
                        ObjtblCompany.StatusStartDate = _ObjtblCompany.StatusStartDate;
                        DX.SubmitChanges();
                        success = 1;
                    }
                }
                catch
                {
                    success = -1;
                }
            }
            return success;
        }

        public bool CheckLeadNumber(string LeadNo)
        {
            bool IsDuplicate = false;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                int cnt = DX.tblCompanies.Where(d => d.LeadNumber == LeadNo).ToList().Count();
                IsDuplicate = (cnt > 0) ? false : true;
            }
            return IsDuplicate;
        }


        /// <summary>
        /// Function To Transfer Lead 
        /// </summary>
        /// <returns></returns>
        public int TransferLead(List<tblCompany> listCompany, Guid fkSalesManagerId)
        {
            DbTransaction DbTrans = null;
            byte LeadType = 1; //ticket #41
            using (ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    ObjEmergeDALDataContext.Connection.Open();
                    DbTrans = ObjEmergeDALDataContext.Connection.BeginTransaction();
                    ObjEmergeDALDataContext.Transaction = DbTrans;
                    for (int i = 0; i < listCompany.Count; i++)
                    {
                        ObjEmergeDALDataContext.TransferLead(listCompany.ElementAt(i).pkCompanyId, fkSalesManagerId, LeadType);
                    }
                    DbTrans.Commit(); // Commit Transaction
                    return (int)1;
                }
                catch (Exception ex)
                {
                    if (DbTrans != null)
                    {
                        DbTrans.Rollback(); // Rollback Transaction
                    }
                    throw ex;
                }
                finally
                {
                    ObjEmergeDALDataContext.Transaction = null;
                    if (ObjEmergeDALDataContext.Connection.State == ConnectionState.Open)
                    {
                        ObjEmergeDALDataContext.Connection.Close(); // Close Connection
                    }
                }
            }
        }

        /// <summary>
        /// Function To Move Lead 
        /// </summary>
        /// <returns></returns>
        public int MoveLead(List<tblCompany> listCompany, byte LeadType)
        {
            DbTransaction DbTrans = null;
            using (ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    ObjEmergeDALDataContext.Connection.Open();
                    DbTrans = ObjEmergeDALDataContext.Connection.BeginTransaction();
                    ObjEmergeDALDataContext.Transaction = DbTrans;
                    for (int i = 0; i < listCompany.Count; i++)
                    {
                        ObjEmergeDALDataContext.MoveLead(listCompany.ElementAt(i).pkCompanyId, LeadType);
                    }
                    DbTrans.Commit(); // Commit Transaction
                    return (int)1;
                }
                catch (Exception ex)
                {
                    if (DbTrans != null)
                    {
                        DbTrans.Rollback(); // Rollback Transaction
                    }
                    throw ex;
                }
                finally
                {
                    ObjEmergeDALDataContext.Transaction = null;
                    if (ObjEmergeDALDataContext.Connection.State == ConnectionState.Open)
                    {
                        ObjEmergeDALDataContext.Connection.Close(); // Close Connection
                    }
                }
            }
        }

        public List<tblCompany> CountLeadType(Guid SalesManagerId, Guid SalesRepId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                //List<tblCompany> lstLeadType = new List<tblCompany>();
                var query = new List<tblCompany>();
                Guid Application_id = new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852");


                if (SalesManagerId != Guid.Empty)
                {
                    query = (from u in DX.tblCompanies
                             join ut in DX.tblLocations
                             on new { a = u.pkCompanyId, b = true } equals new { a = (int)ut.fkCompanyId, b = ut.IsHome }
                             join us in DX.tblStates on ut.fkStateID equals us.pkStateId
                             join ct in DX.tblCompanyTypes on u.fkCompanyTypeId equals ct.pkCompanyTypeId
                             where u.LeadStatus != 0 && u.LeadStatus < 5 && u.IsDeleted == false && u.fkApplicationId == Application_id && u.fkSalesManagerId == SalesManagerId
                             select u).ToList<tblCompany>();
                    return query;
                }
                else if (SalesRepId != Guid.Empty)
                {

                    query = (from u in DX.tblCompanies
                             join ut in DX.tblLocations
                             on new { a = u.pkCompanyId, b = true } equals new { a = (int)ut.fkCompanyId, b = ut.IsHome }
                             join us in DX.tblStates on ut.fkStateID equals us.pkStateId
                             join ct in DX.tblCompanyTypes on u.fkCompanyTypeId equals ct.pkCompanyTypeId
                             where u.LeadStatus != 0 && u.LeadStatus < 5 && u.IsDeleted == false && u.fkApplicationId == Application_id && u.fkSalesRepId == SalesRepId
                             select u).ToList <tblCompany>();
                    return query;
                }
                else
                {

                    query = (from u in DX.tblCompanies
                             join ut in DX.tblLocations
                             on new { a = u.pkCompanyId, b = true } equals new { a = (int)ut.fkCompanyId, b = ut.IsHome }
                             join us in DX.tblStates on ut.fkStateID equals us.pkStateId
                             join ct in DX.tblCompanyTypes on u.fkCompanyTypeId equals ct.pkCompanyTypeId
                             where u.LeadStatus != 0 && u.LeadStatus < 5 && u.IsDeleted == false && u.fkApplicationId == Application_id
                             select u).ToList <tblCompany>();
                    return query;



                }





            }
        }

        public List<Proc_GetMangerlistbyMangerIdResult> GetMangerListByMangerId(Guid MangerId)
        {
            using (EmergeDALDataContext objdb = new EmergeDALDataContext())
            {
                return objdb.GetMangerlistbyMangerId(MangerId).ToList();
            }
        }


        public List<Proc_LeadSalesRepNameDropDownvalueResult> GetSalesRepdataId(Guid? MangerId)
        {
            using (EmergeDALDataContext objdb = new EmergeDALDataContext())
            {
                return objdb.LeadSalesRepNameDropDownvalue(MangerId).ToList();
            }
        }



        public List<Proc_LeadNextStepDropDownvalueResult> GetNextStepdataId(Guid? MangerId)
        {
            using (EmergeDALDataContext objdb = new EmergeDALDataContext())
            {
                return objdb.LeadNextStepDropDownvalue(MangerId).ToList();
            }
        }
        public List<tblOfficeDocument> GetDocuments()
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                //List<tblOfficeDocument> lstDocuments = new List<tblOfficeDocument>();
                return DX.tblOfficeDocuments.ToList <tblOfficeDocument>();
            }
        }


        public int AddDemoSchedule(tblDemoSchedule ObjtblDemoSchedule, List<tblCompanyTrainee> ObjtblCompanyTrainee, tblCallLog ObjtblCallLog)
        {
            DbTransaction ObjDbTransaction = null;
            try
            {
                using (ObjEmergeDALDataContext = new EmergeDALDataContext())
                {
                    int DemoScheduleId = 0;
                    ObjEmergeDALDataContext.Connection.Open();
                    ObjDbTransaction = ObjEmergeDALDataContext.Connection.BeginTransaction();
                    ObjEmergeDALDataContext.Transaction = ObjDbTransaction;
                    var resultdemoId = ObjEmergeDALDataContext.AddDemoScheduleInfo(ObjtblDemoSchedule.fkCompanyId, ObjtblDemoSchedule.DemoDateTime, ObjtblDemoSchedule.Comments);
                    DemoScheduleId = Convert.ToInt32(resultdemoId.FirstOrDefault().Column1);
                    if (DemoScheduleId < 0)
                    {
                        if (ObjDbTransaction != null)
                        {
                            ObjDbTransaction.Rollback();
                        }
                        return -1;
                    }

                    int CompanyTraineeId = 0;
                    for (int i = 0; i < ObjtblCompanyTrainee.Count; i++)
                    {
                        var TraineeId = ObjEmergeDALDataContext.AddCompanyTrainee(DemoScheduleId,
                                                                            ObjtblCompanyTrainee.ElementAt(i).FirstName,
                                                                            ObjtblCompanyTrainee.ElementAt(i).LastName,
                                                                            ObjtblCompanyTrainee.ElementAt(i).Title,
                                                                            ObjtblCompanyTrainee.ElementAt(i).Phone,
                                                                            ObjtblCompanyTrainee.ElementAt(i).Ext,
                                                                            ObjtblCompanyTrainee.ElementAt(i).EmailId
                                                                            );
                        CompanyTraineeId = Convert.ToInt32(TraineeId.FirstOrDefault().Column1);
                        if (CompanyTraineeId < 0)
                        {
                            if (ObjDbTransaction != null)
                            {
                                ObjDbTransaction.Rollback();
                            }
                            return -2;
                        }
                    }

                    string comments = "##10##" + ObjtblCallLog.LeadComments + "##" + Convert.ToString(DemoScheduleId);
                    int calllogId = ObjEmergeDALDataContext.AddDemoScheduleToCallLog(ObjtblCallLog.fkCompanyId, ObjtblCallLog.fkUserId, comments, ObjtblCallLog.CreatedDate);

                    if (calllogId < 0)
                    {
                        if (ObjDbTransaction != null)
                        {
                            ObjDbTransaction.Rollback();
                        }
                        return -3;
                    }
                    ObjDbTransaction.Commit();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public tblDemoSchedule GetDemoScheduleInfoByDemoId(int pkDemoScheduleId)
        {
            tblDemoSchedule DemoScheduleInfo = new tblDemoSchedule();
            try
            {
                using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
                {
                    DemoScheduleInfo = ObjEmergeDALDataContext.tblDemoSchedules.Where(d => d.pkDemoScheduleId == pkDemoScheduleId).FirstOrDefault();

                }
            }
            catch
            {

            }
            return DemoScheduleInfo;
        }

        public tblDocumentSent GetDocumentSentinfobyDocId(int pkDocumentsentId)
        {
            tblDocumentSent DocumentsentInfo = new tblDocumentSent();
            try
            {
                using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
                {
                    DocumentsentInfo = ObjEmergeDALDataContext.tblDocumentSents.Where(d => d.pkDocumentSentId == pkDocumentsentId).FirstOrDefault();

                }
            }
            catch
            {

            }
            return DocumentsentInfo;
        }


        public tblOfficeDocument GetDocInfoByDocId(int pkDocumentId)
        {
            tblOfficeDocument DocumentInfo = new tblOfficeDocument();
            try
            {
                using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
                {
                    DocumentInfo = ObjEmergeDALDataContext.tblOfficeDocuments.Where(d => d.pkDocumentId == pkDocumentId).FirstOrDefault();
                }
            }
            catch
            {

            }
            return DocumentInfo;
        }

        public int AddDDocumentSent(tblDocumentSent ObjtblDocumentSent, List<tblOfficeDocumentSent> ObjtblOfficeDocumentSent, tblCallLog ObjtblCallLog)
        {
            DbTransaction ObjDbTransaction = null;
            try
            {
                using (ObjEmergeDALDataContext = new EmergeDALDataContext())
                {
                    int DocumentSentId = 0;
                    ObjEmergeDALDataContext.Connection.Open();
                    ObjDbTransaction = ObjEmergeDALDataContext.Connection.BeginTransaction();
                    ObjEmergeDALDataContext.Transaction = ObjDbTransaction;
                    var resultdocumentSentId = ObjEmergeDALDataContext.AddDocumentSentInfo(ObjtblDocumentSent.fkCompanyId, ObjtblDocumentSent.Comments);
                    DocumentSentId = Convert.ToInt32(resultdocumentSentId.FirstOrDefault().Column1);
                    if (DocumentSentId < 0)
                    {
                        if (ObjDbTransaction != null)
                        {
                            ObjDbTransaction.Rollback();
                        }
                        return -1;
                    }

                    int OfficeDocumentSentId = 0;
                    for (int i = 0; i < ObjtblOfficeDocumentSent.Count; i++)
                    {
                        var DocumentId = ObjEmergeDALDataContext.AddOfficeDocumentSentInfo(ObjtblOfficeDocumentSent.ElementAt(i).pkDocumentId,
                                                                                     DocumentSentId,
                                                                                     ObjtblOfficeDocumentSent.ElementAt(i).DocumentTitle,
                                                                                     ObjtblOfficeDocumentSent.ElementAt(i).DocumentPath
                                                                                     );
                        OfficeDocumentSentId = Convert.ToInt32(DocumentId.FirstOrDefault().Column1);
                        if (OfficeDocumentSentId < 0)
                        {
                            if (ObjDbTransaction != null)
                            {
                                ObjDbTransaction.Rollback();
                            }
                            return -2;
                        }
                    }

                    string comments = "##11##" + ObjtblCallLog.LeadComments + "##" + Convert.ToString(DocumentSentId);
                    int calllogId = ObjEmergeDALDataContext.AddDemoScheduleToCallLog(ObjtblCallLog.fkCompanyId, ObjtblCallLog.fkUserId, comments, ObjtblCallLog.CreatedDate);

                    if (calllogId < 0)
                    {
                        if (ObjDbTransaction != null)
                        {
                            ObjDbTransaction.Rollback();
                        }
                        return -3;
                    }
                    ObjDbTransaction.Commit();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }






    }

    [Serializable]
    public class cTrainee
    {
        public int pkCompanyTraineeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string Phone { get; set; }
        public string Ext { get; set; }
        public string EmailId { get; set; }
    }









}
