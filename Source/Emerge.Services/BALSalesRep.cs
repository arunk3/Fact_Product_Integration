﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;
using System.Data.Linq;
using System.Xml.Linq;


namespace Emerge.Services
{
    public class BALSalesRep
    {
        ///// <summary>
        ///// Function to Get All Sales Reps in form of ClsSalesRep User Defined Class
        ///// </summary>
        ///// <returns></returns>
        //public List<ClsSalesRep> GetAllSalesRepForAdminForGridview()
        //{
        //    using ( EmergeDALDataContext ObjDALDataContext = new  EmergeDALDataContext())
        //    {
        //        var Response = from s in ObjDALDataContext.tblSalesReps
        //                       join m in ObjDALDataContext.aspnet_Users
        //                       on s.fkUserId equals m.UserId
        //                       select new ClsSalesRep
        //                       {
        //                           pkSalesRepId = s.pkSalesRepId,
        //                           EmailId = m.UserName,
        //                           FirstName = s.FirstName,
        //                           LastName = s.LastName,
        //                           Initial = s.Initial,
        //                           IsEnable = s.IsEnable,
        //                           FullName= s.FirstName +" "+ s.LastName
        //                       };
        //        return Response.ToList<ClsSalesRep>();

        //    }
        //}

        ///// <summary>
        ///// Function to Get Sales Rep by their role
        ///// </summary>
        ///// <returns></returns>
        //public List<Proc_Get_SalesRepResult> GetSalesRep(string RoleName)
        //{
        //    using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
        //    {
        //        return ObjDALDataContext.GetSalesRep(RoleName).ToList<Proc_Get_SalesRepResult>();
        //    }
        //}

        /// <summary>
        /// Function to Get Sales Manager
        /// </summary>
        /// <returns></returns>
        public List<Proc_Get_SalesManagersResult> GetSalesManagers(string RoleName)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.Get_SalesManagers(RoleName).ToList<Proc_Get_SalesManagersResult>();
            }
        }

        /// <summary>
        /// Function to Get Sales Rep by their Managers
        /// </summary>
        /// <returns></returns>
        public List<Proc_Get_SalesRepByManagerIdResult> GetSalesRepByManagerId(Guid fkManagerId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetSalesRepByManagerId(fkManagerId).ToList<Proc_Get_SalesRepByManagerIdResult>();
            }
        }


        ///// <summary>
        ///// Function to Get All Sales Rep in form of Table  tblSalesRep
        ///// </summary>
        ///// <returns></returns>
        //public List<tblSalesRep> GetAllSalesRepForAdmin()
        //{
        //    using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
        //    {
        //        return ObjDALDataContext.tblSalesReps.ToList<tblSalesRep>();
        //    }
        //}


        //public Guid GetSalesManagerId(Guid fkSalesManagerId)
        //{
        //    using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
        //    {
        //        return new Guid(ObjDALDataContext.GetSalesManagerId(fkSalesManagerId).ElementAt(0).fkSalesManagerId.ToString()); 
        //    }
        //}

        ///// <summary>
        ///// Function to Add New Sales Rep To the System.
        ///// </summary>
        ///// <param name="ObjtblSalesRep"></param>
        //public Guid AddNewSalesRep(tblSalesRep ObjtblSalesRep)
        //{
        //    using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
        //    {
        //        var Response = ObjDALDataContext.AddSalesRep(
        //                                        ObjtblSalesRep.pkSalesRepId,
        //                                        ObjtblSalesRep.fkUserId,
        //                                        ObjtblSalesRep.FirstName,
        //                                        ObjtblSalesRep.LastName,
        //                                        ObjtblSalesRep.Initial,
        //                                        ObjtblSalesRep.IsEnable,
        //                                        ObjtblSalesRep.UserImage,
        //                                        ObjtblSalesRep.CreatedDate,
        //                                        ObjtblSalesRep.ModifyDate);
        //        return new Guid(Response.ElementAt(0).PkSalesRepId.ToString());

        //    }
        //}

        ///// <summary>
        ///// Function to Update Sales Rep
        ///// </summary>
        ///// <param name="ObjtblSalesRep"></param>
        ///// <returns></returns>
        //public bool UpdateSalesRep(tblSalesRep ObjtblSalesRep)
        //{
        //    using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
        //    {
        //        int Response = ObjDALDataContext.UpdateSalesRep(ObjtblSalesRep.pkSalesRepId,
        //                                        ObjtblSalesRep.FirstName,
        //                                        ObjtblSalesRep.LastName,
        //                                        ObjtblSalesRep.Initial,
        //                                        ObjtblSalesRep.IsEnable,
        //                                        ObjtblSalesRep.UserImage,
        //                                        ObjtblSalesRep.ModifyDate);
        //        return Response == 1 ? true : false;
        //    }
        //}

        ///// <summary>
        ///// Function to Delete Sales Representative if 
        ///// </summary>
        ///// <param name="SalesRepId"></param>
        ///// <returns></returns>
        //public bool DeleteSalesRep(Guid SalesRepId)
        //{
        //    using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
        //    {
        //        int Response = ObjDALDataContext.DeleteSalesRep(SalesRepId);
        //        return Response == 1 ? true : false;
        //    }
        //}
        //public tblSalesRep GetSalesRepById(Guid pkSalesRepId)
        //{
        //    using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
        //    {
        //        return (from db in ObjDALDataContext.tblSalesReps
        //                where db.pkSalesRepId == pkSalesRepId
        //                select db).First();
        //    }
        //}

        //public tblCompany GetCompanyAccountNumber(Guid pkCompanyId)
        //{
        //    using (EmergeDALDataContext DX = new EmergeDALDataContext())
        //    {
        //       /*var result=   from s in DX.tblSalesReps
        //                     join c in DX.tblCompanies
        //                     on s.pkSalesRepId equals c.fkSalesRepId
        //                     where c.pkCompanyId == pkCompanyId
        //                     select new ClsSalesRep
        //                     {
        //                         FirstName = s.FirstName,
        //                         LastName = s.LastName,
        //                         CompanyAccountNumber = c.CompanyAccountNumber
        //                     };
        //        return (ClsSalesRep)result;*/



                
        //        return (from db in DX.tblCompanies
        //             where db.pkCompanyId == pkCompanyId
        //              select db).FirstOrDefault();
                 
        //    }

        //}
        //public List<Proc_GetCompACWithSalesRepResult> GetCompanyAcSalesRep(Guid pkCompanyId)
        //{
        //    using (EmergeDALDataContext DX = new EmergeDALDataContext())
        //    {
        //        return DX.GetCompACWithSalesRep(pkCompanyId).ToList<Proc_GetCompACWithSalesRepResult>();

        //    }

        //}
        ///// <summary>
        ///// Function To Get All Sales Office Users For Sales Admin Only
        ///// </summary>
        ///// <param name="PageIndex"></param>
        ///// <param name="PageSize"></param>
        ///// <param name="IsPaging"></param>
        ///// <param name="SearchString"></param>
        ///// <returns></returns>            
        //public List<Proc_Get_AllSalesUsersForAdminResult> GetAllSalesUsersForAdmin(int PageIndex, int PageSize, bool IsPaging, string SearchString, Int16 StatusType, Guid CompanyId, Guid FkLocationId, string SortingColumn, string SortingDirection)
        //{
        //    using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
        //    {
        //        return ObjDALDataContext.GetAllSalesUsersForAdmin(PageIndex, PageSize, IsPaging, SearchString, StatusType, CompanyId, FkLocationId, SortingColumn, SortingDirection).ToList<Proc_Get_AllSalesUsersForAdminResult>();
        //    }
        //}
        ///// <summary>
        ///// Function To Get All Sales Office Users For Sales Manager Only
        ///// </summary>
        ///// <param name="PageIndex"></param>
        ///// <param name="PageSize"></param>
        ///// <param name="IsPaging"></param>
        ///// <param name="SearchString"></param>
        ///// <returns></returns>            
        //public List<Proc_Get_AllSalesUsersForManagerResult> GetAllSalesUsersForSalesManager(int PageIndex, int PageSize, bool IsPaging, string SearchString, Int16 StatusType, Guid CompanyId, Guid FkLocationId, string SortingColumn, string SortingDirection)
        //{
        //    using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
        //    {
        //        return ObjDALDataContext.GetAllSalesUsersForManager(PageIndex, PageSize, IsPaging, SearchString, StatusType, CompanyId, FkLocationId, SortingColumn, SortingDirection).ToList<Proc_Get_AllSalesUsersForManagerResult>();
        //    }
        //}
    }

    public class ClsSalesRep
    {
        public Guid pkSalesRepId { get; set; }
        public string EmailId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Initial { get; set; }
        public bool IsEnable { get; set; }
        public string FullName { get; set; }
        public string CompanyAccountNumber { get; set; }
    }

}
