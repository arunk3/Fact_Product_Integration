﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;
using System.Data.Common;
using System.Data;

namespace Emerge.Services
{
    public class BALProductPackages
    {
        /// <summary>
        /// This method is used to get packages by company.
        /// </summary>
        /// <param name="LocationId"></param>
        /// <param name="ApplicationId"></param>
        /// <returns></returns>
        public List<Proc_Get_EmergeProductPackagesForAdminResult> GetEmergeProductPackagesForAdmin(int LocationId, Guid ApplicationId, int fkCompanyId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                List<Proc_Get_EmergeProductPackagesForAdminResult> ObjCollection = ObjDALDataContext.GetEmergeProductPackagesForAdmin(LocationId, ApplicationId, fkCompanyId).ToList();
                return ObjCollection;
            }
        }

        /// <summary>
        /// This function is used to get Packages with their ProductNames
        /// </summary>
        /// <param name="LocationId"></param>
        /// <param name="ApplicationId"></param>
        /// <param name="fkCompanyId"></param>
        /// <returns></returns>
        public List<Proc_GetPackagesWithProductsResult> GetPackagesWithProducts(int LocationId, Guid ApplicationId, int fkCompanyId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                List<Proc_GetPackagesWithProductsResult> ObjCollection = ObjDALDataContext.GetPackagesWithProducts(LocationId, ApplicationId, fkCompanyId).ToList();
                return ObjCollection;
            }
        }

        /// <summary>
        /// This function is used to get Reports in Package.
        /// </summary>
        /// <param name="packageId"></param>
        /// <returns></returns>
        public List<proc_Get_ProductInPackage_ByPckgIdResult> GetReportsInPackage(int packageId)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                return Dx.GetProductInPackage_ByPckgId(packageId).ToList();
            }
        }

        /// <summary>
        /// This method is used to get product packages
        /// </summary>
        /// <param name="ApplicationId"></param>
        /// <returns></returns>
        public List<tblProductPackage> GetProductPackages(Guid ApplicationId, int CompanyId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                IQueryable<tblProductPackage> ObjCollection = ObjDALDataContext.tblProductPackages.Where(db => db.fkApplicationId == ApplicationId && db.IsDelete == false).OrderBy(db => db.PackageDisplayName);
                return ObjCollection.ToList();
            }
        }

        public List<Proc_Get_UniversalPackagesForAdminResult> GetEmergeUniversalPackagesForAdmin(int LocationId, Guid ApplicationId, int fkCompanyId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                List<Proc_Get_UniversalPackagesForAdminResult> ObjCollection = ObjDALDataContext.Proc_Get_UniversalPackagesForAdmin(LocationId, ApplicationId, fkCompanyId).ToList();
                return ObjCollection;
            }
        }

        /// <summary>
        /// To get packahe id based on the selected location and company id.
        /// </summary>
        /// <param name="LocationId"></param>
        /// <param name="fkCompanyId"></param>
        /// <returns></returns>
        public int GetPackageIdBasedOnLocationAndCompany(int LocationId, int fkCompanyId,string packageCode)
        {
            int packageId = 0;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                packageId = ObjDALDataContext.tblProductPackages.Where(x => x.fkCompanyId == fkCompanyId && x.fkLocationid == LocationId && x.PackageCode == packageCode).Select(x => x.pkPackageId).FirstOrDefault();
            }
            return packageId;
        }


        /// <summary>
        /// This method is used to change package delete status
        /// </summary>
        /// <param name="ObjCollectionPackages"></param>
        /// <param name="Action"></param>
        /// <returns></returns>
        public int ChangePackageDeleteStatus(List<int> ObjCollectionPackages, bool Action)
        {
            int iResult = -1;

            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                IQueryable<tblProductPackage> ObjData = ObjDALDataContext.tblProductPackages.Where(db => ObjCollectionPackages.Contains(db.pkPackageId));

                foreach (tblProductPackage ObjUpdate in ObjData)
                {
                    ObjUpdate.IsDelete = Action;
                    ObjDALDataContext.SubmitChanges();
                    iResult = 1;
                }
            }
            return iResult;
        }

        public int DeleteCustomPackageById(int pkPackageId, bool Action)
        {
            int iResult = -1;
            try
            {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                tblProductPackage delPackageId = ObjDALDataContext.tblProductPackages.Where(db => db.pkPackageId == pkPackageId).FirstOrDefault();
                if (delPackageId != null)
                {
                    tblPackageAccess objtblPackageAccess= ObjDALDataContext.tblPackageAccesses.Where(db => db.fkPackageId == pkPackageId && db.fkLocationId == delPackageId.fkLocationid).FirstOrDefault();
                    if (objtblPackageAccess != null)
                    {
                        ObjDALDataContext.tblPackageAccesses.DeleteOnSubmit(objtblPackageAccess);
                        ObjDALDataContext.SubmitChanges();
                        List<tblPackageReport> objtblPackageReport = ObjDALDataContext.tblPackageReports.Where(db => db.fkPackageId == pkPackageId).ToList();
                        foreach (tblPackageReport objSingle in objtblPackageReport)
                        {
                            ObjDALDataContext.tblPackageReports.DeleteOnSubmit(objSingle);
                            ObjDALDataContext.SubmitChanges();
                        }
                        delPackageId.IsDelete = true;//Soft deletion
                        //ObjDALDataContext.tblProductPackages.DeleteOnSubmit(delPackageId);//Hard deletion
                        ObjDALDataContext.SubmitChanges();
                        iResult = 1;
                    }
                }
            }
            }
            catch (Exception)
            {

                //throw;
            }
            return iResult;
        }



        /// <summary>
        /// Function to change the Package Status
        /// </summary>
        /// <param name="PkPackageId"></param>
        /// <param name="PackagePropertyName"></param>
        /// <param name="PackagePropertyValue"></param>
        /// <returns></returns>
        public bool ChangePackageStatus(int PkPackageId, string PackagePropertyName, bool PackagePropertyValue)
        {
            bool ReturnValue = false;
            try
            {
                using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
                {
                    tblProductPackage ObjtblProductPackage = ObjDALDataContext.tblProductPackages.Single(data => data.pkPackageId == PkPackageId);
                    switch (PackagePropertyName)
                    {
                        case "Global":
                            ObjtblProductPackage.IsPublic = PackagePropertyValue;
                            break;
                        case "Active":
                            ObjtblProductPackage.IsEnabled = PackagePropertyValue;
                            break;
                        case "Default":
                            ObjtblProductPackage.IsDefault = PackagePropertyValue;
                            break;
                    }
                    ObjDALDataContext.SubmitChanges();
                }
                ReturnValue = true;
            }
            catch (Exception)
            {
                ReturnValue = false;
            }
            return ReturnValue;
        }

        /// <summary>
        /// This method is used to get all emerge products in packages
        /// </summary>
        /// <param name="PackageId"></param>
        /// <param name="ApplicationId"></param>
        /// <returns></returns>
        public List<Proc_Get_AllEmergeProductsInPackagesResult> GetAllEmergeProductsInPackages(int PackageId, Guid ApplicationId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                List<Proc_Get_AllEmergeProductsInPackagesResult> ObjCollection = ObjDALDataContext.Get_AllEmergeProductsInPackages(PackageId, ApplicationId).ToList();
                return ObjCollection;
            }
        }
        /// <summary>
        /// This method is used to get product package by id
        /// </summary>
        /// <param name="PackageId"></param>
        /// <returns></returns>
        public List<tblProductPackage> GetProductPackageById(int PackageId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                IQueryable<tblProductPackage> ObjCollection = ObjDALDataContext.tblProductPackages.Where(db => db.pkPackageId == PackageId && db.IsDelete == false);
                return ObjCollection.ToList();
            }
        }

        /// <summary>
        /// This method is used to update universal packages for location
        /// </summary>
        /// <param name="LocationId"></param>
        /// <param name="ApplicationId"></param>
        /// <returns></returns>
        public int UpdateUniversalPackagesLocationWise(List<tblPackageAccess> lstADDPackageAccess, int LocationId)
        {
            int IResult = -1;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                try
                {
                    List<tblPackageAccess> lstDeletePackages = new List<tblPackageAccess>();
                    List<tblProductPackage> LstUniversalPackages = DX.tblProductPackages.Where(d => d.fkCompanyId == 0 && d.IsDelete == false).ToList<tblProductPackage>();
                    if (LstUniversalPackages.Count > 0)
                    {
                        for (int i = 0; i < LstUniversalPackages.Count; i++)
                        {
                            tblPackageAccess ObjDeletePackages = DX.tblPackageAccesses.Where(d => d.fkPackageId == LstUniversalPackages.ElementAt(i).pkPackageId && d.fkLocationId == LocationId).FirstOrDefault();
                            if (ObjDeletePackages != null)
                            {
                                lstDeletePackages.Add(ObjDeletePackages);
                            }
                        }
                    }

                    if (lstDeletePackages.Count > 0)
                    {
                        DX.tblPackageAccesses.DeleteAllOnSubmit(lstDeletePackages);
                    }
                    if (lstADDPackageAccess.Count > 0)
                    {
                        DX.tblPackageAccesses.InsertAllOnSubmit(lstADDPackageAccess);
                        DX.SubmitChanges();
                        IResult = 1;
                    }
                }
                catch
                {
                }

                return IResult;
            }
        }



        public int AddUniversalPackagesForNewCompany(List<tblCompany_PackagePrice> lstCompany_PackagePrice, List<tblPackageAccess> lstPackageAccess)
        {
            int IResult = -1;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                try
                {
                    DX.tblCompany_PackagePrices.InsertAllOnSubmit(lstCompany_PackagePrice);

                    DX.tblPackageAccesses.InsertAllOnSubmit(lstPackageAccess);

                    DX.SubmitChanges();
                    IResult = 1;
                }
                catch
                {
                }

                return IResult;
            }
        }

        /// <summary>
        /// This method is used to update product packages
        /// </summary>
        /// <param name="DeletingProducts"></param>
        /// <param name="ObjProductPackage"></param>
        /// <param name="ObjCollectionPackageReports"></param>
        /// <returns></returns>
        public int UpdateProductPackages(List<Guid> DeletingProducts, tblProductPackage ObjProductPackage, List<tblPackageReport> ObjCollectionPackageReports)
        {
            #region Variable Assignment

            int iResult = -1;
            DbTransaction DbTrans = null;

            #endregion

            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    #region Connection Open and Transaction Assignment

                    ObjDALDataContext.Connection.Open(); /* Connection Open */
                    DbTrans = ObjDALDataContext.Connection.BeginTransaction(); /* Assign Transation */
                    ObjDALDataContext.Transaction = DbTrans;

                    #endregion

                    #region Product Packages

                    iResult = ObjDALDataContext.UpdateEmergeProductPackages(
                                                               ObjProductPackage.pkPackageId,
                                                                ObjProductPackage.fkApplicationId,
                                                                ObjProductPackage.fkCompanyId,
                                                               ObjProductPackage.PackageName,
                                                               ObjProductPackage.PackageCode,
                                                               ObjProductPackage.PackagePrice,
                                                               ObjProductPackage.PackageDisplayName,
                                                               ObjProductPackage.PackageDescription,
                                                               ObjProductPackage.IsEnabled,
                                                               ObjProductPackage.IsDefault,
                                                               ObjProductPackage.IsPublic,
                                                               ObjProductPackage.LastModifiedDate,
                                                               ObjProductPackage.include,
                                                               ObjProductPackage.IsEnableCompanyPackage,
                                                               ObjProductPackage.fkLocationid,
                                                               ObjProductPackage.IsPackageNameDisplay,//INT-101
                                                               ObjProductPackage.Is7YearFilter,
                                                               ObjProductPackage.Is10YearFilter
                                                               );

                    if (iResult < 0)
                    {
                        if (DbTrans != null)
                        {
                            DbTrans.Rollback(); /* Rollback Transaction  */
                        }
                        return iResult;
                    }

                    #endregion

                    #region Delete unselect packages

                    //if (DeletingProducts.Count > 0)
                    //{
                    //IQueryable<tblPackageReport> ObjDeletingProducts = ObjDALDataContext.tblPackageReports.Where(db => DeletingProducts.Contains(db.pkPackageReportId));
                    IQueryable<tblPackageReport> ObjDeletingProducts = ObjDALDataContext.tblPackageReports.Where(db => db.fkPackageId == ObjProductPackage.pkPackageId);

                    if (ObjDeletingProducts.Count() > 0)
                    {
                        ObjDALDataContext.tblPackageReports.DeleteAllOnSubmit(ObjDeletingProducts);
                        ObjDALDataContext.SubmitChanges();
                    }
                    //}

                    #endregion

                    #region Add new product in packages

                    foreach (tblPackageReport ObjPackageReport in ObjCollectionPackageReports)
                    {
                        ObjDALDataContext.tblPackageReports.InsertOnSubmit(ObjPackageReport);
                        ObjDALDataContext.SubmitChanges();
                    }

                    #endregion

                    #region Transaction Committed

                    DbTrans.Commit(); /* Commit Transaction */

                    iResult = 1;

                    #endregion
                }
                catch
                {
                    #region Transaction Rollback

                    iResult = -1;
                    if (DbTrans != null)
                    {
                        DbTrans.Rollback(); /* Rollback Transaction */
                    }

                    #endregion
                }
                finally
                {
                    #region Connection and Transaction Close

                    ObjDALDataContext.Transaction = null;
                    if (ObjDALDataContext.Connection.State == ConnectionState.Open)
                    {
                        ObjDALDataContext.Connection.Close(); /* Close Connection */
                    }

                    #endregion
                }

                #region Return Result

                return iResult; /* Here we return result */

                #endregion
            }
        }


        /// <summary>
        /// This method is used to add product packages
        /// </summary>
        /// <param name="ObjProductPackage"></param>
        /// <param name="ObjCollectionPackageReports"></param>
        /// <param name="oPackageId"></param>
        /// <returns></returns>
        public int AddProductPackages(tblProductPackage ObjProductPackage, List<tblPackageReport> ObjCollectionPackageReports, out int oPackageId)
        {
            #region Variable Assignment

            int iResult = -1;
            int iPackageId = -1;
            DbTransaction DbTrans = null;

            #endregion

            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    #region Connection Open and Transaction Assignment

                    ObjDALDataContext.Connection.Open(); /* Connection Open */
                    DbTrans = ObjDALDataContext.Connection.BeginTransaction(); /* Assign Transation */
                    ObjDALDataContext.Transaction = DbTrans;

                    #endregion

                    #region Product Packages

                    List<Proc_AddEmergeProductPackagesResult> ObjProductPackageSaveColl = ObjDALDataContext.AddEmergeProductPackages(ObjProductPackage.fkApplicationId,
                                                                ObjProductPackage.fkCompanyId,
                                                                ObjProductPackage.fkLocationid,
                                                               ObjProductPackage.PackageName,
                                                               ObjProductPackage.PackageCode,
                                                               ObjProductPackage.PackagePrice,
                                                               ObjProductPackage.PackageDisplayName,
                                                               ObjProductPackage.PackageDescription,
                                                               ObjProductPackage.IsEnabled,
                                                               ObjProductPackage.IsDefault,
                                                               ObjProductPackage.IsPublic,
                                                               ObjProductPackage.CreatedDate,
                                                               ObjProductPackage.include,
                                                               ObjProductPackage.IsEnableCompanyPackage,
                                                               ObjProductPackage.IsPackageNameDisplay,
                                                              ObjProductPackage.Is7YearFilter, ObjProductPackage.Is10YearFilter).ToList();

                    if (ObjProductPackageSaveColl.Count > 0)
                    {
                        iPackageId = int.Parse(ObjProductPackageSaveColl.First().PackageId.ToString());
                        iResult = ObjProductPackageSaveColl.First().ReturnType;
                    }

                    if (iResult < 0)
                    {
                        if (DbTrans != null)
                        {
                            DbTrans.Rollback(); /* Rollback Transaction  */
                        }
                        oPackageId = -1;
                        return iResult;
                    }

                    #endregion

                    #region Add product in packages

                    foreach (tblPackageReport ObjPackageReport in ObjCollectionPackageReports)
                    {
                        //var chVal = ObjDALDataContext.tblPackageReports.Where(x => x.fkPackageId != iPackageId && x.fkProductApplicationId != ObjPackageReport.fkProductApplicationId).FirstOrDefault();
                        var chVal = ObjDALDataContext.tblPackageReports.Where(x => x.fkPackageId == iPackageId && x.fkProductApplicationId == ObjPackageReport.fkProductApplicationId).FirstOrDefault();
                        if (chVal == null)
                        {
                            ObjPackageReport.fkPackageId = iPackageId;
                            ObjDALDataContext.tblPackageReports.InsertOnSubmit(ObjPackageReport);
                            ObjDALDataContext.SubmitChanges();
                        }
                    }

                    #endregion

                    #region Transaction Committed

                    DbTrans.Commit(); /* Commit Transaction */

                    iResult = 1;

                    #endregion
                }
                catch
                {
                    #region Transaction Rollback

                    iResult = -1;
                    if (DbTrans != null)
                    {
                        DbTrans.Rollback(); /* Rollback Transaction */
                    }

                    #endregion
                }
                finally
                {
                    #region Connection and Transaction Close

                    ObjDALDataContext.Transaction = null;
                    if (ObjDALDataContext.Connection.State == ConnectionState.Open)
                    {
                        ObjDALDataContext.Connection.Close(); /* Close Connection */
                    }

                    #endregion
                }

                #region Return Result

                oPackageId = iPackageId;
                return iResult; /* Here we return result */

                #endregion
            }
        }


        public int AddUniversalPackagesForAllCompanies(List<tblCompany_PackagePrice> lstCompany_PackagePrice)   // when adding new universal package
        {
            int IResult = -1;
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    DX.tblCompany_PackagePrices.InsertAllOnSubmit(lstCompany_PackagePrice);
                    DX.SubmitChanges();
                    IResult = 1;
                }
            }
            catch
            {
            }
            return IResult;
        }
        public int AddUniversalPackagesForAllCompanyLocation(List<tblPackageAccess> lstPackageAccess)            // when adding new universal package
        {
            int IResult = -1;
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    DX.tblPackageAccesses.InsertAllOnSubmit(lstPackageAccess);
                    DX.SubmitChanges();
                    IResult = 1;
                }
            }
            catch
            {
            }
            return IResult;
        }

        /// <summary>
        /// This method is used to add/update packages for location
        /// </summary>
        /// <param name="DeletingPackages"></param>
        /// <param name="ObjCollectionPackageAccess"></param>
        /// <returns></returns>
        public int AddUpdateProductPackagesForLocation(List<int> DeletingPackages, List<tblPackageAccess> ObjCollectionPackageAccess)
        {
            #region Variable Assignment

            int iResult = -1;
            DbTransaction DbTrans = null;

            #endregion

            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    #region Connection Open and Transaction Assignment

                    ObjDALDataContext.Connection.Open(); /* Connection Open */
                    DbTrans = ObjDALDataContext.Connection.BeginTransaction(); /* Assign Transation */
                    ObjDALDataContext.Transaction = DbTrans;

                    #endregion

                    #region Delete unselect packages

                    if (DeletingPackages.Count > 0)
                    {
                        IQueryable<tblPackageAccess> ObjDeletingPackages = ObjDALDataContext.tblPackageAccesses.Where(db => DeletingPackages.Contains(db.pkPackageAccessId));

                        if (ObjDeletingPackages.Count() > 0)
                        {
                            ObjDALDataContext.tblPackageAccesses.DeleteAllOnSubmit(ObjDeletingPackages);
                            ObjDALDataContext.SubmitChanges();
                        }
                    }

                    #endregion

                    #region Add product packages for location

                    foreach (tblPackageAccess ObjPackageAccessData in ObjCollectionPackageAccess)
                    {
                        ObjDALDataContext.tblPackageAccesses.InsertOnSubmit(ObjPackageAccessData);
                        ObjDALDataContext.SubmitChanges();
                    }

                    #endregion

                    #region Transaction Committed

                    DbTrans.Commit(); /* Commit Transaction */

                    iResult = 1;

                    #endregion
                }
                catch
                {
                    #region Transaction Rollback

                    iResult = -1;
                    if (DbTrans != null)
                    {
                        DbTrans.Rollback(); /* Rollback Transaction */
                    }

                    #endregion
                }
                finally
                {
                    #region Connection and Transaction Close

                    ObjDALDataContext.Transaction = null;
                    if (ObjDALDataContext.Connection.State == ConnectionState.Open)
                    {
                        ObjDALDataContext.Connection.Close(); /* Close Connection */
                    }

                    #endregion
                }

                #region Return Result

                return iResult; /* Here we return result */

                #endregion
            }
        }

        /// <summary>
        /// This method is used to update universal packages for company
        /// </summary>
        /// <param name="LocationId"></param>
        /// <param name="ApplicationId"></param>
        /// <returns></returns>
        public int UpdateUniversalPackagesForCompany(List<tblCompany_PackagePrice> lstCompany_PackagePrice, bool flag)
        {
            int IResult = -1;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                try
                {
                    if (!flag)
                    {
                        foreach (tblCompany_PackagePrice Obj in lstCompany_PackagePrice)
                        {
                            tblCompany_PackagePrice ObjCompany_PackagePrice = DX.tblCompany_PackagePrices.Where(d => d.pkCompanyPackagePriceId == Obj.pkCompanyPackagePriceId).FirstOrDefault();
                            if (ObjCompany_PackagePrice != null)
                            {
                                ObjCompany_PackagePrice.IsEnableUniversalPackage = Obj.IsEnableUniversalPackage;
                                ObjCompany_PackagePrice.PackagePrice = Obj.PackagePrice;
                                ObjCompany_PackagePrice.fkLocation = Obj.fkLocation;
                                DX.SubmitChanges();
                                IResult = 1;

                            }
                        }
                    }
                    else
                    {
                        var companyId = lstCompany_PackagePrice.Select(x => x.fkCompanyId).FirstOrDefault();
                        //int homeLocation = DX.tblLocations.Where(x => x.fkCompanyId == companyId.Value && x.IsHome == true).FirstOrDefault().pkLocationId;
                        List<int> allLocation = DX.tblLocations.Where(x => x.fkCompanyId == companyId.Value).Select(x => x.pkLocationId).ToList();
                        foreach (var item in allLocation)
                        {
                            foreach (tblCompany_PackagePrice Obj in lstCompany_PackagePrice)
                            {
                                tblCompany_PackagePrice ObjCompany_PackagePrice = DX.tblCompany_PackagePrices.Where(d => d.fkCompanyId == Obj.fkCompanyId && d.fkLocation == item && d.fkPackageId == Obj.fkPackageId).FirstOrDefault();
                                if (ObjCompany_PackagePrice != null)
                                {
                                    ObjCompany_PackagePrice.IsEnableUniversalPackage = Obj.IsEnableUniversalPackage;
                                    ObjCompany_PackagePrice.PackagePrice = Obj.PackagePrice;
                                    ObjCompany_PackagePrice.fkLocation = item;
                                    DX.SubmitChanges();
                                    IResult = 1;

                                }
                                else
                                {
                                    tblCompany_PackagePrice tblcompany_packageprice = new tblCompany_PackagePrice();
                                    tblcompany_packageprice.fkCompanyId = Obj.fkCompanyId;
                                    tblcompany_packageprice.fkLocation = item;
                                    tblcompany_packageprice.IsEnableUniversalPackage = Obj.IsEnableUniversalPackage;
                                    tblcompany_packageprice.PackagePrice = Obj.PackagePrice;
                                    tblcompany_packageprice.fkPackageId = Obj.fkPackageId;
                                    DX.tblCompany_PackagePrices.InsertOnSubmit(tblcompany_packageprice);
                                    DX.SubmitChanges();
                                    IResult = 1;

                                }
                            }
                        }


                    }

                }
                catch
                {
                }

                return IResult;
            }
        }

        public int AddUniversalPackagesForNewLocation(List<tblPackageAccess> lstPackageAccess)
        {
            int IResult = -1;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                try
                {
                    DX.tblPackageAccesses.InsertAllOnSubmit(lstPackageAccess);
                    DX.SubmitChanges();
                    IResult = 1;
                }
                catch
                {
                }

                return IResult;
            }
        }


        #region Teired Packages

        public int InsertTeriedPackage(tblTieredPackage ObjtblTieredPackage)
        {
            int status = 0;
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                try
                {
                    ObjtblTieredPackage.CreatedDate = DateTime.Now;
                    dx.tblTieredPackages.InsertOnSubmit(ObjtblTieredPackage);
                    dx.SubmitChanges();
                    status = 1;
                }
                catch (Exception ex)
                {
                    status = -1;
                    throw ex;
                }
            }
            return status;
        }
        public int UpdateTeriedPackage(tblTieredPackage ObjtblTieredPackage)
        {
            int status = 0;
            tblTieredPackage ObjTieredPackage = new tblTieredPackage();
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                try
                {
                    ObjTieredPackage = dx.tblTieredPackages.Where(d => d.fkPackageId == ObjtblTieredPackage.fkPackageId).FirstOrDefault();
                    if (ObjTieredPackage != null)
                    {
                        ObjTieredPackage.Host = ObjtblTieredPackage.Host;
                        ObjTieredPackage.IsAutomaticRun = ObjtblTieredPackage.IsAutomaticRun;
                        ObjTieredPackage.LastModifiedDate = ObjtblTieredPackage.LastModifiedDate;
                        ObjTieredPackage.Tired2 = ObjtblTieredPackage.Tired2;
                        ObjTieredPackage.Tired3 = ObjtblTieredPackage.Tired3;
                        ObjTieredPackage.TiredOptional = ObjtblTieredPackage.TiredOptional;
                        ObjTieredPackage.Variable = ObjtblTieredPackage.Variable;
                        ObjTieredPackage.TimeFrame = ObjtblTieredPackage.TimeFrame;
                        ObjTieredPackage.HostPrice = ObjtblTieredPackage.HostPrice;
                        ObjTieredPackage.Tired2Price = ObjtblTieredPackage.Tired2Price;
                        ObjTieredPackage.Tired3Price = ObjtblTieredPackage.Tired3Price;
                        ObjTieredPackage.OptionalPrice = ObjtblTieredPackage.OptionalPrice;
                        dx.SubmitChanges();
                        status = 1;
                    }
                    else
                    {
                        InsertTeriedPackage(ObjtblTieredPackage);
                        status = 1;
                    }
                }
                catch (Exception ex)
                {
                    status = -1;
                    throw ex;
                }
            }
            return status;
        }
        public tblTieredPackage GetTeieredPackage(int PackageId)
        {
            tblTieredPackage ObjtblTieredPackage = new tblTieredPackage();
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                ObjtblTieredPackage = dx.tblTieredPackages.Where(d => d.fkPackageId == PackageId).FirstOrDefault();
            }
            return ObjtblTieredPackage;
        }

        public int DeleteTeiredPacakge(int PackageId)
        {
            int status = 0;
            try
            {
                tblTieredPackage ObjtblTieredPackage = new tblTieredPackage();
                using (EmergeDALDataContext dx = new EmergeDALDataContext())
                {
                    ObjtblTieredPackage = dx.tblTieredPackages.Where(d => d.fkPackageId == PackageId).FirstOrDefault();
                    if (ObjtblTieredPackage != null)
                    {
                        dx.tblTieredPackages.DeleteOnSubmit(ObjtblTieredPackage);
                        dx.SubmitChanges();
                    }
                    status = 1;
                }
            }
            catch (Exception ex)
            {
                status = -1;
                throw ex;
            }
            return status;
        }

        #endregion

        /// <summary>
        /// To get Global Credential
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public bool GetCompanyGlobalCredentialCheck(int companyId)
        {
            bool flagGC = false;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                flagGC = DX.tblCompanies.Where(tblc => tblc.pkCompanyId == companyId).Select(tblc => tblc.IsGlobalCredential).FirstOrDefault();
            }
            return flagGC;
        }

        /// <summary>
        /// To get all location based on Company Id.
        /// </summary>
        /// <param name="companyId"></param>
        public List<int> GetAllLocationIdsBasedOnCompany(int companyId)
        {
            List<int> lstLocations = new List<int>();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                lstLocations = DX.tblLocations.Where(tblc => tblc.fkCompanyId == companyId && tblc.IsDeleted == false && tblc.IsEnabled == true).Select(tblc => tblc.pkLocationId).ToList();
            }
            return lstLocations;
        }

        /// <summary>
        /// To get all location based on Company Id.
        /// </summary>
        /// <param name="companyId"></param>
        public List<int> GetAllPackageIdsBasedOnPackageName(string packName)
        {
            List<int> lstLocations = new List<int>();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                lstLocations = DX.tblProductPackages.Where(tblc => tblc.PackageName == packName).Select(tblc => tblc.pkPackageId).ToList();
            }
            return lstLocations;
        }




    }
}
