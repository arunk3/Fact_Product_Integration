﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;
using Emerge;


namespace Emerge.Services
{
    public class BALSSNComplainceReviews
    {
        public int SSNComplainceReviews_InsertGlobleFilter(tblSSNComplainceReviewsMain tblGlobleFilter)
        {
            int Result = -1;

            using (EmergeDALDataContext DbContext = new EmergeDALDataContext())
            {
                tblSSNComplainceReviewsMain objTtblGlobleFilter = new tblSSNComplainceReviewsMain();

                objTtblGlobleFilter.SSN_NO = tblGlobleFilter.SSN_NO;
                objTtblGlobleFilter.IsEnabled = true;
                objTtblGlobleFilter.AddingDate_ = System.DateTime.Today;
                DbContext.tblSSNComplainceReviewsMains.InsertOnSubmit(objTtblGlobleFilter);
                DbContext.SubmitChanges();
                Result = 1;
            }

            return Result;
        }

        public List<tblSSNComplainceReviewsMain> SSNComplainceReviews_GetGlobleFilters()
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {

                return DX.tblSSNComplainceReviewsMains.ToList();
            }
        }

        public tblSSNComplainceReviewsMain SSNComplainceReviews_GetGlobleFilters(int GlobleFilterId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                tblSSNComplainceReviewsMain ObjData = new tblSSNComplainceReviewsMain();
                ObjData = DX.tblSSNComplainceReviewsMains.Where(x => x.PkSSNComplaince == GlobleFilterId).FirstOrDefault();
                return ObjData;
            }
        }

        public List<SSNComplainceReviews> SSNComplainceReviews_GetGlobleFiltersForRemoveRowCourtHouseData()
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                List<SSNComplainceReviews> ObjData = new List<SSNComplainceReviews>();
                ObjData = DX.tblSSNComplainceReviewsMains.Select(x => new SSNComplainceReviews
                {
                    GlobleFilterId = x.PkSSNComplaince,

                    DocketNumber = x.SSN_NO != null ? x.SSN_NO : null,

                    IsEnable = x.IsEnabled,

                }).Where(x => x.IsEnable == true).ToList();
                return ObjData;
            }
        }

        public int SSNComplainceReviews_UpdateGlobleFilters(tblSSNComplainceReviewsMain tblSSNComplainceReview)
        {
            int result = -1;
            tblSSNComplainceReviewsMain objTblGlobleFilter = new tblSSNComplainceReviewsMain();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                objTblGlobleFilter = DX.tblSSNComplainceReviewsMains.Where(x => x.PkSSNComplaince == tblSSNComplainceReview.PkSSNComplaince).FirstOrDefault();

                if (objTblGlobleFilter != null)
                {
                    objTblGlobleFilter.IsEnabled = tblSSNComplainceReview.IsEnabled;

                    objTblGlobleFilter.SSN_NO = tblSSNComplainceReview.SSN_NO;

                    DX.SubmitChanges();
                    result = 1;
                }
            }
            return result;
        }

        public int SSNComplainceReviews_DeleteGlobleFilters(int GlobleFilterId)
        {
            int result = -1;
            tblSSNComplainceReviewsMain objTblGlobleFilter = new tblSSNComplainceReviewsMain();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                objTblGlobleFilter = DX.tblSSNComplainceReviewsMains.Where(x => x.PkSSNComplaince == GlobleFilterId).FirstOrDefault();
                if (objTblGlobleFilter != null)
                {
                    DX.tblSSNComplainceReviewsMains.DeleteOnSubmit(objTblGlobleFilter);
                    DX.SubmitChanges();
                    result = 1;
                }
            }
            return result;
        }


    }

    public class SSNComplainceReviews
    {
        public int GlobleFilterId { get; set; }
        public string ReportType { get; set; }
        public string Keywords { get; set; }
        public string Source { get; set; }
        public string DocketNumber { get; set; }
        public string DeliveryAddress { get; set; }
        public bool IsEnable { get; set; }
    }



}
