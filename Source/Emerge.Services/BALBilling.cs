﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using EmergeBilling;
using System.Data.Linq.Mapping;
using System.Data.Linq;
using System.Reflection;
using Emerge.Data;

namespace Emerge.Services
{
    public class BALBillingSummary
    {
        public string CategoryName { get; set; }
        public int pkProductApplicationId { get; set; }
        public string ProductDisplayName { get; set; }
        public string ProductCode { get; set; }
        public int TotalReports { get; set; }
        public byte pkReportCategoryId { get; set; }
        public decimal TotalEarn { get; set; }
    }

    public class BALBillingSummary2
    {
        public string CategoryName { get; set; }
        public int pkProductApplicationId { get; set; }
        public string ProductDisplayName { get; set; }
        public string ProductCode { get; set; }
        public int TotalReports { get; set; }
        public byte pkReportCategoryId { get; set; }
        public decimal TotalEarn { get; set; }
    }

    public class BALCompanyLocations
    {
        public int pkCompanyId { get; set; }
        public int pkLocationId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyStatus { get; set; }
        public DateTime StatusStartDate { get; set; }
        public string CompanyCity { get; set; }
        public string CompanyState { get; set; }
        public string LocationCity { get; set; }
        public string PhoneNo1 { get; set; }
        public string MainEmailId { get; set; }
        public bool IsHome { get; set; }
        public DateTime LocationCreatedDate { get; set; }
    }

    public class BALProductsPerLocation
    {
        public int fkLocationId { get; set; }
        public int fkProductApplicationId { get; set; }
        public string ProductDisplayName { get; set; }
        public string ProductCode { get; set; }
        public byte fkReportCategoryId { get; set; }
        public int TotalReports { get; set; }
        public decimal ReportAmount { get; set; }
        public bool IsManualReport { get; set; }
        public byte IsAdjustmentProduct { get; set; }
        public int ProductQty { get; set; }
    }

    /*public class BALMVRFee
    {
        public Guid fkLocationId { get; set; }
        public string ProductCode { get; set; }
        public string StateCode { get; set; }
        public decimal MVRFee { get; set; }
        public int TotalReports { get; set; }
        public decimal MVRAmt { get; set; }
    }

    public class BALSCRFee
    {
        public Guid fkLocationId { get; set; }
        public string ProductCode { get; set; }
        public string StateCode { get; set; }
        public decimal SCRFee { get; set; }
        public int TotalReports { get; set; }
        public decimal SCRAmt { get; set; }
    }

    public class BALCCR1Fee
    {
        public Guid fkLocationId { get; set; }
        public string ProductCode { get; set; }
        public string CountyCode { get; set; }
        public decimal CCRFee { get; set; }
        public int TotalReports { get; set; }
        public decimal CCR1Amt { get; set; }
    }

    public class BALCCR2Fee
    {
        public Guid fkLocationId { get; set; }
        public string ProductCode { get; set; }
        public string CountyCode { get; set; }
        public decimal CCRFee { get; set; }
        public int TotalReports { get; set; }
        public decimal CCR2Amt { get; set; }
    }
    public class BALRcxFee
    {
        public Guid fkLocationId { get; set; }
        public string ProductCode { get; set; }
        public string CountyCode { get; set; }
        public decimal RcxFee { get; set; }
        public int TotalReports { get; set; }
        public decimal RcxAmt { get; set; }
    }*/
    public class BALPackagesPrice
    {
        public string PackageInclude { get; set; }
        public decimal ReportAmount { get; set; }
        public int fkPackageId { get; set; }
        public int fkLocationId { get; set; }
        public string PackageName { get; set; }
        public int TotalPackages { get; set; }
    }

    public class BALAdditionalFeeprocforInvoice
    {
        public int fkLocationId { get; set; }

        public string PackageDisplayName { get; set; }
        public string orderNo { get; set; }
        public string ReportIncludes { get; set; }
        public int IsDrugTestingOrder { get; set; }
        public string ReportWithStatus { get; set; }
        public decimal AdditionalFees { get; set; }
    }


    public class BALReportsFee
    {
        public int fkLocationId { get; set; }
        public string ProductCode { get; set; }
        public string CCR1CountyCode { get; set; }
        public string CCR2CountyCode { get; set; }
        public string MVRStateCode { get; set; }
        public string SCRStateCode { get; set; }
        public string RCXCountyCode { get; set; }
        public string FCRCountyCode { get; set; }
        public decimal AdditionalFee { get; set; }
        public decimal ReportFee { get; set; }
        public int TotalReports { get; set; }
        public decimal ReportAmount { get; set; }
    }
    public class BALPackageReport
    {
        public int fkLocationId { get; set; }
        public string ProductCode { get; set; }
        public string CCR1CountyCode { get; set; }
        public string CCR2CountyCode { get; set; }
        public string MVRStateCode { get; set; }
        public string SCRStateCode { get; set; }
        public string RCXCountyCode { get; set; }
        public string FCRCountyCode { get; set; }
        public decimal AdditionalFee { get; set; }
        public int fkPackageId { get; set; }
        public decimal ReportAmount { get; set; }
        public int TotalReports { get; set; }
        public string PackageInclude { get; set; }
    }

    [Database(Name = "emergemvc2012")]
    public class BALBilling : EmergeDALDataContext
    {
        [Function(Name = "dbo.Proc_Get_BillingForAdmin")]

        [ResultType(typeof(BALBillingSummary))]
        [ResultType(typeof(BALCompanyLocations))]
        [ResultType(typeof(BALProductsPerLocation))]
        /*[ResultType(typeof(BALMVRFee))]
        [ResultType(typeof(BALSCRFee))]
        [ResultType(typeof(BALCCR1Fee))]
        [ResultType(typeof(BALCCR2Fee))]
        [ResultType(typeof(BALRcxFee))]*/
        [ResultType(typeof(BALPackagesPrice))]
        [ResultType(typeof(BALReportsFee))]
        [ResultType(typeof(BALPackageReport))]
        public IMultipleResults GetBillingForAdmin(Guid ApplicationId, int BillingMonth, int BillingYear, Guid CompanyId, Guid SalesRepId, Guid LocationId, Guid UserId, int RefCodeId)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), ApplicationId, BillingMonth, BillingYear, CompanyId, SalesRepId, LocationId, UserId, RefCodeId);
            return (IMultipleResults)(result.ReturnValue);

        }

        [Function(Name = "dbo.Proc_Get_BillingForOthers")]

        [ResultType(typeof(BALBillingSummary))]
        [ResultType(typeof(BALCompanyInformation))]
        [ResultType(typeof(BALCompanyLocationsForOthers))]
        [ResultType(typeof(BALProductsPerLocation))]

        public IMultipleResults GetBillingForOthers(Guid ApplicationId, int BillingMonth, int BillingYear, Guid CompanyId, Guid LocationId, Guid UserId, Guid SalesRepId, int RefCodeId)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), ApplicationId, BillingMonth, BillingYear, CompanyId, LocationId, UserId, SalesRepId, RefCodeId);
            return (IMultipleResults)(result.ReturnValue);
        }

        [Function(Name = "dbo.Proc_BillingDrugRestingFirstResultset")]
        [ResultType(typeof(BALBillingSummary))]
        public IMultipleResults BillingDrugTestingFirstResultset(Guid ApplicationId, int BillingMonth, int BillingYear, int CompanyId, int LocationId, int UserId, Guid SalesRepId, int RefCodeId, short OrderType)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), ApplicationId, BillingMonth, BillingYear, CompanyId, LocationId, UserId, SalesRepId, RefCodeId, OrderType);
            return (IMultipleResults)(result.ReturnValue);
        }

        [Function(Name = "dbo.Proc_BillingCompanyInfoSecondResultset")]
        [ResultType(typeof(BALCompanyInformation))]
        public IMultipleResults BillingCompanyInfoSecondResultset(Guid ApplicationId, int BillingMonth, int BillingYear, int CompanyId, int LocationId, int UserId, Guid SalesRepId, int RefCodeId, short OrderType)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), ApplicationId, BillingMonth, BillingYear, CompanyId, LocationId, UserId, SalesRepId, RefCodeId, OrderType);
            return (IMultipleResults)(result.ReturnValue);
        }

        [Function(Name = "dbo.Proc_BillingCompanyLocationInfoThirdResultset")]
        [ResultType(typeof(BALCompanyLocationsForOthers))]
        public IMultipleResults BillingCompanyLocationInfoThirdResultset(Guid ApplicationId, int BillingMonth, int BillingYear, int CompanyId, int LocationId, int UserId, Guid SalesRepId, int RefCodeId, short OrderType)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), ApplicationId, BillingMonth, BillingYear, CompanyId, LocationId, UserId, SalesRepId, RefCodeId, OrderType);
            return (IMultipleResults)(result.ReturnValue);
        }

        [Function(Name = "dbo.Proc_BillingCompanyProdctInfoForthdResultset")]
        [ResultType(typeof(BALProductsPerLocation))]
        public IMultipleResults BillingCompanyProdctInfoForthdResultset(Guid ApplicationId, int BillingMonth, int BillingYear, int CompanyId, int LocationId, int UserId, Guid SalesRepId, int RefCodeId, short OrderType)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), ApplicationId, BillingMonth, BillingYear, CompanyId, LocationId, UserId, SalesRepId, RefCodeId, OrderType);
            return (IMultipleResults)(result.ReturnValue);
        }

        [Function(Name = "dbo.Proc_BillingCompanyAdditionalFeesInfoFivthResultset")]
        [ResultType(typeof(BALPackagesPrice))]
        public IMultipleResults BillingCompanyAdditionalFeesInfoFivthResultset(Guid ApplicationId, int BillingMonth, int BillingYear, int CompanyId, int LocationId, int UserId, Guid SalesRepId, int RefCodeId, short OrderType)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), ApplicationId, BillingMonth, BillingYear, CompanyId, LocationId, UserId, SalesRepId, RefCodeId, OrderType);
            return (IMultipleResults)(result.ReturnValue);
        }


        [Function(Name = "dbo.Proc_BillingCompanyCCRAdditionlInfoSixthResultset")]
        [ResultType(typeof(BALPackageReport))]
        public IMultipleResults BillingCompanyCCRAdditionlInfoSixthResultset(Guid ApplicationId, int BillingMonth, int BillingYear, int CompanyId, int LocationId, int UserId, Guid SalesRepId, int RefCodeId, short OrderType)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), ApplicationId, BillingMonth, BillingYear, CompanyId, LocationId, UserId, SalesRepId, RefCodeId, OrderType);
            return (IMultipleResults)(result.ReturnValue);
        }

        [Function(Name = "dbo.Proc_BillingCompanyEMV_EDVSeventhhResultset")]
        [ResultType(typeof(BALReportsFee))]
        public IMultipleResults BillingCompanyEMV_EDVSeventhhResultset(Guid ApplicationId, int BillingMonth, int BillingYear, int CompanyId, int LocationId, int UserId, Guid SalesRepId, int RefCodeId, short OrderType)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), ApplicationId, BillingMonth, BillingYear, CompanyId, LocationId, UserId, SalesRepId, RefCodeId, OrderType);
            return (IMultipleResults)(result.ReturnValue);
        }




        [Function(Name = "dbo.Proc_Get_BillingForSales")]
        [ResultType(typeof(BALBillingSummary))]
        [ResultType(typeof(BALCompanyLocations))]
        [ResultType(typeof(BALProductsPerLocation))]
        [ResultType(typeof(BALPackagesPrice))]
        [ResultType(typeof(BALReportsFee))]
        [ResultType(typeof(BALPackageReport))]
        public IMultipleResults GetBillingForSales(Guid ApplicationId, int BillingMonth, int BillingYear, int CompanyId, Guid SalesRepId, int LocationId, int UserId, int RefCodeId, string CompStatus)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), ApplicationId, BillingMonth, BillingYear, CompanyId, SalesRepId, LocationId, UserId, RefCodeId, CompStatus);
            return (IMultipleResults)(result.ReturnValue);
        }

        [Function(Name = "dbo.Proc_Get_BillingForOthersMVC")]

        [ResultType(typeof(BALBillingSummary))]
        [ResultType(typeof(BALCompanyInformation))]
        [ResultType(typeof(BALCompanyLocationsForOthers))]
        [ResultType(typeof(BALProductsPerLocation))]

        public IMultipleResults GetBillingForOthersMVC(Guid ApplicationId, int BillingMonth, int BillingYear, int CompanyId, int LocationId, int UserId, Guid SalesRepId, int RefCodeId, short OrderType)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), ApplicationId, BillingMonth, BillingYear, CompanyId, LocationId, UserId, SalesRepId, RefCodeId, OrderType);
            return (IMultipleResults)(result.ReturnValue);
        }

        [Function(Name = "dbo.Proc_Get_BillingForAdminMVC")]

        [ResultType(typeof(BALBillingSummary))]
        [ResultType(typeof(BALCompanyLocations))]
        [ResultType(typeof(BALProductsPerLocation))]
        /*[ResultType(typeof(BALMVRFee))]
        [ResultType(typeof(BALSCRFee))]
        [ResultType(typeof(BALCCR1Fee))]
        [ResultType(typeof(BALCCR2Fee))]
        [ResultType(typeof(BALRcxFee))]*/
        [ResultType(typeof(BALPackagesPrice))]
        [ResultType(typeof(BALReportsFee))]
        [ResultType(typeof(BALPackageReport))]
        public IMultipleResults GetBillingForAdminMVC(Guid ApplicationId, int BillingMonth, int BillingYear, int CompanyId, Guid SalesRepId, int LocationId, int UserId, int RefCodeId, short OrderType)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), ApplicationId, BillingMonth, BillingYear, CompanyId, SalesRepId, LocationId, UserId, RefCodeId, OrderType);
            return (IMultipleResults)(result.ReturnValue);

        }
        [Function(Name = "dbo.Proc_GetBillingReportData")]
        [ResultType(typeof(BALBillingSummary))]
        [ResultType(typeof(BALBillingSummary2))]
        [ResultType(typeof(BALProductsPerLocation))]
        [ResultType(typeof(BALReportsByLocation))]
        [ResultType(typeof(proc_GetBillingAdjustmentsResult))]
        [ResultType(typeof(Totals))]
        [ResultType(typeof(Totals2))]
        public IMultipleResults GetBillingReportData(Guid ApplicationId, int BillingMonth, int BillingYear, int CompanyId, Guid SalesRepId, int LocationId, int UserId, int RefCodeId)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), ApplicationId, BillingMonth, BillingYear, CompanyId, SalesRepId, LocationId, UserId, RefCodeId);
            return (IMultipleResults)(result.ReturnValue);

        }

        [Function(Name = "dbo.Proc_GetBillingReportData_API")]
        [ResultType(typeof(BALBillingSummary))]
        [ResultType(typeof(BALBillingSummary2))]
        [ResultType(typeof(BALReportsByLocation))]
        [ResultType(typeof(proc_GetBillingAdjustmentsResult))]
        public IMultipleResults GetBillingReportData_API(Guid ApplicationId, int BillingMonth, int BillingYear, int CompanyId, Guid SalesRepId, int LocationId, int UserId, int RefCodeId)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), ApplicationId, BillingMonth, BillingYear, CompanyId, SalesRepId, LocationId, UserId, RefCodeId);
            return (IMultipleResults)(result.ReturnValue);

        }
    }

    public class BALCompanyInformation
    {
        public string CompanyName { get; set; }
        public string MainEmailId { get; set; }
        public string BillingEmailId { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string StateCode { get; set; }

        public string MainContactPersonName { get; set; }
        public string Address1 { get; set; }
        public string PhoneNo1 { get; set; }
        public string PhoneNo2 { get; set; }
        public string Fax { get; set; }
        public string Fax2 { get; set; }
        public string BillingContactPersonName { get; set; }
        public string Address2 { get; set; }
        public bool IsHome { get; set; }
        public string StaticBillingId { get; set; }
        public string StaticPhone { get; set; }
    }

    public class BALCompanyLocationsForOthers
    {
        public int pkCompanyId { get; set; }
        public int pkLocationId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyState { get; set; }
        public string LocationCity { get; set; }
        public bool IsHome { get; set; }
    }

    public class BALReportsByLocation
    {
        public string CompanyDisplay { get; set; }
        public string ProductDisplayName { get; set; }
        public bool IsMainHeading { get; set; }
        public int TotalReports { get; set; }
        public int TotalOrderedReports { get; set; }
        public decimal ReportAmount { get; set; }
        public decimal TotalReportAmount { get; set; }
        public bool IsManualReport { get; set; }
        public byte IsAdjustmentProduct { get; set; }
        public Int16 ProductQty { get; set; }
        public bool IsHome { get; set; }
        public string CompanyName { get; set; }
        public string CompanyState { get; set; }
        public string LocationCity { get; set; }
        public Int16 IsNull { get; set; }
        public bool IsCompany { get; set; }
        public bool IsProduct { get; set; }
        public bool IsSubProduct { get; set; }
        public bool IsPackage { get; set; }
        public bool IsSubPackage { get; set; }
        public bool IsDrugTestingProducts { get; set; }
        public bool IsHomeIcon { get; set; }
        public bool IsCompanyIcon { get; set; }
        public bool IsTieredIcon { get; set; }
        public bool IsTotal { get; set; }
        public string LocationDisplayName { get; set; }
        public Int16 SerialNumber { get; set; }
    }


}
