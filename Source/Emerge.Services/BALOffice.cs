﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;
using System.Data.Common;
using System.Data;
using Emerge.Data;

namespace Emerge.Services
{
    public class BALOffice
    {
        EmergeDALDataContext ObjEmergeDALDataContext;

        /// <summary>
        /// Function to Get All Sales Reps in form of ClsSalesRep User Defined Class
        /// </summary>
        /// <returns></returns>
        public tblCompanyUser GetCompanyUser(Guid fkUserId, out string Sel_RoleName, out Guid Sel_RoleID)
        {
            string RoleName = string.Empty;
            Guid RoleID = Guid.Empty;
            tblCompanyUser ObjtblCompanyUser = new tblCompanyUser();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                ObjtblCompanyUser = DX.tblCompanyUsers.Where(d => d.fkUserId == fkUserId).FirstOrDefault();

                if (ObjtblCompanyUser != null)
                    RoleName = GetUserRoleIDByUsername(ObjtblCompanyUser.fkUserId, out RoleID);
            }
            Sel_RoleID = RoleID;
            Sel_RoleName = RoleName;
            return ObjtblCompanyUser;
        }

        public List<Proc_Get_Recent_ReviewClientsResult> GetRecentReviewClients(Guid UserId)
        {

            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.Get_Recent_ReviewClients(UserId).ToList();
            }
        }


        //public Guid GetRoleid(Guid UserId)
        //{
        //    using (EmergeDALDataContext DX = new EmergeDALDataContext())
        //    {
        //        return (DX.aspnet_UsersInRoles.Where(d => d.UserId == UserId).Select(d => d.RoleId).FirstOrDefault());
        //    }
        //}
 
        public List<Proc_GetAlertsForOfficeUserResult> GetAlertsForOfficeUser(Guid Userid,int PageNum,int PageSize,string SortingColumn,string SortingDirection)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                return Dx.GetAlertsForOfficeUser(Userid, PageNum, PageSize, SortingColumn, SortingDirection).ToList();
            }
        }

        public List<sManagers> GetManagerList(Guid SaleManagerRoleID)
        {
            //string RoleName = string.Empty;
           // Guid RoleID = Guid.Empty;
            // RoleID=  GetRoleid(userid);
            List<sManagers> ObjsManagers = new List<sManagers>();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                ObjsManagers = (from cu in DX.tblCompanyUsers
                                join uir in DX.aspnet_UsersInRoles
                                on cu.fkUserId equals uir.UserId
                                where (uir.RoleId == SaleManagerRoleID && cu.IsDeleted == false)
                                select new sManagers
                                {
                                    FirstName = cu.FirstName,
                                    LastName = cu.LastName,
                                    FullName = cu.FirstName + " " + cu.LastName,
                                    UserId = uir.UserId
                                }).OrderBy(d => d.FullName).ToList<sManagers>();

            }

            return ObjsManagers;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Uid"></param>
        /// <param name="SelectedRoleId"></param>
        /// <returns></returns>
        public string GetUserRoleIDByUsername(Guid Uid, out Guid SelectedRoleId)
        {
            string RoleName = "";
            Guid RoleID1 = Guid.Empty;
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                var Results = from a_u in ObjEmergeDALDataContext.aspnet_Users
                              join u_r in ObjEmergeDALDataContext.aspnet_UsersInRoles
                                  on a_u.UserId equals u_r.UserId
                              join r in ObjEmergeDALDataContext.aspnet_Roles
                                  on u_r.RoleId equals r.RoleId
                              where a_u.UserId == Uid
                              select new
                              {
                                  r.RoleName,
                                  r.RoleId
                              };
                RoleName = Results.FirstOrDefault().RoleName.ToLower();
                RoleID1 = Results.FirstOrDefault().RoleId;
            }
            SelectedRoleId = RoleID1;
            return RoleName;
        }
        ///// <summary>
        ///// Function To Get All Sales Office Users By Manager and Admin Role
        ///// </summary>
        ///// <param name="PageIndex"></param>
        ///// <param name="PageSize"></param>
        ///// <param name="IsPaging"></param>
        ///// <param name="SearchString"></param>
        ///// <returns></returns>            
        public List<Proc_GetSalesUsersByRoleResult> GetAllSalesUsers(int PageIndex, int PageSize, bool IsPaging, Int16 StatusType, int CompanyId, int FkLocationId, string SortingColumn, string SortingDirection, string UserRole, Guid UserId, string FilterColumn, byte FilterAction, string FilterValue, out int TotalRec)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                int? count = 0;
                var list= ObjEmergeDALDataContext.GetSalesUsersByRole(PageIndex, PageSize, IsPaging, StatusType, CompanyId, FkLocationId, SortingColumn, SortingDirection, UserRole, UserId, FilterColumn, FilterAction, FilterValue,ref count).ToList<Proc_GetSalesUsersByRoleResult>();
                TotalRec = (int)count;
                return list;
            }
        }

        public int InsertRelation(tblSalesUserRelationship ObjSalRel)
        {
            int iresult = 0;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                DX.tblSalesUserRelationships.InsertOnSubmit(ObjSalRel);
                DX.SubmitChanges();
                iresult = 1;
            }
            return iresult;
        }
        public int UpdateRelation(tblSalesUserRelationship objRel1)
        {
            int iresult = 0;
            tblSalesUserRelationship ObjRel = new tblSalesUserRelationship();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                ObjRel = DX.tblSalesUserRelationships.Where(d => d.pkSalesRelationId == objRel1.pkSalesRelationId).FirstOrDefault();
                if (ObjRel != null)
                {
                    ObjRel.fkSalesManagerId = objRel1.fkSalesManagerId;
                    // ObjRel.fkSalesAssociateId = objRel1.fkSalesAssociateId;
                    DX.SubmitChanges();
                    iresult = 1;
                }
            }
            return iresult;
        }
        //public int GetRelationId(Guid ManagerId, Guid AssocateId)
        //{
        //    int iresult = 0;
        //    using (EmergeDALDataContext DX = new EmergeDALDataContext())
        //    {
        //        iresult = DX.tblSalesUserRelationships.Where(d => d.fkSalesManagerId == ManagerId && d.fkSalesAssociateId == AssocateId).Select(d => d.pkSalesRelationId).FirstOrDefault();
        //    }
        //    return iresult;
        //}
        public Guid GetManagerID(Guid userid, out int pkRelationID)
        {
            Guid mangerId = Guid.Empty;
            int RelID = 0;
            tblSalesUserRelationship ObjRel = new tblSalesUserRelationship();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                ObjRel = DX.tblSalesUserRelationships.Where(d => d.fkSalesAssociateId == userid).FirstOrDefault();
                if (ObjRel != null)
                {
                    RelID = ObjRel.pkSalesRelationId;
                    mangerId = ObjRel.fkSalesManagerId;
                }
            }
            pkRelationID = RelID;
            return mangerId;
        }

        public bool DeleteEmergeUserById(int CompanyUserId)
        {
            Guid managerID = Guid.Empty;
            List<tblSalesUserRelationship> ObjRel = new List<tblSalesUserRelationship>();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                if (DX.DeleteCompanyUser(CompanyUserId) == 1)
                {
                    managerID = DX.tblCompanyUsers.Where(d => d.pkCompanyUserId == CompanyUserId).Select(d => d.fkUserId).FirstOrDefault();
                    if (managerID != Guid.Empty)
                    {
                        ObjRel = DX.tblSalesUserRelationships.Where(d => d.fkSalesManagerId == managerID).ToList<tblSalesUserRelationship>();
                        if (ObjRel.Count > 0)
                        {
                            DX.tblSalesUserRelationships.DeleteAllOnSubmit(ObjRel);
                            DX.SubmitChanges();
                        }
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
        } 
        
        
        /////// <summary>
        /////// Function to Add New Lead 
        /////// </summary>
        /////// <param name="ObjtblCompany"></param>
        ////public List<Guid> InsertNewCompanyLead(tblCompany ObjtblCompany, tblLocation ObjtblLocation)
        ////{
        ////    List<Guid> ObjGuid_CompLocation = new List<Guid>();

        ////    BALLocation ObjBALLocation;

        ////    ObjBALLocation = new BALLocation();
        ////    DbTransaction ObjDbTransaction = null;
        ////    Guid NewLocationId = Guid.Empty;
        ////    Guid CompanyId = Guid.Empty;
        ////    try
        ////    {
        ////        using (ObjEmergeDALDataContext = new EmergeDALDataContext())
        ////        {
        ////            //if (ObjDbTransaction.Connection.State == ConnectionState.Open)
        ////            //{
        ////            //    ObjDbTransaction.Connection.Close();
        ////            //}
        ////            //ObjDbTransaction.Connection.Open();
        ////            //ObjDbTransaction = ObjEmergeDALDataContext.Connection.BeginTransaction();
        ////            //ObjEmergeDALDataContext.Transaction = ObjDbTransaction;
        ////            //int IsDuplicateAccountNumber = (from db in ObjEmergeDALDataContext.tblCompanies
        ////            //                                where db.CompanyAccountNumber == ObjtblCompany.CompanyAccountNumber
        ////            //                                select db).Count();
        ////            //if (IsDuplicateAccountNumber == 0)
        ////            //{
        ////                var CompanyResult = ObjEmergeDALDataContext.AddCompanyLead(
        ////                                             ObjtblCompany.fkCompanyTypeId,
        ////                                             ObjtblCompany.fkApplicationId,
        ////                                             ObjtblCompany.CompanyCode,
        ////                                             ObjtblCompany.fkSalesRepId,
        ////                                             ObjtblCompany.fkSalesManagerId,
        ////                                             ObjtblCompany.CompanyAccountNumber,
        ////                                             ObjtblCompany.CompanyNote,
        ////                                             ObjtblCompany.CompanyName,
        ////                                             ObjtblCompany.FranchiseName,
        ////                                             ObjtblCompany.MainContactPersonName,
        ////                                             ObjtblCompany.BillingContactPersonName,
        ////                                             ObjtblCompany.MainEmailId,
        ////                                             ObjtblCompany.BillingEmailId,
        ////                                             ObjtblCompany.LeadSource,
        ////                                             ObjtblCompany.LeadNumber,
        ////                                             ObjtblCompany.CurrentProvider,
        ////                                             ObjtblCompany.NoOfLocations,
        ////                                             ObjtblCompany.LeadStatus,
        ////                                             ObjtblCompany.Priority1,
        ////                                             ObjtblCompany.Priority2,
        ////                                             ObjtblCompany.Priority3,
        ////                                             ObjtblCompany.IsEnabled,
        ////                                             ObjtblCompany.IsDeleted,
        ////                                             ObjtblCompany.CreatedDate,
        ////                                             ObjtblCompany.LastModifiedDate,
        ////                                             ObjtblCompany.CreatedById,
        ////                                             ObjtblCompany.LastModifiedById,
        ////                                             ObjtblCompany.IsEnableReview
        ////                );

        ////                CompanyId = new Guid(CompanyResult.ElementAt(0).CompanyId.ToString());

        ////                ObjGuid_CompLocation.Add(CompanyId == Guid.Empty ? Guid.Empty : CompanyId);

        ////                //if (CompanyId == Guid.Empty)
        ////                //{
        ////                //    if (ObjDbTransaction != null)
        ////                //    {
        ////                //        ObjDbTransaction.Rollback();
        ////                //    }
        ////                //    return CompanyId;
        ////                //}

        ////                NewLocationId = ObjBALLocation.InsertNewLocation(CompanyId, ObjtblLocation);

        ////                ObjGuid_CompLocation.Add(NewLocationId == Guid.Empty ? Guid.Empty : NewLocationId);

        ////                //if (NewLocationId == Guid.Empty)
        ////                //{
        ////                //    if (ObjDbTransaction != null)
        ////                //    {
        ////                //        ObjDbTransaction.Rollback();
        ////                //    }
        ////                //    return NewLocationId;
        ////                //}
        ////                //ObjDbTransaction.Commit();
        ////                //return NewLocationId;
        ////            //}
        ////            //else
        ////            //{
        ////            //    return ObjGuid_CompLocation;
        ////            //}
        ////        }
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        throw ex;
        ////    }
        ////    finally
        ////    {

        ////    }
        ////    return ObjGuid_CompLocation;
        ////}


        /////// <summary>
        /////// Function To Update Lead 
        /////// </summary>
        /////// <returns></returns>
        ////public int UpdateLead(tblCompany ObjtblCompany, tblLocation ObjtblLocation)
        ////{
        ////    DbTransaction ObjDbTransaction = null;
        ////    BALLocation ObjBALLocation;
        ////    using (ObjEmergeDALDataContext = new EmergeDALDataContext())
        ////    {
        ////        ObjEmergeDALDataContext.Connection.Open();
        ////        ObjDbTransaction = ObjEmergeDALDataContext.Connection.BeginTransaction();
        ////        ObjEmergeDALDataContext.Transaction = ObjDbTransaction;
        ////        //int IsDuplicateAccountNumber = (from db in ObjEmergeDALDataContext.tblCompanies
        ////        //                                where db.CompanyAccountNumber == ObjtblCompany.CompanyAccountNumber
        ////        //                                && db.pkCompanyId != ObjtblCompany.pkCompanyId && db.IsDeleted == false
        ////        //                                select db).Count();
        ////        //if (IsDuplicateAccountNumber == 0)
        ////        //{
        ////            int Result = ObjEmergeDALDataContext.UpdateLead(ObjtblCompany.pkCompanyId,
        ////                                                    ObjtblCompany.fkSalesRepId,
        ////                                                    ObjtblCompany.CompanyAccountNumber,
        ////                                                    ObjtblCompany.fkCompanyTypeId,
        ////                                                    ObjtblCompany.CompanyName,
        ////                                                    ObjtblCompany.CompanyNote,
        ////                                                    ObjtblCompany.FranchiseName,
        ////                                                    ObjtblCompany.MainContactPersonName,
        ////                                                    ObjtblCompany.BillingContactPersonName,
        ////                                                    ObjtblCompany.MainEmailId,
        ////                                                    ObjtblCompany.BillingEmailId,
        ////                                                    ObjtblCompany.IsEnabled,
        ////                                                    ObjtblCompany.LastModifiedDate,
        ////                                                    ObjtblCompany.LastModifiedById,
        ////                                                    ObjtblCompany.IsEnableReview,
        ////                                                    ObjtblCompany.fkSalesManagerId,
        ////                                                    ObjtblCompany.LeadSource,
        ////                                                    ObjtblCompany.CurrentProvider,
        ////                                                    ObjtblCompany.NoOfLocations,
        ////                                                    ObjtblCompany.Priority1,
        ////                                                    ObjtblCompany.Priority2,
        ////                                                    ObjtblCompany.Priority3,
        ////                                                    ObjtblCompany.LeadStatus);

        ////            if (Result < 0)
        ////            {
        ////                if (ObjDbTransaction != null)
        ////                {
        ////                    ObjDbTransaction.Rollback();
        ////                }
        ////                return -2;//false   //-2 error in company updating
        ////            }

        ////            ObjBALLocation = new BALLocation();

        ////            int LocationResult = ObjBALLocation.UpdateLocation(ObjtblLocation, ObjtblCompany.pkCompanyId);

        ////            if (LocationResult != 1)
        ////            {
        ////                if (ObjDbTransaction != null)
        ////                {
        ////                    ObjDbTransaction.Rollback();
        ////                }
        ////                return -3;//LocationResult // -3 error in Location updating

        ////            }

        ////            ObjDbTransaction.Commit();

        ////            return 1;//LocationResult //1 for success
        ////        //}
        ////        //return -4;//false //-4 for already exist
        ////    }
        ////}


        ///// <summary>
        ///// Function To Add Recent Reviewed Lead 
        ///// </summary>
        ///// <returns></returns>
        public int AddRecentReviewedLead(Guid UserId, int pkCompanyId)
        {
            int iresult = 0;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                DX.AddRecentLeads(UserId, pkCompanyId);
                DX.SubmitChanges();
                iresult = 1;
            }
            return iresult;
        }

        ///// <summary>
        ///// Function To Get Recent Review Leads by UserId
        ///// </summary>
        ///// <param name="UserId"></param>
        ///// <returns></returns>            
        public List<Proc_Get_Recent_ReviewsResult> GetRecentReviewLeads(Guid UserId)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                //return ObjEmergeDALDataContext.GetRecentReviewLeads(UserId).ToList<Proc_Get_Recent_ReviewLeadsResult>();
                return ObjEmergeDALDataContext.Get_Recent_Reviews(UserId).ToList<Proc_Get_Recent_ReviewsResult>();
            }
        }

        ///// <summary>
        ///// Function To Get Recent Review Clients by UserId
        ///// </summary>
        ///// <param name="UserId"></param>
        ///// <returns></returns>            
        //public List<Proc_Get_Recent_ReviewClientsResult> GetRecentReviewClients(Guid UserId)
        //{
        //    using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
        //    {
        //        return ObjEmergeDALDataContext.GetRecentReviewClients(UserId).ToList<Proc_Get_Recent_ReviewClientsResult>();
        //    }
        //}

        public int InsertDocumentDetails(tblOfficeDocument ObjtblOfficeDocument)
        {
            int Result = 0;
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    if ((ObjEmergeDALDataContext.tblOfficeDocuments.Where(d => d.DocumentTitle == ObjtblOfficeDocument.DocumentTitle).Count()) == 0)
                    {
                        tblOfficeDocument tbl = new tblOfficeDocument();
                        tbl.pkDocumentId = ObjtblOfficeDocument.pkDocumentId;
                        tbl.DocumentTitle = ObjtblOfficeDocument.DocumentTitle;
                        tbl.DocumentPath = ObjtblOfficeDocument.DocumentPath;
                        tbl.CreatedDate = ObjtblOfficeDocument.CreatedDate;

                        ObjEmergeDALDataContext.tblOfficeDocuments.InsertOnSubmit(tbl);
                        ObjEmergeDALDataContext.SubmitChanges();
                        Result = 1;
                    }
                    else
                    {
                        Result = -1;
                    }
                }
                catch (Exception)
                {

                    Result = 0;
                }
            }
            return Result;
        }


        public int UpdateDocumentDetails(tblOfficeDocument ObjtblOfficeDocument)
        {
            int Result = 0;
            tblOfficeDocument ObjDoc = new tblOfficeDocument();
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    if ((ObjEmergeDALDataContext.tblOfficeDocuments.Where(d => d.DocumentTitle == ObjtblOfficeDocument.DocumentTitle && d.pkDocumentId != ObjtblOfficeDocument.pkDocumentId).Count()) == 0)
                    {
                        ObjDoc = ObjEmergeDALDataContext.tblOfficeDocuments.Where(d => d.pkDocumentId == ObjtblOfficeDocument.pkDocumentId).FirstOrDefault();
                        if (ObjDoc != null)
                        {
                            ObjDoc.DocumentTitle = ObjtblOfficeDocument.DocumentTitle;
                            ObjDoc.DocumentPath = ObjtblOfficeDocument.DocumentPath;
                            ObjDoc.LastModifiedDate = ObjtblOfficeDocument.LastModifiedDate;
                            ObjEmergeDALDataContext.SubmitChanges();
                            Result = 1;
                        }
                    }
                    else
                    {
                        Result = -1;
                    }

                }
                catch (Exception)
                {

                    Result = 0;
                }
            }
            return Result;
        }


        public int DeleteDocumentsByDocId(int pkDocumentId)
        {
            int Result = 0;
            try
            {
                using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
                {
                    tblOfficeDocument tbl = ObjEmergeDALDataContext.tblOfficeDocuments.Where(d => d.pkDocumentId == pkDocumentId).FirstOrDefault();
                    if (tbl != null)
                    {
                        ObjEmergeDALDataContext.tblOfficeDocuments.DeleteOnSubmit(tbl);
                        ObjEmergeDALDataContext.SubmitChanges();
                        Result = 1;
                    }

                }
            }
            catch
            {

            }
            return Result;
        }

        public List<tblOfficeDocument> GetAllDocuments()
        {
            List<tblOfficeDocument> lst = new List<tblOfficeDocument>();
            try
            {
                using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
                {
                    lst = ObjEmergeDALDataContext.tblOfficeDocuments.OrderBy(d => d.CreatedDate).ToList();

                }
            }
            catch
            {

            }
            return lst;
        }
        public tblOfficeDocument GetDocumentsByDocId(int pkDocumentId)
        {
            tblOfficeDocument lst = new tblOfficeDocument();
            try
            {
                using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
                {
                    lst = ObjEmergeDALDataContext.tblOfficeDocuments.Where(d => d.pkDocumentId == pkDocumentId).FirstOrDefault();

                }
            }
            catch
            {

            }
            return lst;
        }

        /// <summary>
        /// Function to Get All the Leads for emergeOffice by application id,Enabled,Deleted
        /// </summary>
        /// <param name="ApplicationId"></param>
        /// <param name="Enabled"></param>
        /// <param name="Deleted"></param>
        /// <returns></returns>
        public List<CompanyList> GetAllLeadsForEmergeOffice(Guid ApplicationId, bool Enabled, bool Deleted)
        {
            using (ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                IQueryable<CompanyList> ObjData = from c in ObjEmergeDALDataContext.tblCompanies
                                                  join l in ObjEmergeDALDataContext.tblLocations on c.pkCompanyId equals l.fkCompanyId
                                                  join s in ObjEmergeDALDataContext.tblStates on l.fkStateID equals s.pkStateId
                                                  where c.fkApplicationId == ApplicationId && c.IsDeleted == Deleted && l.IsHome == true && c.LeadStatus >= 1 && c.LeadStatus <= 4
                                                  orderby c.CompanyName ascending
                                                  select new CompanyList
                                                  {
                                                      CompanyName = c.CompanyName + "  [" + l.City + ", " + s.StateCode + "]",
                                                      pkCompanyId = c.pkCompanyId
                                                  };
                return ObjData.ToList();
            }
        }

        ///// <summary>
        ///// This method is used to get Demo Schedule
        ///// </summary>
        ///// <param name="SortingColumn"></param>
        ///// <param name="SortingDirection"></param>
        ///// <param name="AllowPaging"></param>
        ///// <param name="PageNum"></param>
        ///// <param name="PageSize"></param>
        ///// <param name="OrderStatus"></param>
        ///// <returns></returns>
        public List<Proc_GetDemoScheduleResult> GetDemoSchedule(string SortingColumn, string SortingDirection,
                                                                bool AllowPaging, int PageNum, int PageSize, byte DemoStatus)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {

                return ObjEmergeDALDataContext.GetDemoSchedule(SortingColumn, SortingDirection,
                                                         AllowPaging, PageNum, PageSize, DemoStatus).ToList();
            }
        }





        public int UpdateTraineeStatus(int TraineeId, bool TraineeStatus)
        {
            int iresult = 0;
            tblCompanyTrainee ObjTrainee = new tblCompanyTrainee();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                ObjTrainee = DX.tblCompanyTrainees.Where(d => d.pkCompanyTraineeId == TraineeId).FirstOrDefault();
                if (ObjTrainee != null)
                {
                    ObjTrainee.TraineeStatus = TraineeStatus;
                    DX.SubmitChanges();
                    iresult = 1;
                }
            }
            return iresult;
        }

        public int UpdateDemoStatus(int DemoScheduleId, string Comments, byte DemoStatus)
        {
            int iresult = 0;
            tblDemoSchedule ObjDemo = new tblDemoSchedule();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                ObjDemo = DX.tblDemoSchedules.Where(d => d.pkDemoScheduleId == DemoScheduleId).FirstOrDefault();
                if (ObjDemo != null)
                {
                    ObjDemo.Comments = Comments;
                    ObjDemo.DemoStatus = DemoStatus;
                    DX.SubmitChanges();
                    iresult = 1;
                }
            }
            return iresult;
        }

        #region Office SystemTemplates

        /// <summary>
        /// This method is used to Get Email Templates
        /// </summary>
        /// <param name="TemplateId"></param>
        /// <param name="ApplicationId"></param>
        /// <returns></returns>
        public List<Proc_USA_LoadEmailTemplatesResult> GetEmailTemplates(int TemplateId, Guid ApplicationId, Boolean IsSystemTemplate, int TemplateCode)
        {
            try
            {
                using (EmergeDALDataContext ObjDataContext = new EmergeDALDataContext())
                {
                    return ObjDataContext.LoadEmailTemplates(TemplateId, ApplicationId, IsSystemTemplate, TemplateCode).ToList<Proc_USA_LoadEmailTemplatesResult>();
                }
            }
            catch //(Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// This method is used to Insert Email Template
        /// </summary>
        /// <param name="ObjEmailTemplate"></param>
        /// <returns></returns>
        public int InsertEmailTemplate(tblEmailTemplate ObjEmailTemplate)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                int val = Dx.InsertEmailTemplates(ObjEmailTemplate.fkApplicationId, ObjEmailTemplate.TemplateName,
                                                  ObjEmailTemplate.TemplateSubject,
                                                  ObjEmailTemplate.TemplateContent, ObjEmailTemplate.DefaultTemplateContent,
                                                  ObjEmailTemplate.IsSystemTemplate,
                                                  ObjEmailTemplate.CreatedDate,
                                                  ObjEmailTemplate.TemplateCode);
                return val;

            }
        }

        /// <summary>
        /// This method is used to update Email Template
        /// </summary>
        /// <param name="ObjEmailTemplate"></param>
        /// <returns></returns>
        public int UpdateEmailTemplate(tblEmailTemplate ObjEmailTemplate)
        {
            string TemplateName = ObjEmailTemplate.TemplateName;
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                int val = Dx.UpdateEmailTemplates(ObjEmailTemplate.pkTemplateId, ObjEmailTemplate.fkApplicationId,
                                                        TemplateName, ObjEmailTemplate.TemplateSubject,
                                                        ObjEmailTemplate.TemplateContent,
                                                        ObjEmailTemplate.LastModifiedDate, ObjEmailTemplate.TemplateType);
                return val;
            }
        }

        /// <summary>
        /// This method is used to delete Template
        /// </summary>
        /// <param name="ObjEmailTemplate"></param>
        /// <returns></returns>
        public int DeleteEmailTemplate(int pkTemplateId)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                int val = Dx.DeleteEmailTemplate(pkTemplateId);
                return val;
            }
        }
        public int DeleteMultiple_Template(List<tblEmailTemplate> lstTemplates)
        {
            DbTransaction DbTrans = null;
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    ObjEmergeDALDataContext.Connection.Open();
                    DbTrans = ObjEmergeDALDataContext.Connection.BeginTransaction();
                    ObjEmergeDALDataContext.Transaction = DbTrans;
                    for (int i = 0; i < lstTemplates.Count; i++)
                    {
                        ObjEmergeDALDataContext.DeleteEmailTemplate(lstTemplates.ElementAt(i).pkTemplateId);
                    }
                    DbTrans.Commit(); // Commit Transaction
                    return (int)1;
                }
                catch (Exception ex)
                {
                    if (DbTrans != null)
                    {
                        DbTrans.Rollback(); // Rollback Transaction
                    }
                    throw ex;
                }
                finally
                {
                    ObjEmergeDALDataContext.Transaction = null;
                    if (ObjEmergeDALDataContext.Connection.State == ConnectionState.Open)
                    {
                        ObjEmergeDALDataContext.Connection.Close(); // Close Connection
                    }
                }
            }
        }

        public List<proc_Get_UserEmail_ByRoleIdResult> LoadUsers(int UserType, int pkcompanyId)
        {
            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
            {
                return Dx.Get_UserEmail_ByRoleId(UserType, pkcompanyId).ToList();
            }
        }

        #endregion

    }
    public class sManagers
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public Guid? UserId { get; set; }
    }

    public struct clsDocuments
    {
        public Guid pkDocumentId { get; set; }
        public string DocumentTitle { get; set; }
        public string DocumentPath { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
    }
}
