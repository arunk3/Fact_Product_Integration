﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Emerge.Services.Helper
{
    public static class ApplicationPath
    {
        public static string GetApplicationPath()
        {
            string Port = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
            if (Port == null || Port == "80" || Port == "443")
                Port = "";
            else
                Port = ":" + Port;
            string Protocol = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT_SECURE"];
            if (Protocol == null || Protocol == "0")
                Protocol = "http://";
            else
                Protocol = "https://";
            string p = Protocol + System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + Port + System.Web.HttpContext.Current.Request.ApplicationPath;
            p = p.Trim();
            if (!p.EndsWith("/"))
                p += "/";
            return p;
        }

    }
}
