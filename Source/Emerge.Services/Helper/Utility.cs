﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//using MomentumInfotech.EncryptDecrypt;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.IO;
//using BusinessLogicLayer;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web.Security;
using Emerge.Services;
using System.Security.Cryptography;
namespace Emerge.Services.Helper
{
    public static class Utilitys
    {
        /// <summary>
        /// This variable used in 4 reports PS3, EMV2, TDR2 and PL2 for Authorized Purposes.
        /// 1. PS3 report having 27 values.
        /// 2. TDR2 report having 8 values.  
        /// 3. EMV2 report having 9 values.
        /// <value>6 Fraud Prevention or Detection</value>
        /// </summary>
        public static int AuthorizedPurposes = 6;
        public static void Logout(string UserName)
        {
            MembershipUser ObjMembershipUser = Membership.GetUser(UserName);
            System.Web.Security.FormsAuthentication.SignOut();

            if (ObjMembershipUser != null)
            {
                BLLMembership ObjBLLMembership = new BLLMembership();
                ObjBLLMembership.SignOut(ObjMembershipUser.UserName, DateTime.Now.ToUniversalTime().AddMinutes(-(Membership.UserIsOnlineTimeWindow)));
            }

            HttpContext.Current.Response.Redirect(FormsAuthentication.LoginUrl);

        }

        public static string BillingId
        {
            get
            {
                return ConfigurationManager.AppSettings["BillingId"] != null ? ConfigurationManager.AppSettings["BillingId"] : string.Empty;
            }
        }

        public static string BillingPhone
        {
            get
            {
                return ConfigurationManager.AppSettings["BillingPhone"] != null ? ConfigurationManager.AppSettings["BillingPhone"] : string.Empty;
            }
        }

        /// <summary>
        /// This variable used for PS3 Search Type.   
        /// <value>25 – LTA</value>
        /// </summary>
        public static string SearchType = "25 – LTA";

        // private static string _sCookiesU = "vewgjkgkmdfsdhhgdferyutsfejhwfjhekjfjkefkj";
        private static string _sCookiesU = "UserCookie";

        private static string _sCookieUserName = "fjhefvk";

        private static string _sCookieUserPassword = "kejgfj9";

        private static string _sCookieUserRole = "kepqrj9";

        public static int PageSize = 5;

        public static void SetCookie(string UserName, string Password, string UserRole, bool IsCreate)
        {
            string cookietemp = _sCookiesU;

            HttpCookie cookie = HttpContext.Current.Request.Cookies[cookietemp];

            #region //Create Cookie
            if (IsCreate)
            {
                if (cookie == null)
                {
                    cookie = new HttpCookie(cookietemp);
                }
                try
                {
                    cookie[_sCookieUserName] = EncryptDecrypts.Encrypt(UserName);
                    cookie[_sCookieUserPassword] = EncryptDecrypts.Encrypt(Password);
                    cookie[_sCookieUserRole] = EncryptDecrypts.Encrypt(UserRole);
                    //cookie[_sCookieUserName] =UserName;
                    //cookie[_sCookieUserPassword] = MD5Hash(Password);
                    //cookie[_sCookieUserRole] = UserRole;
                    cookie.Expires = DateTime.Now.AddDays(10);
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
                catch
                {
                    throw;
                }

                finally
                {
                    cookie = null;
                }
            }
            #endregion

            #region //Remove Cookie
            if (cookie != null)
            {
                HttpContext.Current.Response.Cookies[cookietemp].Expires = DateTime.Now.AddYears(-1);
            }
            #endregion
        }

        public static void GetCookie(out string UserName, out string Password, out string UserRole, out bool IsFound)
        {
            UserName = Password = UserRole = "";
            IsFound = false;
            string cookieTemp = _sCookiesU;

            HttpCookie cookie = HttpContext.Current.Request.Cookies[cookieTemp];
            if (cookie != null)
            {
                if (cookie.Values.Get(_sCookieUserName) != null && cookie.Values.Get(_sCookieUserPassword) != null && cookie.Values.Get(_sCookieUserRole) != null)
                {
                    UserName = EncryptDecrypts.Decrypt(cookie[_sCookieUserName]);
                    Password = EncryptDecrypts.Decrypt(cookie[_sCookieUserPassword]);
                    UserRole = EncryptDecrypts.Decrypt(cookie[_sCookieUserRole]);
                    //UserName = cookie[_sCookieUserName];
                    //Password = cookie[_sCookieUserPassword];
                    //UserRole = cookie[_sCookieUserRole];
                    IsFound = true;
                }
            }
        }

        public static string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            //compute hash from the bytes of text
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

            //get hash result after compute it
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits
                //for each byte
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }

        public static void SetCache(this Page page)
        {
            TimeSpan freshness = new TimeSpan(0, 0, 5, 0);
            DateTime now = DateTime.Now;
            page.Response.Cache.SetExpires(now.Add(freshness));
            page.Response.Cache.SetMaxAge(freshness);
            page.Response.Cache.SetCacheability(HttpCacheability.Private);
            page.Response.Cache.SetValidUntilExpires(true);
        }

        /// <summary>
        /// Clear all textboxes
        /// </summary>
        /// <param name="container"></param>
        public static void ClearTextboxes(this Control container)
        {
            foreach (Control ctl in container.Controls)
            {
                var textBox = ctl as TextBox;
                if (textBox != null)
                {
                    textBox.Text = String.Empty;
                }
                var chkBox = ctl as CheckBox;
                if (chkBox != null)
                {
                    chkBox.Checked = false;
                }
                if (ctl.Controls.Count > 0)
                {
                    ClearTextboxes(ctl);
                }
            }
        }

        /// <summary>
        /// Convert date format.
        /// </summary>
        /// <param name="DateString"></param>
        /// <returns></returns>
        public static string ConvertDateString(object DateString)
        {
            return Convert.ToDateTime(DateString).ToShortDateString();
        }

        /// <summary>
        /// Check valid GUID format
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static bool IsGuid(this string guid)
        {
            Regex isGuid = new Regex(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", RegexOptions.Compiled);
            bool isValid = false;
            if (!guid.Equals(new Guid("00000000-0000-0000-0000-000000000000")))
            {
                if (isGuid.IsMatch(guid))
                {
                    isValid = true;
                }
            }
            return isValid;
        }

        public static string GetPhysicalPath(this Page page, string fileName)
        {
            string Filepath = page.Server.MapPath(fileName);
            if (File.Exists(Filepath))
            {
                return Filepath;
            }
            return "";
        }

        /// <summary>
        /// This method is used to generate page numbers
        /// </summary>
        /// <param name="TotalRecords"></param>
        /// <param name="ObjDdl"></param>
        /// <param name="CurrentValue"></param>
        public static void GeneratePageNumbers(int TotalRecords, DropDownList ObjDdl, int PageSize, string CurrentValue)
        {
            if (TotalRecords == 0 || PageSize == 0) { return; }
            int iTotalPages = (TotalRecords % PageSize > 0) ? ((TotalRecords / PageSize) + 1) : (TotalRecords / PageSize);

            ObjDdl.Items.Clear();
            for (int iRow = 1; iRow <= iTotalPages; iRow++)
            {
                ObjDdl.Items.Add(new ListItem(iRow.ToString(), iRow.ToString()));
            }

            ListItem LI = ObjDdl.Items.FindByValue(CurrentValue);
            if (LI != null)
            {
                ObjDdl.SelectedValue = CurrentValue;
            }
        }

        public static string GetProductXMLFilePath()
        {
            return @"~\Content\ProductXml\ProductRequireData.xml";
        }
        public static string GetProductBatchXMLFilePath()
        {
            return @"~\Content\ProductXml\ProductRequireDataForBatch.xml";
        }
        /// <summary>
        /// Function to Generate Company Code
        /// </summary>
        /// <param name="CompnayName"></param>
        /// <returns></returns>
        public static string GenerateCompanyCode(string CompanyName, DateTime DateString)
        {

            return CompanyName.Substring(0, 2).ToUpper() + DateString.ToString("yyMMddHHmmss");

        }

        /// <summary>
        /// Function to Generate Location Code
        /// </summary>
        /// <param name="CompnayName"></param>
        /// <returns></returns>
        public static string GenerateLocationCode(string CompanyName, DateTime DateString)
        {
            return CompanyName.Substring(0, 2).ToUpper() + DateString.ToString("yyMMddHHmmssmmss");
        }

        /// <summary>
        /// Function To Get UID of Particular User
        /// </summary>
        /// <param name="CompanyName"></param>
        /// <param name="DateString"></param>
        /// <returns></returns>
        public static string GenerateUserCode(string CompanyName, DateTime DateString)
        {
            return CompanyName.Substring(0, 2).ToUpper() + DateString.ToString("yyHHmmssMMdd");
        }

        /// <summary>
        /// Function to Bind DropDownList When no Record is there in the Database
        /// </summary>
        /// <param name="ObjDropDownList"></param>
        /// <param name="message"></param>
        public static void BindDropDownWithEmptyValue(DropDownList ObjDropDownList, string message)
        {
            ObjDropDownList.Items.Insert(0, new ListItem("No " + message + " Found", "-1"));
        }

        public static Guid SiteApplicationId = new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852");

        public static string SiteApplicationName = "emerge";

        //public static void Logout(string UserName)
        //{
        //    MembershipUser ObjMembershipUser = Membership.GetUser(UserName);
        //    System.Web.Security.FormsAuthentication.SignOut();

        //    if (ObjMembershipUser != null)
        //    {
        //        BLLMembership ObjBLLMembership = new BLLMembership();
        //        ObjBLLMembership.SignOut(ObjMembershipUser.UserName, DateTime.Now.ToUniversalTime().AddMinutes(-(Membership.UserIsOnlineTimeWindow)));
        //    }

        //    HttpContext.Current.Response.Redirect(FormsAuthentication.LoginUrl);

        //}

        /// <summary>
        /// Function To Send Request To Vendor Thru HttpRequest Object
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public static string SendRequest(string uri, string content)
        {

            if (uri == "")
            {
                return "";
            }


            string Response = "";
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(uri);
            request.Timeout = 65000;
            request.ProtocolVersion = HttpVersion.Version11;
            request.Method = WebRequestMethods.Http.Post;
            request.ContentLength = content.Length;
            request.ContentType = "application/x-www-form-urlencoded";
            StreamWriter writer = new StreamWriter(request.GetRequestStream());
            writer.Write(content);
            writer.Close();
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            Response = reader.ReadToEnd();
            response.Close();

            return Response;
        }

        public static void DeleteFile(string FilePath)
        {
            try
            {
                if (File.Exists(FilePath))
                {
                    File.Delete(FilePath);
                }
            }
            catch (Exception)
            {


            }
        }

        public static string ResolveWebUrl(string relativeUrl)
        {
            if (relativeUrl == null) throw new ArgumentNullException("relativeUrl");

            if (relativeUrl.Length == 0 || relativeUrl[0] == '/' || relativeUrl[0] == '\\')
                return relativeUrl;

            int idxOfScheme = relativeUrl.IndexOf(@"://", StringComparison.Ordinal);
            if (idxOfScheme != -1)
            {
                int idxOfQM = relativeUrl.IndexOf('?');
                if (idxOfQM == -1 || idxOfQM > idxOfScheme) return relativeUrl;
            }

            StringBuilder sbUrl = new StringBuilder();
            sbUrl.Append(HttpRuntime.AppDomainAppVirtualPath);
            if (sbUrl.Length == 0 || sbUrl[sbUrl.Length - 1] != '/') sbUrl.Append('/');

            // found question mark already? query string, do not touch!
            bool foundQM = false;
            bool foundSlash; // the latest char was a slash?
            if (relativeUrl.Length > 1
                && relativeUrl[0] == '~'
                && (relativeUrl[1] == '/' || relativeUrl[1] == '\\'))
            {
                relativeUrl = relativeUrl.Substring(2);
                foundSlash = true;
            }
            else foundSlash = false;
            foreach (char c in relativeUrl)
            {
                if (!foundQM)
                {
                    if (c == '?') foundQM = true;
                    else
                    {
                        if (c == '/' || c == '\\')
                        {
                            if (foundSlash) continue;
                            else
                            {
                                sbUrl.Append('/');
                                foundSlash = true;
                                continue;
                            }
                        }
                        else if (foundSlash) foundSlash = false;
                    }
                }
                sbUrl.Append(c);
            }

            return sbUrl.ToString();
        }

        //public static bool SendMail(string mailTo, string CC, string mailFrom, string subject, string MessageBody)
        //{
        //    bool mailSent = false;
        //    try
        //    {
        //        MomentumInfotech.EmailProvider.MailProvider ObjMailProvider = new MomentumInfotech.EmailProvider.MailProvider();

        //        ObjMailProvider.IsBodyHtml = true;


        //        string FromAddress = "";
        //        SmtpClient SmtpClient = new SmtpClient();
        //        System.Net.Mail.MailMessage message = new MailMessage();
        //        message.IsBodyHtml = true;
        //        message.Priority = MailPriority.High;
        //        message.Subject = subject;

        //        message.Body = MessageBody;
        //        SmtpClient.Host =ConfigurationManager.AppSettings["SmtpHostAddress"].ToString();
        //        SmtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPortNo"]);

        //        if (CC != "")
        //        {
        //            message.CC.Add(CC);
        //        }


        //        if (mailFrom == "")
        //        {
        //            FromAddress = ConfigurationManager.AppSettings["FromAddress"].ToString();
        //        }
        //        else
        //        {
        //            FromAddress = mailFrom;
        //        }
        //        message.From = new MailAddress(FromAddress.ToString());
        //        message.To.Add(new MailAddress(mailTo));

        //        SmtpClient.Send(message);            
        //        message.To.Clear();
        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return mailSent;
        //    }

        //}

        public static void DownloadFile(string FilePath)
        {
            if (System.IO.File.Exists(FilePath))
            {
                try
                {
                   // string fileext = System.IO.Path.GetExtension(FilePath);
                    string FileName = System.IO.Path.GetFileName(FilePath);
                    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + FileName);
                    HttpContext.Current.Response.TransmitFile(FilePath);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    HttpContext.Current.Response.End();
                    HttpContext.Current.Response.Close();
                }
            }
        }

        /// <summary>
        /// Function to Fetch the Last Request 
        /// </summary>
        /// <param name="Requests"></param>
        /// <returns></returns>
        public static string GetLastRequest(string Requests)
        {
            string Vendor_Request = "";
            string[] String_Separator = new string[] { "$#$#" };

            if (Requests.Contains("$#$#"))
            {
                string[] Request_array = Requests.Split(String_Separator, StringSplitOptions.RemoveEmptyEntries);
                Vendor_Request = Request_array.Last();
            }
            else
            {
                Vendor_Request = Requests;
            }
            return Vendor_Request;
        }

        /// <summary>
        /// Function to Send an email
        /// </summary>
        /// <param name="TO"></param>
        /// <param name="FROM"></param>
        /// <param name="MESSAGE"></param>
        /// <param name="SUBJECT"></param>
        /// <returns></returns>
        public static bool SendMail(string TO, string FROM, string MESSAGE, string SUBJECT)
        {
            int SendingStatus = 0;

            MailProviders ObjMailProvider = new MailProviders();

            List<string> BCC_List = new List<string>();
            BCC_List.Add(ConfigurationManager.AppSettings["BccAdmin"].ToString());

            ObjMailProvider.Message = MESSAGE;
            ObjMailProvider.MailTo.Add(TO);
            ObjMailProvider.MailFrom = FROM;
            ObjMailProvider.MailBCC = BCC_List;
            ObjMailProvider.Subject = SUBJECT;
            ObjMailProvider.IsBodyHtml = true;
            ObjMailProvider.Priority = MailPriority.High;
            ObjMailProvider.SendMail(out SendingStatus);

            return (SendingStatus == 1 ? true : false);
        }

        public static bool SendMail(List<string> toList, string FROM, string MESSAGE, string SUBJECT)
        {
            int SendingStatus = 0;

            MailProviders ObjMailProvider = new MailProviders();

            List<string> BCC_List = new List<string>();
            BCC_List.Add(ConfigurationManager.AppSettings["BccAdmin"].ToString());

            ObjMailProvider.Message = MESSAGE;
            ObjMailProvider.MailTo = toList;
            ObjMailProvider.MailFrom = FROM;
            ObjMailProvider.MailBCC = BCC_List;
            ObjMailProvider.Subject = SUBJECT;
            ObjMailProvider.IsBodyHtml = true;
            ObjMailProvider.Priority = MailPriority.High;
            ObjMailProvider.SendMail(out SendingStatus);

            return (SendingStatus == 1 ? true : false);
        }

        /// <summary>
        /// Function to Send an email With CC
        /// //Use in ticket #24
        /// </summary>
        /// <param name="TO"></param>
        /// <param name="CC"></param>
        /// <param name="BCC"></param>
        /// <param name="FROM"></param>
        /// <param name="MESSAGE"></param>
        /// <param name="SUBJECT"></param>
        /// <returns></returns>
        public static bool SendMail(string TO, string CC, string BCC, string FROM, string MESSAGE, string SUBJECT)
        {
            int SendingStatus = 0;

            MailProviders ObjMailProvider = new MailProviders();

            ObjMailProvider.Message = MESSAGE;
            ObjMailProvider.MailTo.Add(TO);

            if (CC != "")
            {
                string[] CCInfo = CC.Split(',');
                ObjMailProvider.MailCC = CCInfo.ToList<string>();
            }

            if (BCC != "")
            {
                string[] BCCInfo = BCC.Split(',');
                ObjMailProvider.MailBCC = BCCInfo.ToList<string>();
            }


            /*
            if (!BCC.Contains(ConfigurationManager.AppSettings["BccAdmin"].ToString()))
            {
                ObjMailProvider.MailBCC.Add(ConfigurationManager.AppSettings["BccAdmin"].ToString());
            }*/

            ObjMailProvider.MailFrom = FROM;
            ObjMailProvider.Subject = SUBJECT;
            ObjMailProvider.IsBodyHtml = true;
            ObjMailProvider.Priority = MailPriority.High;
            ObjMailProvider.SendMail(out SendingStatus);

            return (SendingStatus == 1 ? true : false);
        }

        /// <summary>
        /// Function to Send an email With ATTACHMENT
        /// </summary>
        /// <param name="TO"></param>
        /// <param name="CC"></param>
        /// <param name="BCC"></param>
        /// <param name="FROM"></param>
        /// <param name="MESSAGE"></param>
        /// <param name="SUBJECT"></param>
        /// <param name="ATTACHMENT"></param>
        /// <returns></returns>
        public static bool SendMail(string TO, string CC, string BCC, string FROM, string MESSAGE, string SUBJECT, Attachment ATTACHMENT)
        {
            int SendingStatus = 0;

            MailProviders ObjMailProvider = new MailProviders();

            ObjMailProvider.Message = MESSAGE;
            ObjMailProvider.MailTo.Add(TO);

            if (CC != "")
            {
                string[] CCInfo = CC.Split(',');
                ObjMailProvider.MailCC = CCInfo.ToList<string>();
            }

            if (BCC != "")
            {
                string[] BCCInfo = BCC.Split(',');
                ObjMailProvider.MailBCC = BCCInfo.ToList<string>();
            }

            if (!BCC.Contains(ConfigurationManager.AppSettings["BccAdmin"].ToString()))
            {
                ObjMailProvider.MailBCC.Add(ConfigurationManager.AppSettings["BccAdmin"].ToString());
            }

            ObjMailProvider.MailAttachments.Add(ATTACHMENT);


            ObjMailProvider.MailFrom = FROM;
            ObjMailProvider.Subject = SUBJECT;
            ObjMailProvider.IsBodyHtml = true;
            ObjMailProvider.Priority = MailPriority.High;
            ObjMailProvider.SendMail(out SendingStatus);

            return (SendingStatus == 1 ? true : false);
        }

        /// <summary>
        /// Function to Send an email With multiple ATTACHMENTs
        /// </summary>
        /// <param name="TO"></param>
        /// <param name="CC"></param>
        /// <param name="BCC"></param>
        /// <param name="FROM"></param>
        /// <param name="MESSAGE"></param>
        /// <param name="SUBJECT"></param>
        /// <param name="ATTACHMENT"></param>
        /// <returns></returns>
        public static bool SendMail(string TO, string CC, string BCC, string FROM, string MESSAGE, string SUBJECT, List<Attachment> ATTACHMENT)
        {
            int SendingStatus = 0;

            MailProviders ObjMailProvider = new MailProviders();

            ObjMailProvider.Message = MESSAGE;
            ObjMailProvider.MailTo.Add(TO);

            if (CC != "")
            {
                string[] CCInfo = CC.Split(',');
                ObjMailProvider.MailCC = CCInfo.ToList<string>();
            }

            if (BCC != "")
            {
                string[] BCCInfo = BCC.Split(',');
                ObjMailProvider.MailBCC = BCCInfo.ToList<string>();
            }

            if (!BCC.Contains(ConfigurationManager.AppSettings["BccAdmin"].ToString()))
            {
                ObjMailProvider.MailBCC.Add(ConfigurationManager.AppSettings["BccAdmin"].ToString());
            }
            for (int i = 0; i < ATTACHMENT.Count; i++)
            {
                ObjMailProvider.MailAttachments.Add(ATTACHMENT.ElementAt(i));
            }


            ObjMailProvider.MailFrom = FROM;
            ObjMailProvider.Subject = SUBJECT;
            ObjMailProvider.IsBodyHtml = true;
            ObjMailProvider.Priority = MailPriority.High;
            ObjMailProvider.SendMail(out SendingStatus);

            return (SendingStatus == 1 ? true : false);
        }


        /// <summary>
        /// Function To Get The Content of the  Sample  Report .
        /// </summary>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public static string GetSampleReportContent(string FileName)
        {
            if (File.Exists(HttpContext.Current.Server.MapPath("~/Resources/SampleReports/" + FileName + ".html")))
            {
                return File.ReadAllText(HttpContext.Current.Server.MapPath("~/Resources/SampleReports/" + FileName + ".html"), Encoding.Default);
            }
            else
            {
                return "";
            }
        }

        public static string GetTextWithLength(string str, int Length)
        {
            string sContent = string.Empty;
            sContent = str;
            if (sContent.Length > Length)
            {
                sContent = sContent.Substring(0, Length);
            }
            return sContent;
        }

        public static Guid GetUID(string Username)
        {
            Guid UserId = Guid.Empty;
            try
            {
                MembershipUser Obj = Membership.GetUser(Username);
                if (Obj != null)
                {
                    UserId = new Guid(Obj.ProviderUserKey.ToString());
                }
            }
            catch { }
            return UserId;
        }

        public static string GeneraterRandom()
        {
            Random ObjRandom = new Random();
            string strChars = "Q,E,W,R,T,Y,U,I,O,P,A,S,D,F,G,H,J,K,L,Z,X,C,V,B,N,M";
            string strNums = "8,6,3,1,9,2,5,4,7,0";
            string[] arrNumSplit = strNums.Split(',');
            string[] arrCharSplit = strChars.Split(',');
            string strRandom = "";


            for (int i = 0; i < 1; i++)
            {
                int iRandom = ObjRandom.Next(0, arrCharSplit.Length - 1);
                strRandom += arrCharSplit[iRandom].ToString();
            }
            for (int i = 0; i < 5; i++)
            {
                int iRandom = ObjRandom.Next(0, arrNumSplit.Length - 1);
                strRandom += arrNumSplit[iRandom].ToString();
            }

            return strRandom;
        }
        public static string RandomStr()
        {
            string rStr = Path.GetRandomFileName();
            rStr = rStr.Replace(".", "");
            return rStr;
        }
        public static object setval4null(object myObject)
        {
            #region Null Properties convert to String.empty
            object anv = new object();
            foreach (var propertyInfo in myObject.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(myObject, null) == null)
                    {
                        propertyInfo.SetValue(myObject, string.Empty, null);
                    }
                }
            }
            anv = myObject;
            return anv;
            #endregion
        }
        public static void setval4nullAnother(object myObject)
        {
            #region Null Properties convert to String.empty

            foreach (var propertyInfo in myObject.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(myObject, null) == null)
                    {
                        propertyInfo.SetValue(myObject, string.Empty, null);
                    }
                }
            }

            #endregion
        }
        public static object Cell2Property(object myObject, List<string> ProertyColl, string PropertyValue)
        {
            #region Null Properties convert to String.empty
            object anv = new object();
            foreach (var propertyInfo in myObject.GetType().GetProperties())
            {
                if (ProertyColl.Any(d => d.Contains(propertyInfo.Name)))
                {
                    object prop = propertyInfo.GetValue(myObject, null);
                    propertyInfo.SetValue(prop, PropertyValue, null);
                    //if (propertyInfo.GetValue(myObject, null) == null)
                    //{
                    //    propertyInfo.SetValue(myObject, DateTime.Now.ToFileTime().ToString(), null);
                    //}
                }
            }
            anv = myObject;
            return anv;
            #endregion
        }


    }
    public class MailProviders
    {
        SmtpClient objSmtpClient = null;

        #region PrivateVeriable   //Declare all Local Veriables related to email
        //========== Message Related ==========
        private string _MailFrom;
        private List<string> _MailTo = new List<string>();
        private List<string> _MailCC = new List<string>();
        private List<string> _MailBCC = new List<string>();

        private List<Attachment> _MailAttachments = new List<Attachment>();

        private string _Subject;
        private string _Message;
        private bool _IsBodyHtml = true;
        private bool _EnableSSL = false;

        private MailPriority _MailPriority = MailPriority.Normal;

        private System.Text.Encoding _MessageEncoding = System.Text.Encoding.UTF8;
        //=====================================
        #endregion


        #region Properties   //Declare all Properties related to email

        /// <summary>
        ///  Add Sender Address 
        /// </summary>
        public string MailFrom
        {
            get
            {
                return _MailFrom;
            }
            set
            {
                _MailFrom = value;
            }
        }

        /// <summary>
        /// Add Receipient Address (TO) 
        /// </summary>
        public List<string> MailTo
        {
            get
            {
                return _MailTo;
            }
            set
            {
                _MailTo = value;
            }
        }

        /// <summary>
        ///  Add Receipient Address (CC)
        /// </summary>
        public List<string> MailCC
        {
            get
            {
                return _MailCC;
            }
            set
            {
                _MailCC = value;
            }
        }

        /// <summary>
        ///  Add Receipient Address (BCC) 
        /// </summary>
        public List<string> MailBCC
        {
            get
            {
                return _MailBCC;
            }
            set
            {
                _MailBCC = value;
            }
        }

        /// <summary>
        ///  Subject 
        /// </summary>
        public string Subject
        {
            get
            {
                return _Subject;
            }
            set
            {
                _Subject = value;
            }
        }

        /// <summary>
        ///  Message Body 
        /// </summary>
        public string Message
        {
            get
            {
                return _Message;
            }
            set
            {
                _Message = value;
            }
        }

        /// <summary>
        ///  Message Attachment (Optional) 
        ///  File Path of the attachment(file)
        ///  Server.MapPath
        /// </summary>
        public List<Attachment> MailAttachments
        {
            get
            {
                return _MailAttachments;
            }
            set
            {
                _MailAttachments = value;
            }
        }

        /// <summary>
        /// SSL required to send mail
        /// </summary>
        public bool EnableSSL
        {
            get
            {
                return _EnableSSL;
            }
            set
            {
                _EnableSSL = value;
            }


        }

        /// <summary>
        ///  Message Body Html or Text 
        /// </summary>
        public bool IsBodyHtml
        {
            get
            {
                return _IsBodyHtml;
            }
            set
            {
                _IsBodyHtml = value;
            }


        }

        /// <summary>
        /// Specifies the priority of a System.Net.Mail.MailMessage.
        /// </summary>
        public MailPriority Priority
        {
            get
            {
                return _MailPriority;
            }
            set
            {
                _MailPriority = value;
            }
        }


        /// <summary>
        /// Specifies the Message Encoding of a System.Net.Mail.MailMessage.
        /// Default encoding UTF-8
        /// System.Text.Encoding
        /// </summary>
        public System.Text.Encoding MessageEncoding
        {
            get
            {
                return _MessageEncoding;
            }
            set
            {
                MessageEncoding = value;
            }
        }

        /// <summary>
        /// Send Email
        /// </summary>
        #endregion


        #region Constructor   //Declare Constructor related to email

        public MailProviders()
        {
            try
            {
                objSmtpClient = new SmtpClient();
                _EnableSSL = bool.Parse(ConfigurationManager.AppSettings["emailssl"].ToLower());

            }
            catch
            {

                _EnableSSL = false;
            }
        }
        public MailProviders(string SMTPHost)
        {
            objSmtpClient = new SmtpClient();
            objSmtpClient.Host = SMTPHost;
            objSmtpClient.Port = 25;
            try
            {
                _EnableSSL = bool.Parse(ConfigurationManager.AppSettings["emailssl"].ToLower());

            }
            catch
            {

                _EnableSSL = false;
            }
        }
        public MailProviders(string SMTPHost, int SMTPPort)
        {
            objSmtpClient = new SmtpClient();
            objSmtpClient.Host = SMTPHost;
            objSmtpClient.Port = SMTPPort;
            try
            {
                _EnableSSL = bool.Parse(ConfigurationManager.AppSettings["emailssl"].ToLower());

            }
            catch
            {

                _EnableSSL = false;
            }
        }
        public MailProviders(string SMTPHost, int SMTPPort, string UserName, string Password)
        {
            objSmtpClient = new SmtpClient();
            objSmtpClient.Host = SMTPHost;
            objSmtpClient.Port = SMTPPort;
            objSmtpClient.Credentials = new NetworkCredential(UserName, Password);

            try
            {
                _EnableSSL = bool.Parse(ConfigurationManager.AppSettings["emailssl"].ToLower());
            }
            catch
            {
                _EnableSSL = false;
            }

        }


        #endregion


        #region Methods //Declare methods related to mail
        public void SendMail()
        {
            MailMessage objMailMessage = new MailMessage();

            try
            {
                #region //Add Sender Address
                if (_MailFrom != null)
                    objMailMessage.From = new MailAddress(_MailFrom);
                #endregion

                #region //Add Receipient Address (TO)
                foreach (string to in _MailTo)
                {
                    objMailMessage.To.Add(to);
                }
                #endregion

                #region //Add Receipient Address (CC)
                foreach (string cc in _MailCC)
                {
                    objMailMessage.CC.Add(cc);
                }
                #endregion

                #region //Add Receipient Address (BCC)
                foreach (string bcc in _MailBCC)
                {
                    objMailMessage.Bcc.Add(bcc);
                }

                #endregion

                #region //Add Subject
                objMailMessage.Subject = _Subject;
                #endregion

                #region //Message Body Html or Text
                objMailMessage.IsBodyHtml = _IsBodyHtml;
                #endregion

                #region //Message Priority
                objMailMessage.Priority = _MailPriority;
                #endregion

                #region // Message Delivery Notification
                objMailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                #endregion

                #region // Message Body
                if (_IsBodyHtml == true)
                {
                    #region // This way prevent message to be SPAM
                    objMailMessage.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                    AlternateView plainView = AlternateView.CreateAlternateViewFromString
                        (System.Text.RegularExpressions.Regex.Replace(_Message, @"<(.|\n)*?>", string.Empty),
                        null, "text/plain");
                    AlternateView htmlView = AlternateView.CreateAlternateViewFromString(_Message, null, "text/html");

                    objMailMessage.AlternateViews.Add(plainView);
                    objMailMessage.AlternateViews.Add(htmlView);
                    #endregion
                }
                else
                    objMailMessage.Body = _Message;
                #endregion

                #region //Message Encoding
                objMailMessage.BodyEncoding = _MessageEncoding;
                #endregion

                #region //Attachment
                foreach (Attachment ObjAttachment in _MailAttachments)
                {
                    objMailMessage.Attachments.Add(ObjAttachment);
                }
                #endregion

                #region //SSL checking
                objSmtpClient.EnableSsl = _EnableSSL;
                #endregion

                #region // Message Sent
                objSmtpClient.Send(objMailMessage);
                #endregion


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objMailMessage.Dispose();
            }

        }

        /// <summary>
        /// send email pass the SendingStatus parameter with this methods
        /// O for failed, 1 for sucess
        /// Used in ticket #24
        /// </summary>
        /// <param name="SendingStatus"></param>
        public void SendMail(out int SendingStatus)
        {
            MailMessage objMailMessage = new MailMessage();

            try
            {
                #region //Add Sender Address
                if (_MailFrom != null)
                    objMailMessage.From = new MailAddress(_MailFrom);
                #endregion

                #region //Add Receipient Address (TO)
                foreach (string to in _MailTo)
                {
                    objMailMessage.To.Add(to);
                }
                #endregion

                #region //Add Receipient Address (CC)
                foreach (string cc in _MailCC)
                {
                    objMailMessage.CC.Add(cc);
                }
                #endregion

                #region //Add Receipient Address (BCC)
                foreach (string bcc in _MailBCC)
                {
                    objMailMessage.Bcc.Add(bcc);
                }

                #endregion

                #region //Add Subject
                objMailMessage.Subject = _Subject;
                #endregion

                #region //Message Body Html or Text
                objMailMessage.IsBodyHtml = _IsBodyHtml;
                #endregion

                #region //Message Priority
                objMailMessage.Priority = _MailPriority;
                #endregion

                #region // Message Delivery Notification
                objMailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                #endregion

                #region //======== Message Body ==========
                if (_IsBodyHtml == true)
                {
                    #region // This way prevent message to be SPAM
                    objMailMessage.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                    AlternateView plainView = AlternateView.CreateAlternateViewFromString
                        (System.Text.RegularExpressions.Regex.Replace(_Message, @"<(.|\n)*?>", string.Empty),
                        null, "text/plain");
                    AlternateView htmlView = AlternateView.CreateAlternateViewFromString(_Message, null, "text/html");

                    objMailMessage.AlternateViews.Add(plainView);
                    objMailMessage.AlternateViews.Add(htmlView);
                    #endregion
                }
                else
                    objMailMessage.Body = _Message;
                #endregion

                #region //Message Encoding
                objMailMessage.BodyEncoding = _MessageEncoding;
                #endregion

                #region //Attachment
                foreach (Attachment ObjAttachment in _MailAttachments)
                {
                    objMailMessage.Attachments.Add(ObjAttachment);
                }
                #endregion

                #region //SSL checking
                objSmtpClient.EnableSsl = _EnableSSL;
                #endregion

                #region // Message Sent
                //objSmtpClient.Host = "mail.dotnetoutsourcing.com";
                //objSmtpClient.Credentials = new NetworkCredential("support@dotnetoutsourcing.com", "test123");
                objSmtpClient.Send(objMailMessage);
                #endregion
                SendingStatus = 1;
            }
            catch //(Exception ex)
            {
                SendingStatus = 0;
            }
            finally
            {
                objMailMessage.Dispose();

            }

        }

        /// <summary>
        /// send email pass the SendingStatus parameter with this methods
        /// ex message for failed, "Success" for success
        /// </summary>
        /// <param name="SendingStatus"></param>
        public void SendMail(out string SendingStatus)
        {
            MailMessage objMailMessage = new MailMessage();

            try
            {
                #region //Add Sender Address
                if (_MailFrom != null)
                    objMailMessage.From = new MailAddress(_MailFrom);
                #endregion

                #region //Add Recipient Address (TO)
                foreach (string to in _MailTo)
                {
                    objMailMessage.To.Add(to);
                }
                #endregion

                #region //Add Recipient Address (CC)
                foreach (string cc in _MailCC)
                {
                    objMailMessage.CC.Add(cc);
                }
                #endregion

                #region //Add Recipient Address (BCC)
                foreach (string bcc in _MailBCC)
                {
                    objMailMessage.Bcc.Add(bcc);
                }

                #endregion

                #region //Add Subject
                objMailMessage.Subject = _Subject;
                #endregion

                #region //Message Body Html or Text
                objMailMessage.IsBodyHtml = _IsBodyHtml;
                #endregion

                #region //Message Priority
                objMailMessage.Priority = _MailPriority;
                #endregion

                #region // Message Delivery Notification
                objMailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                #endregion

                #region //======== Message Body ==========
                if (_IsBodyHtml == true)
                {
                    #region // This way prevent message to be SPAM
                    objMailMessage.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                    AlternateView plainView = AlternateView.CreateAlternateViewFromString
                        (System.Text.RegularExpressions.Regex.Replace(_Message, @"<(.|\n)*?>", string.Empty),
                        null, "text/plain");
                    AlternateView htmlView = AlternateView.CreateAlternateViewFromString(_Message, null, "text/html");

                    objMailMessage.AlternateViews.Add(plainView);
                    objMailMessage.AlternateViews.Add(htmlView);
                    #endregion
                }
                else
                    objMailMessage.Body = _Message;
                #endregion

                #region //Message Encoding
                objMailMessage.BodyEncoding = _MessageEncoding;
                #endregion

                #region //Attachment
                foreach (Attachment ObjAttachment in _MailAttachments)
                {
                    objMailMessage.Attachments.Add(ObjAttachment);
                }
                #endregion

                #region //SSL checking
                objSmtpClient.EnableSsl = _EnableSSL;
                #endregion

                #region // Message Sent
                //objSmtpClient.Host = "mail.dotnetoutsourcing.com";
                //objSmtpClient.Credentials = new NetworkCredential("support@dotnetoutsourcing.com", "test123");
                objSmtpClient.Send(objMailMessage);
                #endregion
                SendingStatus = "Success";
            }
            catch (Exception ex)
            {
                SendingStatus = ex.Message.ToString();
            }
            finally
            {
                objMailMessage.Dispose();

            }

        }


        /// <summary>
        /// Message Sent Asynchronously
        /// </summary>
        public bool SendMailAsc()
        {
            using (MailMessage objMailMessage = new MailMessage())
            {

                try
                {
                    #region //Add Sender Address
                    if (_MailFrom != null)
                        objMailMessage.From = new MailAddress(_MailFrom);
                    #endregion

                    #region //Add Receipient Address (TO)
                    foreach (string to in _MailTo)
                    {
                        objMailMessage.To.Add(to);
                    }
                    #endregion

                    #region //Add Receipient Address (CC)
                    foreach (string cc in _MailCC)
                    {
                        objMailMessage.CC.Add(cc);
                    }
                    #endregion

                    #region //Add Receipient Address (BCC)
                    foreach (string bcc in _MailBCC)
                    {
                        objMailMessage.Bcc.Add(bcc);
                    }

                    #endregion

                    #region //Add Subject
                    objMailMessage.Subject = _Subject;
                    #endregion

                    #region //Message Body Html or Text
                    objMailMessage.IsBodyHtml = _IsBodyHtml;
                    #endregion

                    #region //Message Priority High
                    objMailMessage.Priority = _MailPriority;
                    #endregion

                    #region//Message Delivery Notification
                    objMailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                    #endregion

                    #region //Message Body
                    if (_IsBodyHtml == true)
                    {
                        #region // This way prevent message to be SPAM
                        objMailMessage.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                        AlternateView plainView = AlternateView.CreateAlternateViewFromString
                            (System.Text.RegularExpressions.Regex.Replace(_Message, @"<(.|\n)*?>", string.Empty),
                            null, "text/plain");
                        AlternateView htmlView = AlternateView.CreateAlternateViewFromString(_Message, null, "text/html");

                        objMailMessage.AlternateViews.Add(plainView);
                        objMailMessage.AlternateViews.Add(htmlView);
                        #endregion
                    }
                    else
                        objMailMessage.Body = _Message;
                    #endregion

                    #region //Message Encoding
                    objMailMessage.BodyEncoding = _MessageEncoding;
                    #endregion

                    #region //Attachment
                    foreach (Attachment ObjAttachment in _MailAttachments)
                    {
                        objMailMessage.Attachments.Add(ObjAttachment);
                    }
                    #endregion

                    #region //SSl Settings
                    objSmtpClient.EnableSsl = _EnableSSL;
                    #endregion

                    #region //wire up the event for when the Async send is completed
                    objSmtpClient.SendCompleted += new SendCompletedEventHandler(smtp_SendCompleted);
                    #endregion

                    #region //======== Message Sent Asynchronously ==========
                    object userState = objMailMessage;
                    objSmtpClient.SendAsync(objMailMessage, userState);
                    #endregion

                    return true;

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        void smtp_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {

            //MailMessage mail = (MailMessage)e.UserState;

            //if (e.Error != null)
            //{
            //    Exception ex = new Exception(e.Error.ToString());
            //    try
            //    {
            //        throw ex;
            //    }
            //    finally
            //    {
            //        ex = null;
            //    }

            //}
        }
        #endregion


    }
    public class EncryptDecrypts
    {
        // Encrypt a byte array into a byte array using a key and an IV 
        public static byte[] Encrypt(byte[] clearData, byte[] Key, byte[] IV)
        {
            // Create a MemoryStream to accept the encrypted bytes 
            using (MemoryStream ms = new MemoryStream())
            {

                // Create a symmetric algorithm. 
                // We are going to use Rijndael because it is strong and
                // available on all platforms. 
                // You can use other algorithms, to do so substitute the
                // next line with something like 
                //      TripleDES alg = TripleDES.Create(); 
                Rijndael alg = Rijndael.Create();

                // Now set the key and the IV. 
                // We need the IV (Initialization Vector) because
                // the algorithm is operating in its default 
                // mode called CBC (Cipher Block Chaining).
                // The IV is XORed with the first block (8 byte) 
                // of the data before it is encrypted, and then each
                // encrypted block is XORed with the 
                // following block of plaintext.
                // This is done to make encryption more secure. 

                // There is also a mode called ECB which does not need an IV,
                // but it is much less secure. 
                alg.Key = Key;
                alg.IV = IV;

                // Create a CryptoStream through which we are going to be
                // pumping our data. 
                // CryptoStreamMode.Write means that we are going to be
                // writing data to the stream and the output will be written
                // in the MemoryStream we have provided. 
                CryptoStream cs = new CryptoStream(ms,
                    alg.CreateEncryptor(), CryptoStreamMode.Write);

                // Write the data and make it do the encryption 
                cs.Write(clearData, 0, clearData.Length);

                // Close the crypto stream (or do FlushFinalBlock). 
                // This will tell it that we have done our encryption and
                // there is no more data coming in, 
                // and it is now a good time to apply the padding and
                // finalize the encryption process. 
                cs.Close();

                // Now get the encrypted data from the MemoryStream.
                // Some people make a mistake of using GetBuffer() here,
                // which is not the right way. 
                byte[] encryptedData = ms.ToArray();

                return encryptedData;
            }
        }

        // Encrypt a string into a string using a password 
        //    Uses Encrypt(byte[], byte[], byte[]) 

        public static string Encrypt(string clearText, string Password)
        {
            byte[] clearBytes =
                System.Text.Encoding.Unicode.GetBytes(clearText);

            // Then, we need to turn the password into Key and IV 
            // We are using salt to make it harder to guess our key
            // using a dictionary attack - 
            // trying to guess a password by enumerating all possible words. 
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 
								   0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76});

            // Now get the key/IV and do the encryption using the
            // function that accepts byte arrays. 
            // Using PasswordDeriveBytes object we are first getting
            // 32 bytes for the Key 
            // (the default Rijndael key length is 256bit = 32bytes)
            // and then 16 bytes for the IV. 
            // IV should always be the block size, which is by default
            // 16 bytes (128 bit) for Rijndael. 
            // If you are using DES/TripleDES/RC2 the block size is
            // 8 bytes and so should be the IV size. 
            // You can also read KeySize/BlockSize properties off
            // the algorithm to find out the sizes. 
            byte[] encryptedData = Encrypt(clearBytes,
                pdb.GetBytes(32), pdb.GetBytes(16));

            // Now we need to turn the resulting byte array into a string. 
            // A common mistake would be to use an Encoding class for that.
            //It does not work because not all byte values can be
            // represented by characters. 
            // We are going to be using Base64 encoding that is designed
            //exactly for what we are trying to do. 
            return Convert.ToBase64String(encryptedData);

        }

        public static string Encrypt(string clearText)
        {
            string Password = EncryptDecrypts.EncryptDecryptKey;
            byte[] clearBytes =
                System.Text.Encoding.Unicode.GetBytes(clearText);

            // Then, we need to turn the password into Key and IV 
            // We are using salt to make it harder to guess our key
            // using a dictionary attack - 
            // trying to guess a password by enumerating all possible words. 
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 
								   0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76});

            // Now get the key/IV and do the encryption using the
            // function that accepts byte arrays. 
            // Using PasswordDeriveBytes object we are first getting
            // 32 bytes for the Key 
            // (the default Rijndael key length is 256bit = 32bytes)
            // and then 16 bytes for the IV. 
            // IV should always be the block size, which is by default
            // 16 bytes (128 bit) for Rijndael. 
            // If you are using DES/TripleDES/RC2 the block size is
            // 8 bytes and so should be the IV size. 
            // You can also read KeySize/BlockSize properties off
            // the algorithm to find out the sizes. 
            byte[] encryptedData = Encrypt(clearBytes,
                pdb.GetBytes(32), pdb.GetBytes(16));

            // Now we need to turn the resulting byte array into a string. 
            // A common mistake would be to use an Encoding class for that.
            //It does not work because not all byte values can be
            // represented by characters. 
            // We are going to be using Base64 encoding that is designed
            //exactly for what we are trying to do. 
            return Convert.ToBase64String(encryptedData);

        }


        // Encrypt bytes into bytes using a password 
        //    Uses Encrypt(byte[], byte[], byte[]) 

        public static byte[] Encrypt(byte[] clearData, string Password)
        {
            // We need to turn the password into Key and IV. 
            // We are using salt to make it harder to guess our key
            // using a dictionary attack - 
            // trying to guess a password by enumerating all possible words. 
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 
								   0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76});

            // Now get the key/IV and do the encryption using the function
            // that accepts byte arrays. 
            // Using PasswordDeriveBytes object we are first getting
            // 32 bytes for the Key 
            // (the default Rijndael key length is 256bit = 32bytes)
            // and then 16 bytes for the IV. 
            // IV should always be the block size, which is by default
            // 16 bytes (128 bit) for Rijndael. 
            // If you are using DES/TripleDES/RC2 the block size is 8
            // bytes and so should be the IV size. 
            // You can also read KeySize/BlockSize properties off the
            // algorithm to find out the sizes. 
            return Encrypt(clearData, pdb.GetBytes(32), pdb.GetBytes(16));

        }

        // Encrypt a file into another file using a password 
        public static void Encrypt(string fileIn,
            string fileOut, string Password)
        {

            // First we are going to open the file streams 
            using (FileStream fsIn = new FileStream(fileIn,
                FileMode.Open, FileAccess.Read))
            {
                FileStream fsOut = new FileStream(fileOut,
                    FileMode.OpenOrCreate, FileAccess.Write);

                // Then we are going to derive a Key and an IV from the
                // Password and create an algorithm 
                PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                    new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 
								   0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76});

                Rijndael alg = Rijndael.Create();
                alg.Key = pdb.GetBytes(32);
                alg.IV = pdb.GetBytes(16);

                // Now create a crypto stream through which we are going
                // to be pumping data. 
                // Our fileOut is going to be receiving the encrypted bytes. 
                CryptoStream cs = new CryptoStream(fsOut,
                    alg.CreateEncryptor(), CryptoStreamMode.Write);

                // Now will will initialize a buffer and will be processing
                // the input file in chunks. 
                // This is done to avoid reading the whole file (which can
                // be huge) into memory. 
                int bufferLen = 4096;
                byte[] buffer = new byte[bufferLen];
                int bytesRead;

                do
                {
                    // read a chunk of data from the input file 
                    bytesRead = fsIn.Read(buffer, 0, bufferLen);

                    // encrypt it 
                    cs.Write(buffer, 0, bytesRead);
                } while (bytesRead != 0);

                // close everything 

                // this will also close the unrelying fsOut stream
                cs.Close();
                fsIn.Close();
            }
        }

        // Decrypt a byte array into a byte array using a key and an IV 
        public static byte[] Decrypt(byte[] cipherData, byte[] Key, byte[] IV)
        {
            // Create a MemoryStream that is going to accept the
            // decrypted bytes 
            using (MemoryStream ms = new MemoryStream())
            {

                // Create a symmetric algorithm. 
                // We are going to use Rijndael because it is strong and
                // available on all platforms. 
                // You can use other algorithms, to do so substitute the next
                // line with something like 
                //     TripleDES alg = TripleDES.Create(); 
                Rijndael alg = Rijndael.Create();

                // Now set the key and the IV. 
                // We need the IV (Initialization Vector) because the algorithm
                // is operating in its default 
                // mode called CBC (Cipher Block Chaining). The IV is XORed with
                // the first block (8 byte) 
                // of the data after it is decrypted, and then each decrypted
                // block is XORed with the previous 
                // cipher block. This is done to make encryption more secure. 
                // There is also a mode called ECB which does not need an IV,
                // but it is much less secure. 
                alg.Key = Key;
                alg.IV = IV;

                // Create a CryptoStream through which we are going to be
                // pumping our data. 
                // CryptoStreamMode.Write means that we are going to be
                // writing data to the stream 
                // and the output will be written in the MemoryStream
                // we have provided. 
                CryptoStream cs = new CryptoStream(ms,
                    alg.CreateDecryptor(), CryptoStreamMode.Write);

                // Write the data and make it do the decryption 
                cs.Write(cipherData, 0, cipherData.Length);

                // Close the crypto stream (or do FlushFinalBlock). 
                // This will tell it that we have done our decryption
                // and there is no more data coming in, 
                // and it is now a good time to remove the padding
                // and finalize the decryption process. 
                cs.Close();

                // Now get the decrypted data from the MemoryStream. 
                // Some people make a mistake of using GetBuffer() here,
                // which is not the right way. 
                byte[] decryptedData = ms.ToArray();
                return decryptedData;
            }
        }

        // Decrypt a string into a string using a password 
        //    Uses Decrypt(byte[], byte[], byte[]) 

        public static string Decrypt(string cipherText, string Password)
        {
            // First we need to turn the input string into a byte array. 
            // We presume that Base64 encoding was used 
            byte[] cipherBytes = Convert.FromBase64String(cipherText);

            // Then, we need to turn the password into Key and IV 
            // We are using salt to make it harder to guess our key
            // using a dictionary attack - 
            // trying to guess a password by enumerating all possible words. 
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 
								   0x64, 0x76, 0x65, 0x64, 0x65, 0x76});

            // Now get the key/IV and do the decryption using
            // the function that accepts byte arrays. 
            // Using PasswordDeriveBytes object we are first
            // getting 32 bytes for the Key 
            // (the default Rijndael key length is 256bit = 32bytes)
            // and then 16 bytes for the IV. 
            // IV should always be the block size, which is by
            // default 16 bytes (128 bit) for Rijndael. 
            // If you are using DES/TripleDES/RC2 the block size is
            // 8 bytes and so should be the IV size. 
            // You can also read KeySize/BlockSize properties off
            // the algorithm to find out the sizes. 
            byte[] decryptedData = Decrypt(cipherBytes,
                pdb.GetBytes(32), pdb.GetBytes(16));

            // Now we need to turn the resulting byte array into a string. 
            // A common mistake would be to use an Encoding class for that.
            // It does not work 
            // because not all byte values can be represented by characters. 
            // We are going to be using Base64 encoding that is 
            // designed exactly for what we are trying to do. 
            return System.Text.Encoding.Unicode.GetString(decryptedData);
        }

        public static string Decrypt(string cipherText)
        {
            string Password = EncryptDecrypts.EncryptDecryptKey;
            // First we need to turn the input string into a byte array. 
            // We presume that Base64 encoding was used 
            byte[] cipherBytes = Convert.FromBase64String(cipherText);

            // Then, we need to turn the password into Key and IV 
            // We are using salt to make it harder to guess our key
            // using a dictionary attack - 
            // trying to guess a password by enumerating all possible words. 
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 
								   0x64, 0x76, 0x65, 0x64, 0x65, 0x76});

            // Now get the key/IV and do the decryption using
            // the function that accepts byte arrays. 
            // Using PasswordDeriveBytes object we are first
            // getting 32 bytes for the Key 
            // (the default Rijndael key length is 256bit = 32bytes)
            // and then 16 bytes for the IV. 
            // IV should always be the block size, which is by
            // default 16 bytes (128 bit) for Rijndael. 
            // If you are using DES/TripleDES/RC2 the block size is
            // 8 bytes and so should be the IV size. 
            // You can also read KeySize/BlockSize properties off
            // the algorithm to find out the sizes. 
            byte[] decryptedData = Decrypt(cipherBytes,
                pdb.GetBytes(32), pdb.GetBytes(16));

            // Now we need to turn the resulting byte array into a string. 
            // A common mistake would be to use an Encoding class for that.
            // It does not work 
            // because not all byte values can be represented by characters. 
            // We are going to be using Base64 encoding that is 
            // designed exactly for what we are trying to do. 
            return System.Text.Encoding.Unicode.GetString(decryptedData);
        }

        // Decrypt bytes into bytes using a password 
        //    Uses Decrypt(byte[], byte[], byte[]) 

        public static byte[] Decrypt(byte[] cipherData, string Password)
        {
            // We need to turn the password into Key and IV. 
            // We are using salt to make it harder to guess our key
            // using a dictionary attack - 
            // trying to guess a password by enumerating all possible words. 
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 
								   0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76});

            // Now get the key/IV and do the Decryption using the 
            //function that accepts byte arrays. 
            // Using PasswordDeriveBytes object we are first getting
            // 32 bytes for the Key 
            // (the default Rijndael key length is 256bit = 32bytes)
            // and then 16 bytes for the IV. 
            // IV should always be the block size, which is by default
            // 16 bytes (128 bit) for Rijndael. 
            // If you are using DES/TripleDES/RC2 the block size is
            // 8 bytes and so should be the IV size. 

            // You can also read KeySize/BlockSize properties off the
            // algorithm to find out the sizes. 
            return Decrypt(cipherData, pdb.GetBytes(32), pdb.GetBytes(16));
        }

        // Decrypt a file into another file using a password 
        public static void Decrypt(string fileIn,
            string fileOut, string Password)
        {

            // First we are going to open the file streams 
            using (FileStream fsIn = new FileStream(fileIn,
                 FileMode.Open, FileAccess.Read))
            {
                FileStream fsOut = new FileStream(fileOut,
                    FileMode.OpenOrCreate, FileAccess.Write);

                // Then we are going to derive a Key and an IV from
                // the Password and create an algorithm 
                PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                    new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 
								   0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76});
                Rijndael alg = Rijndael.Create();

                alg.Key = pdb.GetBytes(32);
                alg.IV = pdb.GetBytes(16);

                // Now create a crypto stream through which we are going
                // to be pumping data. 
                // Our fileOut is going to be receiving the Decrypted bytes. 
                CryptoStream cs = new CryptoStream(fsOut,
                    alg.CreateDecryptor(), CryptoStreamMode.Write);

                // Now will will initialize a buffer and will be 
                // processing the input file in chunks. 
                // This is done to avoid reading the whole file (which can be
                // huge) into memory. 
                int bufferLen = 4096;
                byte[] buffer = new byte[bufferLen];
                int bytesRead;

                do
                {
                    // read a chunk of data from the input file 
                    bytesRead = fsIn.Read(buffer, 0, bufferLen);

                    // Decrypt it 
                    cs.Write(buffer, 0, bytesRead);

                } while (bytesRead != 0);

                // close everything 
                cs.Close(); // this will also close the unrelying fsOut stream 
                fsIn.Close();
            }
        }

        private static string _EncryptDecryptKey = ConfigurationManager.AppSettings["EncryptDecryptKey"] != null ? ConfigurationManager.AppSettings["EncryptDecryptKey"] : "Password~123";

        public static string EncryptDecryptKey
        {
            set
            {
                _EncryptDecryptKey = value;
            }
            get
            {
                return _EncryptDecryptKey;
            }
        }

    }
}
