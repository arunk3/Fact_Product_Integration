﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;
using Emerge.Services.Helper;
using System.IO;
using System.Web;
namespace Emerge.Services
{
    public class BALTemplateBookmarks
    {
        /// <summary>
        /// This method will replace the bookmarks with the applicable parameters.
        /// </summary>
        /// <param name="EmailContent"></param>
        /// <param name="EmailParameters"></param>
        /// <returns></returns>
        public string ReplaceBookmarks(Bookmarks objBoomarks, bool isuniversal)
        {
            #region Declare Variables

            string MessageBody = "",
              ApplicantName = "", BillingContact = "", BillingEmail = "", BillingPhone = "", CompanyName = "", CompanyAddress = "",
              CompanyAddress2 = "", CompanyCity = "", CompanyState = "", CompanyZipcode = "", EmergeLogo = "", EmergeIcon = "",
              EmergeBacksideLogo = "", EmergeOfficeLogo = "", EmergeEditLogo = "", EmergeEditIcon = "", EmergeAssistIcon = "",
              EmergeAssistComment = "", EmergeAssistSubject = "", LocationPhone = "", LocationCity = "", LiveRunnerLogo = "",
              LiveRunnerIcon = "", LocationState = "", LoginLink = "", MainContact = "", MainEmail = "", MainPhone = "", OrderNumber = "",
              OrderQuantity = "", ProductName = "", ReportName = "", ReportCode = "", ReportDescription = "", PackageName = "", ProductType = "",
              ReportPrice = "", Subject = "", Suggestion = "", TotalPrice = "", UnitPrice = "", UsaintelLogo = "", UsaintelIcon = "", UserPicture = "",
              UserFirstName = "", UserLastName = "", UserEmail = "", UserPassword = "", UserLocation = "", UserCompany = "",
            OrderType = "", DemoDescription="",Date="", CompanyStatus = "", Rating="", Comments="";

            #endregion

            #region Assing Values to Parameters

            ApplicantName = (objBoomarks.ApplicantName != null) ? objBoomarks.ApplicantName : "";
            BillingContact = (objBoomarks.BillingContact != null) ? objBoomarks.BillingContact : "";
            BillingEmail = (objBoomarks.BillingEmail != null) ? objBoomarks.BillingEmail : "";
            BillingPhone = (objBoomarks.BillingPhone != null) ? objBoomarks.BillingPhone : "";
            CompanyName = (objBoomarks.CompanyName != null) ? objBoomarks.CompanyName : "";
            CompanyAddress = (objBoomarks.CompanyAddress != null) ? objBoomarks.CompanyAddress : "";
            CompanyAddress2 = (objBoomarks.CompanyAddress2 != null) ? objBoomarks.CompanyAddress2 : "";
            CompanyCity = (objBoomarks.CompanyCity != null) ? objBoomarks.CompanyCity : "";
            CompanyState = (objBoomarks.CompanyState != null) ? objBoomarks.CompanyState : "";
            CompanyZipcode = (objBoomarks.CompanyZipcode != null) ? objBoomarks.CompanyZipcode : "";
            EmergeAssistComment = (objBoomarks.EmergeAssistComment != null) ? objBoomarks.EmergeAssistComment : "";
            EmergeAssistIcon = (objBoomarks.EmergeAssistIcon != null) ? objBoomarks.EmergeAssistIcon : "";
            EmergeAssistSubject = (objBoomarks.EmergeAssistSubject != null) ? objBoomarks.EmergeAssistSubject : "";
            EmergeBacksideLogo = (objBoomarks.EmergeBacksideLogo != null) ? objBoomarks.EmergeBacksideLogo : "";
            EmergeEditIcon = (objBoomarks.EmergeEditIcon != null) ? objBoomarks.EmergeEditIcon : "";
            EmergeEditLogo = (objBoomarks.EmergeEditLogo != null) ? objBoomarks.EmergeEditLogo : "";
            EmergeIcon = (objBoomarks.EmergeIcon != null) ? objBoomarks.EmergeIcon : "";
            EmergeLogo = (objBoomarks.EmergeLogo != null) ? objBoomarks.EmergeLogo : "";
            EmergeOfficeLogo = (objBoomarks.EmergeOfficeLogo != null) ? objBoomarks.EmergeOfficeLogo : "";
            LiveRunnerIcon = (objBoomarks.LiveRunnerIcon != null) ? objBoomarks.LiveRunnerIcon : "";
            LiveRunnerLogo = (objBoomarks.LiveRunnerLogo != null) ? objBoomarks.LiveRunnerLogo : "";
            LocationCity = (objBoomarks.LocationCity != null) ? objBoomarks.LocationCity : "";
            LocationPhone = (objBoomarks.LocationPhone != null) ? objBoomarks.LocationPhone : "";
            LocationState = (objBoomarks.LocationState != null) ? objBoomarks.LocationState : "";
            LoginLink = (objBoomarks.LoginLink != null) ? objBoomarks.LoginLink : "";
            MainContact = (objBoomarks.MainContact != null) ? objBoomarks.MainContact : "";
            MainEmail = (objBoomarks.MainEmail != null) ? objBoomarks.MainEmail : "";
            MainPhone = (objBoomarks.MainPhone != null) ? objBoomarks.MainPhone : "";
            OrderNumber = (objBoomarks.OrderNumber != null) ? objBoomarks.OrderNumber : "";
            OrderQuantity = (objBoomarks.OrderQuantity != null) ? objBoomarks.OrderQuantity : "";
            PackageName = (objBoomarks.PackageName != null) ? objBoomarks.PackageName : "";
            ProductName = (objBoomarks.ProductName != null) ? objBoomarks.ProductName : "";
            ProductType = (objBoomarks.ProductType != null) ? objBoomarks.ProductType : "";
            ReportCode = (objBoomarks.ReportCode != null) ? objBoomarks.ReportCode : "";
            ReportDescription = (objBoomarks.ReportDescription != null) ? objBoomarks.ReportDescription : "";
            ReportName = (objBoomarks.ReportName != null) ? objBoomarks.ReportName : "";
            ReportPrice = (objBoomarks.ReportPrice != null) ? objBoomarks.ReportPrice : "";
            Subject = (objBoomarks.Subject != null) ? objBoomarks.Subject : "";
            Suggestion = (objBoomarks.Suggestion != null) ? objBoomarks.Suggestion : "";
            TotalPrice = (objBoomarks.TotalPrice != null) ? objBoomarks.TotalPrice : "";
            UnitPrice = (objBoomarks.UnitPrice != null) ? objBoomarks.UnitPrice : "";
            UsaintelIcon = (objBoomarks.UsaintelIcon != null) ? objBoomarks.UsaintelIcon : "";
            UsaintelLogo = (objBoomarks.UsaintelLogo != null) ? objBoomarks.UsaintelLogo : "";
            UserCompany = (objBoomarks.UserCompany != null) ? objBoomarks.UserCompany : "";
            UserEmail = (objBoomarks.UserEmail != null) ? objBoomarks.UserEmail : "";
            UserFirstName = (objBoomarks.UserFirstName != null) ? objBoomarks.UserFirstName : "";
            UserLastName = (objBoomarks.UserLastName != null) ? objBoomarks.UserLastName : "";
            UserLocation = (objBoomarks.UserLocation != null) ? objBoomarks.UserLocation : "";
            UserPassword = (objBoomarks.UserPassword != null) ? objBoomarks.UserPassword : "";
            UserPicture = (objBoomarks.UserPicture != null) ? objBoomarks.UserPicture : "";
            OrderType = (objBoomarks.OrderType != null) ? objBoomarks.OrderType : "";
            DemoDescription = (objBoomarks.DemoDescription != null) ? objBoomarks.DemoDescription : "";
            Date = (objBoomarks.Date != null) ? objBoomarks.Date : "";
            CompanyStatus = (objBoomarks.CompanyStatus != null) ? objBoomarks.CompanyStatus : "";
            Rating = (objBoomarks.Rating != null) ? objBoomarks.Rating : "";
            Comments = (objBoomarks.Comments != null) ? objBoomarks.Comments : "";

            #endregion

            #region Replace Bookmarks

            MessageBody = objBoomarks.EmailContent.
                       Replace("%ApplicantName%", ApplicantName).
                       Replace("%BillingContact%", BillingContact).
                       Replace("%BillingEmail%", BillingEmail).
                       Replace("%BillingPhone%", BillingPhone).
                       Replace("%CompanyName%", CompanyName).
                       Replace("%CompanyAddress%", CompanyAddress).
                       Replace("%CompanyAddress2%", CompanyAddress2).
                       Replace("%CompanyCity%", CompanyCity).
                       Replace("%CompanyState%", CompanyState).
                       Replace("%CompanyZipcode%", CompanyZipcode).
                       Replace("%EmergeLogo%", EmergeLogo).
                       Replace("%EmergeIcon%", EmergeIcon).
                       Replace("%EmergeBacksideLogo%", EmergeBacksideLogo).
                       Replace("%EmergeOfficeLogo%", EmergeOfficeLogo).
                       Replace("%EmergeEditLogo%", EmergeOfficeLogo).
                       Replace("%EmergeEditIcon%", EmergeEditIcon).
                       Replace("%EmergeAssistIcon%", EmergeAssistIcon).
                       Replace("%EmergeAssistComment%", EmergeAssistComment).
                       Replace("%EmergeAssistSubject%", EmergeAssistSubject).
                       Replace("%LocationPhone%", LocationPhone).
                       Replace("%LocationCity%", LocationCity).
                       Replace("%LiveRunnerLogo%", LiveRunnerLogo).
                       Replace("%LiveRunnerIcon%", LiveRunnerIcon).
                       Replace("%LocationState%", LocationState).
                       Replace("%LoginLink%", LoginLink).
                       Replace("%MainContact%", MainContact).
                       Replace("%MainEmail%", MainEmail).
                       Replace("%MainPhone%", MainPhone).
                       Replace("%OrderNumber%", OrderNumber).
                       Replace("%OrderQuantity%", OrderQuantity).
                       Replace("%ProductName%", ProductName).
                       Replace("%ReportName%", ReportName).
                       Replace("%ReportCode%", ReportCode).
                       Replace("%ReportDescription%", ReportDescription).
                       Replace("%PackageName%", PackageName).
                       Replace("%ProductType%", ProductType).
                       Replace("%ReportPrice%", ReportPrice).
                       Replace("%Subject%", Subject).
                       Replace("%Suggestion%", Suggestion).
                       Replace("%TotalPrice%", TotalPrice).
                       Replace("%UnitPrice%", UnitPrice).
                       Replace("%UsaintelLogo%", UsaintelLogo).
                       Replace("%UsaintelIcon%", UsaintelIcon).
                       Replace("%UserPicture%", UserPicture).
                       Replace("%UserFirstName%", UserFirstName).
                       Replace("%UserLastName%", UserLastName).
                       Replace("%UserEmail%", UserEmail).
                       Replace("%UserPassword%", UserPassword).
                       Replace("%UserLocation%", UserLocation).
                       Replace("%UserCompany%", UserCompany).
                       Replace("%OrderType%", OrderType).
                       Replace("%DemoDescription%", DemoDescription).
                       Replace("%Date%", Date).
                       Replace("%CompanyStatus%", CompanyStatus).
                       Replace("%Rating%", Rating).
                       Replace("%Comments%", Comments);

            #endregion

            return MessageBody;
        }

        /// <summary>
        /// This methos is used to send bookmark values.
        /// </summary>
        /// <param name="objBoomarks"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        /// 
        public proc_Get_UserAllInfoResult GetUserInfo(Guid userid)
        {
            //string userinfo = string.Empty;
             proc_Get_UserAllInfoResult usr = new proc_Get_UserAllInfoResult();
             using (EmergeDALDataContext DX = new EmergeDALDataContext())
             {
                 usr = DX.GetUserAllInfo(userid).FirstOrDefault();
             }

             return usr;
                 
        }
        public string ReplaceBookmarks(Bookmarks objBoomarks, Guid UserId)
        {
            string MessageBody = "";
            Bookmarks myObject = new Bookmarks();
            myObject = objBoomarks;

            #region Null Properties convert to String.empty

            foreach (var propertyInfo in myObject.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(myObject, null) == null)
                    {
                        propertyInfo.SetValue(myObject, string.Empty, null);
                    }
                }
            }

            #endregion

            #region Get Curent user detail and assign values to universal bookamrk
            proc_Get_UserAllInfoResult usr = new proc_Get_UserAllInfoResult();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                usr = DX.GetUserAllInfo(UserId).FirstOrDefault();
                if (usr != null)
                {
                    myObject.UserPicture = ChkEmpty(myObject.UserPicture, usr.UserImage);
                    myObject.UserFirstName = ChkEmpty(myObject.UserFirstName, usr.FirstName);
                    myObject.UserLastName = ChkEmpty(myObject.UserLastName, usr.LastName);
                    myObject.UserEmail = ChkEmpty(myObject.UserEmail, usr.Email);
                    //myObject.UserLocation = ChkEmpty(myObject.UserLocation, usr.company);
                    myObject.UserCompany = ChkEmpty(myObject.UserCompany, usr.CompanyName);
                    myObject.LocationCity = ChkEmpty(myObject.LocationCity, usr.LocationCity);/*Pending change to locationcity*/
                    myObject.LocationState = ChkEmpty(myObject.LocationState, usr.LocationState);
                    myObject.CompanyName = ChkEmpty(myObject.CompanyName, usr.CompanyName);
                    myObject.CompanyAddress = ChkEmpty(myObject.CompanyAddress, usr.CompanyAddress1);
                    myObject.CompanyAddress2 = ChkEmpty(myObject.CompanyAddress2, usr.CompanyAddress2);
                    myObject.CompanyCity = ChkEmpty(myObject.CompanyCity, usr.LocationCity);
                    myObject.CompanyState = ChkEmpty(myObject.CompanyState, usr.LocationState);
                    myObject.CompanyZipcode = ChkEmpty(myObject.CompanyZipcode, usr.ZipCode);
                    myObject.MainContact = ChkEmpty(myObject.MainContact, usr.MainContactPersonName);
                    myObject.MainEmail = ChkEmpty(myObject.MainEmail, usr.MainEmailId);
                    myObject.MainPhone = ChkEmpty(myObject.MainPhone, usr.PhoneNo);
                    myObject.BillingContact = ChkEmpty(myObject.BillingContact, usr.BillingContactPersonName);
                    myObject.BillingEmail = ChkEmpty(myObject.BillingEmail, usr.BillingEmailId);
                    //myObject.BillingPhone = ChkEmpty(myObject.BillingPhone, usr.phone);
                    //myObject.Subject = ChkEmpty(myObject.Subject, usr.);
                    //myObject.ApplicantName = ChkEmpty(myObject.ApplicantName, usr.);
                }
               // string PageRequest = "http://test.intelifi.com/testintelifi/Default";
                myObject.LoginLink = ChkEmpty(myObject.LoginLink, "<a href='" + ApplicationPath.GetApplicationPath() + "'> Click here </a>");
                string WebsiteLogo = "http://test.intelifi.com/testintelifi/Resources/Upload/Images/" + BALGeneral.GetGeneralSettings().WebSiteLogo;
                myObject.EmergeLogo = ChkEmpty(myObject.EmergeLogo, "<img src='" + WebsiteLogo + "' />");
                myObject.EmergeIcon = ChkEmpty(myObject.EmergeIcon, "http://test.intelifi.com/testintelifi/favicon.gif");
                myObject.EmergeBacksideLogo = ChkEmpty(myObject.EmergeBacksideLogo, "");
                myObject.EmergeOfficeLogo = ChkEmpty(myObject.EmergeOfficeLogo, "http://test.intelifi.com/testintelifi/Resources/Images/EmergeOfficeLogo.jpg");

            }

            #endregion

            MessageBody = ManageBookMark(myObject);
            return MessageBody;
        }



        public tblEmailTemplate GetEmergetemplateForRegistration(Bookmarks objBoomarks, Guid UserId)
        {

            

            #region Get Curent user detail and assign values to universal bookamrk
            proc_Get_UserAllInfoResult usr = new proc_Get_UserAllInfoResult();
            tblEmailTemplate ObjtblEmailTemplate = new tblEmailTemplate();

            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                usr = DX.GetUserAllInfo(UserId).FirstOrDefault();
                ObjtblEmailTemplate = DX.tblEmailTemplates.Where(rec => rec.pkTemplateId == 1).FirstOrDefault();
                
               
            }

            #endregion

        
            return ObjtblEmailTemplate;
        }
        //ND-21
        public string ReplaceBookmarks_AttachmentDocuments(Bookmarks objBoomarks, Guid UserId)
        {
            string MessageBody = "";
            Bookmarks myObject = new Bookmarks();
            myObject = objBoomarks;

            #region Null Properties convert to String.empty

            foreach (var propertyInfo in myObject.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(myObject, null) == null)
                    {
                        propertyInfo.SetValue(myObject, string.Empty, null);
                    }
                }
            }

            #endregion

            #region Get Curent user detail and assign values to universal bookamrk
            proc_Get_UserAllInfoResult usr = new proc_Get_UserAllInfoResult();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                usr = DX.GetUserAllInfo(UserId).FirstOrDefault();
                if (usr != null)
                {
                    myObject.UserPicture = ChkEmpty(myObject.UserPicture, usr.UserImage);
                    myObject.UserFirstName = ChkEmpty(usr.FirstName, myObject.UserFirstName);
                    myObject.UserLastName = ChkEmpty(usr.LastName, myObject.UserLastName);
                    myObject.UserEmail = ChkEmpty(usr.Email, myObject.UserEmail);
                    myObject.UserCompany = ChkEmpty(myObject.UserCompany, usr.CompanyName);
                    myObject.LocationCity = ChkEmpty(myObject.LocationCity, usr.LocationCity);/*Pending change to locationcity*/
                    myObject.LocationState = ChkEmpty(myObject.LocationState, usr.LocationState);
                    myObject.CompanyName = ChkEmpty(myObject.CompanyName, usr.CompanyName);
                    myObject.CompanyAddress = ChkEmpty(myObject.CompanyAddress, usr.CompanyAddress1);
                    myObject.CompanyAddress2 = ChkEmpty(myObject.CompanyAddress2, usr.CompanyAddress2);
                    myObject.CompanyCity = ChkEmpty(myObject.CompanyCity, usr.LocationCity);
                    myObject.CompanyState = ChkEmpty(myObject.CompanyState, usr.LocationState);
                    myObject.CompanyZipcode = ChkEmpty(myObject.CompanyZipcode, usr.ZipCode);
                    myObject.MainContact = ChkEmpty(myObject.MainContact, usr.MainContactPersonName);
                    myObject.MainEmail = ChkEmpty(myObject.MainEmail, usr.MainEmailId);
                    myObject.MainPhone = ChkEmpty(myObject.MainPhone, usr.PhoneNo);
                    myObject.BillingContact = ChkEmpty(myObject.BillingContact, usr.BillingContactPersonName);
                    myObject.BillingEmail = ChkEmpty(myObject.BillingEmail, usr.BillingEmailId);
                }
                //string PageRequest = "http://test.intelifi.com/testintelifi/Default";
                myObject.LoginLink = ChkEmpty(myObject.LoginLink, "<a href='" + ApplicationPath.GetApplicationPath() + "'> Click here </a>");
                //string WebsiteLogo = "http://test.intelifi.com/testintelifi/Resources/Upload/Images/" + BALGeneral.GetGeneralSettings().WebSiteLogo;
                //myObject.EmergeLogo = ChkEmpty(myObject.EmergeLogo, "<img src='" + WebsiteLogo + "' />");
                //myObject.EmergeIcon = ChkEmpty(myObject.EmergeIcon, "http://test.intelifi.com/testintelifi/favicon.gif");
                //myObject.EmergeBacksideLogo = ChkEmpty(myObject.EmergeBacksideLogo, "");
                // myObject.EmergeOfficeLogo = ChkEmpty(myObject.EmergeOfficeLogo, "http://test.intelifi.com/testintelifi/Resources/Images/EmergeOfficeLogo.jpg");

            }

            #endregion

            MessageBody = ManageBookMark(myObject);
            return MessageBody;
        }

        public string getBookMarkFile(string filePath)
        {
           // string getfile = string.Empty;
            //string FileName = filePath;
           // StringBuilder sb = new StringBuilder();
            

            if (File.Exists(HttpContext.Current.Server.MapPath("~/Resources/Templates/BookMarkTemplate.htm")))
            {
                return File.ReadAllText(HttpContext.Current.Server.MapPath("~/Resources/Templates/BookMarkTemplate.htm"), Encoding.Default);
            }
            else
            {
                return "";
            }

        }
        public string ManageBookMark(Bookmarks objBM)
        {
            string MsgBody = "";
            if (objBM.EmailContent != null)
            {
                string mystring = objBM.SSN;
                var result = mystring.Substring(mystring.Length - Math.Min(4, mystring.Length));
                objBM.SSN = "XXX-XX-" + result;  
                MsgBody = objBM.EmailContent.
                          Replace("%ApplicantName%", objBM.ApplicantName).
                          Replace("%BillingContact%", objBM.BillingContact).
                          Replace("%BillingEmail%", objBM.BillingEmail).
                          Replace("%BillingPhone%", objBM.BillingPhone).
                          Replace("%CompanyName%", objBM.CompanyName).
                          Replace("%CompanyAddress%", objBM.CompanyAddress).
                          Replace("%CompanyAddress2%", objBM.CompanyAddress2).
                          Replace("%CompanyCity%", objBM.CompanyCity).
                          Replace("%CompanyState%", objBM.CompanyState).
                          Replace("%CompanyZipcode%", objBM.CompanyZipcode).
                          Replace("%EmergeLogo%", objBM.EmergeLogo).
                          Replace("%EmergeIcon%", objBM.EmergeIcon).
                          Replace("%EmergeBacksideLogo%", objBM.EmergeBacksideLogo).
                          Replace("%EmergeOfficeLogo%", objBM.EmergeOfficeLogo).
                          Replace("%EmergeEditLogo%", objBM.EmergeOfficeLogo).
                          Replace("%EmergeEditIcon%", objBM.EmergeEditIcon).
                          Replace("%EmergeAssistIcon%", objBM.EmergeAssistIcon).
                          Replace("%EmergeAssistComment%", objBM.EmergeAssistComment).
                          Replace("%EmergeAssistSubject%", objBM.EmergeAssistSubject).
                          Replace("%EmergeReviewComment%", objBM.EmergeReviewComment).
                          Replace("%EmergeReviewSubject%", objBM.EmergeReviewSubject).
                          Replace("%LocationPhone%", objBM.LocationPhone).
                          Replace("%LocationCity%", objBM.LocationCity).
                          Replace("%LiveRunnerLogo%", objBM.LiveRunnerLogo).
                          Replace("%LiveRunnerIcon%", objBM.LiveRunnerIcon).
                          Replace("%LocationState%", objBM.LocationState).
                          Replace("%LoginLink%", objBM.LoginLink).
                          Replace("%MainContact%", objBM.MainContact).
                          Replace("%MainEmail%", objBM.MainEmail).
                          Replace("%MainPhone%", objBM.MainPhone).
                          Replace("%OrderNumber%", objBM.OrderNumber).
                          Replace("%OrderQuantity%", objBM.OrderQuantity).
                          Replace("%ProductName%", objBM.ProductName).
                          Replace("%ReportName%", objBM.ReportName).
                          Replace("%ReportCode%", objBM.ReportCode).
                          Replace("%ReportDescription%", objBM.ReportDescription).
                          Replace("%PackageName%", objBM.PackageName).
                          Replace("%ProductType%", objBM.ProductType).
                          Replace("%ReportPrice%", objBM.ReportPrice).
                          Replace("%Subject%", objBM.Subject).
                          Replace("%Suggestion%", objBM.Suggestion).
                          Replace("%TotalPrice%", objBM.TotalPrice).
                          Replace("%UnitPrice%", objBM.UnitPrice).
                          Replace("%UsaintelLogo%", objBM.UsaintelLogo).
                          Replace("%UsaintelIcon%", objBM.UsaintelIcon).
                          Replace("%UserPicture%", objBM.UserPicture).
                          Replace("%UserFirstName%", objBM.UserFirstName).
                          Replace("%UserLastName%", objBM.UserLastName).
                          Replace("%UserEmail%", objBM.UserEmail).
                          Replace("%UserPassword%", objBM.UserPassword).
                          Replace("%UserLocation%", objBM.UserLocation).
                          Replace("%UserCompany%", objBM.UserCompany).
                          Replace("%OrderType%", objBM.OrderType).
                          Replace("%Error%", objBM.Error).
                          Replace("%ViewReportLink%", objBM.ViewReportLink).
                          Replace("%WebsiteName%", objBM.WebsiteName).
                          Replace("%OrderDescription%", objBM.OrderDescription).
                          Replace("%Name%", objBM.Name).
                          Replace("%DemoDescription%", objBM.DemoDescription).
                          Replace("%Date%", objBM.Date).
                          Replace("%CompanyStatus%", objBM.CompanyStatus).
                          Replace("%Rating%", objBM.Rating).
                          Replace("%DOB%", objBM.DOB).
                          Replace("%SSN%", objBM.SSN).
                          Replace("%DLNumber%", objBM.DLNumber).
                          Replace("%Comments%", objBM.Comments);

            }

            return MsgBody;
        }

        public string ChkEmpty(string strBokMrkString, string strusrDetail)
        {
            return (strBokMrkString != "") ? strBokMrkString : strusrDetail;
        }
    }
    /// <summary>
    /// Assign Properties to class. Keep in mind all propertities are of string datatype
    /// </summary>
    public class Bookmarks
    {

        public string EmailContent { get; set; }
        public string ApplicantName { get; set; }
        public string BillingContact { get; set; }
        public string BillingEmail { get; set; }
        public string BillingPhone { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyAddress2 { get; set; }
        public string CompanyCity { get; set; }
        public string CompanyState { get; set; }
        public string CompanyZipcode { get; set; }
        public string EmergeLogo { get; set; }
        public string EmergeIcon { get; set; }
        public string EmergeBacksideLogo { get; set; }
        public string EmergeOfficeLogo { get; set; }
        public string EmergeEditLogo { get; set; }
        public string EmergeEditIcon { get; set; }
        public string EmergeAssistIcon { get; set; }
        public string EmergeAssistComment { get; set; }
        public string EmergeAssistSubject { get; set; }
        public string EmergeReviewComment { get; set; }
        public string EmergeReviewSubject { get; set; }
        public string LocationPhone { get; set; }
        public string LocationCity { get; set; }
        public string LiveRunnerLogo { get; set; }
        public string LiveRunnerIcon { get; set; }
        public string LocationState { get; set; }
        public string LoginLink { get; set; }
        public string MainContact { get; set; }
        public string MainEmail { get; set; }
        public string MainPhone { get; set; }
        public string OrderNumber { get; set; }
        public string OrderQuantity { get; set; }
        public string ProductName { get; set; }
        public string ReportName { get; set; }
        public string ReportCode { get; set; }
        public string ReportDescription { get; set; }
        public string PackageName { get; set; }
        public string ProductType { get; set; }
        public string ReportPrice { get; set; }
        public string Subject { get; set; }
        public string Suggestion { get; set; }
        public string TotalPrice { get; set; }
        public string UnitPrice { get; set; }
        public string UsaintelLogo { get; set; }
        public string UsaintelIcon { get; set; }
        public string UserPicture { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string UserEmail { get; set; }
        public string UserPassword { get; set; }
        public string UserLocation { get; set; }
        public string UserCompany { get; set; }
        public string DOB { get; set; }
        public string SSN { get; set; }
        public string DLNumber { get; set; }
        public string ShipToAddress { get; set; }
        
        #region Addional Bookmarks

        public string OrderType { get; set; }
        public string Error { get; set; }
        public string ViewReportLink { get; set; }
        public string WebsiteName { get; set; }
        public string OrderDescription { get; set; }
        public string Name { get; set; }
        public string DemoDescription { get; set; }
        public string Date { get; set; }
        public string CompanyStatus { get; set; }
        public string Rating { get; set; }
        public string Comments { get; set; }

        #endregion

    }
}
