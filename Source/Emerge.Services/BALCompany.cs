﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;
using Emerge;
using System.Data.Common;
using System.Data;
using System.Xml.Linq;
using Emerge.Services.Helper;
using System.Web.Security;
using System.Web.UI;
using System.Web;

namespace Emerge.Services
{
    public class BALCompany
    {
        /// <summary>
        /// Function to Add New Account 
        /// </summary>
        /// <param name="ObjtblCompany"></param>
        public int InsertNewAccount(tblCompany ObjtblCompany, tblCompanyDocument ObjtblCompanyDocument)
        {
            int update = 0;
            try
            {
                using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
                {
                    int IsDuplicateAccountNumber = (from db in ObjDALDataContext.tblCompanies
                                                    where db.CompanyAccountNumber == ObjtblCompany.CompanyAccountNumber
                                                    select db).Count();
                    if (IsDuplicateAccountNumber == 0)
                    {
                        tblCompany objtblCompany = new tblCompany();
                        objtblCompany = ObjDALDataContext.tblCompanies.Where(db => db.pkCompanyId == ObjtblCompany.pkCompanyId).FirstOrDefault();
                        if (objtblCompany != null)
                        {
                            objtblCompany.CompanyAccountNumber = ObjtblCompany.CompanyAccountNumber;
                            objtblCompany.LeadStatus = ObjtblCompany.LeadStatus;
                            ObjtblCompany.LastModifiedDate = DateTime.Now;
                            ObjtblCompany.StatusStartDate = DateTime.Now;

                            ObjDALDataContext.SubmitChanges();


                            tblCompanyDocument tbl = new tblCompanyDocument();
                            tbl.pkCompanyDocumentId = ObjtblCompanyDocument.pkCompanyDocumentId;
                            tbl.fkCompanyId = ObjtblCompanyDocument.fkCompanyId;
                            tbl.DocumentTitle = ObjtblCompanyDocument.DocumentTitle;
                            tbl.DocumentName = ObjtblCompanyDocument.DocumentName;
                            tbl.CreatedDate = ObjtblCompanyDocument.CreatedDate;
                            tbl.IsUserAgreement = ObjtblCompanyDocument.IsUserAgreement;

                            ObjDALDataContext.tblCompanyDocuments.InsertOnSubmit(tbl);
                            ObjDALDataContext.SubmitChanges();

                            update = 1;

                            return update;
                        }
                    }
                    else
                    {
                        return update;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
            return update;
        }

        public int GetCompanyPaymentType(int companyId)
        {
            int retVal = 0;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                //to get payment Term based on selected company.
                var chkPaymentType = DX.tblCompanies.Where(cmp => cmp.pkCompanyId == companyId).Select(cmp => cmp.PaymentTerm).FirstOrDefault();
                if (chkPaymentType != null)
                {
                    retVal = chkPaymentType;
                }
            }
            return retVal;
        }

        public List<proc_GetAllInvoicePerInvoiceResult> GetAllInvoicePerInvoice(int CompanyId, int pkInvoiceId)
        {
            using (EmergeDALDataContext dbobj = new EmergeDALDataContext())
            {
                return dbobj.GetAllInvoicePerInvoice(CompanyId, pkInvoiceId).ToList();
            }
        }
        public List<proc_LoadOpenInvoiceByInvoiceIdDetailsResult1> LoadOpenInVoice_PerDate(int PkCompanyId, int invoiceno)
        {
            using (EmergeDALDataContext dbObj = new EmergeDALDataContext())
            {
                return dbObj.LoadOpenInvoiceByInvoiceIdDetails(PkCompanyId, invoiceno).ToList();
            }
        }
      

        public int SavePackageVisiblityonDB(int packid, bool isVisible)
        {
            int retVal = 0;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                var package = DX.tblProductPackages.Where(p => p.pkPackageId == packid).FirstOrDefault();
                if (package != null)
                {
                    package.IsDefault = isVisible;
                    DX.SubmitChanges();
                    retVal = 1;
                }
            }
            return retVal;
        }
        public string GetSalesRepEmailByCompanyId(int pkCompanyId)
        {
            string SalesRepEmailId = "";
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                tblCompany objtblCompany = new tblCompany();
                objtblCompany = ObjDALDataContext.tblCompanies.Where(db => db.pkCompanyId == pkCompanyId).FirstOrDefault();
                if (objtblCompany != null)
                {
                    if (objtblCompany.fkSalesRepId != null)
                    {
                        //SalesRepEmailId = ObjDALDataContext.aspnet_Users.Where(t =>
                        //        t.UserId == objtblCompany.fkSalesRepId).Select(t => t.UserName).First();
                        bool IsSalesRepDeleted = (from cu in ObjDALDataContext.tblCompanyUsers
                                                  join au in ObjDALDataContext.aspnet_Users
                                                  on cu.fkUserId equals au.UserId
                                                  where (au.UserId == objtblCompany.fkSalesRepId)
                                                  select cu.IsDeleted).FirstOrDefault();
                        if (IsSalesRepDeleted == false)
                        {
                            SalesRepEmailId = ObjDALDataContext.aspnet_Users.Where(t =>
                                    t.UserId == objtblCompany.fkSalesRepId).Select(t => t.UserName).First();

                        }
                    }
                }
            }
            return SalesRepEmailId;
        }

        public List<tblReferenceCode> GetAllReferenceCodes()
        {
            List<tblReferenceCode> ObjList = new List<tblReferenceCode>();
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                ObjList = dx.tblReferenceCodes.ToList<tblReferenceCode>();
            }
            return ObjList;
        }

        public List<Proc_GetReferenceCodebyCompanyIdResult> GetReferenceCodeListbypkCompanyId(int pkCompanyId)
        {
            List<Proc_GetReferenceCodebyCompanyIdResult> objrefenceCodelist = new List<Proc_GetReferenceCodebyCompanyIdResult>();
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {

                objrefenceCodelist = dx.GetReferenceCodebyCompanyId(pkCompanyId).ToList();
            }
            return objrefenceCodelist;
        }
        //1) Create GetAllUser Page For Admin In Emerge 2) Create GetAllLocation Page For Admin In Emerge 3)Enable And Disable Selected Location and User
        public List<tblReferenceCode> GetAllReferenceCodes(int fkCompanyId)
        {
            List<tblReferenceCode> ObjList = new List<tblReferenceCode>();
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                if (fkCompanyId == 0)
                    ObjList = dx.tblReferenceCodes.OrderBy(refCode => refCode.ReferenceCode).ToList<tblReferenceCode>();//INT-50
                else
                    ObjList = dx.tblReferenceCodes.Where(d => d.fkCompanyId == fkCompanyId).OrderBy(refCode => refCode.ReferenceCode).ToList<tblReferenceCode>();//INT-50
            }
            return ObjList;
        }

        public List<Proc_GetClientQualityControlForOfficeResult> GetClientQualityControlForOffice(int PageIndex, int PageSize, bool IsPaging, string SearchString,
          Int16 CompanyType, Int16 StatusType, string SortingColumn,
          string SortingDirection, string FromDate, string ToDate, Guid SalesManagerId, Guid SalesRepId, Guid CurrentUserId, string LeadSource)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.GetClientQualityControlForOffice(PageIndex, PageSize, IsPaging, SearchString, CompanyType, StatusType,
                                                                  SortingColumn, SortingDirection, FromDate, ToDate, SalesManagerId, SalesRepId, CurrentUserId, LeadSource).ToList<Proc_GetClientQualityControlForOfficeResult>();
            }
        }

        public string GetReferenceNote(int pkReferenceCodeId)
        {
            string refNote = string.Empty;
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                refNote = dx.tblReferenceCodes.Where(d => d.pkReferenceCodeId == pkReferenceCodeId).Select(d => d.ReferenceNote).FirstOrDefault();
            }
            return refNote;
        }

        /// <summary>
        /// Function to Get All the Company by application id,Enabled,Deleted
        /// </summary>
        /// <param name="ApplicationId"></param>
        /// <param name="Enabled"></param>
        /// <param name="Deleted"></param>
        /// <returns></returns>
        public List<CompanyList> GetAllCompany(Guid ApplicationId, bool Enabled, bool Deleted)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                IQueryable<CompanyList> ObjData = from c in ObjDALDataContext.tblCompanies
                                                  join l in ObjDALDataContext.tblLocations on c.pkCompanyId equals l.fkCompanyId
                                                  join s in ObjDALDataContext.tblStates on l.fkStateID equals s.pkStateId
                                                  where c.fkApplicationId == ApplicationId && c.IsDeleted == Deleted && l.IsHome == true
                                                  orderby c.CompanyName ascending
                                                  select new CompanyList
                                                  {
                                                      CompanyName = c.CompanyName + "  [" + l.City + ", " + s.StateCode + "]",
                                                      pkCompanyId = c.pkCompanyId
                                                  };
                return ObjData.ToList();
            }
        }
        public string GetSalesManagerEmailByCompanyId(int pkCompanyId)
        {
            string SalesManagerEmailId = "";
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                tblCompany objtblCompany = new tblCompany();
                objtblCompany = ObjDALDataContext.tblCompanies.Where(db => db.pkCompanyId == pkCompanyId).FirstOrDefault();
                if (objtblCompany != null)
                {
                    if (objtblCompany.fkSalesManagerId != null)
                    {
                        SalesManagerEmailId = ObjDALDataContext.aspnet_Users.Where(t =>
                                t.UserId == objtblCompany.fkSalesManagerId).Select(t => t.UserName).First();
                    }
                }
            }
            return SalesManagerEmailId;
        }

        //public int UpdateIsLinkedPrice(Guid fkProductApplicationId, Guid fkCompanyId)
        //{
        //    int success = 0;
        //    using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
        //    {
        //        try
        //        {
        //            success = ObjDALDataContext.UpdateLinkedReportId(fkProductApplicationId, fkCompanyId);
        //        }
        //        catch
        //        {
        //            success = -1;
        //        }
        //        return success;
        //    }
        //}
        /// <summary>
        /// Function to Get Company Locations  By Company ID
        /// </summary>
        /// <returns></returns>
        public List<Proc_Get_CompanyLocationsByCompanyIdResult> GetLocationsByCompanyId(int CompanyId, Guid ApplicationId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.Get_CompanyLocationsByCompanyId(CompanyId, ApplicationId).ToList<Proc_Get_CompanyLocationsByCompanyIdResult>();
            }
        }

        public List<proc_GetVolumeValuesforGraphResult> GetVolumeValuesforGraph(int pkCompanyId, int Month, int Year)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return DX.GetVolumeValuesforGraph(pkCompanyId, Month, Year).ToList();
            }
        }

        public List<proc_GetCompanyNameForCallLogByIdResult> GetCompanyNameForCallLogById(int pKCompanyId)
        {
            using (EmergeDALDataContext objDal = new EmergeDALDataContext())
            {
                return objDal.GetCompanyNameForCallLogById(pKCompanyId).ToList();
            }
        }

        public List<proc_GetUserEmailandCompanyEmailByCompanyUserIdResult> GetUserAndCompanyDetails(int? pkCompanyUserId)
        {

            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return DX.GetUserEmailandCompanyEmailByCompanyUserId(pkCompanyUserId).ToList<proc_GetUserEmailandCompanyEmailByCompanyUserIdResult>(); ;
            }

        }

        /// <summary>
        /// Function To Get the Company Details if membership user Id is Passed to the function
        /// </summary>
        /// <param name="fkUserId"></param>
        /// <returns></returns>
        public List<Proc_Get_CompanyDetailByMemberShipIdResult> GetCompanyDetailByMemberShipIdResult(Guid fkUserId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetCompanyDetailByMemberShipId(fkUserId).ToList();
            }
        }

        public List<Proc_Get_AllCompaniesForAdminResult> GetAllCompanyForAdmin(int PageIndex, int PageSize, bool IsPaging, Int16 CompanyType, string StatusType, string SortingColumn, string SortingDirection,
                                                                   string FilterColumn, byte FilterAction, string FilterValue, out int TotalRec
                                                                   )
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                int? count = 0;
                List<Proc_Get_AllCompaniesForAdminResult> List = ObjDALDataContext.GetAllCompaniesForAdmin(PageIndex, PageSize, IsPaging, CompanyType, StatusType, SortingColumn, SortingDirection,
                                                                  FilterColumn, FilterAction, FilterValue, ref count
                                                                  ).ToList<Proc_Get_AllCompaniesForAdminResult>();
                TotalRec = (int)count;
                return List;
            }
        }

        public List<tblCompanyType> GetCompanyList()
        {
            using (EmergeDALDataContext Obj = new EmergeDALDataContext())
            {
                var Co = from p in Obj.tblCompanyTypes
                         orderby p.pkCompanyTypeId, p.CompanyType
                         select p;
                return Co.ToList();
            }
        }
        //INT-220 User with status for a company lavel
        public List<Proc_Get_AllEmergeUsersWithStatusForAdminResult> GetAllEmergeUsersWithStatusForAdmin(int PageIndex, int PageSize, bool IsPaging, Int16 StatusType, int CompanyId, int FkLocationId, string SortingColumn, string SortingDirection, string FilterColumn, byte FilterAction, string FilterValue, bool IsEnabled, out int TotalRec)
        {
            using (EmergeDALDataContext _EmergeDALDataContext = new EmergeDALDataContext())
            {
                int? count = 0;
                List<Proc_Get_AllEmergeUsersWithStatusForAdminResult> List = _EmergeDALDataContext.GetAllEmergeUsersWithStatusForAdmin(PageIndex, PageSize, IsPaging, StatusType, CompanyId, FkLocationId, SortingColumn, SortingDirection, FilterColumn, FilterAction, FilterValue, IsEnabled, ref count).ToList();
                TotalRec = (int)count;
                return List;
            }
        } 

        public List<Proc_Get_AllLocationsForAdminResult> GetAllLocationsForAdmin(int PageIndex, int PageSize, bool IsPaging, int LocationId, Int16 StatusType, int FkCompanyId, string SortingColumn, string SortingDirection, string FilterColumn, byte FilterAction, string FilterValue, out int TotalRec)
        {
            using (EmergeDALDataContext db = new EmergeDALDataContext())
            {
                int? count = 0;
                List<Proc_Get_AllLocationsForAdminResult> List = db.GetAllLocationsForAdmin(PageIndex, PageSize, IsPaging, LocationId, StatusType, FkCompanyId, SortingColumn, SortingDirection, FilterColumn, FilterAction, FilterValue, ref count).ToList();
                TotalRec = (int)count;
                return List;
            }
        }

        public List<Proc_Get_AllEmergeUsersForAdminResult> GetAllEmergeUsersForAdmin(int PageIndex, int PageSize, bool IsPaging, Int16 StatusType, int CompanyId, int FkLocationId, string SortingColumn, string SortingDirection, string FilterColumn, byte FilterAction, string FilterValue, out int TotalRec)
        {
            using (EmergeDALDataContext dbObj = new EmergeDALDataContext())
            {
                int? count = 0;
                List<Proc_Get_AllEmergeUsersForAdminResult> List = dbObj.GetAllEmergeUsersForAdmin(PageIndex, PageSize, IsPaging, StatusType, CompanyId, FkLocationId, SortingColumn, SortingDirection, FilterColumn, FilterAction, FilterValue, ref count).ToList();
                TotalRec = (int)count;
                return List;
            }
        }

        /// <summary>
        /// Function To Update Company 
        /// </summary>
        /// <returns></returns>
        public int UpdateCompany(tblCompany ObjtblCompany, tblLocation ObjtblLocation)
        {
            DbTransaction ObjDbTransaction = null;
            BALLocation ObjBALLocation;
            int IsDuplicateAPIUserName = 0;
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                ObjEmergeDALDataContext.Connection.Open();
                ObjDbTransaction = ObjEmergeDALDataContext.Connection.BeginTransaction();
                ObjEmergeDALDataContext.Transaction = ObjDbTransaction;
                int IsDuplicateAccountNumber = (from db in ObjEmergeDALDataContext.tblCompanies
                                                where db.CompanyAccountNumber == ObjtblCompany.CompanyAccountNumber
                                                && db.pkCompanyId != ObjtblCompany.pkCompanyId && db.IsDeleted == false
                                                select db).Count();
                if (IsDuplicateAccountNumber == 0)
                {
                    if (ObjtblCompany.APIUsername != string.Empty)
                    {
                        //Changes based on the INT-324
                        //If ApiUserName admin@usaintel.com then company infromation will be updated.
                        if (ObjtblCompany.APIUsername == "admin@usaintel.com")
                        {
                            IsDuplicateAPIUserName = 0;
                        }
                        else
                        {
                            IsDuplicateAPIUserName = (from db in ObjEmergeDALDataContext.tblCompanies
                                                      where db.APIUsername == ObjtblCompany.APIUsername
                                                        && db.pkCompanyId != ObjtblCompany.pkCompanyId
                                                      select db).Count();
                        }

                    }
                    if (IsDuplicateAPIUserName == 0)
                    {
                        ObjtblCompany.CompanyAccountNumber = (ObjtblCompany.CompanyAccountNumber != null) ? ObjtblCompany.CompanyAccountNumber : string.Empty;
                        int Result = ObjEmergeDALDataContext.UpdateCompany(ObjtblCompany.pkCompanyId,
                                                                ObjtblCompany.fkSalesRepId,
                                                                ObjtblCompany.fkSalesManagerId,
                                                                ObjtblCompany.CompanyAccountNumber,
                                                                ObjtblCompany.fkCompanyTypeId,
                                                                ObjtblCompany.CompanyName,
                                                                ObjtblCompany.CompanyNote,
                                                                ObjtblCompany.FranchiseName,
                                                                ObjtblCompany.MainContactPersonName,
                                                                ObjtblCompany.BillingContactPersonName,
                                                                ObjtblCompany.MainEmailId,
                                                                ObjtblCompany.BillingEmailId,
                                                                ObjtblCompany.IsEnabled,
                                                                ObjtblCompany.LastModifiedDate,
                                                                ObjtblCompany.LastModifiedById,
                                                                ObjtblCompany.IsEnableReview,
                                                                ObjtblCompany.ReportEmailAddress,
                                                                ObjtblCompany.IsReportEmailActivated,
                                                                ObjtblCompany.PaymentTerm,
                                                                ObjtblCompany.Rating,
                                                                ObjtblCompany.ReviewComments,
                                                                ObjtblCompany.LastReviewDate,
                                                                ObjtblCompany.EnableAPI,
                                                                ObjtblCompany.Integration,
                                                                ObjtblCompany.APIUsername,
                                                                ObjtblCompany.APIPassword,
                                                                ObjtblCompany.PaymentTerms,
                                                                ObjtblCompany.CompanyLogo,
                                                                ObjtblCompany.APILocation,
                                                                ObjtblCompany.PermissiblePurpose, ObjtblCompany.ApiUrl, ObjtblCompany.Is7YearFilter, ObjtblCompany.IsReportStatus, ObjtblCompany.IsturnoffSSN, ObjtblCompany.Is7YearAutoCheck, ObjtblCompany.isfcrjurisdictionfee, ObjtblCompany.IsDuplicateSSNReportAlertEnabled, ObjtblCompany.isEnabledBillingLocation);

                        if (Result < 0)
                        {
                            if (ObjDbTransaction != null)
                            {
                                ObjDbTransaction.Rollback();
                            }
                            return -2;//false   //-2 error in company updating
                        }

                        ObjBALLocation = new BALLocation();

                        int LocationResult = ObjBALLocation.UpdateLocation(ObjtblLocation, ObjtblCompany.pkCompanyId);

                        if (LocationResult != 1)
                        {
                            if (ObjDbTransaction != null)
                            {
                                ObjDbTransaction.Rollback();
                            }
                            return -3;//LocationResult // -3 error in Location updating

                        }

                        ObjDbTransaction.Commit();

                        return 1;//LocationResult //1 for success
                    }
                    return -5;//false//-5 for APIUserName already exist
                }
                return -4;//false //-4 for already exist
            }
        }


        public List<Proc_Get_EmergeReportsForAdminResult> GetAdminReport(byte reportid)
        {
            using (EmergeDALDataContext obj = new EmergeDALDataContext())
            {
                return obj.GetEmergeReportsForAdmin(reportid).ToList();

            }
        }

        public List<tblCompany_Price> GetCompanyPricebyPkProductid(int pkproductidappid)
        {
            using (EmergeDALDataContext obj = new EmergeDALDataContext())
            {
                List<tblCompany_Price> ObjtblCompany_Price = obj.tblCompany_Prices.Where(data => data.fkProductApplicationId == pkproductidappid).ToList<tblCompany_Price>();
                return ObjtblCompany_Price;
            }

        }
        public List<proc_GetSalesValuesforGraphResult> GetSalesValuesforGraph(int pkCompanyId, int Month, int Year)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetSalesValuesforGraph(pkCompanyId, Month, Year).ToList();
            }
        }

        public List<proc_GetSalesValuesforGraphforPdfResult> GetSalesValuesforGraphforPdf(int pkCompanyId, int Month, int Year)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetSalesValuesforGraphforPdf(pkCompanyId, Month, Year).ToList();
            }
        }

        public List<proc_GetSalesValuesforGraphMVCResult> GetSalesValuesforGraphMVC(int pkCompanyId, int NumberOfMonth)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetSalesValuesforGraphMVC(pkCompanyId, NumberOfMonth).ToList();
            }
        }

        public List<proc_GetVolumeValuesforGraphMVCResult> GetVolumeValuesforGraphMVC(int pkCompanyId, int NumberOfMonth)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetVolumeValuesforGraphMVC(pkCompanyId, NumberOfMonth).ToList();
            }
        }

        public List<Proc_GetReportProductsByIdResult> GetReportByIdatEdit(int locationid, byte reportcatid, int fkcompanyid)
        {
            using (EmergeDALDataContext objdb = new EmergeDALDataContext())
            {
                return objdb.GetReportProductsById(locationid, reportcatid, fkcompanyid).ToList();

            }
        }

        public void AddProductPricePerCompany(List<tblCompany_Price> ObjtblCompany_Price)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                if (ObjtblCompany_Price.Count != 0)
                {
                    foreach (var obj in ObjtblCompany_Price)
                    {
                        tblCompany_Price record = ObjDALDataContext.tblCompany_Prices.Where(data => (data.fkCompanyId == obj.fkCompanyId && data.fkProductApplicationId == obj.fkProductApplicationId)).FirstOrDefault();
                        if (record != null)
                        {
                            //var updateResult = 
                                ObjDALDataContext.UpdateProductPricePerCompany(record.pkCompanyPriceId, obj.IsDefaultPrice, obj.EnableReviewPerCompany, obj.CompanyPrice, obj.MonthlySaving, obj.MonthlyVolume, obj.CurrentPrice);
                        }
                        else
                        {
                            ObjDALDataContext.tblCompany_Prices.InsertOnSubmit(obj);
                            ObjDALDataContext.SubmitChanges();
                        }
                    }

                }
            }
        }

        public List<Proc_Get_ReportsByCategoryIdResult> GetReportProductsById(int LocationId, byte ReportCategoryId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.Get_ReportsByCategoryId(LocationId, ReportCategoryId).ToList();
            }
        }

        public List<Proc_Get_ReportCategoriesResult> GetReportCategories()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetReportCategories().ToList();
            }
        }
        public List<Proc_GetProductDescrptionResult> GetProductDecrption(int pkid)
        {
            using (EmergeDALDataContext obj = new EmergeDALDataContext())
            {
                return obj.Proc_GetProductDescrption(pkid).ToList();
            }
        }

        public int UpdateEnableCompanies(int pkcompnayid)
        {
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                return dx.EnableCompanies(pkcompnayid);

            }
        }

        public tblLocation GetCompanyIdByLocationId(int pkLocationId)
        {
            tblLocation tbl = new tblLocation();
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                tbl = ObjDALDataContext.tblLocations.Where(d => d.pkLocationId == pkLocationId).FirstOrDefault();
            }
            return tbl;
        }

        public tblCompany GetCompanyByCompanyId(int pkCompanyId)
        {
            tblCompany tbl = new tblCompany();
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                tbl = ObjDALDataContext.tblCompanies.Where(d => d.pkCompanyId == pkCompanyId).FirstOrDefault();
            }
            return tbl;
        }

        //public void AddProductPricePerCompany(List<tblCompany_Price> ObjtblCompany_Price)
        //{
        //    using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
        //    {
        //        if (ObjtblCompany_Price.Count != 0)
        //        {
        //            List<tblCompany_Price> ObjtblCompany_Price_Delete = ObjDALDataContext.tblCompany_Prices.Where(data => data.fkCompanyId == ObjtblCompany_Price.FirstOrDefault().fkCompanyId).ToList<tblCompany_Price>();
        //            if (ObjtblCompany_Price_Delete.Count > 0)
        //            {
        //                ObjDALDataContext.tblCompany_Prices.DeleteAllOnSubmit(ObjtblCompany_Price_Delete);
        //            }
        //            ObjDALDataContext.tblCompany_Prices.InsertAllOnSubmit(ObjtblCompany_Price);
        //            ObjDALDataContext.SubmitChanges();
        //        }
        //    }
        //}

        public tblCompanyDocument GetCompanyDocumentsByCompanyDocId(int pkCompanyDocumentId)
        {
            tblCompanyDocument tbl = new tblCompanyDocument();
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    tbl = DX.tblCompanyDocuments.Where(d => d.pkCompanyDocumentId == pkCompanyDocumentId).FirstOrDefault();

                }
            }
            catch
            {

            }
            return tbl;
        }
        public int UpdateDisableCompanies(int pkcompnayid)
        {
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                return dx.UpdateDisableCompany(pkcompnayid);

            }
        }
        public List<Proc_Get_NewAccountsResult> GetNewAccounts(int PageIndex, int PageSize, bool IsPaging, string SearchString, string SortingColumn, string SortingDirection)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetNewAccounts(PageIndex, PageSize, IsPaging, SearchString, SortingColumn, SortingDirection).ToList<Proc_Get_NewAccountsResult>();
            }
        }


        public List<int> AddNewCompany(tblCompany objtbl, tblLocation ObjtblLocation, tblCompanyDocument objtbldocuments)
        {
            List<int> ObjGuid_CompLocation = new List<int>();
            List<int> abc = new List<int>();
            int CompanyId = 0;
            int NewLocationId = 0;
            BALLocation objballocation = new BALLocation();
            abc.Add(0);
            if (objtbl != null)
            {
                using (EmergeDALDataContext db = new EmergeDALDataContext())
                {
                    int IsDuplicateAccountNumber = (from datbase in db.tblCompanies
                                                    where datbase.CompanyAccountNumber == objtbl.CompanyAccountNumber
                                                    select datbase).Count();
                    if (IsDuplicateAccountNumber == 0)
                    {
                        var Comapnyresult = db.AddCompany(objtbl.fkCompanyTypeId,
                                                        objtbl.fkApplicationId,
                                                        objtbl.CompanyCode,
                                                        objtbl.fkSalesRepId,
                                                        objtbl.fkSalesManagerId,
                                                        objtbl.CompanyAccountNumber,
                                                        objtbl.CompanyNote,
                                                        objtbl.CompanyName,
                                                        objtbl.FranchiseName,
                                                        objtbl.MainContactPersonName,
                                                        objtbl.BillingContactPersonName,
                                                        objtbl.MainEmailId,
                                                        objtbl.BillingEmailId,
                                                        objtbl.IsEnabled,
                                                        objtbl.IsDeleted,
                                                        objtbl.CreatedDate,
                                                        objtbl.LastModifiedDate,
                                                        objtbl.CreatedById,
                                                        objtbl.LastModifiedById,
                                                        objtbl.IsEnableReview,
                                                        objtbl.ReportEmailAddress,
                                                        objtbl.IsReportEmailActivated,
                                                        objtbl.PaymentTerm,
                                                        objtbl.Rating,
                                                        objtbl.ReviewComments,
                                                        objtbl.LastReviewDate,
                                                        objtbl.EnableAPI,
                                                        objtbl.Integration,
                                                        objtbl.APIUsername,
                                                        objtbl.APIPassword,
                                                        objtbl.PaymentTerms,
                                                        objtbl.CompanyLogo,
                                                        objtbl.APILocation,
                                                        objtbl.PermissiblePurpose, objtbl.ApiUrl, objtbl.IsDuplicateSSNReportAlertEnabled, objtbl.isEnabledBillingLocation);


                        CompanyId = Convert.ToInt16(Comapnyresult.ElementAt(0).CompanyId);


                        ObjGuid_CompLocation.Add(CompanyId == 0 ? 0 : CompanyId);

                        NewLocationId = objballocation.InsertNewLocation(CompanyId, ObjtblLocation);

                        ObjGuid_CompLocation.Add(NewLocationId == 0 ? 0 : NewLocationId);

                        tblCompanyDocument tbl = new tblCompanyDocument();
                        tbl.pkCompanyDocumentId = objtbldocuments.pkCompanyDocumentId;
                        tbl.fkCompanyId = CompanyId == 0 ? 0 : CompanyId;
                        tbl.DocumentTitle = objtbldocuments.DocumentTitle;
                        tbl.DocumentName = objtbldocuments.DocumentName;
                        tbl.CreatedDate = objtbldocuments.CreatedDate;
                        tbl.IsUserAgreement = objtbldocuments.IsUserAgreement;

                        db.tblCompanyDocuments.InsertOnSubmit(tbl);
                        db.SubmitChanges();


                        return ObjGuid_CompLocation;
                    }
                    else
                    {
                        return null;
                    }

                }
            }
            else
            {
                return null;
            }
        }

        public tblCompany CheckDuplicasebyComapnyName(string CompnayName)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                tblCompany ObjRspnData = DX.tblCompanies.Where(db => db.CompanyName == CompnayName && db.IsDeleted == false).FirstOrDefault();
                return ObjRspnData;
            }

        }

        /// <summary>
        /// Check duplicate company name and email for new lead and client for ticket #154
        /// Get Sales Manager Name
        /// </summary>
        /// <param name="CompnayName"></param>
        /// <returns></returns>
        public string CheckDuplicateComapnyName(string CompnayName)
        {
            string SalesManagerName = string.Empty;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                SalesManagerName = (from tcu in DX.tblCompanyUsers
                                    join tc in DX.tblCompanies on tcu.fkUserId equals tc.fkSalesManagerId
                                    where tc.CompanyName == CompnayName && tc.IsDeleted == false
                                    select tcu.FirstName + " " + tcu.LastName).FirstOrDefault();

            }
            return SalesManagerName;
        }

        /// <summary>
        /// Check duplicate email for new lead and client for ticket #154
        /// Get Sales Manager Name for tickt #154
        /// </summary>
        /// <param name="pkCompanyDocumentId"></param>
        /// <returns></returns>

        public List<Proc_GetSaleMangerByDuplicateEmailResult> CheckDuplicateEmail(string Email, string companyName)
        {
            //string SalesManagerName = string.Empty;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return DX.GetSaleMangerByDuplicateEmail(Email, companyName).ToList();
            }
        }

        public int DeleteCompanyDocumentsByCompanyDocId(int pkCompanyDocumentId)
        {
            int Result = 0;
            try
            {
                using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
                {
                    tblCompanyDocument tbl = ObjDALDataContext.tblCompanyDocuments.Where(d => d.pkCompanyDocumentId == pkCompanyDocumentId).FirstOrDefault();
                    if (tbl != null)
                    {
                        ObjDALDataContext.tblCompanyDocuments.DeleteOnSubmit(tbl);
                        ObjDALDataContext.SubmitChanges();
                        Result = 1;
                    }

                }
            }
            catch
            {

            }
            return Result;
        }
        //public List<Guid> InsertNewCompany(tblCompany ObjtblCompany, tblLocation ObjtblLocation, tblCompanyDocument ObjtblCompanyDocument)
        //{
        //    List<Guid> ObjGuid_CompLocation = new List<Guid>();

        //    ObjBALLocation = new BALLocation();
        //    DbTransaction ObjDbTransaction = null;
        //    Guid NewLocationId = Guid.Empty;
        //    int CompanyId = Guid.Empty;
        //    try
        //    {
        //        using (EmergeDALDataContext DALDataContext = new EmergeDALDataContext())
        //        {
        //            //if (ObjDbTransaction.Connection.State == ConnectionState.Open)
        //            //{
        //            //    ObjDbTransaction.Connection.Close();
        //            //}
        //            //ObjDbTransaction.Connection.Open();
        //            //ObjDbTransaction = ObjDALDataContext.Connection.BeginTransaction();
        //            //ObjDALDataContext.Transaction = ObjDbTransaction;
        //            int IsDuplicateAccountNumber = (from db in DALDataContext.tblCompanies
        //                                            where db.CompanyAccountNumber == ObjtblCompany.CompanyAccountNumber
        //                                            select db).Count();
        //            if (IsDuplicateAccountNumber == 0)
        //            {
        //                var CompanyResult = DALDataContext.AddCompany(
        //                                             ObjtblCompany.fkCompanyTypeId,
        //                                             ObjtblCompany.fkApplicationId,
        //                                             ObjtblCompany.CompanyCode,
        //                                             ObjtblCompany.fkSalesRepId,
        //                                             ObjtblCompany.fkSalesManagerId,
        //                                             ObjtblCompany.CompanyAccountNumber,
        //                                             ObjtblCompany.CompanyNote,
        //                                             ObjtblCompany.CompanyName,
        //                                             ObjtblCompany.FranchiseName,
        //                                             ObjtblCompany.MainContactPersonName,
        //                                             ObjtblCompany.BillingContactPersonName,
        //                                             ObjtblCompany.MainEmailId,
        //                                             ObjtblCompany.BillingEmailId,
        //                                             ObjtblCompany.IsEnabled,
        //                                             ObjtblCompany.IsDeleted,
        //                                             ObjtblCompany.CreatedDate,
        //                                             ObjtblCompany.LastModifiedDate,
        //                                             ObjtblCompany.CreatedById,
        //                                             ObjtblCompany.LastModifiedById,
        //                                             ObjtblCompany.IsEnableReview,
        //                                             ObjtblCompany.ReportEmailAddress,
        //                                             ObjtblCompany.IsReportEmailActivated,
        //                                             ObjtblCompany.PaymentTerm,
        //                                             ObjtblCompany.Rating,
        //                                             ObjtblCompany.ReviewComments,
        //                                             ObjtblCompany.LastReviewDate
        //                );


        //                CompanyId = new Guid(CompanyResult.ElementAt(0).CompanyId.ToString());


        //                ObjGuid_CompLocation.Add(CompanyId == Guid.Empty ? Guid.Empty : CompanyId);


        //                //if (CompanyId == Guid.Empty)
        //                //{
        //                //    if (ObjDbTransaction != null)
        //                //    {
        //                //        ObjDbTransaction.Rollback();
        //                //    }
        //                //    return CompanyId;
        //                //}

        //                NewLocationId = ObjBALLocation.InsertNewLocation(CompanyId, ObjtblLocation);

        //                ObjGuid_CompLocation.Add(NewLocationId == Guid.Empty ? Guid.Empty : NewLocationId);


        //                tblCompanyDocument tbl = new tblCompanyDocument();
        //                tbl.pkCompanyDocumentId = ObjtblCompanyDocument.pkCompanyDocumentId;
        //                tbl.fkCompanyId = CompanyId == Guid.Empty ? Guid.Empty : CompanyId;
        //                tbl.DocumentTitle = ObjtblCompanyDocument.DocumentTitle;
        //                tbl.DocumentName = ObjtblCompanyDocument.DocumentName;
        //                tbl.CreatedDate = ObjtblCompanyDocument.CreatedDate;
        //                tbl.IsUserAgreement = ObjtblCompanyDocument.IsUserAgreement;

        //                ObjDALDataContext.tblCompanyDocuments.InsertOnSubmit(tbl);
        //                ObjDALDataContext.SubmitChanges();


        //                //if (NewLocationId == Guid.Empty)
        //                //{
        //                //    if (ObjDbTransaction != null)
        //                //    {
        //                //        ObjDbTransaction.Rollback();
        //                //    }
        //                //    return NewLocationId;
        //                //}
        //                //ObjDbTransaction.Commit();
        //                //return NewLocationId;
        //            }
        //            else
        //            {
        //                return ObjGuid_CompLocation;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {

        //    }
        //    return ObjGuid_CompLocation;
        //}




        public int AddNewReference(int fkcompnayid, string RefenceCode, string referenceNote)
        {
            using (EmergeDALDataContext dbobj = new EmergeDALDataContext())
            {
                var result2 = dbobj.AddReference(fkcompnayid, RefenceCode, referenceNote);
                return Convert.ToInt16(result2);
            }

        }

        public int UpdateEnableLocation(int pkcompnayid)
        {
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                return dx.UpdateEnableLocation(pkcompnayid);

            }
        }

        public int UpdateDisableLocation(int pkcompnayid)
        {
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                return dx.UpdateDisableLocation(pkcompnayid);

            }
        }


        public int UpdateDisableUser(Guid pkuserid)
        {
            using (EmergeDALDataContext db = new EmergeDALDataContext())
            {
                return db.UpdateDisableUser(pkuserid);
            }

        }

        public List<Proc_Get_SalesManagersResult> GetManagers(string RoleName)
        {
            using (EmergeDALDataContext dbobj = new EmergeDALDataContext())
            {
                return dbobj.Get_SalesManagers(RoleName).ToList();
            }
        }

        public List<Proc_GetStateListResult> GetState()
        {
            using (EmergeDALDataContext dbobj = new EmergeDALDataContext())
            {
                return dbobj.GetStateList().ToList();
            }
        }

        //public List<Proc_Get_CityListByStateIdResult> GetCityStateWise(int StateId)
        //{
        //    using (EmergeDALDataContext DX = new EmergeDALDataContext())
        //    {
        //        return DX.Get_CityListByStateId(StateId).ToList();
        //    }
        //}

        public List<City> GetCityStateWise(int StateId)
        {

            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                List<City> ObjCity = new List<City>();
                ObjCity = (from db in DX.tblCities
                           where db.fkStateId == StateId
                           select new City
                           {
                               CityId = db.pkCityId,
                               CityName = db.City
                           }).Distinct().OrderBy(dt => dt.CityName).ToList<City>();

                //ObjCity = DX.tblCities.Where(db => db.fkStateId == StateId).Select(d => new City { CityId = d.pkCityId, CityName = d.City }).Distinct().OrderBy(d => d.CityName).ToList();

                //List<tblCity> ObjTblCity = DX.tblCities.Where(db => db.fkStateId == StateId).Distinct().OrderBy(dt => dt.City).ToList();
                return ObjCity;
            }
        }



        public int GetMaxAccountNumber()
        {
            int MaxAccountNmber = 0;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                var v = DX.tblCompanies.Select(d => d.CompanyAccountNumber).Max();
                if (v != null)
                {
                    MaxAccountNmber = Convert.ToInt32(v);
                    MaxAccountNmber++;
                }
            }
            return MaxAccountNmber;
        }

        public List<tblCompanyType> GetAllCompanyType()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.tblCompanyTypes.OrderBy(db => db.CompanyType).ToList<tblCompanyType>();
            }
        }

        public int UpdateEnableUser(Guid pkuserid)
        {
            using (EmergeDALDataContext db = new EmergeDALDataContext())
            {
                return db.UpdateEnableUser(pkuserid);
            }
        }


        public Tuple<string, int, string> GetCurrentCompanyName(string url)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {

                var query = (from u in ObjDALDataContext.tblCompanies
                             where u.ApiUrl == url
                             select new { u.CompanyName, u.pkCompanyId, u.CompanyLogo }).FirstOrDefault();

                string CompanyName = string.Empty;
                int pkCompanyId = 0;
                string CompanyLogo = string.Empty;
                if (query != null)
                {
                    CompanyName = query.CompanyName;
                    pkCompanyId = query.pkCompanyId;
                    CompanyLogo = query.CompanyLogo;
                }
                return new Tuple<string, int, string>(CompanyName, pkCompanyId, CompanyLogo);

            }


        }

        public Tuple<bool> IsEscreenEnableForCompany(int CompanyId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                bool IsEscreenenable = false;
                var Obj_thirdpartycredentialdata = ObjDALDataContext.tblthirdpartycredentials.Where(db => db.fkCompanyId == CompanyId).FirstOrDefault();
                if (Obj_thirdpartycredentialdata != null)
                {

                    IsEscreenenable = Obj_thirdpartycredentialdata.Isenable;
                }



                return new Tuple<bool>(IsEscreenenable);

            }


        }



        public List<Proc_Get_AllClientsResult> GetAllClients(int PageIndex, int PageSize, bool IsPaging, string SearchString,
            Int16 CompanyType, string StatusType, string SortingColumn,
            string SortingDirection, string FromDate, string ToDate, Guid SalesManagerId, Guid SalesRepId, Guid CurrentUserId, string LeadSource, string filterColumn, byte filterAction, string filterValue)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.Get_AllClients(PageIndex, PageSize, IsPaging, SearchString, CompanyType, StatusType,
                                                                  SortingColumn, SortingDirection, FromDate, ToDate, SalesManagerId, SalesRepId, CurrentUserId, LeadSource, filterColumn, filterAction, filterValue).ToList<Proc_Get_AllClientsResult>();
            }
        }



        public void sendStatusChangedMail(byte Status, string CompanyName, string EmailId)
        {
            try
            {
                //MembershipUser ObjMembershipUser = Membership.GetUser(Utilitys.GetUID(HttpContext.Current.User.Identity.Name));
                //bool CheckSend = false;
                //string emailFrom = ObjMembershipUser.Email;
                string emailFrom = "admin@intelifi.com";
                string StrDate = DateTime.Now.ToString();

                #region Replace Bookmark
                string MessageBody = "";
                string emailText = "";
                string imgSrc = "";
                BALGeneral ObjBALGeneral = new BALGeneral();
                string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;
                var emailContent = ObjBALGeneral.GetEmailTemplate(Utilitys.SiteApplicationId, 33);


                emailText = emailContent.TemplateContent;

                BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();



                Bookmarks objBookmark = new Bookmarks();
                objBookmark.EmailContent = emailText;
                objBookmark.Date = StrDate;
                if (!string.IsNullOrEmpty(CompanyName))
                {
                    objBookmark.CompanyName = CompanyName;
                }
                if (Status == 7)
                {
                    imgSrc = ApplicationPath.GetApplicationPath() + @"Resources/Images/emerge-office-milestone-7-on.png";
                }
                else if (Status == 8)
                {
                    imgSrc = ApplicationPath.GetApplicationPath() + @"Resources/Images/emerge-office-milestone-8-on.png";
                }
                else if (Status == 9)
                {
                    imgSrc = ApplicationPath.GetApplicationPath() + @"Resources/Images/emerge-office-milestone-9-on.png";
                }
                objBookmark.CompanyStatus = "<img src='" + imgSrc + "' />";
                objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                objBookmark.LoginLink = "<a style='color: #87ceea;' href='" + ApplicationPath.GetApplicationPath() + "'> Click here </a>";

                MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Utilitys.GetUID(!string.IsNullOrEmpty(System.Web.HttpContext.Current.User.Identity.Name.ToString()) ? System.Web.HttpContext.Current.User.Identity.Name.ToString() : "admin@intelifi.com"));

                #endregion
                string emailTo = "";
                if (!string.IsNullOrEmpty(EmailId))
                {
                    emailTo = EmailId;
                    //emailTo = "support@dotnetoutsourcing.com";
                }

                //CheckSend = 
                    Utilitys.SendMail(emailTo, emailFrom, MessageBody, "Account Status Change");
            }
            catch
            {

            }
        }

        public void UpdateCompanyIsActive(byte preStatus, int fkCompanyid)
        {
            BALCompany ObjBALCompany = new BALCompany();
            tblCompany ObjtblCompany = new tblCompany();
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();

            //int CompanyId = 0;
            Guid UserId = Guid.Empty;
            var dbCollection = ObjBALCompanyUsers.GetUserInformationByMembershipUserId(new Guid(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString()));
            if (dbCollection.Count > 0)
            {
                UserId = dbCollection.First().fkUserId;

            }


            if (fkCompanyid != null)
            {
                ObjtblCompany.pkCompanyId = fkCompanyid;
                ObjtblCompany.IsActive = preStatus;
                ObjtblCompany.LastModifiedDate = DateTime.Now;
                ObjtblCompany.StatusStartDate = DateTime.Now;
                ObjtblCompany.LastModifiedById = UserId;
                //int success =
                    ObjBALCompany.UpdateCompanyIsActive(ObjtblCompany);

            }
        }


        public void UpdateCallLog(string btnName, int? fkCompanyid)
        {
            Guid UserId;
            int? pkCompanyId;
            MembershipUser ObjMembershipUser = Membership.GetUser(Utilitys.GetUID(HttpContext.Current.User.Identity.Name));
            if (ObjMembershipUser != null)
            {
                UserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
                pkCompanyId = fkCompanyid;
                string Comments = "";
                string[] Btnstr = btnName.Split('_');
                switch (Btnstr[1].ToLower())
                {
                    case "5": Comments = "##5##"; break;
                    case "6": Comments = "##6##"; break;
                    case "7": Comments = "##7##"; break;
                    case "8": Comments = "##8##"; break;
                    case "9": Comments = "##9##"; break;
                }
                DateTime CreatedDate = System.DateTime.Now;
                AddStatus_ToCallLog(pkCompanyId, UserId, Comments, CreatedDate);
            }

        }
        public int AddStatus_ToCallLog(int? pkCompanyId, Guid UserId, string Comments, DateTime CreatedDate)
        {
            BALCompany ObjBALCompany = new BALCompany();
            int Result = 0;
            try
            {
                Result = ObjBALCompany.AddStatusToCallLog(pkCompanyId, UserId, Comments, CreatedDate);
            }
            catch (Exception)
            {

            }
            finally
            {
                ObjBALCompany = null;
            }
            return Result;
        }



        public void InactiveRunReportOver30Days()
        {
            Guid UserId = new Guid("fdd976e2-222f-47c9-b181-68e5e15ea358");


            var Update = GetAllClients(1, 25, false, "", -1, "", "CreatedDate", "Desc", "", "", Guid.Empty, Guid.Empty, UserId, string.Empty, string.Empty, 2, string.Empty);

            if (Update.Count > 0)
            {
                for (int m = 0; m < Update.Count(); m++)
                {
                    if ((Update.ElementAt(m).LeadStatusText == "InActive"))
                    {
                        int pkCompanyId = (Update.ElementAt(m).pkCompanyId != null) ? (int)Update.ElementAt(m).pkCompanyId : 0;

                        bool? IsActive = GetLatestOrderDateForInActiveService(pkCompanyId);
                        if (IsActive == true)
                        {
                            UpdateCallLog("imgbtn_5", pkCompanyId);
                            UpdateCompanyIsActive(0, pkCompanyId);
                            //sendStatusChangedMail(5, Update.ElementAt(m).CompanyName, Update.ElementAt(m).MainEmailId);
                        }
                        if (IsActive == null)
                        {
                            UpdateCompanyStatusStartDate(pkCompanyId);// new accounts that haven't run show up in the Inactive Accounts 30 days section.
                        }
                    }
                    if (Update.ElementAt(m).LeadStatusText == "Active")
                    {
                        int pkCompanyId = (Update.ElementAt(m).pkCompanyId != null) ? (int)Update.ElementAt(m).pkCompanyId : 0;
                        bool? IsActive = GetLatestOrderDateForInActiveService(pkCompanyId);
                        if (IsActive == false)
                        {
                            UpdateCallLog("imgbtn_6", pkCompanyId);
                            UpdateCompanyIsActive(1, pkCompanyId);
                            sendStatusChangedMail(6, Update.ElementAt(m).CompanyName, Update.ElementAt(m).MainEmailId);
                        }
                    }
                }

            }
        }


        public void UpdateCompanyStatusStartDate(int? pkCompanyId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                tblCompany ObjtblCompany = new tblCompany();
                try
                {
                    ObjtblCompany = DX.tblCompanies.Where(d => d.pkCompanyId == pkCompanyId).FirstOrDefault();
                    if (ObjtblCompany != null)
                    {
                        ObjtblCompany.StatusStartDate = DateTime.Now.AddMonths(-1);
                        DX.SubmitChanges();
                    }
                }
                catch
                {
                }
            }
        }



        public int DeleteReference(int PkRefcodeId, int fkcompanyid)
        {
            using (EmergeDALDataContext dbobj = new EmergeDALDataContext())
            {
                return dbobj.DeleteReference(PkRefcodeId, fkcompanyid);
            }
        }
        public List<tblLocation> GetAllCompanyLocation()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.tblLocations.OrderBy(db => db.City).ToList<tblLocation>();
            }
        }


        public List<proc_LoadOpenInvoiceByCompanyIdResult> LoadOpenInVoice(int PkCompanyId)
        {
            using (EmergeDALDataContext dbObj = new EmergeDALDataContext())
            {
                return dbObj.LoadOpenInvoiceByCompanyId(PkCompanyId).ToList();
            }
        }

        public List<Proc_Get_CompanyDetailByLocationIdResult> GetCompanyDetailByLocationId(int PkLocationId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.Proc_Get_CompanyDetailByLocationId(PkLocationId).ToList<Proc_Get_CompanyDetailByLocationIdResult>();
            }
        }

        public List<Proc_GetAllReportsByLocationIdResult> GetAllReportsLocationId(int PkLocationId)
        {
            using (EmergeDALDataContext dbobj = new EmergeDALDataContext())
            {
                return dbobj.GetAllReportsByLocationId(PkLocationId).ToList();
            }
        }


        public List<Proc_GetReferencesResult> GetRefernceNote(int fkCompanyId)
        {
            using (EmergeDALDataContext dbobj = new EmergeDALDataContext())
            {
                return dbobj.GetReferences(fkCompanyId).ToList();
            }
        }

        public List<Proc_Get_AllEmergeProductsInPackagesResult> GetAllEmergeProducts(int packageid, Guid pkApplicationId)
        {
            using (EmergeDALDataContext dbobj = new EmergeDALDataContext())
            {
                return dbobj.Get_AllEmergeProductsInPackages(packageid, pkApplicationId).ToList();
            }
        }

        public int InsertDocumentDetailsByCompanyId(tblCompanyDocument ObjtblCompanyDocument)
        {
            int Result = 0;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                tblCompanyDocument tbl = new tblCompanyDocument();
                tbl.pkCompanyDocumentId = ObjtblCompanyDocument.pkCompanyDocumentId;
                tbl.fkCompanyId = ObjtblCompanyDocument.fkCompanyId;
                tbl.DocumentTitle = ObjtblCompanyDocument.DocumentTitle;
                tbl.DocumentName = ObjtblCompanyDocument.DocumentName;
                tbl.CreatedDate = ObjtblCompanyDocument.CreatedDate;
                tbl.IsUserAgreement = ObjtblCompanyDocument.IsUserAgreement;

                ObjDALDataContext.tblCompanyDocuments.InsertOnSubmit(tbl);
                ObjDALDataContext.SubmitChanges();
                Result = 1;
            }
            return Result;
        }

        public List<tblCompanyDocument> GetAllCompanyDocumentsByCompanyId(int CompanyId)
        {
            List<tblCompanyDocument> lst = new List<tblCompanyDocument>();
            try
            {
                using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
                {
                    lst = ObjDALDataContext.tblCompanyDocuments.Where(d => d.fkCompanyId == CompanyId).OrderBy(d => d.CreatedDate).ToList();

                }
            }
            catch
            {

            }
            return lst;
        }

        public List<Proc_GetAdditionalReportsByPkOrderIdResult> GetAdditionalReportsByPkOrderId(int PkOrderId)
        {
            using (EmergeDALDataContext objDalContext = new EmergeDALDataContext())
            {
                return objDalContext.GetAdditionalReportsByPkOrderId(PkOrderId).ToList();
            }
        }
        /// <summary>
        /// Function To Get Comments by CompanyId
        /// </summary>
        /// <param name="fkCompanyId"></param>
        /// <returns></returns>            
        public List<Proc_Get_CommentsForLeadResult> GetCommentsForCompany(int? fkCompanyId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.Get_CommentsForLead(fkCompanyId).ToList<Proc_Get_CommentsForLeadResult>();
            }
        }

        /// <summary>
        /// Function To Add Comments to Call Log 
        /// </summary>
        /// <returns></returns>
        public int AddCommentsToCallLog(int pkCompanyId, Guid UserId, string Comments, DateTime CreatedDate)
        {
            int iresult = 0;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                //var Coll = 
                    DX.AddCommentsToCallLog(pkCompanyId, UserId, Comments, CreatedDate).ToList();
                iresult = 1;
            }
            return iresult;
        }


        public List<Proc_Get_CompanyUsersByCompanyIdResult> GetCompanyUserByPkCompanyId(int pkCompanyId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                return DX.Get_CompanyUsersByCompanyId(pkCompanyId).ToList();
            }
        }

        public List<proc_GetCompanyStatusBypkCompanyIdResult> GetCompanyStatausByCompanyId(int pkCompanyId)
        {
            using (EmergeDALDataContext objdata = new EmergeDALDataContext())
            {
                return objdata.GetCompanyStatusBypkCompanyId(pkCompanyId).ToList();
            }
        }


        /// <summary>
        /// Function to Get All the Company for emerge by application id,Enabled,Deleted
        /// </summary>
        /// <param name="ApplicationId"></param>
        /// <param name="Enabled"></param>
        /// <param name="Deleted"></param>
        /// <returns></returns>
        public List<CompanyList> GetAllCompanyForEmerge(Guid ApplicationId, bool Enabled, bool Deleted)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                IQueryable<CompanyList> ObjData = from c in ObjDALDataContext.tblCompanies
                                                  join l in ObjDALDataContext.tblLocations on c.pkCompanyId equals l.fkCompanyId
                                                  join s in ObjDALDataContext.tblStates on l.fkStateID equals s.pkStateId
                                                  where c.fkApplicationId == ApplicationId && c.IsDeleted == Deleted && l.IsHome == true && (c.LeadStatus == 0 || c.LeadStatus > 4)
                                                  orderby c.CompanyName ascending
                                                  select new CompanyList
                                                  {
                                                      CompanyName = c.CompanyName + "  [" + l.City + ", " + s.StateCode + "]",
                                                      pkCompanyId = c.pkCompanyId
                                                  };
                return ObjData.ToList();
            }
        }


        public List<Proc_GetSalesAssociatebypkCompanyIdResult> GetSalesAssociatebypkCompanyId(int pkcompanyid)
        {
            using (EmergeDALDataContext objdal = new EmergeDALDataContext())
            {
                return objdal.GetSalesAssociatebypkCompanyId(pkcompanyid).ToList();
            }
        }
        public List<Proc_GetAccountManagerbypkCompanyIdResult> GetAccountManagerbypkCompanyId(int pkcompanyid)
        {
            using (EmergeDALDataContext objdal = new EmergeDALDataContext())
            {
                return objdal.GetAccountManagerbypkCompanyId(pkcompanyid).ToList();
            }
        }


        /// <summary>
        /// Function to Get All Clients for Quality Control
        /// </summary>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <param name="IsPaging"></param>
        /// <param name="SearchString"></param>
        /// <param name="CompanyType"></param>
        /// <param name="StatusType"></param>
        /// <param name="SortingColumn"></param>
        /// <param name="SortingDirection"></param>
        /// <returns></returns>
        public List<Proc_GetClientsforQualityControlResult> GetClientsforQualityControl(int PageIndex, int PageSize, bool IsPaging, string SearchString,
            Int16 CompanyType, Int16 StatusType, string SortingColumn,
            string SortingDirection, string FromDate, string ToDate, Guid SalesManagerId, Guid SalesRepId, Guid CurrentUserId, string LeadSource)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.Proc_GetClientsforQualityControl(PageIndex, PageSize, IsPaging, SearchString, CompanyType, StatusType,
                                                                  SortingColumn, SortingDirection, FromDate, ToDate, SalesManagerId, SalesRepId, CurrentUserId, LeadSource).ToList<Proc_GetClientsforQualityControlResult>();

            }


        }



        public int UpdateCompanyRating(tblCompany _ObjtblCompany)
        {
            int success = 0;
            tblCompany ObjtblCompany = new tblCompany();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                try
                {
                    ObjtblCompany = DX.tblCompanies.Where(d => d.pkCompanyId == _ObjtblCompany.pkCompanyId).FirstOrDefault();
                    if (ObjtblCompany != null)
                    {
                        ObjtblCompany.Rating = _ObjtblCompany.Rating;
                        ObjtblCompany.SurveyorId = _ObjtblCompany.SurveyorId;
                        ObjtblCompany.SuveyorName = _ObjtblCompany.SuveyorName;
                        ObjtblCompany.LastReviewDate = _ObjtblCompany.LastReviewDate;
                        ObjtblCompany.ReviewComments = _ObjtblCompany.ReviewComments;
                        DX.SubmitChanges();
                        success = 1;
                    }
                }
                catch
                {
                    success = -1;
                }
            }
            return success;
        }


        public List<Proc_Get_CommentsForLead_ServiceResult> GetCommentsForCompany_Service(int fkCompanyId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.Get_CommentsForLead_Service(fkCompanyId).ToList<Proc_Get_CommentsForLead_ServiceResult>();
            }
        }



        /// <summary>
        /// To make Enable/disable users and Delete them
        /// </summary>
        /// <param name="CompanyId"></param>
        /// <param name="ActionToDo"></param>
        /// <param name="ActionValue"></param>
        /// <returns></returns>
        public int ActionOnCompany(List<tblCompany> listCompany, byte ActionToDo, bool ActionValue)
        {

            DbTransaction DbTrans = null;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    ObjDALDataContext.Connection.Open();
                    DbTrans = ObjDALDataContext.Connection.BeginTransaction();
                    ObjDALDataContext.Transaction = DbTrans;
                    for (int i = 0; i < listCompany.Count; i++)
                    {
                        ObjDALDataContext.TakeActionOnCompanies(listCompany.ElementAt(i).pkCompanyId, ActionToDo, ActionValue);
                    }
                    DbTrans.Commit(); // Commit Transaction
                    return (int)1;
                }
                catch (Exception ex)
                {
                    if (DbTrans != null)
                    {
                        DbTrans.Rollback(); // Rollback Transaction
                    }
                    throw ex;
                }
                finally
                {
                    ObjDALDataContext.Transaction = null;
                    if (ObjDALDataContext.Connection.State == ConnectionState.Open)
                    {
                        ObjDALDataContext.Connection.Close(); // Close Connection
                    }
                }
            }
        }

        /// <summary>
        /// Function To Update the IsDeleted Status of Single Row By ID
        /// </summary>
        /// <param name="CompanyId"></param>
        /// <returns></returns>
        public int DeleteCompanyById(int CompanyId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.DeleteCompany(CompanyId);
            }
        }

        public bool IsEnableGlobalCredentialForCompany(int pkCompanyId)
        {
            bool GlobalCredential = false;
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                GlobalCredential = dx.tblCompanies.Where(d => d.pkCompanyId == pkCompanyId).Select(d => d.IsGlobalCredential).FirstOrDefault();
            }
            return GlobalCredential;
        }

        public bool IsEnableCompany(int pkCompanyId)
        {
            bool retval = false;
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                retval = dx.tblCompanies.Where(d => d.pkCompanyId == pkCompanyId).Select(d => d.IsEnabled).FirstOrDefault();
            }
            return retval;
        }

        public int CompanyHomeLocationByCompanyId(int pkCompanyId)
        {
            int LocationId = 0;
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                LocationId = dx.tblLocations.Where(d => d.fkCompanyId == pkCompanyId && d.IsHome == true).Select(d => d.pkLocationId).FirstOrDefault();
            }
            return LocationId;
        }

        public int UpdateCompanyLeadStatus(tblCompany _ObjtblCompany)
        {
            int success = 0;
            tblCompany ObjtblCompany = new tblCompany();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                try
                {
                    ObjtblCompany = DX.tblCompanies.Where(d => d.pkCompanyId == _ObjtblCompany.pkCompanyId).FirstOrDefault();
                    if (ObjtblCompany != null)
                    {
                        //INT-56 New code
                        DX.UpdateCompanyLeadStatusOnPastDue(_ObjtblCompany.pkCompanyId, _ObjtblCompany.LeadStatus, _ObjtblCompany.IsEnabled, _ObjtblCompany.LastModifiedById);

                        //INT-56 Old Code
                        //ObjtblCompany.LeadStatus = _ObjtblCompany.LeadStatus;
                        //ObjtblCompany.IsEnabled = _ObjtblCompany.IsEnabled;
                        //ObjtblCompany.LastModifiedById = _ObjtblCompany.LastModifiedById;
                        //ObjtblCompany.LastModifiedDate = _ObjtblCompany.LastModifiedDate;
                        //ObjtblCompany.StatusStartDate = _ObjtblCompany.LastModifiedDate;
                        //DX.SubmitChanges();
                        success = 1;
                    }
                }
                catch
                {
                    success = -1;
                }
            }
            return success;
        }

        public int UpdateCompanyGlobalCredential(tblCompany _ObjtblCompany)
        {
            int success = 0;
            tblCompany ObjtblCompany = new tblCompany();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                try
                {
                    ObjtblCompany = DX.tblCompanies.Where(d => d.pkCompanyId == _ObjtblCompany.pkCompanyId).FirstOrDefault();
                    if (ObjtblCompany != null)
                    {
                        ObjtblCompany.IsGlobalCredential = _ObjtblCompany.IsGlobalCredential;
                        DX.SubmitChanges();
                        success = 1;
                    }
                }
                catch
                {
                    success = -1;
                }
            }
            return success;
        }

        public int SetCompanyEnableDisable(tblCompany _ObjtblCompany)
        {
            int success = 0;
            tblCompany ObjtblCompany = new tblCompany();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                try
                {
                    ObjtblCompany = DX.tblCompanies.Where(d => d.pkCompanyId == _ObjtblCompany.pkCompanyId).FirstOrDefault();
                    if (ObjtblCompany != null)
                    {
                        ObjtblCompany.IsEnabled = _ObjtblCompany.IsEnabled;
                        ObjtblCompany.IsActive = _ObjtblCompany.IsActive;
                        ObjtblCompany.LastModifiedById = _ObjtblCompany.LastModifiedById;
                        ObjtblCompany.LastModifiedDate = _ObjtblCompany.LastModifiedDate;
                        DX.SubmitChanges();
                        success = 1;
                    }
                }
                catch
                {
                    success = -1;
                }
            }
            return success;
        }

        public int UpdateDataSourceSettingsForLocation(int? pkCompanyId, int? HomeLocationid)
        {
            #region Variable Assignment

            int iResult = -1;
            DbTransaction DbTrans = null;

            #endregion

            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    #region Connection Open and Transaction Assignment

                    ObjDALDataContext.Connection.Open(); /* Connection Open */
                    DbTrans = ObjDALDataContext.Connection.BeginTransaction(); /* Assign Transation */
                    ObjDALDataContext.Transaction = DbTrans;

                    #endregion

                    #region Datasurce Settings for locations

                    ObjDALDataContext.UpdateDataSourcesByGlobalSettings(HomeLocationid, pkCompanyId);

                    #endregion

                    #region Transaction Committed

                    DbTrans.Commit(); /* Commit Transaction */

                    iResult = 1;

                    #endregion
                }
                catch
                {
                    #region Transaction Rollback

                    iResult = -1;
                    if (DbTrans != null)
                    {
                        DbTrans.Rollback(); /* Rollback Transaction */
                    }

                    #endregion
                }
                finally
                {
                    #region Connection and Transaction Close

                    ObjDALDataContext.Transaction = null;
                    if (ObjDALDataContext.Connection.State == ConnectionState.Open)
                    {
                        ObjDALDataContext.Connection.Close(); /* Close Connection */
                    }

                    #endregion
                }

                #region Return Result

                return iResult; /* Here we return result */

                #endregion
            }
        }

        public int UpdateCompanyDocByDocId(tblCompanyDocument ObjtblCompanyDocument)
        {
            int Result = 0;
            tblCompanyDocument ObjDoc = new tblCompanyDocument();
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    if ((ObjEmergeDALDataContext.tblCompanyDocuments.Where(d => d.DocumentTitle == ObjtblCompanyDocument.DocumentTitle && d.pkCompanyDocumentId != ObjtblCompanyDocument.pkCompanyDocumentId).Count()) == 0)
                    {
                        ObjDoc = ObjEmergeDALDataContext.tblCompanyDocuments.Where(d => d.pkCompanyDocumentId == ObjtblCompanyDocument.pkCompanyDocumentId).FirstOrDefault();
                        if (ObjDoc != null)
                        {
                            ObjDoc.DocumentTitle = ObjtblCompanyDocument.DocumentTitle;
                            ObjDoc.DocumentName = ObjtblCompanyDocument.DocumentName;
                            ObjEmergeDALDataContext.SubmitChanges();
                            Result = 1;
                        }
                    }
                    else
                    {
                        Result = -1;
                    }

                }
                catch (Exception)
                {

                    Result = 0;
                }
            }
            return Result;
        }



        public int DeleteCompanyDocumentsByDocId(int pkDocumentId)
        {
            int Result = 0;
            try
            {
                using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
                {
                    tblCompanyDocument tbl = ObjEmergeDALDataContext.tblCompanyDocuments.Where(d => d.pkCompanyDocumentId == pkDocumentId).FirstOrDefault();
                    if (tbl != null)
                    {
                        ObjEmergeDALDataContext.tblCompanyDocuments.DeleteOnSubmit(tbl);
                        ObjEmergeDALDataContext.SubmitChanges();
                        Result = 1;
                    }

                }
            }
            catch
            {

            }
            return Result;
        }

        public bool? GetLatestOrderDate(int? fkCompanyId)
        {
            bool? IsActive = null;
            DateTime Orderdt;
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                var Collection = dx.tblOrderDetails.Where(d => d.fkCompanyId == fkCompanyId);
                if (Collection != null)
                {
                    Orderdt = Convert.ToDateTime(Collection.Select(d => d.OrderDt).Max());
                    DateTime startTime = DateTime.Now;
                    TimeSpan span = startTime.Subtract(Orderdt);
                    if (span.Days <= 30)
                    {
                        IsActive = true;
                    }
                    else
                    {
                        IsActive = false;
                    }
                }
            }
            return IsActive;
        }

        public bool? GetLatestOrderDateForInActiveService(int? fkCompanyId)
        {
            bool? IsActive = null;
            DateTime Orderdt;
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                List<tblOrderDetail> Collection = dx.tblOrderDetails.Where(d => d.fkCompanyId == fkCompanyId).ToList();
                if (Collection != null && Collection.Count > 0)
                {
                    Orderdt = Convert.ToDateTime(Collection.Select(d => d.OrderDt).Max());
                    DateTime startTime = DateTime.Now;
                    TimeSpan span = startTime.Subtract(Orderdt);
                    if (span.Days <= 30)
                    {
                        IsActive = true;
                    }
                    else
                    {
                        IsActive = false;
                    }
                }
                else
                {
                    List<tblOrderDetails_Archieve> Collection_Archive = dx.tblOrderDetails_Archieves.Where(d => d.fkCompanyId == fkCompanyId).ToList();
                    if (Collection_Archive != null && Collection_Archive.Count > 0)
                    {
                        Orderdt = Convert.ToDateTime(Collection_Archive.Select(d => d.OrderDt).Max());
                        DateTime startTime = DateTime.Now;
                        TimeSpan span = startTime.Subtract(Orderdt);
                        if (span.Days <= 30)
                        {
                            IsActive = true;
                        }
                        else
                        {
                            IsActive = false;
                        }
                    }
                }
            }
            return IsActive;
        }

        public int UpdateCompanyStatus(tblCompany _ObjtblCompany)
        {
            int success = 0;
            tblCompany ObjtblCompany = new tblCompany();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                try
                {
                    ObjtblCompany = DX.tblCompanies.Where(d => d.pkCompanyId == _ObjtblCompany.pkCompanyId).FirstOrDefault();
                    if (ObjtblCompany != null)
                    {

                        //Updated Code for Ticket INT-56
                        DX.UpdateCompanyStatusOnLock(_ObjtblCompany.pkCompanyId, _ObjtblCompany.IsEnabled, _ObjtblCompany.LastModifiedById);
                        //Old code INT-56 
                        //ObjtblCompany.IsEnabled = _ObjtblCompany.IsEnabled;
                        //ObjtblCompany.LastModifiedById = _ObjtblCompany.LastModifiedById;
                        //ObjtblCompany.LastModifiedDate = _ObjtblCompany.LastModifiedDate;
                        //ObjtblCompany.StatusStartDate = _ObjtblCompany.LastModifiedDate;
                        //DX.SubmitChanges();
                        success = 1;
                    }
                }
                catch
                {
                    success = -1;
                }
            }
            return success;
        }




        /// <summary>
        /// Function To Add Status to Call Log 
        /// </summary>
        /// <returns></returns>


        public int AddStatusToCallLog(int? pkCompanyId, Guid UserId, string Comments, DateTime CreatedDate)
        {
            int iresult = 0;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                try
                {
                    tblCallLog ObjtblCallLog = new tblCallLog();
                    List<tblCallLog> lstcalllog = new List<tblCallLog>();
                    lstcalllog = DX.tblCallLogs.Where(db => db.fkCompanyId == pkCompanyId).ToList();
                    string[] stringSeparators = new string[] { "##" };
                    string[] statusArr = Comments.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                    if (statusArr.Length > 0)
                    {
                        int Status = Convert.ToInt32(statusArr[0]);
                        if (lstcalllog.Count > 0)
                        {
                            if (!lstcalllog.Any(db => db.LeadComments.Contains(("##" + Status + "##"))))
                            {
                                ObjtblCallLog = new tblCallLog();
                                int pkCallLogId = 0;
                                ObjtblCallLog.pkCallLogId = pkCallLogId;
                                ObjtblCallLog.fkCompanyId = pkCompanyId;
                                ObjtblCallLog.fkUserId = UserId;
                                ObjtblCallLog.LeadComments = "##" + Status + "##";
                                ObjtblCallLog.CreatedDate = CreatedDate;
                                DX.tblCallLogs.InsertOnSubmit(ObjtblCallLog);
                                DX.SubmitChanges();
                                iresult = 1;
                            }
                            else
                            {
                                foreach (tblCallLog objtblCallLog in lstcalllog)
                                {
                                    if (Status >= 7)
                                    {
                                        for (int i = Status; i >= 7; i--)
                                        {
                                            if (objtblCallLog.LeadComments.Contains("##" + i + "##"))
                                            {
                                                DX.tblCallLogs.DeleteOnSubmit(objtblCallLog);
                                                DX.SubmitChanges();
                                            }
                                        }
                                    }

                                    iresult = 1;
                                }

                            }
                        }
                        else
                        {
                            ObjtblCallLog = new tblCallLog();
                            int pkCallLogId = 0;
                            ObjtblCallLog.pkCallLogId = pkCallLogId;
                            ObjtblCallLog.fkCompanyId = pkCompanyId;
                            ObjtblCallLog.fkUserId = UserId;
                            ObjtblCallLog.LeadComments = "##" + Status + "##";
                            ObjtblCallLog.CreatedDate = CreatedDate;
                            DX.tblCallLogs.InsertOnSubmit(ObjtblCallLog);
                            DX.SubmitChanges();
                            iresult = 1;
                        }
                    }
                }
                catch
                {
                    iresult = -1;
                }
            }
            return iresult;
        }

        public int Add_StatusToCallLog(int pkCompanyId, Guid UserId, string Comments, DateTime CreatedDate)
        {
            int iresult = 0;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                try
                {
                    tblCallLog ObjtblCallLog = new tblCallLog();
                    List<tblCallLog> lstcalllog = new List<tblCallLog>();
                    lstcalllog = DX.tblCallLogs.Where(db => db.fkCompanyId == pkCompanyId).ToList();
                    string[] stringSeparators = new string[] { "##" };
                    string[] statusArr = Comments.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                    if (statusArr.Length > 0)
                    {
                        int Status = Convert.ToInt32(statusArr[0]);
                        if (lstcalllog.Count > 0)
                        {
                            if (!lstcalllog.Any(db => db.LeadComments.Contains(("##" + Status + "##"))))
                            {

                                for (int i = 1; i <= Status; i++)
                                {
                                    if (!lstcalllog.Any(db => db.LeadComments.Contains(("##" + i + "##"))))
                                    {

                                        var Obj_tblCallLog = new tblCallLog();
                                        int pkCallLogId = 0;
                                        Obj_tblCallLog.pkCallLogId = pkCallLogId;
                                        Obj_tblCallLog.fkCompanyId = pkCompanyId;
                                        Obj_tblCallLog.fkUserId = UserId;
                                        Obj_tblCallLog.LeadComments = "##" + i + "##";
                                        Obj_tblCallLog.CreatedDate = CreatedDate;
                                        DX.tblCallLogs.InsertOnSubmit(Obj_tblCallLog);

                                    }
                                }
                                DX.SubmitChanges();
                                iresult = 1;
                            }
                            else
                            {
                                foreach (tblCallLog objtblCallLog in lstcalllog)
                                {
                                    if (Status >= 7)
                                    {
                                        for (int i = Status; i >= 7; i--)
                                        {
                                            if (objtblCallLog.LeadComments.Contains("##" + i + "##"))
                                            {
                                                DX.tblCallLogs.DeleteOnSubmit(objtblCallLog);
                                                DX.SubmitChanges();
                                            }
                                        }
                                    }

                                    iresult = 1;
                                }

                            }
                        }
                        else
                        {
                            ObjtblCallLog = new tblCallLog();
                            int pkCallLogId = 0;
                            ObjtblCallLog.pkCallLogId = pkCallLogId;
                            ObjtblCallLog.fkCompanyId = pkCompanyId;
                            ObjtblCallLog.fkUserId = UserId;
                            ObjtblCallLog.LeadComments = "##" + Status + "##";
                            ObjtblCallLog.CreatedDate = CreatedDate;
                            DX.tblCallLogs.InsertOnSubmit(ObjtblCallLog);
                            DX.SubmitChanges();
                            iresult = 1;
                        }
                    }
                }
                catch
                {
                    iresult = -1;
                }
            }
            return iresult;
        }

        public List<ScreencatgoriesList> GetThirdPartyDetails(string CompanyId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                try
                {


                    var ObjData = (from u in DX.tblReportCategories
                                   where u.pkReportCategoryId == 10
                                   select new ScreencatgoriesList
                                   {
                                       fkReportCategoryId = Convert.ToInt32(u.pkReportCategoryId.ToString()),
                                       CategoryName = u.CategoryName.ToString(),
                                       fkCompanyId = Convert.ToInt16(CompanyId),
                                   }).OrderBy(x => x.fkReportCategoryId);

                    return ObjData.ToList();

                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
        }
        public List<tblthirdpartycredential> GetThirdPartyDetailsByCompanyId(int fkReportCategoryId, string CompanyId)
        {

            using (EmergeDALDataContext ObjUSAIntelDataDataContext = new EmergeDALDataContext())
            {
                try
                {
                    List<tblthirdpartycredential> CredentialDetails = (from u in ObjUSAIntelDataDataContext.tblthirdpartycredentials
                                                                       where u.fkReportCategoryId == fkReportCategoryId && u.fkCompanyId == Convert.ToInt16(CompanyId)
                                                                       select u).ToList();
                    if (CredentialDetails.Count != 0)
                    {
                        return CredentialDetails;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }




        }


        public int CreateThirdPartyDetailsByCompanyId(List<tblthirdpartycredential> tblRowList)
        {
            int iResult = -1;
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    //iterate through all row to update
                    foreach (var tblrow in tblRowList)
                    {
                        tblthirdpartycredential Obj_tblthirdpartycredential = new tblthirdpartycredential
                        {
                            Isenable = tblrow.Isenable,
                            username = tblrow.username,
                            password = tblrow.password,
                            accountno = tblrow.accountno,
                            subaccountno = tblrow.subaccountno,
                            fkCompanyId = tblrow.fkCompanyId,
                            fkReportCategoryId = 10 //Only for Escreen.
                        };
                        ObjEmergeDALDataContext.tblthirdpartycredentials.InsertOnSubmit(Obj_tblthirdpartycredential);
                        ObjEmergeDALDataContext.SubmitChanges();
                    }

                    iResult = 1;
                }
                catch
                {
                    iResult = -1;
                }
                return iResult; /* Here we return result */
            }
        }




        public int SaveThirdPartyDetailsByCompanyId(tblthirdpartycredential Objthirdpartycredential)
        {
            int iResult = -1;
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    IQueryable<tblthirdpartycredential> Obj_thirdpartycredentialdata = ObjEmergeDALDataContext.tblthirdpartycredentials.Where(db => db.pkthirdpartycredentialsId == Objthirdpartycredential.pkthirdpartycredentialsId);
                    if (Obj_thirdpartycredentialdata.Count() == 1)
                    {
                        tblthirdpartycredential ObjUpdate = Obj_thirdpartycredentialdata.First();
                        ObjUpdate.Isenable = Objthirdpartycredential.Isenable;
                        ObjUpdate.accountno = Objthirdpartycredential.accountno;
                        ObjUpdate.username = Objthirdpartycredential.username;
                        ObjUpdate.password = Objthirdpartycredential.password;
                        ObjUpdate.subaccountno = Objthirdpartycredential.subaccountno;
                        ObjEmergeDALDataContext.SubmitChanges();
                    }
                }
                catch
                {
                    iResult = -1;
                }
                return iResult; /* Here we return result */
            }
        }
        public List<FiltersCls> GetFilters()
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                List<FiltersCls> ObjData = new List<FiltersCls>();
                ObjData = (from u in DX.tblFilters
                           join s in DX.tblFilterConditions on u.pkFilterId equals s.fkFilterId
                           select new FiltersCls
                           {
                               FilterName = u.FilterName,
                               ReportType = u.ReportTypes,
                               Users = u.UserType,
                               Packages = u.IsPackages,
                               Enabled = u.IsEnabled,
                               StateId = u.StatesInclude,
                               Keywords = s.Keywords,
                               Year = s.Year,
                               Count = s.Count,
                               IsEmergeReview = s.IsEmergeReview,
                               IsWatchList = s.IsWatchList,
                               IsRemoveCharges = s.IsRemoveCharges,
                               fkFilterId = u.pkFilterId

                           }).ToList();
                return ObjData.ToList();

            }
        }
        public List<FiltersCls> GetFiltersAccordingFilterId(string FilterId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {

                int Filter_Id = int.Parse(FilterId);
                List<FiltersCls> ObjData = new List<FiltersCls>();
                ObjData = (from u in DX.tblFilters
                           join s in DX.tblFilterConditions on u.pkFilterId equals s.fkFilterId
                           where u.pkFilterId == Filter_Id
                           select new FiltersCls
                           {
                               FilterName = u.FilterName,
                               ReportType = u.ReportTypes,
                               Users = u.UserType,
                               Packages = u.IsPackages,
                               Enabled = u.IsEnabled,
                               StateId = u.StatesInclude,
                               Keywords = s.Keywords,
                               Year = s.Year,
                               Count = s.Count,
                               CountMax = s.CountMax,
                               CountMin = s.CountMin,
                               TimeFrameFrom = s.TimeFrameFrom,
                               TimeFrameIn = s.TimeFrameIn,
                               IsEmergeReview = s.IsEmergeReview,
                               IsWatchList = s.IsWatchList,
                               IsRemoveCharges = s.IsRemoveCharges,
                               fkFilterId = u.pkFilterId,
                               IsEnableForAllState = u.IsEnableForAllState

                           }).ToList();
                return ObjData.ToList();

            }
        }
        public List<StateName> GetStateName(List<tblState> State)
        {

            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                List<StateName> ObjData = new List<StateName>();
                ObjData = DX.tblStates.AsEnumerable().Join(State, tblst => tblst.pkStateId, st => st.pkStateId, (tblst, st) => new StateName
                {
                    State_Name = tblst.StateName,
                    pkStateId = tblst.pkStateId,
                    StateCode = tblst.StateCode
                }).ToList();

                return ObjData;

            }

        }

        public List<StateName> GetStateNameForEnable7YearFilter()
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                List<StateName> ObjData = new List<StateName>();
                ObjData = (from dts in DX.tblStates
                           where dts.IsEnabled == true
                           select new StateName
                           {
                               State_Name = dts.StateName,
                               pkStateId = dts.pkStateId,
                               StateCode = dts.StateCode
                           }).ToList();

                return ObjData;

            }

        }

        public List<LocationList> GetAllLocationsByCompanyId(int fkCompanyId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                IQueryable<LocationList> ObjData = from l in ObjDALDataContext.tblLocations
                                                   join s in ObjDALDataContext.tblStates on l.fkStateID equals s.pkStateId
                                                   where l.fkCompanyId == fkCompanyId && l.IsDeleted == false
                                                   orderby l.City ascending
                                                   select new LocationList
                                                   {
                                                       LocationCity = l.City + "  [" + s.StateCode + "]",
                                                       pkLocationId = l.pkLocationId
                                                   };
                return ObjData.ToList();
            }
        }
        public List<tblState> GetStates()
        {
            using (EmergeDALDataContext ObjUSAIntelDataDataContext = new EmergeDALDataContext())
            {
                try
                {
                    List<tblState> CredentialDetails = (from u in ObjUSAIntelDataDataContext.tblStates
                                                        select u).ToList();
                    if (CredentialDetails.Count != 0)
                    {
                        return CredentialDetails;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }




        }
        public int UpdateFilters(Filters_Class ObjFilters_Class)
        {
            int iResult = -1;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {

                try
                {
                    int Filter_Id = ObjFilters_Class.FilterId;
                    var Obj = (from t1 in DX.tblFilters
                               join t2 in DX.tblFilterConditions on t1.pkFilterId equals t2.fkFilterId
                               where t1.pkFilterId == Filter_Id
                               select new { t1, t2 }).First();

                    if (Obj != null)
                    {
                        Obj.t1.UserType = Convert.ToByte(ObjFilters_Class.Filter_Users.ToString());
                        Obj.t1.IsPackages = ObjFilters_Class.Packages;
                        Obj.t1.ReportTypes = ObjFilters_Class.DataSourceDropdown;
                        Obj.t1.IsEnabled = ObjFilters_Class.Enabled;
                        Obj.t1.StatesInclude = !string.IsNullOrEmpty(ObjFilters_Class.StateDropdown) ? ObjFilters_Class.StateDropdown : string.Empty;
                        Obj.t1.CountyInclude = !string.IsNullOrEmpty(ObjFilters_Class.CountyDropdown) ? ObjFilters_Class.CountyDropdown : string.Empty;
                        Obj.t2.Keywords = ObjFilters_Class.Key_words;
                        Obj.t2.IsEmergeReview = ObjFilters_Class.IsEmergeReview;
                        Obj.t2.IsRemoveCharges = ObjFilters_Class.IsRemoveCharges;
                        //Obj.t2.IsWatchList = ObjFilters_Class.IsWatchList;
                        Obj.t2.TimeFrameFrom = !string.IsNullOrEmpty(ObjFilters_Class.TimeFrameFrom) ? ObjFilters_Class.TimeFrameFrom : string.Empty;
                        Obj.t2.TimeFrameIn = !string.IsNullOrEmpty(ObjFilters_Class.TimeFrameIn) ? ObjFilters_Class.TimeFrameIn : string.Empty;
                        if (string.IsNullOrEmpty(ObjFilters_Class.Year) == false) { Obj.t2.Year = int.Parse(ObjFilters_Class.Year); }
                        if (string.IsNullOrEmpty(ObjFilters_Class.Count) == false) { Obj.t2.Count = int.Parse(ObjFilters_Class.Count); }
                        if (string.IsNullOrEmpty(ObjFilters_Class.Count) == false) { Obj.t2.Count = int.Parse(ObjFilters_Class.Count); }
                        if (string.IsNullOrEmpty(ObjFilters_Class.CountMax) == false) { Obj.t2.CountMax = int.Parse(ObjFilters_Class.CountMax); }
                        if (string.IsNullOrEmpty(ObjFilters_Class.CountMin) == false) { Obj.t2.CountMin = int.Parse(ObjFilters_Class.CountMin); }
                        DX.SubmitChanges();
                        iResult = 1;
                    }

                }
                catch
                {
                    iResult = -1;
                }
                return iResult;



            }
        }
        public int UpdateCompanyIsActive(tblCompany _ObjtblCompany)
        {
            int success = 0;
            tblCompany ObjtblCompany = new tblCompany();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                try
                {
                    ObjtblCompany = DX.tblCompanies.Where(d => d.pkCompanyId == _ObjtblCompany.pkCompanyId).FirstOrDefault();
                    if (ObjtblCompany != null)
                    {
                        ObjtblCompany.IsActive = _ObjtblCompany.IsActive;
                        ObjtblCompany.LastModifiedById = _ObjtblCompany.LastModifiedById;
                        ObjtblCompany.LastModifiedDate = _ObjtblCompany.LastModifiedDate;
                        ObjtblCompany.StatusStartDate = _ObjtblCompany.LastModifiedDate;
                        DX.SubmitChanges();
                        success = 1;
                    }
                }
                catch
                {
                    success = -1;
                }
            }
            return success;
        }
        public int UpdatetblOrderSearchedData(tblOrderSearchedData _ObjtblOrderSearchedData, int Order_Ids)
        {
            int success = 0;
            tblOrderSearchedData ObjtblOrderSearchedData = new tblOrderSearchedData();
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    ObjtblOrderSearchedData = DX.tblOrderSearchedDatas.Where(d => d.fkOrderId == Order_Ids).FirstOrDefault();
                    if (ObjtblOrderSearchedData != null)
                    {
                        ObjtblOrderSearchedData.SearchedLastName = _ObjtblOrderSearchedData.SearchedLastName;
                        ObjtblOrderSearchedData.SearchedFirstName = _ObjtblOrderSearchedData.SearchedFirstName;
                        ObjtblOrderSearchedData.SearchedMiddleInitial = _ObjtblOrderSearchedData.SearchedMiddleInitial;
                        ObjtblOrderSearchedData.SearchedSuffix = _ObjtblOrderSearchedData.SearchedSuffix;
                        ObjtblOrderSearchedData.SearchedSsn = _ObjtblOrderSearchedData.SearchedSsn;
                        ObjtblOrderSearchedData.SearchedDob = _ObjtblOrderSearchedData.SearchedDob;
                        ObjtblOrderSearchedData.SearchedPhoneNumber = _ObjtblOrderSearchedData.SearchedPhoneNumber;
                        ObjtblOrderSearchedData.SearchedApplicantEmail = _ObjtblOrderSearchedData.SearchedApplicantEmail;
                        ObjtblOrderSearchedData.SearchedStreetAddress = _ObjtblOrderSearchedData.SearchedStreetAddress;
                        ObjtblOrderSearchedData.SearchedDirection = _ObjtblOrderSearchedData.SearchedDirection;
                        ObjtblOrderSearchedData.SearchedStreetName = _ObjtblOrderSearchedData.SearchedStreetName;
                        ObjtblOrderSearchedData.SearchedStreetType = _ObjtblOrderSearchedData.SearchedStreetType;
                        ObjtblOrderSearchedData.SearchedApt = _ObjtblOrderSearchedData.SearchedApt;
                        ObjtblOrderSearchedData.SearchedCity = _ObjtblOrderSearchedData.SearchedCity;
                        ObjtblOrderSearchedData.SearchedStateId = _ObjtblOrderSearchedData.SearchedStateId;
                        ObjtblOrderSearchedData.SearchedZipCode = _ObjtblOrderSearchedData.SearchedZipCode;
                        ObjtblOrderSearchedData.best_SearchedZipCode = _ObjtblOrderSearchedData.best_SearchedZipCode;
                        ObjtblOrderSearchedData.search_county = _ObjtblOrderSearchedData.search_county;
                        ObjtblOrderSearchedData.best_search_county = _ObjtblOrderSearchedData.best_search_county;
                        ObjtblOrderSearchedData.SearchedCountyId = _ObjtblOrderSearchedData.SearchedCountyId;
                        ObjtblOrderSearchedData.SearchedDriverLicense = _ObjtblOrderSearchedData.SearchedDriverLicense;
                        ObjtblOrderSearchedData.SearchedVehicleVin = _ObjtblOrderSearchedData.SearchedVehicleVin;
                        ObjtblOrderSearchedData.SearchedDLStateId = _ObjtblOrderSearchedData.SearchedDLStateId;
                        ObjtblOrderSearchedData.SearchedSex = _ObjtblOrderSearchedData.SearchedSex;
                        ObjtblOrderSearchedData.SearchedBusinessName = _ObjtblOrderSearchedData.SearchedBusinessName;
                        ObjtblOrderSearchedData.SearchedBusinessCity = _ObjtblOrderSearchedData.SearchedBusinessCity;
                        ObjtblOrderSearchedData.SearchedBusinessStateId = _ObjtblOrderSearchedData.SearchedBusinessStateId;
                        ObjtblOrderSearchedData.SearchedTrackingRef = _ObjtblOrderSearchedData.SearchedTrackingRef;
                        ObjtblOrderSearchedData.SearchedTrackingRefId = _ObjtblOrderSearchedData.SearchedTrackingRefId;
                        ObjtblOrderSearchedData.SearchedTrackingNotes = _ObjtblOrderSearchedData.SearchedTrackingNotes;
                        DX.SubmitChanges();
                        success = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
            return success;
        }

        /// <summary>
        /// To get the reference code text by ref code selected value
        /// </summary>
        /// <param name="refCode"></param>
        /// <returns></returns>
        public string GetReferenceValueByRefCode(int refCode)
        {
            string strRefCode = string.Empty;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                var refVar = DX.tblReferenceCodes.Where(r => r.pkReferenceCodeId == refCode).Select(r => r.ReferenceCode).FirstOrDefault();
                if (refVar != null)
                {
                    strRefCode = Convert.ToString(refVar);
                }
            }
            return strRefCode;
        }








        public List<tblOrderSearchedEmploymentInfo> DeleteOrderSearchedEmploymentInfo(List<tblOrderSearchedEmploymentInfo> tblRowList)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                foreach (var tblrow in tblRowList)
                {

                    DX.tblOrderSearchedEmploymentInfos.Attach(tblrow);
                    DX.tblOrderSearchedEmploymentInfos.DeleteOnSubmit(tblrow);
                }


                DX.SubmitChanges();


                return tblRowList;
            }
        }
        public List<tblOrderSearchedEmploymentInfo> CreateSearchedEmploymentInfo(List<tblOrderSearchedEmploymentInfo> tblRowList)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {

                foreach (var tblrow in tblRowList)
                {
                    tblOrderSearchedEmploymentInfo Objtbl = new tblOrderSearchedEmploymentInfo
                    {
                        fkOrderId = tblrow.fkOrderId,
                        SearchedAlias = tblrow.SearchedAlias,
                        SearchedName = tblrow.SearchedName,
                        SearchedCity = tblrow.SearchedCity,
                        SearchedState = tblrow.SearchedState,
                        SearchedPhone = tblrow.SearchedPhone,
                        SearchedContact = tblrow.SearchedContact,
                        SearchedStarted = tblrow.SearchedStarted,
                        SearchedEnded = tblrow.SearchedEnded,
                        SearchedEmail = tblrow.SearchedEmail,
                        SearchedTitleStart = tblrow.SearchedTitleStart,
                        SearchedTitleEnd = tblrow.SearchedTitleEnd,
                        SearchedSupervisorContact = tblrow.SearchedSupervisorContact,
                        SearchedReason = tblrow.SearchedReason
                    };

                    DX.tblOrderSearchedEmploymentInfos.InsertOnSubmit(Objtbl);
                    DX.SubmitChanges();

                }
                return tblRowList;
            }
        }
        public List<tblOrderSearchedEducationInfo> DeleteOrderSearchedEducationInfo(List<tblOrderSearchedEducationInfo> tblRowList)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                foreach (var tblrow in tblRowList)
                {

                    DX.tblOrderSearchedEducationInfos.Attach(tblrow);
                    DX.tblOrderSearchedEducationInfos.DeleteOnSubmit(tblrow);
                }


                DX.SubmitChanges();


                return tblRowList;
            }
        }

        /// <summary>
        /// To get the County list
        /// </summary>
        /// <returns></returns>
        public List<string> GetCountyList()
        {
            List<string> lstCounty = new List<string>();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                lstCounty = DX.tblCounties.Select(c => c.CountyName.ToUpper()).ToList();
            }
            return lstCounty;
        }



        public List<tblOrderSearchedEducationInfo> CreateSearchedEducationInfo(List<tblOrderSearchedEducationInfo> tblRowList)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {

                foreach (var tblrow in tblRowList)
                {
                    tblOrderSearchedEducationInfo Objtbl = new tblOrderSearchedEducationInfo
                    {
                        fkOrderId = tblrow.fkOrderId,
                        SearchedSchoolName = tblrow.SearchedSchoolName,
                        SearchedCity = tblrow.SearchedCity,
                        SearchedPhone = tblrow.SearchedPhone,
                        SearchedAlias = tblrow.SearchedAlias,
                        SearchedState = tblrow.SearchedState,
                        SearchedStarted = tblrow.SearchedStarted,
                        SearchedDegree = tblrow.SearchedDegree,
                        SearchedGraduationDt = tblrow.SearchedGraduationDt,
                        SearchedEnded = tblrow.SearchedEnded,
                        SearchedMajor = tblrow.SearchedMajor,
                        SearchedFax = tblrow.SearchedFax,
                        SearchedGrade = tblrow.SearchedGrade,
                        SearchedCredit = tblrow.SearchedCredit,
                        SearchedEmail = tblrow.SearchedEmail
                    };

                    DX.tblOrderSearchedEducationInfos.InsertOnSubmit(Objtbl);
                    DX.SubmitChanges();

                }
                return tblRowList;
            }
        }
        public List<tblOrderSearchedPersonalInfo> DeleteOrderSearchedPersonalInfo(List<tblOrderSearchedPersonalInfo> tblRowList)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                foreach (var tblrow in tblRowList)
                {

                    DX.tblOrderSearchedPersonalInfos.Attach(tblrow);
                    DX.tblOrderSearchedPersonalInfos.DeleteOnSubmit(tblrow);
                }


                DX.SubmitChanges();


                return tblRowList;
            }
        }
        public List<tblOrderSearchedPersonalInfo> CreateSearchedPersonalInfo(List<tblOrderSearchedPersonalInfo> tblRowList)
        {

            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {

                foreach (var tblrow in tblRowList)
                {
                    tblOrderSearchedPersonalInfo Objtbl = new tblOrderSearchedPersonalInfo
                    {
                        fkOrderId = tblrow.fkOrderId,
                        SearchedName = tblrow.SearchedName,
                        SearchedPhone = tblrow.SearchedPhone,
                        pkOrderSearchedPersnlId = tblrow.pkOrderSearchedPersnlId,
                        SearchedCity = tblrow.SearchedCity,
                        SearchedState = tblrow.SearchedState,
                        SearchedAltPhone = tblrow.SearchedAltPhone,
                        SearchedRelation = tblrow.SearchedRelation,
                        SearchedEmail = tblrow.SearchedEmail,
                        SearchedSalary = tblrow.SearchedSalary,
                        SearchedTitle = tblrow.SearchedTitle,
                        SearchedAlias = tblrow.SearchedAlias

                    };

                    DX.tblOrderSearchedPersonalInfos.InsertOnSubmit(Objtbl);
                    DX.SubmitChanges();

                }
                return tblRowList;
            }



        }
        public List<tblOrderSearchedLicenseInfo> DeleteOrderSearchedLicenseInfo(List<tblOrderSearchedLicenseInfo> tblRowList)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                foreach (var tblrow in tblRowList)
                {

                    DX.tblOrderSearchedLicenseInfos.Attach(tblrow);
                    DX.tblOrderSearchedLicenseInfos.DeleteOnSubmit(tblrow);
                }
                DX.SubmitChanges();


                return tblRowList;
            }
        }
        public List<tblOrderSearchedLicenseInfo> CreateOrderSearchedLicenseInfo(List<tblOrderSearchedLicenseInfo> tblRowList)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                foreach (var tblrow in tblRowList)
                {
                    tblOrderSearchedLicenseInfo Objtbl = new tblOrderSearchedLicenseInfo
                    {
                        fkOrderId = tblrow.fkOrderId,
                        pkOrderLicenseVerifyId = tblrow.pkOrderLicenseVerifyId,
                        SearchedLicenseType = tblrow.SearchedLicenseType,
                        SearchedIssueDate = tblrow.SearchedIssueDate,
                        SearchedIssuingState = tblrow.SearchedIssuingState,
                        SearchedLicenseNo = tblrow.SearchedLicenseNo,
                        SearchedAlias = tblrow.SearchedAlias,
                        SearchedLicenseName = tblrow.SearchedLicenseName,
                        SearchedAuthority = tblrow.SearchedAuthority,
                        SearchedPhone = tblrow.SearchedPhone,
                        SearchedExpiryDate = tblrow.SearchedExpiryDate,
                        SearchedStatus = tblrow.SearchedStatus
                    };
                    DX.tblOrderSearchedLicenseInfos.InsertOnSubmit(Objtbl);
                    DX.SubmitChanges();
                }
                return tblRowList;
            }



        }


        public List<tblOrderSearchedLiveRunnerInfo> DeleteOrderSearchedLiveRunnerInfo(List<tblOrderSearchedLiveRunnerInfo> tblRowList)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                foreach (var tblrow in tblRowList)
                {

                    DX.tblOrderSearchedLiveRunnerInfos.Attach(tblrow);
                    DX.tblOrderSearchedLiveRunnerInfos.DeleteOnSubmit(tblrow);
                }
                DX.SubmitChanges();


                return tblRowList;
            }
        }
        public List<tblOrderSearchedLiveRunnerInfo> CreateOrderSearchedLiveRunnerInfo(List<tblOrderSearchedLiveRunnerInfo> tblRowList)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                foreach (var tblrow in tblRowList)
                {
                    tblOrderSearchedLiveRunnerInfo Objtbl = new tblOrderSearchedLiveRunnerInfo
                    {
                        pkOrderSearchedLiveRunnerId = 0,
                        fkOrderId = tblrow.fkOrderId,
                        LiveRunnerCounty = tblrow.LiveRunnerCounty,
                        LiveRunnerState = tblrow.LiveRunnerState,
                        SearchedCountyId = tblrow.SearchedCountyId
                    };
                    DX.tblOrderSearchedLiveRunnerInfos.InsertOnSubmit(Objtbl);
                    DX.SubmitChanges();
                }

                return tblRowList;
            }



        }



        public List<State_Name> GetStateList(List<Region_Name> RegionName, string SearchedStateName, string firstname_item, string lastname_item, string middlename_item)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                List<State_Name> ObjState_Name = new List<State_Name>();
                //List<tblState> tblStateCollection = new List<tblState>();
                var objList = (from u in DX.tblStates
                               select new State_Coll()
                               {
                                   StateCode = u.StateCode,
                                   StateName = u.StateName

                               }).ToList();
                foreach (var item in RegionName)
                {
                    var localItem = item;
                    var Coll = objList.Where(x => x.StateCode.Contains(localItem.Region)).FirstOrDefault();
                    ObjState_Name.Add(new State_Name() { StateName = Coll.StateName, fName = item.Fname.ToString(), lName = item.Lname.ToString(), mName = item.Mname.ToString() });
                }


                int check_item = 1;
                for (int i = 0; i < ObjState_Name.Count; i++)
                {
                    if (ObjState_Name.ElementAt(i).StateName.ToString() == SearchedStateName)
                    {
                        check_item = 2;
                    }
                }
                if (check_item != 2)
                {
                    ObjState_Name.Add(new State_Name() { StateName = SearchedStateName, fName = firstname_item, lName = lastname_item, mName = middlename_item });
                }

                return ObjState_Name;
            }
        }

        public List<JurisdictionColl> GetJurisdictionList(List<State_Name> State_Name, Guid pkProductIdfcr)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                List<JurisdictionColl> ObjColl = new List<JurisdictionColl>();
                var objList = (from u in DX.tblJurisdictions
                               join ut in DX.tblJurisdictionFees on u.pkJurisdictionId equals ut.fkJurisdictionId
                               select new JurisdictionName()
                               {
                                   pkJurisdictionId = u.pkJurisdictionId,
                                   Jurisdiction_Name = u.JurisdictionName,
                                   AdditionalFee = ut.AdditionalFee
                               }).ToList();
                foreach (var item in State_Name)
                {
                    var localItem = item;
                    var Coll = objList.Where(p => p.Jurisdiction_Name.Contains(localItem.StateName)).ToList();
                    if (Coll.Count > 0)
                    {
                        for (int l = 0; l < Coll.Count; l++)
                        {
                            ObjColl.Add(new JurisdictionColl() { Jurisdiction_Name = Coll.ElementAt(l).Jurisdiction_Name, pkJurisdictionId = Coll.ElementAt(l).pkJurisdictionId, AdditionalFee = Coll.ElementAt(l).AdditionalFee, fName = item.fName, lName = item.lName, mName = item.mName });
                        }
                    }
                }

                return ObjColl;
            }
        }


        public void UpdateOrderStatusFcr(int OrderId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                var Collection = DX.tblOrders.Where(p => p.pkOrderId == OrderId).FirstOrDefault();
                if (Collection != null)
                {

                    Collection.OrderStatus = 1;

                }
                DX.SubmitChanges();

            }
        }
        /// <summary>
        /// This method used to update the report status in database (Pass/Fail).
        /// </summary>
        /// <param name="IsRStatus"></param>
        /// <param name="IsApplied"></param>
        /// <param name="OrderId"></param>
        public void UpdateReportStatusInDatabase(bool IsRStatus, bool IsApplied, int OrderId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                var Collection = DX.tblOrders.Where(p => p.pkOrderId == OrderId).FirstOrDefault();
                if (Collection != null)
                {
                    //Update Pass or Fail Status.
                    Collection.IsPassReport = IsRStatus;
                    //Update Applied Status.
                    Collection.IsAppliedStatus = IsApplied;
                    DX.SubmitChanges();
                }
            }
        }
        /// <summary>
        /// To get Report data based on OrderId from Orders table.
        /// </summary>
        /// <param name="OrderId"></param>
        /// <returns></returns>
        public List<bool> GetReportStatusData(int OrderId)
        {
            List<bool> lst = new List<bool>();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                //To get order
                var Collection = DX.tblOrders.Where(p => p.pkOrderId == OrderId).FirstOrDefault();
                if (Collection != null)
                {
                    //Get Report Pass or Fail of any Report..
                    lst.Add(Collection.IsPassReport);
                    //Get Applied Status of any Report.
                    lst.Add(Collection.IsAppliedStatus);
                }
            }
            return lst;

        }
        /// <summary>
        /// To get the Report Status(Pass/Fail) based on logged on user email id. 
        /// </summary>
        /// <param name="emailid"></param>
        /// <returns></returns>
        public bool GetPassFailReportStatus(string emailid)
        {
            bool retStatus = false;
            string strCompanyName = string.Empty;
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                //To get the company name based on email id.
                strCompanyName = DX.GetCompanyNamebyUserId(emailid).Select(cmp => cmp.CompanyName).FirstOrDefault();
                //To get Report Status (Pass/Fail) based on selected company id.
                retStatus = DX.tblCompanies.Where(cmp => cmp.CompanyName.Equals(strCompanyName)).Select(cmp => cmp.IsReportStatus).FirstOrDefault();
            }
            return retStatus;
        }

        public Dictionary<string, bool> Get7YearFilterStatus(int company_id,int? pkOrderDetailId)//Find the status of Is7YearAutoCheck and Is7YearFilter
        {
            Dictionary<string, bool> ObjData = new Dictionary<string, bool>();
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                var orderDetail = DX.tblOrderDetails.Where(x => x.pkOrderDetailId == pkOrderDetailId).FirstOrDefault();
                if(orderDetail!=null)
                {
                    if (Convert.ToInt32(orderDetail.fkCompanyId) != company_id)
                    {
                        company_id = Convert.ToInt32(orderDetail.fkCompanyId);
                    }
                    
                    tblCompany tblc = (from u in DX.tblCompanies
                                       where u.pkCompanyId == company_id
                                       select u).FirstOrDefault();

                    ObjData.Add("Auto", tblc.Is7YearAutoCheck);
                    ObjData.Add("Manual", tblc.Is7YearFilter);
                }
                return ObjData;
            }
        }
    }

    public class CompanyList
    {
        public string CompanyName { get; set; }
        public int pkCompanyId { get; set; }
        public Guid? fkSalesRepId { get; set; }
    }

    public class Product_Code
    {
        public string ProductCode { get; set; }

    }


    public class LocationList
    {
        public string LocationCity { get; set; }
        public int pkLocationId { get; set; }
    }



    public class ScreencatgoriesList
    {
        public Guid pkthirdpartycredentialsId { get; set; }
        public string ProductCode { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public int fkCompanyId { get; set; }
        public int fkReportCategoryId { get; set; }
        public string CategoryName { get; set; }
    }



    public class FiltersCls
    {
        public string FilterName { get; set; }
        public string ReportType { get; set; }
        public string StateId { get; set; }
        public string Counties { get; set; }
        public string Keywords { get; set; }
        public int? Year { get; set; }
        public int? Count { get; set; }
        public int? CountMax { get; set; }
        public int? CountMin { get; set; }
        public string TimeFrameFrom { get; set; }
        public string TimeFrameIn { get; set; }
        public bool? IsEmergeReview { get; set; }
        public bool? IsWatchList { get; set; }
        public bool? IsRemoveCharges { get; set; }
        public int Users { get; set; }
        public bool Packages { get; set; }
        public bool Enabled { get; set; }
        public int fkFilterId { get; set; }
        public bool IsEnableForAllState { get; set; }
    }

    public class StateName
    {
        public string State_Name { get; set; }
        public int pkStateId { get; set; }
        public string StateCode { get; set; }
    }

    public class StateIds
    {
        public int State_Id { get; set; }
    }

    public class Filters_Class
    {
        public int FilterId { get; set; }
        public string StateDropdown { get; set; }
        public string CountyDropdown { get; set; }
        public string DataSourceDropdown { get; set; }
        public string FilterName { get; set; }
        public string ReportType { get; set; }
        public string Key_words { get; set; }
        public string Year { get; set; }
        public string Count { get; set; }
        public string CountMax { get; set; }
        public string CountMin { get; set; }
        public string TimeFrameFrom { get; set; }
        public string TimeFrameIn { get; set; }
        public string UserType { get; set; }
        public bool Packages { get; set; }
        public bool IsEmergeReview { get; set; }
        public bool IsWatchList { get; set; }
        public bool IsRemoveCharges { get; set; }
        public bool Enabled { get; set; }
        public string Filter_Users { get; set; }

    }

    public class State_Name
    {
        public string StateName { get; set; }
        public string fName { get; set; }
        public string mName { get; set; }
        public string lName { get; set; }
    }
    public class JurisdictionName
    {
        public string Jurisdiction_Name { get; set; }
        public int pkJurisdictionId { get; set; }
        public decimal AdditionalFee { get; set; }
    }
    public class JurisdictionColl
    {
        public string Jurisdiction_Name { get; set; }
        public int pkJurisdictionId { get; set; }
        public decimal AdditionalFee { get; set; }
        public string fName { get; set; }
        public string mName { get; set; }
        public string lName { get; set; }
    }

    public class JurisdictionListColl
    {
        public string Jurisdiction_Name { get; set; }
        public int pkJurisdictionId { get; set; }
        public decimal AdditionalFee { get; set; }
        public string fName { get; set; }
        public string mName { get; set; }
        public string lName { get; set; }
    }
    public class Region_Name
    {
        public string Region { get; set; }
        public string Fname { get; set; }
        public string Mname { get; set; }
        public string Lname { get; set; }

    }

    public class State_Coll
    {
        public string StateName { get; set; }
        public string StateCode { get; set; }
    }



}
