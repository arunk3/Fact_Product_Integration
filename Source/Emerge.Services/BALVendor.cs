﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;
using System.Data;
using System.Data.Common;
using System.Xml.Linq;

namespace Emerge.Services
{
    public class BALVendor
    {
        //EmergeDALDataContext ObjEmergeDALDataContext;

        //public List<Proc_GetVendorCredentialResult> GetVendorCredential(string ProductCode, byte VendorID)
        //{
        //    using (ObjEmergeDALDataContext = new EmergeDALDataContext())
        //    {
        //        return ObjEmergeDALDataContext.GetVendorCredential(ProductCode, VendorID).ToList<Proc_GetVendorCredentialResult>();
        //    }
        //}
        public List<tblVendor> GetVendorDetails()
        {
            using (EmergeDALDataContext ObjUSAIntelDataDataContext = new EmergeDALDataContext())
            {
                try
                {
                    List<tblVendor> VendordDetails = ObjUSAIntelDataDataContext.tblVendors.ToList();
                    if (VendordDetails.Count != 0)
                    {
                        return VendordDetails;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
        }

        public int UpdateVendorDetails(tblVendor _ObjClsVendor)
        {
            using (EmergeDALDataContext ObjUSAIntelDataDataContext = new EmergeDALDataContext())
            {
                try
                {
                    tblVendor ObjClsVendor = ObjUSAIntelDataDataContext.tblVendors.Where(data => data.pkVendorId == _ObjClsVendor.pkVendorId).FirstOrDefault();
                    ObjClsVendor.ServiceUserId = _ObjClsVendor.ServiceUserId;
                    ObjClsVendor.ServicePassword = _ObjClsVendor.ServicePassword;
                    ObjClsVendor.ServiceUrl = _ObjClsVendor.ServiceUrl;
                    ObjClsVendor.LastModifiedDate = _ObjClsVendor.LastModifiedDate;
                    ObjUSAIntelDataDataContext.SubmitChanges();
                    return 1;

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public int UpdateMvrPassword(string NewPassword)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    if (NewPassword != "")
                    {
                        var SoftechRecord = ObjEmergeDALDataContext.tblVendors.Single(data => data.pkVendorId == 5);

                        string OldPass = "";
                        var Node_OLD = SoftechRecord.AdditionalParams.Descendants("secondarypassword").First();
                        if (Node_OLD != null)
                        {
                            OldPass = Node_OLD.Value;
                        }

                        SoftechRecord.AdditionalParams = XElement.Parse(SoftechRecord.AdditionalParams.ToString().Replace(OldPass, NewPassword));

                        ObjEmergeDALDataContext.SubmitChanges();

                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                catch (Exception)
                {
                    return 0;
                }
            }
        }
        public class VendorReports
        {
            public int pkReportId { get; set; }
            public string ReportName { get; set; }
            public string ProductCode { get; set; }
        }
        public List<VendorReports> GetReportsByVendorId(int VendorId)
        {
            List<VendorReports> lst = new List<VendorReports>();
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    lst = (from data in ObjEmergeDALDataContext.tblProducts where data.fkVendorId == VendorId select new VendorReports { pkReportId = data.pkProductId, ReportName = data.ProductName, ProductCode = data.ProductCode }).ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return lst;
        }
        //public List<Proc_Get_VendorReportForAdminResult> GetVendorReport(Guid ApplicationId, int BillingMonth, int BillingYear, byte VendorId, Guid ProductId)
        //{
        //    using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
        //    {
        //        return ObjEmergeDALDataContext.Proc_Get_VendorReportForAdmin(ApplicationId, BillingMonth, BillingYear, VendorId, ProductId).ToList<Proc_Get_VendorReportForAdminResult>();
        //    }
        //}
        //public List<proc_GetDuplicateVendorRecordsResult> GetDuplicateVendorRecords(int BillingMonth, int BillingYear)
        //{
        //    using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
        //    {
        //        return ObjEmergeDALDataContext.proc_GetDuplicateVendorRecords(BillingMonth, BillingYear).ToList<proc_GetDuplicateVendorRecordsResult>();
        //    }
        //}
        public List<Proc_Get_EmergeReportsByVendorIdResult> GetEmergeReportsByVendorId(byte VendorId)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                return ObjEmergeDALDataContext.GetEmergeReportsByVendorId(VendorId).ToList();
            }
        }

        public int SaveProductVendorPrice(tblProductPerApplication ObjProductPerApplication)
        {
            int iResult = -1;
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    IQueryable<tblProductPerApplication> ObjProductPerApplicationData = ObjEmergeDALDataContext.tblProductPerApplications.Where(db => db.pkProductApplicationId == ObjProductPerApplication.pkProductApplicationId);
                    if (ObjProductPerApplicationData.Count() == 1)
                    {
                        tblProductPerApplication ObjUpdate = ObjProductPerApplicationData.First();
                        ObjUpdate.ProductVendorPrice = ObjProductPerApplication.ProductVendorPrice;
                        ObjEmergeDALDataContext.SubmitChanges();
                    }
                }
                catch
                {
                    iResult = -1;
                }
                return iResult; /* Here we return result */
            }
        }
    }
}
