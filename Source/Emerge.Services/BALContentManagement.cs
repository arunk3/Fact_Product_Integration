﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emerge.Data;

namespace Emerge.Services
{
    public class BALContentManagement
    {
        public List<tblFeature> GetAllFeatures()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.tblFeatures.ToList<tblFeature>();
            }
        }

        public int InsertReviewStatusLog(int OrderId, string Comment)
        {
            DateTime CreatedDate = DateTime.Now;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                ObjDALDataContext.InsertReviewStatusComments(OrderId, CreatedDate, Comment);
                return 1;
            }
        }

        public List<tblReviewStatusComment> GetReviewStatusComment(int OrderId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                var Collection = (from m in ObjDALDataContext.tblReviewStatusComments where m.OrderId == OrderId orderby Convert.ToDateTime(m.CreatedDate) descending select m).ToList();
                return Collection;
            }
        }

        public List<tblCustomReviewMessage> GetReviewCustomMsg()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.tblCustomReviewMessages.ToList<tblCustomReviewMessage>();
            }
        }



        


        public List<tblFeature> GetEnabledFeatures()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.tblFeatures.Where(d => d.IsEnabled == true).ToList<tblFeature>();
            }
        }

        public List<Proc_Get_CustomReviewMsgsListResult> GetCustomMessageList()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.Get_CustomReviewMsgsList().ToList();
            }
        }
        public List<Proc_GetContentsResult> GetContents(Guid ApplicationId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetContents(ApplicationId).ToList();
            }
        }
        public int AddContent(tblContentManagement ObjtblContentManagement, Guid SiteApplicationId)
        {
            int Result = 0;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    if ((ObjDALDataContext.tblContentManagements.Where(d => d.Title == ObjtblContentManagement.Title).Count()) == 0)
                    {
                        ObjDALDataContext.AddContent(SiteApplicationId,
                        ObjtblContentManagement.Title,
                        ObjtblContentManagement.Comment,
                        ObjtblContentManagement.IsEnabled,
                        ObjtblContentManagement.CreatedDate);
                        Result = 1;
                    }
                    else
                    {
                        Result = -1;
                    }
                }
                catch (Exception)
                {

                    Result = 0;
                }


            }
            return Result;
        }


        public Proc_GetUserImageAndCompanyNameResult GetCompanyNameByEmailId(string EmailId)
        {
           // string CompanyName = string.Empty;
            Proc_GetUserImageAndCompanyNameResult GetUserImageAndCompanyName = new Proc_GetUserImageAndCompanyNameResult();
            GetUserImageAndCompanyName = null;
            using (EmergeDALDataContext objdataContext = new EmergeDALDataContext())
            {
                GetUserImageAndCompanyName = objdataContext.GetUserImageAndCompanyName(EmailId).First();
            }

            return GetUserImageAndCompanyName;
        }

        public Proc_GetCompanyNamebyUserIdResult GetCompanyNameByUserId(string EmailId)
        {
            Proc_GetCompanyNamebyUserIdResult ObjGetCompanyNameByUserId = new Proc_GetCompanyNamebyUserIdResult();
            using (EmergeDALDataContext objdataContext = new EmergeDALDataContext())
            {
                ObjGetCompanyNameByUserId = objdataContext.GetCompanyNamebyUserId(EmailId).First();
            }
            return ObjGetCompanyNameByUserId;
        }

        public tblLoginPageSetting GetLogoImg()
        {
            //string ImagePath = string.Empty;

            tblLoginPageSetting record = new tblLoginPageSetting();
            using (EmergeDALDataContext ObjdataContext = new EmergeDALDataContext())
            {
                record = ObjdataContext.tblLoginPageSettings.Where(item => item.pkLoginSettingId == 1).FirstOrDefault();


            }
            return record;
        }
        public int UpdateLoginPagesContents(int PageId, string PageContents, string SelectedColor, bool IsEnable)
        {
            int Result = 0;
            using (EmergeDALDataContext objDalDataContext = new EmergeDALDataContext())
            {
                try
                {
                    //var ObjLoginPageManagement = 
                    objDalDataContext.UpdateloginPageContents(PageId, PageContents, SelectedColor, IsEnable);//.Where(db => db.pkPageId == PageId).FirstOrDefault();
                    Result = 1;
                }
                catch
                {

                }
                return Result;
            }
        }

        public int UpdatetblLoginPageSetting(string LoadingImage, string LoadingImgColor, string LoginBackImg, string LoginBackColor, string LoginLogoImg, string LoginBGPosition = "-1")
        {
            int result = 0;
            using (EmergeDALDataContext objbalText = new EmergeDALDataContext())
            {
                try
                {
                    objbalText.UpdatetblLoginPageSetting(LoginBackColor, LoginLogoImg, LoginBackImg, LoadingImgColor, LoadingImage, LoginBGPosition);

                }
                catch
                {
                }
            }
            return result;
        }

        public tblLoginPageManagement GetLoginPageContents(int PageId)
        {
            tblLoginPageManagement objtblLoginMgt = new tblLoginPageManagement();
            objtblLoginMgt = null;
            using (EmergeDALDataContext objDalDataContext = new EmergeDALDataContext())
            {
                try
                {
                    objtblLoginMgt = objDalDataContext.tblLoginPageManagements.Where(item => item.pkPageId == PageId).FirstOrDefault();

                }
                catch
                {
                }
            }
            return objtblLoginMgt;
        }



        public List<tblLoginPageManagement> Get_Login_Page_Content()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.tblLoginPageManagements.ToList<tblLoginPageManagement>();
            }
        }



        public int UpdateContent(tblContentManagement ObjtblContentManagement)
        {
            int Result = 0;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    if ((ObjDALDataContext.tblContentManagements.Where(d => d.Title == ObjtblContentManagement.Title && d.Id != ObjtblContentManagement.Id).Count()) == 0)
                    {
                        ObjDALDataContext.UpdateContent(ObjtblContentManagement.Id,
                        ObjtblContentManagement.Title,
                        ObjtblContentManagement.Comment,
                        ObjtblContentManagement.IsEnabled,
                        ObjtblContentManagement.LastModifiedDate);
                        Result = 1;
                    }
                    else
                    {
                        Result = -1;
                    }

                }
                catch (Exception)
                {

                    Result = 0;
                }


            }
            return Result;
        }
        public int DeleteContent(int Id, Guid ApplicationId)
        {
            int Result = 0;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    ObjDALDataContext.DeleteContent(Id, ApplicationId);
                    Result = 1;

                }
                catch (Exception)
                {
                    Result = 0;
                }
            }
            return Result;
        }
        //public List<Proc_GetContentByIdResult> GetContentsById(int Id)
        //{
        //    using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
        //    {
        //        return ObjDALDataContext.GetContentById(Id).ToList();
        //    }

        //}


        //public List<Proc_GetRandomContentsResult> GetRandomContents(Guid ApplicationId, int Limit)
        //{
        //    using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
        //    {
        //        return ObjDALDataContext.GetRandomContents(ApplicationId, Limit).ToList();
        //    }
        //}

        public List<tblContentManagement> GetRandomContentsAll(Guid ApplicationId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.tblContentManagements.Where(d => d.ApplicationId == ApplicationId && d.IsEnabled == true).ToList<tblContentManagement>();
            }
        }

        public int DeleteFeature(int Id)
        {
            int Result = 0;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    ObjDALDataContext.DeleteFeature(Id);
                    Result = 1;

                }
                catch (Exception)
                {
                    Result = 0;
                }
            }
            return Result;
        }

        public int DeleteReviewLog(int pkLogId)
        {
            int Result = 0;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    tblCustomReviewMessage ObjtblCustomReviewMesg = new tblCustomReviewMessage();
                    ObjtblCustomReviewMesg = ObjDALDataContext.tblCustomReviewMessages.Single(p => p.pkLogId == pkLogId);
                    ObjDALDataContext.tblCustomReviewMessages.DeleteOnSubmit(ObjtblCustomReviewMesg);
                    ObjDALDataContext.SubmitChanges();
                    Result = 1;

                }
                catch (Exception)
                {
                    Result = 0;
                }
            }
            return Result;
        }

       


        public int UpdateFeatureDetails(tblFeature ObjtblFeature)
        {
            int Result = 0;
            tblFeature ObjDoc = new tblFeature();
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    if ((ObjDALDataContext.tblFeatures.Where(d => d.Title == ObjtblFeature.Title && d.FeatureId != ObjtblFeature.FeatureId).Count()) == 0)
                    {
                        ObjDoc = ObjDALDataContext.tblFeatures.Where(d => d.FeatureId == ObjtblFeature.FeatureId).FirstOrDefault();
                        if (ObjDoc != null)
                        {
                            ObjDoc.Title = ObjtblFeature.Title;
                            ObjDoc.Description = ObjtblFeature.Description;
                            ObjDoc.UploadedFile = ObjtblFeature.UploadedFile;
                            ObjDoc.IsEnabled = ObjtblFeature.IsEnabled;
                            ObjDoc.UploadedTitleImage = ObjtblFeature.UploadedTitleImage;
                            ObjDoc.LastModifiedDate = ObjtblFeature.LastModifiedDate;
                            ObjDALDataContext.SubmitChanges();
                            Result = 1;
                        }
                    }
                    else
                    {
                        Result = -1;
                    }

                }
                catch (Exception)
                {

                    Result = 0;
                }
            }
            return Result;
        }

        public int UpdateCustomMessages(tblCustomReviewMessage ObjCustomReviewMessage)
        {
            int Result = 0;
            tblCustomReviewMessage ObjDoc = new tblCustomReviewMessage();
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    if ((ObjDALDataContext.tblCustomReviewMessages.Where(d => d.LogTitle == ObjCustomReviewMessage.LogTitle && d.pkLogId != ObjCustomReviewMessage.pkLogId).Count()) == 0)
                    {
                        ObjDoc = ObjDALDataContext.tblCustomReviewMessages.Where(d => d.pkLogId == ObjCustomReviewMessage.pkLogId).FirstOrDefault();
                        if (ObjDoc != null)
                        {
                            ObjDoc.LogTitle = ObjCustomReviewMessage.LogTitle;
                            ObjDoc.Descrption = ObjCustomReviewMessage.Descrption;

                            ObjDALDataContext.SubmitChanges();
                            Result = 1;
                        }
                    }
                    else
                    {
                        Result = -1;
                    }

                }
                catch (Exception)
                {

                    Result = 0;
                }
            }
            return Result;

        }


        





        public int InsertFeatureDetails(tblFeature ObjtblFeature)
        {
            int Result = 0;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    if ((ObjDALDataContext.tblFeatures.Where(d => d.Title == ObjtblFeature.Title).Count()) == 0)
                    {
                        tblFeature tbl = new tblFeature();
                        tbl.Title = ObjtblFeature.Title;
                        tbl.Description = ObjtblFeature.Description;
                        tbl.UploadedFile = ObjtblFeature.UploadedFile;
                        tbl.IsEnabled = ObjtblFeature.IsEnabled;
                        tbl.FeatureCreatedDate = ObjtblFeature.FeatureCreatedDate;
                        tbl.UploadedTitleImage = ObjtblFeature.UploadedTitleImage;


                        ObjDALDataContext.tblFeatures.InsertOnSubmit(tbl);
                        ObjDALDataContext.SubmitChanges();
                        Result = tbl.FeatureId;
                    }
                    else
                    {
                        Result = -1;
                    }
                }
                catch (Exception)
                {

                    Result = 0;
                }
            }
            return Result;
        }

        public int InsertReviewLogMsgs(tblCustomReviewMessage ObjtblCustomReviewMegs)
        {
            int Result = 0;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    if ((ObjDALDataContext.tblCustomReviewMessages.Where(d => d.LogTitle == ObjtblCustomReviewMegs.LogTitle).Count()) == 0)
                    {
                        tblCustomReviewMessage tbl = new tblCustomReviewMessage();
                        tbl.LogTitle = ObjtblCustomReviewMegs.LogTitle;
                        tbl.Descrption = ObjtblCustomReviewMegs.Descrption;


                        ObjDALDataContext.tblCustomReviewMessages.InsertOnSubmit(tbl);
                        ObjDALDataContext.SubmitChanges();
                        Result = tbl.pkLogId;
                    }
                    else
                    {
                        Result = -1;
                    }
                }
                catch (Exception)
                {

                    Result = 0;
                }
            }
            return Result;
        }


        public int InsertRepotComments(tblReportComment ObjtblReportComment)
        {
            int Result = 0;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    if ((ObjDALDataContext.tblReportComments.Where(d => d.ButtonTitle == ObjtblReportComment.ButtonTitle).Count()) == 0)
                    {
                        tblReportComment tbl = new tblReportComment();
                        tbl.ButtonTitle = ObjtblReportComment.ButtonTitle;
                        tbl.Comments = ObjtblReportComment.Comments;
                        tbl.fkLineId = ObjtblReportComment.fkLineId;
                        ObjDALDataContext.tblReportComments.InsertOnSubmit(tbl);
                        ObjDALDataContext.SubmitChanges();
                        Result = tbl.pkReportCommentsId;
                    }
                    else
                    {
                        Result = -1;
                    }
                }
                catch (Exception)
                {

                    Result = 0;
                }
            }
            return Result;
        }

        public int UpdateReportComments(tblReportComment ObjtblReportComment)
        {
            int Result = 0;
            tblReportComment ObjDoc = new tblReportComment();
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    ObjDoc = ObjDALDataContext.tblReportComments.Where(d => d.pkReportCommentsId == ObjtblReportComment.pkReportCommentsId).FirstOrDefault();
                    if (ObjDoc != null)
                    {
                        ObjDoc.ButtonTitle = ObjtblReportComment.ButtonTitle;
                        ObjDoc.Comments = ObjtblReportComment.Comments;
                        ObjDoc.fkLineId = ObjtblReportComment.fkLineId;
                        ObjDALDataContext.SubmitChanges();
                        Result = 1;
                    }

                    else
                    {
                        Result = -1;
                    }

                }
                catch (Exception)
                {

                    Result = 0;
                }
            }
            return Result;

        }

        public int DeleteReportComments(int pkReportCommentId)
        {
            int Result = 0;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    tblReportComment ObjtblReportComment = new tblReportComment();
                    ObjtblReportComment = ObjDALDataContext.tblReportComments.Single(p => p.pkReportCommentsId == pkReportCommentId);
                    ObjDALDataContext.tblReportComments.DeleteOnSubmit(ObjtblReportComment);
                    ObjDALDataContext.SubmitChanges();
                    Result = 1;

                }
                catch (Exception)
                {
                    Result = 0;
                }
            }
            return Result;
        }

       
        public List<tblLinesForReportComment> GetLineNoForReportCommentsFromDB()
        {
            List<tblLinesForReportComment> lst = new List<tblLinesForReportComment>();
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    lst = ObjDALDataContext.tblLinesForReportComments.ToList<tblLinesForReportComment>();
                }
                catch
                { }
            }
            return lst;
        }


        public List<Proc_GetLineReportCommentsResult> GetReportCommentsRows()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                return ObjDALDataContext.GetLineReportComments().ToList<Proc_GetLineReportCommentsResult>();
            }
        }

        public Dictionary<string, string> InsertLineNumberforReport(string lineName)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    var objLine = ObjDALDataContext.tblLinesForReportComments.Where(d => d.LineName.Equals(lineName)).FirstOrDefault();
                    if (objLine == null)
                    {
                        tblLinesForReportComment objNewLine = new tblLinesForReportComment();
                        objNewLine.LineName = lineName;
                        objNewLine.IsVisible = true;
                        ObjDALDataContext.tblLinesForReportComments.InsertOnSubmit(objNewLine);
                        ObjDALDataContext.SubmitChanges();
                        dic.Add("result", "Inserted");
                        dic.Add("id", objNewLine.pkLineId.ToString());
                        dic.Add("LineName", objNewLine.LineName);
                        
                    }
                    else
                    {
                        dic.Add("result", "Aleary Exists");

                    }
                }
                catch(Exception)
                {
                    dic.Add("result", "Error");
                }
            }
            return dic;
        }






        //public tblFeature GetFeatureById(int FeatureId)
        //{
        //    tblFeature objtblFeature = new tblFeature();
        //    try
        //    {
        //        using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
        //        {
        //            objtblFeature = ObjDALDataContext.tblFeatures.Where(d => d.FeatureId == FeatureId).FirstOrDefault();

        //        }
        //    }
        //    catch
        //    {

        //    }
        //    return objtblFeature;
        //}

        public tblFeature GetFeatureListById(int FeatureId)
        {
            tblFeature objtblFeature = new tblFeature();
            try
            {
                using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
                {
                    objtblFeature = ObjDALDataContext.tblFeatures.Where(d => d.FeatureId == FeatureId).FirstOrDefault();

                }
            }
            catch
            {

            }
            return objtblFeature;
        }
        public List<tblFeature> GetAllFeaturesEnabled()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {

                //tblFeature ObjtblFeature = new tblFeature();
                var list = (from t in ObjDALDataContext.tblFeatures
                            where t.IsEnabled == true
                            orderby t.FeatureCreatedDate descending
                            select t).ToList();


                return list;
            }
        }

        public List<Proc_GetFeaturesForUserResult> GetFeaturesForUser(string SortingColumn, string SortingDirection,
                                                        bool AllowPaging, int PageNum, int PageSize, DateTime LastActivityDate)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {

                return ObjDALDataContext.GetFeaturesForUser(SortingColumn, SortingDirection,
                                                         AllowPaging, PageNum, PageSize, LastActivityDate).ToList();
            }
        }

        public int UpdateFeature(int Id, int ViewCount)
        {
            int Status = 0;
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {
                    tblFeature ObjFeature = ObjDALDataContext.tblFeatures.Where(db => db.FeatureId == Id).FirstOrDefault();
                    if (ObjFeature != null)
                    {
                        ObjFeature.ViewCount = ViewCount;
                        ObjDALDataContext.SubmitChanges();
                        Status = 1;
                    }
                }
                catch
                {

                }
            }
            return Status;
        }

        public List<ContactInfo> GetContactInfo()
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                Guid CompanyId = new Guid("7DC87D77-6BB5-4BB3-9837-FA861F36F281");
                List<ContactInfo> ObjData = new List<ContactInfo>();
                ObjData = (from u in DX.tblGeneralSettings
                           join us in DX.tblStates on u.State equals us.pkStateId
                           select new ContactInfo
                           {
                               CntctAddress1 = u.Address,
                               CntctAddress2 = u.Address2,
                               CntctCity = u.City,
                               CntctStateCode = us.StateCode,
                               CntctZipCode = u.ZipCode,
                               CntctPhoneNo1 = u.PhoneNo,
                               CntctFax = u.Fax,
                               CntctEmail = u.SupportEmailId
                           }).ToList();
                //ObjData = (from u in DX.tblLocations
                //           join us in DX.tblStates on u.fkStateID equals us.pkStateId
                //           join ct in DX.tblCompanies on u.fkCompanyId equals ct.pkCompanyId
                //           where ct.pkCompanyId == CompanyId && u.IsHome == true
                //           select new ContactInfo
                //           {
                //               CntctAddress1 = u.Address1,
                //               CntctAddress2 = u.Address2,
                //               CntctCity = u.City,
                //               CntctStateCode = us.StateCode,
                //               CntctZipCode = u.ZipCode,
                //               CntctPhoneNo1 = u.PhoneNo1,
                //               CntctFax = u.Fax,
                //               CntctEmail=ct.MainEmailId
                //           }).ToList();
                return ObjData.ToList();




            }
        }


    }
    public struct clsManageContent
    {
        public int Id { get; set; }
        public Guid ApplicationId { get; set; }
        public string Title { get; set; }
        public string Comment { get; set; }
        public bool IsEnabled { get; set; }
    }
    public struct clsManageFeatures
    {
        public int FeatureId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string UploadedFile { get; set; }
        public bool IsEnabled { get; set; }
        public DateTime FeatureCreatedDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public int ViewCount { get; set; }


    }
    public class ContactInfo
    {

        public string CntctAddress1 { get; set; }
        public string CntctAddress2 { get; set; }
        public string CntctCity { get; set; }
        public string CntctStateCode { get; set; }
        public string CntctZipCode { get; set; }
        public string CntctPhoneNo1 { get; set; }
        public string CntctFax { get; set; }
        public string CntctEmail { get; set; }



    }



}
