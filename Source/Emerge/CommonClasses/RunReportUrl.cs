﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using Emerge.Data;
using System.Web.Mvc;
using System.Web.Http;

namespace Emerge.CommonClasses
{
    public static class RunReportUrl
    {

        public static void UpdateRouteRegistration()
        {
            RouteCollection routes = RouteTable.Routes;
            using (routes.GetWriteLock())
            {
                routes.Clear();
                routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
                routes.IgnoreRoute("{*allaxd}", new { allaxd = @".*\.axd(/.*)?" });
                routes.MapRoute(
                   "RunReport", // Route name
                   "RunReport",// URL with parameters
                   new { controller = "RunReport", action = "Default_RunReport", id = UrlParameter.Optional }
                );
                RegisterCustomRoutes(routes);
                routes.MapRoute(
                    "Office", // Route name
                    "Office/{action}/{id}", // URL with parameters
                    new { controller = "Office", action = "Default", id = UrlParameter.Optional } // Parameter defaults
                );
                routes.MapHttpRoute(
                    name: "DefaultApi",// Route name
                    routeTemplate: "api/{controller}/{id}",// URL with parameters
                    defaults: new { id = RouteParameter.Optional }
                );
                routes.MapRoute(
                    name: "Default",// Route name
                    url: "{controller}/{action}/{id}",// URL with parameters
                    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
                );
            }
        }

        public static void RegisterCustomRoutes(RouteCollection routes)
        {
            using (EmergeDALDataContext db = new EmergeDALDataContext())
            {
               // var listOfStrings = new List<string>();
               // List<string> lst = new List<string>();
                var qry = from company in db.tblCompanies
                          where company.EnableAPI == true
                          group company by new { company.pkCompanyId, company.ApiUrl } into grp
                          select new { grp.Key.pkCompanyId, grp.Key.ApiUrl };
                if (qry != null)
                {
                    List<string> Companyname = qry.Select(x => x.ApiUrl).Distinct().ToList();
                    for (int i = 0; i < Companyname.Count; i++)
                    {
                        string CompanyName = string.Empty;
                        CompanyName = Companyname.ElementAt(i);
                        if (string.IsNullOrEmpty(CompanyName) == false)
                        {
                            routes.MapRoute(
                            name: CompanyName,
                            url: CompanyName,
                            defaults: new { Controller = "RunReport", Action = "Index", id = UrlParameter.Optional });
                        }
                    }
                }
            }
        }

    }
}