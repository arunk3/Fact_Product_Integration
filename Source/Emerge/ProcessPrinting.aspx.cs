﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Emerge.Data;
using Emerge.Services;
using System.Xml;
using System.Text;
using System.IO;
using System.Net.Mail;
using System.Configuration;
using System.Web.UI.HtmlControls;
using System.Web;
using System.Xml.Linq;
using iTextSharp.text;
using System.Web.Security;
using Ionic.Zip;
using Emerge.Common;
using System.Xml.Xsl;
using SelectPdf;
using Emerge.Models;





namespace Emerge
{
    public partial class ProcessPrinting : System.Web.UI.Page
    {
        BALOrders ObjBALOrders;
        int MicroBiltEndPoint = 1;
        int vwpsStatus = 0;
        string PSResponse = string.Empty;
        int ObjOrderID = 0;
        string FileName = "";
        int MVR_Counter = 0;
        public Guid GlobalMemberUserId { get; set; }
        string CurrentUserCompanyName = string.Empty;
        string UserEMailId = "";
        string qrystr = "";
        string[] Querystr = { "" };
        //StringWriter stw;
        //HtmlTextWriter htextw;

        //PlaceHolder phAllrecords = new PlaceHolder();
        protected void Page_Load(object sender, EventArgs e)
        {
            string fileNamePDF = string.Empty;
            string orderStatusValue = string.Empty;
            LoadCss();
            Page.Items.Add("GlobalMemberUserId", GlobalMemberUserId);
            if (Session["CurrentUserMailId"] != null)
                UserEMailId = Convert.ToString(Session["CurrentUserMailId"]);

            GlobalMemberUserId = new Guid(Page.Items["GlobalMemberUserId"].ToString());
            Label ObjLabel = new Label();
            Response.ContentEncoding = Encoding.Default;
            if (!string.IsNullOrEmpty(Request.QueryString["_O"]))        //  _O here refers to OrderID (GUID)
            {
                qrystr = Request.QueryString["_O"].ToString();
                Querystr = qrystr.Split(',');
            }

            if (!string.IsNullOrEmpty(Request.QueryString["OrderStatus"]))        //  IE FIX ISSUE 
            {
                orderStatusValue = Request.QueryString["OrderStatus"].ToString();

            }
            int pageloadTime = 60;
            //Control ObjControl_eachData = new Control();
            string ReportType = string.Empty;
            List<string> objhtmlstr = new List<string>();
            for (int i = 0; i < Querystr.Count(); i++)
            {
                //string OrderID = Querystr[i];
                ObjOrderID = Convert.ToInt32(Querystr[i].ToString());
                MicroBiltEndPoint = 1;
                vwpsStatus = 0;
                PSResponse = string.Empty;
                if (ObjOrderID != 0)
                {
                    ViewState["vwPkOrderId"] = ObjOrderID;
                    string htmfl = "";
                    int ObjOrderIDtest = ObjOrderID;
                    //tblOrder ReportTypes1 = new tblOrder();
                    BALOrders ObjBALOrders1 = new BALOrders();
                    var ReportTypestest1 = ObjBALOrders1.GetDrugtestbypkid(ObjOrderIDtest);
                    if (ReportTypestest1 != null)
                    {
                        if (ReportTypestest1.IsDrugTestingOrder == true)
                        {
                            htmfl = BindDrugsReports(ObjOrderID);

                            ReportType = "DrugTest";
                        }
                        else
                        {

                            string url = Request.QueryString.ToString();
                            if (url.Contains("SavedReportPrint"))       //  _O here refers to OrderID (GUID)
                            {
                                htmfl = BindReports_forSavedReportPage(ObjOrderID);
                                
                            }
                            else
                            {
                                htmfl = BindReports(ObjOrderID, orderStatusValue);
                                pageloadTime = 120;
                            }
                        }
                    }
                    else
                    {
                        htmfl = BindReports(ObjOrderID, orderStatusValue);
                        pageloadTime = 120;
                    }



                    FileName += (ViewState["ReportName"] != null) ? ViewState["ReportName"].ToString() + "," : string.Empty;
                   

                    objhtmlstr.Add(htmfl);

                    string htmlString = htmfl;

                   


                    
                    

                    // #region HTML to PDF Conversion
                    try
                    {
                       
                        string pdf_page_size = "A4";
                        PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
                            pdf_page_size, true);

                        string pdf_orientation = "Portrait";
                        PdfPageOrientation pdfOrientation =
                            (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
                            pdf_orientation, true);


                        int webPageWidth = 1024;
                        try
                        {
                            webPageWidth = Convert.ToInt32("1024");
                        }
                        catch { }

                        int webPageHeight = 0;
                        try
                        {
                            webPageHeight = Convert.ToInt32(0);
                        }
                        catch { }

                        // instantiate a html to pdf converter object
                        HtmlToPdf converter = new HtmlToPdf();
                        SelectPdf.GlobalProperties.LicenseKey = Convert.ToString(ConfigurationManager.AppSettings["HTMLToPDFConversion"]);

                        // set converter options
                        // set the page timeout (in seconds)
                        converter.Options.MaxPageLoadTime = pageloadTime;
                        converter.Options.PdfPageSize = pageSize;
                        converter.Options.PdfPageOrientation = pdfOrientation;
                        converter.Options.WebPageWidth = webPageWidth;
                        converter.Options.WebPageHeight = webPageHeight;
                        
                        


                        // create a new pdf document converting an url
                        SelectPdf.PdfDocument doc = converter.ConvertHtmlString(htmlString);
                        //To create file name based on Order No.
                        fileNamePDF = GetOrderNo(ObjOrderID) + ".pdf";
                        // save pdf document

                        //WriteLog(htmfl, fileNamePDF);
                        doc.Save(Response, false, fileNamePDF);
                        // close pdf document
                        doc.Close();
                        

                        /*
                        MemoryStream file = new MemoryStream(PdfSharpConvert(htmlString));
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("content-disposition", "attachment;filename=Print.pdf");
                        Response.Buffer = true;
                        file.WriteTo(Response.OutputStream);
                        Response.End();
                         */

                    }
                    catch (Exception ex)
                    {
                        DivError.Style.Add("display", "block");
                       /* WriteLog("**************************", "Error");
                        WriteLog(ex.Message.ToString(), "Error");
                        WriteLog("**************************", "Error");
                        WriteLog(ex.StackTrace.ToString(), "Error");
                        WriteLog("**************************", "Error");
                         */

                    }


                    // #endregion
                }
                else
                {
                    //Label ObjLabel1 = new Label();
                    ObjLabel.Text = "No Record Found";
                    //ObjLabel.ID = "lbl_Error";
                    ObjLabel.Font.Bold = true;
                    ObjLabel.Style.Add("font-size", "20px");
                    ObjLabel.Style.Add("text-align", "left");
                    ObjLabel.Style.Add("color", "#ff0000");
                }
                if (Session["CurrentUserCompanyName"] != null)
                {
                    CurrentUserCompanyName = Session["CurrentUserCompanyName"].ToString();
                }
            }
            Guid UserId;
            MembershipUser ObjMembershipUser = Membership.GetUser(Page.User.Identity.Name);
            if (ObjMembershipUser != null)
            {
                UserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());

                // WriteToFile(objhtmlstr, UserId, ReportType);
            }

            if (!string.IsNullOrEmpty(Request.QueryString["EM"]))
            {
                string emailto = Request.QueryString["EM"].ToString();
                string cc = Request.QueryString["cc"].ToString();
                btnSendMail(emailto, cc);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["SP"]))
            {
                #region Old code
                string option = Request.QueryString["SP"].ToString();
                if (option == "SV")
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["FN"]))
                    {
                        ViewState["FileName"] = Request.QueryString["FN"].ToString();
                    }
                    SaveZipFile();
                    btnSaveReport();
                }
                if (option.ToUpper() == "PT")
                {
                    //Printfiles();
                    ScriptManager.RegisterStartupScript(this.Page, GetType(), "12", "javascript:window.print();", true);
                    ScriptManager.RegisterStartupScript(this.Page, GetType(), "123", "javascript:window.close();", true);
                }
                #endregion
            }
        }

        private void WriteToFile(List<string> strlst, Guid UserId, string ReportType)
        {
            /* try
             {
                 string filename = "";
                 StreamWriter SW;
                 string savedPath = string.Empty;
                 foreach (string str in strlst)
                 {
                     if (str != "")
                     {
                         string[] stringSeparators = new string[] { "@%##%@" };
                         string[] htmlArr = str.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                         if (htmlArr.Length > 0)
                         {
                             if (ReportType == "DrugTest")
                             {
                                 filename = "DrugTest";
                             }
                             else
                             {
                                 filename = htmlArr[1];
                             }
                             savedPath = HttpContext.Current.Server.MapPath("~/Resources/Upload/Temp/" + UserId);
                             if (!Directory.Exists(savedPath))
                             {
                                 DirectoryInfo dir = Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Resources/Upload/Temp/" + UserId));
                             }
                             savedPath = savedPath + "/" + filename + ".htm";
                             SW = File.CreateText(savedPath);
                             SW.WriteLine("\n");
                             SW.WriteLine(htmlArr[0]);
                             SW.Dispose();
                             SW.Close();
                         }
                     }

                 }
                 ViewState["vwDirName"] = HttpContext.Current.Server.MapPath("~/Resources/Upload/Temp/" + UserId);
             }
             catch (Exception ex)
             {

                 throw ex;
             }
              */
        }

        private void LoadCss()
        {
            HtmlLink ObjHtmlLink_Fourth = new HtmlLink();
            ObjHtmlLink_Fourth.Href = "~/Design2/Css/custom.css";
            ObjHtmlLink_Fourth.Attributes.Add("rel", "stylesheet");
            ObjHtmlLink_Fourth.Attributes.Add("type", "text/css");
            ObjHtmlLink_Fourth.Attributes.Add("media", "print");
            Page.Header.Controls.Add(ObjHtmlLink_Fourth);

            HtmlLink ObjHtmlLink_Third = new HtmlLink();
            ObjHtmlLink_Third.Href = "~/Resources/Css/Report_Printing_Style.css";
            ObjHtmlLink_Third.Attributes.Add("rel", "stylesheet");
            ObjHtmlLink_Third.Attributes.Add("type", "text/css");
            ObjHtmlLink_Third.Attributes.Add("media", "print");
            Page.Header.Controls.Add(ObjHtmlLink_Third);

        }



        public string GenerateApplicantName(string sLastname, string sSufix, string sMiddle, string sFirstName)
        {
            string applicantName = string.Empty;
            int getstr = 0, ilen = 0;
            applicantName = (sLastname != "") ? sLastname + ", " : "";
            applicantName += (sSufix != "") ? sSufix + ", " : "";
            applicantName += (sMiddle != "") ? sMiddle + ", " : "";
            applicantName += (sFirstName != "") ? sFirstName + ", " : "";

            ilen = applicantName.LastIndexOf(",");
            getstr = ilen + 2;
            if (getstr == applicantName.Length)
            {
                applicantName = applicantName.Substring(0, ilen);
            }
            return applicantName;
        }

        /// <summary>
        /// Function To Bind The XML Reports
        /// </summary>
        /// 

        private string BindReports(int ObjOrderId, string orderStatusValue)
        {
            string htmlfile = "";
            string ProductCode = "";
            string TopScript = "";
            string topHeader = "";
            string bottomFooter = "";
            string horzLine = "";
            string topMenu = "";
            string BottomScript = "";
            string EmergeAssistScript = "";
            string DisclaimerScript = "";
            string ReportFor = "";
            bool IsShowRawData = true;
            //lblReportType.Text = "";
            //int ReviewQueryString = -1;
            ObjBALOrders = new BALOrders();
            int IsAutoReviewSent = 0;
            BALEmergeReview ObjBALEmergeReview = new BALEmergeReview();
            Dictionary<string, string> dictCodepkOrderResponseId = new Dictionary<string, string>();
            string DisplyProductCode = "";
            //Control hdnReviewDetail = new Control();

            try
            {
                List<Proc_Get_RequestAndResponseResult> ObjProc_Get_RequestAndResponseResult = ObjBALOrders.GetRequestAndResponseByOrderId(ObjOrderId);
                Control ObjControl_eachReport = new Control();
                string SearchedName = string.Empty;
                SearchedName = GenerateApplicantName(ObjProc_Get_RequestAndResponseResult.First().SearchedLastName, ObjProc_Get_RequestAndResponseResult.First().SearchedSuffix, ObjProc_Get_RequestAndResponseResult.First().SearchedMiddleInitial, ObjProc_Get_RequestAndResponseResult.First().SearchedFirstName);
                string FullSearchedName = SearchedName.Replace("'", "");
                //string FileName = ObjProc_Get_RequestAndResponseResult.First().OrderNo.ToString();
                if (ObjProc_Get_RequestAndResponseResult.Count > 0)
                {
                    ViewState["ReportName"] = ObjProc_Get_RequestAndResponseResult.First().SearchedLastName + "_" + ObjProc_Get_RequestAndResponseResult.First().SearchedFirstName;
                }
                else
                {
                    ViewState["ReportName"] = string.Empty;
                }

                BALCompany ObjBALCompany = new BALCompany();

                List<Proc_Get_CompanyDetailByMemberShipIdResult> ObjCompanyDetails = ObjBALCompany.GetCompanyDetailByMemberShipIdResult(ObjProc_Get_RequestAndResponseResult.First().fkUserId);
                if (Roles.IsUserInRole(Page.User.Identity.Name, "SystemAdmin") || Roles.IsUserInRole(Page.User.Identity.Name, "SupportTechnician") || Roles.IsUserInRole(Page.User.Identity.Name, "SupportManager"))
                {
                    IsShowRawData = true;
                }
                else
                {
                    IsShowRawData = false;
                }

                int nameDiff = 0;
                int CountReviewReports = 0;

                //  #region Reports Binding
                Control ObjControl_each = new Control();
                foreach (Proc_Get_RequestAndResponseResult ObjRequestAndResponseResult in ObjProc_Get_RequestAndResponseResult)
                {
                    ProductCode = ObjRequestAndResponseResult.ProductCode;
                    if (ObjRequestAndResponseResult.IsLiveRunner == true)
                    {
                        DisplyProductCode += ObjRequestAndResponseResult.ProductCode + "*" + ",";
                    }
                    else
                        DisplyProductCode += ObjRequestAndResponseResult.ProductCode + ",";
                    Proc_GetStateCountyPerCCROrderResult ObjStateCounty = ObjBALOrders.GetStateCountyPerOrderDetail(ObjRequestAndResponseResult.pkOrderDetailId);
                    if ((ObjRequestAndResponseResult.fkUserId == GlobalMemberUserId) && (ObjRequestAndResponseResult.IsViewed == false) && (ObjRequestAndResponseResult.ReportStatus == 2))
                    {
                        BALGeneral objGeneral = new BALGeneral();
                        objGeneral.UpdateOrderDetail_IsViewed(ObjRequestAndResponseResult.pkOrderDetailId);
                    }
                    string LiveRunnerImagePath = "";
                    if (Request.Url.ToString().Contains("Control"))
                    {
                        LiveRunnerImagePath = "https://emerge.intelifi.com/" + "Content/themes/base/Images/LiveRunnerSmall.png";

                    }
                    else
                    {

                        LiveRunnerImagePath = "https://emerge.intelifi.com/" + "Content/themes/base/Images/LiveRunnerSmall.png";

                    }
                    if (ProductCode == "SNS")
                    {
                        string PathProvider = "";
                        if (Request.Url.ToString().Contains("Control"))
                        {
                            PathProvider = "../../";

                        }
                        else
                        {
                            PathProvider = "../";
                        }

                        HtmlLink ObjHtmlLink_sdstyle = new HtmlLink();

                        ObjHtmlLink_sdstyle.Href = "~/Resources/Css/sdstyle.css";
                        ObjHtmlLink_sdstyle.Attributes.Add("rel", "stylesheet");
                        ObjHtmlLink_sdstyle.Attributes.Add("type", "text/css");

                        Page.Header.Controls.Add(ObjHtmlLink_sdstyle);

                        Page.ClientScript.RegisterClientScriptInclude("progress_bar", PathProvider + "Scripts/SNS/progress_bar.js");
                        Page.ClientScript.RegisterClientScriptInclude("whozat", PathProvider + "Scripts/SNS/whozat.js");
                        Page.ClientScript.RegisterClientScriptInclude("refine", PathProvider + "Scripts/SNS/refine.js");
                        Page.ClientScript.RegisterClientScriptInclude("Js", PathProvider + "Scripts/SNS/Js.js");
                        Page.ClientScript.RegisterClientScriptInclude("jquery.tooltip.pack", PathProvider + "Scripts/SNS/jquery.tooltip.pack.js");
                        Page.ClientScript.RegisterClientScriptInclude("eye", PathProvider + "Scripts/SNS/eye.js");
                        Page.ClientScript.RegisterClientScriptInclude("utils", PathProvider + "Scripts/SNS/utils.js");
                        Page.ClientScript.RegisterClientScriptInclude("zoomimage", PathProvider + "Scripts/SNS/zoomimage.js");

                        Session["ses_sod_order_id"] = ObjRequestAndResponseResult.pkOrderResponseId.ToString();




                    }

                    string IsLiveRunnerProvided = "";
                    if (ObjRequestAndResponseResult.IsLiveRunner == true)
                        IsLiveRunnerProvided = "<table><tr><td >Provided By</td><td><img src='" + LiveRunnerImagePath + "' alt='Live Runner' height='13' style='border:0px'/></td></tr></table>";
                    else
                        IsLiveRunnerProvided = "";
                    string StateCode = "";
                    string CountyName = "";
                    string StateCounty = "";
                    if (ObjStateCounty != null)
                    {
                        StateCode = ObjStateCounty.StateCode;
                        CountyName = ObjStateCounty.LiveRunnerCounty;
                        StateCounty = "[" + CountyName + ", " + StateCode + "]";
                    }
                    string SCRState = "";
                    if (ProductCode.ToLower() == "scr")
                    {
                        SCRState = "[" + ObjRequestAndResponseResult.StateName + "]";
                    }

                    if (ObjRequestAndResponseResult.IsLiveRunner == true)
                    {
                        if (ObjRequestAndResponseResult.ProductCode == "SCR" || ObjRequestAndResponseResult.ProductCode == "CCR1")
                        {
                            if (ObjRequestAndResponseResult.ProductDisplayName == "Manual State Criminal")
                            {
                                ObjRequestAndResponseResult.ProductDisplayName = "LiveRunner State Criminal Report";

                            }

                            if (ObjRequestAndResponseResult.ProductDisplayName == "Manual County Search")
                            {
                                ObjRequestAndResponseResult.ProductDisplayName = "LiveRunner County Search";

                            }
                        }
                    }

                    TopScript = "<div style=\"border:0px !important;\" class=\"main-box\"><div onclick=\"collapse('" + ObjRequestAndResponseResult.ProductCode + "_" + nameDiff.ToString() + "');\">"
                           + "<table class=\"rv-header\"  width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;page-break-inside:avoid;\">"
                           + "<tr><td style=\"background-repeat: no-repeat; background-image: url('https://emerge.intelifi.com/content/themes/base/images/" + ObjRequestAndResponseResult.ProductDisplayName + ".png');  background-position: 0px;cursor: pointer;width: 68px !important;height: 65px !important;background-size: 100% !important;color: transparent;  \"  width=\"39px\" height=\"34px\"></td><td class=\"rv-header\" width=\"95%\">"
                           + "<table style='font-weight:bold; width:100%'><tbody><tr><td style=\"vertical-align: middle; width:50%\" align='left'>" + ObjRequestAndResponseResult.ProductDisplayName + " " + SCRState + StateCounty + "</td>"
                           + "<td style=\"vertical-align: middle; width:50%\" align='right'><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>" + IsLiveRunnerProvided + "</td><td></td></tr> </table> </td></tr></tbody></table>"
                           + "</td><td class=\"rv-header\" width=\"9px\"></td></tr></table></div><div id=\"" + ObjRequestAndResponseResult.ProductCode + "_" + nameDiff.ToString() + "_b\" style=\"display: block; margin-left: 12px;\" class=\"pad-bot10\">"
                           + "<table  width=\"98%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td class=\"middle_left\" width=\"14px\"></td><td  width=\"100%\">";




                    if (ProductCode.ToLower() == "scr")//|| ProductCode.ToLower() ==""
                    {
                        string SearchedName1 = ObjRequestAndResponseResult.SearchedLastName + ", " + ObjRequestAndResponseResult.SearchedFirstName;
                        string FullSearchedName1 = SearchedName1.Replace("'", "");
                        TopScript += "<div style='font-size:13px; font-family:arial'><b>State Searched :</b> " + ObjRequestAndResponseResult.StateName + "</div>";
                        TopScript += "<div style='font-size:13px; font-family:arial'><b>Name :</b> " + FullSearchedName1 + "</div>";
                    }
                    // ObjControl_each.Controls.AddAt(0, CreateLabelControl(TopScript));
                    ObjControl_each.Controls.Add(CreateLabelControl(TopScript));


                    if (ObjRequestAndResponseResult.ReportStatus.ToString() == "3")
                    {
                        //ObjControl_each.Controls.AddAt(1, DisplayInCompleteMessage());
                        ObjControl_each.Controls.Add(DisplayInCompleteMessage());
                    }
                    else if (ObjRequestAndResponseResult.ReportStatus.ToString() == "2" && (ObjRequestAndResponseResult.ManualResponse != string.Empty || ObjRequestAndResponseResult.ResponseFileName != string.Empty))
                    {
                        // ObjControl_each.Controls.AddAt(1, DisplayManualResponse(ObjRequestAndResponseResult));
                        ObjControl_each.Controls.Add(DisplayManualResponse(ObjRequestAndResponseResult));
                    }
                    else if (ObjRequestAndResponseResult.ReportStatus.ToString() == "2" && ObjRequestAndResponseResult.ReviewStatus.ToString() == "1" && IsShowRawData == false)
                    {
                        // ObjControl_each.Controls.AddAt(1, DisplayPendingMessage());
                        ObjControl_each.Controls.Add(DisplayPendingMessage());
                    }
                    else
                    {
                        if (ProductCode == "MVR1")
                        {
                            int MVR_ReportStatus = ObjRequestAndResponseResult.ReportStatus;
                            if (MVR_ReportStatus != 2)
                            {
                                List<Proc_Get_RequestAndResponseResult> ObjProc_Get_RequestAndResponseResult_MVR = null;
                                while (MVR_ReportStatus != 2 && MVR_Counter < 4)
                                {
                                    MVR_Counter++;
                                    System.Threading.Thread.Sleep(4000);
                                    ObjProc_Get_RequestAndResponseResult_MVR = ObjBALOrders.GetRequestAndResponseByOrderId(ObjOrderId).Where(data => data.ProductCode.ToLower() == "mvr").ToList<Proc_Get_RequestAndResponseResult>();
                                    MVR_ReportStatus = ObjProc_Get_RequestAndResponseResult_MVR.Where(data => data.ProductCode.ToLower() == "mvr").FirstOrDefault().ReportStatus;
                                }
                                //ObjControl_each.Controls.AddAt(1, GetMvrResponse(ObjProc_Get_RequestAndResponseResult_MVR.Where(data => data.ProductCode.ToLower() == "mvr").FirstOrDefault()));
                                ObjControl_each.Controls.Add(GetMvrResponse(ObjProc_Get_RequestAndResponseResult_MVR.Where(data => data.ProductCode.ToLower() == "mvr").FirstOrDefault()));
                            }
                            else
                            {
                                /*ObjControl_each.Controls.AddAt(1, GetMvrResponse(ObjRequestAndResponseResult));*/
                                if (ObjRequestAndResponseResult.ResponseData.Contains("INVALID ACCOUNT NAME AND PASSWORD") || ObjRequestAndResponseResult.ResponseData.Contains("<request_status>1</request_status>") || ObjRequestAndResponseResult.ResponseData == "")
                                {
                                    TopScript += "Your order was completed manually through Intelifi support department. Please contact support for any further assistance.";
                                }
                                else
                                {
                                    Label lblMvrResponse = new Label();
                                    lblMvrResponse.Text = ObjRequestAndResponseResult.ResponseData;
                                    //ObjControl_each.Controls.AddAt(1, lblMvrResponse);
                                    ObjControl_each.Controls.Add(lblMvrResponse);

                                }
                            }
                        }
                        else
                        {
                            //ObjControl_each.Controls.AddAt(1, FetchResponse(ObjRequestAndResponseResult));


                            if (orderStatusValue.Contains("Review"))
                            {
                                ObjControl_each.Controls.Add(FetchResponse(ObjRequestAndResponseResult, true, orderStatusValue));

                            }
                            else
                            {
                                ObjControl_each.Controls.Add(FetchResponse(ObjRequestAndResponseResult, false, orderStatusValue));
                            }


                        }
                    }
                    UserInfo objUser = new UserInfo();
                    //string EmergeReviewSection = "";


                    objUser = BLLMembership.GetUserInformation(GlobalMemberUserId);
                    //string ProductDisplayname = "", 
                    string    ApplicantFirstName = ObjRequestAndResponseResult.SearchedFirstName, ApplicantLastName = ObjRequestAndResponseResult.SearchedLastName;
                    ViewState["vwApplicantName"] = ApplicantLastName + " " + ApplicantFirstName;
                    //ProductDisplayname = ObjRequestAndResponseResult.ProductDisplayName;
                    string DisclaimerInfo = string.Empty;
                    //if (ObjRequestAndResponseResults.ReportStatus != 1)
                    //{
                    if (ObjRequestAndResponseResult.ProductCode.ToLower() == "scr" || ObjRequestAndResponseResult.ProductCode.ToLower() == "mvr" || ObjRequestAndResponseResult.ProductCode.ToLower() == "mvr")
                    {
                        int StateId = Convert.ToInt32(ObjRequestAndResponseResult.SearchedStateId);
                        int fkPerApplicationId = Int32.Parse(ObjRequestAndResponseResult.fkProductApplicationId.ToString());
                        BALGeneral objBalGeneral = new BALGeneral();
                        var list = objBalGeneral.GetDisclamierInfoForScrMvrWcb(fkPerApplicationId, StateId);
                        if (list.Count > 0)
                        {
                            DisclaimerInfo = list.ElementAt(0).DisclaimerData;
                        }
                    }


                    if (ObjRequestAndResponseResult.ProductCode.ToLower() == "ccr1" || ObjRequestAndResponseResult.ProductCode.ToLower() == "ccr1" || ObjRequestAndResponseResult.ProductCode.ToLower() == "ccr2")
                    {

                        int pkProductId = Int32.Parse(ObjRequestAndResponseResult.pkProductId.ToString());
                        int fkCountyId = Convert.ToInt32(ObjRequestAndResponseResult.fkCountyId);
                        BALGeneral objBalGeneral = new BALGeneral();
                        var list = objBalGeneral.GetDisclamierInfoForCcr1Ccr2Rcx(fkCountyId, pkProductId);
                        if (list.Count > 0)
                        {
                            DisclaimerInfo = list.ElementAt(0).DisclaimerData;
                        }
                    }

                    if (ObjRequestAndResponseResult.ProductCode.ToLower() == "fcr")
                    {
                        string CountyCode = ObjRequestAndResponseResult.CountyCode;
                        BALGeneral objBalGeneral = new BALGeneral();
                        var list = objBalGeneral.GetDisclamierInfoForFCR(CountyCode);
                        if (list.Count > 0)
                        {
                            DisclaimerInfo = list.ElementAt(0).DisclaimerData;
                        }
                    }

                    //}
                    DisclaimerScript = "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\"><tr><td>";


                    DisclaimerScript = DisclaimerScript + "<div class=\"sectionsummary\"><h3 style=\"line-height: 1em;\">" + DisclaimerInfo + "</h3></div>";


                    DisclaimerScript = DisclaimerScript + "</td></tr></table>";

                    BottomScript = "</td><td class=\"middle_right\" width=\"9px\"></td></tr></table></div><div id=\"" + ProductCode + "_f\" style=\"display: block;\"><table class=\"sectionbgfooter\" width=\"98%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td class=\"bottom_left\" width=\"14px\"></td><td class=\"bottom_center\" width=\"97%\"></td><td class=\"bottom_right\" width=\"9px\"></td> </tr></table></div></div><br/>";


                    if (ObjRequestAndResponseResult.fkReportCategoryId != 2)
                    {
                        EmergeAssistScript = "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\"><tr><td style=\"padding-left: 4%;\"> <a href=\"javascript:ReportReview('" + ObjRequestAndResponseResult.ProductDisplayName + "','" + ApplicantLastName + "','" + ApplicantFirstName + "' ,0,'" + ObjRequestAndResponseResult.pkOrderDetailId + "');\"><img style=\"height:28px;\" src='https://emerge.intelifi.com/content/themes/base/images/help-icon4.png' alt='help'/></a></td><td ><a style=\"font-family: Arial;display:block;font-size:12px;text-decoration:none;font-weight:bold;color:#000;\" href=\"javascript:ReportReview('" + ObjRequestAndResponseResult.ProductDisplayName + "','" + ApplicantLastName + "','" + ApplicantFirstName + "' ,0,'" + ObjRequestAndResponseResult.pkOrderDetailId + "');\"  > Help</a></td>";
                    }
                    else
                    {
                        EmergeAssistScript = "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\"><tr><td></td>";
                    }
                    //  #region Check Product review status for location
                    byte ProductReviewStatus = 0;
                    try
                    {
                        ProductReviewStatus = ObjBALEmergeReview.GetProductReviewStatusPerLocation(Convert.ToInt32(ObjRequestAndResponseResult.fkLocationId.ToString()), Convert.ToInt32(ObjRequestAndResponseResult.fkProductApplicationId.ToString()));
                        if (ProductReviewStatus == 0 && ObjRequestAndResponseResult.fkReportCategoryId == 2 && (ObjRequestAndResponseResult.ProductCode.ToLower() != "ncr3" || ObjRequestAndResponseResult.ProductCode.ToLower() != "rcx" || ObjRequestAndResponseResult.ProductCode.ToLower() != "sor"))
                        {
                            ProductReviewStatus = 1;
                        }
                    }
                    catch
                    {

                    }
                    if (ObjRequestAndResponseResult.IsEnableReview && ObjRequestAndResponseResult.IsAutoReview && ProductReviewStatus == 2 & ObjRequestAndResponseResult.ReportStatus == 2 && ObjRequestAndResponseResult.ReviewStatus != 0)
                    {
                        IsAutoReviewSent = 1;
                    }

                    // #endregion
                    //string[] ProductsWithTextForReview = new string[] { "BS", "BR", "CDLIS", "ER", "DB", "DLS", "OIG", "PL", "RAL", "RPL", "MVR", "MVS", "EEI", "EBP", "ECR", "UCC", "EDV", "EMV", "WCV", "PLV", "PRV", "NCR4", "SNS", "NCR2" };

                    //if (ProductsWithTextForReview.Contains(ObjRequestAndResponseResult.ProductCode) && IsShowRawData)
                    //{
                    //    EmergeReviewSection = "<table style='width:100%' border=\"0\" cellpadding=\"2\" cellspacing=\"0\"><tr><td style='width:85%;'><textarea id='txt_" + ObjRequestAndResponseResult.pkOrderResponseId + "' rows=\"2\" style=\"width:100%;height:40px;background-color:#FFFFE1\">" + ObjRequestAndResponseResult.EmergeReviewNote + "</textarea></td><td align=\"right\" valign=\"bottom\" style=\"vertical-align:bottom;width:10%\"><input type=\"button\" id='btn_" + Convert.ToString(ObjRequestAndResponseResult.pkOrderResponseId) + "' value=\"Save Changes\" onclick=\"javascript:SetHiddenFieldsForOtherReviewReportsMVC('" + ObjRequestAndResponseResult.pkOrderResponseId + "')\" wrap=\"off\" style=\" height:30px; width:110px; text-align:center; vertical-align:middle\"/></td></tr></table>";
                    //}
                    //else if (ProductsWithTextForReview.Contains(ObjRequestAndResponseResult.ProductCode) && ObjRequestAndResponseResult.EmergeReviewNote != "")
                    //{
                    //    EmergeReviewSection = "<table style='width:100%' border=\"0\" cellpadding=\"2\" cellspacing=\"0\"><tr><td class=\"ReviewNote\" style='width:85%;background-color:#FFFFE1'>" + ObjRequestAndResponseResult.EmergeReviewNote + "</td></tr></table>";
                    //}

                    if (ObjRequestAndResponseResult.IsEnableReview != null)
                    {
                        if (ObjRequestAndResponseResult.fkReportCategoryId != 1)
                        {
                            if (ObjRequestAndResponseResult.IsEnableReview == true && ProductReviewStatus > 0)
                            {
                                EmergeAssistScript = EmergeAssistScript + " <td style=\"padding-left: 4%;\"><a href=\"javascript:ReportReview('" + ObjRequestAndResponseResult.ProductDisplayName + "','" + ApplicantLastName + "','" + ApplicantFirstName + "',1,'" + ObjRequestAndResponseResult.pkOrderDetailId + "');\"><img style=\"height:28px;\" src='https://emerge.intelifi.com/content/themes/base/images/emerge-review-button.png' title='Emerge Review'/></a></td><td><a style=\"font-family: Arial;display:block;font-size:12px;text-decoration:none;font-weight:bold;color:#000;\" href=\"javascript:ReportReview('" + ObjRequestAndResponseResult.ProductDisplayName + "','" + ApplicantLastName + "','" + ApplicantFirstName + "',1,'" + ObjRequestAndResponseResult.pkOrderDetailId + "');\"  > Emerge Review</a></td><td style='display:none'><textarea id=" + ObjRequestAndResponseResult.pkOrderDetailId + ">" + ObjRequestAndResponseResult.ReviewComments + " </textarea></td>";
                            }
                            else
                            {
                                EmergeAssistScript = EmergeAssistScript + " <td><a id='" + ObjRequestAndResponseResult.ProductDisplayName + "' href=\"javascript:ActivateReportsVR('" + ObjRequestAndResponseResult.ProductDisplayName + "');\"><img style=\"height:28px;\" src='https://emerge.intelifi.com/content/themes/base/images/emerge-review-button.png' alt='Enable Review' title='Send Request for Emerge Review'/></a></td><td><a style=\"font-family: Arial;display:block;font-size:12px;text-decoration:none;font-weight:bold;color:#000;\" href=\"javascript:ActivateReportsVR('" + ObjRequestAndResponseResult.ProductDisplayName + "');\"  > Emerge Review</a></td>";
                            }
                        }
                        else
                        {
                            EmergeAssistScript = EmergeAssistScript + " <td></td>";
                        }
                    }
                    EmergeAssistScript = EmergeAssistScript + "</tr></table>";

                    string IsLiveRunnerProduct = "0";
                    if (ObjRequestAndResponseResult.IsLiveRunner == true)
                        IsLiveRunnerProduct = "1";
                    dictCodepkOrderResponseId.Add(Convert.ToString(ObjRequestAndResponseResult.pkOrderResponseId).Trim() + "_" + Convert.ToString(ObjRequestAndResponseResult.pkOrderDetailId).Trim() + "_" + IsLiveRunnerProduct, ProductCode);

                    //ObjControl_each.Controls.AddAt(2, CreateLabelControl(BottomScript));
                    ObjControl_each.Controls.Add(CreateLabelControl(BottomScript));
                    //This is done for new help icon
                    //ObjControl_each.Controls.AddAt(2, HelpEmergeAssist(EmergeAssistScript));
                    ObjControl_each.Controls.Add(HelpEmergeAssist(EmergeAssistScript));
                    nameDiff++;
                    if (ObjRequestAndResponseResult.ReviewStatus != 0)
                        CountReviewReports++;
                }
                DisplyProductCode = DisplyProductCode.Substring(0, DisplyProductCode.Length - 1);


                ReportFor = "<div><table class=\"fnt\" cellpadding=\"0\" cellspacing=\"0\" width=\"98%\"><tr><td style=\"width: 15%\">Intelifi Inc.</td>"
                    + "<td style=\"width: 2%\">|</td><td style=\"width: 37%\">Report for " + FullSearchedName + "</td>"
                    + "<td style=\"width: 30%; text-align: right\">Report #" + ObjProc_Get_RequestAndResponseResult.First().OrderNo + "</td>"
                    + "<td style=\"width: 2%;\">&nbsp;</td></tr><tr><td colspan=\"5\"><hr style=\"color: Black; border: 2px solid #000; margin-top: 2px; margin-bottom: 4px;\" /></td>"
                    + "</tr></table></div>";
                //topMenu = "<div style=\"display: block; background: rgb(0, 120, 189) none repeat scroll 0% 0%; margin: auto; width: 98%;height: 70px; margin-left:20px;\"><table><tr id='tr_Menu'><td colspan='2' class='menu_seperator' valign='bottom'> <table width='100%' cellpadding='0' cellspacing='0' style='padding: 0px 0px 0px 0px; height: 35px;'><tbody><tr><td class='Center_ImgMVC' valign='bottom' style='border:none;'><div style='width: 100%; padding-left: 35px; float: left; vertical-align: bottom;'><ul id='tabsDemo1'><li id='liReport' class='current'><a> " + FullSearchedName + "<span id='spanRpt'></span></a></li></ul><ul id='tabsDemo2'></ul></div></td></tr></tbody></table></td></tr></table></div>";
                topMenu = "";
                topHeader = "<img src=\" " + ApplicationPath.GetApplicationPath() + "Content/themes/base/images/IntelifiPowerToKnow_small.png\" alt=\"\" /><div style=\"margin-left:20px;\">"
                    + "<table cellpadding=\"2\" cellspacing=\"2\" style=\"margin: 0px; border:1px solid #ccc; margin-bottom:10px;\" width=\"100%\">"
                    + "<tr><td style=\"width: 100%;\"><table id=\"tblsummary\" style=\"border-collapse: collapse;\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" align=\"center\">"
                    + "<tr>"
                    + "<td bgcolor=\"#ffffff\" class=\"Align_Center\" align=\"center\"><table cellpadding=\"0\" cellspacing=\"0\" style=\"margin: 0px\" width=\"100%\" >"
                    + "<tr><td width=\"33%\" valign=\"top\"><table cellpadding=\"2\" cellspacing=\"2\" style=\"margin: 0px\" width=\"100%\">"
                    + "<tr style=\"font-size:12px !important;font-weight:bold;\"><td colspan=\"3\">Report Information</td></tr><tr style=\"font-size:12px !important;\"><td class=\"Report_Sub_info_left\" style=\"width: 35%\">Order Id</td>"
                    + "<td class=\"Report_Sub_info_middle\"></td><td class=\"Report_Sub_info_right\">" + ObjProc_Get_RequestAndResponseResult.First().OrderNo + "</td>"
                    + "</tr><tr style=\"font-size:12px !important;\"><td class=\"Report_Sub_info_left\">Search Date</td><td class=\"Report_Sub_info_middle\"></td><td class=\"Report_Sub_info_right\">" + ObjProc_Get_RequestAndResponseResult.First().OrderDt.ToString("MM/dd/yyyy HH:mm:ss") + "</td>"
                    + "</tr><tr style=\"font-size:12px !important;\"><td class=\"Report_Sub_info_left\">Report Type</td><td class=\"Report_Sub_info_middle\"></td><td class=\"Report_Sub_info_right\">" + DisplyProductCode + "</td>"
                    + "</tr></table></td><td width=\"33%\" valign=\"top\"><table cellpadding=\"2\" cellspacing=\"2\" style=\"margin: 0px\" width=\"100%\">"
                    + "<tr style=\"font-size:12px !important;font-weight:bold;\"><td style=\"font-size:12px;\" colspan=\"3\">Company Information</td></tr><tr style=\"font-size:12px !important;\"><td class=\"Report_Sub_info_left\" style=\"width: 27%\">Company</td>"
                    + "<td class=\"Report_Sub_info_middle\"></td><td class=\"Report_Sub_info_right\">" + ObjCompanyDetails.First().CompanyName + "</td></tr>"
                    + "<tr style=\"font-size:12px !important;\"><td class=\"Report_Sub_info_left\">Location</td><td class=\"Report_Sub_info_middle\"></td><td class=\"Report_Sub_info_right\">" + ObjCompanyDetails.First().city + ", " + ObjCompanyDetails.First().statecode + "</td></tr>"
                    + "<tr style=\"font-size:12px !important;\"><td class=\"Report_Sub_info_left\">User</td><td class=\"Report_Sub_info_middle\"></td><td class=\"Report_Sub_info_right\">" + ObjCompanyDetails.First().lastname + ", " + ObjCompanyDetails.First().firstname + "</td></tr></table></td>"
                    + "<td width=\"33%\" valign=\"top\"><table cellpadding=\"2\" cellspacing=\"2\" style=\"margin: 0px\" width=\"100%\"><tr style=\"font-size:12px !important;font-weight:bold;\"><td colspan=\"3\">Applicant Information</td></tr>"
                    + "<tr style=\"font-size:12px !important;\"><td class=\"Report_Sub_info_left\" style=\"width: 50%\">Applicant Name</td><td class=\"Report_Sub_info_middle\"></td><td class=\"Report_Sub_info_right\">" + FullSearchedName + "</td></tr>"
                    + "<tr style=\"font-size:12px !important;\"><td class=\"Report_Sub_info_left\">State</td><td class=\"Report_Sub_info_middle\"></td><td class=\"Report_Sub_info_right\">" + ObjProc_Get_RequestAndResponseResult.First().SearchedStateCode + "</td></tr>"
                    + "<tr style=\"font-size:12px !important;\"><td class=\"Report_Sub_info_left\">Date of birth</td><td class=\"Report_Sub_info_middle\"></td><td class=\"Report_Sub_info_right\">" + ObjProc_Get_RequestAndResponseResult.First().SearchedDob + "</td></tr></table>"
                    + "</td></tr></table></td></tr>"
                    + "<tr>"
                    + "</table></td></tr>";


                horzLine = "<hr style=\"color: Gray; margin-top: 2px; margin-bottom: 4px;\" />"
                    + "<hr style=\"color: Gray; margin-top: 2px; margin-bottom: 4px;\" /><br /><br />";



                ViewReportModel objViewReportModel = (ViewReportModel)Session["NCR1TotalCount"];


                string refcode = ObjProc_Get_RequestAndResponseResult.First().SearchedTrackingRef == "" ? "NA" : ObjProc_Get_RequestAndResponseResult.First().SearchedTrackingRef;
                string notes = ObjProc_Get_RequestAndResponseResult.First().SearchedTrackingNotes == "" ? "NA" : ObjProc_Get_RequestAndResponseResult.First().SearchedTrackingNotes;

                string _CountNullDob = string.Empty;

                string _TotalCount = string.Empty;


                // fix issue for INT 410 ON 21jun2016
                if (objViewReportModel != null)
                {
                    _CountNullDob = Convert.ToString(objViewReportModel.CountNullDob);
                    _TotalCount = Convert.ToString(objViewReportModel.TotalCount);
                }

                else
                {
                    _CountNullDob = "0";
                    _TotalCount = "0";
                }



                bottomFooter = "<tr id=\"rowReportAndApplicantInfo_another\"><td style=\"width: 100%;\">"
                     + "<table id=\"Table1\" style=\"border-collapse: collapse; margin-left: 20px !important;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"98%\" align=\"center\">"
                     + "<tr><td width=\"11\" height=\"11\" bgcolor=\"#dadada\" style=\"border-left: 0px solid #cdcdcd;\"></td>"
                     + "<td bgcolor=\"#dadada\" align=\"left\"><table cellpadding=\"2\" cellspacing=\"2\">"
                     + "<tr><td><span class=\"lbl_viewreport_bottom\">Reference Code: </span>&nbsp; <span class=\"lbl_viewreport_bottom_alt\">" + refcode + "</span></td>"
                     + "<td style=\"width: 50px;\">&nbsp;</td><td><span class=\"lbl_viewreport_bottom\">Notes: </span>&nbsp; <span class=\"lbl_viewreport_bottom_alt\">"
                     + "" + notes + "</span></td></tr></table></td><td width=\"11\" height=\"11\" bgcolor=\"#dadada\" style=\"border-right: 0px solid #cdcdcd;\"></td></tr>"
                     + "</table><table style=\"border-collapse: collapse;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"95%\" align=\"center\"><tr><td><div><div style=\"width:100%;margin-top:1%;\"><div style=\"float:left;margin-left:2.5%;font-family: Microsoft Sans Serif; font-size: 14px;\">Match Meter</div><div style=\"float:left;margin-left:0.4%;\"> <img id=\"ImgDescription-0\" onmouseover=\"tooltips('@message', 'Match Meter');\" onmouseout=\"exit();\" src=\"https://emerge.intelifi.com/Content/themes/base/images/drugtesting4help.png\" alt=\"Description\" style=\"height: 16px; width: 16px; border-width: 0px; cursor: pointer;\"></div>"
                     + "</div><div style=\"width:100%;float:right;margin-top:0.5%;\">"
                     + "<div style=\"float:left;margin-left:2.5%;margin-top:0.2%;\"><img id=\"exactmatch\" src=\" https://emerge.intelifi.com/Content/themes/base/images/ico_match_green.jpg\"></div><div style=\"float:left;margin-left:0.5%;font-family: Microsoft Sans Serif; font-size: 14px;\">Exact Match</div><div style=\"float:left;margin-left:4.2%;margin-top:0.2%;\"><img id=\"partialmatch\" src=\"https://emerge.intelifi.com/Content/themes/base/images/ico_red_yellow.png\"></div><div style=\"float:left;margin-left:0.5%;font-family: Microsoft Sans Serif; font-size: 14px;\">Partial Match</div><div style=\"float:left;margin-left:4.2%;margin-top:0.2%;\"><img id=\"mismatch\" src=\"https://emerge.intelifi.com/Content/themes/base/images/ico_red_match.png\"></div><div style=\"float:left;margin-left:0.5%;font-family: Microsoft Sans Serif; font-size: 14px;\">Mismatch</div><div style=\"float:left;margin-left:4.2%;font-family: Microsoft Sans Serif; font-size: 14px;\"><img id=\"nomatch\" src=\"https://emerge.intelifi.com/Content/themes/base/images/ico_darkgrey_match.png\"> Decisioned</div>"
                     + "</div></div></td></tr></table>"
                     + "<table style=\"border-collapse: collapse;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"95%\" align=\"center\"><tr><td>"
                     + "<p style=\"text-align: justify;\"><b>Disclaimer:</b> Information obtained from Intelifi and Emerge Solutions is provided 'as is' and should not be the sole determiner in the evaluation of this individual "
                     + "(Human error in compiling this information is possible). All other factors, references and current situations should be considered.The information in this report is derived "
                     + "from records in accordance with the Fair Credit Reporting Act (FCRA, Public Law 91-508, Title VI). This disclaimer includes without limitation, implied warranties "
                     + "of merchantability and fitness for a particular purpose. Intelifi makes no warranty as to the quality accuracy, completeness, timeliness and validity of the data or "
                     + "its compliance with applicable law. Intelifi is not liable for any claims or damages arising out of or relating to the use of this data. This information should not "
                     + "be used in legal proceedings. Furthermore, Intelifi policy requires purchasers and all users of this report to comply with all applicable law (including the FCRA andGLBA) "
                     + "relating to the access and use of this data while abiding by their obligations, and remaining in compliance of the FCRA. For technical assistance, please contact Intelifi Support at 888-409-1819."
                     + "</p><hr /><table width=\"100%\"><tr><td width=\"89px\"><img src=\"https://emerge.intelifi.com/Content/themes/base/images/emerge_disclaimer.png\" alt=\"\" /></td>"
                     + "<td width=\"285px\" align=\"left\">Intelifi | Powered by Emerge</td><td align=\"right\">"
                     + "</td></td></tr></table></td></tr></table><input type='hidden' id='CountNullDob' value=" + Convert.ToString(_CountNullDob) + "> <input type='hidden' id='TotalCount' value=" + Convert.ToString(_TotalCount) + "> </div>";

                //src=\"https://www.intelifi.com/emerge/Resources/Images/EMEditWhell.png\" 

                //ObjControl_eachReport.Controls.AddAt(0, CreateLabelControl(topMenu));
                // ObjControl_eachReport.Controls.AddAt(1, CreateLabelControl(topHeader));
                ObjControl_eachReport.Controls.Add(CreateLabelControl(topMenu));
                ObjControl_eachReport.Controls.Add(CreateLabelControl(topHeader));

                ObjControl_eachReport.Controls.Add(ObjControl_each);

                //ObjControl_eachReport.Controls.AddAt(3, CreateLabelControl(bottomFooter));
                //ObjControl_eachReport.Controls.AddAt(4, CreateLabelControl(horzLine));
                ObjControl_eachReport.Controls.Add(CreateLabelControl(bottomFooter));
                ObjControl_eachReport.Controls.Add(CreateLabelControl(horzLine));
                //phAllrecords.Controls.Add(ObjControl_eachReport);
                phReports.Controls.Add(ObjControl_eachReport);
                //  #endregion

                Session["dictCodepkOrderResponseId"] = dictCodepkOrderResponseId;
                htmlfile = GetReportHtml();
                if (Request.QueryString["SP"].ToString() == "SV")
                {
                    phReports.Controls.Clear();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["EM"]))
                {
                    phReports.Controls.Clear();
                }
            }
            catch //(Exception)
            {

            }
            finally
            {
                ObjBALOrders = null;
            }

            //string htmlfilecsfile = string.Empty;
            //htmlfilecsfile = "";


            return htmlfile;
        }

        // ND-39 ISSUE FOR PRINT SIDE 
        // DONE ON 23 APRIL 2016 AND LAST MODIFIED ON 24 JUNE 2016
        #region(GENERATE SUMMARY PAGE REPORTS)
        private string BindReports_forSavedReportPage(int ObjOrderId)
        {
            string htmlfile = "";
            string ProductCode = "";
            string TopScript = "";
            string topHead = "";
            //string topHeader = "";
            //string bottomFooter = "";
            //string horzLine = "";
            //string topMenu = "";
            //string BottomScript = "";
            //string EmergeAssistScript = "";
            //string DisclaimerScript = "";
            //string ReportFor = "";
            bool IsShowRawData = true;
            string sOrderStatus = string.Empty;
            string ModifiedResponse = string.Empty;
            string DisplyProductCode = "";
            //lblReportType.Text = "";
            //int ReviewQueryString = -1;
            ObjBALOrders = new BALOrders();
            //int IsAutoReviewSent = 0;
            //BALEmergeReview ObjBALEmergeReview = new BALEmergeReview();
            //Dictionary<string, string> dictCodepkOrderResponseId = new Dictionary<string, string>();
            //Control hdnReviewDetail = new Control();
            try
            {
                List<Proc_Get_RequestAndResponseResult> ObjProc_Get_RequestAndResponseResult = ObjBALOrders.GetRequestAndResponseByOrderId(ObjOrderId);
                //Control ObjControl_eachReport = new Control();
                string SearchedName = string.Empty;
                SearchedName = GenerateApplicantName(ObjProc_Get_RequestAndResponseResult.First().SearchedLastName, ObjProc_Get_RequestAndResponseResult.First().SearchedSuffix, ObjProc_Get_RequestAndResponseResult.First().SearchedMiddleInitial, ObjProc_Get_RequestAndResponseResult.First().SearchedFirstName);
                string FullSearchedName = SearchedName.Replace("'", "");
                //string FileName = ObjProc_Get_RequestAndResponseResult.First().OrderNo.ToString();
                if (ObjProc_Get_RequestAndResponseResult.Count > 0)
                {
                    ViewState["ReportName"] = ObjProc_Get_RequestAndResponseResult.First().SearchedLastName + "_" + ObjProc_Get_RequestAndResponseResult.First().SearchedFirstName;
                }
                else
                {
                    ViewState["ReportName"] = string.Empty;
                }

                BALCompany ObjBALCompany = new BALCompany();

                List<Proc_Get_CompanyDetailByMemberShipIdResult> ObjCompanyDetails = ObjBALCompany.GetCompanyDetailByMemberShipIdResult(ObjProc_Get_RequestAndResponseResult.First().fkUserId);
                if (Roles.IsUserInRole(Page.User.Identity.Name, "SystemAdmin") || Roles.IsUserInRole(Page.User.Identity.Name, "SupportTechnician") || Roles.IsUserInRole(Page.User.Identity.Name, "SupportManager"))
                {
                    IsShowRawData = true;
                }
                else
                {
                    IsShowRawData = false;
                }

                Control ObjControl_each = new Control();
                string FinalPrintSavedReportSection = "<tr><td><hr /><p class='para'>Please note that this is a summary page. Please view the complete report to view any records that have been located.</p><br />";

                foreach (Proc_Get_RequestAndResponseResult ObjRequestAndResponseResult in ObjProc_Get_RequestAndResponseResult)
                {
                    string SearchedStateCode = string.Empty;
                    ProductCode = ObjRequestAndResponseResult.ProductCode;
                    if (ObjRequestAndResponseResult.IsLiveRunner == true)
                    {
                        DisplyProductCode += ObjRequestAndResponseResult.ProductCode + "*" + ",";
                    }
                    else
                    {
                        DisplyProductCode += ObjRequestAndResponseResult.ProductCode + ",";
                    }
                    Proc_GetStateCountyPerCCROrderResult ObjStateCounty = ObjBALOrders.GetStateCountyPerOrderDetail(ObjRequestAndResponseResult.pkOrderDetailId);
                    if ((ObjRequestAndResponseResult.fkUserId == GlobalMemberUserId) && (ObjRequestAndResponseResult.IsViewed == false) && (ObjRequestAndResponseResult.ReportStatus == 2))
                    {
                        BALGeneral objGeneral = new BALGeneral();
                        objGeneral.UpdateOrderDetail_IsViewed(ObjRequestAndResponseResult.pkOrderDetailId);
                    }

                    if (ProductCode == "SNS")
                    {
                        string PathProvider = "";
                        if (Request.Url.ToString().Contains("Control"))
                        {
                            PathProvider = "../../";
                        }
                        else
                        {
                            PathProvider = "../";
                        }

                        HtmlLink ObjHtmlLink_sdstyle = new HtmlLink();
                        ObjHtmlLink_sdstyle.Href = "~/Resources/Css/sdstyle.css";
                        ObjHtmlLink_sdstyle.Attributes.Add("rel", "stylesheet");
                        ObjHtmlLink_sdstyle.Attributes.Add("type", "text/css");

                        Page.Header.Controls.Add(ObjHtmlLink_sdstyle);
                        Page.ClientScript.RegisterClientScriptInclude("progress_bar", PathProvider + "Scripts/SNS/progress_bar.js");
                        Page.ClientScript.RegisterClientScriptInclude("whozat", PathProvider + "Scripts/SNS/whozat.js");
                        Page.ClientScript.RegisterClientScriptInclude("refine", PathProvider + "Scripts/SNS/refine.js");
                        Page.ClientScript.RegisterClientScriptInclude("Js", PathProvider + "Scripts/SNS/Js.js");
                        Page.ClientScript.RegisterClientScriptInclude("jquery.tooltip.pack", PathProvider + "Scripts/SNS/jquery.tooltip.pack.js");
                        Page.ClientScript.RegisterClientScriptInclude("eye", PathProvider + "Scripts/SNS/eye.js");
                        Page.ClientScript.RegisterClientScriptInclude("utils", PathProvider + "Scripts/SNS/utils.js");
                        Page.ClientScript.RegisterClientScriptInclude("zoomimage", PathProvider + "Scripts/SNS/zoomimage.js");

                        Session["ses_sod_order_id"] = ObjRequestAndResponseResult.pkOrderResponseId.ToString();

                    }

                    string StateCode = "";
                    string CountyName = "";
                    string StateCounty = "";
                    if (ObjStateCounty != null)
                    {
                        StateCode = ObjStateCounty.StateCode;
                        CountyName = ObjStateCounty.LiveRunnerCounty;
                        StateCounty = "[" + CountyName + ", " + StateCode + "]";
                    }
                    string SCRState = "";
                    if (ProductCode.ToLower() == "scr")
                    {
                        SCRState = "[" + ObjRequestAndResponseResult.StateName + "]";
                    }

                    if (ObjRequestAndResponseResult.IsLiveRunner == true)
                    {
                        if (ObjRequestAndResponseResult.ProductCode == "SCR" || ObjRequestAndResponseResult.ProductCode == "CCR1")
                        {
                            if (ObjRequestAndResponseResult.ProductDisplayName == "Manual State Criminal")
                            {
                                ObjRequestAndResponseResult.ProductDisplayName = "LiveRunner State Criminal Report";
                            }

                            if (ObjRequestAndResponseResult.ProductDisplayName == "Manual County Search")
                            {
                                ObjRequestAndResponseResult.ProductDisplayName = "LiveRunner County Search";
                            }
                        }
                    }

                    if (ProductCode.ToLower() == "scr")//|| ProductCode.ToLower() ==""
                    {
                        string SearchedName1 = ObjRequestAndResponseResult.SearchedLastName + ", " + ObjRequestAndResponseResult.SearchedFirstName;
                        string FullSearchedName1 = SearchedName1.Replace("'", "");
                        TopScript += "<div style='font-size:13px; font-family:arial'><b>State Searched :</b> " + ObjRequestAndResponseResult.StateName + "</div>";
                        TopScript += "<div style='font-size:13px; font-family:arial'><b>Name :</b> " + FullSearchedName1 + "</div>";
                    }

                    ObjControl_each.Controls.Add(CreateLabelControl(TopScript));

                    if (ObjStateCounty != null)
                    {
                        StateCode = ObjStateCounty.StateCode;
                        CountyName = ObjStateCounty.LiveRunnerCounty;
                        StateCounty = "[" + CountyName + ", " + StateCode + "]";
                        SearchedStateCode = StateCounty;
                    }

                    if (ProductCode.ToLower() == "scr")
                    {
                        SCRState = "[" + ObjRequestAndResponseResult.StateName + "]";
                        StateCounty = SCRState;
                    }

                    if (ObjRequestAndResponseResult.IsLiveRunner == true)
                    {
                        if (ObjRequestAndResponseResult.ProductCode == "SCR" || ObjRequestAndResponseResult.ProductCode == "CCR1")
                        {
                            if (ObjRequestAndResponseResult.ProductDisplayName == "Manual State Criminal")
                            {
                                ObjRequestAndResponseResult.ProductDisplayName = "LiveRunner State Criminal Report";
                            }

                            if (ObjRequestAndResponseResult.ProductDisplayName == "Manual County Search")
                            {
                                ObjRequestAndResponseResult.ProductDisplayName = "LiveRunner County Search";
                            }
                        }
                    }

                    if (ProductCode.ToLower() == "scr")
                    {
                        string SearchedName1 = ObjRequestAndResponseResult.SearchedLastName + ", " + ObjRequestAndResponseResult.SearchedFirstName;
                        string FullSearchedName1 = SearchedName1.Replace("'", "");
                        SearchedStateCode = ObjRequestAndResponseResult.StateName;
                    }

                    if (ObjRequestAndResponseResult.IsLiveRunner == true)
                    {
                        if (ObjRequestAndResponseResult.ProductCode == "SCR" || ObjRequestAndResponseResult.ProductCode == "CCR1")
                        {
                            if (ObjRequestAndResponseResult.ProductDisplayName == "Manual State Criminal")
                            {
                                ObjRequestAndResponseResult.ProductDisplayName = "LiveRunner State Criminal Report";
                            }

                            if (ObjRequestAndResponseResult.ProductDisplayName == "Manual County Search")
                            {
                                ObjRequestAndResponseResult.ProductDisplayName = "LiveRunner County Search";
                            }
                        }
                    }

                    if (ObjRequestAndResponseResult.ReportStatus.ToString() == "3")
                    {
                        ObjControl_each.Controls.Add(DisplayInCompleteMessage());
                    }
                    else if (ObjRequestAndResponseResult.ReportStatus.ToString() == "2" && (ObjRequestAndResponseResult.ManualResponse != string.Empty || ObjRequestAndResponseResult.ResponseFileName != string.Empty))
                    {
                        ObjControl_each.Controls.Add(DisplayManualResponse(ObjRequestAndResponseResult));
                    }
                    else if (ObjRequestAndResponseResult.ReportStatus.ToString() == "2" && ObjRequestAndResponseResult.ReviewStatus.ToString() == "1" && IsShowRawData == false)
                    {
                        ObjControl_each.Controls.Add(DisplayPendingMessage());
                    }
                    else
                    {
                        if (ProductCode == "MVR1")
                        {
                            int MVR_ReportStatus = ObjRequestAndResponseResult.ReportStatus;
                            if (MVR_ReportStatus != 2)
                            {
                                List<Proc_Get_RequestAndResponseResult> ObjProc_Get_RequestAndResponseResult_MVR = null;
                                while (MVR_ReportStatus != 2 && MVR_Counter < 4)
                                {
                                    MVR_Counter++;
                                    System.Threading.Thread.Sleep(4000);
                                    ObjProc_Get_RequestAndResponseResult_MVR = ObjBALOrders.GetRequestAndResponseByOrderId(ObjOrderId).Where(data => data.ProductCode.ToLower() == "mvr").ToList<Proc_Get_RequestAndResponseResult>();
                                    MVR_ReportStatus = ObjProc_Get_RequestAndResponseResult_MVR.Where(data => data.ProductCode.ToLower() == "mvr").FirstOrDefault().ReportStatus;
                                }
                                ObjControl_each.Controls.Add(GetMvrResponse(ObjProc_Get_RequestAndResponseResult_MVR.Where(data => data.ProductCode.ToLower() == "mvr").FirstOrDefault()));
                            }
                            else
                            {
                                if (ObjRequestAndResponseResult.ResponseData.Contains("INVALID ACCOUNT NAME AND PASSWORD") || ObjRequestAndResponseResult.ResponseData.Contains("<request_status>1</request_status>") || ObjRequestAndResponseResult.ResponseData == "")
                                {
                                    TopScript += "Your order was completed manually through Intelifi support department. Please contact support for any further assistance.";
                                }
                                else
                                {
                                    Label lblMvrResponse = new Label();
                                    lblMvrResponse.Text = ObjRequestAndResponseResult.ResponseData;
                                    ObjControl_each.Controls.Add(lblMvrResponse);
                                }
                            }
                        }
                        else
                        {
                            ModifiedResponse = FetchResponse_SavedReport(ObjRequestAndResponseResult);
                        }
                    }
                    //Pass Modified XML Response
                    string response = Convert.ToString(ModifiedResponse);
                    //string Xmlerormessage = string.Empty;
                    XDocument ObjXDocumentresponse = new XDocument();

                    int Countflag = 0;
                    if (string.IsNullOrEmpty(response) == false)
                    {
                        string XmlResponse_Local = response;
                        int xmlstartindex = 0;

                        switch (ProductCode)
                        {
                            case "PS": //Changed By Himesh Kumar
                                {
                                    if (XmlResponse_Local.IndexOf("<addressinformation>") > 0 && XmlResponse_Local.IndexOf("</ieiresponse>") > 0)
                                    {
                                        XmlResponse_Local = XmlResponse_Local.Replace("</ieiresponse>", "");
                                        xmlstartindex = XmlResponse_Local.IndexOf("<addressinformation>");
                                        if (xmlstartindex > -1)
                                        {
                                            XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                        }
                                        ObjXDocumentresponse = XDocument.Parse(XmlResponse_Local);
                                        XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                        int Count = 0;
                                        if (ObjXDocumentresponse.Descendants("record").FirstOrDefault() != null)
                                        {
                                            var ch = ObjXDocumentresponse.Descendants("addressinformation").Descendants("records").Descendants("record").Attributes("Deleted").ToList();
                                            if (ch != null && ch.Count() > 0)
                                            {
                                                int index = 0;
                                                foreach (int i in ch)
                                                {
                                                    int value = Convert.ToInt16((ObjXDocumentresponse.Descendants("addressinformation").Descendants("records").Descendants("record").Attributes("Deleted").ToList())[i].Value);
                                                    if (value == 0)
                                                    {
                                                        index++;
                                                    }
                                                }
                                                Count = index;
                                            }
                                            else
                                            {
                                                Count = ObjXDocumentresponse.Descendants("addressinformation").Descendants("records").Descendants("record").Count();
                                            }
                                            Countflag = Count;
                                        }
                                    } break;
                                }

                            case "NCR1":
                                {
                                    if (XmlResponse_Local.IndexOf("<BackgroundReportPackage>") > 0 && XmlResponse_Local.IndexOf("</BackgroundReports>") > 0)
                                    {
                                        XmlResponse_Local = XmlResponse_Local.Replace("</BackgroundReports>", "");
                                        xmlstartindex = XmlResponse_Local.IndexOf("<BackgroundReportPackage>");
                                        if (xmlstartindex > -1)
                                        {
                                            XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                        }
                                        ObjXDocumentresponse = XDocument.Parse(XmlResponse_Local);
                                        XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                        int Count = 0;
                                        if (ObjXDocumentresponse.Descendants("CriminalCase").FirstOrDefault() != null)
                                        {
                                            var ch = ObjXDocumentresponse.Descendants("BackgroundReportPackage").Descendants("Screenings").Descendants("Screening").Descendants("CriminalReport").Descendants("CriminalCase").Attributes("Deleted").ToList();
                                            if (ch != null && ch.Count() > 0)
                                            {
                                                int index = 0;
                                                for (int i = 0; i < ch.Count(); i++)
                                                {
                                                    int value = Convert.ToInt16((ObjXDocumentresponse.Descendants("BackgroundReportPackage").Descendants("Screenings").Descendants("Screening").Descendants("CriminalReport").Descendants("CriminalCase").Attributes("Deleted").ToList())[i].Value);
                                                    if (value == 0)
                                                    {
                                                        index++;
                                                    }
                                                }
                                                Count = index;
                                            }
                                            else
                                            {
                                                Count = ObjXDocumentresponse.Descendants("BackgroundReportPackage").Descendants("Screenings").Descendants("Screening").Descendants("CriminalReport").Descendants("CriminalCase").Count();
                                            }
                                            Countflag = Count;
                                        }
                                    }
                                    break;
                                }
                            case "NCR2":
                                {
                                    if (XmlResponse_Local.Contains("FastraxNetwork"))
                                    {
                                        ObjXDocumentresponse = XDocument.Parse(XmlResponse_Local);
                                        int Count = ObjXDocumentresponse.Descendants("FastraxNetwork").Descendants("report-requested").Descendants("report").Descendants("report-detail-header").Descendants("report-data").Count();

                                        if (Count > 0)
                                        {
                                            Countflag = Count;
                                        }
                                        else
                                        {
                                            Countflag = Count;
                                        }
                                    }
                                    break;
                                }
                            case "NCR+":
                                {
                                    if (XmlResponse_Local.IndexOf("<BackgroundReportPackage>") > 0 && XmlResponse_Local.IndexOf("</BackgroundReports>") > 0)
                                    {
                                        XmlResponse_Local = XmlResponse_Local.Replace("</BackgroundReports>", "");
                                        xmlstartindex = XmlResponse_Local.IndexOf("<BackgroundReportPackage>");
                                        if (xmlstartindex > -1)
                                        {
                                            XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                        }
                                        ObjXDocumentresponse = XDocument.Parse(XmlResponse_Local);
                                        XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                        int Count = 0;
                                        if (ObjXDocumentresponse.Descendants("CriminalCase").FirstOrDefault() != null)
                                        {
                                            var ch = ObjXDocumentresponse.Descendants("BackgroundReportPackage").Descendants("Screenings").Descendants("Screening").Descendants("CriminalReport").Descendants("CriminalCase").Attributes("Deleted").ToList();
                                            if (ch != null && ch.Count() > 0)
                                            {
                                                int index = 0;
                                                foreach (int i in ch)
                                                {
                                                    int value = Convert.ToInt16((ObjXDocumentresponse.Descendants("BackgroundReportPackage").Descendants("Screenings").Descendants("Screening").Descendants("CriminalReport").Descendants("CriminalCase").Attributes("Deleted").ToList())[i].Value);
                                                    if (value == 0)
                                                    {
                                                        index++;
                                                    }
                                                }
                                                Count = index;
                                            }
                                            else
                                            {
                                                Count = ObjXDocumentresponse.Descendants("BackgroundReportPackage").Descendants("Screenings").Descendants("Screening").Descendants("CriminalReport").Descendants("CriminalCase").Count();
                                            }
                                            Countflag = Count;
                                        }
                                    }
                                    break;
                                }
                            case "CCR1":
                                {
                                    if (ObjRequestAndResponseResult.IsLiveRunner == true)
                                    {
                                        if (XmlResponse_Local.IndexOf("<BackgroundReportPackage>") > 0 && XmlResponse_Local.IndexOf("</BackgroundReports>") > 0)
                                        {
                                            XmlResponse_Local = XmlResponse_Local.Replace("</BackgroundReports>", "");
                                            xmlstartindex = XmlResponse_Local.IndexOf("<BackgroundReportPackage>");
                                            if (xmlstartindex > -1)
                                            {
                                                XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                            }
                                            ObjXDocumentresponse = XDocument.Parse(XmlResponse_Local);
                                            XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                            int Count = 0;
                                            if (ObjXDocumentresponse.Descendants("CriminalCase").FirstOrDefault() != null)
                                            {
                                                var ch = ObjXDocumentresponse.Descendants("BackgroundReportPackage").Descendants("Screenings").Descendants("Screening").Descendants("CriminalReport").Descendants("CriminalCase").Attributes("Deleted").ToList();
                                                if (ch != null && ch.Count() > 0)
                                                {
                                                    int index = 0;
                                                    for (int z = 0; z < ch.Count(); z++)
                                                    {
                                                        int value = Convert.ToInt16((ObjXDocumentresponse.Descendants("BackgroundReportPackage").Descendants("Screenings").Descendants("Screening").Descendants("CriminalReport").Descendants("CriminalCase").Attributes("Deleted").ToList())[z].Value);
                                                        if (value == 0)
                                                        {
                                                            index++;
                                                        }
                                                    }
                                                    Count = index;
                                                }
                                                else
                                                {
                                                    Count = ObjXDocumentresponse.Descendants("BackgroundReportPackage").Descendants("Screenings").Descendants("Screening").Descendants("CriminalReport").Descendants("CriminalCase").Count();
                                                }
                                                Countflag = Count;
                                            }
                                        } break;
                                    }
                                    else
                                    {
                                        if (XmlResponse_Local.Contains("FastraxNetwork"))
                                        {
                                            ObjXDocumentresponse = XDocument.Parse(XmlResponse_Local);
                                            int Count = 0;
                                            if (ObjXDocumentresponse.Descendants("report-incident").FirstOrDefault() != null)
                                            {
                                                var ch = ObjXDocumentresponse.Descendants("FastraxNetwork").Descendants("report-requested").Descendants("report").Descendants("report-detail-header").Descendants("report-incident").Attributes("Deleted").ToList();
                                                if (ch != null && ch.Count() > 0)
                                                {
                                                    int index = 0;
                                                    for (int i = 0; i < ch.Count(); i++)
                                                    {
                                                        int value = Convert.ToInt16((ObjXDocumentresponse.Descendants("FastraxNetwork").Descendants("report-requested").Descendants("report").Descendants("report-detail-header").Descendants("report-incident").Attributes("Deleted").ToList())[i].Value);
                                                        if (value == 0)
                                                        {
                                                            index++;
                                                        }
                                                    }
                                                    Count = index;
                                                }
                                                else
                                                {
                                                    Count = ObjXDocumentresponse.Descendants("FastraxNetwork").Descendants("report-requested").Descendants("report").Descendants("report-detail-header").Descendants("report-incident").Count();
                                                }
                                                Countflag = Count;
                                            }
                                        }
                                        break;
                                    }
                                }
                            case "SCR":
                                {
                                    if (ObjRequestAndResponseResult.IsLiveRunner == true)
                                    {
                                        if (XmlResponse_Local.IndexOf("<BackgroundReportPackage>") > 0 && XmlResponse_Local.IndexOf("</BackgroundReports>") > 0)
                                        {
                                            XmlResponse_Local = XmlResponse_Local.Replace("</BackgroundReports>", "");
                                            xmlstartindex = XmlResponse_Local.IndexOf("<BackgroundReportPackage>");
                                            if (xmlstartindex > -1)
                                            {
                                                XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                            }
                                            ObjXDocumentresponse = XDocument.Parse(XmlResponse_Local);
                                            XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                            int Count = 0;
                                            if (ObjXDocumentresponse.Descendants("BackgroundReportPackage").FirstOrDefault() != null)
                                            {
                                                var ch = ObjXDocumentresponse.Descendants("BackgroundReportPackage").Descendants("Screenings").Descendants("Screening").Descendants("CriminalReport").Descendants("CriminalCase").Attributes("Deleted").ToList();
                                                if (ch != null && ch.Count() > 0)
                                                {
                                                    int index = 0;
                                                    for (int i = 0; i < ch.Count(); i++)
                                                    {
                                                        int value = Convert.ToInt16((ObjXDocumentresponse.Descendants("BackgroundReportPackage").Descendants("Screenings").Descendants("Screening").Descendants("CriminalReport").Descendants("CriminalCase").Attributes("Deleted").ToList())[i].Value);
                                                        if (value == 0)
                                                        {
                                                            index++;
                                                        }
                                                    }
                                                    Count = index;
                                                }
                                                else
                                                {
                                                    Count = ObjXDocumentresponse.Descendants("BackgroundReportPackage").Descendants("Screenings").Descendants("Screening").Descendants("CriminalReport").Descendants("CriminalCase").Count();
                                                }
                                                Countflag = Count;
                                            }
                                        }
                                        break;
                                    }
                                    else
                                    {
                                        if (XmlResponse_Local.IndexOf("<FastraxNetwork>") != -1)
                                        {
                                            XmlResponse_Local = XmlResponse_Local.Replace("</FastraxNetwork>", "");
                                            xmlstartindex = XmlResponse_Local.IndexOf("<FastraxNetwork>");
                                            XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                        }
                                        if (xmlstartindex > -1)
                                        {
                                            XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                        }
                                        ObjXDocumentresponse = XDocument.Parse(XmlResponse_Local);
                                        XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                        int Count = 0;
                                        if (ObjXDocumentresponse.Descendants("FastraxNetwork").FirstOrDefault() != null)
                                        {
                                            var ch = ObjXDocumentresponse.Descendants("FastraxNetwork").Descendants("report-requested").Descendants("report").Descendants("report-detail-header").Descendants("report-incident").Attributes("Deleted").ToList();
                                            if (ch != null && ch.Count() > 0)
                                            {
                                                int index = 0;
                                                foreach (int i in ch)
                                                {
                                                    int value = Convert.ToInt16((ObjXDocumentresponse.Descendants("FastraxNetwork").Descendants("report-requested").Descendants("report").Descendants("report-detail-header").Descendants("report-incident").Attributes("Deleted").ToList())[i].Value);
                                                    if (value == 0)
                                                    {
                                                        index++;
                                                    }
                                                }
                                                Count = index;
                                            }
                                            else
                                            {
                                                Count = ObjXDocumentresponse.Descendants("FastraxNetwork").Descendants("report-requested").Descendants("report").Descendants("report-detail-header").Descendants("report-incident").Count();
                                            }
                                            Countflag = Count;
                                        }

                                        break;
                                    }
                                }
                            case "EMV":
                                {
                                    if (XmlResponse_Local.IndexOf("<FastraxNetwork>") != -1)
                                    {
                                        XmlResponse_Local = XmlResponse_Local.Replace("</FastraxNetwork>", "");
                                        xmlstartindex = XmlResponse_Local.IndexOf("<FastraxNetwork>");
                                        XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                    }
                                    if (xmlstartindex > -1)
                                    {
                                        XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                    }
                                    ObjXDocumentresponse = XDocument.Parse(XmlResponse_Local);
                                    XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                    if (ObjXDocumentresponse.Descendants("OrderStatus").FirstOrDefault() != null)
                                    {
                                        string sOrderStatusValue = ObjXDocumentresponse.Descendants("OrderStatus").FirstOrDefault().Value.ToUpper();
                                        if (sOrderStatusValue != "PENDING-NEW" || sOrderStatusValue != "IN PROGRESS" || sOrderStatusValue != "REVIEW")
                                        {
                                            //Record found
                                            sOrderStatus = "Record found";
                                        }
                                        else
                                        {
                                            //Record not found
                                            sOrderStatus = "No Record found";
                                        }
                                    }
                                    break;
                                }
                            case "EDV":
                                {
                                    if (XmlResponse_Local.IndexOf("<FastraxNetwork>") != -1)
                                    {
                                        XmlResponse_Local = XmlResponse_Local.Replace("</FastraxNetwork>", "");
                                        xmlstartindex = XmlResponse_Local.IndexOf("<FastraxNetwork>");
                                        XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                    }
                                    if (xmlstartindex > -1)
                                    {
                                        XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                    }
                                    ObjXDocumentresponse = XDocument.Parse(XmlResponse_Local);
                                    XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                    if (ObjXDocumentresponse.Descendants("OrderStatus").FirstOrDefault() != null)
                                    {
                                        string sOrderStatusValue = ObjXDocumentresponse.Descendants("OrderStatus").FirstOrDefault().Value.ToUpper();
                                        if (sOrderStatusValue != "PENDING-NEW" || sOrderStatusValue != "IN PROGRESS" || sOrderStatusValue != "REVIEW")
                                        {
                                            //Record found
                                            sOrderStatus = "RECORD FOUND";
                                        }
                                        else
                                        {
                                            //Record not found
                                            sOrderStatus = "NO RECORD FOUND";
                                        }
                                    }
                                    break;
                                }
                            case "PLV":
                                {
                                    if (XmlResponse_Local.IndexOf("<FastraxNetwork>") != -1)
                                    {
                                        XmlResponse_Local = XmlResponse_Local.Replace("</FastraxNetwork>", "");
                                        xmlstartindex = XmlResponse_Local.IndexOf("<FastraxNetwork>");
                                        XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                    }
                                    if (xmlstartindex > -1)
                                    {
                                        XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                    }
                                    ObjXDocumentresponse = XDocument.Parse(XmlResponse_Local);
                                    XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                    if (ObjXDocumentresponse.Descendants("OrderStatus").FirstOrDefault() != null)
                                    {
                                        string sOrderStatusValue = ObjXDocumentresponse.Descendants("OrderStatus").FirstOrDefault().Value.ToUpper();
                                        if (sOrderStatusValue != "PENDING-NEW" || sOrderStatusValue != "IN PROGRESS" || sOrderStatusValue != "REVIEW")
                                        {
                                            //Record found
                                            sOrderStatus = "RECORD FOUND";
                                        }
                                        else
                                        {
                                            //Record not found
                                            sOrderStatus = "NO RECORD FOUND";
                                        }
                                    }
                                    break;
                                }
                            case "PRV":
                                {
                                    if (XmlResponse_Local.IndexOf("<FastraxNetwork>") != -1)
                                    {
                                        XmlResponse_Local = XmlResponse_Local.Replace("</FastraxNetwork>", "");
                                        xmlstartindex = XmlResponse_Local.IndexOf("<FastraxNetwork>");
                                        XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                    }
                                    if (xmlstartindex > -1)
                                    {
                                        XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                    }
                                    ObjXDocumentresponse = XDocument.Parse(XmlResponse_Local);
                                    XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                    if (ObjXDocumentresponse.Descendants("OrderStatus").FirstOrDefault() != null)
                                    {
                                        string sOrderStatusValue = ObjXDocumentresponse.Descendants("OrderStatus").FirstOrDefault().Value.ToUpper();
                                        if (sOrderStatusValue != "PENDING-NEW" || sOrderStatusValue != "IN PROGRESS" || sOrderStatusValue != "REVIEW")
                                        {
                                            //Record found
                                            sOrderStatus = "RECORD FOUND";
                                        }
                                        else
                                        {
                                            //Record not found
                                            sOrderStatus = "NO RECORD FOUND";
                                        }
                                    }
                                    break;
                                }
                            case "PRV2":
                                {
                                    if (XmlResponse_Local.IndexOf("<FastraxNetwork>") != -1)
                                    {
                                        XmlResponse_Local = XmlResponse_Local.Replace("</FastraxNetwork>", "");
                                        xmlstartindex = XmlResponse_Local.IndexOf("<FastraxNetwork>");
                                        XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                    }
                                    if (xmlstartindex > -1)
                                    {
                                        XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                    }
                                    ObjXDocumentresponse = XDocument.Parse(XmlResponse_Local);
                                    XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                    if (ObjXDocumentresponse.Descendants("OrderStatus").FirstOrDefault() != null)
                                    {
                                        string sOrderStatusValue = ObjXDocumentresponse.Descendants("OrderStatus").FirstOrDefault().Value.ToUpper();
                                        if (sOrderStatusValue != "PENDING-NEW" || sOrderStatusValue != "IN PROGRESS" || sOrderStatusValue != "REVIEW")
                                        {
                                            //Record found
                                            sOrderStatus = "RECORD FOUND";
                                        }
                                        else
                                        {
                                            //Record not found
                                            sOrderStatus = "NO RECORD FOUND";
                                        }
                                    }
                                    break;
                                }
                            case "RCX":
                                {
                                    if (XmlResponse_Local.IndexOf("<BackgroundReportPackage>") > 0 && XmlResponse_Local.IndexOf("</BackgroundReports>") > 0)
                                    {
                                        XmlResponse_Local = XmlResponse_Local.Replace("</BackgroundReports>", "");
                                        xmlstartindex = XmlResponse_Local.IndexOf("<BackgroundReportPackage>");
                                        if (xmlstartindex > -1)
                                        {
                                            XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                        }
                                        ObjXDocumentresponse = XDocument.Parse(XmlResponse_Local);
                                        XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                        int Count = 0;
                                        if (ObjXDocumentresponse.Descendants("CriminalCase").FirstOrDefault() != null)
                                        {
                                            var ch = ObjXDocumentresponse.Descendants("BackgroundReportPackage").Descendants("Screenings").Descendants("Screening").Descendants("CriminalReport").Descendants("CriminalCase").Attributes("Deleted").ToList();
                                            if (ch != null && ch.Count() > 0)
                                            {
                                                int index = 0;
                                                foreach (int i in ch)
                                                {
                                                    int value = Convert.ToInt16((ObjXDocumentresponse.Descendants("BackgroundReportPackage").Descendants("Screenings").Descendants("Screening").Descendants("CriminalReport").Descendants("CriminalCase").Attributes("Deleted").ToList())[i].Value);
                                                    if (value == 0)
                                                    {
                                                        index++;
                                                    }
                                                }
                                                Count = index;
                                            }
                                            else
                                            {
                                                Count = ObjXDocumentresponse.Descendants("BackgroundReportPackage").Descendants("Screenings").Descendants("Screening").Descendants("CriminalReport").Descendants("CriminalCase").Count();
                                            }
                                            Countflag = Count;
                                        }
                                    }
                                    break;
                                }
                            case "OFAC":
                                {
                                    if (XmlResponse_Local.Contains("XML_INTERFACE"))
                                    {
                                        ObjXDocumentresponse = XDocument.Parse(XmlResponse_Local);
                                        int Count = 0;
                                        if (ObjXDocumentresponse.Descendants("response_row").FirstOrDefault() != null)
                                        {
                                            var ch = ObjXDocumentresponse.Descendants("XML_INTERFACE").Descendants("CREDITREPORT").Descendants("cp_xmldirect").Descendants("search_response").Descendants("response_data").Descendants("response_row").Attributes("Deleted").ToList();
                                            if (ch != null && ch.Count() > 0)
                                            {
                                                int index = 0;
                                                foreach (int i in ch)
                                                {
                                                    int value = Convert.ToInt16((ObjXDocumentresponse.Descendants("XML_INTERFACE").Descendants("CREDITREPORT").Descendants("cp_xmldirect").Descendants("search_response").Descendants("response_data").Descendants("response_row").Attributes("Deleted").ToList())[i].Value);
                                                    if (value == 0)
                                                    {
                                                        index++;
                                                    }
                                                }
                                                Count = index;
                                            }
                                            else
                                            {
                                                Count = ObjXDocumentresponse.Descendants("XML_INTERFACE").Descendants("CREDITREPORT").Descendants("cp_xmldirect").Descendants("search_response").Descendants("response_data").Descendants("response_row").Count();
                                            }
                                            Countflag = Count;
                                        }
                                    }
                                    break;
                                }
                            case "MVR":
                                {
                                    if (XmlResponse_Local.Contains("MVR"))
                                    {
                                        ObjXDocumentresponse = XDocument.Parse(XmlResponse_Local);
                                        int Count = ObjXDocumentresponse.Descendants("MVR").Descendants("LicenseNumber").Count();
                                        Countflag = Count;
                                    }
                                    break;
                                }
                            case "BR":
                                {
                                    if (XmlResponse_Local.Contains("XML_INTERFACE"))
                                    {
                                        ObjXDocumentresponse = XDocument.Parse(XmlResponse_Local);
                                        int Count = 0;
                                        if (ObjXDocumentresponse.Descendants("response_row").FirstOrDefault() != null)
                                        {
                                            var ch = ObjXDocumentresponse.Descendants("XML_INTERFACE").Descendants("CREDITREPORT").Descendants("cp_xmldirect").Descendants("search_response").Descendants("response_data").Descendants("response_row").Attributes("Deleted").ToList();
                                            if (ch != null && ch.Count() > 0)
                                            {
                                                int index = 0;
                                                foreach (int i in ch)
                                                {
                                                    int value = Convert.ToInt16((ObjXDocumentresponse.Descendants("XML_INTERFACE").Descendants("CREDITREPORT").Descendants("cp_xmldirect").Descendants("search_response").Descendants("response_data").Descendants("response_row").Attributes("Deleted").ToList())[i].Value);
                                                    if (value == 0)
                                                    {
                                                        index++;
                                                    }
                                                }
                                                Count = index;
                                            }
                                            else
                                            {
                                                Count = ObjXDocumentresponse.Descendants("XML_INTERFACE").Descendants("CREDITREPORT").Descendants("cp_xmldirect").Descendants("search_response").Descendants("response_data").Descendants("response_row").Count();
                                            }
                                            Countflag = Count;
                                        }
                                    }
                                    break;
                                }
                            case "TDR":
                                {
                                    if (XmlResponse_Local.Contains("XML_INTERFACE"))
                                    {
                                        ObjXDocumentresponse = XDocument.Parse(XmlResponse_Local);
                                        XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                        if (ObjXDocumentresponse.Descendants("response").FirstOrDefault() != null)
                                        {
                                            int Count = ObjXDocumentresponse.Descendants("XML_INTERFACE").Descendants("CREDITREPORT").Descendants("response").Descendants("individual").Descendants("akas").Descendants("identity").Count();
                                            if (Count > 0)
                                            {
                                                //Record found
                                                sOrderStatus = "RECORD FOUND";
                                            }
                                            else
                                            {
                                                //Record not found
                                                sOrderStatus = "NO RECORD FOUND";
                                            }
                                        }
                                    }
                                    break;
                                }
                            case "CDLIS":
                                {
                                    if (XmlResponse_Local.Contains("RESPONSE"))
                                    {
                                        ObjXDocumentresponse = XDocument.Parse(XmlResponse_Local);
                                        XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                        if (ObjXDocumentresponse.Descendants("LicenseNumber").FirstOrDefault() != null)
                                        {
                                            int Count = ObjXDocumentresponse.Descendants("RESPONSE").Descendants("LicenseNumber").Count();
                                            if (Count > 0)
                                            {
                                                //Record found
                                                sOrderStatus = "RECORD FOUND";
                                            }
                                            else
                                            {
                                                //Record not found
                                                sOrderStatus = "NO RECORD FOUND";
                                            }
                                        }
                                    }
                                    break;
                                }
                            case "SOR":
                                {

                                    if (XmlResponse_Local.Contains("BGC"))
                                    {
                                        ObjXDocumentresponse = XDocument.Parse(XmlResponse_Local);
                                        int Count = 0;
                                        if (ObjXDocumentresponse.Descendants("response").FirstOrDefault() != null)
                                        {
                                            var ch = ObjXDocumentresponse.Descendants("BGC").Descendants("product").Descendants("USOneSearchSexOffender").Descendants("response").Descendants("detail").Descendants("offenders").Descendants("offender").Attributes("Deleted").ToList();
                                            if (ch != null && ch.Count() > 0)
                                            {
                                                int index = 0;
                                                foreach (int i in ch)
                                                {
                                                    int value = Convert.ToInt16((ObjXDocumentresponse.Descendants("BGC").Descendants("product").Descendants("USOneSearchSexOffender").Descendants("response").Descendants("detail").Descendants("offenders").Descendants("offender").Attributes("Deleted").ToList())[i].Value);
                                                    if (value == 0)
                                                    {
                                                        index++;
                                                    }
                                                }
                                                Count = index;
                                            }
                                            else
                                            {
                                                Count = ObjXDocumentresponse.Descendants("BGC").Descendants("product").Descendants("USOneSearchSexOffender").Descendants("response").Descendants("detail").Descendants("offenders").Descendants("offender").Count();
                                            }
                                            Countflag = Count;
                                        }
                                    }
                                    break;
                                }
                            case "ER":
                                {
                                    if (XmlResponse_Local.Contains("XML_INTERFACE"))
                                    {
                                        ObjXDocumentresponse = XDocument.Parse(XmlResponse_Local);
                                        int Count = 0;
                                        if (ObjXDocumentresponse.Descendants("response_data").FirstOrDefault() != null)
                                        {
                                            var ch = ObjXDocumentresponse.Descendants("XML_INTERFACE").Descendants("CREDITREPORT").Descendants("cp_xmldirect").Descendants("search_response").Descendants("response_data").Descendants("response_row").Attributes("Deleted").ToList();
                                            if (ch != null && ch.Count() > 0)
                                            {
                                                int index = 0;
                                                foreach (int i in ch)
                                                {
                                                    int value = Convert.ToInt16((ObjXDocumentresponse.Descendants("XML_INTERFACE").Descendants("CREDITREPORT").Descendants("cp_xmldirect").Descendants("search_response").Descendants("response_data").Descendants("response_row").Attributes("Deleted").ToList())[i].Value);
                                                    if (value == 0)
                                                    {
                                                        index++;
                                                    }
                                                }
                                                Count = index;
                                            }
                                            else
                                            {
                                                Count = ObjXDocumentresponse.Descendants("XML_INTERFACE").Descendants("CREDITREPORT").Descendants("cp_xmldirect").Descendants("search_response").Descendants("response_data").Descendants("response_row").Count();
                                            }
                                            Countflag = Count;
                                        }
                                    }
                                    break;
                                }
                            case "MVS":
                                {
                                    if (XmlResponse_Local.Contains("XML_INTERFACE"))
                                    {
                                        ObjXDocumentresponse = XDocument.Parse(XmlResponse_Local);
                                        int Count = 0;
                                        if (ObjXDocumentresponse.Descendants("response_data").FirstOrDefault() != null)
                                        {
                                            var ch = ObjXDocumentresponse.Descendants("XML_INTERFACE").Descendants("CREDITREPORT").Descendants("response").Attributes("Deleted").ToList();
                                            if (ch != null && ch.Count() > 0)
                                            {
                                                int index = 0;
                                                foreach (int i in ch)
                                                {
                                                    int value = Convert.ToInt16((ObjXDocumentresponse.Descendants("XML_INTERFACE").Descendants("CREDITREPORT").Descendants("response").Attributes("Deleted").ToList())[i].Value);
                                                    if (value == 0)
                                                    {
                                                        index++;
                                                    }
                                                }
                                                Count = index;
                                            }
                                            else
                                            {
                                                Count = ObjXDocumentresponse.Descendants("XML_INTERFACE").Descendants("CREDITREPORT").Descendants("response").Count();
                                            }
                                            Countflag = Count;
                                        }
                                    }
                                    break;
                                }
                            case "PS2":
                                {
                                    if (XmlResponse_Local.Contains("XML_INTERFACE"))
                                    {
                                        ObjXDocumentresponse = XDocument.Parse(XmlResponse_Local);
                                        int Count = 0;
                                        if (ObjXDocumentresponse.Descendants("response_data").FirstOrDefault() != null)
                                        {
                                            var ch = ObjXDocumentresponse.Descendants("XML_INTERFACE").Descendants("CREDITREPORT").Descendants("cp_xmldirect").Descendants("search_response").Descendants("response_data").Descendants("locator").Descendants("response_row").Attributes("Deleted").ToList();
                                            if (ch != null && ch.Count() > 0)
                                            {
                                                int index = 0;
                                                foreach (int i in ch)
                                                {
                                                    int value = Convert.ToInt16((ObjXDocumentresponse.Descendants("XML_INTERFACE").Descendants("CREDITREPORT").Descendants("cp_xmldirect").Descendants("search_response").Descendants("response_data").Descendants("locator").Descendants("response_row").Attributes("Deleted").ToList())[i].Value);
                                                    if (value == 0)
                                                    {
                                                        index++;
                                                    }
                                                }
                                                Count = index;
                                            }
                                            else
                                            {
                                                Count = ObjXDocumentresponse.Descendants("XML_INTERFACE").Descendants("CREDITREPORT").Descendants("cp_xmldirect").Descendants("search_response").Descendants("response_data").Descendants("locator").Descendants("response_row").Count();
                                            }
                                            Countflag = Count;
                                        }
                                    }
                                    break;
                                }
                            case "PSP":
                                {
                                    if (XmlResponse_Local.Contains("DriverReportResponseType"))
                                    {
                                        ObjXDocumentresponse = XDocument.Parse(XmlResponse_Local);
                                        XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                        if (ObjXDocumentresponse.Descendants("DriverLicenseNumber").FirstOrDefault() != null)
                                        {
                                            int Count = ObjXDocumentresponse.Descendants("DriverReportResponseType").Descendants("driverInformationResponse").Descendants("DriverLicenseNumber").Count();
                                            if (Count > 0)
                                            {
                                                //Record found
                                                sOrderStatus = "RECORD FOUND";
                                            }
                                            else
                                            {
                                                //Record not found
                                                sOrderStatus = "NO RECORD FOUND";
                                            }
                                        }
                                    }
                                    break;
                                }
                            case "WCV":
                                {
                                    ObjXDocumentresponse = XDocument.Parse(XmlResponse_Local);
                                    if (ObjXDocumentresponse.Descendants("subOrder").FirstOrDefault() != null)
                                    {
                                        string sOrderStatusValue = ObjXDocumentresponse.Descendants("subOrder").Attributes("filledCode").FirstOrDefault().Value.ToUpper();
                                        if (sOrderStatus != "CLEAR")
                                        {
                                            //Record found
                                            sOrderStatus = "RECORD FOUND";
                                        }
                                        else
                                        {
                                            //Record not found
                                            sOrderStatus = "NO RECORD FOUND";
                                        }
                                    }
                                    break;
                                }
                            case "FCR":
                                {
                                    if (XmlResponse_Local.IndexOf("<BackgroundReportPackage>") > 0 && XmlResponse_Local.IndexOf("</BackgroundReports>") > 0)
                                    {
                                        XmlResponse_Local = XmlResponse_Local.Replace("</BackgroundReports>", "");
                                        xmlstartindex = XmlResponse_Local.IndexOf("<BackgroundReportPackage>");
                                        if (xmlstartindex > -1)
                                        {
                                            XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                        }
                                        int Count = 0;
                                        ObjXDocumentresponse = XDocument.Parse(XmlResponse_Local);
                                        XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                                        //int Count = ObjXDocumentresponse.Descendants("BackgroundReportPackage").Descendants("Screenings").Descendants("Screening").Descendants("ScreeningStatus").Descendants("ResultStatus").Count();
                                        if (ObjXDocumentresponse.Descendants("ResultStatus").FirstOrDefault() != null)
                                        {
                                            string sOrderStatusValue = ObjXDocumentresponse.Descendants("ResultStatus").FirstOrDefault().Value.ToUpper();
                                            if (sOrderStatusValue != "CLEAR")
                                            {
                                                //Record found
                                                Count = 1;
                                            }
                                            else
                                            {
                                                //Record not found
                                                Count = 0;
                                            }
                                        }
                                        Countflag = Count;
                                    }
                                    break;
                                }

                            //For product code PS2,SSR and ESS whose response always HTML, we show magnify glass icon as per greg comment on JIRA

                            default: break;
                        }

                        string ProductName = ObjRequestAndResponseResult.ProductDisplayName;
                        if (ProductCode == "CCR1")
                        {
                            ProductName = ObjStateCounty.LiveRunnerCounty;
                            ProductName = "County of " + ObjStateCounty.LiveRunnerCounty + " , " + ObjStateCounty.StateCode;
                        }
                        else if (ProductCode == "SCR")
                        {
                            ProductName = "State Of " + ObjRequestAndResponseResult.StateName;
                        }
                        else if (ProductCode == "FCR")
                        {
                            //string FCRJurisdiction = string.Empty;
                            tblOrderSearchedDataJurdictionInfo objtblOrderSearchedDataJurdictionInfo = new tblOrderSearchedDataJurdictionInfo();
                            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                            {
                                objtblOrderSearchedDataJurdictionInfo = Dx.tblOrderSearchedDataJurdictionInfos.Where(db => db.FkOrderId == ObjOrderId).FirstOrDefault();
                            }
                            ProductName = "Federal of " + objtblOrderSearchedDataJurdictionInfo.SearchedCountyName;
                        }

                        // FOR ADD .... SUB STRING IF PRODUCT NAME TEXT IS TOO LONG
                        int IProductLength = ProductName.Length;
                        string sValue = string.Empty;
                        string SFinalProductName = string.Empty;
                        if (IProductLength >= 25)
                        {
                            sValue = ProductName.Substring(0, 22);
                            SFinalProductName = sValue + "...";
                        }
                        else
                        {
                            SFinalProductName = ProductName;
                        }

                        //For only EDV,EMV,PLV,PRV,PRV2,TDR,CDLIS,PSP. 
                        if (ProductCode == "EDV" || ProductCode == "EMV" || ProductCode == "PLV" || ProductCode == "PRV" || ProductCode == "PRV2" || ProductCode == "TDR" || ProductCode == "CDLIS" || ProductCode == "PSP")
                        {
                            //For Pending Report Status
                            if (ObjRequestAndResponseResult.ReportStatus == Convert.ToByte("1") || ObjRequestAndResponseResult.ReportStatus == Convert.ToByte("6"))
                            {
                                //Check For In Review Report Status
                                if (ObjRequestAndResponseResult.ReviewStatus == Convert.ToByte("1"))
                                {
                                    FinalPrintSavedReportSection += "<div class='div' style='background: red;'><table style='margin-left: auto; margin-right: auto;'>";
                                    FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-text'>" + Convert.ToString(SFinalProductName) + "</td></tr>";
                                    FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-text'>" + sOrderStatus + "</td></tr>";
                                    FinalPrintSavedReportSection += "<tr style='height: 20px;'><td class='font-text'></td></tr>";
                                    FinalPrintSavedReportSection += "</table></div>";
                                }
                                //For Pending Report Status
                                else
                                {
                                    FinalPrintSavedReportSection += "<div class='div' style='background: #0b55c4;'><table style='margin-left: auto; margin-right: auto;'>";
                                    FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-text'>" + Convert.ToString(SFinalProductName) + "</td></tr>";
                                    FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-numeric'>!</td></tr>";
                                    FinalPrintSavedReportSection += "<tr style='height: 20px;'><td class='font-text'>PENDING</td></tr>";
                                    FinalPrintSavedReportSection += "</table></div>";
                                }
                            }

                            //For Complete Report Status
                            else if (ObjRequestAndResponseResult.ReportStatus == Convert.ToByte("2"))
                            {
                                //Check For In Review Report Status
                                if (ObjRequestAndResponseResult.ReviewStatus == Convert.ToByte("1"))
                                {
                                    FinalPrintSavedReportSection += "<div class='div' style='background: red;'><table style='margin-left: auto; margin-right: auto;'>";
                                    FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-text'>" + Convert.ToString(SFinalProductName) + "</td></tr>";
                                    FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-text'>" + sOrderStatus + "</td></tr>";
                                    FinalPrintSavedReportSection += "<tr style='height: 20px;'><td class='font-text'></td></tr>";
                                    FinalPrintSavedReportSection += "</table></div>";
                                }

                                //For Complete Report Status
                                else
                                {
                                    FinalPrintSavedReportSection += "<div class='div' style='background: #6CC525;'><table style='margin-left: auto; margin-right: auto;'>";
                                    FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-text'>" + Convert.ToString(SFinalProductName) + "</td></tr>";
                                    FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-text'>" + sOrderStatus + "</td></tr>";
                                    FinalPrintSavedReportSection += "<tr style='height: 20px;'><td class='font-text'></td></tr>";
                                    FinalPrintSavedReportSection += "</table></div>";
                                }
                            }

                            //For Incomplete Report Status
                            else if (ObjRequestAndResponseResult.ReportStatus == Convert.ToByte("3"))
                            {
                                FinalPrintSavedReportSection += "<div class='div' style='background: #E68C13;'><table style='margin-left: auto; margin-right: auto;'>";
                                FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-text'>" + Convert.ToString(SFinalProductName) + "</td></tr>";
                                FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-numeric'>?</td></tr>";
                                FinalPrintSavedReportSection += "<tr style='height: 20px;'><td class='font-text'>RECORDS</td></tr>";
                                FinalPrintSavedReportSection += "</table></div>";
                            }
                        }

                        //For PS2,SSR,ESS HTML Response
                        else if (ProductCode == "PS2" || ProductCode == "SSR" || ProductCode == "ESS")
                        {
                            //For Pending Report Status
                            if (ObjRequestAndResponseResult.ReportStatus == Convert.ToByte("1") || ObjRequestAndResponseResult.ReportStatus == Convert.ToByte("6"))
                            {
                                //Check For In Review Report Status
                                if (ObjRequestAndResponseResult.ReviewStatus == Convert.ToByte("1"))
                                {
                                    FinalPrintSavedReportSection += "<div class='div' style='background: red;'><table style='margin-left: auto; margin-right: auto;'>";
                                    FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-text'>" + Convert.ToString(SFinalProductName) + "</td></tr>";
                                    FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-text'><img alt='view' height='35px' width='35px' src='https://test.intelifi.com/testProd4/content/themes/base/images/magnify.png' /></td></tr>";
                                    FinalPrintSavedReportSection += "<tr style='height: 20px;'><td class='font-text'>VIEW REPORT</td></tr>";
                                    FinalPrintSavedReportSection += "</table></div>";
                                }
                                //For Pending Report Status
                                else
                                {
                                    FinalPrintSavedReportSection += "<div class='div' style='background: #0b55c4;'><table style='margin-left: auto; margin-right: auto;'>";
                                    FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-text'>" + Convert.ToString(SFinalProductName) + "</td></tr>";
                                    FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-numeric'>!</td></tr>";
                                    FinalPrintSavedReportSection += "<tr style='height: 20px;'><td class='font-text'>PENDING</td></tr>";
                                    FinalPrintSavedReportSection += "</table></div>";
                                }
                            }

                            //For Complete Report Status
                            else if (ObjRequestAndResponseResult.ReportStatus == Convert.ToByte("2"))
                            {
                                //Check For In Review Report Status
                                if (ObjRequestAndResponseResult.ReviewStatus == Convert.ToByte("1"))
                                {
                                    FinalPrintSavedReportSection += "<div class='div' style='background: red;'><table style='margin-left: auto; margin-right: auto;'>";
                                    FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-text'>" + Convert.ToString(SFinalProductName) + "</td></tr>";
                                    FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-text'><img alt='view' height='35px' width='35px' src='https://test.intelifi.com/testProd4/content/themes/base/images/magnify.png' /></td></tr>";
                                    FinalPrintSavedReportSection += "<tr style='height: 20px;'><td class='font-text'>VIEW REPORT</td></tr>";
                                    FinalPrintSavedReportSection += "</table></div>";
                                }

                                //For Complete Report Status
                                else
                                {
                                    FinalPrintSavedReportSection += "<div class='div' style='background: #6CC525;'><table style='margin-left: auto; margin-right: auto;'>";
                                    FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-text'>" + Convert.ToString(SFinalProductName) + "</td></tr>";
                                    FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-text'><img alt='view' height='35px' width='35px' src='https://test.intelifi.com/testProd4/content/themes/base/images/magnify.png' /></td></tr>";
                                    FinalPrintSavedReportSection += "<tr style='height: 20px;'><td class='font-text'>VIEW REPORT</td></tr>";
                                    FinalPrintSavedReportSection += "</table></div>";
                                }
                            }

                            //For Incomplete Report Status
                            else if (ObjRequestAndResponseResult.ReportStatus == Convert.ToByte("3"))
                            {
                                FinalPrintSavedReportSection += "<div class='div' style='background: #E68C13;'><table style='margin-left: auto; margin-right: auto;'>";
                                FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-text'>" + Convert.ToString(SFinalProductName) + "</td></tr>";
                                FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-numeric'>?</td></tr>";
                                FinalPrintSavedReportSection += "<tr style='height: 20px;'><td class='font-text'>VIEW REPORT</td></tr>";
                                FinalPrintSavedReportSection += "</table></div>";
                            }
                        }

                        //For all main products.
                        else
                        {
                            //For Pending Report Status
                            if (ObjRequestAndResponseResult.ReportStatus == Convert.ToByte("1") || ObjRequestAndResponseResult.ReportStatus == Convert.ToByte("6"))
                            {
                                //Check For In Review Report Status
                                if (ObjRequestAndResponseResult.ReviewStatus == Convert.ToByte("1"))
                                {
                                    FinalPrintSavedReportSection += "<div class='div' style='background: red;'><table style='margin-left: auto; margin-right: auto;'>";
                                    FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-text'>" + Convert.ToString(SFinalProductName) + "</td></tr>";
                                    FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-numeric'>" + Countflag + "</td></tr>";
                                    FinalPrintSavedReportSection += "<tr style='height: 20px;'><td class='font-text'>RECORDS</td></tr>";
                                    FinalPrintSavedReportSection += "</table></div>";
                                }
                                //For Pending Report Status
                                else
                                {
                                    FinalPrintSavedReportSection += "<div class='div' style='background: #0b55c4;'><table style='margin-left: auto; margin-right: auto;'>";
                                    FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-text'>" + Convert.ToString(SFinalProductName) + "</td></tr>";
                                    FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-numeric'>!</td></tr>";
                                    FinalPrintSavedReportSection += "<tr style='height: 20px;'><td class='font-text'>PENDING</td></tr>";
                                    FinalPrintSavedReportSection += "</table></div>";
                                }
                            }

                           //For Complete Report Status
                            else if (ObjRequestAndResponseResult.ReportStatus == Convert.ToByte("2"))
                            {
                                //Check For In Review Report Status
                                if (ObjRequestAndResponseResult.ReviewStatus == Convert.ToByte("1"))
                                {
                                    FinalPrintSavedReportSection += "<div class='div' style='background: red;'><table style='margin-left: auto; margin-right: auto;'>";
                                    FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-text'>" + Convert.ToString(SFinalProductName) + "</td></tr>";
                                    FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-numeric'>" + Countflag + "</td></tr>";
                                    FinalPrintSavedReportSection += "<tr style='height: 20px;'><td class='font-text'>RECORDS</td></tr>";
                                    FinalPrintSavedReportSection += "</table></div>";
                                }

                                //For Complete Report Status
                                else
                                {
                                    FinalPrintSavedReportSection += "<div class='div' style='background: #6CC525;'><table style='margin-left: auto; margin-right: auto;'>";
                                    FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-text'>" + Convert.ToString(SFinalProductName) + "</td></tr>";
                                    FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-numeric'>" + Countflag + "</td></tr>";
                                    FinalPrintSavedReportSection += "<tr style='height: 20px;'><td class='font-text'>RECORDS</td></tr>";
                                    FinalPrintSavedReportSection += "</table></div>";
                                }
                            }

                            //For Incomplete Report Status
                            else if (ObjRequestAndResponseResult.ReportStatus == Convert.ToByte("3"))
                            {
                                FinalPrintSavedReportSection += "<div class='div' style='background: #E68C13;'><table style='margin-left: auto; margin-right: auto;'>";
                                FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-text'>" + Convert.ToString(SFinalProductName) + "</td></tr>";
                                FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-numeric'>?</td></tr>";
                                FinalPrintSavedReportSection += "<tr style='height: 20px;'><td class='font-text'>RECORDS</td></tr>";
                                FinalPrintSavedReportSection += "</table></div>";
                            }
                        }
                    }


                    //When response data found blank 
                    else
                    {
                        string ProductName = ObjRequestAndResponseResult.ProductDisplayName;
                        if (ProductCode == "CCR1")
                        {
                            ProductName = "County of " + ObjStateCounty.LiveRunnerCounty + " , " + ObjStateCounty.StateCode;
                        }
                        else if (ProductCode == "SCR")
                        {
                            ProductName = "State Of " + ObjRequestAndResponseResult.StateName;
                        }
                        else if (ProductCode == "FCR")
                        {
                            //string FCRJurisdiction = string.Empty;
                            tblOrderSearchedDataJurdictionInfo objtblOrderSearchedDataJurdictionInfo = new tblOrderSearchedDataJurdictionInfo();
                            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                            {
                                objtblOrderSearchedDataJurdictionInfo = Dx.tblOrderSearchedDataJurdictionInfos.Where(db => db.FkOrderId == ObjOrderId).FirstOrDefault();
                            }
                            ProductName = "Federal of " + objtblOrderSearchedDataJurdictionInfo.SearchedCountyName;
                        }

                        // FOR ADD .... SUB STRING IF PRODUCT NAME TEXT IS TOO LONG
                        int IProductLength = ProductName.Length;
                        string sValue = string.Empty;
                        string SFinalProductName = string.Empty;
                        if (IProductLength >= 25)
                        {
                            sValue = ProductName.Substring(0, 22);
                            SFinalProductName = sValue + "...";
                        }
                        else
                        {
                            SFinalProductName = ProductName;
                        }

                        //For Pending Report Status

                        FinalPrintSavedReportSection += "<div class='div' style='background: #0b55c4;'><table style='margin-left: auto; margin-right: auto;'>";
                        FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-text'>" + Convert.ToString(SFinalProductName) + "</td></tr>";
                        FinalPrintSavedReportSection += "<tr style='height: 35px;'><td class='font-numeric'>!</td></tr>";
                        FinalPrintSavedReportSection += "<tr style='height: 20px;'><td class='font-text'>PENDING</td></tr>";
                        FinalPrintSavedReportSection += "</table></div>";
                    }
                }

                FinalPrintSavedReportSection += "</td></tr>";

                //string ReportFor = "<div><table class=\"fnt\" cellpadding=\"0\" cellspacing=\"0\" width=\"98%\"><tr><td style=\"width: 15%\">Intelifi Inc.</td>"
                     //+ "<td style=\"width: 2%\">|</td><td style=\"width: 37%\">Report for " + FullSearchedName + "</td>"
                     //+ "<td style=\"width: 30%; text-align: right\">Report #" + ObjProc_Get_RequestAndResponseResult.First().OrderNo + "</td>"
                     //+ "<td style=\"width: 2%;\">&nbsp;</td></tr><tr><td colspan=\"5\"><hr style=\"color: Black; border: 2px solid #000; margin-top: 2px; margin-bottom: 4px;\" /></td>"
                     //+ "</tr></table></div>";


                string topHeader = "<img src=\" " + ApplicationPath.GetApplicationPath() + "Content/themes/base/images/IntelifiPowerToKnow_small.png\" alt=\"\" /><div style=\"margin-left:20px;\"><br />"
                     + "<table cellpadding=\"2\" cellspacing=\"2\" style=\"margin: 0px; border:1px solid #ccc; margin-bottom:10px;\" width=\"100%\">"
                     + "<tr><td style=\"width: 100%;\"><table id=\"tblsummary\" style=\"border-collapse: collapse;\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" align=\"center\">"
                     + "<tr>"
                     + "<td bgcolor=\"#ffffff\" class=\"Align_Center\" align=\"center\"><table cellpadding=\"0\" cellspacing=\"0\" style=\"margin: 0px\" width=\"100%\" >"
                     + "<tr><td width=\"33%\" valign=\"top\"><table cellpadding=\"2\" cellspacing=\"2\" style=\"margin: 0px\" width=\"100%\">"
                     + "<tr style=\"font-size:12px !important;font-weight:bold;\"><td colspan=\"3\" style='text-decoration: underline;'>Report Information</td></tr><tr style=\"font-size:12px !important;\"><td class=\"Report_Sub_info_left\" style=\"width: 35%\">Order</td>"
                     + "<td class=\"Report_Sub_info_middle\"></td><td class=\"Report_Sub_info_right\">" + ObjProc_Get_RequestAndResponseResult.First().OrderNo + "</td>"
                     + "</tr><tr style=\"font-size:12px !important;\"><td class=\"Report_Sub_info_left\">Date</td><td class=\"Report_Sub_info_middle\"></td><td class=\"Report_Sub_info_right\">" + ObjProc_Get_RequestAndResponseResult.First().OrderDt.ToString("MM/dd/yyyy HH:mm:ss") + "</td>"
                     + "</tr><tr style=\"font-size:12px !important;\"><td class=\"Report_Sub_info_left\">Package</td><td class=\"Report_Sub_info_middle\"></td><td class=\"Report_Sub_info_right\">" + DisplyProductCode + "</td>"
                     + "</tr></table></td><td width=\"33%\" valign=\"top\"><table cellpadding=\"2\" cellspacing=\"2\" style=\"margin: 0px\" width=\"100%\">"
                     + "<tr style=\"font-size:12px !important;font-weight:bold;\"><td style='font-size:12px; text-decoration: underline;' colspan=\"3\">Company Information</td></tr><tr style=\"font-size:12px !important;\"><td class=\"Report_Sub_info_left\" style=\"width: 27%\">Company</td>"
                     + "<td class=\"Report_Sub_info_middle\"></td><td class=\"Report_Sub_info_right\">" + ObjCompanyDetails.First().CompanyName + "</td></tr>"
                     + "<tr style=\"font-size:12px !important;\"><td class=\"Report_Sub_info_left\">Location</td><td class=\"Report_Sub_info_middle\"></td><td class=\"Report_Sub_info_right\">" + ObjCompanyDetails.First().city + ", " + ObjCompanyDetails.First().statecode + "</td></tr>"
                     + "<tr style=\"font-size:12px !important;\"><td class=\"Report_Sub_info_left\">User</td><td class=\"Report_Sub_info_middle\"></td><td class=\"Report_Sub_info_right\">" + ObjCompanyDetails.First().lastname + ", " + ObjCompanyDetails.First().firstname + "</td></tr></table></td>"
                     + "<td width=\"33%\" valign=\"top\"><table cellpadding=\"2\" cellspacing=\"2\" style=\"margin: 0px\" width=\"100%\"><tr style=\"font-size:12px !important;font-weight:bold;\"><td colspan=\"3\" style='text-decoration: underline;'>Applicant Information</td></tr>"
                     + "<tr style=\"font-size:12px !important;\"><td class=\"Report_Sub_info_left\" style=\"width: 50%\">Applicant</td><td class=\"Report_Sub_info_middle\"></td><td class=\"Report_Sub_info_right\">" + FullSearchedName + "</td></tr>"
                     + "<tr style=\"font-size:12px !important;\"><td class=\"Report_Sub_info_left\">State</td><td class=\"Report_Sub_info_middle\"></td><td class=\"Report_Sub_info_right\">" + ObjProc_Get_RequestAndResponseResult.First().SearchedStateCode + "</td></tr>"
                     + "<tr style=\"font-size:12px !important;\"><td class=\"Report_Sub_info_left\">DOB</td><td class=\"Report_Sub_info_middle\"></td><td class=\"Report_Sub_info_right\">" + ObjProc_Get_RequestAndResponseResult.First().SearchedDob + "</td></tr></table>"
                     + "</td></tr></table></td></tr>"
                     + "</table></td></tr>";


                //string horzLine = "<hr style=\"color: Gray; margin-top: 2px; margin-bottom: 4px;\" />"
                //     + "<hr style=\"color: Gray; margin-top: 2px; margin-bottom: 4px;\" /><br /><br />";



                //string refcode = ObjProc_Get_RequestAndResponseResult.First().SearchedTrackingRef == "" ? "NA" : ObjProc_Get_RequestAndResponseResult.First().SearchedTrackingRef;
               // string notes = ObjProc_Get_RequestAndResponseResult.First().SearchedTrackingNotes == "" ? "NA" : ObjProc_Get_RequestAndResponseResult.First().SearchedTrackingNotes;

                string bottomFooter = "<tr id=\"rowReportAndApplicantInfo_another\"><td style=\"width: 100%;\">"
                    + "<table id=\"Table1\" style=\"border-collapse: collapse;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">"
                    + "<tr><td>"
                    //+ "<td bgcolor=\"#dadada\" align=\"left\"><table cellpadding=\"2\" cellspacing=\"2\">"
                    //+ "<tr><td><span class=\"lbl_viewreport_bottom\">Reference Code: </span>&nbsp; <span class=\"lbl_viewreport_bottom_alt\">" + refcode + "</span></td>"
                    //+ "<td style=\"width: 50px;\">&nbsp;</td><td><span class=\"lbl_viewreport_bottom\">Notes: </span>&nbsp; <span class=\"lbl_viewreport_bottom_alt\">"
                    //+ "" + notes + "</span></td></tr></table></td><td width=\"11\" height=\"11\" bgcolor=\"#dadada\" style=\"border-right: 0px solid #cdcdcd;\"></td></tr>"
                    //+ "</table><table style=\"border-collapse: collapse;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"95%\" align=\"center\"><tr><td><div><div style=\"width:100%;margin-top:1%;\"><div style=\"float:left;margin-left:2.5%;font-family: Microsoft Sans Serif; font-size: 14px;\">Match Meter</div><div style=\"float:left;margin-left:0.4%;\"> <img id=\"ImgDescription-0\" onmouseover=\"tooltips('@message', 'Match Meter');\" onmouseout=\"exit();\" src=\" " + ApplicationPath.GetApplicationPath() + "Content/themes/base/images/drugtesting4help.png\" alt=\"Description\" style=\"height: 16px; width: 16px; border-width: 0px; cursor: pointer;\"></div>"
                    //+ "</div><div style=\"width:100%;float:right;margin-top:0.5%;\">"
                    //+ "<div style=\"float:left;margin-left:2.5%;margin-top:0.2%;\"><img id=\"exactmatch\" src=\" " + ApplicationPath.GetApplicationPath() + "Content/themes/base/images/ico_match_green.jpg\"></div><div style=\"float:left;margin-left:0.5%;font-family: Microsoft Sans Serif; font-size: 14px;\">Exact Match</div><div style=\"float:left;margin-left:4.2%;margin-top:0.2%;\"><img id=\"partialmatch\" src=\" " + ApplicationPath.GetApplicationPath() + "Content/themes/base/images/ico_red_yellow.png\"></div><div style=\"float:left;margin-left:0.5%;font-family: Microsoft Sans Serif; font-size: 14px;\">Partial Match</div><div style=\"float:left;margin-left:4.2%;margin-top:0.2%;\"><img id=\"mismatch\" src=\" " + ApplicationPath.GetApplicationPath() + "Content/themes/base/images/ico_red_match.png\"></div><div style=\"float:left;margin-left:0.5%;font-family: Microsoft Sans Serif; font-size: 14px;\">Mismatch</div><div style=\"float:left;margin-left:4.2%;font-family: Microsoft Sans Serif; font-size: 14px;\"><img id=\"nomatch\" src=\" " + ApplicationPath.GetApplicationPath() + "Content/themes/base/images/ico_darkgrey_match.png\"> Decisioned</div>"

                    + "<table style=\"border-collapse: collapse;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"><tr><td>"
                    + "<hr /><p style=\"text-align: justify;\"><b>Disclaimer:</b> Information obtained from Intelifi and Emerge Solutions is provided 'as is' and should not be the sole determiner in the evaluation of this individual "
                    + "(Human error in compiling this information is possible). All other factors, references and current situations should be considered.The information in this report is derived "
                    + "from records in accordance with the Fair Credit Reporting Act (FCRA, Public Law 91-508, Title VI). This disclaimer includes without limitation, implied warranties "
                    + "of merchantability and fitness for a particular purpose. Intelifi makes no warranty as to the quality accuracy, completeness, timeliness and validity of the data or "
                    + "its compliance with applicable law. Intelifi is not liable for any claims or damages arising out of or relating to the use of this data. This information should not "
                    + "be used in legal proceedings. Furthermore, Intelifi policy requires purchasers and all users of this report to comply with all applicable law (including the FCRA and GLBA) "
                    + "relating to the access and use of this data while abiding by their obligations, and remaining in compliance of the FCRA. For technical assistance, please contact Intelifi Support at 888-409-1819."
                    + "</p><hr /><table width=\"100%\"><tr><td width=\"89px\"><img src=\"https://emerge.intelifi.com/Content/themes/base/images/emerge_disclaimer.png\" alt=\"\" /></td>"
                    + "<td width=\"285px\" align=\"left\">Intelifi | Powered by Emerge</td><td align=\"right\"></tr> </table>"
                    + "</td></tr></table></td></tr></table></td></tr></table></div></body></html>";

                //htmlfile = topHeader + ReportFor + horzLine + FinalPrintSavedReportSection + bottomFooter;
                topHead = "<html xmlns='http://www.w3.org/1999/xhtml'><head><style type='text/css'>.para {margin: 2px;}.div {width: 160px;float: left;height: 100px;border-radius: 5px;margin: 1px;}"
                    + ".font-text {font-size: 11px;text-transform: uppercase;font-weight: 700;color: #fff;font-family: arial,sans-serif;text-align: center;}"
                    + ".font-numeric {font-size: 30px;text-transform: uppercase;font-weight: 500;color: #fff;font-family: arial,sans-serif;text-align: center;}</style></head><body>";

                htmlfile = topHead + topHeader + FinalPrintSavedReportSection + bottomFooter;

            }
            catch //(Exception SD)
            {

            }

            return htmlfile;
        }
        #endregion


        /// <summary>
        /// Function To Display Response Image for pending report.
        /// </summary>
        /// <returns></returns>
        public Control DisplayManualResponse(Proc_Get_RequestAndResponseResult ObjRequestAndResponseResult)
        {
            StringBuilder sbManualResponseData = new StringBuilder();
            string manualresponse = string.Empty;
            if (ObjRequestAndResponseResult.ManualResponse != string.Empty)
            {
                if (ObjRequestAndResponseResult.ManualResponse.Contains("<img alt='ResponseImage'"))
                {
                    sbManualResponseData = sbManualResponseData.Append(ObjRequestAndResponseResult.ManualResponse);
                }
                else
                {
                    sbManualResponseData = sbManualResponseData.Append(ObjRequestAndResponseResult.ManualResponse.Replace("?", " "));
                }
                manualresponse += "<div id='Pending_message' align='left'><div style='clear:both;'></div><div>" + sbManualResponseData + "</div></div>";
            }
            if (ObjRequestAndResponseResult.ResponseFileName != string.Empty)
            {
                //    string FilePath = "";
                int checkext = -1;
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    tblOrderResponseData ObjOrderresponseData = DX.tblOrderResponseDatas.Where(d => d.pkOrderResponseId == ObjRequestAndResponseResult.pkOrderResponseId).FirstOrDefault();
                    if (ObjOrderresponseData != null)
                    {
                        string FileName = ObjOrderresponseData.ResponseFileName;
                        string Ext = Path.GetExtension(FileName).ToLower();
                        if (Ext == ".htm" || Ext == ".html" || Ext == ".pdf")
                        {
                            checkext = 1;
                        }
                        else
                        {
                            checkext = 2;
                        }
                        if (!string.IsNullOrEmpty(FileName))
                        {
                            manualresponse += "<div align='left'><div style='clear:both;'></div><div><a href=\"DownloadAttachment/?FPathTitle=" + FileName + "," + ObjRequestAndResponseResult.ProductCode + "\">Click here to view attachment..</a></div></div>";
                        }
                        //FilePath = ApplicationPath.GetApplicationPath() + "Resources/Upload/ResponseImages/" + FileName;
                        //manualresponse += "<div align='left'><div style='clear:both;'></div><div><a class='easyui-linkbutton' href='#' onclick=\"addTab('" + ObjRequestAndResponseResult.ProductCode + "','" + FilePath + "','" + checkext + "'); \">Click here to view attachment..</a></div></div>";
                    }
                }
            }
            Label ObjLabel = new Label();
            ObjLabel.Text = manualresponse;
            return ObjLabel;
        }
        private string BindDrugsReports(int ObjOrderId)
        {
            string htmlfile = "";
            //string ProductCode = "";
            //string TopScript = "";
            string topHeader = "";
            string topMenu = "";
            string bottomFooter = "";
            string horzLine = "";
            //string BottomScript = "";
            //string EmergeAssistScript = "";
            string ReportFor = "";
            //lblReportType.Text = "";
            //int ReviewQueryString = -1;
            ObjBALOrders = new BALOrders();
            //int IsAutoReviewSent = 0;
            //BALEmergeReview ObjBALEmergeReview = new BALEmergeReview();
            Dictionary<string, string> dictCodepkOrderResponseId = new Dictionary<string, string>();
            //string DisplyProductCode = "";

            try
            {
                //  List<Proc_Get_RequestAndResponseResult> ObjProc_Get_RequestAndResponseResult = ObjBALOrders.GetRequestAndResponseByOrderId(ObjOrderId);
                List<Proc_Get_DrugTestOrderDetailsResult> ObjProc_Get_RequestAndResponseResult = ObjBALOrders.GetDrugTestOrderDetails(ObjOrderId);
                Control ObjControl_eachReport = new Control();
                string SearchedName = string.Empty;
                if (ObjProc_Get_RequestAndResponseResult != null)
                {
                    SearchedName = GenerateApplicantName(ObjProc_Get_RequestAndResponseResult.First().SearchedFirstName, ObjProc_Get_RequestAndResponseResult.First().SearchedFirstName, ObjProc_Get_RequestAndResponseResult.First().SearchedFirstName, ObjProc_Get_RequestAndResponseResult.First().SearchedFirstName);
                    string FullSearchedName = SearchedName.Replace("'", "");
                    //string FileName = ObjProc_Get_RequestAndResponseResult.First().OrderNo.ToString();
                    if (ObjProc_Get_RequestAndResponseResult.Count > 0)
                    {
                        ViewState["ReportName"] = ObjProc_Get_RequestAndResponseResult.First().SearchedFirstName + "_" + ObjProc_Get_RequestAndResponseResult.First().SearchedFirstName;
                    }
                    else
                    {
                        ViewState["ReportName"] = string.Empty;
                    }

                    BALCompany ObjBALCompany = new BALCompany();

                    List<Proc_Get_CompanyDetailByMemberShipIdResult> ObjCompanyDetails = ObjBALCompany.GetCompanyDetailByMemberShipIdResult(ObjProc_Get_RequestAndResponseResult.First().fkUserId);



                    //int CountReviewReports = 0;

                    //#region Reports Binding
                    Control ObjControl_each = new Control();


                    ReportFor = "<div><table class=\"fnt\" cellpadding=\"0\" cellspacing=\"0\" width=\"98%\"><tr><td style=\"width: 15%\">Intelifi Inc.</td>"
                        + "<td style=\"width: 2%\">|</td><td style=\"width: 37%\">Report for " + FullSearchedName + "</td>"
                        + "<td style=\"width: 30%; text-align: right\">Report #" + ObjProc_Get_RequestAndResponseResult.First().OrderNo + "</td>"
                        + "<td style=\"width: 2%;\">&nbsp;</td></tr><tr><td colspan=\"5\"><hr style=\"color: Black; border: 2px solid #000; margin-top: 2px; margin-bottom: 4px;\" /></td>"
                        + "</tr></table></div>";
                    //topMenu = "<div style=\"display: block; background: rgb(0, 120, 189) none repeat scroll 0% 0%; margin: auto; width: 95.5%;height: 48px;\"><table><tr id='tr_Menu'><td colspan='2' class='menu_seperator' valign='bottom'> <table width='100%' cellpadding='0' cellspacing='0' style='padding: 0px 0px 0px 0px; height: 35px;'><tbody><tr><td class='Center_ImgMVC' valign='bottom' style='border:none;'><div style='width: 100%; padding-left: 10px; float: left; vertical-align: bottom;'><ul id='tabsDemo1'><li id='liReport' class='current'><a> " + FullSearchedName + "<span id='spanRpt'></span></a></li></ul><ul id='tabsDemo2'></ul></div></td></tr></tbody></table></td></tr></table></div>";
                    topMenu = "";
                    topHeader = "<img src=\" " + ApplicationPath.GetApplicationPath() + "Content/themes/base/images/IntelifiPowerToKnow_small.png\" alt=\"\" /><div class=\"m\">"
                        + "<table cellpadding=\"2\" cellspacing=\"2\" style=\"margin: 0px\" width=\"100%\">"
                        + "<tr><td style=\"width: 100%;\"><table id=\"tblsummary\" style=\"border-collapse: collapse;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"95%\" align=\"center\">"
                        + "<tr>"
                        + "<td bgcolor=\"#dadada\" class=\"Align_Center\" align=\"center\"><table cellpadding=\"0\" cellspacing=\"0\" style=\"margin: 0px\" width=\"100%\" class=\"Report_Combined_Info\">"
                        + "<tr><td width=\"33%\" valign=\"top\"><table cellpadding=\"2\" cellspacing=\"2\" style=\"margin: 0px\" width=\"100%\">"
                        + "<tr style=\"font-size:12px !important;font-weight:bold;\"><td colspan=\"3\">Report Information</td></tr><tr style=\"font-size:12px !important;\"><td  class=\"Report_Sub_info_left\" style=\"width: 35%\">Order Id</td>"
                        + "<td  class=\"Report_Sub_info_middle\"></td><td   class=\"Report_Sub_info_right\">" + ObjProc_Get_RequestAndResponseResult.First().OrderNo + "</td>"
                        + "</tr><tr style=\"font-size:12px !important;\"><td  class=\"Report_Sub_info_left\">Search Date</td><td  class=\"Report_Sub_info_middle\"></td><td  class=\"Report_Sub_info_right\">" + ObjProc_Get_RequestAndResponseResult.First().OrderDt.ToString("MM/dd/yyyy HH:mm:ss") + "</td>"
                        + "</tr><tr style=\"font-size:12px !important;\"><td class=\"Report_Sub_info_left\">Report Type</td><td  class=\"Report_Sub_info_middle\"></td><td  class=\"Report_Sub_info_right\">" + "DT5" + "</td>"
                        + "</tr></table></td><td width=\"33%\" valign=\"top\"><table cellpadding=\"2\" cellspacing=\"2\" style=\"margin: 0px\" width=\"100%\">"
                        + "<tr style=\"font-size:12px !important;font-weight:bold;\"><td colspan=\"3\">Company Information</td></tr><tr style=\"font-size:12px !important;\"><td  class=\"Report_Sub_info_left\" style=\"width: 27%\">Company</td>"
                        + "<td class=\"Report_Sub_info_middle\"></td><td class=\"Report_Sub_info_right\">" + ObjCompanyDetails.First().CompanyName + "</td></tr>"
                        + "<tr style=\"font-size:12px !important;\"><td  class=\"Report_Sub_info_left\">Location</td><td  class=\"Report_Sub_info_middle\"></td><td  class=\"Report_Sub_info_right\">" + ObjCompanyDetails.First().city + ", " + ObjCompanyDetails.First().statecode + "</td></tr>"
                        + "<tr style=\"font-size:12px !important;\"><td  class=\"Report_Sub_info_left\">User</td><td  class=\"Report_Sub_info_middle\"></td><td  class=\"Report_Sub_info_right\">" + ObjCompanyDetails.First().lastname + ", " + ObjCompanyDetails.First().firstname + "</td></tr></table></td>"
                        + "<td width=\"33%\" valign=\"top\"><table cellpadding=\"2\" cellspacing=\"2\" style=\"margin: 0px\" width=\"100%\"><tr style=\"font-size:12px !important;font-weight:bold;\"><td colspan=\"3\">Applicant Information</td></tr>"
                        + "<tr style=\"font-size:12px !important;font-weight:bold;\"><td  class=\"Report_Sub_info_left\" style=\"width: 50%\">Applicant Name</td><td  class=\"Report_Sub_info_middle\"></td><td  class=\"Report_Sub_info_right\">" + ObjCompanyDetails.First().firstname + "   " + ObjCompanyDetails.First().lastname + "</td></tr>"
                        + "<tr style=\"font-size:12px !important;\"><td  class=\"Report_Sub_info_left\">State</td><td  class=\"Report_Sub_info_middle\"></td><td  class=\"Report_Sub_info_right\">" + ObjProc_Get_RequestAndResponseResult.First().StateCode + "</td></tr>"
                        //  + "<tr><td class=\"Report_Sub_info_left\">Date of birth</td><td class=\"Report_Sub_info_middle\"></td><td class=\"Report_Sub_info_right\">" + ObjProc_Get_RequestAndResponseResult.First(). + "</td></tr>
                      + "</table>"
                        + "</td></tr></table></td></tr>"
                        + "<tr>"
                        + "</table></td></tr>";


                    horzLine = "<hr style=\"color: Gray; margin-top: 2px; margin-bottom: 4px;\" />"
                        + "<hr style=\"color: Gray; margin-top: 2px; margin-bottom: 4px;\" /><br /><br />";



                    //   string refcode = ObjProc_Get_RequestAndResponseResult.First(). == "" ? "NA" : ObjProc_Get_RequestAndResponseResult.First().SearchedFirstName;
                    string notes = ObjProc_Get_RequestAndResponseResult.First().PendingOrderUpdateNote == "" ? "NA" : ObjProc_Get_RequestAndResponseResult.First().SearchedFirstName;

                    bottomFooter = "<tr id=\"rowReportAndApplicantInfo_another\"><td style=\"width: 100%;\">"
                        + "<table id=\"Table1\" style=\"border-collapse: collapse;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"95%\" align=\"center\">"
                        + "<tr><td width=\"11\" height=\"11\" bgcolor=\"#dadada\" style=\"border-left: 0px solid #cdcdcd;\"></td>"
                        + "<td bgcolor=\"#dadada\" align=\"left\"><table cellpadding=\"2\" cellspacing=\"2\">"
                        // + "<tr><td><span class=\"lbl_viewreport_bottom\">Reference Code: </span>&nbsp; <span class=\"lbl_viewreport_bottom_alt\"></span></td>"
                        + "<tr>"
                        + "<td style=\"width: 50px;\">&nbsp;</td><td><span class=\"lbl_viewreport_bottom\">Notes: </span>&nbsp; <span class=\"lbl_viewreport_bottom_alt\">"
                        + "" + notes + "</span></td></tr></table></td><td width=\"11\" height=\"11\" bgcolor=\"#dadada\" style=\"border-right: 0px solid #cdcdcd;\"></td></tr>"
                        + "</table>"
                        + "<table style=\"border-collapse: collapse;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"95%\" align=\"center\"><tr><td>"
                        + "<p style=\"text-align: justify;\"><b>Disclaimer:</b> Information obtained from Intelifi and Emerge Solutions is provided 'as is' and should not be the sole determiner in the evaluation of this individual "
                        + "(Human error in compiling this information is possible). All other factors, references and current situations should be considered.The information in this report is derived "
                        + "from records in accordance with the Fair Credit Reporting Act (FCRA, Public Law 91-508, Title VI). This disclaimer includes without limitation, implied warranties "
                        + "of merchantability and fitness for a particular purpose. Intelifi makes no warranty as to the quality accuracy, completeness, timeliness and validity of the data or "
                        + "its compliance with applicable law. Intelifi is not liable for any claims or damages arising out of or relating to the use of this data. This information should not "
                        + "be used in legal proceedings. Furthermore, Intelifi policy requires purchasers and all users of this report to comply with all applicable law (including the FCRA andGLBA) "
                        + "relating to the access and use of this data while abiding by their obligations, and remaining in compliance of the FCRA. For technical assistance, please contact Intelifi Support at 888-409-1819."
                        + "</p><hr /><table width=\"100%\"><tr><td width=\"89px\"><img src=\"https://emerge.intelifi.com/Content/themes/base/images/emerge_disclaimer.png\" alt=\"\" /></td>"
                        + "<td width=\"285px\" align=\"left\">Intelifi | Powered by Emerge</td><td align=\"right\">"
                        + "</td></td></tr></table></td></tr></table></div>";
                    //src=\"https://www.intelifi.com/emerge/Resources/Images/EMEditWhell.png\"

                    //ObjControl_eachReport.Controls.AddAt(0, CreateLabelControl(ReportFor));
                    ObjControl_eachReport.Controls.AddAt(0, CreateLabelControl(topMenu));
                    ObjControl_eachReport.Controls.AddAt(1, CreateLabelControl(topHeader));


                    ObjControl_eachReport.Controls.AddAt(2, ObjControl_each);

                    ObjControl_eachReport.Controls.AddAt(3, CreateLabelControl(bottomFooter));
                    ObjControl_eachReport.Controls.AddAt(4, CreateLabelControl(horzLine));
                    //phAllrecords.Controls.Add(ObjControl_eachReport);
                    phReports.Controls.Add(ObjControl_eachReport);
                    //   #endregion

                    Session["dictCodepkOrderResponseId"] = dictCodepkOrderResponseId;
                    htmlfile = GetReportHtml();
                    if (Request.QueryString["SP"].ToString() == "SV")
                    {
                        phReports.Controls.Clear();
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["EM"]))
                    {
                        phReports.Controls.Clear();
                    }
                }
            }
            catch// (Exception)
            {

            }
            finally
            {
                ObjBALOrders = null;
            }
            return htmlfile;
        }

        private Control HelpEmergeAssist(string EmergeAssistScript)
        {
            Label ObjLabel = new Label();
            ObjLabel.Text = EmergeAssistScript;
            return ObjLabel;
        }

        private string GetOrderNo(int OrderID)
        {
            string orderNo = string.Empty;
            using (EmergeDALDataContext dal = new EmergeDALDataContext())
            {
                var order = dal.tblOrders.Where(o => o.pkOrderId == OrderID).Select(o => o.OrderNo).FirstOrDefault();
                if (order != null)
                {
                    orderNo = Convert.ToString(order);
                }
                else
                {
                    order = dal.tblOrders_Archieves.Where(o => o.pkOrderId == OrderID).Select(o => o.OrderNo).FirstOrDefault();
                    if (order != null)
                    {
                        orderNo = Convert.ToString(order);
                    }
                }
            }
            return orderNo;
        }


        private Control CreateLabelControl(string TopScript)
        {
            Label ObjLabel = new Label();
            //ObjLabel.ID = "Dynamic";
            ObjLabel.Text = TopScript;
            return ObjLabel;
        }

        /// <summary>
        /// Function To Display Pending Message
        /// </summary>
        /// <returns></returns>
        public Control DisplayPendingMessage()
        {
            Label ObjLabel = new Label();
            ObjLabel.Text = "<div id='Pending_message' align='left' style='font-size:14px'><div style='clear:both;'>STATUS : PENDING<hr align='left' id='Pending_Message_Hr' /></div><div>        Your report is being processed manually.  <br />   when your report is ready, you will be notified by email and your report will appear in your archived reports.   <br />  Emerge support Team <span class='Pending_Message_Separator'> |</span> <a href='mailto:support@intelifi.com' class='Pending_Message_Link'>support@intelifi.com</a> <span class='Pending_Message_Separator'> |</span> 888.409.1819 </div></div>";
            return ObjLabel;
        }

        /// <summary>
        /// Function To Display In-Complete Message.
        /// </summary>
        /// <returns></returns>
        public Control DisplayInCompleteMessage()
        {
            Label ObjLabel = new Label();
            ObjLabel.Text = "<div id='Pending_message' align='left'><div style='clear:both;'>STATUS : <span style='color:red'>Incomplete</span> <hr align='left' id='Pending_Message_Hr' /></div><div>  There was an error while attempting to complete your order.<br/>  Please contact support for more information on this matter.    <br />  Emerge support Team <span class='Pending_Message_Separator'> |</span> <a href='mailto:support@intelifi.com' class='Pending_Message_Link'>support@intelifi.com</a> <span class='Pending_Message_Separator'> |</span> 888.409.1819 </div></div>";
            return ObjLabel;
        }

        /// <summary>
        /// Function To Fetch Response For All Reports Other Than MVR
        /// </summary>
        /// <param name="ObjRequestAndResponseResult"></param>
        private Control FetchResponse(Proc_Get_RequestAndResponseResult ObjRequestAndResponseResult, bool IsReview, string orderStatusValue)
        {
            Control ObjControl = new Control();
            try
            {
                if (ObjRequestAndResponseResult != null)
                {
                    int OrderDetailId = Convert.ToInt32(ObjRequestAndResponseResult.pkOrderDetailId.ToString());
                    int OrderId = Convert.ToInt32(ObjRequestAndResponseResult.fkOrderId.ToString());
                   // string hdnReviewDetail = string.Empty;
                    bool Objdynamic = GetResponse(ObjRequestAndResponseResult.ResponseData);
                    if (Objdynamic)
                    {
                        using (Xml ObjXml = new Xml())
                        {
                            string ProductCode = "";

                            ObjXml.DocumentContent = ObjRequestAndResponseResult.ResponseData;
                            //int IsReportForReview = 0;

                            ObjXml.DocumentContent = (ObjRequestAndResponseResult.ResponseData == null ? "" : ObjRequestAndResponseResult.ResponseData.ToString());
                            if (ObjXml.DocumentContent.ToString() == "<XML_INTERFACE>ZZZ</XML_INTERFACE>" && ObjRequestAndResponseResult.IsFirstPendingRequest == true)
                            {
                                string FullRequest = Utility.GetLastRequest(ObjRequestAndResponseResult.RequestData);
                                string[] Request1 = FullRequest.Split('?');
                                string Uri_string = Request1[0];
                                string Content_string = string.Empty;
                                if (Request1.Length > 1)
                                {
                                    Content_string = Request1[1];
                                }
                                string UriResponse = "<XML_INTERFACE>ZZZ</XML_INTERFACE>";
                                while (UriResponse == "<XML_INTERFACE>ZZZ</XML_INTERFACE>" && MicroBiltEndPoint < 4)
                                {
                                    UriResponse = Utility.SendRequest(Uri_string, Content_string);
                                    MicroBiltEndPoint++;
                                    System.Threading.Thread.Sleep(4000);
                                }
                                if (UriResponse.Contains("<XML_INTERFACE>"))
                                {
                                    UpdateResponse((int)ObjRequestAndResponseResult.pkOrderResponseId, UriResponse);
                                    BALOrders ObjBALOrders = new BALOrders();
                                    try
                                    {
                                        ObjBALOrders.CheckResponseandUpdateStatus(ObjRequestAndResponseResult.ProductCode, ObjRequestAndResponseResult.fkOrderId, UriResponse, ObjRequestAndResponseResult.pkOrderDetailId, false, string.Empty, string.Empty, string.Empty, false);
                                    }
                                    catch //(Exception)
                                    {

                                    }
                                    finally
                                    {
                                        ObjBALOrders = null;
                                    }
                                }
                                if (UriResponse == "<XML_INTERFACE>ZZZ</XML_INTERFACE>")
                                {
                                    if (MicroBiltEndPoint == 4)
                                    {
                                        ObjControl.Controls.Add(DisplayPendingMessage());
                                    }
                                }
                                else
                                {
                                    if (GetResponse(UriResponse) == true)
                                    {
                                        if (ObjRequestAndResponseResult.IsLiveRunner == true)
                                            ProductCode = ObjRequestAndResponseResult.ProductCode.Trim() != "RCX" ? ObjRequestAndResponseResult.ProductCode.Trim() + "LiveRunner" : ObjRequestAndResponseResult.ProductCode.Trim(); /* XSLT for live runner report*/
                                        else
                                            ProductCode = ObjRequestAndResponseResult.ProductCode;
                                        ObjXml.DocumentContent = UriResponse;
                                        ObjXml.TransformSource = "~/Resources/XSLT/" + (ProductCode == "SNS" && Request.Url.ToString().Contains("Control") ? ProductCode + "_CONTROL" : ProductCode) + ".xslt";
                                        ObjControl.Controls.Add(ObjXml);
                                    }

                                }
                                BALGeneral.UpdateFirstPendingRequestStatus(ObjRequestAndResponseResult.pkOrderDetailId);
                            }
                            else
                            {
                                string pcode = "";
                                if (ObjRequestAndResponseResult.ReportStatus == Convert.ToByte("2"))
                                {
                                    string ModifiedResponse = string.Empty;
                                    ModifiedResponse = ObjRequestAndResponseResult.ResponseData;

                                    if (ObjRequestAndResponseResult.IsLiveRunner == true)
                                    {
                                        if (ObjRequestAndResponseResult.ProductCode.Trim() == "CCR1" || ObjRequestAndResponseResult.ProductCode.Trim() == "CCR2" || ObjRequestAndResponseResult.ProductCode.Trim() == "SCR")
                                        {
                                            pcode = "RCX";
                                        }
                                        else
                                        {
                                            pcode = ObjRequestAndResponseResult.ProductCode.Trim();
                                        }
                                        ProductCode = ObjRequestAndResponseResult.ProductCode.Trim() != "RCX" ? ObjRequestAndResponseResult.ProductCode.Trim() + "LiveRunner" : ObjRequestAndResponseResult.ProductCode.Trim(); /* XSLT for live runner report*/

                                    }
                                    else
                                    {
                                        ProductCode = ObjRequestAndResponseResult.ProductCode;
                                        pcode = ProductCode;
                                    }
                                    /* Match meter for NCR1 Report */


                                    string ncr1Response = "";
                                    bool IsNCR1Report = false;
                                    int pkNCR1OrderResponseId = 0;
                                    BALEmergeReview ObjBALEmergeReview = new BALEmergeReview();
                                    bool IsMatchMeter = false;

                                    if (ObjRequestAndResponseResult.ProductCode.ToLower() == "ps")
                                    {
                                        if (ObjRequestAndResponseResult.ResponseData != "")
                                        {
                                            PSResponse = ObjRequestAndResponseResult.ResponseData;
                                            vwpsStatus = 1;
                                        }
                                    }

                                    /* Match meter for NCR1 Report */
                                    if (ProductCode == "NCR1" || ProductCode == "SCR" || ProductCode == "NCR+" || ProductCode == "RCX" || ProductCode == "NCR4" || ProductCode == "CCR1LiveRunner" || ProductCode == "CCR2LiveRunner")
                                    {

                                        ModifiedResponse = AddOrderDetailToXMLResponse_0(ProductCode, ObjXml.DocumentContent, ObjRequestAndResponseResult.pkOrderDetailId);
                                        ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);
                                        ModifiedResponse = AddOrderDetailToXMLResponse_1(ProductCode, ObjXml.DocumentContent, ObjRequestAndResponseResult.pkOrderDetailId);

                                        BALOrders ObjBALOrders = new BALOrders();
                                        ObjBALOrders.UpdateOrderResponse(ObjRequestAndResponseResult.pkOrderDetailId, ModifiedResponse);


                                        // #region MatchMeter

                                        if (ObjRequestAndResponseResult.ProductCode.ToLower() == "ncr1")
                                        {
                                            ncr1Response = ObjRequestAndResponseResult.ResponseData;
                                            IsMatchMeter = ObjRequestAndResponseResult.IsMatchMeter;
                                            if (ncr1Response != "")
                                            {
                                                IsNCR1Report = true;
                                            }
                                            pkNCR1OrderResponseId = ObjRequestAndResponseResult.pkOrderResponseId;
                                        }
                                        if (IsNCR1Report == true && pkNCR1OrderResponseId != 0)
                                        {
                                            if (IsMatchMeter == false)
                                            {
                                                bool IsAdmin = true;
                                                if (Roles.IsUserInRole(Page.User.Identity.Name, "SystemAdmin") || Roles.IsUserInRole(Page.User.Identity.Name, "SupportTechnician") || Roles.IsUserInRole(Page.User.Identity.Name, "SupportManager"))
                                                {
                                                    IsAdmin = true;
                                                }
                                                else
                                                {
                                                    IsAdmin = false;
                                                }
                                                if (vwpsStatus == 1)
                                                {
                                                    string updatedResponse = ObjBALEmergeReview.matchmeter(ObjRequestAndResponseResult.SearchedFirstName, ObjRequestAndResponseResult.SearchedLastName, ObjRequestAndResponseResult.SearchedMiddleInitial, ObjRequestAndResponseResult.SearchedDob, PSResponse, ncr1Response, pkNCR1OrderResponseId, ObjRequestAndResponseResult.ProductCode, OrderDetailId, OrderId, IsAdmin, ObjRequestAndResponseResult.SearchedZipCode, ObjRequestAndResponseResult.SearchedStateCode, ObjRequestAndResponseResult.SearchedCity, ObjRequestAndResponseResult.SearchedStreetAddress + " " + ObjRequestAndResponseResult.SearchedStreetName, ObjRequestAndResponseResult.IsAddressUpdated);
                                                    ModifiedResponse = updatedResponse;
                                                }
                                                else
                                                {
                                                    string updatedResponse = ObjBALEmergeReview.NCR1matchmeter(ObjRequestAndResponseResult.SearchedFirstName, ObjRequestAndResponseResult.SearchedLastName, ObjRequestAndResponseResult.SearchedMiddleInitial, ObjRequestAndResponseResult.SearchedDob, ObjRequestAndResponseResult.SearchedZipCode, ObjRequestAndResponseResult.SearchedStateCode, ObjRequestAndResponseResult.SearchedCity, ObjRequestAndResponseResult.SearchedStreetAddress + " " + ObjRequestAndResponseResult.SearchedStreetName, ncr1Response, pkNCR1OrderResponseId, ObjRequestAndResponseResult.ProductCode, OrderDetailId, OrderId, IsAdmin);
                                                    ModifiedResponse = updatedResponse;
                                                }
                                            }
                                        }
                                        // #endregion

                                    }

                                    // #region Main XML Filteration

                                    bool IsShowRawData = true;
                                    XMLFilteration ObjXMLFilteration = new XMLFilteration();
                                    try
                                    {
                                        string UpdatedResponse = ObjXMLFilteration.GetFilteredXML(ProductCode, pcode, ModifiedResponse, ObjRequestAndResponseResult.pkOrderDetailId, ObjRequestAndResponseResult.WatchListStatus, DateTime.Now, ObjRequestAndResponseResult.IsLiveRunner, out IsShowRawData);
                                        ModifiedResponse = UpdatedResponse;

                                        // #region For 7 Year States
                                        if (Roles.IsUserInRole(Page.User.Identity.Name, "SystemAdmin") || Roles.IsUserInRole(Page.User.Identity.Name, "SupportTechnician") || Roles.IsUserInRole(Page.User.Identity.Name, "SupportManager"))
                                        {
                                            IsShowRawData = true;
                                        }
                                        else
                                        {
                                            IsShowRawData = false;
                                        }
                                        // #endregion For 7 Year States

                                        ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);
                                        if (ProductCode == "NCR1" || ProductCode == "SCR" || ProductCode == "NCR+" || ProductCode == "RCX" || ProductCode == "NCR4" || ProductCode == "CCR1LiveRunner" || ProductCode == "CCR2LiveRunner")
                                        {

                                            ModifiedResponse = AddOrderDetailToXMLResponse_0(ProductCode, ObjXml.DocumentContent, ObjRequestAndResponseResult.pkOrderDetailId);
                                            ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);

                                        }
                                    }
                                    catch
                                    {
                                        ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);
                                    }

                                    // #endregion Main XML Filteration


                                    string[] Role = new string[2];
                                    if (Session["UserRoles"] != null)
                                    {
                                        Role = (string[])(Session["UserRoles"]);
                                    }

                                    if (orderStatusValue != null)
                                    {

                                    }
                                    else
                                    {
                                        orderStatusValue = "ViewReports";
                                    }
                                    string UrlforUserReportdata = Convert.ToString(orderStatusValue);
                                    int _bitUserViewXSLTCheck = 0;
                                    if (UrlforUserReportdata.Contains("UserView_ViewReportReview") || UrlforUserReportdata.Contains("UserView_ViewReport"))
                                    {
                                        _bitUserViewXSLTCheck = 0;

                                    }
                                    else
                                    {
                                        _bitUserViewXSLTCheck = 1;

                                    }


                                    string xsltContent = string.Empty;

                                    if (IsReview)//New check for mvc
                                    {

                                        if (Role != null)
                                        {

                                            if (Role.Contains("SystemAdmin") || Role.Contains("SupportTechnician") || Role.Contains("SupportManager"))//Request.Url.AbsoluteUri.ToLower().Contains("control")) //&& ObjRequestAndResponseResult.ReviewStatus != 0 //03Jun2011//Page.Request.Url.AbsoluteUri.ToLower().Contains("admin") ||
                                            {
                                                if (_bitUserViewXSLTCheck > 0)
                                                {
                                                    // int 232 issue for prinit
                                                    // for admin side 
                                                    ModifiedResponse = AddOrderDetailToXMLResponse(ProductCode, ObjXml.DocumentContent, ObjRequestAndResponseResult.pkOrderDetailId);
                                                    ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);

                                                    xsltContent = File.ReadAllText(Server.MapPath("~/Resources/AdminXSLT/" + (ProductCode == "SNS" && Request.Url.ToString().Contains("Control") ? ProductCode + "_CONTROL" : ProductCode) + ".xslt"));

                                                }
                                                else
                                                {

                                                    // for user View 

                                                    ModifiedResponse = GetFinalXMLResponse(ProductCode, ObjXml.DocumentContent);
                                                    ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);
                                                    xsltContent = File.ReadAllText(Server.MapPath("~/Resources/ReviewXSLT/" + (ProductCode == "SNS" && Request.Url.ToString().Contains("Control") ? ProductCode + "_CONTROL" : ProductCode) + ".xslt"));

                                                }


                                            }
                                            else
                                            {
                                                ModifiedResponse = GetFinalXMLResponse(ProductCode, ObjXml.DocumentContent);
                                                ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);


                                                xsltContent = File.ReadAllText(Server.MapPath("~/Resources/ReviewXSLT/" + (ProductCode == "SNS" && Request.Url.ToString().Contains("Control") ? ProductCode + "_CONTROL" : ProductCode) + ".xslt"));

                                            }
                                        }


                                    }
                                    else
                                    {
                                        xsltContent = File.ReadAllText(Server.MapPath("~/Resources/XSLT/" + (ProductCode == "SNS" && Request.Url.ToString().Contains("Control") ? ProductCode + "_CONTROL" : ProductCode) + ".xslt"));
                                    }


                                    if (ProductCode == "NCR2")
                                    {
                                        ModifiedResponse = ModifiedResponse.Replace("&lt;td&gt;&lt;b&gt;Provider:&lt;/b&gt; Fastrax&lt;/td&gt;", "&lt;td&gt;&lt;b&gt;Provider:&lt;/b&gt; Emerge&lt;/td&gt;");

                                    }

                                    ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);

                                    string output = String.Empty;
                                    using (StringReader srt = new StringReader(xsltContent)) // xslInput is a string that contains xsl
                                    using (StringReader sri = new StringReader(ObjXml.DocumentContent)) // xmlInput is a string that contains xml
                                    {
                                        using (XmlReader xrt = XmlReader.Create(srt))
                                        using (XmlReader xri = XmlReader.Create(sri))
                                        {
                                            XslCompiledTransform xslt = new XslCompiledTransform();
                                            xslt.Load(xrt);
                                            using (StringWriter sw = new StringWriter())
                                            using (XmlWriter xwo = XmlWriter.Create(sw, xslt.OutputSettings)) // use OutputSettings of xsl, so it can be output as HTML
                                            {
                                                xslt.Transform(xri, xwo);
                                                output = sw.ToString();
                                            }
                                        }
                                    }
                                    string strApplicantDob = ObjRequestAndResponseResult.SearchedDob;
                                    string ApplicantDob = "";
                                    if (strApplicantDob != "")
                                    {
                                        string[] spDate = strApplicantDob.Split('/');
                                        ApplicantDob = spDate[2] + "-" + spDate[0] + "-" + spDate[1];
                                    }
                                    Label ObjLabel = new Label();
                                    ObjLabel.Text = output.Replace("UserDOB", ApplicantDob);
                                    ObjControl = ObjLabel;
                                }
                                else if (ObjRequestAndResponseResult.ReportStatus == Convert.ToByte("1"))
                                {
                                    ObjControl = DisplayPendingMessage();
                                }
                                else if (ObjRequestAndResponseResult.ReportStatus == Convert.ToByte("3"))
                                {
                                    ObjControl = DisplayInCompleteMessage();
                                }
                                else if (ObjRequestAndResponseResult.ReportStatus == Convert.ToByte("4"))
                                {
                                    ObjControl = DisplayCancelMessage(ObjRequestAndResponseResult);
                                }
                            }
                        }
                    }
                    else
                    {
                        Label ObjLabel = new Label();
                        ObjLabel.Text = (ObjRequestAndResponseResult.ReportStatus == Convert.ToByte("1") ? ((Label)DisplayPendingMessage()).Text.ToString() : ObjRequestAndResponseResult.ResponseData.ToString());
                        ObjControl = ObjLabel;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ObjControl;
        }

        private string FetchResponse_SavedReport(Proc_Get_RequestAndResponseResult ObjRequestAndResponseResult)
        {
            Control ObjControl = new Control();
            string ModifiedResponse_SavedReport = string.Empty;

            try
            {
                if (ObjRequestAndResponseResult != null)
                {
                    int OrderDetailId = Convert.ToInt32(ObjRequestAndResponseResult.pkOrderDetailId.ToString());
                    int OrderId = Convert.ToInt32(ObjRequestAndResponseResult.fkOrderId.ToString());
                    //string hdnReviewDetail = string.Empty;
                    bool Objdynamic = GetResponse(ObjRequestAndResponseResult.ResponseData);
                    if (Objdynamic == true)
                    {
                        using (Xml ObjXml = new Xml())
                        {
                            string ProductCode = "";

                            ObjXml.DocumentContent = ObjRequestAndResponseResult.ResponseData;
                            //int IsReportForReview = 0;

                            ObjXml.DocumentContent = (ObjRequestAndResponseResult.ResponseData == null ? "" : ObjRequestAndResponseResult.ResponseData.ToString());
                            if (ObjXml.DocumentContent.ToString() == "<XML_INTERFACE>ZZZ</XML_INTERFACE>" && ObjRequestAndResponseResult.IsFirstPendingRequest == true)
                            {
                                string FullRequest = Utility.GetLastRequest(ObjRequestAndResponseResult.RequestData);
                                string[] Request1 = FullRequest.Split('?');
                                string Uri_string = Request1[0];
                                string Content_string = string.Empty;
                                if (Request1.Length > 1)
                                {
                                    Content_string = Request1[1];
                                }
                                string UriResponse = "<XML_INTERFACE>ZZZ</XML_INTERFACE>";
                                while (UriResponse == "<XML_INTERFACE>ZZZ</XML_INTERFACE>" && MicroBiltEndPoint < 4)
                                {
                                    UriResponse = Utility.SendRequest(Uri_string, Content_string);
                                    MicroBiltEndPoint++;
                                    System.Threading.Thread.Sleep(4000);
                                }
                                if (UriResponse.Contains("<XML_INTERFACE>"))
                                {
                                    UpdateResponse((int)ObjRequestAndResponseResult.pkOrderResponseId, UriResponse);
                                    BALOrders ObjBALOrders = new BALOrders();
                                    try
                                    {
                                        ObjBALOrders.CheckResponseandUpdateStatus(ObjRequestAndResponseResult.ProductCode, ObjRequestAndResponseResult.fkOrderId, UriResponse, ObjRequestAndResponseResult.pkOrderDetailId, false, string.Empty, string.Empty, string.Empty, false);
                                    }
                                    catch// (Exception)
                                    {

                                    }
                                    finally
                                    {
                                        ObjBALOrders = null;
                                    }
                                }
                                if (UriResponse == "<XML_INTERFACE>ZZZ</XML_INTERFACE>")
                                {
                                    if (MicroBiltEndPoint == 4)
                                    {
                                        ObjControl.Controls.Add(DisplayPendingMessage());
                                    }
                                }
                                else
                                {
                                    if (GetResponse(UriResponse) == true)
                                    {
                                        if (ObjRequestAndResponseResult.IsLiveRunner == true)
                                            ProductCode = ObjRequestAndResponseResult.ProductCode.Trim() != "RCX" ? ObjRequestAndResponseResult.ProductCode.Trim() + "LiveRunner" : ObjRequestAndResponseResult.ProductCode.Trim(); /* XSLT for live runner report*/
                                        else
                                            ProductCode = ObjRequestAndResponseResult.ProductCode;
                                        ObjXml.DocumentContent = UriResponse;
                                        ObjXml.TransformSource = "~/Resources/XSLT/" + (ProductCode == "SNS" && Request.Url.ToString().Contains("Control") ? ProductCode + "_CONTROL" : ProductCode) + ".xslt";
                                        ObjControl.Controls.Add(ObjXml);
                                    }

                                }
                                BALGeneral.UpdateFirstPendingRequestStatus(ObjRequestAndResponseResult.pkOrderDetailId);
                            }
                            else
                            {
                                string pcode = "";
                                if (ObjRequestAndResponseResult.ReportStatus == Convert.ToByte("2"))
                                {
                                    string ModifiedResponse = string.Empty;
                                    ModifiedResponse = ObjRequestAndResponseResult.ResponseData;

                                    if (ObjRequestAndResponseResult.IsLiveRunner == true)
                                    {
                                        if (ObjRequestAndResponseResult.ProductCode.Trim() == "CCR1" || ObjRequestAndResponseResult.ProductCode.Trim() == "CCR2" || ObjRequestAndResponseResult.ProductCode.Trim() == "SCR")
                                        {
                                            pcode = "RCX";
                                        }
                                        else
                                        {
                                            pcode = ObjRequestAndResponseResult.ProductCode.Trim();
                                        }
                                        ProductCode = ObjRequestAndResponseResult.ProductCode.Trim() != "RCX" ? ObjRequestAndResponseResult.ProductCode.Trim() + "LiveRunner" : ObjRequestAndResponseResult.ProductCode.Trim(); /* XSLT for live runner report*/

                                    }
                                    else
                                    {
                                        ProductCode = ObjRequestAndResponseResult.ProductCode;
                                        pcode = ProductCode;
                                    }
                                    /* Match meter for NCR1 Report */


                                    string ncr1Response = "";
                                    bool IsNCR1Report = false;
                                    int pkNCR1OrderResponseId = 0;
                                    BALEmergeReview ObjBALEmergeReview = new BALEmergeReview();
                                    bool IsMatchMeter = false;

                                    if (ObjRequestAndResponseResult.ProductCode.ToLower() == "ps")
                                    {
                                        if (ObjRequestAndResponseResult.ResponseData != "")
                                        {
                                            PSResponse = ObjRequestAndResponseResult.ResponseData;
                                            vwpsStatus = 1;
                                        }
                                    }

                                    /* Match meter for NCR1 Report */
                                    if (ProductCode == "NCR1" || ProductCode == "SCR" || ProductCode == "NCR+" || ProductCode == "RCX" || ProductCode == "NCR4" || ProductCode == "CCR1LiveRunner" || ProductCode == "CCR2LiveRunner")
                                    {

                                        ModifiedResponse = AddOrderDetailToXMLResponse_0(ProductCode, ObjXml.DocumentContent, ObjRequestAndResponseResult.pkOrderDetailId);
                                        ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);
                                        ModifiedResponse = AddOrderDetailToXMLResponse_1(ProductCode, ObjXml.DocumentContent, ObjRequestAndResponseResult.pkOrderDetailId);

                                        BALOrders ObjBALOrders = new BALOrders();
                                        ObjBALOrders.UpdateOrderResponse(ObjRequestAndResponseResult.pkOrderDetailId, ModifiedResponse);


                                        // #region MatchMeter

                                        if (ObjRequestAndResponseResult.ProductCode.ToLower() == "ncr1")
                                        {
                                            ncr1Response = ObjRequestAndResponseResult.ResponseData;
                                            IsMatchMeter = ObjRequestAndResponseResult.IsMatchMeter;
                                            if (ncr1Response != "")
                                            {
                                                IsNCR1Report = true;
                                            }
                                            pkNCR1OrderResponseId = ObjRequestAndResponseResult.pkOrderResponseId;
                                        }
                                        if (IsNCR1Report == true && pkNCR1OrderResponseId != 0)
                                        {
                                            if (IsMatchMeter == false)
                                            {
                                                bool IsAdmin = true;
                                                if (Roles.IsUserInRole(Page.User.Identity.Name, "SystemAdmin") || Roles.IsUserInRole(Page.User.Identity.Name, "SupportTechnician") || Roles.IsUserInRole(Page.User.Identity.Name, "SupportManager"))
                                                {
                                                    IsAdmin = true;
                                                }
                                                else
                                                {
                                                    IsAdmin = false;
                                                }
                                                if (vwpsStatus == 1)
                                                {
                                                    string updatedResponse = ObjBALEmergeReview.matchmeter(ObjRequestAndResponseResult.SearchedFirstName, ObjRequestAndResponseResult.SearchedLastName, ObjRequestAndResponseResult.SearchedMiddleInitial, ObjRequestAndResponseResult.SearchedDob, PSResponse, ncr1Response, pkNCR1OrderResponseId, ObjRequestAndResponseResult.ProductCode, OrderDetailId, OrderId, IsAdmin, ObjRequestAndResponseResult.SearchedZipCode, ObjRequestAndResponseResult.SearchedStateCode, ObjRequestAndResponseResult.SearchedCity, ObjRequestAndResponseResult.SearchedStreetAddress + " " + ObjRequestAndResponseResult.SearchedStreetName, ObjRequestAndResponseResult.IsAddressUpdated);
                                                    ModifiedResponse = updatedResponse;
                                                }
                                                else
                                                {
                                                    string updatedResponse = ObjBALEmergeReview.NCR1matchmeter(ObjRequestAndResponseResult.SearchedFirstName, ObjRequestAndResponseResult.SearchedLastName, ObjRequestAndResponseResult.SearchedMiddleInitial, ObjRequestAndResponseResult.SearchedDob, ObjRequestAndResponseResult.SearchedZipCode, ObjRequestAndResponseResult.SearchedStateCode, ObjRequestAndResponseResult.SearchedCity, ObjRequestAndResponseResult.SearchedStreetAddress + " " + ObjRequestAndResponseResult.SearchedStreetName, ncr1Response, pkNCR1OrderResponseId, ObjRequestAndResponseResult.ProductCode, OrderDetailId, OrderId, IsAdmin);
                                                    ModifiedResponse = updatedResponse;
                                                }
                                            }
                                        }
                                        // #endregion
                                    }

                                    // #region Main XML Filteration

                                    bool IsShowRawData = true;
                                    XMLFilteration ObjXMLFilteration = new XMLFilteration();
                                    try
                                    {
                                        string UpdatedResponse = ObjXMLFilteration.GetFilteredXML(ProductCode, pcode, ModifiedResponse, ObjRequestAndResponseResult.pkOrderDetailId, ObjRequestAndResponseResult.WatchListStatus, DateTime.Now, ObjRequestAndResponseResult.IsLiveRunner, out IsShowRawData);
                                        ModifiedResponse = UpdatedResponse;
                                        ModifiedResponse_SavedReport = Convert.ToString(ModifiedResponse);
                                        // #region For 7 Year States
                                        if (Roles.IsUserInRole(Page.User.Identity.Name, "SystemAdmin") || Roles.IsUserInRole(Page.User.Identity.Name, "SupportTechnician") || Roles.IsUserInRole(Page.User.Identity.Name, "SupportManager"))
                                        {
                                            IsShowRawData = true;
                                        }
                                        else
                                        {
                                            IsShowRawData = false;
                                        }
                                        // #endregion For 7 Year States

                                        ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);
                                        if (ProductCode == "NCR1" || ProductCode == "SCR" || ProductCode == "NCR+" || ProductCode == "RCX" || ProductCode == "NCR4" || ProductCode == "CCR1LiveRunner" || ProductCode == "CCR2LiveRunner")
                                        {

                                            ModifiedResponse = AddOrderDetailToXMLResponse_0(ProductCode, ObjXml.DocumentContent, ObjRequestAndResponseResult.pkOrderDetailId);
                                            ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);
                                            ModifiedResponse_SavedReport = Convert.ToString(ModifiedResponse);
                                        }
                                    }
                                    catch
                                    {
                                        ModifiedResponse_SavedReport = Convert.ToString(ModifiedResponse);
                                        ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);
                                    }

                                    // #endregion Main XML Filteration

                                    if (ObjRequestAndResponseResult.ReviewStatus > 0 || ProductCode == "NCR1")//New check for mvc
                                    {
                                        ModifiedResponse = GetFinalXMLResponse(ProductCode, ObjXml.DocumentContent);
                                        ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);
                                        ObjXml.TransformSource = "~/Resources/ReviewXSLT/" + (ProductCode == "SNS" && Request.Url.ToString().Contains("Control") ? ProductCode + "_CONTROL" : ProductCode) + ".xslt";
                                        if (ProductCode == "NCR1")
                                        {
                                            string xsltContent = "";
                                            xsltContent = File.ReadAllText(Server.MapPath("~/Resources/ReviewXSLT/" + (ProductCode == "SNS" && Request.Url.ToString().Contains("Control") ? ProductCode + "_CONTROL" : ProductCode) + ".xslt"));
                                            //ObjControl.Controls.Add(ObjXml);
                                            string output = String.Empty;
                                            using (StringReader srt = new StringReader(xsltContent)) // xslInput is a string that contains xsl
                                            using (StringReader sri = new StringReader(ObjXml.DocumentContent)) // xmlInput is a string that contains xml
                                            {
                                                using (XmlReader xrt = XmlReader.Create(srt))
                                                using (XmlReader xri = XmlReader.Create(sri))
                                                {
                                                    XslCompiledTransform xslt = new XslCompiledTransform();
                                                    xslt.Load(xrt);
                                                    using (StringWriter sw = new StringWriter())
                                                    using (XmlWriter xwo = XmlWriter.Create(sw, xslt.OutputSettings)) // use OutputSettings of xsl, so it can be output as HTML
                                                    {
                                                        xslt.Transform(xri, xwo);
                                                        output = sw.ToString();
                                                    }
                                                }
                                            }
                                            string strApplicantDob = ObjRequestAndResponseResult.SearchedDob;
                                            string ApplicantDob = "";
                                            if (strApplicantDob != "")
                                            {
                                                string[] spDate = strApplicantDob.Split('/');
                                                ApplicantDob = spDate[2] + "-" + spDate[0] + "-" + spDate[1];
                                            }
                                            Label ObjLabel = new Label();
                                            ObjLabel.Text = output.Replace("UserDOB", ApplicantDob);
                                            ObjControl = ObjLabel;
                                        }
                                        else
                                        {
                                            ObjControl.Controls.Add(ObjXml);
                                        }
                                    }
                                    else
                                    {
                                        ObjXml.TransformSource = "~/Resources/XSLT/" + (ProductCode == "SNS" && Request.Url.ToString().Contains("Control") ? ProductCode + "_CONTROL" : ProductCode) + ".xslt";
                                        ObjControl.Controls.Add(ObjXml);
                                    }
                                }
                                else if (ObjRequestAndResponseResult.ReportStatus == Convert.ToByte("1"))
                                {
                                    ObjControl = DisplayPendingMessage();
                                }
                                else if (ObjRequestAndResponseResult.ReportStatus == Convert.ToByte("3"))
                                {
                                    ObjControl = DisplayInCompleteMessage();
                                }
                                else if (ObjRequestAndResponseResult.ReportStatus == Convert.ToByte("4"))
                                {
                                    ObjControl = DisplayCancelMessage(ObjRequestAndResponseResult);
                                }
                            }
                        }
                    }
                    else
                    {
                        Label ObjLabel = new Label();
                        ObjLabel.Text = (ObjRequestAndResponseResult.ReportStatus == Convert.ToByte("1") ? ((Label)DisplayPendingMessage()).Text.ToString() : ObjRequestAndResponseResult.ResponseData.ToString());
                        ObjControl = ObjLabel;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ModifiedResponse_SavedReport;
        }


        // #region Final Response Editing For Users Other Than Admin

        private string GetFinalXMLResponse(string ProductCode, string ResponseXML)
        {
            string FinalResponse = "";
            string RootRecordNode = "";
            if (ProductCode.ToUpper().Trim() == "NCR1" || ProductCode.ToUpper().Trim() == "PS" || ProductCode.ToUpper().Trim() == "RCX" || ProductCode.ToUpper().Trim() == "FCR" || ProductCode.ToUpper().Trim() == "NCR+" || ProductCode.ToUpper().Trim() == "CCR1LIVERUNNER" || ProductCode.ToUpper().Trim() == "CCR2LIVERUNNER" || ProductCode.ToUpper().Trim() == "SCRLIVERUNNER")
            {
                string XML_Value = GetRequiredXml_ForRapidCourt(ResponseXML);
                XDocument xmldoc = XDocument.Parse(XML_Value);
                List<XElement> xElement = new List<XElement>();
                if (ProductCode.ToUpper().Trim() == "PS") // Changed By Himesh Kumar
                {
                    RootRecordNode = "record";

                    if (xmldoc.Descendants(RootRecordNode).Attributes("Deleted").FirstOrDefault() != null)
                    {

                        xElement = xmldoc.Descendants(RootRecordNode).Where(x => x.HasAttributes && x.Attribute("Deleted").Value == "1").ToList();
                        xElement.Remove();
                    }
                    // changes for nd-39 on 01 july 2016

                    int TrimedUpTo = ResponseXML.IndexOf("<addressinformation>");
                    StringBuilder CompleteResponse = new StringBuilder();
                    string XMLInitials = ResponseXML.Substring(0, TrimedUpTo);
                    CompleteResponse.Append(XMLInitials);
                    CompleteResponse.Append(Convert.ToString(xmldoc));
                    CompleteResponse.Append("</ieiresponse>");
                    FinalResponse = Convert.ToString(CompleteResponse);
                }
                else
                {
                    RootRecordNode = "CriminalCase";
                    if (xmldoc.Descendants().Count() > 0)
                    {

                        // changes for nd-39 on 01 july 2016
                        if (xmldoc.Descendants(RootRecordNode).Attributes("Deleted").FirstOrDefault() != null)
                        {

                            xElement = xmldoc.Descendants(RootRecordNode).Where(x => x.HasAttributes && x.Attribute("Deleted").Value == "1").ToList();
                            xElement.Remove();
                        }
                        // changes for nd-39 on 01 july 2016

                        int TrimedUpTo = ResponseXML.IndexOf("<BackgroundReportPackage>");
                        StringBuilder CompleteResponse = new StringBuilder();
                        string XMLInitials = ResponseXML.Substring(0, TrimedUpTo);
                        CompleteResponse.Append(XMLInitials);
                        CompleteResponse.Append(Convert.ToString(xmldoc));
                        CompleteResponse.Append("</BackgroundReports>");
                        FinalResponse = Convert.ToString(CompleteResponse);// Convert.ToString(CompleteResponse).Replace("&lt;", "<").Replace("&gt;", ">");
                    }
                    else
                        FinalResponse = ResponseXML;
                }
            }
            else if (ProductCode.ToUpper() == "TDR")
            {
                FinalResponse = ModifyTDRReport(ResponseXML);
            }
            else
            {
                switch (ProductCode.ToUpper())
                {
                    case "SCR":
                        RootRecordNode = "result";
                        break;
                    case "CCR1":
                        RootRecordNode = "report-incident";
                        break;
                    case "CCR2":
                        RootRecordNode = "detail";
                        break;
                    case "OFAC":
                        RootRecordNode = "response_row";
                        break;
                    case "PS2":
                        RootRecordNode = "response_row";
                        break;
                    case "SOR":
                        RootRecordNode = "offender";
                        break;
                    case "SSR":
                        RootRecordNode = "response_row";
                        break;
                }
                FinalResponse = ModifyReport(ResponseXML, RootRecordNode);
            }
            return FinalResponse;
        }

        private string ModifyTDRReport(string ResponseXML)
        {
            string FinalResponse = "";
            XDocument xmldoc = XDocument.Parse(ResponseXML);
            List<XElement> xElement = new List<XElement>();
            if (xmldoc.Descendants("address").Count() > 0 || xmldoc.Descendants("identity").Count() > 0 || xmldoc.Descendants("neighborhood").Count() > 0)
            {
                if (xmldoc.Descendants("address").Count() > 0)
                {
                    xElement = xmldoc.Descendants("address").Where(x => x.HasAttributes && x.Attribute("Deleted").Value == "1").ToList();
                    xElement.Remove();
                }
                if (xmldoc.Descendants("identity").Count() > 0)
                {
                    xElement = xmldoc.Descendants("identity").Where(x => x.HasAttributes && x.Attribute("Deleted").Value == "1").ToList();
                    xElement.Remove();
                }
                if (xmldoc.Descendants("neighborhood").Count() > 0)
                {
                    xElement = xmldoc.Descendants("neighborhood").Where(x => x.HasAttributes && x.Attribute("Deleted").Value == "1").ToList();
                    xElement.Remove();
                }
                FinalResponse = Convert.ToString(xmldoc);
            }
            else
                FinalResponse = ResponseXML;
            return FinalResponse;
        }

        private string ModifyReport(string ResponseXML, string XMLElement)
        {
            string FinalResponse = "";
            XDocument xmldoc = XDocument.Parse(ResponseXML);
            List<XElement> xElement = new List<XElement>();
            if (XMLElement != "")
            {
                if (xmldoc.Descendants(XMLElement).Count() > 0)
                {
                    xElement = xmldoc.Descendants(XMLElement).Where(x => x.HasAttributes && x.Attribute("Deleted").Value == "1").ToList();
                    xElement.Remove();
                    FinalResponse = Convert.ToString(xmldoc);
                }
                else
                    FinalResponse = ResponseXML;
            }
            else
                FinalResponse = ResponseXML;

            return FinalResponse;
        }
        //#endregion

        private string AddOrderDetailToXMLResponse(string ProductCode, string XmlResponse, int pkOrderDetailId)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(XmlResponse);
            XmlElement root = xmlDoc.DocumentElement;
           // XmlNode rootNode = xmlDoc.SelectSingleNode(root.Name.ToString());
            XmlAttribute newAttribute = xmlDoc.CreateAttribute("OrderDetailId");
            newAttribute.Value = Convert.ToString(pkOrderDetailId);
            root.Attributes.Append(newAttribute);
            return xmlDoc.OuterXml.ToString();
        }

        /// <summary>
        /// Function To Display The Product Name At the Top Of the Report
        /// </summary>
        /// <param name="ObjRequestAndResponseResult"></param>
        private void DisplayProductName(Proc_Get_RequestAndResponseResult ObjRequestAndResponseResult)
        {
            /*Label ObjLabel = new Label();
            ObjLabel.Text = ObjRequestAndResponseResult.ProductDisplayName;
            //ObjLabel.ID = "lbl" + ObjRequestAndResponseResult.pkOrderDetailId.ToString();
            ObjLabel.Font.Bold = true;
            ObjLabel.Style.Add("font-size", "20px");
            ObjLabel.Style.Add("text-align", "left");
            phReports.Controls.Add(ObjLabel);
            //phReports.Text = ObjLabel.Text;*/
        }
        private Control DisplayCancelMessage(Proc_Get_RequestAndResponseResult ObjRequestAndResponseResult)
        {
            Label ObjLabel = new Label();
            if (ObjRequestAndResponseResult != null)
            {
                ObjLabel.Text = GetCancelReasonFromVendor(ObjRequestAndResponseResult);
            }
            return ObjLabel;
        }

        private string GetCancelReasonFromVendor(Proc_Get_RequestAndResponseResult ObjRequestAndResponseResult)
        {
            string Reason = "";
            if (ObjRequestAndResponseResult != null)
            {
                switch (ObjRequestAndResponseResult.ProductCode.ToLower())
                {
                    case "ps":
                        Reason = GetRapidCourtReasonToCancelReport(ObjRequestAndResponseResult.ResponseData);
                        break;
                    case "NCR+":
                        Reason = GetRapidCourtReasonToCancelReport(ObjRequestAndResponseResult.ResponseData);
                        break;
                    case "rcx":
                        Reason = GetRapidCourtReasonToCancelReport(ObjRequestAndResponseResult.ResponseData);
                        break;
                    case "fcr":
                        Reason = GetRapidCourtReasonToCancelReport(ObjRequestAndResponseResult.ResponseData);
                        break;
                    case "ncr1":
                        Reason = GetRapidCourtReasonToCancelReport(ObjRequestAndResponseResult.ResponseData);
                        break;
                }
            }
            return Reason;
        }

        private string GetRapidCourtReasonToCancelReport(string ResponseData)
        {
            string Specific_Reason = "<pre>";
            string XML_Value = GetRequiredXml_ForRapidCourt(ResponseData);
            XDocument ObjXDocument = XDocument.Parse(XML_Value);
            var List_Reason = ObjXDocument.Descendants("Text").ToList();
            foreach (XElement ObjXElement in List_Reason)
            {
                if (ObjXElement != null)
                {
                    Specific_Reason += ObjXElement.Value;
                    break;
                }
            }
            return Specific_Reason + "</pre>";
        }

        /// <summary>
        /// Function to Cut the Unwanted Code from XML
        /// </summary>
        /// <param name="XmlResponse"></param>
        /// <returns></returns>
        public static string GetRequiredXml_ForRapidCourt(string XmlResponse)
        {
            string XmlResponse_Local = XmlResponse;
            XmlResponse_Local = XmlResponse_Local.Replace("</BackgroundReports>", "");
            int xmlstartindex = XmlResponse_Local.IndexOf("<BackgroundReportPackage>");
            XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
            return XmlResponse_Local;
        }

        private bool GetResponse(string p)
        {
            XmlDocument ObjXmlDocument = new XmlDocument();
            try
            {
                ObjXmlDocument.LoadXml(p);
                return true;
            }
            catch //(Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Function To Display the Message
        /// </summary>
        /// <param name="Message"></param>
        private void DisplayResult(string Message)
        {
            Label ObjLabel = new Label();
            ObjLabel.Text = "<br>" + Message;
            //ObjLabel.ID = "lblNoRecordFound";
            ObjLabel.Font.Bold = true;
            ObjLabel.Style.Add("font-size", "15px");
            ObjLabel.Style.Add("color", "#ff0000");
            ObjLabel.Style.Add("text-align", "center");
            //phReports.Controls.Add(ObjLabel);
        }

        /// <summary>
        /// Function To Update Response (In Case of ZZZ As Response .)
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="ObjXElement"></param>
        private void UpdateResponse(int guid, string Response)
        {
            BALOrders ObjBALOrders = new BALOrders();
            try
            {
                ObjBALOrders.UpdateResponse(guid, Response);
            }
            catch //(Exception)
            {

            }
            finally
            {
                ObjBALOrders = null;
            }
        }

        /// <summary>
        /// Function To Get MVR Response Again From The Online Services
        /// </summary>
        /// <param name="ObjRequestAndResponseResult"></param>
        private Control GetMvrResponse(Proc_Get_RequestAndResponseResult ObjRequestAndResponseResult)
        {
            Control ObjControl = new Control();
            if (ObjRequestAndResponseResult != null)
            {
                ObjRequestAndResponseResult.ResponseData = ObjRequestAndResponseResult.ResponseData.Trim();
                bool Objdynamic = GetResponse(ObjRequestAndResponseResult.ResponseData);

                if (Objdynamic)
                {
                    using (Xml ObjXml_MVR = new Xml())
                    {
                        ObjXml_MVR.DocumentContent = (ObjRequestAndResponseResult.ResponseData == null ? "" : ObjRequestAndResponseResult.ResponseData.ToString());

                        XmlDocument ObjXmlDocument = new XmlDocument();

                        ObjXmlDocument.LoadXml(ObjXml_MVR.DocumentContent);

                        XmlNode ObjXmlNode = ObjXmlDocument.DocumentElement;

                        if ((ObjRequestAndResponseResult.ReportStatus != Convert.ToByte("2") && ObjRequestAndResponseResult.ReportStatus != Convert.ToByte("3") && ObjRequestAndResponseResult.ReportStatus != Convert.ToByte("4")))
                        {
                            ObjControl = DisplayPendingMessage();
                            return ObjControl;
                        }
                        if (ObjRequestAndResponseResult.ReportStatus == Convert.ToByte("4"))
                        {
                            string Response = (ObjRequestAndResponseResult.ResponseData == null ? "" : ObjRequestAndResponseResult.ResponseData.ToString());
                            XDocument XResponse = XDocument.Parse(Response);
                            //ObjControl = DisplayCancelMessage();
                            //return ObjControl;
                            var xElement = XElement.EmptySequence.ToList();
                            xElement = XResponse.Descendants("errordescription").ToList();
                            String ErrorMsg = ""; //xElement.Value.Trim();
                            if (xElement.Count != 0)
                            {
                                ErrorMsg = xElement.FirstOrDefault().Value.Trim();
                            }
                            Label ObjLabel = new Label();
                            ObjLabel.Text = "<div id='Pending_message' align='left'><div style='clear:both;'>STATUS : <span style='color:red'>Cancelled</span><hr align='left' id='Pending_Message_Hr' /></div><div>Reason : '" + ErrorMsg + "'<br />  Emerge support Team <span class='Pending_Message_Separator'> |</span> <a href='mailto:support@intelifi.com' class='Pending_Message_Link'>support@intelifi.com</a> <span class='Pending_Message_Separator'> |</span> 888.409.1819 </div></div>";
                            ObjControl = ObjLabel;
                        }
                        //  #region Old Code To Get the Formatted MVR Response from the XML
                        if (ObjRequestAndResponseResult.ReportStatus != Convert.ToByte("4"))
                        {
                            string URL_string = ObjXmlNode.SelectSingleNode("ReportLink").ChildNodes[0].Value;

                            string[] str_Uri_Content = URL_string.Replace("_$_", "&").Split('?');

                            string Uri_string = str_Uri_Content[0].Replace("mvr.", "ptp.");

                            string Content_string = "";

                            if (str_Uri_Content[1].Contains("&amp;"))
                            {
                                Content_string = str_Uri_Content[1].Replace("&amp;", "&");
                            }
                            else
                            {
                                Content_string = str_Uri_Content[1];
                            }

                            string UriResponse = Utility.SendRequest(Uri_string, Content_string);

                            //int LengthOfResponse = UriResponse.Length;

                            int FirstHalfIndex = UriResponse.IndexOf("** END OF RECORD **");

                            UriResponse = UriResponse.Substring(0, FirstHalfIndex);

                            int startindex = UriResponse.IndexOf("<table border=\"1\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">", 300);

                            UriResponse = UriResponse.Substring(startindex);

                            UriResponse = "<pre><div style=\"font-family: Arial; font-size: 12px;\">" + UriResponse + "</div></pre><br /><div style=\"font-size: 16px;\"><center>** END OF RECORD ***</center></div>";

                            Label ObjLabel_FormatedData = new Label();
                            ObjLabel_FormatedData.Text = UriResponse + (ObjRequestAndResponseResult.ReportStatus != Convert.ToByte("2") && ObjRequestAndResponseResult.ReportStatus != Convert.ToByte("3") ? ((Label)DisplayPendingMessage()).Text.ToString() : "");
                            //ObjLabel_FormatedData.ID = "lbl_Format" + ObjRequestAndResponseResult.pkOrderDetailId.ToString();

                            ObjControl = ObjLabel_FormatedData;
                        }
                        //#endregion
                    }
                }
                else
                {
                    Label ObjLabel = new Label();
                    //ObjLabel.ID = "XML_Write";
                    ObjLabel.Text = ObjRequestAndResponseResult.ResponseData + (ObjRequestAndResponseResult.ReportStatus != Convert.ToByte("2") && ObjRequestAndResponseResult.ReportStatus != Convert.ToByte("3") ? ((Label)DisplayPendingMessage()).Text.ToString() : "");
                    ObjControl = ObjLabel;
                }
            }
            return ObjControl;
        }

        public static string ResolveWebUrl(string relativeUrl)
        {
            if (relativeUrl == null) throw new ArgumentNullException("relativeUrl");

            if (relativeUrl.Length == 0 || relativeUrl[0] == '/' || relativeUrl[0] == '\\')
                return relativeUrl;

            int idxOfScheme = relativeUrl.IndexOf(@"://", StringComparison.Ordinal);
            if (idxOfScheme != -1)
            {
                int idxOfQM = relativeUrl.IndexOf('?');
                if (idxOfQM == -1 || idxOfQM > idxOfScheme) return relativeUrl;
            }

            StringBuilder sbUrl = new StringBuilder();
            sbUrl.Append(HttpRuntime.AppDomainAppVirtualPath);
            if (sbUrl.Length == 0 || sbUrl[sbUrl.Length - 1] != '/') sbUrl.Append('/');

            // found question mark already? query string, do not touch!
            bool foundQM = false;
            bool foundSlash; // the latest char was a slash?
            if (relativeUrl.Length > 1
                && relativeUrl[0] == '~'
                && (relativeUrl[1] == '/' || relativeUrl[1] == '\\'))
            {
                relativeUrl = relativeUrl.Substring(2);
                foundSlash = true;
            }
            else foundSlash = false;
            foreach (char c in relativeUrl)
            {
                if (!foundQM)
                {
                    if (c == '?') foundQM = true;
                    else
                    {
                        if (c == '/' || c == '\\')
                        {
                            if (foundSlash) continue;
                            else
                            {
                                sbUrl.Append('/');
                                foundSlash = true;
                                continue;
                            }
                        }
                        else if (foundSlash) foundSlash = false;
                    }
                }
                sbUrl.Append(c);
            }

            return sbUrl.ToString();
        }

        private string AddOrderDetailToXMLResponse_0(string ProductCode, string XmlResponse, int pkOrderDetailId)
        {
            // #region Here we set the null DOB at te end of xml doc

            string FinalResponse = "";
            string DeletedRecords = "";
            try
            {
                string XML_Value = GetRequiredXml_ForRapidCourt(XmlResponse);
                XElement yourDoc = XElement.Parse(XML_Value);
                int RemainingNUllDOB = 0;
                int Totalcount = 0;
                List<XElement> elementToDelete = new List<XElement>();
                var emp = yourDoc.Descendants("CriminalCase").Any();
                if (emp)
                {
                    Totalcount = yourDoc.Descendants("CriminalCase").Count();/*Just testing purpose*/
                    foreach (XElement user in yourDoc.Descendants("CriminalCase"))
                    {
                        //foreach (XElement contact in user.Descendants("DemographicDetail"))
                        //{
                        var dob = user.Descendants("DateOfBirth");
                        XElement IsAdded = user.Descendants("DemographicDetail").Where(x => x.HasAttributes && x.Attribute("IsAdded").Value == "true").FirstOrDefault();
                        if (dob.Count() == 0 && IsAdded == null)
                        { RemainingNUllDOB++; }
                        else { }
                        bool hasDOB = (dob.Count() > 0);
                        if (!hasDOB && IsAdded == null)
                        {
                            elementToDelete.Add(user);
                            //user.Remove();
                        }
                        //}
                    }
                    List<XElement> elementToDelete1 = elementToDelete;
                    foreach (XElement XE in elementToDelete1)
                    {
                        DeletedRecords += Convert.ToString(XE);
                        XE.Remove();
                    }
                   // XDocument xmldoc = XDocument.Parse(XML_Value);
                    //List<XElement> xElement = new List<XElement>();

                    if (yourDoc.Descendants().Count() > 0)
                    {
                        var CriminalCaseNode = yourDoc.Descendants("CriminalCase").Any();
                        if (CriminalCaseNode)
                        {
                            XElement Node = yourDoc.Descendants("CriminalCase").Last();
                            //xElement = xmldoc.Descendants(RootRecordNode).Where(x => x.HasAttributes && x.Attribute("Deleted").Value == "1").ToList();
                            //xElement.Remove();            
                            foreach (XElement XE in elementToDelete1)
                            {
                                Node.AddAfterSelf(XE);
                            }
                        }
                        else
                        {
                            XElement Node = yourDoc.Descendants("CriminalReport").FirstOrDefault();
                            foreach (XElement XE in elementToDelete1)
                            {
                                var xelement = XE.Descendants("DemographicDetail").FirstOrDefault();
                                if (xelement.Attribute("IsAdded") == null)
                                {
                                    xelement.Add(new XAttribute("IsAdded", "false"));
                                }
                                Node.Add(XE);
                            }
                        }
                        int TrimedUpTo = XmlResponse.IndexOf("<BackgroundReportPackage>");
                        StringBuilder CompleteResponse = new StringBuilder();
                        string XMLInitials = XmlResponse.Substring(0, TrimedUpTo);
                        CompleteResponse.Append(XMLInitials);
                        CompleteResponse.Append(Convert.ToString(yourDoc));
                        CompleteResponse.Append("</BackgroundReports>");
                        FinalResponse = Convert.ToString(CompleteResponse);// Convert.ToString(CompleteResponse).Replace("&lt;", "<").Replace("&gt;", ">");
                    }
                }
                else
                {
                    FinalResponse = XmlResponse;
                }
            }
            catch
            {
                FinalResponse = XmlResponse;
            }

            //  #endregion
            return FinalResponse;
        }

        private string AddOrderDetailToXMLResponse_1(string ProductCode, string XmlResponse, int pkOrderDetailId)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(XmlResponse);
            XmlElement root = xmlDoc.DocumentElement;
           // XmlNode rootNode = xmlDoc.SelectSingleNode(root.Name.ToString());
            XmlAttribute newAttribute = xmlDoc.CreateAttribute("OrderDetailId");
            newAttribute.Value = Convert.ToString(pkOrderDetailId);
            root.Attributes.Append(newAttribute);
            return xmlDoc.OuterXml.ToString();
        }

        private void btnSendMail(string EmailTo, string CC)
        {
            bool EmailSent = false;
            string PageRequest = "";
            string MessageBody = "", emailText = "", Subject = "", Name = "";// LinkLocation = "";
            Name = FileName.Contains('_') ? FileName.Replace('_', ' ') : FileName;
            Name = Name.TrimEnd(',');
            try
            {
                BALGeneral ObjBALGeneral = new BALGeneral();
                string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;
                var emailContent = ObjBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, Convert.ToInt32(EmailTemplates.EmailedReportRequest));//"Emailed Report Request"
                Subject = emailContent.TemplateSubject.Replace("%Name%", Name);

                emailText = emailContent.TemplateContent;

                string Substring = Request.Url.AbsoluteUri.Contains("&Message") ? Request.Url.AbsoluteUri.Substring(Request.Url.AbsoluteUri.IndexOf("&Message"), (Request.Url.AbsoluteUri.Length - Request.Url.AbsoluteUri.IndexOf("&Message"))) : "";

                PageRequest = Request.Url.AbsoluteUri.Contains("&Message") ? Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.Length - Substring.Length) : Request.Url.AbsoluteUri;

                //  #region New way Bookmark

                BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                Bookmarks objBookmark = new Bookmarks();

                #region For New Style SystemTemplate
                string Bookmarkfile = string.Empty;
                string bookmarkfilepath = string.Empty;

                Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                emailText = Bookmarkfile;
                #endregion


                objBookmark.EmailContent = emailText;

                objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                objBookmark.Name = Name;
                objBookmark.LoginLink = "<a href='" + PageRequest + "'> Click here </a>";

                MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Utility.GetUID(HttpContext.Current.User.Identity.Name.ToString()));

                // #endregion
                List<Attachment> lstattach = new List<Attachment>();
                //string FilePath;
                if (ViewState["vwDirName"] != null)
                {
                    string[] args = new string[1];
                    args[0] = (ViewState["vwDirName"] != null) ? ViewState["vwDirName"].ToString() : string.Empty;
                    String[] filenames = System.IO.Directory.GetFiles(args[0]);

                    foreach (String filename in filenames)
                    {
                        using (Attachment attach = new Attachment(filename))
                        {
                            lstattach.Add(attach);
                        }
                    }

                }
                EmailSent = Utility.SendMail(EmailTo, CC, string.Empty, BALGeneral.GetGeneralSettings().SupportEmailId, MessageBody, Subject, lstattach);

                if (EmailSent)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(string), "Request", "document.location.href='" + PageRequest + "&Message=Email Sent Successfully&Type=s';", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(string), "Request", "document.location.href='" + PageRequest + "&Message=Some error occured,please try again&Type=e';", true);
                }
            }
            catch //(Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, typeof(string), "Request", "document.location.href='" + PageRequest + "&Message=Some error occured,please try again&Type=e';", true);
            }
            finally
            { //txtSaveReport.Text = ""; 
                if (ViewState["vwDirName"] != null)
                {
                    DirectoryInfo dir = new DirectoryInfo(ViewState["vwDirName"].ToString());
                    if (dir != null)
                    {
                        foreach (FileInfo file in dir.GetFiles())
                        {
                            file.Delete();
                        }
                        dir.Delete();
                    }
                }
            }
        }

        private void btnSaveReport()
        {
            try
            {
                string filename = "";
                if (ViewState["FileName"] != null)
                {
                    filename = Convert.ToString(ViewState["FileName"]);
                }
                else if (ViewState["ReportName"] != null)
                {
                    filename = Convert.ToString(ViewState["ReportName"]);
                    filename = filename + "_" + System.DateTime.Now.ToString();
                }
                string attachment = "attachment; filename=" + filename + ".html";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/html";
                Response.Write(GetReportHtml());
                Response.End();
            }
            catch
            {
            }
        }

        private void SaveZipFile()
        {
            try
            {
                string UserfileName = "";
                if (ViewState["FileName"] != null)
                {
                    UserfileName = Convert.ToString(ViewState["FileName"]);
                }
                string zipfilename = UserfileName + String.Format("{0:MMM-dd-yyyy_hh-mm-ss}", System.DateTime.Now) + ".zip";
                string filepath = Server.MapPath("~/Resources/Upload/Temp/") + zipfilename;
                // Zip the contents of the selected files

                string[] args = new string[1];
                // args[0] = Server.MapPath("zipme");

                //args[0] = Server.MapPath("~/Resources/Temp" + files);
                args[0] = (ViewState["vwDirName"] != null) ? ViewState["vwDirName"].ToString() : string.Empty;
                String[] filenames = System.IO.Directory.GetFiles(args[0]);

                using (ZipFile zip = new ZipFile())
                {
                    foreach (String filename in filenames)
                    {
                        zip.AddFile(filename, "");
                    }

                    // Send the contents of the ZIP back to the output stream
                    zip.Save(filepath);
                }

                FileInfo file = new FileInfo(filepath);
                Response.Clear();
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + zipfilename);
                Response.ContentType = "application/x-zip-compressed";
                Response.WriteFile(filepath);
                Response.End();
            }
            catch //(Exception ex)
            {
                //Response.Write(ex.ToString());
            }
            finally
            {
                if (ViewState["vwDirName"] != null)
                {
                    DirectoryInfo dir = new DirectoryInfo(ViewState["vwDirName"].ToString());

                    if (dir != null)
                    {
                        foreach (FileInfo file in dir.GetFiles())
                        {
                            file.Delete();
                        }

                        dir.Delete();
                    }
                }
            }

        }

        //public string GetReportHtml()
        //{
        //    StringWriter stw = new StringWriter();
        //    HtmlTextWriter htextw = new HtmlTextWriter(stw);
        //    Response.ContentEncoding = Encoding.Default;
        //    phReports.RenderControl(htextw);
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("<html xmlns='http://www.w3.org/1999/xhtml'>");
        //    sb.Append("<head>");
        //    sb.Append("<style type='text/css'>");
        //    sb.Append(GetCSS());
        //    sb.Append("</style>");
        //    sb.Append("</head>");
        //    sb.Append("<body>");
        //    string oldStr1 = "<span class=\"facetip\">";
        //    string oldStr2 = "<span class=\"tip\">";
        //    sb.Append(stw.ToString().Replace(oldStr1, "<span class=\"facetip\" style=\"display:none\"> ").Replace(oldStr2, "<span class=\"tip\" style=\"display:none\"> "));

        //    sb.Append("</body>");
        //    sb.Append("</html>");



        //    string ZipRegex = @"^\{?}$";
        //    if (Regex.IsMatch(sb.ToString(), ZipRegex))
        //    {
        //        // lbl.Text = "ZIP is valid!";
        //    }
        //    else
        //    {
        //        // lbl.Text = "ZIP is invalid!";
        //    }
        //    sb = sb.Replace("/emerge/Resources/images/", ApplicationPath.GetApplicationPath() + "Content/themes/base/images/");
        //    sb = sb.Replace("url('../", "url('" + ApplicationPath.GetApplicationPath() + "Resources/");

        //    //phAllrecords.Controls.Clear();

        //    return sb.ToString();

        //}
        public string GetReportHtml()
        {
            StringBuilder sb = new StringBuilder();
            using (StringWriter stw = new StringWriter())
            {
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                Response.ContentEncoding = Encoding.Default;
                phReports.RenderControl(htextw);

                sb.Append("<html xmlns='http://www.w3.org/1999/xhtml'>");
                sb.Append("<head>");
                sb.Append("<script type='text/javascript' src='http://code.jquery.com/jquery-1.10.0.min.js'></script>");
                sb.Append("<style type='text/css'>");
                sb.Append(GetCSS());
                sb.Append("</style>");
                sb.Append("<script type='text/javascript'>");
                sb.Append(GetProcessPrintingScript());
                sb.Append("</script>");
                sb.Append("</head>");
                sb.Append("<body>");
                string oldStr1 = "<span class=\"facetip\">";
                string oldStr2 = "<span class=\"tip\">";
                sb.Append(stw.ToString().Replace(oldStr1, "<span class=\"facetip\" style=\"display:none\"> ").Replace(oldStr2, "<span class=\"tip\" style=\"display:none\"> "));
            }
            sb.Append("</body>");
            sb.Append("</html>");
            //string ZipRegex = @"^\{?}$";
            //if (Regex.IsMatch(sb.ToString(), ZipRegex))
            //{
            //    // lbl.Text = "ZIP is valid!";
            //}
            //else
            //{
            //    // lbl.Text = "ZIP is invalid!";
            //}
            sb = sb.Replace("/emerge/Resources/images/", "https://emerge.intelifi.com/Content/themes/base/images/");
            sb = sb.Replace("url('../", "url('https://emerge.intelifi.com/Resources/");

            //phAllrecords.Controls.Clear();

            return sb.ToString();

        }

        public string GetCSS()
        {
            string Css_Code = "";
            string CssPath = Server.MapPath("~/Content/Css/Report_Printing.css");
            string CssPath_Style = Server.MapPath("~/Content/Css/Report_Style_Printing.css");
            string CssPath_Custom_Style = Server.MapPath("~/Design2/Css/custom_Printing.css");

            if (File.Exists(CssPath))
            {
                Css_Code = File.OpenText(CssPath).ReadToEnd();
            }
            if (File.Exists(CssPath_Style))
            {
                Css_Code = Css_Code + File.OpenText(CssPath_Style).ReadToEnd();
            }
            if (File.Exists(CssPath_Custom_Style))
            {
                Css_Code = Css_Code + File.OpenText(CssPath_Custom_Style).ReadToEnd();
            }
            return Css_Code;
        }

        public string GetProcessPrintingScript()
        {
            string script_code = string.Empty;
            string ProcessPrintingScript = Server.MapPath("~/Scripts/ProcessPrintingScript.js");

            if (File.Exists(ProcessPrintingScript))
            {
                script_code = File.OpenText(ProcessPrintingScript).ReadToEnd();
            }

            return script_code;
        }

        public static void WriteLog(string Content, string FilePrefix)
        {
            string path = System.Configuration.ConfigurationManager.AppSettings["LogPath"];
            string FileName = path+"PrintProcessing/" + FilePrefix + ".txt";
            StringBuilder sb = new StringBuilder();
            StreamWriter sw;
            if (!File.Exists(FileName))
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                sw = File.CreateText(FileName);
                sb.Append(Content);
                sw.WriteLine(sb);
                sw.Close();
            }
            else
            {
                sw = File.AppendText(FileName);
                sb.Append(Content);
                sw.WriteLine(sb);
                sw.Close();
            }
        }

        //public Byte[] PdfSharpConvert(String html)
        //{
        //    Byte[] res = null;
        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        var config = new PdfGenerateConfig();
        //        //config.PageOrientation = PageOrientation.Landscape; // page orientation
        //        config.PageSize = PdfSharp.PageSize.A4; // for page size 

        //        var pdf = TheArtOfDev.HtmlRenderer.PdfSharp.PdfGenerator.GeneratePdf(html, config);
        //        pdf.Save(ms);

        //        res = ms.ToArray();
        //    }
        //    return res;
        //}

    }
}