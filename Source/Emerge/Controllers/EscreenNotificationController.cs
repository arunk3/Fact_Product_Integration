﻿using System;
using System.Linq;
using System.Text;
using Emerge.Data;
using System.Xml.Linq;
using System.IO;
using System.Web.Mvc;



namespace Emerge.Controllers
{
    public class EscreenNotificationController : Controller
    {
        //Escreen Notification Service 

        public ActionResult Index()
        {
            return View();
        }


        public string EscreenNotificationService()
        {
            return "Test";
        }

        public string EscreenUpdateStatus(string XmlParam)
        {

            VendorServices.GeneralService.WriteLog(Environment.NewLine + "----StartProcess---------" + System.DateTime.Now.ToString(), "EscreenUpdateStatus");


            string url = Request.Url.GetLeftPart(UriPartial.Authority);
            string Rawurl = Request.RawUrl.ToString();
            VendorServices.GeneralService.WriteLog(Environment.NewLine + "----StartProcess---------" + url.ToString(), "EscreenUpdateStatus");
            VendorServices.GeneralService.WriteLog(Environment.NewLine + "----StartProcess---------" + Rawurl.ToString(), "EscreenUpdateStatus");
            string documentContents=string.Empty;
            using (Stream receiveStream = Request.InputStream)
            {
                using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                {
                    documentContents = readStream.ReadToEnd();
                }
            }

            string XmlResponse = string.Empty;
            XmlResponse = documentContents;
            VendorServices.GeneralService.WriteLog(Environment.NewLine + "----Start---------" + System.DateTime.Now.ToString(), "EscreenUpdateStatus");

            string ResponseOut = string.Empty;

            if (XmlResponse != null)
            {
                VendorServices.GeneralService.WriteLog(Environment.NewLine + "----XmlResponse---------" + XmlResponse.ToString(), "EscreenUpdateStatus");

                string response = XmlResponse;
                //string Xmlerormessage = string.Empty;
                XDocument ObjXDocumentresponse = new XDocument();
                if (string.IsNullOrEmpty(response) == false)
                {

                    ObjXDocumentresponse = XDocument.Parse(response);
                    var eScreenData = ObjXDocumentresponse.Descendants("eScreenData").ToList();

                    VendorServices.GeneralService.WriteLog(Environment.NewLine + "-SchedulingNotification--\n" + eScreenData.ToString(), "EscreenUpdateStatus");


                    var SpecimenStatus = ObjXDocumentresponse.Descendants("SpecimenStatus").ToList();
                    if (SpecimenStatus.Count > 0)
                    {

                        //var eScreenID = SpecimenStatus.Descendants("eScreenID").FirstOrDefault().Value.ToString();
                        //var ConfirmationNumber = SpecimenStatus.Descendants("ConfirmationNumber").FirstOrDefault().Value.ToString();

                        //var eScreenClientAccount = SpecimenStatus.Descendants("eScreenClientAccount").FirstOrDefault().Value.ToString();
                        //var eScreenClientSubAccount = SpecimenStatus.Descendants("eScreenClientSubAccount").FirstOrDefault().Value.ToString();
                        //var LabName = SpecimenStatus.Descendants("LabName").FirstOrDefault().Value.ToString();
                        //var LabAccount = SpecimenStatus.Descendants("LabAccount").FirstOrDefault().Value.ToString();
                        //var ClientName = SpecimenStatus.Descendants("ClientName").FirstOrDefault().Value.ToString();

                        //var InternalClientID = SpecimenStatus.Descendants("InternalClientID").FirstOrDefault().Value.ToString();
                        //var ElectronicClientID = SpecimenStatus.Descendants("ElectronicClientID").FirstOrDefault().Value.ToString();
                        //var LocationInfo = SpecimenStatus.Descendants("LocationInfo").FirstOrDefault().Value.ToString();
                        //var CollectionSite = SpecimenStatus.Descendants("CollectionSite").FirstOrDefault().Value.ToString();
                        //var AccessionNum = SpecimenStatus.Descendants("AccessionNum").FirstOrDefault().Value.ToString();
                        //var DonorName = SpecimenStatus.Descendants("DonorName").FirstOrDefault().Value.ToString();

                        //var SpecimenType = SpecimenStatus.Descendants("SpecimenType").FirstOrDefault().Value.ToString();
                        //var SSN = SpecimenStatus.Descendants("SSN").FirstOrDefault().Value.ToString();
                        //var OtherID = SpecimenStatus.Descendants("OtherID").FirstOrDefault().Value.ToString();
                        //var OtherIDType = SpecimenStatus.Descendants("OtherIDType").FirstOrDefault().Value.ToString();

                        //var DOB = SpecimenStatus.Descendants("DOB").FirstOrDefault().Value.ToString();
                        //var HomePhone = SpecimenStatus.Descendants("HomePhone").FirstOrDefault().Value.ToString();
                        //var HomePhoneExt = SpecimenStatus.Descendants("HomePhoneExt").FirstOrDefault().Value.ToString();
                        //var WorkPhone = SpecimenStatus.Descendants("WorkPhone").FirstOrDefault().Value.ToString();

                        //var WorkPhoneExt = SpecimenStatus.Descendants("WorkPhoneExt").FirstOrDefault().Value.ToString();
                        //var ChainOfCustody = SpecimenStatus.Descendants("ChainOfCustody").FirstOrDefault().Value.ToString();
                        //var CollectionDate = SpecimenStatus.Descendants("CollectionDate").FirstOrDefault().Value.ToString();
                        //var CollectionTime = SpecimenStatus.Descendants("CollectionTime").FirstOrDefault().Value.ToString();
                        //var LabReceivedDate = SpecimenStatus.Descendants("LabReceivedDate").FirstOrDefault().Value.ToString();
                        //var LabReceivedTime = SpecimenStatus.Descendants("LabReceivedTime").FirstOrDefault().Value.ToString();
                        //var LabReportDate = SpecimenStatus.Descendants("LabReportDate").FirstOrDefault().Value.ToString();
                        //var LabReportTime = SpecimenStatus.Descendants("LabReportTime").FirstOrDefault().Value.ToString();
                        //var VerificationDate = SpecimenStatus.Descendants("VerificationDate").FirstOrDefault().Value.ToString();

                        //var VerificationTime = SpecimenStatus.Descendants("VerificationTime").FirstOrDefault().Value.ToString();
                        //var ReasonForTest = SpecimenStatus.Descendants("ReasonForTest").FirstOrDefault().Value.ToString();
                        //var SpecimenCollector = SpecimenStatus.Descendants("SpecimenCollector").FirstOrDefault().Value.ToString();
                        //var PrintedComments = SpecimenStatus.Descendants("PrintedComments").FirstOrDefault().Value.ToString();



                        //var Regulation = SpecimenStatus.Descendants("Regulation").FirstOrDefault().Value.ToString();
                        //var RegulationType = SpecimenStatus.Descendants("RegulationType").FirstOrDefault().Value.ToString();


                        var ExternalDonorID = SpecimenStatus.Descendants("ExternalDonorID").FirstOrDefault().Value.ToString();
                        if (ExternalDonorID != null)
                        {

                            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
                            {
                                try
                                {



                                    //int fKorderId = Convert.ToInt32(ExternalDonorID);
                                    //int fkOrderDetailid = ObjDALDataContext.tblOrderDetails.Where(cmp => cmp.fkOrderId == fKorderId && cmp.fkProductApplicationId == 20).Select(cmp => cmp.pkOrderDetailId).FirstOrDefault();
                                    //var tblOrderResponseManulResponseupdation = ObjDALDataContext.tblOrderResponseDatas.Where(x => x.fkOrderDetailId == fkOrderDetailid).FirstOrDefault();

                                    string OrderStatus = ObjXDocumentresponse.Descendants("SpecimenStatus").Attributes("Status").FirstOrDefault().Value.ToString();

                                    if (OrderStatus != null)
                                    {

                                        ResponseOut = "<eScreenData TransmissionID='" + ObjXDocumentresponse.Descendants("eScreenData").Attributes("TransmissionID").FirstOrDefault().Value.ToString() + "' Status='S'/>";

                                    }



                                }

                                catch (Exception ss)
                                {

                                    VendorServices.GeneralService.WriteLog(Environment.NewLine + "----eRROR ---------" + System.DateTime.Now.ToString() + "---------" + ss.Message.ToString(), "EscreenUpdateStatus");

                                }

                            }
                        }


                        VendorServices.GeneralService.WriteLog(Environment.NewLine + "----Done ---------" + System.DateTime.Now.ToString() + "---------", "EscreenUpdateStatus");




                    }



                }
            }

            return ResponseOut;

        }

        public string EscreenGetResponse(string XmlParam)
        {
            //string url = Request.Url.GetLeftPart(UriPartial.Authority);
            //string Rawurl = Request.RawUrl.ToString();

            string documentContents;
            using (Stream receiveStream = Request.InputStream)
            {
                using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                {
                    documentContents = readStream.ReadToEnd();
                }
            }

            string XmlResponse = string.Empty;
            XmlResponse = documentContents;
            VendorServices.GeneralService.WriteLog(Environment.NewLine + "----Start---------" + System.DateTime.Now.ToString(), "EscreenNotificationSerive");

            string ResponseOut = string.Empty;

            if (XmlResponse != null)
            {
                VendorServices.GeneralService.WriteLog(Environment.NewLine + "----XmlResponse---------" + XmlResponse.ToString(), "EscreenNotificationSerive");

                string response = XmlResponse;
                //string Xmlerormessage = string.Empty;
                XDocument ObjXDocumentresponse = new XDocument();
                if (string.IsNullOrEmpty(response) == false)
                {
                    ObjXDocumentresponse = XDocument.Parse(response);
                    var SchedulingNotification = ObjXDocumentresponse.Descendants("SchedulingNotification").ToList();

                    VendorServices.GeneralService.WriteLog(Environment.NewLine + "-SchedulingNotification--\n" + SchedulingNotification.ToString(), "EscreenNotificationSerive");


                    var DonorInfo = ObjXDocumentresponse.Descendants("DonorInfo").ToList();
                    if (DonorInfo.Count > 0)
                    {
                        //var FirstName = DonorInfo.Descendants("FirstName").FirstOrDefault().Value.ToString();
                       // var ExpirationTime = DonorInfo.Descendants("MiddleName").FirstOrDefault().Value.ToString();
                        //var LastName = DonorInfo.Descendants("LastName").FirstOrDefault().Value.ToString();

                        //var SSN = DonorInfo.Descendants("SSN").FirstOrDefault().Value.ToString();
                        //var DateOfBirth = DonorInfo.Descendants("DateOfBirth").FirstOrDefault().Value.ToString();
                        //var DonorPhone = DonorInfo.Descendants("DonorPhone").FirstOrDefault().Value.ToString();
                        //var OtherID = DonorInfo.Descendants("OtherID").FirstOrDefault().Value.ToString();
                        //var OtherIDType = DonorInfo.Descendants("OtherIDType").FirstOrDefault().Value.ToString();
                        var ExternalDonorID = DonorInfo.Descendants("ExternalDonorID").FirstOrDefault().Value.ToString();

                        if (ExternalDonorID != null)
                        {

                            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
                            {
                                try
                                {
                                    ResponseOut = "<eScreenData TransmissionID='" + ObjXDocumentresponse.Descendants("eScreenData").Attributes("TransmissionID").FirstOrDefault().Value.ToString() + "' Status='S'/>";
                                    int fKorderId = Convert.ToInt32(ExternalDonorID);
                                    int fkOrderDetailid = ObjDALDataContext.tblOrderDetails.Where(cmp => cmp.fkOrderId == fKorderId && cmp.fkProductApplicationId == 20).Select(cmp => cmp.pkOrderDetailId).FirstOrDefault();
                                    var tblOrderResponseManulResponseupdation = ObjDALDataContext.tblOrderResponseDatas.Where(x => x.fkOrderDetailId == fkOrderDetailid).FirstOrDefault();

                                    if (SchedulingNotification.Count > 0)
                                    {
                                        var EventId = SchedulingNotification.Descendants("SSOEventID").FirstOrDefault().Value.ToString();


                                        var DateScheduled = SchedulingNotification.Descendants("DateScheduled").FirstOrDefault().Value.ToString();
                                        var TimeScheduled = SchedulingNotification.Descendants("TimeScheduled").FirstOrDefault().Value.ToString();
                                        VendorServices.GeneralService.WriteLog(Environment.NewLine + "-SchedulingNotification--\n" + EventId.ToString(), "EscreenNotificationSerive");
                                        VendorServices.GeneralService.WriteLog(Environment.NewLine + "-SchedulingNotification--\n" + DateScheduled.ToString(), "EscreenNotificationSerive");
                                        VendorServices.GeneralService.WriteLog(Environment.NewLine + "-SchedulingNotification--\n" + TimeScheduled.ToString(), "EscreenNotificationSerive");


                                        if (EventId != null)
                                        {
                                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "-EventId--\n" + EventId.ToString(), "EscreenNotificationSerive");

                                            if (tblOrderResponseManulResponseupdation != null)
                                            {

                                                try
                                                {
                                                    //DateTime date = System.DateTime.Now;
                                                    tblOrderResponseManulResponseupdation.ManualResponse = Convert.ToString(EventId);

                                                    ObjDALDataContext.SubmitChanges();
                                                    VendorServices.GeneralService.WriteLog(Environment.NewLine + "-ObjDALDataContext.SubmitChanges ------Done--\n" + TimeScheduled.ToString(), "EscreenNotificationSerive");

                                                }
                                                catch (Exception asda)
                                                {
                                                    VendorServices.GeneralService.WriteLog(Environment.NewLine + "-Exception in updattion -----SubmitChanges ------Done--\n" + asda.Message.ToString(), "EscreenNotificationSerive");
                                                }

                                            }

                                        }

                                    }




                                }
                                catch (Exception asd)
                                {
                                    VendorServices.GeneralService.WriteLog(Environment.NewLine + "-Exception--------- Error ------Done--\n" + asd.Message.ToString(), "EscreenNotificationSerive");
                                }
                                finally
                                {


                                }
                            }

                        }
                    }


                    var NotificationType = ObjXDocumentresponse.Descendants("NotificationType").ToList();

                    VendorServices.GeneralService.WriteLog(Environment.NewLine + "-NotificationType--\n" + NotificationType.ToString(), "EscreenNotificationSerive");
                    if (NotificationType.Count > 0)
                    {
                        var Type = NotificationType.Descendants("Type").FirstOrDefault().Value.ToString();
                        var SystemScheduled = NotificationType.Descendants("SystemScheduled").FirstOrDefault().Value.ToString();
                        VendorServices.GeneralService.WriteLog(Environment.NewLine + "-NotificationType--\n" + SystemScheduled.ToString(), "EscreenNotificationSerive");
                        if (Type == null)
                        {

                        }
                    }

                    var EventSettings = ObjXDocumentresponse.Descendants("EventSettings").ToList();

                    if (EventSettings.Count > 0)
                    {
                        //var ExpirationDate = EventSettings.Descendants("ExpirationDate").FirstOrDefault().Value.ToString();
                        //var ExpirationTime = EventSettings.Descendants("ExpirationTime").FirstOrDefault().Value.ToString();
                        //var FixedEnd = EventSettings.Descendants("FixedEnd").FirstOrDefault();
                        //if (ExpirationDate == null)
                        //{



                        //}
                    }

                    //var ClinicInfo = ObjXDocumentresponse.Descendants("ClinicInfo").ToList();

                    //if (ClinicInfo.Count > 0)
                    //{

                    //    var ClinicID = ClinicInfo.Descendants("ClinicID").FirstOrDefault().Value.ToString();
                    //    var ClinicName = ClinicInfo.Descendants("ClinicName").FirstOrDefault().Value.ToString();
                    //    var ClinicType = ClinicInfo.Descendants("ClinicType").FirstOrDefault().Value.ToString();
                    //    if (ClinicID == null)
                    //    {



                    //    }
                    //}
                    var ClientInfo = ObjXDocumentresponse.Descendants("ClientInfo").ToList();

                    if (ClientInfo.Count > 0)
                    {

                        //var ClientName = ClientInfo.Descendants("ClientName").FirstOrDefault().Value.ToString();
                        //var eScreenClientAccount = ClientInfo.Descendants("eScreenClientAccount").FirstOrDefault().Value.ToString();
                        //var eScreenClientSubAccount = ClientInfo.Descendants("eScreenClientSubAccount").FirstOrDefault().Value.ToString();
                       // var ElectronicClientID = ClientInfo.Descendants("ElectronicClientID").FirstOrDefault().Value.ToString();
                        //var InternalClientID = ClientInfo.Descendants("InternalClientID").FirstOrDefault().Value.ToString();
                        //if (ClientName == null)
                        //{



                        //}
                    }


                    var Services = ObjXDocumentresponse.Descendants("DrugTests").ToList();
                    if (Services.Count > 0)
                    {
                        var DrugTests = Services.Descendants("DrugTests").ToList();
                        //var Service = DrugTests.Descendants("Service").FirstOrDefault().Value.ToString();

                        if (DrugTests == null)
                        {



                        }
                    }
                }
            }



            VendorServices.GeneralService.WriteLog(Environment.NewLine + "----End---------" + System.DateTime.Now.ToString(), "EscreenNotificationSerive");
            //return "EscreenGetResponse Process is completed";
            return ResponseOut;
        }


    }
}

