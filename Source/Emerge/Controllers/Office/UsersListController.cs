﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using System.Web.Security;
using System.Text;
using System.IO;
using System.Configuration;

namespace Emerge.Controllers.Office
{
    public partial class OfficeController : Controller
    {

        BALCompanyUsers ObjBALCompanyUsers;
        public ActionResult UsersList()
        {
            if (Request.QueryString["_c"] != null)
            {

                ViewData["CompanyId"] = Request.QueryString["_c"];
            }
            else
            {


                ViewData["CompanyId"] = Guid.Empty;

            }
           ViewBag.PwdToExportCsv= ConfigurationManager.AppSettings["PwdToExportCSV"].ToString();
            return View();
        }

       

        public ActionResult UsersListBind(int page, int pageSize, List<GridSort> sort, List<GridFilter> filters)
        {
            try
            {
                string UserRole = Session["LoggedUser"].ToString();

                Guid UserId = Guid.Empty;
                MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));
                int PageNum = page;
                int PageSize = pageSize;
                bool IsPaging = true;
                string SortingColumn = "FirstName";
                string SortingDirection = "ASC";
                Int16 StatusType = -1;
                int TotalRec = 0;
                //Guid CompanyId = Guid.Parse("7dc87d77-6bb5-4bb3-9837-fa861f36f281");
                //Guid FkLocationId = Guid.Parse("2e37dd1e-f60e-40cf-82e3-de973d56f5df");
                //int CompanyId = 9514;
                //int FkLocationId = 8917; 
                //INT69
                int CompanyId = 0;
                int FkLocationId = 0; 
                BALOffice ObjOffice = new BALOffice();
               // List<UserViewModel> ObjUserViewModel = new List<UserViewModel>();
                if (sort != null)
                {
                    if (sort[0].Field == "UserDisplayRole")
                    {

                        sort[0].Field = "UserRole";
                    }
                    if (sort[0].Field == "City+IsHome")
                    {

                        sort[0].Field = "City";

                    }
                    SortingDirection = sort[0].Dir;
                    SortingColumn = sort[0].Field;
                }
                if (ObjMembershipUser != null)
                {
                    UserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());

                }
                List<Proc_GetSalesUsersByRoleResult> OData = new List<Proc_GetSalesUsersByRoleResult>();
                if (filters != null)
                {

                    foreach (var ObjFilter in filters)
                    {
                        OData = FilterUsers(ObjFilter, PageNum, PageSize, IsPaging, StatusType, CompanyId, FkLocationId, SortingColumn, SortingDirection, UserRole, UserId, out TotalRec);
                    }
                }

                else
                {
                    //INT69
                    //OData = ObjOffice.GetAllSalesUsers(PageNum, PageSize, true, -1, 9514, 8917, SortingColumn, SortingDirection, UserRole, UserId, string.Empty, 0, string.Empty, out TotalRec).ToList();
                    OData = ObjOffice.GetAllSalesUsers(PageNum, PageSize, true, -1, 0, 0, SortingColumn, SortingDirection, UserRole, UserId, string.Empty, 0, string.Empty, out TotalRec).ToList();
                }

                Session["TotalOfficeEmergeUser"] = OData.ElementAt(0).TotalRec;
                if (OData.Count > 0)
                {
                    return Json(new { Reclist = OData, TotalRec = OData.ElementAt(0).TotalRec });
                }
                else
                {
                    return Json(new { Reclist = OData, TotalRec = OData.Count });
                }
               
                



            }
            catch (Exception)
            {
                return View(new List<UserViewModel>());
            }





        }

        public FileResult ExportOfficeUsersInfoToCSV()
        {


            //   IEnumerable products = db.Products.ToDataSourceResult(request).Data;

            //BALChat objBalChat = new BALChat();
            int PageNum = 1;
            int PageSize = Convert.ToInt32(Session["TotalOfficeEmergeUser"]);
            Guid UserId = Guid.Empty;
            string UserRole = Session["LoggedUser"].ToString();
           // bool IsPaging = true;
            string SortingColumn = "FirstName";
            string SortingDirection = "ASC";
            //Int16 StatusType = -1;
            int TotalRec = 0;
            //Guid CompanyId = Guid.Parse("7dc87d77-6bb5-4bb3-9837-fa861f36f281");
            //Guid FkLocationId = Guid.Parse("2e37dd1e-f60e-40cf-82e3-de973d56f5df");

            int CompanyId = 9514;
            int FkLocationId = 8917; 
            BALOffice ObjOffice = new BALOffice();

            var AllUsersList = ObjOffice.GetAllSalesUsers(PageNum, PageSize, true, -1, CompanyId, FkLocationId, SortingColumn, SortingDirection, UserRole, UserId, string.Empty, 0, string.Empty, out TotalRec).ToList();
            MemoryStream output = new MemoryStream();
            StreamWriter writer = new StreamWriter(output, Encoding.UTF8);

            writer.Write("User Note,");
            writer.Write("Last Login,");
            writer.Write("Enabled,");
            writer.Write("Signed FCRA,");

            writer.Write("Name ,");
            writer.Write("Initials ,");

            writer.Write("User Name,");
            writer.Write("Password  ,");
           

           
            writer.Write("Phone No ,");
          

            writer.Write("Status, ");
            writer.Write("Account Manager  ");
            writer.WriteLine();

            for (int i = 0; i < AllUsersList.Count(); i++)
            {

                BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
                Guid Selected_ManagerID = Guid.Empty;
                BALOffice ObjBALOffice = new BALOffice();
                proc_Get_CompanyUser_ByCompanyUserIdResult ObjtblCompanyUser = ObjBALCompanyUsers.SelectCompanyUserByCompanyUserId(Convert.ToInt32(AllUsersList.ElementAt(i).pkCompanyUserId.ToString()));
                Selected_ManagerID = ObjBALOffice.GetManagerID(ObjtblCompanyUser.fkUserId, out Selected_RelationID);
                string SaleManager = string.Empty;
                if (Selected_ManagerID != Guid.Empty)
                {
                    List<sManagers> ObjCollection = new List<sManagers>();

                    ObjCollection = ObjBALOffice.GetManagerList(new Guid("4DF0B5B0-755B-438A-92B4-0E33C294CA52"));
                    var list3 = ObjCollection.Where(rec => rec.UserId == Selected_ManagerID).FirstOrDefault();
                    SaleManager = list3.FirstName + " " + list3.LastName;
                }

                
                MembershipUser ObjMembershipUser = Membership.GetUser(ObjtblCompanyUser.fkUserId);
                string[] ObjRoles = Roles.GetRolesForUser(ObjMembershipUser.UserName);
                string RoleName = string.Empty;
                if (ObjRoles.Count() == 2)
                {
                    foreach (string rol in ObjRoles)
                    {
                        if (rol.ToLower() != "corporatemanager")
                        {
                            RoleName = rol;
                        }
                    }
                }
                else
                {
                    RoleName = ObjRoles[0].ToString();
                }
                string LastLoginByUser = ObjMembershipUser.LastLoginDate.ToString();
                string UserEmail = ObjMembershipUser.UserName == null ? string.Empty : ObjMembershipUser.UserName; 
                string Password = ObjMembershipUser.GetPassword();

                writer.Write(ObjtblCompanyUser.UserNote == null ? string.Empty : ObjtblCompanyUser.UserNote.Replace(",", " ")); 
                writer.Write(",");
                writer.Write("\"");
                writer.Write(LastLoginByUser);
                writer.Write("\"");
                writer.Write(",");
                writer.Write("\"");

                writer.Write(ObjtblCompanyUser.IsEnabled);
                writer.Write("\"");
                writer.Write(",");
                writer.Write("\"");

                writer.Write(ObjtblCompanyUser.IsSignedFcra);
                writer.Write("\"");
                writer.Write(",");
                writer.Write("\"");

                writer.Write(ObjtblCompanyUser.FirstName + " " + ObjtblCompanyUser.LastName);
                writer.Write("\"");
                writer.Write(",");
                writer.Write("\"");

                writer.Write(ObjtblCompanyUser.Initials);
                writer.Write("\"");
                writer.Write(",");
                writer.Write("\"");

                writer.Write(UserEmail);
                writer.Write("\"");
                writer.Write(",");
                writer.Write("\"");

                writer.Write(Password);
                writer.Write("\"");
                writer.Write(",");
                writer.Write("\"");

                writer.Write(ObjtblCompanyUser.PhoneNo);
                writer.Write("\"");
                writer.Write(",");
                writer.Write("\"");


                writer.Write(RoleName);
                writer.Write("\"");
                writer.Write(",");
                writer.Write("\"");

                writer.Write(SaleManager);
                writer.Write("\"");
                writer.WriteLine();
            }
            writer.Flush();
            output.Position = 0;

            return File(output, "text/comma-separated-values", "EmergeOfficeUsers.csv");
        }



        private static List<Proc_GetSalesUsersByRoleResult> FilterUsers(GridFilter filter, int PageNum, int PageSize, bool IsPaging, Int16 StatusType, int CompanyId, int FkLocationId, string SortingColumn, string SortingDirection, string UserRole, Guid UserId, out int TotalRec)
        {
            BALOffice ObjBALOffice = new BALOffice();
            List<Proc_GetSalesUsersByRoleResult> source = new List<Proc_GetSalesUsersByRoleResult>();
            byte ActionType = 0;
            string ColumnName = filter.Field;
            string FilterValue = filter.Value.ToString();
            ActionType = GetFilterAction(filter, ActionType);
            source = ObjBALOffice.GetAllSalesUsers(PageNum, PageSize, IsPaging, StatusType, CompanyId, FkLocationId, SortingColumn, SortingDirection, UserRole, UserId, ColumnName, ActionType, FilterValue, out TotalRec).ToList();
                                          
            return source;
        }



        public static byte GetFilterAction(GridFilter filter, byte ActionType)
        {
            switch (filter.Operator)
            {
                case "contains":
                    ActionType = 1;
                    break;
                case "startswith":
                    ActionType = 2;
                    break;
                case "endswith":
                    ActionType = 3;
                    break;
                case "eq":
                    ActionType = 4;
                    break;
                case "neq":
                    ActionType = 5;
                    break;
                case "gt":
                    ActionType = 6;
                    break;
                case "lt":
                    ActionType = 7;
                    break;
                case "lte":
                    ActionType = 8;
                    break;
                case "gte":
                    ActionType = 9;
                    break;
            }
            return ActionType;
        }





        public ActionResult DeleteUserRecord(int CompanyserId)
        {
            BALOffice ObjBALOffice = new BALOffice();
            ObjBALOffice.DeleteEmergeUserById(CompanyserId);

            return View();
        }



        public ActionResult UpdateDisableOfficeUser(string UserId)
        {
            int result = 0;
            ObjBALCompanyUsers = new BALCompanyUsers();
            List<tblCompanyUser> UserList = new List<tblCompanyUser>();
            string[] stringSeparators = new string[] { "#" };
            string[] Listcompanyuserid = UserId.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
            try
            {
                for (int i = 0; i < Listcompanyuserid.Length; i++)
                {
                    tblCompanyUser objtblCompanyUser = new tblCompanyUser();
                    objtblCompanyUser.fkUserId = new Guid(Listcompanyuserid[i]);
                    UserList.Add(objtblCompanyUser);

                }
                result = ObjBALCompanyUsers.ActionOnUsers(UserList, Convert.ToByte(1), Convert.ToBoolean(0));

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBALCompanyUsers = null;
            }



            return View();

        }



        public ActionResult UpdateEnableOfficeUser(string UserId)
        {
            int result = 0;
            ObjBALCompanyUsers = new BALCompanyUsers();
            List<tblCompanyUser> UserList = new List<tblCompanyUser>();
            string[] stringSeparators = new string[] { "#" };
            string[] Listcompanyuserid = UserId.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

            try
            {
                for (int i = 0; i < Listcompanyuserid.Length; i++)
                {


                    tblCompanyUser objtblCompanyUser = new tblCompanyUser();
                    objtblCompanyUser.fkUserId = new Guid(Listcompanyuserid[i]);
                    UserList.Add(objtblCompanyUser);

                }
                result = ObjBALCompanyUsers.ActionOnUsers(UserList, Convert.ToByte(1), Convert.ToBoolean(1));

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBALCompanyUsers = null;
            }

            return Json(result);


        }
        [HttpPost]
        public ActionResult DeleteSelectedOfficeUser(string UserId)
        {
            int result = 0;
            ObjBALCompanyUsers = new BALCompanyUsers();
            List<tblCompanyUser> UserList = new List<tblCompanyUser>();
            string[] stringSeparators = new string[] { "#" };
            string[] Listcompanyuserid = UserId.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
            try
            {
                for (int i = 0; i < Listcompanyuserid.Length; i++)
                {


                    tblCompanyUser objtblCompanyUser = new tblCompanyUser();
                    objtblCompanyUser.fkUserId = new Guid(Listcompanyuserid[i]);
                    UserList.Add(objtblCompanyUser);


                }

                result = ObjBALCompanyUsers.ActionOnUsers(UserList, Convert.ToByte(2), Convert.ToBoolean(1));

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBALCompanyUsers = null;
            }
            return Json(result);


        }




    }
}
