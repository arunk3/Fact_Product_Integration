﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using System.Web.Security;
using System.Text;
using System.IO;
using System.Data;
using System.Data.OleDb;
using LumenWorks.Framework.IO.Csv;
using System.Text.RegularExpressions;

namespace Emerge.Controllers.Office
{
    public partial class OfficeController : Controller
    {
        //
        // GET: /Junos/

        public ActionResult Juno()
        {
            JunosModel ObjJunosModel = new JunosModel();
            BALJuno ObjBALLeads = new BALJuno();
            List<tblCompany> ObjDbCollection = new List<tblCompany>();
            ObjDbCollection = ObjBALLeads.CountJunoType();
            ObjJunosModel.AllJunos = "(" + Convert.ToString(ObjDbCollection.Count()) + ")";
            ObjJunosModel.NewProspects = "(" + Convert.ToString(ObjDbCollection.Count(dx => dx.LeadStatus == 11)) + ")";
            ObjJunosModel.AddedLeads = "(" + Convert.ToString(ObjDbCollection.Count(dx => dx.LeadStatus > 0 && dx.LeadStatus < 5)) + ")";
            ObjJunosModel.CallBack = "(" + Convert.ToString(ObjDbCollection.Count(dx => dx.LeadStatus == 12)) + ")";
            ObjJunosModel.NotInterested = "(" + Convert.ToString(ObjDbCollection.Count(dx => dx.LeadStatus == 13)) + ")";
            ObjJunosModel.BadLeads = "(" + Convert.ToString(ObjDbCollection.Count(dx => dx.LeadStatus == 14)) + ")";
            ObjJunosModel.Message = "<span class='infomessage'>Prospects in this folder are renewed every 6 months.</span>";





            return View(ObjJunosModel);
        }

        public ActionResult JunosListBind(int page, int pageSize, List<GridFilter> filters)
        {



            BALJuno ObjBALJuno = new BALJuno();
            string SortingColumn = "CreatedDate";
            string SortingDirection = "DESC";
            int PageNum = page;
            byte LeadStatus = 0;

            int PageSize = pageSize;
            if (filters != null)
            {

                LeadStatus = Convert.ToByte(filters[0].Value);
            }

            var ObjDbCollection = ObjBALJuno.GetJunos(SortingColumn, SortingDirection, true, "", PageNum, PageSize, LeadStatus);

            if (ObjDbCollection.Count == 0)
            {

                return Json(new { Reclist = 0, TotalRec = 0 });


            }
            else
            {
                return Json(new { Reclist = ObjDbCollection, TotalRec = ObjDbCollection.ElementAt(0).TotalRec });
            }
        }

        public ActionResult LeadStatusUpdate(int CompanyId)
        {
            string strMessage = "";
            BALLeads ObjBALLeads = new BALLeads();
            tblCompany ObjtblCompany = new tblCompany();
            try
            {
                Guid UserId;
                MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));

                if (ObjMembershipUser != null)
                {
                    UserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
                    ObjtblCompany.pkCompanyId = CompanyId;
                    ObjtblCompany.LastModifiedById = UserId;
                    ObjtblCompany.LastModifiedDate = DateTime.Now;
                    ObjtblCompany.StatusStartDate = DateTime.Now;
                    ObjtblCompany.LeadStatus = 11;
                    ObjBALLeads.UpdateLeadStatus(ObjtblCompany);
                    strMessage = "<span class='successmsg'>Juno Recycled successfully.</span>";

                }
            }
            finally
            {
                ObjBALLeads = null;
            }

            return Json(new { UpdateMessage = strMessage });

        }

        public ActionResult Move_Click(byte Status, string CompanyId)
        {
            int result;
            BALJuno ObjBALJuno = new BALJuno();
            List<tblCompany> CompanyUserList = new List<tblCompany>();
            string[] stringSeparators = new string[] { "#" };
            string[] ListCompanyId = CompanyId.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
            try
            {
                for (int i = 0; i < ListCompanyId.Length; i++)
                {
                    tblCompany objTblCompany = new tblCompany();
                    objTblCompany.pkCompanyId = Int32.Parse(ListCompanyId[i]);
                    CompanyUserList.Add(objTblCompany);
                }

                byte LeadStatus = Convert.ToByte(Status.ToString());
                result = ObjBALJuno.MoveJuno(CompanyUserList, LeadStatus);


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBALJuno = null;

            }


            return Json(new { });

        }

        public PartialViewResult PartialJuno()
        {
            return PartialView("PartialJuno");
        }

        [HttpPost]
        public ActionResult UploadJuno(IEnumerable<HttpPostedFileBase> Junofile)
        {
            string sFileName = string.Empty;
            try
            {
                MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
                Guid UserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
                string Extension = Path.GetExtension(Junofile.ElementAt(0).FileName).ToLower();
                if (Extension == ".csv" || Extension == ".xls" || Extension == ".xlsx")
                {
                    //sFileName = UserId.ToString() + Junofile.ElementAt(0).FileName;
                    sFileName = UserId.ToString() + System.IO.Path.GetFileName(Junofile.ElementAt(0).FileName); ;
                    HttpPostedFileBase ObjHttpPostedFile = Request.Files[0];
                    string FilePath = Server.MapPath(@"~/Resources/Upload/JunoFiles/") + sFileName;
                    try
                    {
                        if (System.IO.File.Exists(FilePath))
                        {
                            System.IO.File.Delete(FilePath);
                        }
                    }
                    catch (Exception)
                    {

                    }
                    ObjHttpPostedFile.SaveAs(FilePath);
                    Session["sFileName"] = sFileName;
                }
            }
            catch (Exception)
            {

            }
            return Content(string.Empty);
        }

        DataTable DtFileData = new DataTable();
        DataColumn Dc1 = new DataColumn("Id", System.Type.GetType("System.Int32"));
        DataColumn Dc2 = new DataColumn("FileColumnName", System.Type.GetType("System.String"));

        public ActionResult btnShowHeader()
        {
            string sFileName = Convert.ToString(Session["sFileName"]);
            string sFilePath = Server.MapPath(@"~/Resources/Upload/JunoFiles/") + sFileName;
            string SheetNames = "";
            if (!sFileName.ToLower().Contains(".csv"))
            {
                string xConnStr = string.Empty;
                if (sFileName.ToLower().Contains(".xlx"))
                {
                    xConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + sFilePath + ";" + "Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\"";
                }
                else if (sFileName.ToLower().Contains(".xlsx"))
                {
                    xConnStr = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=" + sFilePath + ";" + "Extended Properties=Excel 12.0;";
                }

                Session["ExcelConnectionString"] = xConnStr;
                System.Data.DataTable dt = null;
                using (OleDbConnection objXConn = new OleDbConnection(xConnStr))
                {
                    objXConn.Open();
                    dt = objXConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                }
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SheetNames += Convert.ToString(dt.Rows[i]["TABLE_NAME"]) + ",";

                    }
                    if (SheetNames != "")
                    {
                        SheetNames = SheetNames.Substring(0, SheetNames.Length - 1);
                    }
                }
            }

            return Json(SheetNames);
        }


        private int GetExcelHeaders(string xConnStr, string SheetName, List<string> FileColumns)
        {
            int ColumnValue = 0;
            using (DataSet objDataSet = new DataSet())
            {
                using (OleDbDataAdapter objDataAdapter = new OleDbDataAdapter())
                {
                    objDataAdapter.SelectCommand = GetExcelFileInformation(xConnStr, SheetName);

                    objDataAdapter.Fill(objDataSet);
                }
                for (int i = 0; i < objDataSet.Tables[0].Columns.Count; i++)
                {
                    if (objDataSet.Tables[0].Columns.Count == 1 && objDataSet.Tables[0].Columns[i].Caption.Trim() == "F1")
                    {
                        FileColumns.Add("");
                        ColumnValue = 0;
                    }
                    else
                    {
                        ColumnValue = 1;
                        FileColumns.Add(objDataSet.Tables[0].Columns[i].Caption.Trim());
                    }
                }
            }
            return ColumnValue;
        }
        protected void GetCSVHeaders()
        {
            string sFileName = Convert.ToString(Session["sFileName"]);
            string sFilepath = Server.MapPath(@"~\Resources/Upload/JunoFiles/") + sFileName;
            StreamReader sr = new StreamReader(sFilepath);
            try
            {
                using (CsvReader ObjCSV = new CsvReader(sr, true, ','))
                {
                    string[] arrFields = ObjCSV.GetFieldHeaders();

                    DtFileData.Columns.Add(Dc1);
                    DtFileData.Columns.Add(Dc2);

                    for (int iRow = 0; iRow < arrFields.Count(); iRow++)
                    {
                        DataRow Dr = DtFileData.NewRow();
                        Dr[0] = int.Parse((iRow + 1).ToString());
                        Dr[1] = arrFields[iRow];
                        DtFileData.Rows.Add(Dr);
                    }
                    if (arrFields.Count() == 0)
                    {
                        Session["NoHeaders"] = "0";
                        DataRow Dr = DtFileData.NewRow();
                        Dr[0] = 1;
                        Dr[1] = "";
                        DtFileData.Rows.Add(Dr);
                    }
                    else { Session["NoHeaders"] = "1"; }
                    Session["DtCSVHeaders"] = DtFileData;
                }
            }
            catch //(Exception ex)
            {
            }
            finally
            {
                sr.Close();
            }
        }
        protected void GetExcelHeaders(string xConnStr)
        {
            using (DataSet objDataSet = new DataSet())
            {
                using (OleDbDataAdapter objDataAdapter = new OleDbDataAdapter())
                {
                    objDataAdapter.SelectCommand = GetExcelFileInformation(xConnStr);
                    objDataAdapter.Fill(objDataSet);
                }

                DtFileData.Columns.Add(Dc1);
                DtFileData.Columns.Add(Dc2);

                for (int i = 0; i < objDataSet.Tables[0].Columns.Count; i++)
                {

                    DataRow Dr = DtFileData.NewRow();
                    Dr[0] = int.Parse((i + 1).ToString());

                    if (objDataSet.Tables[0].Columns.Count == 1 && objDataSet.Tables[0].Columns[i].Caption.Trim() == "F1")
                    {
                        Dr[1] = "";
                        Session["NoHeaders"] = "0";
                    }
                    else
                    {
                        Session["NoHeaders"] = "1";
                        Dr[1] = objDataSet.Tables[0].Columns[i].Caption.Trim();
                    }
                    DtFileData.Rows.Add(Dr);
                }
            }
            Session["DtExcelHeaders"] = DtFileData;
        }
        protected OleDbCommand GetExcelFileInformation(string xConnStr)
        {
            OleDbCommand objCommand = new OleDbCommand();
            try
            {
                if (xConnStr != string.Empty)
                {
                    System.Data.DataTable dt = null;
                    using (OleDbConnection objXConn = new OleDbConnection(xConnStr))
                    {
                        objXConn.Open();

                        dt = objXConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                        string SheetName = Session["SheetName"].ToString();

                        if (dt.Rows.Count > 0)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                if (dt.Rows[i]["TABLE_NAME"].ToString() == SheetName)
                                {
                                    SheetName = "[" + SheetName + "]";
                                    objCommand = new OleDbCommand("SELECT * FROM " + SheetName, objXConn);

                                }
                            }
                        }
                    }
                }
            }
            catch
            {
            }

            return objCommand;
        }

        protected OleDbCommand GetExcelFileInformation(string xConnStr, string SheetName)
        {
            OleDbCommand objCommand = new OleDbCommand();
            try
            {
                if (xConnStr != string.Empty)
                {
                    System.Data.DataTable dt = null;
                    using (OleDbConnection objXConn = new OleDbConnection(xConnStr))
                    {
                        objXConn.Open();

                        dt = objXConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                        if (dt.Rows.Count > 0)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                if (dt.Rows[i]["TABLE_NAME"].ToString() == SheetName)
                                {
                                    SheetName = "[" + SheetName + "]";
                                    objCommand = new OleDbCommand("SELECT * FROM " + SheetName, objXConn);

                                }
                            }
                        }
                    }
                }
            }
            catch
            {
            }

            return objCommand;
        }

        protected int GetCSVHeaders(List<string> FileColumns)
        {
            int ColumnValue = 0;
            string sFileName = Convert.ToString(Session["sFileName"]);
            string sFilepath = Server.MapPath(@"~\Resources/Upload/JunoFiles/") + sFileName;
            StreamReader sr = new StreamReader(sFilepath);
            try
            {


                using (CsvReader ObjCSV = new CsvReader(sr, true, ','))
                {
                    string[] arrFields = ObjCSV.GetFieldHeaders();

                    for (int iRow = 0; iRow < arrFields.Count(); iRow++)
                    {
                        FileColumns.Add(arrFields[iRow]);
                    }
                    if (arrFields.Count() == 0)
                    {
                        ColumnValue = 0;
                        FileColumns.Add("");
                    }
                    else { ColumnValue = 1; }
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                sr.Close();
            }
            return ColumnValue;
        }
        //Update for ND-23
        private static void Logic4Preview(DataTable dt, DataTable dtPreviewGrd, string ddlname, ref int iprev, ref int piRow, int iRow, int d, string CellName, string invalidColumn)
        {
            if (iRow == 0)
            {
                DataRow dr = dtPreviewGrd.NewRow();
                if (ddlname.Contains("ddl"))
                    dr["column"] = (ddlname).Replace("ddl", "");
                else
                    dr["column"] = ddlname;
                dr["r1"] = dt.Rows[iRow][d].ToString();
                dr["r2"] = "";
                dr["r3"] = "";
                dr["r4"] = "";
                dr["r5"] = "";
                dr["r6"] = "";
                dr["r7"] = "";
                dr["Invalid Column"] = "";
                dr["Valid"] = (CellName != "") ? CellName : "";
                dtPreviewGrd.Rows.Add(dr);
            }
            else
            {
                if (piRow != iRow)
                {
                    iprev = 0;
                }
                else
                {
                    iprev++;
                }
                int iiRow = iRow + 1;
                if (iRow < 7)
                {
                    try
                    {
                        dtPreviewGrd.Rows[iprev][iiRow] = dt.Rows[iRow][d].ToString();
                        //string sOldCols = dtPreviewGrd.Rows[iprev]["Valid"].ToString();
                        if (CellName != "")
                        {
                            dtPreviewGrd.Rows[iprev]["Valid"] = CellName;// sOldCols + CellName + ", ";
                            dtPreviewGrd.Rows[iprev]["Invalid Column"] = invalidColumn;
                        }
                        if (iRow == 6)
                        {
                            dtPreviewGrd.Rows[iprev][iiRow] = ".....";
                        }
                    }
                    catch { }
                }
                else
                {
                    //string sOldCols = dtPreviewGrd.Rows[iprev]["Valid"].ToString();
                    if (CellName != "")
                    {
                        //dtPreviewGrd.Rows[iprev]["Valid"] = sOldCols + CellName + ", ";
                        dtPreviewGrd.Rows[iprev]["Valid"] = CellName;
                        dtPreviewGrd.Rows[iprev]["Invalid Column"] = invalidColumn;
                    }
                }
                piRow = iRow;
            }
        }

        public string GetCellAddress(int row, int col, bool chkIsJunoHeaders)
        {
            string[] CharArray = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            string strCellPosition = string.Empty;
            if (col <= 26)
            {
                if (chkIsJunoHeaders)
                    strCellPosition = CharArray[col].ToString() + (row + 2).ToString();
                else
                    strCellPosition = CharArray[col].ToString() + (row + 1).ToString();
            }
            else
            {
                strCellPosition = "Col: " + (col + 1).ToString() + "Row: " + (row + 1).ToString();
            }
            return strCellPosition;
        }



        #region Juno Upload

        public ActionResult JunoUpload()
        {
            return View();
        }

        public ActionResult GetFileColumns1(string SheetName, bool chkIsBatchHeaders)
        {
            #region Declare Collection

            List<string> ControlList = new List<string>();
            List<string> FileColumns = new List<string>();
            List<SelectListItem> lstCompanyType = new List<SelectListItem>();
            #endregion
            Session["chkIsBatchHeaders"] = chkIsBatchHeaders;
            int Status = 1;
            string sFileName = Convert.ToString(Session["sFileName"]);
            if (!sFileName.ToLower().Contains(".csv"))
            {
                Session["SheetName"] = SheetName;
                string ExcelConnectionString = Convert.ToString(Session["ExcelConnectionString"]);
                Session["ExcelConnectionString"] = ExcelConnectionString;
                int ReturnValue = GetExcelHeaders(ExcelConnectionString, SheetName, FileColumns);
               // string Message = string.Empty;
                if (ReturnValue == 0)
                {
                    Status = (chkIsBatchHeaders) ? 0 : -1;
                }

            }
            else
            {
                Status = GetCSVHeaders(FileColumns);
            }
            if (Status == 1)
            {


                ControlList.Add("ddlcompanyName");
                ControlList.Add("ddlCompanyUrl");
                ControlList.Add("ddlCompanyType");
                ControlList.Add("ddlLeadNumber");
                ControlList.Add("ddlLeadSource");
                ControlList.Add("ddlAccountManager");
                ControlList.Add("ddlSalesAssociate");
                ControlList.Add("ddlAddress1");
                ControlList.Add("ddlAddress2");
                ControlList.Add("ddlState");
                ControlList.Add("ddlCity");
                ControlList.Add("ddlZip");
                ControlList.Add("ddlMainContact");
                ControlList.Add("ddlMainTitle");
                ControlList.Add("ddlMainEmail");
                ControlList.Add("ddlMainPhone");
                ControlList.Add("ddlMainPhoneExt");
                ControlList.Add("ddlMainFaxNumber");
                ControlList.Add("ddlSecondaryContact");
                ControlList.Add("ddlSecondaryTitle");
                ControlList.Add("ddlSecondaryEmail");
                ControlList.Add("ddlSecondaryPhone");
                ControlList.Add("ddlSecondaryPhoneExt");
                ControlList.Add("ddlSecondaryFaxNumber");
                ControlList.Add("ddlMonthlyVolume");
                ControlList.Add("ddlNumberOfLocation");
                ControlList.Add("ddlNumberOfEmployee");
                ControlList.Add("ddlCurrentProvider");
                ControlList.Add("ddlPriority1");
                ControlList.Add("ddlPriority2");
                ControlList.Add("ddlPriority3");
                ControlList.Add("ddlLeadNote");

                BALCompanyType ObjBALCompanyType = new BALCompanyType();
                var CompanyTypes = ObjBALCompanyType.GetAllCompanyType();
                if (CompanyTypes.Count != 0)
                {
                    lstCompanyType.Clear();
                    for (int iRow = 0; iRow < CompanyTypes.Count(); iRow++)
                    {
                        lstCompanyType.Add(new SelectListItem { Text = CompanyTypes.ElementAt(iRow).CompanyType, Value = CompanyTypes.ElementAt(iRow).pkCompanyTypeId.ToString() });
                    }
                }
            }
            return Json(new { Status = Status, ControlList = ControlList, FileColumns = FileColumns, lstCompanyType = lstCompanyType });
        }
        //update for ND-23
        public ActionResult ValidateJuno1(FormCollection frm)
        {
            bool isAllValid = false;

            StringBuilder st = new StringBuilder();
            int CountInvalid = 0;
            try
            {
                using (DataTable dtPreviewData = new DataTable())
                {
                    List<TempJuno> tempJunoArray = new List<TempJuno>();
                    string sFileName = Convert.ToString(Session["sFileName"]);
                    string sFilepath = Server.MapPath(@"~\Resources/Upload/JunoFiles/") + sFileName;

                    tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlcompanyName", RegExp = @"", IsRequired = true });
                    tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlLeadSource", RegExp = @"", IsRequired = true });//"^[a-zA-Z]+$",
                    tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlState", RegExp = @"", IsRequired = true });
                    tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlMainContact", RegExp = @"", IsRequired = true });
                    tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlMainEmail", RegExp = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", IsRequired = true });
                    tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlMainPhone", RegExp = @"^(\([0-9]{3}\) |\([0-9]{3}\)|[0-9]{3}-)[0-9]{3}-[0-9]{4}$", IsRequired = true });
                    if (frm["ddlCompanyUrl"].ToString() != "-1" && frm["ddlCompanyUrl"].ToString() != "null")
                    {
                        tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlCompanyUrl", RegExp = @"(http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?", IsRequired = false });
                    }

                    if (frm["ddlAccountManager"].ToString() != "-1" && frm["ddlAccountManager"].ToString() != "null")
                    {
                        tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlAccountManager", RegExp = "^[a-zA-Z' ']+$", IsRequired = false });
                    }
                    if (frm["ddlSalesAssociate"].ToString() != "-1" && frm["ddlSalesAssociate"].ToString() != "null")
                    {
                        tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlSalesAssociate", RegExp = "^[a-zA-Z' ']+$", IsRequired = false });
                    }
                    if (frm["ddlCity"].ToString() != "-1" && frm["ddlCity"].ToString() != "null")
                    {
                        tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlCity", RegExp = @"", IsRequired = false });
                    }
                    if (frm["ddlZip"].ToString() != "-1" && frm["ddlZip"].ToString() != "null")
                    {
                        tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlZip", RegExp = @"^(\d{3,5})$", IsRequired = false });
                    }
                    if (frm["ddlMainTitle"].ToString() != "-1" && frm["ddlMainTitle"].ToString() != "null")
                    {
                        tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlMainTitle", RegExp = @"", IsRequired = false });
                    }
                    if (frm["ddlMainPhoneExt"].ToString() != "-1" && frm["ddlMainPhoneExt"].ToString() != "null")
                    {
                        tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlMainPhoneExt", RegExp = @"^\d{3}$", IsRequired = false });
                    }
                    if (frm["ddlMainFaxNumber"].ToString() != "-1" && frm["ddlMainFaxNumber"].ToString() != "null")
                    {
                        tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlMainFaxNumber", RegExp = @"^(\([0-9]{3}\) |\([0-9]{3}\)|[0-9]{3}-)[0-9]{3}-[0-9]{4}$", IsRequired = false });
                    }
                    if (frm["ddlSecondaryContact"].ToString() != "-1" && frm["ddlSecondaryContact"].ToString() != "null")
                    {
                        tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlSecondaryContact", RegExp = @"", IsRequired = false });
                    }
                    if (frm["ddlSecondaryTitle"].ToString() != "-1" && frm["ddlSecondaryTitle"].ToString() != "null")
                    {
                        tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlSecondaryTitle", RegExp = @"", IsRequired = false });
                    }
                    if (frm["ddlSecondaryEmail"].ToString() != "-1" && frm["ddlSecondaryEmail"].ToString() != "null")
                    {
                        tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlSecondaryEmail", RegExp = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", IsRequired = false });
                    }
                    if (frm["ddlSecondaryPhone"].ToString() != "-1" && frm["ddlSecondaryPhone"].ToString() != "null")
                    {
                        tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlSecondaryPhone", RegExp = @"^(\([0-9]{3}\) |\([0-9]{3}\)|[0-9]{3}-)[0-9]{3}-[0-9]{4}$", IsRequired = false });
                    }
                    if (frm["ddlSecondaryPhoneExt"].ToString() != "-1" && frm["ddlSecondaryPhoneExt"].ToString() != "null")
                    {
                        tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlSecondaryPhoneExt", RegExp = @"^\d{3}$", IsRequired = false });
                    }
                    if (frm["ddlSecondaryFaxNumber"].ToString() != "-1" && frm["ddlSecondaryFaxNumber"].ToString() != "null")
                    {
                        tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlSecondaryFaxNumber", RegExp = @"^(\([0-9]{3}\) |\([0-9]{3}\)|[0-9]{3}-)[0-9]{3}-[0-9]{4}$", IsRequired = false });
                    }
                    if (frm["ddlMonthlyVolume"].ToString() != "-1" && frm["ddlMonthlyVolume"].ToString() != "null")
                    {
                        tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlMonthlyVolume", RegExp = @"", IsRequired = false });
                    }
                    if (frm["ddlNumberOfLocation"].ToString() != "-1" && frm["ddlNumberOfLocation"].ToString() != "null")
                    {
                        tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlNumberOfLocation", RegExp = @"^\d+$", IsRequired = false });
                    }
                    if (frm["ddlNumberOfEmployee"].ToString() != "-1" && frm["ddlNumberOfEmployee"].ToString() != "null")
                    {
                        tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlNumberOfEmployee", RegExp = @"", IsRequired = false });
                    }
                    if (frm["ddlCurrentProvider"].ToString() != "-1" && frm["ddlCurrentProvider"].ToString() != "null")
                    {
                        tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlCurrentProvider", RegExp = @"", IsRequired = false });
                    }
                    if (frm["ddlPriority1"].ToString() != "-1" && frm["ddlPriority1"].ToString() != "null")
                    {
                        tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlPriority1", RegExp = @"", IsRequired = false });
                    }
                    if (frm["ddlPriority2"].ToString() != "-1" && frm["ddlPriority2"].ToString() != "null")
                    {
                        tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlPriority2", RegExp = @"", IsRequired = false });
                    }
                    if (frm["ddlPriority3"].ToString() != "-1" && frm["ddlPriority3"].ToString() != "null")
                    {
                        tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlPriority3", RegExp = @"", IsRequired = false });
                    }
                    if (frm["ddlLeadNote"].ToString() != "-1" && frm["ddlLeadNote"].ToString() != "null")
                    {
                        tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlLeadNote", RegExp = @"", IsRequired = false });
                    }
                    if (!sFileName.ToLower().Contains(".csv"))
                    {
                        #region Excel Step 3
                        using (DataSet objDataSet = new DataSet())
                        {
                            using (OleDbDataAdapter objDataAdapter = new OleDbDataAdapter())
                            {
                                objDataAdapter.SelectCommand = GetExcelFileInformation(Session["ExcelConnectionString"].ToString());
                                objDataAdapter.Fill(objDataSet);
                            }
                            List<string> lstExcelHeaders = new List<string>();
                            //StringBuilder sbValidations = new StringBuilder();

                            for (int i = 0; i < objDataSet.Tables[0].Columns.Count; i++)
                            {
                                lstExcelHeaders.Add(objDataSet.Tables[0].Columns[i].Caption);
                                dtPreviewData.Columns.Add("Column" + i.ToString(), Type.GetType("System.String"));
                            }

                            if (Session["chkIsBatchHeaders"].ToString().ToLower() == "false")
                            {
                                DataRow dr = dtPreviewData.NewRow();
                                for (int iRow = 0; iRow < dtPreviewData.Columns.Count; iRow++)
                                {
                                    dr[iRow] = objDataSet.Tables[0].Columns[iRow].Caption;
                                }
                                dtPreviewData.Rows.Add(dr);
                            }

                            for (int total = 0; total < objDataSet.Tables[0].Rows.Count; total++)
                            {
                                var isInsertRow = false;
                                //var RowIndex = ObjCSV.CurrentRecordIndex;
                                var TotalRowCount = objDataSet.Tables[0].Rows.Count;
                                DataRow dr = dtPreviewData.NewRow();
                                for (int iRow = 0; iRow < dtPreviewData.Columns.Count; iRow++)
                                {
                                    dr[iRow] = (objDataSet.Tables[0].Rows[total][lstExcelHeaders[iRow].ToString()]).ToString().Trim();
                                    if (!isInsertRow)
                                    {
                                        isInsertRow = dr[iRow].ToString() != "" ? true : isInsertRow;
                                    }
                                }
                                if (isInsertRow || (!isInsertRow && (total != (TotalRowCount - 1))))
                                {
                                    dtPreviewData.Rows.Add(dr);
                                }
                                // dtPreviewData.Rows.Add(dr);
                            }
                        }
                        #endregion Excel Step 3
                    }
                    else
                    {
                        #region CSV Step 3
                        StreamReader sr = new StreamReader(sFilepath);
                        try
                        {
                            if (Session["DtCSVHeaders"] != null)
                            {
                                DtFileData = (DataTable)Session["DtCSVHeaders"];
                                Session["DtCSVHeaders"] = null;
                            }
                            else
                            {
                                GetCSVHeaders();
                            }
                            for (int i = 0; i < DtFileData.Rows.Count; i++)
                            {
                                dtPreviewData.Columns.Add("Column" + i.ToString(), Type.GetType("System.String"));

                            }
                            string[] arrFields;
                            bool IsAddHeaders = false;

                            using (CsvReader ObjCSV = new CsvReader(sr, true, ','))
                            {
                                arrFields = ObjCSV.GetFieldHeaders();

                                while (ObjCSV.ReadNextRecord())
                                {
                                    IsAddHeaders = true;
                                    DataRow dr = dtPreviewData.NewRow();
                                    var isInsertRow = false;
                                    var RowIndex = ObjCSV.CurrentRecordIndex;
                                    var TotalRowCount = ObjCSV.FieldCount;
                                    for (int iRow = 0; iRow < dtPreviewData.Columns.Count; iRow++)
                                    {
                                        dr[iRow] = ObjCSV[iRow].Trim();
                                        if (!isInsertRow)
                                        {
                                            isInsertRow = dr[iRow].ToString() != "" ? true : isInsertRow;
                                        }

                                    }

                                    if (isInsertRow || (!isInsertRow && (RowIndex != (TotalRowCount - 1))))
                                    {
                                        dtPreviewData.Rows.Add(dr);
                                    }

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            sr.Close();
                        }
                        #endregion CSV Step 3
                    }

                    DataTable dt = GetCSVPreviewAfter1(dtPreviewData, tempJunoArray, ref isAllValid, ref CountInvalid, frm);

                    if (!isAllValid)
                    {
                        Session["Invalid"] = false;
                    }
                    else if (dtPreviewData.Rows.Count == 0)
                    {
                    }
                    else
                    {
                        Session["Invalid"] = true;
                    }

                    Session["StoreData"] = dtPreviewData;
                    TempData["FileData"] = dtPreviewData;

                    st.Append("<table cellpadding='2' cellspacing='0' border='1' width='565px' style='border:1px solid black;'><tr>");
                    st.Append("<td bgcolor='#B8B8B8' align='center'><b>Columns</b></td>");
                    st.Append("<td bgcolor='#B8B8B8' align='center'><b>Row1</b></td>");
                    st.Append("<td bgcolor='#B8B8B8' align='center'><b>Row2</b></td>");
                    st.Append("<td bgcolor='#B8B8B8' align='center'><b>Row3</b></td>");
                    st.Append("<td bgcolor='#B8B8B8' align='center'><b>Row4</b></td>");
                    st.Append("<td bgcolor='#B8B8B8' align='center'><b>Row5</b></td>");
                    st.Append("<td bgcolor='#B8B8B8' align='center'><b>Row6</b></td>");
                    st.Append("<td bgcolor='#B8B8B8' align='center'><b>.....</b></td>");
                    st.Append("<td bgcolor='#B8B8B8' align='center'><b>Invalid Cells</b></td>");
                    st.Append("<td bgcolor='#B8B8B8' align='center'><b>Validate</b></td></tr>");

                    for (int irow = 0; irow < dt.Rows.Count; irow++)
                    {
                        st.Append("<tr>");
                        if (irow % 2 == 0)
                        {
                            for (int icol = 0; icol < dt.Columns.Count; icol++)
                            {
                                st.Append("<td bgcolor='#E8E8E8'>" + dt.Rows[irow][icol] + "</td>");
                            }

                        }
                        else
                        {
                            for (int icol = 0; icol < dt.Columns.Count; icol++)
                            {
                                st.Append("<td>" + dt.Rows[irow][icol] + "</td>");
                            }
                        }
                        st.Append("</tr>");
                    }
                }
                st.Append("</table>");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(new { grid = st.ToString(), CountInvalid = CountInvalid });
        }

        /// <summary>
        /// upadte for ND-23
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="tempJunoArray"></param>
        /// <param name="isAllValid"></param>
        /// <param name="CountInvalid"></param>
        /// <param name="frm"></param>
        /// <returns></returns>
        protected DataTable GetCSVPreviewAfter1(DataTable dt, List<TempJuno> tempJunoArray, ref bool isAllValid, ref int CountInvalid, FormCollection frm)
        {
            BALJuno ObjBALJuno = new BALJuno();
            StringBuilder sHTML, sInValidCell;
            sHTML = new StringBuilder();
            sInValidCell = new StringBuilder();
            sHTML.Append("");
            sInValidCell.Append("");
            BALGeneral ObjBALGeneral = new BALGeneral();
            List<tblState> tblStateColl = new List<tblState>();
            List<tblCity> tblCityColl = new List<tblCity>();
            List<tblCompanyType> tblCompanyTypeColl = new List<tblCompanyType>();
            List<tblLocation> tblLocationColl = new List<tblLocation>();
            List<tblCompany> tblCompanyColl = new List<tblCompany>();

            List<string> Priority = new List<string>();
            Priority.Add("accuracy");
            Priority.Add("coverage");
            Priority.Add("pricing");
            Priority.Add("speed");
            Priority.Add("support");
            Priority.Add("other");
            tblStateColl = ObjBALGeneral.GetStates();
            tblCityColl = ObjBALGeneral.GetCities();
            tblCompanyTypeColl = ObjBALGeneral.GetCompanyTypes();
            tblLocationColl = ObjBALGeneral.GetMainPhone();
            tblCompanyColl = ObjBALGeneral.GetMainEmailId();
            var accountManagerList = ObjBALJuno.GetManagerList("salesmanager");
            var salesAssociateList = ObjBALJuno.GetManagerList("salesrep");

            DataTable dtPreviewGrd = new DataTable();

            dtPreviewGrd.Columns.Add("column", Type.GetType("System.String"));
            dtPreviewGrd.Columns.Add("r1", Type.GetType("System.String"));
            dtPreviewGrd.Columns.Add("r2", Type.GetType("System.String"));
            dtPreviewGrd.Columns.Add("r3", Type.GetType("System.String"));
            dtPreviewGrd.Columns.Add("r4", Type.GetType("System.String"));
            dtPreviewGrd.Columns.Add("r5", Type.GetType("System.String"));
            dtPreviewGrd.Columns.Add("r6", Type.GetType("System.String"));
            dtPreviewGrd.Columns.Add("r7", Type.GetType("System.String"));
            dtPreviewGrd.Columns.Add("Invalid Column", Type.GetType("System.String"));
            dtPreviewGrd.Columns.Add("Valid", Type.GetType("System.String"));
            bool IsRowVlaid = true;
            isAllValid = true;
            int iprev = -1, piRow = -1;
            //string sFileName = Convert.ToString(Session["sFileName"]);
            List<bool> IsAllRowValid = new List<bool>();
            List<string> titleValue = new List<string>();
            for (int iarr = 0; iarr < tempJunoArray.Count(); iarr++)
            {
                IsAllRowValid.Add(true);
                titleValue.Add("");
            }
            try
            {
                for (int iRow = 0; iRow < dt.Rows.Count; iRow++)
                {
                    for (int iarr = 0; iarr < tempJunoArray.Count(); iarr++)
                    {
                        string ddlname = tempJunoArray.ElementAt(iarr).JunoRequiredFields.ToString();//ddl +
                        string CellName = "";
                        string invalidColumn = "";
                        string[] Value = frm.GetValue(ddlname).AttemptedValue.Split(',');
                        int No = 0;
                        if (Value.Length > 0)
                        {
                            No = Convert.ToInt32(Value[0]);
                        }
                        IsRowVlaid = RowValidation1(tempJunoArray.ElementAt(iarr).JunoRequiredFields.ToString(), dt.Rows[iRow][No].ToString(), tempJunoArray.ElementAt(iarr).RegExp, tempJunoArray.ElementAt(iarr).IsRequired, tblStateColl, tblCityColl, tblCompanyTypeColl, tblCompanyColl, accountManagerList, salesAssociateList, Priority);
                        if (IsRowVlaid && IsAllRowValid[iarr])
                        {
                            CellName = "<img style='padding-left: 17px;' src='../Content/themes/base/images/successmsg.png'/>";
                            invalidColumn = titleValue[iarr];
                        }
                        else
                        {
                            IsAllRowValid[iarr] = false;
                            if (!IsRowVlaid)
                            {
                                titleValue[iarr] = titleValue[iarr] == "" ? GetCellAddress(iRow, No) : titleValue[iarr] + "," + GetCellAddress(iRow, No);
                            }
                            CountInvalid++;
                            isAllValid = false;
                            invalidColumn = titleValue[iarr];
                            CellName = "<img style='padding-left: 17px;' title=\"" + titleValue[iarr] + "\" src='../Content/themes/base/images/publish_x.png'/>";  // GetCellAddress(iRow, No);
                        }

                        #region Logic for preview
                        Logic4Preview(dt, dtPreviewGrd, ddlname, ref iprev, ref piRow, iRow, No, CellName, invalidColumn);
                        #endregion
                    }
                }
            }
            catch (IndexOutOfRangeException inx)
            {

            }
            catch(Exception ex)
            {
                throw ex;
            }
            return dtPreviewGrd;
        }


        public string GetCellAddress(int row, int col)
        {
            string[] CharArray = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            string strCellPosition = string.Empty;
            if (!Convert.ToString(Session["sFileName"]).ToLower().Contains(".csv"))
            {
                if (col < 26)
                {
                    if (Session["chkIsBatchHeaders"].ToString().ToLower() == "true")
                        strCellPosition = CharArray[col].ToString() + (row + 2).ToString();
                    else
                        strCellPosition = CharArray[col].ToString() + (row + 1).ToString();
                }
                else
                {
                    if (Session["chkIsBatchHeaders"].ToString().ToLower() == "true")
                        strCellPosition = CharArray[(col / 26) - 1].ToString() + CharArray[col % 26].ToString() + (row + 2).ToString();
                    else
                        strCellPosition = CharArray[(col / 26) - 1].ToString() + CharArray[col % 26].ToString() + (row + 1).ToString();
                }
            }
            else
            {
                if (col < 26)
                {
                    strCellPosition = CharArray[col].ToString() + (row + 2).ToString();
                }
                else
                {
                    strCellPosition = CharArray[(col / 26) - 1].ToString() + CharArray[col % 26].ToString() + (row + 2).ToString();
                }
            }
            return strCellPosition;
        }

        public bool RowValidation1(string ColumnName, string CellData, string Expr, bool IsRequried, List<tblState> StateColl, List<tblCity> CityColl, List<tblCompanyType> CompanyTypeColl, List<tblCompany> CompanyColl, List<Proc_Get_SalesManagersResult> accountManagerList, List<Proc_Get_SalesManagersResult> salesAssociateList, List<string> Priority)
        {
            if (ColumnName == "ddlZip")
            {
                string[] splitArr = { "-" };
                var splitData = CellData.Split(splitArr, StringSplitOptions.RemoveEmptyEntries);
                if (CellData.Length > 0) // ND-23
                {
                    CellData = splitData.ElementAt(0).Trim();
                }
                else
                {
                    CellData = string.Empty;
                }
            }
            else if (ColumnName == "ddlMainPhone")
            {
                string[] splitArr = { "x" };
                var splitData = CellData.Split(splitArr, StringSplitOptions.RemoveEmptyEntries);
                if (splitData.Length > 0)
                {
                    if (splitData != null)
                    {
                        CellData = splitData.ElementAt(0).Trim();
                    }
                }

                else //Nd-23
                {
                    CellData = string.Empty;
                }
                // CellData = splitData.ElementAt(0).Trim();
            }

            bool IsCellValid = false;
            if ((Expr != ""))
            {
                Regex reg = new Regex(Expr);
                if (IsRequried && CellData == string.Empty)
                {
                    IsCellValid = false;
                }
                else if ((IsRequried && CellData != string.Empty) || (!IsRequried && CellData != string.Empty))
                {
                    IsCellValid = (reg.IsMatch(CellData)) ? true : false;
                }
                else
                {
                    IsCellValid = true;
                }
            }
            else if ((Expr == "") && IsRequried)
            {
                IsCellValid = CellData != string.Empty ? true : false;
            }
            else { IsCellValid = true; }

            if (ColumnName.ToLower().Contains("state") && IsCellValid == true)
            {
                IsCellValid = (StateColl.Where(d => d.StateName.ToLower() == CellData.ToLower()).Count() > 0) ? true : false;

                IsCellValid = IsCellValid == false ? (StateColl.Where(d => d.StateCode.ToLower() == CellData.ToLower()).Count() > 0) ? true : false : IsCellValid;
            }


            else if (ColumnName.ToLower().Contains("companytype"))
            {
                IsCellValid = CellData.Trim() != string.Empty ? ((CompanyTypeColl.Where(d => d.CompanyType.ToLower() == CellData.ToLower()).Count() > 0) ? true : false) : true;

            }
            else if (ColumnName.ToLower().Contains("accountmanager"))
            {
                IsCellValid = CellData.Trim() != string.Empty ? ((accountManagerList.Where(d => d.FullName.ToLower() == CellData.ToLower()).Count() > 0) ? true : false) : true;
            }
            else if (ColumnName.ToLower().Contains("salesassociate"))
            {
                IsCellValid = CellData.Trim() != string.Empty ? ((salesAssociateList.Where(d => d.FullName.ToLower() == CellData.ToLower()).Count() > 0) ? true : false) : true;
            }
            else if (ColumnName.ToLower().Contains("priority") && IsCellValid == true)
            {
                IsCellValid = CellData.Trim() != string.Empty ? (Priority.Contains(CellData.ToLower())) : true;
            }
            return IsCellValid;
        }

        public JsonResult InsertJuno1(FormCollection frm)
        {
            int status = 0;
            string messresult = "";
            if (Session["Invalid"] != null)
            {
                if (Convert.ToBoolean(Session["Invalid"].ToString()) == false)
                {
                    status = 0;
                    messresult = "";
                }
                else
                {
                    # region add junos in DB
                    DataTable DtFinal = null;
                    if (Session["StoreData"] != null)
                    {
                        DtFinal = (DataTable)Session["StoreData"];
                        int Success = InsertCSVDataToDB1(frm);
                        if (Success == 0)
                        {
                            messresult = "Some error occurred while saving data.";
                            status = -1;
                        }
                        else if (Success < DtFinal.Rows.Count && Success > 0)
                        {
                            messresult = "Only " + Success + " Juno Prospect(s) Saved Successfully.";
                            status = -1;
                        }
                        else
                        {
                            status = 1;
                            messresult = +Success + " Juno Prospects are saved successfully.";
                        }
                    }
                    # endregion
                }
            }
            return Json(new { status = status, messresult = messresult }, JsonRequestBehavior.AllowGet);
        }


        public DataTable CreateFixedSchemaDataTable()
        {
            DataTable _FixedSchemaDataTable = new DataTable("JunoImport");
            DataColumn col_CompanyName = new DataColumn("CompanyName");
            col_CompanyName.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_CompanyName);

            DataColumn col_CompanyUrl = new DataColumn("CompanyUrl");
            col_CompanyUrl.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_CompanyUrl);

            DataColumn col_LeadSource = new DataColumn("LeadSource");
            col_LeadSource.DataType = typeof(string);
            _FixedSchemaDataTable.Columns.Add(col_LeadSource);

            DataColumn col_AccountManager = new DataColumn("AccountManager");
            col_AccountManager.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_AccountManager);

            DataColumn col_SalesAssociate = new DataColumn("SalesAssociate");
            col_SalesAssociate.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_SalesAssociate);

            DataColumn col_Address1 = new DataColumn("Address1");
            col_Address1.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_Address1);

            DataColumn col_Address2 = new DataColumn("Address2");
            col_Address2.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_Address2);

            DataColumn col_State = new DataColumn("State");
            col_State.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_State);

            DataColumn col_City = new DataColumn("City");
            col_City.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_City);

            DataColumn col_ZipCode = new DataColumn("ZipCode");
            col_ZipCode.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_ZipCode);

            DataColumn col_MainContactPersonName = new DataColumn("MainContactPersonName");
            col_MainContactPersonName.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_MainContactPersonName);

            DataColumn col_MainTitle = new DataColumn("MainTitle");
            col_MainTitle.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_MainTitle);

            DataColumn col_MainEmailId = new DataColumn("MainEmailId");
            col_MainEmailId.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_MainEmailId);

            DataColumn col_PhoneNo1 = new DataColumn("PhoneNo1");
            col_PhoneNo1.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_PhoneNo1);

            DataColumn col_Ext1 = new DataColumn("Ext1");
            col_Ext1.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_Ext1);

            DataColumn col_MainFax = new DataColumn("MainFax");
            col_MainFax.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_MainFax);

            DataColumn col_SecondaryMainContactPersonName = new DataColumn("SecondaryMainContactPersonName");
            col_SecondaryMainContactPersonName.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_SecondaryMainContactPersonName);

            DataColumn col_SecondaryTitle = new DataColumn("SecondaryTitle");
            col_SecondaryTitle.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_SecondaryTitle);

            DataColumn col_SecondaryEmail = new DataColumn("SecondaryEmail");
            col_SecondaryEmail.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_SecondaryEmail);

            DataColumn col_PhoneNo2 = new DataColumn("PhoneNo2");
            col_PhoneNo2.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_PhoneNo2);

            DataColumn col_Ext2 = new DataColumn("Ext2");
            col_Ext2.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_Ext2);

            DataColumn col_SecondaryFax = new DataColumn("SecondaryFax");
            col_SecondaryFax.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_SecondaryFax);

            DataColumn col_MonthlyVolume = new DataColumn("MonthlyVolume");
            col_MonthlyVolume.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_MonthlyVolume);

            DataColumn col_NoOfLocations = new DataColumn("NoOfLocations");
            col_NoOfLocations.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_NoOfLocations);

            DataColumn col_NoofEmployees = new DataColumn("NoofEmployees");
            col_NoofEmployees.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_NoofEmployees);

            DataColumn col_CurrentProvider = new DataColumn("CurrentProvider");
            col_CurrentProvider.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_CurrentProvider);

            DataColumn col_Priority1 = new DataColumn("Priority1");
            col_Priority1.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_Priority1);

            DataColumn col_Priority2 = new DataColumn("Priority2");
            col_Priority2.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_Priority2);

            DataColumn col_Priority3 = new DataColumn("Priority3");
            col_Priority3.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_Priority3);

            DataColumn col_LeadNotes = new DataColumn("LeadNotes");
            col_LeadNotes.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_LeadNotes);

            DataColumn col_CreatedDate = new DataColumn("CreatedDate");
            col_CreatedDate.DataType = typeof(DateTime);
            col_CreatedDate.DefaultValue = DateTime.Now;
            _FixedSchemaDataTable.Columns.Add(col_CreatedDate);

            Guid guidID = Guid.NewGuid();
            string guid = Convert.ToString(guidID).Substring(0, 8);
            DataColumn Col_ImportBatch = new DataColumn("_ImportBatch");
            Col_ImportBatch.DataType = Type.GetType("System.String");
            Col_ImportBatch.DefaultValue = guid;
            _FixedSchemaDataTable.Columns.Add(Col_ImportBatch);

            DataColumn col_fkApplicationId = new DataColumn("fkApplicationId");
            col_fkApplicationId.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_fkApplicationId);


            DataColumn col_fkCompanyTypeId = new DataColumn("fkCompanyTypeId");
            col_fkCompanyTypeId.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_fkCompanyTypeId);

            DataColumn col_CreatedById = new DataColumn("CreatedById");
            col_CreatedById.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_CreatedById);

            DataColumn col_LastModifiedById = new DataColumn("LastModifiedById");
            col_LastModifiedById.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_LastModifiedById);

            DataColumn col_LeadStatus = new DataColumn("LeadStatus");
            col_LeadStatus.DataType = typeof(int);
            _FixedSchemaDataTable.Columns.Add(col_LeadStatus);

            DataColumn col_LeadNumber = new DataColumn("LeadNumber");
            col_LeadNumber.DataType = Type.GetType("System.String");
            _FixedSchemaDataTable.Columns.Add(col_LeadNumber);

            DataColumn col_IsEnable = new DataColumn("IsEnable");
            col_IsEnable.DataType = typeof(bool);
            col_IsEnable.DefaultValue = false;
            _FixedSchemaDataTable.Columns.Add(col_IsEnable);

            DataColumn col_LastModifiedDate = new DataColumn("LastModifiedDate");
            col_LastModifiedDate.DataType = typeof(DateTime);
            col_LastModifiedDate.DefaultValue = DateTime.Now;
            _FixedSchemaDataTable.Columns.Add(col_LastModifiedDate);

            return _FixedSchemaDataTable;
        }

        private int InsertCSVDataToDB1(FormCollection frm)
        {
            #region Declaring variables
            DataTable DtFinal = null;
            //string CompanyType = string.Empty;
            string StateName = string.Empty;


            #endregion
            if (Session["StoreData"] != null)
            {
                DtFinal = (DataTable)Session["StoreData"];


                BALJuno ObjBALJuno = new BALJuno();
                BALGeneral ObjBALGeneral = new BALGeneral();

                //List<DataTable> JunoList = new List<DataTable>();
               // List<tblCompany> objcomapnylist = new List<tblCompany>();
                //List<tblLocation> objlocationlist = new List<tblLocation>();
                List<tblState> StateColl = new List<tblState>();
                List<tblCompanyType> CompanyTypeColl = new List<tblCompanyType>();
                StateColl = ObjBALGeneral.GetStates();
                CompanyTypeColl = ObjBALGeneral.GetCompanyTypes();
                List<tblCity> tblCityColl = new List<tblCity>();
                tblCityColl = ObjBALGeneral.GetCities();
                var AccManagerList = ObjBALJuno.GetManagerList("salesmanager");
                var salesAssList = ObjBALJuno.GetManagerList("salesrep");
                Guid UserId = Guid.Empty;
                MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
                if (ObjMembershipUser != null)
                {
                    UserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
                }
                DataTable tempJuno = CreateFixedSchemaDataTable();

                for (int iCount = 0; iCount < DtFinal.Rows.Count; iCount++)
                {

                    DataRow DR = tempJuno.NewRow();
                    tblCompany ObjtblCompany = new tblCompany();
                    tblLocation ObjtblLocation = new tblLocation();

                    DR["IsEnable"] = true;
                    DR["fkCompanyTypeId"] = Convert.ToByte(frm["ddlcompanyType"].ToString());
                    DR["fkApplicationId"] = Utility.SiteApplicationId;
                    DR["CreatedById"] = UserId;
                    DR["LastModifiedById"] = UserId;
                    DR["LeadStatus"] = 11;

                    if (DtFinal.Columns.Contains("Column" + (frm["ddlcompanyName"].ToString())))
                    {

                        DR["CompanyName"] = DtFinal.Rows[iCount]["Column" + (frm["ddlcompanyName"].ToString())].ToString().Trim();

                    }

                    if (frm["ddlCompanyUrl"].ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (frm["ddlCompanyUrl"].ToString())))
                        {
                            DR["CompanyUrl"] = DtFinal.Rows[iCount]["Column" + (frm["ddlCompanyUrl"].ToString())].ToString().Trim();

                        }
                    }
                    else
                    {
                        ObjtblCompany.CompanyUrl = string.Empty;
                        DR["CompanyUrl"] = string.Empty;
                    }

                    if (frm["ddlLeadNote"].ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (frm["ddlLeadNote"].ToString())))
                        {
                            DR["LeadNotes"] = DtFinal.Rows[iCount]["Column" + (frm["ddlLeadNote"].ToString())].ToString().Trim();

                        }
                    }

                    DR["LeadNumber"] = GenerateLeadNum();
                    DR["LeadSource"] = DtFinal.Rows[iCount]["Column" + (frm["ddlLeadSource"].ToString())].ToString().Trim();

                    if (frm["ddlAccountManager"].ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (frm["ddlAccountManager"].ToString())))
                        {
                            string AccManager = DtFinal.Rows[iCount]["Column" + (frm["ddlAccountManager"].ToString())].ToString().Trim();
                            ObjtblCompany.fkSalesManagerId = (AccManagerList.Where(d => d.FullName.ToLower() == AccManager.ToLower()).Select(db => db.fkUserId)).FirstOrDefault();
                            DR["AccountManager"] = ObjtblCompany.fkSalesManagerId != Guid.Empty ? ObjtblCompany.fkSalesManagerId : null;

                        }
                    }
                    else
                    {
                        DR["AccountManager"] = null;
                    }
                    if (frm["ddlSalesAssociate"].ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (frm["ddlSalesAssociate"].ToString())))
                        {
                            string AccManager = DtFinal.Rows[iCount]["Column" + (frm["ddlSalesAssociate"].ToString())].ToString().Trim();
                            ObjtblCompany.fkSalesRepId = (salesAssList.Where(d => d.FullName.ToLower() == AccManager.ToLower()).Select(db => db.fkUserId)).FirstOrDefault();
                            DR["SalesAssociate"] = ObjtblCompany.fkSalesRepId != Guid.Empty ? ObjtblCompany.fkSalesRepId : null;
                        }
                    }
                    else
                    {
                        DR["SalesAssociate"] = null;
                    }
                    if (frm["ddlAddress1"].ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (frm["ddlAddress1"].ToString())))
                        {
                            DR["Address1"] = DtFinal.Rows[iCount]["Column" + (frm["ddlAddress1"].ToString())].ToString().Trim();
                        }
                    }
                    else
                    {
                        DR["Address1"] = string.Empty;
                    }
                    if (frm["ddlAddress2"].ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (frm["ddlAddress2"].ToString())))
                        {
                            ObjtblLocation.Address2 = DtFinal.Rows[iCount]["Column" + (frm["ddlAddress2"].ToString())].ToString().Trim();
                            DR["Address2"] = DtFinal.Rows[iCount]["Column" + (frm["ddlAddress2"].ToString())].ToString().Trim();
                        }
                    }
                    else
                    {
                        DR["Address2"] = string.Empty;
                    }
                    if (DtFinal.Columns.Contains("Column" + (frm["ddlState"].ToString())))
                    {
                        StateName = DtFinal.Rows[iCount]["Column" + (frm["ddlState"].ToString())].ToString().Trim();

                        ObjtblLocation.fkStateID = (StateColl.Where(d => d.StateName.ToLower() == StateName.ToLower()).Select(db => db.pkStateId)).FirstOrDefault();
                        DR["State"] = ObjtblLocation.fkStateID == 0 ? (StateColl.Where(d => d.StateCode.ToLower() == StateName.ToLower()).Select(db => db.pkStateId)).FirstOrDefault() : ObjtblLocation.fkStateID;
                    }
                    if (frm["ddlCity"].ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (frm["ddlCity"].ToString())))
                        {
                            ObjtblLocation.City = DtFinal.Rows[iCount]["Column" + (frm["ddlCity"].ToString())].ToString().Trim();
                            DR["City"] = tblCityColl.Where(d => d.City.ToLower().Trim() == ObjtblLocation.City.ToLower()).Count() > 0 ? false : true;
                        }
                    }
                    else
                    {
                        DR["City"] = string.Empty;
                    }
                    if (frm["ddlZip"].ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (frm["ddlZip"].ToString())))
                        {
                            ObjtblLocation.ZipCode = DtFinal.Rows[iCount]["Column" + (frm["ddlZip"].ToString())].ToString().Trim();
                            string[] splitArr = { "-" };
                            var splitData = ObjtblLocation.ZipCode.Split(splitArr, StringSplitOptions.RemoveEmptyEntries);
                            DR["ZipCode"] = splitData.ElementAt(0).Trim();

                        }
                    }
                    else
                    {
                        DR["ZipCode"] = string.Empty;
                    }
                    if (DtFinal.Columns.Contains("Column" + (frm["ddlMainContact"].ToString())))
                    {
                        DR["MainContactPersonName"] = DtFinal.Rows[iCount]["Column" + (frm["ddlMainContact"].ToString())].ToString().Trim();
                    }
                    if (frm["ddlMainTitle"].ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (frm["ddlMainTitle"].ToString())))
                        {
                            DR["MainTitle"] = DtFinal.Rows[iCount]["Column" + (frm["ddlMainTitle"].ToString())].ToString().Trim();
                        }
                    }
                    else
                    {
                        DR["MainTitle"] = string.Empty;
                    }

                    if (DtFinal.Columns.Contains("Column" + (frm["ddlMainEmail"].ToString())))
                    {
                        DR["MainEmailId"] = DtFinal.Rows[iCount]["Column" + (frm["ddlMainEmail"].ToString())].ToString().Trim();
                    }
                    if (DtFinal.Columns.Contains("Column" + (frm["ddlMainPhone"].ToString())))
                    {
                        ObjtblLocation.PhoneNo1 = DtFinal.Rows[iCount]["Column" + (frm["ddlMainPhone"].ToString())].ToString().Trim();
                        string[] splitArr = { "x" };
                        var splitData = ObjtblLocation.PhoneNo1.Split(splitArr, StringSplitOptions.RemoveEmptyEntries);
                        DR["PhoneNo1"] = splitData.ElementAt(0).Trim();

                    }
                    if (frm["ddlMainPhoneExt"].ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (frm["ddlMainPhoneExt"].ToString())))
                        {
                            DR["Ext1"] = DtFinal.Rows[iCount]["Column" + (frm["ddlMainPhoneExt"].ToString())].ToString().Trim();
                        }
                    }
                    else
                    {
                        DR["Ext1"] = string.Empty;
                    }
                    if (frm["ddlMainFaxNumber"].ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (frm["ddlMainFaxNumber"].ToString())))
                        {
                            DR["MainFax"] = DtFinal.Rows[iCount]["Column" + (frm["ddlMainFaxNumber"].ToString())].ToString().Trim();
                        }
                    }
                    else
                    {
                        DR["MainFax"] = string.Empty;
                    }
                    if (frm["ddlSecondaryContact"].ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (frm["ddlSecondaryContact"].ToString())))
                        {
                            DR["SecondaryMainContactPersonName"] = DtFinal.Rows[iCount]["Column" + (frm["ddlSecondaryContact"].ToString())].ToString().Trim();
                        }
                    }
                    else
                    {
                        DR["SecondaryMainContactPersonName"] = string.Empty;
                    }
                    if (frm["ddlSecondaryTitle"].ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (frm["ddlSecondaryTitle"].ToString())))
                        {
                            DR["SecondaryTitle"] = DtFinal.Rows[iCount]["Column" + (frm["ddlSecondaryTitle"].ToString())].ToString().Trim();
                        }
                    }
                    else
                    {
                        DR["SecondaryTitle"] = string.Empty;
                    }
                    if (frm["ddlSecondaryEmail"].ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (frm["ddlSecondaryEmail"].ToString())))
                        {
                            DR["SecondaryEmail"] = DtFinal.Rows[iCount]["Column" + (frm["ddlSecondaryEmail"].ToString())].ToString().Trim();
                        }
                    }
                    else
                    {
                        DR["SecondaryEmail"] = string.Empty;
                    }
                    if (frm["ddlSecondaryPhone"].ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (frm["ddlSecondaryPhone"].ToString())))
                        {
                            DR["PhoneNo2"] = DtFinal.Rows[iCount]["Column" + (frm["ddlSecondaryPhone"].ToString())].ToString().Trim();
                        }
                    }
                    else
                    {
                        DR["PhoneNo2"] = string.Empty;
                    }
                    if (frm["ddlSecondaryPhoneExt"].ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (frm["ddlSecondaryPhoneExt"].ToString())))
                        {
                            DR["Ext2"] = DtFinal.Rows[iCount]["Column" + (frm["ddlSecondaryPhoneExt"].ToString())].ToString().Trim();
                        }
                    }
                    else
                    {
                        DR["Ext2"] = string.Empty;
                    }
                    if (frm["ddlSecondaryFaxNumber"].ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (frm["ddlSecondaryFaxNumber"].ToString())))
                        {
                            DR["SecondaryFax"] = DtFinal.Rows[iCount]["Column" + (frm["ddlSecondaryFaxNumber"].ToString())].ToString().Trim();
                        }
                    }
                    else
                    {
                        DR["SecondaryFax"] = string.Empty;
                    }
                    if (frm["ddlMonthlyVolume"].ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (frm["ddlMonthlyVolume"].ToString())))
                        {
                            int temp = 0;
                            int.TryParse(DtFinal.Rows[iCount]["Column" + (frm["ddlMonthlyVolume"].ToString())].ToString().Trim(), out temp);
                            DR["MonthlyVolume"] = temp;
                        }
                    }
                    else
                    {
                        DR["MonthlyVolume"] = 0;
                    }
                    if (frm["ddlNumberOfLocation"].ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (frm["ddlNumberOfLocation"].ToString())))
                        {
                            byte temp = 0;
                            Byte.TryParse(DtFinal.Rows[iCount]["Column" + (frm["ddlNumberOfLocation"].ToString())].ToString().Trim(), out temp);
                            DR["NoOfLocations"] = temp;
                        }
                    }
                    else
                    {
                        DR["NoOfLocations"] = 0;
                    }

                    if (frm["ddlNumberOfEmployee"].ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (frm["ddlNumberOfEmployee"].ToString())))
                        {
                            DR["NoofEmployees"] = DtFinal.Rows[iCount]["Column" + (frm["ddlNumberOfEmployee"].ToString())].ToString().Trim();
                        }
                    }
                    else
                    {
                        DR["NoofEmployees"] = "";
                    }

                    if (frm["ddlCurrentProvider"].ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (frm["ddlCurrentProvider"].ToString())))
                        {
                            DR["CurrentProvider"] = DtFinal.Rows[iCount]["Column" + (frm["ddlCurrentProvider"].ToString())].ToString().Trim();
                        }
                    }
                    else
                    {
                        DR["CurrentProvider"] = string.Empty;
                    }
                    if (frm["ddlPriority1"].ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (frm["ddlPriority1"].ToString())))
                        {
                            DR["Priority1"] = DtFinal.Rows[iCount]["Column" + (frm["ddlPriority1"].ToString())].ToString().Trim();
                        }
                    }
                    else
                    {
                        DR["Priority1"] = string.Empty;
                    }
                    if (frm["ddlPriority2"].ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (frm["ddlPriority2"].ToString())))
                        {
                            DR["Priority2"] = DtFinal.Rows[iCount]["Column" + (frm["ddlPriority2"].ToString())].ToString().Trim();
                        }
                    }
                    else
                    {
                        DR["Priority2"] = string.Empty;
                    }
                    if (frm["ddlPriority3"].ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (frm["ddlPriority3"].ToString())))
                        {
                            DR["Priority3"] = DtFinal.Rows[iCount]["Column" + (frm["ddlPriority3"].ToString())].ToString().Trim();
                        }
                    }
                    else
                    {
                        DR["Priority3"] = string.Empty;
                    }
                    tempJuno.Rows.Add(DR);
                }
                //int Results = 
                    ObjBALJuno.UploadJuno(tempJuno);// ObjBALJuno.InsertJuno(tempJuno);


            }
            return DtFinal.Rows.Count;
        }


        private string GenerateLeadNum()
        {
            string num = Utility.GeneraterRandom();
            if (ObjBALLeads.CheckLeadNumber(num))
            {
                return num;
            }
            else
            {
                GenerateLeadNum();
            }
            return string.Empty;
        }

        public FilePathResult DownloadSample1()
        {
            string FilePath = Server.MapPath("~/Resources/Upload/JunoFiles/NewSampleFile.csv");
            string fileext = System.IO.Path.GetExtension(FilePath);
            string CType = "application/" + fileext.Replace(".", "");
            string fileName = Path.GetFileName(FilePath);
            return File(FilePath, CType, fileName);
        }

        #endregion


        #region old code for Juno Upload

        //public ActionResult GetFileColumns(string SheetName, bool chkIsBatchHeaders)
        //{
        //    #region Declare Collection

        //    List<string> ControlList = new List<string>();
        //    List<string> FileColumns = new List<string>();
        //    List<SelectListItem> lstCompanyType = new List<SelectListItem>();
        //    #endregion

        //    int Status = 1;
        //    string sFileName = Convert.ToString(Session["sFileName"]);
        //    if (!sFileName.ToLower().Contains(".csv"))
        //    {
        //        Session["SheetName"] = SheetName;
        //        string ExcelConnectionString = Convert.ToString(Session["ExcelConnectionString"]);
        //        Session["ExcelConnectionString"] = ExcelConnectionString;
        //        int ReturnValue = GetExcelHeaders(ExcelConnectionString, SheetName, FileColumns);
        //        string Message = string.Empty;
        //        if (ReturnValue == 0)
        //        {
        //            Status = (chkIsBatchHeaders) ? 0 : -1;
        //        }

        //    }
        //    else
        //    {
        //        Status = GetCSVHeaders(FileColumns);
        //    }
        //    if (Status == 1)
        //    {
        //        ControlList.Add("ddlcompanyName");
        //        ControlList.Add("ddlState");
        //        ControlList.Add("ddlCity");
        //        ControlList.Add("ddlZip");
        //        ControlList.Add("ddlAddress");
        //        ControlList.Add("ddlMainContact");
        //        ControlList.Add("ddlMainEmail");
        //        ControlList.Add("ddlMainPhone");
        //        BALCompanyType ObjBALCompanyType = new BALCompanyType();
        //        var CompanyTypes = ObjBALCompanyType.GetAllCompanyType();
        //        if (CompanyTypes.Count != 0)
        //        {
        //            lstCompanyType.Clear();
        //            for (int iRow = 0; iRow < CompanyTypes.Count(); iRow++)
        //            {
        //                lstCompanyType.Add(new SelectListItem { Text = CompanyTypes.ElementAt(iRow).CompanyType, Value = CompanyTypes.ElementAt(iRow).pkCompanyTypeId.ToString() });
        //            }
        //        }
        //    }
        //    return Json(new { Status = Status, ControlList = ControlList, FileColumns = FileColumns, lstCompanyType = lstCompanyType });
        //}

        //public ActionResult ValidateJuno(FormCollection frm)
        //{
        //    bool isAllValid = false;
        //    StringBuilder st = new StringBuilder();
        //    int CountInvalid = 0;
        //    try
        //    {

        //        DataTable dtPreviewData = new DataTable();
        //        List<TempJuno> tempJunoArray = new List<TempJuno>();
        //        string sFileName = Convert.ToString(Session["sFileName"]);
        //        string sFilepath = Server.MapPath(@"~\Resources/Upload/JunoFiles/") + sFileName;
        //        tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlcompanyName", RegExp = "^[a-zA-Z' ']+$" });
        //        //tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlcompanyType", RegExp = "^[a-zA-Z' ']+$" });
        //        //tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlLeadSource", RegExp = "^[a-zA-Z' ']+$" });
        //        if (frm.GetValue("ddlZip") != null)
        //        {
        //            if (frm["ddlZip"].ToString() != "-1")
        //            {
        //                tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlZip", RegExp = @"^\d{5}$" });
        //            }
        //        }
        //        tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlState", RegExp = "^[a-zA-Z' ']+$" });
        //        tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlCity", RegExp = "^[a-zA-Z' ']+$" });
        //        if (frm["ddlMainContact"].ToString() != "-1")
        //        {
        //            tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlMainContact", RegExp = "^[a-zA-Z' ']+$" });
        //        }
        //        if (frm["ddlMainEmail"].ToString() != "-1")
        //        {
        //            tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlMainEmail", RegExp = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" });
        //        }
        //        tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlMainPhone", RegExp = @"((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}" });
        //        if (!sFileName.ToLower().Contains(".csv"))
        //        {
        //            #region Excel Step 3


        //            OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
        //            objDataAdapter.SelectCommand = GetExcelFileInformation(Session["ExcelConnectionString"].ToString());
        //            DataSet objDataSet = new DataSet();
        //            objDataAdapter.Fill(objDataSet);

        //            List<string> lstExcelHeaders = new List<string>();
        //            StringBuilder sbValidations = new StringBuilder();

        //            for (int i = 0; i < objDataSet.Tables[0].Columns.Count; i++)
        //            {
        //                lstExcelHeaders.Add(objDataSet.Tables[0].Columns[i].Caption.Trim());
        //                dtPreviewData.Columns.Add("Column" + i.ToString(), Type.GetType("System.String"));
        //            }

        //            if (frm["chkIsJunoHeaders"].ToString().ToLower() == "false")
        //            {
        //                DataRow dr = dtPreviewData.NewRow();
        //                for (int iRow = 0; iRow < dtPreviewData.Columns.Count; iRow++)
        //                {
        //                    dr[iRow] = objDataSet.Tables[0].Columns[iRow].Caption.Trim();

        //                }
        //                dtPreviewData.Rows.Add(dr);
        //            }

        //            for (int total = 0; total < objDataSet.Tables[0].Rows.Count; total++)
        //            {
        //                DataRow dr = dtPreviewData.NewRow();

        //                for (int iRow = 0; iRow < dtPreviewData.Columns.Count; iRow++)
        //                {
        //                    dr[iRow] = objDataSet.Tables[0].Rows[total][lstExcelHeaders[iRow].ToString()];
        //                }
        //                dtPreviewData.Rows.Add(dr);
        //            }
        //            #endregion Excel Step 3
        //        }
        //        else
        //        {
        //            #region CSV Step 3
        //            StreamReader sr = new StreamReader(sFilepath);
        //            try
        //            {
        //                if (Session["DtCSVHeaders"] != null)
        //                {
        //                    DtFileData = (DataTable)Session["DtCSVHeaders"];
        //                }
        //                else
        //                {
        //                    GetCSVHeaders();
        //                }
        //                for (int i = 0; i < DtFileData.Rows.Count; i++)
        //                {
        //                    dtPreviewData.Columns.Add("Column" + i.ToString(), Type.GetType("System.String"));

        //                }
        //                string[] arrFields;
        //                bool IsAddHeaders = false;

        //                using (CsvReader ObjCSV = new CsvReader(sr, true, ','))
        //                {
        //                    arrFields = ObjCSV.GetFieldHeaders();

        //                    if (frm["chkIsJunoHeaders"].ToString().ToLower() == "false" && IsAddHeaders == false)
        //                    {
        //                        DataRow dr = dtPreviewData.NewRow();
        //                        for (int iRow = 0; iRow < arrFields.Count(); iRow++)
        //                        {
        //                            dr[iRow] = arrFields[iRow].ToString();
        //                        }
        //                        dtPreviewData.Rows.Add(dr);
        //                    }

        //                    while (ObjCSV.ReadNextRecord())
        //                    {
        //                        IsAddHeaders = true;
        //                        DataRow dr = dtPreviewData.NewRow();
        //                        for (int iRow = 0; iRow < dtPreviewData.Columns.Count; iRow++)
        //                        {
        //                            dr[iRow] = ObjCSV[iRow];
        //                        }
        //                        dtPreviewData.Rows.Add(dr);
        //                    }
        //                }

        //            }
        //            catch (Exception ex)
        //            {
        //                throw ex;
        //            }
        //            finally
        //            {
        //                sr.Close();
        //            }
        //            #endregion CSV Step 3
        //        }

        //        DataTable dt = GetCSVPreviewAfter(dtPreviewData, tempJunoArray, ref isAllValid, ref CountInvalid, frm);

        //        if (!isAllValid)
        //        {
        //            Session["Invalid"] = false;
        //        }
        //        else if (dtPreviewData.Rows.Count == 0)
        //        {
        //        }
        //        else
        //        {
        //            Session["Invalid"] = true;
        //        }

        //        Session["StoreData"] = dtPreviewData;
        //        st.Append("<table cellpadding='2' cellspacing='0' border='1' width='565px' style='border:1px solid black;'><tr>");
        //        st.Append("<td align='center'><b>Columns</b></td>");
        //        st.Append("<td  align='center'><b>Row1</b></td>");
        //        st.Append("<td  align='center'><b>Row2</b></td>");
        //        st.Append("<td  align='center'><b>Row3</b></td>");
        //        st.Append("<td  align='center'><b>.....</b></td>");
        //        st.Append("<td  align='center'><b>Validate</b></td></tr>");

        //        for (int irow = 0; irow < dt.Rows.Count; irow++)
        //        {
        //            st.Append("<tr>");
        //            for (int icol = 0; icol < dt.Columns.Count; icol++)
        //            {
        //                st.Append("<td>" + dt.Rows[irow][icol] + "</td>");
        //            }
        //            st.Append("</tr>");

        //        }
        //        st.Append("</table>");
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return Json(new { grid = st.ToString(), CountInvalid = CountInvalid });
        //}

        //protected DataTable GetCSVPreviewAfter(DataTable dt, List<TempJuno> tempJunoArray, ref bool isAllValid, ref int CountInvalid, FormCollection frm)
        //{
        //    StringBuilder sHTML, sInValidCell;
        //    sHTML = new StringBuilder();
        //    sInValidCell = new StringBuilder();
        //    sHTML.Append("");
        //    sInValidCell.Append("");
        //    BALGeneral ObjBALGeneral = new BALGeneral();
        //    List<tblState> tblStateColl = new List<tblState>();
        //    List<tblCity> tblCityColl = new List<tblCity>();
        //    List<tblCompanyType> tblCompanyTypeColl = new List<tblCompanyType>();
        //    List<tblLocation> tblLocationColl = new List<tblLocation>();
        //    List<tblCompany> tblCompanyColl = new List<tblCompany>();
        //    tblStateColl = ObjBALGeneral.GetStates();
        //    tblCityColl = ObjBALGeneral.GetCities();
        //    tblCompanyTypeColl = ObjBALGeneral.GetCompanyTypes();
        //    tblLocationColl = ObjBALGeneral.GetMainPhone();
        //    tblCompanyColl = ObjBALGeneral.GetMainEmailId();
        //    DataTable dtPreviewGrd = new DataTable();

        //    dtPreviewGrd.Columns.Add("column", Type.GetType("System.String"));
        //    dtPreviewGrd.Columns.Add("r1", Type.GetType("System.String"));
        //    dtPreviewGrd.Columns.Add("r2", Type.GetType("System.String"));
        //    dtPreviewGrd.Columns.Add("r3", Type.GetType("System.String"));
        //    dtPreviewGrd.Columns.Add("r4", Type.GetType("System.String"));
        //    dtPreviewGrd.Columns.Add("Valid", Type.GetType("System.String"));

        //    //Dictionary<string, int> DDLControl = new Dictionary<string, int>();
        //    //if (frm["chkIsJunoHeaders"].ToString()ddlcompanyName != "-1")
        //    //{
        //    //    DDLControl.Add("ddlcompanyName", Convert.ToInt32(ddlcompanyName));
        //    //}
        //    //if (frm["chkIsJunoHeaders"].ToString()ddlState != "-1")
        //    //{
        //    //    DDLControl.Add("ddlState", Convert.ToInt32(ddlState));
        //    //}
        //    //if (ddlCity != "-1")
        //    //{
        //    //    DDLControl.Add("ddlCity", Convert.ToInt32(ddlCity));
        //    //}
        //    //if (ddlZip != "-1")
        //    //{
        //    //    DDLControl.Add("ddlZip", Convert.ToInt32(ddlZip));
        //    //}
        //    //if (ddlAddress != "-1")
        //    //{
        //    //    DDLControl.Add("ddlAddress", Convert.ToInt32(ddlAddress));
        //    //}
        //    //if (ddlMainContact != "-1")
        //    //{
        //    //    DDLControl.Add("ddlMainContact", Convert.ToInt32(ddlMainContact));
        //    //}
        //    //if (ddlMainEmail != "-1")
        //    //{
        //    //    DDLControl.Add("ddlMainEmail", Convert.ToInt32(ddlMainEmail));
        //    //}
        //    //if (ddlMainPhone != "-1")
        //    //{
        //    //    DDLControl.Add("ddlMainPhone", Convert.ToInt32(ddlMainPhone));
        //    //}
        //    bool IsRowVlaid = true;
        //    isAllValid = true;
        //    int iprev = -1, piRow = -1;
        //    try
        //    {
        //        for (int iRow = 0; iRow < dt.Rows.Count; iRow++)
        //        {
        //            if (iRow == 0 && frm["chkIsJunoHeaders"].ToString().ToLower() == "true")
        //            {
        //                //iRow++;
        //            }
        //            for (int iarr = 0; iarr < tempJunoArray.Count(); iarr++)
        //            {
        //                string ddlname = tempJunoArray.ElementAt(iarr).JunoRequiredFields.ToString();//ddl +
        //                string CellName = "";
        //                string[] Value = frm.GetValue(ddlname).AttemptedValue.Split(',');
        //                int No = 0;
        //                if (Value.Length > 0)
        //                {
        //                    No = Convert.ToInt32(Value[0]);
        //                }
        //                IsRowVlaid = RowValidation(tempJunoArray.ElementAt(iarr).JunoRequiredFields.ToString(), dt.Rows[iRow][No].ToString(), tempJunoArray.ElementAt(iarr).RegExp, tblStateColl, tblCityColl, tblCompanyTypeColl, tblCompanyColl, tblLocationColl);
        //                if (!IsRowVlaid)
        //                {
        //                    CountInvalid++;
        //                    isAllValid = false;
        //                    CellName = "<img src='../Content/themes/base/images/publish_x.png'/>";  // GetCellAddress(iRow, No);
        //                }
        //                else
        //                {
        //                    CellName = "<img src='../Content/themes/base/images/successmsg.png'/>";
        //                }

        //                //foreach (KeyValuePair<string, int> data in DDLControl)
        //                //{
        //                //    d = data.Value;
        //                //    ddln = data.Key;
        //                //    ddln = tblMap.FindControl(ddlname) as DropDownList;
        //                //    if (ddln != string.Empty)
        //                //    {
        //                //        IsRowVlaid = RowValidation(tempJunoArray.ElementAt(iarr).JunoRequiredFields.ToString(), dt.Rows[iRow][d].ToString(), tempJunoArray.ElementAt(iarr).RegExp, tblStateColl, tblCityColl, tblCompanyTypeColl, tblCompanyColl, tblLocationColl);
        //                //        if (!IsRowVlaid)
        //                //        {
        //                //            CountInvalid++;
        //                //            DontBind = false;
        //                //            CellName = GetCellAddress(iRow, d, chkIsJunoHeaders);
        //                //        }
        //                //        else
        //                //        {
        //                //            CellName = GetCellAddress(iRow, d, chkIsJunoHeaders);
        //                //        }
        //                //if (DontBind)
        //                //{
        //                //    try
        //                //    {
        //                //        IsRowVlaid = RowValidation(tempJunoArray.ElementAt(iarr).JunoRequiredFields.ToString(), dt.Rows[iRow][d].ToString(), tempJunoArray.ElementAt(iarr).RegExp, tblStateColl, tblCityColl, tblCompanyTypeColl, tblCompanyColl, tblLocationColl);
        //                //        if (!IsRowVlaid) { CountInvalid++; DontBind = false; CellName = GetCellAddress(iRow, d, chkIsJunoHeaders); }
        //                //    }
        //                //    catch (Exception ex) { throw ex; }
        //                //}
        //                //else
        //                //{
        //                //    try
        //                //    {
        //                //        IsRowVlaid = RowValidation(tempJunoArray.ElementAt(iarr).JunoRequiredFields.ToString(), dt.Rows[iRow][d].ToString(), tempJunoArray.ElementAt(iarr).RegExp, tblStateColl, tblCityColl, tblCompanyTypeColl, tblCompanyColl, tblLocationColl);
        //                //        if (!IsRowVlaid) { CountInvalid++; CellName = GetCellAddress(iRow, d, chkIsJunoHeaders); }
        //                //    }
        //                //    catch (Exception ex)
        //                //    {
        //                //        throw ex;
        //                //    }

        //                //}
        //                #region Logic for preview

        //                Logic4Preview(dt, dtPreviewGrd, ddlname, ref iprev, ref piRow, iRow, No, CellName);

        //                #endregion
        //            }
        //        }
        //        //gvPreview.DataSource = dtPreviewGrd;
        //        //gvPreview.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return dtPreviewGrd;
        //}

        //public bool RowValidation(string ColumnName, string CellData, string Expr, List<tblState> StateColl, List<tblCity> CityColl, List<tblCompanyType> CompanyTypeColl, List<tblCompany> CompanyColl, List<tblLocation> LocationColl)
        //{
        //    bool IsCellValid = false;
        //    if (Expr != "")
        //    {
        //        Regex reg = new Regex(Expr);
        //        IsCellValid = (reg.IsMatch(CellData)) ? true : false;
        //    }
        //    else { IsCellValid = true; }
        //    if (ColumnName.ToLower().Contains("state"))
        //    {
        //        IsCellValid = (StateColl.Where(d => d.StateName.ToLower() == CellData.ToLower()).Count() > 0) ? true : false;
        //    }
        //    else if (ColumnName.ToLower().Contains("city"))
        //    {
        //        IsCellValid = (CityColl.Where(d => d.City.ToLower() == CellData.ToLower()).Count() > 0) ? true : false;
        //    }
        //    else if (ColumnName.ToLower().Contains("companytype"))
        //    {
        //        IsCellValid = (CompanyTypeColl.Where(d => d.CompanyType.ToLower() == CellData.ToLower()).Count() > 0) ? true : false;
        //    }
        //    else if (ColumnName.ToLower().Contains("mainemail") && IsCellValid == true)
        //    {
        //        IsCellValid = (CompanyColl.Where(d => d.MainEmailId.ToLower() == CellData.ToLower()).Count() > 0) ? false : true;
        //    }
        //    else if (ColumnName.ToLower().Contains("mainphone") && IsCellValid == true)
        //    {
        //        IsCellValid = (LocationColl.Where(d => d.PhoneNo1.ToLower() == CellData.ToLower()).Count() > 0) ? false : true;
        //    }
        //    return IsCellValid;
        //}

        //public ActionResult InsertJuno(FormCollection frm)
        //{
        //    int status = 0;
        //    string messresult = "";
        //    if (Session["Invalid"] != null)
        //    {
        //        if (Convert.ToBoolean(Session["Invalid"].ToString()) == false)
        //        {
        //            status = 0;
        //            messresult = "";
        //        }
        //        else
        //        {

        //            # region add junos in DB
        //            DataTable DtFinal = null;
        //            if (Session["StoreData"] != null)
        //            {
        //                DtFinal = (DataTable)Session["StoreData"];
        //                int Success = InsertCSVDataToDB1(frm);
        //                if (Success != DtFinal.Rows.Count)
        //                {
        //                    messresult = "Some error occurred while saving data.";
        //                    status = -1;
        //                }
        //                else if (Success != DtFinal.Rows.Count)
        //                {
        //                    messresult = "Only " + Success + " Juno Prospect(s) Saved Successfully.";
        //                    status = -1;
        //                }
        //                else
        //                {
        //                    status = 1;
        //                }
        //            }
        //            # endregion
        //        }
        //    }
        //    return Json(new { status = status, messresult = messresult });
        //}

        //private int InsertCSVDataToDB(FormCollection frm)
        //{
        //    #region Declaring variables
        //    DataTable DtFinal = null;
        //    string CompanyType = string.Empty;
        //    string StateName = string.Empty;
        //    #endregion
        //    if (Session["StoreData"] != null)
        //    {
        //        DtFinal = (DataTable)Session["StoreData"];
        //        BALJuno ObjBALJuno = new BALJuno();

        //        BALGeneral ObjBALGeneral = new BALGeneral();
        //        tblCompany ObjtblCompany = new tblCompany();
        //        tblLocation ObjtblLocation = new tblLocation();
        //        List<tblState> StateColl = new List<tblState>();
        //        List<tblCompanyType> CompanyTypeColl = new List<tblCompanyType>();
        //        StateColl = ObjBALGeneral.GetStates();
        //        CompanyTypeColl = ObjBALGeneral.GetCompanyTypes();
        //        Guid UserId = Guid.Empty;
        //        MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
        //        if (ObjMembershipUser != null)
        //        {
        //            UserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
        //        }
        //        ObjtblCompany.fkCompanyTypeId = Convert.ToByte(frm["ddlcompanyType"].ToString());
        //        ObjtblCompany.LeadSource = frm["TxtLeadSource"].ToString().Trim();
        //        ObjtblCompany.fkApplicationId = Utility.SiteApplicationId;
        //        ObjtblCompany.CreatedById = UserId;
        //        ObjtblCompany.LastModifiedById = UserId;
        //        ObjtblCompany.LeadStatus = 11;// 11 - New Prospect status for juno. 
        //        ObjtblLocation.CreatedById = UserId;
        //        for (int iCount = 0; iCount < DtFinal.Rows.Count; iCount++)
        //        {
        //            if (DtFinal.Columns.Contains("Column" + (frm["ddlcompanyName"].ToString())))
        //            {
        //                ObjtblCompany.CompanyName = DtFinal.Rows[iCount]["Column" + (frm["ddlcompanyName"].ToString())].ToString().Trim();
        //                ObjtblCompany.CompanyCode = Utility.GenerateCompanyCode(ObjtblCompany.CompanyName, DateTime.Now);
        //                ObjtblLocation.LocationCode = Utility.GenerateLocationCode(ObjtblCompany.CompanyName, DateTime.Now);
        //            }
        //            //if (DtFinal.Columns.Contains("Column" + (Convert.ToInt32(ddlcompanyType.SelectedValue) - 1).ToString()))
        //            //{
        //            //    CompanyType = DtFinal.Rows[iCount]["Column" + (Convert.ToInt32(ddlcompanyType.SelectedValue) - 1).ToString()].ToString().Trim();
        //            //    ObjtblCompany.fkCompanyTypeId = (CompanyTypeColl.Where(d => d.CompanyType.ToLower() == CompanyType.ToLower()).Select(db => db.pkCompanyTypeId)).FirstOrDefault();
        //            //}
        //            //if (DtFinal.Columns.Contains("Column" + (Convert.ToInt32(ddlLeadSource.SelectedValue) - 1).ToString()))
        //            //{
        //            //    ObjtblCompany.LeadSource = DtFinal.Rows[iCount]["Column" + (Convert.ToInt32(ddlLeadSource.SelectedValue) - 1).ToString()].ToString().Trim();
        //            //}
        //            if (DtFinal.Columns.Contains("Column" + (frm["ddlState"].ToString())))
        //            {
        //                StateName = DtFinal.Rows[iCount]["Column" + (frm["ddlState"].ToString())].ToString().Trim();

        //                ObjtblLocation.fkStateID = (StateColl.Where(d => d.StateName.ToLower() == StateName.ToLower()).Select(db => db.pkStateId)).FirstOrDefault();
        //            }
        //            if (DtFinal.Columns.Contains("Column" + (frm["ddlCity"].ToString())))
        //            {
        //                ObjtblLocation.City = DtFinal.Rows[iCount]["Column" + (frm["ddlCity"].ToString())].ToString().Trim();
        //            }
        //            if (frm["ddlMainContact"].ToString() != "-1")
        //            {
        //                if (DtFinal.Columns.Contains("Column" + (frm["ddlMainContact"].ToString())))
        //                {
        //                    ObjtblCompany.MainContactPersonName = DtFinal.Rows[iCount]["Column" + (frm["ddlMainContact"].ToString())].ToString().Trim();
        //                }
        //            }
        //            else
        //            {
        //                ObjtblCompany.MainContactPersonName = string.Empty;
        //            }
        //            if (frm["ddlMainEmail"].ToString() != "-1")
        //            {
        //                if (DtFinal.Columns.Contains("Column" + (frm["ddlMainEmail"].ToString())))
        //                {
        //                    ObjtblCompany.MainEmailId = DtFinal.Rows[iCount]["Column" + (frm["ddlMainEmail"].ToString())].ToString().Trim();
        //                }
        //            }
        //            else
        //            {
        //                ObjtblCompany.MainEmailId = string.Empty;
        //            }
        //            if (frm["ddlAddress"].ToString() != "-1")
        //            {
        //                if (DtFinal.Columns.Contains("Column" + (frm["ddlAddress"].ToString())))
        //                {
        //                    ObjtblLocation.Address1 = DtFinal.Rows[iCount]["Column" + (frm["ddlAddress"].ToString())].ToString().Trim();
        //                }
        //            }
        //            else
        //            {
        //                ObjtblLocation.Address1 = string.Empty;
        //            }
        //            if (frm["ddlZip"].ToString() != "-1")
        //            {
        //                if (DtFinal.Columns.Contains("Column" + (frm["ddlZip"].ToString())))
        //                {
        //                    ObjtblLocation.ZipCode = DtFinal.Rows[iCount]["Column" + (frm["ddlZip"].ToString())].ToString().Trim();
        //                }
        //            }
        //            else
        //            {
        //                ObjtblLocation.ZipCode = string.Empty;
        //            }
        //            if (DtFinal.Columns.Contains("Column" + (frm["ddlMainPhone"].ToString())))
        //            {
        //                ObjtblLocation.PhoneNo1 = DtFinal.Rows[iCount]["Column" + (frm["ddlMainPhone"].ToString())].ToString().Trim();
        //            }

        //            int result = ObjBALJuno.InsertJuno(ObjtblCompany, ObjtblLocation);
        //            if (result != 1)
        //            {
        //                return iCount;
        //            }

        //        }
        //    }
        //    return DtFinal.Rows.Count;
        //}

        //public FilePathResult DownloadSample()
        //{
        //    string FilePath = Server.MapPath("~/Resources/Upload/JunoFiles/SampleFile.csv");
        //    string fileext = System.IO.Path.GetExtension(FilePath);
        //    string CType = "application/" + fileext.Replace(".", "");
        //    string fileName = Path.GetFileName(FilePath);
        //    return File(FilePath, CType, fileName);
        //}

        #endregion

        public ActionResult PartialRunJunoUnique()
        {
            return View();
        }

        [HttpGet]
        public ActionResult UpdateInterval(List<int> CompanyId)
        {
            string strMessage = "";
            BALJuno ObjBALJuno = new BALJuno();
            Guid UserId = Utility.GetUID(User.Identity.Name);
            DateTime Currentdate = DateTime.Now;
            var tblRowList = new List<tblJunoInterval>();
            if (CompanyId != null)
            {
                for (int i = 0; i < CompanyId.Count(); i++)  // Create a new entity and set its properties from the posted CompanyId
                {
                    var newtblRow = new tblJunoInterval
                    {
                        fkUserId = UserId,
                        CreatedDate = Currentdate,
                        fkCompanyId = Int32.Parse(CompanyId.ElementAt(i).ToString())
                    };
                    tblRowList.Add(newtblRow); // Store the entity for later use
                }
              //  var updateResult = 
                    ObjBALJuno.CreateJunoInterval(tblRowList);//Create new Table rows
            }
            return Json(new { UpdateMessage = strMessage });

        }

        [HttpPost]
        public ActionResult CheckJunoStatus(List<JunoList> JunoList)
        {
            BALJuno ObjBALJuno = new BALJuno();
            List<DeletedJunoList> ObjDlJunoList = new List<DeletedJunoList>();
            var Collection = ObjBALJuno.GetEditLeadList(JunoList);
            if (Collection.Count > 0)
            {
                for (int i = 0; i < Collection.Count; i++)
                {
                    if (JunoList.Count > 0)
                    {
                        for (int j = 0; j < JunoList.Count; j++)
                        {
                            if (JunoList.ElementAt(j).pkCompanyId == Collection.ElementAt(i).pkCompanyId)
                            {
                                if (JunoList.ElementAt(j).LeadStatus != Collection.ElementAt(i).LeadStatus)
                                {
                                    ObjDlJunoList.Add(new DeletedJunoList() { pkCompanyId = JunoList.ElementAt(j).pkCompanyId });
                                }
                            }
                        }
                    }
                }
            }
            return Json(new { List = ObjDlJunoList });
        }
    }

}
