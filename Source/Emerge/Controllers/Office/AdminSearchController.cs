﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Data;
using Emerge.Services;
using Emerge.Models;
using System.Web.Security;

namespace Emerge.Controllers.Office
{
    public partial class OfficeController : Controller
    {
        //
        // GET: /AdminSearch/

        public ActionResult AdminSearch()
        {
             MembershipUser objMembershipUser = Membership.GetUser();

             var UserName = objMembershipUser.UserName;
             if (Roles.IsUserInRole(UserName, "SystemAdmin"))//Roles.IsUserInRole(UserName, "Admin") || 
             {
                 ViewBag.QualityContentsofficeDiv = "expand";
                 ViewBag.DemomContensofficeDiv = "expand";
             }
             else
             {
                 List<tblDashboardList> Records = new BALCompanyUsers().GetDashBoardStatusByUserId(Guid.Parse(objMembershipUser.ProviderUserKey.ToString()));


                 bool DemomContensofficeDiv = Records.Where(rec => rec.DivName == "DemomContensofficeDiv").ElementAt(0).IsDivOpen;
                 if (DemomContensofficeDiv == true)
                 {
                     ViewBag.DemomContensofficeDiv = "expand";
                 }
                 else
                 {
                     ViewBag.DemomContensofficeDiv = "colapse";
                 }

                 bool QualityContentsofficeDiv = Records.Where(rec => rec.DivName == "QualityContentsofficeDiv").ElementAt(0).IsDivOpen;
                 if (QualityContentsofficeDiv == true)
                 {
                     ViewBag.QualityContentsofficeDiv = "expand";
                 }
                 else
                 {
                     ViewBag.QualityContentsofficeDiv = "colapse";
                 }
             }


            return View();

        }

    }
}
