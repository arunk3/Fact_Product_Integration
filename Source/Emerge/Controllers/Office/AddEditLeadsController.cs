﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Emerge.Common;
using Emerge.Models;
using Emerge.Data;
using System.Web.Security;
using System.IO;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Text;
using System.Configuration;


namespace Emerge.Controllers.Office
{
    public partial class OfficeController : Controller
    {
        #region  // Global  Variable Definition
        List<int> ObjCompLocation = new List<int>();
        int objlocationid = 0;
        public int StausType = 0;
        List<tblOfficeDocumentSent> objColltblOfficeDocumentSent = new List<tblOfficeDocumentSent>();
        BALLeads ObjBALLeads = new BALLeads();
        #endregion

        #region // Page Load Method
        public ActionResult AddEditLeads()
        {
            AddEditLeadsModel objModel = new AddEditLeadsModel();
            BALCompany objbalCompany = new BALCompany();
            bool IsDuplicate = false;
            string Num = string.Empty;
            BALLeads ObjBALLeads = new BALLeads();
            //BALCompany ObjBALCompany = new BALCompany();
            int? fkCompanyId = null;
            MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));

            objModel.imgbtn_1 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-1-off.png");
            objModel.imgbtn_2 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-2-off.png");
            objModel.imgbtn_3 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-3-off.png");
            objModel.imgbtn_4 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-4-off.png");
            //  objModel.leadStatusTxt = "imgbtn_1";
            if (Request.QueryString["Add"] != null)
            {
                string Addstatus = Request.QueryString["Add"].ToString();
                if (Addstatus == "done")
                {
                    objModel.successmsg = "Lead Created Successfully";
                }

            }
            try
            {
                if (Request.QueryString["Status"] != null)
                {
                    int status = Convert.ToInt32(Request.QueryString["Status"]);
                    if (status == 1)
                    {
                        objModel.strMessage = "PDF Uploaded Successfully";
                    }
                    else if (status == 2)
                    {
                        objModel.strMessage = "Error Occurred While Add Lead try Again latter";
                    }

                }

                else if (Request.QueryString["_a"] != null)
                {
                    objModel.IsEdit = true;
                    objModel.LeadSourceJuno = "N_Juno";
                    ViewBag.locationid = Convert.ToInt16(Request.QueryString["_a"]);
                    var locationid1 = Request.QueryString["_a"];

                    #region For Forward/ Backward Functionlity
                    if (Request.QueryString["Add"] == null || Request.QueryString["Add"] != "done")
                    {
                        if (Request.QueryString["Update"] == null || Request.QueryString["Update"] != "update")
                        {
                            if (Session["LeadForwardBackwardInfo"] != null)
                            {
                                var LeadForwardBackwardInfo = (List<LeadForwardBackwardInfo>)Session["LeadForwardBackwardInfo"];

                                if (LeadForwardBackwardInfo.Count > 0)
                                {
                                    objModel._TotalRecords = LeadForwardBackwardInfo.ElementAt(0).TotalCount;
                                    objModel._CurrentPageNum = LeadForwardBackwardInfo.FirstOrDefault(n => n.Id.ToString() == locationid1).RowIndex;
                                    objModel._NextRecoedId = objModel._CurrentPageNum < objModel._TotalRecords ? "AddEditLeads?_a=" + LeadForwardBackwardInfo.FirstOrDefault(n => n.RowIndex == (objModel._CurrentPageNum + 1)).Id + "&bck=l" : "javaScript:void(0)";
                                    objModel._PreviousRecoedId = objModel._CurrentPageNum > 1 ? "AddEditLeads?_a=" + LeadForwardBackwardInfo.FirstOrDefault(n => n.RowIndex == (objModel._CurrentPageNum - 1)).Id + "&bck=l" : "javaScript:void(0)";
                                }
                            }
                        }
                    }
                    #endregion

                    Session["pkLocationIdWhenEdit"] = Convert.ToInt16(Request.QueryString["_a"]);

                    var list1 = objbalCompany.GetCompanyDetailByLocationId(Convert.ToInt16(locationid1));
                    Session["pkCompanyIdWhenEdit"] = list1.ElementAt(0).fkCompanyId;
                    objModel.pkCompanyId = (list1.ElementAt(0).fkCompanyId.GetValueOrDefault() != null) ? list1.ElementAt(0).fkCompanyId.GetValueOrDefault() : 0;
                    objModel.pklocationid = Convert.ToInt16(Request.QueryString["_a"]);
                    Session["LeadStatus"] = list1.ElementAt(0).LeadStatus;
                    objModel.GettblOfficedocuments = ObjBALLeads.GetDocuments();
                    objModel.LeadStatus = list1.ElementAt(0).LeadStatus;
                    var Uname = ObjMembershipUser.UserName;
                    objModel.IsMainEmail = Convert.ToBoolean(list1.ElementAt(0).IsMainEmail);
                    objModel.apptDate = Convert.ToDateTime(list1.ElementAt(0).ApptDate);
                    objModel.nextStep = Convert.ToString(list1.ElementAt(0).NextStep);


                    string CityName = list1.ElementAt(0).City;
                   // BALCompanyType ObjBALCompanyType = new BALCompanyType();


                    var username = Uname.Split('@');
                    ViewBag.Username = username[0].ToString();
                    //var list3 =
                    ObjBALLeads.GetDocuments();


                    if (Session["LeadStatus"] == null)
                    {
                        objModel.imgbtn_1 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-1-off.png");


                        objModel.imgbtn_2 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-2-off.png");
                        objModel.imgbtn_3 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-3-off.png");
                        objModel.imgbtn_4 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-4-off.png");

                    }
                    else if (Convert.ToByte((Session["LeadStatus"])) == 1)
                    {

                        //byte status = Convert.ToByte((Session["LeadStatusView"]));
                        objModel.imgbtn_1 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-1-on.png");
                        objModel.imgbtn_2 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-2-off.png");
                        objModel.imgbtn_3 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-3-off.png");
                        objModel.imgbtn_4 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-4-off.png");
                        objModel.imgbtn_5 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-5-off.png");


                    }

                    else if (Convert.ToByte((Session["LeadStatus"])) == 2)
                    {

                        //byte status = Convert.ToByte((Session["LeadStatusView"]));
                        objModel.imgbtn_1 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-1-on.png");
                        objModel.imgbtn_2 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-2-on.png");
                        objModel.imgbtn_3 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-3-off.png");
                        objModel.imgbtn_4 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-4-off.png");
                        objModel.imgbtn_5 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-5-off.png");


                    }
                    else if (Convert.ToByte((Session["LeadStatus"])) == 3)
                    {

                        //byte status = Convert.ToByte((Session["LeadStatusView"]));
                        objModel.imgbtn_1 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-1-on.png");
                        objModel.imgbtn_2 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-2-on.png");
                        objModel.imgbtn_3 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-3-on.png");
                        objModel.imgbtn_4 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-4-off.png");
                        objModel.imgbtn_5 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-5-off.png");


                    }
                    else if (Convert.ToByte((Session["LeadStatus"])) == 4)
                    {

                        //byte status = Convert.ToByte((Session["LeadStatusView"]));
                        objModel.imgbtn_1 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-1-on.png");
                        objModel.imgbtn_2 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-2-on.png");
                        objModel.imgbtn_3 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-3-on.png");
                        objModel.imgbtn_4 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-4-on.png");
                        objModel.imgbtn_5 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-5-off.png");


                    }
                    else if (Convert.ToByte((Session["LeadStatus"])) == 5)
                    {

                        //byte status = Convert.ToByte((Session["LeadStatusView"]));
                        objModel.imgbtn_1 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-1-on.png");
                        objModel.imgbtn_2 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-2-on.png");
                        objModel.imgbtn_3 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-3-on.png");
                        objModel.imgbtn_4 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-1-on.png");
                        objModel.imgbtn_5 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-1-on.png");


                    }



                    fkCompanyId = Convert.ToInt16(Session["pkCompanyIdWhenEdit"]);
                    AddRecentReview(fkCompanyId.GetValueOrDefault());
                    //List<Proc_Get_CommentsForLeadResult> ObjDbCollection = new List<Proc_Get_CommentsForLeadResult>();
                    List<tblOfficeDocument> GettblOfficedocumentslist = new List<tblOfficeDocument>();
                    objModel.CollGetCommentsForLeadResult = ObjBALLeads.GetCommentsForLead(fkCompanyId.GetValueOrDefault());
                    //var traineelist = 
                        ObjBALLeads.GetCommentsForLead(fkCompanyId.GetValueOrDefault());
                    GettblOfficedocumentslist = ObjBALLeads.GetDocuments(); ;
                    Session["officedocumentslist"] = GettblOfficedocumentslist;

                    //string comments = string.Empty;







                    objModel.democounter = objModel.CollGetCommentsForLeadResult.Count;
                    var list = ObjBALLeads.GetCommentsForLead(fkCompanyId.GetValueOrDefault());
                    if (list.Count > 0)
                    {


                        objModel.Senddocument = objModel.CollGetCommentsForLeadResult.ElementAt(0).LeadComments;

                    }
                    objModel.CompanyNote = list1.ElementAt(0).CompanyNote;
                    objModel.CompanyName = list1.ElementAt(0).CompanyName;
                    objModel.pkCompanyTypeId1 = list1.ElementAt(0).fkCompanyTypeId;

                    objModel.pkCompanyTypeId = list1.ElementAt(0).fkCompanyTypeId;
                    objModel.LeadNumber = list1.ElementAt(0).LeadNumber;
                    objModel.LeadSource = list1.ElementAt(0).LeadSource;
                    objModel.LeadSourceid = (list1.ElementAt(0).LeadSourceid != null ? list1.ElementAt(0).LeadSourceid.ToString() : string.Empty);

                    objModel.fkUserId1 = list1.ElementAt(0).fkSalesManagerId;
                    objModel.fkSalesAssociateId = list1.ElementAt(0).fkSalesRepId;
                    objModel.Address1 = list1.ElementAt(0).Address1;
                    objModel.Address2 = list1.ElementAt(0).Address2;
                    objModel.pkStateId = list1.ElementAt(0).fkStateID;
                    objModel.pkStateId1 = list1.ElementAt(0).fkStateID;

                    objModel.MainFaxNumber = list1.ElementAt(0).Fax2;
                    objModel.ZipCod = list1.ElementAt(0).ZipCode;
                    objModel.CityName = list1.ElementAt(0).City;
                    ViewBag.LocationId = list1.ElementAt(0).pkLocationId;
                    ViewBag.CompanyId = list1.ElementAt(0).fkCompanyId;

                    objModel.MainContact = list1.ElementAt(0).MainContactPersonName;
                    Session["vwUserName"] = list1.ElementAt(0).MainContactPersonName;
                    Session["Maincontact"] = list1.ElementAt(0).MainContactPersonName;
                    objModel.MainEmail = list1.ElementAt(0).MainEmailId;
                    Session["MainEmail"] = list1.ElementAt(0).MainEmailId;
                    objModel.MainPhone = list1.ElementAt(0).PhoneNo1;
                    Session["Mainphone"] = list1.ElementAt(0).PhoneNo1;
                    objModel.BillingFax = list1.ElementAt(0).Fax;
                    objModel.BillingContact = list1.ElementAt(0).BillingContactPersonName;
                    Session["Billingcontact"] = list1.ElementAt(0).BillingContactPersonName;
                    objModel.BillingEmail = list1.ElementAt(0).BillingEmailId;
                    Session["BillingEmail"] = list1.ElementAt(0).BillingEmailId;
                    objModel.BillingPhone = list1.ElementAt(0).PhoneNo2;

                    objModel.CompanyUrl = list1.ElementAt(0).CompanyUrl;
                    objModel.SecondaryMainContactPersonName = list1.ElementAt(0).SecondaryMainContactPersonName;
                    objModel.MainTitle = list1.ElementAt(0).MainTitle;
                    objModel.SecondaryTitle = list1.ElementAt(0).SecondaryTitle;
                    objModel.SecondaryEmail = list1.ElementAt(0).SecondaryEmail;
                    objModel.MonthlyVolume = Convert.ToInt16(list1.ElementAt(0).MonthlyVolume);
                    objModel.SecondaryPhone = list1.ElementAt(0).SecondaryPhone;
                    objModel.SecondaryFax = list1.ElementAt(0).SecondaryFax;
                    objModel.NoofEmployees = list1.ElementAt(0).NoofEmployees.ToString();
                    objModel.Ext1 = list1.ElementAt(0).Ext1;
                    objModel.Ext2 = list1.ElementAt(0).Ext2;




                    ViewBag.City = list1.ElementAt(0).City;
                    BALCompanyType Obj_BALCompanyType = new BALCompanyType();
                    string Cityname = list1.ElementAt(0).City;
                    if (string.IsNullOrEmpty(Cityname) == false)
                    {

                        var coll_cityid = Obj_BALCompanyType.GetCity_Id(Cityname);
                        ViewBag.City_id = coll_cityid.Item1;
                    }
                    objModel.NumberOfLocation = (list1.ElementAt(0).NoOfLocations == 0 ? string.Empty : list1.ElementAt(0).NoOfLocations.ToString()); // list1.ElementAt(0).NoOfLocations.ToString();
                    objModel.CurrentProvider = list1.ElementAt(0).CurrentProvider;

                    objModel.Priority1 = list1.ElementAt(0).Priority1;
                    objModel.Priority2 = list1.ElementAt(0).Priority2;
                    objModel.Priority3 = list1.ElementAt(0).Priority3;
                    if (ObjMembershipUser != null)
                    {

                        if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SalesRep"))
                        {
                            objModel.ddlManagerEnabled = "false";
                            objModel.ddlAssociateEnabled = "false";
                        }
                        if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SalesManager"))
                        {
                            objModel.ddlManagerEnabled = "false";
                            //  objModel.ddlAssociateEnabled = "false";
                        }
                    }




                }
                else if (Request.QueryString["_j"] != null)
                {
                    objModel.IsEdit = true;
                    objModel.LeadSourceJuno = "Juno";
                    ViewBag.locationidJuno = Convert.ToInt16(Request.QueryString["_j"]);
                    var locationid1 = Convert.ToInt16(Request.QueryString["_j"]);
                    Session["pkLocationIdWhenEditJuno"] = Convert.ToInt16(Request.QueryString["_j"]);
                    Session["pkLocationIdWhenEdit"] = Convert.ToInt16(Request.QueryString["_j"]);
                    var list1 = objbalCompany.GetCompanyDetailByLocationId(locationid1);
                    Session["pkCompanyIdWhenEditJuno"] = list1.ElementAt(0).fkCompanyId;
                    objModel.pkCompanyId = (list1.ElementAt(0).fkCompanyId.GetValueOrDefault() != null) ? (list1.ElementAt(0).fkCompanyId.GetValueOrDefault()) : 0;
                    objModel.pklocationid = Convert.ToInt16(Request.QueryString["_j"]);
                    Session["LeadStatus"] = list1.ElementAt(0).LeadStatus;


                   // var lstatus1 = Session["LeadStatus1"];


                    objModel.IsJunoEdit = true;



                    if (Convert.ToByte((Session["LeadStatus"])) == 11)
                    {

                        //byte status = Convert.ToByte((Session["LeadStatusView"]));
                        objModel.imgbtn_1 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-1-off.png");


                        objModel.imgbtn_2 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-2-off.png");
                        objModel.imgbtn_3 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-3-off.png");
                        objModel.imgbtn_4 = @Url.Content("~/Content/themes/base/images/emerge-office-milestone-4-off.png");


                    }



                    objModel.CompanyNote = list1.ElementAt(0).CompanyNote;
                    objModel.CompanyName = list1.ElementAt(0).CompanyName;
                    objModel.pkCompanyTypeId = list1.ElementAt(0).fkCompanyTypeId;
                    Num = Utility.GeneraterRandom();
                    while (IsDuplicate == false)
                    {
                        IsDuplicate = ObjBALLeads.CheckLeadNumber(Num);
                        Num = Utility.GeneraterRandom();
                    }

                    objModel.LeadNumber = Num;
                    objModel.LeadSource = list1.ElementAt(0).LeadSource;
                    objModel.fkUserId1 = list1.ElementAt(0).fkSalesManagerId;
                    objModel.fkSalesAssociateId = list1.ElementAt(0).fkSalesRepId;
                    objModel.Address1 = list1.ElementAt(0).Address1;
                    objModel.Address2 = list1.ElementAt(0).Address2;
                    objModel.pkStateId = list1.ElementAt(0).fkStateID;
                    objModel.ZipCod = list1.ElementAt(0).ZipCode;
                    ViewBag.LocationId = list1.ElementAt(0).pkLocationId;
                    ViewBag.CompanyId = list1.ElementAt(0).fkCompanyId;

                    objModel.MainContact = list1.ElementAt(0).MainContactPersonName;
                    Session["Maincontactfor=JUNO"] = list1.ElementAt(0).MainContactPersonName;
                    objModel.MainEmail = list1.ElementAt(0).MainEmailId;
                    Session["MainEmailforJUNO"] = list1.ElementAt(0).MainEmailId;
                    objModel.MainPhone = list1.ElementAt(0).PhoneNo1;
                    objModel.BillingFax = list1.ElementAt(0).Fax;
                    objModel.BillingContact = list1.ElementAt(0).BillingContactPersonName;
                    Session["BillingcontactForJUNO"] = list1.ElementAt(0).BillingContactPersonName;
                    objModel.BillingEmail = list1.ElementAt(0).BillingEmailId;
                    Session["BillingEmailForJUNO"] = list1.ElementAt(0).BillingEmailId;
                    objModel.BillingPhone = list1.ElementAt(0).PhoneNo2;
                    objModel.MainFaxNumber = list1.ElementAt(0).Fax2;


                    objModel.NumberOfLocation = list1.ElementAt(0).NoOfLocations.ToString();
                    objModel.CurrentProvider = list1.ElementAt(0).CurrentProvider;

                    objModel.Priority1 = list1.ElementAt(0).Priority1;
                    objModel.Priority2 = list1.ElementAt(0).Priority2;
                    objModel.Priority3 = list1.ElementAt(0).Priority3;
                    if (ObjMembershipUser != null)
                    {

                        if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SalesRep"))
                        {
                            objModel.ddlManagerEnabled = "false";
                            objModel.ddlAssociateEnabled = "false";
                            objModel.fkSalesAssociateId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
                        }
                        if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SalesManager"))
                        {
                            objModel.ddlManagerEnabled = "false";
                            objModel.fkUserId1 = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
                            //   objModel.ddlAssociateEnabled = "false";
                        }
                    }
                }
                else
                {

                    Num = Utility.GeneraterRandom();
                    while (IsDuplicate == false)
                    {
                        IsDuplicate = ObjBALLeads.CheckLeadNumber(Num);
                    }

                    objModel.LeadNumber = Num;
                    if (ObjMembershipUser != null)
                    {

                        if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SalesRep"))
                        {
                            objModel.ddlManagerEnabled = "false";
                            objModel.ddlAssociateEnabled = "false";
                            objModel.fkSalesAssociateId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
                        }
                        if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SalesManager"))
                        {
                            objModel.ddlManagerEnabled = "false";
                            // objModel.ddlAssociateEnabled = "false";
                            objModel.fkUserId1 = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
                            // objModel.fkSalesAssociateId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
                        }
                    }

                }
                if (Request.QueryString["_j"] != null)
                {
                    if (Request.QueryString["_n"].ToString() == "1")
                    {
                        objModel.successmsg = "Lead Updated Successfully";
                    }
                }

            }
            catch
            {

            }
            objModel.callLogText = "";

            return View(objModel);
        }
        #endregion

        [HttpPost]
        public ActionResult EditCallLogComments(string pkCompanyId, string callLogId, string callText)
        {
            BALLeads ObjBALLeads = new BALLeads();
            int cid=(!string.IsNullOrEmpty(pkCompanyId)?Convert.ToInt32(pkCompanyId):0);
            int callId = (!string.IsNullOrEmpty(callLogId) ? Convert.ToInt32(callLogId) : 0);
            ObjBALLeads.UpdateCallLogs(cid, callId, callText);
            return Json("Success");
        }






        public ActionResult GetMangerNamebyPhoneNo(string MainPhoneNo, string CompanyName)
        {
            string saleManger = string.Empty;
            string cType = string.Empty;
            int count = 0;
            try
            {
                BALLeads ObjBALLeads = new BALLeads();
                if (MainPhoneNo != string.Empty)
                {
                    var list = ObjBALLeads.GetSaleMangerByMainPhoneNo(MainPhoneNo, CompanyName);
                    count = list.Count();
                    if (list.Count() > 0)
                    {
                        saleManger = list.ElementAt(0).SalesManager;
                        cType = list.ElementAt(0).CType;//User for flag: Same Company and Other Company.
                    }
                }
            }
            catch //(Exception ex)
            {

            }
            finally
            {
                ObjBALLeads = null;
            }
            return Json(new { saleManger = saleManger, cType = cType, count = count });
        }
        private int AddRecentReview(int pkCompanyId)
        {
            ObjBALOffice = new BALOffice();
            int Result = 0;
            try
            {
               // MembershipUser ObjMembershipUser = 
                    Membership.GetUser(User.Identity.Name);
                Guid UserId = Utility.GetUID(User.Identity.Name);
                Result = ObjBALOffice.AddRecentReviewedLead(UserId, pkCompanyId);
            }
            catch (Exception)
            {

            }
            finally
            {
                ObjBALOffice = null;
            }
            return Result;
        }

        public ActionResult GetPrintLead()
        {
            string fklocationid = Session["pkLocationIdWhenEdit"].ToString();
            return Json(new { pkloc = fklocationid });
        }


        public ActionResult GetCmpnyType()
        {
            BALCompanyType serboj = new BALCompanyType();
            var list = serboj.GetAllCompanyType();
            return Json(list);
        }

        public ActionResult GetStateList()
        {
            BALCompany serobj = new BALCompany();
            var list1 = serobj.GetState();
            return Json(list1);
        }

        public ActionResult GetManagerList()
        {
            BALCompany serobj = new BALCompany();
            string rolename = "salesmanager";
            var list = serobj.GetManagers(rolename);

            return Json(list);

        }



        public ActionResult GetSalesAssociateList()
        {

            MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));
            if (Request.QueryString["_a"] == null)
            {
                BALCompany serobj = new BALCompany();
                if (ObjMembershipUser != null)
                {
                    if (Roles.IsUserInRole(ObjMembershipUser.UserName, "SalesManager"))
                    {

                        string rolename = "salesrep"; //string rolename = "salesmanager";
                        var list = serobj.GetManagers(rolename);
                        return Json(list);
                    }
                    else
                    {

                        string rolename = "salesrep";
                        var list = serobj.GetManagers(rolename);

                        return Json(list);
                    }
                }
                else
                {

                    string rolename = "salesrep";
                    var list = serobj.GetManagers(rolename);

                    return Json(list);
                }
            }
            else
            {
                BALCompany serobj = new BALCompany();
                string rolename = "salesrep";
                var list = serobj.GetManagers(rolename);

                return Json(list);
            }

        }


        #region// Bind Sent Documents PopUp In Call Logo
        public ActionResult BindSentDocuments(string DocumentsSendid)
        {
            BALLeads ObjBALLeads = new BALLeads();
            int fkCompanyId = Convert.ToInt16(Session["pkCompanyIdWhenEdit"]);
            var DocumentSent = ObjBALLeads.GetDocumentsBySentIdcompanyId(Convert.ToInt16(DocumentsSendid), fkCompanyId);
            var txtbindComment = ObjBALLeads.GetDocumentSentinfobyDocId(Convert.ToInt16(DocumentsSendid));
            Session["pkdocumentid"] = DocumentSent.ElementAt(0).pkDocumentId;
            int id =DocumentSent.ElementAt(0).pkDocumentId;
           // BALOffice ObjBALOffice = new BALOffice();
//            var Document = ObjBALOffice.GetDocumentsByDocId(id);
            StringBuilder sb = new StringBuilder();
            sb.Append("<table style='width:650px;'>");
            for (int j = 0; j < DocumentSent.Count; j++)
            {
                sb.Append("<tr style='width:650px'>");
                sb.Append("<td style='text-align: left; width:90px' ></td>");
                sb.Append("<td style=' text-align: left; width:200px'>" + DocumentSent.ElementAt(j).DocumentTitle + "</td>");
                sb.Append("<td style=' text-align: left'> <a   href='DownloadDocumentsFile/?FPathTitle=" + DocumentSent.ElementAt(j).DocumentPath + "_" + DocumentSent.ElementAt(j).DocumentTitle + "' title='View Document' >View</a></td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td >" + "<input type='hidden' id=' " + DocumentSent.ElementAt(j).pkDocumentId + "'  />" + "</td>");
                sb.Append("</tr>");

            }

            sb.Append("</table>");
            return Json(new { Comment = txtbindComment.Comments, label = sb.ToString() });
        }
        #endregion

        #region // Bind Demo Schedule PopUp In CallLogo
        public ActionResult BindDemoschedule(string DemoScheduleId)
        {
            Session["DemoScheduleId"] = DemoScheduleId;
            string email = string.Empty;
            string contactperson = string.Empty;
            //string fklocationid = Session["pkLocationIdWhenEdit"].ToString();

            int fkCompanyId = Convert.ToInt16(Session["pkCompanyIdWhenEdit"]);
            var DemoscheduleList = ObjBALLeads.GetTraineeByDemoIdCompanyId(Convert.ToInt16(DemoScheduleId), fkCompanyId);
            var list = ObjBALLeads.GetDemoScheduleInfoByDemoId(Convert.ToInt16(DemoScheduleId));
            string datetime = list.DemoDateTime.ToString();
            string date = Convert.ToDateTime(datetime).ToShortDateString();
            string time = Convert.ToDateTime(datetime).ToShortTimeString();
            //List<cTrainee> ObjCollcTrainee = new List<cTrainee>();

            var Comments = list.Comments;

            StringBuilder sb = new StringBuilder();

            sb.Append("<table cellspacing='2' cellpadding='5' width='750px' style='height: 30px; font-weight: bold; margin-left:25px' id='tbln'>");
            for (int i = 0; i < DemoscheduleList.Count; i++)
            {
                sb.Append("<tr>");
                sb.Append("<td style='width:50px' >" + "<input type='text' id='FirstName' style='width:90%' name='Title' class='k-textbox' value='  " + DemoscheduleList.ElementAt(i).FirstName + " '/>" + "</td>");
                sb.Append("<td style='width:50px'>" + "<input type='text' id='LastName' style='width:90%' name='Title' class='k-textbox' value='  " + DemoscheduleList.ElementAt(i).LastName + " '/>" + "</td>");
                sb.Append("<td  style='width:50px'>" + "<input type='text' id='Title' style='width:90%' name='Title' class='k-textbox' value='  " + DemoscheduleList.ElementAt(i).Title + " '/>" + "</td>");
                sb.Append("<td  style='width:50px'>" + "<input type='text' id='Phone' style='width:90%' name='Title' class='k-textbox' value='  " + DemoscheduleList.ElementAt(i).Phone + " '/>" + "</td>");
                sb.Append("<td  style='width:50px'>" + "<input type='text' id='Ext' style='width:90%' name='Title' class='k-textbox' value='  " + DemoscheduleList.ElementAt(i).Ext + " '/>" + "</td>");
                sb.Append("<td style='width:50px'>" + "<input type='text' id='Email' style='width:90%' name='Title' class='k-textbox' value='  " + DemoscheduleList.ElementAt(i).EmailId + " '/>" + "</td>");
                sb.Append("<td style='width:50px'>" + "<input type='button' id=' " + DemoscheduleList.ElementAt(i).pkCompanyTraineeId + "' style='width:90%' name='delete' class='k-textbox' value='Delete' class='EditDel' style='cursor:pointer; '  onclick='return DeleteDocuments(this.id)'  />" + "</td>");
                sb.Append("<td style='width:50px'></td>");

                sb.Append("</tr>");
                email = DemoscheduleList.ElementAt(i).EmailId;
                contactperson = DemoscheduleList.ElementAt(i).FirstName;

            }

            sb.Append("<table>");

            return Json(new { label = sb.ToString(), date = date, time = time, Comments = Comments, email = email, contactperson = contactperson });




        }
        #endregion

        #region// Send Selected Documents
        public ActionResult SendDocuments(string pkCompanyIds, string Comments)
        {
            int result = 0;
            List<tblOfficeDocument> objColltblOfficeDocument = new List<tblOfficeDocument>();
           // BALCompany ObjBALCompany = new BALCompany();
            string fklocationid = Session["pkLocationIdWhenEdit"].ToString();
            string[] stringSeparators = new string[] { "#" };
            string[] ListpkCompanyIds = pkCompanyIds.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
            try
            {

                tblDocumentSent objtblDocumentSent = new tblDocumentSent();
                if (Session["pkCompanyIdWhenEdit"] != null)
                {
                    objtblDocumentSent.fkCompanyId = Convert.ToInt16(Session["pkCompanyIdWhenEdit"]);
                    objtblDocumentSent.Comments = Comments;
                }

                tblOfficeDocument objtblOfficeDocument = new tblOfficeDocument();

                for (int i = 0; i < ListpkCompanyIds.Length; i++)
                {

                    int passDocumentID = Convert.ToInt16(ListpkCompanyIds.ElementAt(i));
                    objtblOfficeDocument = ObjBALLeads.GetDocInfoByDocId(passDocumentID);
                    objColltblOfficeDocument.Add(objtblOfficeDocument);
                }


                List<tblOfficeDocument> GettblOfficedocumentslist = (List<tblOfficeDocument>)Session["officedocumentslist"];
                string DocTitle = string.Empty;
                for (int j = 0; j < GettblOfficedocumentslist.Count; j++)
                {
                    for (int i = 0; i < ListpkCompanyIds.Length; i++)
                    {
                        // List<tblOfficeDocument> GettblOfficedocumentslist = new List<tblOfficeDocument>();

                        // GettblOfficedocumentslist=Session["officedocumentslist"];
                        int fkdocumentsid = Convert.ToInt16(ListpkCompanyIds.ElementAt(i));
                        int pkdocumentsid = fkdocumentsid;

                        if (GettblOfficedocumentslist.ElementAt(j).pkDocumentId == pkdocumentsid)
                        {
                            tblOfficeDocumentSent ObjtblOfficeDocumentSent = new tblOfficeDocumentSent();
                            ObjtblOfficeDocumentSent.pkDocumentId = GettblOfficedocumentslist.ElementAt(j).pkDocumentId;
                            ObjtblOfficeDocumentSent.DocumentTitle = GettblOfficedocumentslist.ElementAt(j).DocumentTitle;
                            ObjtblOfficeDocumentSent.DocumentPath = GettblOfficedocumentslist.ElementAt(j).DocumentPath;
                            objColltblOfficeDocumentSent.Add(ObjtblOfficeDocumentSent);
                            DocTitle = GettblOfficedocumentslist.ElementAt(j).DocumentTitle;

                        }
                    }
                }

                Guid UserId;
                int pkCompanyId;
                tblCallLog objtblCallLog = new tblCallLog();
                MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));
                if (ObjMembershipUser != null)
                {
                    UserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
                    pkCompanyId = Convert.ToInt16(Session["pkCompanyIdWhenEdit"]);
                    string LeadComments = DocTitle;// txtDate.Text.Trim() + ", " + ddlTime.SelectedItem.Text.Trim();
                    DateTime CreatedDate = DateTime.Now;
                    objtblCallLog.fkCompanyId = pkCompanyId;
                    objtblCallLog.fkUserId = UserId;
                    objtblCallLog.LeadComments = LeadComments;
                    objtblCallLog.CreatedDate = CreatedDate;
                }

                int DocumentSentResult = ObjBALLeads.AddDDocumentSent(objtblDocumentSent, objColltblOfficeDocumentSent, objtblCallLog);

                if (DocumentSentResult == 1)
                {

                    if (Session["MainEmail"] != null)
                    {
                        string ReciverEmail = Session["MainEmail"].ToString();
                        //string ReciverEmail = "support@dotnetoutsourcing.com";
                        string MainPhone = Session["Mainphone"].ToString();
                        bool CheckSend = false;
                        string emailFrom = ObjMembershipUser.Email;

                        #region Replace Bookmark
                        string MessageBody = "";
                        string emailText = "";
                        BALGeneral ObjBALGeneral = new BALGeneral();
                        string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;
                        var emailContent = ObjBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, Convert.ToInt32(EmailTemplates.SendDocument));
                        emailText = emailContent.TemplateContent;
                        BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                        Bookmarks objBookmark = new Bookmarks();
                        #region For New Style SystemTemplate
                        string Bookmarkfile = string.Empty;
                        string bookmarkfilepath = string.Empty;

                        Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                        Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                        Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                        emailText = Bookmarkfile;
                        #endregion


                        objBookmark.EmailContent = emailText;
                        if (Session["vwUserName"] != null)
                        {
                            objBookmark.UserFirstName = Session["vwUserName"].ToString();
                        }
                        objBookmark.MainPhone = MainPhone;
                        objBookmark.UserEmail = emailFrom;
                        objBookmark.DemoDescription = "<tr><td>Please check the attachment file(s).</td></tr>";
                        objBookmark.EmergeOfficeLogo = "<img src='" + WebsiteLogo + "' />";

                        MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Utility.GetUID(HttpContext.User.Identity.Name.ToString()));

                        #endregion
                        List<Attachment> lstattach = new List<Attachment>();
                        string FilePath;
                        for (int i = 0; i < objColltblOfficeDocumentSent.Count; i++)
                        {

                            FilePath = Request.PhysicalApplicationPath + "Resources/Upload/OfficeDocuments/" + objColltblOfficeDocumentSent.ElementAt(i).DocumentPath;


                            Attachment attach = new Attachment(FilePath);
                            // Attach the newly created email attachment

                            lstattach.Add(attach);
                        }


                        CheckSend = Utility.SendMail(ReciverEmail, string.Empty, string.Empty, emailFrom, MessageBody, "Send Documents", lstattach);
                        if (CheckSend == true)
                        {
                            result = 1;//Email sent successfully

                        }
                        else
                        {
                            result = 2;//Error Occured while sending email
                            // ScriptManager.RegisterStartupScript(this.Page, typeof(string), "success", "alert('Error Occured while sending email !');", true);
                        }

                        // lblMessage1.Text = "Document sent Successfully.";
                    }
                }
                else if (DocumentSentResult == -1)
                {
                    result = -1;//Error occurred on Sending Document(s)
                }
                else if (DocumentSentResult == -2)
                {
                    result = -2;//Error occurred on Sending Document(s)
                }
                else if (DocumentSentResult == -3)
                {
                    result = -3; //Error occurred on updating Call log.
                }



                //result = ObjBALCompany.ActionOnCompany(CompanyList, Convert.ToByte(1), Convert.ToBoolean(0));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //ObjBALCompany = null;
            }
            //  return Json(result);
            return Json(new { Status = result, Id = fklocationid });


        }
        #endregion

        #region // Update Company Price
        public ActionResult UpdateProducts(IEnumerable<AddEditLeadsModel> Detail, string CID, string LocationId, string CompanyId)
        {
            //string strMessage = string.Empty;
            //BALProducts ObjBALProducts = new BALProducts();
            int StatusType = 0;
            //Guid Result = Guid.Empty;
            BALLocation ObjBALLocation = new BALLocation();
            BALCompany ObjBALCompany = new BALCompany();
            int Location = Convert.ToInt16(Session["NewPklocationid"]) != 0 ? Convert.ToInt16(Session["NewPklocationid"]) : 0;
            if (Detail != null)// When Update
            {
                for (int i = 0; i < Detail.Count(); i++)
                {
                    tblProduct ObjProduct = new tblProduct();
                    ObjProduct.pkProductId = Detail.ElementAt(i).pkProductId;
                    ObjProduct.VendorNotes = string.Empty;

                    List<tblProductAccess> listtblProductAccess = new List<tblProductAccess>();
                    List<tblProductAccess> deletetblProductAccess = new List<tblProductAccess>();
                    List<tblCompany_Price> listtblCompany_Price = new List<tblCompany_Price>();
                    //if (Detail.ElementAt(i).IsDefault != null && Convert.ToBoolean(Detail.ElementAt(i).IsDefault))
                    //{
                    tblProductAccess ObjtblProductAccess = new tblProductAccess();
                    ObjtblProductAccess.pkProductAccessId = (Detail.ElementAt(i).ProductAccessId.GetValueOrDefault() != null) ? Detail.ElementAt(i).ProductAccessId.GetValueOrDefault() : 0;
                    ObjtblProductAccess.fkLocationId = Location;
                    ObjtblProductAccess.fkProductApplicationId = Detail.ElementAt(i).pkProductApplicationId;
                    ObjtblProductAccess.IsLiveRunner = (Detail.ElementAt(i).IsLiveRunnerLocation != null) ? Convert.ToBoolean(Detail.ElementAt(i).IsLiveRunnerLocation) : false;
                    ObjtblProductAccess.ProductReviewStatus = (Detail.ElementAt(i).AutoReviewStatusForLocation != null) ? byte.Parse(Detail.ElementAt(i).AutoReviewStatusForLocation) : byte.MinValue;
                    ObjtblProductAccess.IsProductEnabled = Detail.ElementAt(i).IsDefault != null ? Convert.ToBoolean(Detail.ElementAt(i).IsDefault) : false;
                    ObjtblProductAccess.LocationProductPrice = Detail.ElementAt(i).CompanyPrice;
                    listtblProductAccess.Add(ObjtblProductAccess);
                    //}
                    //else
                    //{
                    //    tblProductAccess ObjtblProductAccess = new tblProductAccess();
                    //    ObjtblProductAccess.pkProductAccessId = (Detail.ElementAt(i).ProductAccessId != null) ? new Guid(Detail.ElementAt(i).ProductAccessId.ToString()) : Guid.Empty;
                    //    ObjtblProductAccess.fkLocationId = Location;
                    //    ObjtblProductAccess.fkProductApplicationId = Detail.ElementAt(i).pkProductApplicationId;
                    //    ObjtblProductAccess.IsLiveRunner = (Detail.ElementAt(i).IsLiveRunnerLocation != null) ? Convert.ToBoolean(Detail.ElementAt(i).IsLiveRunnerLocation) : false;
                    //    ObjtblProductAccess.ProductReviewStatus = (Detail.ElementAt(i).AutoReviewStatusForLocation != null) ? byte.Parse(Detail.ElementAt(i).AutoReviewStatusForLocation) : byte.MinValue; ;
                    //    deletetblProductAccess.Add(ObjtblProductAccess);

                    //}



                    tblCompany_Price ObjtblCompany_Price = new tblCompany_Price();
                    var MonthlySaving = (Detail.ElementAt(i).MonthlyVolumeProductWise * Detail.ElementAt(i).CurrentPrice) - (Detail.ElementAt(i).MonthlyVolumeProductWise * Detail.ElementAt(i).CompanyPrice);

                    ObjtblCompany_Price.MonthlySaving = MonthlySaving;
                    ObjtblCompany_Price.CurrentPrice = Detail.ElementAt(i).CurrentPrice;
                    ObjtblCompany_Price.MonthlyVolume = Detail.ElementAt(i).MonthlyVolumeProductWise;
                    ObjtblCompany_Price.fkCompanyId = Convert.ToInt16(Session["Newpkcompanyid"]) != 0 ? Convert.ToInt16(Session["Newpkcompanyid"]) : 0;
                    ObjtblCompany_Price.IsDefaultPrice = Convert.ToBoolean(Detail.ElementAt(i).IsDefaultPrice);
                    ObjtblCompany_Price.CompanyPrice = Convert.ToDecimal(Detail.ElementAt(i).CompanyPrice);

                    ObjtblCompany_Price.fkProductApplicationId = Detail.ElementAt(i).pkProductApplicationId;

                    ObjtblCompany_Price.pkCompanyPriceId = 0;

                    ObjtblCompany_Price.EnableReviewPerCompany = Detail.ElementAt(i).EnableReviewPerCompany;

                    listtblCompany_Price.Add(ObjtblCompany_Price);
                    int Result1 = ObjBALLocation.InsertUpdateLocationProduct(listtblProductAccess, deletetblProductAccess);
                    ObjBALCompany.AddProductPricePerCompany(listtblCompany_Price);



                    if (Result1 == 1)
                    {
                        StatusType = 5;// "<span class='successmsg'>Product Information successfully updated.</span>";
                    }
                    else
                    {
                        StatusType = 6;//"<span class='errormsg'>Some problem while updating data.</span>";
                    }


                }
            }

            return View();
        }
        #endregion


        #region  // Bind City DropDown
        public ActionResult GetCityListbyStateId(int pkid)
        {
            BALLocation serobj = new BALLocation();
            var list1 = serobj.GetCityStatewise(pkid);
            return Json(list1);
        }

        #endregion

        #region  // Change Lead Status
        public ActionResult ChangeStatus(string status)
        {
            string fklocationid = Session["pkLocationIdWhenEdit"].ToString();
            byte vwLeadStatus = Convert.ToByte(Session["LeadStatus"]);
            string[] Btnstr = status.Split('_');
            int LeadStatusView = 0;
            byte LeadStatus = Convert.ToByte(Btnstr[1]);
            bool CheckStatusSelect = false;

            if (LeadStatus <= vwLeadStatus)
            {
                CheckStatusSelect = false;
                UpdateLeadStatus(status, false);
                //   SetRollOver((byte)(LeadStatus - 1));
                Session["LeadStatus"] = LeadStatus - 1;
            }
            else
            {
                CheckStatusSelect = true;
                UpdateLeadStatus(status, true);
                //  SetRollOver(LeadStatus);
                Session["LeadStatus"] = LeadStatusView;
            }
            Session["LeadStatusView"] = LeadStatusView;
            UpdateCallLog(status, CheckStatusSelect);

            //  return RedirectToAction("AddEditLeads", new { _a = fklocationid });
            return Json(new { Id = fklocationid });

        }
        #endregion

        #region   // Update CallLogo
        public void UpdateCallLog(string btnName, bool CheckStatusSelect)
        {
            Guid UserId;
            int pkCompanyId;

            MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));
            if (ObjMembershipUser != null)
            {
                UserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
                pkCompanyId = Convert.ToInt16(Session["pkCompanyIdWhenEdit"]);
                string LeadComments = "";
                string[] Btnstr = btnName.Split('_');
                switch (Btnstr[1].ToLower())
                {
                    case "1": LeadComments = "##1##"; break;
                    case "2": LeadComments = "##2##"; break;
                    case "3": LeadComments = "##3##"; break;
                    case "4": LeadComments = "##4##"; break;
                    case "5": LeadComments = "##5##"; break;
                }
                DateTime CreatedDate = System.DateTime.Now;
                AddStatusToCallLog(pkCompanyId, UserId, LeadComments, CreatedDate, CheckStatusSelect);
            }
            //PageLoadBind();
            //BindLeadComments();
        }
        #endregion
        [HttpPost]
        public ActionResult UpdateLeadMilestoneInCallLog(string CompanyId, int NewStatus, int PreviousStatus)
        {

            BALLeads ObjBALLeads = new BALLeads();
            string CompanyNme = CompanyId.Split('[')[0];
            int NewCompanyId = 0;
            var CompanyRec = ObjBALLeads.GetCompanyIdByComapnyName(CompanyNme);
            if (CompanyRec != null)
            {
                NewCompanyId = CompanyRec.pkCompanyId;
            }

            if (NewStatus <= PreviousStatus)
            {
                UpdateLeadStatusbyCallLog(NewStatus, false, NewCompanyId);

            }
            else
            {
                UpdateLeadStatusbyCallLog(NewStatus, true, NewCompanyId);

            }

            UpdateCallLogbyPopUp(NewStatus, NewCompanyId);
            return Json(new { Msg = 1, Css = 2 });
        }
        #region  // Update Laed Staus
        public void UpdateLeadStatus(string btnName, bool updown)
        {
            //lblMessageBox.Visible = true;
            //lblMessageBox.Text = btnName;
            BALLeads ObjBALLeads = new BALLeads();
            tblCompany ObjtblCompany = new tblCompany();
            //int CompanyId = 0;
            Guid CompanyUserId = Guid.Empty;
            ;
            int Status = 1;
            string Resultmsg = string.Empty;
            MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));
            CompanyUserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
            string[] Btnstr = btnName.Split('_');
            switch (Btnstr[1].ToLower())
            {
                case "1": Status = 1; break;
                case "2": Status = 2; break;
                case "3": Status = 3; break;
                case "4": Status = 4; break;
                case "5": Status = 5; break;
            }
            if (Session["pkCompanyIdWhenEdit"] != null)
            {
                ObjtblCompany.pkCompanyId = Convert.ToInt16(Session["pkCompanyIdWhenEdit"]);
                if (Status == 1)
                {
                    ObjtblCompany.LeadStatus = (byte)Status;
                }
                else
                {
                    ObjtblCompany.LeadStatus = (updown) ? (byte)Status : (byte)(Status - 1);
                }
                ObjtblCompany.LastModifiedDate = DateTime.Now;
                ObjtblCompany.StatusStartDate = DateTime.Now;
                ObjtblCompany.LastModifiedById = (CompanyUserId != null) ? CompanyUserId : Guid.Empty;
                int success = ObjBALLeads.UpdateLeadStatus(ObjtblCompany);
                if (success > 0)
                {
                    Resultmsg = "Lead Status updated Successfully";
                    // lblMessageBox.CssClass = "successmsg";
                }
                else
                {
                    Resultmsg = "Some error is occured";
                    // lblMessageBox.CssClass = "errormsg";
                }
            }
        }
        #endregion

        public void UpdateLeadStatusbyCallLog(int btnid, bool updown, int pkCompanyId)
        {

            BALLeads ObjBALLeads = new BALLeads();
            tblCompany ObjtblCompany = new tblCompany();
           // int CompanyId = 0;
            Guid CompanyUserId = Guid.Empty;
            int Status = 1;
            string Resultmsg = string.Empty;
            MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));
            CompanyUserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());

            switch (btnid)
            {
                case 1: Status = 1; break;
                case 2: Status = 2; break;
                case 3: Status = 3; break;
                case 4: Status = 4; break;
                case 5: Status = 5; break;
            }

            ObjtblCompany.pkCompanyId = pkCompanyId;
            if (Status == 1)
            {
                ObjtblCompany.LeadStatus = (byte)Status;
            }
            else
            {
                ObjtblCompany.LeadStatus = (updown) ? (byte)Status : (byte)(Status - 1);
            }
            ObjtblCompany.LastModifiedDate = DateTime.Now;
            ObjtblCompany.StatusStartDate = DateTime.Now;
            ObjtblCompany.LastModifiedById = (CompanyUserId != null) ? CompanyUserId : Guid.Empty;
            int success = ObjBALLeads.UpdateLeadStatus(ObjtblCompany);
            if (success > 0)
            {
                Resultmsg = "Lead Status updated Successfully";
                // lblMessageBox.CssClass = "successmsg";
            }
            else
            {
                Resultmsg = "Some error is occured";
                // lblMessageBox.CssClass = "errormsg";
            }

        }
        public void UpdateCallLogbyPopUp(int btnName, int pkCompanyId)
        {
            Guid UserId;
            bool CheckStatusSelect = false;

            MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));
            if (ObjMembershipUser != null)
            {
                UserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());

                string LeadComments = "";

                switch (btnName)
                {
                    case 1: LeadComments = "##1##"; break;
                    case 2: LeadComments = "##2##"; break;
                    case 3: LeadComments = "##3##"; break;
                    case 4: LeadComments = "##4##"; break;
                    case 5: LeadComments = "##5##"; break;
                }
                DateTime CreatedDate = System.DateTime.Now;
                AddStatusToCallLog(pkCompanyId, UserId, LeadComments, CreatedDate, CheckStatusSelect);
            }

        }
        #region   // Add Updated Lead Status Into Call Logo
        private int AddStatusToCallLog(int pkCompanyId, Guid UserId, string LeadComments, DateTime CreatedDate, bool CheckStatusSelect)
        {
            ObjBALLeads = new BALLeads();
            int Result = 0;
            try
            {
                Result = ObjBALLeads.AddStatusToCallLog(pkCompanyId, UserId, LeadComments, CreatedDate, CheckStatusSelect);
            }
            catch (Exception)
            {

            }
            finally
            {
                ObjBALOffice = null;
            }

            return Result;
        }
        #endregion

        #region   // Add Trainee For Demo Schedule //ND-21
        public ActionResult AddTrainee(FormCollection CollectionObj)
        {
            List<cTrainee> ObjCollcTrainee;
            cTrainee ObjcTrainee = new cTrainee();

            string MaincontactPerson = string.Empty;
            string MaincontactEmail = string.Empty;
            string MainPhone = string.Empty;
            int pktraineeid = 1;
            string LastName = string.Empty;
            string Title = string.Empty;
            string Ext = string.Empty;


            try
            {
                //get existing session object
                if (Session["TraineeCollection"] != null)//ND-21
                {
                    ObjCollcTrainee = (List<cTrainee>)Session["TraineeCollection"];
                    if (ObjCollcTrainee.Count > 0)
                    {
                        //Session["TraineeCollection"] = null;                    
                        pktraineeid = ObjCollcTrainee.Max(m => m.pkCompanyTraineeId) + 1;
                        TempData["PKtraineeid"] = pktraineeid;
                    }
                    else if (TempData["PKtraineeid"] != null)
                    {
                        ObjCollcTrainee = new List<cTrainee>();
                        ////set id
                        pktraineeid = (Int32)TempData["PKtraineeid"] + 1;
                    }
                    else
                    {
                        ObjCollcTrainee = new List<cTrainee>();
                        ////set id
                        pktraineeid = 1;
                    }
                }
                else
                {
                    //session not found, so create new object
                    ObjCollcTrainee = new List<cTrainee>();
                    ////set id
                    pktraineeid = 1;//(Int32)TempData["PKtraineeid"];
                }
                MaincontactPerson = CollectionObj.GetValue("FirstName").AttemptedValue;
                MaincontactEmail = CollectionObj.GetValue("Email").AttemptedValue;
                MainPhone = CollectionObj.GetValue("Phone").AttemptedValue;
                Title =CollectionObj.GetValue("Title").AttemptedValue;
                LastName =CollectionObj.GetValue("LastName").AttemptedValue;
                Ext = CollectionObj.GetValue("Ext").AttemptedValue;
                ObjcTrainee.FirstName = CollectionObj.GetValue("FirstName").AttemptedValue;
                ObjcTrainee.LastName = CollectionObj.GetValue("LastName").AttemptedValue;
                ObjcTrainee.Phone = CollectionObj.GetValue("Phone").AttemptedValue;
                ObjcTrainee.EmailId = CollectionObj.GetValue("Email").AttemptedValue;
                ObjcTrainee.Title = CollectionObj.GetValue("Title").AttemptedValue;
                ObjcTrainee.Ext = CollectionObj.GetValue("Ext").AttemptedValue;
                ObjcTrainee.pkCompanyTraineeId = pktraineeid;

                ObjCollcTrainee.Add(ObjcTrainee);

                Session["TraineeCollection"] = ObjCollcTrainee;

                /*int pklocationid = Convert.ToInt16(Session["pkLocationIdWhenEdit"]);
                Session["LastId"] = Convert.ToInt32(Session["LastId"]) + 1;*/
                Session["Rowid"] = Convert.ToInt32(Session["Rowid"]) + 1;
            }
            catch (Exception ex)
            {
                BALDemo.WritesLog(Environment.NewLine + " Exception " + ex);
            }
            return Json(ObjcTrainee);
        }
        #endregion

        #region   // Bind Demo Schedule PopUp //ND-21
        public ActionResult GetContactValue(string contactValue, string Maincontact)
        {
            List<cTrainee> ObjCollcTrainee = new List<cTrainee>();
            cTrainee ObjcTrainee = new cTrainee();
            BALCompany objbalCompany = new BALCompany();
            //AddEditLeadsModel objmodel = new AddEditLeadsModel();
            //Session["TraineeCollection"] = null;
            string MaincontactPerson = string.Empty;
            string MaincontactEmail = string.Empty;
            string MainPhone = string.Empty;
            string MainExt = string.Empty;
            string MainTitle = string.Empty;
            string City = string.Empty;
            string LastName = string.Empty;
            int pktraineeid = 0;

            if (Session["TraineeCollection"] != null)//ND-21
            {
                ObjCollcTrainee = (List<cTrainee>)Session["TraineeCollection"];
                if (ObjCollcTrainee.Count == 0)
                {
                    Session["TraineeCollection"] = null;
                }

            }

            if (Session["pkLocationIdWhenEdit"] != null)
            {
                int pklocationid = Convert.ToInt16(Session["pkLocationIdWhenEdit"]);
                var list1 = objbalCompany.GetCompanyDetailByLocationId(pklocationid);
                MaincontactPerson = Maincontact;
                MaincontactEmail = contactValue;
                LastName = Maincontact;
                //pktraineeid = Convert.ToInt32(Session["LastId"] == null ? 0 : Session["LastId"]);
                if (Maincontact == list1.ElementAt(0).MainContactPersonName)
                {
                    MainPhone = list1.ElementAt(0).PhoneNo1;
                    MainExt = list1.ElementAt(0).Ext1;
                    MainTitle = list1.ElementAt(0).MainTitle;
                    City = list1.ElementAt(0).City;
                }
                else
                {
                    MainPhone = list1.ElementAt(0).PhoneNo2;
                    MainExt = list1.ElementAt(0).Ext2;
                }

                ObjcTrainee.LastName = Maincontact;
                //string str = string.Empty;
                string[] lstName = Maincontact.Split(' ');
                //foreach (string lst in lstName)
                //{
                MaincontactPerson = lstName[0].ToString();
                if (lstName.Length > 1)
                {
                    LastName = lstName[1].ToString();
                }
                else
                {
                    LastName = "";
                }
                ObjcTrainee.FirstName = Maincontact;
                ObjcTrainee.EmailId = MaincontactEmail;
                ObjcTrainee.Title = list1.ElementAt(0).MainTitle;
                ObjcTrainee.Ext = list1.ElementAt(0).Ext1;
                ObjcTrainee.Phone = MainPhone;
                ObjcTrainee.pkCompanyTraineeId = pktraineeid;

                // ND-21 by pk
                if (ObjCollcTrainee.Count > 0)
                {
                    ObjCollcTrainee.Clear();
                }

                ObjCollcTrainee.Add(ObjcTrainee);

                Session["TraineeCollection"] = ObjCollcTrainee;

                Session["LastId"] = Convert.ToInt32(Session["LastId"]) + 1;

            }
            return Json(new { MaincontactPerson = MaincontactPerson, LastName = LastName, MainTitle = MainTitle, MainPhone = MainPhone, MainExt = MainExt, MaincontactEmail = MaincontactEmail, pktraineeid = pktraineeid });
        }
        #endregion
        #region
        public ActionResult Getcontactlist()
        {
           // AddEditLeadsModel objmodel = new AddEditLeadsModel();
            List<SelectListItem> list = new List<SelectListItem>();
            if (Session["pkCompanyIdWhenEdit"] != null)
            {
                //string pkcomapnyidwhenedit = Session["pkCompanyIdWhenEdit"].ToString();
                string maincontact = Session["Maincontact"].ToString();
                string mainemail = Session["MainEmail"].ToString();
                string billinContact = Session["Billingcontact"].ToString();
                string billingemail = Session["BillingEmail"].ToString();
                if (billinContact != maincontact)
                {
                    list.Add(new SelectListItem { Text = maincontact, Value = mainemail });
                    list.Add(new SelectListItem { Text = billinContact, Value = billingemail });

                }
                else
                {
                    list.Add(new SelectListItem { Text = maincontact, Value = mainemail });
                }


            }
            else if (Session["pkCompanyIdWhenEditJuno"] != null)
            {
                string pkcompanyidwheneedit = Session["pkCompanyIdWhenEditJuno"].ToString();
                string junomaincontact = Session["Maincontactfor=JUNO"].ToString();
                string junomainemail = Session["MainEmailforJUNO"].ToString();
                string junobillingcontact = Session["BillingcontactForJUNO"].ToString();
                string junobillingemail = Session["BillingEmailForJUNO"].ToString();

                if (junomaincontact != junobillingcontact)
                {
                    list.Add(new SelectListItem { Text = junomaincontact, Value = junomainemail });
                    list.Add(new SelectListItem { Text = junobillingcontact, Value = junobillingemail });

                }
                else
                {
                    list.Add(new SelectListItem { Text = junomaincontact, Value = junomainemail });
                }


            }
            else
            {
                list.Add(new SelectListItem { Text = "----", Value = "-1" });

            }

            return Json(list);
        }
        #endregion

        [ActionName("Dates")]
        public ActionResult GetDemoDates()
        {
            BALOffice ObjDemo = new BALOffice();


            var DemoScheduleCollection = ObjDemo.GetDemoSchedule("DemoDateTime", "DESC", false, 1, 5, 0);

            List<string> lst = new List<string>();
            List<string> lstDistinct = new List<string>();
            foreach (var demo in DemoScheduleCollection)
            {
                string str = Convert.ToString(Convert.ToDateTime(demo.DemoDateTime).ToShortDateString());
                lst.Add(str);
                //var temp = demo.DemoDateTime;
            }
            lstDistinct = lst.Distinct().ToList();
            return Json(new
            {
                dates = lstDistinct

            },
                          JsonRequestBehavior.AllowGet);

        }


        #region   // Delete Trainee
        public ActionResult DeleteTrainee(int PkTraineeId)
        {
            List<cTrainee> ObjCollcTrainee = (List<cTrainee>)Session["TraineeCollection"];

            string Emailid = string.Empty;
            try
            {
                if (ObjCollcTrainee != null)
                {
                    //find on id in session and remnove object
                    cTrainee ObjRemove = ObjCollcTrainee.Where(t => t.pkCompanyTraineeId == PkTraineeId).FirstOrDefault();
                    Emailid = ObjRemove.EmailId;
                    ObjCollcTrainee.Remove(ObjRemove);
                    //resave to seesion
                    Session["TraineeCollection"] = ObjCollcTrainee;
                }

                /*cTrainee ObjcTrainee = new cTrainee();
                string Emailid = string.Empty;
                try
                {
                    if (ObjCollcTrainee.Count > 0 && Session["TraineeCollection"] != null)
                    {
                    
                        cTrainee ObjRemove = ObjCollcTrainee.Where(t => t.pkCompanyTraineeId == PkTraineeId).FirstOrDefault();
                        if (ObjRemove != null)
                        {
                            Emailid = ObjRemove.EmailId;
                        }                   
                        ObjCollcTrainee.Remove(ObjRemove);                    
                        Session["TraineeCollection"] = ObjCollcTrainee;
                    }

                }
                catch { }
                */
            }
            catch (Exception ex)
            {
                BALDemo.WritesLog(Environment.NewLine + " Exception " + ex);
            }

            return Json(new { PkTraineeId = Emailid });

        }
        #endregion

        #region   // Save Trainee CallLogo

        public JsonResult SaveTraineeandCalllogo(string comments, string ScheduleDate, string ScheduleTime)
        {
            DateTime DemoDateTime;
            int rowid = 0;
            string DDT = ScheduleDate + " " + ScheduleTime;
            DemoDateTime = Convert.ToDateTime(DDT);


            int ResultStatus = 0;

            int pklocationId = Convert.ToInt16(Session["pkLocationIdWhenEdit"]);

            BALLeads ObjBALLeads = new BALLeads();
            tblDemoSchedule objtblDemoSchedule = new tblDemoSchedule();
            //List<tblDemoSchedule> objDemoSchedule = new List<tblDemoSchedule>();

            if (!(DateTime.Now < DemoDateTime))
            {
                ResultStatus = 6;
                return Json(new { Status = ResultStatus, Id = pklocationId });
            }



            if (Session["pkCompanyIdWhenEdit"] != null)
            {
                objtblDemoSchedule.fkCompanyId = Convert.ToInt16(Session["pkCompanyIdWhenEdit"]);
                objtblDemoSchedule.DemoDateTime = DemoDateTime;
                objtblDemoSchedule.Comments = comments;
            }


            bool alreadyExist = ObjBALLeads.GetCountByDateTime(Convert.ToInt16(objtblDemoSchedule.fkCompanyId), DemoDateTime);
            if (alreadyExist)
            {


                ResultStatus = 7;
                return Json(new { Status = ResultStatus, Id = pklocationId });
            }

            List<tblCompanyTrainee> objColltblCompanyTrainee = new List<tblCompanyTrainee>();

            if (Session["TraineeCollection"] != null)
            {
                List<cTrainee> ObjCollcTrainee = (List<cTrainee>)Session["TraineeCollection"];
                for (int i = 0; i < ObjCollcTrainee.Count; i++)
                {
                    tblCompanyTrainee ObjtblcTrainee = new tblCompanyTrainee();
                    ObjtblcTrainee.pkCompanyTraineeId = ObjCollcTrainee.ElementAt(i).pkCompanyTraineeId;
                    ObjtblcTrainee.EmailId = ObjCollcTrainee.ElementAt(i).EmailId;
                    ObjtblcTrainee.Ext = ObjCollcTrainee.ElementAt(i).Ext;
                    var fname = ObjCollcTrainee.ElementAt(i).FirstName;
                    string[] tempname = fname.Split(' ');
                    if (tempname.Count() > 1)
                    {
                        ObjtblcTrainee.FirstName = tempname[0].ToString();
                        ObjtblcTrainee.LastName = tempname[1].ToString();
                    }
                    else
                    {
                        ObjtblcTrainee.FirstName = ObjCollcTrainee.ElementAt(i).FirstName;
                        ObjtblcTrainee.LastName = ObjCollcTrainee.ElementAt(i).LastName;
                    }

                    ObjtblcTrainee.Phone = ObjCollcTrainee.ElementAt(i).Phone;
                    ObjtblcTrainee.Title = ObjCollcTrainee.ElementAt(i).Title;
                    objColltblCompanyTrainee.Add(ObjtblcTrainee);
                }
                Session["TraineeCollection"] = null;
                //Session["TraineeCollection"] = string.Empty;
            }

            Guid UserId;
            int pkCompanyId;
            tblCallLog objtblCallLog = new tblCallLog();

            MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));
            if (ObjMembershipUser != null)
            {
                UserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
                pkCompanyId = Convert.ToInt16(Session["pkCompanyIdWhenEdit"]);
                string LeadComments = ScheduleDate + ", " + ScheduleTime;
                DateTime CreatedDate = System.DateTime.Now;

                objtblCallLog.fkCompanyId = pkCompanyId;
                objtblCallLog.fkUserId = UserId;
                objtblCallLog.CreatedDate = CreatedDate;
                objtblCallLog.LeadComments = LeadComments;
            }

            int DemoResult = ObjBALLeads.AddDemoSchedule(objtblDemoSchedule, objColltblCompanyTrainee, objtblCallLog);

            if (objColltblCompanyTrainee.Count() == 0)
            {
                DemoResult = 5;
                ResultStatus = 5;
                //return Json(new { Status = ResultStatus, Id = pklocationId });
            }

            if (DemoResult == 1)
            {

                List<string> LstReciverEmailIds = new List<string>();
                for (int i = 0; i < objColltblCompanyTrainee.Count; i++)
                {
                    string ReciverEmail;
                    ReciverEmail = objColltblCompanyTrainee.ElementAt(i).EmailId.ToString();
                    LstReciverEmailIds.Add(ReciverEmail);
                }
                bool CheckSend = false;
                int countmail = 0;
                if (LstReciverEmailIds.Count > 0)
                {
                    foreach (var traineeEmail in objColltblCompanyTrainee)
                    {

                        string emailFrom = ConfigurationManager.AppSettings["EmergeAdmin"].ToString(); //ObjMembershipUser.Email;
                        string Demotitle = (Convert.ToString(traineeEmail.Title) + " " + Convert.ToString(traineeEmail.FirstName)).Trim();
                        string Comments = objtblDemoSchedule.Comments.ToString();
                        string MessageBody = "";
                        string emailText = "";
                        BALGeneral ObjBALGeneral = new BALGeneral();
                        string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;
                        var emailContent = ObjBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, Convert.ToInt32(EmailTemplates.DemoSchedule));
                        emailText = emailContent.TemplateContent;
                        BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                        Bookmarks objBookmark = new Bookmarks();
                        #region For New Style SystemTemplate
                        string Bookmarkfile = string.Empty;
                        string bookmarkfilepath = string.Empty;

                        Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                        Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                        Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                        emailText = Bookmarkfile;
                        #endregion

                        objBookmark.EmailContent = emailText;

                        Session["vwUserName"] = ObjMembershipUser.UserName.ToString();

                        if (Session["vwUserName"] != null)
                        {
                            objBookmark.UserFirstName = Session["vwUserName"].ToString();
                        }

                        objBookmark.UserEmail = emailFrom;

                        objBookmark.DemoDescription = "<table width='100%' cellspacing='0' cellpadding='6' border='0'><tbody><tr style='background-color: white;'><td width='30%' align='right'>Title : </td><td align=left' style='padding-left: 5px;'>" + Demotitle + " </td></tr><tr style='background-color: white;'><td align='right'>Demo Date & Time: </td><td align='left' style='padding-left: 5px;'>" + ScheduleDate + " " + ScheduleTime + " </td></tr><tr style='background-color: white;'><td width='30%' align='right'>Comments : </td><td align='left' style='padding-left: 5px;'>" + Comments + " </td></tr></tbody></table></td></tr><tr><td><br /></td></tr></tbody></table>";
                        objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";

                        MessageBody = objTemplateBookmarks.ReplaceBookmarks_AttachmentDocuments(objBookmark, Utility.GetUID(HttpContext.User.Identity.Name.ToString()));
                        CheckSend = Utility.SendMail(traineeEmail.EmailId, emailFrom, MessageBody, "Demo Scheduled");
                        if (CheckSend == true)
                        {
                            countmail = countmail + 1;
                        }

                    }
                    if (countmail > 0)
                    {
                        string slaesManagerEmail = string.Empty;
                        if (Session["salesassociateid"] != null)
                        {
                            MembershipUser ObjMembershipUser_salesassociate = Membership.GetUser(Session["salesassociateid"]);
                            if (ObjMembershipUser_salesassociate != null)
                            {
                                slaesManagerEmail = ObjMembershipUser_salesassociate.Email;
                            }
                            else
                            {
                                slaesManagerEmail = ObjMembershipUser.Email;

                            }
                        }
                        else
                        {
                            slaesManagerEmail = ObjMembershipUser.Email;
                        }

                        string emailFrom = ConfigurationManager.AppSettings["EmergeAdmin"].ToString();//ObjMembershipUser.Email;
                        string Attendee = "";
                        foreach (var v in objColltblCompanyTrainee)
                        {
                            Attendee = Attendee + (Convert.ToString(v.Title) + " " + Convert.ToString(v.FirstName)).Trim() + ",";
                        }
                        Attendee = Attendee.Trim(',').Trim();
                        string Comments = objtblDemoSchedule.Comments.ToString();
                        string MessageBody = "";
                        string emailText = "";
                        BALGeneral ObjBALGeneral = new BALGeneral();
                        string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;
                        var emailContent = ObjBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, Convert.ToInt32(EmailTemplates.DemoSchedule));
                        emailText = emailContent.TemplateContent;
                        BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                        Bookmarks objBookmark = new Bookmarks();
                        #region For New Style SystemTemplate
                        string Bookmarkfile = string.Empty;
                        string bookmarkfilepath = string.Empty;

                        Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                        Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                        Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                        emailText = Bookmarkfile;
                        #endregion

                        objBookmark.EmailContent = emailText;

                        Session["vwUserName"] = ObjMembershipUser.UserName.ToString();

                        if (Session["vwUserName"] != null)
                        {
                            objBookmark.UserFirstName = Session["vwUserName"].ToString();
                        }

                        objBookmark.UserEmail = emailFrom;

                        objBookmark.DemoDescription = "<table width='100%' cellspacing='0' cellpadding='6' border='0'><tbody><tr style='background-color: white;'><td width='30%' align='right'>Attendee: </td><td align=left' style='padding-left: 5px;'>" + Attendee + " </td></tr><tr style='background-color: white;'><td align='right'>Demo Date & Time: </td><td align='left' style='padding-left: 5px;'>" + ScheduleDate + " " + ScheduleTime + " </td></tr><tr style='background-color: white;'><td width='30%' align='right'>Comments : </td><td align='left' style='padding-left: 5px;'>" + Comments + " </td></tr></tbody></table></td></tr><tr><td><br /></td></tr></tbody></table>";
                        objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";

                        MessageBody = objTemplateBookmarks.ReplaceBookmarks_AttachmentDocuments(objBookmark, Utility.GetUID(HttpContext.User.Identity.Name.ToString()));
                        CheckSend = Utility.SendMail(slaesManagerEmail, emailFrom, MessageBody, "Demo Scheduled");
                        //CheckSend = Utility.SendMail("shaleshm@chetu.com", emailFrom, MessageBody, "Demo Scheduled");
                    }
                }


                if (countmail > 0)
                {
                    //ScriptManager.RegisterStartupScript(this.Page, typeof(string), "success", "alert('Email sent successfully to All Trainees');", true);
                    ResultStatus = 1;//Email sent successfully to All Trainees

                }
                else
                {
                    //  ScriptManager.RegisterStartupScript(this.Page, typeof(string), "success", "alert('Error Occured while sending email !');", true);
                    ResultStatus = 2; //Error Occured while sending email

                }

                if (DemoResult == -1)
                {

                    //  lblMessage.Text = "Error occurred on Scheduling Demo.";
                    ResultStatus = -1;
                }
                if (DemoResult == -2)
                {

                    //  lblMessage.Text = "Error occurred on add trainees.";
                    ResultStatus = -2;
                }
                if (DemoResult == -3)
                {

                    // lblMessage.Text = "Error occurred on updating Call log.";
                    ResultStatus = -3;
                }


               

                if (Session["Rowid"] != null)
                {
                    Session["Rowid"] = null;

                }
                rowid = 0;

            }

            return Json(new { Status = ResultStatus, Id = pklocationId });
        }
        #endregion

        #region
        [HttpGet]
        [ActionName("Contact")]
        public ActionResult GetContact()
        {
            //AddEditLeadsModel objmodel = new AddEditLeadsModel();
            //Session["Rowid"] = string.Empty;
            if (Convert.ToInt32(Session["Rowid"]) > 2)
            {
                Session["Rowid"] = null;
            }
            Contact contact = new Contact();
            if (Session["pkCompanyIdWhenEdit"] != null)
            {
                string pkcomapnyidwhenedit = Session["pkCompanyIdWhenEdit"].ToString();
                string maincontact = Session["Maincontact"].ToString();
                string mainemail = Session["MainEmail"].ToString();
                //string billinContact = Session["Billingcontact"].ToString();
                //string billingemail = Session["BillingEmail"].ToString();
                //if (billinContact != maincontact)
                //{
                //    list.Add(new SelectListItem { Text = maincontact, Value = mainemail });
                //    list.Add(new SelectListItem { Text = billinContact, Value = billingemail });

                //}
                //else
                //{
                //    list.Add(new SelectListItem { Text = maincontact, Value = mainemail });
                //}

                contact.Name = maincontact;
                contact.Email = mainemail;
            }
            else if (Session["pkCompanyIdWhenEditJuno"] != null)
            {
                string pkcompanyidwheneedit = Session["pkCompanyIdWhenEditJuno"].ToString();
                string junomaincontact = Session["Maincontactfor=JUNO"].ToString();
                string junomainemail = Session["MainEmailforJUNO"].ToString();
                
                contact.Name = junomaincontact;
                contact.Email = junomainemail;


            }
           
            return Json(contact, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult GetLeadSourceDDL()
        {
            BALCompanyType serboj = new BALCompanyType();
            //var list = 
                serboj.GetAllCompanyType();
            List<SelectListItem> listLeadStatus = new List<SelectListItem>();

            listLeadStatus.Add(new SelectListItem { Text = "Outbound Call", Value = "1", Selected = false });
            listLeadStatus.Add(new SelectListItem { Text = "Inbound Call", Value = "2", Selected = false });
            listLeadStatus.Add(new SelectListItem { Text = "Inbound Email", Value = "3", Selected = false });
            listLeadStatus.Add(new SelectListItem { Text = "Live Transfer", Value = "4", Selected = false });
            listLeadStatus.Add(new SelectListItem { Text = "Referral", Value = "5", Selected = false });
            listLeadStatus.Add(new SelectListItem { Text = "Tradeshow", Value = "6", Selected = false });
            listLeadStatus.Add(new SelectListItem { Text = "Friend or Family", Value = "7", Selected = false });


            return Json(listLeadStatus);
        }


        #region   // Common Method For Add New Lead OR Update Existing Lead
        public ActionResult AddUpdateLeads(AddEditLeadsModel objmodel, FormCollection frm)
        {

            int? Result = null;
            bool CheckStatusSelect = false;
            //string sucessmsg = string.Empty;
            int StatusType = 0;
            if (frm["txtApptDate"] == "")
            {
                objmodel.apptDate = Convert.ToDateTime("01/01/1900");
            }
            else
            {
                objmodel.apptDate = Convert.ToDateTime(frm["txtApptDate"]);
            }
           
            objmodel.nextStep = Convert.ToString(frm["ddlNextStep"]);

            if (objmodel.IsEdit)
            {
                int Isupdate = UpdateLead(objmodel);
                Session["Newpkcompanyid"] = Session["pkCompanyIdWhenEdit"];
                Session["NewPklocationid"] = Session["pkLocationIdWhenEdit"];
                if (Isupdate == 1)
                {
                    StatusType = 1;// "laed Updated Sucessfully";

                }
                else if (Isupdate == -2)
                {
                    StatusType = 2;// "Error Occured While Updating Lead";
                }

                else if (Isupdate == -3)
                {
                    StatusType = 3;// "Error Occured While Updating Location";
                }
                else if (Isupdate == -4)
                {
                    StatusType = 4;// "A Lead With This Account Number Already Exists";

                }
                else if (Isupdate == -7)
                {
                    StatusType = 7;// "Error Occured While Updating call Logs";

                }

                Result = Convert.ToInt16(Session["pkLocationIdWhenEdit"]);

                return Json(new { Save = StatusType, Id = Result });

            }
            else
            {
                List<int> CompanyIdAndLocationId = AddLeads(objmodel);
                int pkcomapnyid = 0;
                if (CompanyIdAndLocationId.Count != 0)
                {

                    //int creation =
                        AddUniversalPackagesForCompany(CompanyIdAndLocationId[0], CompanyIdAndLocationId[1]);
                    objlocationid = CompanyIdAndLocationId[1];
                    pkcomapnyid = CompanyIdAndLocationId[0];
                    Session["Newpkcompanyid"] = CompanyIdAndLocationId[0];
                    Session["NewPklocationid"] = CompanyIdAndLocationId[1];

                    AddProducts(CompanyIdAndLocationId[0], CompanyIdAndLocationId[1]);

                }
                var _leadStatus = 1;
                if (objmodel.leadStatusTxt != null && objmodel.leadStatusTxt != "")
                {
                    char[] splitBy = { '_' };
                    var str = objmodel.leadStatusTxt.Split(splitBy);
                    _leadStatus = Convert.ToByte(str[1]);
                }



                if (Convert.ToInt16(CompanyIdAndLocationId[1]) != 0)
                {
                    MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
                    Guid UserId = Utility.GetUID(User.Identity.Name);
                    AddStatusToCallLog(pkcomapnyid, UserId, "##" + _leadStatus + "##", DateTime.Now, CheckStatusSelect);
                    StatusType = 5;//for sucessully add
                }
                else
                {
                    StatusType = 6;// un sucess
                }

                Result = objlocationid;
                return Json(new { Save = StatusType, Id = Result });
            }

        }
        #endregion

        #region   // Add Prooducts In Relevant SP Class
        private void AddProducts(int PkCompanyId, int LocationId)
        {
            //  ObjGuid_ProductIds = new Dictionary<Guid, string>();
            //  IsLiveRunner = new List<bool>();
            List<SubReportsModel> data = new List<SubReportsModel>();
            List<tblCompany_Price> ObjtblCompany_Price_List = new List<tblCompany_Price>();
            List<tblProductAccess> deletetblProductAccess = new List<tblProductAccess>();
            List<tblProductAccess> ObjtblProductAccess_List = new List<tblProductAccess>();
            BALLocation ObjBALLocation = new BALLocation();
            BALCompany ObjBALCompany = new BALCompany();
            int count = 0;
            var categories = ObjBALCompany.GetReportCategories();
            if (categories.Count > 0)
            {
                foreach (var category in categories)
                {
                    var List = ObjBALCompany.GetReportProductsById(LocationId, category.pkReportCategoryId);
                    if (List.Count > 0)
                    {
                        if (data.Count > 0)
                        {
                            count = data.Count - 1;
                        }
                        data.InsertRange(count, List.Select(x => new SubReportsModel
                        {
                            pkProductApplicationId = x.pkProductApplicationId,
                            pkProductId = x.pkProductId,
                            ProductAccessId = Convert.ToInt16(x.ProductAccessId),
                            AutoReviewStatusForLocation = (x.AutoReviewStatusForLocation != null) ? x.AutoReviewStatusForLocation.ToString() : "0",
                            ProductDisplayName = x.ProductDisplayName,
                            ProductName = x.ProductName,
                            ProductCode = x.ProductCode,
                            ProductDescription = x.ProductDescription,
                            CategoryName = x.CategoryName,
                            CompanyPrice = x.CompanyPrice,
                            CoveragePDFPath = x.CoveragePDFPath,
                            IsDefault = x.IsDefault,
                            IsDefaultPrice = x.IsDefaultPrice,
                            IsLiveRunnerLocation = x.IsLiveRunnerLocation,
                            IsLiveRunnerProduct = x.IsLiveRunnerProduct,
                            IsThirdPartyFee = x.IsThirdPartyFee,
                            LinkedReportId = x.LinkedReportId,
                            ReportsLinked = x.ReportsLinked
                        }).ToList());
                    }
                }
            }
            for (int iRow = 0; iRow < data.Count; iRow++)
            {
                tblCompany_Price ObjtblCompany_Price = new tblCompany_Price();
                ObjtblCompany_Price.IsDefaultPrice = true;
                ObjtblCompany_Price.CompanyPrice = data.ElementAt(iRow).CompanyPrice;
                ObjtblCompany_Price.fkCompanyId = PkCompanyId;
                ObjtblCompany_Price.fkProductApplicationId = data.ElementAt(iRow).pkProductApplicationId;

                ObjtblCompany_Price.pkCompanyPriceId = 0;

                ObjtblCompany_Price.EnableReviewPerCompany = data.ElementAt(iRow).EnableReviewPerCompany;
                ObjtblCompany_Price.LinkedReportId = null;

                ObjtblCompany_Price_List.Add(ObjtblCompany_Price);
                if (data.ElementAt(iRow).IsDefault != null && Convert.ToBoolean(data.ElementAt(iRow).IsDefault))
                {
                    tblProductAccess ObjtblProductAccess = new tblProductAccess();
                    ObjtblProductAccess.fkLocationId = LocationId;
                    ObjtblProductAccess.fkProductApplicationId = data.ElementAt(iRow).pkProductApplicationId;
                    ObjtblProductAccess.IsLiveRunner = data.ElementAt(iRow).IsLiveRunnerProduct;
                    ObjtblProductAccess.ProductReviewStatus = (data.ElementAt(iRow).AutoReviewStatusForLocation != null) ? byte.Parse(data.ElementAt(iRow).AutoReviewStatusForLocation) : byte.MinValue;
                    ObjtblProductAccess.IsProductEnabled = data.ElementAt(iRow).IsDefault != null ? Convert.ToBoolean(data.ElementAt(iRow).IsDefault) : false;
                    ObjtblProductAccess.LocationProductPrice = data.ElementAt(iRow).CompanyPrice;
                    ObjtblProductAccess_List.Add(ObjtblProductAccess);
                }
            }

            ObjBALLocation.InsertUpdateLocationProduct(ObjtblProductAccess_List, deletetblProductAccess);
            ObjBALCompany.AddProductPricePerCompany(ObjtblCompany_Price_List);

        }
        #endregion

        #region   // Update Existing Lead
        public int UpdateLead(AddEditLeadsModel objmodel)
        {
            tblCompany ObjtblCompany = new tblCompany();
            tblLocation ObjtblLocation = new tblLocation();
            Guid CompanyUserId = Guid.Empty;
            BALLeads ObjBALLeads = new BALLeads();
            bool checkselectstatus = false;

            if (objmodel.LeadSourceJuno == "Juno")
            {
                ObjtblCompany.LeadStatus = 1;
                char[] splitBy = { '_' };
                if (objmodel.leadStatusTxt != null && objmodel.leadStatusTxt != "")
                {
                    var str = objmodel.leadStatusTxt.Split(splitBy);
                    ObjtblCompany.LeadStatus = Convert.ToByte(str[1]);
                }
            }
            else
            {
                ObjtblCompany.LeadStatus = Convert.ToByte(objmodel.LeadStatus);
            }
            int Result = 0;
            //BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();

            //string ext1 = objmodel.MainPhone;
            //string ext2 = objmodel.BillingPhone;
            MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));
            CompanyUserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
            try
            {
                //ObjtblCompany.pkCompanyId = Guid.Parse(Session["pkCompanyIdWhenEdit"].ToString());
                ObjtblCompany.pkCompanyId = objmodel.pkCompanyId;


                if (ObjtblCompany.BillingContactPersonName == null)
                {
                    ObjtblCompany.BillingContactPersonName = "";
                }
                else
                {
                    ObjtblCompany.BillingContactPersonName = objmodel.BillingContact;
                }


                if (ObjtblCompany.BillingEmailId == null)
                {
                    ObjtblCompany.BillingEmailId = "";
                }
                else
                {
                    ObjtblCompany.BillingEmailId = objmodel.BillingEmail;
                }
                ObjtblCompany.CompanyCode = Utility.GenerateCompanyCode(objmodel.CompanyName, DateTime.Now);
                ObjtblCompany.CompanyName = objmodel.CompanyName;
                ObjtblCompany.CompanyNote = objmodel.CompanyNote;
                ObjtblCompany.CreatedDate = DateTime.Now;

                ObjtblCompany.fkApplicationId = Utility.SiteApplicationId;
                ObjtblCompany.fkCompanyTypeId = objmodel.pkCompanyTypeId;
                ObjtblCompany.IsDeleted = false;
                ObjtblCompany.IsEnabled = true;
                ObjtblCompany.MainContactPersonName = objmodel.MainContact;
                ObjtblCompany.MainEmailId = objmodel.MainEmail == null ? string.Empty : objmodel.MainEmail;
                ObjtblCompany.fkSalesManagerId = objmodel.fkUserId1;
                ObjtblCompany.fkSalesRepId = objmodel.fkSalesAssociateId;
                ObjtblCompany.CompanyAccountNumber = string.Empty;
                ObjtblCompany.FranchiseName = string.Empty;
                ObjtblCompany.LastModifiedById = CompanyUserId;
                ObjtblCompany.LastModifiedDate = DateTime.Now;
                ObjtblCompany.IsMainEmail = objmodel.IsMainEmail;


                ObjtblCompany.LeadNumber = objmodel.LeadNumber;
                ObjtblCompany.LeadSource = objmodel.LeadSource == null ? string.Empty : objmodel.LeadSource;
                ObjtblCompany.NoOfLocations = Convert.ToByte(objmodel.NumberOfLocation);
                ObjtblCompany.CompanyUrl = objmodel.CompanyUrl == null ? string.Empty : objmodel.CompanyUrl;
                ObjtblCompany.SecondaryMainContactPersonName = objmodel.SecondaryMainContactPersonName == null ? string.Empty : objmodel.SecondaryMainContactPersonName;
                ObjtblCompany.MainTitle = objmodel.MainTitle == null ? string.Empty : objmodel.MainTitle;
                ObjtblCompany.SecondaryTitle = objmodel.SecondaryTitle == null ? string.Empty : objmodel.SecondaryTitle;
                ObjtblCompany.SecondaryEmail = objmodel.SecondaryEmail == null ? string.Empty : objmodel.SecondaryEmail;
                ObjtblCompany.MonthlyVolume = objmodel.MonthlyVolume == null ? 0 : objmodel.MonthlyVolume;
                ObjtblCompany.NoofEmployees = objmodel.NoofEmployees == null ? "" : objmodel.NoofEmployees;
                ObjtblCompany.LeadSourceId = objmodel.LeadSourceid == null ? 0 : int.Parse(objmodel.LeadSourceid);


                if (objmodel.CurrentProvider == null)
                {
                    ObjtblCompany.CurrentProvider = "";
                }
                else
                {
                    ObjtblCompany.CurrentProvider = objmodel.CurrentProvider;
                }
                ObjtblCompany.IsEnabled = true;

                // ObjtblCompany.LeadStatus = Convert.ToByte(objmodel.LeadStatus); //not clear

                //   string pklocationid = Session["pkLocationIdWhenEdit"].ToString();

                //ObjtblLocation.pkLocationId = Guid.Parse(pklocationid);
                ObjtblLocation.pkLocationId = objmodel.pklocationid;
                ObjtblLocation.LocationContact = objmodel.MainContact;
                if (objmodel.Address1 == null)
                {
                    ObjtblLocation.Address1 = "";
                }
                else
                {
                    ObjtblLocation.Address1 = objmodel.Address1;
                }

                if (objmodel.Address2 == null)
                {
                    ObjtblLocation.Address2 = "";
                }
                else
                {
                    ObjtblLocation.Address2 = objmodel.Address2;
                }
                ObjtblCompany.Priority1 = objmodel.Priority1;
                if (objmodel.Priority1 == "Other")
                {
                    ObjtblCompany.Priority1Notes = objmodel.Priority01;

                }
                ObjtblCompany.Priority2 = objmodel.Priority2;
                if (objmodel.Priority2 == "Other")
                {
                    ObjtblCompany.Priority2Notes = objmodel.Priority02;

                }

                ObjtblCompany.Priority3 = objmodel.Priority3;
                if (objmodel.Priority3 == "Other")
                {
                    ObjtblCompany.Priority3Notes = objmodel.Priority03;

                }
                else
                {
                    ObjtblCompany.Priority3Notes = string.Empty;
                }

                if (objmodel.CityName == null)
                {
                    ObjtblLocation.City = "";
                }
                else
                {
                    ObjtblLocation.City = objmodel.CityName;
                }
                ObjtblLocation.Ext1 = objmodel.Ext1 == null ? string.Empty : objmodel.Ext1;
                ObjtblLocation.Ext2 = objmodel.Ext2 == null ? string.Empty : objmodel.Ext2;
                ObjtblLocation.fkStateID = objmodel.pkStateId;
                ObjtblLocation.IsHome = true; // not clear

                if (objmodel.BillingPhone == null)
                {
                    ObjtblLocation.PhoneNo2 = "";
                }
                else
                {

                    ObjtblLocation.PhoneNo2 = objmodel.BillingPhone;
                }
                ObjtblLocation.PhoneNo1 = objmodel.MainPhone == null ? string.Empty : objmodel.MainPhone;
                if (objmodel.ZipCod == null)
                {
                    ObjtblLocation.ZipCode = "";
                }
                else
                {

                    ObjtblLocation.ZipCode = objmodel.ZipCod;
                }
                ObjtblLocation.LastModifiedById = CompanyUserId;
                ObjtblLocation.LastModifiedDate = DateTime.Now;

                if (objmodel.BillingFax == null)
                {
                    ObjtblLocation.Fax = "";
                }
                else
                {
                    ObjtblLocation.Fax = objmodel.BillingFax;
                }
                ObjtblLocation.LocationEmail = string.Empty;

                if (objmodel.MainFaxNumber == null)
                {
                    ObjtblLocation.Fax2 = "";
                }
                else
                {
                    ObjtblLocation.Fax2 = objmodel.MainFaxNumber;
                }

                ObjtblLocation.SecondaryPhone = objmodel.SecondaryPhone == null ? string.Empty : objmodel.SecondaryPhone;
                ObjtblLocation.SecondaryFax = objmodel.SecondaryFax == null ? string.Empty : objmodel.SecondaryFax;
                ObjtblLocation.IsEnabled = true;
                if (objmodel.leadStatusTxt != null && objmodel.leadStatusTxt != "")
                {
                    byte vwLeadStatus = Convert.ToByte(objmodel.LeadStatus);
                    string[] Btnstr = objmodel.leadStatusTxt.Split('_');
                    byte Status = Convert.ToByte(Btnstr[1]);
                    bool updown = false;
                    updown = Status <= vwLeadStatus ? updown : true;

                    if (Status == 1)
                    {
                        ObjtblCompany.LeadStatus = (byte)Status;
                    }
                    else
                    {
                        ObjtblCompany.LeadStatus = (updown) ? (byte)Status : (byte)(Status - 1);
                    }
                    ObjtblCompany.LastModifiedDate = DateTime.Now;
                    ObjtblCompany.StatusStartDate = DateTime.Now;
                    ObjtblCompany.LastModifiedById = (CompanyUserId != null) ? CompanyUserId : Guid.Empty;
                    // int success = ObjBALLeads.UpdateLeadStatus(ObjtblCompany);
                }
                ObjtblCompany.ApptDate = objmodel.apptDate;
                ObjtblCompany.NextStep = objmodel.nextStep;
                Result = ObjBALLeads.UpdateLead(ObjtblCompany, ObjtblLocation, objmodel.callLogText);
                if (Result == 1)
                {
                    if (objmodel.leadStatusTxt != null && objmodel.leadStatusTxt != "")
                    {
                        byte vwLeadStatus = Convert.ToByte(objmodel.LeadStatus);
                        string[] Btnstr = objmodel.leadStatusTxt.Split('_');
                        byte Status = Convert.ToByte(Btnstr[1]);
                        bool updown = false;
                        updown = Status <= vwLeadStatus ? updown : true;

                        UpdateCallLog(objmodel.leadStatusTxt, checkselectstatus);
                        Session["LeadStatus"] = updown ? 0 : Status - 1;
                        Session["LeadStatusView"] = 0;
                    }
                }
            }
            catch (Exception)
            {

            }
            finally
            {

            }
            return Result;



        }

        public int SaveCallLogValue(string Comment, int pkcompanyid)
        {
            Guid UserId;
            int pkCompanyId;
            MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));

            if (ObjMembershipUser != null)
            {
                UserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
                pkCompanyId = pkcompanyid;
                string Comments = Comment;
                DateTime CreatedDate = System.DateTime.Now;
                return AddCommentToCallLog(pkCompanyId, UserId, Comments, CreatedDate);
            }

            return 0;
        }
        #endregion

        #region   // Upload Company Coverege PDF
        public ActionResult UploadCoveragePDF(ManageReportModel ObjModel, HttpPostedFileBase CoveragePDFFile, string hdProductIdPDF, string hdPreviousPDFFile)
        {
            string strMessage = string.Empty;
            BALProducts ObjBALProducts = new BALProducts();
            if (CoveragePDFFile != null)
            {

                if (CoveragePDFFile != null && CoveragePDFFile.ContentLength > 0)
                {
                    if (CheckPhotoExtensions(CoveragePDFFile))
                    {

                        string Pcode = "";
                        int ProductId = 0;
                        string[] ch = new string[] { "##" };

                        string[] arr = hdProductIdPDF.Split(ch, StringSplitOptions.RemoveEmptyEntries);
                        if (arr.Length == 2)
                        {
                            Pcode = arr[0];
                            ProductId = Convert.ToInt16(arr[1]);
                        }
                        string sExt = Path.GetExtension(CoveragePDFFile.FileName).ToLower();
                        string sFileName = Pcode + "_" + DateTime.Now.ToFileTime().ToString() + sExt;
                        string sFilePath = "";

                        sFilePath = HttpContext.Server.MapPath("~/Resources/Upload/CoveragePDF/");

                        if (!System.IO.Directory.Exists(sFilePath))
                        {
                            System.IO.Directory.CreateDirectory(sFilePath);
                        }
                        if (System.IO.File.Exists(HttpContext.Server.MapPath(@"~\Resources\Upload\CoveragePDF\") + sFileName))
                        {
                            try
                            {
                                System.IO.File.Delete(HttpContext.Server.MapPath(@"~\Resources\Upload\CoveragePDF\") + sFileName);
                            }
                            catch { }
                        }
                        CoveragePDFFile.SaveAs(HttpContext.Server.MapPath(@"~\Resources\Upload\CoveragePDF\") + sFileName);

                        int success = ObjBALProducts.UploadCoveragePDF(ProductId, sFileName);

                        if (success == 1)
                        {
                            StausType = 1;// "File Uploaded Successfully";

                            if (hdPreviousPDFFile != null)
                            {
                                if (!String.IsNullOrEmpty(hdPreviousPDFFile))
                                {
                                    try
                                    {
                                        System.IO.File.Delete(HttpContext.Server.MapPath(@"~\Resources\Upload\CoveragePDF\") + hdPreviousPDFFile);
                                    }
                                    catch { }
                                }
                            }

                        }
                        else
                        {
                            StausType = 2;// "Some error occur while uploading, please try later";
                        }
                    }
                    else
                    {
                        //
                    }

                }
            }
            ViewBag.Message1 = strMessage;
            return RedirectToAction("AddEditLeads", new { Status = StausType });
        }
        #endregion


        #region  // Check File Extentation is .pdf or .png..etc
        public bool CheckPhotoExtensions(HttpPostedFileBase fUploader)
        {
            string Extension = string.Empty;

            if (fUploader.FileName != string.Empty)
            {
                Extension = Path.GetExtension(fUploader.FileName).ToLower();
                if (fUploader.ContentLength > 0)
                {
                    if (Extension == ".pdf")
                    { }
                    else
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        #endregion

        #region    // Add  New Lead
        public List<int> AddLeads(AddEditLeadsModel objmodel)
        {
            List<int> ObjCompLocation = new List<int>();
            Guid CompanyUserId = Guid.Empty;
            tblCompany objtblCompany = new tblCompany();
            tblLocation objtbllocation = new tblLocation();

            MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));
            CompanyUserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
            char[] splitBy = { '_' };

            objtblCompany.LeadStatus = 1;
            if (objmodel.leadStatusTxt != null && objmodel.leadStatusTxt != "")
            {
                var str = objmodel.leadStatusTxt.Split(splitBy);
                objtblCompany.LeadStatus = Convert.ToByte(str[1]);
            }
            objtblCompany.CreatedById = CompanyUserId;
            if (objtblCompany.BillingContactPersonName == null)
            {
                objtblCompany.BillingContactPersonName = "";
            }
            else
            {
                objtblCompany.BillingContactPersonName = objmodel.BillingContact;
            }

            if (objtblCompany.BillingEmailId == null)
            {
                objtblCompany.BillingEmailId = "";
            }
            else
            {
                objtblCompany.BillingEmailId = objmodel.BillingEmail;
            }
            objtblCompany.CompanyCode = Utility.GenerateCompanyCode(objmodel.CompanyName, DateTime.Now);
            objtblCompany.CompanyName = objmodel.CompanyName;
            objtblCompany.CompanyNote = objmodel.CompanyNote;
            objtblCompany.CreatedDate = DateTime.Now;
            objtblCompany.fkApplicationId = Utility.SiteApplicationId;
            objtblCompany.fkCompanyTypeId = objmodel.pkCompanyTypeId;
            objtblCompany.IsDeleted = false;
            objtblCompany.IsEnabled = true;
            objtblCompany.MainContactPersonName = objmodel.MainContact;
            objtblCompany.MainEmailId = objmodel.MainEmail == null ? string.Empty : objmodel.MainEmail;
            objtblCompany.fkSalesManagerId = objmodel.fkUserId1;
            objtblCompany.fkSalesRepId = objmodel.fkSalesAssociateId;
            objtblCompany.CompanyAccountNumber = " ";


            objtblCompany.LastModifiedDate = DateTime.Now;
            objtblCompany.LeadNumber = objmodel.LeadNumber;
            objtblCompany.LeadSource = objmodel.LeadSource == null ? string.Empty : objmodel.LeadSource;
            objtblCompany.NoOfLocations = Convert.ToByte(objmodel.NumberOfLocation);
            objtblCompany.CompanyUrl = objmodel.CompanyUrl == null ? string.Empty : objmodel.CompanyUrl;
            objtblCompany.SecondaryMainContactPersonName = objmodel.SecondaryMainContactPersonName == null ? string.Empty : objmodel.SecondaryMainContactPersonName;
            objtblCompany.MainTitle = objmodel.MainTitle == null ? string.Empty : objmodel.MainTitle;
            objtblCompany.SecondaryTitle = objmodel.SecondaryTitle == null ? string.Empty : objmodel.SecondaryTitle;
            objtblCompany.SecondaryEmail = objmodel.SecondaryEmail == null ? string.Empty : objmodel.SecondaryEmail;
            objtblCompany.MonthlyVolume = objmodel.MonthlyVolume == null ? 0 : objmodel.MonthlyVolume;
            objtblCompany.NoofEmployees = objmodel.NoofEmployees == null ? "" : objmodel.NoofEmployees;
            objtblCompany.LeadSourceId = objmodel.LeadSourceid == null ? 0 : int.Parse(objmodel.LeadSourceid);
            if (objmodel.CurrentProvider == null)
            {
                objtblCompany.CurrentProvider = "";
            }
            else
            {
                objtblCompany.CurrentProvider = objmodel.CurrentProvider;

            }
            objtblCompany.IsEnableReview = !true;
            objtblCompany.Priority1 = objmodel.Priority1;
            objtblCompany.Priority1Notes = string.Empty;
            objtblCompany.Priority2 = objmodel.Priority2;
            objtblCompany.Priority2Notes = string.Empty;
            objtblCompany.Priority3 = objmodel.Priority3;
            objtblCompany.Priority3Notes = string.Empty;

            objtblCompany.FranchiseName = "";
            objtblCompany.IsMainEmail = objmodel.IsMainEmail;


            if (objmodel.Address1 == null)
            {
                objtbllocation.Address1 = "";
            }
            else
            {
                objtbllocation.Address1 = objmodel.Address1;
            }



            if (objmodel.Address2 == null)
            {
                objtbllocation.Address2 = "";
            }
            else
            {
                objtbllocation.Address2 = objmodel.Address2;
            }
            if (objmodel.CityName == null)
            {
                objtbllocation.City = "";
            }
            else
            {
                objtbllocation.City = objmodel.CityName;
            }
            objtbllocation.CreatedById = CompanyUserId;
            objtbllocation.LocationContact = objmodel.BillingContact == null ? string.Empty : objmodel.BillingContact;
            objtbllocation.CreatedDate = DateTime.Now;
            objtbllocation.Ext1 = objmodel.Ext1 == null ? string.Empty : objmodel.Ext1;
            objtbllocation.Ext2 = objmodel.Ext2 == null ? string.Empty : objmodel.Ext2;
            if (objmodel.BillingFax == null)
            {
                objtbllocation.Fax = "";
            }
            else
            {
                objtbllocation.Fax = objmodel.BillingFax;
            }
            objtbllocation.fkStateID = objmodel.pkStateId;
            objtbllocation.IsHome = true;
            objtbllocation.LocationCode = Utility.GenerateLocationCode(objmodel.CompanyName, DateTime.Now);



            if (objmodel.BillingPhone == null)
            {
                objtbllocation.PhoneNo2 = "";
            }
            else
            {

                objtbllocation.PhoneNo2 = objmodel.BillingPhone;
            }

            objtbllocation.PhoneNo1 = objmodel.MainPhone == null ? string.Empty : objmodel.MainPhone;
            if (objmodel.ZipCod == null)
            {
                objtbllocation.ZipCode = "";
            }
            else
            {

                objtbllocation.ZipCode = objmodel.ZipCod;
            }
            if (objmodel.MainFaxNumber == null)
            {
                objtbllocation.Fax2 = "";
            }
            else
            {
                objtbllocation.Fax2 = objmodel.MainFaxNumber;
            }



            objtbllocation.LocationEmail = string.Empty;

            objtbllocation.SecondaryPhone = objmodel.SecondaryPhone == null ? string.Empty : objmodel.SecondaryPhone;
            objtbllocation.SecondaryFax = objmodel.SecondaryFax == null ? string.Empty : objmodel.SecondaryFax;
            objtbllocation.IsEnabled = true;

            BALLeads serobj = new BALLeads();
            objtblCompany.ApptDate = Convert.ToDateTime(objmodel.apptDate);
            objtblCompany.NextStep = objmodel.nextStep;


            ObjCompLocation = serobj.InsertNewCompanyLead(objtblCompany, objtbllocation, objmodel.callLogText);

            return ObjCompLocation;


        }
        #endregion

        #region  // Get List Of Main Category of List
        public ActionResult GetCategoryList()
        {
            BALCompany ObjBal = new BALCompany();
            var list = ObjBal.GetReportCategories();
            return Json(list);

        }
        #endregion

        #region   // Get Sub Category On Base Of Main Category
        public ActionResult GetReportByCategoryId(byte pkCategoryId, string CompanyId1, string LocationId)
        {
            //AdCompanyModel objmodel = new AdCompanyModel();
            // Guid CompanyId = Guid.Empty;
            List<SubReportsModel> data = new List<SubReportsModel>();
            int locationbyCategioryId = 0;
            int CompanyId = 0;
            int cmpId=(!string.IsNullOrEmpty(CompanyId1)?Convert.ToInt32(CompanyId1):0);
            int locId=(!string.IsNullOrEmpty(LocationId)?Convert.ToInt32(LocationId):0);
            
            if (cmpId != 0 && locId != 0)
            {
                locationbyCategioryId = locId;
                CompanyId = cmpId;
                BALCompany serobj = new BALCompany();
                var List = serobj.GetReportByIdatEdit(locationbyCategioryId, pkCategoryId, CompanyId);
                if (List.Count > 0)
                {
                    data = List.Select(x => new SubReportsModel
                    {
                        pkProductApplicationId = x.pkProductApplicationId,
                        pkProductId = x.pkProductId,
                        ProductAccessId = x.ProductAccessId,
                        AutoReviewStatusForLocation = (x.AutoReviewStatusForLocation != null) ? x.AutoReviewStatusForLocation.ToString() : "0",
                        ProductDisplayName = x.ProductDisplayName,
                        ProductName = x.ProductName,
                        ProductCode = x.ProductCode,
                        ProductDescription = x.ProductDescription,
                        CategoryName = x.CategoryName,
                        CompanyPrice = (x.CompanyPrice != null) ? decimal.Parse(x.CompanyPrice.ToString()) : 0,
                        CoveragePDFPath = x.CoveragePDFPath,
                        IsDefault = x.IsProductEnabled,
                        IsDefaultPrice = x.IsDefaultPrice,
                        IsLiveRunnerLocation = x.IsLiveRunnerLocation,
                        IsLiveRunnerProduct = x.IsLiveRunnerProduct,
                        IsThirdPartyFee = x.IsThirdPartyFee,
                        MonthlyVolumeProductWise = (x.MonthlyVolume != null) ? decimal.Parse(x.MonthlyVolume.ToString()) : 0,
                        MonthlySaving = (x.MonthlySaving != null) ? decimal.Parse(x.MonthlySaving.ToString()) : 0,
                        MonthlySavinghdn = (x.MonthlySaving != null) ? decimal.Parse(x.MonthlySaving.ToString()) : 0,
                        CurrentPrice = (x.CurrentPrice != null) ? decimal.Parse(x.CurrentPrice.ToString()) : 0

                    }).ToList();
                }
                // var List = "";

                // var List = "";
                return Json(data);
            }
            else
            {
                locationbyCategioryId = 0;
                CompanyId = 0;
                //var comapnyprice = "";
                BALCompany serobj = new BALCompany();
                var List = serobj.GetReportProductsById(locationbyCategioryId, pkCategoryId);
                if (List.Count > 0)
                {
                    data = List.Select(x => new SubReportsModel
                    {
                        pkProductApplicationId = x.pkProductApplicationId,
                        pkProductId = x.pkProductId,
                        ProductAccessId = x.ProductAccessId,
                        AutoReviewStatusForLocation = (x.AutoReviewStatusForLocation != null) ? x.AutoReviewStatusForLocation.ToString() : "0",
                        ProductDisplayName = x.ProductDisplayName,
                        ProductName = x.ProductName,
                        ProductCode = x.ProductCode,
                        ProductDescription = x.ProductDescription,
                        CategoryName = x.CategoryName,
                        CompanyPrice = x.CompanyPrice,
                        CoveragePDFPath = x.CoveragePDFPath,
                        IsDefault = x.IsDefault,
                        IsDefaultPrice = x.IsDefaultPrice,
                        IsLiveRunnerLocation = x.IsLiveRunnerLocation,
                        IsLiveRunnerProduct = x.IsLiveRunnerProduct,
                        IsThirdPartyFee = x.IsThirdPartyFee,
                        MonthlyVolumeProductWise = 0,
                        MonthlySaving = 0,
                        MonthlySavinghdn = 0,
                        CurrentPrice = 0

                    }).ToList();
                }
                // var List = "";
                return Json(data);

            }
            //objmodel=
        }
        #endregion

        private int AddUniversalPackagesForCompany(int CompanyId, int LocationId)
        {
            int Result = -1;
            BALProductPackages ObjBALProductPackages = new BALProductPackages();
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    try
                    {
                        if (CompanyId != 0)
                        {
                            List<tblProductPackage> lst = DX.tblProductPackages.Where(d => d.fkCompanyId == null && d.IsDelete == false).ToList<tblProductPackage>();
                            if (lst.Count > 0)
                            {
                                List<tblCompany_PackagePrice> lstCompany_PackagePrice = new List<tblCompany_PackagePrice>();
                                List<tblPackageAccess> lstPackageAccess = new List<tblPackageAccess>();

                                for (int i = 0; i < lst.Count; i++)
                                {
                                    tblCompany_PackagePrice ObjCompany_PackagePrice = new tblCompany_PackagePrice();    // add in new company (company wise)

                                    // ObjCompany_PackagePrice.pkCompanyPackagePriceId = Guid.NewGuid();
                                    ObjCompany_PackagePrice.fkCompanyId = CompanyId;
                                    ObjCompany_PackagePrice.fkPackageId = lst.ElementAt(i).pkPackageId;
                                    ObjCompany_PackagePrice.PackagePrice = lst.ElementAt(i).PackagePrice;
                                    ObjCompany_PackagePrice.IsEnableUniversalPackage = true;

                                    lstCompany_PackagePrice.Add(ObjCompany_PackagePrice);

                                    tblPackageAccess ObjPackageAccess = new tblPackageAccess();             // add in new company location wise

                                    ObjPackageAccess.pkPackageAccessId = 0;
                                    ObjPackageAccess.fkLocationId = LocationId;
                                    ObjPackageAccess.fkPackageId = lst.ElementAt(i).pkPackageId;

                                    lstPackageAccess.Add(ObjPackageAccess);
                                }
                                Result = ObjBALProductPackages.AddUniversalPackagesForNewCompany(lstCompany_PackagePrice, lstPackageAccess);
                            }
                        }

                    }
                    catch
                    {
                    }
                }
            }
            catch
            {
            }
            finally
            {
                ObjBALProductPackages = null;
            }
            return Result;
        }



    }

    public class Contact
    {
        public string Name { get; set; }
        public string Email { get; set; }

    }
}
