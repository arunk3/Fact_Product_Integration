﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Common;
using Emerge.Services;
using System.Web.Security;
using System.Globalization;
using Emerge.Data;
using MangoChat;
using Emerge.Services.Helper;

namespace Emerge.Controllers.Office
{
    public partial class OfficeController : Controller
    {
        #region Office Login
        public void Index()
        {

            Response.Redirect(Emerge.Services.Helper.ApplicationPath.GetApplicationPath() + "Office/Default");
        }

        public ActionResult Default()
        {
            LoginModel Model = new LoginModel();
            string username;
            string password;
            string UserRole;
            bool Remembered;
            try
            {
                Utility.GetCookie(out username, out password, out UserRole, out Remembered);
                //Model.listDefaultPages = checkusers(UserRole);
                if (Remembered)
                {
                    Model.UserName = username;
                    ViewBag.Password = password;
                   // ViewBag.Password = EncryptDecrypt.Decrypt(password);
                    Model.RemeberMe = true;
                    ProfileCommon Profile = new ProfileCommon();
                   
                    //  for New Login Page

                    BALContentManagement objContentsMgt = new BALContentManagement();
                    Model.UserFistLastName = string.Empty;

                    var list = objContentsMgt.GetCompanyNameByEmailId(username);

                    if (list.UserImage != "" && list.UserImage != null)
                    {
                        Model.UserImage = list.UserImage;
                       
                    }
                    else
                    {
                        Model.UserImage = null;
                    }
                    var list2 = objContentsMgt.GetCompanyNameByUserId(username);
                    var FirstName = list.FirstName;
                    var LastName = list.LastName;
                    var firstandlastname = FirstName + "  " + LastName;

                    CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
                    TextInfo textInfo = cultureInfo.TextInfo;
                    string Searchedfirstname = firstandlastname != null ? textInfo.ToTitleCase(firstandlastname) : string.Empty;


                    Model.UserFistLastName = Searchedfirstname;
                    Model.CompanyName = list2.CompanyName;


                    // end New Login Page





                    ProfileModel userProfile = Profile.GetProfile(username);
                    ViewBag.tdTakeMeTo = "true";
                    ViewBag.tdDDLDefaultPage = "true";
                    ViewBag.DDLDefaultPage = "true";
                    ViewBag.lblTakeMeTo = "true";
                    if (userProfile.DefaultPage != "")
                    {
                        Model.listDefaultPages = Model.listDefaultPages.Select(x => new SelectListItem { Text = x.Text, Value = x.Value, Selected = (x.Value == userProfile.DefaultPage) }).ToList();
                    }
                }
                BALGeneral ObjBALGeneral = new BALGeneral();
                byte settingId = 1;
                var settings = ObjBALGeneral.GetSettings(settingId);
                if (settings != null)
                {
                  //  ViewBag.Content = settings.PageContents.Replace("WebApp/Resources/Upload/emerge_v3.jpg", "Resources/Upload/emerge_v3.jpg");
                  //  ViewBag.Content = Emerge.CommonClasses.WPPosts.RecentPosts(ViewBag.Content);
                  //  ViewBag.Content = Server.HtmlDecode(ViewBag.Content);

                    // new Login page

                    BALContentManagement objContents = new BALContentManagement();
                    tblLoginPageManagement ObjtblLoginMgt = new tblLoginPageManagement();
                    tblLoginPageSetting objtblLoginPageSetting = new tblLoginPageSetting();
                    objtblLoginPageSetting = objContents.GetLogoImg();
                    string logoImagePath = objtblLoginPageSetting.LoginLogoImg;
                    ViewBag.loginbackcolor = objtblLoginPageSetting.LoginBackColor;
                    var backimg = objtblLoginPageSetting.LoginBackImg;


                    var loadingBgImg = objtblLoginPageSetting.LoadingImage;
                    ViewBag.loadingbgImg = "Resources/Upload/LoginLogo/" + loadingBgImg;

                    var FinalBackImg = "Resources/Upload/LoginLogo/" + backimg;
                    ViewBag.loginbackImg = FinalBackImg;

                    string FinalPath = "/Resources/Upload/LoginLogo/" + logoImagePath;
                    ViewBag.logoImagePath = FinalPath;
                    ObjtblLoginMgt = objContents.GetLoginPageContents(1);
                    ViewBag.HomeContents = ObjtblLoginMgt.Contents;
                    ViewBag.HomeColor = ObjtblLoginMgt.ModuleColor;
                    ViewBag.loadingbgColor = objtblLoginPageSetting.LoadingImgColor;

                    ObjtblLoginMgt = objContents.GetLoginPageContents(2);
                    ViewBag.NewsContents = ObjtblLoginMgt.Contents;
                    ViewBag.NewsColor = ObjtblLoginMgt.ModuleColor;

                    ObjtblLoginMgt = objContents.GetLoginPageContents(3);
                    ViewBag.ProductsContents = ObjtblLoginMgt.Contents;
                    ViewBag.ProductsColor = ObjtblLoginMgt.ModuleColor;

                    ObjtblLoginMgt = objContents.GetLoginPageContents(4);
                    ViewBag.FeaturesContents = ObjtblLoginMgt.Contents;
                    ViewBag.FeaturesColor = ObjtblLoginMgt.ModuleColor;

                    ObjtblLoginMgt = objContents.GetLoginPageContents(5);
                    ViewBag.ExploreContents = ObjtblLoginMgt.Contents;
                    ViewBag.ExploreColor = ObjtblLoginMgt.ModuleColor;

                    ObjtblLoginMgt = objContents.GetLoginPageContents(6);
                    ViewBag.ComplianceContents = ObjtblLoginMgt.Contents;
                    ViewBag.ComplianceColor = ObjtblLoginMgt.ModuleColor;

                    ObjtblLoginMgt = objContents.GetLoginPageContents(7);
                    ViewBag.FormsContents = ObjtblLoginMgt.Contents;
                    ViewBag.FormsColor = ObjtblLoginMgt.ModuleColor;

                    ObjtblLoginMgt = objContents.GetLoginPageContents(8);
                    ViewBag.PrivacyContents = ObjtblLoginMgt.Contents;
                    ViewBag.PrivacyColor = ObjtblLoginMgt.ModuleColor;

                    ObjtblLoginMgt = objContents.GetLoginPageContents(9);
                    ViewBag.ContactContents = ObjtblLoginMgt.Contents;
                    ViewBag.ContactColor = ObjtblLoginMgt.ModuleColor;

                    ObjtblLoginMgt = objContents.GetLoginPageContents(10);
                    ViewBag.CalenderContents = ObjtblLoginMgt.Contents;
                    ViewBag.CalenderColor = ObjtblLoginMgt.ModuleColor;


                    // end new login page



                }
            }
            catch { }

            return View(Model);
        }
        #endregion

        #region Login
        [HttpPost]
        public ActionResult Login(LoginModel Model)
        {
            return ProcessLogin(Model);
        }

        public ActionResult ProcessLogin(LoginModel Model)
        {
            MembershipUser ObjMembershipUser = Membership.GetUser(Model.UserName);
            if (ObjMembershipUser != null)
            {
                HttpContext.Session["LastLoginDate"] = ObjMembershipUser.LastLoginDate;
            }
            string ReturnUrl = "../Corporate/NewReport";
            string Status = string.Empty;
            string Css = string.Empty;
            string CurrentUserName = string.Empty;

            if (ObjMembershipUser != null)
            {
                Guid UserId = Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
                BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
                List<Proc_Get_UserInfoByMemberShipUserIdResult> UserInfoByMemberShip = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(UserId);


                ChatService.StartUserSession(System.Web.HttpContext.Current,
                   new MangoChat.User
                   {
                       // Unique user id by which MangoChat.net control will keep track of user
                       UserId = UserId.ToString(),
                       // User name to display in control
                       Username = UserInfoByMemberShip.ElementAt(0).FirstName,
                       // User avatar or display picture absolute path to be shown in control   
                       //   string refcode = ObjProc_Get_RequestAndResponseResult.First().SearchedTrackingRef == "" ? "NA" : ObjProc_Get_RequestAndResponseResult.First().SearchedTrackingRef;
                       ImageUrl = string.Empty
                   });

            }


            if (ObjMembershipUser != null)
            {
                if ((Roles.IsUserInRole(Model.UserName, "SystemAdmin")))//)!(Roles.IsUserInRole(UserName, "Admin") || 
                {
                    if (ObjMembershipUser.IsOnline)
                    {
                        // ScriptManager.RegisterStartupScript(this.Page, typeof(string), "IsLogged", "Alreadylogged();", true);
                        Status = "IsLogged";
                        Css = string.Empty;
                    }
                    else if (ObjMembershipUser.IsLockedOut)
                    {
                        //ScriptManager.RegisterStartupScript(this.Page, typeof(string), "IsLocked", "IsLocked();", true);
                        Status = "IsLocked";
                        Css = string.Empty;
                    }
                }
            }

            if (Membership.ValidateUser(Model.UserName, Model.Password))
            {
                int status = CheckAccountStatus(new Guid(Membership.GetUser(Model.UserName).ProviderUserKey.ToString()));
                BALChat objBALChat = new BALChat();
                CurrentUserName = objBALChat.GetUserNamebyId(Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString()));


                if (Convert.ToInt32(status) > 0)
                {
                    //FormsAuthentication.RedirectFromLoginPage(Model.UserName, false);

                    string[] multiple_Roles = Roles.GetRolesForUser(Model.UserName);

                    if (Model.RemeberMe)
                    {
                        Utility.SetCookie(Model.UserName.Trim(), Model.Password.Trim(), multiple_Roles[0], true);
                    }
                    else
                    {
                        Utility.SetCookie(Model.UserName.Trim(), Model.Password.Trim(), multiple_Roles[0], false);
                    }

                    #region UserRoles

                    string[] UserRole = Roles.GetRolesForUser(Model.UserName);
                    Session["UserRoles"] = UserRole;

                    #endregion

                    #region InsertDashBoard Colaps Status For New User

                    if (Roles.IsUserInRole(Model.UserName, "SystemAdmin"))//Roles.IsUserInRole(UserName, "Admin") || 
                    {

                    }
                    else
                    {
                        Guid CurrentUserId = Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
                        BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
                        ObjBALCompanyUsers.InsertDashBoardStatus(CurrentUserId);

                    }


                    #endregion


                    #region Role count check here
                    if (multiple_Roles.Contains("SalesAdmin"))//If multiple role exist or systemadmin loggeed on//
                    {
                        if (Roles.IsUserInRole(Model.UserName, "SalesAdmin"))//Roles.IsUserInRole(UserName, "Admin") || 
                        {
                            Session["LoggedUser"] = "SalesAdmin";
                            if (!string.IsNullOrEmpty(Request.QueryString["ReturnUrl"]))
                            {
                                HttpContext.Response.Redirect(Request.QueryString["ReturnUrl"].ToString(), true);
                            }
                            else
                            {
                                    ReturnUrl = "../Office/Leads";
                                    Status = "Success";
                                    Css = string.Empty;
                               
                            }
                        }
                    }

                    else
                    {
                        Session["LoggedUser"] = multiple_Roles[0];
                        if (Roles.IsUserInRole(Model.UserName, "SalesAdmin"))
                        {
                            ReturnUrl = "../Office/Leads";
                            Status = "Success";
                            Css = string.Empty;
                        }
                        else if (Roles.IsUserInRole(Model.UserName, "SalesManager"))
                        {
                            ReturnUrl = "../Office/Leads";
                            Status = "Success";
                            Css = string.Empty;
                        }
                        else if (Roles.IsUserInRole(Model.UserName, "SystemAdmin"))
                        {
                            ReturnUrl = "../Office/Leads";
                            Status = "Success";
                            Css = string.Empty;
                        }
                        else if (Roles.IsUserInRole(Model.UserName, "SalesRep"))
                        {
                            ReturnUrl = "../Office/Leads";
                            Status = "Success";
                            Css = string.Empty;
                        }
                    }
                    #endregion
                }
                else
                {
                    if (status == -2)
                    {
                        ReturnUrl = string.Empty;
                        Status = "IsCompanyDeactivated";
                        Css = "Error";
                    }
                    else if (status == -4)
                    {
                        ReturnUrl = string.Empty;
                        Status = "IsLocationDeactivated";
                        Css = "Error";
                    }
                    else if (status == -6)
                    {
                        ReturnUrl = string.Empty;
                        Status = "IsUserDeactivated";
                        Css = "Error";
                    }
                    else if (status == -1 || status == -3 || status == -5)
                    {
                        ReturnUrl = string.Empty;
                        Status = "InvalidUser";
                        Css = "Error";

                    }
                    BLLMembership ObjBLLMembership = new BLLMembership();
                    ObjBLLMembership.SignOut(Model.UserName, DateTime.Now.ToUniversalTime().AddMinutes(-(Membership.UserIsOnlineTimeWindow)));
                }
            }
            else
            {
                ClearCookie(Model.UserName.Trim(), Model.Password.Trim());
                ReturnUrl = string.Empty;
                Status = "InvalidUser";
                Css = "Error";
            }

            return Json(new { Sts = Status, css = Css, PageUrl = ReturnUrl, Retunusername = CurrentUserName });

        }

        public ActionResult RedirectToPage(string Email, string Page)
        {
            FormsAuthentication.RedirectFromLoginPage(Email, true);
            return RedirectToAction(Page);
        }
     
        #endregion

        #region Other Functions
        public int CheckAccountStatus(Guid UserId)
        {
            BLLMembership ObjBLLMembership = new BLLMembership();
            try
            {
                return ObjBLLMembership.CheckUserIsValid(UserId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBLLMembership = null;
            }
        }

        private void ClearCookie(string UserName, string UserPassword)
        {
            Utility.SetCookie(UserName, UserPassword, "", false);
            //txtUsername.Text = "";
            //txtPassword.Attributes.Add("value", "");
            //txtUsername.Focus();
            //chkRemeberMe.Checked = false;
        }
        #endregion
        public ActionResult LogOut(object sender, EventArgs e)
        {
            Session["Admin"] = null;
            Session.Abandon();
            //ChatControl.StopSession();
            //SaveUserProfile();
            ChatService.StopUserSession(System.Web.HttpContext.Current);
            //   ChatService.ChatConfig. null;

            MembershipUser ObjMembershipUser = Membership.GetUser(HttpContext.User.Identity.Name);
            System.Web.Security.FormsAuthentication.SignOut();

            if (ObjMembershipUser != null)
            {
                ProfileCommon ObjProfileCommon = new ProfileCommon();
                ObjProfileCommon.SaveUserProfile(HttpContext.User.Identity.Name);

                BALChat objBALChat = new BALChat();
                DateTime Dated = DateTime.Now;
                string ChatSessionId = Dated.ToString("yyMMddHHmmssfff");
                string UniquePublisherId = ObjMembershipUser.ProviderUserKey.ToString() + "-" + ChatSessionId;
                objBALChat.RemoveMangoChatWindows(ObjMembershipUser.ProviderUserKey.ToString(), UniquePublisherId);
                // Important Note: To Access Mango Chat User History Please UserId as Client Id With ClientId+"-logout" like  ClentID="b6e8cb8d-ac97-43e5-a1b6-47f0997665db-logout"

                BLLMembership ObjBLLMembership = new BLLMembership();
                ObjBLLMembership.SignOut(ObjMembershipUser.UserName, DateTime.Now.ToUniversalTime().AddMinutes(-(Membership.UserIsOnlineTimeWindow)));
            }
            return RedirectToAction("Default");
        }
    }
}
