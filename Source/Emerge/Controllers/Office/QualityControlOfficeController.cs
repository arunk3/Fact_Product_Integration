﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using System.Web.Security;

namespace Emerge.Controllers.Office
{
    public partial class OfficeController : Controller
    {
        //
        // GET: /QualityControlOffice/

        public ActionResult PartialQualityControlOffice()
        {
            return View();
        }

        public ActionResult QualityOfficeBind(int page, int pageSize)
        {

            BALCompany ObjBALCompany = new BALCompany();
            int PageSize = pageSize;
            int iPageNo = page;
          

             var QualityOfficeCollection = ObjBALCompany.GetClientQualityControlForOffice(
                                                        iPageNo,
                                                        PageSize,
                                                        true,
                                                        string.Empty,
                                                        -1,
                                                        -1,
                                                        "CreatedDate",
                                                        "DESC", string.Empty, string.Empty,
                                                        Guid.Empty,
                                                        Guid.Empty,
                                                         new Guid(Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString()),
                                                        string.Empty);

           

            return Json(new
            {
                Products = QualityOfficeCollection,
                TotalCount = QualityOfficeCollection.Count > 0 ? QualityOfficeCollection.ElementAt(0).TotalRec : 0
            },
                           JsonRequestBehavior.AllowGet);

        }











    }
}

