﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Emerge.Common;
using Emerge.Models;
using Emerge.Data;
using System.Web.Security;
using System.IO;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Text;

namespace Emerge.Controllers.Office
{
    public partial class OfficeController : Controller
    {
        // Page Load Method
        public ActionResult Alerts()
        {

            return View();
        }

        // Grid Bind Method
        public ActionResult GetAllAlerts(int page, int pageSize)
        {
             MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));
            Guid Userid=  new Guid(ObjMembershipUser.ProviderUserKey.ToString());
            BALOffice  objbalOffice= new BALOffice();
            var list = objbalOffice.GetAlertsForOfficeUser(Userid, page, pageSize, "CreatedDate", "DESC").ToList();

            return Json(new { Products = list});
        }

    }
}
