﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Data;
using Emerge.Services;
using Emerge.Models;
using System.Web.Security;
using Emerge.Common;

namespace Emerge.Controllers.Office
{
    public partial class OfficeController : Controller
    {

        #region // Page load Method
        public ActionResult MyClients()
        {

            return View();
        }
        #endregion

        #region // Grid Bind Method
        public ActionResult GetClientsByCurrentUserId(int page, int pageSize, List<GridSort> sort)
        {

            try
            {
                BALLeads ObjBALLeads = new BALLeads();
                Guid UserId = Utility.GetUID(User.Identity.Name);

                var Managerid = Guid.Empty;
                Guid saleid = Guid.Empty;
                string Searchkeyword = "";
                string copanytype = "-1";
                string LeadSource = string.Empty;
                string Direction = "Desc";
                string ColumnName = "CreatedDate";
                if (sort != null)
                {
                    Direction = sort[0].Dir;
                    ColumnName = sort[0].Field;
                }

                var Myckientlist = ObjBALLeads.GetClientsByCurrentUserId(page, pageSize, true, Searchkeyword, short.Parse(copanytype), "-1", ColumnName, Direction, Managerid, saleid, UserId, LeadSource);
                if (Myckientlist.Count > 0)
                {
                    return Json(new { Reclist = Myckientlist, TotalRec = Myckientlist.ElementAt(0).TotalRec });
                }
                else
                {
                    return Json(new { Reclist = 0, TotalRec = 0 });

                }
            }
            catch
            {
                return Json(new { Reclist = 0, TotalRec = 0 });
            }
        }
        #endregion
    }
}
