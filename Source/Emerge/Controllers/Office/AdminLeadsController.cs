﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Data;
using Emerge.Services;
using Emerge.Models;
using System.Web.Security;
using Emerge.Common;

namespace Emerge.Controllers.Office
{
    public partial class OfficeController : Controller
    {
       
        //Page Load Method 
        public ActionResult AdminLeads()
        {
            return View();
        }
        // Grid Bind Method
        public ActionResult GetAllAdminLeads(int page, int pageSize, List<GridSort> sort, List<GridFilter> filters)
        {


            BALLeads ObjBALLeads = new BALLeads();
            Guid UserId = Utility.GetUID(User.Identity.Name);
            byte LeadType = 0;
            var Managerid = Guid.Empty;
            Guid saleid = Guid.Empty;
            string Searchkeyword = "";
            string copanytype = "-1";
            string Direction = "Desc";
            string ColumnName = "CreatedDate";
            if (sort != null)
            {
                Direction = sort[0].Dir;
                ColumnName = sort[0].Field;
            }
            byte LeadStatus = 0;
            string LeadSource = string.Empty;
           
            string ToDate = "";
            string FromDate = "";
            byte FilterAction = 2; // Set FilterAction =1 if filter from coloumn from
            string FilterColumn = string.Empty;
            string FilterValue = string.Empty;

            string LeadNextStep = string.Empty;
            string SalesAssosiate = string.Empty;

            if (filters != null)
            {
               
                    FilterAction = 1;
                    FilterColumn = filters[0].Field;
                    FilterValue = filters[0].Value;
                
            }

            var ObjDbCollection = ObjBALLeads.GetAllLeads(page, pageSize, true, Searchkeyword, short.Parse(copanytype), -1, ColumnName, Direction, FromDate, ToDate, LeadStatus, Managerid, saleid, UserId, LeadSource, LeadType, FilterColumn, FilterAction, FilterValue,LeadNextStep, SalesAssosiate);
            if (ObjDbCollection.Count > 0)
            {
                return Json(new { Products = ObjDbCollection, TotalCount = ObjDbCollection.ElementAt(0).TotalRec });
            }
            else
            {
                return Json(new { Products = ObjDbCollection, TotalCount = 0 });
            }

        }

    }
}