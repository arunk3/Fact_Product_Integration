﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using System.Web.Security;
using System.Text;
using System.IO;
using System.Configuration;

namespace Emerge.Controllers.Office
{
    public partial class OfficeController : Controller
    {
        //
        // GET: /Leads/

        public ActionResult Leads()
        {
            Guid UserId = Utility.GetUID(User.Identity.Name);
            BALLeads ObjBALLeads = new BALLeads();
            BALCompanyType ObjBALCompanyType = new BALCompanyType();
            BALSalesRep ObjBALSalesRep = new BALSalesRep();
            List<SelectListItem> listleadSrc = new List<SelectListItem>();
            MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));
            ViewBag.PwdToExportLeadCsv = ConfigurationManager.AppSettings["PwdToExportCSV"].ToString();
            //List<Proc_GetDistinctLeadSourceResult> leadSrc = ObjBALLeads.GetLeadSource();
            //if (leadSrc.Count > 0)
            //{
            //    listleadSrc = leadSrc.Select(x => new SelectListItem { Text = x.LeadSource, Value = x.LeadSource }).ToList();
            //}
            //listleadSrc.Insert(0, new SelectListItem { Text = "--All--", Value = string.Empty });

            //lead Source
            listleadSrc.Add(new SelectListItem { Text = "Outbound Call", Value = "1" });
            listleadSrc.Add(new SelectListItem { Text = "Inbound Call", Value = "2" });
            listleadSrc.Add(new SelectListItem { Text = "Inbound Email", Value = "3" });
            listleadSrc.Add(new SelectListItem { Text = "Live Transfer", Value = "4" });
            listleadSrc.Add(new SelectListItem { Text = "Referral", Value = "5" });
            listleadSrc.Add(new SelectListItem { Text = "Tradeshow", Value = "6" });
            listleadSrc.Add(new SelectListItem { Text = "Friend or Family", Value = "7" });
            listleadSrc.Insert(0, new SelectListItem { Text = "--All--", Value = string.Empty });
            ViewData["LeadSource"] = listleadSrc;
            //  lead Source


            ViewData["LeadSource"] = listleadSrc;

            List<SelectListItem> listCompanyType = new List<SelectListItem>();
            var ObjCompanyType = ObjBALCompanyType.GetAllCompanyType();
            if (ObjCompanyType.Count > 0)
            {
                listCompanyType = ObjCompanyType.Select(x => new SelectListItem { Text = x.CompanyType, Value = x.pkCompanyTypeId.ToString() }).ToList();
            }
            listCompanyType.Insert(0, new SelectListItem { Text = "--All--", Value = "-1" });
            ViewData["CompanyTypes"] = listCompanyType;
            ViewBag.PageSize = 100;
            ViewBag.PageNo = 1;
            //ViewBag.CompanyType = -1;
            List<SelectListItem> listsalesRep = new List<SelectListItem>();
            Guid fkManagerId = Guid.Empty;
            if (UserId == Guid.Empty)
            {
                fkManagerId = Guid.Empty;
            }
            else
            {
                fkManagerId = UserId;
            }

            List<Proc_Get_SalesRepByManagerIdResult> salesRep = ObjBALSalesRep.GetSalesRepByManagerId(fkManagerId);
            if (salesRep.Count > 0)
            {
                listsalesRep = salesRep.Select(x => new SelectListItem { Text = x.FullName, Value = x.fkUserId.ToString() }).ToList();
            }
            listsalesRep.Insert(0, new SelectListItem { Text = "--All--", Value = Guid.Empty.ToString() });
            ViewData["SalesRep"] = listsalesRep;

            List<SelectListItem> listSalesMgr = new List<SelectListItem>();


            if (Roles.IsUserInRole(User.Identity.Name, "SalesManager"))
            {
                Guid UserId1 = Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
                List<Proc_GetMangerlistbyMangerIdResult> salesManger = ObjBALLeads.GetMangerListByMangerId(UserId1);

                if (salesManger.Count > 0)
                {
                    listSalesMgr = salesManger.Select(x => new SelectListItem { Text = x.FullName, Value = x.fkUserId.ToString() }).ToList();
                }
                listSalesMgr.Insert(0, new SelectListItem { Text = listSalesMgr.ElementAt(0).Text, Value = listSalesMgr.ElementAt(0).Value });
                ViewData["Salesmanager"] = listSalesMgr;

            }
            else
            {
                string RoleName = "salesmanager";
                List<Proc_Get_SalesManagersResult> salesMgr = ObjBALSalesRep.GetSalesManagers(RoleName);
                if (salesMgr.Count > 0)
                {
                    listSalesMgr = salesMgr.Select(x => new SelectListItem { Text = x.FullName, Value = x.fkUserId.ToString() }).ToList();
                }
                listSalesMgr.Insert(0, new SelectListItem { Text = "--All--", Value = Guid.Empty.ToString() });
                ViewData["Salesmanager"] = listSalesMgr;
            }


            #region Sales Assosiate data
            List<SelectListItem> listSalesAssosiate = new List<SelectListItem>();

            Guid UserIdInfor = Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
            List<Proc_LeadSalesRepNameDropDownvalueResult> listlistSalesRepData = ObjBALLeads.GetSalesRepdataId(UserIdInfor);

            if (listlistSalesRepData.Count > 0)
            {
                listSalesAssosiate = listlistSalesRepData.Select(x => new SelectListItem { Text = x.SalesRepName, Value = x.SalesRepName.ToString() }).ToList();
            }
            listSalesAssosiate.Insert(0, new SelectListItem { Text = "--All--", Value = "-1" });
            ViewData["listSalesAssosiate"] = listSalesAssosiate;



            #endregion



            #region Next step data

            List<SelectListItem> listNextStep = new List<SelectListItem>();

            List<Proc_LeadNextStepDropDownvalueResult> listNextStepData = ObjBALLeads.GetNextStepdataId(UserIdInfor);

            if (listNextStepData.Count > 0)
            {
                listNextStep = listNextStepData.Select(x => new SelectListItem { Text = x.NextStep, Value = x.NextStep.ToString() }).ToList();
            }
            listNextStep.Insert(0, new SelectListItem { Text = "--All--", Value = "-1" });
            ViewData["LeadNextStep"] = listNextStep;


            #endregion












            List<SelectListItem> listLeadStatus = new List<SelectListItem>();
            listLeadStatus.Add(new SelectListItem { Text = "--All--", Value = "0", Selected = false });
            listLeadStatus.Add(new SelectListItem { Text = "File Started", Value = "1", Selected = false });
            listLeadStatus.Add(new SelectListItem { Text = "Proposal Out", Value = "2", Selected = false });
            listLeadStatus.Add(new SelectListItem { Text = "Agreement Out", Value = "3", Selected = false });
            listLeadStatus.Add(new SelectListItem { Text = "Agreement In", Value = "4", Selected = false });
            ViewData["LeadStatusListData"] = listLeadStatus;
            ViewBag.HiddenLeadType = 7;

            List<SelectListItem> leadDateList = new List<SelectListItem>();
            leadDateList.Add(new SelectListItem { Text = "--All--", Value = "0", Selected = false });
            leadDateList.Add(new SelectListItem { Text = "Today", Value = "1", Selected = false });
            leadDateList.Add(new SelectListItem { Text = "This Month", Value = "2", Selected = false });
            leadDateList.Add(new SelectListItem { Text = "Last Month", Value = "3", Selected = false });
            leadDateList.Add(new SelectListItem { Text = "Specific Month", Value = "4", Selected = false });
            leadDateList.Add(new SelectListItem { Text = "Specific Date", Value = "5", Selected = false });
            leadDateList.Add(new SelectListItem { Text = "Specific Date Range", Value = "6", Selected = false });
            ViewData["leadDateList"] = leadDateList;

            #region Count Leads
            Guid SalesManagerId = Guid.Empty;
            Guid SalesRepId = Guid.Empty;

            if (Roles.IsUserInRole(User.Identity.Name, "SalesManager"))
            {
                Guid UserId1 = Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
                SalesManagerId = UserId1;
            }

            if (Roles.IsUserInRole(User.Identity.Name, "SalesRep"))
            {
                Guid UserId1 = Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
                SalesRepId = UserId1;
            }
            List<tblCompany> ObjDbCollection = new List<tblCompany>();
            ObjDbCollection = ObjBALLeads.CountLeadType(SalesManagerId, SalesRepId);
            int s = 0;

            for (int i = 0; i < ObjDbCollection.Count(); i++)
            {
                if (ObjDbCollection.ElementAt(i).LeadType != 4 && ObjDbCollection.ElementAt(i).LeadType != 10)
                {
                    //if (ObjDbCollection.ElementAt(i).LeadType== 0)
                    {
                        if (ObjDbCollection.ElementAt(i).LeadStatus == 1 || ObjDbCollection.ElementAt(i).LeadStatus == 2 || ObjDbCollection.ElementAt(i).LeadStatus == 3 || ObjDbCollection.ElementAt(i).LeadStatus == 4)
                        {
                            s++;
                        }
                    }
                }


            }
            string ActiveLeads = "(" + Convert.ToString(s) + ")";
            ViewBag.ActiveLeads = ActiveLeads;

            //INT107 (As per Client comment, In all lead suppose to be show dead lead also.
            //Emerge Office folders are not configured correctly.
            //string alllead = "(" + Convert.ToString(ObjDbCollection.Count(dx => dx.LeadType != 4)) + ")";
            string alllead = "(" + Convert.ToString(ObjDbCollection.Count()) + ")";
            ViewBag.AllLead = alllead;
            //    ViewBag.NewLead = "(" + Convert.ToString(ObjDbCollection.Count(dx => dx.LeadType == 1)) + ")";
            string newlead = "(" + Convert.ToString(ObjDbCollection.Count(dx => dx.LeadType == 1)) + ")";
            ViewBag.NewLead = newlead;
            string hotlead = "(" + Convert.ToString(ObjDbCollection.Count(dx => dx.LeadType == 2)) + ")";
            ViewBag.HotLead = hotlead;
            string coldlead = "(" + Convert.ToString(ObjDbCollection.Count(dx => dx.LeadType == 3)) + ")";
            ViewBag.ColdLead = coldlead;
            string dedelead = "(" + Convert.ToString(ObjDbCollection.Count(dx => dx.LeadType == 4)) + ")";
            ViewBag.DeadLead = dedelead;

            /***********************New Folders Added Ticket 128 :Add three folders to Emerge Office menu******************************/

            //for Pending Demos folder
            string pendingDemos = "(" + Convert.ToString(ObjDbCollection.Count(dx => dx.LeadType == 8)) + ")";
            ViewBag.pendingDemos = pendingDemos;

            //for Pending Agreements folder
            string pendingAgreements = "(" + Convert.ToString(ObjDbCollection.Count(dx => dx.LeadType == 9)) + ")";
            ViewBag.pendingAgreements = pendingAgreements;

            //for Deep Freeze folder
            string deepFreeze = "(" + Convert.ToString(ObjDbCollection.Count(dx => dx.LeadType == 10)) + ")";
            ViewBag.deepFreeze = deepFreeze;



            /*********************************************End*************************************************************************/
            #endregion

            #region Recent Review Lead
            // Recent Review Lead
            ClientsModel objclientModel = new ClientsModel();
            BALOffice objbal = new BALOffice();


            if (Roles.IsUserInRole(User.Identity.Name, "SystemAdmin"))
            {
                objclientModel.IsAdmin = true;
            }


            objclientModel.RecentReviewLeadList = objbal.GetRecentReviewLeads(UserId);


            if (Request.QueryString["SearchQry"] != null)
            {
                ViewBag.CompanyName = Request.QueryString["SearchQry"].ToString();
                Session["CompanyName2"] = Request.QueryString["SearchQry"].ToString();
                objclientModel.txtOpenSearch = Request.QueryString["SearchQry"].ToString();

            }
            #endregion


            #region Get Columns from Profile

            try
            {
                MembershipUser Member = Membership.GetUser(User.Identity.Name);
                if (Member != null)
                {
                    string FilterValues = "";
                    ProfileCommon Profile = new ProfileCommon();
                    ProfileModel userProfile = Profile.GetProfile(Member.Email);

                    if (Request.QueryString["_bck"] != null)
                    {

                        ViewBag.PageSize = userProfile.LeadPageSize;
                        ViewBag.PageNo = userProfile.LeadPageNo;
                        objclientModel.txtOpenSearch = userProfile.LeadSearchedText;
                        objclientModel.txtSpecificFrom = userProfile.LeadFromDate;
                        objclientModel.txtSpecificTo = userProfile.LeadToDate;
                        if (ObjCompanyType.Count > 0)
                        {
                            listCompanyType = ObjCompanyType.Select(x => new SelectListItem { Text = x.CompanyType, Value = x.pkCompanyTypeId.ToString(), Selected = false }).ToList();
                        }
                        listCompanyType.Insert(0, new SelectListItem { Text = "--All--", Value = "-1", Selected = false });
                        listCompanyType.FirstOrDefault(n => n.Value == userProfile.LeadCompanyType).Selected = true;
                        ViewData["CompanyTypes"] = listCompanyType;

                        if (Roles.IsUserInRole(User.Identity.Name, "SalesManager"))
                        {
                            Guid UserId1 = Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
                            List<Proc_GetMangerlistbyMangerIdResult> salesManger = ObjBALLeads.GetMangerListByMangerId(UserId1);

                            if (salesManger.Count > 0)
                            {
                                listSalesMgr = salesManger.Select(x => new SelectListItem { Text = x.FullName, Value = x.fkUserId.ToString(), Selected = false }).ToList();
                            }
                            // listSalesMgr.Insert(0, new SelectListItem { Text = listSalesMgr.ElementAt(0).Text, Value = listSalesMgr.ElementAt(0).Value, Selected = false });
                            // ViewData["Salesmanager"] = listSalesMgr;

                        }
                        else
                        {
                            string RoleName = "salesmanager";
                            List<Proc_Get_SalesManagersResult> salesMgr = ObjBALSalesRep.GetSalesManagers(RoleName);
                            if (salesMgr.Count > 0)
                            {
                                listSalesMgr = salesMgr.Select(x => new SelectListItem { Text = x.FullName, Value = x.fkUserId.ToString(), Selected = false }).ToList();
                            }
                            // listSalesMgr.Insert(0, new SelectListItem { Text = "--All--", Value = Guid.Empty.ToString(), Selected = false });
                            //ViewData["Salesmanager"] = listSalesMgr;
                        }
                        listSalesMgr.Insert(0, new SelectListItem { Text = "--All--", Value = Guid.Empty.ToString(), Selected = false });
                        listSalesMgr.FirstOrDefault(n => n.Value == userProfile.LeadSalesManager).Selected = true;
                        ViewData["Salesmanager"] = listSalesMgr;
                        leadDateList.FirstOrDefault(n => n.Value == userProfile.LeadDateList).Selected = true;
                        ViewData["leadDateList"] = leadDateList;

                        //if (leadSrc.Count > 0)
                        //{
                        //    listleadSrc = leadSrc.Select(x => new SelectListItem { Text = x.LeadSource, Value = x.LeadSource, Selected = false }).ToList();
                        //}
                        //listleadSrc.Insert(0, new SelectListItem { Text = "--All--", Value = string.Empty, Selected = false });
                        //listleadSrc.FirstOrDefault(n => n.Value == userProfile.LeadSource).Selected = true;
                        //ViewData["LeadSource"] = listleadSrc;


                        //lead Source
                        listleadSrc = new List<SelectListItem>();
                        listleadSrc.Add(new SelectListItem { Text = "Outbound Call", Value = "1", Selected = false });
                        listleadSrc.Add(new SelectListItem { Text = "Inbound Call", Value = "2", Selected = false });
                        listleadSrc.Add(new SelectListItem { Text = "Inbound Email", Value = "3", Selected = false });
                        listleadSrc.Add(new SelectListItem { Text = "Live Transfer", Value = "4", Selected = false });
                        listleadSrc.Add(new SelectListItem { Text = "Referral", Value = "5", Selected = false });
                        listleadSrc.Add(new SelectListItem { Text = "Tradeshow", Value = "6", Selected = false });
                        listleadSrc.Add(new SelectListItem { Text = "Friend or Family", Value = "7", Selected = false });
                        listleadSrc.Insert(0, new SelectListItem { Text = "--All--", Value = string.Empty, Selected = false });
                        listleadSrc.FirstOrDefault(n => n.Value == userProfile.LeadSource).Selected = true;
                        ViewData["LeadSource"] = listleadSrc;
                        listLeadStatus.FirstOrDefault(n => n.Value == userProfile.LeadStatus).Selected = true;
                        ViewData["LeadStatusListData"] = listLeadStatus;
                        //lead Source



                        listLeadStatus.FirstOrDefault(n => n.Value == userProfile.LeadStatus).Selected = true;
                        ViewData["LeadStatusListData"] = listLeadStatus;

                        ViewBag.HiddenLeadType = Convert.ToInt16(userProfile.LeadType);

                    }
                    ViewBag.FilterValues = FilterValues;
                }
            }
            catch (Exception)
            {
            }
            #endregion

            return View(objclientModel);
        }

        [HttpPost]
        public ActionResult GetLeads(int page, int pageSize, string group, List<GridSort> sort, List<GridFilter> filters, string txtOpenSearch, string companyType,
            Guid? SalesRepId1, Guid? SalesManagerId1, string LeadSource, byte LeadStatus, byte LeadType, int? pkOrderDateId, string ToDate, string FromDate,
            string LeadNextStep, string SalesAssosiate

            )
        {

            Session["extrainfo"] = filters; // int 400 Lead issue for Export leads 

            BALLeads ObjBALLeads = new BALLeads();
            // string ToDate = "";
            //string FromDate = "";
            //byte LeadType = 0;
            int iTotalRecords = 0;
            int iPageNo = page;
            int PageSize = pageSize;
            string ColumnName = "CreatedDate";
            string Direction = "DESC";
            byte FilterAction = 2; // Set FilterAction =1 if filter from coloumn from
            string FilterColumn = string.Empty;
            string FilterValue = string.Empty;

            #region Get Columns from Profile

            MembershipUser Member = Membership.GetUser(User.Identity.Name);
            //string[] UserRole = Roles.GetRolesForUser(Member.Email);

            if (Member != null)
            {
                ProfileCommon Profile = new ProfileCommon();
                ProfileModel userProfile = Profile.GetProfile(Member.Email);
                if (userProfile.LeadSortColumn != string.Empty)
                {
                    ColumnName = userProfile.LeadSortColumn;

                    if (ColumnName == "CompanyName" || ColumnName == "volumn" || ColumnName == "State" || ColumnName == "MainContact" || ColumnName == "email" || ColumnName == "companyType" || ColumnName == "leadSource" || ColumnName == "manager" || ColumnName == "Associate")
                    {
                        Direction = "ASC";
                    }
                    else
                    {
                        Direction = "DESC";
                    }

                }
                if (TempData["LeadPageSize"] != null)
                {
                    pageSize = Convert.ToInt32(TempData["LeadPageSize"]);
                }
                if (TempData["LeadPageNo"] != null)
                {
                    page = Convert.ToInt32(TempData["LeadPageNo"]);
                }
            }

            #endregion

            #region Sorting



            if (sort != null)
            {
                Direction = sort[0].Dir;
                ColumnName = sort[0].Field;
            }

            #endregion

            #region Filtering

            //Guid pkProductApplicationId = Guid.Empty;
            //Guid CompanyUserId = Guid.Empty;
            Guid CurrentUserId = new Guid(Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString());
            Guid SalesRepId = SalesRepId1.Value; ;
            MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));
            Guid SalesManagerId = SalesManagerId1.Value;

            if (Roles.IsUserInRole(User.Identity.Name, "SalesManager"))
            {
                Guid UserId1 = Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
                SalesManagerId = UserId1;
            }

            if (Roles.IsUserInRole(User.Identity.Name, "SalesRep"))
            {
                Guid UserId1 = Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
                SalesRepId = UserId1;
            }
            string Keyword = txtOpenSearch.ToString();
            //string companyType = "-1";
            //string LeadSource = string.Empty;
            //byte LeadStatus = 0;
            //string OrderDate = string.Empty;
            if (Session["CompanyName2"] != null)
            {
                Keyword = Session["CompanyName2"].ToString();
                Session["CompanyName2"] = null;
            }
            //string ToDate = "";
            //string FromDate = "";
            //string check = string.Empty;
            if (filters != null)
            {
                if (filters.Count() == 1)
                {
                    FilterAction = 1;
                    FilterColumn = filters[0].Field;
                    FilterValue = filters[0].Value;
                }
                else
                {


                    pkOrderDateId = Convert.ToInt32(filters[0].Value);
                    if (pkOrderDateId != -5)
                    {

                        int Month = Convert.ToInt32(filters[5].Value);
                        int Year = Convert.ToInt32(filters[6].Value);
                        LeadStatus = Convert.ToByte(filters[2].Value);
                        LeadSource = filters[7].Value;


                        LeadStatus = Convert.ToByte(filters[2].Value);

                        LeadType = Convert.ToByte(filters[12].Value);
                        LeadNextStep = Convert.ToString(filters[13].Value);
                        SalesAssosiate = Convert.ToString(filters[14].Value);

                        if (LeadNextStep == null)
                        {
                            LeadNextStep = "";
                        }

                        if (SalesAssosiate == null)
                        {
                            SalesAssosiate = "";
                        }



                        if (filters[1].Value == null)
                        {
                            companyType = "-1";
                        }
                        else
                        {
                            companyType = filters[1].Value.ToString();
                        }

                        if (filters[3].Value == null)
                        {
                            SalesManagerId = Guid.Empty;

                        }
                        else
                        {
                            SalesManagerId = Guid.Parse(filters[3].Value);
                        }
                        if (filters[4].Value == null)
                        {
                            Keyword = "";
                        }
                        else
                        {
                            Keyword = filters[4].Value;
                        }

                        DateTime today = DateTime.Today;
                        if (pkOrderDateId == 1)
                        {
                            ToDate = Convert.ToString(today.ToShortDateString());
                            FromDate = Convert.ToString(today.ToShortDateString());
                        }
                        else if (pkOrderDateId == 2)
                        {
                            FromDate = new DateTime(today.Year, today.Month, 1).ToShortDateString();
                            ToDate = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month)).ToShortDateString();
                        }
                        else if (pkOrderDateId == 3)
                        {
                            FromDate = new DateTime(today.Year, today.Month, 1).AddMonths(-1).ToShortDateString();
                            ToDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(-1).ToShortDateString();
                        }
                        else if (pkOrderDateId == 4)
                        {
                            try
                            {
                                FromDate = new DateTime(Year, Month, 1).ToShortDateString();
                                ToDate = new DateTime(Year, Month, DateTime.DaysInMonth(Year, Month)).ToShortDateString();
                            }
                            catch
                            {

                            }

                        }
                        else if (pkOrderDateId == 5)
                        {
                            FromDate = filters[9].Value;
                            ToDate = filters[9].Value;
                        }
                        else if (pkOrderDateId == 6)
                        {
                            FromDate = filters[10].Value;
                            ToDate = filters[11].Value;
                        }



                    }
                }
                //else
                //{
                //    Keyword = string.Empty;
                //}

            }

            //if (txtOpenSearch != string.Empty || txtOpenSearch != "")
            //{
            //    Keyword = txtOpenSearch;
            //}
            #endregion


            #region Save Profile

            if (Member != null)
            {
                try
                {
                    ProfileCommon Profile = new ProfileCommon();
                    ProfileModel userProfile = Profile.GetProfile(Member.Email);
                    Profile.SetGeneralProfile(Member.Email, "LeadSearchedText", Keyword);
                    Profile.SetGeneralProfile(Member.Email, "LeadStatus", LeadStatus.ToString());
                    Profile.SetGeneralProfile(Member.Email, "LeadSource", LeadSource);

                    //Profile.SetGeneralProfile(Member.Email, "LeadNextStep", LeadNextStep);
                    // Profile.SetGeneralProfile(Member.Email, "SalesAssosiate", SalesAssosiate);



                    Profile.SetGeneralProfile(Member.Email, "LeadCompanyType", companyType);
                    Profile.SetGeneralProfile(Member.Email, "LeadSalesManager", SalesManagerId.ToString());
                    Profile.SetGeneralProfile(Member.Email, "LeadSortColumn", ColumnName);
                    Profile.SetGeneralProfile(Member.Email, "LeadSortOrder", Direction);
                    Profile.SetGeneralProfile(Member.Email, "LeadPageNo", iPageNo.ToString());
                    Profile.SetGeneralProfile(Member.Email, "LeadPageSize", PageSize.ToString());
                    Profile.SetGeneralProfile(Member.Email, "LeadToDate", ToDate.ToString());
                    Profile.SetGeneralProfile(Member.Email, "LeadFromDate", FromDate.ToString());
                    Profile.SetGeneralProfile(Member.Email, "LeadSalesRep", SalesRepId.ToString());
                    Profile.SetGeneralProfile(Member.Email, "LeadCurrentUser", CurrentUserId.ToString());
                    Profile.SetGeneralProfile(Member.Email, "LeadType", LeadType.ToString());
                    Profile.SetGeneralProfile(Member.Email, "LeadDateList", pkOrderDateId.ToString());

                }
                catch (Exception)
                {
                }
            }

            #endregion


            var ObjDbCollection = ObjBALLeads.GetAllLeads(
                iPageNo,
                PageSize,
                true,
                Keyword,
                short.Parse(companyType),
                -1,
                ColumnName,
                Direction, FromDate, ToDate,
                LeadStatus,
                SalesManagerId,
                SalesRepId,
                CurrentUserId,
                LeadSource,
               LeadType, FilterColumn, FilterAction, FilterValue,
               LeadNextStep,
               SalesAssosiate

                );

            Session["TotalLeadInfo"] = ObjDbCollection.Count == 0 ? 0 : ObjDbCollection.ElementAt(0).TotalRec;

            if (ObjDbCollection.Count > 0)
            {
                iTotalRecords = int.Parse(ObjDbCollection.ElementAt(0).TotalRec.ToString());
            }

            List<LeadForwardBackwardInfo> objLeadForwardBackwardInfo = new List<LeadForwardBackwardInfo>();
            if (ObjDbCollection.Count > 0)
            {
                var LeadLists = new List<Proc_Get_AllLeadsResult>();
                if (PageSize < ObjDbCollection.ElementAt(0).TotalRec.Value)
                {
                    LeadLists = ObjBALLeads.GetAllLeads(
                        1,
                        ObjDbCollection.ElementAt(0).TotalRec.Value,
                        false,
                        Keyword,
                        short.Parse(companyType),
                        -1,
                        ColumnName,
                        Direction, FromDate, ToDate,
                        LeadStatus,
                        SalesManagerId,
                        SalesRepId,
                        CurrentUserId,
                        LeadSource,
                       LeadType, FilterColumn, FilterAction, FilterValue,
                       LeadNextStep,
               SalesAssosiate
                        );
                }
                else
                {
                    LeadLists = ObjDbCollection;
                }
                int i = 1;
                LeadLists.ForEach(n =>
                {
                    LeadForwardBackwardInfo obj = new LeadForwardBackwardInfo();
                    obj.Id = n.pkLocationId.Value;
                    obj.RowIndex = i;
                    obj.TotalCount = LeadLists.Count;
                    objLeadForwardBackwardInfo.Add(obj);
                    i++;
                });
            }

            Session["LeadForwardBackwardInfo"] = objLeadForwardBackwardInfo;
            return Json(new { Products = ObjDbCollection, TotalCount = iTotalRecords });
        }

        [HttpPost]
        public ActionResult DeleteLeads(IEnumerable<tblCompany> records)
        {
            return View();
        }

        public FileResult ExportLeadInfotoCSV()
        {
            List<GridFilter> filters = (List<GridFilter>)Session["extrainfo"];  // int 400

            MemoryStream output = new MemoryStream();
            string LogFilename = "ExportLeadInfotoCSV_" + DateTime.Now.ToString("yyMMdd");
            string strLog = string.Empty;//For Report Log.

            try
            {

                BALGeneral.WriteLogForDeletion(DateTime.Now.ToString() + " Method Started : ExportLeadInfotoCSV ", LogFilename);

                BALLeads ObjBALLeads = new BALLeads();
                string ToDate = "";
                string FromDate = "";
                byte LeadType = 7;
                int iPageNo = 1;
                int PageSize = Convert.ToInt32(Session["TotalLeadInfo"]);
                #region Sorting
                string ColumnName = "CreatedDate";
                string Direction = "DESC";
                #endregion
                //int pkProductApplicationId = 0;
                //int CompanyUserId = 0;
                Guid CurrentUserId = new Guid(Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString());
                Guid SalesRepId = Guid.Empty;
                MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));
                Guid SalesManagerId = Guid.Empty;

                if (Roles.IsUserInRole(User.Identity.Name, "SalesManager"))
                {
                    Guid UserId1 = Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
                    SalesManagerId = UserId1;
                }

                if (Roles.IsUserInRole(User.Identity.Name, "SalesRep"))
                {
                    Guid UserId1 = Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
                    SalesRepId = UserId1;
                }
                string Keyword = string.Empty;
                string companyType = "-1";
                string LeadSource = string.Empty;
                byte LeadStatus = 0;
                //string OrderDate = string.Empty;
                byte FilterAction = 2; // Set FilterAction =1 if filter from coloumn from
                string FilterColumn = string.Empty;
                string FilterValue = string.Empty;
                string LeadNextStep = "-1";
                string SalesAssosiate = "-1";

                //Filter Process
                if (filters != null)
                {
                    if (filters.Count() == 1)
                    {
                        FilterAction = 1;
                        FilterColumn = filters[0].Field;
                        FilterValue = filters[0].Value;
                    }
                    else
                    {
                        int pkOrderDateId = Convert.ToInt32(filters[0].Value);
                        if (pkOrderDateId != -5)
                        {

                            int Month = Convert.ToInt32(filters[5].Value);
                            int Year = Convert.ToInt32(filters[6].Value);
                            LeadStatus = Convert.ToByte(filters[2].Value);
                            LeadSource = filters[7].Value;


                            LeadStatus = Convert.ToByte(filters[2].Value);
                            //1
                            LeadType = Convert.ToByte(filters[12].Value);
                            LeadNextStep = Convert.ToString(filters[13].Value);
                            SalesAssosiate = Convert.ToString(filters[14].Value);

                            if (LeadNextStep == null)
                            {
                                LeadNextStep = "";
                            }

                            if (SalesAssosiate == null)
                            {
                                SalesAssosiate = "";
                            }



                            if (filters[1].Value == null)
                            {
                                companyType = "-1";
                            }
                            else
                            {
                                companyType = filters[1].Value.ToString();
                            }

                            if (filters[3].Value == null)
                            {
                                SalesManagerId = Guid.Empty;

                            }
                            else
                            {
                                SalesManagerId = Guid.Parse(filters[3].Value);
                            }
                            if (filters[4].Value == null)
                            {
                                Keyword = "";
                            }
                            else
                            {
                                Keyword = filters[4].Value;
                            }

                            DateTime today = DateTime.Today;
                            if (pkOrderDateId == 1)
                            {
                                ToDate = Convert.ToString(today.ToShortDateString());
                                FromDate = Convert.ToString(today.ToShortDateString());
                            }
                            else if (pkOrderDateId == 2)
                            {
                                FromDate = new DateTime(today.Year, today.Month, 1).ToShortDateString();
                                ToDate = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month)).ToShortDateString();
                            }
                            else if (pkOrderDateId == 3)
                            {
                                FromDate = new DateTime(today.Year, today.Month, 1).AddMonths(-1).ToShortDateString();
                                ToDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(-1).ToShortDateString();
                            }
                            else if (pkOrderDateId == 4)
                            {
                                try
                                {
                                    FromDate = new DateTime(Year, Month, 1).ToShortDateString();
                                    ToDate = new DateTime(Year, Month, DateTime.DaysInMonth(Year, Month)).ToShortDateString();
                                }
                                catch
                                {

                                }

                            }
                            else if (pkOrderDateId == 5)
                            {
                                FromDate = filters[9].Value;
                                ToDate = filters[9].Value;
                            }
                            else if (pkOrderDateId == 6)
                            {
                                FromDate = filters[10].Value;
                                ToDate = filters[11].Value;
                            }
                        }
                    }
                }




                var AllLeadList = ObjBALLeads.GetAllLeads(
                    iPageNo,
                    PageSize,
                    false,
                    Keyword,
                    short.Parse(companyType),
                    -1,
                    ColumnName,
                    Direction, FromDate, ToDate,
                    LeadStatus,
                    SalesManagerId,
                    SalesRepId,
                    CurrentUserId,
                    LeadSource,
                   LeadType, FilterColumn, FilterAction, FilterValue,
                   LeadNextStep,
               SalesAssosiate
                    );



                strLog = " Total Leads are : " + Convert.ToString(AllLeadList.Count());
                BALGeneral.WriteLogForDeletion(DateTime.Now.ToString() + " " + strLog, LogFilename);
                StreamWriter writer = new StreamWriter(output, Encoding.UTF8);

                writer.Write("Lead Note,");
                writer.Write("Company Name,");
                writer.Write("Company Url,");
                writer.Write("Company Type,");


                writer.Write("Lead Number ,");
                writer.Write("Lead Source  ,");

                writer.Write("Account Manager,");
                writer.Write("Sales Associate,");
                writer.Write("Address1,");


                writer.Write("Address2 , ");


                writer.Write("State,");
                writer.Write("City,");
                writer.Write("Zip Code,");
                writer.Write("Main Contact ,");
                writer.Write("Main Title,");
                writer.Write("Main Email,");
                writer.Write("Main Phone,");
                writer.Write("Ext 1,");

                writer.Write("Main Fax,");
                // for Secondary Info
                writer.Write("Secondary Contact,");
                writer.Write("Secondary Email,");
                writer.Write("Secondary Phone,");
                writer.Write("Ext 2,");
                writer.Write("Secondary Fax,");
                writer.Write("Monthly Volume,");




                writer.Write("Number of Location,");
                writer.Write("Number of Employees,");
                writer.Write("Current Provider,");

                writer.Write("Priority 1,");
                writer.Write("Priority 2,");



                writer.Write("Priority 3  ");
                writer.WriteLine();

                for (int i = 0; i < AllLeadList.Count(); i++)
                {

                    BALCompany objbalCompany = new BALCompany();
                    var list1 = objbalCompany.GetCompanyDetailByLocationId(Int32.Parse(AllLeadList.ElementAt(i).pkLocationId.ToString()));
                    string StateName = string.Empty;
                    BALCompany serobj = new BALCompany();
                    var list2 = serobj.GetState();
                    if (list1.ElementAt(0).fkStateID != null)
                    {
                        var StateName1 = list2.Where(rec => rec.pkStateId == list1.ElementAt(0).fkStateID).FirstOrDefault();
                        if (StateName1 != null)
                        {
                            StateName = StateName1.StateName == null ? string.Empty : StateName1.StateName;
                        }
                    }
                    string MangerName = string.Empty;
                    if (list1.ElementAt(0).fkSalesManagerId != null)
                    {

                        string rolename = "salesmanager";
                        var Managerslist = serobj.GetManagers(rolename);
                        var Manager = Managerslist.Where(rec => rec.fkUserId == list1.ElementAt(0).fkSalesManagerId).FirstOrDefault();
                        if (Manager != null)
                        {
                            MangerName = Manager.FullName == null ? string.Empty : Manager.FullName;
                        }
                    }

                    string SaleAssociative = string.Empty;
                    if (list1.ElementAt(0).fkSalesRepId != null)
                    {
                        string rolename = "salesrep";
                        var AssociativeList = serobj.GetManagers(rolename);
                        var Associative = AssociativeList.Where(rec => rec.fkUserId == list1.ElementAt(0).fkSalesRepId).FirstOrDefault();

                        if (Associative != null)
                        {
                            SaleAssociative = Associative.FullName == null ? string.Empty : Associative.FullName;
                        }
                    }

                    string CompanyNote = list1.ElementAt(0).CompanyNote == null ? string.Empty : list1.ElementAt(0).CompanyNote.ToString().Replace(",", " ").Replace("\r\n", " ").Trim();

                    writer.Write(CompanyNote.Replace(",", " "));
                    writer.Write(",");
                    writer.Write("\"");
                    writer.Write(AllLeadList.ElementAt(i).CompanyName == null ? string.Empty : AllLeadList.ElementAt(i).CompanyName.Replace(",", " "));
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");
                    writer.Write(list1.ElementAt(0).CompanyUrl == null ? string.Empty : list1.ElementAt(0).CompanyUrl.Replace(",", " "));
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");



                    writer.Write(AllLeadList.ElementAt(i).CompanyType == null ? string.Empty : AllLeadList.ElementAt(i).CompanyType.Replace(",", " "));
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");

                    writer.Write(AllLeadList.ElementAt(i).LeadNumber == null ? string.Empty : AllLeadList.ElementAt(i).LeadNumber);
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");

                    writer.Write(AllLeadList.ElementAt(i).LeadSource == null ? string.Empty : AllLeadList.ElementAt(i).LeadSource.Replace(",", " "));
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");

                    writer.Write(MangerName == null ? string.Empty : MangerName.Replace(",", " "));
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");

                    writer.Write(SaleAssociative == null ? string.Empty : SaleAssociative.Replace(",", " "));
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");

                    writer.Write(list1.ElementAt(0).Address1 == null ? string.Empty : list1.ElementAt(0).Address1.Replace(",", " "));
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");

                    writer.Write(list1.ElementAt(0).Address2 == null ? string.Empty : list1.ElementAt(0).Address2.Replace(",", " "));
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");


                    writer.Write(StateName == null ? string.Empty : StateName);
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");

                    writer.Write(AllLeadList.ElementAt(i).City == null ? string.Empty : AllLeadList.ElementAt(i).City);
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");

                    writer.Write(list1.ElementAt(0).ZipCode == null ? string.Empty : list1.ElementAt(0).ZipCode);
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");


                    writer.Write(AllLeadList.ElementAt(i).MainContactPersonName == null ? string.Empty : AllLeadList.ElementAt(i).MainContactPersonName.Replace(",", " "));
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");

                    writer.Write(list1.ElementAt(0).MainTitle == null ? string.Empty : list1.ElementAt(0).MainTitle.Replace(",", " "));
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");
                    writer.Write(AllLeadList.ElementAt(i).MainEmailId == null ? string.Empty : AllLeadList.ElementAt(i).MainEmailId);
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");

                    writer.Write(AllLeadList.ElementAt(i).PhoneNo1 == null ? string.Empty : AllLeadList.ElementAt(i).PhoneNo1);
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");

                    writer.Write(AllLeadList.ElementAt(i).Ext1 == null ? string.Empty : AllLeadList.ElementAt(i).Ext1);
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");


                    writer.Write(list1.ElementAt(0).Fax == null ? string.Empty : list1.ElementAt(0).Fax);
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");

                    writer.Write(list1.ElementAt(0).SecondaryMainContactPersonName == null ? string.Empty : list1.ElementAt(0).SecondaryMainContactPersonName.Replace(",", " "));
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");

                    writer.Write(list1.ElementAt(0).SecondaryEmail == null ? string.Empty : list1.ElementAt(0).SecondaryEmail);
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");

                    writer.Write(list1.ElementAt(0).SecondaryPhone == null ? string.Empty : list1.ElementAt(0).SecondaryPhone);
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");
                    writer.Write(list1.ElementAt(0).Ext2 == null ? string.Empty : list1.ElementAt(0).Ext2);
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");
                    writer.Write(list1.ElementAt(0).SecondaryFax == null ? string.Empty : list1.ElementAt(0).SecondaryFax);
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");
                    writer.Write(list1.ElementAt(0).MonthlyVolume == null ? string.Empty : list1.ElementAt(0).MonthlyVolume.ToString());
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");

                    writer.Write(list1.ElementAt(0).NoOfLocations);
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");

                    writer.Write(list1.ElementAt(0).NoofEmployees);
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");

                    writer.Write(list1.ElementAt(0).CurrentProvider == null ? string.Empty : list1.ElementAt(0).CurrentProvider);
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");


                    List<SelectListItem> list9 = new List<SelectListItem>();
                    list9.Add(new SelectListItem { Text = "--Select--", Value = "-1" });
                    list9.Add(new SelectListItem { Text = "Accuracy", Value = "1" });
                    list9.Add(new SelectListItem { Text = "Coverage", Value = "2" });
                    list9.Add(new SelectListItem { Text = "Pricing", Value = "3" });
                    list9.Add(new SelectListItem { Text = "Speed", Value = "4" });
                    list9.Add(new SelectListItem { Text = "Support", Value = "5" });
                    list9.Add(new SelectListItem { Text = "Other", Value = "6" });

                    var Priority1 = list9.Where(rec => rec.Value == list1.ElementAt(0).Priority1).FirstOrDefault();
                    string Priority1Name = string.Empty;
                    if (Priority1 != null)
                    {
                        Priority1Name = Priority1.Text == null ? string.Empty : Priority1.Text;
                    }

                    writer.Write(list1.ElementAt(0).Priority1 == "-1" ? string.Empty : Priority1Name);
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");


                    var Priority2 = list9.Where(rec => rec.Value == list1.ElementAt(0).Priority2).FirstOrDefault();

                    string Priority2Name = string.Empty;
                    if (Priority1 != null)
                    {
                        Priority2Name = Priority2.Text == null ? string.Empty : Priority2.Text;
                    }
                    writer.Write(list1.ElementAt(0).Priority2 == "-1" ? string.Empty : Priority2Name);
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");

                    var Priority3 = list9.Where(rec => rec.Value == list1.ElementAt(0).Priority3).FirstOrDefault();

                    string Priority3Name = string.Empty;
                    if (Priority3 != null)
                    {
                        Priority3Name = Priority2.Text == null ? string.Empty : Priority3.Text;
                    }


                    writer.Write(list1.ElementAt(0).Priority3 == "-1" ? string.Empty : Priority3Name);
                    writer.Write("\"");
                    writer.WriteLine();
                }
                writer.Flush();
                output.Position = 0;
            }
            catch (Exception ex)
            {

                strLog = "InnerException Exception:" + ex.InnerException.ToString() + " StackTrace:" + ex.StackTrace.ToString();
                BALGeneral.WriteLogForDeletion(DateTime.Now.ToString() + " " + strLog, LogFilename);
            }
            BALGeneral.WriteLogForDeletion(DateTime.Now.ToString() + " Method Ended : ExportLeadInfotoCSV ", LogFilename);

            return File(output, "text/comma-separated-values", "Leads.csv");
        }

        #region Delete Lead
        [HttpPost]
        public JsonResult DeleteLead(int pkCompanyId)
        {

            BALCompany ObjBALCompany = new BALCompany();
            List<tblCompany> CompanyUserList = new List<tblCompany>();
            try
            {
                tblCompany objTblCompany = new tblCompany();
                objTblCompany.pkCompanyId = pkCompanyId;
                CompanyUserList.Add(objTblCompany);
                int result = ObjBALCompany.ActionOnCompany(CompanyUserList, Convert.ToByte(2), Convert.ToBoolean(1));
                if (result == 1)
                {
                    //BindData();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBALCompany = null;
            }

            return Json(new { Data = "delete" });
        }

        [HttpPost]
        public JsonResult DeleteBatchLeads(string pkIds)
        {
            string Message = string.Empty;
            string CSS = string.Empty;
            string[] Ids = { };
            if (!string.IsNullOrEmpty(pkIds))
            {
                Ids = pkIds.Split(',').Where(x => x != string.Empty).ToArray();
            }
            //BALLeads ObjBALLeads = new BALLeads();
            BALCompany ObjBALCompany = new BALCompany();
            List<tblCompany> CompanyUserList = new List<tblCompany>();
            try
            {
                if (Ids.Count() > 0)
                {
                    for (int i = 0; i < Ids.Count(); i++)
                    {
                        tblCompany objTblCompany = new tblCompany();
                        objTblCompany.pkCompanyId = Int32.Parse(Ids[i]);
                        CompanyUserList.Add(objTblCompany);
                    }
                    int result = ObjBALCompany.ActionOnCompany(CompanyUserList, Convert.ToByte(2), Convert.ToBoolean(1));
                    if (result == 1)
                    {
                        Message = "Transfered successfully";
                        CSS = "successmsg";
                    }
                    else
                    {
                        Message = "Some error ocurred. Please try again later.";
                        CSS = "Error";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBALLeads = null;
            }

            return Json(new { Msg = Message, css = CSS });
        }
        #endregion

        #region Move Lead
        [HttpPost]
        public JsonResult MoveLeads(string pkIds, string LeadType)
        {
            string Message = string.Empty;
            string CSS = string.Empty;
            string ActiveLeads = string.Empty;
            string alllead = string.Empty;
            string newlead = string.Empty;
            string hotlead = string.Empty;
            string coldlead = string.Empty;
            string dedelead = string.Empty;
            string pendingDemos = string.Empty;
            string pendingAgreements = string.Empty;
            string deepFreeze = string.Empty;


            string[] Ids = { };
            if (!string.IsNullOrEmpty(pkIds))
            {
                Ids = pkIds.Split(',').Where(x => x != string.Empty).ToArray();
            }
            BALLeads ObjBALLeads = new BALLeads();
            List<tblCompany> CompanyUserList = new List<tblCompany>();
            try
            {
                if (Ids.Count() > 0)
                {
                    for (int i = 0; i < Ids.Count(); i++)
                    {
                        tblCompany objTblCompany = new tblCompany();
                        objTblCompany.pkCompanyId = Int32.Parse(Ids[i]);
                        CompanyUserList.Add(objTblCompany);
                    }
                    int result = ObjBALLeads.MoveLead(CompanyUserList, byte.Parse(LeadType));
                    if (result == 1)
                    {
                        MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));
                        Guid SalesManagerId = Guid.Empty;
                        Guid SalesRepId = Guid.Empty;

                        if (Roles.IsUserInRole(User.Identity.Name, "SalesManager"))
                        {
                            Guid UserId1 = Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
                            SalesManagerId = UserId1;
                        }

                        if (Roles.IsUserInRole(User.Identity.Name, "SalesRep"))
                        {
                            Guid UserId1 = Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
                            SalesRepId = UserId1;
                        }
                        List<tblCompany> ObjDbCollection = new List<tblCompany>();
                        ObjDbCollection = ObjBALLeads.CountLeadType(SalesManagerId, SalesRepId);
                        int s = 0;

                        for (int i = 0; i < ObjDbCollection.Count(); i++)
                        {
                            if (ObjDbCollection.ElementAt(i).LeadType != 4)
                            {
                                if (ObjDbCollection.ElementAt(i).LeadType == 0)
                                {
                                    if (ObjDbCollection.ElementAt(i).LeadStatus == 1 || ObjDbCollection.ElementAt(i).LeadStatus == 2 || ObjDbCollection.ElementAt(i).LeadStatus == 3)
                                    {
                                        s++;
                                    }
                                }
                            }


                        }
                        ActiveLeads = "(" + Convert.ToString(s) + ")";


                        alllead = "(" + Convert.ToString(ObjDbCollection.Count(dx => dx.LeadType != 4)) + ")";

                        newlead = "(" + Convert.ToString(ObjDbCollection.Count(dx => dx.LeadType == 1)) + ")";

                        hotlead = "(" + Convert.ToString(ObjDbCollection.Count(dx => dx.LeadType == 2)) + ")";

                        coldlead = "(" + Convert.ToString(ObjDbCollection.Count(dx => dx.LeadType == 3)) + ")";

                        dedelead = "(" + Convert.ToString(ObjDbCollection.Count(dx => dx.LeadType == 4)) + ")";


                        /***********************New Folders Added Ticket 128 :Add three folders to Emerge Office menu******************************/
                        //for Pending Demos folder
                        pendingDemos = "(" + Convert.ToString(ObjDbCollection.Count(dx => dx.LeadType == 8)) + ")";
                        //for Pending Agreements folder
                        pendingAgreements = "(" + Convert.ToString(ObjDbCollection.Count(dx => dx.LeadType == 9)) + ")";
                        //for Deep Freeze folder
                        deepFreeze = "(" + Convert.ToString(ObjDbCollection.Count(dx => dx.LeadType == 10)) + ")";
                        /*********************************************End*************************************************************************/






                        Message = "Moved successfully";
                        CSS = "successmsg";
                    }
                    else
                    {
                        Message = "Some error ocurred. Please try again later.";
                        CSS = "Error";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBALLeads = null;
            }

            return Json(new { Msg = Message, css = CSS, Active_Leads = ActiveLeads, all_lead = alllead, new_lead = newlead, hot_lead = hotlead, cold_lead = coldlead, dede_lead = dedelead, pending_Demos = pendingDemos, pending_Agreements = pendingAgreements, deep_Freeze = deepFreeze });
        }
        #endregion

        #region Transfer Lead
        [HttpPost]
        public JsonResult TransferLeads(string pkIds, string SalesManager)
        {
            string Message = string.Empty;
            string CSS = string.Empty;
            string[] Ids = { };
            if (!string.IsNullOrEmpty(pkIds))
            {
                Ids = pkIds.Split(',').Where(x => x != string.Empty).ToArray();
            }
            BALLeads ObjBALLeads = new BALLeads();
            List<tblCompany> CompanyUserList = new List<tblCompany>();
            try
            {
                if (Ids.Count() > 0)
                {
                    for (int i = 0; i < Ids.Count(); i++)
                    {
                        tblCompany objTblCompany = new tblCompany();
                        objTblCompany.pkCompanyId = Int32.Parse(Ids[i]);
                        CompanyUserList.Add(objTblCompany);
                    }
                    Guid fkSalesManagerId = new Guid(SalesManager);
                    int result = ObjBALLeads.TransferLead(CompanyUserList, fkSalesManagerId);
                    if (result == 1)
                    {
                        Message = "Transfered successfully";
                        CSS = "successmsg";
                    }
                    else
                    {
                        Message = "Some error ocurred. Please try again later.";
                        CSS = "Error";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBALLeads = null;
            }

            return Json(new { Msg = Message, css = CSS });
        }
        #endregion

        #region RunJuno

        [HttpPost]
        public ActionResult GetJunoforRun(int page, int pageSize)
        {
            BALJuno ObjBALJuno = new BALJuno();
            int iPageNo = page;
            int PageSize = pageSize;
            int iTotalRecords = 0;
            Guid UserId = Utility.GetUID(User.Identity.Name);
            //var tblRowList = new List<tblJunoInterval>();

            ObjBALJuno.DeleteJunoIntervalByUserId(UserId);

            List<Proc_GetNewJunosIntervalResult> ObjDbCollection = new List<Proc_GetNewJunosIntervalResult>();
            ObjDbCollection = ObjBALJuno.GetNewJunosInterval("CreatedDate", "DESC", true, iPageNo, PageSize, 0, UserId);

            if (ObjDbCollection.Count > 0)
            {
                iTotalRecords = int.Parse(ObjDbCollection.ElementAt(0).TotalRec.ToString());
            }
            // return Json(ObjDbCollection);
            return Json(new { Juno = ObjDbCollection, TotalCount = iTotalRecords });
        }
        public ActionResult UpdateCallBack(byte Lead_Status, Guid pkCompanyId)
        {
            string strMessage = "";
            BALLeads ObjBALLeads = new BALLeads();
            tblCompany ObjtblCompany = new tblCompany();
            try
            {
                Guid UserId;
                MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));
                if (ObjMembershipUser != null)
                {
                    UserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
                    int CompanyId = Int32.Parse(pkCompanyId.ToString());
                    ObjtblCompany.pkCompanyId = CompanyId;
                    ObjtblCompany.LastModifiedById = UserId;
                    ObjtblCompany.LastModifiedDate = DateTime.Now;
                    ObjtblCompany.StatusStartDate = DateTime.Now;

                }
                ObjtblCompany.LeadStatus = Lead_Status;
                ObjBALLeads.UpdateLeadStatus(ObjtblCompany);
                strMessage = "<span class='successmsg'>Juno Status updated successfully.</span>";


            }
            finally
            {
                ObjBALLeads = null;
            }
            return Json(new { UpdateMessage = strMessage });
        }

        #endregion
        #region Auto Complete

        public ActionResult GetTop10RecordforAutoComplete(string searchText)
        {
            BALOrders ObjOrder = new BALOrders();


            bool Isadmin;
            //BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            Guid UserId = Utility.GetUID(User.Identity.Name);
            Session["UserIdByRole"] = Utility.GetUID(User.Identity.Name);
            string[] multiple_Roles = Roles.GetRolesForUser(User.Identity.Name);
            if (multiple_Roles.Contains("SystemAdmin") || multiple_Roles.Contains("SalesAdmin") || multiple_Roles.Contains("SupportTechnician") || multiple_Roles.Contains("SupportManager"))//If multiple role exist or systemadmin loggeed on//
            {
                Isadmin = true;
            }
            else
            {
                Isadmin = false;

            }



            var Coll = ObjOrder.GetAutoCompleteTop10Records(searchText, UserId, Isadmin).Select(d => d.Item).ToList();





            if (Coll.Count() == 0)
            {
                Coll.Insert(0, "No Record Found");
            }
            return Json(Coll, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region SearchByCompanyName
        [HttpPost]
        public ActionResult SearchByCompanyName(string CompanyName)
        {
            string status = "";
            int Count = 0;
            var Redirect = "";
            bool Isadmin;
            //BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            Guid UserId = Utility.GetUID(User.Identity.Name);
            Session["UserIdByRole"] = Utility.GetUID(User.Identity.Name);
            string[] multiple_Roles = Roles.GetRolesForUser(User.Identity.Name);
            if (multiple_Roles.Contains("SystemAdmin") || multiple_Roles.Contains("SalesAdmin") || multiple_Roles.Contains("SupportTechnician") || multiple_Roles.Contains("SupportManager"))//If multiple role exist or systemadmin loggeed on//
            {
                Isadmin = true;
            }
            else
            {
                Isadmin = false;

            }
            var list = ObjBALLeads.GetAllLeadAndClientByUserid(UserId, CompanyName, Isadmin);
            Count = list.Count();

            for (int i = 0; i < Count; i++)
            {
                status = list.ElementAt(i).LeadStatus.ToString();
            }
            if (Count > 0)
            {
                if (status == "1" || status == "2" || status == "3" || status == "4")
                {
                    Redirect = "LeadPage";
                }
                else
                {
                    Redirect = "ClientPage";
                }
            }


            return Json(new { label = status, Total = Count, RedirectTopage = Redirect, CurrentUserId = Session["UserIdByRole"] });
        }

        #endregion

    }

    public class LeadForwardBackwardInfo
    {
        public long? RowIndex { get; set; }
        public int Id { get; set; }
        public long TotalCount { get; set; }
    }
}
