﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Net.Mime;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Security;
using System.Data.Linq;
using System.Web.Security;
using Emerge.Common;
using System.Text;

namespace Emerge.Controllers.Office
{
    public partial class OfficeController : Controller
    {
       // Page Load Method 
        public ActionResult AdminClients()
        {
            return View();
        }
        // Grid Bind Method
        public ActionResult GetAdminClients(int page, int pageSize, List<GridSort> sort, List<GridFilter> filters)
        {
            try
            {
                Guid UserId = Utility.GetUID(User.Identity.Name);
                BALLeads ObjBALLeads = new BALLeads();
                var Managerid = Guid.Empty;
                Guid saleid = Guid.Empty;
                string Searchkeyword = "";
                var Status = "SelectStatus";
                string Direction = "Desc";
                string ColumnName = "CreatedDate";
                string ToDate = "";
                string FromDate = "";
              string filterColumn=string.Empty;
              byte FilterAction = 2; 
                string filterValue=string.Empty;
                if (sort != null)
                {
                Direction = sort[0].Dir;
                ColumnName = sort[0].Field;
                }
               // ClientsModel objClientmodel = new ClientsModel();
                if (filters != null)
                {
                        FilterAction = 1;
                        filterColumn = filters[0].Field;
                        filterValue = filters[0].Value;
                    
                }

                var list = ObjBALLeads.GetAllClients(page, pageSize, true, Searchkeyword, -1, Status, ColumnName, Direction, FromDate, ToDate, Managerid, saleid, UserId, "", filterColumn, FilterAction, filterValue);
                if (list.Count > 0)
                {
                    return Json(new { Reclist = list, TotalRec = list.ElementAt(0).TotalRec });
                }
                else
                {
                    return Json(new { Reclist = 0, TotalRec = 0});
 
                }
            }
            catch {
                return Json(new { Reclist = 0, TotalRec = 0 });
            }
        }

    }
}
