﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using System.Web.Security;
using Emerge.Common;
using Emerge.Services;
using Emerge.Data;
using Emerge.Controllers.Control;

namespace Emerge.Controllers.Office
{
    public partial class OfficeController : Controller
    {
        //
        // GET: /MyAccount/
        BALOffice ObjBALOffice;
        BALGeneral ObjBALGeneral;
        tblCompanyUser ObjtblCompanyUser;
        public string Selected_RoleName = string.Empty;
        public Guid Selected_RoleID = Guid.Empty;
        public Guid Selected_ManagerID = Guid.Empty;
        public int Selected_RelationID = 0;

        public ActionResult MyAccount()
        {
            UsersModel ObjUsersModel = new UsersModel();
            if (TempData["message"] != null)
            {
                ObjUsersModel.strResult = TempData["message"].ToString();
            }
            setPageMode(ObjUsersModel);
            MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));
            if (ObjMembershipUser == null)
            {
                HttpContext.Response.Redirect(ApplicationPath.GetApplicationPath() + "Office/Default");
            }
            string[] multiple_Roles = Roles.GetRolesForUser(ObjMembershipUser.UserName);
            ObjUsersModel.CurrentUserEmail = ObjMembershipUser.UserName;
            Guid ObjGuid = Guid.Empty;
            //BindCompany();
            //BindState();
            //BindCompany();

            if (ObjUsersModel.IsPageModeView)
            {
                //trEnable.Visible = false;
                //trSignedFCRA.Visible = false;
            }


            if (multiple_Roles.Contains("SalesAdmin") && ObjUsersModel.IsPageModeView == false && ObjUsersModel.IsPageModeEdit == true || (multiple_Roles.Contains("SalesManager")) || (multiple_Roles.Contains("SalesRep")))
            {
                // LoadManagers();
                ObjUsersModel.trManagerVisible = true;
                ObjUsersModel.ddlManagersEnabled = "true";
              
                if (multiple_Roles.Contains("SalesRep") && ObjUsersModel.IsPageModeView == true)
                {
                    ObjUsersModel.ddlManagersEnabled = "false";
                }
                if (multiple_Roles.Contains("SalesManager") && ObjUsersModel.IsPageModeEdit == true)
                {
                    ObjUsersModel.ddlManagersEnabled = "false";
                }
            }
            else if (multiple_Roles.Contains("SalesAdmin") && ObjUsersModel.IsPageModeAdd == true)
            {
                //LoadManagers();
                ObjUsersModel.trManagerVisible = false;
                ObjUsersModel.ddlManagersEnabled = "false";
               
            }
            else
            {
                ObjUsersModel.ddlManagersEnabled = "false";           
                ObjUsersModel.trManagerVisible = false;
            }

            if (multiple_Roles.Contains("SalesManager") && ObjUsersModel.IsPageModeEdit == true)
            {
                //imgbtnAdd.Visible = false;
            }
            else
            {
                //imgbtnAdd.Visible = !false;
            }

            if (!string.IsNullOrEmpty(Request.QueryString["_c"]))         //  _c Contains Company ID
            {
                ObjUsersModel.QueryStringCompanyId = Request.QueryString["_c"].ToString();
                //ViewState["vwCompanyId"] = Request.QueryString["_c"];
            }
            if (!string.IsNullOrEmpty(Request.QueryString["_an"]))         //  _c Contains Company ID
            {
                //BindStatus("");
                //ddlStatus.Enabled = true;
            }
            else if (!string.IsNullOrEmpty(Request.QueryString["_a"]))         //  _a Contains PkUser ID
            {
                ObjBALCompanyUsers = new BALCompanyUsers();
                ObjUsersModel.QueryStringUserId = Request.QueryString["_a"].ToString();
                ObjGuid = new Guid(Request.QueryString["_a"].ToString());
            }
            else
            {
                if (ObjMembershipUser != null)
                {
                    ObjGuid = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
                }

                if (!String.IsNullOrEmpty(multiple_Roles[0]))
                {
                    // BindStatus(multiple_Roles[0]);
                }

            }
            if (ObjGuid != Guid.Empty)
            {
                ObjBALCompanyUsers = new BALCompanyUsers();
                ObjBALOffice = new BALOffice();

                ObjtblCompanyUser = ObjBALOffice.GetCompanyUser(ObjGuid, out Selected_RoleName, out Selected_RoleID);

                if (ObjtblCompanyUser != null)
                {
                    BindFieldsOfficeMyAccount(ObjtblCompanyUser, ObjUsersModel, Selected_RoleName);
                    //Page.Title = "Edit User";
                }
            }
            // GetCompanyUserId();
            return View(ObjUsersModel);
        }




        private void BindFieldsOfficeMyAccount(tblCompanyUser ObjtblCompanyUser, UsersModel ObjUsersModel, string Selected_RoleName)
        {
            ObjBALOffice = new BALOffice();
            #region Membership Info
            ObjUsersModel.fkUserId = ObjtblCompanyUser.fkUserId;
            MembershipUser ObjMembershipUser = Membership.GetUser(ObjtblCompanyUser.fkUserId);
            //string[] ObjRoles = Roles.GetRolesForUser(ObjMembershipUser.UserName);

            ObjUsersModel.LastLoginByUser = ObjMembershipUser.LastLoginDate.ToString();
            ObjUsersModel.UserEmail = ObjMembershipUser.UserName;

            string Password = ObjMembershipUser.GetPassword();
            ObjUsersModel.UserPassword = Password;


            #endregion

            #region User Personal Info

            Selected_ManagerID = ObjBALOffice.GetManagerID(ObjtblCompanyUser.fkUserId, out Selected_RelationID);
            if (Selected_ManagerID != Guid.Empty)
            {
                ObjUsersModel.Selected_RelationID = Selected_RelationID;
                ObjUsersModel.trManagerVisible = true;
                ObjUsersModel.ddlManagersEnabled = "true";           
               
                ObjUsersModel.ManagerUserId = Selected_ManagerID;
            }
            else
            {
                ObjUsersModel.trManagerVisible = false;
                ObjUsersModel.ddlManagersEnabled = "false";
            }
            //trLastLogin.Visible = true;
            //trUserImage.Visible = true;

            ObjUsersModel.FirstName = ObjtblCompanyUser.FirstName;
            ObjUsersModel.UserNote = ObjtblCompanyUser.UserNote;
            ObjUsersModel.LastName = ObjtblCompanyUser.LastName;
            ObjUsersModel.Selected_RoleName = Selected_RoleName;
            //BindStatus4View(Selected_RoleName, ObjUsersModel);

            ObjUsersModel.RoleId = Selected_RoleID;
            int CompanyId = GetCompanyIdByLocationId((int)ObjtblCompanyUser.fkLocationId);
            //BindLocation(CompanyId);

            ObjUsersModel.PkCompanyId = CompanyId;
            ObjUsersModel.fkLocationId = ObjtblCompanyUser.fkLocationId;

            //ddlCompany.Enabled = false;

            //if (ddlState.Items.Count == 0)
            //{
            // BindState();
            //}

            ObjUsersModel.fkStateId = (ObjtblCompanyUser.fkStateId == null) ? -1 : ObjtblCompanyUser.fkStateId;

            //int StateId;
            //int.TryParse(ObjUsersModel.fkStateId, out StateId);
            //LoadStateCounties(StateId);

            ObjUsersModel.fkCountyId = ObjtblCompanyUser.fkCountyId;
            ObjUsersModel.Initials = (ObjtblCompanyUser.Initials == null) ? string.Empty : ObjtblCompanyUser.Initials;
            ObjUsersModel.UserAddressLine1 = ObjtblCompanyUser.UserAddressLine1;
            ObjUsersModel.UserAddressLine2 = ObjtblCompanyUser.UserAddressLine2;
            ObjUsersModel.PhoneNo = ObjtblCompanyUser.PhoneNo;
            ObjUsersModel.ZipCode = ObjtblCompanyUser.ZipCode;

            ObjUsersModel.IsEnabled = ObjtblCompanyUser.IsEnabled;

            if (ObjtblCompanyUser.IsSignedFcra != null)
            {
                if (ObjtblCompanyUser.IsSignedFcra == true)
                {
                    ObjUsersModel.IsSignedFcra = true;
                }
                else
                {
                    ObjUsersModel.IsSignedFcra = false;
                }
            }


            #region User Image

            if (ObjtblCompanyUser.UserImage == string.Empty || !System.IO.File.Exists(Server.MapPath(@"~\Resources\Upload\Images\" + ObjtblCompanyUser.UserImage)))
            {
                ObjUsersModel.ViewImage = ApplicationPath.GetApplicationPath() + @"Content/themes/base/images/NotAvail.png";

            }
            else
            {
                ObjUsersModel.OldUserImage = ObjtblCompanyUser.UserImage;
                ObjUsersModel.ViewImage = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjtblCompanyUser.UserImage;
            }

            #endregion

            #endregion

            #region ViewState Info

            ObjUsersModel.pkCompanyUserId = ObjtblCompanyUser.pkCompanyUserId;
            ObjUsersModel.fkUserId = ObjtblCompanyUser.fkUserId;
            ObjUsersModel.OldUserPassword = Password;
            ObjUsersModel.OldUserEmail = ObjMembershipUser.UserName.Trim();

            #endregion

        }

        [ValidateInput(false)]
        public ActionResult SaveOfficeUserDetailsById(UsersModel ObjUsersModel, HttpPostedFileBase UserImage)
        {
            string strResult = string.Empty;
            if (!string.IsNullOrEmpty(ObjUsersModel.QueryStringUserId) || string.IsNullOrEmpty(Request.QueryString["_an"]))
            {
                string OldUserName = ObjUsersModel.OldUserEmail;
                string NewUserName = ObjUsersModel.UserEmail; ;
                bool bResult = IsExistsUsername(OldUserName, NewUserName);

                if (bResult)
                {
                    strResult = "<span class='infomessage'>This Username already exists</span>";

                    TempData["message"] = strResult;
                    return RedirectToAction("MyAccount");
                }
                strResult = UpdateOfficeUserInformation(ObjUsersModel, UserImage);
            }

            TempData["message"] = strResult;
            return RedirectToAction("MyAccount");
        }
        /// <summary>
        /// Function To Update User 
        /// </summary>
        public string UpdateOfficeUserInformation(UsersModel ObjUsersModel, HttpPostedFileBase UserImage)
        {
            string strResult = string.Empty;
            tblCompanyUser ObjtblCompanyUser = new tblCompanyUser();
            BALOffice ObjBALOffice = new BALOffice();
            tblSalesUserRelationship ObjtblSalesUserRelationship = new tblSalesUserRelationship();
            ObjBALCompanyUsers = new BALCompanyUsers();
            BLLMembership ObjBLLMembership = new BLLMembership();

            try
            {
                ObjtblCompanyUser.fkUserId = ObjUsersModel.fkUserId;
                ObjtblCompanyUser.pkCompanyUserId = ObjUsersModel.pkCompanyUserId;
                ObjtblCompanyUser.fkLocationId = ObjUsersModel.fkLocationId;
                ObjtblCompanyUser.FirstName = ObjUsersModel.FirstName;
                ObjtblCompanyUser.LastName = ObjUsersModel.LastName;
                ObjtblCompanyUser.Initials = ObjUsersModel.Initials;//string.Empty;
                ObjtblCompanyUser.UserAddressLine1 = !string.IsNullOrEmpty(ObjUsersModel.UserAddressLine1) ? ObjUsersModel.UserAddressLine1 : string.Empty;
                ObjtblCompanyUser.UserAddressLine2 = !string.IsNullOrEmpty(ObjUsersModel.UserAddressLine2) ? ObjUsersModel.UserAddressLine2 : string.Empty;
                ObjtblCompanyUser.PhoneNo = !string.IsNullOrEmpty(ObjUsersModel.PhoneNo) ? ObjUsersModel.PhoneNo : string.Empty;
                ObjtblCompanyUser.ZipCode = !string.IsNullOrEmpty(ObjUsersModel.ZipCode) ? ObjUsersModel.ZipCode : string.Empty;
                ObjtblCompanyUser.UserNote = !string.IsNullOrEmpty(ObjUsersModel.UserNote) ? ObjUsersModel.UserNote : string.Empty;

                ObjtblCompanyUser.fkStateId = ObjUsersModel.fkStateId;

                if (Convert.ToString(ObjUsersModel.fkCountyId) != "-1")
                {
                    ObjtblCompanyUser.fkCountyId = ObjUsersModel.fkCountyId;
                }
                else
                {
                    ObjtblCompanyUser.fkCountyId = null;
                }
                ObjtblCompanyUser.IsEnabled = ObjUsersModel.IsEnabled;
                ObjtblCompanyUser.IsSignedFcra = ObjUsersModel.IsSignedFcra;
                ObjtblCompanyUser.LastModifiedById = ObjUsersModel.CompanyUserId;
                ObjtblCompanyUser.LastModifiedDate = System.DateTime.Now;

                ObjtblCompanyUser.UserImage = string.Empty;

                if (UserImage != null)
                {
                    if (UserImage != null && UserImage.ContentLength > 0)
                    {
                        if (ObjUsersModel.OldUserImage != null)
                        {
                            string oldimage_Path = Server.MapPath(@"~\Resources\Upload\Images\") + ObjUsersModel.OldUserImage;
                            if (System.IO.File.Exists(oldimage_Path))
                            {
                                System.IO.File.Delete(oldimage_Path);

                            }
                        }
                        string UserImageFileName = ObjUsersModel.FirstName + ObjUsersModel.LastName + "_" + DateTime.Now.ToFileTime().ToString() + System.IO.Path.GetExtension(UserImage.FileName);
                        string upload_Path = Server.MapPath(@"~\Resources\Upload\Images\") + UserImageFileName;
                        UserImage.SaveAs(upload_Path);

                        ObjtblCompanyUser.UserImage = UserImageFileName;

                    }
                }
                else
                {
                    if (ObjUsersModel.OldUserImage != null)
                    {
                        ObjtblCompanyUser.UserImage = ObjUsersModel.OldUserImage;
                    }
                }

                int Result = ObjBALCompanyUsers.UpdateMyProfileinOffice(ObjtblCompanyUser, ObjUsersModel.RoleId);

                if (Result > 0)
                {
                    //if (ObjUsersModel.Selected_RelationID != null)
                    //{
                        ObjtblSalesUserRelationship.pkSalesRelationId = ObjUsersModel.Selected_RelationID;
                        ObjtblSalesUserRelationship.fkSalesManagerId = ObjUsersModel.ManagerUserId;
                        //int iresult = 
                            ObjBALOffice.UpdateRelation(ObjtblSalesUserRelationship);
                    //}

                    #region Update User Name and password and Role

                    MembershipUser ObjMembershipUser = Membership.GetUser(ObjUsersModel.OldUserEmail);

                    string OldUserName = ObjUsersModel.OldUserEmail;
                    string NewUserName = ObjUsersModel.UserEmail;

                    if (OldUserName.ToLower().Trim() != NewUserName.ToLower())
                    {
                        UpdateUsername(OldUserName, NewUserName);

                        #region Deduct last activity time

                        if (ObjMembershipUser != null)
                        {
                            ObjBLLMembership.SignOut(ObjMembershipUser.UserName, DateTime.Now.ToUniversalTime().AddMinutes(-(Membership.UserIsOnlineTimeWindow)));
                        }

                        #endregion
                    }


                    string OldPwd = ObjUsersModel.OldUserPassword;

                    if (ObjMembershipUser != null)
                    {
                        if (OldPwd != ObjUsersModel.UserPassword)
                        {
                            Membership.GetUser(ObjUsersModel.OldUserEmail).ChangePassword(OldPwd, ObjUsersModel.UserPassword);
                        }
                    }

                    #endregion
                    strResult = "<span class='successmsg'>Record Updated Successfully</span>";
                }
                else
                {
                    strResult = "<span class='errormsg'>Error Occured While Updating Record</span>";
                }
            }
            catch (Exception ex)
            {
                strResult = "<span class='errormsg'>"+ex.Message.ToString()+"</span>";
            }
            finally
            {
                ObjtblCompanyUser = null;
                ObjBALCompanyUsers = null;
                ObjBLLMembership = null;
            }
            return strResult;
        }
        /// <summary>
        /// This method is used to update Username
        /// </summary>
        /// <param name="OldUserName"></param>
        /// <param name="NewUserName"></param>
        /// <returns></returns>
        private int UpdateUsername(string OldUserName, string NewUserName)
        {
            int iResult = -1;

            NewUserName = NewUserName.Trim();

            BLLMembership ObjBLLMembership = new BLLMembership();

            try
            {
                iResult = ObjBLLMembership.UpdateUserName(OldUserName, NewUserName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBLLMembership = null;
            }
            return iResult;
        }

        /// <summary>
        /// This method is used to check user name existance
        /// </summary>
        /// <param name="OldUserName"></param>
        /// <param name="NewUserName"></param>
        /// <returns></returns>
        private bool IsExistsUsername(string OldUserName, string NewUserName)
        {
            bool bResult = false;
            NewUserName = NewUserName.Trim();

            BLLMembership ObjBLLMembership = new BLLMembership();

            try
            {
                bResult = ObjBLLMembership.IsExistUserName(OldUserName, NewUserName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBLLMembership = null;
            }
            return bResult;
        }

       
        
        public int GetCompanyIdByLocationId(int PkLocationId)
        {
            BALCompany ObjBALCompany = new BALCompany();
            int CompanyId = 0;
            try
            {
                List<Proc_Get_CompanyDetailByLocationIdResult> ObjCompanyDetailByLocationId = ObjBALCompany.GetCompanyDetailByLocationId(PkLocationId);
                CompanyId = ObjCompanyDetailByLocationId.First().pkCompanyId;
            }
            catch (Exception)
            {

            }
            finally
            {
                ObjBALCompany = null;
            }
            return CompanyId;
        }

        public ActionResult BindStatus4View(string RoleName)
        {
            ObjBALGeneral = new BALGeneral();
            List<ClsRoles> RolesColl = new List<ClsRoles>();
            try
            {
                RolesColl = ObjBALGeneral.FetchRolesAll(Utility.SiteApplicationId).Select(d => new ClsRoles
                {
                    RoleId = d.RoleId,
                    Description = d.Description,
                    RoleName = d.RoleName
                }).ToList();

                if (RolesColl.Count != 0)
                {
                    //This is false means selected company from dropdown list is not INTELIFI
                    //Then remove following role from Dropdown list
                    if (!String.IsNullOrEmpty(RoleName))
                    {
                        RolesColl = RolesColl.Where(d => d.RoleName.ToLower() == RoleName.ToLower()).OrderBy(d => d.RoleName).ToList<ClsRoles>();
                        //if (RolesColl.Count == 1)
                        //{
                           
                        //}
                    }
                }
                else
                {
                
                }
            }
            catch (Exception)
            {
              //  throw;
            }
            finally
            {
                ObjBALGeneral = null;
            }
            return Json(RolesColl);
        }
        public ActionResult LoadManagers()
        {
            BALOffice ObjBALOffice = new BALOffice();
            List<sManagers> ObjCollection = new List<sManagers>();

            ObjCollection = ObjBALOffice.GetManagerList(new Guid("4DF0B5B0-755B-438A-92B4-0E33C294CA52"));
            //ObjCollection.Insert(0, new sManagers
            //{
            //    UserId = Guid.Empty,
            //    FullName = "Select"
            //});
            return Json(ObjCollection);
        }

        public void setPageMode(UsersModel ObjUsersModel)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["_an"]))         //  _c Contains Company ID
            {
                ObjUsersModel.IsPageModeAdd = true;
            }
            else if (!string.IsNullOrEmpty(Request.QueryString["_a"]))         //  _a Contains PkUser ID
            {
                ObjUsersModel.IsPageModeEdit = true;
            }
            else if (!string.IsNullOrEmpty(Request.QueryString["vw"]))
            {
                ObjUsersModel.IsPageModeView = true;
            }
            else if (string.IsNullOrEmpty(Request.QueryString["_an"]) || string.IsNullOrEmpty(Request.QueryString["_a"]))
            {
                ObjUsersModel.IsPageModeView = true;
            }
        }
    }
}
