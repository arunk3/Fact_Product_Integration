﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Net.Mime;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Security;
using System.Data.Linq;
using System.Web.Security;
using Emerge.Common;
using System.Text;

namespace Emerge.Controllers.Office
{

    public partial class OfficeController : Controller
    {
        #region // Page Load Method
        public ActionResult Clients()
        {
            ClientsModel objClientmodel = new ClientsModel();
            objClientmodel.isnullRecord = false;
            BALSalesRep ObjBALSalesRep = new BALSalesRep();
            Guid UserId = Utility.GetUID(User.Identity.Name);
            BALOffice objOffice = new BALOffice();

            objClientmodel.RecentReviewList = objOffice.GetRecentReviewClients(UserId);
            //Guid pkcompaniyid = Guid.Parse("12937a77-c79d-42d3-9969-86369811082f");

            List<SelectListItem> listSalesMgr = new List<SelectListItem>();

            MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));
            if (Roles.IsUserInRole(User.Identity.Name, "SalesManager"))
            {
                Guid UserId1 = Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
                List<Proc_GetMangerlistbyMangerIdResult> salesManger = ObjBALLeads.GetMangerListByMangerId(UserId1);

                if (salesManger.Count > 0)
                {
                    listSalesMgr = salesManger.Select(x => new SelectListItem { Text = x.FullName, Value = x.fkUserId.ToString() }).ToList();
                }
                listSalesMgr.Insert(0, new SelectListItem { Text = listSalesMgr.ElementAt(0).Text, Value = listSalesMgr.ElementAt(0).Value });
                ViewData["Salesmanager"] = listSalesMgr;

            }
            else
            {
                string RoleName = "salesmanager";
                List<Proc_Get_SalesManagersResult> salesMgr = ObjBALSalesRep.GetSalesManagers(RoleName);
                if (salesMgr.Count > 0)
                {
                    listSalesMgr = salesMgr.Select(x => new SelectListItem { Text = x.FullName, Value = x.fkUserId.ToString() }).ToList();
                }
                listSalesMgr.Insert(0, new SelectListItem { Text = "--All--", Value = Guid.Empty.ToString() });
                ViewData["Salesmanager"] = listSalesMgr;
            }

            if (Request.QueryString["SearchQry"] != null)
            {
                objClientmodel.Keyword = Request.QueryString["SearchQry"].ToString();


            }

            return View(objClientmodel);

        }
        #endregion

        #region // Load Graph on Mouse Over
        //  public ActionResult LoadGraph(string pkCompanyId)
        //  {
        //      bool GraphMode = true;

        //      Guid pkCompanyID=Guid.Parse(pkCompanyId);
        //       #region For Mini Graph

        //      if (GraphMode == true)
        //      {

        //         //   Image imgSalesGraph ;
        //       //  Image imgBigImage;
        //          string lblSales =" ";
        //          string lblVolume= " ";
        //          int NumberOfMonths = 6;
        //          int rec = 5;
        //          BALCompany ObjBALCompany = new BALCompany();
        //          DateTime ObjDateTime = DateTime.Now.AddMonths(-5);
        //          int iCount = 0;
        //          decimal PreviousTotalSales = 0;
        //          decimal TotalSalesPercentage = 0;
        //          decimal TotalVolumePercentage = 0;
        //          decimal PreviousTotalVolume = 0;
        //          string Xvalue = " ";
        //          string XvalueText = "|";
        //          string ProductOldData = "";
        //          string ReportOldData = "";
        //          string TotalOldData = "";
        //          string ProductData = "";
        //          string ReportData = "";
        //          string TotalData = "";
        //          decimal ProductMaxValue = 0;
        //          decimal CurrentProductMaxValue = 0;
        //          decimal ReportMaxValue = 0;
        //          decimal CurrentReportMaxValue = 0;
        //          decimal TotalMaxValue = 0;
        //          decimal CurrentTotalMaxValue = 0;

        //          decimal CurrentMonthTotalSales = 0;
        //          decimal CurrentMonthTotalVolume = 0;
        //          var listofGraphValue = ObjBALCompany.GetSalesValuesforGraphMVC(pkCompanyID, NumberOfMonths);

        //          for (int i = 0; i < 6; i++)
        //          {

        //              if (iCount == 0)
        //              {

        //                  // var objM1 = listofGraphValue.ElementAt(0).
        //                  // var Obj1 = ObjBALCompany.GetSalesValuesforGraph(pkCompanyID, Convert.ToInt16(ObjDateTime.AddMonths(-1).Month), Convert.ToInt16(ObjDateTime.AddMonths(-1).Year));
        //                  //  var Obj2 = ObjBALCompany.GetVolumeValuesforGraph(pkCompanyID, Convert.ToInt16(ObjDateTime.AddMonths(-1).Month), Convert.ToInt16(ObjDateTime.AddMonths(-1).Year));
        //                  PreviousTotalSales = Convert.ToDecimal(listofGraphValue.ElementAt(6).TotalAmount);
        //                  PreviousTotalVolume = Convert.ToDecimal(listofGraphValue.ElementAt(6).TotalAmount);
        //              }


        //              if (iCount == 1)
        //              {
        //                  rec = 4;
        //              }

        //              //  var objBalGraph = ObjBALCompany.GetSalesValuesforGraphMVC(pkCompanyID, NumberOfMonths);

        //              // var Obj = ObjBALCompany.GetSalesValuesforGraph(pkCompanyID, Convert.ToInt16(ObjDateTime.AddMonths(0).Month), Convert.ToInt16(ObjDateTime.AddMonths(0).Year));
        //              //  var Obj3 = ObjBALCompany.GetVolumeValuesforGraph(pkCompanyID, Convert.ToInt16(ObjDateTime.AddMonths(i).Month), Convert.ToInt16(ObjDateTime.AddMonths(i).Year));
        //              TotalSalesPercentage = 0;
        //              TotalVolumePercentage = 0;
        //              if (PreviousTotalSales > 0)
        //              {
        //                  TotalSalesPercentage = ((Convert.ToDecimal(listofGraphValue.ElementAt(5).TotalAmount) - PreviousTotalSales) / PreviousTotalSales) * 100;
        //              }
        //              if (PreviousTotalVolume > 0)
        //              {
        //                  TotalVolumePercentage = ((Convert.ToDecimal(listofGraphValue.ElementAt(rec).TotalAmount) - PreviousTotalVolume) / PreviousTotalVolume) * 100;
        //              }
        //              //Xvalue += Obj.ElementAt(0).TotalAmount.ToString() + ",";
        //              XvalueText += ObjDateTime.AddMonths(i).ToString("MMM, yyyy") + "|";
        //              ProductOldData += listofGraphValue.ElementAt(rec).ProductAmount.ToString() + ",";
        //              ReportOldData += listofGraphValue.ElementAt(rec).ReportAmount.ToString() + ",";
        //              TotalOldData += listofGraphValue.ElementAt(rec).TotalAmount.ToString() + ",";

        //              ProductMaxValue = Convert.ToDecimal(listofGraphValue.ElementAt(rec).ProductAmount);
        //              ReportMaxValue = Convert.ToDecimal(listofGraphValue.ElementAt(rec).ReportAmount);
        //              TotalMaxValue = Convert.ToDecimal(listofGraphValue.ElementAt(rec).TotalAmount);

        //              CurrentMonthTotalSales = Convert.ToDecimal(listofGraphValue.ElementAt(1).TotalAmount);
        //              CurrentMonthTotalVolume = Convert.ToDecimal(listofGraphValue.ElementAt(rec).TotalAmount);

        //              if (ProductMaxValue > CurrentProductMaxValue)
        //              {
        //                  CurrentProductMaxValue = ProductMaxValue;
        //              }
        //              if (ReportMaxValue > CurrentReportMaxValue)
        //              {
        //                  CurrentReportMaxValue = ReportMaxValue;
        //              }
        //              if (TotalMaxValue > CurrentTotalMaxValue)
        //              {
        //                  CurrentTotalMaxValue = TotalMaxValue;
        //              }

        //              if (iCount != 0)
        //              {
        //                  PreviousTotalSales = Convert.ToDecimal(listofGraphValue.ElementAt(1).TotalAmount);
        //                  PreviousTotalVolume = Convert.ToDecimal(listofGraphValue.ElementAt(rec).TotalAmount);
        //              }

        //              iCount++;
        //              rec--;
        //              if (rec < 0)
        //              {
        //                  i = 6;
        //              }

        //          }

        //          var ProductArray = ProductOldData.Remove(ProductOldData.Length - 1, 1).Split(',');
        //          var ReportArray = ReportOldData.Remove(ReportOldData.Length - 1, 1).Split(',');
        //          var TotalArray = TotalOldData.Remove(TotalOldData.Length - 1, 1).Split(',');
        //          for (int i = 0; i < ProductArray.Length; i++)
        //          {
        //              decimal Value = Convert.ToDecimal(ProductArray[i].ToString());
        //              if (CurrentProductMaxValue > 0)
        //              {
        //                  Value = (Value / CurrentProductMaxValue) * 100;
        //              }
        //              ProductData += Value.ToString() + ",";
        //          }
        //          for (int j = 0; j < ReportArray.Length; j++)
        //          {
        //              decimal Value = Convert.ToDecimal(ReportArray[j].ToString());
        //              if (CurrentReportMaxValue > 0)
        //              {
        //                  Value = (Value / CurrentReportMaxValue) * 100;
        //              }
        //              ReportData += Value.ToString() + ",";
        //          }
        //          for (int k = 0; k < TotalArray.Length; k++)
        //          {
        //              decimal Value = Convert.ToDecimal(TotalArray[k].ToString());
        //              if (CurrentTotalMaxValue > 0)
        //              {
        //                  Value = (Value / CurrentTotalMaxValue) * 100;
        //              }
        //              TotalData += Value.ToString() + ",";
        //          }
        //          XvalueText.Remove(XvalueText.Length - 1, 1);
        //          string ChartMarkers = "&chm=d,FF0000,0,0,12,0|d,FF0000,0,1,12,0|d,FF0000,0,2,12,0|d,FF0000,0,3,12,0|d,FF0000,0,4,12,0|d,FF0000,0,5,12,0";
        //          ChartMarkers += "|d,00FF00,1,0,12,0|d,00FF00,1,1,12,0|d,00FF00,1,2,12,0|d,00FF00,1,3,12,0|d,00FF00,1,4,12,0|d,00FF00,1,5,12,0";
        //          ChartMarkers += "|d,0000FF,2,0,12,0|d,0000FF,2,1,12,0|d,0000FF,2,2,12,0|d,0000FF,2,3,12,0|d,0000FF,2,4,12,0|d,0000FF,2,5,12,0";
        //          string strURL = "http://chart.apis.google.com/chart?cht=lc&chs=430x320&chd=t:" + ProductData.Remove(ProductData.Length - 1, 1) + "|" + ReportData.Remove(ReportData.Length - 1, 1) + "|" + TotalData.Remove(TotalData.Length - 1, 1) + "&chds=0,100&chxt=x,y&chxl=0:" + XvalueText.Remove(XvalueText.Length - 1, 1) + "&chls=3,1,0|3,1,0|3,1,0&chco=FF0000,00FF00,0000FF&chdl=Products|Reports|Total&chg=0,9,1,1&" + ChartMarkers + "";
        //          string imgSalesGraph = "http://chart.apis.google.com/chart?cht=lc&chs=30x20&chd=t:" + TotalData.Remove(TotalData.Length - 1, 1) + "&chds=0,100&chls=3,1,0";
        //         string imgBigImage = strURL;

        //          string Value1 = "";
        //          if (TotalSalesPercentage >= 0)
        //          {
        //              Value1 = "<span style='color: green;'>" + string.Format("{0:0.0%}", (TotalSalesPercentage / 100)) + "</span>";
        //          }
        //          else
        //          {
        //              Value1 = "<span style='color: red;'>" + string.Format("{0:0.0%}", (TotalSalesPercentage / 100)) + "</span>";
        //          }
        //          string SalesAmount = Value1;
        //         lblSales = CurrentMonthTotalSales.ToString("c") + " (" + Value1 + ")";

        //          if (TotalVolumePercentage >= 0)
        //          {
        //              Value1 = "<span style='color: green;'>" + string.Format("{0:0.0%}", (TotalVolumePercentage / 100)) + "</span>";
        //          }
        //          else
        //          {
        //              Value1 = "<span style='color: red;'>" + string.Format("{0:0.0%}", (TotalVolumePercentage / 100)) + "</span>";
        //          }

        //          string VolumeAmount = Value1;
        //          lblVolume = CurrentMonthTotalVolume.ToString() + " (" + Value1 + ")";
        //      #endregion

        //          return Json(new { lblVolume = lblVolume, lblSales = lblSales, imgSalesGraph = imgSalesGraph, imgBigImage = imgBigImage });
        //  }
        //      else
        //      {
        //           return Json(new { lblVolume = 0, lblSales = 0,imgSalesGraph=0 });
        //      }
        //}

        public ActionResult LoadGraph(string pkCompanyId)
        {
            bool GraphMode = true;
            int pkCompanyID = Int32.Parse(pkCompanyId);

            // ND-26 here we call a store procedure for getting invidual sales and valume on hover on graph
            string showTotalSalesValue = "$0&nbsp;(<span style='color: red;'>0.00%</span>)";
            string showTotlaVolumeValue = "0&nbsp;(<span style='color: red;'>0.00%</span>)";

            string adminclient_totalsale = "$0.00";
            string adminclient_totalvolume = "0";

            using (EmergeDALDataContext obj1 = new EmergeDALDataContext())
            {
                var v = obj1.Proc_getTotalsalesandvolume(Convert.ToInt32(pkCompanyId));
                if (v != null)
                {
                    var v1 = v.FirstOrDefault();
                    if (v1 != null)
                    {
                        var TotalSale = Convert.ToInt32(v1.TotalSale);
                        var TotalVolume = Convert.ToInt32(v1.TotalVolume);

                        var PreviousVolume = Convert.ToDecimal(v1.PreTotalVolume);
                        var PreviousSale = Convert.ToDecimal(v1.PreTotalSale);

                        var VolumePercentage1 = 0.00m;
                        var SalePercentage1 = 0.00m;

                        var VolumePercentage = 0.00m;
                        var SalePercentage = 0.00m;

                        if (PreviousVolume > 0)
                        {
                            VolumePercentage1 = ((TotalVolume - PreviousVolume) / PreviousVolume) * 100;
                            VolumePercentage = Math.Round(VolumePercentage1, 2);
                            if (VolumePercentage > 0)
                            {
                                //row.cells[10].innerHTML = TotalVolume + "&nbsp;(<span style='color: green;'>" + VolumePercentage.toFixed(2) + "%</span>)";

                                //showTotlaVolumeValue= TotalVolume+"("+VolumePercentage+"%)";
                                showTotlaVolumeValue = TotalVolume + "&nbsp;(<span style='color: green;'>" + VolumePercentage + "%</span>)";
                            }
                            else
                            {

                                showTotlaVolumeValue = TotalVolume + "&nbsp;(<span style='color: red;'>" + VolumePercentage + "%</span>)";
                                //showTotlaVolumeValue= TotalVolume+"("+VolumePercentage+"%)";
                                //row.cells[10].innerHTML = TotalVolume + "&nbsp;(<span style='color: red;'>" + VolumePercentage.toFixed(2) + "%</span>)";
                            }
                        }
                        else
                        {
                            //row.cells[10].innerHTML = TotalVolume + "&nbsp;(<span style='color: red;'>" + VolumePercentage.toFixed(2) + "%</span>)";
                            showTotlaVolumeValue = TotalVolume + "&nbsp;(<span style='color: red;'>" + VolumePercentage + "%</span>)";
                            //showTotlaVolumeValue= TotalVolume+"("+VolumePercentage+"%)";
                        }
                        adminclient_totalvolume = Convert.ToString(TotalVolume);

                        if (PreviousSale > 0)
                        {
                            SalePercentage1 = ((TotalSale - PreviousSale) / PreviousSale) * 100;
                            SalePercentage = Math.Round(SalePercentage1);
                            if (SalePercentage > 0)
                            {
                                // row.cells[9].innerHTML = "$" + TotalSale + "&nbsp;(<span style='color: green;'>" + SalePercentage.toFixed(2) + "%</span>)";
                                // showTotalSalesValue = TotalSale + "(" + SalePercentage + "%)";
                                showTotalSalesValue = "$" + TotalSale + "&nbsp;(<span style='color: green;'>" + SalePercentage + "%</span>)";
                            }
                            else
                            {
                                // row.cells[9].innerHTML = "$" + TotalSale + "&nbsp;(<span style='color: red;'>" + SalePercentage.toFixed(2) + "%</span>)";
                                //showTotalSalesValue = TotalSale + "(" + SalePercentage + "%)";
                                showTotalSalesValue = "$" + TotalSale + "&nbsp;(<span style='color: red;'>" + SalePercentage + "%</span>)";
                            }
                        }
                        else
                        {
                            // row.cells[9].innerHTML = "$" + TotalSale + "&nbsp;(<span style='color: red;'>" + SalePercentage.toFixed(2) + "%</span>)";
                            //showTotalSalesValue = TotalSale + "(" + SalePercentage + "%)";
                            showTotalSalesValue = "$" + TotalSale + "&nbsp;(<span style='color: red;'>" + SalePercentage + "%</span>)";
                        }

                        adminclient_totalsale = "$" + TotalSale;
                    }
                }
            }
            //-----------------------------------------------------------------------------------------------------------------------------------------




            if (GraphMode == true)
            {
                DateTime ObjDateTime = DateTime.Now.AddMonths(-5);
                int iCount = 0;
                decimal PreviousTotalSales = 0;
                decimal TotalSalesPercentage = 0;
                decimal TotalVolumePercentage = 0;
                decimal PreviousTotalVolume = 0;
                //string Xvalue = "";
                string XvalueText = "|";
                string ProductOldData = "";
                string ReportOldData = "";
                string TotalOldData = "";
                string ProductData = "";
                string ReportData = "";
                string TotalData = "";
                decimal ProductMaxValue = 0;
                decimal CurrentProductMaxValue = 0;
                decimal ReportMaxValue = 0;
                decimal CurrentReportMaxValue = 0;
                decimal TotalMaxValue = 0;
                decimal CurrentTotalMaxValue = 0;
                decimal yaxisInterVal = 1;

                decimal CurrentMonthTotalSales = 0;
                decimal CurrentMonthTotalVolume = 0;
                for (int i = 0; i < 6; i++)
                {
                    BALCompany ObjBALCompany = new BALCompany();
                    if (iCount == 0)
                    {
                        var Obj1 = ObjBALCompany.GetSalesValuesforGraph(pkCompanyID, Convert.ToInt16(ObjDateTime.AddMonths(-1).Month), Convert.ToInt16(ObjDateTime.AddMonths(-1).Year));
                        var Obj2 = ObjBALCompany.GetVolumeValuesforGraph(pkCompanyID, Convert.ToInt16(ObjDateTime.AddMonths(-1).Month), Convert.ToInt16(ObjDateTime.AddMonths(-1).Year));
                        PreviousTotalSales = Convert.ToDecimal(Obj1.ElementAt(0).TotalAmount);
                        PreviousTotalVolume = Convert.ToDecimal(Obj2.ElementAt(0).TotalAmount);
                    }
                    var Obj = ObjBALCompany.GetSalesValuesforGraph(pkCompanyID, Convert.ToInt16(ObjDateTime.AddMonths(i).Month), Convert.ToInt16(ObjDateTime.AddMonths(i).Year));
                    var Obj3 = ObjBALCompany.GetVolumeValuesforGraph(pkCompanyID, Convert.ToInt16(ObjDateTime.AddMonths(i).Month), Convert.ToInt16(ObjDateTime.AddMonths(i).Year));
                    TotalSalesPercentage = 0;
                    TotalVolumePercentage = 0;
                    if (PreviousTotalSales > 0)
                    {
                        TotalSalesPercentage = ((Convert.ToDecimal(Obj.ElementAt(0).TotalAmount) - PreviousTotalSales) / PreviousTotalSales) * 100;
                    }
                    if (PreviousTotalVolume > 0)
                    {
                        TotalVolumePercentage = ((Convert.ToDecimal(Obj3.ElementAt(0).TotalAmount) - PreviousTotalVolume) / PreviousTotalVolume) * 100;
                    }
                    XvalueText += ObjDateTime.AddMonths(i).ToString("MMM, yyyy") + "|";
                    ProductOldData += Obj3.ElementAt(0).ProductAmount.ToString() + ",";
                    ReportOldData += Obj3.ElementAt(0).ReportAmount.ToString() + ",";
                    TotalOldData += Obj3.ElementAt(0).TotalAmount.ToString() + ",";

                    ProductMaxValue = Convert.ToDecimal(Obj3.ElementAt(0).ProductAmount);
                    ReportMaxValue = Convert.ToDecimal(Obj3.ElementAt(0).ReportAmount);
                    TotalMaxValue = Convert.ToDecimal(Obj3.ElementAt(0).TotalAmount);

                    CurrentMonthTotalSales = Convert.ToDecimal(Obj.ElementAt(0).TotalAmount);
                    CurrentMonthTotalVolume = Convert.ToDecimal(Obj3.ElementAt(0).TotalAmount);

                    if (ProductMaxValue > CurrentProductMaxValue)
                    {
                        CurrentProductMaxValue = ProductMaxValue;
                    }
                    if (ReportMaxValue > CurrentReportMaxValue)
                    {
                        CurrentReportMaxValue = ReportMaxValue;
                    }
                    if (TotalMaxValue > CurrentTotalMaxValue)
                    {
                        CurrentTotalMaxValue = TotalMaxValue;
                    }
                    if (iCount != 0)
                    {
                        PreviousTotalSales = Convert.ToDecimal(Obj.ElementAt(0).TotalAmount);
                        PreviousTotalVolume = Convert.ToDecimal(Obj3.ElementAt(0).TotalAmount);
                    }
                    iCount++;
                }
                var ProductArray = ProductOldData.Remove(ProductOldData.Length - 1, 1).Split(',');
                var ReportArray = ReportOldData.Remove(ReportOldData.Length - 1, 1).Split(',');
                var TotalArray = TotalOldData.Remove(TotalOldData.Length - 1, 1).Split(',');
                for (int i = 0; i < ProductArray.Length; i++)
                {
                    decimal Value = Convert.ToDecimal(ProductArray[i].ToString());
                    if (CurrentProductMaxValue > 0)
                    {
                        Value = (Value / CurrentProductMaxValue) * 100;
                    }
                    ProductData += Value.ToString() + ",";
                }
                for (int j = 0; j < ReportArray.Length; j++)
                {
                    decimal Value = Convert.ToDecimal(ReportArray[j].ToString());
                    if (CurrentReportMaxValue > 0)
                    {
                        Value = (Value / CurrentReportMaxValue) * 100;
                    }
                    ReportData += Value.ToString() + ",";
                }
                for (int k = 0; k < TotalArray.Length; k++)
                {
                    decimal Value = Convert.ToDecimal(TotalArray[k].ToString());
                    if (CurrentTotalMaxValue > 0)
                    {
                        Value = (Value / CurrentTotalMaxValue) * 100;
                    }
                    TotalData += Value.ToString() + ",";
                }
                if (CurrentTotalMaxValue > 0)
                {
                    yaxisInterVal = CurrentTotalMaxValue;
                }
                else
                {
                    yaxisInterVal = 1;
                }
                XvalueText.Remove(XvalueText.Length - 1, 1);
                string ChartMarkers = "&chm=d,FF0000,0,0,12,0|d,FF0000,0,1,12,0|d,FF0000,0,2,12,0|d,FF0000,0,3,12,0|d,FF0000,0,4,12,0|d,FF0000,0,5,12,0";
                ChartMarkers += "|d,00FF00,1,0,12,0|d,00FF00,1,1,12,0|d,00FF00,1,2,12,0|d,00FF00,1,3,12,0|d,00FF00,1,4,12,0|d,00FF00,1,5,12,0";
                ChartMarkers += "|d,0000FF,2,0,12,0|d,0000FF,2,1,12,0|d,0000FF,2,2,12,0|d,0000FF,2,3,12,0|d,0000FF,2,4,12,0|d,0000FF,2,5,12,0";
                string strURL = "http://chart.apis.google.com/chart?cht=lc&chs=430x320&chd=t:" + ProductData.Remove(ProductData.Length - 1, 1) + "|" + ReportData.Remove(ReportData.Length - 1, 1) + "|" + TotalData.Remove(TotalData.Length - 1, 1) + "&chds=0,100&chxt=x,y&chxl=0:" + XvalueText.Remove(XvalueText.Length - 1, 1) + "&chls=3,1,0|3,1,0|3,1,0&chco=FF0000,00FF00,0000FF&chdl=Products|Reports|Total&chg=0,9,1,1&" + ChartMarkers + "&chxr=1,0," + yaxisInterVal + "&chds=0,100&chg=20,6,2,0" + "";

                // return Json(new { imgBigImage = strURL}); if we Not using ND-26 then uncomment this line and comment next line
                return Json(new { imgBigImage = strURL, lblVolume = showTotlaVolumeValue, lblSales = showTotalSalesValue, lblvolum1 = adminclient_totalvolume, lblsales1 = adminclient_totalsale });
            }
            else
            {
                return Json(new { lblVolume = 0, lblSales = 0, imgSalesGraph = 0, lblvolum1 = 0, lblsales1 = 0 });
            }
        }
        #endregion

        #region// Grid Bind Method
        public ActionResult GetAllClients(int page, int pageSize, List<GridSort> sort, List<GridFilter> filters, string Keyword)
        {
            Guid UserId = Utility.GetUID(User.Identity.Name);

            BALLeads ObjBALLeads = new BALLeads();

            MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));

            Guid Managerid = Guid.Empty;
            byte FilterAction = 2;
            string FilterColumn = string.Empty;
            string FilterValue = string.Empty;
            if (Roles.IsUserInRole(User.Identity.Name, "SalesManager"))
            {
                Guid UserId1 = Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
                Managerid = UserId1;
            }
            Guid saleid = Guid.Empty;
            string Searchkeyword = "";
            short pkCompanyTypeId = -1;
            var Status = "SelectStatus";
            int Month = 0;
            int Year = 0;
            string Direction = "Desc";
            string ColumnName = "CreatedDate";

            string ToDate = "";
            string FromDate = "";
            ClientsModel objClientmodel = new ClientsModel();
            if (sort != null)
            {
                Direction = sort[0].Dir;
                ColumnName = sort[0].Field;
            }

            if (filters != null)
            {
                if (filters.Count() == 1)
                {
                    FilterAction = 1;
                    FilterColumn = filters[0].Field;
                    FilterValue = filters[0].Value;
                }
                else
                {
                    int pkClientDateId = Convert.ToInt32(filters[0].Value);

                    if (pkClientDateId != -5)
                    {

                        Month = Convert.ToInt32(filters[6].Value);
                        Year = Convert.ToInt32(filters[7].Value);
                        Status = filters[2].Value;
                        if (filters[1].Value == null)
                        {
                            pkCompanyTypeId = -1;

                        }
                        else
                        {
                            pkCompanyTypeId = Convert.ToInt16(filters[1].Value);
                        }

                        if (filters[3].Value == null)
                        {
                            Managerid = Guid.Empty;

                        }
                        else
                        {
                            Managerid = Guid.Parse(filters[3].Value);
                        }
                        if (filters[4].Value == null)
                        {
                            Searchkeyword = "";
                        }
                        else
                        {
                            Searchkeyword = filters[4].Value;
                        }


                        //  CreatedDate = filters[5].Value;

                        DateTime today = DateTime.Today;
                        if (pkClientDateId == 1)
                        {
                            ToDate = Convert.ToString(today.ToShortDateString());
                            FromDate = Convert.ToString(today.ToShortDateString());
                        }
                        else if (pkClientDateId == 2)
                        {
                            FromDate = new DateTime(today.Year, today.Month, 1).ToShortDateString();
                            ToDate = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month)).ToShortDateString();
                        }
                        else if (pkClientDateId == 3)
                        {
                            FromDate = new DateTime(today.Year, today.Month, 1).AddMonths(-1).ToShortDateString();
                            ToDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(-1).ToShortDateString();
                        }
                        else if (pkClientDateId == 4)
                        {
                            FromDate = new DateTime(Year, Month, 1).ToShortDateString();
                            ToDate = new DateTime(Year, Month, DateTime.DaysInMonth(Year, Month)).ToShortDateString();
                        }
                        else if (pkClientDateId == 5)
                        {
                            FromDate = filters[5].Value;
                            ToDate = filters[5].Value;
                        }
                        else if (pkClientDateId == 6)
                        {
                            FromDate = filters[8].Value;
                            ToDate = filters[9].Value;
                        }
                    }
                    else
                    {
                        Searchkeyword = string.Empty;
                    }

                }

            }
            else
            {
                Status = "";
            }
            if (Keyword != "" || Keyword != string.Empty)
            {
                Searchkeyword = Keyword;
            }
            var list = ObjBALLeads.GetAllClients(page, pageSize, true, Searchkeyword, pkCompanyTypeId, Status, ColumnName, Direction, FromDate, ToDate, Managerid, saleid, UserId, " ", FilterColumn, FilterAction, FilterValue);
            if (list.Count == 0)
            {
                objClientmodel.isnullRecord = true;
                return Json(new { Reclist = 0, TotalRec = 0 });

            }
            else
            {

                return Json(new { Reclist = list, TotalRec = list.ElementAt(0).TotalRec });
            }


        }
        #endregion
    }
}
