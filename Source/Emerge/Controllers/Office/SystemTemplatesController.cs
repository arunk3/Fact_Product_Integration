﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Common;
using Emerge.Data;
using Emerge.Services;

namespace Emerge.Controllers.Office
{
    public partial class OfficeController : Controller
    {
        //
        // GET: /SystemTemplates/

        public ActionResult SystemTemplates()
        {
            EmailSystemTemplatesModel ObjModel = new EmailSystemTemplatesModel();   
            List<Proc_USA_LoadEmailTemplatesResult> ObjEmailTemplate = GetEmailTemplate();
            ObjModel.SystemTemplatesList = ObjEmailTemplate;
            return View(ObjModel);
        }

        private static List<Proc_USA_LoadEmailTemplatesResult> GetEmailTemplate()
        {
            BALEmailSystemTemplates ObjBALEmailSystemTemplates = new BALEmailSystemTemplates();
            List<Proc_USA_LoadEmailTemplatesResult> ObjEmailTemplate = new List<Proc_USA_LoadEmailTemplatesResult>();
            ObjEmailTemplate = ObjBALEmailSystemTemplates.GetEmailTemplates(0, Utility.SiteApplicationId, true, 1);
            ObjEmailTemplate.Insert(0, new Proc_USA_LoadEmailTemplatesResult
            {
                pkTemplateId = 0,
                TemplateName = "<--Select Template-->",
                TemplateSubject = string.Empty,
                TemplateContent = string.Empty

            });
            return ObjEmailTemplate;
        }

        public ActionResult GetTemplateContents(string pkTemplateId)
        {
            BALEmailSystemTemplates ObjTemplate = new BALEmailSystemTemplates();
            List<Proc_USA_LoadEmailTemplatesResult> ObjRecords = ObjTemplate.GetEmailTemplates(Convert.ToInt32(pkTemplateId), Utility.SiteApplicationId, true, 1);

            return Json(new
            {
                TempName = ObjRecords.ElementAt(0).TemplateName,
                TempSub = ObjRecords.ElementAt(0).TemplateSubject,
                TempCont = ObjRecords.ElementAt(0).TemplateContent,
                IsTemplateEnabled = ObjRecords.ElementAt(0).IsEnabled,
                DefaultContent = ObjRecords.ElementAt(0).DefaultTemplateContent
            });
        }

        public ActionResult AddUpdateTemplate(EmailSystemTemplatesModel ObjModel, string hdEmailTemplate)
        {
            string strResult = "";
            List<Proc_USA_LoadEmailTemplatesResult> ObjEmailTemplate = new List<Proc_USA_LoadEmailTemplatesResult>();
            if (ObjModel != null)
            {
              
                    if (ObjModel.ddlTemplateName != "0")
                    {
                        strResult = UpdateEmailTemplate(ObjModel);
                    }
                    ObjEmailTemplate = GetEmailTemplate();
                    ObjModel.SystemTemplatesList = ObjEmailTemplate;
            }
            return Json(new { Message = strResult, Templates = ObjEmailTemplate, StatusAddUpdate = "Update" });

        }

        public string UpdateEmailTemplate(EmailSystemTemplatesModel ObjModel)
        {
            string strResult = "";
            BALEmailSystemTemplates ObjEmailTemplate = new BALEmailSystemTemplates();
            try
            {
                tblEmailTemplate ObjTemplate = new tblEmailTemplate();
                ObjTemplate.pkTemplateId = Convert.ToInt32(ObjModel.ddlTemplateName);
                ObjTemplate.fkApplicationId = Utility.SiteApplicationId;
                ObjTemplate.TemplateName = ObjModel.TemplateName;
                ObjTemplate.TemplateSubject = ObjModel.TemplateSubject;
                ObjTemplate.TemplateContent = Server.HtmlDecode(ObjModel.TemplateContent);
                ObjTemplate.LastModifiedDate = DateTime.Now;


                int iResult = ObjEmailTemplate.UpdateEmailTemplate(ObjTemplate);
                if (iResult >= 0)
                {
                    strResult = "<span class='successmsg'>Template updated succesfully</span>";
                   
                }
                else if (iResult == -1)
                {                   
                    strResult = "<span class='errormsg'>Template Name already exists</span>";
                }
                else
                {                    
                    strResult = "<span class='errormsg'>Some error occur, please try later</span>";
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strResult;
        }

    }
}
