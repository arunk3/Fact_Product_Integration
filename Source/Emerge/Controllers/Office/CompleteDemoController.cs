﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using System.Web.Security;
using System.Text;

namespace Emerge.Controllers.Office
{
    public partial class OfficeController : Controller
    {
        //
        // GET: /CompleteDemo/

        public ActionResult PartialCompleteDemo()
        {

            return View();
        }

        public ActionResult CompleteDemoBind(int page, int pageSize)
        {
            BALOffice ObjDemo = new BALOffice();
            int PageSize = pageSize;
            int iPageNo = page;
            int TotalRecords = 0;

            var DemoScheduleCollection = ObjDemo.GetDemoSchedule("DemoDateTime", "DESC", true, iPageNo, PageSize, 0);

            if (DemoScheduleCollection != null && DemoScheduleCollection.Count() > 0)
            {
                TotalRecords =Convert.ToInt32(DemoScheduleCollection.ElementAt(0).TotalRec);
            }

            return Json(new
            {
                Products = DemoScheduleCollection,
                TotalCount = DemoScheduleCollection.ElementAt(0).TotalRec
            },
                           JsonRequestBehavior.AllowGet);

        }


        public ActionResult CompleteDemoPopUp(int DemoScheduleId, int CompanyId)
        {
           // OfficeModel omd = new OfficeModel();
            BALLeads ObjBALLeads = new BALLeads();
            var collection = ObjBALLeads.GetTraineeByDemoIdCompanyId(DemoScheduleId, CompanyId);
            var total = collection.Count;
            StringBuilder sb = new StringBuilder();
            sb.Append("<table style='margin-top: -11px'>");
            sb.Append("<tr style='height:0px;'>");
            sb.Append("<td style='width:0px;'><input type='hidden' name='Demo' value=" + DemoScheduleId + "><input type='hidden' name='CompanyId' value=" + CompanyId + "><th align='left' scope='col'>Trainee Status</th></td>");
            sb.Append("<th align='left' scope='col'>Trainee Name</th>");
            sb.Append("</tr>");
            for (int i = 0; i < collection.Count(); i++)
            {
                bool status = collection.ElementAt(i).TraineeStatus;


                if (status == true)
                {
                    //  sb.Append("<tr><td><center><center></td><td align='left'><input type='checkbox' name='chkTraineeStatus' disabled='disabled' id=" + collection.ElementAt(i).pkCompanyTraineeId + " value=" + collection.ElementAt(i).TraineeStatus + " checked=" + collection.ElementAt(i).TraineeStatus + " /></td><td align='left'><center></center></td><td>" + collection.ElementAt(i).FirstName + "&nbsp;" + collection.ElementAt(i).LastName + "</td></tr>");
                    sb.Append("<tr><td><center><input type='hidden' name='chkTraineeStatus'  value=" + collection.ElementAt(i).pkCompanyTraineeId + " /><center></td><td align='center'><input type='checkbox' disabled='disabled' class='traineeStatus' name='chkTraineeStatus1' id=" + collection.ElementAt(i).pkCompanyTraineeId + " value=" + collection.ElementAt(i).TraineeStatus + " checked=" + collection.ElementAt(i).TraineeStatus + "  /></td><td align='left'>" + collection.ElementAt(i).FirstName + "&nbsp;" + collection.ElementAt(i).LastName + "</td></tr>");
                }
                else
                {
                    sb.Append("<tr><td><center><input type='hidden' name='pkid' value=" + collection.ElementAt(i).pkCompanyTraineeId + " /><center></td><td align='center'><input type='checkbox' class='traineeStatus' name='chkTraineeStatus' id=" + collection.ElementAt(i).pkCompanyTraineeId + " value=" + collection.ElementAt(i).pkCompanyTraineeId + "   /></td><td align='left'>" + collection.ElementAt(i).FirstName + "&nbsp;" + collection.ElementAt(i).LastName + "</td></tr>");
                }
            }
            sb.Append("</table>");
            return Json(new { collec = collection, Total = total, label = sb.ToString() });
        }


        [HttpPost]
        public ActionResult UpdateDemoPopUp(FormCollection Form)
        {
            string[] ID = new string[] { };
            string StrResult = "";
           // bool isDemoStatusUpdate = true;
            BALOffice ObjBALOffice = new BALOffice();
            BALCompany objBALCompany = new BALCompany();

            var TraineeStatus = Request.Form["chkTraineeStatus"];
            if (TraineeStatus != null)
            {
                ID = TraineeStatus.Split(',');
            }
            var comments = Request.Form["comments"];
            for (int J = 0; J < ID.Count(); J++)
            {

                int TraineeID = Convert.ToInt32(ID[J].ToString());
                ObjBALOffice.UpdateTraineeStatus(TraineeID, true);

            }

            if (Request.Form["chkTraineeStatus"] != null)
            {
                var fkDemoScheduleId = Request.Form["Demo"];
                var Company_Id = Request.Form["CompanyId"];
                byte demostatus = Convert.ToByte(Request.Form["DemoStatus"]);

                int CompanyId = Convert.ToInt32(Company_Id.ToString());
                int DemoScheduleId = Convert.ToInt32(fkDemoScheduleId.ToString());
                string SalesManagerEmailId = objBALCompany.GetSalesManagerEmailByCompanyId(CompanyId);
                //bool checksend = true; //
                bool checksend=sendMailToSalesManager(SalesManagerEmailId);

                if (checksend)
                {
                    ObjBALOffice.UpdateDemoStatus(DemoScheduleId, comments.Trim(), demostatus);
                    Guid UserId;
                    MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));

                    if (ObjMembershipUser != null)
                    {
                        UserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
                        DateTime CreatedDate = DateTime.Now;
                        //int result = 
                            AddCommentToCallLog(CompanyId, UserId, "##13##", CreatedDate);
                    }

                    StrResult = "<span class='successmsg'>Scheduled Demo status updated successfully.</span>";


                }
                else
                {

                    StrResult = "<span class='infomessage'>Some error occured while sending mail.</span>";


                }
            }
            else
            {
                StrResult = "<span class='infomessage'>Some error occured while sending mail.</span>";
            }



            return Json(new { Message = StrResult });
        }


        private bool sendMailToSalesManager(string SalesManagerEmailId)
        {
            bool CheckSend = false;
            string UserEmails = string.Empty;
            BALGeneral ObjBALGeneral = new BALGeneral();
            try
            {
                MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));

                string emailFrom = ObjMembershipUser.Email;
                if (!string.IsNullOrEmpty(emailFrom))
                {
                    UserEmails = ObjBALGeneral.GetUserOtherMailIds(emailFrom);
                }


                #region Replace Bookmark
                string MessageBody = "";
                string emailText = "";

                string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;
                var emailContent = ObjBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, Convert.ToInt32(EmailTemplates.DemoCompleted));
                emailText = emailContent.TemplateContent;

                BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                Bookmarks objBookmark = new Bookmarks();
                objBookmark.EmailContent = emailText;
                objBookmark.Date = Request.Form["DemoDateTimes"].ToString();
                objBookmark.CompanyName = Request.Form["CompanyNames"].ToString();
                objBookmark.MainContact = Request.Form["MainContactPersonNames"].ToString();
                objBookmark.MainEmail = Request.Form["MainEmailIds"].ToString();
                objBookmark.MainPhone = Request.Form["PhoneNo1s"].ToString();
                objBookmark.Comments = Request.Form["comments"].ToString() == null || Request.Form["comments"].ToString() == string.Empty ? "NA" : Request.Form["comments"].ToString().Trim();
                objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                objBookmark.LoginLink = "<a style='color: #87ceea;' href='" + ApplicationPath.GetApplicationPath() + "'> Click here </a>";
                MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Utility.GetUID(User.Identity.Name));


                #endregion
                string emailTo = SalesManagerEmailId;
                //for ticket #101

                CheckSend = Utility.SendMail(emailTo, UserEmails, emailFrom, MessageBody, "Demo Completed.");
                //CheckSend = Utility.SendMail(emailTo,UserEmails,string.Empty, emailFrom, MessageBody, "Demo Completed.");
            }
            catch
            {

            }
            return CheckSend;
        }


        private int AddCommentToCallLog(int pkCompanyId, Guid UserId, string LeadComments, DateTime CreatedDate)
        {
            BALLeads ObjBALLeads = new BALLeads();
            int Result = 0;
            try
            {
                Result = ObjBALLeads.AddCommentsToCallLog(pkCompanyId, UserId, LeadComments, CreatedDate);
            }
            catch (Exception)
            {

            }
            finally
            {
                ObjBALLeads = null;
            }
            return Result;
        }

    }
}
