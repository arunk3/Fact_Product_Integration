﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Emerge.Models;
using System.IO;
using Emerge.Common;
using System.Web.UI;
using Emerge.Data;

namespace Emerge.Controllers.Office
{
    public partial class OfficeController : Controller
    {
      
        // Page Load Method
        public ActionResult ManageDocuments()
        {

            ManageDocumentsModel ObjModel = new ManageDocumentsModel();
            BALOffice objBalOffice = new BALOffice();
            var doclist = objBalOffice.GetAllDocuments();
            ObjModel.ObjOfficeDocuments = doclist;

            ObjModel.Message = string.Empty;
            if (Request.QueryString["Status"] != null)
            {
                int status = Convert.ToInt32(Request.QueryString["Status"]);
                if (status == 1)
                {
                    ObjModel.Message = "<span class='successmsg'>Documents  updated successfully</span>";
                }
                else if (status == 2)
                {
                    ObjModel.Message = "<span class='successmsg'>Documents added successfully</span>";
                }
                else if (status == 3)
                {
                    ObjModel.Message = "<span class='successmsg'>Documents deleted successfully</span>";
                }
                else if (status == 4)
                {
                    ObjModel.Message = "<span class='successmsg'>Settings updated successfully.</span>";
                }
                else if (status == -1)
                {
                    ObjModel.Message = "<span class='errormsg'>Title name already exist</span>";
                }
                else
                {
                    ObjModel.Message = "<span class='errormsg'>error is occured while updating Documents</span>";
                }
            }
            return View(ObjModel);

        }
        
        // Add New Documnts  Method
        [HttpPost]
        public ActionResult AddDocuments(FormCollection CollectionObj)
        {


            string strSuccess = string.Empty;

            try
            {
            if (Request.Files[0].FileName != "")
            {
                string sExt = Path.GetExtension(Request.Files[0].FileName).ToLower();
                string sFileName = DateTime.Now.ToFileTime().ToString() + sExt;
                //string sFilePath = "";
                //var abc = 1;



                tblOfficeDocument ObjtblOfficeDocument = new tblOfficeDocument();
                //ObjtblOfficeDocument.pkDocumentId = Guid.NewGuid();
                ObjtblOfficeDocument.DocumentTitle = Convert.ToString(CollectionObj.GetValue("Title").AttemptedValue);
                ObjtblOfficeDocument.DocumentPath = sFileName;
                ObjtblOfficeDocument.CreatedDate = DateTime.Now;

                BALOffice ObjBALOffice = new BALOffice();
                var Success = ObjBALOffice.InsertDocumentDetails(ObjtblOfficeDocument);

                if (Success > 0)
                {

                    HttpPostedFileBase ObjHttpPostedFile = Request.Files[0];
                    ObjHttpPostedFile.SaveAs(Server.MapPath(@"~\Resources\Upload\OfficeDocuments\") + sFileName);

                    BALEmailSystemTemplates ObjEmailTemplate = new BALEmailSystemTemplates();
                    ObjEmailTemplate.InsertAlerts(Success, DateTime.Now);
                    strSuccess = "2";// "<span class='successmsg'>Feature Updated Successfully</span>";
                }

                else if (Success == -1)
                {
                    strSuccess = "-1";// "<span class='errormsg'>Title name already exist</span>";
                }
                else
                {
                    strSuccess = "-2"; //"<span class='errormsg'>error is occured while updating content</span>";
                }
            }
            }

            catch (Exception)
            {
            }
            return RedirectToAction("ManageDocuments", new { Status = strSuccess });
           
        }
        
        // Update Documents
        [HttpPost]
        public ActionResult UpdateDocuments(FormCollection ObjCollection)
        {

            BALOffice ObjBALoffice = new BALOffice();
            string strSuccess = string.Empty;

            try
            {
               // DateTime LastModifiedDate = DateTime.Now;
                string sFileName = string.Empty;

                if (Request.Files[0].FileName != "")
                {
                    string sExt = Path.GetExtension(Request.Files[0].FileName).ToLower();
                    sFileName = DateTime.Now.ToFileTime().ToString() + sExt;
                    //string sFilePath = "";
                    //sFilePath = Server.MapPath("~/Resources/Upload/OfficeDocuments/");

                    HttpPostedFileBase ObjHttpPostedFile = Request.Files[0];
                    ObjHttpPostedFile.SaveAs(Server.MapPath(@"~\Resources\Upload\OfficeDocuments\") + sFileName);

                    string OldFilePath = Server.MapPath(@"~\Resources\Upload\OfficeDocuments\") + Convert.ToString(ObjCollection.GetValue("DocumentPath").AttemptedValue);
                    Utility.DeleteFile(OldFilePath);
                }
                else
                {
                    sFileName = Convert.ToString(ObjCollection.GetValue("DocumentPath").AttemptedValue);
                }

                tblOfficeDocument ObjtblOfficeDocument = new tblOfficeDocument();
                ObjtblOfficeDocument.pkDocumentId = Int32.Parse(ObjCollection.GetValue("pkDocumentId").AttemptedValue);
                ObjtblOfficeDocument.DocumentTitle = Convert.ToString(ObjCollection.GetValue("Title").AttemptedValue);
                ObjtblOfficeDocument.DocumentPath = sFileName;
                ObjtblOfficeDocument.LastModifiedDate = DateTime.Now;

                int Success = 0;

                Success = ObjBALoffice.UpdateDocumentDetails(ObjtblOfficeDocument);

                if (Success == 1)
                {
                    strSuccess = "1";
                }
                else if (Success == -1)
                {
                    strSuccess = "-1";
                }
                else
                {
                    strSuccess = "-2"; 
                }

            }
            catch(Exception)
            { 

            }
            return RedirectToAction("ManageDocuments", new { Status = strSuccess });
        }
        
        // Download Agreement OF PDF Format
        public ActionResult DownloadDocumentsFile(string FPathTitle)
        {
            string[] str = FPathTitle.Split('_');
            string FileName = str[0];
            string Title = str[1].Replace(" ", "_");
            string FilePath = Request.PhysicalApplicationPath + "Resources/Upload/OfficeDocuments/" + FileName;
            string fileext = System.IO.Path.GetExtension(FileName);
            string CType = "application/" + fileext.Replace(".", "");
            //string fileName = Path.GetFileName(FileName);

            string strFileTitle = Title + fileext;



            if (System.IO.File.Exists(FilePath))
            {
                return File(FilePath, CType, strFileTitle);
            }
            else
            {
                FilePath = Request.PhysicalApplicationPath + "Resources/Upload/OfficeDocuments/no-image.jpg";
                return File(FilePath, CType, strFileTitle);
            }


           
        }
        
        
        // Delete Documents
        [HttpPost]
        public ActionResult DeleteDocuments(int pkDocumentsId)
        {

            int Success = 0;
            BALOffice objbaloffice = new BALOffice();
            try
            {


                Success = objbaloffice.DeleteDocumentsByDocId(pkDocumentsId);
                if(Success ==1)
                {
                    Success = 3;

                }
                else
                {
                    Success = -2;
                }

            }
            catch
            { 
            }
            return Json(Success);
        }
    }
}
