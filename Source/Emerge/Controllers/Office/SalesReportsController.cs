﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Emerge.Controllers.Office
{
    public partial class OfficeController : Controller
    {
        public ActionResult SalesReport()
        {
            return View();
        }

        public ActionResult AdminSalesReport()
        {
            return View();
        }

        public PartialViewResult PartialSalesReport(string Month, string Year, string ReferenceNo, string CompanyUserId, string LocationId, string SalesRep, string CompanyId,string Status)
        {
            //INT69
            ViewData["appId"] = "65d1e085-eb17-4f29-96d0-247b0c1ee852";
            ViewData["Month"] = Month;
            ViewData["Year"] = Year;
            //ViewData["CompanyId"] = "7dc87d77-6bb5-4bb3-9837-fa861f36f281";
            ViewData["CompanyId"] = !string.IsNullOrEmpty(CompanyId) ? (CompanyId.Contains("All Companies") ? "0" : CompanyId) : "0";
            ViewData["UserId"] = (!string.IsNullOrEmpty(CompanyUserId) && CompanyUserId != "null") ? CompanyUserId : "0";
            ViewData["LocationId"] = !string.IsNullOrEmpty(LocationId) ? LocationId : "0";
            ViewData["SalesRep"] = !string.IsNullOrEmpty(SalesRep) ? SalesRep : Guid.Empty.ToString();
            ViewData["RefCode"] = !string.IsNullOrEmpty(ReferenceNo) ? ReferenceNo : "0";
            ViewData["CompanyStatus"] = !string.IsNullOrEmpty(Status) ? Status  : "-1";

            return PartialView("../Common/PartialSalesReport");
        }


    }
}
