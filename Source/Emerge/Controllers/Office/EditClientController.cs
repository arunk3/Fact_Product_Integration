﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using System.Web.Security;
using System.Text;
using System.IO;
using System.Web.UI.WebControls;

namespace Emerge.Controllers.Office
{
    public partial class OfficeController : Controller
    {
        int ObjGuid_LocationId = 0;
        //
        // GET: /EditClient/

        public ActionResult EditClient()
        {
            ClientsModel objModel = new ClientsModel();

            BALCompany objbalCompany = new BALCompany();

            //BALLocation ObjLocation = new BALLocation();
            List<SelectListItem> PermissiblePurposeList = new List<SelectListItem>();
            PermissiblePurposeList = GetpermissiblepurposeList();
            ViewBag.IsActive = 3;
            if (Request.QueryString["Id"] != null)
            {

                objModel.IsEdit = true;
                ViewBag.locationid = Int32.Parse(Request.QueryString["Id"]);
                ViewBag.bck = Request.QueryString["bck"];
                var locationid1 = Int32.Parse(Request.QueryString["Id"]);
                var list1 = objbalCompany.GetCompanyDetailByLocationId(locationid1);
                ViewBag.Status = list1.ElementAt(0).LeadStatus;
                int CompanyId = (list1.ElementAt(0).fkCompanyId != 0) ? Int32.Parse(list1.ElementAt(0).fkCompanyId.ToString()) : 0;
                ViewBag.IsEnabled = objbalCompany.IsEnableCompany(CompanyId);

                Session["pkLocationIdWhenpopup"] = Int32.Parse(Request.QueryString["Id"]);
                ViewData["PkCompanyId"] = list1.ElementAt(0).fkCompanyId;
                Session["fkcompanyid"] = list1.ElementAt(0).fkCompanyId;

                objModel.pkPermissibleId = Convert.ToInt32(list1.ElementAt(0).PermissiblePurpose);
                //objModel.PermissiblePurposeList = PermissiblePurposeList.Select(x => new SelectListItem { Text = x.Text.ToString(), Value = x.Value.ToString() }).ToList();
                //objModel.PermissiblePurposeList = objModel.PermissiblePurposeList.Select(x => new SelectListItem { Text = x.Text.ToString(), Value = x.Value.ToString(), Selected = (x.Value == objModel.PermissiblePurpose) }).ToList();
                AddRecentReview(CompanyId);
                objModel.CompnayDocList = objbalCompany.GetAllCompanyDocumentsByCompanyId(CompanyId);
                ViewBag.comment = objbalCompany.GetAllCompanyDocumentsByCompanyId(CompanyId);
                BALCompany ObjBALCompany = new BALCompany();
                int fkCompanyId;
                fkCompanyId = Int32.Parse(CompanyId.ToString());
               // List<Proc_Get_CommentsForLeadResult> ObjDbCollection = new List<Proc_Get_CommentsForLeadResult>();
               // int fk_Companyid = (list1.ElementAt(0).fkCompanyId != null) ? Int32.Parse(list1.ElementAt(0).fkCompanyId.ToString()) : 0;
                bool? IsActive = objbalCompany.GetLatestOrderDate((list1.ElementAt(0).fkCompanyId != 0) ? Int32.Parse(list1.ElementAt(0).fkCompanyId.ToString()) : 0);
                //string Company_Name = !string.IsNullOrEmpty(list1.ElementAt(0).CompanyName) ? list1.ElementAt(0).CompanyName : string.Empty;
               // string Email_id = !string.IsNullOrEmpty(list1.ElementAt(0).MainEmailId) ? list1.ElementAt(0).MainEmailId : string.Empty;
                if (IsActive != null)
                {
                    ViewBag.IsActive = IsActive;
                    if (IsActive == true)
                    {
                        //imgbtn_6.ImageUrl = "~/Resources/Images/emerge-office-milestone-5-on.png";
                        ViewBag.IsActive = 1;
                        //UpdateCallLog("imgbtn_5", fk_Companyid);
                        //UpdateCompanyIsActive(0, fk_Companyid);

                        //sendStatusChangedMail(5, Company_Name, Email_id);




                    }
                    else
                    {
                        //imgbtn_6.ImageUrl = "~/Resources/Images/emerge-office-milestone-6-on.png";
                        ViewBag.IsActive = 2;
                        //    UpdateCallLog("imgbtn_6", fk_Companyid);
                        //    UpdateCompanyIsActive(1, fk_Companyid);
                        //    sendStatusChangedMail(6, Company_Name, Email_id);
                    }
                }

                objModel.CollGetCommentsForLeadResult = ObjBALCompany.GetCommentsForCompany(fkCompanyId);

                objModel.FirstReview = list1.ElementAt(0).FirstReportRanDate == null ? "NA" : list1.ElementAt(0).FirstReportRanDate.ToString();
                objModel.LastReview = list1.ElementAt(0).LastReviewDate == null ? "NA" : list1.ElementAt(0).LastReviewDate.ToString();
                objModel.MemberSince = list1.ElementAt(0).CreatedDate;
                objModel.pkCompanyId = (list1.ElementAt(0).fkCompanyId != 0) ? Int32.Parse(list1.ElementAt(0).fkCompanyId.ToString()) : 0;
                objModel.fkStateId = Convert.ToInt32(list1.ElementAt(0).fkStateID);
                objModel.CityName = list1.ElementAt(0).City;
                BALCompanyType Obj_BALCompanyType = new BALCompanyType();
                string Cityname = list1.ElementAt(0).City;
                if (string.IsNullOrEmpty(Cityname) == false)
                {

                    var coll_cityid = Obj_BALCompanyType.GetCity_Id(Cityname);
                    ViewBag.City_id = coll_cityid.Item1;
                }





                objModel.pkCompanyTypeId = list1.ElementAt(0).fkCompanyTypeId;
                objModel.fkCompanyTypeId = list1.ElementAt(0).fkCompanyTypeId;
                objModel.fkUserId = list1.ElementAt(0).fkSalesManagerId;
                objModel.fkSalesAssociateId = list1.ElementAt(0).fkSalesRepId;
                ViewData["date"] = list1.Select(x => x.CreatedDate).ToList();

                objModel.CompanyType = list1.ElementAt(0).CompanyName;
                objModel.CompanyName = list1.ElementAt(0).CompanyName;
                objModel.CompanyAccountNumber = list1.ElementAt(0).CompanyAccountNumber;
                objModel.Address1 = list1.ElementAt(0).Address1;

                objModel.fkStateId = Convert.ToInt32(list1.ElementAt(0).fkStateID);

                objModel.Address2 = list1.ElementAt(0).Address2;
                objModel.ZipCod = list1.ElementAt(0).ZipCode;
                objModel.MainContact = list1.ElementAt(0).MainContactPersonName;
                Session["MaincontactPerson"] = list1.ElementAt(0).MainContactPersonName;
                objModel.Review = list1.ElementAt(0).ReviewComments == null ? "NA" : list1.ElementAt(0).ReviewComments.ToString();
                objModel.CompanyNote = list1.ElementAt(0).CompanyNote;
                objModel.MainEmail = list1.ElementAt(0).MainEmailId;
                Session["MainContactaEmail"] = list1.ElementAt(0).MainEmailId;
                objModel.MainFaxNumber = list1.ElementAt(0).Fax;
                objModel.MainPhone = list1.ElementAt(0).PhoneNo1;
                ViewBag.State_Code = string.Empty;
                ViewBag.State_Code = GetStateCode(objModel.fkStateId);

                objModel.BillingContact = list1.ElementAt(0).BillingContactPersonName;
                Session["BillingcontactPerson"] = list1.ElementAt(0).BillingContactPersonName;
                objModel.BillingEmail = list1.ElementAt(0).BillingEmailId;
                Session["BillingContactactEmail"] = list1.ElementAt(0).BillingEmailId;
                objModel.BillingPhone = list1.ElementAt(0).PhoneNo2;
                objModel.FaxNumber2 = list1.ElementAt(0).Fax2;
                objModel.Rating = list1.ElementAt(0).Rating.ToString();
                ViewBag.LocationId = list1.ElementAt(0).pkLocationId;
                ViewBag.CompanyId = list1.ElementAt(0).fkCompanyId;
                ViewBag.Status = list1.ElementAt(0).LeadStatus;
                ViewBag.IsEnabled = objbalCompany.IsEnableCompany((list1.ElementAt(0).fkCompanyId != 0) ? Int32.Parse(list1.ElementAt(0).fkCompanyId.ToString()) : 0);


            }
            return View(objModel);




        }


        public ActionResult ClientBillingStatus(string pkLocationId)
        {
            ClientsModel objModel = new ClientsModel();

            BALCompany objbalCompany = new BALCompany();

           // BALLocation ObjLocation = new BALLocation();
            //List<SelectListItem> PermissiblePurposeList = new List<SelectListItem>();
            //PermissiblePurposeList = GetpermissiblepurposeList();
            ViewBag.IsActive = 3;
            if (Request.QueryString["Id"] != null)
            {

                objModel.IsEdit = true;
                ViewBag.locationid = Int32.Parse(Request.QueryString["Id"]);
                ViewBag.bck = Request.QueryString["bck"];
                var locationid1 = Int32.Parse(Request.QueryString["Id"]);
                var list1 = objbalCompany.GetCompanyDetailByLocationId(locationid1);
                ViewBag.Status = list1.ElementAt(0).LeadStatus;
                int CompanyId = (list1.ElementAt(0).fkCompanyId != 0) ? Int32.Parse(list1.ElementAt(0).fkCompanyId.ToString()) : 0;
                ViewBag.IsEnabled = objbalCompany.IsEnableCompany(CompanyId);

                Session["pkLocationIdWhenpopup"] = Int32.Parse(Request.QueryString["Id"]);
                ViewData["PkCompanyId"] = list1.ElementAt(0).fkCompanyId;
                Session["fkcompanyid"] = list1.ElementAt(0).fkCompanyId;

                objModel.pkPermissibleId = Convert.ToInt32(list1.ElementAt(0).PermissiblePurpose);
                objModel.fkStateId = Convert.ToInt32(list1.ElementAt(0).fkStateID);
                objModel.CompnayDocList = objbalCompany.GetAllCompanyDocumentsByCompanyId(CompanyId);
                ViewBag.comment = objbalCompany.GetAllCompanyDocumentsByCompanyId(CompanyId);
                BALCompany ObjBALCompany = new BALCompany();
                int fkCompanyId;
                fkCompanyId = Int32.Parse(CompanyId.ToString());
                //List<Proc_Get_CommentsForLeadResult> ObjDbCollection = new List<Proc_Get_CommentsForLeadResult>();
                //int fk_Companyid = (list1.ElementAt(0).fkCompanyId != null) ? Int32.Parse(list1.ElementAt(0).fkCompanyId.ToString()) : 0;
                bool? IsActive = objbalCompany.GetLatestOrderDate((list1.ElementAt(0).fkCompanyId != 0) ? Int32.Parse(list1.ElementAt(0).fkCompanyId.ToString()) : 0);
                //string Company_Name = !string.IsNullOrEmpty(list1.ElementAt(0).CompanyName) ? list1.ElementAt(0).CompanyName : string.Empty;
                //string Email_id = !string.IsNullOrEmpty(list1.ElementAt(0).MainEmailId) ? list1.ElementAt(0).MainEmailId : string.Empty;
                if (IsActive != null)
                {
                    ViewBag.IsActive = IsActive;
                    if (IsActive == true)
                    {
                        ViewBag.IsActive = 1;

                    }
                    else
                    {
                        ViewBag.IsActive = 2;
                    }
                }

                objModel.BillingContact = list1.ElementAt(0).BillingContactPersonName;
                Session["BillingcontactPerson"] = list1.ElementAt(0).BillingContactPersonName;
                objModel.BillingEmail = list1.ElementAt(0).BillingEmailId;
                Session["BillingContactactEmail"] = list1.ElementAt(0).BillingEmailId;
                objModel.BillingPhone = list1.ElementAt(0).PhoneNo2;
                objModel.FaxNumber2 = list1.ElementAt(0).Fax2;
                objModel.Rating = list1.ElementAt(0).Rating.ToString();


                objModel.CollGetCommentsForLeadResult = ObjBALCompany.GetCommentsForCompany(fkCompanyId);
                objModel.CompanyName = list1.ElementAt(0).CompanyName;
                objModel.CityName = list1.ElementAt(0).City;
                ViewBag.State_Code = string.Empty;
                ViewBag.State_Code = GetStateCode(objModel.fkStateId);
                objModel.pkCompanyId = (list1.ElementAt(0).fkCompanyId != 0) ? Int32.Parse(list1.ElementAt(0).fkCompanyId.ToString()) : 0;
                ViewBag.LocationId = list1.ElementAt(0).pkLocationId;
                ViewBag.CompanyId = list1.ElementAt(0).fkCompanyId;
                ViewBag.Status = list1.ElementAt(0).LeadStatus;
                ViewBag.IsEnabled = objbalCompany.IsEnableCompany((list1.ElementAt(0).fkCompanyId != 0) ? Int32.Parse(list1.ElementAt(0).fkCompanyId.ToString()) : 0);
                LoadSalesGraph((int)list1.ElementAt(0).fkCompanyId);
                LoadVolumeGraph((int)list1.ElementAt(0).fkCompanyId);

            }
            return View(objModel);

        }

        public ActionResult ClientDataSourceInfo(string pkLocationId)
        {
            ClientsModel objModel = new ClientsModel();

            BALCompany objbalCompany = new BALCompany();

            //BALLocation ObjLocation = new BALLocation();
            //List<SelectListItem> PermissiblePurposeList = new List<SelectListItem>();
            //PermissiblePurposeList = GetpermissiblepurposeList();
            ViewBag.IsActive = 3;
            if (Request.QueryString["Id"] != null)
            {

                objModel.IsEdit = true;
                ViewBag.locationid = Int32.Parse(Request.QueryString["Id"]);
                ViewBag.bck = Request.QueryString["bck"];
                var locationid1 = Int32.Parse(Request.QueryString["Id"]);
                var list1 = objbalCompany.GetCompanyDetailByLocationId(locationid1);
                ViewBag.Status = list1.ElementAt(0).LeadStatus;
                int CompanyId = (list1.ElementAt(0).fkCompanyId != 0) ? Int32.Parse(list1.ElementAt(0).fkCompanyId.ToString()) : 0;
                ViewBag.IsEnabled = objbalCompany.IsEnableCompany(CompanyId);

                Session["pkLocationIdWhenpopup"] = Int32.Parse(Request.QueryString["Id"]);
                ViewData["PkCompanyId"] = list1.ElementAt(0).fkCompanyId;
                Session["fkcompanyid"] = list1.ElementAt(0).fkCompanyId;

                objModel.pkPermissibleId = Convert.ToInt32(list1.ElementAt(0).PermissiblePurpose);
                objModel.fkStateId = Convert.ToInt32(list1.ElementAt(0).fkStateID);
                objModel.CompnayDocList = objbalCompany.GetAllCompanyDocumentsByCompanyId(CompanyId);
                ViewBag.comment = objbalCompany.GetAllCompanyDocumentsByCompanyId(CompanyId);
                BALCompany ObjBALCompany = new BALCompany();
                int fkCompanyId;
                fkCompanyId = Int32.Parse(CompanyId.ToString());
                //List<Proc_Get_CommentsForLeadResult> ObjDbCollection = new List<Proc_Get_CommentsForLeadResult>();
                //int fk_Companyid = (list1.ElementAt(0).fkCompanyId != null) ? Int32.Parse(list1.ElementAt(0).fkCompanyId.ToString()) : 0;
                bool? IsActive = objbalCompany.GetLatestOrderDate((list1.ElementAt(0).fkCompanyId != 0) ? Int32.Parse(list1.ElementAt(0).fkCompanyId.ToString()) : 0);
                //string Company_Name = !string.IsNullOrEmpty(list1.ElementAt(0).CompanyName) ? list1.ElementAt(0).CompanyName : string.Empty;
                //string Email_id = !string.IsNullOrEmpty(list1.ElementAt(0).MainEmailId) ? list1.ElementAt(0).MainEmailId : string.Empty;
                if (IsActive != null)
                {
                    ViewBag.IsActive = IsActive;
                    if (IsActive == true)
                    {
                        ViewBag.IsActive = 1;

                    }
                    else
                    {
                        ViewBag.IsActive = 2;
                    }
                }
                objModel.CollGetCommentsForLeadResult = ObjBALCompany.GetCommentsForCompany(fkCompanyId);
                objModel.CompanyName = list1.ElementAt(0).CompanyName;
                objModel.CityName = list1.ElementAt(0).City;
                ViewBag.State_Code = string.Empty;
                ViewBag.State_Code = GetStateCode(objModel.fkStateId);
                objModel.pkCompanyId = (list1.ElementAt(0).fkCompanyId != 0) ? Int32.Parse(list1.ElementAt(0).fkCompanyId.ToString()) : 0;
                ViewBag.LocationId = list1.ElementAt(0).pkLocationId;
                ViewBag.CompanyId = list1.ElementAt(0).fkCompanyId;
                ViewBag.Status = list1.ElementAt(0).LeadStatus;
                ViewBag.IsEnabled = objbalCompany.IsEnableCompany((list1.ElementAt(0).fkCompanyId != 0) ? Int32.Parse(list1.ElementAt(0).fkCompanyId.ToString()) : 0);

            }
            return View(objModel);

        }
        private List<SelectListItem> GetpermissiblepurposeList()
        {
            List<SelectListItem> listpermissiblepurpose = new List<SelectListItem>();
            //listpermissiblepurpose.Add(new SelectListItem { Text = "--Select--", Value = "-1" });
            listpermissiblepurpose.Add(new SelectListItem { Text = "Employment Screening", Value = "Employment Screening" });
            listpermissiblepurpose.Add(new SelectListItem { Text = "Credit Reporting", Value = "Credit Reporting" });
            listpermissiblepurpose.Add(new SelectListItem { Text = "Insurance Underwriting", Value = "Insurance Underwriting" });
            listpermissiblepurpose.Add(new SelectListItem { Text = "Tenant Screening", Value = "Tenant Screening" });
            listpermissiblepurpose.Add(new SelectListItem { Text = "Debt Recovery / Collections", Value = "Debt Recovery / Collections" });
            listpermissiblepurpose.Add(new SelectListItem { Text = "Other", Value = "Other" });
            return listpermissiblepurpose;
        }

        public ActionResult Getpermissiblepurpose()
        {
            BALCompanyType serboj = new BALCompanyType();
            var list = serboj.GetPermissiblePurpose();
            return Json(list);
        }


        public ActionResult GetContactNamelist()
        {

            //AddEditLeadsModel objmodel = new AddEditLeadsModel();
            List<SelectListItem> list = new List<SelectListItem>();
            if (Session["fkcompanyid"] != null)
            {
                //string pkcomapnyidwhenedit = Session["fkcompanyid"].ToString();
                string maincontact = Session["MaincontactPerson"].ToString();
                string mainemail = Session["MainContactaEmail"].ToString();
                string billinContact = Session["BillingcontactPerson"].ToString();
                string billingemail = Session["BillingContactactEmail"].ToString();
                if (billinContact != maincontact)
                {
                    list.Add(new SelectListItem { Text = maincontact, Value = mainemail });
                    list.Add(new SelectListItem { Text = billinContact, Value = billingemail });

                }
                else
                {
                    list.Add(new SelectListItem { Text = maincontact, Value = mainemail });
                }


            }
            else
            {
                list.Add(new SelectListItem { Text = "----", Value = "-1" });

            }

            return Json(list);

        }


        public ActionResult BindDemoschedulepopup(string DemoScheduleId)
        {
            Session["DemoScheduleId"] = DemoScheduleId;
            //string fklocationid = Session["pkLocationIdWhenpopup"].ToString();

            int fkCompanyId = Int32.Parse(Session["fkcompanyid"].ToString());
            var DemoscheduleList = ObjBALLeads.GetTraineeByDemoIdCompanyId(Convert.ToInt32(DemoScheduleId), fkCompanyId);
            var list = ObjBALLeads.GetDemoScheduleInfoByDemoId(Convert.ToInt16(DemoScheduleId));
            string datetime = list.DemoDateTime.ToString();
            string date = Convert.ToDateTime(datetime).ToShortDateString();
            string time = Convert.ToDateTime(datetime).ToShortTimeString();

            var Comments = list.Comments;

            StringBuilder sb = new StringBuilder();

            sb.Append("<table cellspacing='2' cellpadding='5' width='750px' style='height: 30px; font-weight: bold; margin-left:25px' id='tbln'>");
            for (int i = 0; i < DemoscheduleList.Count; i++)
            {
                sb.Append("<tr>");
                sb.Append("<td style='width:50px' >" + "<input type='text' id='FirstName' style='width:90%' name='Title' class='k-textbox' value='  " + DemoscheduleList.ElementAt(i).FirstName + " '/>" + "</td>");
                sb.Append("<td style='width:50px'>" + "<input type='text' id='LastName' style='width:90%' name='Title' class='k-textbox' value='  " + DemoscheduleList.ElementAt(i).LastName + " '/>" + "</td>");
                sb.Append("<td  style='width:50px'>" + "<input type='text' id='Title' style='width:90%' name='Title' class='k-textbox' value='  " + DemoscheduleList.ElementAt(i).Title + " '/>" + "</td>");
                sb.Append("<td  style='width:50px'>" + "<input type='text' id='Phone' style='width:90%' name='Title' class='k-textbox' value='  " + DemoscheduleList.ElementAt(i).Phone + " '/>" + "</td>");
                sb.Append("<td  style='width:50px'>" + "<input type='text' id='Ext' style='width:90%' name='Title' class='k-textbox' value='  " + DemoscheduleList.ElementAt(i).Ext + " '/>" + "</td>");
                sb.Append("<td style='width:50px'>" + "<input type='text' id='Email' style='width:90%' name='Title' class='k-textbox' value='  " + DemoscheduleList.ElementAt(i).EmailId + " '/>" + "</td>");
                sb.Append("<td style='width:50px'>" + "<input type='button' id=' " + DemoscheduleList.ElementAt(i).pkCompanyTraineeId + "+' style='width:90%' name='delete' class='k-textbox' value='Delete' class='EditDel' style='cursor:pointer; '  onclick='return DeleteDocuments(this.id)'  />" + "</td>");

                sb.Append("</tr>");

            }

            sb.Append("<table>");

            return Json(new { label = sb.ToString(), date = date, time = time, Comments = Comments });




        }



        public ActionResult GetCmpnyTypeDrop()
        {
            //ClientsModel ObjClientsModel = new ClientsModel();
            BALCompanyType ObjBALCompanyType = new BALCompanyType();
            var CompanyTypes = ObjBALCompanyType.GetAllCompanyType();
            return Json(CompanyTypes);

        }

        public ActionResult GetManagerListDrop()
        {
            BALSalesRep ObjBALSalesRep = new BALSalesRep();

            string RoleName = "salesmanager";
            var salesRep = ObjBALSalesRep.GetSalesManagers(RoleName);
            return Json(salesRep);
        }


        public ActionResult GetStateslistdrop()
        {
            BALCompany Obj = new BALCompany();

            var list1 = Obj.GetState();

            return Json(list1);

        }


        [HttpPost]
        public ActionResult GetCitieslist(int fkStateId)
        {

            BALLocation ObjLocation = new BALLocation();
            List<SelectListItem> list = new List<SelectListItem>();
            var obj = ObjLocation.GetCityStatewise(fkStateId).ToList();
            if (obj != null)
            {
                list = obj.Select(x => new SelectListItem { Text = x.CityName, Value = x.CityId.ToString() }).ToList();
            }

            list.Add(new SelectListItem { Text = "---------------", Value = "-1" });
            list.Add(new SelectListItem { Text = "---------------", Value = "-1" });
            list.Add(new SelectListItem { Text = "---Custom---", Value = "-3" });
            return Json(list);
        }





        public ActionResult GetAllPackageRecords(int CompanyId)
        {

            //PackagesModel objPackagesModel = new PackagesModel();
            BALProductPackages ObjBALProductPackages = new BALProductPackages();
            int ObjGuid_LocationId = Int32.Parse(Session["pkCompanyId"].ToString());


            var ObjData = ObjBALProductPackages.GetEmergeUniversalPackagesForAdmin(ObjGuid_LocationId, Utility.SiteApplicationId, CompanyId);

            for (int i = 0; i < ObjData.Count(); i++)
            {
                if (ObjData.ElementAt(i).IsEnableUniversalPackage == false)
                {
                    ObjData.ElementAt(i).IsDefault = false;


                }
                else
                {
                    if (ObjData.ElementAt(i).PackageAccessId != 0)
                    {
                        if (ObjData.ElementAt(i).IsDefault != Convert.ToBoolean(null))
                        {
                            ObjData.ElementAt(i).IsDefault = true;
                        }
                    }

                }


            }



            return Json(ObjData);


        }
        public ActionResult GetCustomPackages(string LocationId, string CompanyId)
        {

            BALProductPackages ObjBALProductPackages = new BALProductPackages();
            ObjGuid_LocationId = Int32.Parse(LocationId);

            var ObjData = ObjBALProductPackages.GetEmergeProductPackagesForAdmin(ObjGuid_LocationId, Utility.SiteApplicationId, Int32.Parse(CompanyId));



            for (int i = 0; i < ObjData.Count(); i++)
            {
                if (ObjData.ElementAt(i).IsEnableCompanyPackage == false)
                {
                    ObjData.ElementAt(i).IsDefault = false;


                }
                else
                {
                    if (ObjData.ElementAt(i).PackageAccessId != 0)
                    {
                        if (ObjData.ElementAt(i).IsDefault != false)
                        {
                            ObjData.ElementAt(i).IsDefault = true;
                        }
                    }
                    ObjData.ElementAt(i).IsDefault = true;
                }


                ObjData.ElementAt(i).IsDefault = ObjData.ElementAt(i).IsEnabled;


            }







            return Json(ObjData);


        }

        public ActionResult GetCategory_List()
        {
            BALCompany ObjBal = new BALCompany();
            var list = ObjBal.GetReportCategories();
            return Json(list);
        }



        public ActionResult GetReportBy_CategoryId(byte pkCategoryId, int CompanyId, string LocationId)
        {
            // byte pkid = Convert.ToByte(pkId);
            List<SubReportsModel> data = new List<SubReportsModel>();
            int locationbyCategioryId = Int32.Parse(LocationId);
            //Guid fkcompanyid = Guid.Parse(Session["NewPkCompanyId"].ToString());
            if (CompanyId != null && CompanyId != 0)
            {
                BALCompany serobj = new BALCompany();
                var List = serobj.GetReportByIdatEdit(locationbyCategioryId, pkCategoryId, CompanyId);
                if (List.Count > 0)
                {
                    data = List.Select(x => new SubReportsModel
                    {
                        pkProductApplicationId = x.pkProductApplicationId,
                        pkProductId = x.pkProductId,
                        ProductAccessId = x.ProductAccessId,
                        AutoReviewStatusForLocation = (x.AutoReviewStatusForLocation != null) ? x.AutoReviewStatusForLocation.ToString() : "0",
                        ProductDisplayName = x.ProductDisplayName,
                        ProductName = x.ProductName,
                        ProductCode = x.ProductCode,
                        ProductDescription = x.ProductDescription,
                        CategoryName = x.CategoryName,
                        CompanyPrice = (x.CompanyPrice != null) ? decimal.Parse(x.CompanyPrice.ToString()) : 0,
                        CoveragePDFPath = x.CoveragePDFPath,
                        IsDefault = x.IsDefault,
                        IsDefaultPrice = x.IsDefaultPrice,
                        IsLiveRunnerLocation = x.IsLiveRunnerLocation,
                        IsLiveRunnerProduct = x.IsLiveRunnerProduct,
                        IsThirdPartyFee = x.IsThirdPartyFee,
                        EnableReviewPerCompany = (x.EnableReviewPerCompany == null) ? false : bool.Parse(x.EnableReviewPerCompany.ToString()),

                    }).ToList();
                }
                // var List = "";
                return Json(data);
            }
            else
            {
                BALCompany serobj = new BALCompany();
                var List = serobj.GetReportProductsById(locationbyCategioryId, pkCategoryId);
                if (List.Count > 0)
                {
                    data = List.Select(x => new SubReportsModel
                    {
                        pkProductApplicationId = x.pkProductApplicationId,
                        pkProductId = x.pkProductId,
                        ProductAccessId = x.ProductAccessId,
                        AutoReviewStatusForLocation = (x.AutoReviewStatusForLocation != null) ? x.AutoReviewStatusForLocation.ToString() : "0",
                        ProductDisplayName = x.ProductDisplayName,
                        ProductName = x.ProductName,
                        ProductCode = x.ProductCode,
                        ProductDescription = x.ProductDescription,
                        CategoryName = x.CategoryName,
                        CompanyPrice = x.CompanyPrice,
                        CoveragePDFPath = x.CoveragePDFPath,
                        IsDefault = x.IsDefault,
                        IsDefaultPrice = x.IsDefaultPrice,
                        IsLiveRunnerLocation = x.IsLiveRunnerLocation,
                        IsLiveRunnerProduct = x.IsLiveRunnerProduct,
                        IsThirdPartyFee = x.IsThirdPartyFee
                    }).ToList();
                }
                // var List = "";
                return Json(data);

            }


            //objmodel=
        }



        private void LoadSalesGraph(int pkCompanyId)
        {
            StringBuilder str = new StringBuilder();
            str.Append(@"<script type=text/javascript>                  
                  function drawSalesChart() {
                    var data = new google.visualization.DataTable();
                    data.addColumn('string', 'Year');                    
                    data.addColumn('number', 'Products');
                    data.addColumn({type:'string', role:'tooltip'});
                    data.addColumn('number', 'Reports');
                    data.addColumn({type:'string', role:'tooltip'}); 
                    data.addColumn('number', 'Total');
                    data.addColumn({type:'string', role:'tooltip'});
                    //data.addColumn({type:'boolean', role:'emphasis'});
                    ");


            DateTime ObjDateTime = DateTime.Now.AddMonths(-11);
            int iCount = 0;
            decimal PreviousTotalSales = 0;
            decimal PreviousReports = 0;
            decimal PreviousProducts = 0;
            decimal TotalSalesPercentage = 0;
            decimal TotalReportsPercentage = 0;
            decimal TotalProductsPercentage = 0;
            for (int i = 0; i < 12; i++)
            {
                BALCompany ObjBALCompany = new BALCompany();
                if (iCount == 0)
                {
                    var Obj1 = ObjBALCompany.GetSalesValuesforGraph(pkCompanyId, Convert.ToInt16(ObjDateTime.AddMonths(-1).Month), Convert.ToInt16(ObjDateTime.AddMonths(-1).Year));
                    PreviousTotalSales = Convert.ToDecimal(Obj1.ElementAt(0).TotalAmount);
                    PreviousReports = Convert.ToDecimal(Obj1.ElementAt(0).ReportAmount);
                    PreviousProducts = Convert.ToDecimal(Obj1.ElementAt(0).ProductAmount);
                }
                var Obj = ObjBALCompany.GetSalesValuesforGraph(pkCompanyId, Convert.ToInt16(ObjDateTime.AddMonths(i).Month), Convert.ToInt16(ObjDateTime.AddMonths(i).Year));
                TotalSalesPercentage = 0;
                TotalReportsPercentage = 0;
                TotalProductsPercentage = 0;
                if (PreviousTotalSales > 0)
                {
                    TotalSalesPercentage = ((Convert.ToDecimal(Obj.ElementAt(0).TotalAmount) - PreviousTotalSales) / PreviousTotalSales) * 100;
                }
                if (PreviousReports > 0)
                {
                    TotalReportsPercentage = ((Convert.ToDecimal(Obj.ElementAt(0).ReportAmount) - PreviousReports) / PreviousReports) * 100;
                }
                if (PreviousProducts > 0)
                {
                    TotalProductsPercentage = ((Convert.ToDecimal(Obj.ElementAt(0).ProductAmount) - PreviousProducts) / PreviousProducts) * 100;
                }

                string ToolTipValue = "";
                ToolTipValue = "Total Sales : " + Convert.ToDecimal(Obj.ElementAt(0).TotalAmount).ToString("c") + " (" + string.Format("{0:0.0%}", (TotalSalesPercentage / 100)) + "), Reports : " + Convert.ToDecimal(Obj.ElementAt(0).ReportAmount).ToString("c") + " (" + string.Format("{0:0.0%}", (TotalReportsPercentage / 100)) + "), Products : " + Convert.ToDecimal(Obj.ElementAt(0).ProductAmount).ToString("c") + " (" + string.Format("{0:0.0%}", (TotalProductsPercentage / 100)) + ") ";
                str.Append("data.addRow(['" + ObjDateTime.AddMonths(i).ToString("MMM, yyyy") + "'," + Obj.ElementAt(0).ProductAmount.ToString() + ",'" + ToolTipValue + "'," + Obj.ElementAt(0).ReportAmount.ToString() + ",'" + ToolTipValue + "'," + Obj.ElementAt(0).TotalAmount.ToString() + ",'" + ToolTipValue + "']);");
                if (iCount != 0)
                {
                    PreviousTotalSales = Convert.ToDecimal(Obj.ElementAt(0).TotalAmount);
                    PreviousReports = Convert.ToDecimal(Obj.ElementAt(0).ReportAmount);
                    PreviousProducts = Convert.ToDecimal(Obj.ElementAt(0).ProductAmount);
                }
                iCount++;
            }
            str.Append("   var chart = new google.visualization.LineChart(document.getElementById('Saleschart_div'));");
            str.Append(" chart.draw(data, {width: 970, height: 280, pointSize: 8, fontSize: 10, title: 'Sales Performance',titleTextStyle: {color: 'Red',pointSize: 8, fontSize: 12},colors: ['#00FF00', '#FF9900', '#0000FF']");
            str.Append("}); } google.setOnLoadCallback(drawSalesChart);");
            str.Append("</script>");
            ViewBag.ChartImg = str.ToString().TrimEnd(',').Replace('*', '"');
            ViewBag.SalesTotal = "Total Sales : " + PreviousTotalSales.ToString("c") + " " + strPercentageColor(TotalSalesPercentage).ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Sales : " + PreviousReports.ToString("c") + " " + strPercentageColor(TotalReportsPercentage).ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Product Sales : " + PreviousProducts.ToString("c") + " " + strPercentageColor(TotalProductsPercentage).ToString() + "";
        }

        private void LoadVolumeGraph(int pkCompanyId)
        {
            StringBuilder str = new StringBuilder();
            str.Append(@"<script type=text/javascript>
                  
                  function drawVolumeChart() {
                    var Volumedata = new google.visualization.DataTable();
                    Volumedata.addColumn('string', 'Year');
                    Volumedata.addColumn('number', 'Products');
                    Volumedata.addColumn({type:'string', role:'tooltip'});                    
                    Volumedata.addColumn('number', 'Reports');
                    Volumedata.addColumn({type:'string', role:'tooltip'});
                    Volumedata.addColumn('number', 'Total');
                    Volumedata.addColumn({type:'string', role:'tooltip'});                     
                    //data.addColumn({type:'boolean', role:'emphasis'});
                    ");


            DateTime ObjDateTime = DateTime.Now.AddMonths(-11);
            int iCount = 0;
            decimal PreviousTotalSales = 0;
            decimal PreviousReports = 0;
            decimal PreviousProducts = 0;
            decimal TotalSalesPercentage = 0;
            decimal TotalReportsPercentage = 0;
            decimal TotalProductsPercentage = 0;
            for (int i = 0; i < 12; i++)
            {
                BALCompany ObjBALCompany = new BALCompany();
                if (iCount == 0)
                {
                    var Obj1 = ObjBALCompany.GetVolumeValuesforGraph(pkCompanyId, Convert.ToInt16(ObjDateTime.AddMonths(-1).Month), Convert.ToInt16(ObjDateTime.AddMonths(-1).Year));
                    PreviousTotalSales = Convert.ToDecimal(Obj1.ElementAt(0).TotalAmount);
                    PreviousReports = Convert.ToDecimal(Obj1.ElementAt(0).ReportAmount);
                    PreviousProducts = Convert.ToDecimal(Obj1.ElementAt(0).ProductAmount);
                }
                var Obj = ObjBALCompany.GetVolumeValuesforGraph(pkCompanyId, Convert.ToInt16(ObjDateTime.AddMonths(i).Month), Convert.ToInt16(ObjDateTime.AddMonths(i).Year));
                TotalSalesPercentage = 0;
                TotalReportsPercentage = 0;
                TotalProductsPercentage = 0;
                if (PreviousTotalSales > 0)
                {
                    TotalSalesPercentage = ((Convert.ToDecimal(Obj.ElementAt(0).TotalAmount) - PreviousTotalSales) / PreviousTotalSales) * 100;
                }
                if (PreviousReports > 0)
                {
                    TotalReportsPercentage = ((Convert.ToDecimal(Obj.ElementAt(0).ReportAmount) - PreviousReports) / PreviousReports) * 100;
                }
                if (PreviousProducts > 0)
                {
                    TotalProductsPercentage = ((Convert.ToDecimal(Obj.ElementAt(0).ProductAmount) - PreviousProducts) / PreviousProducts) * 100;
                }
                string ToolTipValue = "";
                ToolTipValue = "Total Volume : " + Convert.ToDecimal(Obj.ElementAt(0).TotalAmount).ToString() + " (" + string.Format("{0:0.0%}", (TotalSalesPercentage / 100)) + "), Reports : " + Convert.ToDecimal(Obj.ElementAt(0).ReportAmount).ToString() + " (" + string.Format("{0:0.0%}", (TotalReportsPercentage / 100)) + "), Products : " + Convert.ToDecimal(Obj.ElementAt(0).ProductAmount).ToString() + " (" + string.Format("{0:0.0%}", (TotalProductsPercentage / 100)) + ") ";
                str.Append("Volumedata.addRow(['" + ObjDateTime.AddMonths(i).ToString("MMM, yyyy") + "'," + Obj.ElementAt(0).ProductAmount.ToString() + ",'" + ToolTipValue + "'," + Obj.ElementAt(0).ReportAmount.ToString() + ",'" + ToolTipValue + "'," + Obj.ElementAt(0).TotalAmount.ToString() + ",'" + ToolTipValue + "']);");
                if (iCount != 0)
                {
                    PreviousTotalSales = Convert.ToDecimal(Obj.ElementAt(0).TotalAmount);
                    PreviousReports = Convert.ToDecimal(Obj.ElementAt(0).ReportAmount);
                    PreviousProducts = Convert.ToDecimal(Obj.ElementAt(0).ProductAmount);
                }
                iCount++;
            }
            str.Append("   var Volumechart = new google.visualization.LineChart(document.getElementById('Volumechart_div'));");
            str.Append(" Volumechart.draw(Volumedata, {width: 970, height: 280, pointSize: 8, fontSize: 10, title: 'Volume Performance',titleTextStyle: {color: 'Red',pointSize: 8, fontSize: 12},colors: ['#00FF00', '#FF9900', '#0000FF']");
            str.Append("}); } \t\ngoogle.load('visualization', '1', {packages:['corechart']});\t\ngoogle.setOnLoadCallback(drawVolumeChart);");
            str.Append("</script>");
            ViewData["VolumeChartImg"] = str.ToString().TrimEnd(',').Replace('*', '"');
            ViewBag.VolumeSalesTotal = "Total Volume : " + PreviousTotalSales.ToString() + " " + strPercentageColor(TotalSalesPercentage).ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Sales : " + PreviousReports.ToString() + " " + strPercentageColor(TotalReportsPercentage).ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Product Sales : " + PreviousProducts.ToString() + " " + strPercentageColor(TotalProductsPercentage).ToString() + "";
        }

        private void LoadVolumeGraph_Old(int pkCompanyId)
        {
            BALCompany ObjBALCompany = new BALCompany();
            StringBuilder str = new StringBuilder();
            str.Append(@"<script type=text/javascript>
                  
                  function drawVolumeChart() {
                    var Volumedata = new google.visualization.DataTable();
                    Volumedata.addColumn('string', 'Year');
                    Volumedata.addColumn('number', 'Products');
                    Volumedata.addColumn({type:'string', role:'tooltip'});                    
                    Volumedata.addColumn('number', 'Reports');
                    Volumedata.addColumn({type:'string', role:'tooltip'});
                    Volumedata.addColumn('number', 'Total');
                    Volumedata.addColumn({type:'string', role:'tooltip'});                     
                    //data.addColumn({type:'boolean', role:'emphasis'});
                    ");


            DateTime ObjDateTime = DateTime.Now.AddMonths(-11);
            int iCount = 0;
            decimal PreviousTotalSales = 0;
            decimal PreviousReports = 0;
            decimal PreviousProducts = 0;
            decimal TotalSalesPercentage = 0;
            decimal TotalReportsPercentage = 0;
            decimal TotalProductsPercentage = 0;
            var Obj = ObjBALCompany.GetVolumeValuesforGraphMVC(pkCompanyId, 11);
            for (int i = 0; i < 12; i++)
            {
                if (iCount == 0)
                {
                    //var Obj1 = ObjBALCompany.GetVolumeValuesforGraph(new Guid(pkCompanyId), Convert.ToInt16(ObjDateTime.AddMonths(-1).Month), Convert.ToInt16(ObjDateTime.AddMonths(-1).Year));
                    PreviousTotalSales = Convert.ToDecimal(Obj.ElementAt(i).TotalAmount);
                    PreviousReports = Convert.ToDecimal(Obj.ElementAt(i).ReportAmount);
                    PreviousProducts = Convert.ToDecimal(Obj.ElementAt(i).ProductAmount);
                }
                // var Obj = ObjBALCompany.GetVolumeValuesforGraph(new Guid(pkCompanyId), Convert.ToInt16(ObjDateTime.AddMonths(i).Month), Convert.ToInt16(ObjDateTime.AddMonths(i).Year));
                TotalSalesPercentage = 0;
                TotalReportsPercentage = 0;
                TotalProductsPercentage = 0;
                if (PreviousTotalSales > 0)
                {
                    TotalSalesPercentage = ((Convert.ToDecimal(Obj.ElementAt(i).TotalAmount) - PreviousTotalSales) / PreviousTotalSales) * 100;
                }
                if (PreviousReports > 0)
                {
                    TotalReportsPercentage = ((Convert.ToDecimal(Obj.ElementAt(i).ReportAmount) - PreviousReports) / PreviousReports) * 100;
                }
                if (PreviousProducts > 0)
                {
                    TotalProductsPercentage = ((Convert.ToDecimal(Obj.ElementAt(i).ProductAmount) - PreviousProducts) / PreviousProducts) * 100;
                }
                string ToolTipValue = "";
                ToolTipValue = "Total Volume : " + Convert.ToDecimal(Obj.ElementAt(i).TotalAmount).ToString() + " (" + string.Format("{0:0.0%}", (TotalSalesPercentage / 100)) + "), Reports : " + Convert.ToDecimal(Obj.ElementAt(i).ReportAmount).ToString() + " (" + string.Format("{0:0.0%}", (TotalReportsPercentage / 100)) + "), Products : " + Convert.ToDecimal(Obj.ElementAt(i).ProductAmount).ToString() + " (" + string.Format("{0:0.0%}", (TotalProductsPercentage / 100)) + ") ";
                str.Append("Volumedata.addRow(['" + ObjDateTime.AddMonths(i).ToString("MMM, yyyy") + "'," + Obj.ElementAt(i).ProductAmount.ToString() + ",'" + ToolTipValue + "'," + Obj.ElementAt(i).ReportAmount.ToString() + ",'" + ToolTipValue + "'," + Obj.ElementAt(i).TotalAmount.ToString() + ",'" + ToolTipValue + "']);");
                if (iCount != 0)
                {
                    PreviousTotalSales = Convert.ToDecimal(Obj.ElementAt(i).TotalAmount);
                    PreviousReports = Convert.ToDecimal(Obj.ElementAt(i).ReportAmount);
                    PreviousProducts = Convert.ToDecimal(Obj.ElementAt(i).ProductAmount);
                }
                iCount++;
            }
            str.Append("   var Volumechart = new google.visualization.LineChart(document.getElementById('Volumechart_div'));");
            str.Append(" Volumechart.draw(Volumedata, {width: 970, height: 280, pointSize: 8, fontSize: 10, title: 'Volume Performance',titleTextStyle: {color: 'Red',pointSize: 8, fontSize: 12},colors: ['#00FF00', '#FF9900', '#0000FF']");
            str.Append("}); } \t\ngoogle.load('visualization', '1', {packages:['corechart']});\t\ngoogle.setOnLoadCallback(drawVolumeChart);");
            str.Append("</script>");
            ViewData["VolumeChartImg"] = str.ToString().TrimEnd(',').Replace('*', '"');
            ViewBag.VolumeSalesTotal = "Total Volume : " + PreviousTotalSales.ToString() + " " + strPercentageColor(TotalSalesPercentage).ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Sales : " + PreviousReports.ToString() + " " + strPercentageColor(TotalReportsPercentage).ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Product Sales : " + PreviousProducts.ToString() + " " + strPercentageColor(TotalProductsPercentage).ToString() + "";
        }

        private string strPercentageColor(decimal PercentageAmount)
        {
            string strColor = "";
            if (PercentageAmount >= 0)
            {
                strColor = "<span style='color : #008000;'> (" + string.Format("{0:0.0%}", (PercentageAmount / 100)) + ") </span>";
            }
            else
            {
                strColor = "<span style='color : #FF0000;'> (" + string.Format("{0:0.0%}", (PercentageAmount / 100)) + ")</span>";
            }
            return strColor;
        }






        public ActionResult SaveCallLogo(string Comment, string pkcompanyid)
        {
            Guid UserId;
            int pkCmId=(!string.IsNullOrEmpty(pkcompanyid)?Convert.ToInt32(pkcompanyid):0);
            MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));

            if (ObjMembershipUser != null)
            {
                UserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
                string Comments = Comment;
                DateTime CreatedDate = System.DateTime.Now;
                AddCommentToCallLog(pkCmId, UserId, Comments, CreatedDate);
            }

            return Json(new { Message = "Successful" });


        }


        public ActionResult DeleteDocument(int DocumentId)
        {

            BALCompany ObjBALCompany = new BALCompany();
            //int Result = 
                ObjBALCompany.DeleteCompanyDocumentsByCompanyDocId(DocumentId);
            return Json(new { Message = "Successful" });
        }







        public ActionResult BindDocumentsentFields(int DocumentsentId)
        {
            BALLeads ObjBALLeads = new BALLeads();
            int fkCompanyId = Int32.Parse(Session["fkcompanyid"].ToString());

            var DocumentSent = ObjBALLeads.GetDocumentsBySentIdcompanyId(DocumentsentId, fkCompanyId);
            var txtbindComment = ObjBALLeads.GetDocumentSentinfobyDocId(DocumentsentId);
            Session["pkdocumentid"] = DocumentSent.ElementAt(0).pkDocumentId;
            //int id = DocumentSent.ElementAt(0).pkDocumentId;
            //BALOffice ObjBALOffice = new BALOffice();
            //var Document = ObjBALOffice.GetDocumentsByDocId(id);
            StringBuilder sb = new StringBuilder();
            sb.Append("<table>");
            for (int j = 0; j < DocumentSent.Count; j++)
            {

                if (System.IO.File.Exists(HttpContext.Server.MapPath(@"~\Resources\Upload\OfficeDocuments\") + DocumentSent.ElementAt(j).DocumentPath))
                {
                    sb.Append("<tr>");
                    sb.Append("<td><a style='color: #0000ee'  href='Download_FeaturedFile/?FPathTitle=" + DocumentSent.ElementAt(j).DocumentPath + "' title='View Document' >View</a></td>");
                    sb.Append("</tr>");
                }
                else
                {

                    sb.Append("<tr>");
                    sb.Append("<td><a style='color: #0000ee' onclick='NotFound()' title='View Document' >View</a></td>");
                    sb.Append("</tr>");



                }


            }

            sb.Append("</table>");
            return Json(new { Doc = DocumentSent.ElementAt(0).DocumentTitle, DocId = DocumentSent.ElementAt(0).pkDocumentId, Comment = txtbindComment.Comments, label = sb.ToString() });
        }







        public FilePathResult Download_FeaturedFile(string FPathTitle)
        {

            string Title = string.Empty;
            string FilePath = string.Empty;
            string FileName = "";
            BALOffice ObjBALOffice = new BALOffice();
            int DocumentsentId = Int32.Parse(Session["pkdocumentid"].ToString());
            var Document = ObjBALOffice.GetDocumentsByDocId(DocumentsentId);
            Title = Document.DocumentTitle.ToString().Replace(" ", "_");
            FilePath = Request.PhysicalApplicationPath + "Resources/Upload/OfficeDocuments/" + FPathTitle;
            FileName = FPathTitle;
            string fileext = System.IO.Path.GetExtension(FileName);
            string CType = "application/" + fileext.Replace(".", "");
            //string fileName = Path.GetFileName(FileName);
            string strFileTitle = Title + fileext;
            return File(FilePath, CType, strFileTitle);

        }

        public ActionResult Download_CompanyDocumentsFile(string FPathTitle)
        {
            string[] str = FPathTitle.Split('_');
            string FileName = str[0];
            string Title = str[1].Replace(" ", "_");
            string FilePath = Request.PhysicalApplicationPath + "Resources/Upload/CompanyPDF/" + FileName;
            string fileext = System.IO.Path.GetExtension(FileName);
            string CType = "application/" + fileext.Replace(".", "");
            //string fileName = Path.GetFileName(FileName);

            string strFileTitle = Title + fileext;



            if (System.IO.File.Exists(FilePath))
            {
                return File(FilePath, CType, strFileTitle);
            }
            else
            {
                FilePath = Request.PhysicalApplicationPath + "Resources/Upload/CompanyPDF/no-image.jpg";
                return File(FilePath, CType, strFileTitle);
            }
        }




        public void UpdateCallLog(string btnName, int fkCompanyid)
        {
            Guid UserId;
            int pkCompanyId;
            MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));
            if (ObjMembershipUser != null)
            {
                UserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
                pkCompanyId = fkCompanyid;
                string Comments = "";
                string[] Btnstr = btnName.Split('_');
                switch (Btnstr[1].ToLower())
                {
                    case "5": Comments = "##5##"; break;
                    case "6": Comments = "##6##"; break;
                    case "7": Comments = "##7##"; break;
                    case "8": Comments = "##8##"; break;
                    case "9": Comments = "##9##"; break;
                }
                DateTime CreatedDate = System.DateTime.Now;
                AddStatus_ToCallLog(pkCompanyId, UserId, Comments, CreatedDate);
            }

        }

        public void UpdateCompanyIsActive(byte preStatus, int fkCompanyid)
        {
            BALCompany ObjBALCompany = new BALCompany();
            tblCompany ObjtblCompany = new tblCompany();
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            //int CompanyId = 0;
            Guid UserId = Guid.Empty;
            var dbCollection = ObjBALCompanyUsers.GetUserInformationByMembershipUserId(new Guid(Membership.GetUser(HttpContext.User.Identity.Name).ProviderUserKey.ToString()));
            if (dbCollection.Count > 0)
            {
                UserId = dbCollection.First().fkUserId;
            }
            if (fkCompanyid != null)
            {
                ObjtblCompany.pkCompanyId = fkCompanyid;
                ObjtblCompany.IsActive = preStatus;
                ObjtblCompany.LastModifiedDate = DateTime.Now;
                ObjtblCompany.LastModifiedById = UserId;
                //int success =
                ObjBALCompany.UpdateCompanyIsActive(ObjtblCompany);

            }
        }
        private int AddStatus_ToCallLog(int pkCompanyId, Guid UserId, string Comments, DateTime CreatedDate)
        {
            BALCompany ObjBALCompany = new BALCompany();
            int Result = 0;
            try
            {
                Result = ObjBALCompany.AddStatusToCallLog(pkCompanyId, UserId, Comments, CreatedDate);
            }
            catch (Exception)
            {

            }
            finally
            {
                ObjBALCompany = null;
            }
            return Result;
        }
        private void sendStatusChangedMail(byte Status, string CompanyName, string EmailId)
        {
            try
            {
                MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
                //bool CheckSend = false;
                string emailFrom = ObjMembershipUser.Email;
                string StrDate = DateTime.Now.ToString();

                #region Replace Bookmark
                string MessageBody = "";
                string emailText = "";
                string imgSrc = "";
                BALGeneral ObjBALGeneral = new BALGeneral();
                string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;
                var emailContent = ObjBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, Convert.ToInt32(EmailTemplates.AccountStatusChange));


                emailText = emailContent.TemplateContent;

                BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                Bookmarks objBookmark = new Bookmarks();
                objBookmark.EmailContent = emailText;
                objBookmark.Date = StrDate;
                if (!string.IsNullOrEmpty(CompanyName))
                {
                    objBookmark.CompanyName = CompanyName;
                }
                if (Status == 7)
                {
                    imgSrc = ApplicationPath.GetApplicationPath() + @"Resources/Images/emerge-office-milestone-7-on.png";
                }
                else if (Status == 8)
                {
                    imgSrc = ApplicationPath.GetApplicationPath() + @"Resources/Images/emerge-office-milestone-8-on.png";
                }
                else if (Status == 9)
                {
                    imgSrc = ApplicationPath.GetApplicationPath() + @"Resources/Images/emerge-office-milestone-9-on.png";
                }
                objBookmark.CompanyStatus = "<img src='" + imgSrc + "' />";
                objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                objBookmark.LoginLink = "<a style='color: #87ceea;' href='" + ApplicationPath.GetApplicationPath() + "'> Click here </a>";

                MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Utility.GetUID(HttpContext.User.Identity.Name.ToString()));

                #endregion
                string emailTo = "";
                if (!string.IsNullOrEmpty(EmailId))
                {
                    emailTo = EmailId;

                }

                //CheckSend = 
                    Utility.SendMail(emailTo, emailFrom, MessageBody, "Account Status Change");

            }
            catch
            {

            }
        }
        public string GetStateCode(int StateId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {

                string StateCode = (from u in ObjDALDataContext.tblStates
                                    where u.pkStateId == StateId
                                    select u.StateCode).FirstOrDefault();
                return StateCode;
            }
        }


    }
}
