﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Emerge.Common;
using Emerge.Models;
using Emerge.Data;
using System.Web.Security;
using System.IO;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Text;
using System.Web.UI;


namespace Emerge.Controllers.Office
{
    public partial class OfficeController : Controller
    {  //
        // GET: /PrintLead/

        public ActionResult PrintLead()
        {
            Proc_Get_CompanyDetailByLocationIdResult list1 = new Proc_Get_CompanyDetailByLocationIdResult();
            AddEditLeadsModel objModel = new AddEditLeadsModel();
            BALLeads ObjBALLeads = new BALLeads();
            if (Request.QueryString["_a"] != null)
                {
                 var locationid1 = Int32.Parse(Request.QueryString["_a"]);
                 BALCompany ObjBALCompany = new BALCompany();
                 try
                 {
                     list1 = ObjBALCompany.GetCompanyDetailByLocationId(locationid1).First();
                    // BindFields(ObjCompanyDetailByLocationId);
                 }
                 catch (Exception)
                 {

                 }
                 finally
                 {
                     ObjBALCompany = null;
                 }

                 
                 objModel.CompanyNote = list1.CompanyNote;
                 objModel.CompanyName = list1.CompanyName;


                 Session["pkLocationIdWhenEdit"] = Guid.Parse(Request.QueryString["_a"]);

                
                 Session["pkCompanyIdWhenEdit"] = list1.fkCompanyId;
                 string fkCompanyId = Session["pkCompanyIdWhenEdit"].ToString();

                 objModel.pkCompanyTypeId = list1.fkCompanyTypeId;
                 var fkcompanyid = list1.fkCompanyTypeId;
                 BALCompanyType objbalcompany = new BALCompanyType();
                // The Value Of Company DropDrown
                 var CompanyTypes = objbalcompany.GetAllCompanyType();
                if (CompanyTypes != null)
                 {
                     var Companylist = CompanyTypes.Where(dx => dx.pkCompanyTypeId == fkcompanyid).FirstOrDefault();
               
                    objModel.CompanyType = Companylist.CompanyType;
                }
                // The Value Of State DropDrown
                var fkststateid = list1.fkStateID;
                BALCompany ObjBALCompany1 = new BALCompany();

                if (fkststateid != null)
                {
                    var list3 = ObjBALCompany1.GetState();

                    var statename = list3.Where(dx => dx.pkStateId == fkststateid).FirstOrDefault();
                    objModel.StateName = statename.StateName;
                }
                
                var fkuserid = list1.fkSalesManagerId;
                // The Value Of Manager DropDrown
                string rolename = "salesmanager";
                var Managerlist = ObjBALCompany1.GetManagers(rolename);
                if (Managerlist != null)
                {
                    var MangerNamebyId = Managerlist.Where(dx => dx.fkUserId == fkuserid).FirstOrDefault();
                    objModel.FullName = MangerNamebyId.FullName;
 
                }


                // string CompanyType = Convert.ToString(CompanyTypes.Find(dx => dx.pkCompanyTypeId == fkcompanyid));
                
                 objModel.LeadNumber = list1.LeadNumber;
                 objModel.LeadSource = list1.LeadSource;
                 objModel.fkUserId = list1.fkSalesManagerId;
                 objModel.Address1 = list1.Address1;
                 objModel.Address2 = list1.Address2;
                 objModel.pkStateId = list1.fkStateID;
                 objModel.CollGetCommentsForLeadResult = ObjBALLeads.GetCommentsForLead(Int32.Parse(fkCompanyId));
                
                 objModel.MainFaxNumber = list1.Fax2;
                 objModel.ZipCod = list1.ZipCode;
                 objModel.CityName = list1.City;
                 ViewBag.LocationId = list1.pkLocationId;
                 ViewBag.CompanyId = list1.fkCompanyId;

                 objModel.MainContact = list1.MainContactPersonName;
                 Session["vwUserName"] = list1.MainContactPersonName;
                 Session["Maincontact"] = list1.MainContactPersonName;
                 objModel.MainEmail = list1.MainEmailId;
                 Session["MainEmail"] = list1.MainEmailId;
                 objModel.MainPhone = list1.PhoneNo2;
                 Session["Mainphone"] = list1.PhoneNo2;
                 objModel.BillingFax = list1.Fax;
                 objModel.BillingContact = list1.BillingContactPersonName;
                 Session["Billingcontact"] = list1.BillingContactPersonName;
                 objModel.BillingEmail = list1.BillingEmailId;
                 Session["BillingEmail"] = list1.BillingEmailId;
                 objModel.BillingPhone = list1.PhoneNo1;


                 ViewBag.City = list1.City;
                 objModel.NumberOfLocation = list1.NoOfLocations.ToString();
                 objModel.CurrentProvider = list1.CurrentProvider;

                 List<SelectListItem> PriorityList = new List<SelectListItem>();
                 PriorityList.Insert(0, new SelectListItem { Text = "Accuracy", Value = "1" });
                 PriorityList.Insert(1, new SelectListItem { Text = "Coverage", Value = "2" });
                 PriorityList.Insert(2, new SelectListItem { Text = "Pricing", Value = "3" });
                 PriorityList.Insert(3, new SelectListItem { Text = "Speed", Value = "4" });
                 PriorityList.Insert(4, new SelectListItem { Text = "Support", Value = "5" });
                 PriorityList.Insert(5, new SelectListItem { Text = "Other", Value = "6" });
                 PriorityList.Insert(6, new SelectListItem { Text = "Other", Value = "-1" });


                 List<SelectListItem> PriorityList1 = PriorityList.Where(rec => rec.Value == list1.Priority1).ToList();
                 List<SelectListItem> PriorityList2 = PriorityList.Where(rec => rec.Value == list1.Priority2).ToList();
                 List<SelectListItem> PriorityList3 = PriorityList.Where(rec => rec.Value == list1.Priority3).ToList();
              

                 objModel.Priority1 = list1.Priority1Notes == "-1" ? "NA" : PriorityList1.ElementAt(0).Text;
                 objModel.Priority2 = list1.Priority1Notes == "-1" ? "NA" : PriorityList2.ElementAt(0).Text;
                 objModel.Priority3 = list1.Priority3 == "-1" ? "NA" : PriorityList3.ElementAt(0).Text; 
                 
                
             }

           // ScriptManager.RegisterStartupScript(this, typeof(string), "successprint", "printandclose();", true);
            return View(objModel);
        }

        private void FetchCompanyDetails(int PkID)
        {
           BALCompany  ObjBALCompany = new BALCompany();
            try
            {
                Proc_Get_CompanyDetailByLocationIdResult ObjCompanyDetailByLocationId = ObjBALCompany.GetCompanyDetailByLocationId(PkID).First();
                BindFields(ObjCompanyDetailByLocationId);
            }
            catch (Exception)
            {

            }
            finally
            {
                ObjBALCompany = null;
            }
        }

        private void BindFields(Proc_Get_CompanyDetailByLocationIdResult ObjCompanyDetailByLocationId)
        {
            AddEditLeadsModel objaddeditModel = new  AddEditLeadsModel();
            objaddeditModel.Address1 = ObjCompanyDetailByLocationId.Address1;
            objaddeditModel.LeadNumber = ObjCompanyDetailByLocationId.LeadNumber;
            objaddeditModel.Address2 = ObjCompanyDetailByLocationId.Address2;
            objaddeditModel.LeadSource = objaddeditModel.LeadSource;
            objaddeditModel.NumberOfLocation = ObjCompanyDetailByLocationId.NoOfLocations.ToString();

            objaddeditModel.CurrentProvider = objaddeditModel.CurrentProvider;
            objaddeditModel.CityName = ObjCompanyDetailByLocationId.City;
            objaddeditModel.CompanyName = ObjCompanyDetailByLocationId.CompanyName;
            objaddeditModel.CompanyNote = ObjCompanyDetailByLocationId.CompanyNote;
            objaddeditModel.MainContact = ObjCompanyDetailByLocationId.MainContactPersonName;
            objaddeditModel.MainEmail = ObjCompanyDetailByLocationId.MainEmailId;
            objaddeditModel.Phone = ObjCompanyDetailByLocationId.PhoneNo1;

            objaddeditModel.ZipCod = ObjCompanyDetailByLocationId.ZipCode;

        }

        public ActionResult GetReportForprintByCategoryId(byte pkCategoryId)
        {

            //    // byte pkid = Convert.ToByte(pkId);
           // AdCompanyModel objmodel = new AdCompanyModel();
            // Guid CompanyId = Guid.Empty;
            List<SubReportsModel> data = new List<SubReportsModel>();
            int locationbyCategioryId = 0;
            int CompanyId = 0;
            // Guid CompanyId = Guid.Empty;
            //Guid fkcompanyid = Guid.Parse(Session["NewPkCompanyId"].ToString());
            string CompanyId1 = Session["pkCompanyIdWhenEdit"].ToString();
            string LocationId = Session["pkLocationIdWhenEdit"].ToString();
            if (CompanyId1 != null && CompanyId1 != string.Empty)
            {
                locationbyCategioryId = Int32.Parse(LocationId);
                CompanyId = Int32.Parse(CompanyId1);
                BALCompany serobj = new BALCompany();
                var List = serobj.GetReportByIdatEdit(locationbyCategioryId, pkCategoryId, CompanyId);
                if (List.Count > 0)
                {
                    data = List.Select(x => new SubReportsModel
                    {
                        pkProductApplicationId = x.pkProductApplicationId,
                        pkProductId = x.pkProductId,
                        ProductAccessId = x.ProductAccessId,
                        AutoReviewStatusForLocation = (x.AutoReviewStatusForLocation != null) ? x.AutoReviewStatusForLocation.ToString() : "0",
                        ProductDisplayName = x.ProductDisplayName,
                        ProductName = x.ProductName,
                        ProductCode = x.ProductCode,
                        ProductDescription = x.ProductDescription,
                        CategoryName = x.CategoryName,
                        CompanyPrice = (x.CompanyPrice != null) ? decimal.Parse(x.CompanyPrice.ToString()) : 0,
                        CoveragePDFPath = x.CoveragePDFPath,
                        IsDefault = x.IsDefault,
                        IsDefaultPrice = x.IsDefaultPrice,
                        IsLiveRunnerLocation = x.IsLiveRunnerLocation,
                        IsLiveRunnerProduct = x.IsLiveRunnerProduct,
                        IsThirdPartyFee = x.IsThirdPartyFee
                    }).ToList();
                }
                // var List = "";

                // var List = "";
                return Json(data);
            }
            else
            {
                locationbyCategioryId = 0;
                CompanyId = 0;
                //var comapnyprice = "";
                BALCompany serobj = new BALCompany();
                var List = serobj.GetReportProductsById(locationbyCategioryId, pkCategoryId);
                if (List.Count > 0)
                {
                    data = List.Select(x => new SubReportsModel
                    {
                        pkProductApplicationId = x.pkProductApplicationId,
                        pkProductId = x.pkProductId,
                        ProductAccessId = x.ProductAccessId,
                        AutoReviewStatusForLocation = (x.AutoReviewStatusForLocation != null) ? x.AutoReviewStatusForLocation.ToString() : "0",
                        ProductDisplayName = x.ProductDisplayName,
                        ProductName = x.ProductName,
                        ProductCode = x.ProductCode,
                        ProductDescription = x.ProductDescription,
                        CategoryName = x.CategoryName,
                        CompanyPrice = x.CompanyPrice,
                        CoveragePDFPath = x.CoveragePDFPath,
                        IsDefault = x.IsDefault,
                        IsDefaultPrice = x.IsDefaultPrice,
                        IsLiveRunnerLocation = x.IsLiveRunnerLocation,
                        IsLiveRunnerProduct = x.IsLiveRunnerProduct,
                        IsThirdPartyFee = x.IsThirdPartyFee
                    }).ToList();
                }
                // var List = "";
                return Json(data);

            }
            //objmodel=
        }



    }
}
