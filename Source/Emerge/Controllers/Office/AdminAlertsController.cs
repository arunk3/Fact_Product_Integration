﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using System.Web.Security;
using System.Text;

namespace Emerge.Controllers.Office
{
    public partial class OfficeController : Controller
    {
      
        //
        // GET: /AdminAlerts/

        public ActionResult AdminAlerts()
        {
            return View();
        }

        public ActionResult AlertsBinding(int page, int pageSize)
        {

        int PageNum = 1;
            int PageSize = 15;
            MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));

            Guid userId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
            BALOffice objBALOffice = new BALOffice();
            var alertsCollec = objBALOffice.GetAlertsForOfficeUser(userId,PageNum,PageSize,"CreatedDate","asc").ToList();
            return Json(new { Products = alertsCollec, TotalCount = alertsCollec.ElementAt(0).TotalRec},
                           JsonRequestBehavior.AllowGet);

        }



    }
}
