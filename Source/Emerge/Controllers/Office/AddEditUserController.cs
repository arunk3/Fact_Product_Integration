﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using System.Web.Security;
using Emerge.Common;
using Emerge.Services;
using Emerge.Data;
using Emerge.Controllers.Control;
using System.Configuration;

namespace Emerge.Controllers.Office
{
    public partial class OfficeController : Controller
    {
        //
        // GET: /AddEditUser/
        string[] multiple_Roles;
        private int CompanyUserId = 0;
        int NewPkCompanyId = 0;
        int NewCompanyUserId = 0;
        public ActionResult UserAccount()
        {
            ObjBALCompanyUsers = new BALCompanyUsers();
            UsersModel ObjUsersModel = new UsersModel();

            if (TempData["message"] != null)
            {
                ObjUsersModel.strResult = TempData["message"].ToString();
            }
            setPageMode(ObjUsersModel);
            MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));

            if (ObjMembershipUser == null)
            {
                HttpContext.Response.Redirect(ApplicationPath.GetApplicationPath() + "Office/Default");
            }
            multiple_Roles = Roles.GetRolesForUser(ObjMembershipUser.UserName);
            ObjUsersModel.CurrentUserEmail = ObjMembershipUser.UserName;
            Guid ObjGuid = Guid.Empty;
            SetRoleBaseAccess(multiple_Roles, ObjUsersModel);
            if (!string.IsNullOrEmpty(Request.QueryString["_a"]))
            {
                ObjUsersModel.QueryStringUserId = Request.QueryString["_a"].ToString();
                ObjGuid = new Guid(Request.QueryString["_a"].ToString());
                ViewBag.UserStatus = ObjGuid.ToString();
            }
            else if (!string.IsNullOrEmpty(Request.QueryString["_c"]))         //  _c Contains Company ID
            {
                ViewBag.UserStatus = string.Empty;
                ObjUsersModel.QueryStringCompanyId = Request.QueryString["_c"].ToString();
                //ObjGuid = new Guid(Request.QueryString["_c"].ToString());
                ObjGuid = new Guid("98880AE3-7F3E-45F7-9EEE-4FBAEB5D9C34");
                ObjUsersModel.ViewImage = ApplicationPath.GetApplicationPath() + @"Content/themes/base/images/NotAvail.png";                                                      // changes to display the Add new user page              
            }
            if (!string.IsNullOrEmpty(Request.QueryString["_an"]))         //  _c Contains Company ID
            {
                ObjUsersModel.IsEnabled = true;
                ObjUsersModel.ddlStatusEnabled = "true";
            }
            if (ObjGuid != Guid.Empty)
            {
                ObjBALOffice = new BALOffice();

                ObjtblCompanyUser = ObjBALOffice.GetCompanyUser(ObjGuid, out Selected_RoleName, out Selected_RoleID);

                if (ObjtblCompanyUser != null)
                {
                    BindFieldsForOfficeUsers(ObjtblCompanyUser, ObjUsersModel);
                    ObjUsersModel.PageTitle = "Edit User";
                }
            }
            else
            {
                BindCurrentCompanyInfo(ObjUsersModel);
            }
            return View(ObjUsersModel);
        }


        private void BindCurrentCompanyInfo(UsersModel ObjUsersModel)
        {
            int LocationId = ConfigurationManager.AppSettings["usaintelLocationId"] != null ? Convert.ToInt32(ConfigurationManager.AppSettings["UsaIntelLocationId"].ToString()) : 8917;
            Proc_Get_CompanyDetailByLocationIdResult ObjCompanyDetailByLocationId = GetCompanyDetailsByLocationId(LocationId);

            if (ObjCompanyDetailByLocationId != null)
            {
                ObjUsersModel.PkCompanyId = ObjCompanyDetailByLocationId.pkCompanyId;
                ObjUsersModel.fkLocationId = ObjCompanyDetailByLocationId.pkLocationId;
                ObjUsersModel.fkStateId = ObjCompanyDetailByLocationId.fkStateID;
                ObjUsersModel.SelectedCompanyName = ObjCompanyDetailByLocationId.CompanyName;
            }
        }
        /// <summary>
        /// Function To Bind the All the Textboxes Values While Updating Process .
        /// </summary>
        /// <param name="ObjUserDetailByLocationId"></param>
        private void BindFieldsForOfficeUsers(tblCompanyUser ObjtblCompanyUser, UsersModel ObjUsersModel)
        {
            ObjBALOffice = new BALOffice();
            #region Membership Info
            ObjUsersModel.fkUserId = ObjtblCompanyUser.fkUserId;
            MembershipUser ObjMembershipUser = Membership.GetUser(ObjtblCompanyUser.fkUserId);
            string[] ObjRoles = Roles.GetRolesForUser(ObjMembershipUser.UserName);

            ObjUsersModel.LastLoginByUser = ObjMembershipUser.LastLoginDate.ToString();
            ObjUsersModel.UserEmail = ObjMembershipUser.UserName;

            string Password = ObjMembershipUser.GetPassword();
            ObjUsersModel.UserPassword = Password;


            #endregion

            #region User Personal Info
            // LoadManagers();
            Selected_ManagerID = ObjBALOffice.GetManagerID(ObjtblCompanyUser.fkUserId, out Selected_RelationID);
            if (Selected_ManagerID != Guid.Empty)
            {
                ObjUsersModel.Selected_RelationID = Selected_RelationID;
                ObjUsersModel.trManagerVisible = true;

                ObjUsersModel.ManagerUserId = Selected_ManagerID;
            }
            else
            {
                ObjUsersModel.trManagerVisible = true;
                if (ObjRoles.Contains("SalesManager") || ObjRoles.Contains("SalesAdmin"))
                {
                    ObjUsersModel.trManagerVisible = true;
                }
            }
            //trLastLogin.Visible = true;
            //trUserImage.Visible = true;

            ObjUsersModel.FirstName = ObjtblCompanyUser.FirstName;
            ObjUsersModel.UserNote = ObjtblCompanyUser.UserNote;
            ObjUsersModel.LastName = ObjtblCompanyUser.LastName;
            ObjUsersModel.Initials = ObjtblCompanyUser.Initials;
            if (multiple_Roles.Count() > 1)
            {
                BindDropDownsStatus(multiple_Roles[1], ObjUsersModel);
            }
            else
            {
                BindDropDownsStatus(multiple_Roles[0], ObjUsersModel);
            }

            ObjUsersModel.RoleId = Selected_RoleID;
            ObjUsersModel.Description = GetRoleDescription(Selected_RoleID);

            int CompanyId = GetCompanyIdByLocationId((int)ObjtblCompanyUser.fkLocationId);
            // BindLocation(CompanyId);

            ObjUsersModel.PkCompanyId = CompanyId;
            ObjUsersModel.fkLocationId = ObjtblCompanyUser.fkLocationId;
            // ddlCompany.Enabled = false;

            //if (ddlState.Items.Count == 0)
            //{
            //    BindState();
            //}

            ObjUsersModel.fkStateId = (ObjtblCompanyUser.fkStateId == null) ? -1 : ObjtblCompanyUser.fkStateId;

            //int StateId;
            //int.TryParse(ddlState.SelectedValue, out StateId);
            //LoadStateCounties(StateId);
            ObjUsersModel.fkCountyId = ObjtblCompanyUser.fkCountyId;

            ObjUsersModel.UserAddressLine1 = ObjtblCompanyUser.UserAddressLine1;
            ObjUsersModel.UserAddressLine2 = ObjtblCompanyUser.UserAddressLine2;
            ObjUsersModel.PhoneNo = ObjtblCompanyUser.PhoneNo;
            ObjUsersModel.ZipCode = ObjtblCompanyUser.ZipCode;

            ObjUsersModel.IsEnabled = ObjtblCompanyUser.IsEnabled;

            if (ObjtblCompanyUser.IsSignedFcra != null)
            {
                if (ObjtblCompanyUser.IsSignedFcra == true)
                {
                    ObjUsersModel.IsSignedFcra = true;
                }
                else
                {
                    ObjUsersModel.IsSignedFcra = false;
                }
            }

            #region User Image

            if (ObjtblCompanyUser.UserImage == string.Empty || !System.IO.File.Exists(Server.MapPath(@"~\Resources\Upload\Images\" + ObjtblCompanyUser.UserImage)))
            {
                ObjUsersModel.ViewImage = ApplicationPath.GetApplicationPath() + @"Content/themes/base/images/NotAvail.png";

            }
            else
            {
                ObjUsersModel.OldUserImage = ObjtblCompanyUser.UserImage;
                ObjUsersModel.ViewImage = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjtblCompanyUser.UserImage;
            }

            #endregion


            #endregion

            #region ViewState Info

            ObjUsersModel.pkCompanyUserId = ObjtblCompanyUser.pkCompanyUserId;
            ObjUsersModel.fkUserId = ObjtblCompanyUser.fkUserId;
            ObjUsersModel.OldUserPassword = Password;
            ObjUsersModel.OldUserEmail = ObjMembershipUser.UserName.Trim();

            #endregion

        }

        public void BindDropDownsStatus(string Role, UsersModel ObjUserModel)
        {
            ObjBALGeneral = new BALGeneral();
            List<aspnet_Role> RolesColl = null;
            try
            {
                RolesColl = ObjBALGeneral.FetchRolesAll(Utility.SiteApplicationId);
                if (RolesColl.Count != 0)
                {
                    //This is false means selected company from dropdown list is not INTELIFI
                    //Then remove following role from Dropdown list
                    if (!String.IsNullOrEmpty(Role))
                    {
                        if ((Role.ToLower() == "systemadmin"))//multiple_Roles[0].ToLower() == "admin" ||
                        {
                            ObjUserModel.ddlStatusEnabled = "true";
                            ObjUserModel.ddlManagersEnabled = "true";
                        }

                        else if ((Role.ToLower() == "salesadmin"))
                        {
                            ObjUserModel.ddlStatusEnabled = "true";
                        }
                        else
                        {
                            ObjUserModel.ddlStatusEnabled = "false";
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ObjBALGeneral = null;
            }
        }

        public ActionResult BindStatusForOfficeUserAccount()
        {
            ObjBALGeneral = new BALGeneral();
            string Role = string.Empty;
            MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));

            if (ObjMembershipUser == null)
            {
                HttpContext.Response.Redirect(ApplicationPath.GetApplicationPath() + "Office/Default");
            }

            multiple_Roles = Roles.GetRolesForUser(ObjMembershipUser.UserName);
            if (multiple_Roles.Count() > 1)
            {
                Role = multiple_Roles[1];
            }
            else
            {
                Role = multiple_Roles[0];
            }
            ObjBALGeneral = new BALGeneral();
            List<ClsRoles> RolesColl = new List<ClsRoles>();


            try
            {
                RolesColl = ObjBALGeneral.FetchRolesAll(Utility.SiteApplicationId).Select(d => new ClsRoles
                {
                    RoleId = d.RoleId,
                    Description = d.Description,
                    RoleName = d.RoleName
                }).ToList();




                if (RolesColl.Count != 0)
                {
                    //This is false means selected company from dropdown list is not INTELIFI
                    //Then remove following role from Dropdown list
                    if (!String.IsNullOrEmpty(Role))
                    {
                        if ((Role.ToLower() == "systemadmin"))//multiple_Roles[0].ToLower() == "admin" ||
                        {

                            RolesColl = RolesColl.Where(d => d.RoleName == "" || d.RoleName == "SalesManager" || d.RoleName == "SalesRep" || d.RoleName == "SalesAdmin").ToList<ClsRoles>();

                        }

                        else if ((Role.ToLower() == "salesadmin"))
                        {
                            RolesColl = RolesColl.Where(d => d.RoleName == "" || d.RoleName == "SalesManager" || d.RoleName == "SalesRep").ToList<ClsRoles>();

                        }
                        else
                        {
                            RolesColl = RolesColl.Where(d => d.RoleName.ToLower() == Role.ToLower()).OrderBy(d => d.RoleName).ToList<ClsRoles>();
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ObjBALGeneral = null;
            }
            return Json(RolesColl.OrderBy(d => d.RoleName));
        }

        public void SetRoleBaseAccess(string[] User_Roles, UsersModel ObjUsersModel)
        {
            if (multiple_Roles.Contains("SystemAdmin") || multiple_Roles.Contains("SalesAdmin"))
            {
                // LoadManagers();
                ObjUsersModel.trManagerVisible = false;
                ObjUsersModel.ddlManagersEnabled = "true";
            }
            else if (multiple_Roles.Contains("SalesRep") && ObjUsersModel.IsPageModeView == true)
            {
                ObjUsersModel.ddlManagersEnabled = "false";
                ObjUsersModel.ddlStatusEnabled = "false";
            }
            else if (multiple_Roles.Contains("SalesManager") && ObjUsersModel.IsPageModeEdit == true)
            {
                ObjUsersModel.ddlStatusEnabled = "false";
                ObjUsersModel.ddlManagersEnabled = "false";
            }

        }

        #region Add/Update Office User

        [ValidateInput(false)]
        public ActionResult SaveOfficeUserAccountInfoInDB(UsersModel ObjUsersModel, HttpPostedFileBase UserImage)
        {
            string strResult = string.Empty;

            if (!string.IsNullOrEmpty(ObjUsersModel.QueryStringUserId))
            {
                string OldUserName = ObjUsersModel.OldUserEmail;
                string NewUserName = ObjUsersModel.UserEmail;
                bool bResult = IsExistsUsername(OldUserName, NewUserName);

                if (bResult)
                {
                    strResult = "<span class='infomessage'>This Username already exists</span>";
                    TempData["message"] = strResult;
                    return Json(new { Message = strResult });
                }

                strResult = UpdateOfficeUserAccountInDB(ObjUsersModel, UserImage);
            }
            else
            {
                strResult = CreateOfficeUserInDB(ObjUsersModel, UserImage);
            }
            TempData["message"] = strResult;
            if (NewPkCompanyId != 0 && NewCompanyUserId != 0)
            {
                ObjBALCompanyUsers = new BALCompanyUsers();
                Guid FkUserId = ObjBALCompanyUsers.GetfkUserId(NewCompanyUserId);
                return RedirectToAction("UserAccount", new { _a = FkUserId, _c = NewPkCompanyId });
            }
            return RedirectToAction("UserAccount", new { _a = ObjUsersModel.QueryStringUserId, _c = ObjUsersModel.QueryStringCompanyId });

        }

        /// <summary>
        /// Function To Update User 
        /// </summary>
        public string UpdateOfficeUserAccountInDB(UsersModel ObjUsersModel, HttpPostedFileBase UserImage)
        {
            string strResult = string.Empty;
            tblCompanyUser ObjtblCompanyUser = new tblCompanyUser();
            BALOffice ObjBALOffice = new BALOffice();
            tblSalesUserRelationship ObjtblSalesUserRelationship = new tblSalesUserRelationship();
            ObjBALCompanyUsers = new BALCompanyUsers();
            BLLMembership ObjBLLMembership = new BLLMembership();

            try
            {
                ObjtblCompanyUser.fkUserId = ObjUsersModel.fkUserId;
                ObjtblCompanyUser.pkCompanyUserId = ObjUsersModel.pkCompanyUserId;
                ObjtblCompanyUser.fkLocationId = ObjUsersModel.fkLocationId;
                ObjtblCompanyUser.FirstName = ObjUsersModel.FirstName;
                ObjtblCompanyUser.LastName = ObjUsersModel.LastName;
                ObjtblCompanyUser.Initials = ObjUsersModel.Initials;
                ObjtblCompanyUser.UserAddressLine1 = !string.IsNullOrEmpty(ObjUsersModel.UserAddressLine1) ? ObjUsersModel.UserAddressLine1 : string.Empty;
                ObjtblCompanyUser.UserAddressLine2 = !string.IsNullOrEmpty(ObjUsersModel.UserAddressLine2) ? ObjUsersModel.UserAddressLine2 : string.Empty;
                ObjtblCompanyUser.PhoneNo = !string.IsNullOrEmpty(ObjUsersModel.PhoneNo) ? ObjUsersModel.PhoneNo : string.Empty;
                ObjtblCompanyUser.UserNote = !string.IsNullOrEmpty(ObjUsersModel.UserNote) ? ObjUsersModel.UserNote : string.Empty;
                ObjtblCompanyUser.ZipCode = !string.IsNullOrEmpty(ObjUsersModel.ZipCode) ? ObjUsersModel.ZipCode : string.Empty;

                ObjtblCompanyUser.fkStateId = ObjUsersModel.fkStateId;


                if (Convert.ToString(ObjUsersModel.fkCountyId) != "-1")
                {
                    ObjtblCompanyUser.fkCountyId = ObjUsersModel.fkCountyId;
                }
                else
                {
                    ObjtblCompanyUser.fkCountyId = null;
                }
                ObjtblCompanyUser.IsEnabled = ObjUsersModel.IsEnabled;
                ObjtblCompanyUser.LastModifiedById = ObjUsersModel.CompanyUserId;
                ObjtblCompanyUser.LastModifiedDate = System.DateTime.Now;
                ObjtblCompanyUser.IsSignedFcra = ObjUsersModel.IsSignedFcra;
                ObjtblCompanyUser.UserImage = string.Empty;

                if (UserImage != null)
                {
                    if (UserImage != null && UserImage.ContentLength > 0)
                    {
                        if (ObjUsersModel.OldUserImage != null)
                        {
                            string oldimage_Path = Server.MapPath(@"~\Resources\Upload\Images\") + ObjUsersModel.OldUserImage;
                            if (System.IO.File.Exists(oldimage_Path))
                            {
                                System.IO.File.Delete(oldimage_Path);
                            }
                        }
                        string UserImageFileName = ObjUsersModel.FirstName + ObjUsersModel.LastName + "_" + DateTime.Now.ToFileTime().ToString() + System.IO.Path.GetExtension(UserImage.FileName);
                        string upload_Path = Server.MapPath(@"~\Resources\Upload\Images\") + UserImageFileName;
                        UserImage.SaveAs(upload_Path);

                        ObjtblCompanyUser.UserImage = UserImageFileName;
                    }

                }
                else
                {
                    if (ObjUsersModel.OldUserImage != null)
                    {
                        ObjtblCompanyUser.UserImage = ObjUsersModel.OldUserImage;
                    }
                }

                int Result = ObjBALCompanyUsers.UpdateCompanyUsers(ObjtblCompanyUser, ObjUsersModel.RoleId);

                if (Result > 0)
                {

                    ObjtblSalesUserRelationship.pkSalesRelationId = ObjUsersModel.Selected_RelationID;
                    ObjtblSalesUserRelationship.fkSalesManagerId = ObjUsersModel.ManagerUserId;
                    //int iresult = 
                        ObjBALOffice.UpdateRelation(ObjtblSalesUserRelationship);


                    #region Update User Name and password and Role

                    MembershipUser ObjMembershipUser = Membership.GetUser(ObjUsersModel.OldUserEmail);

                    string OldUserName = ObjUsersModel.OldUserEmail;
                    string NewUserName = ObjUsersModel.UserEmail;

                    if (OldUserName.ToLower().Trim() != NewUserName.ToLower())
                    {
                        UpdateUsername(OldUserName, NewUserName);

                        #region Change email address
                        UpdateUserEmail(NewUserName, ObjUsersModel.fkUserId);
                        #endregion
                        RegistrationEmail(ObjUsersModel.UserEmail, ObjUsersModel.UserPassword);

                        #region Deduct last activity time

                        if (ObjMembershipUser != null)
                        {
                            ObjBLLMembership.SignOut(ObjMembershipUser.UserName, DateTime.Now.ToUniversalTime().AddMinutes(-(Membership.UserIsOnlineTimeWindow)));
                        }

                        #endregion
                    }

                    string OldPwd = ObjUsersModel.OldUserPassword;

                    if (ObjMembershipUser != null)
                    {
                        if (OldPwd != ObjUsersModel.UserPassword)
                        {
                            Membership.GetUser(ObjUsersModel.OldUserEmail).ChangePassword(OldPwd, ObjUsersModel.UserPassword);
                        }
                    }

                    #endregion

                    strResult = "<span class='successmsg'>Record Updated Successfully</span>";
                }
                else
                {
                    strResult = "<span class='errormsg'>Error Occured While Updating Record</span>";
                }
            }
            catch //(Exception ex)
            {
                strResult = "<span class='errormsg'>Error Occured While Updating Record</span>";
            }
            finally
            {
                ObjtblCompanyUser = null;
                ObjBALCompanyUsers = null;
                ObjBLLMembership = null;
            }
            return strResult;
        }

        /// <summary>
        /// Function To Craete New Membership User 
        /// </summary>
        private string CreateOfficeUserInDB(UsersModel ObjUsersModel, HttpPostedFileBase UserImage)
        {
            string strResult = string.Empty;
            BLLMembership ObjBLLMembership = new BLLMembership();

            try
            {
                MembershipCreateStatus Status_Variable;
                Membership.ApplicationName = Utility.SiteApplicationName;
                MembershipUser ObjMembershipUser = Membership.CreateUser(ObjUsersModel.UserEmail,
                                                                         ObjUsersModel.UserPassword,
                                                                         ObjUsersModel.UserEmail,
                                                                         null,
                                                                         null,
                                                                         true,
                                                                         out Status_Variable);

                #region Deduct last activity time

                ObjMembershipUser = Membership.GetUser(ObjUsersModel.UserEmail);
                if (ObjMembershipUser != null)
                {
                    ObjBLLMembership.SignOut(ObjMembershipUser.UserName, DateTime.Now.ToUniversalTime().AddMinutes(-(Membership.UserIsOnlineTimeWindow)));
                }

                #endregion

                switch (Status_Variable)
                {
                    case MembershipCreateStatus.DuplicateUserName:
                        strResult = "<span class='errormsg'>Email aready exists, Please choose another</span>";
                        break;

                    case MembershipCreateStatus.InvalidUserName:
                        strResult = "<span class='errormsg'>Invalid Email</span>";
                        break;

                    case MembershipCreateStatus.ProviderError:
                        strResult = "<span class='errormsg'>Provider Error occured please try later on</span>";
                        break;

                    case MembershipCreateStatus.Success:
                        string RoleName = ObjUsersModel.Description == "Sales Associate" ? "SalesRep" : ObjUsersModel.Description.Replace(" ", "");
                        Roles.ApplicationName = Utility.SiteApplicationName;// Convert.ToString(Session["ApplicationName"]);
                        Roles.AddUserToRole(ObjUsersModel.UserEmail, RoleName);
                        if ((Roles.IsUserInRole(ObjUsersModel.UserEmail, RoleName)) && (AddUserinDB(new Guid(Membership.GetUser(ObjUsersModel.UserEmail).ProviderUserKey.ToString()), ObjUsersModel, UserImage)))
                        {
                            RegistrationEmail(ObjUsersModel.UserEmail, ObjUsersModel.UserPassword);
                            strResult = "<span class='successmsg'>New User Added Sucessfully</span>";
                        }
                        else
                        {
                            Membership.DeleteUser(ObjUsersModel.UserEmail, true);
                            strResult = "<span class='errormsg'>Error Occured While Adding New User</span>";
                        }
                        break;
                }
            }
            finally
            {
                ObjBLLMembership = null;
            }
            return strResult;
        }

        /// <summary>
        /// This method is used to update user email address
        /// </summary>
        /// <param name="NewEmail"></param>
        private void UpdateUserEmail(string NewEmail, Guid fkUserId)
        {
            if (fkUserId != null)
            {
                MembershipUser mu = Membership.GetUser(fkUserId);

                if (mu != null)
                {
                    mu.Email = NewEmail;
                    Membership.UpdateUser(mu);
                }

            }
        }
        public void RegistrationEmail(string UserEmail, string Password)
        {
            BALGeneral objBALGeneral = new BALGeneral();
            try
            {
                string emailText = "";
                string MessageBody = "";
                BALGeneral ObjBALGeneral = new BALGeneral();
                string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;
                var emailContent = objBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, Convert.ToInt32(EmailTemplates.NewUserRegistration));//"New User Registration"
                emailText = emailContent.TemplateContent;

                #region New way Bookmark

                BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                Bookmarks objBookmark = new Bookmarks();

                objBookmark.EmailContent = emailText;

                objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                objBookmark.CompanyName = ObjBALGeneral.GetSettings(1).CompanyName;
                objBookmark.UserEmail = UserEmail.Trim();
                objBookmark.UserPassword = Password.Trim();
                objBookmark.LoginLink = "<a style='color: #87ceea;' href='" + ApplicationPath.GetApplicationPath() + "'>Login Now !</a>";

                MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Utility.GetUID(User.Identity.Name));

                #endregion

                Utility.SendMail(UserEmail.Trim(), string.Empty, string.Empty, BALGeneral.GetGeneralSettings().SupportEmailId, MessageBody, "eMerge New User Registration");
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        /// <summary>
        /// Function to Add new User and its Location To the Database 
        /// </summary>
        public bool AddUserinDB(Guid FkUserId, UsersModel ObjUsersModel, HttpPostedFileBase UserImage)
        {
            BALOffice ObjBALOffice = new BALOffice();
            tblCompanyUser ObjtblCompanyUser = new tblCompanyUser();
            tblSalesUserRelationship ObjtblSalesUserRelationship = new tblSalesUserRelationship();
            ObjBALCompanyUsers = new BALCompanyUsers();
            bool ReturnValue = false;
            DateTime currentDate = DateTime.Now;
            

            try
            {

                using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
                {
                    var ch = (from aspUser in ObjDALDataContext.aspnet_Users
                              join compUser in ObjDALDataContext.tblCompanyUsers
                              on aspUser.UserId equals compUser.fkUserId
                              where aspUser.UserName.Equals(ObjUsersModel.CurrentUserEmail)
                              select compUser).FirstOrDefault();
                    if (ch != null)
                    {
                        ObjUsersModel.fkLocationId = ch.fkLocationId;
                        ObjUsersModel.fkStateId = ch.fkStateId;
                        ObjUsersModel.fkCountyId = ch.fkCountyId;
                    }
                }


                ObjtblCompanyUser.CreatedById = CompanyUserId;
                ObjtblCompanyUser.CreatedDate = System.DateTime.Now;
                ObjtblCompanyUser.FirstName = ObjUsersModel.FirstName;

                ObjtblCompanyUser.UserCode = Utility.GenerateUserCode(ObjUsersModel.SelectedCompanyName, currentDate);

                if (ObjUsersModel.fkCountyId != -1)
                {
                    ObjtblCompanyUser.fkCountyId = ObjUsersModel.fkCountyId;
                }
                else
                {
                    ObjtblCompanyUser.fkCountyId = null;
                }

                ObjtblCompanyUser.fkLocationId = ObjUsersModel.fkLocationId;
                ObjtblCompanyUser.fkStateId = ObjUsersModel.fkStateId;
                ObjtblCompanyUser.IsDeleted = false;
                ObjtblCompanyUser.IsEnabled = true;
                ObjtblCompanyUser.IsSignedFcra = ObjUsersModel.IsSignedFcra;
                ObjtblCompanyUser.fkUserId = FkUserId;
                ObjtblCompanyUser.LastName = ObjUsersModel.LastName;
                ObjtblCompanyUser.Initials = ObjUsersModel.Initials;


                ObjtblCompanyUser.UserAddressLine1 = !string.IsNullOrEmpty(ObjUsersModel.UserAddressLine1) ? ObjUsersModel.UserAddressLine1 : string.Empty;
                ObjtblCompanyUser.UserAddressLine2 = !string.IsNullOrEmpty(ObjUsersModel.UserAddressLine2) ? ObjUsersModel.UserAddressLine2 : string.Empty;
                ObjtblCompanyUser.PhoneNo = !string.IsNullOrEmpty(ObjUsersModel.PhoneNo) ? ObjUsersModel.PhoneNo : string.Empty;
                ObjtblCompanyUser.UserNote = !string.IsNullOrEmpty(ObjUsersModel.UserNote) ? ObjUsersModel.UserNote : string.Empty;
                ObjtblCompanyUser.ZipCode = !string.IsNullOrEmpty(ObjUsersModel.ZipCode) ? ObjUsersModel.ZipCode : string.Empty;
                ObjtblCompanyUser.UserImage = string.Empty;
                if (UserImage != null)
                {
                    if (UserImage != null && UserImage.ContentLength > 0)
                    {
                        string UserImageFileName = ObjUsersModel.FirstName + ObjUsersModel.LastName + "_" + DateTime.Now.ToFileTime().ToString() + System.IO.Path.GetExtension(UserImage.FileName);
                        string upload_Path = Server.MapPath(@"~\Resources\Upload\Images\") + UserImageFileName;
                        UserImage.SaveAs(upload_Path);

                        ObjtblCompanyUser.UserImage = UserImageFileName;
                    }

                }
                else
                {
                    ObjtblCompanyUser.UserImage = string.Empty;
                }

                int Result = ObjBALCompanyUsers.InsertCompanyUser(ObjtblCompanyUser);

                string RoleName = ObjUsersModel.Description == "Sales Associate" ? "SalesRep" : ObjUsersModel.Description.Replace(" ", "");

                if (RoleName.ToLower().Contains("salesrep") && ObjUsersModel.ManagerUserId != Guid.Empty)
                {
                    ObjtblSalesUserRelationship.fkSalesManagerId = ObjUsersModel.ManagerUserId;
                    ObjtblSalesUserRelationship.fkSalesAssociateId = FkUserId;
                    //int iresult = 
                        ObjBALOffice.InsertRelation(ObjtblSalesUserRelationship);
                }

                if (Result != 0)
                {
                    NewPkCompanyId = ObjUsersModel.PkCompanyId;
                    NewCompanyUserId = Result;
                    ReturnValue = true;
                }
                else if (Result == 0)
                {
                    ReturnValue = false;
                }
            }
            catch (Exception)
            {
                ReturnValue = false;
            }
            finally
            {
                ObjtblCompanyUser = null;
                ObjBALCompanyUsers = null;
            }
            return ReturnValue;
        }
        public Proc_Get_CompanyDetailByLocationIdResult GetCompanyDetailsByLocationId(int PkLocationId)
        {
            Proc_Get_CompanyDetailByLocationIdResult ObjCompanyDetailByLocationId = new Proc_Get_CompanyDetailByLocationIdResult();
            BALCompany ObjBALCompany = new BALCompany();
           // string CompanyId = string.Empty;
            try
            {
                ObjCompanyDetailByLocationId = ObjBALCompany.GetCompanyDetailByLocationId(PkLocationId).FirstOrDefault();

            }
            catch (Exception)
            {

            }
            finally
            {
                ObjBALCompany = null;
            }
            return ObjCompanyDetailByLocationId;
        }
        public string GetRoleDescription(Guid RoleId)
        {
            string RoleDescription = string.Empty;

            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                RoleDescription = DX.aspnet_Roles.Where(d => d.RoleId == RoleId).FirstOrDefault().Description;
            }

            return RoleDescription;
        }
        #endregion Add/Update Office User
    }
}
