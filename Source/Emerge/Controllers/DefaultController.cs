﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Emerge.Services;
using Emerge.Models;
using Emerge.Common;
using System.Text;
using System.Web.Profile;
using WordPress.Net;
using System.Configuration;
using Emerge.Data;
using System.Globalization;
using System.IO;
using System.Web.UI;
using System.Xml.Linq;
using MangoChat;
using System.Web.Routing;
using System.Web.Http.WebHost.Routing;
using System.Data.Linq;
using MangoChat;
using System.Net;
using Newtonsoft.Json.Linq;


namespace Emerge.Controllers
{

    //This [RequireHttps] should be enabled before creating any build on Live or testProd4.
    //If team is working in local environment, then it will be . 
   //[RequireHttps]
    public class DefaultController : Controller
    {
        public string TaleoApiKeyHashCode(string emailaddress)
        {

            //Random _Random = new Random();
            string test_to_encrypt = emailaddress.ToString();


            byte[] arrbyte = new byte[test_to_encrypt.Length];
            using (System.Security.Cryptography.SHA1CryptoServiceProvider hash = new System.Security.Cryptography.SHA1CryptoServiceProvider())
            {
                arrbyte = hash.ComputeHash(Encoding.UTF8.GetBytes(test_to_encrypt));
            }
            string TaleoWebApiKeyOutput = System.Web.HttpServerUtility.UrlTokenEncode(arrbyte);

            return TaleoWebApiKeyOutput;
        }





        #region PageLoad Login
        public ActionResult Index()
        {

            //Added By Himesh Kumar
            System.IO.StreamWriter file = new System.IO.StreamWriter("c:\\EmergeLog.txt", true);
            file.AutoFlush = true;
            string IP = HttpContext.Request.UserHostAddress;


          

            file.WriteLine("The IP of the customer is:- " + IP);
            StringBuilder objStrBuilder = new StringBuilder();
            objStrBuilder.Append(Environment.NewLine + " * ***********************************************************************************************");
            objStrBuilder.Append(DateTime.UtcNow.ToString());
            objStrBuilder.Append("URL: -" + HttpContext.Request.Url + Environment.NewLine);
            objStrBuilder.Append("User Agent: -" + HttpContext.Request.UserAgent + Environment.NewLine);
            objStrBuilder.Append("Version: -" + HttpContext.Request.Browser.Version + Environment.NewLine);
            objStrBuilder.Append("Major Version: -" + HttpContext.Request.Browser.MajorVersion + Environment.NewLine);
            objStrBuilder.Append("Minor Version: -" + HttpContext.Request.Browser.MinorVersion + Environment.NewLine);
            objStrBuilder.Append(Environment.NewLine);
            objStrBuilder.Append(Environment.NewLine);
            objStrBuilder.Append(Environment.NewLine);
            objStrBuilder.Append(Environment.NewLine);
            file.WriteLine(objStrBuilder);
            file.Close();


            // int 260 iframe integration 
            if (Request.Form.HasKeys())
            {
                TempData["IframeData"] = Request.Form;

            }



            LoginModel Model = new LoginModel();
            string username;
            string password;
            string UserRole;
            bool Remembered;
            try
            {
                Utility.GetCookie(out username, out password, out UserRole, out Remembered);
                Model.listDefaultPages = checkusers(UserRole);
                Model.UserFistLastName = null;

                #region auto login funtionality for Taleo


                TempData["loginfrom"] = Convert.ToString(Request.QueryString["loginfrom"]);
                if (TempData["loginfrom"] != null)
                {
                    //nd-16 changes on 02 julyb 2015
                    if (Convert.ToString(TempData["loginfrom"]) == "taleo")
                    {

                        using (EmergeDALDataContext DX = new EmergeDALDataContext())
                        {

                            // string TaleoApiEmailCrossCheck= TaleoApiKeyHashCode(TempData["TaleoWebApiKey"].ToString())

                            var TaleoWebApiKey = (from pd in DX.aspnet_Memberships
                                                  join od in DX.tblCompanyUsers on pd.UserId equals od.fkUserId
                                                  where pd.TaleoApiKey == TempData["TaleoWebApiKey"].ToString()
                                                  select new { pd.Email, pd.Password }).FirstOrDefault();

                            if (TaleoWebApiKey != null)
                            {

                                if (TaleoWebApiKey.Email.ToString() != "")
                                {

                                    TempData["TaleoEmergeLoginEmailId"] = Convert.ToString(TaleoWebApiKey.Email.ToString());
                                    Model.UserName = Convert.ToString(TaleoWebApiKey.Email.ToString());


                                }
                                else
                                {

                                }
                            }
                        }


                    }
                }

                #endregion




                if (Remembered)
                {
                    Model.UserName = username;
                    ViewBag.Password = password;

                    //   Model.CompanyName = CompanyName;

                    Model.RemeberMe = true;
                    ProfileCommon Profile = new ProfileCommon();
                    BALContentManagement objContentsMgt = new BALContentManagement();
                    Model.UserFistLastName = string.Empty;
                    var list = objContentsMgt.GetCompanyNameByEmailId(username);
                    if (list.UserImage != "" && list.UserImage != null)
                    {
                        Model.UserImage = list.UserImage;
                    }
                    else
                    {
                        Model.UserImage = null;
                    }
                    var list2 = objContentsMgt.GetCompanyNameByUserId(username);
                    var FirstName = list.FirstName;
                    var LastName = list.LastName;
                    var firstandlastname = FirstName + "  " + LastName;
                    CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
                    TextInfo textInfo = cultureInfo.TextInfo;
                    string Searchedfirstname = firstandlastname != null ? textInfo.ToTitleCase(firstandlastname) : string.Empty;
                    Model.UserFistLastName = Searchedfirstname;
                    Model.CompanyName = list2.CompanyName;
                    ProfileModel userProfile = Profile.GetProfile(username);
                    ViewBag.tdTakeMeTo = "true";
                    ViewBag.tdDDLDefaultPage = "true";
                    ViewBag.DDLDefaultPage = "true";
                    ViewBag.lblTakeMeTo = "true";
                    if (userProfile.DefaultPage != "")
                    {
                        Model.listDefaultPages = Model.listDefaultPages.Select(x => new SelectListItem { Text = x.Text, Value = x.Value, Selected = (x.Value == userProfile.DefaultPage) }).ToList();
                    }
                }
                BALGeneral ObjBALGeneral = new BALGeneral();
                byte settingId = 1;
                var settings = ObjBALGeneral.GetSettings(settingId);
                if (settings != null)
                {
                    BALContentManagement objContents = new BALContentManagement();
                    tblLoginPageManagement ObjtblLoginMgt = new tblLoginPageManagement();
                    tblLoginPageSetting objtblLoginPageSetting = new tblLoginPageSetting();
                    objtblLoginPageSetting = objContents.GetLogoImg();
                    string logoImagePath = objtblLoginPageSetting.LoginLogoImg;
                    ViewBag.loginbackcolor = objtblLoginPageSetting.LoginBackColor;
                    var backimg = objtblLoginPageSetting.LoginBackImg;
                    ViewBag.loadingbgColor = objtblLoginPageSetting.LoadingImgColor;
                    var loadingBgImg = objtblLoginPageSetting.LoadingImage;
                    ViewBag.loadingbgImg = "Resources/Upload/LoginLogo/" + loadingBgImg;
                    var FinalBackImg = "Resources/Upload/LoginLogo/" + backimg;
                    ViewBag.loginbackImg = FinalBackImg;
                    ViewBag.loginBGPosition = objtblLoginPageSetting.LoginBackGroundPosition;
                    string FinalPath = "Resources/Upload/LoginLogo/" + logoImagePath;
                    ViewBag.logoImagePath = FinalPath;
                    var Col = objContents.Get_Login_Page_Content();
                    ObjtblLoginMgt = Col.Where(p => p.pkPageId == 1).FirstOrDefault();
                    // ViewBag.HomeContens = HttpUtility.HtmlDecode(ObjtblLoginMgt.Contents);
                    ViewBag.HomeColor = ObjtblLoginMgt.ModuleColor;
                    ViewBag.HomeIsEnable = ObjtblLoginMgt.IsEnable;
                    ObjtblLoginMgt = Col.Where(p => p.pkPageId == 2).FirstOrDefault();
                    var Newcontents = ObjtblLoginMgt.Contents;
                    Newcontents.Replace("&lt;", "<");
                    Newcontents.Replace("&gt;", ">");
                    //var finalcotens = Newcontents;
                    // ViewBag.NewsContens = ObjtblLoginMgt.Contents;
                    ViewBag.NewsColor = ObjtblLoginMgt.ModuleColor;
                    ViewBag.NewsIsEnable = ObjtblLoginMgt.IsEnable;
                    ObjtblLoginMgt = Col.Where(p => p.pkPageId == 3).FirstOrDefault();
                    //  ViewBag.ProductsContens = ObjtblLoginMgt.Contents;
                    ViewBag.ProductsColor = ObjtblLoginMgt.ModuleColor;
                    ViewBag.ProductsIsEnable = ObjtblLoginMgt.IsEnable;
                    ObjtblLoginMgt = Col.Where(p => p.pkPageId == 4).FirstOrDefault();
                    //  ViewBag.FeaturesContens = ObjtblLoginMgt.Contents;
                    ViewBag.FeaturesColor = ObjtblLoginMgt.ModuleColor;
                    ViewBag.FeaturesIsEnable = ObjtblLoginMgt.IsEnable;
                    ObjtblLoginMgt = Col.Where(p => p.pkPageId == 5).FirstOrDefault();
                    // ViewBag.ExploreContens = ObjtblLoginMgt.Contents;
                    ViewBag.ExploreColor = ObjtblLoginMgt.ModuleColor;
                    ViewBag.ExploreIsEnable = ObjtblLoginMgt.IsEnable;
                    ObjtblLoginMgt = Col.Where(p => p.pkPageId == 6).FirstOrDefault();
                    // ViewBag.ComplianceContens = ObjtblLoginMgt.Contents;
                    ViewBag.ComplianceColor = ObjtblLoginMgt.ModuleColor;
                    ViewBag.ComplianceIsEnable = ObjtblLoginMgt.IsEnable;
                    ObjtblLoginMgt = Col.Where(p => p.pkPageId == 7).FirstOrDefault();
                    //  ViewBag.FormsContens = ObjtblLoginMgt.Contents;
                    ViewBag.FormsColor = ObjtblLoginMgt.ModuleColor;
                    ViewBag.FormsIsEnable = ObjtblLoginMgt.IsEnable;
                    ObjtblLoginMgt = Col.Where(p => p.pkPageId == 8).FirstOrDefault();
                    // ViewBag.PrivacyContens = ObjtblLoginMgt.Contents;
                    ViewBag.PrivacyColor = ObjtblLoginMgt.ModuleColor;
                    ViewBag.PrivacyIsEnable = ObjtblLoginMgt.IsEnable;
                    ObjtblLoginMgt = Col.Where(p => p.pkPageId == 9).FirstOrDefault();
                    //  ViewBag.ContactContens = ObjtblLoginMgt.Contents;
                    ViewBag.ContactColor = ObjtblLoginMgt.ModuleColor;
                    ViewBag.ContactIsEnable = ObjtblLoginMgt.IsEnable;
                    ObjtblLoginMgt = Col.Where(p => p.pkPageId == 10).FirstOrDefault();
                    //  ViewBag.CalenderContens = ObjtblLoginMgt.Contents;
                    ViewBag.CalenderColor = ObjtblLoginMgt.ModuleColor;
                    ViewBag.CalenderIsEnable = ObjtblLoginMgt.IsEnable;
                    BALContentManagement ObjBALContentManagement = new BALContentManagement();
                    ViewBag.Collection = ObjBALContentManagement.GetAllFeaturesEnabled();
                    ViewBag.rss_col = GetRssFeed();
                    var ContactInfo = ObjBALContentManagement.GetContactInfo().FirstOrDefault();
                    ViewBag.ContactAddress1 = ContactInfo.CntctAddress1 == null ? string.Empty : ContactInfo.CntctAddress1;
                    ViewBag.ContactAddress2 = ContactInfo.CntctAddress2 == null ? string.Empty : ContactInfo.CntctAddress2;
                    ViewBag.CntctCity = ContactInfo.CntctCity == null ? string.Empty : ContactInfo.CntctCity;
                    ViewBag.CntctStateCode = ContactInfo.CntctStateCode == null ? string.Empty : ContactInfo.CntctStateCode;
                    ViewBag.CntctZipCode = ContactInfo.CntctZipCode == null ? string.Empty : ContactInfo.CntctZipCode;
                    ViewBag.CntctPhoneNo1 = ContactInfo.CntctPhoneNo1 == null ? string.Empty : ContactInfo.CntctPhoneNo1;
                    ViewBag.CntctFax = ContactInfo.CntctFax == null ? string.Empty : ContactInfo.CntctFax;
                    ViewBag.CntctEmail = ContactInfo.CntctEmail == null ? string.Empty : ContactInfo.CntctEmail;
                    List<MyFormModelColl> MyFormModel = new List<MyFormModelColl>();
                    var Collection = GetMyFormCollection().ToList();
                    if (Collection.Count > 0)
                    {
                        for (int i = 0; i < Collection.Count; i++)
                        {
                            MyFormModel.Add(new MyFormModelColl()
                            {
                                pkformId = Collection.ElementAt(i).pkformId,
                                Title = Collection.ElementAt(i).Title,
                                Path = Collection.ElementAt(i).Path,
                                Description = Collection.ElementAt(i).Description,
                                ModifiedDate = Convert.ToDateTime(Collection.ElementAt(i).ModifiedDate),
                                ModifiedLoggedId = Collection.ElementAt(i).ModifiedLoggedId != null ? (Guid)Collection.ElementAt(i).ModifiedLoggedId : Guid.Empty
                            });
                        }
                        Model.TotalCounts = Collection.Count;
                        Model.RowCounts = Collection.Count / 5;
                    }
                    Model.Collection = MyFormModel;
                }


                TempData["Model"] = Model;// nd-16
            }
            catch { }

            return View(Model);
        }
        #endregion
        #region Login
        [HttpPost]
        public ActionResult Login(LoginModel Model)
        {
            return ProcessLogin(Model);
        }
        public void taleomain()
        {
            TempData["loginfrom"] = Convert.ToString(Request.QueryString["loginfrom"]);
            if (TempData["loginfrom"] != null)
            {
                Session["loginfrom"] = TempData["loginfrom"];
                if (Convert.ToString(TempData["loginfrom"]) == "taleo")
                {
                    TempData["TaleoCandidateId"] = Convert.ToInt32(Request.QueryString["TaleoCandidateId"]);
                    TempData["TaleoCOMPANY_ID"] = Convert.ToString(Request.QueryString["TaleoCOMPANY_ID"]);
                    TempData["TaleoUSER_EMPLOYEE_ID"] = Convert.ToString(Request.QueryString["TaleoUSER_EMPLOYEE_ID"]);
                    TempData["TaleoUSER_RECIPIENT_ID"] = Convert.ToString(Request.QueryString["TaleoUSER_RECIPIENT_ID"]);
                    TempData["TaleoWebApiKey"] = Convert.ToString(Request.QueryString["TaleoWebApiKey"]);

                    VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n -------TempData[TaleoCOMPANY_ID]-- :" + TempData["TaleoCOMPANY_ID"] + "-----------------------", "TaleoErrorLog");
                    VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n ------- TaleoUSER_EMPLOYEE_ID--:" + TempData["TaleoUSER_EMPLOYEE_ID"] + "-----------------------", "TaleoErrorLog");
                    VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n -------TaleoUSER_RECIPIENT_ID-- :" + TempData["TaleoUSER_RECIPIENT_ID"] + "-----------------------", "TaleoErrorLog");
                    VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n ------- TaleoWebApiKey-- :" + TempData["TaleoWebApiKey"] + "-----------------------", "TaleoErrorLog");
                    if (TempData["TaleoCandidateId"] != null && TempData["TaleoWebApiKey"] != null)
                    {


                        string returnValueResult = getEmerge_TaleoDispatchCredInformation(Convert.ToString(TempData["TaleoWebApiKey"]));

                        VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n ------- returnValueResult-- :" + returnValueResult + "-----------------", "TaleoErrorLog");

                        if (returnValueResult == "success") // check its return type 
                        {


                            if (Convert.ToString(TempData["TaleoWebDispatcherURL"]) != "")
                            {
                                TempData["TaleoDispatcherUrlHit"] = TempData["TaleoWebDispatcherURL"];
                            }

                            else
                            {
                                taleoDispatcherlogin();
                            }



                            if (Convert.ToString(TempData["TaleoDispatchTaleoWebApiCookie"]) != "")
                            {

                                // if web api already exist then use it 

                                TempData["xmlResponseCookie"] = TempData["TaleoDispatchTaleoWebApiCookie"];
                            }
                            else
                            {
                                // create a new web apin key and access it all information

                                taleologin();
                            }


                            TaleoUserWork();
                            LoginModel Model = new LoginModel();
                            Index();
                            Model = TempData["Model"] as LoginModel;
                            if (Model.UserName != null)
                            {
                                ProcessLogin(Model);

                            }

                        }

                    }
                }
            }
        }

        public ActionResult ProcessLogin(LoginModel Model)
        {
            string Status = string.Empty;
            string Css = string.Empty;
            string CurrentUserName = string.Empty;


            MembershipUser ObjMembershipUser = Membership.GetUser(Model.UserName);

            #region taleo get pass word for auto login functionality
            if (TempData["loginfrom"] != null)
            {
                if (Convert.ToString(TempData["loginfrom"]) == "taleo")
                {

                    Model.Password = ObjMembershipUser.GetPassword();

                }
            }
            #endregion

            if (ObjMembershipUser != null)
            {
                HttpContext.Session["LastLoginDate"] = ObjMembershipUser.LastLoginDate;

            }
            string ReturnUrl = "../Corporate/NewReport";
            if (ObjMembershipUser != null)
            {
                Guid UserId = Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
                BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
                List<Proc_Get_UserInfoByMemberShipUserIdResult> UserInfoByMemberShip = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(UserId);


                ChatService.StartUserSession(System.Web.HttpContext.Current,
                   new MangoChat.User
                   {
                       // Unique user id by which MangoChat.net control will keep track of user
                       UserId = UserId.ToString(),
                       // User name to display in control
                       Username = UserInfoByMemberShip.ElementAt(0).FirstName,
                       // User avatar or display picture absolute path to be shown in control   
                       //   string refcode = ObjProc_Get_RequestAndResponseResult.First().SearchedTrackingRef == "" ? "NA" : ObjProc_Get_RequestAndResponseResult.First().SearchedTrackingRef;
                       ImageUrl = string.Empty
                   });

            }




            if (ObjMembershipUser != null)
            {
                if ((Roles.IsUserInRole(Model.UserName, "SystemAdmin")))//)!(Roles.IsUserInRole(UserName, "Admin") || 
                {
                    if (ObjMembershipUser.IsOnline)
                    {
                        // ScriptManager.RegisterStartupScript(this.Page, typeof(string), "IsLogged", "Alreadylogged();", true);
                        Status = "IsLogged";
                        Css = string.Empty;
                    }
                    else if (ObjMembershipUser.IsLockedOut)
                    {
                        //ScriptManager.RegisterStartupScript(this.Page, typeof(string), "IsLocked", "IsLocked();", true);
                        Status = "IsLocked";
                        Css = string.Empty;
                    }
                }
            }

            if (Membership.ValidateUser(Model.UserName, Model.Password))
            {
                Session["user_feature_id"] = ObjMembershipUser.ProviderUserKey;
                BALChat objBALChat = new BALChat();
                CurrentUserName = objBALChat.GetUserNamebyId(Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString()));
                int status = CheckAccountStatus(new Guid(Membership.GetUser(Model.UserName).ProviderUserKey.ToString()));

                //now check for last updated password 
                // IMPROVMENT, can be mergeed in same SQL sp proc_Check_User_Valid

                //get password duration from configuration

                /*
                 Ticket #47: Expiring password/force users to change password on Emerge
                  
                */



            /*    string password_expiration_duration = ConfigurationManager.AppSettings["PasswordExpiryDuration"].ToString();
                int int_password_expiration_duration = 90; //set default to 90, but pick from webconfig file
                if (!int.TryParse(password_expiration_duration, out  int_password_expiration_duration))
                { int_password_expiration_duration = 90; }

                if (ObjMembershipUser.LastPasswordChangedDate.Date.AddDays(int_password_expiration_duration) < DateTime.UtcNow.Date)
                {
                    //has gap 
                    status = -7;
                }*/


                if (Convert.ToInt32(status) > 0)
                {
                    //FormsAuthentication.RedirectFromLoginPage(Model.UserName, false);

                    string[] multiple_Roles = Roles.GetRolesForUser(Model.UserName);

                    if (Model.RemeberMe)
                    {
                        Utility.SetCookie(Model.UserName.Trim(), Model.Password.Trim(), multiple_Roles[0], true);
                    }
                    else
                    {
                        Utility.SetCookie(Model.UserName.Trim(), Model.Password.Trim(), multiple_Roles[0], false);
                    }


                    ProfileCommon profile = new ProfileCommon();
                    //ProfileModel userprofile = profile.GetProfile(Model.UserName);
                    profile.EditDefaultPageOfProfile(Model.UserName, string.Empty, "0");
                    //profile.GetProfileGroup("SearchParams").SetPropertyValue("IsReportNotGenerating", 0);
                    profile.Save();
                    //userProfile.IsReportNotGenerating = "0";
                    //userProfile.Save();

                    #region UserRoles

                    string[] UserRole = Roles.GetRolesForUser(Model.UserName);
                    Session["UserRoles"] = UserRole;

                    #region checkstatusforBilling
                    int BillingLockingStatus = CheckBillingLockingAccountStatus(new Guid(Membership.GetUser(Model.UserName).ProviderUserKey.ToString()));


                    if (BillingLockingStatus == 0)
                    {
                        Session["BillingLockingStatus"] = "0";
                    }
                    else
                    {
                        Session["BillingLockingStatus"] = "1";
                    }

                    #endregion






                    #endregion

                    #region InsertDashBoard Colaps Status For New User

                    if (Roles.IsUserInRole(Model.UserName, "SystemAdmin"))//Roles.IsUserInRole(UserName, "Admin") || 
                    {

                    }
                    else
                    {
                        Guid CurrentUserId = Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
                        BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
                        ObjBALCompanyUsers.InsertDashBoardStatus(CurrentUserId);

                    }


                    #endregion

                    #region Role count check here

                    if (Roles.IsUserInRole(Model.UserName, "SystemAdmin"))//Roles.IsUserInRole(UserName, "Admin") || 
                    {
                        Session["Admin"] = "SystemAdmin";
                        if (!string.IsNullOrEmpty(Request.QueryString["ReturnUrl"]))
                        {
                            HttpContext.Response.Redirect(Request.QueryString["ReturnUrl"].ToString(), true);
                        }
                        else
                        {// add on 05 feb 2016 for Locking user to redirect only on billing page to pay invoice
                            //if (Convert.ToBoolean(ObjtblCompanyUser.New_Report) == true)
                            // {
                            #region taleo work and its check (check its order no and open according to its requirement)


                            //if (System.Web.HttpContext.Current.Session["loginfrom"] != null)
                            if (TempData["loginfrom"] != null)
                            {
                                //nd-16 changes on 02 julyb 2015
                                if (Convert.ToString(TempData["loginfrom"]) == "taleo")
                                {
                                    if (TempData["TaleoOrderNoStatus"] != null)
                                    {

                                        if (Convert.ToString(TempData["TaleoOrderNoStatus"]) != "" && !Convert.ToString(TempData["TaleoOrderNoStatus"]).Contains(","))
                                        {


                                            using (EmergeDALDataContext ObjDX = new EmergeDALDataContext())
                                            {
                                                try
                                                {
                                                    Int64 TaleoOrderId;
                                                    var Obj = (from t1 in ObjDX.tblOrders where t1.OrderNo == Convert.ToString(TempData["TaleoOrderNoStatus"]) select new { t1 }).First();
                                                    TaleoOrderId = Obj.t1.pkOrderId;
                                                    ReturnUrl = "../Corporate/ViewReports";
                                                    //System.Web.HttpContext.Current.Session["TaleoOrderId"] = Convert.ToInt64(TaleoOrderId);
                                                    TempData["TaleoOrderId"] = Convert.ToInt64(TaleoOrderId);  // order id change nd-16
                                                    //System.Web.HttpContext.Current.Session["TaleoOrderId"] = Convert.ToInt64(TaleoOrderId); edi change session to temp data 

                                                    ViewBag.TaleoOrderid = TempData["TaleoOrderId"];
                                                    //TempData
                                                    Status = "Success";
                                                    Css = string.Empty;

                                                }
                                                catch// (Exception ss)
                                                {

                                                }
                                            }




                                        }

                                        else if (Convert.ToString(TempData["TaleoOrderNoStatus"]).Contains(","))
                                        {
                                            ReturnUrl = "../Corporate/SavedReports";
                                            Status = "Success";
                                            Css = string.Empty;
                                        }
                                        else
                                        {
                                            ReturnUrl = "../Corporate/NewReport";
                                            Status = "Success";
                                            Css = string.Empty;
                                        }
                                    }

                                }
                            }

                            #endregion


                            else
                            {


                                ReturnUrl = "../Corporate/NewReport";
                                Status = "Success";
                                Css = string.Empty;




                            }


                        }
                    }
                    else if (Roles.IsUserInRole(Model.UserName, "SupportManager"))
                    {
                        Session["Admin"] = "SupportManager";
                        if (!string.IsNullOrEmpty(Request.QueryString["ReturnUrl"]))
                        {
                            HttpContext.Response.Redirect(Request.QueryString["ReturnUrl"].ToString(), true);
                        }
                        else
                        {
                            
                            //nd-16 changes on 02 julyb 2015
                            #region taleo work and its check (check its order no and open according to its requirement)


                            if (TempData["loginfrom"] != null)
                            {

                                if (Convert.ToString(TempData["loginfrom"]) == "taleo")
                                {
                                    if (TempData["TaleoOrderNoStatus"] != null)
                                    {

                                        if (Convert.ToString(TempData["TaleoOrderNoStatus"]) != "" && !Convert.ToString(TempData["TaleoOrderNoStatus"]).Contains(","))
                                        {
                                            using (EmergeDALDataContext ObjDX = new EmergeDALDataContext())
                                            {
                                                try
                                                {
                                                    Int64 TaleoOrderId;
                                                    var Obj = (from t1 in ObjDX.tblOrders where t1.OrderNo == Convert.ToString(TempData["TaleoOrderNoStatus"]) select new { t1 }).First();
                                                    TaleoOrderId = Obj.t1.pkOrderId;
                                                    ReturnUrl = "../Corporate/ViewReports";
                                                    // System.Web.HttpContext.Current.Session["TaleoOrderId"] = Convert.ToInt64(TaleoOrderId); nd-16

                                                    TempData["TaleoOrderId"] = Convert.ToInt64(TaleoOrderId);
                                                    ViewBag.TaleoOrderid = TempData["TaleoOrderId"];
                                                    Status = "Success";
                                                    Css = string.Empty;

                                                }
                                                catch //(Exception ss)
                                                {

                                                }
                                            }


                                        }

                                        else if (Convert.ToString(TempData["TaleoOrderNoStatus"]).Contains(","))
                                        {
                                            ReturnUrl = "../Corporate/SavedReports";
                                            Status = "Success";
                                            Css = string.Empty;
                                        }
                                        else
                                        {
                                            ReturnUrl = "../Corporate/NewReport";
                                            Status = "Success";
                                            Css = string.Empty;
                                        }
                                    }

                                }
                            }

                            #endregion

                            else
                            {// add on 05 feb 2016 for Locking user to redirect only on billing page to pay invoice
                                if (Convert.ToString(Session["BillingLockingStatus"]) == "0")
                                {
                                    ReturnUrl = "../Corporate/BillingStatements";
                                    Status = "Success";
                                    Css = string.Empty;
                                }

                                else
                                {
                                    ReturnUrl = "../Corporate/NewReport";
                                    Status = "Success";
                                    Css = string.Empty;
                                }




                            }




                        }
                    }


                    else if (Roles.IsUserInRole(Model.UserName, "SupportTechnician"))
                    {
                        Session["Admin"] = "SupportTechnician";
                        if (!string.IsNullOrEmpty(Request.QueryString["ReturnUrl"]))
                        {
                            HttpContext.Response.Redirect(Request.QueryString["ReturnUrl"].ToString(), true);
                        }
                        else
                        {
                            //if (Convert.ToBoolean(ObjtblCompanyUser.New_Report) == true)




                            //{
                            #region taleo work and its check (check its order no and open according to its requirement)

                            //nd-16 changes on 02 julyb 2015
                            if (TempData["loginfrom"] != null)
                            {

                                if (Convert.ToString(TempData["loginfrom"]) == "taleo")
                                {
                                    if (TempData["TaleoOrderNoStatus"] != null)
                                    {

                                        if (Convert.ToString(TempData["TaleoOrderNoStatus"]) != "" && !Convert.ToString(TempData["TaleoOrderNoStatus"]).Contains(","))
                                        {
                                            using (EmergeDALDataContext ObjDX = new EmergeDALDataContext())
                                            {
                                                try
                                                {
                                                    Int64 TaleoOrderId;
                                                    var Obj = (from t1 in ObjDX.tblOrders where t1.OrderNo == Convert.ToString(TempData["TaleoOrderNoStatus"]) select new { t1 }).First();
                                                    TaleoOrderId = Obj.t1.pkOrderId;
                                                    ReturnUrl = "../Corporate/ViewReports";
                                                    //System.Web.HttpContext.Current.Session["TaleoOrderId"] = Convert.ToInt64(TaleoOrderId); nd-16

                                                    TempData["TaleoOrderId"] = Convert.ToInt64(TaleoOrderId);
                                                    ViewBag.TaleoOrderid = TempData["TaleoOrderId"];
                                                    Status = "Success";
                                                    Css = string.Empty;

                                                }
                                                catch //(Exception ss)
                                                {

                                                }
                                            }

                                        }

                                        else if (Convert.ToString(TempData["TaleoOrderNoStatus"]).Contains(","))
                                        {
                                            ReturnUrl = "../Corporate/SavedReports";
                                            Status = "Success";
                                            Css = string.Empty;
                                        }
                                        else
                                        {
                                            ReturnUrl = "../Corporate/NewReport";
                                            Status = "Success";
                                            Css = string.Empty;
                                        }
                                    }

                                }
                            }

                            #endregion


                            else
                            {// add on 05 feb 2016 for Locking user to redirect only on billing page to pay invoice
                                if (Convert.ToString(Session["BillingLockingStatus"]) == "0")
                                {
                                    ReturnUrl = "../Corporate/BillingStatements";
                                    Status = "Success";
                                    Css = string.Empty;
                                }

                                else
                                {
                                    ReturnUrl = "../Corporate/NewReport";
                                    Status = "Success";
                                    Css = string.Empty;
                                }





                            }


                            //}
                        }
                    }
                    else if (Roles.IsUserInRole(Model.UserName, "BillingUser"))
                    {
                        ReturnUrl = "../Corporate/BillingStatements";
                        Status = "Success";
                        Css = string.Empty;


                    }
                    else
                    {
                        Session["Admin"] = "Other";
                        if (Roles.IsUserInRole(Model.UserName, "CorporateManager"))
                        {


                            #region taleo work and its check (check its order no and open according to its requirement)
                            //nd-16 changes on 02 julyb 2015

                            if (TempData["loginfrom"] != null)
                            {

                                if (Convert.ToString(TempData["loginfrom"]) == "taleo")
                                {
                                    if (TempData["TaleoOrderNoStatus"] != null)
                                    {

                                        if (Convert.ToString(TempData["TaleoOrderNoStatus"]) != "" && !Convert.ToString(TempData["TaleoOrderNoStatus"]).Contains(","))
                                        {
                                            using (EmergeDALDataContext ObjDX = new EmergeDALDataContext())
                                            {
                                                try
                                                {
                                                    Int64 TaleoOrderId;
                                                    var Obj = (from t1 in ObjDX.tblOrders where t1.OrderNo == Convert.ToString(TempData["TaleoOrderNoStatus"]) select new { t1 }).First();
                                                    TaleoOrderId = Obj.t1.pkOrderId;
                                                    //  ReturnUrl = "../Corporate/ViewReports&OId=" + Convert.ToInt64(TaleoOrderId) + "";


                                                    //RedirectToAction("../Corporate/ViewReports", new { _OId = TaleoOrderId });
                                                    ReturnUrl = "../Corporate/ViewReports";
                                                    //System.Web.HttpContext.Current.Session["TaleoOrderId"] = Convert.ToInt64(TaleoOrderId); nd-16
                                                    TempData["TaleoOrderId"] = Convert.ToInt64(TaleoOrderId);

                                                    //TempData["TaleoOrderId"] = Convert.ToInt64(TaleoOrderId);
                                                    ViewBag.TaleoOrderid = TempData["TaleoOrderId"];
                                                    Status = "Success";
                                                    Css = string.Empty;

                                                }
                                                catch //(Exception ss)
                                                {

                                                }
                                            }

                                        }

                                        else if (Convert.ToString(TempData["TaleoOrderNoStatus"]).Contains(","))
                                        {
                                            ReturnUrl = "../Corporate/SavedReports";
                                            Status = "Success";
                                            Css = string.Empty;
                                        }
                                        else
                                        {
                                            ReturnUrl = "../Corporate/NewReport";
                                            Status = "Success";
                                            Css = string.Empty;
                                        }
                                    }

                                }
                            }

                            #endregion
                            else
                            {
                                // add on 05 feb 2016 for Locking user to redirect only on billing page to pay invoice
                                if (Convert.ToString(Session["BillingLockingStatus"]) == "0")
                                {
                                    ReturnUrl = "../Corporate/BillingStatements";
                                    Status = "Success";
                                    Css = string.Empty;
                                }

                                else
                                {
                                    ReturnUrl = "../Corporate/NewReport";
                                    Status = "Success";
                                    Css = string.Empty;
                                }




                            }



                        }
                        else if (Roles.IsUserInRole(Model.UserName, "BranchManager"))
                        {


                            #region taleo work and its check (check its order no and open according to its requirement)


                            if (TempData["loginfrom"] != null)
                            {
                                //nd-16 changes on 02 julyb 2015
                                if (Convert.ToString(TempData["loginfrom"]) == "taleo")
                                {
                                    if (TempData["TaleoOrderNoStatus"] != null)
                                    {

                                        if (Convert.ToString(TempData["TaleoOrderNoStatus"]) != "" && !Convert.ToString(TempData["TaleoOrderNoStatus"]).Contains(","))
                                        {
                                            using (EmergeDALDataContext ObjDX = new EmergeDALDataContext())
                                            {
                                                try
                                                {
                                                    Int64 TaleoOrderId;
                                                    var Obj = (from t1 in ObjDX.tblOrders where t1.OrderNo == Convert.ToString(TempData["TaleoOrderNoStatus"]) select new { t1 }).First();
                                                    TaleoOrderId = Obj.t1.pkOrderId;
                                                    ReturnUrl = "../Corporate/ViewReports";
                                                    //System.Web.HttpContext.Current.Session["TaleoOrderId"] = Convert.ToInt64(TaleoOrderId);  // nd-16

                                                    TempData["TaleoOrderId"] = Convert.ToInt64(TaleoOrderId);

                                                    ViewBag.TaleoOrderid = TempData["TaleoOrderId"];
                                                    Status = "Success";
                                                    Css = string.Empty;

                                                }
                                                catch //(Exception ss)
                                                {

                                                }
                                            }

                                        }

                                        else if (Convert.ToString(TempData["TaleoOrderNoStatus"]).Contains(","))
                                        {
                                            ReturnUrl = "../Corporate/SavedReports";
                                            Status = "Success";
                                            Css = string.Empty;
                                        }
                                        else
                                        {
                                            ReturnUrl = "../Corporate/NewReport";
                                            Status = "Success";
                                            Css = string.Empty;
                                        }
                                    }

                                }
                            }

                            #endregion
                            else
                            {// add on 05 feb 2016 for Locking user to redirect only on billing page to pay invoice
                                if (Convert.ToString(Session["BillingLockingStatus"]) == "0")
                                {
                                    ReturnUrl = "../Corporate/BillingStatements";
                                    Status = "Success";
                                    Css = string.Empty;
                                }

                                else
                                {
                                    ReturnUrl = "../Corporate/NewReport";
                                    Status = "Success";
                                    Css = string.Empty;
                                }

                            }



                        }
                        else if (Roles.IsUserInRole(Model.UserName, "BasicUser"))
                        {

                            #region taleo work and its check (check its order no and open according to its requirement)
                            //nd-16 changes on 02 julyb 2015

                            if (TempData["loginfrom"] != null)
                            {

                                if (Convert.ToString(TempData["loginfrom"]) == "taleo")
                                {
                                    if (TempData["TaleoOrderNoStatus"] != null)
                                    {

                                        if (Convert.ToString(TempData["TaleoOrderNoStatus"]) != "" && !Convert.ToString(TempData["TaleoOrderNoStatus"]).Contains(","))
                                        {
                                            using (EmergeDALDataContext ObjDX = new EmergeDALDataContext())
                                            {
                                                try
                                                {
                                                    Int64 TaleoOrderId;
                                                    var Obj = (from t1 in ObjDX.tblOrders where t1.OrderNo == Convert.ToString(TempData["TaleoOrderNoStatus"]) select new { t1 }).First();
                                                    TaleoOrderId = Obj.t1.pkOrderId;
                                                    ReturnUrl = "../Corporate/ViewReports";
                                                    //  System.Web.HttpContext.Current.Session["TaleoOrderId"] = Convert.ToInt64(TaleoOrderId); nd-16

                                                    TempData["TaleoOrderId"] = Convert.ToInt64(TaleoOrderId);

                                                    ViewBag.TaleoOrderid = TempData["TaleoOrderId"];
                                                    Status = "Success";
                                                    Css = string.Empty;

                                                }
                                                catch //(Exception ss)
                                                {

                                                }
                                            }

                                        }

                                        else if (Convert.ToString(TempData["TaleoOrderNoStatus"]).Contains(","))
                                        {
                                            ReturnUrl = "../Corporate/SavedReports";
                                            Status = "Success";
                                            Css = string.Empty;
                                        }
                                        else
                                        {
                                            ReturnUrl = "../Corporate/NewReport";
                                            Status = "Success";
                                            Css = string.Empty;
                                        }
                                    }

                                }
                            }

                            #endregion

                            else
                            {// add on 05 feb 2016 for Locking user to redirect only on billing page to pay invoice
                                if (Convert.ToString(Session["BillingLockingStatus"]) == "0")
                                {
                                    ReturnUrl = "../Corporate/BillingStatements";
                                    Status = "Success";
                                    Css = string.Empty;
                                }

                                else
                                {
                                    ReturnUrl = "../Corporate/NewReport";
                                    Status = "Success";
                                    Css = string.Empty;
                                }


                            }



                        }
                        else if (Roles.IsUserInRole(Model.UserName, "ViewOnlyUser"))
                        {


                            if (!string.IsNullOrEmpty(Request.QueryString["ReturnUrl"]))
                            {
                                HttpContext.Response.Redirect(Request.QueryString["ReturnUrl"].ToString(), true);
                            }
                            else
                            {
                                // if (Convert.ToBoolean(ObjtblCompanyUser.Archive) == true)
                                // {

                                #region taleo work and its check (check its order no and open according to its requirement)

                                //nd-16 changes on 02 julyb 2015
                                if (TempData["loginfrom"] != null)
                                {

                                    if (Convert.ToString(TempData["loginfrom"]) == "taleo")
                                    {
                                        if (TempData["TaleoOrderNoStatus"] != null)
                                        {

                                            if (Convert.ToString(TempData["TaleoOrderNoStatus"]) != "" && !Convert.ToString(TempData["TaleoOrderNoStatus"]).Contains(","))
                                            {
                                                using (EmergeDALDataContext ObjDX = new EmergeDALDataContext())
                                                {
                                                    try
                                                    {
                                                        Int64 TaleoOrderId;
                                                        var Obj = (from t1 in ObjDX.tblOrders where t1.OrderNo == Convert.ToString(TempData["TaleoOrderNoStatus"]) select new { t1 }).First();
                                                        TaleoOrderId = Obj.t1.pkOrderId;
                                                        ReturnUrl = "../Corporate/ViewReports";
                                                        //System.Web.HttpContext.Current.Session["TaleoOrderId"] = Convert.ToInt64(TaleoOrderId); nd-16 change 
                                                        TempData["TaleoOrderId"] = Convert.ToInt64(TaleoOrderId);

                                                        ViewBag.TaleoOrderid = TempData["TaleoOrderId"];
                                                        Status = "Success";
                                                        Css = string.Empty;

                                                    }
                                                    catch //(Exception ss)
                                                    {

                                                    }
                                                }

                                            }

                                            else if (Convert.ToString(TempData["TaleoOrderNoStatus"]).Contains(","))
                                            {
                                                ReturnUrl = "../Corporate/SavedReports";
                                                Status = "Success";
                                                Css = string.Empty;
                                            }
                                            else
                                            {
                                                ReturnUrl = "../Corporate/NewReport";
                                                Status = "Success";
                                                Css = string.Empty;
                                            }
                                        }

                                    }
                                }

                                #endregion
                                else
                                {

                                    // add on 05 feb 2016 for Locking user to redirect only on billing page to pay invoice
                                    if (Convert.ToString(Session["BillingLockingStatus"]) == "0")
                                    {
                                        ReturnUrl = "../Corporate/BillingStatements";
                                        Status = "Success";
                                        Css = string.Empty;
                                    }

                                    else
                                    {
                                        ReturnUrl = "../Corporate/SavedReports";
                                        Status = "Success";
                                        Css = string.Empty;
                                    }

                                }

                                
                            }
                        }




                        else if (Roles.IsUserInRole(Model.UserName, "DataEntry"))
                        {

                            if (Convert.ToString(Session["BillingLockingStatus"]) == "0")
                            {
                                ReturnUrl = "../Corporate/BillingStatements";
                                Status = "Success";
                                Css = string.Empty;
                            }

                            else
                            {
                                ReturnUrl = "../Corporate/NewReport";
                                Status = "Success";
                                Css = string.Empty;
                            }

                        }
                        else if (Roles.IsUserInRole(Model.UserName, "SupportManager"))
                        {
                            if (Convert.ToString(Session["BillingLockingStatus"]) == "0")
                            {
                                ReturnUrl = "../Corporate/BillingStatements";
                                Status = "Success";
                                Css = string.Empty;
                            }

                            else
                            {
                                ReturnUrl = "../Corporate/NewReport";
                                Session["Admin"] = "SupportManager";
                                Status = "Success";
                                Css = string.Empty;
                            }






                        }
                        else if (Roles.IsUserInRole(Model.UserName, "SupportTechnician"))
                        {
                            if (Convert.ToString(Session["BillingLockingStatus"]) == "0")
                            {
                                ReturnUrl = "../Corporate/BillingStatements";
                                Status = "Success";
                                Css = string.Empty;
                            }

                            else
                            {
                                ReturnUrl = "../Corporate/NewReport";
                                Session["Admin"] = "SupportTechnician";
                                Status = "Success";
                                Css = string.Empty;
                            }








                        }
                    }
                    #endregion


                    #region change password after each 90 days


                    #endregion


                }
                else
                {
                    if (status == -2)
                    {
                        // ScriptManager.RegisterStartupScript(this.Page, typeof(string), "isCompDecative", "IsCompanyDeactivated();", true);
                        ReturnUrl = string.Empty;
                        Status = "IsCompanyDeactivated";
                        Css = "Error";
                    }
                    else if (status == -4)
                    {
                        // ScriptManager.RegisterStartupScript(this.Page, typeof(string), "isLocDecative", "IsLocationDeactivated();", true);
                        ReturnUrl = string.Empty;
                        Status = "IsLocationDeactivated";
                        Css = "Error";
                    }
                    else if (status == -6)
                    {
                        //ScriptManager.RegisterStartupScript(this.Page, typeof(string), "isUserDecative", "IsUserDeactivated();", true);
                        ReturnUrl = string.Empty;
                        Status = "IsUserDeactivated";
                        Css = "Error";
                    }
                    else if (status == -1 || status == -3 || status == -5)
                    {
                        //ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alertmsg", "ForInvlidUser();", true);
                        ReturnUrl = string.Empty;
                        Status = "InvlidUser";
                        Css = "Error";

                    }
                    else if (status == -7)
                    {
                        //get user name in session
                        //improve later by taking value from existing fields
                        Session["UserNamePasswordExpired"] = Model.UserName;

                        ReturnUrl = string.Empty;
                        Status = "PasswordExpired";
                        Css = "Error";
                    }
                    BLLMembership ObjBLLMembership = new BLLMembership();
                    ObjBLLMembership.SignOut(Model.UserName, DateTime.Now.ToUniversalTime().AddMinutes(-(Membership.UserIsOnlineTimeWindow)));
                }
            }
            else
            {
                ClearCookie(Model.UserName.Trim(), Model.Password.Trim());
                ReturnUrl = string.Empty;
                Status = "InvlidUser";
                Css = "Error";
                //ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alertmsg", "ForInvlidUser();", true);
            }
            TempData["TaleoPageInforation"] = ReturnUrl;// nd-16 changes
            return Json(new { Sts = Status, css = Css, PageUrl = ReturnUrl, Retunusername = CurrentUserName }, JsonRequestBehavior.AllowGet);

        }



        public void taleoDispatcherlogin()
        {

            // string TaleoResponseURL = string.Empty;
            try
            {
                //Session.Remove("TaleoDispatcherUrlHit");
                // Session.Remove("xmlResponseCookie");

                //to do: string to be kep on config file
                //string loginURL = "https://tbe.taleo.net/MANAGER/dispatcher/api/v1/serviceUrl/INTELIFI";
                //string loginURL = Convert.ToString(System.Web.HttpContext.Current.Session["TaleoDispatcherUrlloginvalue"]);


                string loginURL = Convert.ToString(ConfigurationManager.AppSettings["Taleo_Dispatcher_Key"]) + Convert.ToString(TempData["TaleoDispatchCompCode"]);
                //Taleo_orgCode




                #region test url for dispatcher
                //string loginURL = Convert.ToString(ConfigurationManager.AppSettings["Taleo_Dispatcher_Key"]) + Convert.ToString(ConfigurationManager.AppSettings["Taleo_orgCode"]);
                #endregion

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(loginURL);
                req.Method = "get";
                req.ContentType = "application/json;charset=UTF-8";
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                string xmlResponse = null;
                using (StreamReader sr = new StreamReader(resp.GetResponseStream()))
                {
                    xmlResponse = sr.ReadToEnd();
                    sr.Close();
                }
                resp.Close();

                // TaleoResponseURL = xmlResponse;


                dynamic TaleoDispatcherUrl = JObject.Parse(xmlResponse);
                string TaleoDispatcherUrlHit = (string)TaleoDispatcherUrl["response"]["URL"];
                //System.Web.HttpContext.Current.Session["TaleoDispatcherUrlHit"] = TaleoDispatcherUrlHit;
                TempData["TaleoDispatcherUrlHit"] = TaleoDispatcherUrlHit;
                Session["TaleoDispatcherUrlHit"] = TaleoDispatcherUrlHit;
                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n ------- TempData[TaleoDispatcherUrlHit]-- :" + TempData["TaleoDispatcherUrlHit"] + "-----------------", "TaleoErrorLog");


                int Result = 0;
                using (EmergeDALDataContext ObjDX = new EmergeDALDataContext())
                {
                    var objupdateTaleoWebApikey = ObjDX.tblEmergeTaleoUserInfos.Where(d => d.userName == Convert.ToString(TempData["TaleoDispatchUserName"])
                        && d.PassWord == Convert.ToString(TempData["TaleoDispatchPassword"]) && d.CompCode == Convert.ToString(TempData["TaleoDispatchCompCode"])).FirstOrDefault();
                    if (objupdateTaleoWebApikey != null)
                    {
                        objupdateTaleoWebApikey.TaleoWebDispatcherURL = Convert.ToString(TempData["TaleoDispatcherUrlHit"]);

                        ObjDX.SubmitChanges();
                        Result = 1;



                    }
                    else
                    {
                        VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n ----Error--------------------", "TaleoErrorLog");
                        VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n --update for taleoDispatcherlogin in database is failed -------------------", "TaleoErrorLog");

                    }
                }



            }
            catch (Exception asd)
            {

                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n -----Exception --taleoDispatcherlogin-----------------", "TaleoErrorLog");
                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n -----Exception " + asd.Message.ToString(), "TaleoErrorLog");

                //string pageContent = new StreamReader(resp.GetResponseStream()).ReadToEnd().ToString();
                //return pageContent;
            }

            finally
            {

            }


        }


        public string taleologin()
        {
            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n ----start in --- taleologin-------------------", "TaleoErrorLog");
            string xmlResponseCookie = string.Empty;
            try
            {


                //to do: string to be kep on config file
                //string loginURL = Convert.ToString(System.Web.HttpContext.Current.Session["TaleoDispatcherUrlHit"]) + "login?orgCode=INTELIFI&userName=ksharma&password=chetu@123";
                // ConfigurationManager.AppSettings["Taleo_password"];
                string loginURL = Convert.ToString(TempData["TaleoDispatcherUrlHit"]) + "login?orgCode="
                    + Convert.ToString(TempData["TaleoDispatchCompCode"]) + "&userName=" +
                    Convert.ToString(TempData["TaleoDispatchUserName"]) + "&password=" + Convert.ToString(TempData["TaleoDispatchPassword"]);

                #region test url section
                // comment for discard depenedency fro single test user 
                //string loginURL = Convert.ToString(TempData["TaleoDispatcherUrlHit"]) + "login?orgCode=" + Convert.ToString(ConfigurationManager.AppSettings["Taleo_orgCode"]) + "&userName=" + Convert.ToString(ConfigurationManager.AppSettings["Taleo_userName"] + "&password=" + Convert.ToString(ConfigurationManager.AppSettings["Taleo_password"]));
                #endregion

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(loginURL);
                req.Method = "POST";
                req.ContentType = "application/json;charset=UTF-8";
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                string xmlResponse = null;
                using (StreamReader sr = new StreamReader(resp.GetResponseStream()))
                {
                    xmlResponse = sr.ReadToEnd();
                    sr.Close();
                }
                resp.Close();

                xmlResponseCookie = xmlResponse;
                xmlResponseCookie = xmlResponseCookie.Substring(26, 26);
                //System.Web.HttpContext.Current.Session["xmlResponseCookie"] = xmlResponseCookie;
                TempData["xmlResponseCookie"] = xmlResponseCookie;

                int Result = 0;
                using (EmergeDALDataContext ObjDX = new EmergeDALDataContext())
                {
                    var objupdateTaleoWebApikey = ObjDX.tblEmergeTaleoUserInfos.Where(d => d.userName == Convert.ToString(TempData["TaleoDispatchUserName"])
                        && d.PassWord == Convert.ToString(TempData["TaleoDispatchPassword"]) && d.CompCode == Convert.ToString(TempData["TaleoDispatchCompCode"])).FirstOrDefault();
                    if (objupdateTaleoWebApikey != null)
                    {
                        objupdateTaleoWebApikey.TaleoWebApiCookie = xmlResponseCookie;
                        objupdateTaleoWebApikey.CreatedCookieDate = Convert.ToString(System.DateTime.Now);
                        ObjDX.SubmitChanges();

                        VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n -----update in taleologin is successfully done  ", "TaleoErrorLog");
                        Result = 1;
                    }
                    else
                    {

                        VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n -----else in taleologin ", "TaleoErrorLog");
                    }
                }







            }
            catch (WebException asd)
            {
                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n -----Exception --taleologin-----------------", "TaleoErrorLog");
                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n -----Exception " + asd.Message.ToString(), "TaleoErrorLog");


               // string successResponse = taleologOut();
                HttpWebResponse webResponse = (HttpWebResponse)asd.Response;
                if (webResponse.StatusCode == HttpStatusCode.InternalServerError)
                {

                    if (taleoGloalVariable1 <= 1)
                    {
                        taleoGloalVariable1++;

                        taleologin(); // comment for testing 
                    }
                    else
                    {


                        //not connected to Taleoe ;

                    }
                    //Handle 404 Error...



                }


                //string pageContent = new StreamReader(resp.GetResponseStream()).ReadToEnd().ToString();
                //return pageContent;
            }

            finally
            {

            }

            return xmlResponseCookie;
        }



        int taleoGloalVariable1 = 0;
        public void TaleoUserWork()
        {
            //string xmlResponseCookie = Convert.ToString(System.Web.HttpContext.Current.Session["xmlResponseCookie"]);
            string xmlResponseCookie = Convert.ToString(TempData["xmlResponseCookie"]);
            Session["xmlResponseCookie"] = xmlResponseCookie;
            Session["loginfrom"] = "taleo";
            Session["TaleoDispatcherUrlHit"] = TempData["TaleoDispatcherUrlHit"];
            Session["TaleoCandidateId"] = TempData["TaleoCandidateId"];

            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n -----TaleoUserWork-------------------  ", "TaleoErrorLog");
            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n ------" + Session["xmlResponseCookie"] + "------------------  ", "TaleoErrorLog");
            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n ------" + Session["loginfrom"] + "------------------  ", "TaleoErrorLog");
            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n ------" + Session["TaleoDispatcherUrlHit"] + "------------------  ", "TaleoErrorLog");
            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n ------" + Session["TaleoCandidateId"] + "------------------  ", "TaleoErrorLog");

            if (xmlResponseCookie != "")
            {
                string Candidateurl = Convert.ToString(TempData["TaleoDispatcherUrlHit"]) + "object/candidate/" + Convert.ToInt32(TempData["TaleoCandidateId"]);

                HttpWebRequest req1 = (HttpWebRequest)WebRequest.Create(Candidateurl);
                req1.Method = "get";
                req1.ContentType = "application/json";
                req1.Headers.Add("Cookie: authToken=" + xmlResponseCookie.ToString());

                //Make API call
                string orderNo = string.Empty;
                try
                {
                    req1.Timeout = System.Threading.Timeout.Infinite;
                    req1.KeepAlive = true;
                    var resp1 = (HttpWebResponse)null;
                    if (!req1.HaveResponse)
                    {
                        do
                        {

                            resp1 = (HttpWebResponse)req1.GetResponse();
                        } while (resp1 == null);
                    }

                    string xmlResponse2 = null;
                    using (StreamReader sr = new StreamReader(resp1.GetResponseStream()))
                    {
                        xmlResponse2 = sr.ReadToEnd();
                        sr.Close();
                    }
                    resp1.Close();


                    xmlResponse2 = xmlResponse2.Substring(12, xmlResponse2.Length - 51);


                    dynamic TaleoCandidateInfo_details = JObject.Parse(xmlResponse2);


                    #region object value fill in new report form  for takeo ND-16

                    orderNo = (string)TaleoCandidateInfo_details["candidate"]["Emerge_Order_Id"];
                    VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n ---orderNo---" + orderNo + "------------------  ", "TaleoErrorLog");
                    TempData["TaleoOrderNoStatus"] = orderNo;
                    if (orderNo != "")
                    {
                        BALOrders TaleoBalorder = new BALOrders();
                        #region to update order status and maiantin its value on taleo
                        TaleoBalorder.TaleogenrateOrderNo(Convert.ToString(orderNo)); // 

                        #endregion
                    }
                    else
                    {

                    }


                    #endregion

                }

                catch (WebException exx)
                {

                    VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n ---Error in TaleoUserWork ---" + exx.Message.ToString(), "TaleoErrorLog");
                    //string successResponse = taleologOut();
                    HttpWebResponse webResponse = (HttpWebResponse)exx.Response;
                    if (webResponse.StatusCode == HttpStatusCode.InternalServerError)
                    {

                        if (taleoGloalVariable <= 1)
                        {
                            taleoGloalVariable++;
                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n ---Error in TaleoUserWork(taleoGloalVariable) ---" + taleoGloalVariable, "TaleoErrorLog");
                            //Handle 404 Error...
                            //TaleoUserWork();

                            #region check url and its web api key for working or not

                            //if (Convert.ToString(TempData["TaleoWebDispatcherURL"]) != "")
                            //{
                            //    TempData["TaleoDispatcherUrlHit"] = TempData["TaleoWebDispatcherURL"];
                            //}

                            //else
                            //{
                            taleoDispatcherlogin();
                            //}
                            //if (Convert.ToString(TempData["TaleoDispatchTaleoWebApiCookie"]) != "")
                            //{
                            //    // if web api already exist then use it 
                            //    TempData["xmlResponseCookie"] = TempData["TaleoDispatchTaleoWebApiCookie"];
                            //}
                            //else
                            //{
                            // create a new web apin key and access it all information

                            taleologin();  // nd-16 update login url in datbase 
                            //}

                            #endregion
                            TaleoUserWork();


                        }
                        else
                        {
                            //not connected to Taleoe ;

                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n ---else condito in TaleoUserWork ---" + taleoGloalVariable, "TaleoErrorLog");


                        }


                    }

                }

                finally
                {

                }
            }

            //Session["CandidateId"] = Session["TaleoEmergeOrderStaus"];


        }


        int taleoGloalVariable = 0;
        public string taleologOut()
        {
            //string xmlResponseCookie = Convert.ToString(System.Web.HttpContext.Current.Session["xmlResponseCookie"]);
            string xmlResponseCookie = Convert.ToString(TempData["xmlResponseCookie"]);
            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n ---In taleologOut() ---", "TaleoErrorLog");

            if (xmlResponseCookie != "")
            {
                string str = Convert.ToString(TempData["TaleoDispatcherUrlHit"]);
                string logoutstring = str.Substring(0, str.Length - 3);
                string logoutURL = Convert.ToString(logoutstring) + "logout";

                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n ---In taleologOut() ---" + logoutURL, "TaleoErrorLog");
                HttpWebRequest req2 = (HttpWebRequest)WebRequest.Create(logoutURL);
                req2.Method = "post";
                req2.ContentType = "application/json";
                req2.Headers.Add("Cookie: authToken=" + xmlResponseCookie);

                //req.Headers.Add("Cookie", authToken);

                //Make API call
                try
                {
                    HttpWebResponse response = (HttpWebResponse)req2.GetResponse();

                    //string pageContent = new StreamReader(response.GetResponseStream()).ReadToEnd().ToString();

                    string xmlResponse2 = null;
                    using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                    {
                        xmlResponse2 = sr.ReadToEnd();
                    }

                    response.Close();
                }
                catch (Exception s)
                {
                    VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n ---Error In taleologOut() ---" + s.Message.ToString(), "TaleoErrorLog");
                    //return pageContent;
                }

            }



            return "sucess";
        }


        //  nd- 16 taleo get infromation from data base as username and pass word using taleo key 
        public string getEmerge_TaleoDispatchCredInformation(string EmergeTaleoWebkey)
        {
            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n -- in getEmerge_TaleoDispatchCredInformation-", "TaleoErrorLog");
            string returnans = string.Empty;
            using (EmergeDALDataContext Objdata = new EmergeDALDataContext())
            {
                // create query to get all information use for login in taleo side for to create dispatcher url 
                var taleoEmergeDispatchInfo = (from pd in Objdata.aspnet_Memberships
                                               join od in Objdata.tblEmergeTaleoUserInfos on pd.UserId equals od.UserId
                                               where pd.TaleoApiKey == EmergeTaleoWebkey
                                               select new
                                               {
                                                   od.userName,
                                                   od.CompCode,
                                                   od.PassWord,
                                                   od.CompanyId,
                                                   od.CompanyName,
                                                   od.TaleoWebApiCookie,
                                                   od.TaleoWebDispatcherURL
                                               }).FirstOrDefault();


                if (taleoEmergeDispatchInfo != null)
                {
                    TempData["TaleoDispatchUserName"] = Convert.ToString(taleoEmergeDispatchInfo.userName);
                    TempData["TaleoDispatchCompCode"] = Convert.ToString(taleoEmergeDispatchInfo.CompCode);
                    TempData["TaleoDispatchPassword"] = Convert.ToString(taleoEmergeDispatchInfo.PassWord);
                    TempData["TaleoDispatchTaleoWebApiCookie"] = Convert.ToString(taleoEmergeDispatchInfo.TaleoWebApiCookie);
                    TempData["TaleoWebDispatcherURL"] = Convert.ToString(taleoEmergeDispatchInfo.TaleoWebDispatcherURL);





                    if (TempData["TaleoDispatchUserName"] != null)
                    {
                        returnans = "success";

                        VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n ---In getEmerge_TaleoDispatchCredInformation()---" + returnans, "TaleoErrorLog");
                    }
                    else
                    {
                        returnans = "error";
                    }
                }
                else
                {

                    VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n -- else condition getEmerge_TaleoDispatchCredInformation-", "TaleoErrorLog");
                    returnans = "error";
                }


                // store in temp data to access all infromation in all application



            }
            return returnans;
        }

        public ActionResult RedirectToPage(string Email, string Page)
        {

            #region taleo page re direct to new report page

            TempData["loginfrom"] = Request.QueryString["loginfrom"];

            if (TempData["loginfrom"] != null)
            {
                if (Convert.ToString(TempData["loginfrom"]) == "taleo")
                {


                    VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n -------------- :" + "-----------------------", "TaleoErrorLog");

                    VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n -------start in RedirectToPage-- :" + "-----------------------", "TaleoErrorLog");



                    taleomain(); // for default ,lofin ,process model page 
                    //  TaleoAutoLoginLogoutCompleteChange();
                    if (TempData["TaleoEmergeLoginEmailId"] != null)
                    {
                        if (Convert.ToString(TempData["TaleoEmergeLoginEmailId"]) != "")
                        {
                            FormsAuthentication.RedirectFromLoginPage(Convert.ToString(TempData["TaleoEmergeLoginEmailId"]), true);
                            Page = Convert.ToString(TempData["TaleoPageInforation"]);
                        }
                        else
                        {
                            //FormsAuthentication.RedirectFromLoginPage("admin@usaintel.com", true);
                        }

                    }

                    else
                    {
                        FormsAuthentication.RedirectFromLoginPage("", false);
                        Page = "";
                    }



                }
            }
            #endregion

            else
            {

                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n -------RedirectToPage: " + Convert.ToString(Session["UserRoles"]), "TaleoErrorLog");

                if (Session["UserRoles"] != null)
                {
                    VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n -------RedirectToPage:CurrentUserMailId " + Convert.ToString(Session["CurrentUserMailId"]), "TaleoErrorLog");

                    if (Session["CurrentUserMailId"] == null)
                    {


                        FormsAuthentication.RedirectFromLoginPage(Email, true);



                        VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n -------RedirectToPage:Page: " + Convert.ToString(Page), "TaleoErrorLog");
                        VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n -------RedirectToPage:Email: " + Convert.ToString(Email), "TaleoErrorLog");

                    }
                    else
                    {
                        if (Convert.ToString(Session["CurrentUserMailId"]) == Email)//If user explicitly added any email id other then login user then system will rediredt to default page.
                        {
                            FormsAuthentication.RedirectFromLoginPage(Email, true);
                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n -------RedirectToPage:Email: " + Convert.ToString(Email), "TaleoErrorLog");
                        }
                        else
                        {

                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n -------RedirectToPage:CurrentUserMailId " + "Error2", "TaleoErrorLog");
                            return RedirectToAction("../Default");
                        }
                    }
                }
                else
                {
                    VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n -------RedirectToPage:CurrentUserMailId " + "Error", "TaleoErrorLog");


                    return RedirectToAction("../Default");
                }

            }

            return RedirectToAction(Page);            
        }

        public ActionResult RedirectToPageApi(string Email, string Page)
        {
            FormsAuthentication.RedirectFromLoginPage(Email, true);
            Session["success"] = "true";
            return Redirect(Page);
        }




        [HttpPost]
        public ActionResult LandingLogin(NewReportModel Model)
        {
            MembershipUser ObjMembershipUser = Membership.GetUser(Model.UserName);


            //string url = Request.CurrentExecutionFilePath;

            string ReturnUrl = Model.Urlpath;



            string Status = string.Empty;
            string Css = string.Empty;
            int CompanyID = Model.pkCompanyId;
            if (ObjMembershipUser != null)
            {
                if ((Roles.IsUserInRole(Model.UserName, "SystemAdmin")))//)!(Roles.IsUserInRole(UserName, "Admin") || 
                {
                    if (ObjMembershipUser.IsLockedOut)
                    {
                        //ScriptManager.RegisterStartupScript(this.Page, typeof(string), "IsLocked", "IsLocked();", true);
                        Status = "IsLocked";
                        Css = string.Empty;
                    }
                }
            }

            if (Membership.ValidateUser(Model.UserName, Model.Password))
            {
                int status = CheckAccountStatus(new Guid(Membership.GetUser(Model.UserName).ProviderUserKey.ToString())); //If a user is created on emerge under the api location, then they can login here.

                if (Convert.ToInt32(status) > 0)
                {
                    Guid UserId = new Guid(Membership.GetUser(Model.UserName).ProviderUserKey.ToString());
                    int statusApi = CheckAccountStatusforapilocation(UserId, CompanyID, Model.ApiUrl);
                    if (statusApi == 1)
                    {

                        string[] multiple_Roles = Roles.GetRolesForUser(Model.UserName);

                        if (Model.RemeberMe)
                        {
                            Utility.SetCookie(Model.UserName.Trim(), Model.Password.Trim(), multiple_Roles[0], true);
                        }
                        else
                        {
                            Utility.SetCookie(Model.UserName.Trim(), Model.Password.Trim(), multiple_Roles[0], false);
                        }
                        Status = "Success";
                    }
                    else
                    {
                        ReturnUrl = string.Empty;
                        Status = "InvlidUser";
                        Css = "Error";

                    }

                }
                else
                {
                    if (status == -2)
                    {
                        // ScriptManager.RegisterStartupScript(this.Page, typeof(string), "isCompDecative", "IsCompanyDeactivated();", true);
                        ReturnUrl = string.Empty;
                        Status = "IsCompanyDeactivated";
                        Css = "Error";
                    }
                    else if (status == -4)
                    {
                        // ScriptManager.RegisterStartupScript(this.Page, typeof(string), "isLocDecative", "IsLocationDeactivated();", true);
                        ReturnUrl = string.Empty;
                        Status = "IsLocationDeactivated";
                        Css = "Error";
                    }
                    else if (status == -6)
                    {
                        //ScriptManager.RegisterStartupScript(this.Page, typeof(string), "isUserDecative", "IsUserDeactivated();", true);
                        ReturnUrl = string.Empty;
                        Status = "IsUserDeactivated";
                        Css = "Error";
                    }
                    else if (status == -1 || status == -3 || status == -5)
                    {
                        //ScriptManager.RegisterStartupScript(this.Page, typeof(string), "alertmsg", "ForInvlidUser();", true);
                        ReturnUrl = string.Empty;
                        Status = "InvlidUser";
                        Css = "Error";

                    }
                    BLLMembership ObjBLLMembership = new BLLMembership();
                    ObjBLLMembership.SignOut(Model.UserName, DateTime.Now.ToUniversalTime().AddMinutes(-(Membership.UserIsOnlineTimeWindow)));
                }
            }
            else
            {
                ReturnUrl = string.Empty;
                Status = "InvlidUser";
                Css = "Error";
            }

            return Json(new { Sts = Status, css = Css, PageUrl = ReturnUrl });

        }


        #endregion
        #region LogOut

        public ActionResult LogOut(object sender, EventArgs e)
        {
            userlogout();
            return RedirectToAction("../Default");
        }


        public ActionResult LogOutAPI(string url)
        {
            string Path = url;
            Session["success"] = null;
            userlogout();
            return Redirect(Path);
        }

        [System.Web.Mvc.OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        private void userlogout()
        {
            //Session["Admin"] = null;
            //Session.Abandon();
            //ChatControl.StopSession();
            //SaveUserProfile();
            ChatService.StopUserSession(System.Web.HttpContext.Current);
            //ChatService.ChatConfig. null;

            MembershipUser ObjMembershipUser = Membership.GetUser(HttpContext.User.Identity.Name);
            System.Web.Security.FormsAuthentication.SignOut();

            if (ObjMembershipUser != null)
            {
                ProfileCommon ObjProfileCommon = new ProfileCommon();
                ObjProfileCommon.SaveUserProfile(HttpContext.User.Identity.Name);
                BALChat objBALChat = new BALChat();
                DateTime Dated = DateTime.Now;
                string ChatSessionId = Dated.ToString("yyMMddHHmmssfff");
                string UniquePublisherId = ObjMembershipUser.ProviderUserKey.ToString() + "_" + ChatSessionId;
                objBALChat.RemoveMangoChatWindows(ObjMembershipUser.ProviderUserKey.ToString(), UniquePublisherId);
                // Important Note: To Access Mango Chat User History Please UserId as Client Id With ClientId+"-logout" like  ClentID="b6e8cb8d-ac97-43e5-a1b6-47f0997665db-logout"
                BLLMembership ObjBLLMembership = new BLLMembership();
                ObjBLLMembership.SignOut(ObjMembershipUser.UserName, DateTime.Now.ToUniversalTime().AddMinutes(-(Membership.UserIsOnlineTimeWindow)));
            }
            Session["Admin"] = null;
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();

            HttpCookie aCookie;
            string cookieName;
            int limit = Request.Cookies.Count;
            for (int i = 0; i < limit; i++)
            {
                cookieName = Request.Cookies[i].Name;
                aCookie = new HttpCookie(cookieName);
                aCookie.Expires = DateTime.Now.AddDays(-1); // make it expire yesterday
                Response.Cookies.Add(aCookie); // overwrite it
            }

            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            //Response.Cache.SetNoStore();

        }
        #endregion
        public FilePathResult DownloadFeaturedTitleImage(string FPathTitle)
        {
            string[] str = FPathTitle.Split('_');
            string FileName = str[0];
            string Title = str[1].Replace(" ", "_");
            string FilePath = Request.PhysicalApplicationPath + "Resources/Upload/FeatureFiles/" + FileName;
            string fileext = System.IO.Path.GetExtension(FileName);
            string CType = "application/" + fileext.Replace(".", "");
           // string fileName = Path.GetFileName(FileName);
            string strFileTitle = Title + fileext;
            if (System.IO.File.Exists(FilePath)) { return File(FilePath, CType, strFileTitle); }
            else
            {
                FilePath = Request.PhysicalApplicationPath + "Resources/Upload/FeatureFiles/no-image.jpg";
                return File(FilePath, CType, strFileTitle);
            }
        }
        public FilePathResult DownloadFeaturedFile(string FPathTitle)
        {
            string[] str = FPathTitle.Split('_');
            string FileName = str[0];
            string Title = str[1].Replace(" ", "_");
            string FilePath = Request.PhysicalApplicationPath + "Resources/Upload/FeatureFiles/" + FileName;
            string fileext = System.IO.Path.GetExtension(FileName);
            string CType = "application/" + fileext.Replace(".", "");
            //string fileName = Path.GetFileName(FileName);
            string strFileTitle = Title + fileext;
            if (System.IO.File.Exists(FilePath))
            {
                return File(FilePath, CType, strFileTitle);
            }
            else
            {
                FilePath = Request.PhysicalApplicationPath + "Resources/Upload/FeatureFiles/no-image.jpg";
                return File(FilePath, CType, strFileTitle);
            }
        }
        #region Other Functions
        public int CheckAccountStatus(Guid UserId)
        {
            BLLMembership ObjBLLMembership = new BLLMembership();
            try
            {
                return ObjBLLMembership.CheckUserIsValid(UserId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBLLMembership = null;
            }
        }

        //add on 03 feb 2016 for cheking status in Billing section
        public int CheckBillingLockingAccountStatus(Guid UserId)
        {
            BLLMembership ObjBLLMembership = new BLLMembership();
            try
            {


                return ObjBLLMembership.BillingCheckUserIsValid(UserId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBLLMembership = null;
            }
        }


        public int CheckAccountStatusforapilocation(Guid UserId, int CompanyID, string ApiUrl)
        {
            BALCompanyType ObjBALCompanyType = new BALCompanyType();
            try
            {
                return ObjBALCompanyType.GetApiLocation(UserId, CompanyID, ApiUrl);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBALCompanyType = null;
            }
        }



        private List<SelectListItem> checkusers(string UserRole)
        {
            List<SelectListItem> listPages = new List<SelectListItem>();
            if (UserRole == "BranchManager" || UserRole == "BasicUser")
            {
                listPages.Add(new SelectListItem { Value = "NewReport", Text = "New Reports" });
                listPages.Add(new SelectListItem { Value = "SavedReports", Text = "Archives" });
                listPages.Add(new SelectListItem { Value = "MyAccount", Text = "My Profile" });
                listPages.Add(new SelectListItem { Value = "MyReports", Text = "My Reports" });
                listPages.Add(new SelectListItem { Value = "MyForms", Text = "My Forms" });
                listPages.Add(new SelectListItem { Value = "MyPreferences", Text = "My Preferences" });
            }
            if (UserRole == "SystemAdmin" || UserRole == "SupportManager" || UserRole == "SupportTechnician")//UserRole == "Admin" ||
            {
                listPages.Add(new SelectListItem { Value = "NewReport", Text = "New Reports" });
                listPages.Add(new SelectListItem { Value = "SavedReports", Text = "Archives" });
                listPages.Add(new SelectListItem { Value = "MyAccount", Text = "My Profile" });
                listPages.Add(new SelectListItem { Value = "MyReports", Text = "My Reports" });
                listPages.Add(new SelectListItem { Value = "MyForms", Text = "My Forms" });
                listPages.Add(new SelectListItem { Value = "MyPreferences", Text = "My Preferences" });
                listPages.Add(new SelectListItem { Value = "BillingStatement", Text = "My Statements" });

            }
            if (UserRole == "CorporateManager")
            {
                listPages.Add(new SelectListItem { Value = "NewReport", Text = "New Reports" });
                listPages.Add(new SelectListItem { Value = "SavedReports", Text = "Archives" });
                listPages.Add(new SelectListItem { Value = "MyAccount", Text = "My Profile" });
                listPages.Add(new SelectListItem { Value = "MyReports", Text = "My Reports" });
                listPages.Add(new SelectListItem { Value = "MyForms", Text = "My Forms" });
                listPages.Add(new SelectListItem { Value = "MyPreferences", Text = "My Preferences" });
                listPages.Add(new SelectListItem { Value = "BillingStatement", Text = "My Statements" });
                listPages.Add(new SelectListItem { Value = "BillingStatement?invoiceid=-1", Text = "Pay Invoice" });
            }
            if (UserRole == "DataEntry")
            {
                listPages.Add(new SelectListItem { Value = "NewReport", Text = "New Reports" });
                listPages.Add(new SelectListItem { Value = "MyAccount", Text = "My Profile" });
            }
            if (UserRole == "ViewOnlyUser")
            {
                listPages.Add(new SelectListItem { Value = "SavedReports", Text = "Archives" });
                listPages.Add(new SelectListItem { Value = "MyAccount", Text = "My Profile" });
            }





            if (string.IsNullOrEmpty(UserRole))
            {
                listPages.Add(new SelectListItem { Value = "NewReport", Text = "New Reports" });
            }
            return listPages;
        }

        private void ClearCookie(string UserName, string UserPassword)
        {
            Utility.SetCookie(UserName, UserPassword, "", false);
            //txtUsername.Text = "";
            //txtPassword.Attributes.Add("value", "");
            //txtUsername.Focus();
            //chkRemeberMe.Checked = false;
        }
        #endregion
        public static XDocument GetRssFeed()
        {
            string rssfeedurl = "http://news.google.com/news?cf=all&ned=us&hl=en&output=rss";
            XDocument feedXml = XDocument.Load(rssfeedurl);
            return feedXml;
        }
        public FilePathResult DownloadMyForms(string FPathTitle)
        {
            string FilePath = Server.MapPath("~/Resources/Forms/" + FPathTitle);
            string fileext = System.IO.Path.GetExtension(FilePath);
            string CType = "application/" + fileext.Replace(".", "");
            string fileName = Path.GetFileName(FilePath);
            return File(FilePath, CType, fileName);
        }
        public List<MyFormColl> GetMyFormCollection()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                List<MyFormColl> ObjData = new List<MyFormColl>();
                ObjData = (from u in ObjDALDataContext.tblmyforms
                           where u.IsActive == true
                           select new MyFormColl
                           {
                               pkformId = u.pkformId,
                               Title = u.Title,
                               Description = u.Description,
                               Path = u.Path,
                               ModifiedLoggedId = u.ModifiedLoggedId != null ? (Guid)u.ModifiedLoggedId : Guid.Empty,
                               ModifiedDate = u.ModifiedDate
                           }).ToList();
                return ObjData.ToList();
            }
        }

        #region change password after 90 days(for ticket #47)

        public ActionResult ChangePassword()
        {

            LoginModel ObjLoginModel = new LoginModel();
            ObjLoginModel.UserName = Session["UserName"].ToString();
           // MembershipUser ObjMembershipUser = Membership.GetUser(ObjLoginModel.UserName);
            return View(ObjLoginModel);
        }

        [HttpPost]
        public ActionResult ChangePasswordProcess(LoginModel loginmodel)
        {
            string Msg = string.Empty;
            string ReturnUrl = string.Empty;
            bool IsChangePassword = false;
            MembershipUser ObjMembershipUser = Membership.GetUser(loginmodel.UserName);

            if (Membership.ValidateUser(loginmodel.UserName, loginmodel.OldPassword))
            {
                if (ObjMembershipUser.ChangePassword(loginmodel.OldPassword, loginmodel.NewPassword))
                {
                    Msg = "Password has changed Successfully.";
                    IsChangePassword = true;
                    ReturnUrl = "../Corporate/NewReport";
                }
                else
                {
                    Msg = "Password change failed. Please re-enter your values and try again.";
                }
            }
            else
            {
                Msg = "Invalid Old password. Please check your Old Password and try again.";
            }
            return Json(new { Status = Msg, PageUrl = ReturnUrl, IsChangePassword = IsChangePassword });
        }

        #endregion



    }
    public class MyFormColl
    {
        public int pkformId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
        public Guid ModifiedLoggedId { get; set; }
        public DateTime? ModifiedDate { get; set; }

    }
}
