﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;


namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /GetAllLocation/

        public ActionResult GetAllLocations()
        {
            CompanyModel objModel = new CompanyModel();
            BALCompany serobj = new BALCompany();
            //BALCompanyType objBal = new BALCompanyType();
           // objModel.GetConpanyType = objBal.GetAllCompanyType();
            objModel.GetLocation = serobj.GetAllCompanyLocation();

            List<SelectListItem> County = new List<SelectListItem>();
            SelectListItem s1 = new SelectListItem { Text = "Active", Value = "Active" };
            SelectListItem s2 = new SelectListItem { Text = "InActive", Value = "InActive" };
            SelectListItem s3 = new SelectListItem { Text = "Locked", Value = "Locked" };
            SelectListItem s4 = new SelectListItem { Text = "PastDue", Value = "PastDue" };
            SelectListItem s5 = new SelectListItem { Text = "InCollections", Value = "InCollections" };
            County.Add(s1);
            County.Add(s2);
            County.Add(s3);
            County.Add(s4);
            County.Add(s5);
            objModel.CountryName = County;




            return View(objModel);
        }
        [HttpPost]
        public ActionResult GetLocation(int page, int pageSize, List<GridSort> sort, List<GridFilter> filters)
        {
            # region filter
            try
            {
                int PageNum = page;
                int PageSize = pageSize;
                string SortingColumn = "City";
                string SortingDirection = "asc";
                int TotalRec = 0;
                bool IsPaging = true;
                int LocationId = 0;
                int FkCompanyId = 0;
                if (sort != null)
                {
                    SortingDirection = sort[0].Dir;
                    SortingColumn = sort[0].Field;
                }
                Int16 CompanyType = -1; 
                ObjBALCompany = new BALCompany();
                //List<LocationViewModel> ObjLocationViewModel = new List<LocationViewModel>();
                List<Proc_Get_AllLocationsForAdminResult> OData = new List<Proc_Get_AllLocationsForAdminResult>();

                if (filters != null)
                {
                  
                        foreach (var ObjFilter in filters)
                        {
                            OData = FilterLocation(ObjFilter, PageNum, PageSize, IsPaging, LocationId, CompanyType, FkCompanyId, SortingColumn, SortingDirection, out  TotalRec);
                        }
                }
                else
                {
                    OData = ObjBALCompany.GetAllLocationsForAdmin(PageNum, PageSize, IsPaging, LocationId, CompanyType, FkCompanyId, SortingColumn, SortingDirection, string.Empty, 0, string.Empty, out TotalRec).ToList();
                }
                if (OData.Count > 0)
                {
                    return Json(new { Reclist = OData, TotalRec = OData.ElementAt(0).TotalRec });
                }
                else
                {
                    return Json(new { Reclist = OData, TotalRec = OData.Count });
                }
            }
            catch (Exception)
            {
                return View(new List<LocationViewModel>());
            }

            #endregion
           // var pkCompanyId = Guid.Empty;
           // BALCompany objbal = new BALCompany();
           // CompanyModel objModel = new CompanyModel();
           ////int PageSize=10;
           // var list = objbal.GetAllLocationsForAdmin(page, pageSize, true, "", System.Guid.Empty, -1, pkCompanyId, "City", "ASC");
           // return Json(new { Reclist = list, TotalRec = list.ElementAt(0).TotalRec });
            
        }

        private static List<Proc_Get_AllLocationsForAdminResult> FilterLocation(GridFilter filter, int PageNum, int PageSize, bool IsPaging, int LocationId, Int16 CompanyType, int FkCompanyId, string SortingColumn, string SortingDirection, out int TotalRec)
        {
            BALCompany ObjBALCompany = new BALCompany();
            List<Proc_Get_AllLocationsForAdminResult> source = new List<Proc_Get_AllLocationsForAdminResult>();
            byte ActionType = 0;
            string ColumnName = filter.Field;
            string FilterValue = filter.Value.ToString();
            ActionType = GetFilterAction(filter, ActionType);
            source = ObjBALCompany.GetAllLocationsForAdmin(PageNum, PageSize, IsPaging, LocationId, CompanyType, FkCompanyId, SortingColumn, SortingDirection, ColumnName, ActionType, FilterValue, out TotalRec).ToList();
            return source;
        }


        public ActionResult EnableLocation(string pkLocationIds)
        {
            int result = 0;
            BALLocation ObjBALLocation = new BALLocation();
            List<tblLocation> LocationList = new List<tblLocation>();
            string[] stringSeparators = new string[] { "#" };
            string[] ListpkLocationIds = pkLocationIds.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
            try
            {
                for (int i = 0; i < ListpkLocationIds.Length; i++)
                {

                        tblLocation objtblLocation = new tblLocation();
                        objtblLocation.pkLocationId = Int32.Parse(ListpkLocationIds[i]);
                        LocationList.Add(objtblLocation);
                }
                result = ObjBALLocation.ActionOnLocations(LocationList, Convert.ToByte(1), Convert.ToBoolean(1));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBALLocation = null;
            }
            return Json(result);

        }


        public ActionResult DisableLocation(string pkLocationIds)
        {
            int result = 0;
            BALLocation ObjBALLocation = new BALLocation();
            List<tblLocation> LocationList = new List<tblLocation>();
            string[] stringSeparators = new string[] { "#" };
            string[] ListpkLocationIds = pkLocationIds.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
            try
            {
                for (int i = 0; i < ListpkLocationIds.Length; i++)
                {

                    tblLocation objtblLocation = new tblLocation();
                    objtblLocation.pkLocationId = Int32.Parse(ListpkLocationIds[i]);
                    LocationList.Add(objtblLocation);
                }
                result = ObjBALLocation.ActionOnLocations(LocationList, Convert.ToByte(1), Convert.ToBoolean(0));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBALLocation = null;
            }
            return Json(result);
        }

        public ActionResult DeleteLocation(string pkLocationIds)
        {
            int result = 0;
            BALLocation ObjBALLocation = new BALLocation();
            List<tblLocation> LocationList = new List<tblLocation>();
            string[] stringSeparators = new string[] { "#" };
            string[] ListpkLocationIds = pkLocationIds.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
            try
            {
                for (int i = 0; i < ListpkLocationIds.Length; i++)
                {

                    tblLocation objtblLocation = new tblLocation();
                    objtblLocation.pkLocationId = Int32.Parse(ListpkLocationIds[i]);
                    LocationList.Add(objtblLocation);
                }
                result = ObjBALLocation.ActionOnLocations(LocationList, Convert.ToByte(2), Convert.ToBoolean(1));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBALLocation = null;
            }
            return Json(result);
        }

        #region Get users by pklocationId for new changes in Ticket #336
        public ActionResult GetUsersUnderLocation(int pkLocationId)
        {
            LocationModel ObjModel = new LocationModel();
            if (pkLocationId != 0)
            {
                ObjModel.LocationId = pkLocationId;
                 ViewBag.locationid = pkLocationId;
                ViewData["LocationId"] = pkLocationId;
                ObjModel.IsEdit = true;
                BALLocation ObjBALLocation = new BALLocation();
                ObjGuid_LocationId = pkLocationId;
                var CompanyLocation = ObjBALLocation.GetLocationDetailByLocationId(ObjGuid_LocationId);
                ViewBag.CompanyId = CompanyLocation.ElementAt(0).fkCompanyId;
                ViewData["PkCompanyId"] = CompanyLocation.ElementAt(0).fkCompanyId;
                tblCompany ObjtbCompany = new tblCompany();
                BALCompany ObjBALCompany = new BALCompany();
                ObjModel.State_Code = GetStateCode(Convert.ToInt32(CompanyLocation.ElementAt(0).fkStateID));
                ObjtbCompany = ObjBALCompany.GetCompanyByCompanyId((int)CompanyLocation.ElementAt(0).fkCompanyId);
                ObjModel.CompanyName = !string.IsNullOrEmpty(ObjtbCompany.CompanyName) ? ObjtbCompany.CompanyName : string.Empty;
                ObjModel.CityName = CompanyLocation.ElementAt(0).City;

            }
            else
            {
                ViewData["LocationId"] = 0;
                ViewData["PkCompanyId"] = 0;
                ObjModel.LocationId = 0;
                ViewBag.locationid = 0;
                ViewBag.CompanyId = 0;
                ObjModel.IsEdit = false;
            }

            return View(ObjModel);
 
        }

        [HttpPost]
        public ActionResult GetEmergeUserByLocationId(int pkLocationId ,int page, int pageSize, List<GridSort> sort, List<GridFilter> filters)
        {

            # region filter
            try
            {
                int PageNum = page;
                int PageSize = pageSize;
                //string SortingColumn = "FirstName";
                //string SortingDirection = "asc";
                //int TotalRec = 0;
                bool IsPaging = true;
                //int LocationId = 0;
                //int FkCompanyId = 0;
                if (sort != null)
                {
                    if (sort[0].Field == "UserDisplayRole")
                    {

                        sort[0].Field = "UserRole";
                    }
                    if (sort[0].Field == "City+IsHome")
                    {

                        sort[0].Field = "City";

                    }
                    //SortingDirection = sort[0].Dir;
                    //SortingColumn = sort[0].Field;
                }
                //Int16 CompanyType = -1;
                BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
                //List<UserViewModel> ObjUserViewModel = new List<UserViewModel>();
                List<Proc_Get_UsersUnderLocationResult> OData = new List<Proc_Get_UsersUnderLocationResult>();
                int PkLocationId = pkLocationId;

                if (filters != null)
                {

                    //foreach (var ObjFilter in filters)
                    //{
                    //   // OData = FilterUsers(ObjFilter, PageNum, PageSize, IsPaging, LocationId, CompanyType, FkCompanyId, SortingColumn, SortingDirection, out  TotalRec);
                    //}
                }
                else
                {
                    OData = ObjBALCompanyUsers.GetUsersUnderLocation(PageNum, PageSize, IsPaging,string.Empty,PkLocationId).ToList();
                }

              //  Session["TotalEmergeUser"] = OData.ElementAt(0).TotalRec;

                if (OData.Count > 0)
                {
                    return Json(new { Reclist = OData, TotalRec = OData.ElementAt(0).TotalRec });
                }
                else
                {
                    return Json(new { Reclist = OData, TotalRec = OData.Count });
                }
            }
            catch (Exception)
            {
                return View(new List<UserViewModel>());
            }

            #endregion
        }

        public ActionResult PricebyLocationId(int pkLocationId)
        {
            LocationModel ObjModel = new LocationModel();
            ObjModel.LocationId = pkLocationId;
            if (pkLocationId != 0)
            {
                ViewBag.locationid = pkLocationId;
                ViewData["LocationId"] = pkLocationId;
                Session["pkNewLocationId"] = pkLocationId;
                BALLocation ObjBALLocation = new BALLocation();
                ObjGuid_LocationId = pkLocationId;
                var CompanyLocation = ObjBALLocation.GetLocationDetailByLocationId(ObjGuid_LocationId);
                ViewBag.CompanyId = CompanyLocation.ElementAt(0).fkCompanyId;
                ViewData["PkCompanyId"] = CompanyLocation.ElementAt(0).fkCompanyId;
                ObjModel.IsGlobalCredential = CompanyLocation.ElementAt(0).IsGlobalCredential;
                ObjModel.pkCompanyId = (int)CompanyLocation.ElementAt(0).fkCompanyId;
                ObjModel.IsEdit = true;
                tblCompany ObjtbCompany = new tblCompany();
                BALCompany ObjBALCompany = new BALCompany();
                ObjModel.State_Code = GetStateCode(Convert.ToInt32(CompanyLocation.ElementAt(0).fkStateID));
                ObjtbCompany = ObjBALCompany.GetCompanyByCompanyId((int)CompanyLocation.ElementAt(0).fkCompanyId);
                ObjModel.CompanyName = !string.IsNullOrEmpty(ObjtbCompany.CompanyName) ? ObjtbCompany.CompanyName : string.Empty;
                ObjModel.CityName = CompanyLocation.ElementAt(0).City;
            }
            else
            {
                ViewBag.locationid = 0;
                ViewData["LocationId"] = 0; 
                Session["pkNewLocationId"] = 0; 
                BALLocation ObjBALLocation = new BALLocation();
                ViewBag.CompanyId = 0;
                ViewData["PkCompanyId"] = 0; 
                ObjModel.IsGlobalCredential = false;
                ObjModel.pkCompanyId = 0;
                ObjModel.IsEdit = false;
            }

            return View(ObjModel);
        }

        public ActionResult RefernceCodeByLocation(int pkLocationId)
        {           
                LocationModel ObjModel = new LocationModel();
                if (pkLocationId != 0)
                {
                    ObjModel.LocationId = pkLocationId;
                    ViewBag.locationid = pkLocationId;
                    ViewData["LocationId"] = pkLocationId;
                    ObjModel.IsEdit = true;
                    BALLocation ObjBALLocation = new BALLocation();
                    ObjGuid_LocationId = pkLocationId;
                    var CompanyLocation = ObjBALLocation.GetLocationDetailByLocationId(ObjGuid_LocationId);
                    ViewBag.CompanyId = CompanyLocation.ElementAt(0).fkCompanyId;
                    ViewData["PkCompanyId"] = CompanyLocation.ElementAt(0).fkCompanyId;
                    tblCompany ObjtbCompany = new tblCompany();
                    BALCompany ObjBALCompany = new BALCompany();
                    ObjModel.State_Code = GetStateCode(Convert.ToInt32(CompanyLocation.ElementAt(0).fkStateID));
                    ObjtbCompany = ObjBALCompany.GetCompanyByCompanyId((int)CompanyLocation.ElementAt(0).fkCompanyId);
                    ObjModel.CompanyName = !string.IsNullOrEmpty(ObjtbCompany.CompanyName) ? ObjtbCompany.CompanyName : string.Empty;
                    ObjModel.CityName = CompanyLocation.ElementAt(0).City;
                }
                else
                {
                    ObjModel.LocationId = 0;
                    ViewBag.locationid = 0;
                    ViewData["LocationId"] = 0; ;
                    ObjModel.IsEdit = false;
                    ViewBag.CompanyId = 0;
                    ViewData["PkCompanyId"] = 0;
                }
                
           return View(ObjModel);
        }
        #endregion
        public ActionResult DeleteLocationById(int pkLocationId)
        {
            int result = 0;
            BALLocation ObjBALLocation = new BALLocation();
            try
            {
                result = ObjBALLocation.DeleteLocationById(pkLocationId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBALLocation = null;
            }
            return Json(result);
        }

    }
}
