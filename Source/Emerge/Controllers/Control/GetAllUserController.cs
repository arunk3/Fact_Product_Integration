﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;


namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /GetAllUser/

        public ActionResult GetAllUsers()
        {


            CompanyModel objModel = new CompanyModel();
            BALCompany serobj = new BALCompany();
            //BALCompanyType objBal = new BALCompanyType();
         // objModel.GetConpanyType = objBal.GetAllCompanyType();
            objModel.GetLocation = serobj.GetAllCompanyLocation();

            List<SelectListItem> County = new List<SelectListItem>();
            SelectListItem s1 = new SelectListItem { Text = "Active", Value = "Active" };
            SelectListItem s2 = new SelectListItem { Text = "InActive", Value = "InActive" };
            SelectListItem s3 = new SelectListItem { Text = "Locked", Value = "Locked" };
            SelectListItem s4 = new SelectListItem { Text = "PastDue", Value = "PastDue" };
            SelectListItem s5 = new SelectListItem { Text = "InCollections", Value = "InCollections" };
            County.Add(s1);
            County.Add(s2);
            County.Add(s3);
            County.Add(s4);
            County.Add(s5);
            objModel.CountryName = County;




            return View(objModel);
        }

        //[HttpPost]
        //public ActionResult GetAllEmergeUser(int page, int pageSize)
        //{
        //    BALCompany serobj = new BALCompany();

        //    var fkCompanyId = Guid.Empty;
        //    var fkLocationId = Guid.Empty;
        //    var list = serobj.GetAllEmergeUsersForAdmin(page, 10, true, "", -1, fkCompanyId, fkLocationId, "FirstName", "ASC");

        //    return Json(new { Reclist = list, TotalRec = list.ElementAt(0).TotalRec });
        //}



        //public ActionResult UpdateDisableUser(string pkid)
        //{
        //    int list = 0;
        //    BALCompany serobj = new BALCompany();
        //    string[] strAssigned = pkid.Split('#');

        //    foreach (var i in strAssigned)
        //    {
        //        if (i != "")
        //        {

        //            list = serobj.UpdateDisableUser((Guid.Parse(i)));

        //        }
        //    }
        //    return Json(list);
        
        //}

        //public ActionResult UpdateEnableUser(string pkid)
        //{
        //    int list = 0;
        //    BALCompany serobj = new BALCompany();
        //    string[] strAssigned = pkid.Split('#');

        //    foreach (var i in strAssigned)
        //    {
        //        if (i != "")
        //        {

        //            list = serobj.UpdateEnableUser((Guid.Parse(i)));

        //        }
        //    }
        //    return Json(list);
        //}
    }
}
