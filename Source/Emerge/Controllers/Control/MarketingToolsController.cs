﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Emerge.Models;
using System.IO;
using Emerge.Common;
using System.Web.UI;
using Emerge.Data;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /MarketingTools/

        public ActionResult MarketingTools()
        {

            MarketingContentModel ObjModel = new MarketingContentModel();

            BALContentManagement ObjBALContentManagement = new BALContentManagement();
            var DBContent = ObjBALContentManagement.GetAllFeatures();
            ObjModel.ObjFeature = DBContent;
            ObjModel.GeneralSettingModel = LoadSettings();
            //  ObjModel.editorContents = "1";
            ObjModel.Message = string.Empty;
            if (Request.QueryString["Status"] != null)
            {
                int status = Convert.ToInt32(Request.QueryString["Status"]);
                if (status == 1)
                {
                    ObjModel.Message = "<span class='successmsg'>Feature updated successfully</span>";
                }
                else if (status == 2)
                {
                    ObjModel.Message = "<span class='successmsg'>Feature added successfully</span>";
                }
                else if (status == 3)
                {
                    ObjModel.Message = "<span class='successmsg'>Feature deleted successfully</span>";
                }
                else if (status == 4)
                {
                    ObjModel.Message = "<span class='successmsg'>Settings updated successfully.</span>";
                }
                else if (status == -1)
                {
                    ObjModel.Message = "<span class='errormsg'>Title name already exist</span>";
                }
                else
                {
                    ObjModel.Message = "<span class='errormsg'>error is occured while updating content</span>";
                }
            }

            List<MyFormModelColl> MyFormModel = new List<MyFormModelColl>();
            var Collection = GetMyFormCollection().ToList();
            if (Collection.Count > 0)
            {
                for (int i = 0; i < Collection.Count; i++)
                {

                    MyFormModel.Add(new MyFormModelColl() { pkformId = Collection.ElementAt(i).pkformId, Title = Collection.ElementAt(i).Title, Path = Collection.ElementAt(i).Path, Description = Collection.ElementAt(i).Description, ModifiedDate = Convert.ToDateTime(Collection.ElementAt(i).ModifiedDate) , ModifiedLoggedId = Collection.ElementAt(i).ModifiedLoggedId != null ? (Guid)Collection.ElementAt(i).ModifiedLoggedId : Guid.Empty });

                }

                ObjModel.TotalCounts = Collection.Count;
                ObjModel.RowCounts = Collection.Count / 4;

            }

            ObjModel.Collection = MyFormModel;


            ViewBag.Collection = ObjBALContentManagement.GetAllFeaturesEnabled();
            return View(ObjModel);
        }

        #region General Settings

        BALGeneral ObjBALGeneral = null;

        public GeneralSettingModel LoadSettings()
        {
            GeneralSettingModel ObjModel = new GeneralSettingModel();
           // string data = string.Empty;

            ObjBALGeneral = new BALGeneral();
            byte settingId = 1;
            tblGeneralSetting settings = ObjBALGeneral.GetSettings(settingId);
            if (settings != null)
            {

                ObjModel.PageContents = Server.HtmlDecode(settings.PageContents);
                ObjModel.WebSiteLogo = settings.WebSiteLogo;
            }
            return ObjModel;

        }

        public ActionResult SaveSettings(MarketingContentModel ObjModel)
        {
            string strSuccess = string.Empty;
            ObjBALGeneral = new BALGeneral();
            try
            {
                tblGeneralSetting objSettings = new tblGeneralSetting();
                objSettings.PageContents = Server.HtmlDecode(ObjModel.GeneralSettingModel.PageContents);
                int success = ObjBALGeneral.UpdateSettings(objSettings);
                if (success == 1)
                {
                    strSuccess = "4";
                }
                else
                {
                    strSuccess = "-2";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBALGeneral = null;
            }
            return RedirectToAction("MarketingTools", new { Status = 4 });
        }

        #endregion General Settings

        #region Contents
        [HttpPost]
        public ActionResult LoadContents()
        {

            Guid ApplicationId = new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852");
            BALContentManagement ObjBALContentManagement = new BALContentManagement();

            var DBContent = ObjBALContentManagement.GetContents(ApplicationId).Select(p => new ContentManagementModel
            {
                ContentId = p.Id,
                ContentTitle = p.Title,
                IsCommentEnabled = p.IsEnabled,
                ContentComment = p.Comment,

            }).ToList();
            return Json(DBContent);
        }


        public ActionResult GetModuleContents(int SelectedModuleId)
        {
            var Contents = "";
            var ModuleColor = "";
            tblLoginPageManagement ObjtblLoginMgt = new tblLoginPageManagement();
            BALContentManagement ObjContentsMgt = new BALContentManagement();
            ObjtblLoginMgt = ObjContentsMgt.GetLoginPageContents(SelectedModuleId);
            Contents = HttpUtility.HtmlDecode(ObjtblLoginMgt.Contents);
            ModuleColor = ObjtblLoginMgt.ModuleColor;
            if (ModuleColor == " ")
            {
                ModuleColor = "-1";
            }

            return Json(new { Contents = Contents, ModuleColor = ModuleColor });
        }

        public ActionResult SaveloginLogoSettingInfo1(string LogoColorList, string imageHeight, string LoginLogImage)
        {
            string result = "one";
            if (imageHeight == "")
            {
                imageHeight = "-1";
            }
            if (LoginLogImage == "")
            {
                LoginLogImage = "-1";
            }
            BALContentManagement ObjContentsMgt = new BALContentManagement();

            string sFileName = string.Empty;
            string sFilePath;
            string TitleIamgeFilePath;
            string[] str = LoginLogImage.Split('.');
           // string FileName = str[1];
            string sExt2 = str[1];
            if (sExt2 != "" && LoginLogImage != "-1")
            {
                // when only TITLE IMAGE Uploded

                sFileName = DateTime.Now.ToFileTime().ToString() + sExt2;
                sFilePath = Server.MapPath("~/Resources/Upload/LoginLogo/");

                TitleIamgeFilePath = Server.MapPath("~/Resources/Upload/LoginLogo/");


            }
            if (sExt2 != "")
            {
                HttpPostedFileBase ObjHttpPostedFile = Request.Files[0];
                ObjHttpPostedFile.SaveAs(Server.MapPath(@"~\Resources\Upload\LoginLogo\") + sFileName);


            }

            ObjContentsMgt.UpdatetblLoginPageSetting("-1", "-1", imageHeight, LogoColorList, LoginLogImage);
            return Json(new { result = result });

        }

        public ActionResult SaveModuleContents(string SelectedModuleId, string SelectedModuleContents, string SelectdColor, bool IsEnable)
        {
            BALContentManagement ObjContentsMgt = new BALContentManagement();


            int result = ObjContentsMgt.UpdateLoginPagesContents(Convert.ToInt32(SelectedModuleId.ToString()), SelectedModuleContents, SelectdColor, IsEnable);


            return Json(new { result = result });
        }


        public ActionResult AddContent(IEnumerable<ContentManagementModel> ObjModel)
        {
            string strSuccess = string.Empty;

            for (int i = 0; i < ObjModel.Count(); i++)
            {
                DateTime CreatedDate;
                CreatedDate = DateTime.Now;
                Guid SiteApplicationId = new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852");
                tblContentManagement ObjtblContentManagement = new tblContentManagement();
                ObjtblContentManagement.ApplicationId = SiteApplicationId;
                ObjtblContentManagement.Title = ObjModel.ElementAt(i).ContentTitle.Trim();
                ObjtblContentManagement.Comment = ObjModel.ElementAt(i).ContentComment.Trim();
                ObjtblContentManagement.IsEnabled = ObjModel.ElementAt(i).IsCommentEnabled;
                ObjtblContentManagement.CreatedDate = CreatedDate;
                BALContentManagement ObjBALContentManagement = new BALContentManagement();
                try
                {
                    int Sucess = 0;
                    if (ObjModel.ElementAt(i).ContentTitle != "")
                    {
                        //lblMessageBox.Visible = true;
                        Sucess = ObjBALContentManagement.AddContent(ObjtblContentManagement, SiteApplicationId);
                        if (Sucess == 1)
                        {
                            strSuccess = "<span class='successmsg'>Content Added Successfully</span>";
                        }
                        else if (Sucess == -1)
                        {
                            strSuccess = "<span class='errormsg'>Title name already exist</span>";
                        }
                        else
                        {
                            strSuccess = "<span class='errormsg'>error is occured while updating content</span>";
                        }
                    }
                    else
                    {
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
            Response.StatusCode = 500;
            return Json(new { Message = strSuccess });
        }

        public ActionResult UpdateContentById(IEnumerable<ContentManagementModel> ObjModel)
        {
            string strSuccess = string.Empty;
            try
            {
                for (int i = 0; i < ObjModel.Count(); i++)
                {

                    DateTime LastModifiedDate;
                    LastModifiedDate = DateTime.Now;
                    tblContentManagement ObjtblContentManagement = new tblContentManagement();
                    ObjtblContentManagement.Id = ObjModel.ElementAt(i).ContentId;
                    ObjtblContentManagement.Title = ObjModel.ElementAt(i).ContentTitle.Trim();
                    ObjtblContentManagement.Comment = ObjModel.ElementAt(i).ContentComment.Trim();
                    ObjtblContentManagement.IsEnabled = ObjModel.ElementAt(i).IsCommentEnabled;
                    ObjtblContentManagement.LastModifiedDate = LastModifiedDate;
                    int Success = 0;
                    BALContentManagement ObjBALContentManagement = new BALContentManagement();
                    Success = ObjBALContentManagement.UpdateContent(ObjtblContentManagement);
                    if (Success == 1)
                    {
                        strSuccess = "<span class='successmsg'>Content updated Successfully</span>";
                    }
                    else if (Success == -1)
                    {
                        strSuccess = "<span class='errormsg'>Title name already exist</span>";
                    }
                    else
                    {
                        strSuccess = "<span class='errormsg'>error is occured while updating content</span>";
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
            Response.StatusCode = 500;
            return Json(new { Message = strSuccess });
        }

        public ActionResult DeleteContentById(IEnumerable<ContentManagementModel> ObjModel)
        {
            string strSuccess = string.Empty;
            try
            {
                for (int i = 0; i < ObjModel.Count(); i++)
                {
                    int Success = 0;
                    Guid SiteApplicationId = new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852");
                    int index = ObjModel.ElementAt(i).ContentId;
                    BALContentManagement ObjBALContentManagement = new BALContentManagement();
                    try
                    {
                        Success = ObjBALContentManagement.DeleteContent(index, SiteApplicationId);
                        if (Success == 1)
                        {
                            strSuccess = "<span class='successmsg'>Content successfully deleted</span>";
                        }
                        else
                        {
                            strSuccess = "<span class='errormsg'>Error occured on deleting the Content</span>";
                        }
                    }
                    catch (Exception)
                    {
                    }

                }
            }
            catch
            {
            }
            Response.StatusCode = 500;
            return Json(new { Message = strSuccess });
        }



        #endregion Contents

        #region New Features


        [HttpPost]
        public ActionResult UpdateFeature(FormCollection ObjCollection)
        {
            BALContentManagement ObjBALContentManagement = new BALContentManagement();
            string strSuccess = string.Empty;
            try
            {
                DateTime LastModifiedDate = DateTime.Now;
                string sFileName = string.Empty;
                string sTitleImage = string.Empty;
                if (Request.Files[0].FileName != "")
                {
                    string sExt = Path.GetExtension(Request.Files[0].FileName).ToLower();
                    sTitleImage = DateTime.Now.ToFileTime().ToString() + sExt;
                    //string sFilePath = "";
                   // sFilePath = Server.MapPath("~/Resources/Upload/FeatureFiles/");
                    HttpPostedFileBase ObjHttpPostedFile = Request.Files[0];
                    ObjHttpPostedFile.SaveAs(Server.MapPath(@"~\Resources\Upload\FeatureFiles\") + sTitleImage);
                    string OldInageTitleFilePath = Server.MapPath(@"~\Resources\Upload\FeatureFiles\") + Convert.ToString(ObjCollection.GetValue("ImageFilePath").AttemptedValue);
                    Utility.DeleteFile(OldInageTitleFilePath);

                }
                else
                {
                    sTitleImage = Convert.ToString(ObjCollection.GetValue("ImageFilePath").AttemptedValue);

                }

                if (Request.Files[1].FileName != "")
                {
                    string sExt = Path.GetExtension(Request.Files[1].FileName).ToLower();
                    sFileName = DateTime.Now.ToFileTime().ToString() + sExt;
                    string sFilePath = "";
                    sFilePath = Server.MapPath("~/Resources/Upload/FeatureFiles/");
                    HttpPostedFileBase ObjHttpPostedFile = Request.Files[1];
                    ObjHttpPostedFile.SaveAs(Server.MapPath(@"~\Resources\Upload\FeatureFiles\") + sFileName);
                    string OldFilePath = Server.MapPath(@"~\Resources\Upload\FeatureFiles\") + Convert.ToString(ObjCollection.GetValue("FilePath").AttemptedValue);
                    Utility.DeleteFile(OldFilePath);
                }
                else
                {
                    sFileName = Convert.ToString(ObjCollection.GetValue("FilePath").AttemptedValue);
                }





                tblFeature ObjtblFeature = new tblFeature();
                ObjtblFeature.FeatureId = Convert.ToInt32(ObjCollection.GetValue("FeatureId").AttemptedValue);
                ObjtblFeature.Title = Convert.ToString(ObjCollection.GetValue("Title").AttemptedValue);
                ObjtblFeature.Description = Convert.ToString(ObjCollection.GetValue("description").AttemptedValue);
                ObjtblFeature.UploadedFile = sFileName;
                ObjtblFeature.UploadedTitleImage = sTitleImage;
                ObjtblFeature.IsEnabled = (ObjCollection.GetValue("Enabled").AttemptedValue == null ? false : true);
                ObjtblFeature.LastModifiedDate = LastModifiedDate;
                int Success = 0;

                Success = ObjBALContentManagement.UpdateFeatureDetails(ObjtblFeature);
                if (Success == 1)
                {
                    strSuccess = "1";// "<span class='successmsg'>Feature Updated Successfully</span>";
                }
                else if (Success == -1)
                {
                    strSuccess = "-1";// "<span class='errormsg'>Title name already exist</span>";
                }
                else
                {
                    strSuccess = "-2"; //"<span class='errormsg'>error is occured while updating content</span>";
                }

            }
            catch (Exception)
            {
            }

            //#region Bind Feature Grid Again

            //MarketingContentModel ObjModel = new MarketingContentModel();
            //var DBContent = ObjBALContentManagement.GetAllFeatures();
            //ObjModel.ObjFeature = DBContent;
            //ObjModel.Message = strSuccess;

            //#endregion

            // return View(ObjModel);
            return RedirectToAction("MarketingTools", new { Status = strSuccess }); // PartialView("../Common/PartialFeatureList", ObjModel);
        }

        [HttpPost]
        public ActionResult LoadFeatures()
        {
            //LblMsgf.Visible = false;
            BALContentManagement ObjBALContentManagement = new BALContentManagement();
            //List<clsManageFeatures> ObjColl = new List<clsManageFeatures>();
            var DBContent = ObjBALContentManagement.GetAllFeatures().Select(p => new MarketingToolsModel
            {
                FeatureId = p.FeatureId,
                Title = p.Title,
                Description = p.Description,
                UploadedFile = p.UploadedFile,
                ViewFile = p.UploadedFile,
                IsEnabled = p.IsEnabled,
                FeatureCreatedDate = p.FeatureCreatedDate,
                LastModifiedDate = p.LastModifiedDate,
                ViewCount = p.ViewCount
            }).ToList();

            return Json(DBContent);
        }


        public ActionResult DeleteFeature(int FeatureId)
        {
            int Success = 0;

            BALContentManagement ObjBALContentManagement = new BALContentManagement();
            try
            {
                Success = ObjBALContentManagement.DeleteFeature(FeatureId);
                if (Success == 1)
                {
                    Success = 3;
                }
                else
                {
                    Success = -2;
                }
            }
            catch (Exception)
            {

            }
            return Json(Success);

        }

        [HttpPost]
        public ActionResult AddLoadingImage(FormCollection ObjCollection)
        {

            string LogoImgColor = ObjCollection.GetValue("Color").AttemptedValue;
            string LoadingImage = "-1";

           // string result = "one";


            BALContentManagement ObjContentsMgt = new BALContentManagement();

            string sFilePath = "";
            string TitleIamgeFilePath = "";
            string sFileName = "";

            string sExt2 = Path.GetExtension(Request.Files[0].FileName).ToLower();


            if (sExt2 != "")
            {
                // when only TITLE IMAGE Uploded

                sFileName = DateTime.Now.ToFileTime().ToString() + sExt2;
                LoadingImage = sFileName;
                sFilePath = Server.MapPath("~/Resources/Upload/FeatureFiles/");

                TitleIamgeFilePath = Server.MapPath("~/Resources/Upload/FeatureFiles/");


            }
            if (sExt2 != "")
            {
                HttpPostedFileBase ObjHttpPostedFile1 = Request.Files[0];

                ObjHttpPostedFile1.SaveAs(Server.MapPath(@"~\Resources\Upload\LoginLogo\") + sFileName);


            }

            ObjContentsMgt.UpdatetblLoginPageSetting(LoadingImage, LogoImgColor, "-1", "-1", "-1");
            return RedirectToAction("MarketingTools");
        }

        [HttpPost]
        public ActionResult AddLoginPageLogo(FormCollection ObjCollection)
        {
            string LoginLogImage = "-1";

            //string result = "one";

            BALContentManagement ObjContentsMgt = new BALContentManagement();

            string sFilePath = "";
            string TitleIamgeFilePath = "";
            string sFileName = "";

            string sExt2 = Path.GetExtension(Request.Files[0].FileName).ToLower();


            if (sExt2 != "")
            {
                // when only TITLE IMAGE Uploded

                sFileName = DateTime.Now.ToFileTime().ToString() + sExt2;
                LoginLogImage = sFileName;
                sFilePath = Server.MapPath("~/Resources/Upload/FeatureFiles/");

                TitleIamgeFilePath = Server.MapPath("~/Resources/Upload/FeatureFiles/");


            }
            if (sExt2 != "")
            {
                HttpPostedFileBase ObjHttpPostedFile1 = Request.Files[0];

                ObjHttpPostedFile1.SaveAs(Server.MapPath(@"~\Resources\Upload\LoginLogo\") + sFileName);


            }

            ObjContentsMgt.UpdatetblLoginPageSetting("-1", "-1", "-1", "-1", LoginLogImage);
            return RedirectToAction("MarketingTools");
        }

        [HttpPost]
        public ActionResult AddLoginPageBack(FormCollection ObjCollection)
        {
            string LoginBackColor = ObjCollection.GetValue("Color").AttemptedValue;
            string LoginBackImage = "-1";

            //string result = "one";

            BALContentManagement ObjContentsMgt = new BALContentManagement();

            string sFilePath = "";
            string TitleIamgeFilePath = "";
            string sFileName = "";

            string sExt2 = Path.GetExtension(Request.Files[0].FileName).ToLower();


            if (sExt2 != "")
            {
                // when only TITLE IMAGE Uploded

                sFileName = DateTime.Now.ToFileTime().ToString() + sExt2;
                LoginBackImage = sFileName;
                sFilePath = Server.MapPath("~/Resources/Upload/FeatureFiles/");

                TitleIamgeFilePath = Server.MapPath("~/Resources/Upload/FeatureFiles/");


            }
            if (sExt2 != "")
            {
                HttpPostedFileBase ObjHttpPostedFile1 = Request.Files[0];

                ObjHttpPostedFile1.SaveAs(Server.MapPath(@"~\Resources\Upload\LoginLogo\") + sFileName);


            }

            ObjContentsMgt.UpdatetblLoginPageSetting("-1", "-1", LoginBackImage, LoginBackColor, "-1");
            return RedirectToAction("MarketingTools");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddLoginPageBackGroungImage(FormCollection ObjCollection)
        {
            BALContentManagement ObjContentsMgt = new BALContentManagement();
            string LoginBackPosition = ObjCollection["horizonPosition"].ToString() + " " + ObjCollection["verticalPosition"].ToString();
            string LoginBackImage = "-1";
            string sFileName = "";

            string sExt2 = Path.GetExtension(Request.Files[0].FileName).ToLower();
        
            if (sExt2 != "")
            {
                sFileName = DateTime.Now.ToFileTime().ToString() + sExt2;
                LoginBackImage = sFileName;
                HttpPostedFileBase ObjHttpPostedFile1 = Request.Files[0];
                ObjHttpPostedFile1.SaveAs(Server.MapPath(@"~\Resources\Upload\LoginLogo\") + sFileName);
            }

            ObjContentsMgt.UpdatetblLoginPageSetting("-1", "-1", LoginBackImage, "-1", "-1", LoginBackPosition);
            return RedirectToAction("MarketingTools");
        }

        [HttpPost]
        public ActionResult AddFeature(FormCollection ObjCollection)
        {
            string strSuccess = string.Empty;
            try
            {

                if (Request.Files[0].FileName != "" || Request.Files[1].FileName != "")
                {
                    string sFilePath = "";
                    string TitleIamgeFilePath = "";
                    string sFileName = "";
                    string sFileTitleIamge = "";
                    string sExt = Path.GetExtension(Request.Files[0].FileName).ToLower();
                    string sExt2 = Path.GetExtension(Request.Files[1].FileName).ToLower();
                    if (sExt2 != "")
                    {
                        // when only TITLE IMAGE Uploded

                        sFileName = DateTime.Now.ToFileTime().ToString() + sExt2;
                        sFilePath = Server.MapPath("~/Resources/Upload/FeatureFiles/");

                        TitleIamgeFilePath = Server.MapPath("~/Resources/Upload/FeatureFiles/");


                    }
                    if (sExt != "")
                    {
                        sFilePath = Server.MapPath("~/Resources/Upload/FeatureFiles/");
                        sFileTitleIamge = DateTime.Now.AddMonths(-6).ToFileTime().ToString() + sExt;
                        TitleIamgeFilePath = Server.MapPath("~/Resources/Upload/FeatureFiles/");

                    }



                    tblFeature ObjtblFeature = new tblFeature();
                    ObjtblFeature.Title = Convert.ToString(ObjCollection.GetValue("Title").AttemptedValue);
                    ObjtblFeature.Description = Convert.ToString(ObjCollection.GetValue("description").AttemptedValue);
                    ObjtblFeature.UploadedTitleImage = sFileTitleIamge;
                    ObjtblFeature.UploadedFile = sFileName;
                    ObjtblFeature.IsEnabled = (ObjCollection.GetValue("Enabled").AttemptedValue == null ? false : true);
                    ObjtblFeature.FeatureCreatedDate = DateTime.Now;



                    BALContentManagement ObjBALContentManagement = new BALContentManagement();
                    int Success = 0;
                    Success = ObjBALContentManagement.InsertFeatureDetails(ObjtblFeature);
                    if (Success > 0)
                    {
                        if (sExt2 != "")
                        {
                            HttpPostedFileBase ObjHttpPostedFile = Request.Files[1];
                            ObjHttpPostedFile.SaveAs(Server.MapPath(@"~\Resources\Upload\FeatureFiles\") + sFileName);
                        }
                        if (sExt != "")
                        {
                            HttpPostedFileBase ObjHttpPostedFile = Request.Files[0];
                            ObjHttpPostedFile.SaveAs(Server.MapPath(@"~\Resources\Upload\FeatureFiles\") + sFileTitleIamge);
                        }
                        BALEmailSystemTemplates ObjEmailTemplate = new BALEmailSystemTemplates();
                        ObjEmailTemplate.InsertAlerts(Success, DateTime.Now);


                        strSuccess = "2";// "<span class='successmsg'>Feature Updated Successfully</span>";
                    }
                    else if (Success == -1)
                    {
                        strSuccess = "-1";// "<span class='errormsg'>Title name already exist</span>";
                    }
                    else
                    {
                        strSuccess = "-2"; //"<span class='errormsg'>error is occured while updating content</span>";
                    }
                }

            }
            catch (Exception)
            {
            }
            return RedirectToAction("MarketingTools", new { Status = strSuccess });

        }


        public ActionResult LoadFeaturePreview(int FeatureId)
        {
            BALContentManagement ObjBALContentManagement = new BALContentManagement();
            tblFeature ObjtblFeature = new tblFeature();
            try
            {               
                ObjtblFeature = ObjBALContentManagement.GetFeatureListById(FeatureId);

                if (!System.IO.File.Exists(Server.MapPath(@"~/Resources/Upload/FeatureFiles/" + ObjtblFeature.UploadedFile)))
                {
                    ObjtblFeature.UploadedFile = "../Resources/Upload/FeatureFiles/no_image.jpg";
                }
                else if (!(ObjtblFeature.UploadedFile.ToLower().Contains(".flv") || ObjtblFeature.UploadedFile.ToLower().Contains(".swf")))
                {
                    ObjtblFeature.UploadedFile = ApplicationPath.GetApplicationPath() + "GetImage.ashx?ImagePath=~/Resources/Upload/FeatureFiles/" + ObjtblFeature.UploadedFile + "&Height=350&Width=650";
                }

                if (System.IO.File.Exists(Server.MapPath(@"~/Resources/Upload/FeatureFiles/" + ObjtblFeature.UploadedTitleImage)))
                {
                    //"Content/Handler/Photogallery.ashx?PhotoGallery=~/Resources/Upload/FeatureFiles/"
                    ObjtblFeature.UploadedTitleImage = ApplicationPath.GetApplicationPath() + "GetImage.ashx?ImagePath=~/Resources/Upload/FeatureFiles/" + ObjtblFeature.UploadedTitleImage + "&Height=40&Width=500";
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                ObjBALContentManagement = null;
            }
            return Json(ObjtblFeature);
        }



        public FilePathResult DownloadFeaturedTitleImage(string FPathTitle)
        {
            string[] str = FPathTitle.Split('_');
            string FileName = str[0];
            string Title = str[1].Replace(" ", "_");
            string FilePath = Request.PhysicalApplicationPath + "Resources/Upload/FeatureFiles/" + FileName;
            string fileext = System.IO.Path.GetExtension(FileName);
            string CType = "application/" + fileext.Replace(".", "");
            //string fileName = Path.GetFileName(FileName);

            string strFileTitle = Title + fileext;



            if (System.IO.File.Exists(FilePath))
            {
                return File(FilePath, CType, strFileTitle);
            }
            else
            {
                FilePath = Request.PhysicalApplicationPath + "Resources/Upload/FeatureFiles/no-image.jpg";
                return File(FilePath, CType, strFileTitle);
            }
        }


        public FilePathResult DownloadFeaturedFile(string FPathTitle)
        {
            string[] str = FPathTitle.Split('_');
            string FileName = str[0];
            string Title = str[1].Replace(" ", "_");
            string FilePath = Request.PhysicalApplicationPath + "Resources/Upload/FeatureFiles/" + FileName;
            string fileext = System.IO.Path.GetExtension(FileName);
            string CType = "application/" + fileext.Replace(".", "");
            //string fileName = Path.GetFileName(FileName);

            string strFileTitle = Title + fileext;



            if (System.IO.File.Exists(FilePath))
            {
                return File(FilePath, CType, strFileTitle);
            }
            else
            {
                FilePath = Request.PhysicalApplicationPath + "Resources/Upload/FeatureFiles/no-image.jpg";
                return File(FilePath, CType, strFileTitle);
            }
        }

        public ActionResult SaveResult(IEnumerable<MarketingToolsModel> ObjModel)
        {
            //List<MarketingToolsModel> ObjMarketingToolsModel = new List<MarketingToolsModel>();
            //BALContentManagement ObjBALVendor = new BALContentManagement();
            string strMessage = "";
            for (int i = 0; i < ObjModel.Count(); i++)
            {
                if (ObjModel.ElementAt(i).FeatureId > 0)
                {
                    //byte pkVendorId = ObjVendors.ElementAt(i).pkVendorId;
                    //string ServiceUrl = ObjVendors.ElementAt(i).ServiceUrl;
                    //string ServiceUserId = ObjVendors.ElementAt(i).ServiceUserId;
                    //string ServicePassword = ObjVendors.ElementAt(i).ServicePassword;
                    //if (pkVendorId > 0)
                    //{
                    //    tblVendor ObjtblVendor = new tblVendor();
                    //    ObjtblVendor.pkVendorId = pkVendorId;
                    //    ObjtblVendor.ServiceUrl = ServiceUrl;
                    //    ObjtblVendor.ServiceUserId = ServiceUserId;
                    //    ObjtblVendor.ServicePassword = ServicePassword;
                    //    ObjtblVendor.LastModifiedDate = DateTime.Now;
                    //    int iStatus = ObjBALVendor.UpdateVendorDetails(ObjtblVendor);
                    //    if (iStatus == 1)
                    //    {
                    //        strMessage = "<span class='successmsg'>Vendor details updated successfully</span>";
                    //    }
                    //    else
                    //    {
                    //        strMessage = "<span class='errormsg'>Some error occur, please try later</span>";
                    //    }
                    //}
                }
            }
            return Json(new { Msg = strMessage, Class = "successmsg" });
        }

        #endregion New Features
        public FilePathResult DownloadMyForms(string FPathTitle)
        {
            string FilePath = Server.MapPath("~/Resources/Forms/" + FPathTitle);
            string fileext = System.IO.Path.GetExtension(FilePath);
            string CType = "application/" + fileext.Replace(".", "");
            string fileName = Path.GetFileName(FilePath);
            return File(FilePath, CType, fileName);
        }

        public FilePathResult DownloadCustomForms(string FPathTitle)
        {
            string FilePath = Server.MapPath("~/Resources/Upload/CustomForm/" + FPathTitle);
            string fileext = System.IO.Path.GetExtension(FilePath);
            string CType = "application/" + fileext.Replace(".", "");
            string fileName = Path.GetFileName(FilePath);
            return File(FilePath, CType, fileName);
        }

    }

}
