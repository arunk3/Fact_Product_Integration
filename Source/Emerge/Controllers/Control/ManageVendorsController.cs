﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using Emerge.Models;
using System.Xml.Linq;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /ManageVendors/

        public ActionResult ManageVendors()
        {
            /*BALVendor ObjBALVendor = new BALVendor();
            var iRecord = ObjBALVendor.GetVendorDetails();
            string secondarypassword = GetMvrPassword(iRecord.Where(data => data.pkVendorId==5).First().AdditionalParams);
            ViewData["secondarypassword"] = secondarypassword;     */
            ViewData["secondarypassword"] = "No longer used";
            return View();
        }
        private string GetMvrPassword(XElement xElement)
        {
            var Node = xElement.Descendants("secondarypassword").First();
            string secondarypassword = "";
            if (Node != null)
            {
                secondarypassword= Node.Value;
            }
            return secondarypassword;
        }
        [HttpPost]
        public ActionResult GetVendorDetails()
        {
            BALVendor ObjBALVendor = new BALVendor();
            using (EmergeDALDataContext EmergeDALDataContext = new EmergeDALDataContext())
            {
                var iRecord = ObjBALVendor.GetVendorDetails();

                List<ManageVendorsModel> ObjVendors = iRecord.Select(data => new ManageVendorsModel
                {
                    pkVendorId = data.pkVendorId,
                    VendorName = data.VendorName,
                    ServiceUrl = data.ServiceUrl,
                    ServiceUserId = data.ServiceUserId,
                    ServicePassword = data.ServicePassword
                }).ToList();

                return Json(ObjVendors);
            }
        }

        [HttpPost]
        public ActionResult GetReportsByVendorId(byte pkVendorId)
        {
            BALVendor ObjBALVendor = new BALVendor();
            using (EmergeDALDataContext EmergeDALDataContext = new EmergeDALDataContext())
            {
                var ReportList = ObjBALVendor.GetEmergeReportsByVendorId(pkVendorId).OrderBy(t => t.SortOrder).ToList();
                List<ManageProductVendorPriceModel> ObjVendors = ReportList.Select(data => new ManageProductVendorPriceModel
                {
                    pkProductApplicationId = data.pkProductApplicationId,
                    IsGlobal = data.IsGlobal,
                    IsEnabled = data.IsEnabled,
                    IsDefault = data.IsDefault,
                    ProductName = data.ProductName,
                    ProductDisplayName = data.ProductDisplayName,
                    ProductCode = data.ProductCode,
                    ProductVendorPrice = data.ProductVendorPrice,
                    ProductPerApplicationPrice = data.ProductPerApplicationPrice
                }).ToList();
                return Json(ReportList);
            }
        }

        [HttpPost]
        public ActionResult UpdateVendorDetails(IEnumerable<ManageVendorsModel> ObjVendors)
        {
            //List<ManageVendorsModel> ObjVendorDetail = new List<ManageVendorsModel>();
            BALVendor ObjBALVendor = new BALVendor();
            string strMessage = "";
            for (int i = 0; i < ObjVendors.Count(); i++)
            {
                if (ObjVendors.ElementAt(i).pkVendorId > 0)
                {
                    byte pkVendorId = ObjVendors.ElementAt(i).pkVendorId;
                    string ServiceUrl = ObjVendors.ElementAt(i).ServiceUrl;
                    string ServiceUserId = ObjVendors.ElementAt(i).ServiceUserId;
                    string ServicePassword = ObjVendors.ElementAt(i).ServicePassword;
                    if (pkVendorId > 0)
                    {
                        tblVendor ObjtblVendor = new tblVendor();
                        ObjtblVendor.pkVendorId = pkVendorId;
                        ObjtblVendor.ServiceUrl = ServiceUrl;
                        ObjtblVendor.ServiceUserId = ServiceUserId;
                        ObjtblVendor.ServicePassword = ServicePassword;
                        ObjtblVendor.LastModifiedDate = DateTime.Now;
                        int iStatus = ObjBALVendor.UpdateVendorDetails(ObjtblVendor);
                        if (iStatus == 1)
                        {
                            strMessage = "<span class='successmsg'>Vendor details updated successfully</span>";
                        }
                        else
                        {
                            strMessage = "<span class='errormsg'>Some error occur, please try later</span>";
                        }
                    }
                }
            }
            return Json(new { Msg = strMessage, Class = "successmsg" });
        }

        [HttpPost]
        public ActionResult UpdateVendorPrice(IEnumerable<ManageProductVendorPriceModel> ObjVendorsPrice)
        {
            //List<ManageProductVendorPriceModel> ObjProductVendorPrice = new List<ManageProductVendorPriceModel>();
            BALVendor ObjBALVendor = new BALVendor();
            string strMessage = "";
            for (int i = 0; i < ObjVendorsPrice.Count(); i++)
            {
                if (ObjVendorsPrice.ElementAt(i).pkProductApplicationId != 0)
                {
                    int pkProductApplicationId = ObjVendorsPrice.ElementAt(i).pkProductApplicationId;
                    decimal ProductVendorPrice = ObjVendorsPrice.ElementAt(i).ProductVendorPrice;
                    if (pkProductApplicationId != 0)
                    {
                        tblProductPerApplication ObjProductPerApplication = new tblProductPerApplication();
                        ObjProductPerApplication.pkProductApplicationId = pkProductApplicationId;
                        ObjProductPerApplication.ProductVendorPrice = ProductVendorPrice;
                        int iStatus = ObjBALVendor.SaveProductVendorPrice(ObjProductPerApplication);
                        if (iStatus == 1)
                        {
                            strMessage = "<span class='successmsg'>Vendor details updated successfully</span>";
                        }
                        else
                        {
                            strMessage = "<span class='errormsg'>Some error occur, please try later</span>";
                        }
                    }
                }
            }
            return Json(new { Msg = strMessage, Class = "successmsg" });
        }

        public ActionResult UpdateMvrPassword(string MvrPassword)
        {
            int list = 0;
            BALVendor ObjBALVendor = new BALVendor();
            list = ObjBALVendor.UpdateMvrPassword(MvrPassword);
            
            return Json(list);

        }
    }
}
