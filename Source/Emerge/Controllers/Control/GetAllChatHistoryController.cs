﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Kendo.Mvc.UI;
using System.Collections;
using System.IO;
using System.Text;
using Emerge.Models;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /GetAllChatHistory/

        public ActionResult GetAllChatHistory()
        {

            return View();
        }

        public ActionResult GetAllChatHistoryforAdmin()
        {
            BALChat objBalChat = new BALChat();
            var list = objBalChat.GetChatHistoryforAdmin();


            return Json(new { Reclist = list, TotalRec = 0 });
        }


        public FileResult ExportToExcel()
        {

            //   IEnumerable products = db.Products.ToDataSourceResult(request).Data;

            BALChat objBalChat = new BALChat();
            var products = objBalChat.GetChatHistoryforAdmin();
            MemoryStream output = new MemoryStream();

            using (StreamWriter writer = new StreamWriter(output, Encoding.UTF8))
            {
                writer.Write("City,");
                writer.Write("CompanyName,");
                writer.Write("FirstName");
                writer.WriteLine();

                for (int i = 0; i < products.Count(); i++)
                {

                    writer.Write(products.ElementAt(i).City);
                    writer.Write(",");
                    writer.Write("\"");
                    writer.Write(products.ElementAt(i).CompanyName);
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");
                    writer.Write(products.ElementAt(i).FirstName);
                    writer.WriteLine();
                }
                writer.Flush();
            }
            output.Position = 0;

            return File(output, "text/comma-separated-values", "Products.csv");
        }
    }
}
