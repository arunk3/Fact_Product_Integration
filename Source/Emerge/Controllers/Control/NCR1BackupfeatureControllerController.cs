﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Emerge.Models;
using Emerge.Data;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /NCR1Backupfeature/

        public ActionResult NCR1Backupfeature()
        {

            using (EmergeDALDataContext obj = new EmergeDALDataContext())
            {
                var objtblgeneralsetting = obj.tblGeneralSettings;
                NCR1Backupfeaturemodel o = new NCR1Backupfeaturemodel();
                o.updatestatus = objtblgeneralsetting.FirstOrDefault().IsNCR1Backupfeature;
                return View(o);
            }
        }

        [HttpPost]
        public ActionResult UpdateBackupstatus(FormCollection Collection)
        {
            string Msg = "Some error occured during the updation";
            string Css = "errormsgSMALL";
            try
            {
                string updatestatusvalue = Collection["updatestatus"];
                var updatestatuvalue1 = updatestatusvalue.Split(',')[0];
                using (EmergeDALDataContext DataContext = new EmergeDALDataContext())
                {
                    var objtblgeneralsetting = DataContext.tblGeneralSettings.FirstOrDefault();
                    objtblgeneralsetting.IsNCR1Backupfeature = Convert.ToBoolean(updatestatuvalue1);
                    DataContext.SubmitChanges();
                    Msg = "Updated Successfully";
                    Css = "successmsg";
                }
            }
            catch// (Exception ex)
            {
            }
            return Json(new { Message = Msg, Css = Css });
        }


    }
}
