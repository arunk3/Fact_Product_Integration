﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using System.Web.Security;


namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {

        //
        // GET: /HelpWithOrders/

        public ActionResult PartialHelpWithOrders()
        {
            return View();
        }

        public ActionResult HelpWithOrdersBind(int page, int pageSize, string ReportType)
        {

            int PageSize = pageSize;
            int iPageNo = page;

            string Type = ReportType == "0" ? "1" : ReportType;

            BALEmergeReview ObjReview = new BALEmergeReview();
            var HelpWithOrdersCollection = ObjReview.GetReportsForHelp(0,
                                                                        0,
                                                                        0,
                                                                        0,
                                                                        "OrderDt",
                                                                        "DESC",
                                                                         true,
                                                                          iPageNo,
                                                                           PageSize,
                                                                           string.Empty,
                                                                          string.Empty,
                                                                           string.Empty,
                                                                             0, Convert.ToByte(Type));




            return Json(new
            {
                Products = HelpWithOrdersCollection,
                //TotalCount = HelpWithOrdersCollection.ElementAt(0).TotalRec //commented by chetu
                TotalCount=HelpWithOrdersCollection.Count==0 ? null : HelpWithOrdersCollection.ElementAt(0).TotalRec //codded by chetu
            },
                    JsonRequestBehavior.AllowGet);


        }

        public ActionResult GetArchiveForHelpWithOrders(string ReportType)
        {
            string ArchiveType = string.Empty; string strArchiveLink = "Archive";
            if (ReportType == "1")
            {
                ArchiveType = "2";
                strArchiveLink = "Help With Orders";

            }
            else
            {
                ArchiveType = "1";
                strArchiveLink = "Archive";
            }
            return Json(new { Type = ArchiveType, ArchiveLink = strArchiveLink });

        }

        public ActionResult UpdateHelpWithOrders(int OrderId)
        {
            int status = 0;
            int ReviewStatus = 5;
            BALEmergeReview ObjReview = new BALEmergeReview();
            int pkOrderId = OrderId;
            status = ObjReview.UpdateHelpWithOrderStatus(pkOrderId, ReviewStatus);
            return Json(status);

        }
    }
}
