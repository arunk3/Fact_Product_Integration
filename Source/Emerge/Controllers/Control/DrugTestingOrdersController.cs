﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using System.Web.Security;


namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /DrugTestingOrders/

        public PartialViewResult PartialDrugTestingOrders()
        {
            return PartialView("PartialDrugTestingOrders");
        }

        public ActionResult DrugTestingOrdersBind(int page, int pageSize, string ReportType)
        {

            int PageSize = pageSize;
            int iPageNo = page;
            string Type = ReportType == "0" ? "6" : ReportType;
            BALOrders ObjBALOrders = new BALOrders();
            bool SavedReport6Month = false;



            MembershipUser Member = Membership.GetUser(User.Identity.Name);
            string[] UserRole = Roles.GetRolesForUser(Member.Email);
            if (UserRole.Contains("SystemAdmin"))
            {
                SavedReport6Month = true;
            }
            var ObjCollection = ObjBALOrders.GetSavedReportsMVC(0,
                                                                                                 0,
                                                                                                 0,
                                                                                                 0,
                                                                                                 "OrderDt",
                                                                                                  "DESC",
                                                                                                 true,
                                                                                                 iPageNo,
                                                                                                 PageSize,
                                                                                                 string.Empty,
                                                                                                 string.Empty,
                                                                                                 string.Empty,
                                                                                                 Convert.ToByte(Type), "AllReports", false, string.Empty, SavedReport6Month, string.Empty);/*6 for Drug Testing Products*/


            if (ObjCollection.Count != 0)
            {
                return Json(new
                {
                    Products = ObjCollection,
                    TotalCount = ObjCollection.ElementAt(0).TotalRec
                },
                         JsonRequestBehavior.AllowGet);

            }
            else
            {
                
                return Json(new
                {
                    Products = 0,
                    TotalCount = 0
                },
                                      JsonRequestBehavior.AllowGet);
           
            }

        }
        public ActionResult GetArchiveForDrugTesting(string ReportType)
        {
            string ArchiveType = string.Empty; string strArchiveLink = "Archive";
            if (ReportType == "6")
            {
                ArchiveType = "7";
                strArchiveLink = "Drug Testing Orders";

            }
            else
            {
                ArchiveType = "6";
                strArchiveLink = "Archive";
            }
            return Json(new { Type = ArchiveType, ArchiveLink = strArchiveLink });
        }


        BALDrug ObjBALDrug = null;
        public ActionResult GetDrugTestDetailsByOrderId(int OrderId, int fkLocationId)
        {
            ObjBALDrug = new BALDrug();

            List<proc_GetDrugTestingProductsByLocationIdResult> lstProducts = new List<proc_GetDrugTestingProductsByLocationIdResult>();
            List<SelectListItem> lstQuantities = new List<SelectListItem>();

            string Price = string.Empty, TotalPrice = string.Empty, PkOrderDetailId = string.Empty, ProductApplicationID = string.Empty, CurrentQuantity = string.Empty, fkCompanyId = string.Empty;string Shipingcost = string.Empty;

            if (OrderId != 0)
            {
                proc_GetDrugTestingDetailsByOrderIDResult Obj = ObjBALDrug.GetDrugTestingDetailsByOrderId(OrderId);
                if (Obj != null)
                {
                    if (fkLocationId!=0)
                    {
                        lstProducts = BindProductListOfDrugTestingKiT(fkLocationId).ToList();
                    }

                    #region Bind All Quanties according to Order

                    string sDrugTestQty = Obj.DrugTestQty.Replace(" ", "");
                    string[] arrdelim = { "," };
                    string[] arrSplit = sDrugTestQty.Split(arrdelim, StringSplitOptions.RemoveEmptyEntries);

                    lstQuantities.Clear();
                    for (int iRow = 0; iRow < arrSplit.Count(); iRow++)
                    {
                        lstQuantities.Add(new SelectListItem { Text = arrSplit[iRow], Value = arrSplit[iRow] });
                    }

                    #endregion Bind All Quanties according to Order

                    int? ddlValue = 0;

                    ddlValue =Obj.fkProductApplicationId != 0 ? Obj.fkProductApplicationId: 0;

                    Price = Obj.ReportAmount.ToString();

                    int Quantity = Convert.ToInt32(Obj.ProductQty) != 0 ? Convert.ToInt32(Obj.ProductQty) : 0;
                    CurrentQuantity = Quantity.ToString();
                    Shipingcost = Obj.AdditionalFee.ToString();
                    TotalPrice = "$" + Obj.TotalAmount.ToString();
                    PkOrderDetailId = Obj.pkOrderDetailId.ToString();
                    ProductApplicationID = Obj.fkProductApplicationId.ToString();
                    CurrentQuantity = Obj.ProductQty.ToString();
                    fkCompanyId = Obj.fkCompanyId.ToString();
                }
            }
            return Json(new { ReportAmount = Price, TotalAmount = TotalPrice, PkOrderDetailId = PkOrderDetailId, ProductApplicationID = ProductApplicationID, CurrentQuantity = CurrentQuantity, fkCompanyId = fkCompanyId, CollProducts = lstProducts, CollQuantity = lstQuantities, Shipingcost = Shipingcost });
        }
        protected List<proc_GetDrugTestingProductsByLocationIdResult> BindProductListOfDrugTestingKiT(int fkLocationID)
        {
            ObjBALDrug = new BALDrug();
            List<proc_GetDrugTestingProductsByLocationIdResult> lstProductPerApplication = new List<proc_GetDrugTestingProductsByLocationIdResult>();
            try
            {
                lstProductPerApplication = ObjBALDrug.GetDrugTestingProductsByLocationId(fkLocationID).ToList<proc_GetDrugTestingProductsByLocationIdResult>();
                lstProductPerApplication.Insert(0, new proc_GetDrugTestingProductsByLocationIdResult
                {
                    ProductDisplayName = "Select Product",
                    pkProductApplicationId = 0
                });

            }
            catch
            {
                lstProductPerApplication.Insert(0, new proc_GetDrugTestingProductsByLocationIdResult
                {
                    ProductDisplayName = "Select Product",
                    pkProductApplicationId = 0
                });
            }
            finally
            {
                ObjBALDrug = null;
            }
            return lstProductPerApplication;

        }
        public ActionResult SaveDrugTestOrderByOrderId(int pkOrderDetailId, string ddlQuantity, int ddlProductList, string txtUnitPrice, int pkOrderID, int fkCompanyId, int? chkApplyToCompanyPricing, string shipingcost)
        {
            string strResult = string.Empty;
            ObjBALDrug = new BALDrug();
            try
            {
                tblOrderDetail ObjtblOrderDetail = new tblOrderDetail();

                ObjtblOrderDetail.pkOrderDetailId = pkOrderDetailId;
                ObjtblOrderDetail.ProductQty = Convert.ToInt16(ddlQuantity);
                ObjtblOrderDetail.fkProductApplicationId = ddlProductList;
                ObjtblOrderDetail.ReportAmount = Convert.ToDecimal(txtUnitPrice);
                if (shipingcost != string.Empty)
                {
                    ObjtblOrderDetail.AdditionalFee = Convert.ToDecimal(shipingcost);
                }
                
                tblOrder ObjtblOrder = new tblOrder();
                ObjtblOrder.pkOrderId = pkOrderID;

                tblCompany_Price ObjtblCompany_Price = new tblCompany_Price();

                ObjtblCompany_Price.CompanyPrice = Convert.ToDecimal(txtUnitPrice);
                ObjtblCompany_Price.fkProductApplicationId = ddlProductList;
                ObjtblCompany_Price.fkCompanyId = fkCompanyId;
                

                bool IschkApplyToCompanyPricing = chkApplyToCompanyPricing != null ? true : false;

                int Result = ObjBALDrug.SaveDetailsForDrugTestingByOrderId(ObjtblOrderDetail, ObjtblCompany_Price, ObjtblOrder, IschkApplyToCompanyPricing);

                if (Result == 1)
                {
                    strResult = "<span class='successmsg'>Record Updated Successfully</span>";
                }
                else
                {
                    strResult = "<span class='errormsg'>Problem while saving. Please try again.</span>";

                }
            }
            catch
            {

            }
            finally
            {
                ObjBALDrug = null;
            }
            return Json(new { Message = strResult });
        }
        public ActionResult CompleteDrugTestingOrderByOrderId(int OrderId)
        {

            byte ReviewStatus = 2;
            BALEmergeReview ObjReview = new BALEmergeReview();
            int pkOrderId = OrderId;
            int result = ObjReview.UpdateDKTStatus(pkOrderId, ReviewStatus);
            return Json(new {result = result});
        }

        public ActionResult GetUnitPriceByProductName(int ProductId, int pkOrderDetailId,  int fkLocationId, int OrderId)
        {
            //string result = string.Empty;

            BALProducts ObjBALProducts = new BALProducts();
            List<SelectListItem> lstQuantities = new List<SelectListItem>();
            var listrec = ObjBALProducts.GetDrugTestQtybypkProductApplicationId(ProductId, fkLocationId);
            decimal? ReportAmount = 0;
           
            string currentqty = "1";
            if (listrec != null && listrec.Count >0)
            {

                string sDrugTestQty1 = sDrugTestQty1 = listrec.ElementAt(0).DrugTestQty.ToString();
                //if (sDrugTestQty1 != null && sDrugTestQty1 !="")
                //{
                //     sDrugTestQty1 = listrec.ElementAt(0).DrugTestQty.ToString();
                //}
                string sDrugTestQty = sDrugTestQty1.Replace(" ", ""); //list.DrugTestQty.Replace(" ", "");
                string[] arrdelim = { "," };
                string[] arrSplit = sDrugTestQty.Split(arrdelim, StringSplitOptions.RemoveEmptyEntries);
             
                lstQuantities.Clear();
                for (int iRow = 0; iRow < arrSplit.Count(); iRow++)
                {
                    currentqty = arrSplit[0];
                    lstQuantities.Add(new SelectListItem { Text = arrSplit[iRow], Value = arrSplit[iRow] });
                }


                 ReportAmount = listrec.ElementAt(0).LocationProductPrice;// list.CompanyPrice;
                

            }
            
                decimal? totalprice = ((ReportAmount) * (Convert.ToInt32(currentqty)));
           
            string TotalAmount = "$" + totalprice;
            return Json(new { ReportAmount = ReportAmount, TotalAmount = TotalAmount, CollQuantity = lstQuantities, CurrentQuantity = currentqty });

        }
        public ActionResult CancelDrugTestingOrderByOrderId(int OrderId)
        {
            //BALDrug ObjBALDrug = new BALDrug();
            //int Result = ObjBALDrug.CancelDrugTestingByOrderId(OrderId, 3);
            return Json(new { });

        }































    }
}
