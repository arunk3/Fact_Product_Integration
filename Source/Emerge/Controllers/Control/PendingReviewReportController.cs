﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using System.Web.Security;


namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /PendingReviewReport/

        public ActionResult PartialPendingReviewReport()
        {
            return View();
        }
        public ActionResult PendingReviewReportBind(int page, int pageSize, string ReportType)
        {
            int PageSize = pageSize;
            int iPageNo = page;

            string Type = ReportType == "0" ? "1" : ReportType;

            BALEmergeReview ObjReview = new BALEmergeReview();
            var PendingReviewCollection = ObjReview.GetReportsForReview(0,
                                                                                                      0,
                                                                                                      0,
                                                                                                      0,
                                                                                                     "OrderDt",
                                                                                                      "DESC",
                                                                                                      true,
                                                                                                      iPageNo,
                                                                                                      PageSize,
                                                                                                      string.Empty,
                                                                                                      string.Empty,
                                                                                                      string.Empty,
                                                                                                      0, Convert.ToByte(Type));
            return Json(new
            {
                Products = PendingReviewCollection,
                //TotalCount = PendingReviewCollection.ElementAt(0).TotalRec //commented by chetu

                TotalCount = PendingReviewCollection.Count == 0 ? null : PendingReviewCollection.ElementAt(0).TotalRec //coded by chetu
            },
                                        JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetArchiveForEmergeReview(string ReportType)
        {
            string ArchiveType = string.Empty; string strArchiveLink = "Archive";
            if (ReportType == "1")
            {
                ArchiveType = "2";
                strArchiveLink = "Pending Reviews";

            }
            else
            {
                ArchiveType = "1";
                strArchiveLink = "Archive";
            }
            return Json(new { Type = ArchiveType, ArchiveLink = strArchiveLink });
        }
        public ActionResult UpdateReviewReport(int OrderId)
        {
            int status = 0;
            int ReviewStatus = 5;
            BALEmergeReview ObjReview = new BALEmergeReview();
            int pkOrderId = OrderId;
            status = ObjReview.UpdateReportReviewStatus(pkOrderId, ReviewStatus);
            return Json(status);
        
        }
    }
}
