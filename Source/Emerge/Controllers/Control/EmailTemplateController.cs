﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using Emerge.Models;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        public ActionResult EmailTemplate()
        {
            EmailSystemTemplatesModel ObjModel = new EmailSystemTemplatesModel();

            //List<Proc_USA_LoadEmailTemplatesResult> ObjEmailTemplate = GetEmailTemplates(0, Utility.SiteApplicationId, false, 0);

            ObjModel.EmailTemplatesList = GetEmailTemplates(0, Utility.SiteApplicationId, false, 0);

            List<SelectListItem> TemplateType = new List<SelectListItem>();
            TemplateType.Insert(0, new SelectListItem { Text = "--Select--", Value = string.Empty });
            TemplateType.Insert(1, new SelectListItem { Text = "Newsletter", Value = "2" });
            TemplateType.Insert(2, new SelectListItem { Text = "Notice", Value = "1" });
            TemplateType.Insert(3, new SelectListItem { Text = "Other", Value = "0" });
           // ViewData["TemplateType"] = TemplateType;ddlTemplateType
            ObjModel.ddlTemplateType = TemplateType;
            return View(ObjModel);
        }


        public List<Proc_USA_LoadEmailTemplatesResult> GetEmailTemplates(int TemplateId, Guid ApplicationId, bool IsSystemTemplate, int TemplateCode)
        {
            BALEmailSystemTemplates ObjBALEmailSystemTemplates = new BALEmailSystemTemplates();
            List<Proc_USA_LoadEmailTemplatesResult> ObjEmailTemplate = new List<Proc_USA_LoadEmailTemplatesResult>();

            ObjEmailTemplate = ObjBALEmailSystemTemplates.GetEmailTemplates(TemplateId, ApplicationId, IsSystemTemplate, TemplateCode);

            if (IsSystemTemplate == false)
            {
                ObjEmailTemplate.Insert(0, new Proc_USA_LoadEmailTemplatesResult
                {
                    pkTemplateId = -1,
                    TemplateSubject = "Add New Template",
                    TemplateContent = string.Empty,
                    TemplateName = string.Empty
                });
            }
            if (IsSystemTemplate == true)
            {
                ObjEmailTemplate.Insert(0, new Proc_USA_LoadEmailTemplatesResult
                {
                    pkTemplateId = 0,
                    TemplateName = "<--Select Template-->",
                    TemplateSubject = string.Empty,
                    TemplateContent = string.Empty

                });
            }


            return ObjEmailTemplate;
        }
        public ActionResult GetTemplateContents(string pkTemplateId, string IsSystemTemplate)
        {

            BALEmailSystemTemplates ObjTemplate = new BALEmailSystemTemplates();
            List<Proc_USA_LoadEmailTemplatesResult> ObjRecords = ObjTemplate.GetEmailTemplates(Convert.ToInt32(pkTemplateId), Utility.SiteApplicationId, Convert.ToBoolean(IsSystemTemplate), 0);
            return Json(new
            {
                TempName = ObjRecords.ElementAt(0).TemplateName,
                TempSub = ObjRecords.ElementAt(0).TemplateSubject,
                TempCont = ObjRecords.ElementAt(0).TemplateContent,
                IsTemplateEnabled = ObjRecords.ElementAt(0).IsEnabled,
                DefaultContent = ObjRecords.ElementAt(0).DefaultTemplateContent,
                TemplateType = ObjRecords.ElementAt(0).TemplateType
            });
        }
        public ActionResult DeleteTemplateById(string TemplateId)
        {
            BALEmailSystemTemplates ObjTemplate = new BALEmailSystemTemplates();
            string strMessage = "";
            List<Proc_USA_LoadEmailTemplatesResult> ObjEmailTemplate = new List<Proc_USA_LoadEmailTemplatesResult>();
            try
            {
                int pkTemplateId = Convert.ToInt32(TemplateId);
                if (pkTemplateId > 0)
                {
                    int iOutput = ObjTemplate.DeleteEmailTemplate(pkTemplateId);
                    if (iOutput >= 0)
                    {
                        strMessage = "<span class='successmsg'>Template deleted successfully</span>";
                        ObjEmailTemplate = GetEmailTemplates(0, Utility.SiteApplicationId, false, 0);
                    }
                    else
                    {
                        strMessage = "<span class='errormsg'>Some error occur, please try later</span>";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjTemplate = null;
            }
            return Json(new { DeleteMessage = strMessage, Templates = ObjEmailTemplate });
        }
        string IsAddUpdate = string.Empty;

        public ActionResult AddUpdateTemplate(EmailSystemTemplatesModel ObjModel, string hdEmailTemplate)
        {
            string strResult = "";
            List<Proc_USA_LoadEmailTemplatesResult> ObjEmailTemplate = new List<Proc_USA_LoadEmailTemplatesResult>();
            if (ObjModel != null)
            {
                if (!string.IsNullOrEmpty(hdEmailTemplate))
                {
                    if (ObjModel.ddlTemplateSubject != "-1")
                    {
                        strResult = UpdateEmailTemplate(ObjModel);
                    }
                    else
                    {
                        strResult = AddTemplate(ObjModel);
                    }
                    ObjEmailTemplate = GetEmailTemplates(0, Utility.SiteApplicationId, false, 0);
                }
                else
                {
                    if (ObjModel.ddlTemplateName != "0")
                    {
                        strResult = UpdateSystemTemplate(ObjModel);
                    }
                    else
                    {

                    }
                    ObjEmailTemplate = GetEmailTemplates(0, Utility.SiteApplicationId, true, 0);
                }


            }
            return Json(new { Message = strResult, Templates = ObjEmailTemplate, StatusAddUpdate = IsAddUpdate });

        }
        public string UpdateEmailTemplate(EmailSystemTemplatesModel ObjModel)
        {
            string strResult = "";
            BALEmailSystemTemplates ObjEmailTemplate = new BALEmailSystemTemplates();
            try
            {
                tblEmailTemplate ObjTemplate = new tblEmailTemplate();
                ObjTemplate.pkTemplateId = Convert.ToInt32(ObjModel.ddlTemplateSubject);
                ObjTemplate.fkApplicationId = Utility.SiteApplicationId;
                ObjTemplate.TemplateName = ObjModel.TemplateName;
                ObjTemplate.TemplateSubject = ObjModel.TemplateSubject;
                ObjTemplate.TemplateContent = Server.HtmlDecode(ObjModel.TemplateContent);
                ObjTemplate.LastModifiedDate = DateTime.Now;
                ObjTemplate.TemplateType = ObjModel.TemplateType;


                int iResult = ObjEmailTemplate.UpdateEmailTemplate(ObjTemplate);
                if (iResult >= 0)
                {
                    strResult = "<span class='successmsg'>Template updated successfully</span>";
                    IsAddUpdate = "Update";
                }
                else if (iResult == -1)
                {
                    IsAddUpdate = string.Empty;
                    strResult = "<span class='errormsg'>Template Name already exists</span>";
                }
                else
                {
                    IsAddUpdate = string.Empty;
                    strResult = "<span class='errormsg'>Some error occur, please try later</span>";
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strResult;
        }
        public string AddTemplate(EmailSystemTemplatesModel ObjModel)
        {
            string strResult = "";
            BALEmailSystemTemplates ObjEmailTemplate = new BALEmailSystemTemplates();
            try
            {
                tblEmailTemplate ObjTemplate = new tblEmailTemplate();

                ObjTemplate.fkApplicationId = Utility.SiteApplicationId;

                ObjTemplate.TemplateName = ObjModel.TemplateName.Trim();
                ObjTemplate.TemplateSubject = ObjModel.TemplateSubject.Trim();
                ObjTemplate.TemplateContent = ObjModel.TemplateContent.Trim() != null ? ObjModel.TemplateContent.Trim() : string.Empty;

                ObjTemplate.DefaultTemplateContent = ObjModel.TemplateContent.Trim() != null ? ObjModel.TemplateContent.Trim() : string.Empty;
                ObjTemplate.IsSystemTemplate = false;
                ObjTemplate.CreatedDate = DateTime.Now;
                ObjTemplate.TemplateCode = 0;
                int iResult = ObjEmailTemplate.InsertEmailTemplate(ObjTemplate);
                if (iResult > 0)
                {
                    IsAddUpdate = "Add";
                    strResult = "<span class='successmsg'>Template added succesfully</span>";
                }
                else if (iResult == -1)
                {
                    IsAddUpdate = string.Empty;
                    strResult = "<span class='errormsg'>Template Name already exists</span>";
                }
                else
                {
                    IsAddUpdate = string.Empty;
                    strResult = "<span class='errormsg'>Some error occur, please try later</span>";
                }
            }
            catch //(Exception ex)
            {
            }
            return strResult;
        }


        public ActionResult SystemTemplates()
        {
            EmailSystemTemplatesModel ObjModel = new EmailSystemTemplatesModel();
            // List<Proc_USA_LoadEmailTemplatesResult> ObjEmailTemplate = GetEmailTemplates(0, Utility.SiteApplicationId, true, 0);

            ObjModel.SystemTemplatesList = GetEmailTemplates(0, Utility.SiteApplicationId, true, 0).Where(d => d.pkTemplateId != Convert.ToInt32(EmailTemplates.ReportsNotGenerating)).ToList();
            return View(ObjModel);
        }

        public ActionResult NewSystemTemplates()
        {
          

           EmailSystemTemplatesModel ObjModel = new EmailSystemTemplatesModel();
           BALEmailSystemTemplates ObjBALEmailSystemTemplates = new BALEmailSystemTemplates();
           if (Request.QueryString["Status"] != null)
           {
               string status = Request.QueryString["Status"];


               if (status == "done")
                {
                    ObjModel.Message = "<span class='successmsg'>Template updated successfully</span>";
                }
               else if (status == "undo")
                {
                    ObjModel.Message = "<span class='successmsg'>error is occured while updating content</span>";
                }
           }
            


           ObjModel.SystemTemplatesList = ObjBALEmailSystemTemplates.GetEmailTemplates(0, Utility.SiteApplicationId, true, 0).Where(d => d.pkTemplateId != Convert.ToInt32(EmailTemplates.ReportsNotGenerating)).Select(p => new Proc_USA_LoadEmailTemplatesResult
           {
            TemplateContent= HttpUtility.HtmlDecode(p.TemplateContent),
            IsSystemTemplate=p.IsSystemTemplate,
            IsNotificationTemplate=p.IsNotificationTemplate,
            pkTemplateId=p.pkTemplateId,
            TemplateCode=p.TemplateCode,
            CreatedDate=p.CreatedDate,
            DefaultTemplateContent=p.DefaultTemplateContent,
            IsEnabled=p.IsEnabled,
            fkApplicationId=p.fkApplicationId,
            LastModifiedDate=p.LastModifiedDate,
            TemplateSubject=p.TemplateSubject,
            TemplateName=p.TemplateName,
            TemplateType=p.TemplateType
             
           }).ToList();
           //var list = ObjBALEmailSystemTemplates.GetEmailTemplates(0, Utility.SiteApplicationId, true, 0).Where(d => d.pkTemplateId != Convert.ToInt32(EmailTemplates.ReportsNotGenerating)).ToList();
           

            return View(ObjModel);
        }


      

        [HttpPost]
        public ActionResult UpdateNewSystemTemplate(FormCollection ObjCollection)
        {
            string strSuccess = string.Empty;

            string pkTemplateId = Convert.ToString(ObjCollection.GetValue("pkTemplateId").AttemptedValue);
            string TemplateTitle = Convert.ToString(ObjCollection.GetValue("TemplateTitle").AttemptedValue);
            string TemplateHeader = Convert.ToString(ObjCollection.GetValue("TemplateHeader").AttemptedValue);
            string TemplateBodyMessage = Convert.ToString(ObjCollection.GetValue("TemplateBodyMessage").AttemptedValue);
            bool IsMyPreference;
            if ((ObjCollection["IsNotificationTemplate_" + pkTemplateId]) != null && (ObjCollection["IsNotificationTemplate_" + pkTemplateId]).ToLower() != "no")
            {
                IsMyPreference = true;
               
            }
            else
            {
                IsMyPreference = false;
                
            }

           
            //string Role = Convert.ToString(ObjCollection.GetValue("Role").AttemptedValue);

           // string strResult = "";
            BALEmailSystemTemplates ObjEmailTemplate = new BALEmailSystemTemplates();
            try
            {
                tblEmailTemplate ObjTemplate = new tblEmailTemplate();
                ObjTemplate.pkTemplateId = Convert.ToInt32(pkTemplateId);
                ObjTemplate.fkApplicationId = Utility.SiteApplicationId;
                ObjTemplate.TemplateName = TemplateTitle;
                ObjTemplate.TemplateSubject = TemplateHeader;
                ObjTemplate.TemplateContent =HttpUtility.HtmlDecode(TemplateBodyMessage);
                ObjTemplate.LastModifiedDate = DateTime.Now;
                ObjTemplate.IsNotificationTemplate = Convert.ToBoolean(IsMyPreference);

                  int iResult = ObjEmailTemplate.UpdateEmailTemplateNew(ObjTemplate);
              //  int iResult = 0;
                if (iResult == 1)
                {
                    //strResult = "<span class='successmsg'>Template updated successfully</span>";
                    IsAddUpdate = "Update";
                    strSuccess = "done";
                }
                else if (iResult == -1)
                {
                    IsAddUpdate = string.Empty;
                    strSuccess = "undo";
                    //strResult = "<span class='errormsg'>Template Name already exists</span>";
                }
                
            }
            catch (Exception ex)
            {

                throw ex;
            }



            return RedirectToAction("NewSystemTemplates", new { Status = strSuccess });
        }

        public string UpdateSystemTemplate(EmailSystemTemplatesModel ObjModel)
        {
            string strResult = "";
            BALEmailSystemTemplates ObjEmailTemplate = new BALEmailSystemTemplates();
            try
            {
                tblEmailTemplate ObjTemplate = new tblEmailTemplate();
                ObjTemplate.pkTemplateId = Convert.ToInt32(ObjModel.ddlTemplateName);
                ObjTemplate.fkApplicationId = Utility.SiteApplicationId;
                ObjTemplate.TemplateName = ObjModel.TemplateName;
                ObjTemplate.TemplateSubject = ObjModel.TemplateSubject;
                ObjTemplate.TemplateContent = Server.HtmlDecode(ObjModel.TemplateContent);
                ObjTemplate.LastModifiedDate = DateTime.Now;
                ObjTemplate.IsEnabled = ObjModel.IsEnabled;

                int iResult = ObjEmailTemplate.UpdateSystemTemplate(ObjTemplate);
                if (iResult >= 0)
                {
                    strResult = "<span class='successmsg'>Template updated succesfully</span>";
                    IsAddUpdate = "Update";
                }
                else
                {
                    IsAddUpdate = string.Empty;
                    strResult = "<span class='errormsg'>Template Name already exists</span>";
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strResult;
        }
    }
}
