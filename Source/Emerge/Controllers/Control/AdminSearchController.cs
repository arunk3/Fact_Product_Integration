﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using System.Web.Security;
using System.IO;



namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
       
        //
        // GET: /AdminSearch/
        [ValidateInput(false)]
       //[RequireHttps]
        public ActionResult AdminSearch()
        {
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            MembershipUser objMembershipUser = Membership.GetUser();
             var UserName = objMembershipUser.UserName;
             if (Roles.IsUserInRole(UserName, "SystemAdmin"))//Roles.IsUserInRole(UserName, "Admin") || 
             {
               
                 ViewBag.NewAccountContetns = "expand";
                 ViewBag.NoticeContent = "expand";
                 ViewBag.WatchListContent = "expand";
                 ViewBag.PendingReviewReportContent = "expand";
                 ViewBag.MannualReportContent = "expand";
                 ViewBag.QualityContent = "expand";
                 ViewBag.HelpWithOrdersContent = "expand";
                 ViewBag.PendingReportsContent = "expand";
                 ViewBag.IncompleteReportsContent = "expand";
                 ViewBag.DrugTestingOrdersContent = "expand";
                 ViewBag.BatchOrdersContent = "expand";
                 
             }
             else
             {
                 List<tblDashboardList> Records = ObjBALCompanyUsers.GetDashBoardStatusByUserId(Guid.Parse(objMembershipUser.ProviderUserKey.ToString()));
                 bool NewAccountContetnsDiv = Records.Where(rec=>rec.DivName == "NewAccountContetnsDiv" ).ElementAt(0).IsDivOpen;
                 if (NewAccountContetnsDiv == true)
                 {
                     ViewBag.NewAccountContetns = "expand";
                 }
                 else
                 {
                     ViewBag.NewAccountContetns = "colapse";
                 }


                 bool NoticeContentDiv = Records.Where(rec => rec.DivName == "613NoticeContentDiv").ElementAt(0).IsDivOpen;
                 if (NoticeContentDiv == true)
                 {
                     ViewBag.NoticeContent = "expand";
                 }
                 else
                 {
                     ViewBag.NoticeContentDiv = "colapse";
                 }


                 bool WatchListContentDiv = Records.Where(rec => rec.DivName == "WatchListContentDiv").ElementAt(0).IsDivOpen;
                 if (WatchListContentDiv == true)
                 {
                     ViewBag.WatchListContent = "expand";
                 }
                 else
                 {
                     ViewBag.WatchListContent = "colapse";
                 }


                 bool PendingReviewReportContentDiv = Records.Where(rec => rec.DivName == "PendingReviewReportContentDiv").ElementAt(0).IsDivOpen;
                 if (PendingReviewReportContentDiv == true)
                 {
                     ViewBag.PendingReviewReportContent = "expand";
                 }
                 else
                 {
                     ViewBag.PendingReviewReportContent = "colapse";
                 }


                 bool MannualReportContentDiv = Records.Where(rec => rec.DivName == "MannualReportContentDiv").ElementAt(0).IsDivOpen;
                 if (MannualReportContentDiv == true)
                 {
                     ViewBag.MannualReportContent = "expand";
                 }
                 else
                 {
                     ViewBag.MannualReportContent = "colapse";
                 }

                 bool QualityContentDiv = Records.Where(rec => rec.DivName == "QualityContentDiv").ElementAt(0).IsDivOpen;
                 if (QualityContentDiv == true)
                 {
                     ViewBag.QualityContent = "expand";
                 }
                 else
                 {
                     ViewBag.QualityContent = "colapse";
                 }

                 bool HelpWithOrdersContentDiv = Records.Where(rec => rec.DivName == "HelpWithOrdersContentDiv").ElementAt(0).IsDivOpen;
                 if (HelpWithOrdersContentDiv == true)
                 {
                     ViewBag.HelpWithOrdersContent = "expand";
                 }
                 else
                 {
                     ViewBag.HelpWithOrdersContent = "colapse";
                 }
                 var  IsexisitContentDiv = Records.Where(rec => rec.DivName == "BatchOrdersContentDiv").Count();
              
                 if (IsexisitContentDiv > 0)
                 {
                     bool BatchOrdersContentDiv = Records.Where(rec => rec.DivName == "BatchOrdersContentDiv").ElementAt(0).IsDivOpen;
                     if (BatchOrdersContentDiv == true)
                     {
                         ViewBag.BatchOrdersContent = "expand";
                     }
                     else
                     {
                         ViewBag.BatchOrdersContent = "colapse";
                     }
                 }
                 else
                 {
                     ViewBag.BatchOrdersContent = "expand";
 
                 }

                 bool PendingReportsContentDiv = Records.Where(rec => rec.DivName == "PendingReportsContentDiv").ElementAt(0).IsDivOpen;
                 if (PendingReportsContentDiv == true)
                 {
                     ViewBag.PendingReportsContent = "expand";
                 }
                 else
                 {
                     ViewBag.PendingReportsContent = "colapse";
                 }

                 bool IncompleteReportsContentDiv = Records.Where(rec => rec.DivName == "IncompleteReportsContentDiv").ElementAt(0).IsDivOpen;
                 if (IncompleteReportsContentDiv == true)
                 {
                     ViewBag.IncompleteReportsContent = "expand";
                 }
                 else
                 {
                     ViewBag.IncompleteReportsContent = "colapse";
                 }
                 bool DrugTestingOrdersContentDiv = Records.Where(rec => rec.DivName == "DrugTestingOrdersContentDiv").ElementAt(0).IsDivOpen;
                 if (DrugTestingOrdersContentDiv == true)
                 {
                     ViewBag.DrugTestingOrdersContent = "expand";
                 }
                 else
                 {
                     ViewBag.DrugTestingOrdersContent = "colapse";
                 }

             }

            
            AdminSearchModel ObjAdminSearchModel = new AdminSearchModel();
            if (TempData["NewAccounts"] != null)
            {
                if (TempData["message"] != null)
                {
                    ObjAdminSearchModel.strResultNewAccounts = TempData["message"].ToString();
                }
            }
            if (TempData["AdminConsent"] != null)
            {
                if (TempData["messages"] != null)
                {
                    ObjAdminSearchModel.strResultAdminConsent = TempData["messages"].ToString();
                }
            }
            ObjAdminSearchModel.DrugTestingReportType = 6;
            ObjAdminSearchModel.lnkDrugTesting = "Archive";

            ObjAdminSearchModel.HelpWithOrdersReportType = 1;
            ObjAdminSearchModel.lnkHelpWithOrders = "Archive";

            ObjAdminSearchModel.EmergeReviewReportType = 1;
            ObjAdminSearchModel.lnkEmergeReview = "Archive";

            ObjAdminSearchModel.AdminConsentReportType = 1;
            ObjAdminSearchModel.lnkAdminConsent = "Archive";

            ObjAdminSearchModel.CollTemplateContents = GetTemplateContentsById(Convert.ToInt32(EmailTemplates.ReportsNotGenerating), true);
            if (ObjAdminSearchModel.CollTemplateContents != null)
            {
             
                        ObjAdminSearchModel.SystemNotificationTitle = ObjAdminSearchModel.CollTemplateContents.ElementAt(0).TemplateSubject;
                        ObjAdminSearchModel.SystemNotificationContent = ObjAdminSearchModel.CollTemplateContents.ElementAt(0).TemplateContent;
                        ObjAdminSearchModel.SystemNotificationStatus = ObjAdminSearchModel.CollTemplateContents.ElementAt(0).IsEnabled;
                  

            }

            return View(ObjAdminSearchModel);
        }

        public ActionResult SaveSystemNotificationDetails(string Title, string Details)
        {
            string strSaveResult = string.Empty;

            BALEmailSystemTemplates ObjBALEmailSystemTemplates = new BALEmailSystemTemplates();
            try
            {
                int result = ObjBALEmailSystemTemplates.UpdateSystemNotification(Convert.ToInt32(EmailTemplates.ReportsNotGenerating), Title, Details, true);
                if (result == 1)
                {
                    strSaveResult = "<span class='successmsg'>Notification Updated Succesfully</span>";
                }
                else
                {
                    strSaveResult = "<span class='errormsg'>Error occurred while saving. Please try again!</span>";
                }
            }
            catch
            {
            }
            finally
            {
                ObjBALEmailSystemTemplates = null;
            }
            //List<Proc_USA_LoadEmailTemplatesResult> Obj = GetTemplateContentsById(Convert.ToInt32(EmailTemplates.ReportsNotGenerating), true);
            //if (Obj != null)
            //{
            // string TempCont = Obj.ElementAt(0).TemplateContent;

            //   string updatedContent = TempCont.Replace("%%NotificationSubject%%", Title).Replace("%%NotificationDetails%%", Details);
            //}

            return Json(new { Message = strSaveResult });
        }
        public ActionResult ClearSystemNotificationDetails()
        {
            string strSaveResult = string.Empty;
            BALEmailSystemTemplates ObjBALEmailSystemTemplates = new BALEmailSystemTemplates();
            try
            {
                int result = ObjBALEmailSystemTemplates.UpdateSystemNotificationStatus(Convert.ToInt32(EmailTemplates.ReportsNotGenerating), false);
                if (result == 1)
                {
                    strSaveResult = "<span class='successmsg'>Notification Updated Succesfully</span>";
                    
                }
                else
                {
                   
                    strSaveResult = "<span class='errormsg'>Error occurred while saving. Please try again!</span>";
                }
            }
            catch
            {
            }
            finally
            {
                ObjBALEmailSystemTemplates = null;
            }
            return Json(new { Message = strSaveResult });
        }

        public List<Proc_USA_LoadEmailTemplatesResult> GetTemplateContentsById(int pkTemplateId, bool IsSystemTemplate)
        {
            BALEmailSystemTemplates ObjTemplate = new BALEmailSystemTemplates();
            List<Proc_USA_LoadEmailTemplatesResult> ObjRecords = ObjTemplate.GetEmailTemplates(pkTemplateId, Utility.SiteApplicationId, IsSystemTemplate, 0);
            return ObjRecords;
        }

        public ActionResult GetSearchedLocations(int page, int pageSize, string Searchstring, List<GridFilter> filters)
        {

            BALCompany objbal = new BALCompany();
            int TotalRec = 0;

            if (filters != null)
            {
                Searchstring = filters[0].Value ?? string.Empty;
            }
            else
            {
                Searchstring = string.Empty;
            }
            var list = objbal.GetAllLocationsForAdmin(page, pageSize, false, 0, -1, 0, "City", "ASC", "City", 1, Searchstring, out TotalRec);
            if (list.Count == 0)
            {
                return Json(new { Reclist = 0, TotalRec = 0 });
            }
            else
            {
                return Json(new { Reclist = list, TotalRec = list.ElementAt(0).TotalRec });
            }

        }

        public ActionResult GetSearchedCompanies(int page, int pageSize, string Searchstring, List<GridFilter> filters)
        {

            BALCompany objbal = new BALCompany();
            int TotalRec = 0;

            if (filters != null)
            {
                Searchstring = filters[0].Value ?? string.Empty;
            }
            else
            {
                Searchstring = string.Empty;
            }
            var list = objbal.GetAllCompanyForAdmin(page, pageSize, false, -1, "SelectStatus", "CompanyName", "ASC", "CompanyName", 1, Searchstring, out TotalRec);
            if (list.Count == 0)
            {
                return Json(new { Reclist = 0, TotalRec = 0 });
            }
            else
            {
                return Json(new { Reclist = list, TotalRec = list.ElementAt(0).TotalRec });
            }

        }

        public ActionResult GetSearchedUsers(int page, int pageSize, string Searchstring, List<GridFilter> filters)
        {

            BALCompany objbal = new BALCompany();
            int TotalRec = 0;

            if (filters != null)
            {
                Searchstring = filters[0].Value ?? string.Empty;
            }
            else
            {
                Searchstring = string.Empty;
            }
            var list = objbal.GetAllEmergeUsersForAdmin(page, pageSize, false, -1, 0, 0, "FirstName", "ASC", "UserName", 1, Searchstring, out TotalRec);
            if (list.Count == 0)
            {
                return Json(new { Reclist = 0, TotalRec = 0 });
            }
            else
            {
                return Json(new { Reclist = list, TotalRec = list.ElementAt(0).TotalRec });
            }

        }
     

    }
}
