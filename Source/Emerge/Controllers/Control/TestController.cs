﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using System.Web.Security;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /Test/

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult testadminPendingReportsForAdmin(int skip, int take, int page, int pageSize, string group)
        {

            int PageSize = pageSize;
            int iPageNo = page;

            BALOrders ObjBALOrders = new BALOrders();

            var PendingReportsForAdminBindCollection = ObjBALOrders.GetSavedReports(0,
                                                                                            0,
                                                                                            0,
                                                                                            0,
                                                                                            "OrderDt",
                                                                                            "DESC",
                                                                                            true,
                                                                                            iPageNo,
                                                                                            PageSize,
                                                                                            string.Empty,
                                                                                            string.Empty,
                                                                                            string.Empty,
                                                                                            1, "AllReports");



            return Json(new
            {
                Products = PendingReportsForAdminBindCollection,
                TotalCount = PendingReportsForAdminBindCollection.ElementAt(0).TotalRec
            },
                  JsonRequestBehavior.AllowGet);




        }















    }
}
