﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using System.Web.Security;
using Emerge.Common;
using Emerge.Services;
using Emerge.Data;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /AddEditUser/
        BALCompanyUsers ObjBALCompanyUsers = null;

        private int CompanyUserId = 0;
        public bool _ISAdmin;
        public bool ISAdmin
        {
            get
            {
                return _ISAdmin;
            }
            set
            {
                value = _ISAdmin;
            }
        }
        int NewPkCompanyId = 0;
        int NewCompanyUserId = 0;

        [ValidateInput(false)]
        public ActionResult AddCompanyUser()
        {
            UsersModel ObjUserModel = new UsersModel();
            ObjBALCompany = new BALCompany();
            var collection = ObjBALCompany.GetAllCompany(Utility.SiteApplicationId, true, false);
            var CompanyList = new List<SelectListItem>();
            CompanyList.Insert(0, new SelectListItem { Text = "Select", Value = "" });
            if (collection.Count > 0)
            {
                for (int item = 0; item < collection.Count; item++)
                {
                    CompanyList.Add(new SelectListItem { Text = collection.ElementAt(item).CompanyName, Value = collection.ElementAt(item).pkCompanyId.ToString() });
                }
            }

            ObjUserModel.ListCompany_Id = CompanyList;



            Session["pklocationid"] = 0;
            ObjUserModel.IsAdmin = false;
            ObjUserModel.IsEdit = false;
            ObjUserModel.IsLiveRunnerEnabled = true;
            if (TempData["message"] != null)
            {
                ObjUserModel.strResult = TempData["message"].ToString();
            }
            MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));
            string[] multiple_Roles = Roles.GetRolesForUser(ObjMembershipUser.UserName);

            if (multiple_Roles.Count() == 2)
            {
                foreach (string rol in multiple_Roles)
                {
                    if (rol.ToLower() != "corporatemanager")
                    {
                        ObjUserModel.CurrentLoginUserRoleName = rol;
                    }
                }
            }
            else
            {
                ObjUserModel.CurrentLoginUserRoleName = multiple_Roles[0].ToString();
            }
            string[] ObjRoles = Roles.GetRolesForUser(ObjMembershipUser.UserName);

            if (!ObjRoles.Contains("SupportTechnician") && !ObjRoles.Contains("SupportManager"))
            {
                ObjUserModel.IsAdmin = true;
            }
            if (!string.IsNullOrEmpty(Request.QueryString["_c"]))         //  _c Contains Company ID
            {

                int CompanyId = Convert.ToInt32(Request.QueryString["_c"].ToString());
                ObjUserModel.QueryStringCompanyId = Request.QueryString["_c"].ToString();
                ObjUserModel.PkCompanyId = CompanyId;

                tblCompany ObjtblCompany = ObjBALCompany.GetCompanyByCompanyId(CompanyId);
                if (ObjtblCompany != null)
                {
                    ObjUserModel.SelectedCompanyName = ObjtblCompany.CompanyName;
                }
                else
                {
                    ObjUserModel.SelectedCompanyName = string.Empty;
                }
            }

            if (!string.IsNullOrEmpty(Request.QueryString["_a"]))         //  _a Contains PkUser ID
            {
                ObjUserModel.IsEdit = true;
                ObjBALCompanyUsers = new BALCompanyUsers();


                int ObjGuid = Convert.ToInt16(Request.QueryString["_a"]);
                ViewBag.UserStatus = ObjGuid.ToString();//INT210
                Session["ObjGuid"] = ObjGuid;



                ObjUserModel.QueryStringUserId = ObjGuid.ToString();

                TempData["TaleoWebApiObjGuid"] = ObjGuid; //nd-16 changes 

                proc_Get_CompanyUser_ByCompanyUserIdResult ObjtblCompanyUser = ObjBALCompanyUsers.SelectCompanyUserByCompanyUserId(ObjGuid);
                Session["pklocationid"] = ObjtblCompanyUser.fkLocationId;
                if (ObjtblCompanyUser != null)
                {
                    BindFields(ObjtblCompanyUser, ObjUserModel);
                }
            }
            else
            {
                ViewBag.UserStatus = "";//INT210
                Session["ObjGuid"] = Guid.Empty;
                if (multiple_Roles.Contains("SystemAdmin"))
                {
                    ISAdmin = false;
                }
                else
                {
                    ISAdmin = false;
                }
            }

            //  GetCompanyUserId();
            //  ObjUserModel.IsEnabled = true;
            ObjUserModel.objtblLocation = null;
            return View(ObjUserModel);
        }
        public ActionResult BindLocationsByCompanyId(int CompanyId, bool IsHome, int PkCompanyUserId)
        {
            //UsersModel objModel = new UsersModel();
            BALLocation ObjBALLocation = new BALLocation();
            List<tblLocation> lstLocations = new List<tblLocation>();
            List<tblUsersLocation> listtblCompanyOtherLocation = new List<tblUsersLocation>();
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            int? HomefkLocationId = null;
            proc_Get_CompanyUser_ByCompanyUserIdResult ObjtblCompanyUser = ObjBALCompanyUsers.SelectCompanyUserByCompanyUserId(PkCompanyUserId);
            if (ObjtblCompanyUser != null)
            {//for Remove Selected Home Location From Oher Location List(DropDown)

                HomefkLocationId = ObjtblCompanyUser.fkLocationId;
            }
            string Content = string.Empty;
            try
            {
                if (IsHome == true || IsHome.ToString() == "True" || CompanyId == 0)
                {
                    // for New Add Company Show All Locations
                    if (CompanyId == 0)
                    {
                        CompanyId = 0;

                    }
                    lstLocations = ObjBALLocation.GetLocationsByCompanyId((CompanyId)).Select(p => new tblLocation
                    {
                        pkLocationId = p.pkLocationId,
                        City = p.City

                    }).ToList();
                    Content = "<select  onchange='GetSelectedValues();' onclick='RemoveHomeLocation()' name='user_id12' class='divisions_dd'  id='OtherLocationId'  multiple='multiple' style='color:Lime'>";
                    for (int i = 0; i < lstLocations.Count; i++)
                    {
                        Content += "<option value='" + lstLocations.ElementAt(i).pkLocationId + "'>'" + lstLocations.ElementAt(i).City + "'</option>";
                    }
                    Content += "</select>";
                }
                else
                {

                    lstLocations = ObjBALLocation.GetLocationsByCompanyId(CompanyId).Select(p => new tblLocation
                    {

                        pkLocationId = p.pkLocationId,
                        City = p.City

                    }).ToList();


                    // Get List From tblOtherLocation
                    listtblCompanyOtherLocation = ObjBALLocation.GetLocationsByPkCompanyUserId(PkCompanyUserId);
                    if (listtblCompanyOtherLocation.Count != 0)
                    {

                        Content = "<select  onchange='GetSelectedValues();' onclick='RemoveHomeLocation()' name='user_id12' class='divisions_dd'  id='OtherLocationId'  multiple='multiple' style='color:Lime'>";
                        for (int i = 0; i < lstLocations.Count; i++)
                        {
                            if (lstLocations.ElementAt(i).pkLocationId != HomefkLocationId.GetValueOrDefault())
                            {

                                var tblCompanyOther = listtblCompanyOtherLocation.Where(item => item.fklocationId == lstLocations.ElementAt(i).pkLocationId).FirstOrDefault(); //if (listtblCompanyOtherLocation.ElementAt(i).IsHome == false)
                                if (tblCompanyOther != null)
                                {

                                    if (tblCompanyOther.IsHome == false)
                                    {// for Unchecked Location
                                        Content += "<option value='" + lstLocations.ElementAt(i).pkLocationId + "'  >'" + lstLocations.ElementAt(i).City + "'</option>";
                                    }
                                    else
                                    {// for checked Location
                                        Content += "<option value='" + lstLocations.ElementAt(i).pkLocationId + "'selected='selected' >'" + lstLocations.ElementAt(i).City + "'</option>";
                                    }
                                }
                                else
                                {// for Unchecked Location
                                    Content += "<option value='" + lstLocations.ElementAt(i).pkLocationId + "' >'" + lstLocations.ElementAt(i).City + "'</option>";
                                }
                            }

                        }
                        Content += "</select>";
                    }
                    else
                    {
                        Content = "<select  onchange='GetSelectedValues();'  onclick='RemoveHomeLocation()' name='user_id12' class='divisions_dd'  id='OtherLocationId'  multiple='multiple' style='color:Lime'>";
                        for (int i = 0; i < lstLocations.Count; i++)
                        {
                            if (lstLocations.ElementAt(i).pkLocationId != HomefkLocationId.GetValueOrDefault())
                            {
                                Content += "<option value='" + lstLocations.ElementAt(i).pkLocationId + "'>'" + lstLocations.ElementAt(i).City + "'</option>";
                            }
                        }
                        Content += "</select>";
                    }


                }



            }
            catch (Exception)
            {
                //throw;
            }
            finally
            {
                ObjBALLocation = null;
            }
            return Json(new { ResultList = Content });
        }


        public Proc_Get_CompanyDetailByLocationIdResult GetCompanyDetailsByLocationId(int PkLocationId)
        {
            Proc_Get_CompanyDetailByLocationIdResult ObjCompanyDetailByLocationId = new Proc_Get_CompanyDetailByLocationIdResult();
            BALCompany ObjBALCompany = new BALCompany();
           // string CompanyId = string.Empty;
            try
            {
                ObjCompanyDetailByLocationId = ObjBALCompany.GetCompanyDetailByLocationId(PkLocationId).FirstOrDefault();

            }
            catch (Exception)
            {

            }
            finally
            {
                ObjBALCompany = null;
            }
            return ObjCompanyDetailByLocationId;
        }
        private UsersModel BindFields(proc_Get_CompanyUser_ByCompanyUserIdResult ObjtblCompanyUser, UsersModel ObjUserModel)
        {

            #region Membership Info
            ObjUserModel.fkUserId = ObjtblCompanyUser.fkUserId;
            MembershipUser ObjMembershipUser = Membership.GetUser(ObjtblCompanyUser.fkUserId);
            string[] ObjRoles = Roles.GetRolesForUser(ObjMembershipUser.UserName);
            if (ObjRoles.Count() == 2)
            {
                foreach (string rol in ObjRoles)
                {
                    if (rol.ToLower() != "corporatemanager")
                    {
                        ObjUserModel.CurrentEditUserRoleName = rol;
                    }
                }
            }
            else
            {
                ObjUserModel.CurrentEditUserRoleName = ObjRoles[0].ToString();
            }
            ObjUserModel.LastLoginByUser = ObjMembershipUser.LastLoginDate.ToString();
            ObjUserModel.UserEmail = ObjMembershipUser.UserName;

            string Password = ObjMembershipUser.GetPassword();
            ObjUserModel.UserPassword = Password;

            #endregion

            #region User Personal Info

            //trLastLogin.Visible = true;
            // trUserImage.Visible = true;

            ObjUserModel.FirstName = ObjtblCompanyUser.FirstName;
            ObjUserModel.UserNote = ObjtblCompanyUser.UserNote;
            ObjUserModel.LastName = ObjtblCompanyUser.LastName;
            ObjUserModel.PkCompanyId = 0;
            ObjUserModel.SelectedCompanyName = string.Empty;

            Proc_Get_CompanyDetailByLocationIdResult ObjCompanyDetailByLocationId = GetCompanyDetailsByLocationId(ObjtblCompanyUser.fkLocationId.GetValueOrDefault());

            if (ObjCompanyDetailByLocationId != null)
            {
                ObjUserModel.PkCompanyId = (ObjCompanyDetailByLocationId.fkCompanyId.GetValueOrDefault() != null) ? ObjCompanyDetailByLocationId.fkCompanyId.GetValueOrDefault() : 0;
                ObjUserModel.SelectedCompanyName = ObjCompanyDetailByLocationId.CompanyName;
            }

            ///<summary>
            ///if company id "7dc87d77-6bb5-4bb3-9837-fa861f36f281", it means 
            ///users under root company.
            ///</summary>
            ///
            // #19
            // if (ObjUserModel.PkCompanyId == new Guid("7dc87d77-6bb5-4bb3-9837-fa861f36f281"))

            if (ObjUserModel.PkCompanyId == 9514)
            {
                ObjUserModel.IsRootCompany = true;
            }
            else
            {
                ObjUserModel.IsRootCompany = false;
            }

            CheckRoleForEdit(ObjUserModel);

            ObjUserModel.fkLocationId = ObjtblCompanyUser.fkLocationId;
            ObjUserModel.RoleId = (Guid)ObjtblCompanyUser.RoleId;
            ObjUserModel.Description = GetRoleDescription((Guid)ObjtblCompanyUser.RoleId);
            //BindLocation(CompanyId);

            ObjUserModel.fkStateId = (ObjtblCompanyUser.fkStateId == null || ObjtblCompanyUser.fkStateId.ToString() == "") ? -1 : ObjtblCompanyUser.fkStateId;

            //int StateId;
            //int.TryParse(ObjUserModel.fkStateId.ToString(), out StateId);

            // ObjUserModel.CollCounties = LoadStateCounties(StateId);

            ObjUserModel.fkCountyId = ObjtblCompanyUser.fkCountyId;
            ObjUserModel.Initials = (ObjtblCompanyUser.Initials == null) ? string.Empty : ObjtblCompanyUser.Initials;
            ObjUserModel.UserAddressLine1 = ObjtblCompanyUser.UserAddressLine1;
            ObjUserModel.UserAddressLine2 = ObjtblCompanyUser.UserAddressLine2;
            ObjUserModel.PhoneNo = ObjtblCompanyUser.PhoneNo;
            ObjUserModel.ZipCode = ObjtblCompanyUser.ZipCode;
            ObjUserModel.UserEmailOne = ObjtblCompanyUser.UserEmailOne;
            ObjUserModel.UserEmailTwo = ObjtblCompanyUser.UserEmailTwo;



            ObjUserModel.TaleoApiKey = ObjtblCompanyUser.TaleoApiKey;//nd-16
            ObjUserModel.IsAutoEmergeReview = ObjtblCompanyUser.AutoEmergeReview;//INT202
            ObjUserModel.IsEnabled = ObjtblCompanyUser.IsEnabled;


            if (ObjtblCompanyUser.IsSignedFcra != null)
            {
                ObjUserModel.IsSignedFcra = Convert.ToBoolean(ObjtblCompanyUser.IsSignedFcra);
            }
            ObjUserModel.IsLiveRunnerEnabled = Convert.ToBoolean(ObjtblCompanyUser.IsLiveRunnerEnabled);

            #region User Image

            if (ObjtblCompanyUser.UserImage == string.Empty || !System.IO.File.Exists(Server.MapPath(@"~\Resources\Upload\Images\" + ObjtblCompanyUser.UserImage)))
            {
                ObjUserModel.ViewImage = ApplicationPath.GetApplicationPath() + @"Content/themes/base/images/NotAvail.png";
            }
            else
            {
                ObjUserModel.OldUserImage = ObjtblCompanyUser.UserImage;
                ObjUserModel.ViewImage = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjtblCompanyUser.UserImage;
            }

            #endregion

            #endregion

            #region ViewState Info

            ObjUserModel.pkCompanyUserId = ObjtblCompanyUser.pkCompanyUserId;
            ObjUserModel.fkUserId = ObjtblCompanyUser.fkUserId;
            ObjUserModel.OldUserPassword = Password;
            ObjUserModel.OldUserEmail = ObjMembershipUser.UserName.Trim();

            return ObjUserModel;
            #endregion
        }

        public string GetRoleDescription(Guid RoleId)
        {
            string RoleDescription = string.Empty;

            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                RoleDescription = DX.aspnet_Roles.Where(d => d.RoleId == RoleId).FirstOrDefault().Description;
            }

            return RoleDescription;
        }

        /// <summary>
        /// Function To Get User User Id OF Logged in Person 
        /// </summary>
        private void GetCompanyUserId()
        {
            ObjBALCompanyUsers = new BALCompanyUsers();
            try
            {
                Guid UserId = Utility.GetUID(User.Identity.Name);
                var dbCollection = ObjBALCompanyUsers.GetUserInformationByMembershipUserId(UserId);
                if (dbCollection.Count > 0)
                {
                    CompanyUserId = dbCollection.First().pkCompanyUserId;
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                ObjBALCompanyUsers = null;
            }
        }
        /// <summary>
        /// Function To Bind the All the Textboxes Values While Updating Process .
        /// </summary>
        /// <param name="ObjUserDetailByLocationId"></param>



        public UsersModel CheckRoleForEdit(UsersModel ObjUserModel)
        {
            string CurrentRole = string.Empty; string EditUserRole = string.Empty;
            if (ObjUserModel.CurrentLoginUserRoleName != null || ObjUserModel.CurrentEditUserRoleName != null)
            {
                CurrentRole = ObjUserModel.CurrentLoginUserRoleName.ToLower();
                EditUserRole = ObjUserModel.CurrentEditUserRoleName.ToLower();
            }
            try
            {
                if ((CurrentRole == "systemadmin") && (EditUserRole == "supportmanager" || EditUserRole == "supporttechnician" || EditUserRole == "salesadmin" || EditUserRole == "systemadmin"))  //CurrentRole == "admin" || 
                {
                    ObjUserModel.ddlCompanyEnabled = "!disabled";
                    ObjUserModel.ddlLocationEnabled = "!disabled";
                    ObjUserModel.ddlStatusEnabled = "!disabled";
                }
                else if ((CurrentRole == "supportmanager") && (EditUserRole == "supportmanager" || EditUserRole == "systemadmin"))//EditUserRole == "admin" || 
                {
                    //  imgbtnAdd.Visible = false;                       
                    ObjUserModel.txtPasswordMode = "password";
                    ObjUserModel.txtPasswordReadOnly = "readonly";
                    ObjUserModel.ddlCompanyEnabled = "disabled";
                    ObjUserModel.ddlLocationEnabled = "disabled";
                    ObjUserModel.ddlStatusEnabled = "disabled";
                }
                else if ((CurrentRole == "supporttechnician") && (EditUserRole == "supportmanager" || EditUserRole == "supporttechnician" || EditUserRole == "systemadmin"))//EditUserRole == "admin" ||
                {
                    // imgbtnAdd.Visible = false;
                    ObjUserModel.txtPasswordMode = "password";
                    ObjUserModel.txtPasswordReadOnly = "readonly";
                    ObjUserModel.ddlCompanyEnabled = "disabled";
                    ObjUserModel.ddlLocationEnabled = "disabled";
                    ObjUserModel.ddlStatusEnabled = "disabled";
                }

                else if (CurrentRole == "systemadmin" && EditUserRole == "systemadmin")
                {
                    ObjUserModel.ddlCompanyEnabled = "!disabled";
                    ObjUserModel.ddlLocationEnabled = "!disabled";
                    ObjUserModel.ddlStatusEnabled = "!disabled";
                }
                else
                {
                    //imgbtnAdd.Visible = true;
                    ObjUserModel.ddlCompanyEnabled = "!disabled";
                    ObjUserModel.ddlLocationEnabled = "!disabled";
                    ObjUserModel.ddlStatusEnabled = "!disabled";
                }
            }
            catch //(Exception ex)
            {
            }
            return ObjUserModel;
        }

        public ActionResult BindStatusForAdminSection1(string IsRoot, string EditUserRole, string CurrentRole)
        {
            #region OLd Code
            //BALGeneral ObjGeneral = new BALGeneral();
            //List<ClsRoles> RolesColl = new List<ClsRoles>();
            //try
            //{

            //    RolesColl = ObjGeneral.FetchRolesAll(Utility.SiteApplicationId).Select(d => new ClsRoles
            //    {
            //        RoleId = d.RoleId,
            //        Description = d.Description,
            //        RoleName = d.RoleName
            //    }).ToList();

            //    if (RolesColl.Count != 0)
            //    {
            //        if (Convert.ToBoolean(IsRoot))
            //        {
            //            RolesColl = RolesColl.Where(d => d.RoleName != "BasicUser"
            //                && d.RoleName != "CorporateManager" && d.RoleName != "BranchManager"
            //                 && d.RoleName != "DataEntry").ToList<ClsRoles>();
            //        }
            //        else
            //        {
            //            RolesColl = RolesColl.Where(d => d.RoleName != "SalesRep" && d.RoleName != "SupportManager"
            //                && d.RoleName != "SupportTechnician" && d.RoleName != "SalesAdmin"
            //                && d.RoleName != "SalesManager"
            //                && d.RoleName != "SystemAdmin").ToList<ClsRoles>();
            //        }
            //        if (CurrentRole == "systemadmin" && EditUserRole == "systemadmin")
            //        {
            //            RolesColl = RolesColl.Where(d => d.RoleName == "SystemAdmin").ToList<ClsRoles>();

            //        }
            //    }
            //}
            //catch
            //{ }
            //return Json(RolesColl);
            #endregion

            #region Updated Code Ashish
            BALGeneral ObjGeneral = new BALGeneral();
            List<ClsRoles> RolesColl = new List<ClsRoles>();
            try
            {

                RolesColl = ObjGeneral.FetchRolesAll(Utility.SiteApplicationId).Select(d => new ClsRoles
                {
                    RoleId = d.RoleId,
                    Description = d.Description,
                    RoleName = d.RoleName
                }).ToList();

                if (RolesColl.Count != 0)
                {

                    if (Convert.ToBoolean(IsRoot))
                    {
                        if (CurrentRole == "systemadmin" && EditUserRole == "systemadmin")
                        {
                            RolesColl = RolesColl.Where(d => d.RoleName == "SystemAdmin").ToList<ClsRoles>();

                        }
                        else
                        {
                            RolesColl = RolesColl.Where(d => d.RoleName != "BasicUser"
                                && d.RoleName != "CorporateManager" && d.RoleName != "BranchManager"
                                 && d.RoleName != "DataEntry").ToList<ClsRoles>();
                        }
                    }

                    else
                    {
                        if (CurrentRole == "systemadmin" && EditUserRole == "systemadmin")
                        {
                            RolesColl = RolesColl.Where(d => d.RoleName == "SystemAdmin").ToList<ClsRoles>();

                        }
                        else
                        {
                            RolesColl = RolesColl.Where(d => d.RoleName != "SalesRep" && d.RoleName != "SupportManager"
                                && d.RoleName != "SupportTechnician" && d.RoleName != "SalesAdmin"
                                && d.RoleName != "SalesManager"
                                && d.RoleName != "SystemAdmin").ToList<ClsRoles>();
                        }
                    }

                }
            }
            catch
            { }
            return Json(RolesColl);

            #endregion



        }

        [ValidateInput(false)]
        public ActionResult AddUpdateUser(UsersModel ObjUsersModel, HttpPostedFileBase UserImage)
        {
            string strResult = string.Empty;


            if (!string.IsNullOrEmpty(ObjUsersModel.QueryStringUserId))
            {
                string OldUserName = ObjUsersModel.OldUserEmail;
                string NewUserName = ObjUsersModel.UserEmail;
                bool bResult = IsExistsUsername(OldUserName, NewUserName);

                if (bResult)
                {
                    strResult = "<span class='infomessage'>This Username already exists</span>";
                    TempData["message"] = strResult;
                    return Json(new { Message = strResult });
                }

                #region If User not having Corporate Role then assign to it.

                if (ObjUsersModel.Description.Replace(" ", "").ToLower() == "supportmanager" || ObjUsersModel.Description.Replace(" ", "").ToLower() == "supporttechnician" || ObjUsersModel.Description.Replace(" ", "").ToLower() == "systemadmin")
                {
                    if (!Roles.IsUserInRole(NewUserName, "CorporateManager"))
                    {
                        Roles.AddUserToRole(NewUserName, "CorporateManager");
                    }
                }
                else if (ObjUsersModel.Description.Replace(" ", "").ToLower() == "salesadmin" || ObjUsersModel.Description.Replace(" ", "").ToLower() == "salesmanager" || ObjUsersModel.Description.Replace("Sales Representative", "SalesRep").Replace(" ", "").ToLower() == "salesrep")
                {
                    if (Roles.IsUserInRole(NewUserName, "CorporateManager"))
                    {
                        Roles.RemoveUserFromRole(NewUserName, "CorporateManager");
                    }
                }

                #endregion

                strResult = UpdateUserInformationinDB(ObjUsersModel, UserImage);
            }
            else
            {
                strResult = CreateMember(ObjUsersModel, UserImage);
            }
            TempData["message"] = strResult;

            if (NewPkCompanyId != 0 && NewCompanyUserId != 0)
            {
                return RedirectToAction("AddCompanyUser", new { _a = NewCompanyUserId, _c = NewPkCompanyId });
            }
            return RedirectToAction("AddCompanyUser", new { _a = ObjUsersModel.QueryStringUserId, _c = ObjUsersModel.QueryStringCompanyId });

        }

        /// <summary>
        /// Function To Update User 
        /// </summary>
        public string UpdateUserInformationinDB(UsersModel ObjUsersModel, HttpPostedFileBase UserImage)
        {
            string strResult = string.Empty;
            tblCompanyUser ObjtblCompanyUser = new tblCompanyUser();
            ObjBALCompanyUsers = new BALCompanyUsers();
            BLLMembership ObjBLLMembership = new BLLMembership();

            try
            {
                ObjtblCompanyUser.fkUserId = ObjUsersModel.fkUserId;
                ObjtblCompanyUser.pkCompanyUserId = ObjUsersModel.pkCompanyUserId;
                ObjtblCompanyUser.fkLocationId = ObjUsersModel.fkLocationId;
                ObjtblCompanyUser.FirstName = ObjUsersModel.FirstName;
                ObjtblCompanyUser.LastName = ObjUsersModel.LastName;
                ObjtblCompanyUser.Initials = string.Empty;
                ObjtblCompanyUser.UserAddressLine1 = !string.IsNullOrEmpty(ObjUsersModel.UserAddressLine1) ? ObjUsersModel.UserAddressLine1 : string.Empty;
                ObjtblCompanyUser.UserAddressLine2 = !string.IsNullOrEmpty(ObjUsersModel.UserAddressLine2) ? ObjUsersModel.UserAddressLine2 : string.Empty;
                ObjtblCompanyUser.PhoneNo = !string.IsNullOrEmpty(ObjUsersModel.PhoneNo) ? ObjUsersModel.PhoneNo : string.Empty;
                ObjtblCompanyUser.UserNote = !string.IsNullOrEmpty(ObjUsersModel.UserNote) ? ObjUsersModel.UserNote : string.Empty;
                ObjtblCompanyUser.ZipCode = !string.IsNullOrEmpty(ObjUsersModel.ZipCode) ? ObjUsersModel.ZipCode : string.Empty;
                ObjtblCompanyUser.fkStateId = ObjUsersModel.fkStateId;
                ObjtblCompanyUser.UserEmailOne = ObjUsersModel.UserEmailOne;
                ObjtblCompanyUser.UserEmailTwo = ObjUsersModel.UserEmailTwo;
                ObjtblCompanyUser.AutoEmergeReview = ObjUsersModel.IsAutoEmergeReview;//INT202

                if (Convert.ToString(ObjUsersModel.fkCountyId) != "-1")
                {
                    ObjtblCompanyUser.fkCountyId = ObjUsersModel.fkCountyId;
                }
                else
                {
                    ObjtblCompanyUser.fkCountyId = null;
                }

                ObjtblCompanyUser.IsEnabled = ObjUsersModel.IsEnabled;
                ObjtblCompanyUser.LastModifiedById = ObjUsersModel.CompanyUserId;
                ObjtblCompanyUser.LastModifiedDate = System.DateTime.Now;
                ObjtblCompanyUser.IsSignedFcra = ObjUsersModel.IsSignedFcra;
                ObjtblCompanyUser.IsLiveRunnerEnabled = ObjUsersModel.IsLiveRunnerEnabled;
                ObjtblCompanyUser.UserImage = string.Empty;

                if (UserImage != null)
                {
                    if (UserImage != null && UserImage.ContentLength > 0)
                    {
                        if (ObjUsersModel.OldUserImage != null)
                        {
                            string oldimage_Path = Server.MapPath(@"~\Resources\Upload\Images\") + ObjUsersModel.OldUserImage;
                            if (System.IO.File.Exists(oldimage_Path))
                            {
                                System.IO.File.Delete(oldimage_Path);
                            }
                        }
                        string UserImageFileName = ObjUsersModel.FirstName + ObjUsersModel.LastName + "_" + DateTime.Now.ToFileTime().ToString() + System.IO.Path.GetExtension(UserImage.FileName);
                        string upload_Path = Server.MapPath(@"~\Resources\Upload\Images\") + UserImageFileName;
                        UserImage.SaveAs(upload_Path);

                        ObjtblCompanyUser.UserImage = UserImageFileName;
                    }

                }
                else
                {
                    if (ObjUsersModel.OldUserImage != null)
                    {
                        ObjtblCompanyUser.UserImage = ObjUsersModel.OldUserImage;
                    }
                }
                string OldRole = "";

                if (ObjUsersModel.CurrentEditUserRoleName != null)
                {
                    OldRole = ObjUsersModel.CurrentEditUserRoleName;

                    string Description = ObjUsersModel.Description.Replace("Sales Representative", "SalesRep").Replace(" ", "");
                    if (OldRole.ToLower() != Description.ToLower())
                    {
                        if (Roles.IsUserInRole(ObjUsersModel.UserEmail, OldRole))
                        {
                            Roles.RemoveUserFromRole(ObjUsersModel.UserEmail, OldRole);
                            if (!Roles.IsUserInRole(ObjUsersModel.UserEmail, Description))
                                Roles.AddUserToRole(ObjUsersModel.UserEmail, Description);
                            ObjUsersModel.CurrentEditUserRoleName = Description;
                        }
                    }
                }
                else
                {

                }

                BALCompanyUsers newObjBALCompanyUsers = new BALCompanyUsers();
                int Result = newObjBALCompanyUsers.UpdateCompanyUsers(ObjtblCompanyUser, ObjUsersModel.RoleId, OldRole);


                #region  For Delete And Insert  Locations

                int pkCompanyUserId = (ObjUsersModel.pkCompanyUserId);
                // Delete PreviousLocation
                //int DeletePreviousRec = 
                    ObjBALCompanyUsers.DeleteOtherLocation(pkCompanyUserId);

                string SelectedLocationId = ObjUsersModel.SelectedLocationId;
                if (SelectedLocationId != null)
                {
                    string[] stringSeparators = new string[] { "," };
                    string[] ListSelectedLocationIds = SelectedLocationId.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);



                    BALLocation ObjBALLocation = new BALLocation();
                    List<tblLocation> lstLocations = new List<tblLocation>();
                    int PkCompanyId = ObjUsersModel.PkCompanyId;
                    lstLocations = ObjBALLocation.GetLocationsByCompanyId(PkCompanyId).Select(p => new tblLocation
                    {
                        pkLocationId = p.pkLocationId,
                        City = p.City

                    }).ToList();

                    // Add All loCations
                    for (int i = 0; i < lstLocations.Count; i++)
                    {
                        int fkLocationid = lstLocations[i].pkLocationId;
                        ObjBALCompanyUsers.InsertOtherLocationOfCompany(pkCompanyUserId, fkLocationid, false);

                    }

                    // update Selected loCations
                    for (int i = 0; i < ListSelectedLocationIds.Length; i++)
                    {
                        string fkLocationid = ListSelectedLocationIds[i];
                        ObjBALCompanyUsers.UpdateOtherLocationOfCompany(pkCompanyUserId, Convert.ToInt16(fkLocationid), true);

                    }

                }
                else
                {

                }

                #endregion
                if (Result > 0)
                {
                    #region Update User Name and password and Role

                    MembershipUser ObjMembershipUser = Membership.GetUser(ObjUsersModel.OldUserEmail);

                    string OldUserName = ObjUsersModel.OldUserEmail;
                    string NewUserName = ObjUsersModel.UserEmail;

                    if (OldUserName.ToLower().Trim() != NewUserName.ToLower())
                    {
                        UpdateUsername(OldUserName, NewUserName);

                        #region Change email address

                        UpdateUserEmail(NewUserName, ObjUsersModel.fkUserId);

                        #endregion

                        RegistrationEmail(ObjUsersModel.UserEmail, ObjUsersModel.UserPassword);

                        #region Deduct last activity time

                        if (ObjMembershipUser != null)
                        {
                            ObjBLLMembership.SignOut(ObjMembershipUser.UserName, DateTime.Now.ToUniversalTime().AddMinutes(-(Membership.UserIsOnlineTimeWindow)));
                        }

                        #endregion
                    }

                    string OldPwd = ObjUsersModel.OldUserPassword;

                    if (ObjMembershipUser != null)
                    {
                        if (OldPwd != ObjUsersModel.UserPassword)
                        {
                            Membership.GetUser(ObjUsersModel.OldUserEmail).ChangePassword(OldPwd, ObjUsersModel.UserPassword);
                        }
                    }

                    #endregion
                    strResult = "<span class='successmsg'>Record Updated Successfully</span>";
                }
                else
                {
                    strResult = "<span class='errormsg'>Error Occured While Updating Record</span>";
                }
            }
            catch//(Exception ex)
            {
                strResult = "<span class='errormsg'>Error Occured While Updating Record</span>";

            }
            finally
            {
                ObjtblCompanyUser = null;
                ObjBALCompanyUsers = null;
                ObjBLLMembership = null;
            }
            return strResult;
        }

        /// <summary>
        /// Function To Craete New Membership User 
        /// </summary>
        private string CreateMember(UsersModel ObjUsersModel, HttpPostedFileBase UserImage)
        {
            string strResult = string.Empty;
            BLLMembership ObjBLLMembership = new BLLMembership();

            try
            {
                MembershipCreateStatus Status_Variable;
                Membership.ApplicationName = Utility.SiteApplicationName;
                MembershipUser ObjMembershipUser = Membership.CreateUser(ObjUsersModel.UserEmail,
                                                                         ObjUsersModel.UserPassword,
                                                                         ObjUsersModel.UserEmail,
                                                                         null,
                                                                         null,
                                                                         true,
                                                                         out Status_Variable);

                #region Deduct last activity time

                ObjMembershipUser = Membership.GetUser(ObjUsersModel.UserEmail);
                if (ObjMembershipUser != null)
                {
                    ObjBLLMembership.SignOut(ObjMembershipUser.UserName, DateTime.Now.ToUniversalTime().AddMinutes(-(Membership.UserIsOnlineTimeWindow)));
                }

                #endregion

                switch (Status_Variable)
                {
                    case MembershipCreateStatus.DuplicateUserName:
                        strResult = "<span class='errormsg'>Email aready exists, Please choose another</span>";
                        break;

                    case MembershipCreateStatus.InvalidUserName:
                        strResult = "<span class='errormsg'>Invalid Email</span>";
                        break;

                    case MembershipCreateStatus.ProviderError:
                        strResult = "<span class='errormsg'>Provider Error occured please try later on</span>";
                        break;

                    case MembershipCreateStatus.Success:
                        Roles.ApplicationName = Utility.SiteApplicationName;// Convert.ToString(Session["ApplicationName"]);
                        Roles.AddUserToRole(ObjUsersModel.UserEmail, ObjUsersModel.Description.Replace(" ", ""));
                        if (ObjUsersModel.Description.Replace(" ", "").ToLower() == "systemadmin" || ObjUsersModel.Description.Replace(" ", "").ToLower() == "supportmanager" || ObjUsersModel.Description.Replace(" ", "").ToLower() == "supporttechnician")
                        {
                            Roles.AddUserToRole(ObjUsersModel.UserEmail, "CorporateManager");
                        }
                        if ((Roles.IsUserInRole(ObjUsersModel.UserEmail, ObjUsersModel.Description.Replace(" ", ""))) && (AddUserinDB(new Guid(Membership.GetUser(ObjUsersModel.UserEmail).ProviderUserKey.ToString()), ObjUsersModel, UserImage)))
                        {
                            RegistrationEmail(ObjUsersModel.UserEmail, ObjUsersModel.UserPassword);
                            strResult = "<span class='successmsg'>New User Added Sucessfully</span>";
                            //btnAddNew.Visible = true;
                        }
                        else
                        {
                            Membership.DeleteUser(ObjUsersModel.UserEmail, true);
                            strResult = "<span class='errormsg'>Error Occured While Adding New User</span>";
                        }
                        break;
                }
            }
            finally
            {
                ObjBLLMembership = null;
            }
            return strResult;
        }


        /// <summary>
        /// This method is used to update user email address
        /// </summary>
        /// <param name="NewEmail"></param>
        private void UpdateUserEmail(string NewEmail, Guid fkUserId)
        {
            if (fkUserId != null)
            {
                MembershipUser mu = Membership.GetUser(fkUserId);

                if (mu != null)
                {
                    mu.Email = NewEmail;
                    Membership.UpdateUser(mu);
                }

            }
        }



        public void RegistrationEmail(string UserEmail, string Password)
        {
            BALGeneral objBALGeneral = new BALGeneral();
            try
            {
                string emailText = "";
                string MessageBody = "";
                BALGeneral ObjBALGeneral = new BALGeneral();
                string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;
                var emailContent = objBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, Convert.ToInt32(EmailTemplates.NewUserRegistration));//"New User Registration"
                emailText = emailContent.TemplateContent;
                //for ticket #101
                string usersMailsIds = string.Empty;
                if (!string.IsNullOrEmpty(UserEmail))
                {
                    usersMailsIds = ObjBALGeneral.GetUserOtherMailIds(UserEmail);
                }


                #region New way Bookmark

                BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                Bookmarks objBookmark = new Bookmarks();

                #region For New Style SystemTemplate
                string Bookmarkfile = string.Empty;
                string bookmarkfilepath = string.Empty;
                Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                emailText = Bookmarkfile;
                #endregion



                objBookmark.EmailContent = emailText;
                objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                objBookmark.CompanyName = ObjBALGeneral.GetSettings(1).CompanyName;
                objBookmark.UserEmail = UserEmail.Trim();
                objBookmark.UserPassword = Password.Trim();
                objBookmark.LoginLink = "<a style='color: #87ceea;' href='" + ApplicationPath.GetApplicationPath() + "'>Login Now !</a>";
                MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Utility.GetUID(User.Identity.Name));

                #endregion
                //INT-210 Resend mail 
                bool result = Utility.SendMail(UserEmail.Trim(), usersMailsIds, string.Empty, BALGeneral.GetGeneralSettings().SupportEmailId, MessageBody, "eMerge New User Registration");
                TempData["Mailstatus"] = result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }




        /// <summary>
        /// This method is used to update Username
        /// </summary>
        /// <param name="OldUserName"></param>
        /// <param name="NewUserName"></param>
        /// <returns></returns>
        private int UpdateUsername(string OldUserName, string NewUserName)
        {
            int iResult = -1;

            NewUserName = NewUserName.Trim();

            BLLMembership ObjBLLMembership = new BLLMembership();

            try
            {
                iResult = ObjBLLMembership.UpdateUserName(OldUserName, NewUserName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBLLMembership = null;
            }
            return iResult;
        }
        /// <summary>
        /// This method is used to check user name existance
        /// </summary>
        /// <param name="OldUserName"></param>
        /// <param name="NewUserName"></param>
        /// <returns></returns>
        private bool IsExistsUsername(string OldUserName, string NewUserName)
        {
            bool bResult = false;
            NewUserName = NewUserName.Trim();

            BLLMembership ObjBLLMembership = new BLLMembership();

            try
            {
                bResult = ObjBLLMembership.IsExistUserName(OldUserName, NewUserName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBLLMembership = null;
            }
            return bResult;
        }


        public ActionResult TaleoWebApiKeyGenrate(string UserEmail)
        {

            int fkuserid = Convert.ToInt32(TempData["TaleoWebApiObjGuid"]);


            Random _Random = new Random();
            string test_to_encrypt = _Random.Next().ToString();
            byte[] arrbyte = new byte[test_to_encrypt.Length];
            string TaleoWebApikEYavalue = string.Empty;
            using (System.Security.Cryptography.SHA1CryptoServiceProvider hash = new System.Security.Cryptography.SHA1CryptoServiceProvider())
            {
                arrbyte = hash.ComputeHash(System.Text.Encoding.UTF8.GetBytes(test_to_encrypt));
                TaleoWebApikEYavalue = System.Web.HttpServerUtility.UrlTokenEncode(arrbyte);

            }
            using (EmergeDALDataContext _EmergeDALDataContext = new EmergeDALDataContext())
            {

                var tmp = (from c in _EmergeDALDataContext.aspnet_Memberships
                           join cop in _EmergeDALDataContext.tblCompanyUsers on c.UserId equals cop.fkUserId
                           where cop.pkCompanyUserId == fkuserid && c.Email == UserEmail
                           select c);


                if (tmp.Count() == 1)
                {
                    aspnet_Membership objaspnet_Membership = tmp.First();

                    objaspnet_Membership.TaleoApiKey = TaleoWebApikEYavalue.ToString();
                    _EmergeDALDataContext.SubmitChanges();
                }
            }

            return Json(new { Fname = "SUCCESS" }, JsonRequestBehavior.AllowGet);
            //return null;
        }


        /// <summary>
        /// For sending email of existing user with regenarated password
        /// INT-210
        /// </summary>
        [HttpPost]
        public string ReSendmailToExistUser(string UserEmail, string NewUserPassword, string OldUserPassword)
        {
            string result_msg = string.Empty;
            MembershipUser ObjMembershipUser = Membership.GetUser(UserEmail);
            int PasswordStatus = 0;

            //Update password and send a mail to user
            if (ObjMembershipUser != null)
            {
                if (OldUserPassword != NewUserPassword)
                {
                    ObjMembershipUser.ChangePassword(OldUserPassword, NewUserPassword);
                    RegistrationEmail(UserEmail, NewUserPassword);
                    PasswordStatus = 1;
                }
                else
                {
                    RegistrationEmail(UserEmail, NewUserPassword);
                    PasswordStatus = 2;
                }
            }

            // Sending mail status
            //var flag = TempData["Mailstatus"];
            int MAilSendstatus = 1;//Convert.ToInt32(flag);
            if (MAilSendstatus == 1 && PasswordStatus == 1)
            {
                result_msg = "Mail has been sent to user successfully with updated password.";
            }
            else if (MAilSendstatus == 1 && PasswordStatus == 2)
            {
                result_msg = "Mail has been sent to user successfully.";
            }
            else
            {
                result_msg = "Mail not to be sent due to some error.";
            }
            return result_msg;
        }
        public string RandomPasswordGeneration()
        {
            int PaswordLength = 8;
            int numberOfNonAlphanumericCharacters = 0;
            string Password = Membership.GeneratePassword(PaswordLength, numberOfNonAlphanumericCharacters);
            return Password;
        }

        //INT-210



        /// <summary>
        /// Function to Add new User and its Location To the Databse 
        /// </summary>
        public bool AddUserinDB(Guid FkUserId, UsersModel ObjUsersModel, HttpPostedFileBase UserImage)
        {
            tblCompanyUser ObjtblCompanyUser = new tblCompanyUser();
            ObjBALCompanyUsers = new BALCompanyUsers();

            //// insrting email1 and email2 into database
            //EmergeDALDataContext myDB = new EmergeDALDataContext();
            //tblCompanyUser usr = new tblCompanyUser();
            //usr.UserEmailOne = ObjUsersModel.UserEmailOne;
            //usr.UserEmailTwo = ObjUsersModel.UserEmailTwo;
            //myDB.tblCompanyUsers.InsertOnSubmit(usr);
            //myDB.SubmitChanges(); 


            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                var ch = (from aspUser in ObjDALDataContext.aspnet_Users
                          join compUser in ObjDALDataContext.tblCompanyUsers
                          on aspUser.UserId equals compUser.fkUserId
                          where aspUser.UserName.Equals(ObjUsersModel.CurrentUserEmail)
                          select compUser).FirstOrDefault();
                if (ch != null)
                {
                    ObjUsersModel.fkLocationId = ch.fkLocationId;
                    ObjUsersModel.fkStateId = ch.fkStateId;
                    ObjUsersModel.fkCountyId = ch.fkCountyId;
                }
            }




            bool ReturnValue = false;

            try
            {
                ObjtblCompanyUser.CreatedById = CompanyUserId;
                ObjtblCompanyUser.CreatedDate = System.DateTime.Now;
                ObjtblCompanyUser.FirstName = ObjUsersModel.FirstName;
                if (!string.IsNullOrEmpty(ObjUsersModel.SelectedCompanyName))
                {
                    ObjtblCompanyUser.UserCode = Utility.GenerateUserCode(ObjUsersModel.SelectedCompanyName, currentDate);
                }
                else
                {
                    ObjtblCompanyUser.UserCode = currentDate.ToString("yyHHmmssMMdd");
                }

                if (ObjUsersModel.fkCountyId != -1)
                {
                    ObjtblCompanyUser.fkCountyId = ObjUsersModel.fkCountyId;
                }
                else
                {
                    ObjtblCompanyUser.fkCountyId = null;
                }

                ObjtblCompanyUser.fkLocationId = ObjUsersModel.fkLocationId;
                ObjtblCompanyUser.fkStateId = ObjUsersModel.fkStateId;
                ObjtblCompanyUser.IsDeleted = false;
                ObjtblCompanyUser.IsEnabled = true;
                ObjtblCompanyUser.IsSignedFcra = ObjUsersModel.IsSignedFcra;
                ObjtblCompanyUser.IsLiveRunnerEnabled = ObjUsersModel.IsLiveRunnerEnabled;
                ObjtblCompanyUser.fkUserId = FkUserId;
                ObjtblCompanyUser.LastName = ObjUsersModel.LastName;
                ObjtblCompanyUser.UserAddressLine1 = !string.IsNullOrEmpty(ObjUsersModel.UserAddressLine1) ? ObjUsersModel.UserAddressLine1 : string.Empty;
                ObjtblCompanyUser.UserAddressLine2 = !string.IsNullOrEmpty(ObjUsersModel.UserAddressLine2) ? ObjUsersModel.UserAddressLine2 : string.Empty;
                ObjtblCompanyUser.PhoneNo = !string.IsNullOrEmpty(ObjUsersModel.PhoneNo) ? ObjUsersModel.PhoneNo : string.Empty;
                ObjtblCompanyUser.UserNote = !string.IsNullOrEmpty(ObjUsersModel.UserNote) ? ObjUsersModel.UserNote : string.Empty;
                ObjtblCompanyUser.ZipCode = !string.IsNullOrEmpty(ObjUsersModel.ZipCode) ? ObjUsersModel.ZipCode : string.Empty;
                ObjtblCompanyUser.UserEmailOne = ObjUsersModel.UserEmailOne;
                ObjtblCompanyUser.UserEmailTwo = ObjUsersModel.UserEmailTwo;
                if (UserImage != null)
                {
                    if (UserImage != null && UserImage.ContentLength > 0)
                    {
                        string UserImageFileName = ObjUsersModel.FirstName + ObjUsersModel.LastName + "_" + DateTime.Now.ToFileTime().ToString() + System.IO.Path.GetExtension(UserImage.FileName);
                        string upload_Path = Server.MapPath(@"~\Resources\Upload\Images\") + UserImageFileName;
                        UserImage.SaveAs(upload_Path);
                        ObjtblCompanyUser.UserImage = UserImageFileName;
                    }

                }
                else
                {
                    ObjtblCompanyUser.UserImage = string.Empty;
                }

                ObjtblCompanyUser.Initials = string.Empty;
                int Result = ObjBALCompanyUsers.InsertCompanyUser(ObjtblCompanyUser);

                int pkCompanyUserId = ObjBALCompanyUsers.GetPkCompanyUserId(FkUserId, ObjUsersModel.fkLocationId.GetValueOrDefault());
                string SelectedLocationId = ObjUsersModel.SelectedLocationId;
                if (SelectedLocationId != null)
                {
                    string[] stringSeparators = new string[] { "," };

                    string[] ListSelectedLocationIds = SelectedLocationId.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

                    //Guid fkLocationIdforHomeLoc = Guid.Parse(ObjUsersModel.fkLocationId.ToString());
                    List<tblLocation> lstLocations = new List<tblLocation>();
                    BALLocation ObjBALLocation = new BALLocation();
                    int PkCompanyId = ObjUsersModel.PkCompanyId;
                    lstLocations = ObjBALLocation.GetLocationsByCompanyId(PkCompanyId).Select(p => new tblLocation
                    {
                        pkLocationId = p.pkLocationId,
                        City = p.City

                    }).ToList();

                    // Add All loCations
                    for (int i = 0; i < lstLocations.Count; i++)
                    {
                        int fkLocationid = lstLocations[i].pkLocationId;
                        ObjBALCompanyUsers.InsertOtherLocationOfCompany(pkCompanyUserId, fkLocationid, false);

                    }

                    // update Selected loCations
                    for (int i = 0; i < ListSelectedLocationIds.Length; i++)
                    {
                        string fkLocationid = ListSelectedLocationIds[i];
                        ObjBALCompanyUsers.UpdateOtherLocationOfCompany(pkCompanyUserId, Convert.ToInt16(fkLocationid), true);

                    }

                }
                else
                {

                }
                if (Result != 0)
                {
                    NewPkCompanyId = ObjUsersModel.PkCompanyId;
                    NewCompanyUserId = Result;
                    ReturnValue = true;
                }
                else if (Result == 0)
                {
                    ReturnValue = false;
                }
            }
            catch (Exception)
            {
                Membership.DeleteUser(ObjUsersModel.UserEmail);
                // strResult = "<span class='errormsg'>Error Occured While Inserting Record</span>";               
                ReturnValue = false;
            }
            finally
            {
                ObjtblCompanyUser = null;
                ObjBALCompanyUsers = null;
            }
            return ReturnValue;
        }



    }
    public class ClsRoles
    {
        public Guid RoleId { get; set; }
        public string Description { get; set; }
        public string RoleName { get; set; }
    }
}
