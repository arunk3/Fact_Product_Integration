﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Emerge.Data;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        BALGeneral objGeneral;

        #region StateFee
        public ActionResult StateFee()
        {
            objGeneral = new BALGeneral();
            string Mode = string.Empty;
            if (!string.IsNullOrEmpty(Request.QueryString["cd"]))
            {
                string code = Request.QueryString["cd"].ToString();
                if (code == "SCR")
                {
                    ViewBag.Heading = "SCR Fee";
                    Mode = "1";
                    ViewData["FeeData"] = objGeneral.GetAllStatesByCode("SCR");
                }
                if (code == "MVR")
                {
                    ViewBag.Heading = "MVR Fee";
                    Mode = "2";
                    ViewData["FeeData"] = objGeneral.GetAllStatesByCode("MVR");
                }
                if (code == "WCV")
                {
                    ViewBag.Heading = "WCV Consent Form";
                    Mode = "3";
                    ViewData["FeeData"] = objGeneral.GetAllStatesByCode("WCV");
                }
            }
            ViewBag.Type = Mode;
            // ViewData["FeeData"] = GetFee();
            ViewBag.Page = !string.IsNullOrEmpty(Request.QueryString["Page"]) ? Request.QueryString["Page"] : string.Empty;

            return View();
        }

        private List<tblState> GetFee()
        {
            objGeneral = new BALGeneral();
            List<tblState> states = objGeneral.GetAllStates();
            return states;
        }

        [HttpPost]
        public ActionResult UpdateStateFee(FormCollection frm)
        {
            if (frm["hdType"] == "1")
            {
                return updateSCRFee(frm);
            }
            else if (frm["hdType"] == "2")
            {
                return updateMVRFee(frm);
            }
            else if (frm["hdType"] == "3")
            {
                return UpdateWCVConsentDetails(frm);
            }
            return Json(new { Msg = "", css = "" });
        }
        public JsonResult updateSCRFee(FormCollection frm)
        {
            string Message = string.Empty;
            string CssClass = string.Empty;
            objGeneral = new BALGeneral();
            List<tblState> lstState = new List<tblState>();
            List<tblStateFee> lsttblStateFee = new List<tblStateFee>();
            for (int iRow = 0; iRow < frm.Keys.Count; iRow++)
            {
                if (frm.Keys[iRow].Contains("txtLiveRunnerFees_"))
                {
                    tblState objstate = new tblState();
                    tblStateFee objtblStateFee = new tblStateFee();

                    string commonId = frm.GetKey(iRow).Split('_')[1];


                    objtblStateFee.fkProductId = Int32.Parse(frm["fkProductId_" + commonId]);
                    objtblStateFee.fkStateId = Convert.ToInt32(frm["pkStateId_" + commonId]);
                    objstate.pkStateId = Convert.ToInt32(frm["pkStateId_" + commonId]);
                    objstate.LiveRunnerFees = Convert.ToDecimal(frm["txtLiveRunnerFees_" + commonId]);
                    objtblStateFee.AdditionalFee = Convert.ToDecimal(frm["txtRcxFees_" + commonId]);
                    objtblStateFee.DisclaimerData = frm["DisclaimerData_" + commonId];
                    if ((frm["chkIsLiveRunnerState_" + commonId]) != null && (frm["chkIsLiveRunnerState_" + commonId]).ToLower() != "no")
                    {
                        objstate.IsStateLiveRunner = true;
                    }
                    else
                    {
                        objstate.IsStateLiveRunner = false;
                    }
                    if ((frm["chkIsEnabled_" + commonId]) != null && (frm["chkIsEnabled_" + commonId]).ToLower() != "no")
                    {
                        objstate.IsEnabled = true;
                    }
                    else
                    {
                        objstate.IsEnabled = false;
                    }
                    if ((frm["chkConsentForm_" + commonId]) != null && (frm["chkConsentForm_" + commonId]).ToLower() != "no")
                    {
                        objstate.IsConsentForm_SCR = true;
                        objtblStateFee.IsConsent = true;
                    }
                    else
                    {
                        objstate.IsConsentForm_SCR = false;
                        objtblStateFee.IsConsent = false;
                    }
                    lstState.Add(objstate);
                    lsttblStateFee.Add(objtblStateFee);
                }
            }

            int success = objGeneral.UpdateSCRFee(lstState, lsttblStateFee);
            if (success == 1)
            {
                Message = "Record updated successfully.";
                CssClass = "successmsg";
            }
            else
            {
                Message = "Some error occured while updation.";
                CssClass = "errormsg";
            }
            return Json(new { Msg = Message, css = CssClass });

        }

        public JsonResult updateMVRFee(FormCollection frm)
        {
            string Message = string.Empty;
            string CssClass = string.Empty;
            objGeneral = new BALGeneral();
            List<tblStateFee> lsttblStateFee = new List<tblStateFee>();
            for (int iRow = 0; iRow < frm.Keys.Count; iRow++)
            {
                if (frm.Keys[iRow].Contains("txtMVRFee_"))
                {
                    tblStateFee objtblStateFee = new tblStateFee();
                    string commonId = frm.GetKey(iRow).Split('_')[1];

                    objtblStateFee.fkProductId = Int32.Parse(frm["fkProductId_" + commonId]);
                    objtblStateFee.fkStateId = Convert.ToInt32(frm["pkStateId_" + commonId]);
                    objtblStateFee.DisclaimerData = (frm["DisclaimerData_" + commonId]);
                    objtblStateFee.AdditionalFee = Convert.ToDecimal(frm["txtMVRFee_" + commonId]);
                    if ((frm["chkConsentForm_" + commonId]) != null && (frm["chkConsentForm_" + commonId]).ToLower() != "no")
                    {
                        objtblStateFee.IsConsent = true;
                    }
                    else
                    {
                        objtblStateFee.IsConsent = false;
                    }
                    lsttblStateFee.Add(objtblStateFee);
                }
            }

            int success = objGeneral.UpdateStateFee(lsttblStateFee);
            if (success == 1)
            {
                Message = "Record updated successfully.";
                CssClass = "successmsg";
            }
            else
            {
                Message = "Some error occured while updation.";
                CssClass = "errormsg";
            }
            return Json(new { Msg = Message, css = CssClass });

        }

        public JsonResult UpdateWCVConsentDetails(FormCollection frm)
        {
            string Message = string.Empty;
            string CssClass = string.Empty;
            objGeneral = new BALGeneral();
            List<tblStateFee> lsttblStateFee = new List<tblStateFee>();
            for (int iRow = 0; iRow < frm.Keys.Count; iRow++)
            {
                if (frm.Keys[iRow].Contains("DisclaimerData_"))
                {
                    tblStateFee objtblStateFee = new tblStateFee();
                    string commonId = frm.GetKey(iRow).Split('_')[1];

                    objtblStateFee.fkProductId = Int32.Parse(frm["fkProductId_" + commonId]);
                    objtblStateFee.fkStateId = Convert.ToInt32(frm["pkStateId_" + commonId]);

                    objtblStateFee.DisclaimerData = (frm["DisclaimerData_" + commonId]);
                    if ((frm["chkConsentForm_" + commonId]) != null && (frm["chkConsentForm_" + commonId]).ToLower() != "no")
                    {
                        objtblStateFee.IsConsent = true;
                    }
                    else
                    {
                        objtblStateFee.IsConsent = false;
                    }
                    lsttblStateFee.Add(objtblStateFee);
                }
            }

            int success = objGeneral.UpdateStateFee(lsttblStateFee);
            if (success == 1)
            {
                Message = "Record updated successfully.";
                CssClass = "successmsg";
            }
            else
            {
                Message = "Some error occured while updation.";
                CssClass = "errormsg";
            }
            return Json(new { Msg = Message, css = CssClass });

        }
        #endregion

        #region StateCountyFee
        public ActionResult StateCountyFee()
        {
            string ProductCode = string.Empty;
            objGeneral = new BALGeneral();
            if (!string.IsNullOrEmpty(Request.QueryString["cd"]))
            {
                ProductCode = Convert.ToString(Request.QueryString["cd"]);
            }
            ViewBag.ProductCode = ProductCode;
            List<tblState> states = objGeneral.GetStates();
            List<SelectListItem> listStates = states.Select(x => new SelectListItem { Text = x.StateName, Value = x.pkStateId.ToString() }).ToList();
            ViewData["states"] = listStates;
            ViewBag.Page = !string.IsNullOrEmpty(Request.QueryString["Page"]) ? Request.QueryString["Page"] : string.Empty;
            return View();
        }

        [HttpPost]
        public ActionResult UpdateStateCountyFee(FormCollection frm)
        {
            string Message = string.Empty;
            string CssClass = string.Empty;
            objGeneral = new BALGeneral();
            List<tblCounty> lstCounty = new List<tblCounty>();
            List<tblCountyFee> lsttblCountyFee = new List<tblCountyFee>();

            for (int iRow = 0; iRow < frm.Keys.Count; iRow++)
            {

                if (frm.Keys[iRow].Contains("lblpkCountyId_"))
                {

                    tblCounty objtblCounty = new tblCounty();
                    tblCountyFee objtblCountyFee = new tblCountyFee();
                    string commonId = frm.GetKey(iRow).Split('_')[1];

                    if (frm["hdProductCode"] != null && frm["hdProductCode"].ToLower() == "rcx")
                    {
                        /*************RCX****************/
                        objtblCountyFee.fkProductId = Int32.Parse(frm["lblfkProdId_" + commonId]);
                        objtblCountyFee.pkCountyFeeId = Convert.ToInt32(frm["fkProductId_" + commonId]);
                       // objtblCountyFee.fkProductId = new Guid(frm["fkProductId_" + commonId]);
                        objtblCountyFee.fkCountyId = Convert.ToInt32(frm["lblpkCountyId_" + commonId]);

                        objtblCountyFee.AdditionalFee = Convert.ToDecimal(frm["txtRcxFees_" + commonId]);
                        objtblCountyFee.DisclaimerData = (frm["txtDisclaimerData_" + commonId]);
                        if ((frm["chkConsentFormRCX_" + commonId]) != null && (frm["chkConsentFormRCX_" + commonId]).ToLower() != "no")
                        {
                            objtblCountyFee.IsConsent = true;
                        }
                        else
                        {
                            objtblCountyFee.IsConsent = false;
                        }

                        lsttblCountyFee.Add(objtblCountyFee);
                    }
                    else
                    {
                        objtblCountyFee.fkProductId = Int32.Parse(frm["lblfkProdId_" + commonId]);
                        objtblCountyFee.pkCountyFeeId = Convert.ToInt32(frm["fkProductId_" + commonId]);
                        objtblCountyFee.fkCountyId = Convert.ToInt32(frm["lblpkCountyId_" + commonId]);
                        objtblCounty.pkCountyId = Convert.ToInt32(frm["lblpkCountyId_" + commonId]);
                        if ((frm["chkIsRcxCounty_" + commonId]) != null && (frm["chkIsRcxCounty_" + commonId]).ToLower() != "no")
                        {
                            objtblCounty.IsRcxCounty = true;
                        }
                        else
                        {
                            objtblCounty.IsRcxCounty = false;
                        }
                        objtblCounty.CCRFee = Convert.ToDecimal(frm["txtCountyFee_" + commonId]);
                        objtblCountyFee.AdditionalFee = Convert.ToDecimal(frm["txtCountyFee_" + commonId]);
                        objtblCountyFee.DisclaimerData = (frm["txtDisclaimerData_" + commonId]);
                       
                            if ((frm["chkConsentFormCCR_" + commonId]) != null && (frm["chkConsentFormCCR_" + commonId]).ToLower() != "no")
                            {
                                objtblCounty.IsConsentForm_CCR = true;
                                objtblCountyFee.IsConsent = true;
                            }
                            else
                            {
                                objtblCounty.IsConsentForm_CCR = false;
                                objtblCountyFee.IsConsent = false;
                            }

                        lstCounty.Add(objtblCounty);
                        lsttblCountyFee.Add(objtblCountyFee);
                    }
                }
            }
            int success = 0;
            if (frm["hdProductCode"] != null && frm["hdProductCode"].ToLower() == "rcx")
            {
                success = objGeneral.UpdateStateCountyFeeRCX(lsttblCountyFee);
            }
            else
            {
                success = objGeneral.UpdateCountyFees_CCR(lstCounty,lsttblCountyFee);
            }



            if (success == 1)
            {
                Message = "Record updated successfully.";
                CssClass = "successmsg";
            }
            else
            {
                Message = "Some error occured while updation.";
                CssClass = "errormsg";
            }
            return Json(new { Msg = Message, css = CssClass });
        }
        #endregion

        #region Jurisdiction Fees
        public ActionResult JurisdictionFees()
        {
            ObjBALGeneral = new BALGeneral();
            List<tblJurisdiction> ObjJurisdiction = ObjBALGeneral.GetJurisdictions();
            ViewData["FeeData"] = ObjJurisdiction;

            return View();
        }

        public JsonResult UpdateJurisdictionfee(FormCollection frm)
        {
            string Message = string.Empty;
            string CssClass = string.Empty;
            objGeneral = new BALGeneral();
            List<tblJurisdiction> ObjJurisColl = new List<tblJurisdiction>();
            for (int iRow = 0; iRow < frm.Keys.Count; iRow++)
            {
                if (frm.Keys[iRow].Contains("pkJurisdictionId_"))
                {
                    tblJurisdiction ObjJurisdiction = new tblJurisdiction();
                    string commonId = frm.GetKey(iRow).Split('_')[1];

                    ObjJurisdiction.pkJurisdictionId = Convert.ToInt32(frm["pkJurisdictionId_" + commonId]);
                    ObjJurisdiction.JurisdictionFee = Convert.ToDecimal(frm["txtJurisdictionFee_" + commonId]);
                    ObjJurisdiction.DisclaimerData = (frm["txtDisclaimerData_" + commonId]); 
                    if ((frm["chkConsentForm_" + commonId]) != null && (frm["chkConsentForm_" + commonId]).ToLower() != "no")
                    {
                        ObjJurisdiction.IsConsentForm = true;
                    }
                    else
                    {
                        ObjJurisdiction.IsConsentForm = false;
                    }
                    ObjJurisColl.Add(ObjJurisdiction);
                }
            }

            int Result = objGeneral.UpdateJurisdictionfee(ObjJurisColl);
            if (Result == 1)
            {
                Message = "Record updated successfully.";
                CssClass = "successmsg";
            }
            else
            {
                Message = "Some Error Occured While Updating Jurisdiction Fees.";
                CssClass = "errormsg";
            }
            return Json(new { Msg = Message, css = CssClass });

        }
        #endregion
    }
}
