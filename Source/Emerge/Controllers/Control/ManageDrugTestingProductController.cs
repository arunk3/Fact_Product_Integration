﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Emerge.Models;
using Emerge.Data;
using Emerge.Common;
using System.IO;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /ManageDrugTestingProduct/

        public ActionResult ManageProducts()
        {
            DrugTestingProductModel ObjDrugTestingProductModel = new DrugTestingProductModel();
            BALDrug Objbaldrub = new BALDrug();
            try
            {
                var item = Objbaldrub.GetDrugtestingkit();
                ObjDrugTestingProductModel.SalivaPrice = item.Item1;
                ObjDrugTestingProductModel.EZcupPrice = item.Item2;

                if (TempData["message"] != null)
                {
                    ObjDrugTestingProductModel.strResult = TempData["message"].ToString();
                }
            }
            //Added log for exception handling.
            catch (Exception ex)
            { 
                VendorServices.GeneralService.WriteLog("Error Occured in ManageProducts(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "ManageProduct");
            }

            return View(ObjDrugTestingProductModel);

        }

        /// <summary>
        /// Reads the available Drug Testing Products to provide data for the Kendo Grid
        /// </summary>
        /// <returns>All available Drug Testing Products as JSON</returns>
        [HttpPost]
        public ActionResult GetDrugProducts()
        {

            BALDrug ObjBALDrug = new BALDrug();
            var DrugProducts = ObjBALDrug.GetDrugProducts().Select(p => new DrugTestingProductModel
            {
                pkProductApplicationId = p.pkProductApplicationId,
                pkProductId = (int)p.fkProductId,
                ProductDisplayName = p.ProductDisplayName,
                ProductPerApplicationPrice = p.ProductPerApplicationPrice,
                LongDescription = p.LongDescription,
                IsEnabled = p.IsEnabled != null ? p.IsEnabled : false,
                IsDefault = p.IsDefault != null ? p.IsDefault : false,
                IsGlobal = p.IsGlobal != null ? p.IsGlobal : false,
                IsDeleted = p.IsDeleted != null ? p.IsDeleted : false,
                IsNonMemberRpt = p.IsNonMemberRpt != null ? p.IsNonMemberRpt : false,
                CreatedDate = p.CreatedDate,
                ProductImage = p.ProductImage,
                ProductImagepath = !string.IsNullOrEmpty(p.ProductImage) ? (System.IO.File.Exists(Server.MapPath(@"~/Resources/Upload/Images/ProductImages/" + p.ProductImage)) ? ApplicationPath.GetApplicationPath() + "Resources/Upload/Images/ProductImages/" + p.ProductImage : ApplicationPath.GetApplicationPath() + "Content/themes/base/images/no_image.jpg") : ApplicationPath.GetApplicationPath() + "Content/themes/base/images/no_image.jpg",

                DrugTestCategoryId = p.DrugTestCategoryId,
                DrugTestCategoryName = p.DrugTestCategoryName,
                DrugTestQty = p.DrugTestQty,
                DrugTestPanels = p.DrugTestPanels,
                ProductCode = p.ProductCode,
                SortOrder = p.SortOrder,
                fkReportCategoryId = p.fkReportCategoryId
            }).ToList().OrderBy(p => p.SortOrder);

            return Json(DrugProducts);

        }


        public ActionResult ChangeSortOrder(int pkProductId, int SortOrder, int IsUp)
        {
            int Success = 0;
            try
            {
                BALDrug ObjBALDrug = new BALDrug();
                List<Proc_GetDrugProductforSortChangeResult> objDrugProductforSortChange = new List<Proc_GetDrugProductforSortChangeResult>();
                objDrugProductforSortChange = ObjBALDrug.GetDrugProductforSortChange((byte)SortOrder).ToList();
                int pkProductIdFst = pkProductId;
                byte SOFst = (byte)SortOrder;
                int pkProductIdSec = 0;
                int? SOSec = 0;
                if (IsUp == 1)
                {
                    pkProductIdSec = objDrugProductforSortChange.Where(d => d.Indentifier == 2).Select(d => d.pkProductId).FirstOrDefault().Value;
                    SOSec = objDrugProductforSortChange.Where(d => d.Indentifier == 2).Select(d => d.SortOrder).FirstOrDefault();
                }
                else if (IsUp == 0)
                {
                    pkProductIdSec = objDrugProductforSortChange.Where(d => d.Indentifier == 1).Select(d => d.pkProductId).FirstOrDefault().Value;
                    SOSec = objDrugProductforSortChange.Where(d => d.Indentifier == 1).Select(d => d.SortOrder).FirstOrDefault();
                }
                if (pkProductIdSec != 0)
                {
                    ObjBALDrug.ChangeOrder(pkProductIdFst, pkProductIdSec, SOFst, (byte)SOSec);
                }
            }
            //Added log for exception handling.
            catch (Exception ex)
            {
                VendorServices.GeneralService.WriteLog("Error Occured in ChangeSortOrder(int pkProductId, int SortOrder, int IsUp): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "ManageProduct");
            }
            return Json(Success);

        }

        public ActionResult DeleteDrugProduct(int pkProductId)
        {
            string strSuccess = string.Empty;
            try
            {
                BALDrug ObjBALDrug = new BALDrug();
                string Imagename = string.Empty;
                int success = 0;
                success = ObjBALDrug.DeleteDrugProduct(pkProductId, out Imagename);
                if (success == 1)
                {
                    if (!String.IsNullOrEmpty(Imagename))
                    {
                        if (System.IO.File.Exists(HttpContext.Server.MapPath(@"~\Resources\Upload\Images\ProductImages\") + Imagename))
                        {
                            //Added log for exception handling. Why this is commneted?
                            try
                            {
                                // File.Delete(HttpContext.Current.Server.MapPath(@"~\Resources\Upload\Images\ProductImages\") + Imagename);
                            }
                            catch { }
                        }
                    }
                    strSuccess = "<span class='successmsg'>Product deleted Successfully</span>";
                }
                else
                {
                    strSuccess = "<span class='errormsg'>error is occured while deleting product</span>";
                }
            }
            //Added log for exception handling.
            catch (Exception ex)
            {
                VendorServices.GeneralService.WriteLog("Error Occured in DeleteDrugProduct(int pkProductId): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "ManageProduct");
                //throw;
            }
            finally
            {
                ObjBALDrug = null;
            }
            return Json(new { Message = strSuccess });
        }


        [HttpPost]
        public ActionResult GetDrugTestCategories()
        {

            BALDrug ObjBALDrug = new BALDrug();
            var obj = ObjBALDrug.GetDrugTestCategories();
            return Json(obj);

        }

        [HttpPost]
        public ActionResult UpdateDrugProduct(int pkProductId, bool IsGlobal, bool IsEnabled, bool IsDefault, string ProductName, string ProductCode, string PPPrice, string DrugTestQty, string DrugTestCat, string Description)
        {
            BALDrug ObjBALDrug = new BALDrug();
            tblProduct ObjtblProduct = new tblProduct();
            tblProductPerApplication ObjtblProductPerApplication = new tblProductPerApplication();
            int success = 0;
            string sPnl = string.Empty;
            string strSuccess = string.Empty;
            try
            {
                success = ObjBALDrug.CheckProductCode(ProductCode, pkProductId);
                if (success == 0)
                {
                    try
                    {

                        #region tblproduct
                        ObjtblProduct.CoveragePDFPath = string.Empty;
                        ObjtblProduct.fkVendorId = null;
                        ObjtblProduct.IsDeleted = false;
                        ObjtblProduct.IsEnabled = (IsEnabled == true) ? true : false;
                        ObjtblProduct.IsInstantReport = null;
                        ObjtblProduct.IsLiveRunner = false;
                        ObjtblProduct.IsThirdPartyFee = false;
                        ObjtblProduct.IsUSAProduct = false;
                        ObjtblProduct.LastModifiedDate = DateTime.Now; ;
                        ObjtblProduct.pkProductId = pkProductId;
                        ObjtblProduct.ProductCode = ProductCode;
                        ObjtblProduct.ProductDefaultPrice = Convert.ToDecimal(PPPrice);
                        ObjtblProduct.ProductDescription = Description;
                        ObjtblProduct.ProductName = ProductName;
                        ObjtblProduct.SortOrder = null;
                        ObjtblProduct.VendorNotes = string.Empty;
                        #endregion

                        #region tblproductperapplications
                        ObjtblProductPerApplication.ApplicationId = Utility.SiteApplicationId;
                        ObjtblProductPerApplication.fkProductId = pkProductId;
                        ObjtblProductPerApplication.DrugTestCategoryId = Convert.ToByte(DrugTestCat);
                        ObjtblProductPerApplication.DrugTestPanels = sPnl;
                        ObjtblProductPerApplication.DrugTestQty = DrugTestQty;
                        ObjtblProductPerApplication.fkReportCategoryId = Convert.ToByte(Emerge.Services.ReportCategories.DrugTesting);
                        ObjtblProductPerApplication.IsDefault = (IsDefault == true) ? true : false;
                        ObjtblProductPerApplication.IsDeleted = false;
                        ObjtblProductPerApplication.IsEnabled = (IsEnabled == true) ? true : false;
                        ObjtblProductPerApplication.IsGlobal = (IsGlobal == true) ? true : false;
                        ObjtblProductPerApplication.IsNonMemberRpt = (bool?)true;
                        ObjtblProductPerApplication.LongDescription = Utility.GetTextWithLength(Description, 1024);
                        ObjtblProductPerApplication.LastModifiedDate = DateTime.Now;
                        //ObjtblProductPerApplication.pkProductApplicationId = Guid.NewGuid();
                        ObjtblProductPerApplication.ProductBulletPoint = null;
                        ObjtblProductPerApplication.ProductDisplayName = ProductName;
                        //string sFileName = string.Empty; ;

                        //ObjtblProductPerApplication.ProductImage = sFileName;
                        ObjtblProductPerApplication.ProductPerApplicationPrice = Convert.ToDecimal(PPPrice);
                        ObjtblProductPerApplication.ShortDescription = "";
                        #endregion

                        success = ObjBALDrug.UpdateDrugProduct(ObjtblProduct, ObjtblProductPerApplication);
                        if (success == 1)
                        {
                            strSuccess = "<span class='successmsg'>Product updated Successfully</span>";
                        }
                        else
                        {
                            strSuccess = "<span class='errormsg'>error is occured while updating product</span>";
                        }

                    }
                    catch (Exception ex)
                    {
                        VendorServices.GeneralService.WriteLog("Error Occured in UpdateDrugProduct(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "ManageProduct");
                        throw;
                    }
                    finally
                    {
                        ObjBALDrug = null;
                        ObjtblProduct = null;
                        ObjtblProductPerApplication = null;
                    }
                }
                else
                {
                    strSuccess = "<span class='errormsg'>Product Code already exist</span>";
                }
            }
            //Added log for exception handling.
            catch (Exception ex)
            {
                VendorServices.GeneralService.WriteLog("Error Occured in UpdateDrugProduct(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "ManageProduct");
            }

            //Response.StatusCode = 500;
            return Json(new { Message = strSuccess });
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddDrugTestingProduct(string chk_IsGlobal, string chk_IsEnabled, string chk_IsDefault, string txtProductDisplayName, string txtDescription, string ddlProductCategory, string txtAddProductCode, string txtPrice, string hdAddQuantity, HttpPostedFileBase fpAddProductImage)
        {
            string strResult = string.Empty;

            BALDrug ObjBALDrug = new BALDrug();
            tblProduct ObjtblProduct = new tblProduct();
            tblProductPerApplication ObjtblProductPerApplication = new tblProductPerApplication();
            int success = 0;
            //Added log for exception handling.
            try
            {

                success = ObjBALDrug.CheckProductCode(txtAddProductCode, 0);
                if (success == 0)
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(hdAddQuantity))
                        {
                            hdAddQuantity = hdAddQuantity.Replace("##", ",");
                            hdAddQuantity = hdAddQuantity.Replace("#", "");
                        }
                        #region tblproduct

                        ObjtblProduct.CoveragePDFPath = string.Empty;
                        ObjtblProduct.CreatedDate = DateTime.Now;
                        ObjtblProduct.fkVendorId = null;
                        ObjtblProduct.IsDeleted = false;
                        ObjtblProduct.IsEnabled = true; //(chkFActive.Checked == true) ? true : false; /* This is by default enabled true.   */
                        ObjtblProduct.IsInstantReport = null;
                        ObjtblProduct.IsLiveRunner = false;
                        ObjtblProduct.IsThirdPartyFee = false;
                        ObjtblProduct.IsUSAProduct = false;
                        ObjtblProduct.LastModifiedDate = null;
                        //ObjtblProduct.pkProductId = Guid.NewGuid();
                        ObjtblProduct.ProductCode = txtAddProductCode.Trim();
                        ObjtblProduct.ProductDefaultPrice = Convert.ToDecimal(txtPrice.Trim());
                        ObjtblProduct.ProductDescription = txtDescription.Trim();
                        ObjtblProduct.ProductName = txtProductDisplayName.Trim();
                        ObjtblProduct.SortOrder = null;
                        ObjtblProduct.VendorNotes = string.Empty;

                        #endregion

                        #region tblproductperapplications

                        ObjtblProductPerApplication.ApplicationId = Utility.SiteApplicationId;
                        ObjtblProductPerApplication.CreatedDate = DateTime.Now;
                        ObjtblProductPerApplication.DrugTestCategoryId = Convert.ToByte(ddlProductCategory);
                        ObjtblProductPerApplication.DrugTestPanels = string.Empty;
                        ObjtblProductPerApplication.DrugTestQty = hdAddQuantity;
                        ObjtblProductPerApplication.fkReportCategoryId = Convert.ToByte(Emerge.Common.ReportCategories.DrugTesting);
                        ObjtblProductPerApplication.IsDefault = (chk_IsDefault != null) ? true : false;
                        ObjtblProductPerApplication.IsDeleted = false;
                        ObjtblProductPerApplication.IsEnabled = (chk_IsEnabled != null) ? true : false;
                        ObjtblProductPerApplication.IsGlobal = (chk_IsGlobal != null) ? true : false;
                        ObjtblProductPerApplication.IsNonMemberRpt = (bool?)true;
                        ObjtblProductPerApplication.LongDescription = Utility.GetTextWithLength(txtDescription.Trim(), 1024);
                        //ObjtblProductPerApplication.pkProductApplicationId = Guid.NewGuid();
                        ObjtblProductPerApplication.ProductBulletPoint = null;
                        ObjtblProductPerApplication.ProductDisplayName = txtProductDisplayName.Trim();
                        ObjtblProductPerApplication.ProductCode = txtAddProductCode.Trim();
                        string sExt = string.Empty, sFileName = string.Empty, sFilePath = string.Empty;
                        if (fpAddProductImage != null)
                        {
                            if (CheckImageExtensions(fpAddProductImage, ".jpg, .gif, .bmp, .jpeg, .png"))
                            {
                                sExt = Path.GetExtension(fpAddProductImage.FileName).ToLower();
                                sFileName = txtAddProductCode.Trim() + "_" + DateTime.Now.ToFileTime().ToString() + sExt;

                                sFilePath = HttpContext.Server.MapPath("~/Resources/Upload/Images/ProductImages/");
                                if (!System.IO.Directory.Exists(sFilePath))
                                {
                                    System.IO.Directory.CreateDirectory(sFilePath);
                                }
                                if (System.IO.File.Exists(HttpContext.Server.MapPath(@"~\Resources\Upload\Images\ProductImages\") + sFileName))
                                {
                                    try
                                    {
                                        System.IO.File.Delete(HttpContext.Server.MapPath(@"~\Resources\Upload\Images\ProductImages\") + sFileName);
                                    }
                                    //Added log for exception handling.
                                    catch (Exception ex)
                                    {
                                        VendorServices.GeneralService.WriteLog("Error Occured in AddDrugTestingProduct(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "ManageProduct");
                                        throw;
                                    }
                                }

                                fpAddProductImage.SaveAs(HttpContext.Server.MapPath(@"~\Resources\Upload\Images\ProductImages\") + sFileName);

                            }
                        }

                        ObjtblProductPerApplication.ProductImage = sFileName;
                        ObjtblProductPerApplication.ProductPerApplicationPrice = Convert.ToDecimal(txtPrice.Trim());
                        ObjtblProductPerApplication.ShortDescription = "";

                        #endregion

                        success = ObjBALDrug.AddDrugProduct(ObjtblProduct, ObjtblProductPerApplication);
                        if (success == 1)
                        {
                            strResult = "<span class='successmsg'>Product added Successfully</span>";
                        }
                        else
                        {
                            strResult = "<span class='errormsg'>Error is occured while adding product</span>";
                        }
                    }
                    //Added log for exception handling.
                    catch (Exception ex)
                    {
                        strResult = "<span class='errormsg'>Some error is occured while adding product. Please try again!</span>";
                        VendorServices.GeneralService.WriteLog("Error Occured in AddDrugTestingProduct(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "ManageProduct");
                    }
                    finally
                    {
                        ObjBALDrug = null;
                        ObjtblProduct = null;
                        ObjtblProductPerApplication = null;
                    }
                }
                else
                {
                    strResult = "<span class='errormsg'>Product Code already exist</span>";
                }
            }
            //Added log for exception handling.
            catch (Exception ex)
            {
                strResult = "<span class='errormsg'>Some error is occured while adding product. Please try again!</span>";
                VendorServices.GeneralService.WriteLog("Error Occured in AddDrugTestingProduct(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "ManageProduct");
            }

            //string sQty = string.Empty;
            //string sPnl = string.Empty;

            TempData["message"] = strResult;
            return RedirectToAction("ManageProducts");
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UploadDrugProductImage(HttpPostedFileBase ProductImage, string hdProductIdimage, string hdPreviousimageFile)
        {
            string strResult = string.Empty;
            BALProducts ObjBALProducts = new BALProducts();
            try
            {
                if (ProductImage != null)
                {
                    if (ProductImage != null && ProductImage.ContentLength > 0)
                    {
                        if (ProductImage.FileName != string.Empty)
                        {
                            {
                                if (CheckImageExtensions(ProductImage, ".jpg, .gif, .bmp, .jpeg, .png"))
                                {
                                    string Pcode = "";
                                    int ProductId = 0;
                                    string[] ch = new string[] { "##" };

                                    string[] arr = hdProductIdimage.Split(ch, StringSplitOptions.RemoveEmptyEntries);
                                    if (arr.Length == 2)
                                    {
                                        Pcode = arr[0];
                                        ProductId = Int32.Parse(arr[1].ToString());
                                    }
                                    string sExt = Path.GetExtension(ProductImage.FileName).ToLower();
                                    string sFileName = Pcode + "_" + DateTime.Now.ToFileTime().ToString() + sExt;
                                    string sFilePath = "";
                                    sFilePath = HttpContext.Server.MapPath("~/Resources/Upload/Images/ProductImages/");

                                    if (!System.IO.Directory.Exists(sFilePath))
                                    {
                                        System.IO.Directory.CreateDirectory(sFilePath);
                                    }
                                    if (System.IO.File.Exists(HttpContext.Server.MapPath(@"~\Resources\Upload\Images\ProductImages\") + sFileName))
                                    {
                                        try
                                        {
                                            System.IO.File.Delete(HttpContext.Server.MapPath(@"~\Resources\Upload\Images\ProductImages\") + sFileName);
                                        }
                                        //Added log for exception handling.
                                        catch (Exception ex)
                                        {
                                            VendorServices.GeneralService.WriteLog("Error Occured in UploadDrugProductImage(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "ManageProduct");
                                        }
                                    }
                                    ProductImage.SaveAs(HttpContext.Server.MapPath(@"~\Resources\Upload\Images\ProductImages\") + sFileName);

                                    int success = ObjBALProducts.UploadProductImage(ProductId, sFileName);
                                    if (success == 1)
                                    {
                                        if (hdPreviousimageFile != null)
                                        {
                                            if (!String.IsNullOrEmpty(hdPreviousimageFile))
                                            {
                                                if (System.IO.File.Exists(HttpContext.Server.MapPath(@"~\Resources\Upload\Images\ProductImages\") + hdPreviousimageFile))
                                                {
                                                    try
                                                    {
                                                        System.IO.File.Delete(HttpContext.Server.MapPath(@"~\Resources\Upload\Images\ProductImages\") + hdPreviousimageFile);
                                                    }
                                                    //Added log for exception handling.
                                                    catch (Exception ex)
                                                    {
                                                        VendorServices.GeneralService.WriteLog("Error Occured in UploadDrugProductImage(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "ManageProduct");
                                                        throw;
                                                    }
                                                }
                                            }
                                        }
                                        strResult = "<span class='successmsg'>Image Uploaded Successfully</span>";
                                    }
                                    else
                                    {
                                        strResult = "<span class='errormsg'>Some error occurred while uploading. Please try again!</span>";
                                    }
                                }
                                else
                                {
                                    strResult = "<span class='errormsg'>Only these extensions(.jpg, .gif, .bmp, .jpeg, .png) are allowed to upload image.</span>";
                                }
                            }
                        }

                    }
                }
            }
            //Added log for exception handling.
            catch (Exception ex)
            {
                strResult = "<span class='errormsg'>Some error occurred while uploading. Please try again!</span>";
                VendorServices.GeneralService.WriteLog("Error Occured in UploadDrugProductImage(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "ManageProduct");
            }
            TempData["message"] = strResult;
            return RedirectToAction("ManageProducts");
        }

        private bool CheckImageExtensions(HttpPostedFileBase ObjControl, string sExtensions)
        {
            string Extension = string.Empty;
            string[] sDelimeter = new string[] { "," };
            string[] arrSplit = sExtensions.Split(sDelimeter, StringSplitOptions.None);
            bool bMode = false;

            if (ObjControl.FileName != string.Empty)
            {
                Extension = Path.GetExtension(ObjControl.FileName).ToLower();
                if (ObjControl.ContentLength > 0)
                {
                    for (int iRow = 0; iRow < arrSplit.Length; iRow++)
                    {
                        if (Extension == arrSplit[iRow].Trim().ToLower())
                        {
                            bMode = true;
                            break;
                        }
                    }
                    if (!bMode)
                    {
                        return false;
                    }
                }
            }
            return true;
        }


        [HttpPost]
        public ActionResult SaveDrugprice(decimal SalivaPrice, decimal EZcupPrice)
        {

            string strResult = string.Empty;
            try
            {
                int Result = 0;
                BALDrug Objbaldrub = new BALDrug();
                Result = Objbaldrub.UpdateDrugPrice(SalivaPrice, EZcupPrice);
                if (Result == 1)
                {
                    strResult = "<span class='successmsg'>In-House Drug Testing Kits successfully Updated.</span>";
                }
                else
                {
                    strResult = "<span class='error'>error is occured while updating In-House Drug Testing Kits.</span>";
                }
            }
            //Added log for exception handling.
            catch (Exception ex)
            {
                strResult = "<span class='error'>error is occured while updating In-House Drug Testing Kits.</span>";
                VendorServices.GeneralService.WriteLog("Error Occured in UploadDrugProductImage(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "ManageProduct");
            }
            return Json(new { Message = strResult });

        }



    }
}
