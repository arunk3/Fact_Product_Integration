﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Emerge.Models;
using Emerge.Data;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {

        /// <summary>
        /// Get all list of Globle filter for NCR1 report
        /// </summary>
        /// <returns></returns>
        public ActionResult GlobleFilter()
        {

            GlobleFilterModel objGlobalFilter = new GlobleFilterModel();
            objGlobalFilter.Keywords = string.Empty;
            return View(objGlobalFilter);
        }

        /// <summary>
        /// Bind Globle filter in kendo grid used by this action
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>

        public ActionResult BindGlobleFilter(int page, int pageSize)
        {
            BALGlobalFilters objBalGloable = new BALGlobalFilters();
            List<GlobleFilterModel> lstObject = new List<GlobleFilterModel>();

            //Get collection data and bind in model
            try
            {
                var CollectionData = objBalGloable.GetGlobleFilters();
                if (CollectionData.Count > 0 && CollectionData != null)
                {

                    for (int index = 0; index < CollectionData.Count; index++)
                    {
                        lstObject.Add(new GlobleFilterModel
                        {
                            GlobleFilterId = CollectionData.ElementAt(index).PkGlobleFilterId,
                            ReportType = CollectionData.ElementAt(index).ReportsType,
                            Keywords = (CollectionData.ElementAt(index).Source + "," + CollectionData.ElementAt(index).DocketNumber + "," + CollectionData.ElementAt(index).DeliveryAddress).TrimEnd(',').TrimStart(','),
                            IsEnable = CollectionData.ElementAt(index).IsEnabled
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(new { FilterValue = lstObject, TotalCount = lstObject.Count() }, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Add new globle filter in kendo grid
        /// </summary>
        /// <param name="Collection"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddGlobleFilters(FormCollection Collection)
        {
            //string Keywords = string.Empty;
            string source = string.Empty;
            string docketNumber = string.Empty;
            string deliveryAddress = string.Empty;
            string Msg = string.Empty;
            string Css = string.Empty;
            int Result = 0;
            BALGlobalFilters objBalGloableFilter = new BALGlobalFilters();
            tblGlobleFilter objTblGlobleFilter = new tblGlobleFilter();
            //Keywords = Collection["Keywords"];
            source = Collection["Source"];
            docketNumber = Collection["DocketNumber"];
            deliveryAddress = Collection["DeliveryAddress"];
            try
            {
                
                    objTblGlobleFilter.Source = source;
                    objTblGlobleFilter.DocketNumber = docketNumber;
                    objTblGlobleFilter.DeliveryAddress = deliveryAddress;
                //insert in database                
                    Result = objBalGloableFilter.InsertGlobleFilter(objTblGlobleFilter);
                if (Result == 1)
                {
                    Msg = "Insertion successfully";
                    Css = "successmsg";
                }
                else
                {
                    Msg = "Some error occured during the insertion";
                    Css = "errormsgSMALL";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


            return Json(new { Message = Msg, Css = Css });
        }
        /// <summary>
        /// Get update page used this controller
        /// </summary>
        /// <returns></returns>
        public ActionResult UpdateGlobleFilters()
        {
            BALGlobalFilters objBalGloable = new BALGlobalFilters();
            GlobleFilterModel objGlobalFilterModel = new GlobleFilterModel();
            int GloblefilterId = 0;
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["_a"]))         //  _a Contains Filter ID
                {
                    string Filter_Id = Request.QueryString["_a"].ToString();
                    GloblefilterId = int.Parse(Filter_Id);
                    //Get specific filter 
                    var CollectionData = objBalGloable.GetGlobleFilters(GloblefilterId);
                    if (CollectionData != null)
                    {

                        objGlobalFilterModel.Source = CollectionData.Source;
                        objGlobalFilterModel.DocketNumber = CollectionData.DocketNumber != null ? CollectionData.DocketNumber : null;
                        objGlobalFilterModel.DeliveryAddress = CollectionData.DeliveryAddress != null ? CollectionData.DeliveryAddress : null;
                        objGlobalFilterModel.GlobleFilterId = CollectionData.PkGlobleFilterId;                        
                        objGlobalFilterModel.IsEnable = CollectionData.IsEnabled;
                        objGlobalFilterModel.ReportType = CollectionData.ReportsType;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View(objGlobalFilterModel);
        }
        /// <summary>
        /// Delete Globle filter from database
        /// </summary>
        /// <param name="GloblefilterId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DeleteGlobleFilters(int GloblefilterId)
        {
            BALGlobalFilters objBalGloable = new BALGlobalFilters();
            string Message = string.Empty;
            string Css = string.Empty;
            try
            {
                //delete method call
                int Result = objBalGloable.DeleteGlobleFilters(GloblefilterId);
                if (Result == 1)
                {
                    Message = "Delete Successfully";
                    Css = "successmsg";
                }
                else
                {
                    Message = "Some error occured while Delete globle filter";
                    Css = "errormsgSMALL";
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(new { Msg = Message, css = Css });
        }
        /// <summary>
        /// update globle filter in database
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Update_GloableFilters(FormCollection collection)
        {
            BALGlobalFilters objBalGloable = new BALGlobalFilters();
            tblGlobleFilter objTblGolobleFilter = new tblGlobleFilter();
            bool IsEnabled = false;
            string Message = string.Empty;
            string Css = string.Empty;
            int GlobleFilterId = 0;
            string source = string.Empty;
            string docketNumber = string.Empty;
            string delivaryAddress = string.Empty;
            //string KeyWords = string.Empty;

            
            string FilterId = collection["GlobleFilterId"];
            source = collection["Source"];
            docketNumber = collection["DocketNumber"];
            delivaryAddress = collection["DeliveryAddress"];
            string IsEnable = collection["Enabled_Chk"];
            GlobleFilterId = int.Parse(FilterId);
            IsEnabled = bool.Parse(IsEnable);

            objTblGolobleFilter.PkGlobleFilterId = GlobleFilterId;
            objTblGolobleFilter.Source = source;
            objTblGolobleFilter.DocketNumber = docketNumber;
            objTblGolobleFilter.DeliveryAddress = delivaryAddress;
            objTblGolobleFilter.IsEnabled = IsEnabled;
            
            try
            {
                //udpate method call
                int Result = objBalGloable.UpdateGlobleFilters(objTblGolobleFilter);
                if (Result == 1)
                {
                    Message = "Update Successfully";
                    Css = "successmsg";
                }
                else
                {
                    Message = "Some error occured while updating globle filter";
                    Css = "errormsgSMALL";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(new { Msg = Message, css = Css });
        }

    }
}
