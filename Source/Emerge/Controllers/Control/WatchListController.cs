﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using System.Web.Security;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /WatchList/

        public ActionResult PartialWatchList()
        {
            return View();
        }


        public ActionResult UpdateWatchListStatus(int? OrderId)
        {

            int status = 0;
            BALEmergeReview objBALEmergeReview = new BALEmergeReview();
            status = objBALEmergeReview.UpdateWatchListStatusIndb(OrderId, 1);
            return Json(status);
        }
        public ActionResult WatchListReportBind(int page, int pageSize)
        {
            int PageSize = pageSize;
            int iPageNo = page;

            byte WatchListStatus = 1;

            BALEmergeReview ObjReview = new BALEmergeReview();
            var WatchListCollection = ObjReview.GetReportsForWatchList(0,
                                                                      0,
                                                                      0,
                                                                      0,
                                                                      "OrderDt",
                                                                      "DESC",
                                                                      true,
                                                                      iPageNo,
                                                                      PageSize,
                                                                      string.Empty,
                                                                      string.Empty,
                                                                      string.Empty,
                                                                      0, WatchListStatus);
            int TotalRec = 0;
            if (WatchListCollection.Count > 0)
            {
                TotalRec = WatchListCollection.Count;
            }
            return Json(new
            {
                Products = WatchListCollection,
                TotalCount = TotalRec
            }, JsonRequestBehavior.AllowGet);
        }
    }
}
