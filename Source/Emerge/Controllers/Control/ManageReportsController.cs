﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using System.IO;
using Emerge.Common;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        public ActionResult ManageReports()
        {
            ManageReportModel ObjManageReportModel = new ManageReportModel();
       
            ViewData["Code"] = Request.QueryString["code"];
            ViewBag.data = Request.QueryString["code"];

          
            if (TempData["message"] != null)
            {
                ObjManageReportModel.strMessage = TempData["message"].ToString();
            }

            return View(ObjManageReportModel);     
            
            // 1) Create Three PopUp Window On Click Of three Field Of Kendo Grid In Manage Reports Page Of E-merge 2) Display Check Box In Title of Kendo Grid In Of All Countries Page OF E-Merge
        }



        /// <summary>
        /// This method is used to bind product categories
        /// </summary>
        public ActionResult BindProductCategories()
        {
            string strResult = string.Empty;

            BALGeneral ObjBALGeneral = new BALGeneral();
            List<Proc_Get_ReportCategoriesResult> ObjColl = new List<Proc_Get_ReportCategoriesResult>();
            try
            {
                 ObjColl = ObjBALGeneral.GetReportCategories().Where(t => t.pkReportCategoryId != Convert.ToByte(Emerge.Services.ReportCategories.DrugTesting) && t.pkReportCategoryId != Convert.ToByte(Emerge.Services.ReportCategories.OtherAdjustmentsReports)).ToList();
                 var item2 = ObjColl.First(s => s.CategoryName == "Other Tools");
                 var item1 = ObjColl.First(s => s.CategoryName == "eScreen");
                 if (item2 != null) { ObjColl.Remove(item2); }
                 if (item1 != null) { ObjColl.Remove(item1); }
                 if (item1 != null) { ObjColl.Add(item1); }
                 if (item2 != null) { ObjColl.Add(item2); }
                
                if (ObjColl.Count > 0)
                {
                    //rptProductCategories.DataSource = ObjColl;
                    //rptProductCategories.DataBind();
                }
                else
                {
                    strResult = "<span class='infomessage'>Record not found.</span>";
                }
            }
            catch //(Exception ex)
            {
                strResult = "<span class='errormsg'>Some problem while fetching data.</span>";                
            }
            finally
            {
                ObjBALGeneral = null;
            }
            return Json(ObjColl);
        }
        /// <summary>
        /// This method is used to save data in database
        /// </summary>
        public ActionResult SaveDatainDB(IEnumerable<ManageReportModel> ObjModel)
        {
            string strResult = string.Empty;

            BALProducts ObjBALProducts = new BALProducts();
            bool bUpdate = false;

            try
            {
                for (int iRow = 0; iRow < ObjModel.Count(); iRow++)
                {

                    #region ObjModel Varibales
                   // if (ObjModel.ElementAt(iRow).pkProductApplicationId == null || (ObjModel.ElementAt(iRow).pkProductId == null || ObjModel.ElementAt(iRow).IsGlobal == null || ObjModel.ElementAt(iRow).IsDefault == null || ObjModel.ElementAt(iRow).IsEnabled == null || ObjModel.ElementAt(iRow).ProductDisplayName == null || ObjModel.ElementAt(iRow).ProductName == null || ObjModel.ElementAt(iRow).IsLiveRunner == null || ObjModel.ElementAt(iRow).IsEnableReview == null))
                    if (ObjModel.ElementAt(iRow).pkProductApplicationId == null || (ObjModel.ElementAt(iRow).pkProductId == null || ObjModel.ElementAt(iRow).ProductDisplayName == null || ObjModel.ElementAt(iRow).ProductName == null))
                    { 
                        continue; 
                    }

                    if (ObjModel.ElementAt(iRow).ProductDisplayName == string.Empty)
                    {
                        strResult = "<span class='errormsg'>Product display name can not be blank for product : " + ObjModel.ElementAt(iRow).ProductName + "</span>";                              
                        break;
                    }

                    #endregion

                    #region Product Info

                    tblProduct ObjProduct = new tblProduct();
                    ObjProduct.pkProductId = ObjModel.ElementAt(iRow).pkProductId;
                    ObjProduct.VendorNotes = string.Empty;
                    ObjProduct.IsLiveRunner = ObjModel.ElementAt(iRow).IsLiveRunner;
                    ObjProduct.IsEnableReview = ObjModel.ElementAt(iRow).IsEnableReview;

                    #endregion

                    #region Product Per App Info

                    tblProductPerApplication ObjProductPerApplication = new tblProductPerApplication();
                    ObjProductPerApplication.pkProductApplicationId = ObjModel.ElementAt(iRow).pkProductApplicationId;
                    ObjProductPerApplication.ProductDisplayName = ObjModel.ElementAt(iRow).ProductDisplayName.Replace("'", "").Trim();
                    ObjProductPerApplication.IsGlobal = ObjModel.ElementAt(iRow).IsGlobal;
                    ObjProductPerApplication.IsEnabled =  ObjModel.ElementAt(iRow).IsEnabled;
                    ObjProductPerApplication.IsDefault = ObjModel.ElementAt(iRow).IsDefault;
                    ObjProductPerApplication.DoNotShow = ObjModel.ElementAt(iRow).DoNotShow;
                    ObjProductPerApplication.ProductPerApplicationPrice = (ObjModel.ElementAt(iRow).ProductPerApplicationPrice == 0) ? 0 : ObjModel.ElementAt(iRow).ProductPerApplicationPrice;


                    #endregion

                    #region Call DB Method

                    int iResult = ObjBALProducts.SaveProductInfo(ObjProduct, ObjProductPerApplication);

                    if (iResult < 0)
                    {
                        strResult = "<span class='errormsg'>Some problem while updating product : " + ObjModel.ElementAt(iRow).ProductName + "</span>";         
                      
                        break;
                    }
                    bUpdate = true;

                    #endregion


                }

                if (bUpdate)
                {
                    strResult = "<span class='successmsg'>Product Information successfully updated.</span>";

                    BindProductCategories();
                }
            }
            catch
            {
                strResult = "<span class='errormsg'>Some problem while updating data.</span>";
               
            }
            finally
            {
                ObjBALProducts = null;
            }
            Response.StatusCode = 500;
            return Json(new { Message = strResult });
        }

        [HttpPost]
        public ActionResult GetReportslist(byte pkCompanyId)
        {
            // byte pkid = Convert.ToByte(pkId);

            BALCompany serobj = new BALCompany();
          //  var List = serobj.GetAdminReport(pkCompanyId);
           
            var List1 = serobj.GetAdminReport(pkCompanyId).Select(p => new Proc_Get_EmergeReportsForAdminResult 
           {

             ProductCode= p.ProductCode,
             pkProductId=p.pkProductId ,
             IsGlobal=p.IsGlobal,
             IsDefault=p.IsDefault,
             IsEnabled=p.IsEnabled,
             CategoryName=p.CategoryName,
             CoveragePDFPath=p.CoveragePDFPath,
             DoNotShow=p.DoNotShow,
             IsEnableReview=p.IsEnableReview,
             IsDeleted=p.IsDeleted,
             IsLiveRunner=p.IsLiveRunner,
             IsThirdPartyFee=p.IsThirdPartyFee,
             pkProductApplicationId=p.pkProductApplicationId,
             ProductDefaultPrice=p.ProductDefaultPrice,
             ProductDescription=p.ProductDescription,
             ProductIsDeleted=p.ProductIsDeleted,
             ProductDisplayName=p.ProductDisplayName,
             ProductIsEnabled=p.ProductIsEnabled,
             ProductName=p.ProductName,
             ProductPerApplicationPrice=p.ProductPerApplicationPrice,
             ReportCategoryId=p.ReportCategoryId,
             SortImageOrder=p.SortImageOrder,
             SortOrder=p.SortOrder,
             VendorId=p.VendorId,
             IsSampleReportExist = (System.IO.File.Exists(Server.MapPath(@"~/Resources/SampleReports/" + p.ProductCode + "_SampleReport.html"))) ? 1 : 0,
             VendorName=p.VendorName,
             VendorNotes=p.VendorNotes}).ToList().OrderBy(p => p.SortOrder);
            

      
            return Json(List1);
        }



        public ActionResult SortOrderChange(string pkProductId, int SortOrder, int IsUP, int Reportcatid)
        {

            int Success = 0;
            
            //****** COMMENTting this section as it was not returning any data in actionresuly and only returns 'sucess'
            /*BALProducts ObjProduct = new BALProducts();
            BALDrug ObjBAL = new BALDrug();

            List<Proc_UpdateProductSortOrderResult> objProductforSortChange = new List<Proc_UpdateProductSortOrderResult>();
            objProductforSortChange = ObjProduct.UpdateProductSortOrder(Reportcatid, (byte)SortOrder, Convert.ToInt32(pkProductId), IsUP);
           */
            return Json(Success);
        }


          [HttpPost]
        public ActionResult UpdateReports(IEnumerable<ManageReportModel> ObjReportsUpdate)
        {
            BALProducts ObjBALProducts = new BALProducts();
             string strMessage = "";

            for (int i = 0; i < ObjReportsUpdate.Count(); i++)
            {
                tblProduct ObjProduct = new tblProduct();
                ObjProduct.pkProductId = ObjReportsUpdate.ElementAt(i).pkProductId;
                 ObjProduct.VendorNotes = string.Empty;
                ObjProduct.IsLiveRunner = ObjReportsUpdate.ElementAt(i).IsLiveRunner;
                ObjProduct.IsEnableReview = ObjReportsUpdate.ElementAt(i).IsEnableReview;


                tblProductPerApplication ObjProductPerApplication = new tblProductPerApplication();


                ObjProductPerApplication.pkProductApplicationId = ObjReportsUpdate.ElementAt(i).pkProductApplicationId;
                ObjProductPerApplication.ProductDisplayName = ObjReportsUpdate.ElementAt(i).ProductDisplayName;
                ObjProductPerApplication.IsEnabled = ObjReportsUpdate.ElementAt(i).IsEnabled;
                ObjProductPerApplication.IsDefault = ObjReportsUpdate.ElementAt(i).IsDefault;
                ObjProductPerApplication.IsGlobal = ObjReportsUpdate.ElementAt(i).IsGlobal;
                ObjProductPerApplication.DoNotShow = ObjReportsUpdate.ElementAt(i).DoNotShow;
                ObjProductPerApplication.ProductPerApplicationPrice = Convert.ToDecimal(ObjReportsUpdate.ElementAt(i).ProductPerApplicationPrice);


                int Result = ObjBALProducts.SaveProductInfo(ObjProduct, ObjProductPerApplication);

                if (Result == 1)
                {
                    strMessage = "<span class='successmsg'>Product Information successfully updated.</span>";
                }
                else
                {
                    strMessage = "<span class='errormsg'>Some problem while updating data.</span>";
                }


            }
            Response.StatusCode = 500;
            return Json(new { Message = strMessage, Class = "successmsg" });
        }

        [HttpPost]


        public ActionResult ReadSecondGrid1(IEnumerable<ManageReportModel> models)
        {

            //BALCompany serobj = new BALCompany();
           // ManageReportModel objmode = new ManageReportModel();
            // objmode
            var List = "";
            //  = serobj.GetAdminReport(pkid);

            return Json(List);
        }


        public ActionResult UpdateAdminReport(IEnumerable<ManageReportModel> products)
        {
            return RedirectToAction("NestedGRID", "EmergeNestedGrid");

        }

        [HttpPost]
        public ActionResult SaveDesc(int pkid, string descrption)
        {

            //string strResult = string.Empty;
            using (EmergeDALDataContext db = new EmergeDALDataContext())
            {
                var cust =
                            (from c in db.tblProducts
                             where c.pkProductId == pkid
                             select c).First();
                cust.ProductDescription = descrption;


                db.SubmitChanges();
            }

            return RedirectToAction("NestedGRID", "EmergeNestedGrid");
        }

        public ActionResult DeleteNestedGrid()
        {
            return RedirectToAction("NestedGRID", "EmergeNestedGrid");
        }

        [HttpPost]
        public ActionResult GetDescrption(int pkid)
        {
            BALCompany serobj = new BALCompany();
            ManageReportModel objmodel = new ManageReportModel();
            objmodel.GetProductDescrption1 = serobj.GetProductDecrption(pkid);
            return Json(new { Deccrption = objmodel.GetProductDescrption1.ElementAt(0).ProductDescription });


        }
        public ActionResult StateFees(string code)
        {
           // BALCompany serobj = new BALCompany();

            return RedirectToAction("NestedGRID", "EmergeNestedGrid");

        }


        public ActionResult SaveCovergeUpload(IEnumerable<HttpPostedFileBase> attachments)
        {
            
           var lablel = "";
            foreach (var file in attachments)
            {
                var ext = Path.GetExtension(file.FileName);
                if (ext == ".pdf")
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var physicalPath = Path.Combine(Server.MapPath("~/Resources/Upload/CoveragePDF"), fileName);
                    file.SaveAs(physicalPath);
                }
                else
                {
                    lablel = "only PDF Upload";
                    //   Response.Write("only PDF File Allowed Upload");
                    //  return Json(lablel);
                    return Content("only PDF Upload");
                }
            }

            // Return an empty string to signify success
            return Content("");
        }

        [HttpPost]
        public ActionResult RemoveCovergeUploadFile(string[] fileNames)
        {
            // The parameter of the Remove action must be called "fileNames"
            foreach (var fullName in fileNames)
            {
                var fileName = Path.GetFileName(fullName);
                var physicalPath = Path.Combine(Server.MapPath("~/Resources/Upload/CompanyPDF"), fileName);

                // TODO: Verify user permissions
                if (System.IO.File.Exists(physicalPath))
                {
                    System.IO.File.Delete(physicalPath);
                }
            }
            // Return an empty string to signify success
            return Content("");
        }
        //public ActionResult ReadSecondGrid(int pkid)
        //{

        //   BALProdcusts serobj = new BALProdcusts();
        //    kendoGridModel objmode = new kendoGridModel();
        //    // objmode
        //    var List = serobj.GetSubRecord(pkid);

        //    return Json(List);
        //}



        [HttpPost]
        public ActionResult Destroy(IEnumerable<ManageReportModel> records)
        {
            return View();
        }


        [HttpPost]
        public ActionResult Create(IEnumerable<ManageReportModel> records)
        {
            return RedirectToAction("NestedGRID", "EmergeNestedGrid");
        }
        [HttpPost]
        public ActionResult Update(FormCollection records)
        {



            return RedirectToAction("Index", "TelerikGrid");
        }

        public ActionResult GetSampleReport(string ProductCode, string ReportName)
        {
            string strResult = string.Empty;

            //string SampleReportText = "<link rel=\"stylesheet\" type=\"text/css\" href=\"../../Content/CSS/Report.css\"/>";
            string SampleReportText = "<link rel=\"stylesheet\" type=\"text/css\" href=\"" + ApplicationPath.GetApplicationPath() + "Resources/Css/Report.css?v1" + "\"/>";
            string FileContent = "";
           
            FileContent = Utility.GetSampleReportContent(ProductCode + "_SampleReport");
            if (FileContent == "")
            {
                FileContent = Utility.GetSampleReportContent("Common_SampleReport");
                FileContent = FileContent.Replace("%%ReportType%%", ProductCode).Replace("%%ReportName%%", ReportName);
            }
            SampleReportText += FileContent;
          
            strResult = SampleReportText;

           
            return Json(new { SampleReport = strResult });
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UploadCoveragePDF(ManageReportModel ObjModel, HttpPostedFileBase CoveragePDFFile, string hdProductIdPDF, string hdPreviousPDFFile)
        {
            string strMessage = string.Empty;
            BALProducts ObjBALProducts = new BALProducts();
            if (CoveragePDFFile != null)
            {
               
                if (CoveragePDFFile != null && CoveragePDFFile.ContentLength > 0)
                {
                    if (CheckPhotoExtensions(CoveragePDFFile))
                    {
                        
                        string Pcode = "";
                        int ProductId = 0;
                        string[] ch = new string[] { "##" };

                        string[] arr = hdProductIdPDF.Split(ch, StringSplitOptions.RemoveEmptyEntries);
                        if (arr.Length == 2)
                        {
                            Pcode = arr[0];
                            ProductId = Int32.Parse(arr[1].ToString());
                        }
                        string sExt = Path.GetExtension(CoveragePDFFile.FileName).ToLower();
                        string sFileName = Pcode + "_" + DateTime.Now.ToFileTime().ToString() + sExt;
                        string sFilePath = "";

                        sFilePath = HttpContext.Server.MapPath("~/Resources/Upload/CoveragePDF/");

                        if (!System.IO.Directory.Exists(sFilePath))
                        {
                            System.IO.Directory.CreateDirectory(sFilePath);
                        }
                        if (System.IO.File.Exists(HttpContext.Server.MapPath(@"~\Resources\Upload\CoveragePDF\") + sFileName))
                        {
                            try
                            {
                                System.IO.File.Delete(HttpContext.Server.MapPath(@"~\Resources\Upload\CoveragePDF\") + sFileName);
                            }
                            catch { }
                        }
                        CoveragePDFFile.SaveAs(HttpContext.Server.MapPath(@"~\Resources\Upload\CoveragePDF\") + sFileName);

                        int success = ObjBALProducts.UploadCoveragePDF(ProductId, sFileName);
                       
                        if (success == 1)
                        {
                            strMessage = "<span class='successmsg'>File Uploaded Successfully</span>";

                            if (hdPreviousPDFFile != null)
                            {
                                if (!String.IsNullOrEmpty(hdPreviousPDFFile))
                                {
                                    try
                                    {
                                        System.IO.File.Delete(HttpContext.Server.MapPath(@"~\Resources\Upload\CoveragePDF\") + hdPreviousPDFFile);
                                    }
                                    catch { }
                                }
                            }

                        }
                        else
                        {
                            strMessage = "<span class='errormsg'>Some error occur while uploading, please try later</span>";
                        }
                    }
                    else
                    {
                        //
                    }
                    
                }
            }
            TempData["message"] = strMessage;
            return RedirectToAction("ManageReports");
        }


        public bool CheckPhotoExtensions(HttpPostedFileBase fUploader)
        {
            string Extension = string.Empty;

            if (fUploader.FileName != string.Empty)
            {
                Extension = Path.GetExtension(fUploader.FileName).ToLower();
                if (fUploader.ContentLength > 0)
                {
                    if (Extension == ".pdf")
                    { }
                    else
                    {
                        return false;
                    }
                }
            }
            return true;
        }

    }
     
}
