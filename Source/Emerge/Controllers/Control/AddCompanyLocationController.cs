﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using System.Web.Security;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        DateTime currentDate = DateTime.Now;
        Dictionary<int, string> ObjGuid_ProductIds;
        List<bool> IsLiveRunner;
        int ObjGuid_LocationId = 0;
        List<int> ObjGuid_LocationIdColl;
        int CompanyID = 0;
        //string dateTimeToGenerateCode = DateTime.Now.ToString("yyMMddHHmmss");

        public ActionResult AddCompanyLocation()
        {
            BALLocation ObjBALLocation = new BALLocation();
            BALCompany ObjBALCompany = new BALCompany();
            tblCompany ObjtbCompany = new tblCompany();
            LocationModel ObjLocationModel = new LocationModel();
            if (Request.QueryString["lid"] != null && Request.QueryString["lid"] != Guid.Empty.ToString())
            {
                ObjLocationModel.IsEdit = true;
                ObjLocationModel.ddlCompanyEnabled = "disabled";
                ViewBag.data = Request.QueryString["lid"];
                ViewBag.locationid = Convert.ToInt32(Request.QueryString["lid"]);
                ViewData["LocationId"] = Request.QueryString["lid"];
                Session["pkLocationIdWhenEdit"] = Convert.ToInt32(Request.QueryString["lid"]);
                ObjLocationModel.LocationId = Convert.ToInt32(Request.QueryString["lid"]);
                ObjGuid_LocationId = Convert.ToInt32(Request.QueryString["lid"]);
                var CompanyLocation = ObjBALLocation.GetLocationDetailByLocationId(ObjGuid_LocationId);
                if (CompanyLocation != null && CompanyLocation.Count > 0)
                {
                    ViewBag.CompanyId = CompanyLocation.ElementAt(0).fkCompanyId;
                    ViewData["PkCompanyId"] = CompanyLocation.ElementAt(0).fkCompanyId;
                    ObjLocationModel.Enabled = CompanyLocation.ElementAt(0).IsEnabled;
                    ObjLocationModel.pkCompanyId = CompanyLocation.ElementAt(0).fkCompanyId.GetValueOrDefault();
                    ObjLocationModel.Address1 = CompanyLocation.ElementAt(0).Address1;
                    ObjLocationModel.Address2 = CompanyLocation.ElementAt(0).Address2;
                    ObjLocationModel.fkStateId = Convert.ToInt32(CompanyLocation.ElementAt(0).fkStateID);
                    ObjLocationModel.CityName = CompanyLocation.ElementAt(0).City;
                    ObjLocationModel.ZipCode = CompanyLocation.ElementAt(0).ZipCode;
                    ObjLocationModel.LocationContact = CompanyLocation.ElementAt(0).LocationContact;
                    ObjLocationModel.LocationNote = CompanyLocation.ElementAt(0).LocationNote;
                    ObjLocationModel.PhoneNo = CompanyLocation.ElementAt(0).PhoneNo1;
                    ObjLocationModel.PhoneNo2 = CompanyLocation.ElementAt(0).PhoneNo2;
                    ObjLocationModel.FaxNumber = CompanyLocation.ElementAt(0).Fax;
                    ObjLocationModel.ReportEmailNotification = CompanyLocation.ElementAt(0).LocationEmail;
                    ObjLocationModel.Syncpod = CompanyLocation.ElementAt(0).IsSyncpod;
                    ObjLocationModel.IsReportEmailActivated = CompanyLocation.ElementAt(0).IsReportEmailActivated;
                    ObjLocationModel.IsGlobalCredential = CompanyLocation.ElementAt(0).IsGlobalCredential;
                    ObjLocationModel.IsCustomCity = CompanyLocation.ElementAt(0).IsCustomCity;
                    ObjLocationModel.SecondaryPhone = CompanyLocation.ElementAt(0).SecondaryPhone;
                    ObjLocationModel.SecondaryFax = CompanyLocation.ElementAt(0).SecondaryFax;
                    ObjLocationModel.State_Code = GetStateCode(Convert.ToInt32(CompanyLocation.ElementAt(0).fkStateID));
                    ObjtbCompany = ObjBALCompany.GetCompanyByCompanyId(CompanyLocation.ElementAt(0).fkCompanyId.GetValueOrDefault());
                    ObjLocationModel.CompanyName = !string.IsNullOrEmpty(ObjtbCompany.CompanyName) ? ObjtbCompany.CompanyName : string.Empty; //;

                    if (CompanyLocation.ElementAt(0).IsCustomCity)
                    {
                        ObjLocationModel.CityName = CompanyLocation.ElementAt(0).City.Trim();
                        ObjLocationModel.CustomCityName = CompanyLocation.ElementAt(0).City.Trim();//"---Custom---";
                    }
                    else
                    {
                        ObjLocationModel.CustomCityName = string.Empty;
                        ObjLocationModel.CityName = CompanyLocation.ElementAt(0).City.Trim();
                    }
                }
            }
            else
            {
                ObjLocationModel.IsCustomCity = false;
                ObjLocationModel.CustomCityName = string.Empty;
                ObjLocationModel.IsEdit = false;
                if (!string.IsNullOrEmpty(Request.QueryString["_c"]))
                {
                    ObjLocationModel.ddlCompanyEnabled = "disabled";
                    ObjLocationModel.pkCompanyId = Convert.ToInt32(Request.QueryString["_c"]);
                    int cmpId = Convert.ToInt32(Request.QueryString["_c"]);
                    ViewBag.CompanyId = cmpId;
                    ViewData["PkCompanyId"] = cmpId;
                    ObjtbCompany = ObjBALCompany.GetCompanyByCompanyId(cmpId);
                    ObjLocationModel.CompanyName = ObjtbCompany.CompanyName;
                }
                else
                {
                    ObjLocationModel.ddlCompanyEnabled = string.Empty;
                    ObjLocationModel.pkCompanyId = 0;
                    ViewBag.CompanyId = 0;
                    ObjLocationModel.CompanyName = string.Empty;
                }
                ObjLocationModel.LocationId = 0;
                ObjLocationModel.Enabled = true;
                ObjLocationModel.IsGlobalCredential = false;
                ViewBag.locationid = 0;
                ObjLocationModel.SecondaryPhone = string.Empty;
                ObjLocationModel.SecondaryFax = string.Empty;
            }
            if (!string.IsNullOrEmpty(Request.QueryString["_al"]))
            {
                string Action = Request.QueryString["_al"];
                switch (Action)
                {
                    case "2":
                        ViewBag.Message = "<span class='successmsg'>Location updated successfully.</span>";
                        break;
                    case "1":
                        ViewBag.Message = "<span class='successmsg'>Location added successfully.</span>";
                        break;
                    case "-1":
                        ViewBag.Message = "<span class='errormsg'>Some error occuerred.</span>";
                        break;
                }
            }
            return View(ObjLocationModel);

        }

        [HttpPost]
        public ActionResult GetCompany()
        {
            BALCompany ObjBALCompany = new BALCompany();
            var Company = ObjBALCompany.GetAllCompany(Utility.SiteApplicationId, true, false).ToList();
            return Json(Company);
        }

        [HttpPost]
        public ActionResult GetStates()
        {
            BALGeneral Obj = new BALGeneral();
            var State = Obj.GetStates().ToList();
            return Json(State);

        }

        [HttpPost]
        public ActionResult GetCities(int fkStateId)
        {

            BALLocation ObjLocation = new BALLocation();
            List<SelectListItem> list = new List<SelectListItem>();
            var obj = ObjLocation.GetCityStatewise(fkStateId);
            if (obj != null)
            {
                list = obj.Select(x => new SelectListItem { Text = x.CityName, Value = x.CityName.ToString() }).ToList();
            }
            //list.Insert(0, new SelectListItem { Text = "Select", Value= "-1" });
            list.Add(new SelectListItem { Text = "---------------", Value = "-2" });
            list.Add(new SelectListItem { Text = "---Custom---", Value = "-3" });
            return Json(list);
        }

        //  [HttpPost]
        public ActionResult GetAllUsersByLocation(int page, int pageSize, int fkCompanyId, int fkLocationId)
        {
            BALCompanyUsers objBALCompanyUsers = new BALCompanyUsers();
            int TotalRec = 0;
            int PageSize = pageSize;
            int iPageNo = page;
            var list = objBALCompanyUsers.GetAllEmergeUsersForAdmin(iPageNo, PageSize, false, -1, fkCompanyId, fkLocationId, "FirstName", "ASC", string.Empty, 0, string.Empty, out TotalRec);
            return Json(new { Reclist = list, TotalRec = list.Count }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveLocation(LocationModel ObjLocationModel)
        {
            //BALCompany ObjBALCompany = new BALCompany();
           // bool IsLocationDuplicate = false;
            string Result = string.Empty;
            string Save = "0";
            string Action = "1";
            try
            {
                if (ObjLocationModel.LocationId != 0)
                {
                    int LocationId = UpdateLocation(ObjLocationModel);
                    Session["pkNewLocationId"] = ObjLocationModel.LocationId;
                    Save = "1";
                    Action = "2";
                    Result = ObjLocationModel.LocationId.ToString();
                    //return Json(new { Save = "1", strResult = "<span class='successmsg'>Location updated successfully.</span>", Id = ObjLocationModel.LocationId });
                }
                else
                {
                    int LocationId = addLocation(ObjLocationModel);
                    Session["pkNewLocationId"] = LocationId;
                    AddProductsForNewLocation(ObjLocationModel.pkCompanyId, LocationId);
                    AddCustomPackagesForNewLocation(LocationId);
                    AddUniversalPackagesForNewLocation(LocationId);
                    Save = "1";
                    Result = LocationId.ToString();
                    //return Json(new { Save = "1", strResult = "<span class='successmsg'>Location added successfully.</span>", Id = LocationId });
                }
            }
            catch
            {
                Action = "-1";
            }
            return Json(new { Save = Save, Id = Result, Action = Action });
        }

        public void DeleteAllProducts(int ObjLocationId)
        {
            BALLocation ObjBALLocation = new BALLocation();

            try
            {
                ObjBALLocation.DeleteProductsOfLocation(ObjLocationId);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                ObjBALLocation = null;
            }
        }

        private void AddProductsForNewLocation(int PkCompanyId, int LocationId)
        {
            ObjGuid_ProductIds = new Dictionary<int, string>();
            IsLiveRunner = new List<bool>();
            List<SubReportsModel> data = new List<SubReportsModel>();
            //List<tblCompany_Price> ObjtblCompany_Price_List = new List<tblCompany_Price>();
            List<tblProductAccess> deletetblProductAccess = new List<tblProductAccess>();
            List<tblProductAccess> ObjtblProductAccess_List = new List<tblProductAccess>();
            BALLocation ObjBALLocation = new BALLocation();
            BALCompany ObjBALCompany = new BALCompany();
            int count = 0;
            bool GlobalCredential = false;
            GlobalCredential = ObjBALCompany.IsEnableGlobalCredentialForCompany(PkCompanyId);
            var categories = ObjBALCompany.GetReportCategories();
            if (categories.Count > 0)
            {
                foreach (var category in categories)
                {
                    if (GlobalCredential == true)
                    {
                        int HomeLocationId = ObjBALCompany.CompanyHomeLocationByCompanyId(PkCompanyId); ;
                        var List = ObjBALCompany.GetReportByIdatEdit(HomeLocationId, category.pkReportCategoryId, PkCompanyId);
                        if (List.Count > 0)
                        {
                            if (data.Count > 0)
                            {
                                count = data.Count - 1;
                            }
                            data.InsertRange(count, List.Select(x => new SubReportsModel
                            {
                                pkProductApplicationId = x.pkProductApplicationId,
                                pkProductId = x.pkProductId,
                                ProductAccessId = x.ProductAccessId,
                                AutoReviewStatusForLocation = (x.AutoReviewStatusForLocation != null) ? x.AutoReviewStatusForLocation.ToString() : "0",
                                ProductDisplayName = x.ProductDisplayName,
                                ProductName = x.ProductName,
                                ProductCode = x.ProductCode,
                                ProductDescription = x.ProductDescription,
                                CategoryName = x.CategoryName,
                                CompanyPrice = (x.CompanyPrice != null) ? decimal.Parse(x.CompanyPrice.ToString()) : 0,
                                CoveragePDFPath = x.CoveragePDFPath,
                                IsDefault = x.IsProductEnabled,
                                IsDefaultPrice = x.IsDefaultPrice != null ? x.IsDefaultPrice : true,
                                IsLiveRunnerLocation = x.IsLiveRunnerLocation,
                                IsLiveRunnerProduct = x.IsLiveRunnerProduct,
                                IsThirdPartyFee = x.IsThirdPartyFee,
                                EnableReviewPerCompany = (x.EnableReviewPerCompany == null) ? false : bool.Parse(x.EnableReviewPerCompany.ToString()),
                                LinkedReportId = x.LinkedReportId,
                                ReportsLinked = x.ReportsLinked,
                            }).ToList());
                        }
                    }
                    else
                    {
                        var List = ObjBALCompany.GetReportProductsById(LocationId, category.pkReportCategoryId);

                        if (List.Count > 0)
                        {
                            if (data.Count > 0)
                            {
                                count = data.Count - 1;
                            }
                            data.InsertRange(count, List.Select(x => new SubReportsModel
                            {
                                pkProductApplicationId = x.pkProductApplicationId,
                                pkProductId = x.pkProductId,
                                ProductAccessId = x.ProductAccessId,
                                AutoReviewStatusForLocation = (x.AutoReviewStatusForLocation != null) ? x.AutoReviewStatusForLocation.ToString() : "0",
                                ProductDisplayName = x.ProductDisplayName,
                                ProductName = x.ProductName,
                                ProductCode = x.ProductCode,
                                ProductDescription = x.ProductDescription,
                                CategoryName = x.CategoryName,
                                CompanyPrice = x.CompanyPrice,
                                CoveragePDFPath = x.CoveragePDFPath,
                                //IsProductEnabled = x.IsDefault != null? Convert.ToBoolean(x.IsDefault): false,
                                IsDefault = x.IsDefault,
                                IsDefaultPrice = x.IsDefaultPrice,
                                IsLiveRunnerLocation = x.IsLiveRunnerLocation,
                                IsLiveRunnerProduct = x.IsLiveRunnerProduct,
                                IsThirdPartyFee = x.IsThirdPartyFee,
                                LinkedReportId = x.LinkedReportId,
                                ReportsLinked = x.ReportsLinked
                            }).ToList());
                        }
                    }
                }
            }
            for (int iRow = 0; iRow < data.Count; iRow++)
            {
                //if (data.ElementAt(iRow).IsDefault != null && Convert.ToBoolean(data.ElementAt(iRow).IsDefault))
                //{
                tblProductAccess ObjtblProductAccess = new tblProductAccess();
                ObjtblProductAccess.fkLocationId = LocationId;
                ObjtblProductAccess.fkProductApplicationId = data.ElementAt(iRow).pkProductApplicationId;
                ObjtblProductAccess.IsLiveRunner = data.ElementAt(iRow).IsLiveRunnerProduct;
                ObjtblProductAccess.ProductReviewStatus = (data.ElementAt(iRow).AutoReviewStatusForLocation != null) ? byte.Parse(data.ElementAt(iRow).AutoReviewStatusForLocation) : byte.MinValue;
                ObjtblProductAccess.IsProductEnabled = data.ElementAt(iRow).IsDefault != null ? Convert.ToBoolean(data.ElementAt(iRow).IsDefault) : false;
                ObjtblProductAccess.LocationProductPrice = data.ElementAt(iRow).CompanyPrice;
                ObjtblProductAccess_List.Add(ObjtblProductAccess);
                //}
            }
            ObjBALLocation.InsertUpdateLocationProduct(ObjtblProductAccess_List, deletetblProductAccess);

        }

        public int addLocation(LocationModel ObjLocationModel)
        {
            //tblCompany ObjtblCompany = new tblCompany();
            tblLocation ObjtblLocation = new tblLocation();
            int Result = 0;

            //BALCompany ObjBALCompany = new BALCompany();
            BALLocation ObjBALLocation = new BALLocation();
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            try
            {

                ObjtblLocation.Address1 = ObjLocationModel.Address1;
                ObjtblLocation.Address2 = ObjLocationModel.Address2 == null ? string.Empty : ObjLocationModel.Address2;

                if (ObjLocationModel.CityName != "-2" && ObjLocationModel.CityName != "-3")
                {
                    ObjtblLocation.City = ObjLocationModel.CityName;
                    ObjtblLocation.IsCustomCity = false;
                }
                else
                {
                    ObjtblLocation.City = ObjLocationModel.CustomCityName;
                    ObjtblLocation.IsCustomCity = true;
                }

                string ext1 = ObjLocationModel.PhoneNo;
                string ext2 = ObjLocationModel.PhoneNo2 == null ? string.Empty : ObjLocationModel.PhoneNo2;
                //var db = 
                    ObjBALCompanyUsers.GetUserInformationByMembershipUserId(new Guid(Utility.GetUID(User.Identity.Name).ToString()));


                // #19
                // ObjtblLocation.CreatedById = db.First().pkCompanyUserId;
                ObjtblLocation.CreatedDate = currentDate;
                ObjtblLocation.Ext1 = (ext1.Length > 3 ? ext1.Substring(0, 3) : "");
                ObjtblLocation.Ext2 = (ext2.Length > 3 ? ext2.Substring(0, 3) : "");
                ObjtblLocation.Fax = ObjLocationModel.FaxNumber == null ? string.Empty : ObjLocationModel.FaxNumber;
                ObjtblLocation.fkStateID = ObjLocationModel.fkStateId;
                ObjtblLocation.IsEnabled = ObjLocationModel.Enabled;
                ObjtblLocation.LocationNote = ObjLocationModel.LocationNote == null ? string.Empty : ObjLocationModel.LocationNote; ;
                ObjtblLocation.IsHome = false;
                ObjtblLocation.LocationContact = ObjLocationModel.LocationContact == null ? string.Empty : ObjLocationModel.LocationContact;
                ObjtblLocation.LocationCode = Utility.GenerateLocationCode(ObjLocationModel.CompanyName, currentDate);
                ObjtblLocation.PhoneNo1 = ObjLocationModel.PhoneNo;
                ObjtblLocation.PhoneNo2 = ObjLocationModel.PhoneNo2 == null ? string.Empty : ObjLocationModel.PhoneNo2;
                ObjtblLocation.ZipCode = ObjLocationModel.ZipCode; /*Value of txtzipCode.Text is not used because it is causing problems due to read only mode*/
                // ObjtblLocation.LocationEmail = txtLocationEmail.Text.Trim();
                ObjtblLocation.IsSyncpod = ObjLocationModel.Syncpod;
                if (ObjLocationModel.IsReportEmailActivated == true)
                {
                    ObjtblLocation.LocationEmail = ObjLocationModel.ReportEmailNotification == null ? string.Empty : ObjLocationModel.ReportEmailNotification;
                    ObjtblLocation.IsReportEmailActivated = ObjLocationModel.IsReportEmailActivated;
                }
                else
                {
                    ObjtblLocation.LocationEmail = string.Empty;
                    ObjtblLocation.IsReportEmailActivated = false;
                }

                ObjtblLocation.SecondaryFax = ObjLocationModel.SecondaryFax;
                ObjtblLocation.SecondaryPhone = ObjLocationModel.SecondaryPhone;

                Result = ObjBALLocation.InsertNewLocation(ObjLocationModel.pkCompanyId, ObjtblLocation);
                Session["pkNewLocationId"] = Result;
            }
            catch (Exception)
            {

            }
            finally
            {
                //ObjtblCompany = null;
               // ObjBALCompany = null;
                ObjtblLocation = null;
                ObjBALLocation = null;
            }
            return Result;
        }

        private int UpdateLocation(LocationModel ObjLocationModel)
        {
            //tblCompany ObjtblCompany = new tblCompany();
            tblLocation ObjtblLocation = new tblLocation();
            int? Result = 0;

           // BALCompany ObjBALCompany = new BALCompany();
            BALLocation ObjBALLocation = new BALLocation();
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            try
            {

                ObjtblLocation.Address1 = ObjLocationModel.Address1;
                ObjtblLocation.Address2 = ObjLocationModel.Address2 == null ? string.Empty : ObjLocationModel.Address2;

                if (ObjLocationModel.CityName != "-2" && ObjLocationModel.CityName != "-3")
                {
                    ObjtblLocation.City = ObjLocationModel.CityName;
                    ObjtblLocation.IsCustomCity = false;
                }
                else
                {
                    ObjtblLocation.City = ObjLocationModel.CustomCityName;
                    ObjtblLocation.IsCustomCity = true;
                }
                string ext1 = ObjLocationModel.PhoneNo;
                string ext2 = ObjLocationModel.PhoneNo2 == null ? string.Empty : ObjLocationModel.PhoneNo2;
               // var db = 
                    ObjBALCompanyUsers.GetUserInformationByMembershipUserId(new Guid(Utility.GetUID(User.Identity.Name).ToString()));


                // #19
                // ObjtblLocation.CreatedById = db.First().pkCompanyUserId;
                ObjtblLocation.CreatedDate = currentDate;
                ObjtblLocation.Ext1 = (ext1.Length > 3 ? ext1.Substring(0, 3) : "");
                ObjtblLocation.Ext2 = (ext2.Length > 3 ? ext2.Substring(0, 3) : "");
                ObjtblLocation.Fax = ObjLocationModel.FaxNumber == null ? string.Empty : ObjLocationModel.FaxNumber;
                ObjtblLocation.fkStateID = ObjLocationModel.fkStateId;
                ObjtblLocation.LocationNote = ObjLocationModel.LocationNote == null ? string.Empty : ObjLocationModel.LocationNote;
                ObjtblLocation.IsEnabled = ObjLocationModel.Enabled;
                ObjtblLocation.LocationContact = ObjLocationModel.LocationContact == null ? string.Empty : ObjLocationModel.LocationContact;
                //ObjtblLocation.LocationCode = Utility.GenerateLocationCode(ObjLocationModel.CompanyName, currentDate);
                ObjtblLocation.PhoneNo1 = ObjLocationModel.PhoneNo;
                ObjtblLocation.PhoneNo2 = ObjLocationModel.PhoneNo2 == null ? string.Empty : ObjLocationModel.PhoneNo2;
                ObjtblLocation.ZipCode = ObjLocationModel.ZipCode; /*Value of txtzipCode.Text is not used because it is causing problems due to read only mode*/
                // ObjtblLocation.LocationEmail = txtLocationEmail.Text.Trim();
                ObjtblLocation.pkLocationId = ObjLocationModel.LocationId;
                Session["pkNewLocationId"] = ObjLocationModel.LocationId;
                ObjtblLocation.fkCompanyId = ObjLocationModel.pkCompanyId;

                ObjtblLocation.IsSyncpod = ObjLocationModel.Syncpod;
                if (ObjLocationModel.IsReportEmailActivated == true)
                {
                    ObjtblLocation.LocationEmail = ObjLocationModel.ReportEmailNotification == null ? string.Empty : ObjLocationModel.ReportEmailNotification;
                    ObjtblLocation.IsReportEmailActivated = ObjLocationModel.IsReportEmailActivated;
                }
                else
                {
                    ObjtblLocation.LocationEmail = string.Empty;
                    ObjtblLocation.IsReportEmailActivated = false;
                }
                ObjtblLocation.SecondaryFax = ObjLocationModel.SecondaryFax;
                ObjtblLocation.SecondaryPhone = ObjLocationModel.SecondaryPhone;

                Result = ObjBALLocation.UpdateLocation(ObjtblLocation, ObjtblLocation.fkCompanyId.GetValueOrDefault());/*fkCompanyId*/
                if (Result == 1)
                {

                }
                else if (Result == -5)
                {

                }
                else if (Result == -3)
                {

                }
            }
            catch (Exception)
            {
                //lblMessageBox.Visible = true;
                //lblMessageBox.Text = "Error Occured While Updating Record";
                //lblMessageBox.CssClass = "errormsg";
            }
            finally
            {
                //ObjtblCompany = null;
               // ObjBALCompany = null;
                ObjtblLocation = null;
                ObjBALLocation = null;
            }
            return ObjLocationModel.LocationId;
        }
        [HttpPost]
        public ActionResult UpdateReportperLocation(IEnumerable<SubReportsModel> Detail, string CID, string LocationId)
        {

            //List<tblCompany_Price> listtblCompany_Price = new List<tblCompany_Price>();
            List<tblProductAccess> listtblProductAccess = new List<tblProductAccess>();
            List<tblProductAccess> deletetblProductAccess = new List<tblProductAccess>();
            //BALCompany ObjBALCompany = new BALCompany();
            BALLocation ObjBALLocation = new BALLocation();
            if (Detail != null)
            {
                for (int iCol = 0; iCol < Detail.Count(); iCol++)
                {
                    //if (Detail.ElementAt(iCol).IsDefault != null && Convert.ToBoolean(Detail.ElementAt(iCol).IsDefault) || (Convert.ToBoolean(!Detail.ElementAt(iCol).IsDefault) && byte.Parse(Detail.ElementAt(iCol).AutoReviewStatusForLocation) != 0))
                    //{
                    tblProductAccess ObjtblProductAccess = new tblProductAccess();
                    ObjtblProductAccess.pkProductAccessId = Detail.ElementAt(iCol).pkProductApplicationId;
                    ObjtblProductAccess.fkLocationId = (Convert.ToInt32(Session["pkNewLocationId"]) != null) ? Convert.ToInt32(Session["pkNewLocationId"]) : 0;
                    ObjtblProductAccess.fkProductApplicationId = Detail.ElementAt(iCol).pkProductApplicationId;
                    ObjtblProductAccess.IsLiveRunner = (Detail.ElementAt(iCol).IsLiveRunnerLocation != null) ? Convert.ToBoolean(Detail.ElementAt(iCol).IsLiveRunnerLocation) : false;
                    ObjtblProductAccess.ProductReviewStatus = (Detail.ElementAt(iCol).AutoReviewStatusForLocation != null) ? byte.Parse(Detail.ElementAt(iCol).AutoReviewStatusForLocation) : byte.MinValue;
                    ObjtblProductAccess.IsProductEnabled = Detail.ElementAt(iCol).IsDefault != null ? Convert.ToBoolean(Detail.ElementAt(iCol).IsDefault) : false;
                    ObjtblProductAccess.LocationProductPrice = Convert.ToDecimal(Detail.ElementAt(iCol).CompanyPrice);
                    listtblProductAccess.Add(ObjtblProductAccess);

                }

                ObjBALLocation.InsertUpdateLocationProduct(listtblProductAccess, deletetblProductAccess);
            }
            return View();

            //return RedirectToAction("AddCompanies", "Control", new { id =123});

        }

        //Updated for INT101
        protected int AddCustomPackagesForNewLocation(int LocationId)
        {
            BALCompany ObjBalCompany = new BALCompany();
            BALProductPackages ObjBALProductPackages = new BALProductPackages();
            int IResult = -1;
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    try
                    {
                        if (LocationId != 0)
                        {
                            var tbl = ObjBalCompany.GetCompanyIdByLocationId(LocationId);
                            if (tbl != null)
                            {
                                CompanyID = tbl.fkCompanyId.GetValueOrDefault();
                            }

                            int homeLocationID = DX.tblLocations.Where(loc => loc.fkCompanyId == CompanyID && loc.IsHome == true).Select(loc => loc.pkLocationId).FirstOrDefault();

                            List<tblProductPackage> lstCopyHome = DX.tblProductPackages.Where(d => d.fkCompanyId == CompanyID && d.IsDelete == false && d.fkLocationid == homeLocationID).ToList<tblProductPackage>();
                            if (lstCopyHome.Count > 0)
                            {
                                for (int row = 0; row < lstCopyHome.Count; row++)
                                {
                                    tblProductPackage tblpp = new tblProductPackage();
                                    tblpp.fkApplicationId = lstCopyHome.ElementAt(row).fkApplicationId;
                                    tblpp.ParentPackageId = lstCopyHome.ElementAt(row).ParentPackageId;
                                    tblpp.PackageName = lstCopyHome.ElementAt(row).PackageName;
                                    tblpp.PackageCode = lstCopyHome.ElementAt(row).PackageCode;
                                    tblpp.PackageDisplayName = lstCopyHome.ElementAt(row).PackageDisplayName;
                                    tblpp.PackageDescription = lstCopyHome.ElementAt(row).PackageDescription;
                                    tblpp.PackagePrice = lstCopyHome.ElementAt(row).PackagePrice;
                                    tblpp.IsEnabled = lstCopyHome.ElementAt(row).IsEnabled;
                                    tblpp.IsDelete = lstCopyHome.ElementAt(row).IsDelete;
                                    tblpp.IsPublic = lstCopyHome.ElementAt(row).IsPublic;
                                    tblpp.IsDefault = lstCopyHome.ElementAt(row).IsDefault;
                                    tblpp.CreatedDate = DateTime.Now;
                                    tblpp.LastModifiedDate = DateTime.Now;
                                    tblpp.PackageMemberType = lstCopyHome.ElementAt(row).PackageMemberType;
                                    tblpp.PackageLongDescription = lstCopyHome.ElementAt(row).PackageLongDescription;
                                    tblpp.PackageBulletPoints = lstCopyHome.ElementAt(row).PackageBulletPoints;
                                    tblpp.PackageNonMemberPrice = lstCopyHome.ElementAt(row).PackageNonMemberPrice;
                                    tblpp.fkRootPackageId = lstCopyHome.ElementAt(row).fkRootPackageId;
                                    tblpp.include = lstCopyHome.ElementAt(row).include;
                                    tblpp.access = lstCopyHome.ElementAt(row).access;
                                    tblpp.NewPackageId = lstCopyHome.ElementAt(row).NewPackageId;
                                    tblpp.OldPackageId = lstCopyHome.ElementAt(row).OldPackageId;
                                    tblpp.IsEnableCompanyPackage = lstCopyHome.ElementAt(row).IsEnableCompanyPackage;
                                    tblpp.ReportsInclude = lstCopyHome.ElementAt(row).ReportsInclude;
                                    tblpp.fkCompanyId = lstCopyHome.ElementAt(row).fkCompanyId;
                                    tblpp.fkLocationid = LocationId;
                                    DX.tblProductPackages.InsertOnSubmit(tblpp);
                                    DX.SubmitChanges();
                                    int OldPackageId = lstCopyHome.ElementAt(row).pkPackageId;

                                    var ch = DX.tblTieredPackages.Where(tr => tr.fkPackageId == OldPackageId).FirstOrDefault();
                                    if (ch != null)
                                    {
                                        PackagesModel ObjModel = new PackagesModel();
                                        ObjModel.LocationId = LocationId;
                                        ObjModel.CompanyId = (lstCopyHome.ElementAt(row).fkCompanyId) != null ? Convert.ToInt32(lstCopyHome.ElementAt(row).fkCompanyId) : 0;
                                        ObjModel.PackageName = lstCopyHome.ElementAt(row).PackageName;
                                        ObjModel.PackageCode = lstCopyHome.ElementAt(row).PackageCode;
                                        ObjModel.PackageDisplayName = lstCopyHome.ElementAt(row).PackageDisplayName;
                                        ObjModel.PackageDescription = lstCopyHome.ElementAt(row).PackageDescription;
                                        ObjModel.IsEnabled = lstCopyHome.ElementAt(row).IsEnabled;
                                        ObjModel.IsDefault = lstCopyHome.ElementAt(row).IsDefault;
                                        ObjModel.IsPublic = lstCopyHome.ElementAt(row).IsPublic;
                                        ObjModel.IsEnableCompanyPackage = lstCopyHome.ElementAt(row).IsEnableCompanyPackage;
                                        ObjModel.CreatedDate = DateTime.Now;
                                        ObjModel.PackagePrice = lstCopyHome.ElementAt(row).PackagePrice;
                                        ObjModel.ddlTieredHost = ch.Host != null ? Convert.ToString(ch.Host) : "0";
                                        ObjModel.ddlTiered2 = ch.Tired2 != null ? Convert.ToString(ch.Tired2) : "0";
                                        ObjModel.ddlTiered3 = ch.Tired3 != null ? Convert.ToString(ch.Tired3) : "0";
                                        ObjModel.ddlTiredOptional = ch.TiredOptional != null ? Convert.ToString(ch.TiredOptional) : "0";
                                        ObjModel.ddlTimeFrame = ch.TimeFrame != null ? Convert.ToString(ch.TimeFrame) : "0";
                                        ObjModel.HostPrice = ch.HostPrice;
                                        ObjModel.IsAutomaticRun = ch.IsAutomaticRun;
                                        if (ch.Variable == "County State")
                                        {
                                            ObjModel.TieredHost = "PS";
                                        }
                                        else if (ch.Variable == "State")
                                        {
                                            ObjModel.TieredHost = "CCR1";
                                        }
                                        else
                                        {
                                            ObjModel.TieredHost = "";
                                        }
                                        ObjModel.OptionalPrice = ch.OptionalPrice;
                                        ObjModel.Tier2Price = ch.Tired2Price != null ? Convert.ToString(ch.Tired2Price) : "0";
                                        ObjModel.Tier3Price = ch.Tired3Price != null ? Convert.ToString(ch.Tired3Price) : "0";
                                        ObjModel.chkTiered = true;
                                        //int iRet = 
                                            InsertTiered(tblpp.pkPackageId, ObjModel);
                                    }
                                }
                            }
                            List<tblProductPackage> lst = DX.tblProductPackages.Where(d => d.fkCompanyId == CompanyID && d.IsDelete == false && d.fkLocationid == LocationId).ToList<tblProductPackage>();
                            List<tblPackageAccess> lstAddCustomPackageAccess = new List<tblPackageAccess>();
                            if (lst.Count > 0)
                            {
                                for (int i = 0; i < lst.Count; i++)
                                {
                                    tblPackageAccess ObjAddPackageAccess = new tblPackageAccess();
                                    ObjAddPackageAccess.pkPackageAccessId = 0;
                                    ObjAddPackageAccess.fkPackageId = lst.ElementAt(i).pkPackageId;
                                    ObjAddPackageAccess.fkLocationId = LocationId;
                                    lstAddCustomPackageAccess.Add(ObjAddPackageAccess);
                                }
                            }
                            IResult = ObjBALProductPackages.AddUniversalPackagesForNewLocation(lstAddCustomPackageAccess);
                        }
                    }
                    catch
                    { }
                }
            }
            catch
            {
            }
            finally
            {
                ObjBALProductPackages = null;
            }
            return IResult;
        }


        //by arun sir

        protected int AddUniversalPackagesForNewLocation(int LocationId)
        {
            BALProductPackages ObjBALProductPackages = new BALProductPackages();
            int IResult = -1;
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    try
                    {
                        if (LocationId != 0)
                        {
                            List<tblProductPackage> lst = DX.tblProductPackages.Where(d => d.fkCompanyId == null && d.IsDelete == false).ToList<tblProductPackage>();

                            List<tblPackageAccess> lstAddUniversalPackageAccess = new List<tblPackageAccess>();
                            if (lst.Count > 0)
                            {
                                for (int i = 0; i < lst.Count; i++)
                                {
                                    tblPackageAccess ObjAddPackageAccess = new tblPackageAccess();
                                    ObjAddPackageAccess.pkPackageAccessId = 0;
                                    ObjAddPackageAccess.fkPackageId = lst.ElementAt(i).pkPackageId;
                                    ObjAddPackageAccess.fkLocationId = LocationId;
                                    lstAddUniversalPackageAccess.Add(ObjAddPackageAccess);
                                }
                            }
                            IResult = ObjBALProductPackages.AddUniversalPackagesForNewLocation(lstAddUniversalPackageAccess);
                        }
                    }
                    catch
                    { }
                }
            }
            catch
            {
            }
            finally
            {
                ObjBALProductPackages = null;
            }
            return IResult;
        }


        public ActionResult GetReportsByLocationId(byte CategoryId, int LocationId, int CompanyId)
        {
            List<SubReportsModel> data = new List<SubReportsModel>();
            int locationid = LocationId;
            int companyid = CompanyId;
            //AdCompanyModel ObjModel = new AdCompanyModel();
            BALGeneral ObjBALGeneral = new BALGeneral();
            List<Proc_Get_ReportsByCategoryIdResult> ObjReportForNewLocation = new List<Proc_Get_ReportsByCategoryIdResult>();
            List<Proc_GetReportProductsByIdResult> ObjReportExistingLocation = new List<Proc_GetReportProductsByIdResult>();
            if (locationid == 0)
            {
                ObjReportForNewLocation = ObjBALGeneral.GetReportsByCategoryId(locationid, CategoryId);
                if (ObjReportForNewLocation.Count > 0)
                {
                    data = ObjReportForNewLocation.Select(x => new SubReportsModel
                    {
                        pkProductApplicationId = x.pkProductApplicationId,
                        pkProductId = x.pkProductId,
                        ProductAccessId = x.ProductAccessId,
                        AutoReviewStatusForLocation = (x.AutoReviewStatusForLocation != null) ? x.AutoReviewStatusForLocation.ToString() : "0",
                        ProductDisplayName = x.ProductDisplayName,
                        ProductName = x.ProductName,
                        ProductCode = x.ProductCode,
                        ProductDescription = x.ProductDescription,
                        CategoryName = x.CategoryName,
                        CompanyPrice = x.CompanyPrice,
                        CoveragePDFPath = x.CoveragePDFPath,
                        IsDefault = x.IsDefault,
                        IsDefaultPrice = x.IsDefaultPrice,
                        IsLiveRunnerLocation = x.IsLiveRunnerLocation != null ? x.IsLiveRunnerLocation : x.IsLiveRunnerProduct,
                        IsLiveRunnerProduct = x.IsLiveRunnerProduct,
                        IsThirdPartyFee = x.IsThirdPartyFee
                    }).OrderBy(x => x.ProductCode).ToList();
                }
                return Json(data);
            }
            else
            {
                ObjReportExistingLocation = ObjBALGeneral.GetReportProductsById(locationid, CategoryId, companyid);
                if (ObjReportExistingLocation.Count > 0)
                {
                    data = ObjReportExistingLocation.Select(x => new SubReportsModel
                    {
                        pkProductApplicationId = x.pkProductApplicationId,
                        pkProductId = x.pkProductId,
                        ProductAccessId = x.ProductAccessId,
                        AutoReviewStatusForLocation = (x.AutoReviewStatusForLocation != null) ? x.AutoReviewStatusForLocation.ToString() : "0",
                        ProductDisplayName = x.ProductDisplayName,
                        ProductName = x.ProductName,
                        ProductCode = x.ProductCode,
                        ProductDescription = x.ProductDescription,
                        CategoryName = x.CategoryName,
                        CompanyPrice = (x.CompanyPrice != null) ? decimal.Parse(x.CompanyPrice.ToString()) : 0,
                        CoveragePDFPath = x.CoveragePDFPath,
                        IsDefault = x.IsProductEnabled,
                        IsDefaultPrice = x.IsDefaultPrice,
                        IsLiveRunnerLocation = x.IsLiveRunnerLocation,
                        IsLiveRunnerProduct = x.IsLiveRunnerProduct,
                        IsThirdPartyFee = x.IsThirdPartyFee,
                        EnableReviewPerCompany = (x.EnableReviewPerCompany == null) ? false : bool.Parse(x.EnableReviewPerCompany.ToString()),
                    }).OrderBy(x => x.ProductCode).ToList();
                }
                return Json(data);
            }
        }

        public ActionResult GetAll_Records()
        {

          //  AdCompanyModel ObjModel = new AdCompanyModel();
            BALCompany serObj = new BALCompany();


            // locationid =new Guid("2e37dd1e-f60e-40cf-82e3-de973d56f5df");
            int locationid = 9514;
            var list = serObj.GetAllReportsLocationId(locationid);



            for (int i = 0; i < list.Count(); i++)
            {





                if (list.ElementAt(i).ProductCode != null)
                {

                    if (list.ElementAt(i).IsLiveRunnerProduct)
                    {

                        if (list.ElementAt(i).IsLiveRunnerLocation == true)
                        {
                            list.ElementAt(i).IsLiveRunnerProduct = true;
                        }
                        else
                        {
                            list.ElementAt(i).IsLiveRunnerProduct = false;
                        }
                    }

                }



            }


            return Json(list);

        }


        public ActionResult GetAllPackageRecords(int CompanyId)
        {

            //PackagesModel objPackagesModel = new PackagesModel();
            BALProductPackages ObjBALProductPackages = new BALProductPackages();
            //ObjGuid_LocationId = Guid.Parse(Session["NewPkCompanyId"].ToString());
            //ObjGuid_LocationId = Convert.ToInt32(Session["pkCompanyId"]);
            ObjGuid_LocationId = Convert.ToInt32((Request.UrlReferrer.Query.Split('&')[0]).Split('=')[1]);
            var ObjData = ObjBALProductPackages.GetEmergeUniversalPackagesForAdmin(ObjGuid_LocationId, Utility.SiteApplicationId, CompanyId);

            for (int i = 0; i < ObjData.Count(); i++)
            {
                if (ObjData.ElementAt(i).IsEnableUniversalPackage == false)
                {
                    ObjData.ElementAt(i).IsDefault = false;


                }
                else
                {
                    if (ObjData.ElementAt(i).PackageAccessId != 0)
                    {
                        if (ObjData.ElementAt(i).IsDefault != Convert.ToBoolean(null))
                        {
                            ObjData.ElementAt(i).IsDefault = true;
                        }
                    }

                }
            }
            return Json(ObjData);

        }

        public ActionResult GetCustomPackages1(int CompanyId)
        {

            BALProductPackages ObjBALProductPackages = new BALProductPackages();


            var ObjData = ObjBALProductPackages.GetEmergeProductPackagesForAdmin(ObjGuid_LocationId, Utility.SiteApplicationId, CompanyId);



            //for (int i = 0; i < ObjData.Count(); i++)
            //{
            //    if (ObjData.ElementAt(i).IsEnableCompanyPackage == false)
            //    {
            //        ObjData.ElementAt(i).IsDefault = false;


            //    }
            //    else
            //    {
            //        if (ObjData.ElementAt(i).PackageAccessId != 0)
            //        {
            //            if (ObjData.ElementAt(i).IsDefault != false)
            //            {
            //                ObjData.ElementAt(i).IsDefault = true;
            //            }
            //        }
            //        ObjData.ElementAt(i).IsDefault = true;
            //    }


            //    ObjData.ElementAt(i).IsDefault = ObjData.ElementAt(i).IsEnabled;


            //}


            //var ObjCollSavedPackages = ObjData.Where(db => db.PackageAccessId != Guid.Empty); 
            //Guid?[] arrSavedPackages = ObjCollSavedPackages.Select(db => db.PackageAccessId).ToArray();




            return Json(ObjData);


        }

        [HttpPost]
        public ActionResult Update_UniversalPackages(string PackagesIds, int loId)
        {

            string strSuccess = string.Empty;

            BALProductPackages ObjBALProductPackages = new BALProductPackages();

            if (loId != 0)
            {
                List<tblPackageAccess> lstAddUniversalPackageAccess = new List<tblPackageAccess>();
                int DeleteUnderScore = PackagesIds.LastIndexOf('_');

                string strDeleteBracket = PackagesIds.Substring(0, DeleteUnderScore != -1 ? DeleteUnderScore : 0).Trim().ToLower();

                string[] str = strDeleteBracket.Split('_');



                if (str.Count() > 0)
                {
                    for (int i = 0; i < str.Count(); i++)
                    {


                        tblPackageAccess ObjAddPackageAccess = new tblPackageAccess();
                        ObjAddPackageAccess.pkPackageAccessId = 0;
                        ObjAddPackageAccess.fkLocationId = loId;
                        ObjAddPackageAccess.fkPackageId = int.Parse(str[i].ToString());
                        lstAddUniversalPackageAccess.Add(ObjAddPackageAccess);


                    }
                }

                var result = ObjBALProductPackages.UpdateUniversalPackagesLocationWise(lstAddUniversalPackageAccess, loId);
                if (result == 1)
                {

                    strSuccess = "<span class='successmsg'>Settings updated successfully.</span>";
                }
                else
                {

                    strSuccess = "<span class='errormsg'>Some error occured while updation.</span>";

                }
            }

            else
            {


            }


            return Json(new { Message = strSuccess });

        }


        public ActionResult DeleteCustomPackages(int Ids)
        {
            BALProductPackages ObjBALProductPackages = new BALProductPackages();
            List<int> ObjPackageIds = new List<int>();
            ObjPackageIds.Add(int.Parse(Ids.ToString()));
            ObjBALProductPackages.ChangePackageDeleteStatus(ObjPackageIds, true);

            return View();


        }

        public ActionResult changestatus(string checke, string typeprop, string pkPackageId)
        {

            int DelUnderScore = checke.LastIndexOf('_');
            string DelBracket = checke.Substring(0, DelUnderScore != -1 ? DelUnderScore : 0).Trim().ToLower();

            string[] check = DelBracket.Split('_');

            int DeleteUnder_Score = pkPackageId.LastIndexOf('_');
            string strDelBracket = pkPackageId.Substring(0, DeleteUnder_Score != -1 ? DeleteUnder_Score : 0).Trim().ToLower();

            string[] PackageId = strDelBracket.Split('_');

            if (PackageId.Count() > 0)
            {
                for (int i = 0; i < PackageId.Count(); i++)
                {
                    BALProductPackages objBALProductPackages = new BALProductPackages();
                    bool Value = bool.Parse(check[i].ToString());
                    int packageId = int.Parse(PackageId[i].ToString());
                    objBALProductPackages.ChangePackageStatus(packageId, typeprop, Value);
                }
            }

            return View();

        }

        public ActionResult Get_ReferenceNote(int CompanyId)
        {
            BALReferenceCode objBALReferenceCode = new BALReferenceCode();
            int? fkcompanyid = null;

            if (CompanyId != 0)
            {
                if (CompanyId != null)
                {
                    fkcompanyid = CompanyId;
                }
                var referencelist = objBALReferenceCode.GetReferences((int)fkcompanyid).Select(c => new ReferenceCodeModel
                {
                    pkReferenceCodeId = c.pkReferenceCodeId,
                    ReferenceCode = c.ReferenceCode,
                    ReferenceNote = c.ReferenceNote
                }).ToList();
                return Json(referencelist);
            }
            else
            {
                var referencelist = 0;
                return Json(referencelist);
            }

        }

        public ActionResult Update_ReferenceCode(IEnumerable<ReferenceCodeModel> referencecodes)
        {
            BALReferenceCode ObjBALReferenceCode = new BALReferenceCode();
            foreach (var referencecodeModel in referencecodes)
            {
                var ReferenceCode = new tblReferenceCode
                {
                    pkReferenceCodeId = referencecodeModel.pkReferenceCodeId,
                    ReferenceCode = referencecodeModel.ReferenceCode,
                    ReferenceNote = referencecodeModel.ReferenceNote
                };
               //int Success = 
                    ObjBALReferenceCode.UpdateReference(ReferenceCode);
            }

            return Json(null);
        }

        public ActionResult Delete_ReferenceCode(IEnumerable<ReferenceCodeModel> referencecodes, int CompanyId)
        {
            int fkcompanyid = 0;
            if (CompanyId == null)
            {
                fkcompanyid = CompanyId;
                foreach (var referencecodeModel in referencecodes)
                {
                    BALReferenceCode ObjBALReferenceCode = new BALReferenceCode();
                    int success = 0;
                    success = ObjBALReferenceCode.DeleteReference(referencecodeModel.pkReferenceCodeId, fkcompanyid);
                }
            }
            //Return emtpy result
            return Json(null);
        }

        public ActionResult Add_ReferenceCode(IEnumerable<ReferenceCodeModel> referencecodes, int CompanyId)
        {
            var result = new List<tblReferenceCode>();
            int? fkcompanyid = null;
            if (CompanyId != null)
            {
                fkcompanyid = CompanyId;
                foreach (var referencecodeModel in referencecodes)
                {
                    var ReferenceCode = new tblReferenceCode
                    {
                        pkReferenceCodeId = referencecodeModel.pkReferenceCodeId,
                        fkCompanyId = fkcompanyid,
                        ReferenceCode = referencecodeModel.ReferenceCode,
                        ReferenceNote = referencecodeModel.ReferenceNote
                    };

                    BALReferenceCode ObjBALReferenceCode = new BALReferenceCode();
                    //int success = 0;
                    //success = 
                        ObjBALReferenceCode.AddReference(ReferenceCode, fkcompanyid.GetValueOrDefault());
                }
            }
            return Json(result.Select(c => new ReferenceCodeModel
            {
                pkReferenceCodeId = c.pkReferenceCodeId,
                ReferenceCode = c.ReferenceCode,
                ReferenceNote = c.ReferenceNote
            }).ToList());
        }

        public ActionResult UpdateProduct(int locationId, string AutoReviewStatus, string Global, string LiveRunnerProduct, string ProductApplication)
        {
            BALLocation ObjBALLocation = new BALLocation();
            tblProductAccess ObjtblProductAccess = new tblProductAccess();
            //var deleteproducts = 
                ObjBALLocation.DeleteProductsOfLocation(locationId);
            int DelUnderScore = AutoReviewStatus.LastIndexOf('_');
            string DelBracket = AutoReviewStatus.Substring(0, DelUnderScore != -1 ? DelUnderScore : 0).Trim().ToLower();
            string[] ARStatus = DelBracket.Split('_');

            int DeleteUnderScore = ProductApplication.LastIndexOf('_');
            string DeleteBracket = ProductApplication.Substring(0, DeleteUnderScore != -1 ? DeleteUnderScore : 0).Trim().ToLower();
            string[] PrApplication = DeleteBracket.Split('_');

            int DelUnder_Score = Global.LastIndexOf('_');
            string Del_Bracket = Global.Substring(0, DelUnder_Score != -1 ? DelUnder_Score : 0).Trim().ToLower();
           // string[] Gl = 
                Del_Bracket.Split('_');


            int Del_UnderScore = LiveRunnerProduct.LastIndexOf('_');
            string DelBraces = LiveRunnerProduct.Substring(0, Del_UnderScore != -1 ? Del_UnderScore : 0).Trim().ToLower();
            string[] lr = DelBraces.Split('_');

            for (int i = 0; i < lr.Count(); i++)
            {

                ObjtblProductAccess.fkLocationId = locationId;
                ObjtblProductAccess.fkProductApplicationId = Convert.ToInt32(PrApplication[i]);
                ObjtblProductAccess.IsLiveRunner = Convert.ToBoolean(lr[i]);
                ObjtblProductAccess.ProductReviewStatus = Convert.ToByte(ARStatus[i]);
                ObjBALLocation.InsertNewLocationProduct(ObjtblProductAccess);






            }




            //BALCompany serObj = new BALCompany();


            //var deleteproducts = ObjBALLocation.DeleteProductsOfLocation(locationId);

            //ObjBALLocation = new BALLocation();


            ////for (int i = 0; i < list.Count;i++)
            ////{
            ////    //bool LiveRunner = false, AutoReview = false;
            ////    string[] val = data.Value.Split('_');
            //    ObjtblProductAccess.fkLocationId = LocationId;
            //    ObjtblProductAccess.fkProductApplicationId = data.Key;
            //    ObjtblProductAccess.IsLiveRunner = Convert.ToBoolean(val[0]);
            //    ObjtblProductAccess.ProductReviewStatus = Convert.ToByte(val[1]);
            //    ObjBALLocation.InsertNewLocationProduct(ObjtblProductAccess);
            //}

            return View();








        }


    }
}
