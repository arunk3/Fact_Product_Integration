﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Emerge.Services;
using System.IO;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /PartialBatchOrders/

        public ActionResult PartialBatchOrders()
        {
            return View();
        }

        public ActionResult GetBatchOrders()
        {
          MembershipUser ObjMembershipUser = Membership.GetUser(HttpContext.User.Identity.Name);
          Guid CurrentUserId=Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
          BALBatch objbalbatch = new BALBatch();
          var BatchOrderList= objbalbatch.GetBatchOrderByPkCompanyUserId(CurrentUserId);
          return Json(new { Products = BatchOrderList, TotalCount = BatchOrderList.Count() }, JsonRequestBehavior.AllowGet);
        }

        public FilePathResult DownloadBatchFile(string BatchFileName)
        {

            string FilePath = string.Empty;
            string CType = string.Empty;
            string strFileTitle = string.Empty;
           
                 FilePath = Request.PhysicalApplicationPath + "Resources/Upload/TempBatchFiles/" + BatchFileName;
                string fileext = System.IO.Path.GetExtension(BatchFileName);
                 CType = "application/" + fileext.Replace(".", "");
               // string fileName = Path.GetFileName(BatchFileName);

                 strFileTitle = "BatchFile" + fileext;
                 if (System.IO.File.Exists(FilePath))
                 {
                     return File(FilePath, CType, strFileTitle);
                 }
                 else
                 {
                     strFileTitle = "NoFiles.jpg";
                     FilePath = Request.PhysicalApplicationPath + "Resources/Upload/FeatureFiles/no-image.jpg";
                     return File(FilePath, CType, strFileTitle);
                 }
            
           

        }
    }
}
