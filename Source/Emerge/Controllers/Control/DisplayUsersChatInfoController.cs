﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Emerge.Models;
using Emerge.Data;
using System.Text;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /DisplayUsersChatInfo/

        public ActionResult DisiplayUsersChat()
        {
            MangoChatSessionModel objMangoChatSessionModel = new MangoChatSessionModel();
            if (Request.QueryString["UserId"] != null)
            {
              
                string UserId = Request.QueryString["UserId"].ToString();
                objMangoChatSessionModel.SuccessMsg = "!deleted";
                if (Request.QueryString["Status"] != null)
                {
                    objMangoChatSessionModel.SuccessMsg = "Chat Log Deleted successfully.";
                }
                BALChat objBALChat = new BALChat();
               var list1= objBALChat.GetChatUsersByChatid(UserId);
               var list =list1.Where(rec=>rec.ChatSession!=null && rec.ChatSession !="3").ToList();
               List<Proc_Get_ChatInfoBySessionIdForUserResult> Userlist = new List<Proc_Get_ChatInfoBySessionIdForUserResult>();
               List<Proc_Get_ChatInfoBySessionIdForUserResult> FinalUserlist = new List<Proc_Get_ChatInfoBySessionIdForUserResult>();


               for (int i = 0; i < list.Count(); i++)
               {
                   string ChatSession = list.ElementAt(i).ChatSession;
                   Userlist = objBALChat.GetChatUsersByPublishID(ChatSession, UserId);
                    FinalUserlist.Insert(i, Userlist.ElementAt(0));
               }

               FinalUserlist.OrderBy(p => p.EndTime);
               ViewBag.ChatUserName = objBALChat.GetUserNamebyId(Guid.Parse(UserId));
               objMangoChatSessionModel.GetChatHistorybySessionId = FinalUserlist;
            }
            return View(objMangoChatSessionModel);
        }

        public ActionResult DeleteChatSessionfromDb(string ChatSessionId)
        {

            //BALChat objbalchat = new BALChat();
           // int success =objbalchat.DeleteChatbySessionID(ChatSessionId);
            string successMessage =string.Empty;
            return Json(new
            {
                finalinput = successMessage


            });
        }
        public ActionResult GetUserChatbySessionId(string ChatSessionId)
        {

            BALChat objBALChat = new BALChat();
            
            var list = objBALChat.GetUsersChatbySeesionid(ChatSessionId);
            ViewBag.finalinput = list;
            //BALChat objbalchat = new BALChat();
            StringBuilder sb = new StringBuilder();
            sb.Append("<table id='divresAlert'  >");

            for (int i = 0; i <= list.Count-1; i++)
            {
                

                string FromUserName = objBALChat.GetUserNamebyId(Guid.Parse(list.ElementAt(i).FromUserId.ToString()));
                //sb.Append("<td align='left' style='padding-left:10px' ><b>From  </b> :" + FromUserName);
                //sb.Append("</td>");
               // string ToUserName = objBALChat.GetUserNamebyId(Guid.Parse(list.ElementAt(i).ToUserId.ToString()));
                //sb.Append("<td align='left' style='margin-left:10px' > <b> To </b>:      " + ToUserName);
                //sb.Append("</td>");
                
                //sb.Append("<td align='left' style='padding-left:10px' ><b>Time </b> :  " + list.ElementAt(i).Time);
                //sb.Append("</td>");
                //sb.Append("<td align='left' style='padding-left:15px' >:     " + list.ElementAt(i).ActualContent).Replace("}", "");
                //sb.Append("</td>");
                //sb.Append("</tr>");

                sb.Append("<tr style='margin-top:1px' >");
                sb.Append("<td align='left' style='padding-left:10px; width:400px'  > <div class='Center_ImgMVC1'><b>" + FromUserName + "  </b>  (" + list.ElementAt(i).Time + ")<br/> <div style='width:101.2%; min-height:35px;margin-left:-3px; padding-left:3px;border-top-width:1px; border-top-color:#979797; border-top-style:solid; margin-top:5px; background-color:white'>" + list.ElementAt(i).ActualContent.Replace("}", "").Replace('"', ' ') + "</div></div>");
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr style=''>");
                sb.Append("<td align='left' style='padding-left:10px; font-color:white'   >" );
                sb.Append("</td>");
                sb.Append("</tr>");
            }
            sb.Append("</table>");
            return Json(new
                           { finalinput = sb.ToString()

                               
                           });
        }
        
    }
}
