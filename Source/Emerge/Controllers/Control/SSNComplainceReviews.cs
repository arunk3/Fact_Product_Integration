﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Emerge.Models;
using Emerge.Data;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /SSNComplainceReviews/

        //public ActionResult Index()
        //{
        //    return View();
        //}


        public ActionResult SSNComplainceReviews()
        {

            SSNComplainceReviewsModel objGlobalFilter = new SSNComplainceReviewsModel();
            objGlobalFilter.Keywords = string.Empty;
            return View(objGlobalFilter);
        }


        public ActionResult SSNComplainceReviews_BindGlobleFilter(int page, int pageSize)
        {
            BALSSNComplainceReviews objBalGloable = new BALSSNComplainceReviews();
            List<SSNComplainceReviewsModel> lstObject = new List<SSNComplainceReviewsModel>();

            //Get collection data and bind in model
            try
            {
                var CollectionData = objBalGloable.SSNComplainceReviews_GetGlobleFilters();
                if (CollectionData.Count > 0 && CollectionData != null)
                {

                    for (int index = 0; index < CollectionData.Count; index++)
                    {
                        lstObject.Add(new SSNComplainceReviewsModel
                        {
                            GlobleFilterId = CollectionData.ElementAt(index).PkSSNComplaince,
                            //ReportType = CollectionData.ElementAt(index).ReportsType,
                            Keywords = CollectionData.ElementAt(index).SSN_NO,
                            IsEnable = CollectionData.ElementAt(index).IsEnabled
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(new { FilterValue = lstObject, TotalCount = lstObject.Count() }, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Add new globle filter in kendo grid
        /// </summary>
        /// <param name="Collection"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SSNComplainceReviews_AddGlobleFilters(FormCollection Collection)
        {
            //string Keywords = string.Empty;
            string source = string.Empty;
            string docketNumber = string.Empty;
            string deliveryAddress = string.Empty;
            string Msg = string.Empty;
            string Css = string.Empty;
            int Result = 0;
            BALSSNComplainceReviews objBalGloableFilter = new BALSSNComplainceReviews();
            tblSSNComplainceReviewsMain objTblGlobleFilter = new tblSSNComplainceReviewsMain();

            //Keywords = Collection["Keywords"];
            source = Collection["Source"];
            docketNumber = Collection["DocketNumber"];
            deliveryAddress = Collection["DeliveryAddress"];
            try
            {

                
                objTblGlobleFilter.SSN_NO = docketNumber;
                objTblGlobleFilter.AddingDate_ = System.DateTime.Today;
                //insert in database                
                Result = objBalGloableFilter.SSNComplainceReviews_InsertGlobleFilter(objTblGlobleFilter);
                if (Result == 1)
                {
                    Msg = "Insertion successfully";
                    Css = "successmsg";
                }
                else
                {
                    Msg = "Some error occured during the insertion";
                    Css = "errormsgSMALL";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


            return Json(new { Message = Msg, Css = Css });
        }
        /// <summary>
        /// Get update page used this controller
        /// </summary>
        /// <returns></returns>
        public ActionResult SSNComplainceReviews_UpdateGlobleFilters()
        {
            BALSSNComplainceReviews objBalGloable = new BALSSNComplainceReviews();
            SSNComplainceReviewsModel objGlobalFilterModel = new SSNComplainceReviewsModel();
            int GloblefilterId = 0;
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["_a"]))         //  _a Contains Filter ID
                {
                    string Filter_Id = Request.QueryString["_a"].ToString();
                    GloblefilterId = int.Parse(Filter_Id);
                    //Get specific filter 
                    var CollectionData = objBalGloable.SSNComplainceReviews_GetGlobleFilters(GloblefilterId);
                    //var CollectionData = objBalGloable.SSNComplainceReviews_Update_GloableFilters();
                    
                    
                    //SSNComplainceReviews_Update_GloableFilters
                    if (CollectionData != null)
                    {


                        objGlobalFilterModel.DocketNumber = CollectionData.SSN_NO != null ? CollectionData.SSN_NO : null;
                        
                        objGlobalFilterModel.GlobleFilterId = CollectionData.PkSSNComplaince;
                        objGlobalFilterModel.IsEnable = CollectionData.IsEnabled;
                        
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View(objGlobalFilterModel);
        }
        /// <summary>
        /// Delete Globle filter from database
        /// </summary>
        /// <param name="GloblefilterId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SSNComplainceReviews_DeleteGlobleFilters(int GloblefilterId)
        {
            BALSSNComplainceReviews objBalGloable = new BALSSNComplainceReviews();
            string Message = string.Empty;
            string Css = string.Empty;
            try
            {
                //delete method call
                int Result = objBalGloable.SSNComplainceReviews_DeleteGlobleFilters(GloblefilterId);
                if (Result == 1)
                {
                    Message = "Delete Successfully";
                    Css = "successmsg";
                }
                else
                {
                    Message = "Some error occured while Delete globle filter";
                    Css = "errormsgSMALL";
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(new { Msg = Message, css = Css });
        }
        /// <summary>
        /// update globle filter in database
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SSNComplainceReviews_Update_GloableFilters(FormCollection collection)
        {
            BALSSNComplainceReviews objBalGloable = new BALSSNComplainceReviews();
            tblSSNComplainceReviewsMain objTblGolobleFilter = new tblSSNComplainceReviewsMain();
            bool IsEnabled = false;
            string Message = string.Empty;
            string Css = string.Empty;
            int GlobleFilterId = 0;
            string source = string.Empty;
            string docketNumber = string.Empty;
            string delivaryAddress = string.Empty;
            //string KeyWords = string.Empty;


            string FilterId = collection["GlobleFilterId"];
            source = collection["Source"];
            docketNumber = collection["DocketNumber"];
            delivaryAddress = collection["DeliveryAddress"];
            string IsEnable = collection["Enabled_Chk"];
            GlobleFilterId = int.Parse(FilterId);
            IsEnabled = bool.Parse(IsEnable);

            objTblGolobleFilter.PkSSNComplaince = GlobleFilterId;
            
            objTblGolobleFilter.SSN_NO = docketNumber;
            objTblGolobleFilter.AddingDate_ = System.DateTime.Today;
            objTblGolobleFilter.IsEnabled = IsEnabled;

            try
            {
                //udpate method call
                int Result = objBalGloable.SSNComplainceReviews_UpdateGlobleFilters(objTblGolobleFilter);
                if (Result == 1)
                {
                    Message = "Update Successfully";
                    Css = "successmsg";
                }
                else
                {
                    Message = "Some error occured while updating globle filter";
                    Css = "errormsgSMALL";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(new { Msg = Message, css = Css });
        }

    }
}
