﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //int CompanyId = 0;

        public PartialViewResult PartialManageCustomPackages()
        {
            PackagesModel ObjModel = new PackagesModel();

            ObjModel.QueryStringPackageId = "-1";

            if (!string.IsNullOrEmpty(Request.QueryString["_a"]))         //  _a Contains PackageId
            {
                int.TryParse(Request.QueryString["_a"], out iPackageId);
                if (iPackageId > 0)
                {
                    ObjModel.QueryStringPackageId = iPackageId.ToString();
                    ObjModel = LoadPackageData(int.Parse(ObjModel.QueryStringPackageId), ObjModel);
                }
                else
                {
                    //ObjModel.CustomMessages = "<span class='infomessage'>Please do not try with invalid querystring values</span>";

                }
                ObjModel.CompanyId = Int32.Parse(Request.QueryString["_companyid"].ToString());
                ObjModel.LocationId = Int32.Parse(Request.QueryString["_locationid"].ToString());
            }

            ObjModel.lstProductsByPackageId = BindProducts(int.Parse(ObjModel.QueryStringPackageId));

            return PartialView(ObjModel);
        }

        public PartialViewResult PartialAddEditCustomPackages(string _a, string _locationid, string _companyid)
        {
            PackagesModel ObjModel = new PackagesModel();

            ObjModel.QueryStringPackageId = "-1";

            if (!string.IsNullOrEmpty(_a))         //  _a Contains PackageId
            {
                int.TryParse(_a, out iPackageId);
                if (iPackageId > 0)
                {
                    ObjModel.QueryStringPackageId = iPackageId.ToString();
                    ObjModel = LoadPackageData(int.Parse(ObjModel.QueryStringPackageId), ObjModel);
                }
                else
                {
                    ObjModel.IsEnabled = true;
                    ObjModel.IsEnableCompanyPackage = true;
                }
                ObjModel.CompanyId = Int32.Parse(_companyid.ToString());
                ObjModel.LocationId = Int32.Parse(_locationid);
            }

            ObjModel.lstProductsByPackageId = BindProducts(int.Parse(ObjModel.QueryStringPackageId));
            ObjModel = BindTiered(int.Parse(ObjModel.QueryStringPackageId), ObjModel);
            return PartialView("PartialManageCustomPackages", ObjModel);
        }

        public PackagesModel BindTiered(int PackageId, PackagesModel ObjModel)
        {
            Dictionary<string, string> tieredHostList = new Dictionary<string, string>();
            Dictionary<string, string> tiered2List = new Dictionary<string, string>();
            Dictionary<string, string> tiered3List = new Dictionary<string, string>();
            Dictionary<string, string> tieredOptionalList = new Dictionary<string, string>();
            Dictionary<string, string> timeframeList = new Dictionary<string, string>();
            // tieredHostList.Add("-1", "--");
            tiered2List.Add("-1", "--");
            tiered3List.Add("-1", "--");
            tieredOptionalList.Add("-1", "--");
            timeframeList.Add("-1", "--");
            timeframeList.Add("1", "1");
            timeframeList.Add("2", "2");
            timeframeList.Add("3", "3");
            timeframeList.Add("4", "4");
            timeframeList.Add("5", "5");
            timeframeList.Add("6", "6");
            timeframeList.Add("7", "7");
            timeframeList.Add("10", "10");//INT-174 as per client discussion.

            for (int iRow = 0; iRow < ObjModel.lstProductsByPackageId.Count; iRow++)
            {
                if (ObjModel.lstProductsByPackageId[iRow].PackageReportId != 0 && ObjModel.lstProductsByPackageId[iRow].ProductPerApplicationIsEnabled == true)
                {
                    if (ObjModel.lstProductsByPackageId[iRow].ProductCode == "PS")
                    {
                        if (!tieredHostList.ContainsKey(ObjModel.lstProductsByPackageId[iRow].pkProductApplicationId.ToString()))
                            tieredHostList.Add(ObjModel.lstProductsByPackageId[iRow].pkProductApplicationId.ToString(), ObjModel.lstProductsByPackageId[iRow].ProductDisplayName + " - " + ObjModel.lstProductsByPackageId[iRow].ProductCode);
                    }
                    if (ObjModel.lstProductsByPackageId[iRow].ProductCode == "CCR1" || ObjModel.lstProductsByPackageId[iRow].ProductCode == "SCR")
                    {
                        if (!tiered2List.ContainsKey(ObjModel.lstProductsByPackageId[iRow].pkProductApplicationId.ToString()))
                        {
                            tiered2List.Add(ObjModel.lstProductsByPackageId[iRow].pkProductApplicationId.ToString(), ObjModel.lstProductsByPackageId[iRow].ProductDisplayName + " - " + ObjModel.lstProductsByPackageId[iRow].ProductCode);
                        }
                        if (!tiered3List.ContainsKey(ObjModel.lstProductsByPackageId[iRow].pkProductApplicationId.ToString()))
                        {
                            tiered3List.Add(ObjModel.lstProductsByPackageId[iRow].pkProductApplicationId.ToString(), ObjModel.lstProductsByPackageId[iRow].ProductDisplayName + " - " + ObjModel.lstProductsByPackageId[iRow].ProductCode);
                        }
                        if (!tieredOptionalList.ContainsKey(ObjModel.lstProductsByPackageId[iRow].pkProductApplicationId.ToString()))
                        {
                            tieredOptionalList.Add(ObjModel.lstProductsByPackageId[iRow].pkProductApplicationId.ToString(), ObjModel.lstProductsByPackageId[iRow].ProductDisplayName + " - " + ObjModel.lstProductsByPackageId[iRow].ProductCode);
                        }
                    }
                    if (ObjModel.lstProductsByPackageId[iRow].ProductCode == "FCR")
                    {
                        if (!tiered2List.ContainsKey(ObjModel.lstProductsByPackageId[iRow].pkProductApplicationId.ToString()))
                            tiered2List.Add(ObjModel.lstProductsByPackageId[iRow].pkProductApplicationId.ToString(), ObjModel.lstProductsByPackageId[iRow].ProductDisplayName + " - " + ObjModel.lstProductsByPackageId[iRow].ProductCode);
                        if (!tiered3List.ContainsKey(ObjModel.lstProductsByPackageId[iRow].pkProductApplicationId.ToString()))
                            tiered3List.Add(ObjModel.lstProductsByPackageId[iRow].pkProductApplicationId.ToString(), ObjModel.lstProductsByPackageId[iRow].ProductDisplayName + " - " + ObjModel.lstProductsByPackageId[iRow].ProductCode);
                        if (!tieredOptionalList.ContainsKey(ObjModel.lstProductsByPackageId[iRow].pkProductApplicationId.ToString()))
                            tieredOptionalList.Add(ObjModel.lstProductsByPackageId[iRow].pkProductApplicationId.ToString(), ObjModel.lstProductsByPackageId[iRow].ProductDisplayName + " - " + ObjModel.lstProductsByPackageId[iRow].ProductCode);
                    }

                }
                else
                {
                    if (ObjModel.lstProductsByPackageId[iRow].ProductCode == "PS")
                    {
                        tieredHostList.Add(ObjModel.lstProductsByPackageId[iRow].pkProductApplicationId.ToString(), ObjModel.lstProductsByPackageId[iRow].ProductDisplayName + " - " + ObjModel.lstProductsByPackageId[iRow].ProductCode);
                    }
                    if (ObjModel.lstProductsByPackageId[iRow].ProductCode == "CCR1" || ObjModel.lstProductsByPackageId[iRow].ProductCode == "SCR")
                    {

                        tiered2List.Add(ObjModel.lstProductsByPackageId[iRow].pkProductApplicationId.ToString(), ObjModel.lstProductsByPackageId[iRow].ProductDisplayName + " - " + ObjModel.lstProductsByPackageId[iRow].ProductCode);
                        tiered3List.Add(ObjModel.lstProductsByPackageId[iRow].pkProductApplicationId.ToString(), ObjModel.lstProductsByPackageId[iRow].ProductDisplayName + " - " + ObjModel.lstProductsByPackageId[iRow].ProductCode);
                        tieredOptionalList.Add(ObjModel.lstProductsByPackageId[iRow].pkProductApplicationId.ToString(), ObjModel.lstProductsByPackageId[iRow].ProductDisplayName + " - " + ObjModel.lstProductsByPackageId[iRow].ProductCode);
                    }

                    if (ObjModel.lstProductsByPackageId[iRow].ProductCode == "FCR")
                    {
                        tiered2List.Add(ObjModel.lstProductsByPackageId[iRow].pkProductApplicationId.ToString(), ObjModel.lstProductsByPackageId[iRow].ProductDisplayName + " - " + ObjModel.lstProductsByPackageId[iRow].ProductCode);
                        tiered3List.Add(ObjModel.lstProductsByPackageId[iRow].pkProductApplicationId.ToString(), ObjModel.lstProductsByPackageId[iRow].ProductDisplayName + " - " + ObjModel.lstProductsByPackageId[iRow].ProductCode);
                        tieredOptionalList.Add(ObjModel.lstProductsByPackageId[iRow].pkProductApplicationId.ToString(), ObjModel.lstProductsByPackageId[iRow].ProductDisplayName + " - " + ObjModel.lstProductsByPackageId[iRow].ProductCode);
                    }

                }
            }

            ObjModel.chkTiered = true;
            ObjModel.TieredHostList = tieredHostList;
            ObjModel.Tiered2List = tiered2List;
            ObjModel.Tiered3List = tiered3List;
            ObjModel.TieredOptionalList = tieredOptionalList;
            ObjModel.TimeFrameList = timeframeList;

            if (PackageId != 0)
            {
                BALProductPackages objBALProductPackages = new BALProductPackages();
                tblTieredPackage objtblTieredPackage = new tblTieredPackage();

                objtblTieredPackage = objBALProductPackages.GetTeieredPackage(PackageId);
                if (objtblTieredPackage != null)
                {
                    ObjModel.IsAutomaticRun = objtblTieredPackage.IsAutomaticRun;
                    ViewData["vwpkTieredPackgeId"] = objtblTieredPackage.pkTieredPackgeId;
                    ObjModel.ddlTieredHost = objtblTieredPackage.Host.ToString();
                    ObjModel.ddlTiered2 = objtblTieredPackage.Tired2.ToString();
                    ObjModel.ddlTiered3 = objtblTieredPackage.Tired3.ToString();
                    ObjModel.ddlTiredOptional = (objtblTieredPackage.TiredOptional != null) ? objtblTieredPackage.TiredOptional.ToString() : "-1";
                    ObjModel.TieredHost = (objtblTieredPackage.Variable.ToLower() == "county state") ? "PS" : "CCR1";
                    ObjModel.ddlTimeFrame = (objtblTieredPackage.TimeFrame != null) ? objtblTieredPackage.TimeFrame.ToString() : "-1";
                    ObjModel.Tier2Price = objtblTieredPackage.Tired2Price == null ? string.Empty : objtblTieredPackage.Tired2Price.ToString();
                    ObjModel.Tier3Price = objtblTieredPackage.Tired3Price == null ? string.Empty : objtblTieredPackage.Tired3Price.ToString();
                    ObjModel.HostPrice = objtblTieredPackage.HostPrice;
                    ObjModel.OptionalPrice = objtblTieredPackage.OptionalPrice;
                }
                else
                {
                    ObjModel.ddlTieredHost = string.Empty;
                    ObjModel.ddlTiered2 = string.Empty;
                    ObjModel.ddlTiered3 = string.Empty;
                    ObjModel.ddlTiredOptional = string.Empty;
                    ObjModel.chkTiered = !true;
                    ObjModel.IsAutomaticRun = false;
                    ObjModel.TieredHost = string.Empty;
                    ObjModel.TimeFrame = 7;
                    ObjModel.Tier2Price = string.Empty;
                    ObjModel.Tier3Price = string.Empty;
                    ObjModel.HostPrice = null;
                    ObjModel.OptionalPrice = null;

                }
            }
            return ObjModel;
        }

        public ActionResult SaveCustomPackages(PackagesModel ObjModel, FormCollection frm)
        {
            string strResult = "";
            //If select all button is clicked.
            int isSelectAll=Convert.ToInt32(frm["btnHiddensubmittoall"]);
            if (isSelectAll == 0)
            {
                ObjModel.OldPackageId = 0;

                string strCommon = string.Empty;
                ObjModel.HostPrice = (decimal)0.00;
                for (int iRow = 0; iRow < frm.Keys.Count; iRow++)
                {
                    PackageReports ObjPackageReports = new PackageReports();

                    if (frm.Keys[iRow].Contains("chkReport_"))
                    {
                        string commonId = frm.GetKey(iRow).Split('_')[1];

                        if ((frm["chkReport_" + commonId]) != null && (frm["chkReport_" + commonId]).ToLower() != "no")
                        {
                            if (frm.GetValue("hdProductCode_" + commonId) != null && frm.GetValue("hd_ProductApplicationId_" + commonId) != null && frm.GetValue("hd_PackageReportId_" + commonId) != null)
                            {

                                ObjPackageReports.ProductCode = frm["hdProductCode_" + commonId].ToString();
                                ObjPackageReports.fkProductApplicationID = Int32.Parse(frm["hd_ProductApplicationId_" + commonId].ToString());
                                ObjPackageReports.PackageReportId = Int32.Parse(frm["hd_PackageReportId_" + commonId].ToString());
                                ObjPackageReports.IsChecked = true;

                                PackageInclude += frm["hdProductCode_" + commonId];
                                PackageInclude += ", ";

                                strCommon = commonId;
                                lstPackageReports.Add(ObjPackageReports);
                            }
                        }
                    }
                    if ((!frm.Keys[iRow].Contains("chkReport_")) && (frm.Keys[iRow].Contains("hdProductCode_")))
                    {

                        string commonId = frm.GetKey(iRow).Split('_')[1];

                        if (frm.GetValue("hdProductCode_" + commonId) != null && frm.GetValue("hd_ProductApplicationId_" + commonId) != null && frm.GetValue("hd_PackageReportId_" + commonId) != null && commonId != strCommon)
                        {

                            ObjPackageReports.ProductCode = frm["hdProductCode_" + commonId].ToString();
                            ObjPackageReports.fkProductApplicationID = Int32.Parse(frm["hd_ProductApplicationId_" + commonId].ToString());
                            ObjPackageReports.PackageReportId = Int32.Parse(frm["hd_PackageReportId_" + commonId].ToString());
                            ObjPackageReports.IsChecked = false;

                            lstPackageReports.Add(ObjPackageReports);
                        }

                    }


                }
                string _PackageId = string.Empty;
                _PackageId = ObjModel.QueryStringPackageId != null ? ObjModel.QueryStringPackageId : GetQueryStringValue();   // Get QueryString

                if (_PackageId == "-1" || _PackageId == null)                           /* We are in insert mode */
                {
                    strResult = AddCustomPackages(ObjModel, isSelectAll);
                }
                else /* We are in update mode  */
                {
                    if (_PackageId != null)
                    {
                        strResult = UpdateCustomPackages(int.Parse(_PackageId), ObjModel);
                    }
                }
            }
            else
            {

                ObjModel.OldPackageId = 1;
                string strCommon = string.Empty;
                ObjModel.HostPrice = (decimal)0.00;
                List<int> lstLocations = new BALCompany().GetAllLocationsByCompanyId(ObjModel.CompanyId).Select(x => x.pkLocationId).ToList();




                foreach (int loc in lstLocations)
                {
                    lstPackageReports.Clear();
                    ObjModel.LocationId = loc;
                    PackageInclude = string.Empty;
                    for (int iRow = 0; iRow < frm.Keys.Count; iRow++)
                    {
                        PackageReports ObjPackageReports = new PackageReports();

                        if (frm.Keys[iRow].Contains("chkReport_"))
                        {
                            string commonId = frm.GetKey(iRow).Split('_')[1];

                            if ((frm["chkReport_" + commonId]) != null && (frm["chkReport_" + commonId]).ToLower() != "no")
                            {
                                if (frm.GetValue("hdProductCode_" + commonId) != null && frm.GetValue("hd_ProductApplicationId_" + commonId) != null && frm.GetValue("hd_PackageReportId_" + commonId) != null)
                                {

                                    ObjPackageReports.ProductCode = frm["hdProductCode_" + commonId].ToString();
                                    ObjPackageReports.fkProductApplicationID = Int32.Parse(frm["hd_ProductApplicationId_" + commonId].ToString());
                                    ObjPackageReports.PackageReportId = Int32.Parse(frm["hd_PackageReportId_" + commonId].ToString());
                                    ObjPackageReports.IsChecked = true;

                                    PackageInclude += frm["hdProductCode_" + commonId];
                                    PackageInclude += ", ";

                                    strCommon = commonId;
                                    lstPackageReports.Add(ObjPackageReports);
                                }
                            }
                        }
                        if ((!frm.Keys[iRow].Contains("chkReport_")) && (frm.Keys[iRow].Contains("hdProductCode_")))
                        {

                            string commonId = frm.GetKey(iRow).Split('_')[1];

                            if (frm.GetValue("hdProductCode_" + commonId) != null && frm.GetValue("hd_ProductApplicationId_" + commonId) != null && frm.GetValue("hd_PackageReportId_" + commonId) != null && commonId != strCommon)
                            {

                                ObjPackageReports.ProductCode = frm["hdProductCode_" + commonId].ToString();
                                ObjPackageReports.fkProductApplicationID = Int32.Parse(frm["hd_ProductApplicationId_" + commonId].ToString());
                                ObjPackageReports.PackageReportId = Int32.Parse(frm["hd_PackageReportId_" + commonId].ToString());
                                ObjPackageReports.IsChecked = false;

                                lstPackageReports.Add(ObjPackageReports);
                            }

                        }


                    }
                    string _PackageId = string.Empty;
                    _PackageId = ObjModel.QueryStringPackageId != null ? ObjModel.QueryStringPackageId : GetQueryStringValue();   // Get QueryString

                    if (_PackageId == "-1" || _PackageId == null)                           /* We are in insert mode */
                    {
                        strResult = AddPackageToAllLocation(ObjModel);
                    }
                    else /* We are in update mode  */
                    {
                        if (_PackageId != null)
                        {
                            int packageIds = new BALProductPackages().GetPackageIdBasedOnLocationAndCompany(ObjModel.LocationId, ObjModel.CompanyId, ObjModel.PackageCode);
                            if (packageIds > 0)
                            {
                                strResult = UpdateCustomPackages(packageIds, ObjModel);
                            }
                            else
                            {
                                strResult = AddPackageToAllLocation(ObjModel);
                            }
                        }
                    }
                }


            }
            return Json(new { Message = strResult });

        }

        /// <summary>
        /// This method is used to add packages
        /// </summary>
        private string AddCustomPackages(PackagesModel ObjModel,int isSelectAll)
        {
            string strSuccess = string.Empty;
            List<int> ObjGuid_LocationIdColl;

            BALProductPackages ObjBALProductPackages = new BALProductPackages();
            int iResult = -1;
            try
            {
                //bool globalCriendialCheck = new BALProductPackages().GetCompanyGlobalCredentialCheck(ObjModel.CompanyId);
                //if select all button is not clicked.
                if (isSelectAll==0)
                {
                    tblProductPackage ObjProductPackage = new tblProductPackage();
                    ObjProductPackage.fkApplicationId = Utility.SiteApplicationId;

                    ObjProductPackage.fkLocationid = ObjModel.LocationId;
                    ObjProductPackage.fkCompanyId = ObjModel.CompanyId;
                    ObjProductPackage.PackageName = ObjModel.PackageName.Trim();
                    ObjProductPackage.PackageCode = ObjModel.PackageCode.Trim();
                    ObjProductPackage.PackageDisplayName = ObjModel.PackageDisplayName.Trim();
                    ObjProductPackage.PackageDescription = !string.IsNullOrEmpty(ObjModel.PackageDescription) ? ObjModel.PackageDescription.Trim() : string.Empty;

                    ObjProductPackage.IsEnabled = ObjModel.IsEnabled;
                    ObjProductPackage.IsDefault = ObjModel.IsDefault;
                    ObjProductPackage.IsPublic = ObjModel.IsPublic;
                    ObjProductPackage.IsEnableCompanyPackage = ObjModel.IsEnableCompanyPackage;
                    ObjProductPackage.CreatedDate = DateTime.Now;
                    ObjProductPackage.PackagePrice = ObjModel.PackagePrice;
                    ObjProductPackage.IsPackageNameDisplay = ObjModel.IsPackageNamedisplay;
                    ObjProductPackage.Is7YearFilter = ObjModel.is7YearFilterpackage;
                    ObjProductPackage.Is10YearFilter = ObjModel.is10YearFilterpackage;


                    List<tblPackageReport> ObjCollectionPackageReports = new List<tblPackageReport>();

                    for (int iRow = 0; iRow < lstPackageReports.Count; iRow++)
                    {
                        if (lstPackageReports.ElementAt(iRow).IsChecked == true)
                        {
                            tblPackageReport ObjData = new tblPackageReport();
                            //ObjData.pkPackageReportId = Guid.NewGuid();
                            ObjData.fkPackageId = -1;
                            ObjData.fkProductApplicationId = lstPackageReports.ElementAt(iRow).fkProductApplicationID;
                            ObjCollectionPackageReports.Add(ObjData);
                        }
                    }
                    int oPackageId;
                    if (PackageInclude.Length > 2 && PackageInclude.Contains(","))
                    {
                        PackageInclude = PackageInclude.Substring(0, PackageInclude.Length - 2);
                    }
                    ObjProductPackage.include = PackageInclude;
                    iResult = ObjBALProductPackages.AddProductPackages(ObjProductPackage, ObjCollectionPackageReports, out oPackageId);

                    if (iResult == 1)
                    {
                        if (ObjModel.OldPackageId == 1)
                        {
                            ObjGuid_LocationIdColl = GetLocationIds(ObjModel.CompanyId);
                        }
                        else
                        {
                            ObjGuid_LocationIdColl = new List<int>();
                            ObjGuid_LocationIdColl.Add(ObjModel.LocationId);
                        }

                        AddProductPackagesForLocation(ObjGuid_LocationIdColl, ObjModel.CompanyId, oPackageId);

                        if (InsertTiered(oPackageId, ObjModel) == 1)
                        {
                            strSuccess = "<span class='successmsg'>Package successfully created.</span>";
                        }
                        else
                        {
                            strSuccess = "<span class='errormsg'>Some problem while inserting data.</span>";
                        }
                    }
                    else if (iResult == -1)
                    {
                        strSuccess = "<span class='errormsg'>Some problem while inserting data.</span>";

                        return strSuccess;
                    }
                    else if (iResult == -2)
                    {
                        strSuccess = "<span class='infomessage'>Duplicate package name not allowed here.</span>";

                        return strSuccess;
                    }
                    else if (iResult == -3)
                    {
                        strSuccess = "<span class='infomessage'>Duplicate package code not allowed here.</span>";

                        return strSuccess;
                    }
                    else if (iResult == -4)
                    {
                        strSuccess = "<span class='infomessage'>Duplicate package display name not allowed here.</span>";

                        return strSuccess;
                    }

                }
                else
                {

                    List<int> locations = new List<int>();
                    //To get all location based on Company
                    locations = new BALProductPackages().GetAllLocationIdsBasedOnCompany(ObjModel.CompanyId);
                    if (PackageInclude.Length > 2 && PackageInclude.Contains(","))
                    {
                        PackageInclude = PackageInclude.Substring(0, PackageInclude.Length - 2);
                    }
                    for (int iLoc = 0; iLoc < locations.Count(); iLoc++)
                    {
                        ObjModel.LocationId = locations[iLoc];

                        tblProductPackage ObjProductPackage = new tblProductPackage();

                        ObjProductPackage.fkApplicationId = Utility.SiteApplicationId;

                        ObjProductPackage.fkLocationid = ObjModel.LocationId;
                        ObjProductPackage.fkCompanyId = ObjModel.CompanyId;
                        ObjProductPackage.PackageName = ObjModel.PackageName.Trim();
                        ObjProductPackage.PackageCode = ObjModel.PackageCode.Trim();
                        ObjProductPackage.PackageDisplayName = ObjModel.PackageDisplayName.Trim();
                        ObjProductPackage.PackageDescription = !string.IsNullOrEmpty(ObjModel.PackageDescription) ? ObjModel.PackageDescription.Trim() : string.Empty;

                        ObjProductPackage.IsEnabled = ObjModel.IsEnabled;
                        ObjProductPackage.IsDefault = ObjModel.IsDefault;
                        ObjProductPackage.IsPublic = ObjModel.IsPublic;
                        ObjProductPackage.IsEnableCompanyPackage = ObjModel.IsEnableCompanyPackage;
                        ObjProductPackage.CreatedDate = DateTime.Now;
                        ObjProductPackage.PackagePrice = ObjModel.PackagePrice;
                        ObjProductPackage.IsPackageNameDisplay = ObjModel.IsPackageNamedisplay;
                        ObjProductPackage.Is7YearFilter = ObjModel.is7YearFilterpackage;
                        ObjProductPackage.Is10YearFilter = ObjModel.is10YearFilterpackage;

                        List<tblPackageReport> ObjCollectionPackageReports = new List<tblPackageReport>();

                        for (int iRow = 0; iRow < lstPackageReports.Count; iRow++)
                        {
                            if (lstPackageReports.ElementAt(iRow).IsChecked == true)
                            {
                                tblPackageReport ObjData = new tblPackageReport();
                                //ObjData.pkPackageReportId = Guid.NewGuid();
                                ObjData.fkPackageId = -1;
                                ObjData.fkProductApplicationId = lstPackageReports.ElementAt(iRow).fkProductApplicationID;
                                ObjCollectionPackageReports.Add(ObjData);
                            }
                        }

                        int oPackageId;

                        ObjProductPackage.include = PackageInclude;
                        iResult = ObjBALProductPackages.AddProductPackages(ObjProductPackage, ObjCollectionPackageReports, out oPackageId);

                        if (iResult == 1)
                        {
                            //ObjGuid_LocationIdColl = GetLocationIds(ObjModel.CompanyId);
                            ObjGuid_LocationIdColl = new List<int>();
                            ObjGuid_LocationIdColl.Add(ObjModel.LocationId);
                            AddProductPackagesForLocation(ObjGuid_LocationIdColl, ObjModel.CompanyId, oPackageId);

                            if (InsertTiered(oPackageId, ObjModel) == 1)
                            {
                                strSuccess = "<span class='successmsg'>Package successfully created.</span>";
                            }
                            else
                            {
                                strSuccess = "<span class='errormsg'>Some problem while inserting data.</span>";
                            }
                        }
                        else if (iResult == -1)
                        {
                            strSuccess = "<span class='errormsg'>Some problem while inserting data.</span>";

                            return strSuccess;
                        }
                        else if (iResult == -2)
                        {
                            strSuccess = "<span class='infomessage'>Duplicate package name not allowed here.</span>";

                            return strSuccess;
                        }
                        else if (iResult == -3)
                        {
                            strSuccess = "<span class='infomessage'>Duplicate package code not allowed here.</span>";

                            return strSuccess;
                        }
                        else if (iResult == -4)
                        {
                            strSuccess = "<span class='infomessage'>Duplicate package display name not allowed here.</span>";

                            return strSuccess;
                        }
                    }
                }
            }
            catch
            {
                strSuccess = "<span class='errormsg'>Some problem while inserting data.</span>";
            }
            finally
            {
                ObjBALProductPackages = null;
            }
            return strSuccess;
        }

        public List<int> GetLocationIds(int fkCompanyId)
        {
            List<int> ObjGuid_LocationIdColl;
            BALLocation ObjBALLocation = new BALLocation();
            List<tblLocation> ObjColl = new List<tblLocation>();
            ObjColl = ObjBALLocation.GetLocationsByCompanyId(fkCompanyId);
            ObjGuid_LocationIdColl = ObjColl.Select(d => d.pkLocationId).ToList<int>();
            return ObjGuid_LocationIdColl;
        }

        public string AddPackageToAllLocation(PackagesModel ObjModel)
        {
            int iResult = 0;
            BALProductPackages ObjBALProductPackages = new BALProductPackages();
            string strSuccess = string.Empty;
            List<int> locations = new List<int>();
            //To get all location based on Company
            locations = new BALProductPackages().GetAllLocationIdsBasedOnCompany(ObjModel.CompanyId);
            if (PackageInclude.Length > 2 && PackageInclude.Contains(","))
            {
                PackageInclude = PackageInclude.Substring(0, PackageInclude.Length - 2);
            }

            tblProductPackage ObjProductPackage = new tblProductPackage();

            ObjProductPackage.fkApplicationId = Utility.SiteApplicationId;

            ObjProductPackage.fkLocationid = ObjModel.LocationId;
            ObjProductPackage.fkCompanyId = ObjModel.CompanyId;
            ObjProductPackage.PackageName = ObjModel.PackageName.Trim();
            ObjProductPackage.PackageCode = ObjModel.PackageCode.Trim();
            ObjProductPackage.PackageDisplayName = ObjModel.PackageDisplayName.Trim();
            ObjProductPackage.PackageDescription = !string.IsNullOrEmpty(ObjModel.PackageDescription) ? ObjModel.PackageDescription.Trim() : string.Empty;

            ObjProductPackage.IsEnabled = ObjModel.IsEnabled;
            ObjProductPackage.IsDefault = ObjModel.IsDefault;
            ObjProductPackage.IsPublic = ObjModel.IsPublic;
            ObjProductPackage.IsEnableCompanyPackage = ObjModel.IsEnableCompanyPackage;
            ObjProductPackage.CreatedDate = DateTime.Now;
            ObjProductPackage.PackagePrice = ObjModel.PackagePrice;
            ObjProductPackage.IsPackageNameDisplay = ObjModel.IsPackageNamedisplay;
            ObjProductPackage.Is7YearFilter = ObjModel.is7YearFilterpackage;
            ObjProductPackage.Is10YearFilter = ObjModel.is10YearFilterpackage;

            List<tblPackageReport> ObjCollectionPackageReports = new List<tblPackageReport>();

            for (int iRow = 0; iRow < lstPackageReports.Count; iRow++)
            {
                if (lstPackageReports.ElementAt(iRow).IsChecked == true)
                {
                    tblPackageReport ObjData = new tblPackageReport();
                    //ObjData.pkPackageReportId = Guid.NewGuid();
                    ObjData.fkPackageId = -1;
                    ObjData.fkProductApplicationId = lstPackageReports.ElementAt(iRow).fkProductApplicationID;
                    ObjCollectionPackageReports.Add(ObjData);
                }
            }

            int oPackageId;

            ObjProductPackage.include = PackageInclude;
            iResult = ObjBALProductPackages.AddProductPackages(ObjProductPackage, ObjCollectionPackageReports, out oPackageId);

            if (iResult == 1)
            {
                //ObjGuid_LocationIdColl = GetLocationIds(ObjModel.CompanyId);
                ObjGuid_LocationIdColl = new List<int>();
                ObjGuid_LocationIdColl.Add(ObjModel.LocationId);
                AddProductPackagesForLocation(ObjGuid_LocationIdColl, ObjModel.CompanyId, oPackageId);

                if (InsertTiered(oPackageId, ObjModel) == 1)
                {
                    strSuccess = "<span class='successmsg'>Package successfully created.</span>";
                }
                else
                {
                    strSuccess = "<span class='errormsg'>Some problem while inserting data.</span>";
                }
            }
            else if (iResult == -1)
            {
                strSuccess = "<span class='errormsg'>Some problem while inserting data.</span>";

                return strSuccess;
            }
            else if (iResult == -2)
            {
                strSuccess = "<span class='infomessage'>Duplicate package name not allowed here.</span>";

                return strSuccess;
            }
            else if (iResult == -3)
            {
                strSuccess = "<span class='infomessage'>Duplicate package code not allowed here.</span>";

                return strSuccess;
            }
            else if (iResult == -4)
            {
                strSuccess = "<span class='infomessage'>Duplicate package display name not allowed here.</span>";

                return strSuccess;
            }
            return strSuccess;
        }







        private int AddProductPackagesForLocation(List<int> LocationIdColl, int CompanyId, int PackageId)
        {
            BALProductPackages ObjBALProductPackages = new BALProductPackages();
            try
            {
                #region Variables

                //   List<Proc_Get_EmergeProductPackagesForAdminResult> ObjPackagesCollectionDB = ObjBALProductPackages.GetEmergeProductPackagesForAdmin(LocationId, Utility.SiteApplicationId, CompanyId); /* Pick data for location packages */

                #endregion
                List<int> LstDeletingPackages = new List<int>();

                //   #endregion

                #region Call DB
                List<tblPackageAccess> ObjCollectionPackageAccess = new List<tblPackageAccess>();
                if (LocationIdColl.Count > 0)
                {
                    foreach (int LocationId in LocationIdColl)
                    {
                        tblPackageAccess ObjPackageAccess = new tblPackageAccess();
                        //ObjPackageAccess.pkPackageAccessId = Guid.NewGuid();
                        ObjPackageAccess.fkPackageId = PackageId;
                        ObjPackageAccess.fkLocationId = LocationId;
                        ObjCollectionPackageAccess.Add(ObjPackageAccess);
                    }
                }

                int success = ObjBALProductPackages.AddUpdateProductPackagesForLocation(LstDeletingPackages, ObjCollectionPackageAccess);

                return success;

                #endregion


            }
            finally
            {
                ObjBALProductPackages = null;
            }
        }

        /// <summary>
        /// This method is used to update packages
        /// </summary>
        /// <param name="PackageId"></param>
        private string UpdateCustomPackages(int PackageId, PackagesModel ObjModel)
        {
            string strSuccess = string.Empty;


            BALProductPackages ObjBALProductPackages = new BALProductPackages();
            int iResult = -1;

            try
            {
                #region Update data in main package table

                tblProductPackage ObjProductPackage = new tblProductPackage();

                ObjProductPackage.pkPackageId = PackageId;
                ObjProductPackage.fkCompanyId = ObjModel.CompanyId;
                ObjProductPackage.fkLocationid = ObjModel.LocationId;//INT-101
                ObjProductPackage.fkApplicationId = Utility.SiteApplicationId;
                ObjProductPackage.PackageName = ObjModel.PackageName.Trim();
                ObjProductPackage.PackageCode = ObjModel.PackageCode.Trim();
                ObjProductPackage.PackagePrice = ObjModel.PackagePrice;
                ObjProductPackage.PackageDisplayName = ObjModel.PackageDisplayName.Trim();
                ObjProductPackage.PackageDescription = !string.IsNullOrEmpty(ObjModel.PackageDescription) ? ObjModel.PackageDescription.Trim() : string.Empty;
                ObjProductPackage.IsEnabled = ObjModel.IsEnabled;
                ObjProductPackage.IsDefault = ObjModel.IsDefault;
                ObjProductPackage.IsPublic = ObjModel.IsPublic;
                ObjProductPackage.IsEnableCompanyPackage = ObjModel.IsEnableCompanyPackage;
                ObjProductPackage.IsPackageNameDisplay = ObjModel.IsPackageNamedisplay;
                ObjProductPackage.LastModifiedDate = DateTime.Now;
                ObjProductPackage.Is7YearFilter = ObjModel.is7YearFilterpackage;
                ObjProductPackage.Is10YearFilter = ObjModel.is10YearFilterpackage;
                #endregion

                #region Update Product List in Packages

                #region Variables

                //List<Proc_Get_AllEmergeProductsInPackagesResult> ObjProductsCollectionDB = ObjBALProductPackages.GetAllEmergeProductsInPackages(PackageId, Utility.SiteApplicationId); /* Pick data for products */

                //var ObjCollSavedProducts = ObjProductsCollectionDB.Where(db => db.PackageReportId != 0); /* Pick collection of all saved products for package */

               // int[] arrSavedProducts = ObjCollSavedProducts.Select(db => db.PackageReportId).ToArray(); /* Store saved reports in packages array  */

                #endregion



                List<Guid> LstDeletingProducts = new List<Guid>();


                #endregion

                #region New Reports Add for package

                List<tblPackageReport> ObjCollectionPackageReports = new List<tblPackageReport>();



                for (int iLvl2 = 0; iLvl2 < lstPackageReports.Count; iLvl2++)
                {
                    if (lstPackageReports.ElementAt(iLvl2).IsChecked == true)
                    {
                        tblPackageReport ObjPackageReport = new tblPackageReport();


                        //ObjPackageReport.pkPackageReportId = Guid.NewGuid();
                        ObjPackageReport.fkPackageId = PackageId;
                        ObjPackageReport.fkProductApplicationId = lstPackageReports.ElementAt(iLvl2).fkProductApplicationID;

                        ObjCollectionPackageReports.Add(ObjPackageReport);

                    }
                }

                #endregion

                #region Call DB
                if (PackageInclude.Length > 2 && PackageInclude.Contains(","))
                {
                    PackageInclude = PackageInclude.Substring(0, PackageInclude.Length - 2);
                }
                ObjProductPackage.include = PackageInclude;
                iResult = ObjBALProductPackages.UpdateProductPackages(LstDeletingProducts, ObjProductPackage, ObjCollectionPackageReports);

                #endregion

                #region Method Result

                if (iResult == 1)
                {
                    //  LoadPackageData(PackageId);
                    //  BindProducts(PackageId);
                    #region Tiered

                    UpdateTiered(PackageId, ObjModel);

                    #endregion
                    strSuccess = "<span class='successmsg'>Package successfully updated.</span>";
                }
                else if (iResult == -1)
                {
                    strSuccess = "<span class='errormsg'>Some problem while updating data.</span>";

                    return strSuccess;
                }
                else if (iResult == -2)
                {
                    strSuccess = "<span class='infomessage'>Duplicate package name not allowed here.</span>";

                    return strSuccess;
                }
                else if (iResult == -3)
                {
                    strSuccess = "<span class='infomessage'>Duplicate package code not allowed here.</span>";

                    return strSuccess;
                }
                else if (iResult == -4)
                {
                    strSuccess = "<span class='infomessage'>Duplicate package display name not allowed here.</span>";

                    return strSuccess;
                }

                #endregion


            }
            catch
            {
                strSuccess = "<span class='errormsg'>Some problem while updating data.</span>";

            }
            finally
            {
                ObjBALProductPackages = null;
            }
            return strSuccess;
        }

        public int InsertTiered(int fkPackageId, PackagesModel ObjModel)
        {
            int status = 0;
            BALProductPackages objBALProductPackages = new BALProductPackages();
            tblTieredPackage objtblTieredPackage = new tblTieredPackage();
            int? optional = null;
            objtblTieredPackage.CreatedDate = DateTime.Now;
            objtblTieredPackage.fkPackageId = fkPackageId;
            objtblTieredPackage.IsAutomaticRun = ObjModel.IsAutomaticRun;
            objtblTieredPackage.Host = (ObjModel.ddlTieredHost != "-1") ? Int32.Parse(ObjModel.ddlTieredHost) : 0;
            objtblTieredPackage.Tired2 = (ObjModel.ddlTiered2 != "-1") ? Int32.Parse(ObjModel.ddlTiered2) : 0;
            objtblTieredPackage.Tired3 = (ObjModel.ddlTiered3 != "-1") ? Int32.Parse(ObjModel.ddlTiered3) : 0;
            objtblTieredPackage.TiredOptional = (ObjModel.ddlTiredOptional != "-1") ? Int32.Parse(ObjModel.ddlTiredOptional) : optional;
            objtblTieredPackage.TimeFrame = (ObjModel.ddlTimeFrame != "-1") ? Convert.ToInt32(ObjModel.ddlTimeFrame) : 0;
            objtblTieredPackage.HostPrice = ObjModel.HostPrice;
            objtblTieredPackage.OptionalPrice = ObjModel.OptionalPrice;
            if (ObjModel.Tier2Price == null) { objtblTieredPackage.Tired2Price = null; } else { objtblTieredPackage.Tired2Price = decimal.Parse(ObjModel.Tier2Price); }
            if (ObjModel.Tier3Price == null) { objtblTieredPackage.Tired3Price = null; } else { objtblTieredPackage.Tired3Price = decimal.Parse(ObjModel.Tier3Price); }

            ObjModel.TieredHost = !string.IsNullOrEmpty(ObjModel.TieredHost) ? ObjModel.TieredHost : string.Empty;
            if (ObjModel.TieredHost == "PS")
                objtblTieredPackage.Variable = "County State";
            else if (ObjModel.TieredHost == "CCR1")
                objtblTieredPackage.Variable = "State";
            else
                objtblTieredPackage.Variable = "";
            if (ObjModel.chkTiered == true)
            {
                status = objBALProductPackages.InsertTeriedPackage(objtblTieredPackage);
            }
            else { status = 1; }
            return status;
        }

        public int UpdateTiered(int fkPackageId, PackagesModel ObjModel)
        {
            int status = 0;
            BALProductPackages objBALProductPackages = new BALProductPackages();
            tblTieredPackage objtblTieredPackage = new tblTieredPackage();
            int? optional = null;
            objtblTieredPackage.LastModifiedDate = DateTime.Now;
            objtblTieredPackage.fkPackageId = fkPackageId;
            objtblTieredPackage.IsAutomaticRun = ObjModel.IsAutomaticRun;
            objtblTieredPackage.Host = (ObjModel.ddlTieredHost != "-1") ? Int32.Parse(ObjModel.ddlTieredHost) : 0;
            objtblTieredPackage.Tired2 = (ObjModel.ddlTiered2 != "-1") ? Int32.Parse(ObjModel.ddlTiered2) : 0;
            objtblTieredPackage.Tired3 = (ObjModel.ddlTiered3 != "-1") ? Int32.Parse(ObjModel.ddlTiered3) : 0;
            objtblTieredPackage.TiredOptional = (ObjModel.ddlTiredOptional != "-1") ? Int32.Parse(ObjModel.ddlTiredOptional) : optional;
            objtblTieredPackage.TimeFrame = (ObjModel.ddlTimeFrame != "-1") ? Convert.ToInt32(ObjModel.ddlTimeFrame) : 0;
            objtblTieredPackage.HostPrice = ObjModel.HostPrice;
            objtblTieredPackage.OptionalPrice = ObjModel.OptionalPrice;
            if (ObjModel.Tier2Price == null) { objtblTieredPackage.Tired2Price = null; } else { objtblTieredPackage.Tired2Price = decimal.Parse(ObjModel.Tier2Price); }
            if (ObjModel.Tier3Price == null) { objtblTieredPackage.Tired3Price = null; } else { objtblTieredPackage.Tired3Price = decimal.Parse(ObjModel.Tier3Price); }
            ObjModel.TieredHost = !string.IsNullOrEmpty(ObjModel.TieredHost) ? ObjModel.TieredHost : string.Empty;
            if (ObjModel.TieredHost == "PS")
                objtblTieredPackage.Variable = "County State";
            else if (ObjModel.TieredHost == "CCR1")
                objtblTieredPackage.Variable = "State";
            else
                objtblTieredPackage.Variable = "";
            if (ObjModel.chkTiered == true)
            {
                status = objBALProductPackages.UpdateTeriedPackage(objtblTieredPackage);
            }
            else
            {
                status = objBALProductPackages.DeleteTeiredPacakge(fkPackageId);
            }
            return status;
        }

    }
}



