﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using System.Web.Security;
using Emerge.Common;
using Emerge.Data;
using Emerge.Services;
using System.IO;
using System.Text;
using Emerge.Controllers.Common;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /EmergeReviews/

        public ActionResult EmergeReviews()
        {
           int pkCompanyId = 0;
            int pkLocationId = 0;
            int pkCompanyUserId = 0;
            SavedReportModel ObjModel = new SavedReportModel();
            ObjModel.pkProductApplicationId = 0;
            string strqryst = string.Empty;
            bool ShowIncomplete = false;
            //BALContentManagement objBALContentManagement = new BALContentManagement();
            MembershipUser ObjMembership = Membership.GetUser(User.Identity.Name);
            string[] Role;
            Role = new string[0];
            if (ObjMembership != null)
            {
                Role = Roles.GetRolesForUser(ObjMembership.Email);
                ObjModel.UserRoles = Role;
                ObjModel.Role = 1;//Admin
            }

            #region Query String

            if (!string.IsNullOrEmpty(Request.QueryString["qryst"]))
            {
                strqryst = Request.QueryString["qryst"];
                ObjModel.Keyword = strqryst;
            }

            #endregion

            #region Get Columns from Profile

            try
            {
                MembershipUser Member = Membership.GetUser(User.Identity.Name);
                if (Member != null)
                {
                    ProfileCommon Profile = new ProfileCommon();
                    ProfileModel userProfile = Profile.GetProfile(Member.Email);
                    if (userProfile.RecordSize != string.Empty)
                    {
                        TempData["PageSize"] = userProfile.RecordSize;
                        ObjModel.PageSize = Convert.ToInt32(userProfile.RecordSize);
                    }
                    else
                    {
                        ObjModel.PageSize = 25;
                    }
                    if (userProfile.PageNo != string.Empty)
                    {
                        TempData["PageNo"] = userProfile.PageNo;
                        ObjModel.PageNo = Convert.ToInt32(userProfile.PageNo);
                    }
                    else
                    {
                        ObjModel.PageNo = 1;
                    }
                    if (Request.QueryString["_bck"] != null)
                    {
                        ObjModel.Keyword = userProfile.SearchedText;
                        strqryst = userProfile.SearchedText;
                        pkCompanyId = Convert.ToInt32(userProfile.Company);
                        pkLocationId = Convert.ToInt32(userProfile.Location);
                        pkCompanyUserId = Convert.ToInt32(userProfile.User);
                        ObjModel.pkCompanyUserId= Convert.ToInt32(userProfile.User);
                        ObjModel.pkProductApplicationId = Convert.ToInt32(userProfile.DataSource);
                       
                        
                    }
                }

                #region User FullName
                if (Session["UserFullName"] != null)
                {
                    ViewBag.UserFullName = Convert.ToString(Session["UserFullName"]);
                }
                else
                {
                    if (Member != null)
                    {
                        BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
                        Guid UserId = new Guid(Member.ProviderUserKey.ToString());
                        List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjData = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(UserId);
                        Session["CurrentUserCompanyName"] = ObjData.First().CompanyName;
                        ViewBag.UserFullName = ObjData.First().FirstName + " " + ObjData.First().LastName;

                    }
                }
                #endregion
            }
            catch (Exception)
            {
            }
            #endregion

            using (Corporate.CorporateController ObjCorporate = new Corporate.CorporateController())
            {
                ObjCorporate.SavedReportPageLoad(ObjModel, pkCompanyId, pkLocationId, pkCompanyUserId, strqryst.Replace("'", "''"), ShowIncomplete, Role);
            }
            int rpttotal = GetReportCount();
            ObjModel.TotalRecords = rpttotal;
            return View(ObjModel);
        }


        public int GetReportCount()
        {
            int ReportTotal = 0;

            BALOrders ObjBALOrders = new BALOrders();

            List<proc_GetSavedReportsCountResult> ObjCountStatus = new List<proc_GetSavedReportsCountResult>();

            ObjCountStatus = ObjBALOrders.GetSavedReportsCount(0,
                                                                                                                 0,
                                                                                                                 0,
                                                                                                                 0,
                                                                                                                 string.Empty,
                                                                                                                 string.Empty,
                                                                                                                 string.Empty,                                                                                                                
                                                                                                                 0,
                                                                                                                 "InReviewReports"
                                                                                                                 ,false,true);

            //CommonSavedReportController ObjController = new CommonSavedReportController();

          
            ObjCountStatus = ObjCountStatus.Where(rec => rec.OrderStatusDisplay == "In Review").ToList();
            ObjCountStatus = ObjCountStatus.Where(rec => rec.OrderStatus != 4).ToList();
          
            ReportTotal = ObjCountStatus.Count();
            return ReportTotal;
        }

      






        public ActionResult EnableDisableNoResponseEmail(int OrderId, bool IsEnable)
        {
            int status= -1;
            BALEmergeReview ObjReview = new BALEmergeReview();
            try
            {
                status = ObjReview.SetNoResponseStatus(OrderId,IsEnable);
            }
            catch
            { 
            }
            return Json(status);
        }

        public ActionResult GetListofInReviewReports(int page, int pageSize, string group, List<GridSort> sort, List<GridFilter> filters, int pkCompanyId, int LocationId, int CompanyUserId, string Keyword, int pkProductApplicationId, bool ShowIncomplete)
        {


            //List<Proc_GetSavedReportsForMVCResult> ObjData = null;
            //BALOrders ObjBALOrders = new BALOrders();
            //static values
            string ColumnName = "OrderDt";
            string Direction = "DESC";
            //string ReferenceCode = string.Empty;
            //bool SavedReport6Month = false;
            #region Get Columns from Profile


            MembershipUser Member = Membership.GetUser(User.Identity.Name);
            string[] UserRole = Roles.GetRolesForUser(Member.Email);
            //if (UserRole.Contains("SystemAdmin"))
            //{
            //    SavedReport6Month = true;
            //}

            if (Member != null)
            {
                ProfileCommon Profile = new ProfileCommon();
                ProfileModel userProfile = Profile.GetProfile(Member.Email);
                if (userProfile.SortColumn != string.Empty)
                {
                    ColumnName = userProfile.SortColumn;

                    if (ColumnName == "ApplicantName" || ColumnName == "OrderNo" || ColumnName == "ReportIncludes" || ColumnName == "CompanyName" || ColumnName == "Location" || ColumnName == "User")
                    {
                        Direction = "ASC";
                    }
                    else
                    {
                        Direction = "DESC";
                    }

                }
                if (TempData["PageSize"] != null)
                {
                    pageSize = Convert.ToInt32(TempData["PageSize"]);
                }
                if (TempData["PageNo"] != null)
                {
                    page = Convert.ToInt32(TempData["PageNo"]);
                }
            }

            #endregion

            #region Sorting

            if (sort != null)
            {
                Direction = sort[0].Dir;
                ColumnName = sort[0].Field;
            }

            #endregion

            #region Filtering

            string ToDate = "";
            string FromDate = "";
            string ReportName = "InReviewReports";

            if (filters != null)
            {
                int pkOrderDateId = Convert.ToInt32(filters[0].Value);
                if (pkOrderDateId != -5) //-5 Means Reset Filters
                {
                    #region Set Values

                    pkProductApplicationId = Int32.Parse(filters[1].Value);
                    if (UserRole.Contains("BasicUser"))
                    {
                        CompanyUserId = 0;
                    }
                    else
                    {
                        CompanyUserId = (filters[2].Value == null ? 0 : Int32.Parse(filters[2].Value));
                    }
                    pkCompanyId = (filters[3].Value == null ? 0 : Int32.Parse(filters[3].Value));
                    LocationId = (filters[4].Value == null ? 0 : Int32.Parse(filters[4].Value));
                    Keyword = filters[5].Value ?? string.Empty;
                    int Month = Convert.ToInt32(filters[6].Value);
                    int Year = Convert.ToInt32(filters[7].Value);

                    DateTime today = DateTime.Today;
                    if (pkOrderDateId == 1)
                    {
                        ToDate = Convert.ToString(today.ToShortDateString());
                        FromDate = Convert.ToString(today.ToShortDateString());
                    }
                    else if (pkOrderDateId == 2)
                    {
                        FromDate = new DateTime(today.Year, today.Month, 1).ToShortDateString();
                        ToDate = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month)).ToShortDateString();
                    }
                    else if (pkOrderDateId == 3)
                    {
                        FromDate = new DateTime(today.Year, today.Month, 1).AddMonths(-1).ToShortDateString();
                        ToDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(-1).ToShortDateString();
                    }
                    else if (pkOrderDateId == 4)
                    {
                        FromDate = new DateTime(Year, Month, 1).ToShortDateString();
                        ToDate = new DateTime(Year, Month, DateTime.DaysInMonth(Year, Month)).ToShortDateString();
                    }
                    else if (pkOrderDateId == 5)
                    {
                        FromDate = filters[8].Value;
                        ToDate = filters[8].Value;
                    }
                    else if (pkOrderDateId == 6)
                    {
                        FromDate = filters[9].Value;
                        ToDate = filters[10].Value;
                    }
                  //  ReportName = filters[11].Value;
                    ShowIncomplete = Convert.ToBoolean(filters[13].Value);
                    //string pkReferenceCode = filters[14].Value;
                    //if (pkReferenceCode != "--All--")
                    //{
                    //    ReferenceCode = filters[14].Value;
                    //}

                    #endregion

                }
                else //Reset filters
                {
                    #region Reset Filters

                    string[] Role = Roles.GetRolesForUser(Member.Email);
                    if (Role.Contains("SystemAdmin"))
                    {
                        pkCompanyId = 0;
                    }
                    if (Role.Contains("SystemAdmin") || Role.Contains("CorporateManager") || Role.Contains("SupportManager") || Role.Contains("SupportTechnician"))
                    {
                        LocationId = 0;
                        CompanyUserId = 0;
                    }
                    Keyword = string.Empty;
                    pkProductApplicationId = 0;
                    ShowIncomplete = false;
                    #endregion
                }
            }
            #endregion

            #region Save Profile

            if (Member != null)
            {
                try
                {
                    ProfileCommon Profile = new ProfileCommon();
                    ProfileModel userProfile = Profile.GetProfile(Member.Email);
                    Profile.SetGeneralProfile(Member.Email, "PageNo", page.ToString());
                    Profile.SetGeneralProfile(Member.Email, "RecordSize", pageSize.ToString());

                    Profile.SetGeneralProfile(Member.Email, "SearchedText", Keyword);
                    Profile.SetGeneralProfile(Member.Email, "DataSource", pkProductApplicationId.ToString());
                    Profile.SetGeneralProfile(Member.Email, "Company", pkCompanyId.ToString());
                    Profile.SetGeneralProfile(Member.Email, "Location", LocationId.ToString());
                    Profile.SetGeneralProfile(Member.Email, "User", CompanyUserId.ToString());
                }
                catch (Exception)
                {
                }
            }

            #endregion

            #region Call DB
            //BALGeneral ObjBALGeneral = new BALGeneral();

            //ObjData = ObjBALGeneral.GetSavedEmergeReviewList(pkProductApplicationId,
            //                                          CompanyUserId,
            //                                          pkCompanyId,
            //                                          LocationId,
            //                                          ColumnName,
            //                                          Direction,
            //                                          true,
            //                                          page,
            //                                          pageSize,
            //                                          FromDate,
            //                                          ToDate,
            //                                          Keyword.Replace("'", "''"),
            //                                          0,
            //                                          ReportName,
            //                                          ShowIncomplete, ReferenceCode, SavedReport6Month).Select(p => new Proc_GetSavedReportsForMVCResult
            //                                          {
            //                                              OrderDt = p.OrderDt,
            //                                              IsSyncpod = p.IsSyncpod,
            //                                              InReviewed = p.InReviewed,
            //                                              IsDrugTestingOrder = p.IsDrugTestingOrder,
            //                                              TotalRec = p.TotalRec,
            //                                              OrderId = p.OrderId,
            //                                              PendingOrderUpdateNote = p.PendingOrderUpdateNote,
            //                                              OrderNo = p.OrderNo,
            //                                              OrderStatus = p.OrderStatus,
            //                                              ApplicantDOB = p.ApplicantDOB,
            //                                              fkLocationId = p.fkLocationId,
            //                                              ApplicantName = p.ApplicantName,
            //                                              CompanyName = p.CompanyName,
            //                                              Days = p.Days,
            //                                              FirstName = p.FirstName,
            //                                              IsViewed = p.IsViewed,
            //                                              TrackingReferenceCode = p.TrackingReferenceCode,
            //                                              LastName = p.LastName,
            //                                              Location = p.Location,
            //                                              OrderStatusDisplay = p.OrderStatusDisplay,
            //                                              OrderType = p.OrderType,
            //                                              ReportsWithStatus = ReportsWithStatus(p.ReportsWithStatus, Guid.Parse(p.OrderId.ToString())), //
            //                                              ReportIncludes = p.ReportIncludes,
            //                                              RowNo = p.RowNo,
            //                                              StateCode = p.StateCode,
            //                                              UserEmail = p.UserEmail,
            //                                              IsSubmitted = p.IsSubmitted,
            //                                              IsNoResponse= p.IsNoResponse,
            //                                              HitStatus = p.HitStatus
            //                                          }).ToList();

            BALEmergeReview ObjReview = new BALEmergeReview();
            var PendingReviewCollection = ObjReview.GetReportsForReview(pkProductApplicationId,
                                                                                                CompanyUserId,
                                                                                                pkCompanyId,
                                                                                                LocationId,
                                                                                                ColumnName,
                                                                                                Direction,
                                                                                                true,
                                                                                                page,
                                                                                                pageSize,
                                                                                                FromDate,
                                                                                                ToDate,
                                                                                                Keyword,
                                                                                                 0,1);


            //ObjData = ObjBALOrders.GetSavedReportsMVC(pkProductApplicationId,
            //                                          CompanyUserId,
            //                                          pkCompanyId,
            //                                          LocationId,
            //                                          ColumnName,
            //                                          Direction,
            //                                          true,
            //                                          page,
            //                                          pageSize,
            //                                          FromDate,
            //                                          ToDate,
            //                                          Keyword,
            //                                          0,
            //                                          ReportName,
            //                                          ShowIncomplete);



            int TotalRec = 0;
          
            
            // OrderDate = new DateTime(today.Year, today.Month, 1).ToShortDateString();
            // OrderDate = new DateTime(2013, 7, 28).ToShortDateString();
            // ObjData = ObjData.Where(rec => rec.OrderStatusDisplay == "In Review" && rec.OrderDt > OrderDate).ToList();
           // ObjData = ObjData.Where(rec => rec.OrderStatusDisplay == "In Review").ToList();
         //   ObjData = ObjData.Where(rec => rec.OrderStatus != 4).ToList();
            if (PendingReviewCollection.Count() > 0)
            {
                TotalRec = PendingReviewCollection.Count();
            }

            #endregion

            #region Get Count Mails

            string ReportCounts = string.Empty;

           
            #endregion

            return Json(new { Products = PendingReviewCollection, TotalCount = TotalRec, ReportCounts = ReportCounts, ReportName = ReportName });

        }


        public string GetReportCounter(List<proc_GetSavedReportsCountResult> ObjCountStatus)
        {
            StringBuilder st = new StringBuilder();
            int j = 0, k = 0, l = 0, m = 0, n = 0, O = 0, P = 0;
            for (int i = 0; i < ObjCountStatus.Count; i++)
            {
                if (ObjCountStatus.ElementAt(i).OrderStatus == 1) { l++; }
                if (ObjCountStatus.ElementAt(i).OrderType == 2 && ObjCountStatus.ElementAt(i).IsSubmitted == false && ObjCountStatus.ElementAt(i).OrderStatus != 2) { P++; }
                else
                {
                    if (ObjCountStatus.ElementAt(i).IsViewed == true && ObjCountStatus.ElementAt(i).OrderStatus != 1 && ObjCountStatus.ElementAt(i).InReviewed == false) { j++; }
                    if (ObjCountStatus.ElementAt(i).IsViewed == false) { k++; }
                    if (ObjCountStatus.ElementAt(i).InReviewed == true) { m++; }
                    if (ObjCountStatus.ElementAt(i).OrderStatus == 3) { n++; }
                    if (ObjCountStatus.ElementAt(i).OrderStatus == 2 || ObjCountStatus.ElementAt(i).InReviewed == true) { O++; }
                }
            }
            st.Append("<Select id='kendodropdownlist' style='width:168px;' onchange='onChanged()'>");
            st.Append("<option  id='AllReports'  onclick='BindGrid(0,this.id)'>All (" + ObjCountStatus.Count() + ")" + "</option>");
            st.Append("<option  id='ReadReports' onclick='BindGrid(0,this.id)'>Read (" + j + ")</option>");
            st.Append("<option  id='Submitted'  onclick='BindGrid(0,this.id)'>Submit (" + P + ")</option>");
            st.Append("<option  id='UnreadReports'  onclick='BindGrid(0,this.id)'>Unread  (" + k + ")</option>");
            st.Append("<option  id='PendingReports'  onclick='BindGrid(0,this.id)'>Pending (" + l + ")</option>");
            st.Append("<option  id='InReviewReports'  onclick='BindGrid(0,this.id)'>In Review (" + m + ")</option>");
            st.Append("<option id='IncompleteReports'  onclick='BindGrid(0,this.id)'>Incomplete (" + n + ")</option>");
            st.Append("<option id='CompleteReports'   onclick='BindGrid(0,this.id)'>Complete (" + O + ")</option>");
            st.Append("</Select>");
            return st.ToString();
        }


        public string GetArchiveReportCounter(List<proc_GetSavedReportsCountResult> ObjCountStatus)
        {
            StringBuilder st = new StringBuilder();
            st.Append("<table style='font-size: 12px;' class='RoundBorder' width='100%'>");
            st.Append("<tr><td><a name='anchorfolder' id='IncompleteReports'>Incomplete (" + ObjCountStatus.Count() + ")</a></td></tr>");
            st.Append("</table>");
            return st.ToString();
        }



        public string ReportsWithStatus(string ReportsWithStatus, int OrderId)
        {
            string FormattedProductCode = "";
            try
            {
                if (ReportsWithStatus != null)
                {
                    if (ReportsWithStatus.Trim() != "")
                    {
                        FormattedProductCode = ReportWithImage(ReportsWithStatus.Trim());
                        if (FormattedProductCode == "")
                        {
                            FormattedProductCode = GetProductForReview(OrderId);
                        }
                    }
                    else
                    {
                        FormattedProductCode = GetProductForReview(OrderId);
                    }
                }
                else { }
            }
            catch
            {
            }
            return FormattedProductCode;
        }

        public ActionResult GetStatusLogDescrptionById(int pklogId)
        {
            string GetDescrption= string.Empty;
            BALEmergeReview objBALEmergeReview = new BALEmergeReview();
            GetDescrption=  objBALEmergeReview.GetStatusLogDescrptionById(pklogId);
            return Json(new {GetDescrption = GetDescrption});
        }


        private string ReportWithImage(string Status)
        {
            string ReportNames = Status;
            string Reportwithstatus = "";
            string[] strSplit = ReportNames.Split(',');
            Reportwithstatus += "";
            for (int Irow = 0; Irow < strSplit.Length; Irow++)
            {
                if (strSplit[Irow] != "")
                {
                    //if (Irow > 0)
                    //{
                    //    Reportwithstatus += ", ";
                    //}
                    string[] subSplit = strSplit[Irow].Split('_');
                    if (subSplit.Count() > 2)
                    {
                        //this for live runner
                        if (subSplit[1] == "1")
                        {
                            Reportwithstatus += subSplit[0] + "<img src=" + @Url.Content("~/Content/themes/base/Images/LiveRunnerSmallLogo.png") + " height='12px' width='12px'/>,";
                        }
                        else
                        {
                            Reportwithstatus += subSplit[0] + ",";
                        }
                        //this for for review report
                        //if (subSplit[2] == "1")
                        //{
                        //    if (File.Exists(Request.PhysicalApplicationPath + ImgReview))
                        //    {
                        //    }
                        //}
                    }
                }
            }

            Reportwithstatus += "";
            int CommaIndex = Reportwithstatus.LastIndexOf(',');
            //char[] FormattedProductCodesChar = Reportwithstatus.ToCharArray();

            string FinalString = Reportwithstatus.Remove(CommaIndex, 1);
            return FinalString; // Reportwithstatus;
        }

        private string GetProductForReview(int OrderId)
        {
            string FormattedProductCodes = "";
            BALEmergeReview ObjBALEmergeReview = new BALEmergeReview();
            FormattedProductCodes = "";
            Dictionary<string, string> ObjLiveRunnerProduct = ObjBALEmergeReview.GetProductsForFormatting(OrderId, "liverunner");
            if (ObjLiveRunnerProduct.Count != 0)
            {
                foreach (KeyValuePair<string, string> KV in ObjLiveRunnerProduct)
                {
                    string[] KeyArr = KV.Key.Split('_');
                    string LiveRunner = KeyArr[0].Trim();
                    string ProductCode = KV.Value.Trim();
                    if (LiveRunner.ToLower() == "true" && ProductCode.ToLower() != "rcx")
                    {
                        FormattedProductCodes += KV.Value + "<img src='../Resources/Images/LiveRunnerSmallLogo.png' height='11px' width='11px'/>,";
                    }
                    else
                        FormattedProductCodes += KV.Value + ", ";
                }
            }
            FormattedProductCodes += "";
            int CommaIndex = FormattedProductCodes.LastIndexOf(',');
            //char[] FormattedProductCodesChar = FormattedProductCodes.ToCharArray();

            string FinalString = FormattedProductCodes.Remove(CommaIndex, 1);
            // string FinalString = string.Empty;
            return FinalString;
        }



    }
}
