﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using System.Web.Security;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using Ionic.Zip;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /Notice613/
        List<Proc_GetNoticeForAdminDashboardResult> RecordList = new List<Proc_GetNoticeForAdminDashboardResult>();
        public ActionResult PartialNotice613()
        {


            return PartialView("PartialNotice613");
        }
        //INT-250//added on 15 july 2016 for  INT-250
        public ActionResult Partial613NoticeExport()
        {
            return View();
        }

        //INT-250//added on 15 july 2016 for  INT-250
        public ActionResult ExportData(FormCollection frm)
        {
            try
            {
                using (System.Web.UI.WebControls.GridView gv = new System.Web.UI.WebControls.GridView())
                {
                    string dtFrom = frm["DateFrom"];
                    string dtTo = frm["DateTo"];
                    var Pdf = frm["pdf"];
                    var Email = frm["mail"];
                    if (Pdf == "on")
                    {
                        gv.DataSource = new BALOrders().SelectNotice613PDFStatus(dtFrom, dtTo);
                    }
                    else if (Email == "on")
                    {
                        gv.DataSource = new BALOrders().SelectNotice613EmailStatus(dtFrom, dtTo);
                    }
                    else
                    {
                        return RedirectToAction("Partial613NoticeExport");
                    }

                    gv.DataBind();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment; filename=613NoticeLog.xls");
                    Response.ContentType = "application/ms-excel";
                    Response.Charset = "";
                    using (StringWriter sw = new StringWriter())
                    {
                        System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
                        gv.RenderControl(htw);
                        Response.Output.Write(sw.ToString());
                        Response.Flush();
                        Response.End();
                    }
                }
            }
            catch //(Exception ex)
            {
                throw;
            }

            return RedirectToAction("Partial613NoticeExport");

        }

        //added on 15 july 2016 for  INT-250


        public ActionResult GetReportforAdminNotice(int page, int pageSize, bool ReportType)
        {
            BALOrders ObjBALOrders = new BALOrders();
            string SortingColoum = "OrderDt";
            string SortingDir = "asc";
            int reporttype = 2;
            if (Session["pagesize"] != null)
            {
                Session["pagesize"] = null;
            }
            Session["pagesize"] = pageSize;
            int fkOrderId = 0;

            var list = ObjBALOrders.GetNoticeForAdminDashboard(SortingColoum, SortingDir, true, page, pageSize, reporttype, ReportType, fkOrderId);

            if (list.Count != 0)
            {
                return Json(new
                {
                    Products = list,
                    TotalCount = list.ElementAt(0).TotalRec
                },
                        JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    Products = 0,
                    TotalCount = 0
                },
                                      JsonRequestBehavior.AllowGet);
            }
        }



        public FilePathResult DownLoadPdfforSent(string Status, string pkOrderDetailId, string fkOrderId)
        {
            BALOrders ObjBALOrders = new BALOrders();

            string pdfFileName = string.Empty;
            var Pagesize = 10;
            if (Status == "ToView")
            {
                RecordList = ObjBALOrders.GetNoticeForAdminDashboard("OrderDt", "asc", true, 1, Pagesize, 2, false, Int32.Parse(fkOrderId));
            }
            else if (Status == "ToAll" || Status == "ToSelected")
            {
                //Guid OredreId= Status == "ToView" ? Guid.Parse(fkOrderId) : Guid.Empty ;
                RecordList = ObjBALOrders.GetNoticeForAdminDashboard("OrderDt", "asc", true, 1, Pagesize, 2, true, 0);
                if (Status == "ToSelected")
                {
                    char[] arr = { '_' };
                    var fkOrderIdList = fkOrderId.Split(arr, StringSplitOptions.RemoveEmptyEntries);
                    RecordList = RecordList.Where(t => fkOrderIdList.Contains(t.fkOrderId.ToString())).ToList();
                }
                // zipfilename = RecordList.ElementAt(0).ApplicantName + ".zip";
            }
            else
            {
                RecordList = ObjBALOrders.GetNoticeForAdminDashboard("OrderDt", "asc", true, 1, Pagesize, 2, true, Int32.Parse(fkOrderId));
            }
            pdfFileName = GeneratePdfOf613NoticeSent(fkOrderId, RecordList);
            if (Status == "ToAll" || Status == "ToSelected")
            {
                pdfFileName = "Intelifi_Inc_613Notice";
            }

            string FilePath = HttpContext.Server.MapPath(@"~/Resources/Upload/613NoticePdf/613NoticePdfFileSent.pdf");
            VendorServices.GeneralService.WriteLog("Downloaded file path : " + FilePath, "613FileIssue");
            string fileext = System.IO.Path.GetExtension("613NoticePdf.pdf");
            string CType = "application/" + fileext.Replace(".", "");
            string strFileTitle = pdfFileName;
            VendorServices.GeneralService.WriteLog("Downloaded file path : " + pdfFileName, "613FileIssue");
            return File(FilePath, CType, strFileTitle);


        }
        public FilePathResult GenerateAuthorizationform(string OrderId)
        {
            int Order_ID = Int32.Parse(OrderId);
            string pdfFileName = string.Empty;
            BALGeneral ObjBALGeneral = new BALGeneral();
            CustomOrderCollection ObjCustomOrderCollection = new CustomOrderCollection();
            BALOrders ObjBALOrders = new BALOrders();
            ObjCustomOrderCollection = ObjBALOrders.GetOrderDetailsByOrderId(Order_ID);
            DateTime Ordereddate = Convert.ToDateTime(ObjCustomOrderCollection.ListOrderDetail.ElementAt(0).OrderDt.ToString());
            string Ordered_date = Convert.ToString(Ordereddate.ToShortDateString());

            string firstname = ObjCustomOrderCollection.ObjOrderSearchedData.SearchedFirstName;
            string LastName = ObjCustomOrderCollection.ObjOrderSearchedData.SearchedLastName;
            string ssn = ObjCustomOrderCollection.ObjOrderSearchedData.SearchedSsn.ToString();
            string dob = ObjCustomOrderCollection.ObjOrderSearchedData.SearchedDob.ToString();
            string email = ObjCustomOrderCollection.ObjOrderSearchedData.SearchedApplicantEmail.ToString();
            int CompanyId = Int32.Parse(ObjCustomOrderCollection.ListOrderDetail.ElementAt(0).fkCompanyId.ToString());
            string FilePath = string.Empty;
            pdfFileName = ObjBALGeneral.GenerateAuthorizationform(firstname, ssn, dob, email, CompanyId, Ordered_date, LastName);
            if (CompanyId == 6587)
            {
                FilePath = HttpContext.Server.MapPath(@"~/Resources/Upload/613NoticePdf/axiomstaffingdisclosureandauthorization.pdf");
            }

            else
            {
                FilePath = HttpContext.Server.MapPath(@"~/Resources/Upload/613NoticePdf/disclosureandauthorization.pdf");
            }
            string fileext = System.IO.Path.GetExtension("613NoticePdf.pdf");
            string CType = "application/" + fileext.Replace(".", "");
            string strFileTitle = pdfFileName;

            return File(FilePath, CType, strFileTitle);


        }

        public ActionResult Complete613Notice(string pkOrderDetailId)
        {
            BALOrders ObjBALOrders = new BALOrders();
            int IsSendNotice = ObjBALOrders.IsSendNoticeEmail(Int32.Parse(pkOrderDetailId));
            if (IsSendNotice != 1)
            {
                ObjBALOrders.UpdateIsEmailNotice(Int32.Parse(pkOrderDetailId), true, true);
            }
            else
            {
                return RedirectToAction("AdminSearch", "Control");
            }

            return Json(new { Success = 1 });
        }


        public ActionResult EditPDFfile(string Status, string pkOrderDetailId, string fkOrderId)
        {
            BALOrders ObjBALOrders = new BALOrders();
            string zipfilename = string.Empty;
            string pdfFileName = string.Empty;
            try
            {
                var Pagesize = 0;
                ///Pagesize is Equal to All Applicant Show In Grid or value
                if (Session["pagesize"] != null)
                {
                    Pagesize = Convert.ToInt32(Session["pagesize"]);
                }
                else
                {
                    Pagesize = 10;
                }

                //   var Pagesize = 20; // This is Equal to All Applicant Show In Grid or value of Page Size Dropdown
                if (Status == "ToAll")
                {
                    RecordList = ObjBALOrders.GetNoticeForAdminDashboard("OrderDt", "asc", true, 1, Pagesize, 2, false, 0);
                    zipfilename = RecordList.ElementAt(0).ApplicantName + ".zip";
                }
                else if (Status == "ToOne")
                {
                    RecordList = ObjBALOrders.GetNoticeForAdminDashboard("OrderDt", "asc", true, 1, Pagesize, 2, false, Int32.Parse(fkOrderId));
                    zipfilename = RecordList.ElementAt(0).ApplicantName + ".zip";
                }

                if (RecordList.Count > 0)
                {
                    for (int i = 0; i < RecordList.Count; i++)
                    {
                        //  Temporary passing false in IsNoticeEmail parameters finally it will be true.
                        int IsSendNotice = ObjBALOrders.IsSendNoticeEmail(Int32.Parse(RecordList.ElementAt(i).pkOrderDetailId.ToString()));
                        if (IsSendNotice != 1)
                        {
                            ObjBALOrders.UpdateIsEmailNotice(Int32.Parse(RecordList.ElementAt(i).pkOrderDetailId.ToString()), true, true);
                        }
                        else
                        {
                            return RedirectToAction("AdminSearch", "Control");
                        }
                    }
                }
                else
                {
                    return RedirectToAction("AdminSearch", "Control");
                }

                pdfFileName = GeneratePdfOf613Notice(fkOrderId, RecordList);
                if (Status == "ToAll")
                {
                    pdfFileName = "usaintel_613Notice";
                }
            }


            catch //(Exception ex)
            {

            }
            //if (Status == "ToOne")
            //{
            string FilePath = HttpContext.Server.MapPath(@"~/Resources/Upload/613NoticePdf/613NoticePdfFile.pdf");
            string fileext = System.IO.Path.GetExtension("613NoticePdf.pdf");
            string CType = "application/" + fileext.Replace(".", "");
            string strFileTitle = pdfFileName;

            return File(FilePath, CType, strFileTitle);
            //}
            //else
            //{
            //    string FilePath = HttpContext.Server.MapPath(@"~/Resources/Upload/613NoticePdf/613NoticePdfFile.pdf");
            //    string fileext = System.IO.Path.GetExtension("613NoticePdf.pdf");
            //    string CType = "application/" + fileext.Replace(".", "");
            //    string strFileTitle = "613_Notice.zip";
            //    using (ZipFile zip = new ZipFile())
            //    {
            //        zip.AddFile(FilePath, "");
            //        zip.Save(FilePath);
            //    }

            //    FileInfo file = new FileInfo(FilePath);
            //    Response.Clear();
            //    Response.AppendHeader("Content-Disposition", "attachment; filename=" + strFileTitle);
            //    Response.ContentType = "application/x-zip-compressed";
            //    Response.WriteFile(FilePath);
            //    Response.End();
            //    return File(FilePath, CType, strFileTitle);
            //}


        }


        public string GeneratePdfOf613Notice(string OrderIds, List<Proc_GetNoticeForAdminDashboardResult> RecordList)
        {
            string pdfFileName = string.Empty;
            pdfFileName = "Intelifi_Inc_613Notice_" + DateTime.Now.ToFileTime().ToString() + ".pdf";
            iTextSharp.text.Rectangle ObjPageSize = new iTextSharp.text.Rectangle(PageSize.LETTER);
            Document ObjDocument = new Document(ObjPageSize, 55, 55, 15, 12);


            try
            {

                BALCompany ObjBALCompany = new BALCompany();
                BALOrders ObjBALOrders = new BALOrders();
                string UserName = string.Empty;
                string CompanyName = string.Empty;
                string Address = string.Empty;
                //string Address2 = string.Empty;
                string ApplicantName = string.Empty;
               // string ApplicantNamepdf = string.Empty;

                List<Proc_Get_RequestAndResponseResult> ObjProc_Get_RequestAndResponseResult = new List<Proc_Get_RequestAndResponseResult>();
                Proc_Get_RequestAndResponseResult ObjRequestAndResponseResult = new Proc_Get_RequestAndResponseResult();
                List<Proc_Get_CompanyDetailByMemberShipIdResult> ObjCompanyDetails = new List<Proc_Get_CompanyDetailByMemberShipIdResult>();
                Proc_Get_CompanyDetailByMemberShipIdResult ObjCompanyDetailByMemberShipIdResult = new Proc_Get_CompanyDetailByMemberShipIdResult();


                float Leading = 13;
                DateTime DateToBePrint = DateTime.Now;


                //string path = Request.PhysicalApplicationPath + "Resources\\Upload\\613NoticePdf\\613NoticePdfFile.pdf";


               // var writer = PdfWriter.GetInstance(ObjDocument, new FileStream(path, FileMode.Create));
                iTextSharp.text.Font FontInfoHeader = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.BOLD, iTextSharp.text.Color.BLACK);
                iTextSharp.text.Font FontInfoNormal = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 8, iTextSharp.text.Font.NORMAL, iTextSharp.text.Color.BLACK);
                iTextSharp.text.Font FontInfoHeadings = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 11, iTextSharp.text.Font.BOLD, iTextSharp.text.Color.BLACK);
                iTextSharp.text.Font FontInfoTotal = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.BOLD, iTextSharp.text.Color.BLACK);
                if (!string.IsNullOrEmpty(OrderIds))
                {
                    //string[] stringSeparators = new string[] { "," };
                   // string[] OrderIdArr = OrderIds.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                    #region for blankspace
                    Chunk spaceChunk = new Chunk("                                                                                                                     ");
                    #endregion
                    HeaderFooter ObjPdfFooter = new HeaderFooter(new Phrase(" ", FontInfoNormal), new Phrase(spaceChunk + DateToBePrint.ToString("MMMM dd, yyyy hh:mm tt"), FontInfoNormal));
                    // ObjDocument.Footer = ObjPdfFooter;
                    ObjPdfFooter.Alignment = Element.ALIGN_LEFT;
                    ObjDocument.Open();

                    if (RecordList.Count > 0)
                    {


                        for (int i = 0; i < RecordList.Count; i++)
                        {

                            ObjProc_Get_RequestAndResponseResult = ObjBALOrders.GetRequestAndResponseByOrderId(Int32.Parse(RecordList.ElementAt(i).fkOrderId.ToString()));
                            if (ObjProc_Get_RequestAndResponseResult.Count > 0)
                            {
                                ObjRequestAndResponseResult = ObjProc_Get_RequestAndResponseResult.First();
                                ObjCompanyDetails = ObjBALCompany.GetCompanyDetailByMemberShipIdResult(ObjRequestAndResponseResult.fkUserId);
                                if (ObjCompanyDetails.Count > 0)
                                {
                                    ObjCompanyDetailByMemberShipIdResult = ObjCompanyDetails.First();

                                    UserName = ObjCompanyDetailByMemberShipIdResult.lastname + ", " + ObjCompanyDetailByMemberShipIdResult.firstname;
                                    CompanyName = ObjCompanyDetailByMemberShipIdResult.CompanyName;
                                    Address = ObjCompanyDetailByMemberShipIdResult.city + ", " + ObjCompanyDetailByMemberShipIdResult.statecode;
                                    ApplicantName = ObjRequestAndResponseResult.SearchedFirstName + " " + ObjRequestAndResponseResult.SearchedLastName + ",";
                                    pdfFileName = "Intelifi_Inc_613Notice_" + ObjRequestAndResponseResult.SearchedLastName + "_" + ObjRequestAndResponseResult.SearchedFirstName + ".pdf";


                                    #region Code For footer at specific Page's

                                    string footer = "";

                                    #region Mailing Address

                                    var ObjData = new tblOrderSearchedData();
                                    string stateName = "";
                                    string mailingAddress = "";
                                    string StreetAddress = string.Empty;
                                    string StreetName = string.Empty;
                                    int OrderId = Int32.Parse(RecordList.ElementAt(i).fkOrderId.ToString());
                                    using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                                    {
                                        ObjData = Dx.tblOrderSearchedDatas.FirstOrDefault(n => n.fkOrderId == OrderId);
                                        if (ObjData != null)
                                        {
                                            if (ObjData.search_state != null && ObjData.search_state != string.Empty)
                                            {
                                                stateName = ((ObjData.search_state != "-1") && (ObjData.search_state != null)) ? Dx.tblStates.FirstOrDefault(n => n.pkStateId == Convert.ToInt32(ObjData.search_state)).StateCode : stateName;
                                            }
                                            else
                                            {
                                                stateName = ((ObjData.SearchedStateId.ToString() != "-1") && (ObjData.SearchedStateId != null)) ? Dx.tblStates.FirstOrDefault(n => n.pkStateId == Convert.ToInt32(ObjData.SearchedStateId)).StateCode : stateName;
                                            }
                                            mailingAddress = ObjData.SearchedFirstName != string.Empty ? ObjData.SearchedFirstName + " " : mailingAddress;
                                            mailingAddress = ObjData.SearchedLastName != string.Empty ? mailingAddress + ObjData.SearchedLastName + "\n" : mailingAddress + "\n";
                                            if (ObjData.best_SearchedStreetAddress != null && ObjData.best_SearchedStreetAddress != string.Empty)
                                            {
                                                mailingAddress = ObjData.best_SearchedStreetAddress != string.Empty ? mailingAddress + ObjData.best_SearchedStreetAddress + " " : mailingAddress;
                                                StreetAddress = ObjData.best_SearchedStreetAddress != string.Empty ? ObjData.best_SearchedStreetAddress : string.Empty;
                                            }
                                            else if (ObjData.SearchedStreetAddress != null && ObjData.SearchedStreetAddress != string.Empty)
                                            {
                                                mailingAddress = ObjData.SearchedStreetAddress != string.Empty ? mailingAddress + ObjData.SearchedStreetAddress + " " : mailingAddress;
                                                StreetAddress = ObjData.SearchedStreetAddress != string.Empty ? ObjData.SearchedStreetAddress : string.Empty;
                                            }
                                            if (ObjData.best_SearchedStreetName != null && ObjData.best_SearchedStreetName != string.Empty)
                                            {
                                                StreetName = ObjData.best_SearchedStreetAddress != string.Empty ? ObjData.best_SearchedStreetAddress : string.Empty;
                                            }
                                            else if (ObjData.SearchedStreetName != null && ObjData.SearchedStreetName != string.Empty)
                                            {
                                                StreetName = ObjData.SearchedStreetName != string.Empty ? ObjData.SearchedStreetName : string.Empty;
                                            }

                                            if (StreetAddress != string.Empty && StreetName != string.Empty && StreetName != StreetAddress)
                                            {
                                                if (ObjData.best_SearchedStreetName != null && ObjData.best_SearchedStreetName != string.Empty)
                                                {
                                                    mailingAddress = ObjData.best_SearchedStreetName != string.Empty ? mailingAddress + ObjData.best_SearchedStreetName + " " : mailingAddress;
                                                }
                                                else if (ObjData.SearchedStreetName != null && ObjData.SearchedStreetName != string.Empty)
                                                {
                                                    mailingAddress = ObjData.SearchedStreetName != string.Empty ? mailingAddress + ObjData.SearchedStreetName + " " : mailingAddress;
                                                }
                                            }

                                            mailingAddress = ObjData.SearchedStreetType != string.Empty ? mailingAddress + ObjData.SearchedStreetType + " " : mailingAddress;
                                            mailingAddress = ObjData.SearchedApt != string.Empty ? mailingAddress + ObjData.SearchedApt + "\n" : mailingAddress + "\n";
                                            //   mailingAddress = ObjData.best_SearchedCity != string.Empty ? mailingAddress + ObjData.best_SearchedCity + ", ": mailingAddress;
                                            if (ObjData.best_SearchedCity != null && ObjData.best_SearchedCity != string.Empty)
                                            {
                                                mailingAddress = ObjData.best_SearchedCity != string.Empty ? mailingAddress + ObjData.best_SearchedCity + ", " : mailingAddress;
                                            }
                                            else if (ObjData.SearchedCity != null && ObjData.SearchedCity != string.Empty)
                                            {
                                                mailingAddress = ObjData.SearchedCity != string.Empty ? mailingAddress + ObjData.SearchedCity + ", " : mailingAddress;
                                            }

                                            //bool AddressStatus = mailingAddress != string.Empty ? true : false;
                                            if (mailingAddress != string.Empty)
                                            {
                                                mailingAddress = stateName != string.Empty ? mailingAddress + stateName : mailingAddress;
                                                if (ObjData.best_SearchedZipCode != null && ObjData.best_SearchedZipCode != string.Empty)
                                                {
                                                    mailingAddress = ObjData.best_SearchedZipCode != string.Empty ? mailingAddress + " " + ObjData.best_SearchedZipCode : mailingAddress;
                                                }
                                                else if (ObjData.SearchedZipCode != null && ObjData.SearchedZipCode != string.Empty)
                                                {
                                                    mailingAddress = ObjData.SearchedZipCode != string.Empty ? mailingAddress + " " + ObjData.SearchedZipCode : mailingAddress;
                                                }
                                            }
                                        }
                                    }

                                    #endregion

                                    #region Page 1 Table Declare
                                    iTextSharp.text.Table MainPageTbl = new iTextSharp.text.Table(2);
                                    MainPageTbl.Width = 100;
                                    MainPageTbl.Padding = 0;
                                    MainPageTbl.Cellpadding = 2;
                                    //MainPageTbl.Cellspacing = 1;
                                    MainPageTbl.DefaultCell.BackgroundColor = Color.WHITE;
                                    // MainPageTbl.CellsFitPage = true;
                                    MainPageTbl.TableFitsPage = true;
                                    MainPageTbl.BorderColor = Color.WHITE;
                                    MainPageTbl.Border = 0;
                                    #endregion

                                    #region  Page 1 Address Blocks  /*commented */

                                    //string usaIntelAddress = " USAintel \n 8730 Wilshire Blvd Suite 412 \n Beverly Hills, CA 90211";
                                    //Phrase fPhrase = new Phrase(Leading, usaIntelAddress, FontInfoNormal);
                                    //Cell fCell = new Cell();
                                    //fCell.BackgroundColor = new Color(255, 255, 255);
                                    //fCell.BorderColor = Color.BLACK;
                                    //fCell.BorderWidth = 1;
                                    //fCell.BorderWidthBottom = 1;
                                    //fCell.BorderWidthLeft = 1;
                                    //fCell.BorderWidthRight = 1;
                                    //fCell.BorderWidthTop = 1;
                                    //fCell.Width = 50;
                                    //fCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //fCell.Add(fPhrase);
                                    //MainPageTbl.AddCell(fCell);

                                    //fPhrase = new Phrase("\n\n");
                                    //fCell = new Cell();
                                    //fCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    //fCell.BackgroundColor = new Color(255, 255, 255);
                                    //fCell.BorderColor = Color.WHITE;
                                    //// fCell.BorderWidthTop = 1;
                                    //// fCell.BorderColorTop = Color.BLACK;
                                    //fCell.Width = 100;
                                    //fCell.Add(fPhrase);
                                    //MainPageTbl.AddCell(fCell);

                                    //fPhrase = mailingAddress != string.Empty ? new Phrase(Leading, mailingAddress, FontInfoNormal) : new Phrase(Leading, "\n\n\n\n", FontInfoNormal);
                                    //fCell = new Cell();
                                    //fCell.BackgroundColor = new Color(255, 255, 255);
                                    //fCell.BorderColor = Color.BLACK;
                                    //fCell.BorderWidth = 1;
                                    //fCell.BorderWidthBottom = 1;
                                    //fCell.BorderWidthLeft = 1;
                                    //fCell.BorderWidthRight = 1;
                                    //fCell.BorderWidthTop = 1;
                                    //fCell.Width = 50;
                                    //fCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //fCell.Add(fPhrase);
                                    //MainPageTbl.AddCell(fCell);


                                    //fPhrase = new Phrase("\n \n");
                                    //fCell = new Cell();
                                    //fCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    //fCell.BackgroundColor = new Color(255, 255, 255);
                                    //fCell.Width = 100;
                                    //fCell.BorderColor = Color.WHITE;
                                    //fCell.BorderColorBottom = Color.BLACK;
                                    //fCell.BorderWidthBottom = 1;
                                    //fCell.Add(fPhrase);
                                    //MainPageTbl.AddCell(fCell);
                                    #endregion

                                    #region Page 1 Address Blocks

                                    string usaIntelAddress = "\n Intelifi, Inc. \n 8730 Wilshire Blvd Suite 412 \n Beverly Hills, CA 90211 \n\n";
                                    Phrase fPhrase = new Phrase(Leading, usaIntelAddress, FontInfoNormal);
                                    Cell fCell = new Cell();
                                    fCell.BackgroundColor = new Color(255, 255, 255);
                                    //fCell.BorderColor = Color.BLACK;
                                    //fCell.BorderWidth = 1;
                                    //fCell.BorderWidthBottom = 1;
                                    //fCell.BorderWidthLeft = 1;
                                    //fCell.BorderWidthRight = 1;
                                    //fCell.BorderWidthTop = 1;
                                    fCell.Border = Rectangle.NO_BORDER;
                                    fCell.Width = 50;
                                    fCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    fCell.Add(fPhrase);
                                    MainPageTbl.AddCell(fCell);

                                    fCell = new Cell();
                                    fCell.BorderColor = Color.WHITE;
                                    fCell.Border = Rectangle.NO_BORDER;
                                    fCell.Width = 50;
                                    MainPageTbl.AddCell(fCell);



                                    fPhrase = new Phrase("\n\n");
                                    fCell = new Cell();
                                    fCell.Colspan = 2;
                                    fCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    fCell.BackgroundColor = new Color(255, 255, 255);
                                    fCell.BorderColor = Color.WHITE;
                                    fCell.Width = 100;
                                    fCell.Add(fPhrase);
                                    MainPageTbl.AddCell(fCell);




                                    //start commentt for int-132



                                    #region start commentt for int-132
                                    //fPhrase = mailingAddress != string.Empty ? new Phrase(Leading, "\n" + mailingAddress + "\n\n", FontInfoNormal) : new Phrase(Leading, "\n\n\n", FontInfoNormal);
                                    //fCell = new Cell();
                                    //fCell.BackgroundColor = new Color(255, 255, 255);
                                    //fCell.Border = Rectangle.NO_BORDER;
                                    //fCell.Width = 50;
                                    //fCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //fCell.Add(fPhrase);
                                    //MainPageTbl.AddCell(fCell);

                                    //fCell = new Cell();
                                    //fCell.BorderColor = Color.WHITE;
                                    //fCell.Border = Rectangle.NO_BORDER;
                                    //fCell.Width = 50;
                                    //MainPageTbl.AddCell(fCell);

                                    fPhrase = new Phrase("\n \n");
                                    fCell = new Cell();
                                    fCell.Colspan = 2;
                                    fCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    fCell.BackgroundColor = new Color(255, 255, 255);
                                    fCell.Width = 100;
                                    fCell.BorderColor = Color.WHITE;
                                    fCell.BorderColorBottom = Color.BLACK;
                                    fCell.BorderWidthBottom = 1;
                                    fCell.Add(fPhrase);
                                    MainPageTbl.AddCell(fCell);


                                    #endregion

                                    #endregion

                                    //end commentt for int-132







                                    #region page 1 Lower Table Declare
                                    iTextSharp.text.Table TblMain = new iTextSharp.text.Table(1);
                                    TblMain.Width = 100;
                                    TblMain.Padding = 0;
                                    TblMain.Cellpadding = 2;
                                    TblMain.Cellspacing = 1;
                                    TblMain.DefaultCell.Border = Rectangle.NO_BORDER;
                                    TblMain.Border = 2;
                                    TblMain.BorderColor = Color.WHITE;
                                    TblMain.CellsFitPage = true;
                                    TblMain.TableFitsPage = true;
                                    #endregion

                                    #region page 3 Table Declare
                                    iTextSharp.text.Table TblMain1 = new iTextSharp.text.Table(1);
                                    TblMain1.Width = 100;
                                    TblMain1.Padding = 0;
                                    TblMain1.Cellpadding = 2;
                                    TblMain1.Cellspacing = 1;
                                    TblMain1.DefaultCell.Border = Rectangle.NO_BORDER;
                                    TblMain1.CellsFitPage = true;
                                    TblMain1.TableFitsPage = true;
                                    #endregion


                                    //changes by ak

                                    #region page 1 Lower Table Data
                                    Phrase op = new Phrase();
                                    Cell ocell = new Cell(op);
                                    //Chunk ock = new Chunk();

                                    op = new Phrase(Leading, DateTime.Now.ToString("MMM-dd-yyyy"), FontInfoNormal);
                                    ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell.BackgroundColor = new Color(255, 255, 255);
                                    ocell.Add(op);
                                    TblMain.AddCell(ocell);


                                    ocell = new Cell();
                                    op = new Phrase(Leading, "Dear " + ApplicantName, FontInfoNormal);
                                    ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell.BackgroundColor = new Color(255, 255, 255);
                                    ocell.Add(op);
                                    TblMain.AddCell(ocell);

                                    string paragraph = string.Empty;
                                    #region Main Paragraph
                                    paragraph = "";
                                    paragraph = "Pursuant to FCRA 613 this is to advise you that " + CompanyName + " at " + Address
                                       + " has requested that we furnish information from public records regarding you. Enclosed is "
                                       + " a copy of the Summary of Rights prepared by the Consumer Financial Protection Bureau , and  "
                                       + " a  document to request a copy of your report. If you have any questions or concerns  regarding "
                                       + "the background report please contact Intelifi, Inc. 8730 Wilshire Blvd. Suite 412 Beverly Hills, "
                                       + " CA 90211 (800) 409-1819.\n\n";

                                    #endregion


                                    iTextSharp.text.Font FontInfoNormal1 = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.NORMAL, iTextSharp.text.Color.BLACK);


                                    ocell = new Cell();
                                    op = new Phrase(Leading, paragraph, FontInfoNormal1);
                                    ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell.BackgroundColor = new Color(255, 255, 255);
                                    ocell.Add(op);
                                    TblMain.AddCell(ocell);

                                    ocell = new Cell();
                                    op = new Phrase(Leading, "Sincerely,", FontInfoNormal);
                                    ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell.BackgroundColor = new Color(255, 255, 255);
                                    ocell.Add(op);
                                    TblMain.AddCell(ocell);

                                    ocell = new Cell();
                                    op = new Phrase(Leading, "Intelifi, Inc.", FontInfoNormal);
                                    ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell.BackgroundColor = new Color(255, 255, 255);
                                    ocell.Add(op);
                                    TblMain.AddCell(ocell);

                                    ocell = new Cell();
                                    op = new Phrase(Leading, "Enclosures: Consumer Summary of Rights\nDocuments Request Form", FontInfoNormal);
                                    ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell.BackgroundColor = new Color(255, 255, 255);
                                    ocell.Add(op);
                                    TblMain.AddCell(ocell);

                                    ocell = new Cell();
                                    op = new Phrase(Leading, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", FontInfoNormal);
                                    ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell.BackgroundColor = new Color(255, 255, 255);
                                    ocell.Add(op);
                                    TblMain.AddCell(ocell);
                                    #endregion
                                    //changes by ak

                                    #region Page1 Footer
                                    iTextSharp.text.Table footerTbl1 = new iTextSharp.text.Table(1);
                                    footerTbl1.Width = 100;
                                    footerTbl1.Padding = 0;
                                    footerTbl1.Cellpadding = 2;
                                    footerTbl1.Cellspacing = 1;
                                    footerTbl1.DefaultCell.Border = Rectangle.NO_BORDER;
                                    footerTbl1.CellsFitPage = true;
                                    footerTbl1.TableFitsPage = true;
                                    footerTbl1.BorderColor = Color.WHITE;
                                    footerTbl1.Border = 0;

                                    Cell ocell3 = new Cell();
                                    Phrase op3 = new Phrase(Leading, "\n\n\n\n\n\n", FontInfoNormal);
                                    ocell3.HorizontalAlignment = Element.ALIGN_CENTER;
                                    ocell3.BackgroundColor = new Color(255, 255, 255);
                                    ocell3.BorderColor = Color.WHITE;
                                    ocell3.Border = 0;
                                    ocell3.Add(op3);
                                    footerTbl1.AddCell(ocell3);

                                    footer = "1                                                                                                                              " + DateToBePrint.ToString("MMMM dd, yyyy hh:mm tt");
                                    ocell3 = new Cell();
                                    op3 = new Phrase(Leading, footer, FontInfoNormal);
                                    ocell3.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell3.BackgroundColor = new Color(255, 255, 255);
                                    ocell3.BorderColor = Color.BLACK;
                                    ocell3.BorderWidthBottom = 1;
                                    ocell3.BorderWidthLeft = 0;
                                    ocell3.BorderWidthRight = 0;
                                    ocell3.BorderWidthTop = 1;
                                    ocell3.Add(op3);
                                    footerTbl1.AddCell(ocell3);
                                    #endregion

                                    #region page 3 Table Data
                                    iTextSharp.text.Font FontInfoItalic = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.ITALIC, iTextSharp.text.Color.BLACK);
                                    iTextSharp.text.Font FontInfoBoldUnderLine = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.UNDERLINE, iTextSharp.text.Color.BLACK);
                                    iTextSharp.text.Font FontInfoBold = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.BOLD, iTextSharp.text.Color.BLACK);

                                    string paragraph2 = string.Empty;

                                    paragraph2 = "";
                                    paragraph2 = "Para information en espanol, visite www.consumerfinance.gov/learnmore  o escribe a la Consumer "
                                        + " Financial Protection Bureau , 1700 G Street N.W., Washington, DC 2006.";



                                    ocell = new Cell();
                                    op = new Phrase(Leading, paragraph2, FontInfoNormal);
                                    ocell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    ocell.BackgroundColor = new Color(255, 255, 255);
                                    ocell.Add(op);
                                    TblMain1.AddCell(ocell);


                                    ocell = new Cell();
                                    op = new Phrase(Leading, "A Summary of Your Rights Under the Fair Credit Reporting Act", FontInfoBoldUnderLine);
                                    ocell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    ocell.BackgroundColor = new Color(255, 255, 255);
                                    ocell.Add(op);
                                    TblMain1.AddCell(ocell);


                                    //ocell = new Cell();
                                    //op = new Phrase(Leading, str21(), FontInfoNormal);
                                    //op.Leading = 13;
                                    //ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ocell.BackgroundColor = new Color(255, 255, 255);
                                    //ocell.Add(op);
                                    //TblMain1.AddCell(ocell);

                                    ocell = new Cell();
                                    op = new Phrase(Leading, str22(), FontInfoNormal);
                                    op.Leading = 13;
                                    ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell.BackgroundColor = new Color(255, 255, 255);
                                    ocell.Add(op);
                                    TblMain1.AddCell(ocell);

                                    ocell = new Cell();
                                    op = new Phrase(Leading, str23(), FontInfoNormal);
                                    op.Leading = 13;
                                    ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell.BackgroundColor = new Color(255, 255, 255);
                                    ocell.Add(op);
                                    TblMain1.AddCell(ocell);

                                    ocell = new Cell();
                                    op = new Phrase(Leading, str3(), FontInfoNormal);
                                    op.Leading = 15;
                                    ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell.BackgroundColor = new Color(255, 255, 255);
                                    ocell.Add(op);
                                    TblMain1.AddCell(ocell);

                                    //ocell = new Cell();
                                    //op = new Phrase(Leading, "\n\n", FontInfoNormal);
                                    //ocell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    //ocell.BackgroundColor = new Color(255, 255, 255);
                                    //ocell.Add(op);
                                    //TblMain1.AddCell(ocell);
                                    #endregion

                                    #region Page3 Footer
                                    iTextSharp.text.Table footerTbl3 = new iTextSharp.text.Table(1);
                                    footerTbl3.Width = 100;
                                    footerTbl3.Padding = 0;
                                    footerTbl3.Cellpadding = 2;
                                    footerTbl3.Cellspacing = 1;
                                    footerTbl3.DefaultCell.Border = Rectangle.NO_BORDER;
                                    footerTbl3.CellsFitPage = true;
                                    footerTbl3.TableFitsPage = true;
                                    footerTbl3.BorderColor = Color.WHITE;
                                    footerTbl3.Border = 0;

                                    Cell ocell2 = new Cell();
                                    Phrase op2 = new Phrase(Leading, "\n\n", FontInfoNormal);
                                    ocell2.HorizontalAlignment = Element.ALIGN_CENTER;
                                    ocell2.BackgroundColor = new Color(255, 255, 255);
                                    ocell2.BorderColor = Color.WHITE;
                                    ocell2.Border = 0;
                                    ocell2.BorderWidthTop = 1;
                                    ocell2.BorderColorTop = Color.BLACK;
                                    ocell2.Add(op2);
                                    footerTbl3.AddCell(ocell2);

                                    footer = "2                                                                                                                              " + DateToBePrint.ToString("MMMM dd, yyyy hh:mm tt");
                                    ocell2 = new Cell();
                                    op2 = new Phrase(Leading, footer, FontInfoNormal);
                                    ocell2.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell2.BackgroundColor = new Color(255, 255, 255);
                                    ocell2.BorderColor = Color.BLACK;
                                    ocell2.BorderWidthBottom = 1;
                                    ocell2.BorderWidthLeft = 0;
                                    ocell2.BorderWidthRight = 0;
                                    ocell2.BorderWidthTop = 1;
                                    ocell2.Add(op2);
                                    footerTbl3.AddCell(ocell2);
                                    #endregion

                                    #region Page 4
                                    iTextSharp.text.Table ObjPdfTblMainBottom = new iTextSharp.text.Table(2);
                                    ObjPdfTblMainBottom.Width = 100;
                                    ObjPdfTblMainBottom.Padding = 0;
                                    ObjPdfTblMainBottom.Cellpadding = 2;
                                    ObjPdfTblMainBottom.Cellspacing = 1;
                                    //ObjPdfTblMainBottom.DefaultCell.Border = Rectangle.NO_BORDER;
                                    ObjPdfTblMainBottom.CellsFitPage = true;
                                    ObjPdfTblMainBottom.TableFitsPage = true;
                                    ObjPdfTblMainBottom.SetWidths(new int[2] { 60, 40 });
                                    ObjPdfTblMainBottom.BorderColor = Color.BLACK;
                                    ObjPdfTblMainBottom.Border = 1;

                                    Phrase TPhrase = new Phrase();
                                    Cell TCell = new Cell(TPhrase);

                                    TCell = new Cell();
                                    TPhrase = new Phrase(Leading, "TYPE OF BUSINESS:", FontInfoBold);
                                    TPhrase.Leading = 15;
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    TCell = new Cell();
                                    TPhrase = new Phrase(Leading, "CONTACT:", FontInfoBold);
                                    TPhrase.Leading = 15;
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    string strTL1 = string.Empty;
                                    strTL1 = "1.a. Banks, saving associations, and credit unions with total assets of over $10 billion and their"
                                    + " affiliates."
                                    + "\nb. Such affiliates that are not banks,saving associations, or credit unions also should list, in"
                                    + " addition to the Bureau:";
                                    TCell = new Cell();
                                    TPhrase = new Phrase(13, strTL1, FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    string strTR1 = string.Empty;
                                    strTR1 = "a. Bureau of Consumer Financial Protection"
                                    + "\n1700 G Street NW"
                                    + "\nWashington, DC 20006"
                                    + "\nb. Federal Trade Commission: ConsumerResponse Center- FCRA"
                                    + "\nWashington, DC 20580"
                                    + "\n(877)382-4357";

                                    TCell = new Cell();
                                    TPhrase = new Phrase();
                                    TPhrase = new Phrase(13, strTR1, FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    string strTL2 = string.Empty;
                                    strTL2 = "2. To the extent not included in item 1 above:"
                                    + "\na. National Banks, federal saving associations, and federal branches ad federal agencies of"
                                    + "foreign banks"
                                    + "\nb. State member banks, branches and agencies of foreign banks( other than federal branches,"
                                    + " federal agencies and insured state branches of foreign banks), commercial leading companies"
                                    + " owned or controlled by foreign banks, and organizations operating under section 25 or 25A of"
                                    + " the Federal Reserve Act"
                                    + "\nc. Nonmember Insured Banks, Insured State Branches of Foreign banks and insured state"
                                    + " saving organizations"
                                    + "\nd. Federal Credit Unions";
                                    TCell = new Cell();
                                    TPhrase = new Phrase(13, strTL2, FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    string strTR2 = string.Empty;
                                    strTR2 = "a. Office of Comptroller of the Currency"
                                    + "\nCustomer Assistance Group"
                                    + "\n1301 McKinnney Street, Suite 3450"
                                    + "\nHouston, TX 77010-9050"
                                    + "\nb. Federal Reserve Consumer Help Center"
                                    + "\nPO Box 1200"
                                    + "\nMinneapolis, MN55480"
                                    + "\nc. FDIC Consumer Response Center"
                                    + "\n1100 Walnut Street, Box #11"
                                    + "\nKansas City, MO 64106"
                                    + "\nd. National Credit Union Administrator"
                                    + "\nOffice of Consumer Protection(OCP)"
                                    + "\nDivision of Consumer Compliance and Outreach(DCCO)"
                                    + "\n1775 Duke Street, Alexandria, VA 22314";

                                    TCell = new Cell();
                                    TPhrase = new Phrase();
                                    TPhrase = new Phrase(13, strTR2, FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    TCell = new Cell();
                                    TPhrase = new Phrase(13, "3. Air carriers", FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    string strTR3 = string.Empty;
                                    strTR3 = "Asst.General Counsel for Aviation Enforcement & Proceeding"
                                    + "\n Department ofTransportation"
                                    + "\n400 Seventh Street SW"
                                    + "\nWashington, DC 20590";

                                    TCell = new Cell();
                                    TPhrase = new Phrase();
                                    TPhrase = new Phrase(13, strTR3, FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);


                                    TCell = new Cell();
                                    TPhrase = new Phrase(13, "4. Creditors Subject to Surface Transportation Board", FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    string strTR4 = string.Empty;
                                    strTR4 = "Office of Proceedings, Surface Transportation Board"
                                    + "\nDepartment of Transportation"
                                    + "\n1925 K Street NW"
                                    + "\nWashington, DC 20423";

                                    TCell = new Cell();
                                    TPhrase = new Phrase();
                                    TPhrase = new Phrase(13, strTR4, FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);


                                    TCell = new Cell();
                                    TPhrase = new Phrase(13, "5. Creditors Subject to Packers and Stockyards Act", FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    string strTR5 = string.Empty;
                                    strTR5 = "Nearest packers and Stockyards Administration area supervisor";

                                    TCell = new Cell();
                                    TPhrase = new Phrase();
                                    TPhrase = new Phrase(13, strTR5, FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    TCell = new Cell();
                                    TPhrase = new Phrase(13, "6. Small Business Investment Companies", FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    string strTR6 = string.Empty;
                                    strTR6 = "Associate Deputy Administrator for CapitalAccess"
                                    + " United States Small Business Administration"
                                    + " 406 Third Street, SW, 8TH Floor"
                                    + " Washington, DC 20416";

                                    TCell = new Cell();
                                    TPhrase = new Phrase();
                                    TPhrase = new Phrase(13, strTR6, FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    TCell = new Cell();
                                    TPhrase = new Phrase(13, "7. Brokers and Dealers", FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    string strTR7 = string.Empty;
                                    strTR7 = "Securities and Exchange Commission"
                                    + "\n100 F St NE"
                                    + "\nWashington, DC 20549";

                                    TCell = new Cell();
                                    TPhrase = new Phrase();
                                    TPhrase = new Phrase(13, strTR7, FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    string strTL8 = string.Empty;
                                    strTL8 = "8. Federal Land Banks, Federal Land Bank Associations, Federal Intermediate Credit Banks,"
                                    + " and production Credit Associations";
                                    TCell = new Cell();
                                    TPhrase = new Phrase(13, strTL8, FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    string strTR8 = string.Empty;
                                    strTR8 = "Farm Credit Administration"
                                    + "\n1501 Farm Credit Drive"
                                    + "\nMcLean, VA 22102-5090";

                                    TCell = new Cell();
                                    TPhrase = new Phrase(13, strTR8, FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    TCell = new Cell();
                                    TPhrase = new Phrase();
                                    TPhrase = new Phrase(13, "9. Retailers, Finance Companies, and All Other Creditors Not Listed Above", FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);


                                    string strTR9 = string.Empty;
                                    strTR9 = "FTC Regional Office for region in which the creditor operates or"
                                    + "\nFederal Trade Commission: Consumer Response Center- FCRA"
                                    + "\nWashington, DC 20580"
                                    + "\n(877)382-4357";

                                    TCell = new Cell();
                                    TPhrase = new Phrase();
                                    TPhrase = new Phrase(13, strTR9, FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    #endregion

                                    #region Page Num 4 Footer
                                    iTextSharp.text.Table footerTbl4 = new iTextSharp.text.Table(1);
                                    footerTbl4.Width = 100;
                                    footerTbl4.Padding = 0;
                                    footerTbl4.Cellpadding = 2;
                                    footerTbl4.Cellspacing = 1;
                                    footerTbl4.DefaultCell.Border = Rectangle.NO_BORDER;
                                    footerTbl4.CellsFitPage = true;
                                    footerTbl4.TableFitsPage = true;
                                    //footerTbl.BorderColor = Color.WHITE;
                                    footerTbl4.Border = 0;

                                    Cell ocell4 = new Cell();
                                    Phrase op4 = new Phrase(Leading, "\n", FontInfoNormal);
                                    ocell4.HorizontalAlignment = Element.ALIGN_CENTER;
                                    ocell4.BackgroundColor = new Color(255, 255, 255);
                                    ocell4.BorderColor = Color.WHITE;
                                    ocell4.Border = 0;
                                    ocell4.BorderWidthTop = 1;
                                    ocell4.BorderColorTop = Color.BLACK;
                                    ocell4.Add(op4);
                                    footerTbl4.AddCell(ocell4);

                                    footer = "3                                                                                                                              " + DateToBePrint.ToString("MMMM dd, yyyy hh:mm tt");
                                    ocell4 = new Cell();
                                    op4 = new Phrase(Leading, footer, FontInfoNormal);
                                    ocell4.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell4.BackgroundColor = new Color(255, 255, 255);
                                    ocell4.BorderColor = Color.BLACK;
                                    ocell4.BorderWidthBottom = 1;
                                    ocell4.BorderWidthLeft = 0;
                                    ocell4.BorderWidthRight = 0;
                                    ocell4.BorderWidthTop = 1;
                                    ocell4.Add(op4);
                                    footerTbl4.AddCell(ocell4);
                                    #endregion

                                    // New  Documents 

                                    #region Page 5 in Pdf

                                    iTextSharp.text.Table ObjPdfTblNewAddDoc = new iTextSharp.text.Table(1);

                                    ObjPdfTblNewAddDoc.Width = 100;
                                    ObjPdfTblNewAddDoc.Padding = 0;
                                    ObjPdfTblNewAddDoc.Cellpadding = 2;
                                    ObjPdfTblNewAddDoc.Cellspacing = 1;
                                    ObjPdfTblNewAddDoc.DefaultCell.Border = Rectangle.NO_BORDER;
                                    ObjPdfTblNewAddDoc.CellsFitPage = true;
                                    ObjPdfTblNewAddDoc.TableFitsPage = true;


                                    Phrase nPhrase = new Phrase();
                                    string paragraph3 = string.Empty;
                                    Cell ncell = new Cell(nPhrase);

                                    iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(new Uri(ApplicationPath.GetApplicationPath() + @"Content\themes\base\images\usaintelPowerToKnow.png"));
                                    nPhrase = new Phrase();
                                    ImgLogo.ScaleAbsoluteHeight(40);
                                    ImgLogo.ScaleAbsoluteWidth(100);
                                    Chunk chk = new Chunk(ImgLogo, 0, -2);
                                    nPhrase.Add(chk);
                                    ObjPdfTblNewAddDoc.AddCell(nPhrase);


                                    ncell = new Cell();
                                    nPhrase = new Phrase(Leading, "Document Request Form", FontInfoBold);
                                    ncell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);

                                    paragraph3 = "";
                                    paragraph3 = "This form is to be used to request a copy of your Consumer Background Investigative Report. Pursuant"
                                    + " to FCRA§ 609 and the abridged notification in theincluded Summary of Your Rights Under the Fair Credit Reporting"
                                    + " Act, “Every consumer reporting agency shall,upon request, and subject to 610(a)(1) [§ 1681h], clearly and "
                                    + " accurately disclose to theconsumer all information in the consumer's file at the time of the request.” \n \n";



                                    ncell = new Cell();
                                    nPhrase = new Phrase(Leading, paragraph3, FontInfoNormal);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);


                                    string paragraph4 = string.Empty;

                                    paragraph4 = "";
                                    paragraph4 = "To receive a copy of your Consumer Background Investigative Report by mail, simply send the below"
                                    + " identifying information along with proof of your identity.  Proof of your identity can include a photocopy of "
                                    + " one of the following documents: a State Driver License, a State Issued ID Card, a Military ID Card or a W2 form. \n";




                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, paragraph4, FontInfoNormal);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(op);
                                    ObjPdfTblNewAddDoc.AddCell(nPhrase);

                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, "\nRequired Information: \n", FontInfoBold);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);

                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, "Your Social Security Number: ____ ____ ____ - ____ ____ - ____ ____ ____ ____ \n", FontInfoNormal);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);

                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, "Full Legal Name:______________________________________________________________________________ ", FontInfoNormal);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);

                                    ocell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, "DOB:________________________________________________________________________________________ ", FontInfoNormal);
                                    ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell.BackgroundColor = new Color(255, 255, 255);
                                    ocell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ocell);

                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, "Street Address (residence):____________________________________________________________________ ", FontInfoNormal);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);

                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, "City, State, Zip: _____________________________________________________________________________ ", FontInfoNormal);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);

                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, "Phone Number: (_______)____________________________________________________________________ ", FontInfoNormal);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);

                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, "E-mail Address: _____________________________________________________________________________ ", FontInfoNormal);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);


                                    string PleaseLeadPhrase = "\n Please read and sign the following statement. Your signature acknowledges your agreement. "
                                                             + "By submitting this form, I state that all the information contained is true to the best of my knowledge.";
                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, PleaseLeadPhrase, FontInfoBold);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);



                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, "\nSignature: _________________________________________________________Date: __________________ \n", FontInfoNormal);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);



                                    string AfterCompletePhrase = "After completing this form, please return it by mail or fax. The mailing address and fax number are provided "
                                                                + "below. Remember to include your proof of identity.";


                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, AfterCompletePhrase, FontInfoBold);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);


                                    string AddressInfo = "Intelifi Inc                                                                     \n"
                                                        + "Attn: Compliance Department                                        \n "
                                                        + "8730 Wilshire Blvd, Suite 412                                          \n"
                                                        + "Beverly Hills, CA  90211                                                  \n";



                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, AddressInfo, FontInfoNormal);
                                    ncell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);


                                    string AddressInfo2 = "Fax No. (310) 623-1820                                                  \n"
                                                         + "Customer Service No (888) 409-1819                             \n";
                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, AddressInfo2, FontInfoNormal);
                                    ncell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);



                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, "Thank you,", FontInfoNormal);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);

                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, "Intelifi Compliance Department", FontInfoNormal);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);


                                    #endregion

                                    #region Page Num 5 Footer
                                    iTextSharp.text.Table footerTbl5 = new iTextSharp.text.Table(1);
                                    footerTbl5.Width = 100;
                                    footerTbl5.Padding = 0;
                                    footerTbl5.Cellpadding = 2;
                                    footerTbl5.Cellspacing = 1;
                                    footerTbl5.DefaultCell.Border = Rectangle.NO_BORDER;
                                    footerTbl5.CellsFitPage = true;
                                    footerTbl5.TableFitsPage = true;
                                    //footerTbl.BorderColor = Color.WHITE;
                                    footerTbl5.Border = 0;

                                    Cell ocell1 = new Cell();
                                    Phrase op1 = new Phrase(Leading, "\n\n\n\n\n\n\n\n", FontInfoNormal);
                                    ocell1.HorizontalAlignment = Element.ALIGN_CENTER;
                                    ocell1.BackgroundColor = new Color(255, 255, 255);
                                    ocell1.BorderColor = Color.WHITE;
                                    ocell1.Border = 0;
                                    ocell1.BorderWidthTop = 1;
                                    ocell1.BorderColorTop = Color.BLACK;
                                    ocell1.Add(op1);
                                    footerTbl5.AddCell(ocell1);

                                    ocell1 = new Cell();
                                    footer = "4                                                                                                                              " + DateToBePrint.ToString("MMMM dd, yyyy hh:mm tt");
                                    op1 = new Phrase(Leading, footer, FontInfoNormal);
                                    ocell1.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell1.BackgroundColor = new Color(255, 255, 255);
                                    ocell1.BorderColor = Color.BLACK;
                                    ocell1.BorderWidthBottom = 1;
                                    ocell1.BorderWidthLeft = 0;
                                    ocell1.BorderWidthRight = 0;
                                    ocell1.BorderWidthTop = 1;
                                    ocell1.Add(op1);
                                    footerTbl5.AddCell(ocell1);
                                    #endregion

                                    #region Blank Page Insert In PDF
                                    iTextSharp.text.Table blankTbl = new iTextSharp.text.Table(1);
                                    blankTbl.Width = 100;
                                    blankTbl.Padding = 0;
                                    blankTbl.Cellpadding = 2;
                                    blankTbl.Cellspacing = 1;
                                    blankTbl.DefaultCell.Border = Rectangle.NO_BORDER;
                                    blankTbl.CellsFitPage = true;
                                    blankTbl.TableFitsPage = true;
                                    #endregion

                                    #region Add Table in Document

                                    ObjDocument.Add(MainPageTbl);
                                    ObjDocument.Add(TblMain);
                                    ObjDocument.Add(footerTbl1);
                                    ObjDocument.NewPage();
                                    ObjDocument.Add(blankTbl);
                                    ObjDocument.NewPage();
                                    ObjDocument.Add(TblMain1);
                                    ObjDocument.Add(blankTbl);
                                    ObjDocument.Add(footerTbl3);
                                    ObjDocument.NewPage();
                                    ObjDocument.Add(ObjPdfTblMainBottom);
                                    ObjDocument.Add(blankTbl);
                                    ObjDocument.Add(footerTbl4);
                                    ObjDocument.NewPage();
                                    ObjDocument.Add(ObjPdfTblNewAddDoc);
                                    ObjDocument.Add(footerTbl5);
                                    ObjDocument.NewPage();
                                    ObjDocument.Add(blankTbl);
                                    ObjDocument.NewPage();
                                    #endregion

                                    #endregion
                                }
                            }

                        }
                    }
                }


            }
            catch //(Exception ex)
            {
                ObjDocument.NewPage();
            }
            finally
            {
                ObjDocument.Close();
            }
            return pdfFileName;
        }

        public string GeneratePdfOf613NoticeSent(string OrderIds, List<Proc_GetNoticeForAdminDashboardResult> RecordList)
        {

            string pdfFileName = string.Empty;

            iTextSharp.text.Rectangle ObjPageSize = new iTextSharp.text.Rectangle(PageSize.LETTER);
            Document ObjDocument = new Document(ObjPageSize, 55, 55, 15, 12);
            float Leading = 13;
            DateTime DateToBePrint = DateTime.Now;

            string path = Request.PhysicalApplicationPath + "Resources\\Upload\\613NoticePdf\\613NoticePdfFileSent.pdf";

            if (System.IO.File.Exists(path))
            {
                try
                {
                    VendorServices.GeneralService.WriteLog("Start file Reading", "613FileIssue");
                    FileInfo finfo = new FileInfo(path);
                    FileStream stream = finfo.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                    stream.Flush();
                    stream.Close();
                    stream.Dispose();
                    finfo.Delete();
                    VendorServices.GeneralService.WriteLog(finfo.FullName, "613FileIssue");
                }
                catch (Exception ex)
                {
                    VendorServices.GeneralService.WriteLog(ex.InnerException.ToString(), "613FileIssue");
                }
            }


            //var writer = PdfWriter.GetInstance(ObjDocument, new FileStream(path, FileMode.Create));
            VendorServices.GeneralService.WriteLog("New File Creation", "613FileIssue");
            pdfFileName = "Intelifi_Inc_613Notice_" + DateTime.Now.ToFileTime().ToString() + ".pdf";
            try
            {
                BALCompany ObjBALCompany = new BALCompany();
                BALOrders ObjBALOrders = new BALOrders();
                string UserName = string.Empty;
                string CompanyName = string.Empty;
                string Address = string.Empty;
                //string Address2 = string.Empty;
                string ApplicantName = string.Empty;
                //string ApplicantNamepdf = string.Empty;
                List<Proc_Get_RequestAndResponseResult> ObjProc_Get_RequestAndResponseResult = new List<Proc_Get_RequestAndResponseResult>();
                Proc_Get_RequestAndResponseResult ObjRequestAndResponseResult = new Proc_Get_RequestAndResponseResult();
                List<Proc_Get_CompanyDetailByMemberShipIdResult> ObjCompanyDetails = new List<Proc_Get_CompanyDetailByMemberShipIdResult>();
                Proc_Get_CompanyDetailByMemberShipIdResult ObjCompanyDetailByMemberShipIdResult = new Proc_Get_CompanyDetailByMemberShipIdResult();


                iTextSharp.text.Font FontInfoHeader = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.BOLD, iTextSharp.text.Color.BLACK);
                iTextSharp.text.Font FontInfoNormal = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 8, iTextSharp.text.Font.NORMAL, iTextSharp.text.Color.BLACK);
                iTextSharp.text.Font FontInfoHeadings = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 11, iTextSharp.text.Font.BOLD, iTextSharp.text.Color.BLACK);
                iTextSharp.text.Font FontInfoTotal = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.BOLD, iTextSharp.text.Color.BLACK);
                if (!string.IsNullOrEmpty(OrderIds))
                {
                    //string[] stringSeparators = new string[] { "," };
                    //string[] OrderIdArr = OrderIds.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                    #region for blankspace
                    Chunk spaceChunk = new Chunk("                                                                                                                     ");
                    #endregion
                    HeaderFooter ObjPdfFooter = new HeaderFooter(new Phrase(" ", FontInfoNormal), new Phrase(spaceChunk + DateToBePrint.ToString("MMMM dd, yyyy hh:mm tt"), FontInfoNormal));
                    // ObjDocument.Footer = ObjPdfFooter;
                    ObjPdfFooter.Alignment = Element.ALIGN_LEFT;
                    VendorServices.GeneralService.WriteLog("Openning", "613FileIssue");
                    ObjDocument.Open();
                    VendorServices.GeneralService.WriteLog("Opened", "613FileIssue");
                    if (RecordList.Count > 0)
                    {


                        for (int i = 0; i < RecordList.Count; i++)
                        {
                            //int IsSendNotice = ObjBALOrders.IsSendNoticeEmail(Guid.Parse(RecordList.ElementAt(i).pkOrderDetailId.ToString()));
                            //if (IsSendNotice != 1)
                            //{



                            ObjProc_Get_RequestAndResponseResult = ObjBALOrders.GetRequestAndResponseByOrderId(Int32.Parse(RecordList.ElementAt(i).fkOrderId.ToString()));
                            if (ObjProc_Get_RequestAndResponseResult.Count > 0)
                            {
                                ObjRequestAndResponseResult = ObjProc_Get_RequestAndResponseResult.First();
                                ObjCompanyDetails = ObjBALCompany.GetCompanyDetailByMemberShipIdResult(ObjRequestAndResponseResult.fkUserId);
                                if (ObjCompanyDetails.Count > 0)
                                {
                                    ObjCompanyDetailByMemberShipIdResult = ObjCompanyDetails.First();

                                    UserName = ObjCompanyDetailByMemberShipIdResult.lastname + ", " + ObjCompanyDetailByMemberShipIdResult.firstname;
                                    CompanyName = ObjCompanyDetailByMemberShipIdResult.CompanyName;
                                    Address = ObjCompanyDetailByMemberShipIdResult.city + ", " + ObjCompanyDetailByMemberShipIdResult.statecode;

                                    ApplicantName = GenerateApplicantName(ObjRequestAndResponseResult.SearchedLastName, ObjRequestAndResponseResult.SearchedSuffix, ObjRequestAndResponseResult.SearchedMiddleInitial, ObjRequestAndResponseResult.SearchedFirstName); ;
                                    //   ApplicantName = ObjRequestAndResponseResult.SearchedLastName + "_" + ObjRequestAndResponseResult.SearchedFirstName;
                                    pdfFileName = "Intelifi_Inc_613Notice_" + ObjRequestAndResponseResult.SearchedLastName + "_" + ObjRequestAndResponseResult.SearchedFirstName + ".pdf";
                                    VendorServices.GeneralService.WriteLog(pdfFileName, "613FileIssue");
                                    #region Pdf Generation Old Code
                                    //iTextSharp.text.Table TblMain = new iTextSharp.text.Table(1);
                                    //TblMain.Width = 100;
                                    //TblMain.Padding = 0;
                                    //TblMain.Cellpadding = 2;
                                    //TblMain.Cellspacing = 1;
                                    //TblMain.DefaultCell.Border = Rectangle.NO_BORDER;
                                    //TblMain.CellsFitPage = true;
                                    //TblMain.TableFitsPage = true;


                                    //iTextSharp.text.Table TblMain1 = new iTextSharp.text.Table(1);
                                    //TblMain1.Width = 100;
                                    //TblMain1.Padding = 0;
                                    //TblMain1.Cellpadding = 2;
                                    //TblMain1.Cellspacing = 1;
                                    //TblMain1.DefaultCell.Border = Rectangle.NO_BORDER;
                                    //TblMain1.CellsFitPage = true;
                                    //TblMain1.TableFitsPage = true;
                                    //Phrase op = new Phrase();
                                    //Cell ocell = new Cell(op);
                                    //Chunk ock = new Chunk();

                                    //op = new Phrase(Leading, DateTime.Now.ToString("dd-MMM-yyyy"), FontInfoNormal);
                                    //ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ocell.BackgroundColor = new Color(255, 255, 255);
                                    //ocell.Add(op);
                                    //TblMain.AddCell(ocell);

                                    //ocell = new Cell();
                                    //op = new Phrase(Leading, ApplicantName, FontInfoNormal);
                                    //ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ocell.BackgroundColor = new Color(255, 255, 255);
                                    //ocell.Add(op); TblMain.AddCell(ocell);

                                    ////ocell = new Cell();
                                    ////op = new Phrase(Leading, Address2, FontInfoNormal);
                                    ////ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ////ocell.BackgroundColor = new Color(255, 255, 255);
                                    ////ocell.Add(op);
                                    ////TblMain.AddCell(ocell);

                                    //ocell = new Cell();
                                    //op = new Phrase(Leading, Address, FontInfoNormal);
                                    //ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ocell.BackgroundColor = new Color(255, 255, 255);
                                    //ocell.Add(op);
                                    //TblMain.AddCell(ocell);



                                    //ocell = new Cell();
                                    //op = new Phrase(Leading, "Dear " + ApplicantName, FontInfoNormal);
                                    //ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ocell.BackgroundColor = new Color(255, 255, 255);
                                    //ocell.Add(op);
                                    //TblMain.AddCell(ocell);

                                    //string paragraph = string.Empty;
                                    //#region Main Paragraph
                                    //paragraph = "";
                                    //paragraph = "Pursuant to FCRA 613 this is to advise you that " + CompanyName + " at " + Address
                                    //   + " has requested that we furnish information from public records regarding you. Enclosed is "
                                    //   + " a copy of the Summary of Rights prepared by the Consumer Financial Protection Bureau , and  "
                                    //   + " a  document to request a copy of your report. If you have any questions or concerns  regarding "
                                    //   + "the background report please contact USA Intel Inc  8730 Wilshire Blvd. Suite 412 Beverly Hills, "
                                    //   + " CA 90211 (800) 409-1819.\n\n";

                                    //#endregion


                                    //iTextSharp.text.Font FontInfoNormal1 = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.NORMAL, iTextSharp.text.Color.BLACK);


                                    //ocell = new Cell();
                                    //op = new Phrase(Leading, paragraph, FontInfoNormal1);
                                    //ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ocell.BackgroundColor = new Color(255, 255, 255);
                                    //ocell.Add(op);
                                    //TblMain.AddCell(ocell);

                                    //ocell = new Cell();
                                    //op = new Phrase(Leading, "Sincerely,", FontInfoNormal);
                                    //ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ocell.BackgroundColor = new Color(255, 255, 255);
                                    //ocell.Add(op);
                                    //TblMain.AddCell(ocell);

                                    //ocell = new Cell();
                                    //op = new Phrase(Leading, "Intelifi", FontInfoNormal);
                                    //ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ocell.BackgroundColor = new Color(255, 255, 255);
                                    //ocell.Add(op);
                                    //TblMain.AddCell(ocell);

                                    //ocell = new Cell();
                                    //op = new Phrase(Leading, "Enclosures: Consumer Summary of Rights\nDocuments Request Form", FontInfoNormal);
                                    //ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ocell.BackgroundColor = new Color(255, 255, 255);
                                    //ocell.Add(op);
                                    //TblMain.AddCell(ocell);

                                    //iTextSharp.text.Font FontInfoItalic = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.ITALIC, iTextSharp.text.Color.BLACK);
                                    //iTextSharp.text.Font FontInfoBoldUnderLine = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.UNDERLINE, iTextSharp.text.Color.BLACK);
                                    //iTextSharp.text.Font FontInfoBold = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.BOLD, iTextSharp.text.Color.BLACK);

                                    //string paragraph2 = string.Empty;

                                    //paragraph2 = "";
                                    //paragraph2 = "Para information en espanol, visite www.consumerfinance.gov/learnmore  o escribe a la Consumer "
                                    //    + " Financial Protection Bureau , 1700 G Street N.W., Washington, DC 2006.";



                                    //ocell = new Cell();
                                    //op = new Phrase(Leading, paragraph2, FontInfoNormal);
                                    //ocell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    //ocell.BackgroundColor = new Color(255, 255, 255);
                                    //ocell.Add(op);
                                    //TblMain1.AddCell(ocell);


                                    //ocell = new Cell();
                                    //op = new Phrase(Leading, "A Summary of Your Rights Under the Fair Credit Reporting Act", FontInfoBoldUnderLine);
                                    //ocell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    //ocell.BackgroundColor = new Color(255, 255, 255);
                                    //ocell.Add(op);
                                    //TblMain1.AddCell(ocell);


                                    ////ocell = new Cell();
                                    ////op = new Phrase(Leading, str21(), FontInfoNormal);
                                    ////op.Leading = 13;
                                    ////ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ////ocell.BackgroundColor = new Color(255, 255, 255);
                                    ////ocell.Add(op);
                                    ////TblMain1.AddCell(ocell);

                                    //ocell = new Cell();
                                    //op = new Phrase(Leading, str22(), FontInfoNormal);
                                    //op.Leading = 13;
                                    //ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ocell.BackgroundColor = new Color(255, 255, 255);
                                    //ocell.Add(op);
                                    //TblMain1.AddCell(ocell);

                                    //ocell = new Cell();
                                    //op = new Phrase(Leading, str23(), FontInfoNormal);
                                    //op.Leading = 13;
                                    //ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ocell.BackgroundColor = new Color(255, 255, 255);
                                    //ocell.Add(op);
                                    //TblMain1.AddCell(ocell);

                                    //ocell = new Cell();
                                    //op = new Phrase(Leading, str3(), FontInfoNormal);
                                    //op.Leading = 15;
                                    //ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ocell.BackgroundColor = new Color(255, 255, 255);
                                    //ocell.Add(op);
                                    //TblMain1.AddCell(ocell);

                                    //iTextSharp.text.Table ObjPdfTblMainBottom = new iTextSharp.text.Table(2);
                                    //ObjPdfTblMainBottom.SetWidths(new int[2] { 60, 40 });
                                    //ObjPdfTblMainBottom.BorderColor = new Color(191, 217, 235);
                                    //ObjPdfTblMainBottom.Border = 2;

                                    //Phrase TPhrase = new Phrase();
                                    //Cell TCell = new Cell(TPhrase);

                                    //TCell = new Cell();
                                    //TPhrase = new Phrase(Leading, "TYPE OF BUSINESS:", FontInfoBold);
                                    //TPhrase.Leading = 15;
                                    //TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //TCell.BackgroundColor = new Color(255, 255, 255);
                                    //TCell.Add(TPhrase);
                                    //ObjPdfTblMainBottom.AddCell(TCell);

                                    //TCell = new Cell();
                                    //TPhrase = new Phrase(Leading, "CONTACT:", FontInfoBold);
                                    //TPhrase.Leading = 15;
                                    //TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //TCell.BackgroundColor = new Color(255, 255, 255);
                                    //TCell.Add(TPhrase);
                                    //ObjPdfTblMainBottom.AddCell(TCell);

                                    //string strTL1 = string.Empty;
                                    //strTL1 = "1.a. Banks, saving associations, and credit unions with total assets of over $10 billion and their"
                                    //+ " affiliates."
                                    //+ "\nb. Such affiliates that are not banks,saving associations, or credit unions also should list, in"
                                    //+ " addition to the Bureau:";
                                    //TCell = new Cell();
                                    //TPhrase = new Phrase(13, strTL1, FontInfoNormal);
                                    //TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //TCell.BackgroundColor = new Color(255, 255, 255);
                                    //TCell.Add(TPhrase);
                                    //ObjPdfTblMainBottom.AddCell(TCell);

                                    //string strTR1 = string.Empty;
                                    //strTR1 = "a. Bureau of Consumer Financial Protection"
                                    //+ "\n1700 G Street NW"
                                    //+ "\nWashington, DC 20006"
                                    //+ "\nb. Federal Trade Commission: ConsumerResponse Center-"
                                    //+ "\nFCRA"
                                    //+ "\nWashington, DC 20580"
                                    //+ "\n(877)382-4357";

                                    //TCell = new Cell();
                                    //TPhrase = new Phrase();
                                    //TPhrase = new Phrase(13, strTR1, FontInfoNormal);
                                    //TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //TCell.BackgroundColor = new Color(255, 255, 255);
                                    //TCell.Add(TPhrase);
                                    //ObjPdfTblMainBottom.AddCell(TCell);

                                    //string strTL2 = string.Empty;
                                    //strTL2 = "2. To the extent not included in item 1 above:"
                                    //+ "\na. National Banks, federal saving associations, and federal branches ad federal agencies of"
                                    //+ "foreign banks"
                                    //+ "\nb. State member banks, branches and agencies of foreign banks( other than federal branches,"
                                    //+ " federal agencies and insured state branches of foreign banks), commercial leading companies"
                                    //+ " owned or controlled by foreign banks, and organizations operating under section 25 or 25A of"
                                    //+ " the Federal Reserve Act"
                                    //+ "\nc. Nonmember Insured Banks, Insured State Branches of Foreign banks and insured state"
                                    //+ " saving organizations"
                                    //+ "\nd. Federal Credit Unions";
                                    //TCell = new Cell();
                                    //TPhrase = new Phrase(13, strTL2, FontInfoNormal);
                                    //TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //TCell.BackgroundColor = new Color(255, 255, 255);
                                    //TCell.Add(TPhrase);
                                    //ObjPdfTblMainBottom.AddCell(TCell);

                                    //string strTR2 = string.Empty;
                                    //strTR2 = "a. Office of Comptroller of the Currency"
                                    //+ "\nCustomer Assistance Group"
                                    //+ "\n1301 McKinnney Street, Suite 3450"
                                    //+ "\nHouston, TX 77010-9050"
                                    //+ "\nb. Federal Reserve Consumer Help Center"
                                    //+ "\nPO Box 1200"
                                    //+ "\nMinneapolis, MN55480"
                                    //+ "\nc. FDIC Consumer Response Center"
                                    //+ "\n1100 Walnut Street, Box #11"
                                    //+ "\nKansas City, MO 64106"
                                    //+ "\nd. National Credit Union Administrator"
                                    //+ "\nOffice of Consumer Protection(OCP)"
                                    //+ "\nDivision of Consumer Compliance and Outreach(DCCO)"
                                    //+ "\n1775 Duke Street"
                                    //+ "\nAlexandria, VA 22314";

                                    //TCell = new Cell();
                                    //TPhrase = new Phrase();
                                    //TPhrase = new Phrase(13, strTR2, FontInfoNormal);
                                    //TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //TCell.BackgroundColor = new Color(255, 255, 255);
                                    //TCell.Add(TPhrase);
                                    //ObjPdfTblMainBottom.AddCell(TCell);

                                    //TCell = new Cell();
                                    //TPhrase = new Phrase(13, "3. Air carriers", FontInfoNormal);
                                    //TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //TCell.BackgroundColor = new Color(255, 255, 255);
                                    //TCell.Add(TPhrase);
                                    //ObjPdfTblMainBottom.AddCell(TCell);

                                    //string strTR3 = string.Empty;
                                    //strTR3 = "Asst.General Counsel for Aviation Enforcement & Proceeding"
                                    //+ "\n Department ofTransportation"
                                    //+ "\n400 Seventh Street SW"
                                    //+ "\nWashington, DC 20590";

                                    //TCell = new Cell();
                                    //TPhrase = new Phrase();
                                    //TPhrase = new Phrase(13, strTR3, FontInfoNormal);
                                    //TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //TCell.BackgroundColor = new Color(255, 255, 255);
                                    //TCell.Add(TPhrase);
                                    //ObjPdfTblMainBottom.AddCell(TCell);


                                    //TCell = new Cell();
                                    //TPhrase = new Phrase(13, "4. Creditors Subject to Surface Transportation Board", FontInfoNormal);
                                    //TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //TCell.BackgroundColor = new Color(255, 255, 255);
                                    //TCell.Add(TPhrase);
                                    //ObjPdfTblMainBottom.AddCell(TCell);

                                    //string strTR4 = string.Empty;
                                    //strTR4 = "Office of Proceedings, Surface Transportation Board"
                                    //+ "\nDepartment of Transportation"
                                    //+ "\n1925 K Street NW"
                                    //+ "\nWashington, DC 20423";

                                    //TCell = new Cell();
                                    //TPhrase = new Phrase();
                                    //TPhrase = new Phrase(13, strTR4, FontInfoNormal);
                                    //TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //TCell.BackgroundColor = new Color(255, 255, 255);
                                    //TCell.Add(TPhrase);
                                    //ObjPdfTblMainBottom.AddCell(TCell);


                                    //TCell = new Cell();
                                    //TPhrase = new Phrase(13, "5. Creditors Subject to Packers and Stockyards Act", FontInfoNormal);
                                    //TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //TCell.BackgroundColor = new Color(255, 255, 255);
                                    //TCell.Add(TPhrase);
                                    //ObjPdfTblMainBottom.AddCell(TCell);

                                    //string strTR5 = string.Empty;
                                    //strTR5 = "Nearest packers and Stockyards Administration area supervisor";

                                    //TCell = new Cell();
                                    //TPhrase = new Phrase();
                                    //TPhrase = new Phrase(13, strTR5, FontInfoNormal);
                                    //TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //TCell.BackgroundColor = new Color(255, 255, 255);
                                    //TCell.Add(TPhrase);
                                    //ObjPdfTblMainBottom.AddCell(TCell);

                                    //TCell = new Cell();
                                    //TPhrase = new Phrase(13, "6. Small Business Investment Companies", FontInfoNormal);
                                    //TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //TCell.BackgroundColor = new Color(255, 255, 255);
                                    //TCell.Add(TPhrase);
                                    //ObjPdfTblMainBottom.AddCell(TCell);

                                    //string strTR6 = string.Empty;
                                    //strTR6 = "Associate Deputy Administrator for CapitalAccess"
                                    //+ " United States Small Business Administration"
                                    //+ " 406 Third Street, SW, 8TH Floor"
                                    //+ " Washington, DC 20416";

                                    //TCell = new Cell();
                                    //TPhrase = new Phrase();
                                    //TPhrase = new Phrase(13, strTR6, FontInfoNormal);
                                    //TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //TCell.BackgroundColor = new Color(255, 255, 255);
                                    //TCell.Add(TPhrase);
                                    //ObjPdfTblMainBottom.AddCell(TCell);

                                    //TCell = new Cell();
                                    //TPhrase = new Phrase(13, "7. Brokers and Dealers", FontInfoNormal);
                                    //TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //TCell.BackgroundColor = new Color(255, 255, 255);
                                    //TCell.Add(TPhrase);
                                    //ObjPdfTblMainBottom.AddCell(TCell);

                                    //string strTR7 = string.Empty;
                                    //strTR7 = "Securities and Exchange Commission"
                                    //+ "\n100 F St NE"
                                    //+ "\nWashington, DC 20549";

                                    //TCell = new Cell();
                                    //TPhrase = new Phrase();
                                    //TPhrase = new Phrase(13, strTR7, FontInfoNormal);
                                    //TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //TCell.BackgroundColor = new Color(255, 255, 255);
                                    //TCell.Add(TPhrase);
                                    //ObjPdfTblMainBottom.AddCell(TCell);

                                    //string strTL8 = string.Empty;
                                    //strTL8 = "8. Federal Land Banks, Federal Land Bank Associations, Federal Intermediate Credit Banks,"
                                    //+ " and production Credit Associations";
                                    //TCell = new Cell();
                                    //TPhrase = new Phrase(13, strTL8, FontInfoNormal);
                                    //TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //TCell.BackgroundColor = new Color(255, 255, 255);
                                    //TCell.Add(TPhrase);
                                    //ObjPdfTblMainBottom.AddCell(TCell);

                                    //string strTR8 = string.Empty;
                                    //strTR8 = "Farm Credit Administration"
                                    //+ "\n1501 Farm Credit Drive"
                                    //+ "\nMcLean, VA 22102-5090";

                                    //TCell = new Cell();
                                    //TPhrase = new Phrase(13, strTR8, FontInfoNormal);
                                    //TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //TCell.BackgroundColor = new Color(255, 255, 255);
                                    //TCell.Add(TPhrase);
                                    //ObjPdfTblMainBottom.AddCell(TCell);

                                    //TCell = new Cell();
                                    //TPhrase = new Phrase();
                                    //TPhrase = new Phrase(13, "9. Retailers, Finance Companies, and All Other Creditors Not Listed Above", FontInfoNormal);
                                    //TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //TCell.BackgroundColor = new Color(255, 255, 255);
                                    //TCell.Add(TPhrase);
                                    //ObjPdfTblMainBottom.AddCell(TCell);


                                    //string strTR9 = string.Empty;
                                    //strTR9 = "FTC Regional Office for region in which the creditor operates or"
                                    //+ "\nFederal Trade Commission: Consumer Response Center- FCRA"
                                    //+ "\nWashington, DC 20580"
                                    //+ "\n(877)382-4357";

                                    //TCell = new Cell();
                                    //TPhrase = new Phrase();
                                    //TPhrase = new Phrase(13, strTR9, FontInfoNormal);
                                    //TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //TCell.BackgroundColor = new Color(255, 255, 255);
                                    //TCell.Add(TPhrase);
                                    //ObjPdfTblMainBottom.AddCell(TCell);

                                    //// New  Documents 

                                    //iTextSharp.text.Table ObjPdfTblNewAddDoc = new iTextSharp.text.Table(1);

                                    //ObjPdfTblNewAddDoc.Width = 100;
                                    //ObjPdfTblNewAddDoc.Padding = 0;
                                    //ObjPdfTblNewAddDoc.Cellpadding = 2;
                                    //ObjPdfTblNewAddDoc.Cellspacing = 1;
                                    //ObjPdfTblNewAddDoc.DefaultCell.Border = Rectangle.NO_BORDER;
                                    //ObjPdfTblNewAddDoc.CellsFitPage = true;
                                    //ObjPdfTblNewAddDoc.TableFitsPage = true;


                                    //Phrase nPhrase = new Phrase();
                                    //string paragraph3 = string.Empty;
                                    //Cell ncell = new Cell(nPhrase);

                                    //iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(new Uri(ApplicationPath.GetApplicationPath() + @"Content\themes\base\images\IntelifiPowerToKnow.png"));
                                    //nPhrase = new Phrase();
                                    //ImgLogo.ScaleAbsoluteHeight(40);
                                    //ImgLogo.ScaleAbsoluteWidth(100);
                                    //Chunk chk = new Chunk(ImgLogo, 0, -2);
                                    //nPhrase.Add(chk);
                                    //ObjPdfTblNewAddDoc.AddCell(nPhrase);


                                    //ncell = new Cell();
                                    //nPhrase = new Phrase(Leading, "Document Request Form", FontInfoBold);
                                    //ncell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    //ncell.BackgroundColor = new Color(255, 255, 255);
                                    //ncell.Add(nPhrase);
                                    //ObjPdfTblNewAddDoc.AddCell(ncell);

                                    //paragraph3 = "";
                                    //paragraph3 = "This form is to be used to request a copy of your Consumer Background Investigative Report. Pursuant"
                                    //+ " to FCRA§ 609 and the abridged notification in theincluded Summary of Your Rights Under the Fair Credit Reporting"
                                    //+ " Act, “Every consumer reporting agency shall,upon request, and subject to 610(a)(1) [§ 1681h], clearly and "
                                    //+ " accurately disclose to theconsumer all information in the consumer's file at the time of the request.” \n \n";



                                    //ncell = new Cell();
                                    //nPhrase = new Phrase(Leading, paragraph3, FontInfoNormal);
                                    //ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ncell.BackgroundColor = new Color(255, 255, 255);
                                    //ncell.Add(nPhrase);
                                    //ObjPdfTblNewAddDoc.AddCell(ncell);


                                    //string paragraph4 = string.Empty;

                                    //paragraph4 = "";
                                    //paragraph4 = "To receive a copy of your Consumer Background Investigative Report by mail, simply send the below"
                                    //+ " identifying information along with proof of your identity.  Proof of your identity can include a photocopy of "
                                    //+ " one of the following documents: a State Driver License, a State Issued ID Card, a Military ID Card or a W2 form. \n";




                                    //ncell = new Cell();
                                    //nPhrase = new Phrase();
                                    //nPhrase = new Phrase(Leading, paragraph4, FontInfoNormal);
                                    //ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ncell.BackgroundColor = new Color(255, 255, 255);
                                    //ncell.Add(op);
                                    //ObjPdfTblNewAddDoc.AddCell(nPhrase);

                                    //ncell = new Cell();
                                    //nPhrase = new Phrase();
                                    //nPhrase = new Phrase(Leading, "\nRequired Information: \n", FontInfoBold);
                                    //ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ncell.BackgroundColor = new Color(255, 255, 255);
                                    //ncell.Add(nPhrase);
                                    //ObjPdfTblNewAddDoc.AddCell(ncell);

                                    //ncell = new Cell();
                                    //nPhrase = new Phrase();
                                    //nPhrase = new Phrase(Leading, "Your Social Security Number: ____ ____ ____ - ____ ____ - ____ ____ ____ ____ \n", FontInfoNormal);
                                    //ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ncell.BackgroundColor = new Color(255, 255, 255);
                                    //ncell.Add(nPhrase);
                                    //ObjPdfTblNewAddDoc.AddCell(ncell);

                                    //ncell = new Cell();
                                    //nPhrase = new Phrase();
                                    //nPhrase = new Phrase(Leading, "Full Legal Name:______________________________________________________________________________ ", FontInfoNormal);
                                    //ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ncell.BackgroundColor = new Color(255, 255, 255);
                                    //ncell.Add(nPhrase);
                                    //ObjPdfTblNewAddDoc.AddCell(ncell);

                                    //ocell = new Cell();
                                    //nPhrase = new Phrase();
                                    //nPhrase = new Phrase(Leading, "DOB:________________________________________________________________________________________ ", FontInfoNormal);
                                    //ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ocell.BackgroundColor = new Color(255, 255, 255);
                                    //ocell.Add(nPhrase);
                                    //ObjPdfTblNewAddDoc.AddCell(ocell);

                                    //ncell = new Cell();
                                    //nPhrase = new Phrase();
                                    //nPhrase = new Phrase(Leading, "Street Address (residence):____________________________________________________________________ ", FontInfoNormal);
                                    //ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ncell.BackgroundColor = new Color(255, 255, 255);
                                    //ncell.Add(nPhrase);
                                    //ObjPdfTblNewAddDoc.AddCell(ncell);

                                    //ncell = new Cell();
                                    //nPhrase = new Phrase();
                                    //nPhrase = new Phrase(Leading, "City, State, Zip: _____________________________________________________________________________ ", FontInfoNormal);
                                    //ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ncell.BackgroundColor = new Color(255, 255, 255);
                                    //ncell.Add(nPhrase);
                                    //ObjPdfTblNewAddDoc.AddCell(ncell);

                                    //ncell = new Cell();
                                    //nPhrase = new Phrase();
                                    //nPhrase = new Phrase(Leading, "Phone Number: (_______)____________________________________________________________________ ", FontInfoNormal);
                                    //ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ncell.BackgroundColor = new Color(255, 255, 255);
                                    //ncell.Add(nPhrase);
                                    //ObjPdfTblNewAddDoc.AddCell(ncell);

                                    //ncell = new Cell();
                                    //nPhrase = new Phrase();
                                    //nPhrase = new Phrase(Leading, "E-mail Address: _____________________________________________________________________________ ", FontInfoNormal);
                                    //ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ncell.BackgroundColor = new Color(255, 255, 255);
                                    //ncell.Add(nPhrase);
                                    //ObjPdfTblNewAddDoc.AddCell(ncell);


                                    //string PleaseLeadPhrase = "\n Please read and sign the following statement. Your signature acknowledges your agreement. "
                                    //                         + "By submitting this form, I state that all the information contained is true to the best of my knowledge.";
                                    //ncell = new Cell();
                                    //nPhrase = new Phrase();
                                    //nPhrase = new Phrase(Leading, PleaseLeadPhrase, FontInfoBold);
                                    //ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ncell.BackgroundColor = new Color(255, 255, 255);
                                    //ncell.Add(nPhrase);
                                    //ObjPdfTblNewAddDoc.AddCell(ncell);



                                    //ncell = new Cell();
                                    //nPhrase = new Phrase();
                                    //nPhrase = new Phrase(Leading, "\nSignature: _________________________________________________________Date: __________________ \n", FontInfoNormal);
                                    //ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ncell.BackgroundColor = new Color(255, 255, 255);
                                    //ncell.Add(nPhrase);
                                    //ObjPdfTblNewAddDoc.AddCell(ncell);



                                    //string AfterCompletePhrase = "After completing this form, please return it by mail or fax. The mailing address and fax number are provided "
                                    //                            + "below. Remember to include your proof of identity.";


                                    //ncell = new Cell();
                                    //nPhrase = new Phrase();
                                    //nPhrase = new Phrase(Leading, AfterCompletePhrase, FontInfoBold);
                                    //ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ncell.BackgroundColor = new Color(255, 255, 255);
                                    //ncell.Add(nPhrase);
                                    //ObjPdfTblNewAddDoc.AddCell(ncell);


                                    //string AddressInfo = "Intelifi Inc                                                                     \n"
                                    //                    + "Attn: Compliance Department                                        \n "
                                    //                    + "8730 Wilshire Blvd, Suite 412                                          \n"
                                    //                    + "Beverly Hills, CA  90211                                                  \n";



                                    //ncell = new Cell();
                                    //nPhrase = new Phrase();
                                    //nPhrase = new Phrase(Leading, AddressInfo, FontInfoNormal);
                                    //ncell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    //ncell.BackgroundColor = new Color(255, 255, 255);
                                    //ncell.Add(nPhrase);
                                    //ObjPdfTblNewAddDoc.AddCell(ncell);


                                    //string AddressInfo2 = "Fax No. (310) 623-1820                                                  \n"
                                    //                     + "Customer Service No (888) 409-1819                             \n";
                                    //ncell = new Cell();
                                    //nPhrase = new Phrase();
                                    //nPhrase = new Phrase(Leading, AddressInfo2, FontInfoNormal);
                                    //ncell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    //ncell.BackgroundColor = new Color(255, 255, 255);
                                    //ncell.Add(nPhrase);
                                    //ObjPdfTblNewAddDoc.AddCell(ncell);



                                    //ncell = new Cell();
                                    //nPhrase = new Phrase();
                                    //nPhrase = new Phrase(Leading, "Thank you,", FontInfoNormal);
                                    //ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ncell.BackgroundColor = new Color(255, 255, 255);
                                    //ncell.Add(nPhrase);
                                    //ObjPdfTblNewAddDoc.AddCell(ncell);

                                    //ncell = new Cell();
                                    //nPhrase = new Phrase();
                                    //nPhrase = new Phrase(Leading, "Intelifi Compliance Department", FontInfoNormal);
                                    //ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ncell.BackgroundColor = new Color(255, 255, 255);
                                    //ncell.Add(nPhrase);
                                    //ObjPdfTblNewAddDoc.AddCell(ncell);


                                    //TblMain1.InsertTable(ObjPdfTblMainBottom);

                                    //ObjDocument.Add(TblMain);
                                    //ObjDocument.NewPage();
                                    //ObjDocument.Add(TblMain1);
                                    //ObjDocument.NewPage();
                                    //ObjDocument.Add(ObjPdfTblNewAddDoc);

                                    //ObjDocument.NewPage();
                                    #endregion

                                    string footer = "";
                                    #region Mailing Address

                                    var ObjData = new tblOrderSearchedData();
                                    string stateName = "";
                                    string mailingAddress = "";
                                    using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                                    {
                                        ObjData = Dx.tblOrderSearchedDatas.FirstOrDefault(n => n.fkOrderId == RecordList.ElementAt(i).fkOrderId);
                                        if (ObjData != null)
                                        {
                                            if (ObjData.search_state != null && ObjData.search_state != string.Empty)
                                            {
                                                stateName = ((ObjData.search_state != "-1") && (ObjData.search_state != null)) ? Dx.tblStates.FirstOrDefault(n => n.pkStateId == Convert.ToInt32(ObjData.search_state)).StateCode : stateName;
                                            }
                                            else
                                            {
                                                stateName = ((ObjData.SearchedStateId.ToString() != "-1") && (ObjData.SearchedStateId != null)) ? Dx.tblStates.FirstOrDefault(n => n.pkStateId == Convert.ToInt32(ObjData.SearchedStateId)).StateCode : stateName;
                                            }

                                        }
                                    }

                                    mailingAddress = ObjData.SearchedFirstName != string.Empty ? ObjData.SearchedFirstName + " " : mailingAddress;
                                    mailingAddress = ObjData.SearchedLastName != string.Empty ? mailingAddress + ObjData.SearchedLastName + "\n" : mailingAddress + "\n";
                                    if (ObjData.best_SearchedStreetAddress != null && ObjData.best_SearchedStreetAddress != string.Empty)
                                    {
                                        mailingAddress = ObjData.best_SearchedStreetAddress != string.Empty ? mailingAddress + ObjData.best_SearchedStreetAddress + " " : mailingAddress;
                                    }
                                    else if (ObjData.SearchedStreetAddress != null && ObjData.SearchedStreetAddress != string.Empty)
                                    {
                                        mailingAddress = ObjData.SearchedStreetAddress != string.Empty ? mailingAddress + ObjData.SearchedStreetAddress + " " : mailingAddress;
                                    }

                                    if (ObjData.best_SearchedStreetAddress != null && ObjData.best_SearchedStreetAddress != string.Empty && ObjData.SearchedStreetAddress != null && ObjData.SearchedStreetAddress != string.Empty)
                                    {

                                        if (ObjData.best_SearchedStreetName != null && ObjData.best_SearchedStreetName != string.Empty)
                                        {
                                            mailingAddress = ObjData.best_SearchedStreetName != string.Empty ? mailingAddress + ObjData.best_SearchedStreetName + " " : mailingAddress;
                                        }
                                        else if (ObjData.SearchedStreetName != null)
                                        {
                                            mailingAddress = ObjData.SearchedStreetName != string.Empty ? mailingAddress + ObjData.SearchedStreetName + " " : mailingAddress;
                                        }
                                    }




                                    mailingAddress = ObjData.SearchedStreetType != string.Empty ? mailingAddress + ObjData.SearchedStreetType + " " : mailingAddress;
                                    mailingAddress = ObjData.SearchedApt != string.Empty ? mailingAddress + ObjData.SearchedApt + "\n" : mailingAddress + "\n";
                                    //   mailingAddress = ObjData.best_SearchedCity != string.Empty ? mailingAddress + ObjData.best_SearchedCity + ", ": mailingAddress;
                                    if (ObjData.best_SearchedCity != null && ObjData.best_SearchedCity != string.Empty)
                                    {
                                        mailingAddress = ObjData.best_SearchedCity != string.Empty ? mailingAddress + ObjData.best_SearchedCity + ", " : mailingAddress;
                                    }
                                    else if (ObjData.SearchedCity != null && ObjData.SearchedCity != string.Empty)
                                    {
                                        mailingAddress = ObjData.SearchedCity != string.Empty ? mailingAddress + ObjData.SearchedCity + ", " : mailingAddress;
                                    }

                                    //bool AddressStatus = mailingAddress != string.Empty ? true : false;
                                    if (mailingAddress != string.Empty)
                                    {
                                        mailingAddress = stateName != string.Empty ? mailingAddress + stateName : mailingAddress;
                                        if (ObjData.best_SearchedZipCode != null && ObjData.best_SearchedZipCode != string.Empty)
                                        {
                                            mailingAddress = ObjData.best_SearchedZipCode != string.Empty ? mailingAddress + " " + ObjData.best_SearchedZipCode : mailingAddress;
                                        }
                                        else if (ObjData.SearchedZipCode != null && ObjData.SearchedZipCode != string.Empty)
                                        {
                                            mailingAddress = ObjData.SearchedZipCode != string.Empty ? mailingAddress + " " + ObjData.SearchedZipCode : mailingAddress;
                                        }
                                    }

                                    #endregion

                                    #region Page 1 Table Declare
                                    iTextSharp.text.Table MainPageTbl = new iTextSharp.text.Table(2);
                                    MainPageTbl.Width = 100;
                                    MainPageTbl.Padding = 0;
                                    MainPageTbl.Cellpadding = 2;
                                    //MainPageTbl.Cellspacing = 1;
                                    MainPageTbl.DefaultCell.BackgroundColor = Color.WHITE;
                                    // MainPageTbl.CellsFitPage = true;
                                    MainPageTbl.TableFitsPage = true;
                                    MainPageTbl.BorderColor = Color.WHITE;
                                    MainPageTbl.Border = 0;
                                    #endregion

                                    #region  Page 1 Address Blocks  /*commented */

                                    //string usaIntelAddress = " USAintel \n 8730 Wilshire Blvd Suite 412 \n Beverly Hills, CA 90211";
                                    //Phrase fPhrase = new Phrase(Leading, usaIntelAddress, FontInfoNormal);
                                    //Cell fCell = new Cell();
                                    //fCell.BackgroundColor = new Color(255, 255, 255);
                                    //fCell.BorderColor = Color.BLACK;
                                    //fCell.BorderWidth = 1;
                                    //fCell.BorderWidthBottom = 1;
                                    //fCell.BorderWidthLeft = 1;
                                    //fCell.BorderWidthRight = 1;
                                    //fCell.BorderWidthTop = 1;
                                    //fCell.Width = 50;
                                    //fCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //fCell.Add(fPhrase);
                                    //MainPageTbl.AddCell(fCell);

                                    //fPhrase = new Phrase("\n\n");
                                    //fCell = new Cell();
                                    //fCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    //fCell.BackgroundColor = new Color(255, 255, 255);
                                    //fCell.BorderColor = Color.WHITE;
                                    //// fCell.BorderWidthTop = 1;
                                    //// fCell.BorderColorTop = Color.BLACK;
                                    //fCell.Width = 100;
                                    //fCell.Add(fPhrase);
                                    //MainPageTbl.AddCell(fCell);

                                    //fPhrase = mailingAddress != string.Empty ? new Phrase(Leading, mailingAddress, FontInfoNormal) : new Phrase(Leading, "\n\n\n\n", FontInfoNormal);
                                    //fCell = new Cell();
                                    //fCell.BackgroundColor = new Color(255, 255, 255);
                                    //fCell.BorderColor = Color.BLACK;
                                    //fCell.BorderWidth = 1;
                                    //fCell.BorderWidthBottom = 1;
                                    //fCell.BorderWidthLeft = 1;
                                    //fCell.BorderWidthRight = 1;
                                    //fCell.BorderWidthTop = 1;
                                    //fCell.Width = 50;
                                    //fCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //fCell.Add(fPhrase);
                                    //MainPageTbl.AddCell(fCell);


                                    //fPhrase = new Phrase("\n \n");
                                    //fCell = new Cell();
                                    //fCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    //fCell.BackgroundColor = new Color(255, 255, 255);
                                    //fCell.Width = 100;
                                    //fCell.BorderColor = Color.WHITE;
                                    //fCell.BorderColorBottom = Color.BLACK;
                                    //fCell.BorderWidthBottom = 1;
                                    //fCell.Add(fPhrase);
                                    //MainPageTbl.AddCell(fCell);
                                    #endregion

                                    #region Page 1 Address Blocks

                                    string usaIntelAddress = "\n Intelifi,Inc. \n 8730 Wilshire Blvd Suite 412 \n Beverly Hills, CA 90211 \n\n";
                                    Phrase fPhrase = new Phrase(Leading, usaIntelAddress, FontInfoNormal);
                                    Cell fCell = new Cell();
                                    fCell.BackgroundColor = new Color(255, 255, 255);
                                    //fCell.BorderColor = Color.BLACK;
                                    //fCell.BorderWidth = 1;
                                    //fCell.BorderWidthBottom = 1;
                                    //fCell.BorderWidthLeft = 1;
                                    //fCell.BorderWidthRight = 1;
                                    //fCell.BorderWidthTop = 1;
                                    fCell.Border = Rectangle.NO_BORDER;
                                    fCell.Width = 50;
                                    fCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    fCell.Add(fPhrase);
                                    MainPageTbl.AddCell(fCell);

                                    fCell = new Cell();
                                    fCell.BorderColor = Color.WHITE;
                                    fCell.Border = Rectangle.NO_BORDER;
                                    fCell.Width = 50;
                                    MainPageTbl.AddCell(fCell);


                                    fPhrase = new Phrase("\n\n");
                                    fCell = new Cell();
                                    fCell.Colspan = 2;
                                    fCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    fCell.BackgroundColor = new Color(255, 255, 255);
                                    fCell.BorderColor = Color.WHITE;
                                    // fCell.BorderWidthTop = 1;
                                    // fCell.BorderColorTop = Color.BLACK;
                                    fCell.Width = 100;
                                    fCell.Add(fPhrase);
                                    MainPageTbl.AddCell(fCell);


                                    //fPhrase = mailingAddress != string.Empty ? new Phrase(Leading, "\n" + mailingAddress + "\n\n", FontInfoNormal) : new Phrase(Leading, "\n\n\n\n\n\n", FontInfoNormal);

                                    fPhrase = mailingAddress != string.Empty ? new Phrase(Leading, "\n" + mailingAddress + "\n\n", FontInfoNormal) : new Phrase(Leading, "\n\n\n", FontInfoNormal);
                                    fCell = new Cell();
                                    fCell.BackgroundColor = new Color(255, 255, 255);
                                    //fCell.BorderColor = Color.BLACK;
                                    //fCell.BorderWidth = 1;
                                    //fCell.BorderWidthBottom = 1;
                                    //fCell.BorderWidthLeft = 1;
                                    //fCell.BorderWidthRight = 1;
                                    //fCell.BorderWidthTop = 1;
                                    fCell.Border = Rectangle.NO_BORDER;
                                    fCell.Width = 50;
                                    fCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    fCell.Add(fPhrase);
                                    MainPageTbl.AddCell(fCell);

                                    fCell = new Cell();
                                    fCell.BorderColor = Color.WHITE;
                                    fCell.Border = Rectangle.NO_BORDER;
                                    fCell.Width = 50;
                                    MainPageTbl.AddCell(fCell);

                                    fPhrase = new Phrase("\n \n");
                                    fCell = new Cell();
                                    fCell.Colspan = 2;
                                    fCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    fCell.BackgroundColor = new Color(255, 255, 255);
                                    fCell.Width = 100;
                                    fCell.BorderColor = Color.WHITE;
                                    fCell.BorderColorBottom = Color.BLACK;
                                    fCell.BorderWidthBottom = 1;
                                    fCell.Add(fPhrase);
                                    MainPageTbl.AddCell(fCell);
                                    #endregion

                                    #region page 1 Lower Table Declare
                                    iTextSharp.text.Table TblMain = new iTextSharp.text.Table(1);
                                    TblMain.Width = 100;
                                    TblMain.Padding = 0;
                                    TblMain.Cellpadding = 2;
                                    TblMain.Cellspacing = 1;
                                    TblMain.DefaultCell.Border = Rectangle.NO_BORDER;
                                    TblMain.Border = 2;
                                    TblMain.BorderColor = Color.WHITE;
                                    TblMain.CellsFitPage = true;
                                    TblMain.TableFitsPage = true;
                                    #endregion

                                    #region page 3 Table Declare
                                    iTextSharp.text.Table TblMain1 = new iTextSharp.text.Table(1);
                                    TblMain1.Width = 100;
                                    TblMain1.Padding = 0;
                                    TblMain1.Cellpadding = 2;
                                    TblMain1.Cellspacing = 1;
                                    TblMain1.DefaultCell.Border = Rectangle.NO_BORDER;
                                    TblMain1.CellsFitPage = true;
                                    TblMain1.TableFitsPage = true;
                                    #endregion

                                    #region page 1 Lower Table Data
                                    Phrase op = new Phrase();
                                    Cell ocell = new Cell(op);
                                    //Chunk ock = new Chunk();

                                    op = new Phrase(Leading, DateTime.Now.ToString("MMM-dd-yyyy"), FontInfoNormal);
                                    ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell.BackgroundColor = new Color(255, 255, 255);
                                    ocell.Add(op);
                                    TblMain.AddCell(ocell);


                                    ocell = new Cell();
                                    op = new Phrase(Leading, "Dear " + ApplicantName, FontInfoNormal);
                                    ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell.BackgroundColor = new Color(255, 255, 255);
                                    ocell.Add(op);
                                    TblMain.AddCell(ocell);

                                    string paragraph = string.Empty;
                                    #region Main Paragraph
                                    paragraph = "";
                                    paragraph = "Pursuant to FCRA 613 this is to advise you that " + CompanyName + " at " + Address
                                       + " has requested that we furnish information from public records regarding you. Enclosed is "
                                       + " a copy of the Summary of Rights prepared by the Consumer Financial Protection Bureau , and  "
                                       + " a  document to request a copy of your report. If you have any questions or concerns  regarding "
                                       + "the background report please contact USA Intelifi,Inc.  8730 Wilshire Blvd. Suite 412 Beverly Hills, "
                                       + " CA 90211 (800) 409-1819.\n\n";

                                    #endregion


                                    iTextSharp.text.Font FontInfoNormal1 = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.NORMAL, iTextSharp.text.Color.BLACK);


                                    ocell = new Cell();
                                    op = new Phrase(Leading, paragraph, FontInfoNormal1);
                                    ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell.BackgroundColor = new Color(255, 255, 255);
                                    ocell.Add(op);
                                    TblMain.AddCell(ocell);

                                    ocell = new Cell();
                                    op = new Phrase(Leading, "Sincerely,", FontInfoNormal);
                                    ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell.BackgroundColor = new Color(255, 255, 255);
                                    ocell.Add(op);
                                    TblMain.AddCell(ocell);

                                    ocell = new Cell();
                                    op = new Phrase(Leading, "Intelifi,Inc.", FontInfoNormal);
                                    ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell.BackgroundColor = new Color(255, 255, 255);
                                    ocell.Add(op);
                                    TblMain.AddCell(ocell);

                                    ocell = new Cell();
                                    op = new Phrase(Leading, "Enclosures: Consumer Summary of Rights\nDocuments Request Form", FontInfoNormal);
                                    ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell.BackgroundColor = new Color(255, 255, 255);
                                    ocell.Add(op);
                                    TblMain.AddCell(ocell);

                                    ocell = new Cell();
                                    op = new Phrase(Leading, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", FontInfoNormal);
                                    ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell.BackgroundColor = new Color(255, 255, 255);
                                    ocell.Add(op);
                                    TblMain.AddCell(ocell);
                                    #endregion

                                    #region Page1 Footer
                                    iTextSharp.text.Table footerTbl1 = new iTextSharp.text.Table(1);
                                    footerTbl1.Width = 100;
                                    footerTbl1.Padding = 0;
                                    footerTbl1.Cellpadding = 2;
                                    footerTbl1.Cellspacing = 1;
                                    footerTbl1.DefaultCell.Border = Rectangle.NO_BORDER;
                                    footerTbl1.CellsFitPage = true;
                                    footerTbl1.TableFitsPage = true;
                                    footerTbl1.BorderColor = Color.WHITE;
                                    footerTbl1.Border = 0;

                                    Cell ocell3 = new Cell();
                                    Phrase op3 = new Phrase(Leading, "\n\n\n\n\n\n", FontInfoNormal);
                                    ocell3.HorizontalAlignment = Element.ALIGN_CENTER;
                                    ocell3.BackgroundColor = new Color(255, 255, 255);
                                    ocell3.BorderColor = Color.WHITE;
                                    ocell3.Border = 0;
                                    ocell3.Add(op3);
                                    footerTbl1.AddCell(ocell3);

                                    ocell3 = new Cell();
                                    footer = "1                                                                                                                              " + DateToBePrint.ToString("MMMM dd, yyyy hh:mm tt");
                                    op3 = new Phrase(Leading, footer, FontInfoNormal);
                                    ocell3.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell3.BackgroundColor = new Color(255, 255, 255);
                                    ocell3.BorderColor = Color.BLACK;
                                    ocell3.BorderWidthBottom = 1;
                                    ocell3.BorderWidthLeft = 0;
                                    ocell3.BorderWidthRight = 0;
                                    ocell3.BorderWidthTop = 1;
                                    ocell3.Add(op3);
                                    footerTbl1.AddCell(ocell3);
                                    #endregion

                                    #region page 3 Table Data
                                    iTextSharp.text.Font FontInfoItalic = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.ITALIC, iTextSharp.text.Color.BLACK);
                                    iTextSharp.text.Font FontInfoBoldUnderLine = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.UNDERLINE, iTextSharp.text.Color.BLACK);
                                    iTextSharp.text.Font FontInfoBold = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.BOLD, iTextSharp.text.Color.BLACK);

                                    string paragraph2 = string.Empty;

                                    paragraph2 = "";
                                    paragraph2 = "Para information en espanol, visite www.consumerfinance.gov/learnmore  o escribe a la Consumer "
                                        + " Financial Protection Bureau , 1700 G Street N.W., Washington, DC 2006.";



                                    ocell = new Cell();
                                    op = new Phrase(Leading, paragraph2, FontInfoNormal);
                                    ocell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    ocell.BackgroundColor = new Color(255, 255, 255);
                                    ocell.Add(op);
                                    TblMain1.AddCell(ocell);


                                    ocell = new Cell();
                                    op = new Phrase(Leading, "A Summary of Your Rights Under the Fair Credit Reporting Act", FontInfoBoldUnderLine);
                                    ocell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    ocell.BackgroundColor = new Color(255, 255, 255);
                                    ocell.Add(op);
                                    TblMain1.AddCell(ocell);


                                    //ocell = new Cell();
                                    //op = new Phrase(Leading, str21(), FontInfoNormal);
                                    //op.Leading = 13;
                                    //ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    //ocell.BackgroundColor = new Color(255, 255, 255);
                                    //ocell.Add(op);
                                    //TblMain1.AddCell(ocell);

                                    ocell = new Cell();
                                    op = new Phrase(Leading, str22(), FontInfoNormal);
                                    op.Leading = 13;
                                    ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell.BackgroundColor = new Color(255, 255, 255);
                                    ocell.Add(op);
                                    TblMain1.AddCell(ocell);

                                    ocell = new Cell();
                                    op = new Phrase(Leading, str23(), FontInfoNormal);
                                    op.Leading = 13;
                                    ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell.BackgroundColor = new Color(255, 255, 255);
                                    ocell.Add(op);
                                    TblMain1.AddCell(ocell);

                                    ocell = new Cell();
                                    op = new Phrase(Leading, str3(), FontInfoNormal);
                                    op.Leading = 15;
                                    ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell.BackgroundColor = new Color(255, 255, 255);
                                    ocell.Add(op);
                                    TblMain1.AddCell(ocell);

                                    //ocell = new Cell();
                                    //op = new Phrase(Leading, "\n\n", FontInfoNormal);
                                    //ocell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    //ocell.BackgroundColor = new Color(255, 255, 255);
                                    //ocell.Add(op);
                                    //TblMain1.AddCell(ocell);
                                    #endregion

                                    #region Page3 Footer
                                    iTextSharp.text.Table footerTbl3 = new iTextSharp.text.Table(1);
                                    footerTbl3.Width = 100;
                                    footerTbl3.Padding = 0;
                                    footerTbl3.Cellpadding = 2;
                                    footerTbl3.Cellspacing = 1;
                                    footerTbl3.DefaultCell.Border = Rectangle.NO_BORDER;
                                    footerTbl3.CellsFitPage = true;
                                    footerTbl3.TableFitsPage = true;
                                    footerTbl3.BorderColor = Color.WHITE;
                                    footerTbl3.Border = 0;

                                    Cell ocell2 = new Cell();
                                    Phrase op2 = new Phrase(Leading, "\n\n", FontInfoNormal);
                                    ocell2.HorizontalAlignment = Element.ALIGN_CENTER;
                                    ocell2.BackgroundColor = new Color(255, 255, 255);
                                    ocell2.BorderColor = Color.WHITE;
                                    ocell2.Border = 0;
                                    ocell2.BorderWidthTop = 1;
                                    ocell2.BorderColorTop = Color.BLACK;
                                    ocell2.Add(op2);
                                    footerTbl3.AddCell(ocell2);

                                    ocell2 = new Cell();
                                    footer = "2                                                                                                                              " + DateToBePrint.ToString("MMMM dd, yyyy hh:mm tt");
                                    op2 = new Phrase(Leading, footer, FontInfoNormal);
                                    ocell2.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell2.BackgroundColor = new Color(255, 255, 255);
                                    ocell2.BorderColor = Color.BLACK;
                                    ocell2.BorderWidthBottom = 1;
                                    ocell2.BorderWidthLeft = 0;
                                    ocell2.BorderWidthRight = 0;
                                    ocell2.BorderWidthTop = 1;
                                    ocell2.Add(op2);
                                    footerTbl3.AddCell(ocell2);
                                    #endregion

                                    #region Page 4
                                    iTextSharp.text.Table ObjPdfTblMainBottom = new iTextSharp.text.Table(2);
                                    ObjPdfTblMainBottom.Width = 100;
                                    ObjPdfTblMainBottom.Padding = 0;
                                    ObjPdfTblMainBottom.Cellpadding = 2;
                                    ObjPdfTblMainBottom.Cellspacing = 1;
                                    //ObjPdfTblMainBottom.DefaultCell.Border = Rectangle.NO_BORDER;
                                    ObjPdfTblMainBottom.CellsFitPage = true;
                                    ObjPdfTblMainBottom.TableFitsPage = true;
                                    ObjPdfTblMainBottom.SetWidths(new int[2] { 60, 40 });
                                    ObjPdfTblMainBottom.BorderColor = Color.BLACK;
                                    ObjPdfTblMainBottom.Border = 1;

                                    Phrase TPhrase = new Phrase();
                                    Cell TCell = new Cell(TPhrase);

                                    TCell = new Cell();
                                    TPhrase = new Phrase(Leading, "TYPE OF BUSINESS:", FontInfoBold);
                                    TPhrase.Leading = 15;
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    TCell = new Cell();
                                    TPhrase = new Phrase(Leading, "CONTACT:", FontInfoBold);
                                    TPhrase.Leading = 15;
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    string strTL1 = string.Empty;
                                    strTL1 = "1.a. Banks, saving associations, and credit unions with total assets of over $10 billion and their"
                                    + " affiliates."
                                    + "\nb. Such affiliates that are not banks,saving associations, or credit unions also should list, in"
                                    + " addition to the Bureau:";
                                    TCell = new Cell();
                                    TPhrase = new Phrase(13, strTL1, FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    string strTR1 = string.Empty;
                                    strTR1 = "a. Bureau of Consumer Financial Protection"
                                    + "\n1700 G Street NW"
                                    + "\nWashington, DC 20006"
                                    + "\nb. Federal Trade Commission: ConsumerResponse Center- FCRA"
                                    + "\nWashington, DC 20580"
                                    + "\n(877)382-4357";

                                    TCell = new Cell();
                                    TPhrase = new Phrase();
                                    TPhrase = new Phrase(13, strTR1, FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    string strTL2 = string.Empty;
                                    strTL2 = "2. To the extent not included in item 1 above:"
                                    + "\na. National Banks, federal saving associations, and federal branches ad federal agencies of"
                                    + "foreign banks"
                                    + "\nb. State member banks, branches and agencies of foreign banks( other than federal branches,"
                                    + " federal agencies and insured state branches of foreign banks), commercial leading companies"
                                    + " owned or controlled by foreign banks, and organizations operating under section 25 or 25A of"
                                    + " the Federal Reserve Act"
                                    + "\nc. Nonmember Insured Banks, Insured State Branches of Foreign banks and insured state"
                                    + " saving organizations"
                                    + "\nd. Federal Credit Unions";
                                    TCell = new Cell();
                                    TPhrase = new Phrase(13, strTL2, FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    string strTR2 = string.Empty;
                                    strTR2 = "a. Office of Comptroller of the Currency"
                                    + "\nCustomer Assistance Group"
                                    + "\n1301 McKinnney Street, Suite 3450"
                                    + "\nHouston, TX 77010-9050"
                                    + "\nb. Federal Reserve Consumer Help Center"
                                    + "\nPO Box 1200"
                                    + "\nMinneapolis, MN55480"
                                    + "\nc. FDIC Consumer Response Center"
                                    + "\n1100 Walnut Street, Box #11"
                                    + "\nKansas City, MO 64106"
                                    + "\nd. National Credit Union Administrator"
                                    + "\nOffice of Consumer Protection(OCP)"
                                    + "\nDivision of Consumer Compliance and Outreach(DCCO)"
                                    + "\n1775 Duke Street, Alexandria, VA 22314";

                                    TCell = new Cell();
                                    TPhrase = new Phrase();
                                    TPhrase = new Phrase(13, strTR2, FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    TCell = new Cell();
                                    TPhrase = new Phrase(13, "3. Air carriers", FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    string strTR3 = string.Empty;
                                    strTR3 = "Asst.General Counsel for Aviation Enforcement & Proceeding"
                                    + "\n Department ofTransportation"
                                    + "\n400 Seventh Street SW"
                                    + "\nWashington, DC 20590";

                                    TCell = new Cell();
                                    TPhrase = new Phrase();
                                    TPhrase = new Phrase(13, strTR3, FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);


                                    TCell = new Cell();
                                    TPhrase = new Phrase(13, "4. Creditors Subject to Surface Transportation Board", FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    string strTR4 = string.Empty;
                                    strTR4 = "Office of Proceedings, Surface Transportation Board"
                                    + "\nDepartment of Transportation"
                                    + "\n1925 K Street NW"
                                    + "\nWashington, DC 20423";

                                    TCell = new Cell();
                                    TPhrase = new Phrase();
                                    TPhrase = new Phrase(13, strTR4, FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);


                                    TCell = new Cell();
                                    TPhrase = new Phrase(13, "5. Creditors Subject to Packers and Stockyards Act", FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    string strTR5 = string.Empty;
                                    strTR5 = "Nearest packers and Stockyards Administration area supervisor";

                                    TCell = new Cell();
                                    TPhrase = new Phrase();
                                    TPhrase = new Phrase(13, strTR5, FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    TCell = new Cell();
                                    TPhrase = new Phrase(13, "6. Small Business Investment Companies", FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    string strTR6 = string.Empty;
                                    strTR6 = "Associate Deputy Administrator for CapitalAccess"
                                    + " United States Small Business Administration"
                                    + " 406 Third Street, SW, 8TH Floor"
                                    + " Washington, DC 20416";

                                    TCell = new Cell();
                                    TPhrase = new Phrase();
                                    TPhrase = new Phrase(13, strTR6, FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    TCell = new Cell();
                                    TPhrase = new Phrase(13, "7. Brokers and Dealers", FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    string strTR7 = string.Empty;
                                    strTR7 = "Securities and Exchange Commission"
                                    + "\n100 F St NE"
                                    + "\nWashington, DC 20549";

                                    TCell = new Cell();
                                    TPhrase = new Phrase();
                                    TPhrase = new Phrase(13, strTR7, FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    string strTL8 = string.Empty;
                                    strTL8 = "8. Federal Land Banks, Federal Land Bank Associations, Federal Intermediate Credit Banks,"
                                    + " and production Credit Associations";
                                    TCell = new Cell();
                                    TPhrase = new Phrase(13, strTL8, FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    string strTR8 = string.Empty;
                                    strTR8 = "Farm Credit Administration"
                                    + "\n1501 Farm Credit Drive"
                                    + "\nMcLean, VA 22102-5090";

                                    TCell = new Cell();
                                    TPhrase = new Phrase(13, strTR8, FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    TCell = new Cell();
                                    TPhrase = new Phrase();
                                    TPhrase = new Phrase(13, "9. Retailers, Finance Companies, and All Other Creditors Not Listed Above", FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);


                                    string strTR9 = string.Empty;
                                    strTR9 = "FTC Regional Office for region in which the creditor operates or"
                                    + "\nFederal Trade Commission: Consumer Response Center- FCRA"
                                    + "\nWashington, DC 20580"
                                    + "\n(877)382-4357";

                                    TCell = new Cell();
                                    TPhrase = new Phrase();
                                    TPhrase = new Phrase(13, strTR9, FontInfoNormal);
                                    TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    TCell.BackgroundColor = new Color(255, 255, 255);
                                    TCell.Add(TPhrase);
                                    ObjPdfTblMainBottom.AddCell(TCell);

                                    #endregion

                                    #region Page Num 4 Footer
                                    iTextSharp.text.Table footerTbl4 = new iTextSharp.text.Table(1);
                                    footerTbl4.Width = 100;
                                    footerTbl4.Padding = 0;
                                    footerTbl4.Cellpadding = 2;
                                    footerTbl4.Cellspacing = 1;
                                    footerTbl4.DefaultCell.Border = Rectangle.NO_BORDER;
                                    footerTbl4.CellsFitPage = true;
                                    footerTbl4.TableFitsPage = true;
                                    //footerTbl.BorderColor = Color.WHITE;
                                    footerTbl4.Border = 0;

                                    Cell ocell4 = new Cell();
                                    Phrase op4 = new Phrase(Leading, "\n", FontInfoNormal);
                                    ocell4.HorizontalAlignment = Element.ALIGN_CENTER;
                                    ocell4.BackgroundColor = new Color(255, 255, 255);
                                    ocell4.BorderColor = Color.WHITE;
                                    ocell4.Border = 0;
                                    ocell4.BorderWidthTop = 1;
                                    ocell4.BorderColorTop = Color.BLACK;
                                    ocell4.Add(op4);
                                    footerTbl4.AddCell(ocell4);

                                    ocell4 = new Cell();
                                    footer = "3                                                                                                                              " + DateToBePrint.ToString("MMMM dd, yyyy hh:mm tt");
                                    op4 = new Phrase(Leading, footer, FontInfoNormal);
                                    ocell4.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell4.BackgroundColor = new Color(255, 255, 255);
                                    ocell4.BorderColor = Color.BLACK;
                                    ocell4.BorderWidthBottom = 1;
                                    ocell4.BorderWidthLeft = 0;
                                    ocell4.BorderWidthRight = 0;
                                    ocell4.BorderWidthTop = 1;
                                    ocell4.Add(op4);
                                    footerTbl4.AddCell(ocell4);
                                    #endregion

                                    // New  Documents 

                                    #region Page 5 in Pdf

                                    iTextSharp.text.Table ObjPdfTblNewAddDoc = new iTextSharp.text.Table(1);

                                    ObjPdfTblNewAddDoc.Width = 100;
                                    ObjPdfTblNewAddDoc.Padding = 0;
                                    ObjPdfTblNewAddDoc.Cellpadding = 2;
                                    ObjPdfTblNewAddDoc.Cellspacing = 1;
                                    ObjPdfTblNewAddDoc.DefaultCell.Border = Rectangle.NO_BORDER;
                                    ObjPdfTblNewAddDoc.CellsFitPage = true;
                                    ObjPdfTblNewAddDoc.TableFitsPage = true;


                                    Phrase nPhrase = new Phrase();
                                    string paragraph3 = string.Empty;
                                    Cell ncell = new Cell(nPhrase);

                                    iTextSharp.text.Image ImgLogo = iTextSharp.text.Image.GetInstance(new Uri(ApplicationPath.GetApplicationPath() + @"Content\themes\base\images\IntelifiPowerToKnow.png"));
                                    nPhrase = new Phrase();
                                    ImgLogo.ScaleAbsoluteHeight(40);
                                    ImgLogo.ScaleAbsoluteWidth(100);
                                    Chunk chk = new Chunk(ImgLogo, 0, -2);
                                    nPhrase.Add(chk);
                                    ObjPdfTblNewAddDoc.AddCell(nPhrase);


                                    ncell = new Cell();
                                    nPhrase = new Phrase(Leading, "Document Request Form", FontInfoBold);
                                    ncell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);

                                    paragraph3 = "";
                                    paragraph3 = "This form is to be used to request a copy of your Consumer Background Investigative Report. Pursuant"
                                    + " to FCRA§ 609 and the abridged notification in theincluded Summary of Your Rights Under the Fair Credit Reporting"
                                    + " Act, “Every consumer reporting agency shall,upon request, and subject to 610(a)(1) [§ 1681h], clearly and "
                                    + " accurately disclose to theconsumer all information in the consumer's file at the time of the request.” \n \n";



                                    ncell = new Cell();
                                    nPhrase = new Phrase(Leading, paragraph3, FontInfoNormal);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);


                                    string paragraph4 = string.Empty;

                                    paragraph4 = "";
                                    paragraph4 = "To receive a copy of your Consumer Background Investigative Report by mail, simply send the below"
                                    + " identifying information along with proof of your identity.  Proof of your identity can include a photocopy of "
                                    + " one of the following documents: a State Driver License, a State Issued ID Card, a Military ID Card or a W2 form. \n";




                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, paragraph4, FontInfoNormal);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(op);
                                    ObjPdfTblNewAddDoc.AddCell(nPhrase);

                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, "\nRequired Information: \n", FontInfoBold);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);

                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, "Your Social Security Number: ____ ____ ____ - ____ ____ - ____ ____ ____ ____ \n", FontInfoNormal);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);

                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, "Full Legal Name:______________________________________________________________________________ ", FontInfoNormal);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);

                                    ocell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, "DOB:________________________________________________________________________________________ ", FontInfoNormal);
                                    ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell.BackgroundColor = new Color(255, 255, 255);
                                    ocell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ocell);

                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, "Street Address (residence):____________________________________________________________________ ", FontInfoNormal);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);

                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, "City, State, Zip: _____________________________________________________________________________ ", FontInfoNormal);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);

                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, "Phone Number: (_______)____________________________________________________________________ ", FontInfoNormal);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);

                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, "E-mail Address: _____________________________________________________________________________ ", FontInfoNormal);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);


                                    string PleaseLeadPhrase = "\n Please read and sign the following statement. Your signature acknowledges your agreement. "
                                                             + "By submitting this form, I state that all the information contained is true to the best of my knowledge.";
                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, PleaseLeadPhrase, FontInfoBold);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);



                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, "\nSignature: _________________________________________________________Date: __________________ \n", FontInfoNormal);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);



                                    string AfterCompletePhrase = "After completing this form, please return it by mail or fax. The mailing address and fax number are provided "
                                                                + "below. Remember to include your proof of identity.";


                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, AfterCompletePhrase, FontInfoBold);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);


                                    string AddressInfo = "Intelifi Inc                                                                     \n"
                                                        + "Attn: Compliance Department                                        \n "
                                                        + "8730 Wilshire Blvd, Suite 412                                          \n"
                                                        + "Beverly Hills, CA  90211                                                  \n";



                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, AddressInfo, FontInfoNormal);
                                    ncell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);


                                    string AddressInfo2 = "Fax No. (310) 623-1820                                                  \n"
                                                         + "Customer Service No (888) 409-1819                             \n";
                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, AddressInfo2, FontInfoNormal);
                                    ncell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);



                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, "Thank you,", FontInfoNormal);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);

                                    ncell = new Cell();
                                    nPhrase = new Phrase();
                                    nPhrase = new Phrase(Leading, "Intelifi Compliance Department", FontInfoNormal);
                                    ncell.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ncell.BackgroundColor = new Color(255, 255, 255);
                                    ncell.Add(nPhrase);
                                    ObjPdfTblNewAddDoc.AddCell(ncell);


                                    #endregion

                                    #region Page Num 5 Footer
                                    iTextSharp.text.Table footerTbl5 = new iTextSharp.text.Table(1);
                                    footerTbl5.Width = 100;
                                    footerTbl5.Padding = 0;
                                    footerTbl5.Cellpadding = 2;
                                    footerTbl5.Cellspacing = 1;
                                    footerTbl5.DefaultCell.Border = Rectangle.NO_BORDER;
                                    footerTbl5.CellsFitPage = true;
                                    footerTbl5.TableFitsPage = true;
                                    //footerTbl.BorderColor = Color.WHITE;
                                    footerTbl5.Border = 0;

                                    Cell ocell1 = new Cell();
                                    Phrase op1 = new Phrase(Leading, "\n\n\n\n\n\n\n\n", FontInfoNormal);
                                    ocell1.HorizontalAlignment = Element.ALIGN_CENTER;
                                    ocell1.BackgroundColor = new Color(255, 255, 255);
                                    ocell1.BorderColor = Color.WHITE;
                                    ocell1.Border = 0;
                                    ocell1.BorderWidthTop = 1;
                                    ocell1.BorderColorTop = Color.BLACK;
                                    ocell1.Add(op1);
                                    footerTbl5.AddCell(ocell1);

                                    ocell1 = new Cell();
                                    footer = "4                                                                                                                              " + DateToBePrint.ToString("MMMM dd, yyyy hh:mm tt");
                                    op1 = new Phrase(Leading, footer, FontInfoNormal);
                                    ocell1.HorizontalAlignment = Element.ALIGN_LEFT;
                                    ocell1.BackgroundColor = new Color(255, 255, 255);
                                    ocell1.BorderColor = Color.BLACK;
                                    ocell1.BorderWidthBottom = 1;
                                    ocell1.BorderWidthLeft = 0;
                                    ocell1.BorderWidthRight = 0;
                                    ocell1.BorderWidthTop = 1;
                                    ocell1.Add(op1);
                                    footerTbl5.AddCell(ocell1);
                                    #endregion

                                    #region Blank Page Insert In PDF
                                    iTextSharp.text.Table blankTbl = new iTextSharp.text.Table(1);
                                    blankTbl.Width = 100;
                                    blankTbl.Padding = 0;
                                    blankTbl.Cellpadding = 2;
                                    blankTbl.Cellspacing = 1;
                                    blankTbl.DefaultCell.Border = Rectangle.NO_BORDER;
                                    blankTbl.CellsFitPage = true;
                                    blankTbl.TableFitsPage = true;
                                    #endregion

                                    #region Add Table in Document
                                    //TblMain1.InsertTable(footerTbl);
                                    //TblMain1.InsertTable(ObjPdfTblMainBottom);
                                    //MainPageTbl.InsertTable(TblMain);
                                    //ObjDocument.Add(MainPageTbl);

                                    ObjDocument.Add(MainPageTbl);
                                    ObjDocument.Add(TblMain);
                                    ObjDocument.Add(footerTbl1);
                                    ObjDocument.NewPage();
                                    ObjDocument.Add(blankTbl);
                                    ObjDocument.NewPage();
                                    ObjDocument.Add(TblMain1);
                                    ObjDocument.Add(blankTbl);
                                    ObjDocument.Add(footerTbl3);
                                    ObjDocument.NewPage();
                                    ObjDocument.Add(ObjPdfTblMainBottom);
                                    ObjDocument.Add(blankTbl);
                                    ObjDocument.Add(footerTbl4);
                                    ObjDocument.NewPage();
                                    ObjDocument.Add(ObjPdfTblNewAddDoc);
                                    ObjDocument.Add(footerTbl5);
                                    ObjDocument.NewPage();
                                    ObjDocument.Add(blankTbl);
                                    ObjDocument.NewPage();
                                    #endregion
                                }
                            }
                            //  }
                            // else
                            // {
                            //     return pdfFileName = "NotValid";
                            // }
                        }
                    }
                }
                // ObjDocument.Close();

            }
            catch (Exception ex)
            {
                ObjDocument.NewPage();
            }
            finally
            {

                ObjDocument.Close();

            }
            return pdfFileName;
        }


        public string GenerateApplicantName(string sLastname, string sSufix, string sMiddle, string sFirstName)
        {
            //string applicantName = string.Empty;
            //int getstr = 0, ilen = 0;
            //applicantName = (sLastname != "") ? sLastname + ", " : "";
            //applicantName += (sSufix != "") ? sSufix + ", " : "";
            //applicantName += (sMiddle != "") ? sMiddle + ", " : "";
            //applicantName += (sFirstName != "") ? sFirstName + ", " : "";

            //ilen = applicantName.LastIndexOf(",");
            //getstr = ilen + 2;
            //if (getstr == applicantName.Length)
            //{
            //    applicantName = applicantName.Substring(0, ilen);
            //}
            //return applicantName;
            //INT-132--Based on the client requirement (FirstName + Middle Name+ Last Name).
            string applicantName = string.Empty;
            int getstr = 0, ilen = 0;
            applicantName += (sSufix != "") ? sSufix + ", " : "";
            applicantName += (sFirstName != "") ? sFirstName + " " : "";
            applicantName += (sMiddle != "") ? sMiddle + " " : "";
            applicantName += (sLastname != "") ? sLastname + ", " : "";
            ilen = applicantName.LastIndexOf(",");
            getstr = ilen + 2;
            if (getstr == applicantName.Length)
            {
                applicantName = applicantName.Substring(0, ilen + 1);
            }
            return applicantName;

        }

        public string str21()
        {
            string str = string.Empty;

            str = " ";
            return str;
        }

        public string str22()
        {
            string str = "The federal Fair Credit Reporting Act (FCRA) promotes the accuracy, fairness, and "
               + "privacy of information in the files of consumer reporting agencies. There are many "
               + "types of consumer reporting agencies, including credit bureaus and specialty agencies "
               + "(such as agencies that sell information about check writing histories, medical "
               + "records, and rental history records). Here is a summary of your major "
               + "rights under the FCRA. For more information, including information about additional "
               + "rights, go to www.consumerfinance.gov/learnmore or write to: Consumer Financial Protection Bureau, "
               + "1700 G Street N.W., Washington, D.C. 20006. "
               + "\nYou may have additional rights under Maine's FCRA, Me. Rev. Stat. Ann. 10, Sec 1311 et"
               + "seq.\n"
               +

                 // Paragraph 2

                "\n•You must be told if information in your file has been used against you. Anyone who"
                        + "uses a credit report or another type of consumer report to deny your application for"
                        + "credit, insurance, or employment – or to take another adverse action against you –"
                        + "must tell you, and must give you the name, address, and phone number of the agency"
                        + "that provided the information."
                + "\n•	You have the right to know what is in your file. You may request and obtain all "
                        + "the information about you in the files of a consumer reporting agency (your “file"
                        + "disclosure”). You will be required to provide proper identification, which may "
                        + "include your Social Security number. In many cases, the disclosure will be free."
                        + "You are entitled to a free file disclosure  if:"
                // Paragraph 3
                + "\n\t\t\t\t\t\t\t\t\t\t•	A person has taken adverse action against you because of information in your"
                + "credit report;"
                + "\n\t\t\t\t\t\t\t\t\t\t•	You are the victim of identify theft and place a fraud alert in your file;"
                + "\n\t\t\t\t\t\t\t\t\t\t•	Your file contains inaccurate information as a result of fraud;"
                + "\n\t\t\t\t\t\t\t\t\t\t•	You are on public assistance;"
                + "\n\t\t\t\t\t\t\t\t\t\t•	You are unemployed but expect to apply for employment within 60 days."
                + "\n  \t\t\t\t\t\t\tIn addition, all consumers are  entitled to one free disclosure"
                + " every 12 months upon request from each nationwide credit bureau and \n"
                + " from nationwide specialty consumer reporting agencies. See www.consumerfinance.gov/learnmore for additional "
                + " information."
                + "\n•	You have the right to ask for a credit score. Credit scores are numerical"
                + " summaries of your credit-worthiness based on information from credit bureaus."
                + " You may request a credit score from consumer reporting agencies that create scores"
                + " or distribute scores used in residential real property loans, but you will have to"
                + " pay for it. In some mortgage transactions, you will receive credit score information "
                + " for free from the mortgage lender."
                // Paragraph 4
                + "\n•	You have the right to dispute incomplete or inaccurate information. If you identify"
                + " information in your file that is incomplete or inaccurate, and report it to the"
                + " consumer reporting agency, the agency must investigate unless your dispute is "
                + " frivolous. See www.consumerfinance.gov/learnmore for an explanation of dispute procedures."
                + "\n•	Consumer reporting agencies must correct or delete inaccurate, incomplete,"
                + " or unverifiable information. Inaccurate, incomplete or unverifiable information"
                + " must be removed or corrected, usually within 30 days. However, a consumer reporting"
                + " agency may continue to report information it has verified as accurate."
                + "\n•	Consumer reporting agencies may not report outdated negative information."
                + "In most cases, a consumer reporting agency may not report negative information"
                + "that is more than seven years old, or bankruptcies that are more than 10 years old."
                + "\n•	Access to your file is limited. A consumer reporting agency may provide"
                + " information about you only to people with a valid need -- usually to consider "
                + " an application with a creditor, insurer, employer, landlord, or other business."
                + " The FCRA specifies those with a valid need for access."

                 // Paragraph 5
                + "\n•	You must give your consent for reports to be provided to employers. A "
                + " consumer reporting agency may not give out information about you to your"
                + " employer, or a potential employer, without your written consent given to"
                + " the employer. Written consent generally is not required in the trucking industry. "
                + " For more information, go to www.consumerfinance.gov/learnmore."
                + "\n•	You may limit “prescreened” offers of credit and insurance you get based on information in "
                + " your credit report. Unsolicited “prescreened” offers for credit and insurance must"
                + " include a toll-free phone number you can call if you choose to remove your name"
                + " and address from the lists these offers are based on. You may opt-out with the"
                + " nationwide credit bureaus at 1-888-567-8688."
                + "\n•	You may seek damages from violators. If a consumer reporting agency, or,"
                + " in some cases, a user of consumer reports or a furnisher    of information to "
                + " a consumer reporting agency violates the FCRA, you may be able to sue in state"
                + " or federal court.\n•	Identity theft victims and active duty military personnel have"
                + " additional rights. For more information, visit www.consumerfinance.gov/learnmore.";
            return str;
        }

        public string str23()
        {
            string str = string.Empty;
            str = " ";

            return str;
        }

        public string str3()
        {
            string str = string.Empty;
            str = "States may enforce the FCRA, and many states have their own consumer reporting "
                + "laws. In some cases, you may have more rights under state law. For more information,"
                + "contact your state or local consumer protection agency or your state Attorney"
                + "General. For information about your federal rights, contact: ";

            return str;
        }


        //public ActionResult DownloadCompanyCommonPDF(string FPathName)
        //{

        //    string FilePath = HttpContext.Server.MapPath(@"~/Resources/Upload/613NoticePdf/613NoticePdfFile.pdf");
        //    string fileext = System.IO.Path.GetExtension("613NoticePdf.pdf");
        //    string CType = "application/" + fileext.Replace(".", "");
        //    string strFileTitle = "613_Notice.pdf";
        //     return File(FilePath, CType, strFileTitle);


        //}
    }
}
