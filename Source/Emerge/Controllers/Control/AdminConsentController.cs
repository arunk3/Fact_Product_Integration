﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using System.Web.Security;


namespace Emerge.Controllers.Control
{
   public partial class ControlController : Controller
    {
        //
        // GET: /AdminConsentReview/

       public ActionResult PartialAdminConsent()
        {
            return View();
        }

       public ActionResult AdminConsentReviewBind(int page, int pageSize, string ReportType)
       {
           int PageSize = pageSize;
           int iPageNo = page;

           string Type = ReportType == "0" ? "1" : ReportType;

           BALEmergeReview ObjReview = new BALEmergeReview();
          
           

           var AdminConsentCollection = ObjReview.GetConsentPendingReports(0,
                                                                                              0,
                                                                                              0,
                                                                                              0,
                                                                                              "OrderDt",
                                                                                             "DESC",
                                                                                              true,
                                                                                              iPageNo,
                                                                                              PageSize,
                                                                                              string.Empty,
                                                                                              string.Empty,
                                                                                              string.Empty,
                                                                                              0, Convert.ToByte(Type));

           
               return Json(new
               {

                   Products = AdminConsentCollection?? new List<Proc_GetConsentPendingReportsResult>() ,
                   TotalCount = AdminConsentCollection.Count == 0 ? 0 : AdminConsentCollection.ElementAt(0).TotalRec

               },
                                 JsonRequestBehavior.AllowGet);
           
          

       }
       public ActionResult GetArchiveForAdminConsent(string ReportType)
       {
           string ArchiveType = string.Empty; string strArchiveLink = "Archive";
           if (ReportType == "1")
           {
               ArchiveType = "2";
               strArchiveLink = "Manual Reports";
           }
           else
           {
               ArchiveType = "1";
               strArchiveLink = "Archive";
           }
           return Json(new { Type = ArchiveType, ArchiveLink = strArchiveLink });
       }

       public ActionResult UpdateCompleteReview(int OrderId)
       {

           int ReviewStatus = 5;
           BALEmergeReview ObjReview = new BALEmergeReview();
           int pkOrderId = OrderId;
           ObjReview.UpdateReportReviewStatus(pkOrderId, ReviewStatus);

           return Json(new { });


       }



      

      


    }
}
