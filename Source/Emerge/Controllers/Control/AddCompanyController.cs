﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Net.Mime;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Security;
using System.Data.Linq;
using System.Web.Security;
using Emerge.Common;
using System.Text;
using System.Configuration;
using AuthorizeNet;
using System.Web.Routing;
using Emerge.CommonClasses;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        List<int> ObjCompLocation = new List<int>();
        //int editmode = 0;
        int objlocationid;

        public ActionResult AddCompanies()
        {
            AdCompanyModel1 objModel = new AdCompanyModel1();
            BALCompany objbalCompany = new BALCompany();
            List<SelectListItem> LocationList = new List<SelectListItem>();
            //List<SelectListItem> PermissiblePurposeList = new List<SelectListItem>();
            ViewBag.IsActive = 3;
            ViewBag.Date = DateTime.Now;
            //PermissiblePurposeList = GetpermissiblepurposeList();
            // When EDIT Company
            if (string.IsNullOrEmpty(Request.QueryString["Id1"]) == false)
            {
                objModel.IsEdit = true;
                ViewBag.locationid = int.Parse(Request.QueryString["Id1"]);
                var locationid1 = int.Parse(Request.QueryString["Id1"]);
                Session["pkLocationIdWhenEdit"] = int.Parse(Request.QueryString["Id1"]);
                //change in proc
                var list1 = objbalCompany.GetCompanyDetailByLocationId(locationid1);
                Session["pkCompanyIdWhenEdit"] = list1.ElementAt(0).fkCompanyId;
                ViewData["PkCompanyId"] = list1.ElementAt(0).fkCompanyId;
                Session["pk_LocationIdWhenpopup"] = int.Parse(Request.QueryString["Id1"]);
                Session["fk_companyid"] = list1.ElementAt(0).fkCompanyId;
                //int? fk_Companyid = (list1.ElementAt(0).fkCompanyId != null) ? (list1.ElementAt(0).fkCompanyId) : null;
               // var newrating = objModel.Rating;
                bool? IsActive = objbalCompany.GetLatestOrderDate((list1.ElementAt(0).fkCompanyId != null) ? (list1.ElementAt(0).fkCompanyId) : null);
                //string Company_Name = !string.IsNullOrEmpty(list1.ElementAt(0).CompanyName) ? list1.ElementAt(0).CompanyName : string.Empty;
                //string Email_id = !string.IsNullOrEmpty(list1.ElementAt(0).MainEmailId) ? list1.ElementAt(0).MainEmailId : string.Empty;
                if (IsActive != null)
                {
                    ViewBag.IsActive = IsActive;
                    if (IsActive == true)
                    {
                        ViewBag.IsActive = 1;
                    }
                    else
                    {
                        ViewBag.IsActive = 2;
                    }
                }
                objModel.Rating = list1.ElementAt(0).Rating.ToString();
                objModel.MemberSince = list1.ElementAt(0).CreatedDate;
                int? CompanyId = (list1.ElementAt(0).fkCompanyId != null) ? (list1.ElementAt(0).fkCompanyId) : null;
                //List<Proc_Get_CommentsForLeadResult> ObjDbCollection = new List<Proc_Get_CommentsForLeadResult>();
                objModel.CollGetCommentsForLeadResult = objbalCompany.GetCommentsForCompany(CompanyId);
                objModel.pkCompanyId = (int)CompanyId;
                objModel.pkPermissibleId = Convert.ToInt32(list1.ElementAt(0).PermissiblePurpose);
                List<LocationList> locations = objbalCompany.GetAllLocationsByCompanyId((int)CompanyId);
                if (locations.Count() > 0)
                {
                    LocationList = locations.Select(x => new SelectListItem { Text = x.LocationCity, Value = x.pkLocationId.ToString() }).ToList();
                    LocationList.Insert(0, new SelectListItem { Text = "---Select Location---", Value = "" });
                }
                objModel.FirstReview = list1.ElementAt(0).FirstReportRanDate == null ? "NA" : list1.ElementAt(0).FirstReportRanDate.ToString();
                objModel.LastReview = list1.ElementAt(0).LastReviewDate == null ? "NA" : list1.ElementAt(0).LastReviewDate.ToString();
                objModel.Review = list1.ElementAt(0).ReviewComments == null ? "NA" : list1.ElementAt(0).ReviewComments.ToString();
                objModel.ReviewComments = list1.ElementAt(0).ReviewComments == null ? "NA" : list1.ElementAt(0).ReviewComments.ToString();
                objModel.CompanyNote = list1.ElementAt(0).CompanyNote == null ? "NA" : list1.ElementAt(0).CompanyNote.ToString();
                objModel.pkCompanyTypeId = list1.ElementAt(0).fkCompanyTypeId;
                objModel.fkUserId = list1.ElementAt(0).fkSalesManagerId;
                objModel.fkSalesAssociateId = list1.ElementAt(0).fkSalesRepId;
                objModel.IsEnabled = list1.ElementAt(0).IsEnabled;
                objModel.CompanyName = list1.ElementAt(0).CompanyName;
                objModel.CompanyAccountNumber = list1.ElementAt(0).CompanyAccountNumber;
                objModel.Address1 = list1.ElementAt(0).Address1;
                objModel.fkCompanyTypeId = list1.ElementAt(0).fkCompanyTypeId;
                objModel.pkStateId = Convert.ToInt32(list1.ElementAt(0).fkStateID);
                objModel.State_Code = GetStateCode(objModel.pkStateId);
                objModel.IsReportStatus = list1.ElementAt(0).IsReportStatus;
                objModel.IsturnoffSSN = list1.ElementAt(0).IsturnoffSSN;
                objModel.isEnabledBillingLocation = Convert.ToBoolean(list1.ElementAt(0).isEnabledBillingLocation);

                objModel.IsDuplicateSSNReportAlertEnabled = Convert.ToBoolean(list1.ElementAt(0).IsDuplicateSSNReportAlertEnabled); //INT-219 Display SSN alert status for a company.
                try
                {
                    if (list1.ElementAt(0).IsCustomCity != null && Convert.ToBoolean(list1.ElementAt(0).IsCustomCity))
                    {
                        ViewBag.IsCustomCity = "1";
                        objModel.City = "-3";
                        objModel.CityName = !string.IsNullOrEmpty(list1.ElementAt(0).City) ? list1.ElementAt(0).City : string.Empty;
                    }
                    else
                    {
                        ViewBag.IsCustomCity = "2";
                        objModel.City = list1.ElementAt(0).City;
                    }
                }
                catch { }
                objModel.Address2 = !string.IsNullOrEmpty(list1.ElementAt(0).Address2) ? list1.ElementAt(0).Address2 : string.Empty;
                objModel.ZipCod = list1.ElementAt(0).ZipCode;
                objModel.MainContact = list1.ElementAt(0).MainContactPersonName;

                objModel.MainEmail = list1.ElementAt(0).MainEmailId;
                objModel.MainFaxNumber = list1.ElementAt(0).Fax;
                objModel.MainPhone = list1.ElementAt(0).PhoneNo1;

                objModel.BillingContact = list1.ElementAt(0).BillingContactPersonName;
                objModel.BillingEmail = list1.ElementAt(0).BillingEmailId;
                objModel.BillingPhone = list1.ElementAt(0).PhoneNo2;
                objModel.BillingFax = list1.ElementAt(0).Fax2;
                objModel.ReportEmailNotification = list1.ElementAt(0).ReportEmailAddress;
                objModel.IsReportEmailActivated = (list1.ElementAt(0).IsReportemailActivated != null) ? bool.Parse(list1.ElementAt(0).IsReportemailActivated.ToString()) : false;
                objModel.Rating = list1.ElementAt(0).Rating.ToString();
                objModel.PaymentTerm = list1.ElementAt(0).PaymentTerm;
                objModel.ObjCompLocationId = list1.ElementAt(0).pkLocationId != 0 ? (int)list1.ElementAt(0).pkLocationId : 0;
                objModel.IsHome = (list1.ElementAt(0).IsHome != null) ? Convert.ToBoolean(list1.ElementAt(0).IsHome) : false;
                objModel.IsEnableAPI = list1.ElementAt(0).EnableAPI;
                objModel.IsIntegration = list1.ElementAt(0).Integration;
                objModel.APIPassword = list1.ElementAt(0).APIPassword;
                objModel.APIUsername = list1.ElementAt(0).APIUsername;
                objModel.APIPaymentTerms = list1.ElementAt(0).PaymentTerms;
                objModel.CompanyLogo = list1.ElementAt(0).CompanyLogo;
                objModel.APILocation = list1.ElementAt(0).APILocation != 0 ? list1.ElementAt(0).APILocation : 0;
                objModel.APILocationList = LocationList;
                objModel.IsGlobalCredential = list1.ElementAt(0).IsGlobalCredential;
                objModel.SecondaryFax = list1.ElementAt(0).SecondaryFax;
                objModel.SecondaryPhone = list1.ElementAt(0).SecondaryPhone;
                Session["Main_contactPerson"] = list1.ElementAt(0).MainContactPersonName;
                Session["Main_ContactaEmail"] = list1.ElementAt(0).MainEmailId;
                Session["Billing_contactPerson"] = list1.ElementAt(0).BillingContactPersonName;
                Session["Billing_ContactactEmail"] = list1.ElementAt(0).BillingEmailId;
                objModel.ApIUrl = !string.IsNullOrEmpty(list1.ElementAt(0).ApiUrl) ? list1.ElementAt(0).ApiUrl : string.Empty;
                objModel.Is7YearFilter = list1.ElementAt(0).Is7YearFilter;
                //#for int-9
                objModel.isfcrjurisdictionfee = (list1.ElementAt(0).isfcrjurisdictionfee != null ? Convert.ToBoolean(list1.ElementAt(0).isfcrjurisdictionfee) : false);

                objModel.Is7YearAuto = list1.ElementAt(0).Is7YearAutoCheck;


                if (objModel.CompanyName != string.Empty || objModel.CompanyName != null)
                {
                    string Companynamevar = objModel.CompanyName;
                    while (Companynamevar.IndexOf(" ") >= 0)
                    {
                        Companynamevar = Companynamevar.Replace(" ", "");
                    }
                    if (objModel.ApIUrl == null || objModel.ApIUrl == string.Empty)
                    {
                        objModel.ApIUrl = Companynamevar.ToLower();
                    }

                }
                ViewBag.LocationId = objModel.ObjCompLocationId;
                ViewBag.CompanyId = CompanyId;
                ViewBag.Status = list1.ElementAt(0).LeadStatus;
                objModel.IsPDFShow = true;
                var tbldocumts = objbalCompany.GetAllCompanyDocumentsByCompanyId((int)CompanyId);
                objModel.CompnayDocList = objbalCompany.GetAllCompanyDocumentsByCompanyId((int)CompanyId);
                ViewData["Documents"] = tbldocumts;
                ViewBag.IsEnabled = objbalCompany.IsEnableCompany((int)CompanyId);

                if (Request.QueryString["Status"] != null)
                {
                    int status = Convert.ToInt32(Request.QueryString["Status"]);


                    if (status == 1)
                    {
                        objModel.Message = "<span class='successmsg'> Document  updated successfully </span>";
                    }
                    else if (status == 2)
                    {
                        objModel.Message = "<span class='successmsg'> Document added successfully </span>";
                    }
                    else if (status == 3)
                    {
                        objModel.Message = "<span class='successmsg'> Document deleted successfully </span>";
                    }

                    else if (status == -1)
                    {
                        objModel.Message = "<span class='error'>Title name already exist</span>";
                    }
                    else
                    {
                        objModel.Message = "<span class='error'>error is occured while updating Documents</span>";
                    }

                }

            }
            // When Add New Company
            else
            {
                BALCompany serobj = new BALCompany();
                objModel.IsEdit = false;
                objModel.IsPDFShow = false;
                var Accountnumber = serobj.GetMaxAccountNumber();
                objModel.CompanyAccountNumber = Accountnumber.ToString();
                ViewBag.LocationId = 0;
                ViewBag.CompanyId = 0;
                objModel.pkCompanyId = 0;
                LocationList.Insert(0, new SelectListItem { Text = "---Select Location---", Value = "" });
                objModel.APILocation = 0;
                objModel.APILocationList = LocationList;
                objModel.IsEnabled = true;
                objModel.IsGlobalCredential = true;
                objModel.ApIUrl = string.Empty;
                objModel.SecondaryFax = string.Empty;
                objModel.SecondaryPhone = string.Empty;
            }
            Session["CompanyLogo"] = null;
            if (!string.IsNullOrEmpty(Request.QueryString["_ac"]))
            {
                string Action = Request.QueryString["_ac"];
                switch (Action)
                {
                    case "2":
                        ViewBag.Message = "<span class='successmsg'>Company added successfully</span>";
                        break;
                    case "1":
                        ViewBag.Message = "<span class='successmsg'>Company updated successfully</span>";
                        break;
                    case "-1":
                        ViewBag.Message = "<span class='errormsg'>Some error occurred</span>";
                        break;
                    case "-4":
                        ViewBag.Message = "<span class='errormsg'>Company Account no already exists</span>";
                        break;
                    case "-5":
                        ViewBag.Message = "<span class='errormsg'>API Username already exists.</span>";
                        break;
                }
            }



            return View(objModel);
        }

        //ND-22

        public ActionResult BindDocument()
        {
            int pkLocationId = 0;
            AdCompanyModel1 objModel = new AdCompanyModel1();
            if (string.IsNullOrEmpty(Request.QueryString["Id1"]) == false)
            {
                pkLocationId = Convert.ToInt32(Request.QueryString["Id1"]);
            }
            CommonPageload(objModel, pkLocationId);
            BALCompany objbalCompany = new BALCompany();
            if (pkLocationId != 0)
            {
                var list1 = objbalCompany.GetCompanyDetailByLocationId(pkLocationId);

                int? fk_Companyid = (list1.ElementAt(0).fkCompanyId != null) ? (list1.ElementAt(0).fkCompanyId) : null;


                objModel.CompnayDocList = objbalCompany.GetAllCompanyDocumentsByCompanyId((int)fk_Companyid);
            }
            return Json(objModel);
        }

        //

        public ActionResult LocationByCompanyId()
        {
            int pkLocationId = 0;
            AdCompanyModel1 objModel = new AdCompanyModel1();
            if (Convert.ToInt32(Request.QueryString["Id1"]) == 0)
            {
                pkLocationId = Convert.ToInt32(Request.QueryString["Id1"]);
            }
            CommonPageload(objModel, pkLocationId);
            return View(objModel);
        }

        public ActionResult UsersByCompanyId()
        {
            int pkLocationId = 0;
            AdCompanyModel1 objModel = new AdCompanyModel1();
            CommonPageload(objModel, pkLocationId);

            return View(objModel);
        }

        public ActionResult ReferenceByCompanyId()
        {
            int pkLocationId = 0;
            AdCompanyModel1 objModel = new AdCompanyModel1();
            CommonPageload(objModel, pkLocationId);

            return View(objModel);
        }
        public ActionResult DocumentsByCompanyId(FormCollection frm)
        {
            int pkLocationId = 0;
            AdCompanyModel1 objModel = new AdCompanyModel1();
            if (string.IsNullOrEmpty(Request.QueryString["Id1"]) == false)
            {
                pkLocationId = Convert.ToInt32(Request.QueryString["Id1"]);
                Session["hiddenLocation"] = pkLocationId;
            }
            else
            {
                pkLocationId = Convert.ToInt32(Session["hiddenLocation"]);
            }
            CommonPageload(objModel, pkLocationId);
            BALCompany objbalCompany = new BALCompany();
            if (pkLocationId != 0)
            {
                var list1 = objbalCompany.GetCompanyDetailByLocationId(pkLocationId);

                int? fk_Companyid = (list1.ElementAt(0).fkCompanyId != null) ? (list1.ElementAt(0).fkCompanyId) : null;


                objModel.CompnayDocList = objbalCompany.GetAllCompanyDocumentsByCompanyId((int)fk_Companyid);
            }
            return View(objModel);
        }
        public ActionResult APIByCompanyId()
        {

            int pkLocationId = 0;

            List<SelectListItem> LocationList = new List<SelectListItem>();
            BALCompany objbalCompany = new BALCompany();
            AdCompanyModel1 objModel = new AdCompanyModel1();
            if (string.IsNullOrEmpty(Request.QueryString["Id1"]) == false)
            {
                pkLocationId = Convert.ToInt32(Request.QueryString["Id1"]);
                TempData["LocationID"] = pkLocationId;
            }
            CommonPageload(objModel, pkLocationId);
            if (pkLocationId != null && pkLocationId != 0)
            {
                var list1 = objbalCompany.GetCompanyDetailByLocationId(pkLocationId);
                int? fk_Companyid = (list1.ElementAt(0).fkCompanyId != null) ? (list1.ElementAt(0).fkCompanyId) : null;

                ViewData["PkCompanyId"] = fk_Companyid;

                List<LocationList> locations = objbalCompany.GetAllLocationsByCompanyId((int)fk_Companyid);
                if (locations.Count() > 0)
                {
                    LocationList = locations.Select(x => new SelectListItem { Text = x.LocationCity, Value = x.pkLocationId.ToString() }).ToList();
                    LocationList.Insert(0, new SelectListItem { Text = "---Select Location---", Value = "" });
                }
                objModel.IsEnableAPI = list1.ElementAt(0).EnableAPI;
                objModel.IsIntegration = list1.ElementAt(0).Integration;
                objModel.APIPassword = list1.ElementAt(0).APIPassword;
                objModel.APIUsername = list1.ElementAt(0).APIUsername;
                objModel.APIPaymentTerms = list1.ElementAt(0).PaymentTerms;
                objModel.CompanyLogo = list1.ElementAt(0).CompanyLogo;
                objModel.APILocation = list1.ElementAt(0).APILocation != 0 ? list1.ElementAt(0).APILocation : 0;
                objModel.APILocationList = LocationList;
                objModel.ApIUrl = !string.IsNullOrEmpty(list1.ElementAt(0).ApiUrl) ? list1.ElementAt(0).ApiUrl : string.Empty;
                int fkReportCategoryId = 10;
                string Company_Id = fk_Companyid.ToString();
                var ReportList = new List<tblthirdpartycredential>();
                ReportList = objbalCompany.GetThirdPartyDetailsByCompanyId(fkReportCategoryId, Company_Id);
                ViewData["exist"] = "0";
                if (ReportList != null) { ViewData["exist"] = "1"; }


            }



            return View(objModel);
        }

        // INT-106
        public ActionResult BindAPILocationsByCompanyId()
        {
            int pkLocationId = 0;
            BALCompany objbalCompany = new BALCompany();
            //List<SelectListItem> LocationList = new List<SelectListItem>();
            pkLocationId = Convert.ToInt32(TempData["LocationID"]);
            var list1 = objbalCompany.GetCompanyDetailByLocationId(pkLocationId);
            int? fk_Companyid = (list1.ElementAt(0).fkCompanyId != null) ? (list1.ElementAt(0).fkCompanyId) : null;

            List<string> result = new List<string>();
            List<LocationList> locations = objbalCompany.GetAllLocationsByCompanyId((int)fk_Companyid);
            string Content = string.Empty;
            using (EmergeDALDataContext _EmergeDALDataContext = new EmergeDALDataContext())
            {
                var location = (from tc in _EmergeDALDataContext.tblCompanies
                                where tc.pkCompanyId == fk_Companyid
                                select tc.APILocations).FirstOrDefault();
                
                Content = "<select  onchange='GetSelectedValues();' onclick='RemoveHomeLocation()' name='user_id12' class='divisions_dd'  id='OtherLocationId'  multiple='multiple'>";
                if (location != null)
                {
                    result = location.Split(',').ToList();

                    for (int j = 0; j <= locations.Count - 1; j++)
                    {

                        for (int i = 0; i <= result.Count - 1; i++)
                        {
                            if (locations[j].pkLocationId.ToString() == result[i].ToString())
                            {
                                Content += "<option selected=1  value='" + locations[j].pkLocationId.ToString() + "'>'" + locations[j].LocationCity.ToString() + "'</option>";
                            }
                            else
                            {
                                if (!Content.Contains(locations[j].pkLocationId.ToString()))
                                {
                                    Content += "<option value='" + locations[j].pkLocationId.ToString() + "'>'" + locations[j].LocationCity.ToString() + "'</option>";
                                }
                            }


                        }

                    }
                    Content += "</select>";
                }
                else
                {
                    for (int i = 0; i < locations.Count; i++)
                    {
                        Content += "<option select=1  value='" + locations.ElementAt(i).pkLocationId + "'>'" + locations.ElementAt(i).LocationCity + "'</option>";
                    }
                    Content += "</select>";
                }
            }
            return Json(new { ResultList = Content });
        }
        // INT-106

        public ActionResult PriceByCompanyId()
        {
            int pkLocationId = 0;
            AdCompanyModel1 objModel = new AdCompanyModel1();
            CommonPageload(objModel, pkLocationId);

            return View(objModel);
        }
        public ActionResult SalesByCompanyId()
        {
            int pkLocationId = 0;
            AdCompanyModel1 objModel = new AdCompanyModel1();
            CommonPageload(objModel, pkLocationId);
            if (string.IsNullOrEmpty(Request.QueryString["Id1"]) == false)
            {
                var locationid1 = Int32.Parse(Request.QueryString["Id1"]);
                BALCompany objbalCompany = new BALCompany();
                var list1 = objbalCompany.GetCompanyDetailByLocationId(locationid1);
                LoadSalesGraph(list1.ElementAt(0).fkCompanyId.GetValueOrDefault());
                LoadVolumeGraph(list1.ElementAt(0).fkCompanyId.GetValueOrDefault());
            }
            return View(objModel);
        }
        public ActionResult InvoiceByCompanyId()
        {
            int pkLocationId = 0;
            AdCompanyModel1 objModel = new AdCompanyModel1();
            CommonPageload(objModel, pkLocationId);

            return View(objModel);
        }
        public void CommonPageload(AdCompanyModel1 objModel, int pkLocationId)
        {
            BALCompany objbalCompany = new BALCompany();
            List<SelectListItem> LocationList = new List<SelectListItem>();
            //List<SelectListItem> PermissiblePurposeList = new List<SelectListItem>();
            ViewBag.IsActive = 3;
            ViewBag.Date = DateTime.Now;
            if (string.IsNullOrEmpty(Request.QueryString["Id1"]) == false)
            {
                objModel.IsEdit = true;
                ViewBag.locationid = int.Parse(Request.QueryString["Id1"]);
                var locationid1 = int.Parse(Request.QueryString["Id1"]);
                var list1 = objbalCompany.GetCompanyDetailByLocationId(locationid1);
                int fk_Companyid = list1.ElementAt(0).fkCompanyId != 0 ? (int)list1.ElementAt(0).fkCompanyId : 0;
                //var newrating = objModel.Rating;
                bool? IsActive = objbalCompany.GetLatestOrderDate((list1.ElementAt(0).fkCompanyId != null) ? (list1.ElementAt(0).fkCompanyId) : null);
                //string Company_Name = !string.IsNullOrEmpty(list1.ElementAt(0).CompanyName) ? list1.ElementAt(0).CompanyName : string.Empty;
                //string Email_id = !string.IsNullOrEmpty(list1.ElementAt(0).MainEmailId) ? list1.ElementAt(0).MainEmailId : string.Empty;
                if (IsActive != null)
                {
                    ViewBag.IsActive = IsActive;
                    if (IsActive == true)
                    {
                        ViewBag.IsActive = 1;
                    }
                    else
                    {
                        ViewBag.IsActive = 2;
                    }
                }
                int? CompanyId = (list1.ElementAt(0).fkCompanyId != null) ? (list1.ElementAt(0).fkCompanyId) : null;
                //List<Proc_Get_CommentsForLeadResult> ObjDbCollection = new List<Proc_Get_CommentsForLeadResult>();
                objModel.CollGetCommentsForLeadResult = objbalCompany.GetCommentsForCompany(CompanyId);
                objModel.pkCompanyId = (int)CompanyId;
                objModel.CompanyName = list1.ElementAt(0).CompanyName;
                objModel.pkStateId = Convert.ToInt32(list1.ElementAt(0).fkStateID);
                objModel.State_Code = GetStateCode(objModel.pkStateId);
                Session["NewPkCompanyId"] = fk_Companyid;
                Session["pkCompLocationId"] = int.Parse(Request.QueryString["Id1"]);
                objModel.IsGlobalCredential = list1.ElementAt(0).IsGlobalCredential;
                #region GetCompanyStatausByCompanyId


                BALCompany ObjBalCompany = new BALCompany();
                var GetComapnyStatus = ObjBalCompany.GetCompanyStatausByCompanyId(fk_Companyid);
                string RtnCompanyStatus = string.Empty;


                if (GetComapnyStatus.Count() > 0)
                {
                    if (GetComapnyStatus.ElementAt(0).LeadStatus.ToString() == "8")
                    {
                        RtnCompanyStatus = "8";
                    }
                    else if (GetComapnyStatus.ElementAt(0).LeadStatus.ToString() == "9")
                    {
                        RtnCompanyStatus = "9";
                    }
                    else if (GetComapnyStatus.ElementAt(0).IsEnabled == false)
                    {
                        RtnCompanyStatus = "7";
                    }
                    else if (GetComapnyStatus.ElementAt(0).IsEnabled == true && GetComapnyStatus.ElementAt(0).IsActive.ToString() == "1")
                    {
                        RtnCompanyStatus = "6";
                    }
                    else if (GetComapnyStatus.ElementAt(0).IsEnabled == true && GetComapnyStatus.ElementAt(0).IsActive.ToString() == "0")
                    {
                        RtnCompanyStatus = "10";
                    }
                }
                else
                {
                    RtnCompanyStatus = "-1";
                }

                ViewBag.MilestoneStatus = RtnCompanyStatus;


                #endregion
                try
                {
                    if (list1.ElementAt(0).IsCustomCity != null && Convert.ToBoolean(list1.ElementAt(0).IsCustomCity))
                    {
                        ViewBag.IsCustomCity = "1";
                        objModel.City = "-3";
                        objModel.CityName = !string.IsNullOrEmpty(list1.ElementAt(0).City) ? list1.ElementAt(0).City : string.Empty;
                    }
                    else
                    {
                        ViewBag.IsCustomCity = "2";
                        objModel.City = list1.ElementAt(0).City;
                    }
                }
                catch { }
                objModel.ObjCompLocationId = list1.ElementAt(0).pkLocationId != 0 ? (int)list1.ElementAt(0).pkLocationId : 0;
                if (objModel.CompanyName != string.Empty || objModel.CompanyName != null)
                {
                    string Companynamevar = objModel.CompanyName;
                    while (Companynamevar.IndexOf(" ") >= 0)
                    {
                        Companynamevar = Companynamevar.Replace(" ", "");
                    }
                    if (objModel.ApIUrl == null || objModel.ApIUrl == string.Empty)
                    {
                        objModel.ApIUrl = Companynamevar.ToLower();
                    }

                }
                ViewBag.LocationId = objModel.ObjCompLocationId;
                ViewBag.CompanyId = CompanyId;
                ViewBag.Status = list1.ElementAt(0).LeadStatus;
                ViewBag.IsEnabled = objbalCompany.IsEnableCompany((int)CompanyId);

                if (Request.QueryString["Status"] != null)
                {
                    int status = Convert.ToInt32(Request.QueryString["Status"]);


                    if (status == 1)
                    {
                        objModel.Message = "<span class='successmsg'> Document  updated successfully </span>";
                    }
                    else if (status == 2)
                    {
                        objModel.Message = "<span class='successmsg'> Document added successfully </span>";
                    }
                    else if (status == 3)
                    {
                        objModel.Message = "<span class='successmsg'> Document deleted successfully </span>";
                    }

                    else if (status == -1)
                    {
                        objModel.Message = "<span class='error'>Title name already exist</span>";
                    }
                    else
                    {
                        objModel.Message = "<span class='error'>error is occured while updating Documents</span>";
                    }

                }

            }
            // When Add New Company
            else
            {
                BALCompany serobj = new BALCompany();
                objModel.IsEdit = false;
                objModel.IsPDFShow = false;
                var Accountnumber = serobj.GetMaxAccountNumber();
                objModel.CompanyAccountNumber = Accountnumber.ToString();
                ViewBag.LocationId = 0;
                ViewBag.CompanyId = 0;
                objModel.pkCompanyId = 0;
                LocationList.Insert(0, new SelectListItem { Text = "---Select Location---", Value = "" });
                objModel.APILocation = 0;
                objModel.APILocationList = LocationList;
                objModel.IsEnabled = true;
                objModel.IsGlobalCredential = true;
                objModel.ApIUrl = string.Empty;
                objModel.SecondaryFax = string.Empty;
                objModel.SecondaryPhone = string.Empty;
                //objModel.PermissiblePurpose = "-1";
                //objModel.PermissiblePurposeList = PermissiblePurposeList.Select(x => new SelectListItem { Text = x.Text.ToString(), Value = x.Value.ToString() }).ToList(); 
                //objModel.PermissiblePurposeList = objModel.PermissiblePurposeList.Select(x => new SelectListItem { Text = x.Text.ToString(), Value = x.Value.ToString(), Selected = (x.Value == objModel.PermissiblePurpose) }).ToList();
            }
            Session["CompanyLogo"] = null;
            if (!string.IsNullOrEmpty(Request.QueryString["_ac"]))
            {
                string Action = Request.QueryString["_ac"];
                switch (Action)
                {
                    case "2":
                        ViewBag.Message = "<span class='successmsg'>Company added successfully</span>";
                        break;
                    case "1":
                        ViewBag.Message = "<span class='successmsg'>Company updated successfully</span>";
                        break;
                    case "-1":
                        ViewBag.Message = "<span class='errormsg'>Some error occurred</span>";
                        break;
                    case "-4":
                        ViewBag.Message = "<span class='errormsg'>Company Account no already exists</span>";
                        break;
                    case "-5":
                        ViewBag.Message = "<span class='errormsg'>API Username already exists.</span>";
                        break;
                }
            }
        }



        private List<SelectListItem> GetpermissiblepurposeList()
        {
            List<SelectListItem> listpermissiblepurpose = new List<SelectListItem>();
            //listpermissiblepurpose.Add(new SelectListItem { Text = "--Select--", Value = "-1" });
            listpermissiblepurpose.Add(new SelectListItem { Text = "Employment Screening", Value = "Employment Screening" });
            listpermissiblepurpose.Add(new SelectListItem { Text = "Credit Reporting", Value = "Credit Reporting" });
            listpermissiblepurpose.Add(new SelectListItem { Text = "Insurance Underwriting", Value = "Insurance Underwriting" });
            listpermissiblepurpose.Add(new SelectListItem { Text = "Tenant Screening", Value = "Tenant Screening" });
            listpermissiblepurpose.Add(new SelectListItem { Text = "Debt Recovery / Collections", Value = "Debt Recovery / Collections" });
            listpermissiblepurpose.Add(new SelectListItem { Text = "Other", Value = "Other" });
            return listpermissiblepurpose;
        }

        public ActionResult Getpermissiblepurpose()
        {
            BALCompanyType serboj = new BALCompanyType();
            var list = serboj.GetPermissiblePurpose();
            return Json(list);
        }


        public List<int> AddCompany(AdCompanyModel1 objmodel)
        {
            //string Companynameurl = objmodel.ApIUrl;
            //   RouteTable.Routes.Remove(RouteTable.Routes[Companynameurl]);

            //   RouteCollection routes = new RouteCollection();
            //   string CompanyName = objmodel.ApIUrl;
            //   routes.MapRoute(
            //  name: CompanyName,
            //  url: CompanyName,
            //  defaults: new { Controller = "RunReport", Action = "Index", id = UrlParameter.Optional }
            //);



            //Guid CompanyUserId;
            tblCompany objtblCompany = new tblCompany();
            tblLocation objtbllocation = new tblLocation();
            objtblCompany.CompanyName = objmodel.CompanyName;
            objtblCompany.fkCompanyTypeId = objmodel.pkCompanyTypeId;
            objtblCompany.CompanyAccountNumber = objmodel.CompanyAccountNumber;
            objtblCompany.CreatedDate = DateTime.Now;
            //Ticket #27
            objtblCompany.fkSalesRepId = objmodel.fkUserId;


            //objtblCompany.fkSalesRepId = objmodel.fkSalesAssociateId;
            objtblCompany.BillingContactPersonName = objmodel.BillingContact;
            objtblCompany.BillingEmailId = objmodel.BillingEmail;
            objtblCompany.fkApplicationId = Utility.SiteApplicationId;
            objtblCompany.FranchiseName = " ";
            objtblCompany.fkSalesManagerId = objmodel.fkUserId;
            objtblCompany.IsEnabled = objmodel.IsEnabled;
            objtblCompany.MainEmailId = objmodel.MainEmail;
            objtblCompany.MainContactPersonName = objmodel.MainContact;
            objtblCompany.BillingContactPersonName = objmodel.BillingContact;
            objtblCompany.IsDeleted = false;
            objtblCompany.LastModifiedDate = DateTime.Now;
            objtblCompany.EnableAPI = objmodel.IsEnableAPI;
            objtblCompany.Integration = objmodel.IsIntegration;
            objtblCompany.APIUsername = objmodel.APIUsername != null ? objmodel.APIUsername : string.Empty;
            objtblCompany.APIPassword = objmodel.APIPassword != null ? objmodel.APIPassword : string.Empty;
            objtblCompany.PaymentTerms = objmodel.APIPaymentTerms != null ? objmodel.APIPaymentTerms : string.Empty;
            objtblCompany.CompanyLogo = objmodel.CompanyLogo != null ? objmodel.CompanyLogo : string.Empty;

            objtblCompany.APILocation = objmodel.APILocation;
            objtblCompany.PermissiblePurpose = objmodel.pkPermissibleId;
            objtblCompany.ApiUrl = objmodel.ApIUrl != null ? objmodel.ApIUrl : string.Empty;
            objtblCompany.Is7YearFilter = objmodel.Is7YearFilter;
            objtblCompany.Is7YearAutoCheck = objmodel.Is7YearAuto;

            //#int-9
            objtblCompany.isfcrjurisdictionfee = objmodel.isfcrjurisdictionfee;
            //INT-219 adding the SSN alert value to tblcompany
            objtblCompany.IsDuplicateSSNReportAlertEnabled = objmodel.IsDuplicateSSNReportAlertEnabled;
            ObjBALCompanyUsers = new BALCompanyUsers();
            var dbCollection = ObjBALCompanyUsers.GetUserInformationByMembershipUserId(new Guid(Membership.GetUser(HttpContext.User.Identity.Name).ProviderUserKey.ToString()));
            objtblCompany.LastModifiedById = dbCollection.ElementAt(0).fkUserId;

            objtblCompany.CreatedById = dbCollection.ElementAt(0).fkUserId;

            objtblCompany.IsEnableReview = true;
            objtblCompany.CompanyNote = objmodel.CompanyNote;
            if (objmodel.PaymentTerm != 0)
            {
                objtblCompany.PaymentTerm = objmodel.PaymentTerm;
            }
            objtblCompany.CompanyCode = Utility.GenerateCompanyCode(objmodel.CompanyName, DateTime.Now);
            objtblCompany.Rating = !string.IsNullOrEmpty(objmodel.Rating) ? byte.Parse(objmodel.Rating) : byte.Parse("0");
            objtblCompany.ReviewComments = " ";
            objtblCompany.ReportEmailAddress = objmodel.ReportEmailNotification;


            objtblCompany.LastReviewDate = DateTime.Now;

            if (objmodel.IsReportEmailActivated == true)
            {
                objtblCompany.ReportEmailAddress = objmodel.ReportEmailNotification;
                objtblCompany.IsReportEmailActivated = true;

            }
            else
            {
                objtblCompany.ReportEmailAddress = string.Empty;
                objtblCompany.IsReportEmailActivated = false;
            }



            objtbllocation.Address1 = objmodel.Address1;
            objtbllocation.Address2 = !string.IsNullOrEmpty(objmodel.Address2) ? objmodel.Address2 : string.Empty;
            objtbllocation.IsReportEmailActivated = objmodel.IsReportEmailActivated;
            objtbllocation.City = objmodel.City;

            objtbllocation.CreatedById = objmodel.fkUserId;
            objtbllocation.CreatedDate = DateTime.Now;
            objtbllocation.Fax = !string.IsNullOrEmpty(objmodel.MainFaxNumber) ? objmodel.MainFaxNumber : string.Empty; ;
            objtbllocation.fkStateID = objmodel.pkStateId;
            objtbllocation.LocationCode = Utility.GenerateLocationCode(objmodel.CompanyName, currentDate);
            objtbllocation.IsEnabled = objmodel.IsEnabled;
            objtbllocation.IsHome = true;
            objtbllocation.PhoneNo1 = objmodel.MainPhone;
            objtbllocation.Ext1 = " ";
            objtbllocation.Ext2 = " ";
            objtbllocation.LocationNote = " ";
            objtbllocation.IsSyncpod = false;
            objtbllocation.LocationEmail = objmodel.BillingEmail;

            objtbllocation.PhoneNo2 = !string.IsNullOrEmpty(objmodel.BillingPhone) ? objmodel.BillingPhone : string.Empty;
            objtbllocation.Fax2 = !string.IsNullOrEmpty(objmodel.BillingFax) ? objmodel.BillingFax : string.Empty;
            //objtbllocation.IsCustomCity = true;
            if (objmodel.City.ToString() != "-2" && objmodel.City.ToString() != "-3")
            {
                objtbllocation.City = objmodel.City.ToString();
                objtbllocation.IsCustomCity = false;
            }
            else
            {
                objtbllocation.City = !string.IsNullOrEmpty(objmodel.CityName) ? objmodel.CityName : string.Empty;
                objtbllocation.IsCustomCity = true;
            }
            objtbllocation.LocationContact = objmodel.MainContact;
            objtbllocation.ZipCode = objmodel.ZipCod;
            objtbllocation.SecondaryPhone = !string.IsNullOrEmpty(objmodel.SecondaryPhone) ? objmodel.SecondaryPhone : string.Empty;
            objtbllocation.SecondaryFax = !string.IsNullOrEmpty(objmodel.SecondaryFax) ? objmodel.SecondaryFax : string.Empty;

            Dictionary<string, string> DicDocument = Session["Document"] as Dictionary<string, string>;
            tblCompanyDocument objtbldocuments = new tblCompanyDocument();

            // #19
            // objtbldocuments.pkCompanyDocumentId = Guid.NewGuid();


            objtbldocuments.CreatedDate = DateTime.Now;
            objtbldocuments.DocumentTitle = DicDocument.ElementAt(0).Key;
            objtbldocuments.DocumentName = DicDocument.ElementAt(0).Value;
            objtbldocuments.IsUserAgreement = true;



            BALCompany serobj = new BALCompany();
            ObjCompLocation = serobj.AddNewCompany(objtblCompany, objtbllocation, objtbldocuments);
            List<int> CompanyIdAndLocationId = ObjCompLocation;

            int pkcomapnyid;

            pkcomapnyid = (int)CompanyIdAndLocationId[0];
            var _leadStatus = 1;

            if (int.Parse(CompanyIdAndLocationId[1].ToString()) != null)
            {
               // MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
                Guid UserId = Utility.GetUID(User.Identity.Name);
                AddStatusToCallLog(pkcomapnyid, UserId, "##" + _leadStatus + "##", DateTime.Now);
                string btn = "imgbtn_4";
                Update_Call_Log(btn, pkcomapnyid);

            }


            return ObjCompLocation;


        }

        public void Update_Call_Log(string btnName, int? fkCompanyid)
        {
            Guid UserId;
            int pkCompanyId;

            MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));
            if (ObjMembershipUser != null)
            {
                UserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
                pkCompanyId = (int)fkCompanyid;
                string LeadComments = "";
                string[] Btnstr = btnName.Split('_');
                switch (Btnstr[1].ToLower())
                {
                    case "1": LeadComments = "##1##"; break;
                    case "2": LeadComments = "##2##"; break;
                    case "3": LeadComments = "##3##"; break;
                    case "4": LeadComments = "##4##"; break;
                    case "5": LeadComments = "##5##"; break;
                }
                DateTime CreatedDate = System.DateTime.Now;
                Add_StatusToCallLog(pkCompanyId, UserId, LeadComments, CreatedDate);
            }
            //PageLoadBind();
            //BindLeadComments();
        }




        /// <summary>
        /// Function To Update Company 
        /// </summary>
        public int UpdateCompany(AdCompanyModel1 objmodel)
        {
            //BALCompanyType objnewBALCompanyType = new BALCompanyType();
            //int pkCompanyId = objmodel.pkCompanyId;
           // string Companynameurl = objnewBALCompanyType.GetAlreadyexistapiurl(pkCompanyId);
            tblCompany ObjtblCompany = new tblCompany();
            tblLocation ObjtblLocation = new tblLocation();

            BALCompany ObjBALCompany = new BALCompany();
           // BALLocation ObjBALLocation = new BALLocation();
            int Result = 0;
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();

            //string ext1 = objmodel.MainPhone;
            //string ext2 = objmodel.BillingPhone;

            try
            {
                ObjtblCompany.pkCompanyId = objmodel.pkCompanyId;
                ObjtblCompany.BillingContactPersonName = objmodel.BillingContact;
                ObjtblCompany.BillingEmailId = objmodel.BillingEmail;
                ObjtblCompany.CompanyName = objmodel.CompanyName;
                ObjtblCompany.fkCompanyTypeId = objmodel.pkCompanyTypeId;
                ObjtblCompany.IsDeleted = false;
                ObjtblCompany.IsEnabled = objmodel.IsEnabled;
                ObjtblCompany.fkApplicationId = Utility.SiteApplicationId;
                ObjtblCompany.MainContactPersonName = objmodel.MainContact;
                ObjtblCompany.MainEmailId = objmodel.MainEmail;
                ObjtblCompany.FranchiseName = string.Empty;
                if (objmodel.PaymentTerm != 0)
                {
                    ObjtblCompany.PaymentTerm = Convert.ToByte(objmodel.PaymentTerm);
                }

                ObjtblCompany.CompanyAccountNumber = objmodel.CompanyAccountNumber;

                //Ticket #27
                ObjtblCompany.fkSalesRepId = objmodel.fkUserId;

                // ObjtblCompany.fkSalesRepId = objmodel.fkSalesAssociateId;
                ObjtblCompany.fkSalesManagerId = objmodel.fkUserId;

                var dbCollection = ObjBALCompanyUsers.GetUserInformationByMembershipUserId(new Guid(Membership.GetUser(HttpContext.User.Identity.Name).ProviderUserKey.ToString()));
                ObjtblCompany.LastModifiedById = dbCollection.ElementAt(0).fkUserId;
                ObjtblCompany.LastModifiedDate = System.DateTime.Now;
                ObjtblCompany.CompanyNote = objmodel.CompanyNote;
                ObjtblCompany.IsReportStatus = objmodel.IsReportStatus;
                ObjtblCompany.IsturnoffSSN = objmodel.IsturnoffSSN;
                ObjtblCompany.isEnabledBillingLocation = objmodel.isEnabledBillingLocation;

                if (objmodel.IsReportEmailActivated)
                {
                    ObjtblCompany.ReportEmailAddress = objmodel.ReportEmailNotification;
                    ObjtblCompany.IsReportEmailActivated = objmodel.IsReportEmailActivated;
                }
                else
                {
                    ObjtblCompany.ReportEmailAddress = string.Empty;
                    ObjtblCompany.IsReportEmailActivated = false;
                }
                ObjtblCompany.Rating = Convert.ToByte(objmodel.Rating);
                if (objmodel.Review != null)
                {
                    if (objmodel.Review.ToLower() != "na")
                    {
                        if (objmodel.Review.ToLower() != objmodel.Review.ToLower())
                        {
                            ObjtblCompany.ReviewComments = objmodel.Review;
                            ObjtblCompany.LastReviewDate = System.DateTime.Now;
                        }
                        else
                        {
                            ObjtblCompany.ReviewComments = objmodel.Review;
                            if (objmodel.LastReview != null)
                            {
                                if (objmodel.LastReview.ToLower() != "na")
                                {
                                    ObjtblCompany.LastReviewDate = Convert.ToDateTime(objmodel.LastReview);
                                }
                                else
                                {
                                    ObjtblCompany.LastReviewDate = System.DateTime.Now;
                                }
                            }
                        }
                    }
                    else
                    {
                        ObjtblCompany.ReviewComments = null;
                        ObjtblCompany.LastReviewDate = null;
                    }
                }
                ObjtblCompany.EnableAPI = objmodel.IsEnableAPI;
                ObjtblCompany.Integration = objmodel.IsIntegration;
                ObjtblCompany.APIUsername = objmodel.APIUsername != null ? objmodel.APIUsername : string.Empty;
                ObjtblCompany.APIPassword = objmodel.APIPassword != null ? objmodel.APIPassword : string.Empty;
                ObjtblCompany.PaymentTerms = objmodel.APIPaymentTerms != null ? objmodel.APIPaymentTerms : string.Empty;
                ObjtblCompany.CompanyLogo = objmodel.CompanyLogo != null ? objmodel.CompanyLogo : string.Empty;
                ObjtblCompany.APILocation = objmodel.APILocation;
                ObjtblCompany.ApiUrl = objmodel.ApIUrl != null ? objmodel.ApIUrl : string.Empty;
                ObjtblCompany.PermissiblePurpose = objmodel.pkPermissibleId;
                ObjtblCompany.Is7YearFilter = objmodel.Is7YearFilter;
                ObjtblCompany.Is7YearAutoCheck = objmodel.Is7YearAuto;
                //for int-9
                ObjtblCompany.isfcrjurisdictionfee = objmodel.isfcrjurisdictionfee;
                //INT-219 adding the SSN alert value to tblcompany
                ObjtblCompany.IsDuplicateSSNReportAlertEnabled = objmodel.IsDuplicateSSNReportAlertEnabled;


                ObjtblLocation.pkLocationId = objmodel.ObjCompLocationId;
                ObjtblLocation.LocationContact = objmodel.MainContact;
                ObjtblLocation.Address1 = objmodel.Address1;
                ObjtblLocation.Address2 = !string.IsNullOrEmpty(objmodel.Address2) ? objmodel.Address2 : string.Empty;


                if (objmodel.City.ToString() != "-2" && objmodel.City.ToString() != "-3")
                {
                    ObjtblLocation.City = objmodel.City.ToString();
                    ObjtblLocation.IsCustomCity = false;
                }
                else
                {
                    ObjtblLocation.City = !string.IsNullOrEmpty(objmodel.CityName) ? objmodel.CityName : string.Empty;
                    ObjtblLocation.IsCustomCity = true;
                }
                //string[] LocationCity = ddlCity.SelectedValue.Split('-');
                //string City = LocationCity[0].Trim();

                //ObjtblLocation.City = ddlCity.SelectedItem.Text.Trim();


                ObjtblLocation.Ext1 = string.Empty;
                ObjtblLocation.Ext2 = string.Empty;
                ObjtblLocation.Fax = !string.IsNullOrEmpty(objmodel.MainFaxNumber) ? objmodel.MainFaxNumber : string.Empty;
                ObjtblLocation.fkStateID = Convert.ToByte(objmodel.pkStateId);
                ObjtblLocation.IsEnabled = objmodel.IsEnabled;
                ObjtblLocation.IsHome = objmodel.IsHome;
                ObjtblLocation.PhoneNo1 = objmodel.MainPhone;
                ObjtblLocation.PhoneNo2 = !string.IsNullOrEmpty(objmodel.BillingPhone) ? objmodel.BillingPhone : string.Empty;
                ObjtblLocation.ZipCode = objmodel.ZipCod; /*Value of txtzipCode.Text is not used because it is causing problems due to read only mode*/
                ObjtblLocation.LastModifiedById = dbCollection.ElementAt(0).fkUserId;
                ObjtblLocation.LastModifiedDate = System.DateTime.Now;
                ObjtblLocation.Fax2 = !string.IsNullOrEmpty(objmodel.BillingFax) ? objmodel.BillingFax : string.Empty;
                ObjtblLocation.IsReportEmailActivated = objmodel.IsReportEmailActivated;
                ObjtblLocation.LocationEmail = objmodel.MainEmail;
                ObjtblLocation.SecondaryPhone = !string.IsNullOrEmpty(objmodel.SecondaryPhone) ? objmodel.SecondaryPhone : string.Empty;
                ObjtblLocation.SecondaryFax = !string.IsNullOrEmpty(objmodel.SecondaryFax) ? objmodel.SecondaryFax : string.Empty;

                // ObjtblLocation.IsSyncpod = Convert.ToBoolean();


                Result = ObjBALCompany.UpdateCompany(ObjtblCompany, ObjtblLocation);

                //    FetchCompanyDetails(new Guid(ViewState["PkLocationId"].ToString()));
            }
            catch (Exception)
            {
                //lblMessageBox.Visible = true;
                //lblMessageBox.Text = "Error Occured While Updating Record";
                //lblMessageBox.CssClass = "errormsg";
            }
            finally
            {
                ObjtblCompany = null;
                ObjBALCompany = null;
                ObjtblLocation = null;
                //ObjBALLocation = null;
            }
            return Result;
        }

        [HttpPost]
        public ActionResult SaveRecord(int value)
        {
            return View();
        }


        private void AddProducts(int PkCompanyId, int LocationId, int hdnPSProdId, int hdnNCRProdId, int hdnCCRProdId, int hdnEEIProdId, int hdnEMVProdId)
        {
            ObjGuid_ProductIds = new Dictionary<int, string>();
            IsLiveRunner = new List<bool>();
            List<SubReportsModel> data = new List<SubReportsModel>();
            List<tblCompany_Price> ObjtblCompany_Price_List = new List<tblCompany_Price>();
            List<tblProductAccess> deletetblProductAccess = new List<tblProductAccess>();
            List<tblProductAccess> ObjtblProductAccess_List = new List<tblProductAccess>();
            BALLocation ObjBALLocation = new BALLocation();
            BALCompany ObjBALCompany = new BALCompany();
            int count = 0;
            var categories = ObjBALCompany.GetReportCategories();
            if (categories.Count > 0)
            {
                foreach (var category in categories)
                {
                    var List = ObjBALCompany.GetReportProductsById(LocationId, category.pkReportCategoryId);
                    if (List.Count > 0)
                    {
                        if (data.Count > 0)
                        {
                            count = data.Count - 1;
                        }
                        data.InsertRange(count, List.Select(x => new SubReportsModel
                        {
                            pkProductApplicationId = x.pkProductApplicationId,
                            pkProductId = x.pkProductId,
                            ProductAccessId = x.ProductAccessId,
                            AutoReviewStatusForLocation = (x.AutoReviewStatusForLocation != null) ? x.AutoReviewStatusForLocation.ToString() : "0",
                            ProductDisplayName = x.ProductDisplayName,
                            ProductName = x.ProductName,
                            ProductCode = x.ProductCode,
                            ProductDescription = x.ProductDescription,
                            CategoryName = x.CategoryName,
                            CompanyPrice = x.CompanyPrice,
                            CoveragePDFPath = x.CoveragePDFPath,
                            IsDefault = x.IsDefault,
                            IsDefaultPrice = x.IsDefaultPrice != null ? x.IsDefaultPrice : true,
                            IsLiveRunnerLocation = x.IsLiveRunnerLocation,
                            IsLiveRunnerProduct = x.IsLiveRunnerProduct,
                            IsThirdPartyFee = x.IsThirdPartyFee,
                            LinkedReportId = x.LinkedReportId,
                            ReportsLinked = x.ReportsLinked
                        }).ToList());
                    }
                }
            }

            //var psLinked = data.Where(x => x.ProductCode.ToLower() == "ps").FirstOrDefault();
            //var ncr1Linked = data.Where(x => x.ProductCode.ToLower() == "ncr1").FirstOrDefault();
            //var ccr1Linked = data.Where(x => x.ProductCode.ToLower() == "ccr1").FirstOrDefault();
            //var emvLinked = data.Where(x => x.ProductCode.ToLower() == "emv").FirstOrDefault();
            //var eeiLinked = data.Where(x => x.ProductCode.ToLower() == "ps").FirstOrDefault();

            for (int iRow = 0; iRow < data.Count; iRow++)
            {
                tblCompany_Price ObjtblCompany_Price = new tblCompany_Price();
                ObjtblCompany_Price.IsDefaultPrice = data.ElementAt(iRow).IsDefaultPrice != null ? Convert.ToBoolean(data.ElementAt(iRow).IsDefaultPrice) : true;
                ObjtblCompany_Price.CompanyPrice = data.ElementAt(iRow).CompanyPrice;
                ObjtblCompany_Price.fkCompanyId = PkCompanyId;
                ObjtblCompany_Price.fkProductApplicationId = data.ElementAt(iRow).pkProductApplicationId;

                // #19
                //ObjtblCompany_Price.pkCompanyPriceId = Guid.NewGuid();

                ObjtblCompany_Price.EnableReviewPerCompany = data.ElementAt(iRow).EnableReviewPerCompany;
                if (data.ElementAt(iRow).ProductCode.ToLower() == "ps2" || data.ElementAt(iRow).ProductCode.ToLower() == "ps")
                {
                    if (hdnPSProdId != 0)
                    {
                        ObjtblCompany_Price.LinkedReportId = hdnPSProdId;
                    }
                    else
                    {
                        ObjtblCompany_Price.LinkedReportId = null;
                    }
                }
                else if (data.ElementAt(iRow).ProductCode.ToLower() == "ncr1" || data.ElementAt(iRow).ProductCode.ToLower() == "ncr2" || data.ElementAt(iRow).ProductCode.ToLower() == "ncr3" || data.ElementAt(iRow).ProductCode.ToLower() == "ncr4" || data.ElementAt(iRow).ProductCode.ToLower() == "ncr+")
                {
                    if (hdnNCRProdId != null && hdnNCRProdId != 0)
                    {
                        ObjtblCompany_Price.LinkedReportId = (int)hdnNCRProdId;
                    }
                    else
                    {
                        ObjtblCompany_Price.LinkedReportId = null;
                    }
                }
                else if (data.ElementAt(iRow).ProductCode.ToLower() == "ccr1" || data.ElementAt(iRow).ProductCode.ToLower() == "ccr2" || data.ElementAt(iRow).ProductCode.ToLower() == "rcx")
                {
                    if (hdnCCRProdId != null && hdnCCRProdId != 0)
                    {
                        ObjtblCompany_Price.LinkedReportId = (int)hdnCCRProdId;
                    }
                    else
                    {
                        ObjtblCompany_Price.LinkedReportId = null;
                    }
                }
                else if (data.ElementAt(iRow).ProductCode.ToLower() == "eei" || data.ElementAt(iRow).ProductCode.ToLower() == "ecr")
                {
                    if (hdnEEIProdId != null && hdnEEIProdId != 0)
                    {
                        ObjtblCompany_Price.LinkedReportId = (int)hdnEEIProdId;
                    }
                    else
                    {
                        ObjtblCompany_Price.LinkedReportId = null;
                    }
                }
                else if (data.ElementAt(iRow).ProductCode.ToLower() == "emv" || data.ElementAt(iRow).ProductCode.ToLower() == "edv" || data.ElementAt(iRow).ProductCode.ToLower() == "prv" || data.ElementAt(iRow).ProductCode.ToLower() == "plv")
                {
                    if (hdnEMVProdId != null && hdnEMVProdId != 0)
                    {
                        ObjtblCompany_Price.LinkedReportId = (int)hdnEMVProdId;
                    }
                    else
                    {
                        ObjtblCompany_Price.LinkedReportId = null;
                    }
                }
                else
                {
                    ObjtblCompany_Price.LinkedReportId = null;
                }

                ObjtblCompany_Price_List.Add(ObjtblCompany_Price);
                //if (data.ElementAt(iRow).IsDefault != null && Convert.ToBoolean(data.ElementAt(iRow).IsDefault))
                //{
                tblProductAccess ObjtblProductAccess = new tblProductAccess();
                ObjtblProductAccess.fkLocationId = LocationId;
                ObjtblProductAccess.fkProductApplicationId = data.ElementAt(iRow).pkProductApplicationId;
                ObjtblProductAccess.IsLiveRunner = data.ElementAt(iRow).IsLiveRunnerProduct;

                //int 203
                //start 

                ObjtblProductAccess.ProductReviewStatus = (data.ElementAt(iRow).AutoReviewStatusForLocation != null) ? byte.Parse(data.ElementAt(iRow).AutoReviewStatusForLocation) : byte.MinValue;




                ObjtblProductAccess.IsProductEnabled = data.ElementAt(iRow).IsDefault != null ? Convert.ToBoolean(data.ElementAt(iRow).IsDefault) : false;
                ObjtblProductAccess.LocationProductPrice = data.ElementAt(iRow).CompanyPrice;
                ObjtblProductAccess_List.Add(ObjtblProductAccess);
                //}
            }

            ObjBALLocation.InsertUpdateLocationProduct(ObjtblProductAccess_List, deletetblProductAccess);
            ObjBALCompany.AddProductPricePerCompany(ObjtblCompany_Price_List);

        }

        public ActionResult GetCustomPackages(int? LocationId, int? CompanyId)
        {

            BALProductPackages ObjBALProductPackages = new BALProductPackages();


            ObjGuid_LocationId = (int)LocationId;

            var ObjData = ObjBALProductPackages.GetEmergeProductPackagesForAdmin((int)ObjGuid_LocationId, Utility.SiteApplicationId, (int)CompanyId);



            //for (int i = 0; i < ObjData.Count(); i++)
            //{
            //    if (ObjData.ElementAt(i).IsEnableCompanyPackage == false)
            //    {
            //        ObjData.ElementAt(i).IsDefault = false;


            //    }
            //    else
            //    {
            //        if (ObjData.ElementAt(i).PackageAccessId != 0)
            //        {
            //            if (ObjData.ElementAt(i).IsDefault != false)
            //            {
            //                ObjData.ElementAt(i).IsDefault = true;
            //            }
            //        }
            //        ObjData.ElementAt(i).IsDefault = true;
            //    }


            //    ObjData.ElementAt(i).IsDefault = ObjData.ElementAt(i).IsEnabled;


            //}


            //var ObjCollSavedPackages = ObjData.Where(db => db.PackageAccessId != Guid.Empty); 
            //Guid?[] arrSavedPackages = ObjCollSavedPackages.Select(db => db.PackageAccessId).ToArray();




            return Json(ObjData);


        }


        public ActionResult SaveApiInformation(AdCompanyModel1 objmodel)
        {
            string Result = string.Empty;
            string Save = "0";
            string Action = "1";
            BALCompanyType objBALCompanyType = new BALCompanyType();
            tblCompany ObjtblCompany = new tblCompany();
            try
            {
                ObjtblCompany.pkCompanyId = objmodel.pkCompanyId;
                ObjtblCompany.EnableAPI = objmodel.IsEnableAPI;
                ObjtblCompany.Integration = objmodel.IsIntegration;
                ObjtblCompany.APIUsername = objmodel.APIUsername != null ? objmodel.APIUsername : string.Empty;
                ObjtblCompany.APIPassword = objmodel.APIPassword != null ? objmodel.APIPassword : string.Empty;
                ObjtblCompany.PaymentTerms = objmodel.APIPaymentTerms != null ? objmodel.APIPaymentTerms : string.Empty;
                ObjtblCompany.CompanyLogo = objmodel.CompanyLogo != null ? objmodel.CompanyLogo : string.Empty;
                //ObjtblCompany.APILocation = objmodel.APILocation;
                ObjtblCompany.APILocations = objmodel.SelectedLocationId;//INT106
                ObjtblCompany.ApiUrl = objmodel.ApIUrl != null ? objmodel.ApIUrl : string.Empty;
                int saveresult = objBALCompanyType.UpdateApiInformation(ObjtblCompany);
                Save = saveresult.ToString();
                RunReportUrl.UpdateRouteRegistration();
            }
            catch
            {
            }

            return Json(new { Save = Save, Id = Result, Action = Action });

        }
        public ActionResult SaveRec(AdCompanyModel1 Detail, HttpPostedFileBase uplCompanyLogo, string hdnPSProdId, string hdnNCRProdId, string hdnCCRProdId, string hdnEEIProdId, string hdnEMVProdId)
        {
            string Result = string.Empty;
            string Save = "0";
            string Action = "1";
            try
            {
                if (Detail.IsEdit)
                {
                    Action = UpdateCompany(Detail).ToString();
                    Session["NewPkCompanyId"] = Detail.pkCompanyId;
                    Session["pkCompLocationId"] = Detail.ObjCompLocationId;
                    Save = "1";
                    Result = Detail.ObjCompLocationId.ToString();
                    //return Json(new { Save = "1",Id= Detail.ObjCompLocationId , Action = Action });
                }
                else
                {
                    List<int> CompanyIdAndLocationId = AddCompany(Detail);
                    if (CompanyIdAndLocationId.Count != 0)
                    {
                        //AdCompanyModel objmodel1 = new AdCompanyModel();
                        //int creation = AddUniversalPackagesForCompany(CompanyIdAndLocationId[0], CompanyIdAndLocationId[1]);
                        objlocationid = CompanyIdAndLocationId[1];
                        var NewCompanyid = CompanyIdAndLocationId[0];
                        Detail.ObjCompLocationId = objlocationid;
                        Session["NewPkCompanyId"] = NewCompanyid;
                        Session["pkCompLocationId"] = objlocationid;
                        AddProducts(CompanyIdAndLocationId[0], CompanyIdAndLocationId[1], !string.IsNullOrEmpty(hdnPSProdId) ? Int32.Parse(hdnPSProdId) : 0, !string.IsNullOrEmpty(hdnNCRProdId) ? Int32.Parse(hdnNCRProdId) : 0, !string.IsNullOrEmpty(hdnCCRProdId) ? Int32.Parse(hdnCCRProdId) : 0, !string.IsNullOrEmpty(hdnEEIProdId) ? Int32.Parse(hdnEEIProdId) : 0, !string.IsNullOrEmpty(hdnEMVProdId) ? Int32.Parse(hdnEMVProdId) : 0);
                        Result = objlocationid.ToString();





                        Save = "1";
                        Action = "2";
                    }

                }
            }
            catch
            {
                Action = "-1";
            }
            return Json(new { Save = Save, Id = Result, Action = Action });

        }

        private int AddUniversalPackagesForCompany(int? CompanyId, int? LocationId)
        {
            int Result = -1;
            BALProductPackages ObjBALProductPackages = new BALProductPackages();
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    try
                    {
                        if (CompanyId != 0)
                        {
                            List<tblProductPackage> lst = DX.tblProductPackages.Where(d => d.fkCompanyId == null && d.IsDelete == false).ToList<tblProductPackage>();
                            if (lst.Count > 0)
                            {
                                List<tblCompany_PackagePrice> lstCompany_PackagePrice = new List<tblCompany_PackagePrice>();
                                List<tblPackageAccess> lstPackageAccess = new List<tblPackageAccess>();

                                for (int i = 0; i < lst.Count; i++)
                                {
                                    tblCompany_PackagePrice ObjCompany_PackagePrice = new tblCompany_PackagePrice();    // add in new company (company wise)

                                    // #19                                         
                                    //ObjCompany_PackagePrice.pkCompanyPackagePriceId = Guid.NewGuid();
                                    ObjCompany_PackagePrice.fkCompanyId = CompanyId;
                                    ObjCompany_PackagePrice.fkPackageId = lst.ElementAt(i).pkPackageId;
                                    ObjCompany_PackagePrice.PackagePrice = lst.ElementAt(i).PackagePrice;
                                    ObjCompany_PackagePrice.IsEnableUniversalPackage = true;

                                    lstCompany_PackagePrice.Add(ObjCompany_PackagePrice);

                                    tblPackageAccess ObjPackageAccess = new tblPackageAccess();             // add in new company location wise

                                    //                                 ObjPackageAccess.pkPackageAccessId = Guid.NewGuid();


                                    ObjPackageAccess.fkLocationId = LocationId;
                                    ObjPackageAccess.fkPackageId = lst.ElementAt(i).pkPackageId;

                                    lstPackageAccess.Add(ObjPackageAccess);
                                }
                                Result = ObjBALProductPackages.AddUniversalPackagesForNewCompany(lstCompany_PackagePrice, lstPackageAccess);
                            }
                        }

                    }
                    catch
                    {
                    }
                }
            }
            catch
            {
            }
            finally
            {
                ObjBALProductPackages = null;
            }
            return Result;
        }

        [HttpPost]
        public ActionResult UpdateLinkPrice(int ProductApplicationId, int CompanyId)
        {
            BALProducts objBALProducts = new BALProducts();
            int _ProductApplicationId = ProductApplicationId != 0 ? ProductApplicationId : 0;
            int _CompanyId = CompanyId != 0 ? CompanyId : 0;
            int Result = 0;
            try
            {
                objBALProducts.UpdateIsLinkedPrice(_ProductApplicationId, _CompanyId);
                Result = 1;
            }
            catch
            {
                Result = -1;
            }
            return Json(new { Rslt = Result });
        }


        [HttpPost]
        public ActionResult AddDocuments(FormCollection CollectionObj)
        {
            string strSuccess = string.Empty;
            var pklocid = Session["pkLocationIdWhenEdit"];
            try
            {
                if (Request.Files[0].FileName != "")
                {
                    string sExt = Path.GetExtension(Request.Files[0].FileName).ToLower();
                    string sFileName = DateTime.Now.ToFileTime().ToString() + sExt;
                    //string sFilePath = "";




                    tblCompanyDocument ObjtblComapnyDocument = new tblCompanyDocument();

                    // #19
                    //ObjtblComapnyDocument.pkCompanyDocumentId = Guid.NewGuid();

                    ObjtblComapnyDocument.fkCompanyId = int.Parse(CollectionObj.GetValue("pkCompanyId").AttemptedValue);
                    ObjtblComapnyDocument.DocumentTitle = Convert.ToString(CollectionObj.GetValue("Title").AttemptedValue);
                    ObjtblComapnyDocument.DocumentName = sFileName;
                    ObjtblComapnyDocument.CreatedDate = DateTime.Now;
                    ObjtblComapnyDocument.IsUserAgreement = false;

                    BALCompany ObjBALComapny = new BALCompany();
                    var Success = ObjBALComapny.InsertDocumentDetailsByCompanyId(ObjtblComapnyDocument);
                    if (Success > 0)
                    {

                        HttpPostedFileBase ObjHttpPostedFile = Request.Files[0];
                        ObjHttpPostedFile.SaveAs(Server.MapPath(@"~\Resources\Upload\CompanyPDF\") + sFileName);

                        BALEmailSystemTemplates ObjEmailTemplate = new BALEmailSystemTemplates();
                        ObjEmailTemplate.InsertAlerts(Success, DateTime.Now);
                        strSuccess = "2";// "<span class='successmsg'>Feature Updated Successfully</span>";
                    }

                    else if (Success == -1)
                    {
                        strSuccess = "-1";// "<span class='errormsg'>Title name already exist</span>";
                    }
                    else
                    {
                        strSuccess = "-2"; //"<span class='errormsg'>error is occured while updating content</span>";
                    }
                }
            }

            catch (Exception)
            {
            }
            return RedirectToAction("DocumentsByCompanyId", new { Id1 = pklocid, Status = strSuccess });


        }




        [HttpPost]
        public ActionResult UpdateDocuments(FormCollection ObjCollection)
        {
            var pklocid = Session["pkLocationIdWhenEdit"];
            BALCompany ObjBALCompany = new BALCompany();
            string strSuccess = string.Empty;

            try
            {
                //DateTime LastModifiedDate = DateTime.Now;
                string sFileName = string.Empty;

                if (Request.Files[0].FileName != "")
                {
                    string sExt = Path.GetExtension(Request.Files[0].FileName).ToLower();
                    sFileName = DateTime.Now.ToFileTime().ToString() + sExt;
                    string sFilePath = "";
                    sFilePath = Server.MapPath("~/Resources/Upload/CompanyPDF/");

                    HttpPostedFileBase ObjHttpPostedFile = Request.Files[0];
                    ObjHttpPostedFile.SaveAs(Server.MapPath(@"~\Resources\Upload\CompanyPDF\") + sFileName);

                    string OldFilePath = Server.MapPath(@"~\Resources\Upload\CompanyPDF\") + Convert.ToString(ObjCollection.GetValue("DocumentPath").AttemptedValue);
                    Utility.DeleteFile(OldFilePath);
                }
                else
                {
                    sFileName = Convert.ToString(ObjCollection.GetValue("DocumentPath").AttemptedValue);
                }

                tblCompanyDocument ObjtblComapnyDocument = new tblCompanyDocument();
                ObjtblComapnyDocument.pkCompanyDocumentId = int.Parse(ObjCollection.GetValue("pkDocumentId").AttemptedValue);
                ObjtblComapnyDocument.DocumentTitle = Convert.ToString(ObjCollection.GetValue("Title").AttemptedValue);
                ObjtblComapnyDocument.DocumentName = sFileName;


                int Success = 0;

                Success = ObjBALCompany.UpdateCompanyDocByDocId(ObjtblComapnyDocument);

                if (Success == 1)
                {
                    strSuccess = "1";
                }
                else if (Success == -1)
                {
                    strSuccess = "-1";
                }
                else
                {
                    strSuccess = "-2";
                }

            }
            catch (Exception)
            {

            }
            return RedirectToAction("DocumentsByCompanyId", new { Id1 = pklocid, Status = strSuccess });

        }

        [HttpPost]
        public ActionResult DeleteDocuments(int pkDocumentsId)
        {

            int Success = 0;
            string strSuccess = string.Empty;
            var pklocid = Session["pkLocationIdWhenEdit"];
            BALCompany objbalcompany = new BALCompany();
            try
            {


                Success = objbalcompany.DeleteCompanyDocumentsByDocId(pkDocumentsId);
                if (Success == 1)
                {
                    strSuccess = "3";

                }
                else
                {
                    strSuccess = "-2";
                }

            }
            catch
            {
            }
            return Json(new { Id1 = pklocid, Status = strSuccess });
            //return RedirectToAction("AddCompanies", new { Id = pklocid, Status = Success });
        }

        public ActionResult checkapiurl(string apiurl)
        {
            BALCompanyType ObjBALCompanyType = new BALCompanyType();

            int result = ObjBALCompanyType.Get_apiurlexist(apiurl);
            return Json(result);

        }

        public ActionResult checkapiurlcompany(string apiurl, int PkCompanyId)
        {
            BALCompanyType ObjBALCompanyType = new BALCompanyType();
            int pkCompanyId = PkCompanyId;
            int result = ObjBALCompanyType.Get_apiurlexistcompany(apiurl, pkCompanyId);
            return Json(result);

        }





        public ActionResult DownloadCompanyDocumentsFile(string FPathTitle)
        {
            string[] str = FPathTitle.Split('_');
            string FileName = str[0];
            string Title = str[1].Replace(" ", "_");
            string FilePath = Request.PhysicalApplicationPath + "Resources/Upload/CompanyPDF/" + FileName;
            string fileext = System.IO.Path.GetExtension(FileName);
            string CType = "application/" + fileext.Replace(".", "");
           // string fileName = Path.GetFileName(FileName);

            string strFileTitle = Title + fileext;



            if (System.IO.File.Exists(FilePath))
            {
                return File(FilePath, CType, strFileTitle);
            }
            else
            {
                FilePath = Request.PhysicalApplicationPath + "Resources/Upload/CompanyPDF/no-image.jpg";
                return File(FilePath, CType, strFileTitle);
            }
        }


        public ActionResult SaveUpload(IEnumerable<HttpPostedFileBase> attachments, string Title, int CompanyId)
        {
            // string Extension = string.Empty;
            var lablel = " ";

            //AdCompanyModel objModel = new AdCompanyModel();
            //var doucmentstititi = objModel.DocumentsTitle;

            //if (Session["NewPkCompanyId"] != null)
            //{
            foreach (var file in attachments)
            {
                // Some browsers send file names with full path. This needs to be stripped.
                var ext = Path.GetExtension(file.FileName);
                if (ext == ".pdf")
                {
                    //var fileName = Path.GetFileName(file.FileName);
                    string NewFileName = DateTime.Now.ToFileTime() + Path.GetExtension(file.FileName);
                    var physicalPath = Path.Combine(Server.MapPath("~/Resources/Upload/CompanyPDF"), NewFileName);
                    file.SaveAs(physicalPath);
                    string sFilePath = "";
                    sFilePath = HttpContext.Server.MapPath("~/Resources/Upload/CompanyPDF/");

                    int NoOfPages = 4;
                    if (NoOfPages == 4)
                    {
                        if (CompanyId != 0)
                        {
                            ObjBALCompany = new BALCompany();
                            tblCompanyDocument tbl = new tblCompanyDocument();
                            // #19
                            //tbl.pkCompanyDocumentId = Guid.NewGuid();

                            tbl.fkCompanyId = (int)CompanyId;
                            tbl.DocumentTitle = !string.IsNullOrEmpty(Title) ? Title : "User AgreeMents";
                            tbl.DocumentName = NewFileName;
                            tbl.CreatedDate = DateTime.Now;
                            tbl.IsUserAgreement = false;
                            //int Result = ObjBALCompany.InsertDocumentDetailsByCompanyId(tbl);
                        }
                        else
                        {
                            return Content("There is no Company");
                        }

                    }
                    else
                    {
                        try
                        {

                        }
                        catch
                        {
                        }

                    }
                }
                else
                {
                    lablel = "only PDF Upload";

                    //   Response.Write("only PDF File Allowed Upload");
                    //  return Json(lablel);
                    return Content("only PDF Upload");

                }
            }
            return Content("");
            //}
            //else
            //{
            //  Response.Redirect("GetAllCompanies/Control");
            //}
            // Return an empty string to signify success
            //return Content("");
        }

        public ActionResult UploadCompanyLogo(HttpPostedFileBase uplCompanyLogo)
        {
            string FileName = "";
            if (uplCompanyLogo != null)
            {
                var ext = Path.GetExtension(uplCompanyLogo.FileName);
                if (ext.ToLower() == ".jpg" || ext.ToLower() == ".png" || ext.ToLower() == ".jpeg" || ext.ToLower() == ".gif")
                {
                    FileName = DateTime.Now.ToFileTime() + ext;
                    string FolderPath = HttpContext.Server.MapPath("~/Resources/Upload/CompanyLogo/");
                    if (!Directory.Exists(FolderPath))
                    {
                        Directory.CreateDirectory(FolderPath);
                    }
                    //var physicalPath = Path.Combine(FolderPath, FileName);
                    uplCompanyLogo.SaveAs(FolderPath + FileName);
                }
                else
                {
                    return Content("Invalid File.");
                }
            }
            Session["CompanyLogo"] = FileName;
            ViewData["CompanyLogo"] = FileName;
            return Content("");

        }

        public JsonResult GetCompanyLogo()
        {
            string FileName = string.Empty;
            if (Session["CompanyLogo"] != null)
            {
                FileName = Session["CompanyLogo"].ToString();
            }
            return Json(new { File = FileName });
        }

        [HttpPost]
        public ActionResult RemoveCompanyLogo(string FileName)
        {
            if (!string.IsNullOrEmpty(FileName))
            {
                var physicalPath = Path.Combine(Server.MapPath("~/Resources/Upload/CompanyLogo"), FileName);

                // TODO: Verify user permissions
                if (System.IO.File.Exists(physicalPath))
                {
                    System.IO.File.Delete(physicalPath);
                }
            }
            Session["CompanyLogo"] = "";
            ViewData["CompanyLogo"] = "";
            return Content("");
        }

        [HttpPost]
        public ActionResult RemoveUploadFile(string[] fileNames)
        {
            // The parameter of the Remove action must be called "fileNames"
            foreach (var fullName in fileNames)
            {
                var fileName = Path.GetFileName(fullName);
                var physicalPath = Path.Combine(Server.MapPath("~/Resources/Upload/CompanyPDF"), fileName);

                // TODO: Verify user permissions
                if (System.IO.File.Exists(physicalPath))
                {
                    System.IO.File.Delete(physicalPath);
                }
            }
            // Return an empty string to signify success
            return Content("");
        }

        public ActionResult GetLocations(string LocationId, string CompanyId, int page, int pageSize, string group, List<GridSort> sort)
        {

            BALCompany objbal = new BALCompany();
            //CompanyModel objModel = new CompanyModel();
           // AdCompanyModel objmodel1 = new AdCompanyModel();


            if (string.IsNullOrEmpty(CompanyId) || string.IsNullOrEmpty(LocationId))
            {

                return Json(new { Reclist = 0, TotalRec = 0 });
            }
            else
            {

                int pkCompanyId1 = int.Parse(CompanyId);
                //int pklocationid = int.Parse(LocationId);
                //Guid pkCompanyId1 = Guid.Empty;
                //Guid pklocationid = Guid.Empty;
                int TotalRec = 0;
                int PageSize = pageSize;
                //  page = 0;
                var list = objbal.GetAllLocationsForAdmin(page, PageSize, true, 0, -1, pkCompanyId1, "City", "ASC", string.Empty, 0, string.Empty, out TotalRec);
                if (list.Count == 0)
                {
                    return Json(new { Reclist = 0, TotalRec = 0 });
                }
                else
                {
                    // return Json(new { Reclist = list, TotalRec = list.ElementAt(0).TotalRec });
                    // return Json(new { Reclist = list, TotalRec = list.ElementAt(0).TotalRec });

                    return Json(new
                    {
                        Reclist = list,
                        TotalRec = list.ElementAt(0).TotalRec
                    },
                      JsonRequestBehavior.AllowGet);


                }
            }


        }

        [HttpPost]
        public ActionResult GetAllEmergeUsers(string CompanyId, int page, int pageSize, string group, List<GridSort> sort, int UserStatus)
        {

            if (string.IsNullOrEmpty(CompanyId))
            {
                return Json(new { Reclist = 0, TotalRec = 0 });
            }
            else
            {
                BALCompany serobj = new BALCompany();
                var fkCompanyId = int.Parse(CompanyId);
                var IsEnabled = Convert.ToBoolean(UserStatus);//INT-220
                //  var Page = 1;
                int TotalRec = 0;
                int PageIndex = page;
                int PageSize = pageSize;
                // INT-220 add a parameter to retrive the user on it's status basis.
                var list = serobj.GetAllEmergeUsersWithStatusForAdmin(PageIndex, PageSize, true, -1, fkCompanyId, 0, "FirstName", "ASC", string.Empty, 0, string.Empty, IsEnabled, out TotalRec);
                if (list.Count == 0)
                {
                    return Json(new { Reclist = 0, TotalRec = 0 });
                }
                else
                {
                    return Json(new { Reclist = list, TotalRec = list.ElementAt(0).TotalRec });
                }


            }
        }

        public FilePathResult DownloadUserAgreements(Guid Id)
        {
            string FilePath = Request.PhysicalApplicationPath + "Resources/Forms/" + Id;
            string fileext = System.IO.Path.GetExtension(FilePath);
            string CType = "application/" + fileext.Replace(".", "");
            string fileName = Path.GetFileName(FilePath);
            return File(FilePath, CType, fileName);
        }

        public ActionResult UpdateInvoice(IEnumerable<AdCompanyModel> products)
        {
            return View();
        }

        public ActionResult AddNewReference(IEnumerable<Proc_GetReferencesResult> products, int CompanyId)
        {
            int list = 0;
            try
            {
                list = AddEditReference(products, CompanyId);
            }
            catch { }
            return Json(list);

        }

        public ActionResult UpdateReference(IEnumerable<Proc_GetReferencesResult> products, int CompanyId)
        {
            int list = 0;
            try
            {
                list = AddEditReference(products, CompanyId);
            }
            catch { }
            return Json(list);

        }

        public int AddEditReference(IEnumerable<Proc_GetReferencesResult> products, int CompanyId)
        {
            BALReferenceCode ObjBALReferenceCode = new BALReferenceCode();
            int list = 0;
            int? fkcompanyid = CompanyId != 0 ? (int)CompanyId : 0;
            List<tblReferenceCode> Objlist = new List<tblReferenceCode>();

            foreach (var reflist in products)
            {
                tblReferenceCode Objtbl = new tblReferenceCode();
                if (reflist != null)
                {
                    Objtbl.pkReferenceCodeId = reflist.pkReferenceCodeId;
                }
                else
                {
                    Objtbl.pkReferenceCodeId = 0;
                }
                Objtbl.ReferenceCode = reflist.ReferenceCode;
                Objtbl.ReferenceNote = reflist.ReferenceNote;
                Objtbl.fkCompanyId = fkcompanyid;
                Objlist.Add(Objtbl);
            }
            list = ObjBALReferenceCode.AddEditReference(Objlist);

            return list;

        }

        public ActionResult GetCmpnyType()
        {
            BALCompanyType serboj = new BALCompanyType();
            var list = serboj.GetAllCompanyType();
            return Json(list);
        }


        public ActionResult GetManagerList()
        {
            BALCompany serobj = new BALCompany();
            string rolename = "salesmanager";
            var list = serobj.GetManagers(rolename);

            return Json(list);

        }
        public ActionResult GetSalesAssociateList()
        {
            BALCompany serobj = new BALCompany();
            string rolename = "salesrep";
            var list = serobj.GetManagers(rolename);

            return Json(list);

        }

        public ActionResult GetStateList()
        {
            BALCompany serobj = new BALCompany();
            var list1 = serobj.GetState();
            return Json(list1);
        }

        public ActionResult GetCityList(int pkid)
        {

            BALLocation serobj = new BALLocation();
            var list1 = serobj.GetCityStatewise(pkid);
            list1.Add(new City { CityName = "---------------", CityValue = "" });
            list1.Add(new City { CityName = "----Custom----", CityValue = "-3" });
            return Json(list1);
        }

        public ActionResult GetCity(int ID)
        {

            BALLocation serobj = new BALLocation();
            var list1 = serobj.GetCityStatewise(ID);
            return Json(list1);
        }

        [HttpPost]
        public ActionResult DeleteReferenceCode(int ReferenceCodeId, int CompanyId)
        {
            BALReferenceCode serobj = new BALReferenceCode();
            int list = 0;
            int? fkcompanyid = CompanyId != 0 ? CompanyId : 0;
            list = serobj.DeleteReference(ReferenceCodeId, (int)fkcompanyid);
            return Json(list);
        }

        [HttpPost]
        public ActionResult DeleteLocations(int LocationsId)
        {
            BALLocation ObjBALLocation = new BALLocation();
            var result = ObjBALLocation.DeleteLocationById(LocationsId);
            return Json(result);
        }

        [HttpPost]
        public ActionResult DeleteCompanyUsers(int CompanyUserId)
        {
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            var result = ObjBALCompanyUsers.DeleteEmergeUserById(CompanyUserId);
            return Json(result);
        }

        public ActionResult LoadOpenInvoice(int CompanyId)
        {
           // AdCompanyModel objmodel = new AdCompanyModel();
            if (CompanyId ==
                0)
            {


                return null;
            }
            else
            {


               // AdCompanyModel ObjModel = new AdCompanyModel();
                BALCompany serObj = new BALCompany();
                int CID = CompanyId;
                var list = serObj.LoadOpenInVoice(CID);
                return Json(list);
            }
        }

        public string GetOpenInvoice(int CompanyId, string PageType)
        {
            string content = string.Empty;
           // AdCompanyModel objmodel = new AdCompanyModel();
            if (CompanyId == 0)
            {


                content = "";
            }
            else
            {


                //AdCompanyModel ObjModel = new AdCompanyModel();
                BALCompany serObj = new BALCompany();
                int CID = CompanyId;
                var list = serObj.LoadOpenInVoice(CID);
                if (list.Count > 0)
                {
                    content += "<table align='Center' cellspacing='0' cellpadding='4' border='1' style='background-color: White;width:100%; border-collapse: collapse; float:left;' id='gvOpenInvoice' rules='all' class='Grid'>";
                    content += "<tbody><tr align='center' style='font-weight: bold;color: #0B55C4;' class='GridHeader gridtyle'><th scope='col'  style='width: 24px;'>&nbsp;</th><th scope='col' >Invoice #</th><th scope='col' style='width: 230px;'>Invoice Date</th><th scope='col'>Due Date</th><th scope='col' style='width: 127px; text-align: right;'>Invoice Amount</th><th scope='col' style='text-align: center; width: 125px;'>Adjustment</th><th scope='col'>Amount Paid</th><th scope='col'>Remaining Balance</th>";
                    if (!string.IsNullOrEmpty(PageType))
                    {
                        content += "<th scope='col'> Action </th>";
                    }
                    content += "</tr>";
                    for (var i = 0; i < list.Count; i++)
                    {
                        System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                        content += "<tr valign='middle' align='left' style='border-color: #BBBBBB; border-width: 1px;border-style: solid;' class='GridRow'>";
                        content += "<td>" + (i + 1) + "</td>";
                        content += "<td>" + list.ElementAt(i).InvoiceNumber + "</td>";
                        content += "<td align='left'>" + mfi.GetMonthName(Convert.ToInt32(list.ElementAt(i).InvoiceMonth)).ToString() + " " + list.ElementAt(i).InvoiceYear.ToString() + "</td>";
                        if (list.ElementAt(i).InvoiceDueDate <= DateTime.Now)
                        {
                            content += "<td align='left'>" + string.Format("{0:MMM d, yyyy}", list.ElementAt(i).InvoiceDueDate) + "&nbsp;<span style='color: Red;' >[Past Due]</span></td>";
                        }
                        else
                        {
                            content += "<td align='left'>" + string.Format("{0:MMM d, yyyy}", list.ElementAt(i).InvoiceDueDate) + "</td>";
                        }
                        content += "<td align='left'>$" + Convert.ToDecimal(list.ElementAt(i).TotalInvoiceAmount).ToString("#,##0.00") + "</td>";
                        content += "<td align='left'>$" + Convert.ToDecimal(list.ElementAt(i).InvoiceDiscount).ToString("#,##0.00") + "</td>";
                        content += "<td align='left'>$" + Convert.ToDecimal(list.ElementAt(i).AmountPaid).ToString("#,##0.00") + "</td>";
                        content += "<td align='left'>$" + Convert.ToDecimal(list.ElementAt(i).RemainingBalance).ToString("#,##0.00") + "</td>";
                        if (!string.IsNullOrEmpty(PageType))
                        {
                            content += "<td align='left'><a href='#' ><img src='" + ApplicationPath.GetApplicationPath() + "Content/themes/base/Images/Payinvoice1.png' alt='' /> </a></td>";
                        }
                        content += "</tr>";
                    }

                    content += "<tr align='left' style='color: #0B55C4; background-color: #FFFFDD; font-weight: bold;'><td>&nbsp;</td><td>Total</td><td>&nbsp;</td><td>&nbsp;</td><td><span >$" + Convert.ToDecimal(list.Sum(x => x.TotalInvoiceAmount)).ToString("#,##0.00") + "</span></td><td><span >$" + Convert.ToDecimal(list.Sum(x => x.InvoiceDiscount)).ToString("#,##0.00") + "</span></td><td><span >$" + Convert.ToDecimal(list.Sum(x => x.AmountPaid)).ToString("#,##0.00") + "</span></td><td><span >$" + Convert.ToDecimal(list.Sum(x => x.RemainingBalance)).ToString("#,##0.00") + "</span></td>";
                    if (!string.IsNullOrEmpty(PageType))
                    {
                        content += "<td align='left'><a href='#' ><img src='" + ApplicationPath.GetApplicationPath() + "Content/themes/base/Images/Payallinvoice1.png' alt='' /> </a></td>";
                    }
                    content += "</tr>";
                    content += "</tbody>";
                    content += "</table>";

                }
                else
                {
                    content = " <div id='nullrecord' style='margin-left: 400px'><h4><span class='successmsg'> There is no open Invoice Available For This Company</span></h4></div>";
                }
            }
            return content;
        }

        #region commented by chetu openinvoice
        /*
        public ActionResult GetOpenInvoiceAndRemainingBalance(string CompanyId, string PageType)
        {
            string content = string.Empty;
            AdCompanyModel objmodel = new AdCompanyModel();
            List<proc_LoadOpenInvoiceByCompanyIdResult> list = new List<proc_LoadOpenInvoiceByCompanyIdResult>();
            List<string> chkStatementBalance = new List<string>();
            decimal RemainingBalance = 0;
            if (string.IsNullOrEmpty(CompanyId))
            {


                content = "";
            }
            else
            {


                AdCompanyModel ObjModel = new AdCompanyModel();
                BALCompany serObj = new BALCompany();
                Guid CID = Guid.Parse(CompanyId);
                list = serObj.LoadOpenInVoice(CID);
                if (list.Count > 0)
                {
                    content += "<table align='Center' cellspacing='0' cellpadding='4' border='1' style='background-color: White;width:100%; border-collapse: collapse; float:left;' id='gvOpenInvoice' rules='all' class='Grid'>";
                    content += "<tbody><tr align='center' style='font-weight: bold;color: #0B55C4;' class='GridHeader'><th scope='col'>&nbsp;</th><th scope='col'>Invoice #</th><th scope='col'>Invoice Date</th><th scope='col'>Due Date</th><th scope='col'>Invoice Amount</th><th scope='col'>Adjustment</th><th scope='col'>Amount Paid</th><th scope='col'>Remaining Balance</th>";
                    if (!string.IsNullOrEmpty(PageType))
                    {
                        if (PageType == "0")
                        {
                            content += "<th scope='col'> Action </th>";
                        }
                    }
                    content += "</tr>";
                    for (var i = 0; i < list.Count; i++)
                    {
                        string DisplayText = "";
                        content += "<tr valign='middle' align='left' style='border-color: #BBBBBB; border-width: 1px;border-style: solid;' class='GridRow'>";
                        content += "<td>" + (i + 1) + "</td>";
                        content += "<td>" + list.ElementAt(i).InvoiceNumber + "</td>";
                        content += "<td align='left'>" + string.Format("{0:MMM d, yyyy}", list.ElementAt(i).InvoiceDate) + "</td>";
                        if (list.ElementAt(i).InvoiceDueDate <= DateTime.Now)
                        {
                            content += "<td align='left'>" + string.Format("{0:MMM d, yyyy}", list.ElementAt(i).InvoiceDueDate) + "&nbsp;<span style='color: Red;' >[Past Due]</span></td>";
                            DisplayText = "Statement Balance (#" + list.ElementAt(i).InvoiceNumber + "): <b>" + String.Format("{0:C}", list.ElementAt(i).RemainingBalance) + "</b>  due " + list.ElementAt(i).InvoiceDueDate.ToShortDateString() + "<span style='color:red;'>[Past Due]</span>";
                        }
                        else
                        {
                            content += "<td align='left'>" + string.Format("{0:MMM d, yyyy}", list.ElementAt(i).InvoiceDueDate) + "</td>";
                            DisplayText = "Statement Balance (#" + list.ElementAt(i).InvoiceNumber + "): <b>" + String.Format("{0:C}", list.ElementAt(i).RemainingBalance) + "</b>  due " + list.ElementAt(i).InvoiceDueDate.ToShortDateString() + "";
                        }
                        content += "<td align='left'>$" + Convert.ToDecimal(list.ElementAt(i).TotalInvoiceAmount).ToString("#,##0.00") + "</td>";
                        content += "<td align='left'>$" + Convert.ToDecimal(list.ElementAt(i).InvoiceDiscount).ToString("#,##0.00") + "</td>";
                        content += "<td align='left'>$" + Convert.ToDecimal(list.ElementAt(i).AmountPaid).ToString("#,##0.00") + "</td>";
                        content += "<td align='left'>$" + Convert.ToDecimal(list.ElementAt(i).RemainingBalance).ToString("#,##0.00") + "</td>";
                        if (!string.IsNullOrEmpty(PageType))
                        {
                            if (PageType == "0")
                            {
                                content += "<td align='left'><a onclick=\"OpenPayInvoice('" + i + "')\" ><img src='" + ApplicationPath.GetApplicationPath() + "Content/themes/base/Images/Payinvoice1.png' alt='' /> </a></td>";
                            }
                        }
                        content += "</tr>";
                        //
                        chkStatementBalance.Add(list.ElementAt(i).pkInvoiceId.ToString() + "_" + DisplayText);
                    }
                    RemainingBalance = Convert.ToDecimal(list.Sum(x => x.RemainingBalance));
                    content += "<tr align='left' style='color: #0B55C4; background-color: #FFFFDD; font-weight: bold;'><td>&nbsp;</td><td>Total</td><td>&nbsp;</td><td>&nbsp;</td><td><span >$" + Convert.ToDecimal(list.Sum(x => x.TotalInvoiceAmount)).ToString("#,##0.00") + "</span></td><td><span >$" + Convert.ToDecimal(list.Sum(x => x.InvoiceDiscount)).ToString("#,##0.00") + "</span></td><td><span >$" + Convert.ToDecimal(list.Sum(x => x.AmountPaid)).ToString("#,##0.00") + "</span></td><td><span >$" + Convert.ToDecimal(list.Sum(x => x.RemainingBalance)).ToString("#,##0.00") + "</span></td>";
                    if (!string.IsNullOrEmpty(PageType))
                    {
                        if (PageType == "0")
                        {
                            content += "<td align='left'><a onclick=\"OpenPayInvoice('-1')\" ><img src='" + ApplicationPath.GetApplicationPath() + "Content/themes/base/Images/Payallinvoice1.png' alt='' /> </a></td>";
                        }
                    }
                    content += "</tr>";
                    content += "</tbody>";
                    content += "</table>";

                }
                else
                {
                    content = " <div id='nullrecord' style='margin-left: 400px'><h4><span class='successmsg'> There is no open Invoice Avaliable For This Company</span></h4></div>";
                }
            }


            return Json(new { content = content, list = list, chkStatementBalance = chkStatementBalance, RemainingBalance = RemainingBalance });
        }
        */
        #endregion

        public ActionResult GetBillingInfo(int CompanyId)
        {


            int ComId = CompanyId;
            BALPayment ObjBALPayment = new BALPayment();
            List<proc_GetPaymentBillingInfoPerCompanyIdResult> ObjDbCollection = new List<proc_GetPaymentBillingInfoPerCompanyIdResult>();
            string CardNumber = string.Empty;
            try
            {
                ObjDbCollection = ObjBALPayment.GetPaymentBillingInfoByCompanyId(ComId);
                //if (ObjDbCollection.Count > 0)
                //{
                //    CardNumber = EncryptDecrypt.Decrypt(ObjDbCollection.ElementAt(0).CardNumber.ToString());
                //}
            }
            finally
            {
                ObjBALPayment = null;
            }

            return Json(new { CardNumber = CardNumber, ObjDbCollection = ObjDbCollection });
        }

        public ActionResult LoadState()
        {
            BALGeneral ObjBALGeneral = new BALGeneral();
            List<tblState> States = new List<tblState>();
            try
            {
                States = ObjBALGeneral.GetStates();

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ObjBALGeneral = null;
            }
            var StateNew = States.Select(d => new { pkStateId = d.pkStateId + "_" + d.StateName, StateName = d.StateName });
            return Json(new { States = StateNew });
        }

        public ActionResult MakePayment_Information(string CompanyId, string invoiceid)
        {
            //string Message = string.Empty;
            string content = string.Empty;
            BALCompany serObj = new BALCompany();
            //int CID = Convert.ToInt32(CompanyId);
            //int chkPaymentTerm = serObj.GetCompanyPaymentType(CID);


           // AdCompanyModel ObjModel = new AdCompanyModel();

            int locationid = Convert.ToInt32(invoiceid);
            var list = serObj.GetAllInvoicePerInvoice(Convert.ToInt32(CompanyId), locationid).FirstOrDefault();
            int BillingMonth = Convert.ToInt32(list.InvoiceMonth);
            int BillingYear = Convert.ToInt32(list.InvoiceYear);
            int invoiceidmain = Convert.ToInt32(list.pkInvoiceId);

            using (Emerge.Services.BALInvoiceQuickBook a = new Services.BALInvoiceQuickBook())
            {
                var PrdoctData = a.getAllRecord(Convert.ToInt32(CompanyId), BillingMonth, BillingYear, Convert.ToInt32(invoiceidmain));


                List<InvoiceRecordDiscoountPerOrderResult> InvoiceLevelData = new List<InvoiceRecordDiscoountPerOrderResult>();
                List<InvoiceLevelDiscountQuickBookResult> InvoiceLevelDiscountQuickBook = new List<InvoiceLevelDiscountQuickBookResult>();
                using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
                {
                    InvoiceLevelData = ObjDALDataContext.InvoiceRecordDiscoountPerOrder(Convert.ToInt32(CompanyId), BillingMonth, BillingYear, Convert.ToInt32(invoiceidmain)).ToList<InvoiceRecordDiscoountPerOrderResult>();

                    InvoiceLevelDiscountQuickBook = ObjDALDataContext.InvoiceLevelDiscountQuickBook(Convert.ToInt32(invoiceidmain)).ToList<InvoiceLevelDiscountQuickBookResult>();

                }

                // jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(data);
                if (PrdoctData != null)
                {

                    if (PrdoctData.Count > 0)
                    {

                        //EmergeDALDataContext myDB = new EmergeDALDataContext();
                        content = "<div style='float:right;'><img href></div>";
                        content += "</br><table align='Center' cellspacing='0' cellpadding='4' border='1' style='background-color: White;width:100%; border-collapse: collapse; float:left;' id='gvOpenInvoice' rules='all' class='Grid'>";
                        content += "<tbody><tr align='center' style='font-weight: bold;color: #0B55C4;' class='GridHeader'><th scope='col' style='width: 70px;text-align: center; padding: 5px 0px;'>Quantity</th><th scope='col' style='padding: 5px 0px;text-align: center;width: 90px;'>Product</th><th scope='col' style='text-align: center;padding: 5px 0px;'>Description</th><th scope='col' style='width: 50px;text-align: center;padding: 5px 0px;'>Price</th><th scope='col' style='width: 65px;padding: 5px 0px;text-align: center;'>Amount</th>";
                        decimal PrdoctData_Total = 0;
                        if (PrdoctData != null)
                        {
                            for (var i = 0; i < PrdoctData.Count; i++)
                            {
                                content += "<tr>";
                                content += "<td align='right' style='text-align: center;padding: 4px 0px;'>" + (PrdoctData.ElementAt(i).ProductQty).ToString() + "</td>";
                                content += "<td align='right' style='text-align: center;'>" + (PrdoctData.ElementAt(i).ProductDisplayName).ToString() + "</td>";
                                using (EmergeDALDataContext myDB = new EmergeDALDataContext())
                                {
                                    content += "<td align='right' style='text-align: center;'>" + (myDB.tblLocations.Where(m => m.pkLocationId == PrdoctData.ElementAt(i).fkLocationId).SingleOrDefault().City).ToString() + "</td>";
                                }
                                content += "<td align='right' style='text-align: center;'>" + "$" + (PrdoctData.ElementAt(i).ReportAmount).ToString() + "</td>";
                                content += "<td align='right' style='text-align: center;'>" + "$" + ((PrdoctData.ElementAt(i).ReportAmount * PrdoctData.ElementAt(i).ProductQty)).ToString() + "</td>";
                                content += "</tr>";

                                PrdoctData_Total = Convert.ToDecimal(PrdoctData_Total + (PrdoctData.ElementAt(i).ReportAmount * PrdoctData.ElementAt(i).ProductQty));

                            }
                        }


                        decimal InvoiceLevelData_Total = 0;
                        if (InvoiceLevelData != null)
                        {

                            for (var i = 0; i < InvoiceLevelData.Count; i++)
                            {
                                content += "<tr>";
                                content += "<td align='right' style='text-align: center;'>" + (1).ToString() + "</td>";
                                content += "<td align='right' style='text-align: center;'>" + (InvoiceLevelData.ElementAt(i).AdjustmentType).ToString() + "</td>";
                                content += "<td align='right' style='text-align: center;'>" + "Order No:" + InvoiceLevelData.ElementAt(i).OrderNo.ToString() + " Product:" + InvoiceLevelData.ElementAt(i).ProductDisplayName.ToString() + " Date:" + InvoiceLevelData.ElementAt(i).DiscountDate.ToString() + " Note:  " + InvoiceLevelData.ElementAt(i).Note.ToString() + "</td>";

                                if (InvoiceLevelData.ElementAt(i).AdjustmentType == "Discount")
                                {
                                    content += "<td align='right' style='text-align: center;'>" + "$" + "-" + (InvoiceLevelData.ElementAt(i).DiscountAmount).ToString() + "</td>";
                                    content += "<td align='right' style='text-align: center;'>" + "$" + "-" + (InvoiceLevelData.ElementAt(i).DiscountAmount).ToString() + "</td>";
                                    InvoiceLevelData_Total = Convert.ToDecimal(InvoiceLevelData_Total - (InvoiceLevelData.ElementAt(i).DiscountAmount));

                                }
                                else
                                {
                                    content += "<td align='right' style='text-align: center;'>" + "$" + (InvoiceLevelData.ElementAt(i).DiscountAmount).ToString() + "</td>";
                                    content += "<td align='right' style='text-align: center;'>" + "$" + (InvoiceLevelData.ElementAt(i).DiscountAmount).ToString() + "</td>";
                                    InvoiceLevelData_Total = Convert.ToDecimal(InvoiceLevelData_Total + (InvoiceLevelData.ElementAt(i).DiscountAmount));
                                }


                                content += "</tr>";
                            }
                        }

                        decimal InvoiceLevelDiscountQuickBook_Total = 0;
                        if (InvoiceLevelDiscountQuickBook != null)
                        {

                            for (var i = 0; i < InvoiceLevelDiscountQuickBook.Count; i++)
                            {
                                content += "<tr>";
                                content += "<td align='right' style='text-align: center;'>" + (1).ToString() + "</td>";
                                content += "<td align='right' style='text-align: center;'>" + (InvoiceLevelDiscountQuickBook.ElementAt(i).AdjustmentType).ToString() + "</td>";
                                content += "<td align='right' style='text-align: center;'>" + ("Invoice Data").ToString() + "</td>";

                                if (InvoiceLevelDiscountQuickBook.ElementAt(i).AdjustmentType == "Discount")
                                {
                                    content += "<td align='right' style='text-align: center;'>" + "$" + "-" + InvoiceLevelDiscountQuickBook.ElementAt(i).DiscountAmount + "</td>";
                                    content += "<td align='right' style='text-align: center;'>" + "$" + "-" + (InvoiceLevelDiscountQuickBook.ElementAt(i).DiscountAmount).ToString() + "</td>";
                                    InvoiceLevelDiscountQuickBook_Total = Convert.ToDecimal(InvoiceLevelDiscountQuickBook_Total - (InvoiceLevelDiscountQuickBook.ElementAt(i).DiscountAmount));
                                }
                                else
                                {
                                    content += "<td align='right' style='text-align: center;'>" + "$" + InvoiceLevelDiscountQuickBook.ElementAt(i).DiscountAmount + "</td>";
                                    content += "<td align='right' style='text-align: center;'>" + "$" + (InvoiceLevelDiscountQuickBook.ElementAt(i).DiscountAmount).ToString() + "</td>";
                                    InvoiceLevelDiscountQuickBook_Total = Convert.ToDecimal(InvoiceLevelDiscountQuickBook_Total + (InvoiceLevelDiscountQuickBook.ElementAt(i).DiscountAmount));
                                }

                                content += "</tr>";

                            }
                        }


                        content += "<tr>";
                        content += "<tr><td colspan='4' align='right'>Total Amount :</td>";
                        content += "<td align='right' style='text-align: center;'>" + "$" + Convert.ToDecimal(PrdoctData_Total + InvoiceLevelData_Total + InvoiceLevelDiscountQuickBook_Total) + "</td>";
                        content += "</tr>";

                        content += "</tbody>";
                        content += "</table>";




                    }
                }
            }
            //List<proc_LoadOpenInvoiceByCompanyIdResult> list = new List<proc_LoadOpenInvoiceByCompanyIdResult>();
            //list = serObj.LoadOpenInVoice(CID).ToList();
            return Json(new { content = content });
        }


        public ActionResult MakePayment(FormCollection frm)
        {
            int TranErrorMsgCount = 0;
            long pkInvoiceId;
            string Message = string.Empty;
            string[] rdbPaymentType = frm.GetValue("rdbPaymentType").AttemptedValue.Split(',');
            string[] OutStandingAmount = frm.GetValue("OutStandingAmount").AttemptedValue.Split(',');
            string[] ddlOpenInvoice = frm.GetValue("ddlOpenInvoice").AttemptedValue.Split(',');
            string[] txtOtherAmount = frm.GetValue("txtOtherAmount").AttemptedValue.Split(',');
            string[] chckPayment = frm.GetValue("chckPayment").AttemptedValue.Split(',');
            if (rdbPaymentType[0] == "1")
            {
                for (int num = 0; num < chckPayment.Length; num++)
                {
                    pkInvoiceId = Convert.ToInt64(chckPayment[num]);
                    Message = MakePay(Convert.ToDecimal(GetInvoiceAmount(pkInvoiceId)), false, pkInvoiceId, GetInvoiceNumber(pkInvoiceId), frm, TranErrorMsgCount);
                }
            }
            if (rdbPaymentType[0] == "2")
            {
                pkInvoiceId = 0;
                Message = MakePay(Convert.ToDecimal(OutStandingAmount[0]), true, pkInvoiceId, "", frm, TranErrorMsgCount);
            }
            if (rdbPaymentType[0] == "3")
            {
                pkInvoiceId = Convert.ToInt64(ddlOpenInvoice[0].ToString());
                Message = MakePay(Convert.ToDecimal(txtOtherAmount[0]), false, pkInvoiceId, GetInvoiceNumber(pkInvoiceId), frm, TranErrorMsgCount);
            }
            return Json(Message);
        }

        private string GetInvoiceNumber(long pkInvoiceId)
        {
            string InvoiceNumber = "";
            BALInvoice ObjBALInvoice = new BALInvoice();
            try
            {
                List<tblInvoice> ObjList = new List<tblInvoice>();
                ObjList = ObjBALInvoice.GetInvoiceNumber(pkInvoiceId);
                InvoiceNumber = ObjList.ElementAt(0).InvoiceNumber.ToString();
            }
            finally
            {
                ObjBALInvoice = null;
            }
            return InvoiceNumber;
        }

        private string GetInvoiceAmount(long pkInvoiceId)
        {
            string InvoiceBalAmount = "0";
            BALInvoice ObjBALInvoice = new BALInvoice();
            try
            {
                List<tblInvoice> ObjList = new List<tblInvoice>();
                ObjList = ObjBALInvoice.GetInvoiceBalanceAmount(pkInvoiceId);
                decimal Total = 0;//ObjList.ElementAt(0).InvoiceAmount- ObjList.ElementAt(0).TotalPaidAmount;
                InvoiceBalAmount = Total.ToString();
            }
            finally
            {
                ObjBALInvoice = null;
            }
            return InvoiceBalAmount;
        }


        private string MakePay(decimal PayInvoiceAmount, bool PayALLInvoice, long pkInvoiceId, string strInvoiceNumber, FormCollection frm, int TranErrorMsgCount)
        {
            StringBuilder ObjStringBuilder1 = new StringBuilder();
            BALInvoice ObjBALInvoice = new BALInvoice();
            try
            {
                string APILoginId = ConfigurationManager.AppSettings["APILoginID"];
                string TransactionKey = ConfigurationManager.AppSettings["TransactionKey"];
                bool TestMode = Convert.ToBoolean(ConfigurationManager.AppSettings["TestMode"]);
                Gateway gateway = new Gateway(APILoginId, TransactionKey, TestMode);
                decimal PayAmount = PayInvoiceAmount;

                string[] ddlCardExpiryMonth = frm.GetValue("ddlCardExpiryMonth").AttemptedValue.Split(',');
                string[] ddlCardExpiryYear = frm.GetValue("ddlCardExpiryYear").AttemptedValue.Split(',');
                string[] txtCardumber = frm.GetValue("txtCardumber").AttemptedValue.Split(',');

                string[] txtFname = frm.GetValue("txtFname").AttemptedValue.Split(',');
                string[] txtLname = frm.GetValue("txtLname").AttemptedValue.Split(',');
                string[] txtComapny = frm.GetValue("txtComapny").AttemptedValue.Split(',');
                string[] txtAddress = frm.GetValue("txtAddress").AttemptedValue.Split(',');
                string[] txtCity = frm.GetValue("txtCity").AttemptedValue.Split(',');
                string[] ddlState = frm.GetValue("ddlState").AttemptedValue.Split(',');
                string[] ddlCountry = frm.GetValue("ddlCountry").AttemptedValue.Split(',');
                string[] txtEmail = frm.GetValue("txtEmail").AttemptedValue.Split(',');
                string[] txtZip = frm.GetValue("txtZip").AttemptedValue.Split(',');
                string[] CompanyId = frm.GetValue("CompanyId").AttemptedValue.Split(',');

                int ExpiryMonth = Convert.ToInt32(ddlCardExpiryMonth[0]);
                int ExpityYear = Convert.ToInt32(ddlCardExpiryYear[0]);
                string Monthyear = ExpityYear + "-" + ExpiryMonth;
                AuthorizationRequest obj = new AuthorizationRequest(HttpUtility.HtmlEncode(txtCardumber[0].Trim()), Monthyear, PayAmount, "desc");
                obj.FirstName = HttpUtility.HtmlEncode(txtFname[0].Trim());
                obj.LastName = HttpUtility.HtmlEncode(txtLname[0].Trim());
                obj.Company = HttpUtility.HtmlEncode(txtComapny[0].Trim());
                obj.Address = HttpUtility.HtmlEncode(txtAddress[0].Trim());
                obj.City = HttpUtility.HtmlEncode(txtCity[0].Trim());
                obj.State = ddlState[0].Split('_')[1];
                obj.Zip = HttpUtility.HtmlEncode(txtZip[0].Trim());
                obj.Country = ddlCountry[0];
                obj.Email = HttpUtility.HtmlEncode(txtEmail[0].Trim());
                var Result = gateway.Send(obj);
                if (Result.Approved == true)
                {
                    tblPaymentHistory ObjtblPaymentHistory = new tblPaymentHistory();
                    ObjtblPaymentHistory.Address = txtAddress[0].Trim();
                    ObjtblPaymentHistory.CardExpiryMonth = Convert.ToInt16(ddlCardExpiryMonth[0]);
                    ObjtblPaymentHistory.CardExpiryYear = Convert.ToInt16(ddlCardExpiryYear[0]);
                    ObjtblPaymentHistory.CardNumber = EncryptDecrypt.Encrypt(txtCardumber[0].Trim());
                    ObjtblPaymentHistory.City = txtCity[0].Trim();
                    ObjtblPaymentHistory.CompanyName = txtComapny[0].Trim();
                    ObjtblPaymentHistory.CreatedDateTime = DateTime.Now;
                    ObjtblPaymentHistory.Email = txtEmail[0].Trim();
                    ObjtblPaymentHistory.FirstName = txtFname[0].Trim();
                    ObjtblPaymentHistory.fkCompanyId = Convert.ToInt32(CompanyId[0]);
                    ObjtblPaymentHistory.fkInvoiceId = pkInvoiceId;
                    Guid ObjGuid = GetCurrentUserDetail().First().UserId;
                    ObjtblPaymentHistory.fkCompanyUserId = ObjGuid;
                    ObjtblPaymentHistory.fkCountryId = 1;
                    ObjtblPaymentHistory.fkStateId = Convert.ToInt16(ddlState[0].Split('_')[0]);
                    ObjtblPaymentHistory.LastName = txtLname[0].Trim();
                    ObjtblPaymentHistory.PaidAmount = PayAmount;
                    ObjtblPaymentHistory.PaidDate = DateTime.Now;
                    ObjtblPaymentHistory.Zipcode = txtZip[0].Trim();
                    ObjtblPaymentHistory.TransactionId = Result.TransactionID.ToString();
                    int Success = 0;
                    if (PayALLInvoice == true)
                    {
                        Success = ObjBALInvoice.AddPaymentHistoryDetails(ObjtblPaymentHistory, false, true); /*pay all invoice*/
                    }
                    else
                    {
                        Success = ObjBALInvoice.AddPaymentHistoryDetails(ObjtblPaymentHistory, false, false); /*pay invoice*/
                    }
                    if (Success == 1)
                    {
                        //   LoadOpenInvoice();
                        //  TranSuccMsgCount++;
                        ObjStringBuilder1.Append("<tr><td>");
                        if (PayALLInvoice == true)
                        {
                            ObjStringBuilder1.Append("Your transaction is completed successfully at authorize.net");
                        }
                        else
                        {
                            ObjStringBuilder1.Append("Your transaction is completed successfully at authorize.net for Invoice#: " + strInvoiceNumber + ". Your transaction number is " + Result.TransactionID.ToString());
                        }
                        ObjStringBuilder1.Append("</tr></td>");
                    }
                    else
                    {
                        TranErrorMsgCount++;
                        ObjStringBuilder1.Append("<tr><td>");
                        if (PayALLInvoice == true)
                        {
                            ObjStringBuilder1.Append("Your transaction is completed successfully at authorize.net. Your transaction number is  " + Result.TransactionID.ToString() + ". But an error occurred while saving payment details.");
                        }
                        else
                        {
                            ObjStringBuilder1.Append("Your transaction is completed successfully at authorize.net for Invoice#: " + strInvoiceNumber + ". Your transaction number is " + Result.TransactionID.ToString() + ". But an error occurred while saving payment details.");
                        }
                        ObjStringBuilder1.Append("</tr></td>");
                    }
                    //  MultiViewPayInvoice.ActiveViewIndex = 2;
                    // UPPayInvoice.Update();

                }
                else
                {
                    TranErrorMsgCount++;
                    ObjStringBuilder1.Append("<tr><td>");
                    if (PayALLInvoice == true)
                    {
                        ObjStringBuilder1.Append(Result.Message);
                    }
                    else
                    {
                        ObjStringBuilder1.Append(Result.Message.Replace(".", "") + " for Invoice#:  " + strInvoiceNumber);
                    }
                    ObjStringBuilder1.Append("</tr></td>");
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                ObjBALInvoice = null;
            }
            return ObjStringBuilder1.ToString();
        }


        private List<Proc_Get_UserInfoByMemberShipUserIdResult> GetCurrentUserDetail()
        {
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjDbCollection;
            try
            {
                Guid UserId = Guid.Empty;
                MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
                if (ObjMembershipUser != null)
                {
                    UserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
                }
                ObjDbCollection = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(UserId);
            }
            finally
            {
                ObjBALCompanyUsers = null;
            }
            return ObjDbCollection;
        }


        public ActionResult GetAllRecords()
        {
           // AdCompanyModel ObjModel = new AdCompanyModel();
            BALCompany serObj = new BALCompany();
            int locationid = 0;
            var list = serObj.GetAllReportsLocationId(locationid);
            return Json(list);
        }

        [HttpPost]
        public ActionResult UpdateDataSource(IEnumerable<Proc_Get_EmergeProductPackagesForAdminResult> products)
        {
            //AdCompanyModel objModel = new AdCompanyModel();
            //BALCompany balobj = new BALCompany();

           // foreach (var reflist in products)
            //{
                //tblCompany_Price objtblcompanyprice = new tblCompany_Price();
                //objtblcompanyprice.CompanyPrice = reflist.CompanyPrice;
                //objtblcompanyprice.fkCompanyId = reflist.fkCompanyId;
            //}

            return View();

        }


        /// <summary>
        /// To Save package visibility
        /// </summary>
        /// <param name="packId"></param>
        /// <param name="isvisible"></param>
        public int SavePackagevisibility(int packId, bool isvisible)
        {
            return new BALCompany().SavePackageVisiblityonDB(packId, isvisible);
        }






        public ActionResult GetCategoryList()
        {
            BALCompany ObjBal = new BALCompany();
            var list = ObjBal.GetReportCategories();
            return Json(list);
        }

        public ActionResult GetSaleValueForGraph()
        {
            BALCompany ObjBal = new BALCompany();
           // AdCompanyModel objmodel = new AdCompanyModel();
            List<AdCompanyModel> SaleChart = new List<AdCompanyModel>();
           // List<proc_GetSalesValuesforGraphResult> SaleChart1 = new List<proc_GetSalesValuesforGraphResult>();
            DateTime ObjDateTime = DateTime.Now.AddMonths(-11);
            // Guid pkCompanyId1 = Guid.Parse(Session["NewPkCompanyId"].ToString());
            int pkCompanyId1 = Convert.ToInt32(Session["pkCompanyIdWhenEdit"].ToString());
            //int iCount = 0;
            if (Session["pkCompanyIdWhenEdit"] == null)
            {
                return null;
            }
            else
            {
                for (int i = 0; i < 12; i++)
                {

                    AdCompanyModel obj = new AdCompanyModel();
                    obj.MonthYear = ObjDateTime.AddMonths(i).ToString("MMM, yyyy");
                    obj.value = i;

                    obj.TotalSale = ObjBal.GetSalesValuesforGraph(pkCompanyId1, Convert.ToInt16(ObjDateTime.AddMonths(i).Month), Convert.ToInt16(ObjDateTime.AddMonths(i).Year)).FirstOrDefault().TotalAmount;
                    obj.Productsale = ObjBal.GetSalesValuesforGraph(pkCompanyId1, Convert.ToInt16(ObjDateTime.AddMonths(i).Month), Convert.ToInt16(ObjDateTime.AddMonths(i).Year)).FirstOrDefault().ProductAmount;
                    obj.ReportSale = ObjBal.GetSalesValuesforGraph(pkCompanyId1, Convert.ToInt16(ObjDateTime.AddMonths(i).Month), Convert.ToInt16(ObjDateTime.AddMonths(i).Year)).FirstOrDefault().ReportAmount;
                    SaleChart.Add(obj);


                }
                return Json(SaleChart);
            }

        }

        public ActionResult GetReportByCategoryId(byte pkCategoryId, int CompanyId, string LocationId)
        {
            // byte pkid = Convert.ToByte(pkId);
            List<SubReportsModel> data = new List<SubReportsModel>();
            int locationbyCategioryId = Convert.ToInt32(LocationId);
            //Guid fkcompanyid = Guid.Parse(Session["NewPkCompanyId"].ToString());
            if (CompanyId != null && CompanyId != 0)
            {
                BALCompany serobj = new BALCompany();
                var List = serobj.GetReportByIdatEdit(locationbyCategioryId, pkCategoryId, CompanyId);
                if (List.Count > 0)
                {
                    data = List.Select((x, index) => new SubReportsModel
                    {
                        pkProductApplicationId = x.pkProductApplicationId,
                        pkProductId = x.pkProductId,
                        ProductAccessId = x.ProductAccessId,
                        AutoReviewStatusForLocation = (x.AutoReviewStatusForLocation != null) ? x.AutoReviewStatusForLocation.ToString() : "0",
                        ProductDisplayName = x.ProductDisplayName,
                        ProductName = x.ProductName,
                        ProductCode = x.ProductCode,
                        ProductDescription = x.ProductDescription,
                        CategoryName = x.CategoryName,
                        CompanyPrice = (x.CompanyPrice != null) ? decimal.Parse(x.CompanyPrice.ToString()) : 0,
                        CoveragePDFPath = x.CoveragePDFPath,
                        IsDefault = x.IsProductEnabled,
                        IsDefaultPrice = x.IsDefaultPrice != null ? x.IsDefaultPrice : true,
                        IsLiveRunnerLocation = x.IsLiveRunnerLocation,
                        IsLiveRunnerProduct = x.IsLiveRunnerProduct,
                        IsThirdPartyFee = x.IsThirdPartyFee,
                        EnableReviewPerCompany = (x.EnableReviewPerCompany == null) ? false : bool.Parse(x.EnableReviewPerCompany.ToString()),
                        LinkedReportId = x.LinkedReportId,
                        ReportsLinked = x.ReportsLinked,
                        IsEdit = true,
                        CategoryId = pkCategoryId,
                        Index = index
                    }).OrderBy(x => x.ProductCode).ToList();
                }
                // var List = "";
                return Json(data);
            }
            else
            {
                BALCompany serobj = new BALCompany();
                var List = serobj.GetReportProductsById(locationbyCategioryId, pkCategoryId);
                if (List.Count > 0)
                {
                    data = List.Select((x, index) => new SubReportsModel
                    {
                        pkProductApplicationId = x.pkProductApplicationId,
                        pkProductId = x.pkProductId,
                        ProductAccessId = x.ProductAccessId,
                        AutoReviewStatusForLocation = (x.AutoReviewStatusForLocation != null) ? x.AutoReviewStatusForLocation.ToString() : "0",
                        ProductDisplayName = x.ProductDisplayName,
                        ProductName = x.ProductName,
                        ProductCode = x.ProductCode,
                        ProductDescription = x.ProductDescription,
                        CategoryName = x.CategoryName,
                        CompanyPrice = x.CompanyPrice,
                        CoveragePDFPath = x.CoveragePDFPath,
                        IsDefault = x.IsDefault,
                        IsDefaultPrice = x.IsDefaultPrice != null ? x.IsDefaultPrice : true,
                        IsLiveRunnerLocation = x.IsLiveRunnerLocation != null ? x.IsLiveRunnerLocation : x.IsLiveRunnerProduct,
                        IsLiveRunnerProduct = x.IsLiveRunnerProduct,
                        IsThirdPartyFee = x.IsThirdPartyFee,
                        LinkedReportId = x.LinkedReportId,
                        ReportsLinked = x.ReportsLinked,
                        IsEdit = false,
                        CategoryId = pkCategoryId,
                        Index = index
                    }).OrderBy(x => x.ProductCode).ToList();
                }
                // var List = "";
                return Json(data);

            }
            //objmodel=
        }

        [HttpPost]
        public ActionResult UpdateSubReport(IEnumerable<SubReportsModel> Detail, string GlobalCredential)
        {


            List<tblCompany_Price> listtblCompany_Price = new List<tblCompany_Price>();
            List<tblProductAccess> listtblProductAccess = new List<tblProductAccess>();
            List<tblProductAccess> deletetblProductAccess = new List<tblProductAccess>();
            BALCompany ObjBALCompany = new BALCompany();
            BALLocation ObjBALLocation = new BALLocation();
            int Location = Convert.ToInt32(Session["pkCompLocationId"]) != 0 ? Convert.ToInt32(Session["pkCompLocationId"]) : 0;
            int Company = Convert.ToInt32(Session["NewPkCompanyId"]) != 0 ? Convert.ToInt32(Session["NewPkCompanyId"]) : 0;
            if (Detail != null)
            {
                var DetailforSingleLocation = Detail.Where(x => x.MonthlySaving == 1).FirstOrDefault();
                if (DetailforSingleLocation == null)
                {
                    for (int iCol = 0; iCol < Detail.Count(); iCol++)
                    {
                        if (Detail.ElementAt(iCol).IsDefault != null && Convert.ToBoolean(Detail.ElementAt(iCol).IsDefault) || (Convert.ToBoolean(!Detail.ElementAt(iCol).IsDefault) && byte.Parse(Detail.ElementAt(iCol).AutoReviewStatusForLocation) != 0))
                        {
                            tblProductAccess ObjtblProductAccess = new tblProductAccess();
                            ObjtblProductAccess.pkProductAccessId = Detail.ElementAt(iCol).ProductAccessId.GetValueOrDefault() != 0 ? Detail.ElementAt(iCol).ProductAccessId.GetValueOrDefault() : 0;
                            ObjtblProductAccess.fkLocationId = Location;
                            ObjtblProductAccess.fkProductApplicationId = Detail.ElementAt(iCol).pkProductApplicationId;
                            ObjtblProductAccess.IsLiveRunner = (Detail.ElementAt(iCol).IsLiveRunnerLocation != null) ? Convert.ToBoolean(Detail.ElementAt(iCol).IsLiveRunnerLocation) : false;
                            ObjtblProductAccess.ProductReviewStatus = (Detail.ElementAt(iCol).AutoReviewStatusForLocation != null) ? byte.Parse(Detail.ElementAt(iCol).AutoReviewStatusForLocation) : byte.MinValue;
                            ObjtblProductAccess.IsProductEnabled = Detail.ElementAt(iCol).IsDefault != null ? Convert.ToBoolean(Detail.ElementAt(iCol).IsDefault) : false;
                            ObjtblProductAccess.LocationProductPrice = Convert.ToDecimal(Detail.ElementAt(iCol).CompanyPrice);
                            listtblProductAccess.Add(ObjtblProductAccess);
                        }
                        else
                        {
                            tblProductAccess ObjtblProductAccess = new tblProductAccess();
                            ObjtblProductAccess.pkProductAccessId = Detail.ElementAt(iCol).ProductAccessId.GetValueOrDefault() != 0 ? Detail.ElementAt(iCol).ProductAccessId.GetValueOrDefault() : 0;
                            ObjtblProductAccess.fkLocationId = Location;
                            ObjtblProductAccess.fkProductApplicationId = Detail.ElementAt(iCol).pkProductApplicationId;
                            ObjtblProductAccess.IsLiveRunner = (Detail.ElementAt(iCol).IsLiveRunnerLocation != null) ? Convert.ToBoolean(Detail.ElementAt(iCol).IsLiveRunnerLocation) : false;
                            ObjtblProductAccess.ProductReviewStatus = (Detail.ElementAt(iCol).AutoReviewStatusForLocation != null) ? byte.Parse(Detail.ElementAt(iCol).AutoReviewStatusForLocation) : byte.MinValue;
                            ObjtblProductAccess.IsProductEnabled = Detail.ElementAt(iCol).IsDefault != null ? Convert.ToBoolean(Detail.ElementAt(iCol).IsDefault) : false;
                            ObjtblProductAccess.LocationProductPrice = Convert.ToDecimal(Detail.ElementAt(iCol).CompanyPrice);
                            deletetblProductAccess.Add(ObjtblProductAccess);

                        }

                        tblCompany_Price ObjtblCompany_Price = new tblCompany_Price();

                        ObjtblCompany_Price.fkCompanyId = Company;
                        ObjtblCompany_Price.IsDefaultPrice = Convert.ToBoolean(Detail.ElementAt(iCol).IsDefaultPrice);
                        ObjtblCompany_Price.CompanyPrice = Convert.ToDecimal(Detail.ElementAt(iCol).CompanyPrice);
                        //ObjtblCompany_Price.fkCompanyId = new Guid(CID);
                        ObjtblCompany_Price.fkProductApplicationId = Detail.ElementAt(iCol).pkProductApplicationId;

                        // #19
                        // ObjtblCompany_Price.pkCompanyPriceId = Guid.NewGuid();

                        // ObjtblCompany_Price. = Detail.ElementAt(iCol).AutoReviewStatusForLocation;
                        ObjtblCompany_Price.EnableReviewPerCompany = Detail.ElementAt(iCol).EnableReviewPerCompany;
                        if (Detail.ElementAt(iCol).LinkedReportId != 0)
                        {
                            ObjtblCompany_Price.LinkedReportId = Detail.ElementAt(iCol).LinkedReportId;
                        }
                        else
                        {
                            ObjtblCompany_Price.LinkedReportId = null;
                        }

                        listtblCompany_Price.Add(ObjtblCompany_Price);
                    }
                    //ObjBALLocation.DeleteProductsOfLocation(Location);
                    ObjBALLocation.InsertUpdateLocationProduct(listtblProductAccess, deletetblProductAccess);
                    ObjBALCompany.AddProductPricePerCompany(listtblCompany_Price);
                    if (GlobalCredential.ToLower() == "true")
                    {
                       // int PriceSuccess = ObjBALCompany.UpdateDataSourceSettingsForLocation(Company, Location);
                    }
                }
                else
                {
                    UpdateSubReportAllLocation(Detail, GlobalCredential);
                }
            }
            return View();

            //return RedirectToAction("AddCompanies", "Control", new { id =123});

        }


        public void UpdateSubReportAllLocation(IEnumerable<SubReportsModel> Detail, string GlobalCredential)
        {
            List<tblCompany_Price> listtblCompany_Price = new List<tblCompany_Price>();
            List<tblProductAccess> listtblProductAccess = new List<tblProductAccess>();
            List<tblProductAccess> deletetblProductAccess = new List<tblProductAccess>();
            BALCompany ObjBALCompany = new BALCompany();
            BALLocation ObjBALLocation = new BALLocation();
            int Location = Convert.ToInt32(Session["pkCompLocationId"]) != 0 ? Convert.ToInt32(Session["pkCompLocationId"]) : 0;
            int Company = Convert.ToInt32(Session["NewPkCompanyId"]) != 0 ? Convert.ToInt32(Session["NewPkCompanyId"]) : 0;
            //clsProductAccessDetail obj = new clsProductAccessDetail();


            for (int iCol = 0; iCol < Detail.Count(); iCol++)
            {
                //obj = ObjBALLocation.GetProductAccessDetail(Detail.ElementAt(iCol).ProductCode, Company, Location);
                if (Detail.ElementAt(iCol).IsDefault != null && Convert.ToBoolean(Detail.ElementAt(iCol).IsDefault) || (Convert.ToBoolean(!Detail.ElementAt(iCol).IsDefault) && byte.Parse(Detail.ElementAt(iCol).AutoReviewStatusForLocation) != 0))
                {
                    tblProductAccess ObjtblProductAccess = new tblProductAccess();
                    ObjtblProductAccess.pkProductAccessId = Detail.ElementAt(iCol).ProductAccessId.GetValueOrDefault() != 0 ? Detail.ElementAt(iCol).ProductAccessId.GetValueOrDefault() : 0;
                    ObjtblProductAccess.fkLocationId = Location;
                    ObjtblProductAccess.fkProductApplicationId = Detail.ElementAt(iCol).pkProductApplicationId;
                    ObjtblProductAccess.IsLiveRunner = (Detail.ElementAt(iCol).IsLiveRunnerLocation != null) ? Convert.ToBoolean(Detail.ElementAt(iCol).IsLiveRunnerLocation) : false;
                    ObjtblProductAccess.ProductReviewStatus = (Detail.ElementAt(iCol).AutoReviewStatusForLocation != null) ? byte.Parse(Detail.ElementAt(iCol).AutoReviewStatusForLocation) : byte.MinValue;
                    ObjtblProductAccess.IsProductEnabled = Detail.ElementAt(iCol).IsDefault != null ? Convert.ToBoolean(Detail.ElementAt(iCol).IsDefault) : false;
                    ObjtblProductAccess.LocationProductPrice = Convert.ToDecimal(Detail.ElementAt(iCol).CompanyPrice);
                    listtblProductAccess.Add(ObjtblProductAccess);
                }
                else
                {
                    tblProductAccess ObjtblProductAccess = new tblProductAccess();
                    ObjtblProductAccess.pkProductAccessId = Detail.ElementAt(iCol).ProductAccessId.GetValueOrDefault() != 0 ? Detail.ElementAt(iCol).ProductAccessId.GetValueOrDefault() : 0;
                    ObjtblProductAccess.fkLocationId = Location;
                    ObjtblProductAccess.fkProductApplicationId = Detail.ElementAt(iCol).pkProductApplicationId;
                    ObjtblProductAccess.IsLiveRunner = (Detail.ElementAt(iCol).IsLiveRunnerLocation != null) ? Convert.ToBoolean(Detail.ElementAt(iCol).IsLiveRunnerLocation) : false;
                    ObjtblProductAccess.ProductReviewStatus = (Detail.ElementAt(iCol).AutoReviewStatusForLocation != null) ? byte.Parse(Detail.ElementAt(iCol).AutoReviewStatusForLocation) : byte.MinValue;
                    ObjtblProductAccess.IsProductEnabled = Detail.ElementAt(iCol).IsDefault != null ? Convert.ToBoolean(Detail.ElementAt(iCol).IsDefault) : false;
                    ObjtblProductAccess.LocationProductPrice = Convert.ToDecimal(Detail.ElementAt(iCol).CompanyPrice);
                    deletetblProductAccess.Add(ObjtblProductAccess);

                }

                tblCompany_Price ObjtblCompany_Price = new tblCompany_Price();

                ObjtblCompany_Price.fkCompanyId = Company;
                ObjtblCompany_Price.IsDefaultPrice = Convert.ToBoolean(Detail.ElementAt(iCol).IsDefaultPrice);
                ObjtblCompany_Price.CompanyPrice = Convert.ToDecimal(Detail.ElementAt(iCol).CompanyPrice);
                ObjtblCompany_Price.fkProductApplicationId = Detail.ElementAt(iCol).pkProductApplicationId;
                ObjtblCompany_Price.EnableReviewPerCompany = Detail.ElementAt(iCol).EnableReviewPerCompany;
                if (Detail.ElementAt(iCol).LinkedReportId != 0)
                {
                    ObjtblCompany_Price.LinkedReportId = Detail.ElementAt(iCol).LinkedReportId;
                }
                else
                {
                    ObjtblCompany_Price.LinkedReportId = null;
                }

                listtblCompany_Price.Add(ObjtblCompany_Price);
            }
            ObjBALLocation.InsertUpdateAllLocationProduct(listtblProductAccess, deletetblProductAccess, Company);
            ObjBALCompany.AddProductPricePerCompany(listtblCompany_Price);
            if (GlobalCredential.ToLower() == "true")
            {
               // int PriceSuccess = ObjBALCompany.UpdateDataSourceSettingsForLocation(Company, Location);
            }
        }





        //INT197
        public JsonResult UpdateSubReportToallLocation(string ProductCode, string GlobalCredential, bool IsDefault, decimal CompanyPrice, byte Reviews, bool IsLiveRunnerLocation)
        {
            List<tblCompany_Price> listtblCompany_Price = new List<tblCompany_Price>();
            List<tblProductAccess> listtblProductAccess = new List<tblProductAccess>();
            List<tblProductAccess> deletetblProductAccess = new List<tblProductAccess>();
            BALCompany ObjBALCompany = new BALCompany();
            BALLocation ObjBALLocation = new BALLocation();
            int Location = Convert.ToInt32(Session["pkCompLocationId"]) != 0 ? Convert.ToInt32(Session["pkCompLocationId"]) : 0;
            int Company = Convert.ToInt32(Session["NewPkCompanyId"]) != 0 ? Convert.ToInt32(Session["NewPkCompanyId"]) : 0;
            clsProductAccessDetail obj = new clsProductAccessDetail();
            obj = ObjBALLocation.GetProductAccessDetail(ProductCode, Company, Location);

            if (obj != null)
            {
                if (IsDefault != null && Convert.ToBoolean(IsDefault))
                {
                    tblProductAccess ObjtblProductAccess = new tblProductAccess();
                    ObjtblProductAccess.pkProductAccessId = obj._pkProductAccessId;
                    ObjtblProductAccess.fkLocationId = Location;
                    ObjtblProductAccess.fkProductApplicationId = obj._fkProductApplicationId;
                    if (IsLiveRunnerLocation == obj._IsLiveRunner)
                    {
                        ObjtblProductAccess.IsLiveRunner = obj._IsLiveRunner;
                    }
                    else
                    {
                        ObjtblProductAccess.IsLiveRunner = IsLiveRunnerLocation;
                    }
                    if (Reviews == obj._ProductReviewStatus)
                    {
                        ObjtblProductAccess.ProductReviewStatus = obj._ProductReviewStatus;
                    }
                    else
                    {
                        ObjtblProductAccess.ProductReviewStatus = Reviews;
                    }
                    ObjtblProductAccess.IsProductEnabled = obj._IsProductEnabled;
                    if (Convert.ToDecimal(CompanyPrice) == Convert.ToDecimal(obj._LocationProductPrice))
                    {
                        ObjtblProductAccess.LocationProductPrice = Convert.ToDecimal(obj._LocationProductPrice);
                    }
                    else
                    {
                        ObjtblProductAccess.LocationProductPrice = Convert.ToDecimal(CompanyPrice);
                    }
                    listtblProductAccess.Add(ObjtblProductAccess);
                }
                else
                {
                    tblProductAccess ObjtblProductAccess = new tblProductAccess();
                    ObjtblProductAccess.pkProductAccessId = obj._pkProductAccessId;
                    ObjtblProductAccess.fkLocationId = Location;
                    ObjtblProductAccess.fkProductApplicationId = obj._fkProductApplicationId;
                    ObjtblProductAccess.IsLiveRunner = obj._IsLiveRunner;
                    ObjtblProductAccess.ProductReviewStatus = obj._ProductReviewStatus;
                    ObjtblProductAccess.IsProductEnabled = obj._IsProductEnabled;
                    ObjtblProductAccess.LocationProductPrice = Convert.ToDecimal(obj._LocationProductPrice);
                    deletetblProductAccess.Add(ObjtblProductAccess);

                }

                tblCompany_Price ObjtblCompany_Price = new tblCompany_Price();

                ObjtblCompany_Price.fkCompanyId = Company;
                ObjtblCompany_Price.IsDefaultPrice = Convert.ToBoolean(obj._IsDefaultPrice);
                if (Convert.ToDecimal(CompanyPrice) == Convert.ToDecimal(obj._CompanyPrice))
                {
                    ObjtblCompany_Price.CompanyPrice = Convert.ToDecimal(obj._CompanyPrice);
                }
                else
                {
                    ObjtblCompany_Price.CompanyPrice = Convert.ToDecimal(CompanyPrice);
                }
                ObjtblCompany_Price.fkProductApplicationId = obj._fkProductApplicationId;
                ObjtblCompany_Price.EnableReviewPerCompany = obj._EnableReviewPerCompany;
                if (obj._LinkedReportId != 0)
                {
                    ObjtblCompany_Price.LinkedReportId = obj._LinkedReportId;
                }
                else
                {
                    ObjtblCompany_Price.LinkedReportId = null;
                }
                listtblCompany_Price.Add(ObjtblCompany_Price);
                ObjBALLocation.InsertUpdateAllLocationProduct(listtblProductAccess, deletetblProductAccess, Company);
                ObjBALCompany.AddProductPricePerCompany(listtblCompany_Price);
                if (GlobalCredential.ToLower() == "true")
                {
                    //int PriceSuccess = 
                        ObjBALCompany.UpdateDataSourceSettingsForLocation(Company, Location);
                }

            }


            return Json("Save", JsonRequestBehavior.AllowGet);
        }




        [HttpPost]
        public ActionResult SaveDataSourse(string Data)
        {
           // System.Web.Script.Serialization.JavaScriptSerializer jss = new System.Web.Script.Serialization.JavaScriptSerializer();
            //List<SubReportsModel> lst0 = jss.Deserialize<List<SubReportsModel>>(Data);
            //List<tblCompany_Price> listtblCompany_Price = new List<tblCompany_Price>();
            //List<tblProductAccess> listtblProductAccess = new List<tblProductAccess>();
            //BALCompany ObjBALCompany = new BALCompany();
            //BALLocation ObjBALLocation = new BALLocation();
            //if (Detail != null)
            //{
            //    for (int iCol = 0; iCol < Detail.Count(); iCol++)
            //    {
            //        //if (Detail.ElementAt(iCol).IsLiveRunnerProduct)
            //        //{
            //        tblProductAccess ObjtblProductAccess = new tblProductAccess();
            //        ObjtblProductAccess.pkProductAccessId = (Detail.ElementAt(iCol).ProductAccessId != null) ? new Guid(Detail.ElementAt(iCol).ProductAccessId.ToString()) : Guid.Empty;
            //        ObjtblProductAccess.fkLocationId = (Session["pkCompLocationId"] != null) ? new Guid(Session["pkCompLocationId"].ToString()) : Guid.Empty;
            //        ObjtblProductAccess.fkProductApplicationId = Detail.ElementAt(iCol).pkProductApplicationId;
            //        ObjtblProductAccess.IsLiveRunner = Convert.ToBoolean(Detail.ElementAt(iCol).IsLiveRunnerProduct);
            //        ObjtblProductAccess.ProductReviewStatus = Convert.ToByte(Detail.ElementAt(iCol).EnableReviewPerCompany);
            //        listtblProductAccess.Add(ObjtblProductAccess);
            //        //}

            //        tblCompany_Price ObjtblCompany_Price = new tblCompany_Price();

            //        ObjtblCompany_Price.fkCompanyId = (Session["NewPkCompanyId"] != null) ? new Guid(Session["NewPkCompanyId"].ToString()) : Guid.Empty;
            //        ObjtblCompany_Price.IsDefaultPrice = Convert.ToBoolean(Detail.ElementAt(iCol).IsDefaultPrice);
            //        ObjtblCompany_Price.CompanyPrice = Convert.ToDecimal(Detail.ElementAt(iCol).CompanyPrice);
            //        //ObjtblCompany_Price.fkCompanyId = new Guid(CID);
            //        ObjtblCompany_Price.fkProductApplicationId = Detail.ElementAt(iCol).pkProductApplicationId;
            //        ObjtblCompany_Price.pkCompanyPriceId = Guid.NewGuid();
            //        // ObjtblCompany_Price. = Detail.ElementAt(iCol).AutoReviewStatusForLocation;
            //        ObjtblCompany_Price.EnableReviewPerCompany = Detail.ElementAt(iCol).EnableReviewPerCompany;
            //        if (Detail.ElementAt(iCol).LinkedReportId != null && Detail.ElementAt(iCol).LinkedReportId != Guid.Empty)
            //        {
            //            ObjtblCompany_Price.LinkedReportId = Detail.ElementAt(iCol).LinkedReportId;
            //        }
            //        else
            //        {
            //            ObjtblCompany_Price.LinkedReportId = null;
            //        }

            //        listtblCompany_Price.Add(ObjtblCompany_Price);
            //    }
            //    ObjBALLocation.InsertUpdateLocationProduct(listtblProductAccess);
            //    ObjBALCompany.AddProductPricePerCompany(listtblCompany_Price);
            //}
            return View();

            //return RedirectToAction("AddCompanies", "Control", new { id =123});

        }



        //[HttpPost]
        //public ActionResult UpdateSubReport(IEnumerable<Proc_Get_ReportsByCategoryIdResult> Detail, string CID, string LocationId, string CompanyId)
        //{
        //    List<tblCompany_Price> listtblCompany_Price = new List<tblCompany_Price>();
        //    BALCompany ObjBALCompany = new BALCompany();
        //    if (Detail != null)
        //    {
        //        for (int iCol = 0; iCol < Detail.Count(); iCol++)
        //        {
        //            tblCompany_Price ObjtblCompany_Price = new tblCompany_Price();

        //            ObjtblCompany_Price.IsDefaultPrice = Convert.ToBoolean(Detail.ElementAt(iCol).IsDefault);
        //            ObjtblCompany_Price.CompanyPrice = Convert.ToDecimal(Detail.ElementAt(iCol).CompanyPrice);
        //            ObjtblCompany_Price.fkCompanyId = new Guid(CID);
        //            ObjtblCompany_Price.fkProductApplicationId = Detail.ElementAt(iCol).pkProductApplicationId;
        //            ObjtblCompany_Price.pkCompanyPriceId = Guid.NewGuid();
        //            ObjtblCompany_Price.EnableReviewPerCompany = false;
        //            if (Detail.ElementAt(iCol).LinkedReportId != null && Detail.ElementAt(iCol).LinkedReportId != Guid.Empty)
        //            {
        //                ObjtblCompany_Price.LinkedReportId = Detail.ElementAt(iCol).LinkedReportId;
        //            }
        //            else
        //            {
        //                ObjtblCompany_Price.LinkedReportId = null;
        //            }

        //            listtblCompany_Price.Add(ObjtblCompany_Price);
        //        }

        //        ObjBALCompany.AddProductPricePerCompany(listtblCompany_Price);
        //    }
        //    return View();

        //    //return RedirectToAction("AddCompanies", "Control", new { id =123});

        //}


        [HttpPost]
        public ActionResult UpdateUniversalPackages(IEnumerable<Proc_Get_UniversalPackagesForAdminResult> products)
        {



            bool flagSaveToAll = false;
            string strSuccess = string.Empty;

            BALProductPackages ObjBALProductPackages = new BALProductPackages();

            List<tblCompany_PackagePrice> lsttblCompany_PackagePrice = new List<tblCompany_PackagePrice>();

            if (products.Count() > 0)
            {
                for (int i = 0; i < products.Count(); i++)
                {
                    tblCompany_PackagePrice ObjtblCompany_PackagePrice = new tblCompany_PackagePrice();
                    ObjtblCompany_PackagePrice.IsEnableUniversalPackage = bool.Parse(products.ElementAt(i).IsEnableUniversalPackage.ToString());
                    ObjtblCompany_PackagePrice.PackagePrice = Convert.ToDecimal(products.ElementAt(i).PackagePrice);
                    ObjtblCompany_PackagePrice.pkCompanyPackagePriceId = products.ElementAt(i).PkCompanyPackagePriceId.GetValueOrDefault();
                    if (products.ElementAt(i).PackageDescription != null)
                    {
                        if (Convert.ToString(products.ElementAt(i).PackageDescription) == "1")
                        {
                            flagSaveToAll = true;
                        }
                    }
                    ObjtblCompany_PackagePrice.fkCompanyId = products.ElementAt(i).fkCompanyId.GetValueOrDefault();
                    if (products.ElementAt(i).fkLocationId.GetValueOrDefault() == 0)
                    {
                        ObjtblCompany_PackagePrice.fkLocation = Convert.ToInt32((Request.UrlReferrer.Query.Split('&')[0]).Split('=')[1]);
                    }
                    else
                    {
                        ObjtblCompany_PackagePrice.fkLocation = products.ElementAt(i).fkLocationId.GetValueOrDefault();
                    }
                    ObjtblCompany_PackagePrice.fkPackageId = Convert.ToInt32(products.ElementAt(i).pkPackageId);
                    lsttblCompany_PackagePrice.Add(ObjtblCompany_PackagePrice);
                }
            }

            var Result = ObjBALProductPackages.UpdateUniversalPackagesForCompany(lsttblCompany_PackagePrice, flagSaveToAll);
            if (Result == 1)
            {

                strSuccess = "<span class='successmsg'>Settings updated successfully.</span>";
            }
            else
            {

                strSuccess = "<span class='errormsg'>Some error occured while updation.</span>";

            }


            return Json(new { Message = strSuccess });

        }

        public ActionResult UpdateCategory(IEnumerable<AdCompanyModel> Detail1)
        {
            //BALCompany serObj = new BALCompany();

            //List<CustomDetailClass> ObjCustomDetailClass = new List<CustomDetailClass>();
            //int Count = ObjCustomDetailClass.Count();

            //ObjCustomDetailClass = ObjCustomDetailClass.ToList();

            //return Json(new { Detail = ObjCustomDetailClass, TotalCount = Count });
            return View();
        }

        public ActionResult GetAllProducts()
        {
            BALCompany serObj = new BALCompany();
            int packageid = 0;
            Guid ApplicationID = Guid.Parse("65D1E085-EB17-4F29-96D0-247B0C1EE852");

            var Productslist = serObj.GetAllEmergeProducts(packageid, ApplicationID);
            return Json(Productslist);

        }

        public ActionResult GetReferenceNote(string CompanyId)
        {
            if (string.IsNullOrEmpty(CompanyId))
            {
                return null;
            }
            else
            {
                BALCompany serobj = new BALCompany();
                var fkcompanyid = int.Parse(CompanyId);
                var referencelist = serobj.GetRefernceNote(fkcompanyid);
                return Json(referencelist);
            }
        }

        #region Download File
        public ActionResult GetFile(int Id)
        {
            string FilePath = string.Empty;
            string Title = string.Empty;

            try
            {
                BALCompany ObjBALCompany = new BALCompany();
                var Document = ObjBALCompany.GetCompanyDocumentsByCompanyDocId(Id);
                if (Document != null)
                {
                    if (System.IO.File.Exists(Server.MapPath("~/Resources/Upload/CompanyPDF/" + Document.DocumentName)))
                    {
                        Title = Document.DocumentTitle.ToString().Replace(" ", "_");
                        FilePath = "Resources/Upload/CompanyPDF/" + Document.DocumentName;
                    }
                }
            }
            catch// (Exception ex)
            {
            }

            return Json(new { FName = FilePath, FTitle = Title });

        }

        /// <summary>
        /// This function is used to download  file
        /// </summary>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public ActionResult DownloadFile()
        {
            string FilePath = Request.QueryString["FilePath"];
            string FTitle = Request.QueryString["FTitle"];
            byte[] file = { };
            string PhisycalPath = Server.MapPath("~/" + FilePath);
            string ext = System.IO.Path.GetExtension(PhisycalPath);
            try
            {
                if (System.IO.File.Exists(PhisycalPath))
                {
                    file = System.IO.File.ReadAllBytes(PhisycalPath);
                }
            }
            catch //(Exception ex)
            { }

            return File(file, "application/pdf", FTitle + ext);

        }
        #endregion

        [HttpPost]
        public ActionResult AddReferenceCode(IEnumerable<ReferenceCodeModel> referencecodes, int CompanyId)
        {
            var result = new List<tblReferenceCode>();
            int fkcompanyid = 0;
            if (CompanyId != 0)
            {
                fkcompanyid = CompanyId;
                foreach (var referencecodeModel in referencecodes)
                {
                    var ReferenceCode = new tblReferenceCode
                    {
                        pkReferenceCodeId = referencecodeModel.pkReferenceCodeId,
                        fkCompanyId = fkcompanyid,
                        ReferenceCode = referencecodeModel.ReferenceCode,
                        ReferenceNote = referencecodeModel.ReferenceNote
                    };

                    //BALReferenceCode ObjBALReferenceCode = new BALReferenceCode();
                    //int success = 0;
                    //success = 
                    //ObjBALReferenceCode.AddReference(ReferenceCode, fkcompanyid);
                }
            }


            return Json(result.Select(c => new ReferenceCodeModel
            {
                pkReferenceCodeId = c.pkReferenceCodeId,
                ReferenceCode = c.ReferenceCode,
                ReferenceNote = c.ReferenceNote
            }).ToList());
        }

        [HttpPost]
        public ActionResult UpdateReferenceCode(IEnumerable<ReferenceCodeModel> referencecodes)
        {
            BALReferenceCode ObjBALReferenceCode = new BALReferenceCode();
            foreach (var referencecodeModel in referencecodes)
            {
                var ReferenceCode = new tblReferenceCode
                {
                    pkReferenceCodeId = referencecodeModel.pkReferenceCodeId,
                    ReferenceCode = referencecodeModel.ReferenceCode,
                    ReferenceNote = referencecodeModel.ReferenceNote
                };
                //int Success = 
                    ObjBALReferenceCode.UpdateReference(ReferenceCode);
            }

            return Json(null);
        }

        [HttpPost]
        public ActionResult DeleteReferenceCodeById(IEnumerable<ReferenceCodeModel> referencecodes, int CompanyId)
        {
            int fkcompanyid = 0;
            if (CompanyId != 0)
            {
                fkcompanyid = CompanyId;
                foreach (var referencecodeModel in referencecodes)
                {
                    BALReferenceCode ObjBALReferenceCode = new BALReferenceCode();
                    //int success = 0;
                    //success = 
                        ObjBALReferenceCode.DeleteReference(referencecodeModel.pkReferenceCodeId, fkcompanyid);
                }
            }
            //Return emtpy result
            return Json(null);
        }

        private void LoadSalesGraph_Old(int pkCompanyId)
        {
            StringBuilder str = new StringBuilder();
            str.Append(@"<script type='text/javascript'>    
                  function drawSalesChart() {
                    var data = new google.visualization.DataTable();
                    data.addColumn('string', 'Year');                    
                    data.addColumn('number', 'Products');
                    data.addColumn({type:'string', role:'tooltip'});
                    data.addColumn('number', 'Reports');
                    data.addColumn({type:'string', role:'tooltip'}); 
                    data.addColumn('number', 'Total');
                    data.addColumn({type:'string', role:'tooltip'});
                    ");


            DateTime ObjDateTime = DateTime.Now.AddMonths(-11);
            int iCount = 0;
            decimal PreviousTotalSales = 0;
            decimal PreviousReports = 0;
            decimal PreviousProducts = 0;
            decimal TotalSalesPercentage = 0;
            decimal TotalReportsPercentage = 0;
            decimal TotalProductsPercentage = 0;
            for (int i = 0; i < 12; i++)
            {
                BALCompany ObjBALCompany = new BALCompany();
                if (iCount == 0)
                {
                    var Obj1 = ObjBALCompany.GetSalesValuesforGraph(pkCompanyId, Convert.ToInt16(ObjDateTime.AddMonths(-1).Month), Convert.ToInt16(ObjDateTime.AddMonths(-1).Year));
                    PreviousTotalSales = Convert.ToDecimal(Obj1.ElementAt(0).TotalAmount);
                    PreviousReports = Convert.ToDecimal(Obj1.ElementAt(0).ReportAmount);
                    PreviousProducts = Convert.ToDecimal(Obj1.ElementAt(0).ProductAmount);
                }
                var Obj = ObjBALCompany.GetSalesValuesforGraph(pkCompanyId, Convert.ToInt16(ObjDateTime.AddMonths(i).Month), Convert.ToInt16(ObjDateTime.AddMonths(i).Year));
                TotalSalesPercentage = 0;
                TotalReportsPercentage = 0;
                TotalProductsPercentage = 0;
                if (PreviousTotalSales > 0)
                {
                    TotalSalesPercentage = ((Convert.ToDecimal(Obj.ElementAt(0).TotalAmount) - PreviousTotalSales) / PreviousTotalSales) * 100;
                }
                if (PreviousReports > 0)
                {
                    TotalReportsPercentage = ((Convert.ToDecimal(Obj.ElementAt(0).ReportAmount) - PreviousReports) / PreviousReports) * 100;
                }
                if (PreviousProducts > 0)
                {
                    TotalProductsPercentage = ((Convert.ToDecimal(Obj.ElementAt(0).ProductAmount) - PreviousProducts) / PreviousProducts) * 100;
                }

                string ToolTipValue = "";
                ToolTipValue = "Total Sales : " + Convert.ToDecimal(Obj.ElementAt(0).TotalAmount).ToString("c") + " (" + string.Format("{0:0.0%}", (TotalSalesPercentage / 100)) + "), Reports : " + Convert.ToDecimal(Obj.ElementAt(0).ReportAmount).ToString("c") + " (" + string.Format("{0:0.0%}", (TotalReportsPercentage / 100)) + "), Products : " + Convert.ToDecimal(Obj.ElementAt(0).ProductAmount).ToString("c") + " (" + string.Format("{0:0.0%}", (TotalProductsPercentage / 100)) + ") ";
                str.Append("\t\ndata.addRow(['" + ObjDateTime.AddMonths(i).ToString("MMM, yyyy") + "'," + Obj.ElementAt(0).ProductAmount.ToString() + ",'" + ToolTipValue + "'," + Obj.ElementAt(0).ReportAmount.ToString() + ",'" + ToolTipValue + "'," + Obj.ElementAt(0).TotalAmount.ToString() + ",'" + ToolTipValue + "']);");
                if (iCount != 0)
                {
                    PreviousTotalSales = Convert.ToDecimal(Obj.ElementAt(0).TotalAmount);
                    PreviousReports = Convert.ToDecimal(Obj.ElementAt(0).ReportAmount);
                    PreviousProducts = Convert.ToDecimal(Obj.ElementAt(0).ProductAmount);
                }
                iCount++;
            }
            str.Append("\t\nvar chart = new google.visualization.LineChart(document.getElementById('Saleschart_div'));");
            str.Append(" \t\nchart.draw(data, {width: 970, height: 280, pointSize: 8, fontSize: 10, title: 'Sales Performance',titleTextStyle: {color: 'Red',pointSize: 8, fontSize: 12},colors: ['#00FF00', '#FF9900', '#0000FF']");
            str.Append("}); } \t\ngoogle.load('visualization', '1', {packages:['corechart']});\t\ngoogle.setOnLoadCallback(drawSalesChart);");
            str.Append("</script>");
            ViewBag.ChartImg = str.ToString().TrimEnd(',').Replace('*', '"');
            ViewBag.SalesTotal = "Total Sales : " + PreviousTotalSales.ToString("c") + " " + strPercentageColor(TotalSalesPercentage).ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Sales : " + PreviousReports.ToString("c") + " " + strPercentageColor(TotalReportsPercentage).ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Product Sales : " + PreviousProducts.ToString("c") + " " + strPercentageColor(TotalProductsPercentage).ToString() + "";
        }
        private void LoadSalesGraph(int pkCompanyId)
        {
            StringBuilder str = new StringBuilder();
            str.Append(@"<script type=text/javascript>                  
                  function drawSalesChart() {
                    var data = new google.visualization.DataTable();
                    data.addColumn('string', 'Year');                    
                    data.addColumn('number', 'Products');
                    data.addColumn({type:'string', role:'tooltip'});
                    data.addColumn('number', 'Reports');
                    data.addColumn({type:'string', role:'tooltip'}); 
                    data.addColumn('number', 'Total');
                    data.addColumn({type:'string', role:'tooltip'});
                    //data.addColumn({type:'boolean', role:'emphasis'});
                    ");


            DateTime ObjDateTime = DateTime.Now.AddMonths(-11);
            int iCount = 0;
            decimal PreviousTotalSales = 0;
            decimal PreviousReports = 0;
            decimal PreviousProducts = 0;
            decimal TotalSalesPercentage = 0;
            decimal TotalReportsPercentage = 0;
            decimal TotalProductsPercentage = 0;
            for (int i = 0; i < 12; i++)
            {
                BALCompany ObjBALCompany = new BALCompany();
                if (iCount == 0)
                {
                    var Obj1 = ObjBALCompany.GetSalesValuesforGraph(pkCompanyId, Convert.ToInt16(ObjDateTime.AddMonths(-1).Month), Convert.ToInt16(ObjDateTime.AddMonths(-1).Year));
                    PreviousTotalSales = Convert.ToDecimal(Obj1.ElementAt(0).TotalAmount);
                    PreviousReports = Convert.ToDecimal(Obj1.ElementAt(0).ReportAmount);
                    PreviousProducts = Convert.ToDecimal(Obj1.ElementAt(0).ProductAmount);
                }
                var Obj = ObjBALCompany.GetSalesValuesforGraph(pkCompanyId, Convert.ToInt16(ObjDateTime.AddMonths(i).Month), Convert.ToInt16(ObjDateTime.AddMonths(i).Year));
                TotalSalesPercentage = 0;
                TotalReportsPercentage = 0;
                TotalProductsPercentage = 0;
                if (PreviousTotalSales > 0)
                {
                    TotalSalesPercentage = ((Convert.ToDecimal(Obj.ElementAt(0).TotalAmount) - PreviousTotalSales) / PreviousTotalSales) * 100;
                }
                if (PreviousReports > 0)
                {
                    TotalReportsPercentage = ((Convert.ToDecimal(Obj.ElementAt(0).ReportAmount) - PreviousReports) / PreviousReports) * 100;
                }
                if (PreviousProducts > 0)
                {
                    TotalProductsPercentage = ((Convert.ToDecimal(Obj.ElementAt(0).ProductAmount) - PreviousProducts) / PreviousProducts) * 100;
                }

                string ToolTipValue = "";
                ToolTipValue = "Total Sales : " + Convert.ToDecimal(Obj.ElementAt(0).TotalAmount).ToString("c") + " (" + string.Format("{0:0.0%}", (TotalSalesPercentage / 100)) + "), Reports : " + Convert.ToDecimal(Obj.ElementAt(0).ReportAmount).ToString("c") + " (" + string.Format("{0:0.0%}", (TotalReportsPercentage / 100)) + "), Products : " + Convert.ToDecimal(Obj.ElementAt(0).ProductAmount).ToString("c") + " (" + string.Format("{0:0.0%}", (TotalProductsPercentage / 100)) + ") ";
                str.Append("data.addRow(['" + ObjDateTime.AddMonths(i).ToString("MMM, yyyy") + "'," + Obj.ElementAt(0).ProductAmount.ToString() + ",'" + ToolTipValue + "'," + Obj.ElementAt(0).ReportAmount.ToString() + ",'" + ToolTipValue + "'," + Obj.ElementAt(0).TotalAmount.ToString() + ",'" + ToolTipValue + "']);");
                if (iCount != 0)
                {
                    PreviousTotalSales = Convert.ToDecimal(Obj.ElementAt(0).TotalAmount);
                    PreviousReports = Convert.ToDecimal(Obj.ElementAt(0).ReportAmount);
                    PreviousProducts = Convert.ToDecimal(Obj.ElementAt(0).ProductAmount);
                }
                iCount++;
            }
            str.Append("   var chart = new google.visualization.LineChart(document.getElementById('Saleschart_div'));");
            str.Append(" chart.draw(data, {width: 970, height: 280, pointSize: 8, fontSize: 10, title: 'Sales Performance',titleTextStyle: {color: 'Red',pointSize: 8, fontSize: 12},colors: ['#00FF00', '#FF9900', '#0000FF']");
            str.Append("}); } google.setOnLoadCallback(drawSalesChart);");
            str.Append("</script>");
            ViewBag.ChartImg = str.ToString().TrimEnd(',').Replace('*', '"');
            ViewBag.SalesTotal = "Total Sales : " + PreviousTotalSales.ToString("c") + " " + strPercentageColor(TotalSalesPercentage).ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Sales : " + PreviousReports.ToString("c") + " " + strPercentageColor(TotalReportsPercentage).ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Product Sales : " + PreviousProducts.ToString("c") + " " + strPercentageColor(TotalProductsPercentage).ToString() + "";
        }

        private void LoadVolumeGraph_Old(int pkCompanyId)
        {
            StringBuilder str = new StringBuilder();
            str.Append(@"<script type=text/javascript>
                  
                  function drawVolumeChart() {
                    var Volumedata = new google.visualization.DataTable();
                    Volumedata.addColumn('string', 'Year');
                    Volumedata.addColumn('number', 'Products');
                    Volumedata.addColumn({type:'string', role:'tooltip'});                    
                    Volumedata.addColumn('number', 'Reports');
                    Volumedata.addColumn({type:'string', role:'tooltip'});
                    Volumedata.addColumn('number', 'Total');
                    Volumedata.addColumn({type:'string', role:'tooltip'});                     
                    //data.addColumn({type:'boolean', role:'emphasis'});
                    ");


            DateTime ObjDateTime = DateTime.Now.AddMonths(-11);
            int iCount = 0;
            decimal PreviousTotalSales = 0;
            decimal PreviousReports = 0;
            decimal PreviousProducts = 0;
            decimal TotalSalesPercentage = 0;
            decimal TotalReportsPercentage = 0;
            decimal TotalProductsPercentage = 0;
            for (int i = 0; i < 12; i++)
            {
                BALCompany ObjBALCompany = new BALCompany();
                if (iCount == 0)
                {
                    var Obj1 = ObjBALCompany.GetVolumeValuesforGraph(pkCompanyId, Convert.ToInt16(ObjDateTime.AddMonths(-1).Month), Convert.ToInt16(ObjDateTime.AddMonths(-1).Year));
                    PreviousTotalSales = Convert.ToDecimal(Obj1.ElementAt(0).TotalAmount);
                    PreviousReports = Convert.ToDecimal(Obj1.ElementAt(0).ReportAmount);
                    PreviousProducts = Convert.ToDecimal(Obj1.ElementAt(0).ProductAmount);
                }
                var Obj = ObjBALCompany.GetVolumeValuesforGraph(pkCompanyId, Convert.ToInt16(ObjDateTime.AddMonths(i).Month), Convert.ToInt16(ObjDateTime.AddMonths(i).Year));
                TotalSalesPercentage = 0;
                TotalReportsPercentage = 0;
                TotalProductsPercentage = 0;
                if (PreviousTotalSales > 0)
                {
                    TotalSalesPercentage = ((Convert.ToDecimal(Obj.ElementAt(0).TotalAmount) - PreviousTotalSales) / PreviousTotalSales) * 100;
                }
                if (PreviousReports > 0)
                {
                    TotalReportsPercentage = ((Convert.ToDecimal(Obj.ElementAt(0).ReportAmount) - PreviousReports) / PreviousReports) * 100;
                }
                if (PreviousProducts > 0)
                {
                    TotalProductsPercentage = ((Convert.ToDecimal(Obj.ElementAt(0).ProductAmount) - PreviousProducts) / PreviousProducts) * 100;
                }
                string ToolTipValue = "";
                ToolTipValue = "Total Volume : " + Convert.ToDecimal(Obj.ElementAt(0).TotalAmount).ToString() + " (" + string.Format("{0:0.0%}", (TotalSalesPercentage / 100)) + "), Reports : " + Convert.ToDecimal(Obj.ElementAt(0).ReportAmount).ToString() + " (" + string.Format("{0:0.0%}", (TotalReportsPercentage / 100)) + "), Products : " + Convert.ToDecimal(Obj.ElementAt(0).ProductAmount).ToString() + " (" + string.Format("{0:0.0%}", (TotalProductsPercentage / 100)) + ") ";
                str.Append("Volumedata.addRow(['" + ObjDateTime.AddMonths(i).ToString("MMM, yyyy") + "'," + Obj.ElementAt(0).ProductAmount.ToString() + ",'" + ToolTipValue + "'," + Obj.ElementAt(0).ReportAmount.ToString() + ",'" + ToolTipValue + "'," + Obj.ElementAt(0).TotalAmount.ToString() + ",'" + ToolTipValue + "']);");
                if (iCount != 0)
                {
                    PreviousTotalSales = Convert.ToDecimal(Obj.ElementAt(0).TotalAmount);
                    PreviousReports = Convert.ToDecimal(Obj.ElementAt(0).ReportAmount);
                    PreviousProducts = Convert.ToDecimal(Obj.ElementAt(0).ProductAmount);
                }
                iCount++;
            }
            str.Append("   var Volumechart = new google.visualization.LineChart(document.getElementById('Volumechart_div'));");
            str.Append(" Volumechart.draw(Volumedata, {width: 970, height: 280, pointSize: 8, fontSize: 10, title: 'Volume Performance',titleTextStyle: {color: 'Red',pointSize: 8, fontSize: 12},colors: ['#00FF00', '#FF9900', '#0000FF']");
            str.Append("}); } \t\ngoogle.load('visualization', '1', {packages:['corechart']});\t\ngoogle.setOnLoadCallback(drawVolumeChart);");
            str.Append("</script>");
            ViewData["VolumeChartImg"] = str.ToString().TrimEnd(',').Replace('*', '"');
            ViewBag.VolumeSalesTotal = "Total Volume : " + PreviousTotalSales.ToString() + " " + strPercentageColor(TotalSalesPercentage).ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Sales : " + PreviousReports.ToString() + " " + strPercentageColor(TotalReportsPercentage).ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Product Sales : " + PreviousProducts.ToString() + " " + strPercentageColor(TotalProductsPercentage).ToString() + "";
        }

        private void LoadVolumeGraph(int pkCompanyId)
        {
            StringBuilder str = new StringBuilder();
            str.Append(@"<script type=text/javascript>
                  
                  function drawVolumeChart() {
                    var Volumedata = new google.visualization.DataTable();
                    Volumedata.addColumn('string', 'Year');
                    Volumedata.addColumn('number', 'Products');
                    Volumedata.addColumn({type:'string', role:'tooltip'});                    
                    Volumedata.addColumn('number', 'Reports');
                    Volumedata.addColumn({type:'string', role:'tooltip'});
                    Volumedata.addColumn('number', 'Total');
                    Volumedata.addColumn({type:'string', role:'tooltip'});                     
                    //data.addColumn({type:'boolean', role:'emphasis'});
                    ");


            DateTime ObjDateTime = DateTime.Now.AddMonths(-11);
            int iCount = 0;
            decimal PreviousTotalSales = 0;
            decimal PreviousReports = 0;
            decimal PreviousProducts = 0;
            decimal TotalSalesPercentage = 0;
            decimal TotalReportsPercentage = 0;
            decimal TotalProductsPercentage = 0;
            for (int i = 0; i < 12; i++)
            {
                BALCompany ObjBALCompany = new BALCompany();
                if (iCount == 0)
                {
                    var Obj1 = ObjBALCompany.GetVolumeValuesforGraph(pkCompanyId, Convert.ToInt16(ObjDateTime.AddMonths(-1).Month), Convert.ToInt16(ObjDateTime.AddMonths(-1).Year));
                    PreviousTotalSales = Convert.ToDecimal(Obj1.ElementAt(0).TotalAmount);
                    PreviousReports = Convert.ToDecimal(Obj1.ElementAt(0).ReportAmount);
                    PreviousProducts = Convert.ToDecimal(Obj1.ElementAt(0).ProductAmount);
                }
                var Obj = ObjBALCompany.GetVolumeValuesforGraph(pkCompanyId, Convert.ToInt16(ObjDateTime.AddMonths(i).Month), Convert.ToInt16(ObjDateTime.AddMonths(i).Year));
                TotalSalesPercentage = 0;
                TotalReportsPercentage = 0;
                TotalProductsPercentage = 0;
                if (PreviousTotalSales > 0)
                {
                    TotalSalesPercentage = ((Convert.ToDecimal(Obj.ElementAt(0).TotalAmount) - PreviousTotalSales) / PreviousTotalSales) * 100;
                }
                if (PreviousReports > 0)
                {
                    TotalReportsPercentage = ((Convert.ToDecimal(Obj.ElementAt(0).ReportAmount) - PreviousReports) / PreviousReports) * 100;
                }
                if (PreviousProducts > 0)
                {
                    TotalProductsPercentage = ((Convert.ToDecimal(Obj.ElementAt(0).ProductAmount) - PreviousProducts) / PreviousProducts) * 100;
                }
                string ToolTipValue = "";
                ToolTipValue = "Total Volume : " + Convert.ToDecimal(Obj.ElementAt(0).TotalAmount).ToString() + " (" + string.Format("{0:0.0%}", (TotalSalesPercentage / 100)) + "), Reports : " + Convert.ToDecimal(Obj.ElementAt(0).ReportAmount).ToString() + " (" + string.Format("{0:0.0%}", (TotalReportsPercentage / 100)) + "), Products : " + Convert.ToDecimal(Obj.ElementAt(0).ProductAmount).ToString() + " (" + string.Format("{0:0.0%}", (TotalProductsPercentage / 100)) + ") ";
                str.Append("Volumedata.addRow(['" + ObjDateTime.AddMonths(i).ToString("MMM, yyyy") + "'," + Obj.ElementAt(0).ProductAmount.ToString() + ",'" + ToolTipValue + "'," + Obj.ElementAt(0).ReportAmount.ToString() + ",'" + ToolTipValue + "'," + Obj.ElementAt(0).TotalAmount.ToString() + ",'" + ToolTipValue + "']);");
                if (iCount != 0)
                {
                    PreviousTotalSales = Convert.ToDecimal(Obj.ElementAt(0).TotalAmount);
                    PreviousReports = Convert.ToDecimal(Obj.ElementAt(0).ReportAmount);
                    PreviousProducts = Convert.ToDecimal(Obj.ElementAt(0).ProductAmount);
                }
                iCount++;
            }
            str.Append("   var Volumechart = new google.visualization.LineChart(document.getElementById('Volumechart_div'));");
            str.Append(" Volumechart.draw(Volumedata, {width: 970, height: 280, pointSize: 8, fontSize: 10, title: 'Volume Performance',titleTextStyle: {color: 'Red',pointSize: 8, fontSize: 12},colors: ['#00FF00', '#FF9900', '#0000FF']");
            str.Append("}); } \t\ngoogle.load('visualization', '1', {packages:['corechart']});\t\ngoogle.setOnLoadCallback(drawVolumeChart);");
            str.Append("</script>");
            ViewData["VolumeChartImg"] = str.ToString().TrimEnd(',').Replace('*', '"');
            ViewBag.VolumeSalesTotal = "Total Volume : " + PreviousTotalSales.ToString() + " " + strPercentageColor(TotalSalesPercentage).ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Sales : " + PreviousReports.ToString() + " " + strPercentageColor(TotalReportsPercentage).ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Product Sales : " + PreviousProducts.ToString() + " " + strPercentageColor(TotalProductsPercentage).ToString() + "";
        }
        private string strPercentageColor(decimal PercentageAmount)
        {
            string strColor = "";
            if (PercentageAmount >= 0)
            {
                strColor = "<span style='color : #008000;'> (" + string.Format("{0:0.0%}", (PercentageAmount / 100)) + ") </span>";
            }
            else
            {
                strColor = "<span style='color : #FF0000;'> (" + string.Format("{0:0.0%}", (PercentageAmount / 100)) + ")</span>";
            }
            return strColor;
        }

        [HttpPost]
        public ActionResult UpdateCompanyStatus(string IsEnable, string Status, int CompanyId, string CompanyName, string EmailId, string ButtonId)
        {



            string Message = string.Empty;
            string CSS = string.Empty;
            string Result = string.Empty;
            ObjBALCompanyUsers = new BALCompanyUsers();
            var dbCollection = ObjBALCompanyUsers.GetUserInformationByMembershipUserId(new Guid(Membership.GetUser(HttpContext.User.Identity.Name).ProviderUserKey.ToString()));
            byte Button_Id = Convert.ToByte(ButtonId);
            byte LeadStatus = Convert.ToByte(Button_Id);
            if (IsEnable == "true")
            {
                LeadStatus = 5;
            }


            if (CompanyId != 0)
            {
                ObjBALCompany = new BALCompany();
                tblCompany ObjtblCompany = new tblCompany();

                ObjtblCompany.pkCompanyId = CompanyId;
                ObjtblCompany.LeadStatus = LeadStatus;
                ObjtblCompany.IsEnabled = bool.Parse(IsEnable);
                ObjtblCompany.LastModifiedDate = DateTime.Now;

                // #19
                // ObjtblCompany.LastModifiedById = dbCollection.ElementAt(0).pkCompanyUserId;

                int success = ObjBALCompany.UpdateCompanyLeadStatus(ObjtblCompany);
                if (success > 0)
                {
                    if (Status == "8" && IsEnable != "true")
                    {
                        sendStatusChangedMail(8, CompanyName, EmailId);
                    }
                    else if (Status == "9" && IsEnable != "true")
                    {
                        sendStatusChangedMail(9, CompanyName, EmailId);
                    }
                    Message = "Company Status updated Successfully";
                    CSS = "successmsg";
                    Result = "1";
                }
                else
                {
                    Message = "Some error is occured";
                    CSS = "errormsg";
                }
            }

            string Comments = string.Empty;
            switch (ButtonId)
            {
                case "5":
                    Comments = "##5##";
                    break;
                case "6":
                    Comments = "##6##";
                    break;
                case "7":
                    Comments = "##7##";
                    break;
                case "8":
                    Comments = "##8##";
                    break;
                case "9":
                    Comments = "##9##";
                    break;
            }

            if (Comments != string.Empty)
            {
                Guid UserId = new Guid(dbCollection.ElementAt(0).fkUserId.ToString());
                int? pkCompanyId = CompanyId;
                DateTime CreatedDate = System.DateTime.Now;
                ObjBALCompany.AddStatusToCallLog(pkCompanyId, UserId, Comments, CreatedDate);
            }

            return Json(new { Msg = Message, css = CSS, rslt = Result });

        }

        [HttpPost]
        public ActionResult SetLock(string IsEnable, string Reason, int CompanyId, string CompanyName, string EmailId)
        {
            string Message = string.Empty;
            string CSS = string.Empty;
            string Result = string.Empty;
            ObjBALCompanyUsers = new BALCompanyUsers();
            var dbCollection = ObjBALCompanyUsers.GetUserInformationByMembershipUserId(new Guid(Membership.GetUser(HttpContext.User.Identity.Name).ProviderUserKey.ToString()));

            if (CompanyId != 0)
            {
                ObjBALCompany = new BALCompany();
                tblCompany ObjtblCompany = new tblCompany();
                ObjtblCompany.pkCompanyId = CompanyId;
                ObjtblCompany.Reason = Reason;
                ObjtblCompany.IsEnabled = bool.Parse(IsEnable);
                ObjtblCompany.LastModifiedDate = DateTime.Now;

                //  #19
                //ObjtblCompany.LastModifiedById = (dbCollection != null) ? dbCollection.ElementAt(0).pkCompanyUserId : 0;


                int success = ObjBALCompany.UpdateCompanyStatus(ObjtblCompany);
                if (success > 0)
                {
                    if (IsEnable == "false")
                    {
                        sendStatusChangedMail(7, CompanyName, EmailId);
                    }
                    Message = "Company Status updated Successfully";
                    CSS = "successmsg";
                    Result = "1";
                    //  For //
                    Guid UserId = new Guid(dbCollection.ElementAt(0).fkUserId.ToString());
                    int pkCompanyId = CompanyId;
                    string Comments = "##7##";
                    DateTime CreatedDate = System.DateTime.Now;
                    ObjBALCompany.AddStatusToCallLog(pkCompanyId, UserId, Comments, CreatedDate);

                }
                else
                {
                    Message = "Some error is occured";
                    CSS = "errormsg";
                }
            }
            return Json(new { Msg = Message, css = CSS, rslt = Result });

        }

        private void sendStatusChangedMail(byte Status, string CompanyName, string EmailId)
        {
            try
            {
                MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
                bool CheckSend = false;
                string emailFrom = ObjMembershipUser.Email;
                string StrDate = DateTime.Now.ToString();

                #region Replace Bookmark
                string MessageBody = "";
                string emailText = "";
                string imgSrc = "";
                BALGeneral ObjBALGeneral = new BALGeneral();
                string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;
                var emailContent = ObjBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, Convert.ToInt32(EmailTemplates.AccountStatusChange));


                emailText = emailContent.TemplateContent;

                BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                #region For New Style SystemTemplate
                string Bookmarkfile = string.Empty;
                string bookmarkfilepath = string.Empty;

                Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                emailText = Bookmarkfile;
                #endregion


                Bookmarks objBookmark = new Bookmarks();
                objBookmark.EmailContent = emailText;
                objBookmark.Date = StrDate;
                if (!string.IsNullOrEmpty(CompanyName))
                {
                    objBookmark.CompanyName = CompanyName;
                }
                if (Status == 7)
                {
                    imgSrc = ApplicationPath.GetApplicationPath() + @"Resources/Images/emerge-office-milestone-7-on.png";
                }
                else if (Status == 8)
                {
                    imgSrc = ApplicationPath.GetApplicationPath() + @"Resources/Images/emerge-office-milestone-8-on.png";
                }
                else if (Status == 9)
                {
                    imgSrc = ApplicationPath.GetApplicationPath() + @"Resources/Images/emerge-office-milestone-9-on.png";
                }
                objBookmark.CompanyStatus = "<img src='" + imgSrc + "' />";
                objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                objBookmark.LoginLink = "<a style='color: #87ceea;' href='" + ApplicationPath.GetApplicationPath() + "'> Click here </a>";

                MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Utility.GetUID(HttpContext.User.Identity.Name.ToString()));

                #endregion
                string emailTo = "";
                if (!string.IsNullOrEmpty(EmailId))
                {
                    emailTo = EmailId;
                    //emailTo = "support@dotnetoutsourcing.com";
                }

                CheckSend = Utility.SendMail(emailTo, emailFrom, MessageBody, "Account Status Change");
                //if (CheckSend == true)
                //{
                //    ScriptManager.RegisterStartupScript(this.Page, typeof(string), "success", "alert('Email sent successfully');", true);
                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this.Page, typeof(string), "success", "alert('Error Occured while sending email !');", true);
                //}
            }
            catch
            {

            }
        }
        public ActionResult SaveCallLogo(string Comment, int pkcompanyid)
        {
            Guid UserId;
            int pkCompanyId;
            MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));

            if (ObjMembershipUser != null)
            {
                UserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
                pkCompanyId = pkcompanyid;
                string Comments = Comment;
                DateTime CreatedDate = System.DateTime.Now;
                AddCommentToCallLog(pkCompanyId, UserId, Comments, CreatedDate);
            }

            return Json(new { Message = "Successful" });


        }


        public ActionResult BindDemoschedulepopup(string DemoScheduleId)
        {
            BALLeads ObjBALLeads = new BALLeads();
            Session["Demo_ScheduleId"] = DemoScheduleId;
            //string fklocationid = Session["pk_LocationIdWhenpopup"].ToString();

            int fkCompanyId = Convert.ToInt32(Session["fk_companyid"]);
            var DemoscheduleList = ObjBALLeads.GetTraineeByDemoIdCompanyId(Convert.ToInt16(DemoScheduleId), fkCompanyId);
            var list = ObjBALLeads.GetDemoScheduleInfoByDemoId(Convert.ToInt16(DemoScheduleId));
            string datetime = list.DemoDateTime.ToString();
            string date = Convert.ToDateTime(datetime).ToShortDateString();
            string time = Convert.ToDateTime(datetime).ToShortTimeString();

            var Comments = list.Comments;

            StringBuilder sb = new StringBuilder();

            sb.Append("<table cellspacing='2' cellpadding='5' width='750px' style='height: 30px; font-weight: bold; margin-left:25px' id='tbln'>");
            for (int i = 0; i < DemoscheduleList.Count; i++)
            {
                sb.Append("<tr>");
                sb.Append("<td style='width:50px' >" + "<input type='text' id='FirstName' style='width:90%' name='Title' class='k-textbox' value='  " + DemoscheduleList.ElementAt(i).FirstName + " '/>" + "</td>");
                sb.Append("<td style='width:50px'>" + "<input type='text' id='LastName' style='width:90%' name='Title' class='k-textbox' value='  " + DemoscheduleList.ElementAt(i).LastName + " '/>" + "</td>");
                sb.Append("<td  style='width:50px'>" + "<input type='text' id='Title' style='width:90%' name='Title' class='k-textbox' value='  " + DemoscheduleList.ElementAt(i).Title + " '/>" + "</td>");
                sb.Append("<td  style='width:50px'>" + "<input type='text' id='Phone' style='width:90%' name='Title' class='k-textbox' value='  " + DemoscheduleList.ElementAt(i).Phone + " '/>" + "</td>");
                sb.Append("<td  style='width:50px'>" + "<input type='text' id='Ext' style='width:90%' name='Title' class='k-textbox' value='  " + DemoscheduleList.ElementAt(i).Ext + " '/>" + "</td>");
                sb.Append("<td style='width:50px'>" + "<input type='text' id='Email' style='width:90%' name='Title' class='k-textbox' value='  " + DemoscheduleList.ElementAt(i).EmailId + " '/>" + "</td>");
                sb.Append("<td style='width:50px'>" + "<input type='button' id=' " + DemoscheduleList.ElementAt(i).pkCompanyTraineeId + "+' style='width:90%' name='delete' class='k-textbox' value='Delete' class='EditDel' style='cursor:pointer; '  onclick='return DeleteDocuments(this.id)'  />" + "</td>");

                sb.Append("</tr>");

            }

            sb.Append("<table>");

            return Json(new { label = sb.ToString(), date = date, time = time, Comments = Comments });




        }
        private int AddCommentToCallLog(int pkCompanyId, Guid UserId, string LeadComments, DateTime CreatedDate)
        {
            BALLeads ObjBALLeads = new BALLeads();
            int Result = 0;
            try
            {
                Result = ObjBALLeads.AddCommentsToCallLog(pkCompanyId, UserId, LeadComments, CreatedDate);
            }
            catch (Exception)
            {

            }
            finally
            {
                ObjBALLeads = null;
            }
            return Result;
        }


        public ActionResult GetContactNamelist()
        {

           // AddEditLeadsModel objmodel = new AddEditLeadsModel();
            List<SelectListItem> list = new List<SelectListItem>();
            if (Session["fk_companyid"] != null)
            {
                //string pkcomapnyidwhenedit = Session["fk_companyid"].ToString();
                string maincontact = Session["Main_contactPerson"].ToString();
                string mainemail = Session["Main_ContactaEmail"].ToString();
                string billinContact = Session["Billing_contactPerson"].ToString();
                string billingemail = Session["Billing_ContactactEmail"].ToString();
                if (billinContact != maincontact)
                {
                    list.Add(new SelectListItem { Text = maincontact, Value = mainemail });
                    list.Add(new SelectListItem { Text = billinContact, Value = billingemail });

                }
                else
                {
                    list.Add(new SelectListItem { Text = maincontact, Value = mainemail });
                }


            }
            else
            {
                list.Add(new SelectListItem { Text = "----", Value = "-1" });

            }

            return Json(list);

        }

        public ActionResult DeleteCustomPackageByIds(int Ids)
        {
            BALProductPackages ObjBALProductPackages = new BALProductPackages();


            int result = ObjBALProductPackages.DeleteCustomPackageById(Ids, true);
            return Json(result);
        }
        //private void UpdateCompanyIsActive(byte preStatus)
        //{
        //    ObjBALCompany = new BALCompany();
        //    tblCompany ObjtblCompany = new tblCompany();
        //    Guid CompanyId = Guid.Empty;
        //    if (ViewState["PkCompanyId"] != null)
        //    {
        //        ObjtblCompany.pkCompanyId = new Guid(ViewState["PkCompanyId"].ToString());
        //        ObjtblCompany.IsActive = preStatus;
        //        ObjtblCompany.LastModifiedDate = DateTime.Now;
        //        ObjtblCompany.LastModifiedById = (ViewState["vwUserId"] != null) ? new Guid(ViewState["vwUserId"].ToString()) : Guid.Empty;
        //        int success = ObjBALCompany.UpdateCompanyIsActive(ObjtblCompany);
        //        lblMessageBox.Visible = false; if (success > 0)
        //        {
        //            lblMessageBox.Text = "Company Status updated Successfully";
        //            lblMessageBox.CssClass = "successmsg";
        //        }
        //        else
        //        {
        //            lblMessageBox.Text = "Some error is occured";
        //            lblMessageBox.CssClass = "errormsg";
        //        }
        //    }
        //}

        #region Upload Document
        //public ActionResult UploadFile(string DocumentsTitle, HttpPostedFileBase UploadFlePDF, string CompanyId)
        //{

        //    if (!string.IsNullOrEmpty(DocumentsTitle))
        //    {
        //        if (UploadFlePDF.ContentLength > 0)
        //        {
        //            string sExt = Path.GetExtension(UploadFlePDF.FileName).ToLower();
        //            string sFileName = DateTime.Now.ToFileTime().ToString() + sExt;
        //            string sFilePath = "";
        //            sFilePath = System.Web.HttpContext.Current.Server.MapPath("~/Resources/Upload/CompanyPDF/");
        //            if (System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\CompanyPDF\") + sFileName))
        //            {
        //                try
        //                {
        //                    System.IO.File.Delete(System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\CompanyPDF\") + sFileName);
        //                }
        //                catch { }
        //            }

        //            UploadFlePDF.SaveAs(System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\CompanyPDF\") + sFileName);

        //            string fPath = Server.MapPath("~/Resources/Upload/CompanyPDF/") + sFileName;

        //            //int NoOfPages = GetNoOfPagesPDF(fPath);
        //            int NoOfPages = 4;
        //            if (NoOfPages == 4)
        //            {
        //                ObjBALCompany = new BALCompany();
        //                tblCompanyDocument tbl = new tblCompanyDocument();
        //                tbl.pkCompanyDocumentId = Guid.NewGuid();
        //                tbl.fkCompanyId = new Guid(CompanyId);
        //                tbl.DocumentTitle = DocumentsTitle;
        //                tbl.DocumentName = sFileName;
        //                tbl.CreatedDate = DateTime.Now;
        //                tbl.IsUserAgreement = false;
        //                int Result = ObjBALCompany.InsertDocumentDetailsByCompanyId(tbl);
        //                if (Result == 1)
        //                {
        //                    //BindDocumentsByCompanyId(new Guid(CompanyId));
        //                }
        //            }
        //            else
        //            {
        //                try
        //                {
        //                    System.IO.File.Delete(System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\CompanyPDF\") + sFileName);
        //                }
        //                catch
        //                {
        //                }
        //            }

        //        }
        //    }
        //    return Json(new { Message = "" });
        //}
        #endregion

        #region Global Credentials

        [HttpPost]
        public ActionResult EnableDisableGlobalCredential(int? pkCompanyId, int? fkLocationid, string isGlobalCredential)
        {
            if (pkCompanyId != null && fkLocationid != null)
            {
                string Message = string.Empty;
               // string Result = string.Empty;

                bool IsGlobalCredential = false;
                if (isGlobalCredential == "1")
                {
                    IsGlobalCredential = true;
                }
                if (pkCompanyId != 0)
                {
                    ObjBALCompany = new BALCompany();
                    tblCompany ObjtblCompany = new tblCompany();

                    ObjtblCompany.pkCompanyId = pkCompanyId.GetValueOrDefault();
                    ObjtblCompany.IsGlobalCredential = IsGlobalCredential;
                    if (IsGlobalCredential == true)
                    {
                        int PriceSuccess = ObjBALCompany.UpdateDataSourceSettingsForLocation((pkCompanyId), (fkLocationid));//method to update all location data source on the basis of home location data source settings.
                        if (PriceSuccess > 0)
                        {
                            int success = ObjBALCompany.UpdateCompanyGlobalCredential(ObjtblCompany);
                            if (success > 0)
                            {
                                Message = "<span class='successmsg'>Global Credentials updated Successfully.</span>";
                            }
                            else
                            {
                                Message = "<span class='errormsg'>Some error is occurred.</span>";
                            }
                        }
                        else
                        {
                            Message = "<span class='errormsg'>Some error is occurred.</span>";
                        }
                    }
                    else
                    {
                        int success = ObjBALCompany.UpdateCompanyGlobalCredential(ObjtblCompany);
                        if (success > 0)
                        {
                            Message = "<span class='successmsg'>Global Credentials updated Successfully.</span>";
                        }
                        else
                        {
                            Message = "<span class='errormsg'>Some error  occurred.</span>";
                        }
                    }
                }
                return Json(new { Msg = Message });
            }
            else
            {
                return Json(new { Msg = "" });
            }
        }

        #endregion
        #region Third Party Credentials


        [HttpPost]
        public ActionResult GetThirdPartyDetails(string CompanyId)
        {
            BALCompany ObjBALCompany = new BALCompany();
            using (EmergeDALDataContext EmergeDALDataContext = new EmergeDALDataContext())
            {
                var iRecord = ObjBALCompany.GetThirdPartyDetails(CompanyId);

                List<ScreencatgoriesModel> ObjVendors = iRecord.Select(data => new ScreencatgoriesModel
                {
                    fkCompanyId = data.fkCompanyId,
                    CategoryName = data.CategoryName,
                    fkReportCategoryId = int.Parse(data.fkReportCategoryId.ToString())

                }).ToList();

                return Json(ObjVendors);
            }
        }


        public ActionResult GetdetailsbyCompanyId(int fkReportCategoryId, string CompanyId)
        {
            BALCompany ObjBALCompany = new BALCompany();
            using (EmergeDALDataContext EmergeDALDataContext = new EmergeDALDataContext())
            {
                var ReportList = ObjBALCompany.GetThirdPartyDetailsByCompanyId(fkReportCategoryId, CompanyId);


                if (ReportList != null)
                {
                    if (ReportList.Count > 0)
                    {
                        List<ScreenModel> ObjVendors = ReportList.Select(data => new ScreenModel
                        {
                            accountno = data.accountno,
                            subaccountno = data.subaccountno,
                            Isenable = data.Isenable,
                            username = data.username,
                            password = data.password,
                            ProductCode = data.ProductCode,
                            pkthirdpartycredentialsId = Convert.ToString(data.pkthirdpartycredentialsId),
                            fkReportCategoryId = int.Parse(data.fkReportCategoryId.ToString()),
                            fkCompanyId = int.Parse(CompanyId)
                        }).ToList();

                    }
                }
                return Json(ReportList);
            }
        }



        [HttpPost]
        public ActionResult UpdatedetailsbyCompanyId(IEnumerable<ScreenModel> ObjdetailsbyCompanyId)
        {
            BALCompany ObjBALCompany = new BALCompany();
            for (int i = 0; i < ObjdetailsbyCompanyId.Count(); i++)
            {
                int? pkid = Convert.ToInt32(ObjdetailsbyCompanyId.ElementAt(i).pkthirdpartycredentialsId);
                int? pkthid = pkid;
                if (pkthid != null)
                {
                    tblthirdpartycredential Objthirdpartycredential = new tblthirdpartycredential();
                    Objthirdpartycredential.pkthirdpartycredentialsId = pkthid.GetValueOrDefault();
                    Objthirdpartycredential.Isenable = ObjdetailsbyCompanyId.ElementAt(i).Isenable;
                    Objthirdpartycredential.username = ObjdetailsbyCompanyId.ElementAt(i).username;
                    Objthirdpartycredential.password = ObjdetailsbyCompanyId.ElementAt(i).password;
                    Objthirdpartycredential.accountno = ObjdetailsbyCompanyId.ElementAt(i).accountno;
                    Objthirdpartycredential.subaccountno = ObjdetailsbyCompanyId.ElementAt(i).subaccountno;
                    //int iStatus = 
                        ObjBALCompany.SaveThirdPartyDetailsByCompanyId(Objthirdpartycredential);

                }
            }




            return Json(new { });
        }


        [HttpPost]
        public ActionResult AdddetalsbyCompanyId(IEnumerable<ScreenModel> ObjdetailsbyCompanyId, string CompanyId)
        {
            //string msg = string.Empty;
            BALCompany ObjBALCompany = new BALCompany();
            int? Company_Ids = null;
            Company_Ids = int.Parse(CompanyId);
            var tblRowList = new List<tblthirdpartycredential>();
            foreach (var ObjItem in ObjdetailsbyCompanyId)
            {
                // Create a new Product entity and set its properties from the posted Model
                var newtblRow = new tblthirdpartycredential
                {
                    Isenable = ObjItem.Isenable,
                    username = ObjItem.username,
                    password = ObjItem.password,
                    accountno = ObjItem.accountno,
                    subaccountno = ObjItem.subaccountno,
                    fkCompanyId = Company_Ids,
                };
                // Store the entity for later use
                tblRowList.Add(newtblRow);
            }
            // Add all tabel rows
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                IQueryable<tblthirdpartycredential> Obj_thirdpartycredentialdata = ObjEmergeDALDataContext.tblthirdpartycredentials.Where(db => db.fkCompanyId == Company_Ids);
                if (Obj_thirdpartycredentialdata.Count() == 0)
                {
                    //var CreateResult = 
                        ObjBALCompany.CreateThirdPartyDetailsByCompanyId(tblRowList);
                }
            }
            return Json(new { msg = "success" });


        }


        #endregion

        public string GetStateCode(int StateId)
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {

                string StateCode = (from u in ObjDALDataContext.tblStates
                                    where u.pkStateId == StateId
                                    select u.StateCode).FirstOrDefault();
                return StateCode;
            }
        }

        public int SetActiveInactive(int? fkCompanyid)
        {
            int Status = 0;
            BALCompany ObjBALCompany = new BALCompany();
            if (fkCompanyid != null)
            {
                bool? IsActive = ObjBALCompany.GetLatestOrderDate(fkCompanyid);
                if (IsActive == true)
                {
                    Status = 1;
                    UpdateCallLog("imgbtn_5", fkCompanyid);
                    UpdateCompanyIsActive(0, fkCompanyid);
                }
                else
                {
                    Status = 2;
                    UpdateCallLog("imgbtn_6", fkCompanyid);
                    UpdateCompanyIsActive(1, fkCompanyid);
                }
            }
            else
            {
                Status = 3;
            }


            return Status;

        }

        public void UpdateCallLog(string btnName, int? fkCompanyid)
        {
            Guid UserId;
            int? pkCompanyId;
            MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));
            if (ObjMembershipUser != null)
            {
                UserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
                pkCompanyId = fkCompanyid;
                string Comments = "";
                string[] Btnstr = btnName.Split('_');
                switch (Btnstr[1].ToLower())
                {
                    case "5": Comments = "##5##"; break;
                    case "6": Comments = "##6##"; break;
                    case "7": Comments = "##7##"; break;
                    case "8": Comments = "##8##"; break;
                    case "9": Comments = "##9##"; break;
                }
                DateTime CreatedDate = System.DateTime.Now;
                AddStatusToCallLog(pkCompanyId, UserId, Comments, CreatedDate);
            }

        }

        public void UpdateCompanyIsActive(byte preStatus, int? fkCompanyid)
        {
            BALCompany ObjBALCompany = new BALCompany();
            tblCompany ObjtblCompany = new tblCompany();
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            //int CompanyId = 0;
            Guid UserId = Guid.Empty;
            var dbCollection = ObjBALCompanyUsers.GetUserInformationByMembershipUserId(new Guid(Membership.GetUser(HttpContext.User.Identity.Name).ProviderUserKey.ToString()));
            if (dbCollection.Count > 0)
            {
                UserId = dbCollection.First().fkUserId;

            }


            if (fkCompanyid != null)
            {
                ObjtblCompany.pkCompanyId = fkCompanyid.GetValueOrDefault();
                ObjtblCompany.IsActive = preStatus;
                ObjtblCompany.LastModifiedDate = DateTime.Now;
                ObjtblCompany.LastModifiedById = UserId;
               // int success = 
                    ObjBALCompany.UpdateCompanyIsActive(ObjtblCompany);

            }
        }
        private int AddStatusToCallLog(int? pkCompanyId, Guid UserId, string Comments, DateTime CreatedDate)
        {
            BALCompany ObjBALCompany = new BALCompany();
            int Result = 0;
            try
            {
                Result = ObjBALCompany.AddStatusToCallLog(pkCompanyId, UserId, Comments, CreatedDate);
            }
            catch (Exception)
            {

            }
            finally
            {
                ObjBALCompany = null;
            }
            return Result;
        }

        private int Add_StatusToCallLog(int pkCompanyId, Guid UserId, string Comments, DateTime CreatedDate)
        {
            BALCompany ObjBALCompany = new BALCompany();
            int Result = 0;
            try
            {
                Result = ObjBALCompany.Add_StatusToCallLog(pkCompanyId, UserId, Comments, CreatedDate);
            }
            catch (Exception)
            {

            }
            finally
            {
                ObjBALCompany = null;
            }
            return Result;
        }

    }
}
