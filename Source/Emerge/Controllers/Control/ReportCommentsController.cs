﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Emerge.Models;
using System.IO;
using Emerge.Common;
using System.Web.UI;
using Emerge.Data;
using System.Text;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {

        /// <summary>
        /// To Show the Report Comments Records.
        /// </summary>
        /// <returns></returns>
        public ActionResult ReportComments()
        {
            ReportCommentModel ObjModel = new ReportCommentModel();
            BALContentManagement ObjBALContentManagement = new BALContentManagement();
            var DBContent = ObjBALContentManagement.GetReportCommentsRows();
            ObjModel.Rows = DBContent;
            ObjModel.LineNumberDataSource = ObjBALContentManagement.GetLineNoForReportCommentsFromDB();
            List<SelectListItem> obj = new List<SelectListItem>();
            foreach (var line in ObjModel.LineNumberDataSource)
            {
                //Skip those elements, which is disabled.
                if (line.IsVisible)
                {
                    obj.Add(new SelectListItem { Text = line.LineName, Value = line.pkLineId.ToString() });
                }
            }
            ViewData["Lineno"] = obj;//for Dropdown List.
            if (Request.QueryString["Status"] != null)
            {
                int status = Convert.ToInt32(Request.QueryString["Status"]);
                if (status == 1)
                {
                    ObjModel.Message = "<span class='successmsg' style='text-align: center;margin-top: -23px;margin-bottom: 28px;'>Comments updated successfully</span>";
                }
                else if (status == 2)
                {
                    ObjModel.Message = "<span class='successmsg' style='text-align: center;margin-top: -23px;margin-bottom: 28px;'>Comments added successfully</span>";
                }
                else if (status == 3)
                {
                    ObjModel.Message = "<span class='successmsg' style='text-align: center;margin-top: -23px;margin-bottom: 28px;'>Comments deleted successfully</span>";
                }
                else if (status == 4)
                {
                    ObjModel.Message = "<span class='successmsg' style='text-align: center;margin-top: -23px;margin-bottom: 28px;'>Settings updated successfully.</span>";
                }
                else if (status == -1)
                {
                    ObjModel.Message = "<span class='errormsg' >Title or Name already exist</span>";
                }
                else
                {
                    ObjModel.Message = "<span class='errormsg'>error is occured while updating content</span>";
                }
            }
            return View(ObjModel);
        }

        /// <summary>
        /// To adding the new Record.
        /// </summary>
        public string AddReportComments(string buttonTitle, string Comments, int LineNo)
        {
            string strSuccess = string.Empty;
            int Success = 0;
            try
            {
                tblReportComment objtblReportComment = new tblReportComment();
                objtblReportComment.ButtonTitle = buttonTitle;
                objtblReportComment.Comments = Comments;
                objtblReportComment.fkLineId = LineNo;
                BALContentManagement ObjBALContentManagement = new BALContentManagement();
                Success = ObjBALContentManagement.InsertRepotComments(objtblReportComment);
                if (Success > 0)
                {
                    strSuccess = "Added_" +Convert.ToString(Success);
                }
                else if (Success == -1)
                {
                    strSuccess = "Exists_-1";
                }
                else
                {
                    strSuccess = "Error"; 
                }
            }
            catch (Exception)
            {
            }
            return strSuccess;

        }




        /// <summary>
        /// To Updating the selected Record.
        /// </summary>
        public string UpdateReportComments(int pkReportCommentsId, string buttonTitle, string Comments, int LineNo)
        {
            string strSuccess = string.Empty;
            try
            {
                BALContentManagement ObjBALContentManagement = new BALContentManagement();
                tblReportComment objtblReportComment = new tblReportComment();
                objtblReportComment.ButtonTitle = buttonTitle;
                objtblReportComment.Comments = Comments;
                objtblReportComment.fkLineId = LineNo;
                objtblReportComment.pkReportCommentsId = pkReportCommentsId;
                int Success = ObjBALContentManagement.UpdateReportComments(objtblReportComment);
                if (Success == 1)
                {
                    strSuccess = "Updated";
                }
                else if (Success == -1)
                {
                    strSuccess = "Exists";
                }
                else
                {
                    strSuccess = "Error";
                }
            }
            catch //(Exception ee)
            {
                strSuccess = "Error";
            }
            return strSuccess;
        }

        /// <summary>
        /// To Deleting the selected Record.
        /// </summary>
        public ActionResult DeleteReportComments(int pkReportCommentsId)
        {
            int Success = 0;

            BALContentManagement ObjBALContentManagement = new BALContentManagement();
            try
            {
                Success = ObjBALContentManagement.DeleteReportComments(pkReportCommentsId);
                if (Success == 1)
                {
                    Success = 3;
                }
                else
                {
                    Success = -2;
                }
            }
            catch (Exception)
            {

            }
            return Json(Success);

        }

       
        /// <summary>
        /// To Getting the Report Comments Data.
        /// </summary>
        /// <returns></returns>
        public JsonResult GetReportComments()
        {
            List<Proc_GetLineReportCommentsResult> result = new List<Proc_GetLineReportCommentsResult>();
            try
            {
                BALContentManagement ObjBALContentManagement = new BALContentManagement();
                result = ObjBALContentManagement.GetReportCommentsRows();
                
            }
            catch// (Exception ee)
            {
                return Json("Error");
            }
            return Json(result);
        }
        /// <summary>
        /// To Getting the Line Number section data.
        /// </summary>
        public JsonResult AddLineNoforReportComments(string lineName)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            //string strSuccess = string.Empty;
            try
            {
                BALContentManagement ObjBALContentManagement = new BALContentManagement();
                dic = ObjBALContentManagement.InsertLineNumberforReport(lineName);
            }
            catch (Exception)
            {
            }
            return Json(dic);
        }





    }
}
