﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using System.Web.Security;
using System.Globalization;
 

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /PendingReportsForAdmin/

        public ActionResult PartialPendingReportsForAdmin()
        {
            return View();
        }

        public ActionResult PartialPendingReportsForAdminBind( int page, int pageSize)
        {
            int PageSize = pageSize;
            int iPageNo = page;
           
            BALOrders ObjBALOrders = new BALOrders();
            bool SavedReport6Month = false;



            MembershipUser Member = Membership.GetUser(User.Identity.Name);
            string[] UserRole = Roles.GetRolesForUser(Member.Email);
            if (UserRole.Contains("SystemAdmin"))
            {
                SavedReport6Month = true;
            }
            var PendingReportsForAdminBindCollection = ObjBALOrders.GetSavedReportsMVC(0,
                                                                                            0,
                                                                                            0,
                                                                                            0,
                                                                                            "OrderDt",
                                                                                            "DESC",
                                                                                            true,
                                                                                            iPageNo,
                                                                                            PageSize,
                                                                                            string.Empty,
                                                                                            string.Empty,
                                                                                            string.Empty,
                                                                                            1, "AllReports", false, string.Empty, SavedReport6Month, string.Empty);/*1 for Pending reports*/


            return Json(new
            {
                Products = PendingReportsForAdminBindCollection,
                TotalCount =PendingReportsForAdminBindCollection.Count > 0 ? PendingReportsForAdminBindCollection.ElementAt(0).TotalRec : 0
            },
                  JsonRequestBehavior.AllowGet);


        }


        public ActionResult UpdatePendingReportsForAdminCompleteReview(int OrderId, string OrderNoteByOrder)
        {
            BALOrders ObjBALOrders=new BALOrders();

            string strMessage = "";
            int Result = ObjBALOrders.UpdateOrderNoteByOrderId(OrderId,OrderNoteByOrder);
            if (Result == 1)
            {
                strMessage = "<span class='successmsg'>Information Updated Successfully</span>";


            }
            else 
            {
                strMessage = "<span class='errormsg'>Problem while saving. Please try again</span>";
            
            
            }



            return Json(new { InfUpdateMessage = strMessage });
           
        }

        public ActionResult SendEmails(string OrderNo, string hfApplicantName, string hfReportCodes, string hfUserEmail, string hfOrderNote)
        {

            bool EmailSent = false;
            string MessageBody = "";
            string Subject = string.Empty;
            BALGeneral ObjBALGeneral = new BALGeneral();
            string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;
            tblEmailTemplate emailContent = new tblEmailTemplate();
            Guid Gid = new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852");
            int PktemplateId = (int)(EmailTemplates.PendingReportUpdate);
            emailContent = ObjBALGeneral.GetEmailTemplate(Gid, PktemplateId);
            string emailText = emailContent.TemplateContent;
            BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();


            #region For New Style SystemTemplate
            string Bookmarkfile = string.Empty;
            string bookmarkfilepath = string.Empty;

            Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
            Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
            Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
            emailText = Bookmarkfile;
            #endregion



            
            Bookmarks objBookmark = new Bookmarks();

            objBookmark.EmailContent = emailText;

            objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
            objBookmark.WebsiteName = "Emerge";

            string strOrderNumber = "Order Number: " + OrderNo;
            string ApplicantName = hfApplicantName;
            string firstname = string.Empty;
            string lastname = string.Empty;

            CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;

            string strApplicantName = string.Empty;

            if (!string.IsNullOrEmpty(ApplicantName))
            {
                if (ApplicantName.Contains(','))
                {
                    
                    firstname = ApplicantName.Split(',').LastOrDefault();
                    lastname = ApplicantName.Split(',').FirstOrDefault();
                    string strfirstname = firstname != string.Empty ? textInfo.ToTitleCase(firstname) : string.Empty;
                    string strlastname = lastname != string.Empty ? textInfo.ToTitleCase(lastname) : string.Empty;
                    strApplicantName = strfirstname + " " + strlastname;

                }
                else
                {
                    ApplicantName = ApplicantName != string.Empty ? textInfo.ToTitleCase(ApplicantName) : string.Empty;
                    strApplicantName = ApplicantName;
                }

            }
            else
            {
                strApplicantName = strOrderNumber;  
              
            }
           objBookmark.ApplicantName = strApplicantName;

            Subject = emailContent.TemplateSubject.Replace("%ApplicantName%", strApplicantName);

            objBookmark.Comments = "<table style='border-collapse: collapse;' border='0' cellpadding='0' cellspacing='0'  width='95%' align='center'><tr><td style='color:#000; background-color:#FFFFDD'><p style=text-align: justify;'> <div style='padding-left:10px;padding-right:10px'><b>Pending Report Update:</b> " + hfOrderNote + "</p></td></tr></table>";
            objBookmark.ReportCode = hfReportCodes;

            MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Guid.Empty);






           
            //For email status//
            BALGeneral objbalgenral = new BALGeneral();
            MembershipUser ObjMembershipUser = Membership.GetUser(hfUserEmail);
            Guid UserId = Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
            int pktemplateid = PktemplateId;
            Guid user_ID = UserId;
            int fkcompanyuserid = 0;
            bool email_status = objbalgenral.UserEmailStatusCheck(user_ID, pktemplateid, fkcompanyuserid); //

            if (email_status==true)
            {
            EmailSent = Utility.SendMail(hfUserEmail, string.Empty, string.Empty, BALGeneral.GetGeneralSettings().SupportEmailId, MessageBody, Subject);
            }
            if (EmailSent == true)
            {
            }
            else
            {
            }

            return Json(new { });

        }


      



    }
}
