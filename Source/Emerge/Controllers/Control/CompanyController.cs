﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using System.IO;
using System.Text;
using Kendo.Mvc;
using System.ComponentModel;
using Kendo.Mvc.UI;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        [ValidateInput(false)]
        public ActionResult GetAllCompanies()
        {
            CompanyModel ObjCompanyModel = new CompanyModel();
            if (TempData["message"] != null)
            {
                ObjCompanyModel.strResult = TempData["message"].ToString();
            }

            return View(ObjCompanyModel);
        }

        public ActionResult GetCompanylist(int page, int pageSize, string group, List<GridSort> sort, List<GridFilter> filters)
        {
            # region filter
            try
            {
                int PageNum = page;
                int PageSize = pageSize;
                string SortingColumn = "CompanyName";
                string SortingDirection = "asc";
                int TotalRec = 0;
                byte ActionType = 0;
                bool IsPaging = true;
                if (sort != null)
                {
                    SortingDirection = sort[0].Dir;
                    SortingColumn = sort[0].Field;
                }
                Int16 CompanyType = -1; string StatusType = "SelectStatus";
                BALCompany ObjBALCompany = new BALCompany();
                //List<CompanyViewModel> ObjCompanyViewModel = new List<CompanyViewModel>();
                List<Proc_Get_AllCompaniesForAdminResult> OData = new List<Proc_Get_AllCompaniesForAdminResult>();

                if (filters != null)
                {
                    if (filters.Count == 2)
                    {
                        CompanyType = filters[0].Value == null ? Convert.ToInt16(-1) : Convert.ToInt16(filters[0].Value);
                        StatusType = filters[1].Value.ToString();
                        OData = ObjBALCompany.GetAllCompanyForAdmin(PageNum, PageSize, IsPaging, CompanyType, StatusType, SortingColumn, SortingDirection, string.Empty, 0, string.Empty, out TotalRec).ToList();
                    }
                    else if (filters.Count > 2)
                    {
                        CompanyType = filters[0].Value == null ? Convert.ToInt16(-1) : Convert.ToInt16(filters[0].Value);
                        StatusType = filters[1].Value.ToString();
                        string ColumnName = filters[2].Field;
                        string FilterValue = filters[2].Value.ToString();
                        ActionType = GetFilterAction(filters[2], ActionType);
                        OData = ObjBALCompany.GetAllCompanyForAdmin(PageNum, PageSize, IsPaging, CompanyType, StatusType, SortingColumn, SortingDirection, ColumnName, ActionType, FilterValue, out TotalRec).ToList();
                    }
                    else
                    {
                        foreach (var ObjFilter in filters)
                        {
                            OData = FilterCompanies(ObjFilter, PageNum, PageSize, IsPaging, CompanyType, StatusType, SortingColumn, SortingDirection, out TotalRec);
                            
                        }
                    }
                }
                else
                {
                    OData = ObjBALCompany.GetAllCompanyForAdmin(PageNum, PageSize, IsPaging, CompanyType, StatusType, SortingColumn, SortingDirection, string.Empty, 0, string.Empty, out TotalRec).ToList();
                }
                if (OData.Count > 0)
                {
                    //for (int j = 0; j < OData.Count(); j++)
                    //{
                    //    if ((OData.ElementAt(j).StatusText == "InActive") || (OData.ElementAt(j).StatusText == "Active"))
                    //    {
                    //        Guid pkCompanyId = Guid.Parse(OData.ElementAt(j).pkCompanyId.ToString());
                    //        BALCompany objbal = new BALCompany();
                    //        bool? IsActive = objbal.GetLatestOrderDate(pkCompanyId);
                    //        if (IsActive == true)
                    //        {
                    //            OData.ElementAt(j).StatusText = "Active";
                    //        }
                    //        else
                    //        {
                    //            OData.ElementAt(j).StatusText = "InActive";
                    //        }
                    //    }

                    //}
                    return Json(new { Reclist = OData, TotalRec = OData.ElementAt(0).TotalRec });
                }
                else
                {
                    return Json(new { Reclist = OData, TotalRec = OData.Count });
                }
                //if (OData.Count > 0)
                //{
                //    ObjCompanyViewModel = OData.Select(data => new CompanyViewModel
                //    {

                //        Isenabled = data.Isenabled,
                //        CompanyName = data.CompanyName,
                //        City = data.City,
                //        CompanyAccountNumber = data.CompanyAccountNumber,
                //        MainContactPersonName = data.MainContactPersonName,
                //        PhoneNo1 = data.PhoneNo1,
                //        TotalLocations = data.TotalLocations,
                //        TotalUsers = data.TotalUsers,
                //        SaleRepInitial = data.SaleRepInitial,
                //        pkCompanyId = data.pkCompanyId,
                //        pkCompanyTypeId = data.fkCompanyTypeId,
                //        CompanyType = data.CompanyType,
                //        StatusText = data.StatusText,
                //        pkLocationId = data.pkLocationId,
                //        TotalRec = data.TotalRec

                //    }).ToList();
                //}
                //return Json(new { Reclist = OData, TotalRec = OData.ElementAt(0).TotalRec });
            }
            catch (Exception)
            {
                return View(new GridModel(new List<CompanyViewModel>()));
            }

            #endregion
        }

        private static List<Proc_Get_AllCompaniesForAdminResult> FilterCompanies(GridFilter filter, int PageNum, int PageSize, bool IsPaging, Int16 CompanyType, string StatusType, string SortingColumn, string SortingDirection, out int TotalRec)
        {
            BALCompany ObjBALCompany = new BALCompany();
            List<Proc_Get_AllCompaniesForAdminResult> source = new List<Proc_Get_AllCompaniesForAdminResult>();
            byte ActionType = 0;
                string ColumnName = filter.Field;
                string FilterValue = filter.Value.ToString();
                ActionType = GetFilterAction(filter, ActionType);
                source = ObjBALCompany.GetAllCompanyForAdmin(PageNum, PageSize, IsPaging, CompanyType, StatusType, SortingColumn, SortingDirection, ColumnName, ActionType, FilterValue, out TotalRec).ToList();
            return source;
        }

        public static byte GetFilterAction(GridFilter filter, byte ActionType)
        {
            switch (filter.Operator)
            {
                case "contains":
                    ActionType = 1;
                    break;
                case "startswith":
                    ActionType = 2;
                    break;
                case "endswith":
                    ActionType = 3;
                    break;
                case "eq":
                    ActionType = 4;
                    break;
                case "neq":
                    ActionType = 5;
                    break;
                case "gt":
                    ActionType = 6;
                    break;
                case "lt":
                    ActionType = 7;
                    break;
                case "lte":
                    ActionType = 8;
                    break;
                case "gte":
                    ActionType = 9;
                    break;
            }
            return ActionType;
        }
        
        [HttpPost]
        public ActionResult ReadLocation(int pkCompanyId)
        {
            BALCompany ObjBALCompany = new BALCompany();
            int TotalRec = 0;
            List<Proc_Get_AllLocationsForAdminResult> ObjLocations = ObjBALCompany.GetAllLocationsForAdmin(1, 50, false, 0, -1, pkCompanyId, "City", "ASC", string.Empty, 0, string.Empty, out TotalRec).ToList();//.Select(data => new LocationViewModel
            //{
            //    pkLocationId = data.pkLocationId,
            //    fkCompanyId = data.fkCompanyId,
            //    LocationContact = data.LocationContact,
            //    City = data.City,
            //    TotalUsers = data.TotalUsers,
            //    StatusText = data.StatusText

            //}).ToList();

            return Json(ObjLocations);
        }

        [HttpPost]
        public ActionResult ReadUser(int fkCompanyId, int fkLocationId)
        {
            BALCompany ObjBALCompany = new BALCompany();
            int TotalRec=0;
            List<Proc_Get_AllEmergeUsersForAdminResult> ObjLocations = ObjBALCompany.GetAllEmergeUsersForAdmin(1, 50, false, -1, fkCompanyId, fkLocationId, "FirstName", "ASC", string.Empty, 0, string.Empty, out TotalRec).ToList();//.Select(data => new UserViewModel
            //{
            //    pkCompanyId = data.pkCompanyId,
            //    FirstName = data.FirstName,
            //    UserDisplayRole = data.UserDisplayRole,
            //    UserName = data.UserName,
            //    IsEnabledUser = data.IsEnabledUser,
            //    pkCompanyUserId = data.pkCompanyUserId,
            //    pkLocationId = data.fkLocationId

            //}).ToList();
            return Json(new { Reclist = ObjLocations, TotalRec = ObjLocations.Count });
        }

        public ActionResult EnableCompany(string pkCompanyIds)
        {
            int result = 0;
            BALCompany ObjBALCompany = new BALCompany();
            List<tblCompany> CompanyList = new List<tblCompany>();
            string[] stringSeparators = new string[] { "#" };
            string[] ListpkCompanyIds = pkCompanyIds.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
            try
            {
                for (int i = 0; i < ListpkCompanyIds.Length; i++)
                {

                    tblCompany objtblCompany = new tblCompany();
                    objtblCompany.pkCompanyId = Convert.ToInt32(ListpkCompanyIds[i]);
                    CompanyList.Add(objtblCompany);
                }
                result = ObjBALCompany.ActionOnCompany(CompanyList, Convert.ToByte(1), Convert.ToBoolean(1));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBALCompany = null;
            }
            return Json(result);

        }

        public ActionResult DisableCompany(string pkCompanyIds)
        {
            int result = 0;
            BALCompany ObjBALCompany = new BALCompany();
            List<tblCompany> CompanyList = new List<tblCompany>();
            string[] stringSeparators = new string[] { "#" };
            string[] ListpkCompanyIds = pkCompanyIds.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
            try
            {
                for (int i = 0; i < ListpkCompanyIds.Length; i++)
                {

                    tblCompany objtblCompany = new tblCompany();
                    objtblCompany.pkCompanyId = Convert.ToInt32(ListpkCompanyIds[i]);
                    CompanyList.Add(objtblCompany);
                }
                result = ObjBALCompany.ActionOnCompany(CompanyList, Convert.ToByte(1), Convert.ToBoolean(0));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBALCompany = null;
            }
            return Json(result);
        }

        public ActionResult DeleteCompany(string pkCompanyIds)
        {
            int result = 0;
            BALCompany ObjBALCompany = new BALCompany();
            List<tblCompany> CompanyList = new List<tblCompany>();
            string[] stringSeparators = new string[] { "#" };
            string[] ListpkCompanyIds = pkCompanyIds.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
            try
            {
                for (int i = 0; i < ListpkCompanyIds.Length; i++)
                {

                    tblCompany objtblCompany = new tblCompany();
                    objtblCompany.pkCompanyId = Convert.ToInt32(ListpkCompanyIds[i]);
                    CompanyList.Add(objtblCompany);
                }
                result = ObjBALCompany.ActionOnCompany(CompanyList, Convert.ToByte(2), Convert.ToBoolean(1));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBALCompany = null;
            }
            return Json(result);
        }

        public ActionResult DeleteCompanyById(int pkCompanyId)
        {
            int result = 0;
            BALCompany ObjBALCompany = new BALCompany();
            try
            {
                result = ObjBALCompany.DeleteCompanyById(pkCompanyId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBALCompany = null;
            }
            return Json(result);
        }

        public ActionResult GetCompanyType()
        {
            BALCompanyType objBALCompanyType = new BALCompanyType();
            var list = objBALCompanyType.GetAllCompanyType();
            return Json(list);

        }

        public ActionResult GetCompanyStatus()
        {
            List<SelectListItem> StatusText = new List<SelectListItem>();
            SelectListItem s1 = new SelectListItem { Text = "Active", Value = "Active" };
            SelectListItem s2 = new SelectListItem { Text = "InActive", Value = "InActive" };
            SelectListItem s3 = new SelectListItem { Text = "Locked", Value = "Locked" };
            SelectListItem s4 = new SelectListItem { Text = "PastDue", Value = "PastDue" };
            SelectListItem s5 = new SelectListItem { Text = "InCollections", Value = "InCollections" };
            StatusText.Add(s1);
            StatusText.Add(s2);
            StatusText.Add(s3);
            StatusText.Add(s4);
            StatusText.Add(s5);

            return Json(StatusText);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SaveDocumentDetailsToSession(HttpPostedFileBase PDFfileName, string txtDocumentTitle)
        {
            string strPagetoRedirect = string.Empty;
            string strResult = string.Empty;
            try
            {
                if (PDFfileName != null)
                {
                    if (PDFfileName != null && PDFfileName.ContentLength > 0)
                    {
                        if (PDFfileName.FileName != string.Empty)
                        {
                            {
                                if (CheckExtensionsForUserAgreement(PDFfileName))
                                {
                                    string sExt = Path.GetExtension(PDFfileName.FileName).ToLower();
                                    string sFileName = DateTime.Now.ToFileTime().ToString() + sExt;
                                    //string sFilePath = "";
                                   // sFilePath = HttpContext.Server.MapPath("~/Resources/Upload/CompanyPDF/");
                                    if (System.IO.File.Exists(HttpContext.Server.MapPath(@"~\Resources\Upload\CompanyPDF\") + sFileName))
                                    {
                                        try
                                        {
                                            System.IO.File.Delete(HttpContext.Server.MapPath(@"~\Resources\Upload\CompanyPDF\") + sFileName);
                                        }
                                        catch { }
                                    }

                                    PDFfileName.SaveAs(HttpContext.Server.MapPath(@"~\Resources\Upload\CompanyPDF\") + sFileName);

                                    //string fPath = Server.MapPath("~/Resources/Upload/CompanyPDF/") + sFileName;

                                    Dictionary<string, string> DicDocument = new Dictionary<string, string>();
                                    DicDocument.Add(txtDocumentTitle.Trim(), sFileName);
                                    Session["Document"] = DicDocument;
                                    strPagetoRedirect = "AddCompanies";
                                }
                                else
                                {
                                    strPagetoRedirect = "GetAllCompanies";
                                    strResult = "<span class='errormsg'>Only .pdf files are allowed to upload.</span>";
                                }
                            }
                        }

                    }
                }
            }
            catch
            {
                strPagetoRedirect = "GetAllCompanies";
                strResult = "<span class='errormsg'>Some error occurred while uploading. Please try again!</span>";
            }
            TempData["message"] = strResult;
            return RedirectToAction(strPagetoRedirect);
        }

        public JsonResult GetLogCommentsForCompany(int pkCompanyId)
        {
            StringBuilder sb = new StringBuilder();
            int pkCompId = pkCompanyId;
            BALCompany ObjBALCompany = new BALCompany();
            List<Proc_Get_CommentsForLead_ServiceResult> ObjList = new List<Proc_Get_CommentsForLead_ServiceResult>();
            ObjList = ObjBALCompany.GetCommentsForCompany_Service(pkCompId).OrderByDescending(d => d.CreatedDate).ToList<Proc_Get_CommentsForLead_ServiceResult>();
            sb.Append("<table width='100%' style='border: 0px solid #cdcdcd; padding: 0 0 0 10px;'>");
            if (ObjList.Count > 0)
            {
                for (int i = 0; i <= ObjList.Count - 1; i++)
                {
                    sb.Append("<tr><td width='30%' align='left'>");
                    sb.Append(ObjList.ElementAt(i).CreatedDate.ToString("MM/dd/yy, hh:mm tt"));
                    sb.Append("</td><td width='30%' align='left'>");
                    sb.Append(ObjList.ElementAt(i).FirstName + " " + ObjList.ElementAt(i).LastName);
                    sb.Append("</td><td width='30%' align='left'>");
                    string[] stringSeparators = new string[] { "##" };
                    if (ObjList.ElementAt(i).LeadComments.Contains("##"))
                    {
                        string[] StatusArr = ObjList.ElementAt(i).LeadComments.ToString().Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                        if (StatusArr.Length > 0)
                        {
                            string Status = StatusArr[0];
                            if (Status == "10")
                            {
                                sb.Append("<img src=" + @Url.Content("~/Content/themes/base/Images/demo_scheduled.png") + ">");
                            }
                            else if (Status == "11")
                            {
                                sb.Append("<img src=" + @Url.Content("~/Content/themes/base/Images/document_sent.png") + ">");
                            }
                            else if (Status == "12")
                            {
                                sb.Append("<img src=" + @Url.Content("~/Content/themes/base/Images/QualityControlSurvey.png") + ">");
                            }
                            else if (Status == "13")
                            {
                                sb.Append("<img src=" + @Url.Content("~/Content/themes/base/Images/Demo_Completed.png") + ">");
                            }
                            else
                            {
                                sb.Append("<img src=" + @Url.Content("~/Content/themes/base/Images/emerge-office-milestone-" + Status + "-on.png") + ">");
                            }
                        }
                    }
                    else
                    {
                        string retstr = string.Empty;
                        retstr = ObjList.ElementAt(i).LeadComments.ToString();
                        if (retstr.Length > 15)
                        {
                            retstr = retstr.Substring(0, 15);
                            retstr += "...";
                        }
                        sb.Append(retstr);
                    }
                    sb.Append("</td></tr>");
                }
                sb.Append("</table>");
            }
            else
            {
                sb.Append("<tr><td align='left'>No Comments</td></tr></table>");
            }
            return Json(new { Data = sb.ToString() });
        }


    }
}
