﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Data;
using Emerge.Common;
using Emerge.Models;
using Emerge.Services;
using WordPress.Net;
using System.Configuration;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /Newsletter/

        public ActionResult Newsletters()
        {
            EmailSystemTemplatesModel ObjModel = new EmailSystemTemplatesModel();

            ObjModel.EmailTemplatesList = GetNewsletterEmailTemplates(0, Utility.SiteApplicationId, false, 0);

            ObjModel.lstCompanyList = GetCompanies();

            ObjModel.DicUserType = GetUsersType();


            return View(ObjModel);
        }
        public List<Proc_USA_LoadEmailTemplatesResult> GetNewsletterEmailTemplates(int TemplateId, Guid ApplicationId, bool IsSystemTemplate, int TemplateCode)
        {
            BALEmailSystemTemplates ObjBALEmailSystemTemplates = new BALEmailSystemTemplates();
            List<Proc_USA_LoadEmailTemplatesResult> ObjEmailTemplate = new List<Proc_USA_LoadEmailTemplatesResult>();

            ObjEmailTemplate = ObjBALEmailSystemTemplates.GetEmailTemplates(TemplateId, ApplicationId, IsSystemTemplate, TemplateCode);

            if (IsSystemTemplate == false)
            {
                ObjEmailTemplate.Insert(0, new Proc_USA_LoadEmailTemplatesResult
                {
                    pkTemplateId = 0,
                    TemplateSubject = "<--Select Template-->",
                    TemplateContent = string.Empty,
                    TemplateName = string.Empty
                });
            }
            return ObjEmailTemplate;
        }
        public ActionResult GetNewsletterTemplateContents(string pkTemplateId, string IsSystemTemplate)
        {
            BALEmailSystemTemplates ObjTemplate = new BALEmailSystemTemplates();
            List<Proc_USA_LoadEmailTemplatesResult> ObjRecords = ObjTemplate.GetEmailTemplates(Convert.ToInt32(pkTemplateId), Utility.SiteApplicationId, Convert.ToBoolean(IsSystemTemplate), 0);
            string UpdatedContent = RecentPosts(ObjRecords.ElementAt(0).TemplateContent);
            return Json(new
            {
                TempName = ObjRecords.ElementAt(0).TemplateName,
                TempSub = ObjRecords.ElementAt(0).TemplateSubject,
                TempCont = UpdatedContent,
                IsTemplateEnabled = ObjRecords.ElementAt(0).IsEnabled,
                DefaultContent = ObjRecords.ElementAt(0).DefaultTemplateContent,
                TemplateType = ObjRecords.ElementAt(0).TemplateType
            });
        }
        public List<CompanyList> GetCompanies()
        {
            BALCompany objBALCompany = new BALCompany();
            List<CompanyList> lstCompanyList = new List<CompanyList>();
            try
            {
                lstCompanyList = objBALCompany.GetAllCompany(Utility.SiteApplicationId, true, false);

                lstCompanyList.Insert(0, new CompanyList
                {
                    pkCompanyId = 0,
                    CompanyName = "All Companies"
                });

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstCompanyList;
        }

        public Dictionary<int, string> GetUsersType()
        {
            Dictionary<int, string> ObjUsersType = new Dictionary<int, string>();

            ObjUsersType.Add(0, "<--Please Select-->");
            ObjUsersType.Add(1, "All Members");
            ObjUsersType.Add(2, "All Basic User");
            ObjUsersType.Add(3, "All Branch Manager");
            ObjUsersType.Add(4, "All Corporate Manager");

            return ObjUsersType;
        }
        public ActionResult SendEmail(EmailSystemTemplatesModel ObjModel)
        {
            string strResult = "";
            try
            {
                strResult = SendMailMultiple(ObjModel);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return Json(new { Message = strResult });
        }
        public string SendMailMultiple(EmailSystemTemplatesModel ObjModel)
        {
            string strResult = "";
            try
            {
                BALEmailSystemTemplates ObjEmailTemplate = new BALEmailSystemTemplates();
                tblAlert ObjtblAlert = new tblAlert();
                ObjtblAlert.AlertTypeId = ObjModel.ddlTemplateSubject;
                ObjtblAlert.AlertType = ObjModel.TemplateType;
                ObjtblAlert.AlertSentDate = DateTime.Now;

                int UserType = ObjModel.UserType;
                int pkCompanyId = ObjModel.pkCompanyId;
                List<proc_Get_UserEmail_ByRoleIdResult> ObjAllUsers = ObjEmailTemplate.LoadUsers(UserType, pkCompanyId);

                if (ObjAllUsers.Count != 0)
                {
                    //int AlertType = ObjModel.TemplateType;
                   // string AlertTypeId = ObjModel.ddlTemplateSubject;
                    List<string> LstReciverEmailIds = new List<string>();
                    for (int i = 0; i < ObjAllUsers.Count; i++)
                    {
                        string ReciverEmail;
                        ReciverEmail = ObjAllUsers.ElementAt(i).Email.ToString();
                        LstReciverEmailIds.Add(ReciverEmail);
                        ObjtblAlert.fkUserId = new Guid(ObjAllUsers.ElementAt(i).UserId.ToString());
                        //int AlertId =
                            ObjEmailTemplate.InsertAlerts(ObjtblAlert);
                    }

                    bool CheckSend = false;
                    string emailFrom = BALGeneral.GetGeneralSettings().SupportEmailId;

                    #region New way Bookmark
                    string MessageBody = string.Empty;
                    BALGeneral ObjBALGeneral = new BALGeneral();
                    string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Content/themes/base/images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;

                    BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                    Bookmarks objBookmark = new Bookmarks();

                    objBookmark.EmailContent = ObjModel.TemplateContent;
                    objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                    objBookmark.LoginLink = "<a style='color: #87ceea;' href='" + ApplicationPath.GetApplicationPath() + "'> Click here </a>";

                    MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Utility.GetUID(HttpContext.User.Identity.Name.ToString()));

                    //uncomment when login page is worked

                    #endregion


                    foreach (string User in LstReciverEmailIds)
                    {
                        CheckSend = Utility.SendMail(User, emailFrom, MessageBody, ObjModel.TemplateSubject);

                        //uncomment when login page is worked
                    }

                    if (CheckSend == true)
                    {
                        strResult = "<span class='successmsg'>Email sent successfully to All Users</span>";

                    }
                    else
                    {
                        strResult = "<span class='errormsg'>Error Occured while sending an email !</span>";

                    }
                }
                else
                {
                    strResult = "<span class='successmsg'>No user found for this company</span>";

                }
            }
            catch// (Exception ex)
            {

            }
            finally
            {

            }
            return strResult;
        }

        #region Articles
        private string RecentPosts(string Content)
        {
            WP wp;
            string strContent = string.Empty;
            string UserName = ConfigurationManager.AppSettings["WordPressUserName"].ToString();
            string Password = ConfigurationManager.AppSettings["WordPressPassword"].ToString();
            try
            {
                using (wp = new WP(UserName, Password))
                {
                    Post[] p = wp.GetRecentPosts(2);
                    strContent = BindPosts(p, Content);
                }
            }
            catch (Exception)
            {
                strContent = Content;
            }
            finally
            {
                wp = null;
            }
            return strContent;
        }
        private string BindPosts(Post[] p, string UpdContent)
        {
            try
            {
                for (int i = 0; i <= p.Count(); i++)
                {
                    string Title = (p.ElementAt(i).title).ToUpper();
                    string Contents = UpdContent.Replace("#wparticle[" + i + "]", "<div style='font-family: arial; font-size: 8pt; font-weight: bold; line-height: 20px; color: #6699ff; background-color: transparent; vertical-align: baseline; margin: 0px; padding: 0px; border-width: 0px; text-decoration: none;'><a style='color: #6699ff;  text-decoration: none;' href= '" + ApplicationPath.GetApplicationPath() + "articles/?p=" + p.ElementAt(i).postid + "'  target='_blank'>" + Title + "</a></div>");
                    //href='https://emerge.intelifi.com/articles/?p=" + p.ElementAt(i).postid + "'
                    UpdContent = Contents;

                }
            }
            catch (Exception)
            {
            }

            return UpdContent;
        }
        #endregion
    }
}
