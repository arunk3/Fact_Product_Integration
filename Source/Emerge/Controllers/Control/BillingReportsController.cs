﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        Guid SiteApplicationId = Emerge.Common.Utility.SiteApplicationId;// new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852");

        #region Billing
        public ActionResult Billing()
        {
            return View();
        }

        public PartialViewResult PartialBillingReport(string Month, string Year, string ReferenceNo, int? CompanyUserId, int? LocationId, string SalesRep, int? CompanyId)
        {
            ViewData["appId"] = SiteApplicationId;
            ViewData["Month"] = Month;
            ViewData["Year"] = Year;
            //ViewData["CompanyId"] = "7dc87d77-6bb5-4bb3-9837-fa861f36f281";
            ViewData["CompanyId"] = (CompanyId!=null) ?  Convert.ToInt32(CompanyId) : 0;
            ViewData["UserId"] = (CompanyUserId!=null) ? Convert.ToInt32(CompanyUserId) : 0;
            ViewData["LocationId"] = (LocationId!=null) ? Convert.ToInt32(LocationId) : 0;
            ViewData["SalesRep"] = !string.IsNullOrEmpty(SalesRep) ? SalesRep : Guid.Empty.ToString();
            ViewData["RefCode"] = !string.IsNullOrEmpty(ReferenceNo) ? ReferenceNo : "0";
            return PartialView("PartialBillingReport");
        }
        #endregion

        #region Vendor Report
        public ActionResult BillingStatements()
        {
            if (Request.QueryString["Type"] != null)
            {
                int Id = Convert.ToInt32(Request.QueryString["Type"]);
                ViewBag.OpenPopUp = Id;
            }
            else
            {
                ViewBag.OpenPopUp = -2;
            }
            return View();
        }
        #endregion

        #region Vendor Report
        public ActionResult VendorReport()
        {
            return View();
        }

        public PartialViewResult PartialVendorReport(string Month, string Year, string VendorId, string ReportType)
        {
            ViewData["appId"] = SiteApplicationId;
            ViewData["Month"] = Month;
            ViewData["Year"] = Year;
            ViewData["VendorId"] = !string.IsNullOrEmpty(VendorId) ? VendorId : "0";
            ViewData["ReportType"] = !string.IsNullOrEmpty(ReportType) ? ReportType : "0";

            return PartialView("PartialVendorReport");
        }
        #endregion


    }
}
