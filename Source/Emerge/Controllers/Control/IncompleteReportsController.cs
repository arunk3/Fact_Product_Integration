﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using System.Web.Security;


namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /IncompleteReports/

        public ActionResult PartialIncompleteReports()
        {
            return View();
        }

        public ActionResult IncompleteReportsBind(int page, int pageSize)
        {
            int PageSize = pageSize;
            int iPageNo = page;
            BALOrders ObjBALOrders = new BALOrders();

            bool SavedReport6Month = false;



            MembershipUser Member = Membership.GetUser(User.Identity.Name);
            string[] UserRole = Roles.GetRolesForUser(Member.Email);
            if (UserRole.Contains("SystemAdmin"))
            {
                SavedReport6Month = true;
            }
            var IncompleteReportsCollection = ObjBALOrders.GetSavedReportsMVC(0,
                                                                                               0,
                                                                                               0,
                                                                                               0,
                                                                                              "OrderDt",
                                                                                               "DESC",
                                                                                               true,
                                                                                               iPageNo,
                                                                                               PageSize,
                                                                                               string.Empty,
                                                                                               string.Empty,
                                                                                               string.Empty,
                                                                                               3, "AllReports", false, string.Empty, SavedReport6Month, string.Empty);/*3 for Incomplete reports*/



            int TotalRec = 0;
            if (IncompleteReportsCollection.Count > 0)
            {
                TotalRec = IncompleteReportsCollection.Count;
            }
            return Json(new
            {
                Products = IncompleteReportsCollection,
                TotalCount = TotalRec
            }, JsonRequestBehavior.AllowGet);


         

        }









    }
}
