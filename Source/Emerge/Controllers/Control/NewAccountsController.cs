﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using System.Web.Security;
using System.IO;
using System.Text.RegularExpressions;


namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /NewAccounts/

        public ActionResult PartialNewAccounts()
        {

            return View();
        }

        public ActionResult NewAccountsBind(int page, int pageSize)
        {
            int PageSize = pageSize;
            int iPageNo = page;
            BALCompany ObjBALCompany = new BALCompany();
            var NewAccountsCollection = ObjBALCompany.GetNewAccounts(iPageNo, PageSize, true, string.Empty,"CompanyName","ASC");
            //No records are found in collection.
            if (NewAccountsCollection != null && NewAccountsCollection.Count>0)
            {
                return Json(new
                {
                    Products = NewAccountsCollection,
                    TotalCount = NewAccountsCollection.ElementAt(0).TotalRec
                },
                                            JsonRequestBehavior.AllowGet);  
            }
            return Json(new
            {
                Products =string.Empty,
                TotalCount =0
            },
                                          JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveColapseStatus(string SectionId, string Status)
        {
            
            MembershipUser objMembershipUser = Membership.GetUser();
            Guid pkuserid = Guid.Parse(objMembershipUser.ProviderUserKey.ToString());
            var UserName = objMembershipUser.UserName;
            if (Roles.IsUserInRole(UserName, "SystemAdmin"))//Roles.IsUserInRole(UserName, "Admin") || 
            {

            }
            else
            {
                BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
                ObjBALCompanyUsers.UpdateDashBoardStaus(pkuserid, SectionId, Convert.ToBoolean(Status));
 
            }
            return Json(new
            {},JsonRequestBehavior.AllowGet);
        }

        //public ActionResult SaveColapseStatus(List<HttpPostedFileBase> Document)
        //{
        //    string strResult = string.Empty;

        //    if (Document.Count() > 0)
        //    {
        //        for (int i = 0; i < Document.Count(); i++)
        //        {
        //        }
        //    }
        //    return RedirectToAction("AdminSearch");
        //}

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SaveDocumentDetails(HttpPostedFileBase Document, string hdCompanyId, string txtDocumentTitle)
        {
            string strResult = string.Empty;
            if (hdCompanyId != null)
            {
                if (Document != null)
                {
                    if (Document != null && Document.ContentLength > 0)
                    {
                        if (Document.FileName != string.Empty)
                        {
                            if (CheckExtensionsForUserAgreement(Document))
                            {
                                string sExt = Path.GetExtension(Document.FileName).ToLower();
                                string sFileName = DateTime.Now.ToFileTime().ToString() + sExt;
                                //string sFilePath = "";
                                //sFilePath = HttpContext.Server.MapPath("~/Resources/Upload/CompanyPDF/");
                                if (System.IO.File.Exists(HttpContext.Server.MapPath(@"~\Resources\Upload\CompanyPDF\") + sFileName))
                                {
                                    try
                                    {
                                        System.IO.File.Delete(HttpContext.Server.MapPath(@"~\Resources\Upload\CompanyPDF\") + sFileName);
                                    }
                                    catch { }
                                }
                                Document.SaveAs(HttpContext.Server.MapPath(@"~\Resources\Upload\CompanyPDF\") + sFileName);

                                //string fPath = Server.MapPath("~/Resources/Upload/CompanyPDF/") + sFileName;

                                ObjBALCompany = new BALCompany();
                                int accno = 0;
                                accno = ObjBALCompany.GetMaxAccountNumber();

                                tblCompany ObjtblCompany = new tblCompany();
                                ObjtblCompany.pkCompanyId = Int32.Parse(hdCompanyId);
                                ObjtblCompany.CompanyAccountNumber = accno.ToString();
                                ObjtblCompany.LeadStatus = 5; // 5 for client
                                tblCompanyDocument tbl = new tblCompanyDocument();
                                //tbl.pkCompanyDocumentId = Guid.NewGuid();
                                tbl.fkCompanyId = Int32.Parse(hdCompanyId);
                                tbl.DocumentTitle = txtDocumentTitle.Trim();
                                tbl.DocumentName = sFileName;
                                tbl.CreatedDate = DateTime.Now;
                                tbl.IsUserAgreement = true;
                                int result = ObjBALCompany.InsertNewAccount(ObjtblCompany, tbl);
                                //MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
                                Guid UserId = Utility.GetUID(User.Identity.Name);
                                int _leadStatus = 5;
                                if (result == 1)
                                {
                                    strResult = "<span class='successmsg'>New Account added successfully.</span>";
                                    AddStatusToCallLog(Convert.ToInt32(hdCompanyId), UserId, "##" + _leadStatus + "##", DateTime.Now);
                                }
                                else
                                {
                                    strResult = "<span class='errormsg'>Some error occured while adding New Account.</span>";                                
                                }
                            }
                            else
                            {
                                strResult = "<span class='errormsg'>Only .pdf files are allowed to upload.</span>";                               
                            }
                        }
                    }
                }
            }
            TempData["message"] = strResult;
            TempData["NewAccounts"] = "NewAccounts";
            return RedirectToAction("AdminSearch");
        }
        public bool CheckExtensionsForUserAgreement(HttpPostedFileBase Document)
        {
            string Extension = string.Empty;

            if (Document.FileName != string.Empty)
            {
                Extension = Path.GetExtension(Document.FileName).ToLower();
                if (Document.ContentLength > 0)
                {
                    if (Extension == ".pdf")
                    { }
                    else
                    {
                        return false;
                    }
                }
            }
            return true;
        }
       
    }
}
