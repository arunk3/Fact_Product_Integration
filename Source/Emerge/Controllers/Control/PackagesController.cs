﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /PackagesList/
        #region Packages List


        public ActionResult PackagesList()
        {
            PackagesModel ObjModel = new PackagesModel();


            return View(ObjModel);
        }
        BALProductPackages ObjBALProductPackages = null;
        [HttpPost]
        public ActionResult GetPackages()
        {
            ObjBALProductPackages = new BALProductPackages();
            //List<tblProductPackage> ObjData = new List<tblProductPackage>();

            var ObjData = ObjBALProductPackages.GetProductPackages(Utility.SiteApplicationId, 0).Select((p, index) => new PackagesModel
            {
                RowNumber = index + 1,
                pkPackageId = p.pkPackageId,
                IsPublic = p.IsPublic,
                IsEnabled = p.IsEnabled,
                IsDefault = p.IsDefault,
                PackageName = p.PackageName,
                PackageDisplayName = p.PackageDisplayName,
                PackageCode = p.PackageCode,
                PackageDescription = p.PackageDescription


            }).ToList();

            return Json(ObjData);
        }
        public ActionResult DeletePackageById(int PackageId)
        {
            string strSuccess = string.Empty;
            try
            {
                ObjBALProductPackages = new BALProductPackages();

                int Success = 0;
                List<int> ObjPackageIds = new List<int>();
                ObjPackageIds.Add(PackageId);
                Success = ObjBALProductPackages.ChangePackageDeleteStatus(ObjPackageIds, true);
                try
                {

                    if (Success == 1)
                    {
                        strSuccess = "<span class='successmsg'>Package successfully deleted</span>";
                    }
                    else
                    {
                        strSuccess = "<span class='errormsg'>Error occured on deleting the package</span>";
                    }
                }
                catch (Exception)
                {
                }

            }

            catch
            {
            }

            return Json(new { Message = strSuccess });
        }

        public ActionResult ChangePackageStatus(int packageId, string Packagetype, bool Value)
        {
            string strSuccess = string.Empty;
            BALProductPackages ObjBALProductPackages = new BALProductPackages();
            try
            {
                if (ObjBALProductPackages.ChangePackageStatus(packageId, Packagetype, Value) == true)
                {
                    strSuccess = "<span class='successmsg'>Operation Completed Successfully</span>";

                }
                else
                {
                    strSuccess = "<span class='errormsg'Some Error Occured</span>";

                }

            }
            catch (Exception)
            {

            }
            finally
            {
                ObjBALProductPackages = null;
            }
            return Json(new { Message = strSuccess });
        }

        public ActionResult PackageDeleteStatus(string PackagesIds)
        {
            string strSuccess = string.Empty;
            ObjBALProductPackages = new BALProductPackages();


            int DeleteUnderScore = PackagesIds.LastIndexOf('_');

            string strDeleteBracket = PackagesIds.Substring(0, DeleteUnderScore != -1 ? DeleteUnderScore : 0).Trim().ToLower();

            string[] str = strDeleteBracket.Split('_');

            List<int> ObjPackages = new List<int>();

            try
            {
                if (str.Count() > 0)
                {
                    for (int i = 0; i < str.Count(); i++)
                    {
                        ObjPackages.Add(int.Parse(str[i].ToString()));
                    }
                }


                int Success = ObjBALProductPackages.ChangePackageDeleteStatus(ObjPackages, true);
                if (Success == 1)
                {
                    strSuccess = "<span class='successmsg'>Package(s) successfully deleted</span>";
                }
                else
                {
                    strSuccess = "<span class='errormsg'>Error occured on deleting the package</span>";
                }

            }
            finally
            {
                ObjBALProductPackages = null;
            }

            return Json(new { Message = strSuccess });
        }

        #endregion Packages List

        #region Manage Packages

        int iPackageId = 0;

        public ActionResult ManagePackages()
        {
            PackagesModel ObjModel = new PackagesModel();

            ObjModel.QueryStringPackageId = "-1";

            if (!string.IsNullOrEmpty(Request.QueryString["_a"]))         //  _a Contains PackageId
            {
                int.TryParse(Request.QueryString["_a"], out iPackageId);
                if (iPackageId > 0)
                {
                    ObjModel.QueryStringPackageId = iPackageId.ToString();
                    ObjModel = LoadPackageData(int.Parse(ObjModel.QueryStringPackageId), ObjModel);
                }
                else
                {
                    ObjModel.CustomMessages = "<span class='infomessage'>Please do not try with invalid querystring values</span>";

                }
            }

            ObjModel.lstProductsByPackageId = BindProducts(int.Parse(ObjModel.QueryStringPackageId));

            return View(ObjModel);
        }
        public string GetQueryStringValue()
        {
            PackagesModel ObjModel = new PackagesModel();
            ObjModel.QueryStringPackageId = "-1";

            if (!string.IsNullOrEmpty(Request.QueryString["_a"]))         //  _a Contains PackageId
            {
                int.TryParse(Request.QueryString["_a"], out iPackageId);
                if (iPackageId > 0)
                {
                    ObjModel.QueryStringPackageId = iPackageId.ToString();

                }
                else
                {

                }
            }
            return iPackageId.ToString();

        }
        /// <summary>
        /// This method is used to load package data
        /// </summary>
        /// <param name="PackageId"></param>
        public PackagesModel LoadPackageData(int PackageId, PackagesModel ObjModel)
        {
            string strSuccess = string.Empty;

            BALProductPackages ObjBALProductPackages = new BALProductPackages();


            try
            {
                List<tblProductPackage> lstProductPackage = ObjBALProductPackages.GetProductPackageById(PackageId);

                if (lstProductPackage.Count > 0)
                {
                    ObjModel.IsEnableCompanyPackage = lstProductPackage.FirstOrDefault().IsEnableCompanyPackage;
                    ObjModel.IsEnabled = lstProductPackage.FirstOrDefault().IsEnabled;
                    ObjModel.IsDefault = lstProductPackage.FirstOrDefault().IsDefault;
                    ObjModel.IsPublic = lstProductPackage.FirstOrDefault().IsPublic;
                    ObjModel.PackageCode = lstProductPackage.FirstOrDefault().PackageCode;
                    ObjModel.PackageName = lstProductPackage.FirstOrDefault().PackageName;
                    ObjModel.PackagePrice = lstProductPackage.FirstOrDefault().PackagePrice;
                    ObjModel.PackageDisplayName = lstProductPackage.FirstOrDefault().PackageDisplayName;
                    ObjModel.PackageDescription = lstProductPackage.FirstOrDefault().PackageDescription;
                    ObjModel.QueryStringPackageId = PackageId.ToString();
                    ObjModel.IsPackageNamedisplay = lstProductPackage.FirstOrDefault().IsPackageNameDisplay;// add for package (shalesh) int-135
                    ObjModel.is7YearFilterpackage = lstProductPackage.FirstOrDefault().Is7YearFilter;//INT-174
                    ObjModel.is10YearFilterpackage = lstProductPackage.FirstOrDefault().Is10YearFilter;//INT-174
                }
                else
                {
                    strSuccess = "<span class='infomessage'>Record not found.</span>";
                }
            }
            catch
            {
                strSuccess = "<span class='errormsg'>Some problem while fetching data.</span>";

            }
            finally
            {
                ObjBALProductPackages = null;
            }
            return ObjModel;
        }
        public List<Proc_Get_AllEmergeProductsInPackagesResult> BindProducts(int PackageId)
        {

            List<Proc_Get_AllEmergeProductsInPackagesResult> ObjData = new List<Proc_Get_AllEmergeProductsInPackagesResult>();
            //  lblMessageBox.Visible = true;

            BALProductPackages ObjBALProductPackages = new BALProductPackages();

            try
            {
                ObjData = ObjBALProductPackages.GetAllEmergeProductsInPackages(PackageId, Utility.SiteApplicationId);
            }
            catch
            {
                //lblMessageBox.CssClass = "errormsg";
                //lblMessageBox.Text = "Some problem while fetching data.";
            }
            finally
            {
                ObjBALProductPackages = null;
            }
            return ObjData;
        }


        string PackageInclude = string.Empty;
        List<PackageReports> lstPackageReports = new List<PackageReports>();
        public ActionResult AddUpdateUniversalPackageDetails(PackagesModel ObjModel, FormCollection frm)
        {
            string strResult = "";
            string strCommon = string.Empty;

            for (int iRow = 0; iRow < frm.Keys.Count; iRow++)
            {
                PackageReports ObjPackageReports = new PackageReports();

                if (frm.Keys[iRow].Contains("chkReport_"))
                {
                    string commonId = frm.GetKey(iRow).Split('_')[1];

                    if ((frm["chkReport_" + commonId]) != null && (frm["chkReport_" + commonId]).ToLower() != "no")
                    {
                        if (frm.GetValue("hdProductCode_" + commonId) != null && frm.GetValue("hd_ProductApplicationId_" + commonId) != null && frm.GetValue("hd_PackageReportId_" + commonId) != null)
                        {

                            ObjPackageReports.ProductCode = frm["hdProductCode_" + commonId].ToString();
                            ObjPackageReports.fkProductApplicationID = Int32.Parse(frm["hd_ProductApplicationId_" + commonId].ToString());
                            ObjPackageReports.PackageReportId = Int32.Parse(frm["hd_PackageReportId_" + commonId].ToString());
                            ObjPackageReports.IsChecked = true;

                            PackageInclude += frm["hdProductCode_" + commonId];
                            PackageInclude += ", ";

                            strCommon = commonId;
                            lstPackageReports.Add(ObjPackageReports);
                        }
                    }
                }
                if ((!frm.Keys[iRow].Contains("chkReport_")) && (frm.Keys[iRow].Contains("hdProductCode_")))
                {

                    string commonId = frm.GetKey(iRow).Split('_')[1];

                    if (frm.GetValue("hdProductCode_" + commonId) != null && frm.GetValue("hd_ProductApplicationId_" + commonId) != null && frm.GetValue("hd_PackageReportId_" + commonId) != null && commonId != strCommon)
                    {

                        ObjPackageReports.ProductCode = frm["hdProductCode_" + commonId].ToString();
                        ObjPackageReports.fkProductApplicationID = Int32.Parse(frm["hd_ProductApplicationId_" + commonId].ToString());
                        ObjPackageReports.PackageReportId = Int32.Parse(frm["hd_PackageReportId_" + commonId].ToString());
                        ObjPackageReports.IsChecked = false;

                        lstPackageReports.Add(ObjPackageReports);
                    }


                }


            }

            string _PackageId = string.Empty;
            _PackageId = ObjModel.QueryStringPackageId != null ? ObjModel.QueryStringPackageId : GetQueryStringValue();   // Get QueryString

            if (_PackageId == "-1" || _PackageId == null)                           /* We are in insert mode */
            {
                strResult = AddPackages(ObjModel);
            }
            else /* We are in update mode  */
            {
                if (_PackageId != null)
                {
                    strResult = UpdatePackages(int.Parse(_PackageId), ObjModel);
                }
            }
            return Json(new { Message = strResult });

        }
        BALCompany ObjBALCompany = null;
        /// <summary>
        /// This method is used to add packages
        /// </summary>
        private string AddPackages(PackagesModel ObjModel)
        {
            string strSuccess = string.Empty;


            BALProductPackages ObjBALProductPackages = new BALProductPackages();
            int iResult = -1;
            try
            {
                #region Insert data in main package table

                tblProductPackage ObjProductPackage = new tblProductPackage();

                ObjProductPackage.fkApplicationId = Utility.SiteApplicationId;


                ObjProductPackage.fkCompanyId = 0;
                ObjProductPackage.PackageName = ObjModel.PackageName.Trim();
                ObjProductPackage.PackageCode = ObjModel.PackageCode.Trim();
                ObjProductPackage.PackageDisplayName = ObjModel.PackageDisplayName.Trim();
                ObjProductPackage.PackageDescription = ObjModel.PackageDescription.Trim();

                ObjProductPackage.IsEnabled = ObjModel.IsEnabled;
                ObjProductPackage.IsDefault = ObjModel.IsDefault;
                ObjProductPackage.IsPublic = ObjModel.IsPublic;
                ObjProductPackage.CreatedDate = DateTime.Now;
                ObjProductPackage.PackagePrice = ObjModel.PackagePrice;
                #endregion

                #region Insert data in package reports

                List<tblPackageReport> ObjCollectionPackageReports = new List<tblPackageReport>();

                for (int iRow = 0; iRow < lstPackageReports.Count; iRow++)
                {
                    if (lstPackageReports.ElementAt(iRow).IsChecked == true)
                    {
                        tblPackageReport ObjData = new tblPackageReport();
                        //ObjData.pkPackageReportId = Guid.NewGuid();
                        ObjData.fkPackageId = -1;
                        ObjData.fkProductApplicationId = lstPackageReports.ElementAt(iRow).fkProductApplicationID;
                        ObjCollectionPackageReports.Add(ObjData);
                    }
                }

                #endregion


                #region Method Result

                int oPackageId;
                if (PackageInclude.Length > 2 && PackageInclude.Contains(","))
                {
                    PackageInclude = PackageInclude.Substring(0, PackageInclude.Length - 2);
                }
                ObjProductPackage.include = PackageInclude;
                iResult = ObjBALProductPackages.AddProductPackages(ObjProductPackage, ObjCollectionPackageReports, out oPackageId);

                if (iResult == 1)
                {
                    AddUniversalPackagesForAllCompanies(oPackageId, ObjModel.PackagePrice);

                    strSuccess = "<span class='successmsg'>Package successfully created.</span>";


                }
                else if (iResult == -1)
                {
                    strSuccess = "<span class='errormsg'>Some problem while inserting data.</span>";

                    return strSuccess;
                }
                else if (iResult == -2)
                {
                    strSuccess = "<span class='infomessage'>Duplicate package name not allowed here.</span>";

                    return strSuccess;
                }
                else if (iResult == -3)
                {
                    strSuccess = "<span class='infomessage'>Duplicate package code not allowed here.</span>";

                    return strSuccess;
                }
                else if (iResult == -4)
                {
                    strSuccess = "<span class='infomessage'>Duplicate package display name not allowed here.</span>";

                    return strSuccess;
                }
                // ClearFields();

                #endregion
            }
            catch
            {
                strSuccess = "<span class='errormsg'>Some problem while inserting data.</span>";
            }
            finally
            {
                ObjBALProductPackages = null;
            }
            return strSuccess;
        }


        /// <summary>
        /// This method is used to update packages
        /// </summary>
        /// <param name="PackageId"></param>
        private string UpdatePackages(int PackageId, PackagesModel ObjModel)
        {
            string strSuccess = string.Empty;


            BALProductPackages ObjBALProductPackages = new BALProductPackages();
            int iResult = -1;

            try
            {
                #region Update data in main package table

                tblProductPackage ObjProductPackage = new tblProductPackage();

                ObjProductPackage.pkPackageId = PackageId;


                ObjProductPackage.fkCompanyId = 0;
                ObjProductPackage.fkApplicationId = Utility.SiteApplicationId;
                ObjProductPackage.PackageName = ObjModel.PackageName.Trim();
                ObjProductPackage.PackageCode = ObjModel.PackageCode.Trim();
                ObjProductPackage.PackagePrice = ObjModel.PackagePrice;
                ObjProductPackage.PackageDisplayName = ObjModel.PackageDisplayName.Trim();
                ObjProductPackage.PackageDescription = ObjModel.PackageDescription.Trim();
                ObjProductPackage.IsEnabled = ObjModel.IsEnabled;
                ObjProductPackage.IsDefault = ObjModel.IsDefault;
                ObjProductPackage.IsPublic = ObjModel.IsPublic;
                ObjProductPackage.LastModifiedDate = DateTime.Now;


                #endregion

                #region Update Product List in Packages

                #region Variables

                //List<Proc_Get_AllEmergeProductsInPackagesResult> ObjProductsCollectionDB = ObjBALProductPackages.GetAllEmergeProductsInPackages(PackageId, Utility.SiteApplicationId); /* Pick data for products */

               // var ObjCollSavedProducts = ObjProductsCollectionDB.Where(db => db.PackageReportId != 0); /* Pick collection of all saved products for package */

                //int[] arrSavedProducts = ObjCollSavedProducts.Select(db => db.PackageReportId).ToArray(); /* Store saved reports in packages array  */

                #endregion



                List<Guid> LstDeletingProducts = new List<Guid>();


                #endregion

                #region New Reports Add for package

                List<tblPackageReport> ObjCollectionPackageReports = new List<tblPackageReport>();



                for (int iLvl2 = 0; iLvl2 < lstPackageReports.Count; iLvl2++)
                {
                    if (lstPackageReports.ElementAt(iLvl2).IsChecked == true)
                    {
                        tblPackageReport ObjPackageReport = new tblPackageReport();


                        //ObjPackageReport.pkPackageReportId = Guid.NewGuid();
                        ObjPackageReport.fkPackageId = PackageId;
                        ObjPackageReport.fkProductApplicationId = lstPackageReports.ElementAt(iLvl2).fkProductApplicationID;

                        ObjCollectionPackageReports.Add(ObjPackageReport);

                    }
                }

                #endregion

                #region Call DB
                if (PackageInclude.Length > 2 && PackageInclude.Contains(","))
                {
                    PackageInclude = PackageInclude.Substring(0, PackageInclude.Length - 2);
                }
                ObjProductPackage.include = PackageInclude;
                iResult = ObjBALProductPackages.UpdateProductPackages(LstDeletingProducts, ObjProductPackage, ObjCollectionPackageReports);

                #endregion

                #region Method Result

                if (iResult == 1)
                {
                    //  LoadPackageData(PackageId);
                    //  BindProducts(PackageId);

                    strSuccess = "<span class='successmsg'>Package successfully updated.</span>";
                }
                else if (iResult == -1)
                {
                    strSuccess = "<span class='errormsg'>Some problem while updating data.</span>";

                    return strSuccess;
                }
                else if (iResult == -2)
                {
                    strSuccess = "<span class='infomessage'>Duplicate package name not allowed here.</span>";

                    return strSuccess;
                }
                else if (iResult == -3)
                {
                    strSuccess = "<span class='infomessage'>Duplicate package code not allowed here.</span>";

                    return strSuccess;
                }
                else if (iResult == -4)
                {
                    strSuccess = "<span class='infomessage'>Duplicate package display name not allowed here.</span>";

                    return strSuccess;
                }

                #endregion


            }
            catch
            {
                strSuccess = "<span class='errormsg'>Some problem while updating data.</span>";

            }
            finally
            {
                ObjBALProductPackages = null;
            }
            return strSuccess;
        }

        private int AddUniversalPackagesForAllCompanies(int PackageId, decimal PackagePrice)
        {
            BALProductPackages ObjBALProductPackages = new BALProductPackages();
            ObjBALCompany = new BALCompany();
            try
            {

                List<tblCompany_PackagePrice> lstCompany_PackagePrice = new List<tblCompany_PackagePrice>();
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {

                    List<tblCompany> lstCompany = DX.tblCompanies.ToList<tblCompany>();
                    for (int i = 0; i < lstCompany.Count; i++)
                    {
                        tblCompany_PackagePrice ObjCompany_PackagePrice = new tblCompany_PackagePrice();

                        //ObjCompany_PackagePrice.pkCompanyPackagePriceId = Guid.NewGuid();
                        ObjCompany_PackagePrice.fkCompanyId = lstCompany.ElementAt(i).pkCompanyId;
                        ObjCompany_PackagePrice.fkPackageId = PackageId;
                        ObjCompany_PackagePrice.PackagePrice = PackagePrice;
                        ObjCompany_PackagePrice.IsEnableUniversalPackage = true;

                        lstCompany_PackagePrice.Add(ObjCompany_PackagePrice);
                    }


                }
                int success = ObjBALProductPackages.AddUniversalPackagesForAllCompanies(lstCompany_PackagePrice);
                if (success == 1)
                {
                    AddUniversalPackagesForAllCompanyLocations(PackageId);
                }
                return success;
            }
            finally
            {
                ObjBALProductPackages = null;
            }
        }

        private int AddUniversalPackagesForAllCompanyLocations(int PackageId)
        {
            BALProductPackages ObjBALProductPackages = new BALProductPackages();
            ObjBALCompany = new BALCompany();
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    List<Companies> ListCompanyId = DX.tblCompany_PackagePrices.Select(x => new Companies { CompanyId = (int)x.fkCompanyId }).Distinct().ToList<Companies>();

                    List<tblPackageAccess> lstPackageAccess = new List<tblPackageAccess>();

                    for (int i = 0; i < ListCompanyId.Count; i++)
                    {
                        List<tblLocation> lstLocation = DX.tblLocations.Where(d => d.fkCompanyId == ListCompanyId.ElementAt(i).CompanyId).ToList<tblLocation>();

                        for (int loc = 0; loc < lstLocation.Count; loc++)
                        {
                            tblPackageAccess ObjPackageAccess = new tblPackageAccess();

                            //ObjPackageAccess.pkPackageAccessId = Guid.NewGuid();
                            ObjPackageAccess.fkPackageId = PackageId;
                            ObjPackageAccess.fkLocationId = lstLocation.ElementAt(loc).pkLocationId;

                            lstPackageAccess.Add(ObjPackageAccess);
                        }
                    }
                    int success = ObjBALProductPackages.AddUniversalPackagesForAllCompanyLocation(lstPackageAccess);

                    return success;
                }
            }
            finally
            {
                ObjBALProductPackages = null;
            }
        }
        public class PackageReports
        {
            public string ProductCode { get; set; }
            public int fkProductApplicationID { get; set; }
            public int PackageReportId { get; set; }
            public string PackageInclude { get; set; }
            public bool IsChecked { get; set; }
        }
        public class Companies
        {
            public int CompanyId { get; set; }
        }
        #endregion Manage Packages
    }
}
