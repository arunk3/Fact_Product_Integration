﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using System.Web.Security;
using System.IO;
using System.Web.UI.WebControls;


namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /ReviewData/
        public PartialViewResult PartialReviewData()
        {
            AdminSearchModel ObjAdminSearchModel = new AdminSearchModel();
            BALOrders ObjBALOrders = new BALOrders();
           // BALEmergeReview ObjReview = new BALEmergeReview();
            if (Request.QueryString["_o"] != null)
            {
                int pkOrderId = Convert.ToInt32(Request.QueryString["_o"].ToString());
                ObjAdminSearchModel.CollGetConsentInforByOrderId = ObjBALOrders.GetConsentPendingFormsByOrderId(pkOrderId).ToList();
            }
             
            
            //List<proc_GetConsentInforByOrderIdResult> lst = ObjBALOrders.GetConsentPendingFormsByOrderId(OrderId).ToList();
         

            return PartialView(ObjAdminSearchModel);


        }

        //public PartialViewResult PartialReviewData()
        //{
        //    return View();
        //}

        public ActionResult SaveAdminConsentReview(FormCollection Form)
        {
            string[] arr1 = new string[] { };
            string StrResult = "";
            List<tblOrderDetail> ObjCollOrderDetail = new List<tblOrderDetail>();
            tblOrderDetail ObjOrderDetail = new tblOrderDetail();
            var ConsentFilename = Request.Form["ConsentFilename"];
           var pkOrderDetailId = Request.Form["pkOrderDetailId"];
            arr1 = ConsentFilename.Split(',');
            var arr4 = pkOrderDetailId.Split(',');
            int J = 0;
            for (int i = 0; i < arr1.Length; i++)
            {
               
                ObjOrderDetail = new tblOrderDetail();
                if (arr1[i] == "")
                {

                    if (Request.Files != null)
                    {
                        HttpPostedFileBase hpf = Request.Files[J];
                        if (hpf.ContentLength > 0)
                        {

                            string sExt = Path.GetExtension(hpf.FileName).ToLower();
                            string sFileName = DateTime.Now.ToFileTime().ToString() + sExt;

                           // string sFilePath = "";
                           // sFilePath = HttpContext.Server.MapPath("~/Resources/Upload/ConsentForm/");
                            if (System.IO.File.Exists(HttpContext.Server.MapPath(@"~\Resources\Upload\ConsentForm\") + sFileName))
                            {
                                try
                                {
                                    System.IO.File.Delete(HttpContext.Server.MapPath(@"~\Resources\Upload\ConsentForm\") + sFileName);
                                }
                                catch { }
                            }


                            hpf.SaveAs(HttpContext.Server.MapPath(@"~\Resources\Upload\ConsentForm\") + sFileName);
                           // string fPath = Server.MapPath("~/Resources/Upload/ConsentForm/") + sFileName;

                            ObjOrderDetail.pkOrderDetailId = Convert.ToInt32(arr4[i]);
                            ObjOrderDetail.ConsentFileName = sFileName;
                            ObjOrderDetail.IsConsentEmailorFax = false;
                            ObjOrderDetail.IsConsentRequired = true;
                            ObjCollOrderDetail.Add(ObjOrderDetail);
                            J = J + 1;
                        }





                    }
                    else
                    {
                        ObjOrderDetail.pkOrderDetailId = Convert.ToInt32(pkOrderDetailId);
                        ObjOrderDetail.ConsentFileName = string.Empty;
                        ObjOrderDetail.IsConsentEmailorFax = true;
                        ObjOrderDetail.IsConsentRequired = true;
                        ObjCollOrderDetail.Add(ObjOrderDetail);
                    }
                }
                else
                {

                }

            }

            BALOrders ObjBalOrders = new BALOrders();
            int iResult = ObjBalOrders.UpdateConsentInfo(ObjCollOrderDetail);
            if (iResult == 1)
            {
                StrResult = "<span class='successmsg'>Information Updated Successfully</span>";


            }
            else
            {
                StrResult = "<span class='errormsg'>Problem while saving. Please try again</span>";
            }

            TempData["messages"] = StrResult;
            TempData["AdminConsent"] = "AdminConsent";
            return RedirectToAction("AdminSearch");



        }



        public FilePathResult Download_FeaturedFile(string FPathTitle)
        {

            string FileName = FPathTitle;
            string Title = "ConsentForm";
            string FilePath = Request.PhysicalApplicationPath + "Resources/Upload/ConsentForm/" + FileName;
            string fileext = System.IO.Path.GetExtension(FileName);
            string CType = "application/" + fileext.Replace(".", "");
            //string fileName = Path.GetFileName(FileName);

            string strFileTitle = Title + fileext;

            return File(FilePath, CType, strFileTitle);


        }



















    }
}
