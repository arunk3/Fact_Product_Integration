﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Emerge.Models;
using System.IO;
using Emerge.Common;
using System.Web.UI;
using Emerge.Data;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /ReviewStatusLog/

        public ActionResult ReviewStatusLog()
        {
            MarketingContentModel ObjModel = new MarketingContentModel();

            BALContentManagement ObjBALContentManagement = new BALContentManagement();
            var DBContent = ObjBALContentManagement.GetReviewCustomMsg();
            ObjModel.objtblCustomMsg = DBContent;

            if (Request.QueryString["Status"] != null)
            {
                int status = Convert.ToInt32(Request.QueryString["Status"]);
                if (status == 1)
                {
                    ObjModel.Message = "<span class='successmsg'>Review Log updated successfully</span>";
                }
                else if (status == 2)
                {
                    ObjModel.Message = "<span class='successmsg'>Review Log added successfully</span>";
                }
                else if (status == 3)
                {
                    ObjModel.Message = "<span class='successmsg'>Review Log deleted successfully</span>";
                }
                else if (status == 4)
                {
                    ObjModel.Message = "<span class='successmsg'>Settings updated successfully.</span>";
                }
                else if (status == -1)
                {
                    ObjModel.Message = "<span class='errormsg'>Title name already exist</span>";
                }
                else
                {
                    ObjModel.Message = "<span class='errormsg'>error is occured while updating content</span>";
                }
            }




            return View(ObjModel);
        }

        [HttpPost]
        public ActionResult UpdateLogMessage(FormCollection ObjCollection)
        {
            BALContentManagement ObjBALContentManagement = new BALContentManagement();
            string strSuccess = string.Empty;
            string LogTitle = Convert.ToString(ObjCollection.GetValue("LogTitle").AttemptedValue);
            string LogDescrption = Convert.ToString(ObjCollection.GetValue("LogDescrption").AttemptedValue);
            int pkLogId = Convert.ToInt32(ObjCollection.GetValue("pkLogId").AttemptedValue); // Convert.ToInt32(ObjCollection.GetValue("pkLogId").AttemptedValue);

            tblCustomReviewMessage objtblCustomReviewMessage = new tblCustomReviewMessage();
            objtblCustomReviewMessage.LogTitle = LogTitle;
            objtblCustomReviewMessage.Descrption = LogDescrption;
            objtblCustomReviewMessage.pkLogId = pkLogId;
            int Success = ObjBALContentManagement.UpdateCustomMessages(objtblCustomReviewMessage);


            if (Success == 1)
            {
                strSuccess = "1";// "<span class='successmsg'>Feature Updated Successfully</span>";
            }
            else if (Success == -1)
            {
                strSuccess = "-1";// "<span class='errormsg'>Title name already exist</span>";
            }
            else
            {
                strSuccess = "-2"; //"<span class='errormsg'>error is occured while updating content</span>";
            }


            return RedirectToAction("ReviewStatusLog", new { Status = strSuccess });
        }



        public ActionResult DeleteReviewStatusLog(int pkLogId)
        {
            int Success = 0;

            BALContentManagement ObjBALContentManagement = new BALContentManagement();
            try
            {
                Success = ObjBALContentManagement.DeleteReviewLog(pkLogId);
                if (Success == 1)
                {
                    Success = 3;
                }
                else
                {
                    Success = -2;
                }
            }
            catch (Exception)
            {

            }
            return Json(Success);

        }

        public ActionResult AddReviewLogMsg(FormCollection ObjCollection)
        {
            string strSuccess = string.Empty;
            try
            {

                string LogTitle = Convert.ToString(ObjCollection.GetValue("txtCustomMessagesTitle").AttemptedValue);
                string LogDescrption = Convert.ToString(ObjCollection.GetValue("txtDescrption").AttemptedValue);
                // Convert.ToInt32(ObjCollection.GetValue("pkLogId").AttemptedValue);

                tblCustomReviewMessage objtblCustomReviewMessage = new tblCustomReviewMessage();
                objtblCustomReviewMessage.LogTitle = LogTitle;
                objtblCustomReviewMessage.Descrption = LogDescrption;
                // objtblCustomReviewMessage.pkLogId = pkLogId;



                BALContentManagement ObjBALContentManagement = new BALContentManagement();
                int Success = 0;
                Success = ObjBALContentManagement.InsertReviewLogMsgs(objtblCustomReviewMessage);
                if (Success > 0)
                {
                    strSuccess = "2";// "<span class='successmsg'>Review Log Updated Successfully</span>";
                }
                else if (Success == -1)
                {
                    strSuccess = "-1";// "<span class='errormsg'>Title name already exist</span>";
                }
                else
                {
                    strSuccess = "-2"; //"<span class='errormsg'>error is occured while updating content</span>";
                }

            }


            catch (Exception)
            {
            }
            return RedirectToAction("ReviewStatusLog", new { Status = strSuccess });

        }



    }
}
