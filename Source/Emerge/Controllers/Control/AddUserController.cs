﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using System.Web.Security;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /AddUser/

        public ActionResult AddUser()
        {
           
            AddCompanyUser ObjAddCompanyUser = new AddCompanyUser();
            if (Request.QueryString["PkCompanyUserId"] != null)
            {

                ViewBag.data = Request.QueryString["PkCompanyUserId"];
                int PkCompanyUserId = int.Parse(Request.QueryString["PkCompanyUserId"]);
                BALCompanyUsers Obj = new BALCompanyUsers();
                var CompanyUser = Obj.SelectCompanyUserByCompanyUserId(PkCompanyUserId);
                ObjAddCompanyUser.FirstName = CompanyUser.FirstName;
                ObjAddCompanyUser.LastName = CompanyUser.LastName;
                ObjAddCompanyUser.Address1 = CompanyUser.UserAddressLine1;
                ObjAddCompanyUser.Address2 = CompanyUser.UserAddressLine2;
                ObjAddCompanyUser.ZipCode = CompanyUser.ZipCode;
                ObjAddCompanyUser.PhoneNo = CompanyUser.PhoneNo;
                ObjAddCompanyUser.IsEnabled = CompanyUser.IsEnabled;
                ObjAddCompanyUser.IsSignedFcra = bool.Parse(CompanyUser.IsSignedFcra.ToString());


                // #19
                ObjAddCompanyUser.pkLocationId = CompanyUser.fkLocationId.GetValueOrDefault();



                ObjAddCompanyUser.RoleId = Guid.Parse(CompanyUser.RoleId.ToString());
                int CompanyId = GetCompanyIdByLocationId(CompanyUser.fkLocationId.GetValueOrDefault());
                ObjAddCompanyUser.pkStateId = Convert.ToInt32(CompanyUser.fkStateId);
                ObjAddCompanyUser.pkCountyId = Convert.ToInt32(CompanyUser.fkCountyId);
                ObjAddCompanyUser.pkCompanyId = CompanyId;
                ViewBag.data = CompanyId;
                MembershipUser ObjMembershipUser = Membership.GetUser(CompanyUser.fkUserId);
                ObjAddCompanyUser.email = ObjMembershipUser.UserName;
                ObjAddCompanyUser.LastLogin = ObjMembershipUser.LastLoginDate.ToString();
                ObjAddCompanyUser.Password = ObjMembershipUser.GetPassword();
            }

            else
            {
                ViewBag.data = "7dc87d77-6bb5-4bb3-9837-fa861f36f281";
            
            
            
            }


            return View(ObjAddCompanyUser);
            
          
        }


        [HttpPost]
        public ActionResult GetState(IEnumerable<AddCompanyUser> User)
        {
            BALGeneral Obj = new BALGeneral();
            IEnumerable<AddCompanyUser> State = Obj.GetStates().Select(p => new AddCompanyUser
            {
                pkStateId = p.pkStateId,
                StateName = p.StateName,

            }).ToList();
            return Json(State);

        }

        [HttpPost]
        public ActionResult GetCounty(IEnumerable<AddCompanyUser> User, int StateId)
        {

            BALGeneral ObjBALGeneral = new BALGeneral();
            IEnumerable<AddCompanyUser> County = ObjBALGeneral.GetStatesCounties(StateId).Select(p => new AddCompanyUser
            {

                pkCountyId = Convert.ToInt32(p.pkCountyId),
                CountyName = p.CountyName,

            }).ToList();

            return Json(County);
        }




        public ActionResult GetCompany(IEnumerable<AddCompanyUser> User)
        {

            BALCompany ObjBALCompany = new BALCompany();
            var Companies = ObjBALCompany.GetAllCompany(Utility.SiteApplicationId, true, false).Select(p => new AddCompanyUser
            {

                pkCompanyId = p.pkCompanyId,
                CompanyName = p.CompanyName,

            }).ToList();

            return Json(Companies);


        }

      [HttpPost]
        public ActionResult GetLocation1(IEnumerable<AddCompanyUser> User, int CompanyId)
        {
            BALLocation ObjBALLocation = new BALLocation();
         
            var Locations = ObjBALLocation.GetLocationsByCompanyId(CompanyId).Select(p => new AddCompanyUser
            {

                pkLocationId = p.pkLocationId,
                City = p.City,

            }).ToList();
            return Json(Locations);

           
        }
    
        public int GetCompanyIdByLocationId(int PkLocationId)
        {
            BALCompany ObjBALCompany = new BALCompany();
            int CompanyId = 0;
            try
            {
                List<Proc_Get_CompanyDetailByLocationIdResult> ObjCompanyDetailByLocationId = ObjBALCompany.GetCompanyDetailByLocationId(PkLocationId);
                CompanyId = ObjCompanyDetailByLocationId.First().pkCompanyId;
            }
            catch (Exception)
            {

            }
            finally
            {
                ObjBALCompany = null;
            }
            return CompanyId;
        }

        public ActionResult GetStatus(IEnumerable<AddCompanyUser> User, bool IsUsaIntelCompany)
         {
             BALGeneral ObjBALGeneral = new BALGeneral();
             var RolesColl = ObjBALGeneral.FetchRolesAll(Utility.SiteApplicationId);

           if (RolesColl.Count != 0)
             {
                 
                 if (IsUsaIntelCompany == false)
                 {
                   var  RolesColl1 = RolesColl.Where(d => d.RoleName != "SalesRep" &&
                                      d.RoleName != "SupportManager" &&
                                      d.RoleName != "SupportTechnician" &&
                                      d.RoleName != "SalesAdmin" &&
                                      d.RoleName != "SalesManager" &&
                         
                                      d.RoleName != "SystemAdmin").Select(p => new 
                                         {

                                             RoleId = p.RoleId,
                                             RoleName = p.RoleName,

                                         }).ToList();

                   return Json(RolesColl1);
                 }
                 else
                 {
                  var   RolesColl1 = RolesColl.Where(d => d.RoleName != "SalesRep" &&
                                      d.RoleName != "SalesAdmin" &&
                                      d.RoleName != "BasicUser" &&
                                      d.RoleName != "CorporateManager" &&
                                      d.RoleName != "BranchManager" &&
                                      d.RoleName != "SalesManager" &&
                                      d.RoleName != "DataEntry").Select(p => new 
                                         {

                                             RoleId = p.RoleId,
                                             RoleName = p.RoleName,

                                         }).ToList();

                     return Json(RolesColl1);

                 }

             }


           return Json(string.Empty);
         }


        public ActionResult Save(AddCompanyUser User)
        {

            return View();
        
        
        
        
        }


    }
}
