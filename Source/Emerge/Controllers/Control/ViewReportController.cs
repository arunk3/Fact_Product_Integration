﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Common;
using Emerge.Controllers.Corporate;
using System.Web.Security;
using Emerge.Data;
using Emerge.Services;
using System.IO;
using System.Web.UI.WebControls;


namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        public ActionResult ViewReports()
        {

            Boolean flagOrderId = false;
            #region Get Query String
            BALOrders balObj = new BALOrders();
            ViewReportModel ObjModel = new ViewReportModel();
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["RR"]))
                {
                    ViewBag.RR = Convert.ToInt32(Request.QueryString["RR"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["_bck"]))
                {
                    ViewBag.bck = Convert.ToInt32(Request.QueryString["_bck"]);
                }


                #region Get Query String

                int OrderId = 0;

                #region for cookie issue in taleo
                TempData["loginfrom"] = Convert.ToString(Request.QueryString["loginfrom"]);
                if (TempData["loginfrom"] != null)
                {
                    //nd-16 changes on 02 julyb 2015
                    if (Convert.ToString(TempData["loginfrom"]) != "taleo")
                    {

                        TempData["TaleoOrderId"] = null;

                    }
                }
                #endregion
                if (Request.QueryString["_OId"] != null || TempData["TaleoOrderId"] != null)
                {


                    if (TempData["TaleoOrderId"] != null)
                    {
                        VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n ----ViewReports---------start-----" + TempData["TaleoOrderId"], "TaleoErrorLog");
                        VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n ----ViewReports---------Session[TaleoOrderId]-----" + TempData["TaleoOrderId"], "TaleoErrorLog");
                        //change for unique data fetch using iframe
                        if (Request.QueryString["_OId"] != null)
                        {

                            OrderId = Convert.ToInt32(Request.QueryString["_OId"]);
                        }
                        else
                        {
                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n ----ViewReports---------else in Request.QueryString[_OId]-----" + Convert.ToInt32(TempData["TaleoOrderId"]), "TaleoErrorLog");
                            OrderId = Convert.ToInt32(TempData["TaleoOrderId"]);
                            ViewData["TaleoOrderid"] = OrderId; // change on live 06 aug 2015 for taleo order id not found 
                            ViewBag.TaleoOrderid = TempData["TaleoOrderId"];
                        }

                        //comment for some session issue when using 2 user on sinle browser 

                    }
                    else
                    {

                        OrderId = Convert.ToInt32(Request.QueryString["_OId"]);

                    }
                    if (Session["Admin"] != null)
                    {
                        string role = Session["Admin"].ToString();
                        if (role == "SystemAdmin" || role == "SupportManager" || role == "SupportTechnician")
                        {
                            //Pass if logged on user is Admin
                        }
                        else
                        {
                            string cmpId = string.Empty;
                            //if (Session["TaleoOrderId"] != null) nd-16 change 
                            if (TempData["TaleoOrderId"] != null)
                            {
                                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                                {
                                    //int Status = -1;

                                    var OrderComapnyid = (from t1 in DX.tblOrderDetails
                                                          where t1.fkOrderId == Convert.ToInt32(TempData["TaleoOrderId"])
                                                          select new { t1.fkCompanyId }).FirstOrDefault();
                                    // cmpId = 

                                    cmpId = Convert.ToString(OrderComapnyid.fkCompanyId);

                                }


                            }
                            else
                            {
                                cmpId = Convert.ToString(Session["pkCompanyId"]);
                            }


                            flagOrderId = balObj.CheckOrderCompanyByOrderId(OrderId, Convert.ToInt32(cmpId));
                            if (!flagOrderId)
                            {
                                //return RedirectToAction("Index", "Home");
                                return RedirectToAction("SavedReports", "Corporate");
                            }
                        }
                    }



                #endregion



                }





                else if (Request.QueryString["_AdditionalOrderId"] != null)
                {
                    OrderId = Convert.ToInt32(Request.QueryString["_AdditionalOrderId"]);
                }
                BALCompany ObjBALCompany = new BALCompany();
                ObjModel.AdditionalReports = ObjBALCompany.GetAdditionalReportsByPkOrderId(OrderId);
                int ReviewQueryString = -1;
                if (!string.IsNullOrEmpty(Request.QueryString["RR"]))
                {
                    ReviewQueryString = Convert.ToInt32(Request.QueryString["RR"]);
                }

                int ViewReportReview = 0;
                if (Request.Url.AbsolutePath.Contains("ViewReportReview"))
                {
                    ViewReportReview = 1;
                }
                if (Roles.IsUserInRole("SystemAdmin") || Roles.IsUserInRole("SupportManager") || Roles.IsUserInRole("SupportTechnician"))
                {
                    ObjModel.IsAdmin = true;
                }
                else
                {
                    ObjModel.IsAdmin = false;
                }

                ObjModel.OrderId = OrderId;
            #endregion
                if (!string.IsNullOrEmpty(Request.QueryString["RR"]))
                {
                    ViewBag.RR = Convert.ToInt32(Request.QueryString["RR"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["_bck"]))
                {
                    ViewBag.bck = Convert.ToInt32(Request.QueryString["_bck"]);
                }


                #region User FullName
                string Username = string.Empty;
                ViewBag.UserFullName = Username = GetUserName();
                #endregion
                using (Corporate.CorporateController ObjCorporate = new Corporate.CorporateController())
                {
                    ObjCorporate.GetViewReport(ObjModel, OrderId, ReviewQueryString, ViewReportReview, Username);
                }
                ViewBag.CountyExist = "3";
                if (System.Web.HttpContext.Current.Session["CountyExist"] != null)
                {
                    ViewBag.CountyExist = System.Web.HttpContext.Current.Session["CountyExist"];
                    System.Web.HttpContext.Current.Session["CountyExist"] = null;
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in ViewReportReview(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "CustomDeadLockMsg");
                if (ex.Message.Contains("was deadlocked on lock resources with another process") || ex.Message.Contains("Value cannot be null") || ex.Message.Contains("Timeout expired"))
                {
                    return RedirectToAction("CustomDeadLockMsg", "Corporate");
                }
            }
            finally
            {

            }




            return View(ObjModel);
        }

        private string GetUserName()
        {
            string Username = string.Empty;
            if (Session["UserFullName"] != null)
            {
                Username = Convert.ToString(Session["UserFullName"]);
            }
            else
            {
                MembershipUser Member = Membership.GetUser();//User.Identity.Name
                if (Member != null)
                {
                    BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
                    Guid UserId = new Guid(Member.ProviderUserKey.ToString());
                    List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjData = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(UserId);
                    Session["CurrentUserCompanyName"] = ObjData.First().CompanyName;
                    Session["UserFullName"] = ObjData.First().FirstName + " " + ObjData.First().LastName;

                }
            }
            return Username;
        }
        public ActionResult ViewReportReview()
        {
            #region Get Query String
            ViewReportModel ObjModel = new ViewReportModel();
            BALContentManagement objBALContentManagement = new BALContentManagement();
            try
            {
                int OrderId = 0;
                if (Request.QueryString["_OId"] != null)
                {
                    #region Check Guid
                    //#19
                    //bool IsGuid = CommonHelper.IsGuid(Request.QueryString["_OId"]);
                    //if (!IsGuid)
                    //{
                    //    return RedirectToAction("SavedReports");
                    //}
                    OrderId = Convert.ToInt32(Request.QueryString["_OId"]);

                    string FullQueryString = Request.Url.Query;
                    ObjModel.hdnFullQString = FullQueryString;
                    ObjModel.OrderId = OrderId;
                    ObjModel.ReviewStatusComment = objBALContentManagement.GetReviewStatusComment(OrderId);
                    #endregion
                }
                BALCompany ObjBALCompany = new BALCompany();
                ObjModel.AdditionalReports = ObjBALCompany.GetAdditionalReportsByPkOrderId(OrderId);
                int ReviewQueryString = -1;
                if (!string.IsNullOrEmpty(Request.QueryString["RR"]))
                {
                    ReviewQueryString = Convert.ToInt32(Request.QueryString["RR"]);
                }
                ObjModel.CustomMsgsList = objBALContentManagement.GetCustomMessageList();
                List<Proc_Get_CustomReviewMsgsListResult> GetCustomMsgsList = objBALContentManagement.GetCustomMessageList();
                GetCustomMsgsList.Insert(0, new Proc_Get_CustomReviewMsgsListResult { LogTitle = "--Select--", pkLogId = -1 });
                ObjModel.CustomMsgsList = GetCustomMsgsList;
                if (Roles.IsUserInRole("SystemAdmin") || Roles.IsUserInRole("SupportManager") || Roles.IsUserInRole("SupportTechnician"))
                {
                    ObjModel.IsAdmin = true;
                }
                else
                {
                    ObjModel.IsAdmin = false;
                }
            #endregion

                using (Corporate.CorporateController ObjCorporate = new Corporate.CorporateController())
                {
                    ObjCorporate.GetViewReportReview(ObjModel, OrderId, true, ReviewQueryString);
                }
                #region User FullName
                ViewBag.UserFullName = GetUserName();
                #endregion
                ViewBag.CountyExist = "3";
                if (System.Web.HttpContext.Current.Session["CountyExist"] != null)
                {
                    ViewBag.CountyExist = System.Web.HttpContext.Current.Session["CountyExist"];
                    System.Web.HttpContext.Current.Session["CountyExist"] = null;
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in ViewReportReview(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "CustomDeadLockMsg");
                if (ex.Message.Contains("was deadlocked on lock resources with another process") || ex.Message.Contains("Value cannot be null") || ex.Message.Contains("Timeout expired"))
                {
                    return RedirectToAction("CustomDeadLockMsg", "Corporate");
                }
            }
            finally
            {

            }
            return View(ObjModel);

        }

        public ActionResult ViewDrugTestingReports()
        {
            #region Get Query String

            int OrderId = 0;
            if (Request.QueryString["_OId"] != null)
            {
                #region Check Guid
                //#19
                //bool IsGuid = CommonHelper.IsGuid(Request.QueryString["_OId"]);
                //if (!IsGuid)
                //{
                //    return RedirectToAction("SavedReports");
                //}
                OrderId = Convert.ToInt32(Request.QueryString["_OId"]);
                #endregion
            }

            #endregion
            if (!string.IsNullOrEmpty(Request.QueryString["RR"]))
            {
                ViewBag.RR = Convert.ToInt32(Request.QueryString["RR"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["_bck"]))
            {
                ViewBag.bck = Convert.ToInt32(Request.QueryString["_bck"]);
            }
            ViewReportModel ObjModel = new ViewReportModel();
            
            string ProductImagePath = Server.MapPath("~/Resources/Upload/Images/ProductImages/");
            using (CorporateController ObjCorporate = new CorporateController())
            {
                ObjCorporate.DrugTestingPageLoad(OrderId, ObjModel, ProductImagePath);
            }
            #region User FullName
            ViewBag.UserFullName = GetUserName();
            #endregion

            return View(ObjModel);
        }

        /// <summary>
        /// for ticket #17 Edit ship to address
        /// </summary>
        /// <returns></returns>
        public ActionResult EditDrugTestingOrder()
        {
            int OrderId = 0;
            EditDrugTestingModel objEditDreug = new EditDrugTestingModel();
           // string orderInformation = string.Empty;
            string ProductImagePath = Server.MapPath("~/Resources/Upload/Images/ProductImages/");
            if (!string.IsNullOrEmpty(Request.QueryString["_a"]))
            {
                OrderId = Convert.ToInt32(Request.QueryString["_a"]);
            }
            EditDrugTestingPageLoad(OrderId, objEditDreug, ProductImagePath);
            return View(objEditDreug);
        }

        /// <summary>
        /// post udpated data and save update value in db for ticket #17
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateDrugTestingOrder(FormCollection collection)
        {
            bool CheckShipToAddressExist = false;
            int Result = 0;
            string Message = string.Empty;
            string Css = string.Empty;
            int orderId = 0;
            short productQuantity = 0;
            ShipToAddress objShipTo = new ShipToAddress();
            if (collection.Count > 0 && collection != null)
            {
                BALGeneral objBalGeneral = new BALGeneral();
                //get address details
                objShipTo.CompanyName = collection["CompanyName"];
                objShipTo.Address1 = collection["Address1"];
                objShipTo.Address2 = collection["Address2"] != null ? collection["Address2"] : string.Empty;
                objShipTo.StateCode = collection["StateCode"];
                objShipTo.City = collection["City"];
                objShipTo.ZipCode = collection["ZipCode"];
                //get order and company user information
                orderId = Convert.ToInt32(collection["OrderId"]);
                objShipTo.fkOrderId = orderId;
                objShipTo.LocationId = Convert.ToInt32(collection["LocationId"]);
                objShipTo.CompanyUserId = Convert.ToInt32(collection["CompanyUserId"]);
                CheckShipToAddressExist = objBalGeneral.CheckCompanyExistanceForShipAddress(objShipTo.LocationId, objShipTo.CompanyUserId);
                if (CheckShipToAddressExist)
                {
                    objBalGeneral.UpdateShipToAddressInDB(objShipTo);
                }
                else
                {
                    objBalGeneral.SaveShipToAddressInDB(objShipTo);
                }
                //get product information
                decimal ReportAmount = decimal.Parse(collection["ReportAmount"]);
                productQuantity = Convert.ToInt16(collection["ProductQty"]);
                decimal AdditionalFee = decimal.Parse(collection["ShippingCost"]);
                Result = objBalGeneral.UpdateDrugTestProductInDb(orderId, ReportAmount, productQuantity, AdditionalFee);
                if (Result == 1)
                {
                    Message = "Updated Successfully";
                    Css = "SuccessMsg";
                }
                else
                {
                    Message = "Some error occured while update drug testing product information";
                    Css = "ErrorMsg";
                }
            }
            return Json(new { Msg = Message, Css = Css });

        }


        public ActionResult ViewEscreenReport()
        {
            #region Get Query String

            int OrderId = 0;
            if (Request.QueryString["_OId"] != null)
            {
                #region Check Guid
                //#19
                //bool IsGuid = CommonHelper.IsGuid(Request.QueryString["_OId"]);
                //if (!IsGuid)
                //{
                //    return RedirectToAction("SavedReports");
                //}
                OrderId = Convert.ToInt32(Request.QueryString["_OId"]);
                #endregion
            }

            #endregion
            if (!string.IsNullOrEmpty(Request.QueryString["_bck"]))
            {
                ViewBag.bck = Convert.ToInt32(Request.QueryString["_bck"]);
            }
            ViewEscreenReportModel ObjModel = new ViewEscreenReportModel();
            using (CorporateController ObjCorporate = new CorporateController())
            {
                ObjCorporate.eScreenPageLoad(OrderId, ObjModel);
            }
            return View(ObjModel);
        }

        public ActionResult AddReviewStatusLog(int OrderId, string Comment)
        {
            int status = -1;
            try
            {
                BALContentManagement ObjBALContentManagement = new BALContentManagement();
                status = ObjBALContentManagement.InsertReviewStatusLog(OrderId, Comment);
            }
            catch
            {

            }
            //
            return Json(status);

        }

        public ActionResult UpdateReviewReportToNoResponse(int OrderId)
        {
            int status = 0;
            //int ReviewStatus = 6;
            BALEmergeReview ObjReview = new BALEmergeReview();
            int pkOrderId = OrderId;
            status = ObjReview.UpdateReviewStatusToNoResponse(pkOrderId);
            return Json(status);
        }

        [HttpPost]
        public ActionResult CheckLogin(string User)
        {

            #region Get Query String
            ViewReportModel ObjModel = new ViewReportModel();
            BALContentManagement objBALContentManagement = new BALContentManagement();
            try
            {
                int OrderId = 0;
                if (Request.QueryString["_OId"] != null)
                {
                    #region Check Guid
                    //#19
                    //bool IsGuid = CommonHelper.IsGuid(Request.QueryString["_OId"]);
                    //if (!IsGuid)
                    //{
                    //    return RedirectToAction("SavedReports");
                    //}
                    OrderId = Convert.ToInt32(Request.QueryString["_OId"]);

                    string FullQueryString = Request.Url.Query;
                    ObjModel.hdnFullQString = FullQueryString;
                    ObjModel.OrderId = OrderId;
                    ObjModel.ReviewStatusComment = objBALContentManagement.GetReviewStatusComment(OrderId);
                    #endregion
                }
                BALCompany ObjBALCompany = new BALCompany();
                ObjModel.AdditionalReports = ObjBALCompany.GetAdditionalReportsByPkOrderId(OrderId);
                int ReviewQueryString = -1;
                if (!string.IsNullOrEmpty(Request.QueryString["RR"]))
                {
                    ReviewQueryString = Convert.ToInt32(Request.QueryString["RR"]);
                }
                ObjModel.CustomMsgsList = objBALContentManagement.GetCustomMessageList();
                List<Proc_Get_CustomReviewMsgsListResult> GetCustomMsgsList = objBALContentManagement.GetCustomMessageList();
                GetCustomMsgsList.Insert(0, new Proc_Get_CustomReviewMsgsListResult { LogTitle = "--Select--", pkLogId = -1 });
                ObjModel.CustomMsgsList = GetCustomMsgsList;
                if (Roles.IsUserInRole("SystemAdmin") || Roles.IsUserInRole("SupportManager") || Roles.IsUserInRole("SupportTechnician"))
                {
                    ObjModel.IsAdmin = true;
                }
                else
                {
                    ObjModel.IsAdmin = false;
                }
            #endregion

                using (Corporate.CorporateController ObjCorporate = new Corporate.CorporateController())
                {
                    ObjCorporate.GetViewReportReview(ObjModel, OrderId, true, ReviewQueryString);
                }
                #region User FullName
                ViewBag.UserFullName = GetUserName();
                #endregion
                ViewBag.CountyExist = "3";
                if (System.Web.HttpContext.Current.Session["CountyExist"] != null)
                {
                    ViewBag.CountyExist = System.Web.HttpContext.Current.Session["CountyExist"];
                    System.Web.HttpContext.Current.Session["CountyExist"] = null;
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in ViewReportReview(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "CustomDeadLockMsg");
                if (ex.Message.Contains("was deadlocked on lock resources with another process") || ex.Message.Contains("Value cannot be null") || ex.Message.Contains("Timeout expired"))
                {
                    return RedirectToAction("CustomDeadLockMsg", "Corporate");
                }
            }
            finally
            {

            }
            return PartialView("~/Views/Common/PartialViewReportReview.cshtml", ObjModel);
        }
        public FilePathResult DownloadAttachment(string FPathTitle)
        {
            string[] str = FPathTitle.Split(',');
            string FileName = str[0];
            string Title = str[1].Replace(" ", "_");
            string FilePath = Request.PhysicalApplicationPath + "Resources/Upload/ResponseImages/" + FileName;
            string fileext = System.IO.Path.GetExtension(FileName);
            string CType = "application/" + fileext.Replace(".", "");
            //string fileName = Path.GetFileName(FileName);

            string strFileTitle = Title + fileext;



            if (System.IO.File.Exists(FilePath))
            {
                return File(FilePath, CType, strFileTitle);
            }
            else
            {
                FilePath = Request.PhysicalApplicationPath + "Resources/Upload/FeatureFiles/no-image.jpg";
                return File(FilePath, CType, strFileTitle);
            }
        }


        public void EditDrugTestingPageLoad(int OrderId, EditDrugTestingModel objEditDreug, string ProductImagePath)
        {
            BALGeneral objBalGenral = new BALGeneral();
            ShipToAddress objShipToAddress = new ShipToAddress();
            BALOrders ObjBALOrders = new BALOrders();
            List<Proc_Get_DrugTestOrderDetailsResult> ObjData = ObjBALOrders.GetDrugTestOrderDetails(OrderId);
            if (ObjData.Count() > 0)
            {
                ProductImagePath = ProductImagePath + ObjData.First().ProductImage;
                string sImg = string.Empty;

                if (System.IO.File.Exists(ProductImagePath))
                {
                    string ImageVirtualPath = ApplicationPath.GetApplicationPath() + "Resources/Upload/Images/ProductImages/" + ObjData.First().ProductImage;
                    string imgTitle = ObjData.First().LongDescription;

                    System.Drawing.Image ImgProduct = System.Drawing.Image.FromFile(ProductImagePath);
                    int ImgWidth = 0, ImgHeight = 0;

                    if (ImgProduct != null)
                    {
                        ImgWidth = ImgProduct.Width;
                        ImgHeight = ImgProduct.Height;
                    }
                    ImgWidth = (ImgWidth > 130) ? 130 : ImgWidth;
                    ImgHeight = (ImgHeight > 150) ? 150 : ImgHeight;

                    sImg = "<img src='" + ImageVirtualPath + "' width='" + ImgWidth + "px' height='" + ImgHeight + "px' style='border:1px solid black;' title='" + imgTitle + "'/>";
                    objEditDreug.ProductImagePath = sImg;
                }


               // string Address = string.Empty;
                objShipToAddress = objBalGenral.GetUpdatedShipToAddress(Convert.ToInt32(ObjData.First().fkLocationId.ToString()), Convert.ToInt32(ObjData.First().pkCompanyUserId.ToString()), OrderId);
                objEditDreug.LocationId = Convert.ToInt32(ObjData.First().fkLocationId.ToString());
                objEditDreug.CompanyUserId = Convert.ToInt32(ObjData.First().pkCompanyUserId.ToString());
                if (objShipToAddress != null)
                {
                    objEditDreug.CompanyName = objShipToAddress.CompanyName;
                    objEditDreug.Address1 = objShipToAddress.Address1;
                    objEditDreug.Address2 = objShipToAddress.Address2 != null ? objShipToAddress.Address2 : string.Empty;
                    objEditDreug.StateCode = objShipToAddress.StateCode;
                    objEditDreug.City = objShipToAddress.City;
                    objEditDreug.ZipCode = objShipToAddress.ZipCode;
                }
                else
                {
                    objEditDreug.CompanyName = ObjData.First().CompanyName;
                    objEditDreug.Address1 = ObjData.First().Address1;
                    objEditDreug.Address2 = ObjData.First().Address2 != "" ? ObjData.First().Address2 : string.Empty;
                    objEditDreug.StateCode = ObjData.First().StateCode;
                    objEditDreug.City = ObjData.First().City;
                    objEditDreug.ZipCode = ObjData.First().ZipCode;
                }

                decimal ReportAmount;
                decimal.TryParse(ObjData.First().ReportAmount.ToString(), out ReportAmount);
                objEditDreug.ReportAmount = ReportAmount;
                short ProductQty;
                short.TryParse(ObjData.First().ProductQty.ToString(), out ProductQty);
                objEditDreug.ProductQty = ProductQty;
                decimal OrderAmount = ReportAmount * ProductQty;
                objEditDreug.OrderAmount = OrderAmount;

                if (ObjData.First().AdditionalFee != decimal.MinValue)
                {
                    OrderAmount += ObjData.First().AdditionalFee;
                    objEditDreug.ShippingCost = ObjData.First().AdditionalFee;
                }

                objEditDreug.ProductDisplayName = ObjData.First().ProductDisplayName;
                objEditDreug.OrderId = OrderId;
            }
        }

    }
}
