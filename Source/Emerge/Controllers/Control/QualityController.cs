﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using System.Web.Security;
using Emerge.Service;
namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /Quality/

        public ActionResult PartialQuality()
        {
            return View();
        }

        public ActionResult QualityBind(int page, int pageSize)
        {
            BALCompany ObjBALCompany = new BALCompany();
            int PageSize = pageSize;
            int iPageNo = page;

            var QualityCollection = ObjBALCompany.GetClientsforQualityControl(
                                                        iPageNo,
                                                        PageSize,
                                                        true,
                                                        string.Empty,
                                                        -1,
                                                        -1,
                                                        "CreatedDate",
                                                        "DESC", string.Empty, string.Empty,
                                                        Guid.Empty,
                                                        Guid.Empty,
                                                        new Guid(Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString()),
                                                        string.Empty);


            return Json(new
            {
                Products = QualityCollection,
                TotalCount = QualityCollection.Count > 0 ?QualityCollection.ElementAt(0).TotalRec : 0
            },
                           JsonRequestBehavior.AllowGet);
        
        }


        public JsonResult UpdateQuality()
        {
           BALCompanyUsers  ObjBALCompanyUsers=new BALCompanyUsers();
           Guid UserId = new Guid(Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString());
           List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjDbCollection = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(UserId);
           int CompanyUserId =ObjDbCollection.First().pkCompanyUserId;
           string FullName=ObjDbCollection.First().FullName;
          
              return Json(new { data = CompanyUserId, Name=FullName, JsonRequestBehavior = JsonRequestBehavior.AllowGet });
        }


        public ActionResult UpdateClient_Click(int CompanyId, string Surveyor, int Rating,string Comment,string CompanyName)
        {
            string strMessage = "";
             Guid UserId;
            int pkCompanyId = CompanyId;
            UserId = new Guid(Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString());
            DateTime CreatedDate = System.DateTime.Now;
            tblCompany objtblCompany = new tblCompany();
            objtblCompany.pkCompanyId = pkCompanyId;
            objtblCompany.SurveyorId = UserId;
            objtblCompany.SuveyorName = Surveyor;
            objtblCompany.Rating = Convert.ToByte(Rating);
            objtblCompany.ReviewComments = Convert.ToString(Comment);
            objtblCompany.LastReviewDate = CreatedDate;
            BALCompany objBALCompany = new BALCompany();
            int success = objBALCompany.UpdateCompanyRating(objtblCompany);
            string Comments = "##12##" + Comment;
            //int result = 
                objBALCompany.AddCommentsToCallLog(pkCompanyId, UserId, Comments, CreatedDate);

            string SalesRepEmailId = objBALCompany.GetSalesRepEmailByCompanyId(pkCompanyId);

            if (SalesRepEmailId != null && SalesRepEmailId == "")
            {
               //bool checksend = 
                   sendMailToSalesRep(CompanyName, SalesRepEmailId, Convert.ToByte(objtblCompany.Rating), Comments);
            }

            if (success == 1)
            {
                strMessage = "<span class='successmsg'>Quality Control Survey completed.</span>";

               
            }
            return Json(new { UpdateMessage = strMessage});
        
        
        }
        private bool sendMailToSalesRep(string CompanyName, string SalesRepEmailId, byte rating, string Comments)
        {
            bool CheckSend = false;
            try
            {
                BALGeneral ObjBALGeneral = new BALGeneral();
                MembershipUser ObjMembershipUser = Membership.GetUser();
                string emailFrom = ObjMembershipUser.Email;
                string StrDate = DateTime.Now.ToString();

                #region Replace Bookmark
                string MessageBody = "";
                string emailText = "";
                string Status = "Quality Control Survey Completed.";                
                string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;
                string Rating = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/rating_" + rating + ".jpg";
                var emailContent = ObjBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, Convert.ToInt32(EmailTemplates.QualityControlSurvey));
                emailText = emailContent.TemplateContent;

                BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                Bookmarks objBookmark = new Bookmarks();
                #region For New Style SystemTemplate
                string Bookmarkfile = string.Empty;
                string bookmarkfilepath = string.Empty;

                Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                emailText = Bookmarkfile;
                #endregion


                objBookmark.EmailContent = emailText;
                objBookmark.Date = StrDate;
                objBookmark.CompanyName = CompanyName;
                objBookmark.CompanyStatus = Status;
                objBookmark.Comments = Comments == null || Comments == string.Empty ? "NA" : Comments.Trim();
                objBookmark.Rating = "<img src='" + Rating + "' /> (" + rating + " Star)";
                objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                objBookmark.LoginLink = "<a style='color: #87ceea;' href='https://emerge.intelifi.com/'> Click here </a>";

                MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Utility.GetUID(HttpContext.User.Identity.Name.ToString()));
                #endregion

                // Ticket #101 to send mails in CC                
                string UserEmails = string.Empty;
                if (!string.IsNullOrEmpty(emailFrom))
                {
                    UserEmails = ObjBALGeneral.GetUserOtherMailIds(emailFrom);                
                }
                //Utility.SendMail(SalesRepEmailId, UserEmails, string.Empty, emailFrom, MessageBody, "Quality Control Survey");
                CheckSend = Utility.SendMail(SalesRepEmailId,UserEmails, emailFrom, MessageBody, "Quality Control Survey");
            }
            catch
            {

            }
            return CheckSend;
        }
  }
}
