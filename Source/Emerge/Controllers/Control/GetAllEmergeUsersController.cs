﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using System.Web.Security;
using System.IO;
using System.Text;
using System.Configuration;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        //
        // GET: /GetAllEmergeUsers/

        public ActionResult GetEmergeUsers()
        {
            CompanyModel objGetLocation = new CompanyModel();
            int CompanyId = 0;
            BALLocation ObjBALLocation = new BALLocation();
            objGetLocation.GetLocation = ObjBALLocation.GetLocationsByCompanyId(CompanyId);
            List<SelectListItem> County = new List<SelectListItem>();
            SelectListItem s1 = new SelectListItem { Text = "Active", Value = "Active" };
            SelectListItem s2 = new SelectListItem { Text = "InActive", Value = "InActive" };
            SelectListItem s3 = new SelectListItem { Text = "Locked", Value = "Locked" };
            SelectListItem s4 = new SelectListItem { Text = "PastDue", Value = "PastDue" };
            SelectListItem s5 = new SelectListItem { Text = "InCollections", Value = "InCollections" };
            County.Add(s1);
            County.Add(s2);
            County.Add(s3);
            County.Add(s4);
            County.Add(s5);
            objGetLocation.CountryName = County;
            ViewBag.PwdToExport = ConfigurationManager.AppSettings["PwdToExportCSV"].ToString();

            return View(objGetLocation);
        }

        [HttpPost]
        public ActionResult GetAllEmergeUser(int page, int pageSize, List<GridSort> sort, List<GridFilter> filters)
        {
            # region filter
            try
            {
                int PageNum = page;
                int PageSize = pageSize;
                string SortingColumn = "FirstName";
                string SortingDirection = "asc";
                int TotalRec = 0;
                bool IsPaging = true;
                int LocationId = 0;
                int FkCompanyId = 0;
                if (sort != null)
                {
                    if (sort[0].Field == "UserDisplayRole")
                    {

                        sort[0].Field = "UserRole";
                    }
                    if (sort[0].Field == "City+IsHome")
                    {

                        sort[0].Field = "City";

                    }
                    SortingDirection = sort[0].Dir;
                    SortingColumn = sort[0].Field;
                }
                Int16 CompanyType = -1;
                ObjBALCompany = new BALCompany();
                //List<UserViewModel> ObjUserViewModel = new List<UserViewModel>();
                List<Proc_Get_AllEmergeUsersForAdminResult> OData = new List<Proc_Get_AllEmergeUsersForAdminResult>();

                if (filters != null)
                {

                    foreach (var ObjFilter in filters)
                    {
                        OData = FilterUsers(ObjFilter, PageNum, PageSize, IsPaging, LocationId, CompanyType, FkCompanyId, SortingColumn, SortingDirection, out  TotalRec);
                    }
                }
                else
                {
                    OData = ObjBALCompany.GetAllEmergeUsersForAdmin(PageNum, PageSize, IsPaging, CompanyType, LocationId, FkCompanyId, SortingColumn, SortingDirection, string.Empty, 0, string.Empty, out TotalRec).ToList();
                }

                Session["TotalEmergeUser"] = OData.Count == 0 ? 0 : OData.ElementAt(0).TotalRec;

                if (OData.Count > 0)
                {
                    return Json(new { Reclist = OData, TotalRec = OData.ElementAt(0).TotalRec });
                }
                else
                {
                    return Json(new { Reclist = OData, TotalRec = OData.Count });
                }
            }
            catch (Exception)
            {
                return View(new List<UserViewModel>());
            }

            #endregion
            //BALCompany serobj = new BALCompany();
            //Guid fkLocationId = Guid.Empty;
            //var fkCompanyId = Guid.Empty;
            //if (filters != null)
            //{
            //    fkLocationId = new Guid(filters.ElementAt(0).Value);
            //}


            //var list = serobj.GetAllEmergeUsersForAdmin(page, pageSize, true,"", -1, fkCompanyId, fkLocationId, "FirstName", "ASC");

            //return Json(new { Reclist = list, TotalRec = list.ElementAt(0).TotalRec });
        }


        public FileResult ExportUsersInfoToCSV()
        {


            //   IEnumerable products = db.Products.ToDataSourceResult(request).Data;

            //BALChat objBalChat = new BALChat();
            int PageNum = 1;
            int PageSize = Convert.ToInt32(Session["TotalEmergeUser"]);
            string SortingColumn = "FirstName";
            string SortingDirection = "asc";
            int TotalRec = 0;
            bool IsPaging = true;
            int LocationId = 0;
            int FkCompanyId = 0;
            Int16 CompanyType = -1;
            BALCompany ObjBALCompany = new BALCompany();

            var AllUsersList = ObjBALCompany.GetAllEmergeUsersForAdmin(PageNum, PageSize, IsPaging, CompanyType, LocationId, FkCompanyId, SortingColumn, SortingDirection, string.Empty, 0, string.Empty, out TotalRec).ToList();
            MemoryStream output = new MemoryStream();

            StreamWriter writer = new StreamWriter(output, Encoding.UTF8);
            try
            {
                writer.Write("User Name,");
                writer.Write("First Name ,");
                writer.Write("Last Name ,");
                writer.Write("Title  , ");
                writer.Write("Company , ");
                writer.Write("Location   ,");
                writer.Write("State  ,");

                //writer.Write("Status , ");
                writer.Write("Sales Associate    ,");
                writer.Write("Account Manager   ,");

                writer.Write("Last Login");
                writer.WriteLine();

                for (int i = 0; i < AllUsersList.Count(); i++)
                {

                    BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
                    proc_Get_CompanyUser_ByCompanyUserIdResult ObjtblCompanyUser = ObjBALCompanyUsers.SelectCompanyUserByCompanyUserId(Int32.Parse(AllUsersList.ElementAt(i).pkCompanyUserId.ToString()));

                    MembershipUser ObjMembershipUser1 = Membership.GetUser(ObjtblCompanyUser.fkUserId);
                    //string[] ObjRoles = Roles.GetRolesForUser(ObjMembershipUser1.UserName);
                    //string RoleName = string.Empty;
                    //if (ObjRoles.Count() == 2)
                    //{
                    //    foreach (string rol in ObjRoles)
                    //    {
                    //        if (rol.ToLower() != "corporatemanager")
                    //        {
                    //            RoleName = rol;
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    RoleName = ObjRoles[0].ToString();
                    //}
                    string LastLoginByUser = ObjMembershipUser1.LastLoginDate.ToString();
                    string UserEmail = ObjMembershipUser1.UserName;
                    string StateName = string.Empty;
                    BALCompany objBALCompany = new BALCompany();
                    var SalesAssocRec = objBALCompany.GetSalesAssociatebypkCompanyId(Int32.Parse(AllUsersList.ElementAt(i).pkCompanyId.ToString()));
                    string SaleAssociaName = string.Empty;
                    string AccountManger = string.Empty;
                    string JobTiltle = string.Empty;

                    if (SalesAssocRec != null && SalesAssocRec.Count > 0)
                    {
                        SaleAssociaName = SalesAssocRec.ElementAt(0).SalesAssociate;
                    }
                    var AccountMangerRec = objBALCompany.GetAccountManagerbypkCompanyId(Int32.Parse(AllUsersList.ElementAt(i).pkCompanyId.ToString()));

                    if (AccountMangerRec != null && AccountMangerRec.Count > 0)
                    {
                        JobTiltle = AccountMangerRec.ElementAt(0).MainTitle;
                        AccountManger = AccountMangerRec.ElementAt(0).AccounManager;
                    }

                    BALCompany serobj1 = new BALCompany();
                    var list1 = serobj1.GetState();
                    if (ObjtblCompanyUser.fkStateId != null)
                    {
                        var StateName1 = list1.Where(rec => rec.pkStateId == ObjtblCompanyUser.fkStateId).FirstOrDefault();
                        if (StateName1 != null)
                        {
                            StateName = StateName1.StateName == null ? string.Empty : StateName1.StateName;
                        }

                    }
                    List<clsCounty> ObjCollection1 = new List<clsCounty>();
                    BALGeneral ObjBALGeneral1 = new BALGeneral();
                    ObjCollection1 = ObjBALGeneral1.GetStatesCounties(Convert.ToInt32(ObjtblCompanyUser.fkStateId)).Select(p => new clsCounty
                    {
                        pkCountyId = p.pkCountyId,
                        CountyName = p.CountyName

                    }).ToList();
                    //string CountyName2 = string.Empty;
                    if (ObjtblCompanyUser.fkCountyId != null)
                    {
                        var CountyName1 = ObjCollection1.Where(rec => rec.pkCountyId == ObjtblCompanyUser.fkCountyId.ToString()).FirstOrDefault();
                        if (CountyName1 != null)
                        {
                            //string CountyName = CountyName1.CountyName == null ? string.Empty : CountyName1.CountyName;

                            //CountyName2 = CountyName == null ? string.Empty : CountyName;
                        }
                    }
                    //string Password = ObjMembershipUser1.GetPassword();

                    writer.Write(UserEmail == null ? string.Empty : UserEmail);
                    writer.Write(",");
                    writer.Write("\"");


                    //string FullName = ObjtblCompanyUser.FirstName + " " + ObjtblCompanyUser.LastName;
                    writer.Write(ObjtblCompanyUser.FirstName == null ? string.Empty : ObjtblCompanyUser.FirstName.Replace(",", " "));
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");

                    writer.Write(ObjtblCompanyUser.LastName == null ? string.Empty : ObjtblCompanyUser.LastName.Replace(",", " "));
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");

                    writer.Write(JobTiltle == null ? string.Empty : JobTiltle.Replace(",", " "));
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");


                    writer.Write(AllUsersList.ElementAt(i).CompanyName == null ? string.Empty : AllUsersList.ElementAt(i).CompanyName.Replace(",", " "));
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");


                    writer.Write(AllUsersList.ElementAt(i).City == null ? string.Empty : AllUsersList.ElementAt(i).City);
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");

                    writer.Write(StateName == null ? string.Empty : StateName);
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");

                    writer.Write(SaleAssociaName == null ? string.Empty : SaleAssociaName.Replace(",", " "));
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");



                    //writer.Write(RoleName == null ? string.Empty : RoleName.Replace(",", " "));
                    //writer.Write("\"");
                    //writer.Write(",");
                    //writer.Write("\"");



                    writer.Write(AccountManger == null ? string.Empty : AccountManger.Replace(",", " "));
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");



                    writer.Write(LastLoginByUser == null ? string.Empty : LastLoginByUser);
                    writer.Write("\"");
                    writer.WriteLine();


                }
                writer.Flush();
                output.Position = 0;
            }
            catch (Exception)
            {

                throw;
            }
            return File(output, "text/comma-separated-values", "EmergeUsers.csv");
        }

        private static List<Proc_Get_AllEmergeUsersForAdminResult> FilterUsers(GridFilter filter, int PageNum, int PageSize, bool IsPaging, int LocationId, Int16 CompanyType, int FkCompanyId, string SortingColumn, string SortingDirection, out int TotalRec)
        {
            BALCompany ObjBALCompany = new BALCompany();
            List<Proc_Get_AllEmergeUsersForAdminResult> source = new List<Proc_Get_AllEmergeUsersForAdminResult>();
            byte ActionType = 0;
            string ColumnName = filter.Field;
            string FilterValue = filter.Value.ToString();
            ActionType = GetFilterAction(filter, ActionType);
            source = ObjBALCompany.GetAllEmergeUsersForAdmin(PageNum, PageSize, IsPaging, CompanyType, LocationId, FkCompanyId, SortingColumn, SortingDirection, ColumnName, ActionType, FilterValue, out TotalRec).ToList();
            return source;
        }

        public ActionResult UpdateDisableUser(string UserId)
        {
            //int result = 0;
            ObjBALCompanyUsers = new BALCompanyUsers();
            List<tblCompanyUser> UserList = new List<tblCompanyUser>();
            string[] stringSeparators = new string[] { "#" };
            string[] Listcompanyuserid = UserId.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
            try
            {
                for (int i = 0; i < Listcompanyuserid.Length; i++)
                {
                    tblCompanyUser objtblCompanyUser = new tblCompanyUser();
                    objtblCompanyUser.fkUserId = new Guid(Listcompanyuserid[i]);
                    UserList.Add(objtblCompanyUser);

                }
                //result = 
                    ObjBALCompanyUsers.ActionOnUsers(UserList, Convert.ToByte(1), Convert.ToBoolean(0));

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBALCompanyUsers = null;
            }







            return View();

        }


        public ActionResult EnabledDisableMIfield(int pkCompanyUserId, bool Enable)
        {
            BALCompanyUsers objbal = new BALCompanyUsers();
            objbal.UpdateMIfield(pkCompanyUserId, Enable);

            return Json(new { Reclist = 1 });
        }

        public ActionResult UpdateEnableUser(string UserId)
        {

            int result = 0;
            ObjBALCompanyUsers = new BALCompanyUsers();
            List<tblCompanyUser> UserList = new List<tblCompanyUser>();
            string[] stringSeparators = new string[] { "#" };
            string[] Listcompanyuserid = UserId.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
            try
            {
                for (int i = 0; i < Listcompanyuserid.Length; i++)
                {


                    tblCompanyUser objtblCompanyUser = new tblCompanyUser();
                    objtblCompanyUser.fkUserId = new Guid(Listcompanyuserid[i]);
                    UserList.Add(objtblCompanyUser);

                }
                result = ObjBALCompanyUsers.ActionOnUsers(UserList, Convert.ToByte(1), Convert.ToBoolean(1));

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBALCompanyUsers = null;
            }

            return Json(result);
        }


        public ActionResult DeleteSelectedUser(string UserId)
        {
            int result = 0;
            ObjBALCompanyUsers = new BALCompanyUsers();
            List<tblCompanyUser> UserList = new List<tblCompanyUser>();
            string[] stringSeparators = new string[] { "#" };
            string[] Listcompanyuserid = UserId.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
            try
            {
                for (int i = 0; i < Listcompanyuserid.Length; i++)
                {


                    tblCompanyUser objtblCompanyUser = new tblCompanyUser();
                    objtblCompanyUser.fkUserId = new Guid(Listcompanyuserid[i]);
                    UserList.Add(objtblCompanyUser);


                }

                result = ObjBALCompanyUsers.ActionOnUsers(UserList, Convert.ToByte(2), Convert.ToBoolean(1));

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBALCompanyUsers = null;
            }
            return Json(result);


        }
        public ActionResult DeleteUser(int companyserid)
        {

            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            ObjBALCompanyUsers.DeleteEmergeUserById(Int32.Parse(companyserid.ToString()));
            return View();

        }
        public ActionResult GridFiltertext(string GridFiltertext)
        {
            @ViewData["GridFiltertext"] = GridFiltertext;
            return View();

        }

    }
}
