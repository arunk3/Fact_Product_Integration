﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using System.Web.Security;
using Emerge.Common;
using Emerge.Data;
using Emerge.Services;
using System.IO;
using Emerge.Controllers.Common;

namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        public ActionResult SavedReports()
        {

            int pkCompanyId = 0;
            int pkLocationId = 0;
            int pkCompanyUserId = 0;
            SavedReportModel ObjModel = new SavedReportModel();
            ObjModel.pkProductApplicationId = 0;
            string strqryst = string.Empty;
            string strqryst2 = string.Empty;
            bool ShowIncomplete = false;
            string[] Role;
            Role = new string[0];

            MembershipUser ObjMembership = Membership.GetUser(User.Identity.Name);
            if (ObjMembership != null)
            {
                Role = Roles.GetRolesForUser(ObjMembership.Email);
                ObjModel.UserRoles = Role;
                ObjModel.Role = 1;//Admin
            }

            #region Query String

            if (!string.IsNullOrEmpty(Request.QueryString["qryst"]))
            {
                strqryst = Request.QueryString["qryst"];
                ObjModel.Keyword = strqryst;

            }
            if (!string.IsNullOrEmpty(Request.QueryString["qryst2"]))
            {
                strqryst2 = Request.QueryString["qryst2"];
                ObjModel.SearchedOntbl = strqryst2;
               
            }

            #endregion

            #region Get Columns from Profile

            try
            {
                MembershipUser Member = Membership.GetUser(User.Identity.Name);
                if (Member != null)
                {
                    ProfileCommon Profile = new ProfileCommon();
                    ProfileModel userProfile = Profile.GetProfile(Member.Email);
                    if (userProfile.RecordSize != string.Empty)
                    {
                        TempData["PageSize"] = userProfile.RecordSize;
                        ObjModel.PageSize = Convert.ToInt32(userProfile.RecordSize);
                    }
                    else
                    {
                        ObjModel.PageSize = 25;
                    }
                    if (userProfile.PageNo != string.Empty)
                    {
                        TempData["PageNo"] = userProfile.PageNo;
                        ObjModel.PageNo = Convert.ToInt32(userProfile.PageNo);
                    }
                    else
                    {
                        ObjModel.PageNo = 1;
                    }
                    if (Request.QueryString["_bck"] != null)
                    {
                        ObjModel.Keyword = userProfile.SearchedText;
                        strqryst = userProfile.SearchedText;
                        pkCompanyId = Convert.ToInt32(userProfile.Company);
                        pkLocationId = Convert.ToInt32(userProfile.Location);
                        pkCompanyUserId = Convert.ToInt32(userProfile.User);
                        ObjModel.pkCompanyUserId = Convert.ToInt32(userProfile.User);
                        ObjModel.pkProductApplicationId = Convert.ToInt32(userProfile.DataSource);
                    }
                }
                ObjModel.IsReportStatus = new BALCompany().GetPassFailReportStatus(Member.Email);
                #region User FullName
                if (Session["UserFullName"] != null)
                {
                    ViewBag.UserFullName = Convert.ToString(Session["UserFullName"]);
                }
                else
                {
                    if (Member != null)
                    {
                        BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
                        Guid UserId = new Guid(Member.ProviderUserKey.ToString());
                        List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjData = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(UserId);
                        Session["CurrentUserCompanyName"] = ObjData.First().CompanyName;
                        ViewBag.UserFullName = ObjData.First().FirstName + " " + ObjData.First().LastName;

                    }
                }
                #endregion
            }
            catch (Exception)
            {
            }
            #endregion

            using (Corporate.CorporateController ObjCorporate = new Corporate.CorporateController())
            {
                ObjCorporate.SavedReportPageLoad(ObjModel, pkCompanyId, pkLocationId, pkCompanyUserId, strqryst.Replace("'", "''"), ShowIncomplete, Role);
            }
            return View(ObjModel);
        }
       
    }
}
