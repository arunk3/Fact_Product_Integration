﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using System.Text;
using System.IO;
using Emerge.Common;
using System.Web.UI;
using System.Web.Security;
namespace Emerge.Controllers.Control
{
    public partial class ControlController : Controller
    {
        public ActionResult Filters()
        {
            BALGeneral ObjBALGeneral = new BALGeneral();
            var Objkey_word = ObjBALGeneral.Get_Filter_Keywords();
            PenalCodeModel ObjPenalCodeModel = new PenalCodeModel();
            ObjPenalCodeModel.Keywords = Objkey_word;
            return View(ObjPenalCodeModel);
        }

        [HttpPost]
        public ActionResult SaveFilters(FormCollection frm)
        {
            string strResult = string.Empty;
            BALGeneral ObjBALGeneral = new BALGeneral();
            string tem_keyword = frm.GetValue("keywords").AttemptedValue;
            int filter_id = 1;
            var ObjColl = ObjBALGeneral.SaveFilters_keywords(tem_keyword, filter_id);
            if (ObjColl == 1)
            {
                strResult = "<span class='successmsg'>Filters successfully saved.</span>";
            }
            else
            {
                strResult = "<span class='successmsg'>Filters successfully updated.</span>";
            }
            // }
            return Json(new { Message = strResult });
        }
        public ActionResult Forms()
        {
            MyFormModel ObjModel = new MyFormModel();
            List<MyFormModelColl> MyFormModel = new List<MyFormModelColl>();
            var Collection = GetMyFormCollection().ToList();
            if (Collection.Count > 0)
            {
                for (int i = 0; i < Collection.Count; i++)
                {

                    MyFormModel.Add(new MyFormModelColl() { pkformId = Collection.ElementAt(i).pkformId, Title = Collection.ElementAt(i).Title, Path = Collection.ElementAt(i).Path, Description = Collection.ElementAt(i).Description, ModifiedDate = Convert.ToDateTime(Collection.ElementAt(i).ModifiedDate), ModifiedLoggedId = Collection.ElementAt(i).ModifiedLoggedId != null ? (Guid)Collection.ElementAt(i).ModifiedLoggedId : Guid.Empty });

                }
            }

            ObjModel.Collection = MyFormModel;
            //get customform data list
            BALCustomForm ObjBALCustomForm = new BALCustomForm();
            List<CustomFormModelColl> CustomFormModel = new List<CustomFormModelColl>();
            var CustomFormCollection = ObjBALCustomForm.GetCustomFormCollection().ToList();
            if (CustomFormCollection.Count > 0)
            {
                for (int index = 0; index < CustomFormCollection.Count; index++)
                {
                    CustomFormModel.Add(new CustomFormModelColl
                    {
                        PkCustomFormId = CustomFormCollection.ElementAt(index).PkCustomFormId,
                        Title = CustomFormCollection.ElementAt(index).Title,
                        Description = CustomFormCollection.ElementAt(index).Description,
                        Path = CustomFormCollection.ElementAt(index).Path,
                        ModifiedDate = CustomFormCollection.ElementAt(index).ModifiedDate,
                        ModifiedLoggedId = CustomFormCollection.ElementAt(index).ModifiedLoggedId == null ? Guid.Empty : (Guid)CustomFormCollection.ElementAt(index).ModifiedLoggedId
                    });
                }
            }
            ObjModel.CustomFormCollection = CustomFormModel;
            if (TempData["SaveSuccess"] != null)
            {
                ObjModel.Message = "<span class='successmsg'>Forms updated successfully.</span>"; ;

            }
            else
            {
                ObjModel.Message = "";

            }

            return View(ObjModel);

        }


        /// <summary>
        /// update my form or custom form
        /// </summary>
        /// <param name="id"></param>
        /// <param name="title"></param>
        /// <param name="descritption"></param>
        /// <param name="action"></param>
        /// <returns></returns>

        public ActionResult EditFormOrCustomForm(int id, string title, string descritption, string action)
        {
            int Success = 0;
            string Message = string.Empty;
            MembershipUser ObjMembershipUser = Membership.GetUser(HttpContext.User.Identity.Name);
            Guid UserId = Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
            //BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            tblmyform Objtblmyform = new tblmyform();
            tblCustomForm ObjTblCustomForm = new tblCustomForm();

            if (action == "EditF")
            {
                Objtblmyform.pkformId = id;
                Objtblmyform.Title = title;
                Objtblmyform.ModifiedLoggedId = UserId;
                Objtblmyform.Description = descritption;
                //Objtblmyform.Path = Path;
                Objtblmyform.ModifiedDate = DateTime.Now;
                Success = UpdateForms(Objtblmyform);
                if (Success == 1)
                {
                    Message = "Forms updated successfully";
                }
                else
                {
                    Message = "Some problem occured";
                }

            }
            else if (action == "EditCF")
            {
                ObjTblCustomForm.pkCustomFormId = id;
                ObjTblCustomForm.Title = title;
                ObjTblCustomForm.ModifiedLoggedId = UserId;
                ObjTblCustomForm.Description = descritption;
                //ObjTblCustomForm.Path = CustomFormPath;
                ObjTblCustomForm.ModifiedDate = DateTime.Now;
                Success = UpdateCustomForms(ObjTblCustomForm);
                if (Success == 1)
                {
                    Message = "Custom Forms updated successfully";
                }
                else
                {
                    Message = "Some problem occured";
                }

            }
            return Json(new { Msg = Message, action = action });
        }

        /// <summary>
        /// delete my form or custom form
        /// </summary>
        /// <param name="id"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public ActionResult DeleteFormOrCustomForm(int id, string action)
        {
            int Success = 0;
            string Message = string.Empty;
            MembershipUser ObjMembershipUser = Membership.GetUser(HttpContext.User.Identity.Name);
            Guid UserId = Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
            //BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            tblmyform Objtblmyform = new tblmyform();
            tblCustomForm ObjTblCustomForm = new tblCustomForm();

            if (action == "DeleteF")
            {
                Objtblmyform.pkformId = id;
                //Objtblmyform.Title = title;
                Objtblmyform.ModifiedLoggedId = UserId;
                //Objtblmyform.Description = descritption;
                //Objtblmyform.Path = Path;
                Objtblmyform.ModifiedDate = DateTime.Now;
                Success = DeleteForms(Objtblmyform);
                if (Success == 1)
                {
                    Message = "Forms delete successfully";
                }
                else
                {
                    Message = "Some problem occured";
                }

            }
            else if (action == "DeleteCF")
            {
                ObjTblCustomForm.pkCustomFormId = id;
                //ObjTblCustomForm.Title = title;
                ObjTblCustomForm.ModifiedLoggedId = UserId;
                //ObjTblCustomForm.Description = descritption;
                //ObjTblCustomForm.Path = CustomFormPath;
                ObjTblCustomForm.ModifiedDate = DateTime.Now;
                Success = DeleteCustomForms(ObjTblCustomForm);
                if (Success == 1)
                {
                    Message = "Custom Form Delete successfully";
                }
                else
                {
                    Message = "Some problem occured";
                }

            }
            return Json(new { Msg = Message, action = action });
        }

        public List<MyFormColl> GetMyFormCollection()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                List<MyFormColl> ObjData = new List<MyFormColl>();
                ObjData = (from u in ObjDALDataContext.tblmyforms
                           where u.IsActive == true
                           select new MyFormColl
                           {
                               pkformId = u.pkformId,
                               Title = u.Title,
                               Description = u.Description,
                               Path = u.Path,
                               ModifiedLoggedId = u.ModifiedLoggedId != null ? (Guid)u.ModifiedLoggedId : Guid.Empty,
                               ModifiedDate = u.ModifiedDate
                           }).ToList();
                return ObjData.ToList();
            }


        }
        public int UpdateForms(tblmyform Objtblmyform)
        {
            int Result = 0;
            tblmyform ObjDoc = new tblmyform();
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {

                    ObjDoc = ObjDALDataContext.tblmyforms.Where(d => d.pkformId == Objtblmyform.pkformId).FirstOrDefault();
                    if (ObjDoc != null)
                    {
                        ObjDoc.Title = Objtblmyform.Title;
                        ObjDoc.Description = Objtblmyform.Description;
                        ObjDoc.ModifiedDate = Objtblmyform.ModifiedDate;
                        ObjDoc.ModifiedLoggedId = Objtblmyform.ModifiedLoggedId;
                        ObjDALDataContext.SubmitChanges();
                        Result = 1;
                    }


                }
                catch (Exception)
                {

                    Result = 0;
                }
            }
            return Result;
        }

        public int UpdateCustomForms(tblCustomForm Objtblmyform)
        {
            int Result = 0;
            tblCustomForm ObjDoc = new tblCustomForm();
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {

                    ObjDoc = ObjDALDataContext.tblCustomForms.Where(d => d.pkCustomFormId == Objtblmyform.pkCustomFormId).FirstOrDefault();
                    if (ObjDoc != null)
                    {
                        ObjDoc.Title = Objtblmyform.Title;
                        ObjDoc.Description = Objtblmyform.Description;
                        ObjDoc.ModifiedDate = Objtblmyform.ModifiedDate;
                        ObjDoc.ModifiedLoggedId = Objtblmyform.ModifiedLoggedId;
                        ObjDALDataContext.SubmitChanges();
                        Result = 1;
                    }


                }
                catch (Exception)
                {

                    Result = 0;
                }
            }
            return Result;
        }

        public int DeleteCustomForms(tblCustomForm Objtblmyform)
        {
            int Result = 0;
            tblCustomForm ObjDoc = new tblCustomForm();
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {

                    ObjDoc = ObjDALDataContext.tblCustomForms.Where(d => d.pkCustomFormId == Objtblmyform.pkCustomFormId).FirstOrDefault();
                    if (ObjDoc != null)
                    {
                        ObjDoc.IsActive = false;
                        ObjDoc.ModifiedDate = Objtblmyform.ModifiedDate;
                        ObjDoc.ModifiedLoggedId = Objtblmyform.ModifiedLoggedId;
                        ObjDALDataContext.SubmitChanges();
                        Result = 1;
                    }


                }
                catch (Exception)
                {

                    Result = 0;
                }
            }
            return Result;
        }

        public int DeleteForms(tblmyform Objtblmyform)
        {
            int Result = 0;
            tblmyform ObjDoc = new tblmyform();
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {

                    ObjDoc = ObjDALDataContext.tblmyforms.Where(d => d.pkformId == Objtblmyform.pkformId).FirstOrDefault();
                    if (ObjDoc != null)
                    {
                        ObjDoc.IsActive = false;
                        ObjDoc.ModifiedDate = Objtblmyform.ModifiedDate;
                        ObjDoc.ModifiedLoggedId = Objtblmyform.ModifiedLoggedId;
                        ObjDALDataContext.SubmitChanges();
                        Result = 1;
                    }


                }
                catch (Exception)
                {

                    Result = 0;
                }
            }
            return Result;
        }

        public ActionResult filtersBind(int page, int pageSize)
        {
            BALCompany ObjBALCompany = new BALCompany();
            List<Filters> FilterCollection = new List<Filters>();
            //int PageSize = pageSize;
            //int iPageNo = page;

            var Collection = ObjBALCompany.GetFilters();
            if (Collection.Count > 0)
            {
                for (int i = 0; i < Collection.Count(); i++)
                {
                    bool remove = false;
                    bool Emergereview = false;
                    //bool Watchlist = false;
                    string thencondition = string.Empty;
                    string thenremove = string.Empty;
                    string thenEmergereview = string.Empty;
                    string thenWatchList = string.Empty;
                    int IfCondition = 0;
                    string If_Condition = string.Empty;
                    int TotalCount = 0;
                    string IfCoN = string.Empty;
                    string Total_Count = string.Empty;
                    string Keyword = string.Empty;
                    string User_Type = string.Empty;
                    string Statename = string.Empty;
                    //string FilterId = Collection.ElementAt(i).StateId == null ? string.Empty : Collection.ElementAt(i).StateId;
                    //string Comma = ",";
                    int UserType = Collection.ElementAt(i).Users;
                    if (UserType == 1) { User_Type = "All Users"; }
                    if (UserType == 2) { User_Type = "Admin Only"; }
                    if (UserType == 3) { User_Type = "Customers Only"; }
                    TotalCount = Collection.ElementAt(i).Count == null ? 0 : int.Parse(Collection.ElementAt(i).Count.ToString());
                    IfCondition = Collection.ElementAt(i).Year == null ? 0 : int.Parse(Collection.ElementAt(i).Year.ToString());
                    remove = Collection.ElementAt(i).IsRemoveCharges == null ? false : bool.Parse(Collection.ElementAt(i).IsRemoveCharges.ToString());
                    Emergereview = Collection.ElementAt(i).IsEmergeReview == null ? false : bool.Parse(Collection.ElementAt(i).IsEmergeReview.ToString());

                    if (remove == true) { thenremove = "Remove charge<br>"; }
                    if (Emergereview == true)
                    {
                        if (string.IsNullOrEmpty(thenremove) == false) { thenEmergereview = "Send to Emerge review<br>"; }
                        else { thenEmergereview = "Send to Emerge review<br>"; }
                    }

                    thencondition = thenremove + thenEmergereview + thenWatchList;
                    if (string.IsNullOrEmpty(Collection.ElementAt(i).Keywords) == false)
                    { Keyword = "<b>Keyword:</b>" + Collection.ElementAt(i).Keywords + "<br>"; }
                    if (IfCondition != 0)
                    {
                        if (string.IsNullOrEmpty(Keyword) == false)
                        { If_Condition = "<b>Timeframe :</b>" + Collection.ElementAt(i).Year.ToString() + "<br>"; }
                        else { If_Condition = "<b>Timeframe :</b>" + Collection.ElementAt(i).Year.ToString() + "<br>"; }
                    }
                    if (TotalCount != 0)
                    {
                        if (string.IsNullOrEmpty(Keyword) == false || string.IsNullOrEmpty(If_Condition) == false)
                        { Total_Count = "<b>Count =</b>" + Collection.ElementAt(i).Count + "<br>"; }
                        else { Total_Count = "<b>Count =</b>" + Collection.ElementAt(i).Count + "<br>"; }
                    }
                    IfCoN = Keyword + If_Condition + Total_Count;
                    FilterCollection.Add(new Filters()
                    {
                        FilterName = Collection.ElementAt(i).FilterName,
                        ReportType = Collection.ElementAt(i).ReportType,
                        States = Statename,
                        Users = User_Type,
                        Packages = Collection.ElementAt(i).Packages,
                        Counties = "N/A",
                        Coundition1 = IfCoN,
                        Coundition2 = thencondition,
                        Enabled = Collection.ElementAt(i).Enabled,
                        fkFilterId = Collection.ElementAt(i).fkFilterId
                    });
                }
            }



            return Json(new
            {
                Products = FilterCollection,
                TotalCount = FilterCollection.Count()
            },
                           JsonRequestBehavior.AllowGet);

        }
        public ActionResult UpdateFilters()
        {

            BALCompany ObjBALCompany = new BALCompany();
            PenalCodeModel ObjPenalCodeModel = new PenalCodeModel();
            List<tblState> State = new List<tblState>();
            List<Product_Code> ObjProduct = new List<Product_Code>();
            var ListState = new List<StateName>();
            var CollectionStates = ObjBALCompany.GetStates();


            if (!string.IsNullOrEmpty(Request.QueryString["_a"]))         //  _c Contains Filter ID
            {
                string Filter_Id = Request.QueryString["_a"].ToString();
                var Collection = ObjBALCompany.GetFiltersAccordingFilterId(Filter_Id);
                ObjPenalCodeModel.FilterId = int.Parse(Filter_Id);
                ObjPenalCodeModel.FilterName = Collection.ElementAt(0).FilterName;
                ObjPenalCodeModel.ReportType = Collection.ElementAt(0).ReportType;
                ObjPenalCodeModel.Key_words = Collection.ElementAt(0).Keywords;
                ObjPenalCodeModel.Year = Convert.ToString(Collection.ElementAt(0).Year);
                ObjPenalCodeModel.CountMax = Convert.ToString(Collection.ElementAt(0).CountMax);
                ObjPenalCodeModel.CountMin = Convert.ToString(Collection.ElementAt(0).CountMin);
                ObjPenalCodeModel.TimeFrameFrom = Convert.ToString(Collection.ElementAt(0).TimeFrameFrom);
                ObjPenalCodeModel.TimeFrameIn = Convert.ToString(Collection.ElementAt(0).TimeFrameIn);
                ObjPenalCodeModel.IsEmergeReview = (bool)Collection.ElementAt(0).IsEmergeReview;
                ObjPenalCodeModel.IsRemoveCharges = (bool)Collection.ElementAt(0).IsRemoveCharges;
                ObjPenalCodeModel.UserType = Convert.ToString(Collection.ElementAt(0).Users);
                ObjPenalCodeModel.Packages = (bool)Collection.ElementAt(0).Packages;
                ObjPenalCodeModel.Enabled = (bool)Collection.ElementAt(0).Enabled;
                ObjPenalCodeModel.Count = Collection.ElementAt(0).Count == null ? string.Empty : Collection.ElementAt(0).Count.ToString();
                string ProductCode = Collection.ElementAt(0).ReportType == null ? string.Empty : Collection.ElementAt(0).ReportType;
                if (string.IsNullOrEmpty(ProductCode) == false)
                {
                    var CollProduct = ProductCode.Split(',');
                    if (CollProduct.Length > 0)
                    {
                        for (int m = 0; m < CollProduct.Length; m++)
                        {
                            if (string.IsNullOrEmpty(CollProduct[m]) == false)
                            { ObjProduct.Add(new Product_Code { ProductCode = CollProduct[m].ToString() }); }
                        }
                    }
                }
                string FilterId = Collection.ElementAt(0).StateId == null ? string.Empty : Collection.ElementAt(0).StateId;
                if (string.IsNullOrEmpty(FilterId) == false)
                {
                    var Coll = FilterId.Split(',');
                    if (Coll.Length > 0)
                    {
                        for (int r = 0; r < Coll.Length; r++)
                        {
                            if (string.IsNullOrEmpty(Coll[r]) == false)
                            { State.Add(new tblState { pkStateId = int.Parse(Coll[r]) }); }
                        }
                    }
                    ListState = ObjBALCompany.GetStateName(State);
                }
            }
            string StateDrop = StateDropDownList(CollectionStates, ListState);
            string CountyDrop = CountyDropDownList();
            string DataSources = DataSorcesDropdownlist(ObjProduct);
            ObjPenalCodeModel.StateDropdown = StateDrop;
            ObjPenalCodeModel.CountyDropdown = CountyDrop;
            ObjPenalCodeModel.DataSourceDropdown = DataSources;
            return View(ObjPenalCodeModel);
        }
        public string StateDropDownList(List<tblState> CollectionStates, List<StateName> ListState)
        {
            var StateDrop = new StringBuilder();
            StateDrop.Append(@"<Select id='StateDD' multiple='multiple' name='StateDD' >");
            if (CollectionStates.Count() > 0)
            {
                for (int i = 0; i < CollectionStates.Count(); i++)
                {
                    int Flag = 0;
                    if (ListState.Count() > 0)
                    {
                        for (int r = 0; r < ListState.Count(); r++)
                        {
                            if (CollectionStates.ElementAt(i).pkStateId == ListState.ElementAt(r).pkStateId) { Flag = 2; break; }
                        }
                    }
                    if (Flag == 2)
                    {
                        StateDrop.Append(@"<option value='" + CollectionStates.ElementAt(i).pkStateId + "' selected='selected'>" + CollectionStates.ElementAt(i).StateName + "</option>");
                    }
                    else
                    {
                        StateDrop.Append(@"<option value='" + CollectionStates.ElementAt(i).pkStateId + "'>" + CollectionStates.ElementAt(i).StateName + "</option>");

                    }


                }
            }
            StateDrop.Append(@"</Select>");
            return StateDrop.ToString();

        }
        public string CountyDropDownList()
        {
            var CountyDrop = new StringBuilder();
            CountyDrop.Append(@"<Select id='CountyDD' multiple='multiple' name='CountyDD'>");
            CountyDrop.Append(@"</Select>");
            return CountyDrop.ToString();

        }
        public string DataSorcesDropdownlist(List<Product_Code> ObjProduct)
        {
            BALProducts ObjBALProducts = new BALProducts();
            var Collection = ObjBALProducts.GetProducts().Where(t => t.ReportCategoryId != Convert.ToByte(9)).ToList();
            var DataSorcesDrop = new StringBuilder();
            DataSorcesDrop.Append(@"<Select id='DataSorces' multiple='multiple' name='DataSorces'>");
            if (Collection.Count() > 0)
            {
                for (int i = 0; i < Collection.Count(); i++)
                {
                    int Flag = 0;
                    if (ObjProduct.Count() > 0)
                    {
                        for (int n = 0; n < ObjProduct.Count(); n++)
                        {
                            if (Collection.ElementAt(i).ProductCode.ToLower().Trim() == ObjProduct.ElementAt(n).ProductCode.ToLower().Trim())
                            {
                                Flag = 2;
                                break;
                            }
                        }
                    }
                    if (Flag == 2)
                    {
                        DataSorcesDrop.Append(@"<option value='" + Collection.ElementAt(i).ProductCode + "' selected='selected'>" + Collection.ElementAt(i).ProductCode + "</option>");
                    }
                    else
                    {
                        DataSorcesDrop.Append(@"<option value='" + Collection.ElementAt(i).ProductCode + "'>" + Collection.ElementAt(i).ProductCode + "</option>");

                    }




                }
            }

            DataSorcesDrop.Append(@"</Select>");
            return DataSorcesDrop.ToString();

        }

        [HttpPost]
        public ActionResult Update_Filters(FormCollection ObjCollection)
        {
            string strResult = string.Empty;
            Filters_Class ObjFilters_Class = new Filters_Class();
            BALCompany ObjBALCompany = new BALCompany();
            if (string.IsNullOrEmpty(ObjCollection["FilterId"]) == false) { ObjFilters_Class.FilterId = int.Parse(ObjCollection["FilterId"]); }
            ObjFilters_Class.FilterName = ObjCollection["FilterName"];
            ObjFilters_Class.ReportType = ObjCollection["ReportType"];
            ObjFilters_Class.StateDropdown = ObjCollection["StateDD"];
            ObjFilters_Class.CountyDropdown = ObjCollection["CountyDD"];
            ObjFilters_Class.DataSourceDropdown = ObjCollection["DataSorces"] == null ? string.Empty : ObjCollection["DataSorces"];
            ObjFilters_Class.Key_words = ObjCollection["Key_words"];
            ObjFilters_Class.Year = ObjCollection["Year"];
            ObjFilters_Class.Count = ObjCollection["Count"];
            ObjFilters_Class.CountMax = ObjCollection["CountMax"];
            ObjFilters_Class.CountMin = ObjCollection["CountMin"];
            ObjFilters_Class.TimeFrameFrom = ObjCollection["TimeFrameFrom"];
            ObjFilters_Class.TimeFrameIn = ObjCollection["TimeFrameIn"];
            ObjFilters_Class.IsEmergeReview = bool.Parse(ObjCollection["Emerge_Review"]);
            ObjFilters_Class.IsRemoveCharges = bool.Parse(ObjCollection["Remove_Charges"]);
            ObjFilters_Class.Filter_Users = ObjCollection["Filter_Users"];
            ObjFilters_Class.Packages = bool.Parse(ObjCollection["Packages_Chk"]);
            ObjFilters_Class.Enabled = bool.Parse(ObjCollection["Enabled_Chk"]);
            int ObjColl = ObjBALCompany.UpdateFilters(ObjFilters_Class);

            if (ObjColl == 1)
            {
                strResult = "<span class='successmsg'>Filters successfully Updated.</span>";
            }
            else
            {
                strResult = "<span class='error'>error is occured while updating Filters.</span>";
            }

            return Json(new { Message = strResult });
        }

        /// <summary>
        /// add my form by admin only
        /// </summary>
        /// <param name="PDFfileName"></param>
        /// <param name="txtDocumentTitle"></param>
        /// <param name="formColl"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SaveFormDetails(HttpPostedFileBase PDFfileName, string txtDocumentTitle, FormCollection formColl)
        {
            //string strPagetoRedirect = string.Empty;
            int IntResult = 0;
            string Message = string.Empty;
            Guid UserId = Guid.Empty;
            //int pkCompanyId = 0;
            string Description = formColl["CustomFormDescription"];
            BALCustomForm objBalCustomForm = new BALCustomForm();
            MyFormModel ObjMyForm = new MyFormModel();
            try
            {
                if (PDFfileName != null)
                {
                    if (PDFfileName != null && PDFfileName.ContentLength > 0)
                    {
                        if (PDFfileName.FileName != string.Empty)
                        {

                            if (CheckExtensionsForCustomForm(PDFfileName))
                            {
                                //string sExt = Path.GetExtension(PDFfileName.FileName).ToLower();
                                string sFileName = PDFfileName.FileName;
                                string sFilePath = string.Empty;
                                sFilePath = HttpContext.Server.MapPath(@"~/Resources/Forms/") + sFileName;
                                string Title = string.Empty;
                                Title = Path.GetFileNameWithoutExtension(sFilePath);
                                if (System.IO.File.Exists(HttpContext.Server.MapPath(@"~\Resources\Forms\") + sFileName))
                                {
                                    try
                                    {
                                        System.IO.File.Delete(HttpContext.Server.MapPath(@"~\Resources\Forms\") + sFileName);
                                    }
                                    catch { }
                                }

                                PDFfileName.SaveAs(HttpContext.Server.MapPath(@"~\Resources\Forms\") + sFileName);

                                //get user information
                                MembershipUser Member = Membership.GetUser(User.Identity.Name);
                                if (Member != null)
                                    UserId = new Guid(Member.ProviderUserKey.ToString());
                                tblmyform Objtblmyform = new tblmyform();
                                Objtblmyform.Title = Title;
                                Objtblmyform.ModifiedLoggedId = UserId;
                                Objtblmyform.Description = Description;
                                Objtblmyform.Path = sFileName;
                                Objtblmyform.ModifiedDate = DateTime.Now;
                                Objtblmyform.IsActive = true;
                                IntResult = objBalCustomForm.InsertMyFormDetails(Objtblmyform);

                                if (IntResult == 1)
                                {
                                    Message = "My form uploaded successfully.";
                                }
                                else
                                {
                                    Message = "Occured Problem during the upload My form.";
                                }

                                //strPagetoRedirect = "Forms";
                            }
                            else
                            {
                                //strPagetoRedirect = "Forms";
                                Message = "Only .pdf/.doc files are allowed to upload.";
                            }

                        }

                    }
                }
            }
            catch
            {
                //strPagetoRedirect = "Forms";
                Message = "Some error occurred while uploading. Please try again!";
            }
            ObjMyForm.Message = Message;

            return RedirectToAction("Forms");
        }

        public bool CheckExtensionsForCustomForm(HttpPostedFileBase Document)
        {
            string Extension = string.Empty;

            if (Document.FileName != string.Empty)
            {
                Extension = Path.GetExtension(Document.FileName).ToLower();
                if (Document.ContentLength > 0)
                {
                    if (Extension == ".pdf" || Extension == ".doc")
                    {

                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
    public class MyFormColl
    {
        public int pkformId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
        public Guid ModifiedLoggedId { get; set; }
        public DateTime? ModifiedDate { get; set; }

    }
}
