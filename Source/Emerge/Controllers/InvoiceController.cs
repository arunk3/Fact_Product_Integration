﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Net.Mime;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Security;
using System.Data.Linq;
using System.Web.Security;
using Emerge.Common;
using System.Text;
using System.Configuration;
using AuthorizeNet;
using System.Web.Routing;
using Emerge.CommonClasses;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Globalization;
namespace Emerge.Controllers
{
    public class InvoiceController : Controller
    {
        int[] ids = { 1, 2, 3, 4 };
        // GET: /Invoice/
        //EMAIL SENDING FOR INVOICE DETAILS

        public ActionResult GenrateInvoiceSection(string companyid)
        {
            #region**********For all companies

            if (companyid == string.Empty)
            {
                using (InvoiceController sendalert = new InvoiceController())
                {
                    using (EmergeDALDataContext myDB = new EmergeDALDataContext())
                    {
                        tblInvoice Invobj = new tblInvoice();
                        //tblCompany Cmpobj = new tblCompany();
                        string EmailID = string.Empty;
                        DateTime dt = DateTime.Now;
                        int month;
                        int year;

                        if (dt.Month == 1)
                        {
                            month = 12;
                            year = dt.Year - 1;
                        }
                        else
                        {
                            month = dt.Month - 1;
                            year = dt.Year;
                        }

                        var emails = new List<proc_invoiceEmaildataselectionwithInvoiceResult>();


                        using (EmergeDALDataContext dbObj = new EmergeDALDataContext())
                        {
                            int ProcCompanyId = 0;
                            if (companyid != "")
                            {
                                ProcCompanyId = Convert.ToInt32(companyid);
                            }
                            emails = dbObj.proc_invoiceEmaildataselectionwithInvoice(month, year, companyid, Convert.ToInt32(ConfigurationManager.AppSettings["Enable30DaysPeriod"]), "GenrateInvoiceSection").ToList();
                        }

                        if (emails != null)
                        {
                            if (emails.Count > 0)
                            {
                                foreach (var s in emails)
                                {
                                    int countDays = myDB.tblCompanies.Where(cmp => cmp.pkCompanyId == s.pkCompanyId).Select(cmp => cmp.PaymentTerm).FirstOrDefault();
                                    if (countDays > 1)
                                    {
                                        if (s.InvoiceDate == null)
                                        {
                                            var obj = new List<proc_selectCompanyIdInvoiceListResult>();


                                            using (EmergeDALDataContext dbObj = new EmergeDALDataContext())
                                            {
                                                int ProcCompanyId = 0;
                                                if (companyid != "")
                                                {
                                                    ProcCompanyId = Convert.ToInt32(companyid);
                                                }
                                                obj = dbObj.proc_selectCompanyIdInvoiceList(year, month, Convert.ToInt32(ProcCompanyId)).ToList();
                                            }


                                            if (obj != null)
                                            {

                                                if (obj.Count > 0)
                                                {
                                                    foreach (var a in obj)
                                                    {
                                                        if (a.InvoiceDate == null)
                                                        {

                                                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller :" + " a.invoice date is null", "Quikbook");

                                                            //DateTime now = DateTime.Now;

                                                            var tblInvoice = myDB.tblInvoices.Where(x => x.pkInvoiceId == a.pkInvoiceId).FirstOrDefault();
                                                            if (tblInvoice != null)
                                                            {
                                                                DateTime date = System.DateTime.Now;
                                                                a.InvoiceDate = date;
                                                                a.InvoiceDueDate = date.AddDays(countDays);
                                                                myDB.SubmitChanges();
                                                                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller :" + " update ok", "Quikbook");
                                                            }


                                                            EmailID = s.BillingEmailId;
                                                            //bool b = sendalert.SendMailForReportReminder(Invobj, EmailID, Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceCreationEmailTemplate"]), "Invoice Created");

                                                            bool b = true;

                                                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller :" + "quickbook update ok", "Quikbook");

                                                            if (b == true)
                                                            {
                                                                return Json(new { Result = "Succeded" });
                                                            }


                                                        }
                                                    }
                                                }
                                            }


                                        }
                                        if (s.InvoiceYear == year && s.InvoiceMonth == month && s.InvoiceDate != null)
                                        {
                                            Invobj.InvoiceNumber = s.InvoiceNumber;

                                            Invobj.InvoiceDueDate = (s.InvoiceDate).Value.Add(new TimeSpan(countDays, 0, 0, 0));
                                            EmailID = s.BillingEmailId;

                                            bool b = sendalert.SendMailForReportReminder(Invobj, EmailID, Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceCreationEmailTemplate"]), "Invoice Created");
                                            b = true;
                                            if (b == true)
                                            {
                                                return Json(new { Result = "Succeded" });
                                            }


                                        }
                                    }
                                }


                            }
                        }

                        var email = (from i in myDB.tblInvoices
                                     join c in myDB.tblCompanies on i.fkCompanyId equals c.pkCompanyId
                                     where i.CreatedDateTime < System.DateTime.Now && i.InvoiceDueDate != null && ids.Contains(i.InvoiceTypeId)
                                     select new { c.BillingEmailId, c.pkCompanyId, c.PaymentTerm, i.InvoiceNumber, i.InvoiceDate, i.InvoiceDueDate }).ToList();
                        foreach (var s in email)
                        {
                            Invobj.InvoiceNumber = s.InvoiceNumber;
                            Invobj.InvoiceDate = s.InvoiceDate;
                            Invobj.InvoiceDueDate = s.InvoiceDueDate;
                            if (Invobj.InvoiceDueDate < System.DateTime.Now)
                            {
                                EmailID = s.BillingEmailId;
                                bool b = sendalert.SendMailForReportReminder(Invobj, EmailID, Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceDueDateExpiredTemplate"]), "Due Date Expired");
                                b = true;
                                if (b == true)
                                {
                                    return Json(new { Result = "Alert has been sent for Due Date" });
                                }
                            }

                        }
                        // comment on 05 nov 2015
                        var idlock = (from i in myDB.tblInvoices
                                      join c in myDB.tblCompanies on i.fkCompanyId equals c.pkCompanyId
                                      where ids.Contains(i.InvoiceTypeId)
                                      select new { i.fkCompanyId, i.InvoiceDueDate, c.MainEmailId, c.NonPaymentLockDate }).ToList();
                        foreach (var d in idlock)
                        {
                            if (Invobj.InvoiceDueDate < System.DateTime.Now.AddDays(-5))
                            {
                                tblCompany cmp = myDB.tblCompanies.Single(u => u.pkCompanyId.ToString() == d.fkCompanyId.ToString()); //To edit user that matches the Username
                                cmp.NonPaymentLockDate = System.DateTime.Now.AddDays(5);
                                myDB.SubmitChanges();

                            }
                            if (d.NonPaymentLockDate < System.DateTime.Now)
                            {
                                //mail for account lock notification
                                EmailID = d.MainEmailId;
                                bool b = sendalert.SendMailForReportReminder(Invobj, EmailID, Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceAccountLockTemplate"]), "Account Lock");
                                b = true;
                                if (b == true)
                                {
                                    return Json(new { Result = "Account Lock Email sent successfully" });
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            #region**********For all companies
            if (companyid != string.Empty)
            {

                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller : company id is" + companyid, "Quikbook");
                int selectedcompanyId = Convert.ToInt32(companyid);


                using (InvoiceController sendalert = new InvoiceController())
                {
                    using (EmergeDALDataContext myDB = new EmergeDALDataContext())
                    {
                        tblInvoice Invobj = new tblInvoice();
                       // tblCompany Cmpobj = new tblCompany();
                        string EmailID = string.Empty;
                        DateTime dt = DateTime.Now;
                        int month;
                        int year;
                        int countDays = myDB.tblCompanies.Where(cmp => cmp.pkCompanyId == selectedcompanyId).Select(cmp => cmp.PaymentTerm).FirstOrDefault();
                        VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller : countDays  is" + countDays, "Quikbook");
                        if (countDays > 1)
                        {
                            if (dt.Month == 1)
                            {
                                month = 12;
                                year = dt.Year - 1;
                            }
                            else
                            {
                                month = dt.Month - 1;
                                year = dt.Year;
                            }

                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller in email id ", "Quikbook");


                            var emails = new List<proc_invoiceEmaildataselectionwithInvoiceResult>();


                            using (EmergeDALDataContext dbObj = new EmergeDALDataContext())
                            {
                                int ProcCompanyId = 0;
                                if (companyid != "")
                                {
                                    ProcCompanyId = Convert.ToInt32(companyid);
                                }

                                emails = dbObj.proc_invoiceEmaildataselectionwithInvoice(month, year, companyid, Convert.ToInt32(ConfigurationManager.AppSettings["Enable30DaysPeriod"]), "GenrateInvoiceSection").ToList();
                            }




                            if (emails != null)
                            {
                                if (emails.Count > 0)
                                {
                                    foreach (var s in emails)
                                    {
                                        VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller : " + "data in ", "Quikbook");
                                        if (s.InvoiceDate == null)
                                        {
                                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller  s.InvoiceDate: " + s.InvoiceDate, "Quikbook");
                                            var obj = new List<proc_selectCompanyIdInvoiceListResult>();

                                            using (EmergeDALDataContext dbObj = new EmergeDALDataContext())
                                            {

                                                obj = dbObj.proc_selectCompanyIdInvoiceList(year, month, Convert.ToInt32(selectedcompanyId)).ToList();
                                            }

                                            if (obj != null)
                                            {
                                                if (obj.Count > 0)
                                                {
                                                    foreach (var a in obj)
                                                    {
                                                        if (a.InvoiceDate == null)
                                                        {
                                                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller : a.InvoiceDate == null", "Quikbook");

                                                            //DateTime now = DateTime.Now;


                                                            DateTime date = System.DateTime.Now;

                                                            var tblInvoice = myDB.tblInvoices.Where(x => x.pkInvoiceId == a.pkInvoiceId).FirstOrDefault();
                                                            if (tblInvoice != null)
                                                            {
                                                                tblInvoice.InvoiceDate = date;
                                                                tblInvoice.InvoiceDueDate = date.AddDays(countDays);
                                                                myDB.SubmitChanges();
                                                                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller :" + " update ok", "Quikbook");
                                                            }

                                                            EmailID = s.BillingEmailId;

                                                            bool b = true;
                                                            if (b == true)
                                                            {
                                                                return Json(new { Result = "Succeded" });
                                                            }

                                                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller : success", "Quikbook");
                                                        }
                                                    }
                                                }
                                            }

                                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller : " + obj, "Quikbook");

                                        }

                                    }
                                }
                            }
                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller : " + emails, "Quikbook");

                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller in done email   ", "Quikbook");

                        }
                    }
                }
            }
            #endregion
            return Json(new { Result = "Blank" });
        }


        public ActionResult SendInvoiceDaily(string companyid)
        {
            #region**********For all companies
            if (companyid == string.Empty)
            {



                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller :" + "True", "Quikbook");

                using (InvoiceController sendalert = new InvoiceController())
                {
                    using (EmergeDALDataContext myDB = new EmergeDALDataContext())
                    {
                        tblInvoice Invobj = new tblInvoice();
                        //tblCompany Cmpobj = new tblCompany();
                        string EmailID = string.Empty;
                        DateTime dt = DateTime.Now;
                        int month;
                        int year;

                        if (dt.Month == 1)
                        {
                            month = 12;
                            year = dt.Year - 1;
                        }
                        else
                        {
                            month = dt.Month - 1;
                            year = dt.Year;
                        }

                        var emails = new List<proc_invoiceEmaildataselectionwithInvoiceResult>();


                        using (EmergeDALDataContext dbObj = new EmergeDALDataContext())
                        {
                            int ProcCompanyId = 0;
                            if (companyid != "")
                            {
                                ProcCompanyId = Convert.ToInt32(companyid);
                            }

                            emails = dbObj.proc_invoiceEmaildataselectionwithInvoice(month, year, companyid, Convert.ToInt32(ConfigurationManager.AppSettings["Enable30DaysPeriod"]), "SendInvoiceDaily").ToList();
                        }



                        //changes by ak

                        if (emails != null)
                        {
                            if (emails.Count > 0)
                            {
                                foreach (var s in emails)
                                {
                                    int countDays = myDB.tblCompanies.Where(cmp => cmp.pkCompanyId == s.pkCompanyId).Select(cmp => cmp.PaymentTerm).FirstOrDefault();
                                    if (countDays > 1)
                                    {
                                        if (s.InvoiceDate == null)
                                        {

                                            var obj = new List<proc_selectCompanyIdInvoiceListResult>();


                                            using (EmergeDALDataContext dbObj = new EmergeDALDataContext())
                                            {
                                                int ProcCompanyId = 0;
                                                if (companyid != "")
                                                {
                                                    ProcCompanyId = Convert.ToInt32(companyid);
                                                }

                                                obj = dbObj.proc_selectCompanyIdInvoiceList(year, month, Convert.ToInt32(ProcCompanyId)).ToList();
                                            }

                                            if (obj != null)
                                            {
                                                if (obj.Count > 0)
                                                {
                                                    foreach (var a in obj)
                                                    {
                                                        if (a.InvoiceDate == null)
                                                        {

                                                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller :" + " a.invoice date is null", "Quikbook");

                                                           // DateTime now = DateTime.Now;

                                                            DateTime date = System.DateTime.Now;
                                                            a.InvoiceDate = date;

                                                            a.InvoiceDueDate = date.AddDays(countDays);
                                                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller :" + " update ok", "Quikbook");


                                                            EmailID = s.BillingEmailId;
                                                            bool b = sendalert.SendMailForReportReminder(Invobj, EmailID, Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceCreationEmailTemplate"]), "Invoice Created");
                                                            if (b == true)
                                                            {
                                                                return Json(new { Result = "Succeded" });
                                                            }

                                                            QuickBookRequest objQCRequest = new QuickBookRequest();
                                                            objQCRequest.InvoiceID = s.pkInvoiceId;
                                                            objQCRequest.CompanyName = s.CompanyName;
                                                            objQCRequest.CompanyID = s.pkCompanyId;
                                                            objQCRequest.Type = "AddInvoice";
                                                            objQCRequest.InvoiceMonth = Convert.ToInt32(s.InvoiceMonth);
                                                            objQCRequest.InvoiceYear = Convert.ToInt32(s.InvoiceYear);
                                                            myDB.QuickBookRequests.InsertOnSubmit(objQCRequest);
                                                            myDB.SubmitChanges();
                                                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller :" + "quickbook update ok", "Quikbook");
                                                        }
                                                    }

                                                }
                                            }


                                        }
                                        if (s.InvoiceYear == year && s.InvoiceMonth == month && s.InvoiceDate != null)
                                        {
                                            Invobj.InvoiceNumber = s.InvoiceNumber;
                                            Invobj.InvoiceDate = s.InvoiceDate;

                                            EmailID = s.BillingEmailId;
                                            bool b = sendalert.SendMailForReportReminder(Invobj, EmailID, Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceCreationEmailTemplate"]), "Invoice Created");
                                            if (b == true)
                                            {
                                                return Json(new { Result = "Succeded" });
                                            }


                                        }
                                    }
                                }
                            }
                        }

                        var email = (from i in myDB.tblInvoices
                                     join c in myDB.tblCompanies on i.fkCompanyId equals c.pkCompanyId
                                     where i.CreatedDateTime < System.DateTime.Now && i.InvoiceDueDate != null && ids.Contains(i.InvoiceTypeId)
                                     select new { c.BillingEmailId, c.pkCompanyId, c.PaymentTerm, i.InvoiceNumber, i.InvoiceDate, i.InvoiceDueDate }).ToList();
                        foreach (var s in email)
                        {
                            Invobj.InvoiceNumber = s.InvoiceNumber;
                            Invobj.InvoiceDate = s.InvoiceDate;
                            Invobj.InvoiceDueDate = s.InvoiceDueDate;
                            if (Invobj.InvoiceDueDate < System.DateTime.Now)
                            {
                                EmailID = s.BillingEmailId;
                                bool b = sendalert.SendMailForReportReminder(Invobj, EmailID, Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceDueDateExpiredTemplate"]), "Due Date Expired");

                                if (b == true)
                                {
                                    return Json(new { Result = "Alert has been sent for Due Date" });
                                }
                            }

                        }
                        var idlock = (from i in myDB.tblInvoices
                                      join c in myDB.tblCompanies on i.fkCompanyId equals c.pkCompanyId
                                      where ids.Contains(i.InvoiceTypeId)
                                      select new { i.fkCompanyId, i.InvoiceDueDate, c.MainEmailId, c.NonPaymentLockDate }).ToList();
                        foreach (var d in idlock)
                        {
                            if (Invobj.InvoiceDueDate < System.DateTime.Now.AddDays(-5))
                            {
                                tblCompany cmp = myDB.tblCompanies.Single(u => u.pkCompanyId.ToString() == d.fkCompanyId.ToString()); //To edit user that matches the Username
                                cmp.NonPaymentLockDate = System.DateTime.Now.AddDays(5);
                                myDB.SubmitChanges();

                            }
                            if (d.NonPaymentLockDate < System.DateTime.Now)
                            {
                                //mail for account lock notification
                                EmailID = d.MainEmailId;
                                bool b = sendalert.SendMailForReportReminder(Invobj, EmailID, Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceAccountLockTemplate"]), "Account Lock");
                                if (b == true)
                                {
                                    return Json(new { Result = "Account Lock Email sent successfully" });
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            #region**********For all companies
            if (companyid != string.Empty)
            {

                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller : company id is" + companyid, "Quikbook");
                int selectedcompanyId = Convert.ToInt32(companyid);


                using (InvoiceController sendalert = new InvoiceController())
                {
                    using (EmergeDALDataContext myDB = new EmergeDALDataContext())
                    {
                        tblInvoice Invobj = new tblInvoice();
                        //tblCompany Cmpobj = new tblCompany();
                        string EmailID = string.Empty;
                        DateTime dt = DateTime.Now;
                        int month;
                        int year;
                        int countDays = myDB.tblCompanies.Where(cmp => cmp.pkCompanyId == selectedcompanyId).Select(cmp => cmp.PaymentTerm).FirstOrDefault();
                        VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller : countDays  is" + countDays, "Quikbook");
                        if (countDays > 1)
                        {
                            if (dt.Month == 1)
                            {
                                month = 12;
                                year = dt.Year - 1;
                            }
                            else
                            {
                                month = dt.Month - 1;
                                year = dt.Year;
                            }
                            //service to run on 10th of each month to send invoice alert
                            //query to get emails id from tblcompany and other fields including Emails from tblinvoice table 

                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller in email id ", "Quikbook");

                            var emails = new List<proc_invoiceEmaildataselectionwithInvoiceResult>();


                            using (EmergeDALDataContext dbObj = new EmergeDALDataContext())
                            {
                                int ProcCompanyId = 0;
                                if (companyid != "")
                                {
                                    ProcCompanyId = Convert.ToInt32(companyid);
                                }

                                emails = dbObj.proc_invoiceEmaildataselectionwithInvoice(month, year, companyid, Convert.ToInt32(ConfigurationManager.AppSettings["Enable30DaysPeriod"]), "SendInvoiceDaily").ToList();
                            }



                            //changes by ak

                            if (emails != null)
                            {
                                if (emails.Count > 0)
                                {

                                    foreach (var s in emails)
                                    {
                                        VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller : " + "data in ", "Quikbook");
                                        if (s.InvoiceDate == null)
                                        {
                                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller  s.InvoiceDate: " + s.InvoiceDate, "Quikbook");
                                            //var obj = myDB.tblInvoices.Where(u => u.fkCompanyId.ToString() == s.pkCompanyId.ToString() && ids.Contains(u.InvoiceTypeId) && u.fkCompanyId.Equals(selectedcompanyId) && u.InvoiceMonth.Equals(month) && u.InvoiceYear.Equals(year)).ToList(); //To edit user that matches the Username


                                            var obj = new List<proc_selectCompanyIdInvoiceListResult>();

                                            using (EmergeDALDataContext dbObj = new EmergeDALDataContext())
                                            {
                                                int ProcCompanyId = 0;
                                                if (companyid != "")
                                                {
                                                    ProcCompanyId = Convert.ToInt32(companyid);
                                                }

                                                obj = dbObj.proc_selectCompanyIdInvoiceList(year, month, Convert.ToInt32(ProcCompanyId)).ToList();
                                            }

                                            if (obj != null)
                                            {
                                                if (obj.Count > 0)
                                                {
                                                    foreach (var a in obj)
                                                    {
                                                        if (a.InvoiceDate == null)
                                                        {
                                                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller : a.InvoiceDate == null", "Quikbook");

                                                            //DateTime now = DateTime.Now;
                                                            DateTime date = System.DateTime.Now;
                                                            a.InvoiceDate = date;
                                                            a.InvoiceDueDate = date.AddDays(countDays);
                                                            QuickBookRequest objQCRequest = new QuickBookRequest();
                                                            objQCRequest.InvoiceID = s.pkInvoiceId;
                                                            objQCRequest.CompanyName = s.CompanyName;
                                                            objQCRequest.CompanyID = s.pkCompanyId;
                                                            objQCRequest.Type = "AddInvoice";
                                                            objQCRequest.InvoiceMonth = Convert.ToInt32(s.InvoiceMonth); ;
                                                            objQCRequest.InvoiceYear = Convert.ToInt32(s.InvoiceYear);

                                                            myDB.QuickBookRequests.InsertOnSubmit(objQCRequest);
                                                            // myDB.SubmitChanges();


                                                            EmailID = s.BillingEmailId;
                                                            bool b = sendalert.SendMailForReportReminder(Invobj, EmailID, Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceCreationEmailTemplate"]), "Invoice Created");
                                                            if (b == true)
                                                            {
                                                                return Json(new { Result = "Succeded" });
                                                            }

                                                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller : success", "Quikbook");
                                                        }
                                                    }

                                                }
                                            }


                                        }
                                        if (s.InvoiceYear == year && s.InvoiceMonth == month && s.InvoiceDate != null)
                                        {
                                            Invobj.InvoiceNumber = s.InvoiceNumber;
                                            //Invobj.InvoiceDate = s.InvoiceDate;
                                            Invobj.InvoiceDueDate = (s.InvoiceDate).Value.Add(new TimeSpan(countDays, 0, 0, 0));
                                            EmailID = s.BillingEmailId;
                                            bool b = sendalert.SendMailForReportReminder(Invobj, EmailID, Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceCreationEmailTemplate"]), "Invoice Created");
                                            if (b == true)
                                            {
                                                return Json(new { Result = "Succeded" });
                                            }
                                        }
                                    }
                                }
                            }
                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller : " + emails, "Quikbook");

                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller in done email   ", "Quikbook");

                        }
                        //SENDING due date EMAIL reminder for invoices
                        //query for due date reminder
                        var email = (from i in myDB.tblInvoices
                                     join c in myDB.tblCompanies on i.fkCompanyId equals c.pkCompanyId
                                     where i.CreatedDateTime < System.DateTime.Now && i.InvoiceDueDate != null && ids.Contains(i.InvoiceTypeId) && i.fkCompanyId.Equals(selectedcompanyId)
                                     select new { c.BillingEmailId, c.pkCompanyId, c.PaymentTerm, i.InvoiceNumber, i.InvoiceDate, i.InvoiceDueDate }).ToList();
                        foreach (var s in email)
                        {
                            Invobj.InvoiceNumber = s.InvoiceNumber;
                            Invobj.InvoiceDate = s.InvoiceDate;
                            Invobj.InvoiceDueDate = s.InvoiceDueDate;
                            if (Invobj.InvoiceDueDate < System.DateTime.Now)
                            {
                                EmailID = s.BillingEmailId;
                                bool b = sendalert.SendMailForReportReminder(Invobj, EmailID, Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceDueDateExpiredTemplate"]), "Due Date Expired");

                                if (b == true)
                                {
                                    return Json(new { Result = "Due date alert sent" });
                                }
                            }

                        }
                        var idlock = (from i in myDB.tblInvoices
                                      join c in myDB.tblCompanies on i.fkCompanyId equals c.pkCompanyId
                                      where ids.Contains(i.InvoiceTypeId) && i.fkCompanyId.Equals(selectedcompanyId)
                                      select new { i.fkCompanyId, i.InvoiceDueDate, c.BillingEmailId, c.NonPaymentLockDate }).ToList();
                        foreach (var d in idlock)
                        {
                            if (Invobj.InvoiceDueDate < System.DateTime.Now.AddDays(-5))
                            {
                                tblCompany cmp = myDB.tblCompanies.Single(u => u.pkCompanyId.ToString() == d.fkCompanyId.ToString()); //To edit user that matches the Username
                                cmp.NonPaymentLockDate = System.DateTime.Now.AddDays(5);
                                myDB.SubmitChanges();

                            }
                            if (d.NonPaymentLockDate < System.DateTime.Now)
                            {
                                //mail for account lock notification
                                EmailID = d.BillingEmailId;
                                bool b = sendalert.SendMailForReportReminder(Invobj, EmailID, Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceAccountLockTemplate"]), "Account Lock");
                                if (b == true)
                                {
                                    return Json(new { Result = "Account Lock Email sent successfully" });
                                }
                            }
                        }
                    }
                }
            }
            #endregion
            return Json(new { Result = "Blank" });
        }

        public ActionResult seprateSendInvoiceDaily(string companyid)
        {
            #region**********For all companies
            if (companyid == string.Empty)
            {
                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller :" + "True", "Quikbook");

                using (InvoiceController sendalert = new InvoiceController())
                {
                    using (EmergeDALDataContext myDB = new EmergeDALDataContext())
                    {
                        tblInvoice Invobj = new tblInvoice();
                        //tblCompany Cmpobj = new tblCompany();
                        string EmailID = string.Empty;
                        DateTime dt = DateTime.Now;
                        int month;
                        int year;

                        if (dt.Month == 1)
                        {
                            month = 12;
                            year = dt.Year - 1;
                        }
                        else
                        {
                            month = dt.Month - 1;
                            year = dt.Year;
                        }
                        //service to run on 10th of each month to send invoice alert
                        //query to get emails id from tblcompany and other fields including Emails from tblinvoice table 
                        var emails = (from i in myDB.tblInvoices
                                      join c in myDB.tblCompanies on i.fkCompanyId equals c.pkCompanyId
                                      where (i.InvoiceDate == null || (i.InvoiceMonth == month && i.InvoiceYear == year)) && ids.Contains(i.InvoiceTypeId)
                                      select new { c.BillingEmailId, c.PaymentTerm, c.pkCompanyId, i.InvoiceDate, i.InvoiceYear, i.InvoiceMonth, i.InvoiceDueDate, i.InvoiceNumber, i.pkInvoiceId, c.CompanyName }).ToList();



                        //changes by ak



                        foreach (var s in emails)
                        {
                            int countDays = myDB.tblCompanies.Where(cmp => cmp.pkCompanyId == s.pkCompanyId).Select(cmp => cmp.PaymentTerm).FirstOrDefault();
                            if (countDays > 1)
                            {
                                if (s.InvoiceDate == null)
                                {
                                    var obj = myDB.tblInvoices.Where(u => u.fkCompanyId.ToString() == s.pkCompanyId.ToString() && ids.Contains(u.InvoiceTypeId)).ToList(); //To edit user that matches the Username
                                    foreach (var a in obj)
                                    {
                                        if (a.InvoiceDate == null)
                                        {

                                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller :" + " a.invoice date is null", "Quikbook");

                                           // DateTime now = DateTime.Now;

                                            // on behalf of ks
                                            DateTime date = System.DateTime.Now;
                                            a.InvoiceDate = date;
                                            //a.InvoiceDueDate = date.Add(new TimeSpan(countDays, 0, 0, 0));

                                            a.InvoiceDueDate = date.AddDays(countDays);

                                            // myDB.SubmitChanges();
                                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller :" + " update ok", "Quikbook");


                                            EmailID = s.BillingEmailId;
                                            bool b = sendalert.SendMailForReportReminder(Invobj, EmailID, Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceCreationEmailTemplate"]), "Invoice Created");
                                            if (b == true)
                                            {
                                                return Json(new { Result = "Succeded" });
                                            }

                                            QuickBookRequest objQCRequest = new QuickBookRequest();
                                            objQCRequest.InvoiceID = s.pkInvoiceId;
                                            objQCRequest.CompanyName = s.CompanyName;
                                            objQCRequest.CompanyID = s.pkCompanyId;
                                            objQCRequest.Type = "AddInvoice";
                                            objQCRequest.InvoiceMonth = Convert.ToInt32(s.InvoiceMonth);
                                            objQCRequest.InvoiceYear = Convert.ToInt32(s.InvoiceYear);
                                            myDB.QuickBookRequests.InsertOnSubmit(objQCRequest);
                                            // myDB.SubmitChanges();
                                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller :" + "quickbook update ok", "Quikbook");
                                        }
                                    }
                                }
                                if (s.InvoiceYear == year && s.InvoiceMonth == month && s.InvoiceDate != null)
                                {
                                    Invobj.InvoiceNumber = s.InvoiceNumber;
                                    //Invobj.InvoiceDate = s.InvoiceDate;
                                    Invobj.InvoiceDueDate = (s.InvoiceDate).Value.Add(new TimeSpan(countDays, 0, 0, 0));
                                    EmailID = s.BillingEmailId;
                                    myDB.SubmitChanges();
                                    bool b = sendalert.SendMailForReportReminder(Invobj, EmailID, Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceCreationEmailTemplate"]), "Invoice Created");
                                    b = true;
                                    if (b == true)
                                    {
                                        return Json(new { Result = "Succeded" });
                                    }


                                }
                            }
                        }
                        //SENDING due date EMAIL reminder for invoices
                        //query for due date reminder
                        var email = (from i in myDB.tblInvoices
                                     join c in myDB.tblCompanies on i.fkCompanyId equals c.pkCompanyId
                                     where i.CreatedDateTime < System.DateTime.Now && i.InvoiceDueDate != null && ids.Contains(i.InvoiceTypeId)
                                     select new { c.BillingEmailId, c.pkCompanyId, c.PaymentTerm, i.InvoiceNumber, i.InvoiceDate, i.InvoiceDueDate }).ToList();
                        foreach (var s in email)
                        {
                            Invobj.InvoiceNumber = s.InvoiceNumber;
                            Invobj.InvoiceDate = s.InvoiceDate;
                            Invobj.InvoiceDueDate = s.InvoiceDueDate;
                            if (Invobj.InvoiceDueDate < System.DateTime.Now)
                            {
                                EmailID = s.BillingEmailId;
                                bool b = sendalert.SendMailForReportReminder(Invobj, EmailID, Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceDueDateExpiredTemplate"]), "Due Date Expired");

                                if (b == true)
                                {
                                    return Json(new { Result = "Alert has been sent for Due Date" });
                                }
                            }

                        }
                        var idlock = (from i in myDB.tblInvoices
                                      join c in myDB.tblCompanies on i.fkCompanyId equals c.pkCompanyId
                                      where ids.Contains(i.InvoiceTypeId)
                                      select new { i.fkCompanyId, i.InvoiceDueDate, c.MainEmailId, c.NonPaymentLockDate }).ToList();
                        foreach (var d in idlock)
                        {
                            if (Invobj.InvoiceDueDate < System.DateTime.Now.AddDays(-5))
                            {
                                tblCompany cmp = myDB.tblCompanies.Single(u => u.pkCompanyId.ToString() == d.fkCompanyId.ToString()); //To edit user that matches the Username
                                cmp.NonPaymentLockDate = System.DateTime.Now.AddDays(5);
                                myDB.SubmitChanges();

                            }
                            if (d.NonPaymentLockDate < System.DateTime.Now)
                            {
                                //mail for account lock notification
                                EmailID = d.MainEmailId;
                                bool b = sendalert.SendMailForReportReminder(Invobj, EmailID, Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceAccountLockTemplate"]), "Account Lock");
                                if (b == true)
                                {
                                    return Json(new { Result = "Account Lock Email sent successfully" });
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            #region**********For all companies
            if (companyid != string.Empty)
            {

                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller : company id is" + companyid, "Quikbook");
                int selectedcompanyId = Convert.ToInt32(companyid);


                using (InvoiceController sendalert = new InvoiceController())
                {
                    using (EmergeDALDataContext myDB = new EmergeDALDataContext())
                    {
                        tblInvoice Invobj = new tblInvoice();
                        //tblCompany Cmpobj = new tblCompany();
                        string EmailID = string.Empty;
                        DateTime dt = DateTime.Now;
                        int month;
                        int year;
                        int countDays = myDB.tblCompanies.Where(cmp => cmp.pkCompanyId == selectedcompanyId).Select(cmp => cmp.PaymentTerm).FirstOrDefault();
                        VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller : countDays  is" + countDays, "Quikbook");
                        if (countDays > 1)
                        {
                            if (dt.Month == 1)
                            {
                                month = 12;
                                year = dt.Year - 1;
                            }
                            else
                            {
                                month = dt.Month - 1;
                                year = dt.Year;
                            }
                            //service to run on 10th of each month to send invoice alert
                            //query to get emails id from tblcompany and other fields including Emails from tblinvoice table 

                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller in email id ", "Quikbook");

                            var emails = (from i in myDB.tblInvoices
                                          join c in myDB.tblCompanies on i.fkCompanyId equals c.pkCompanyId
                                          where (i.InvoiceDate == null && (i.InvoiceMonth == month && i.InvoiceYear == year)) && ids.Contains(i.InvoiceTypeId) && i.fkCompanyId.Equals(selectedcompanyId)
                                          select new { c.BillingEmailId, c.PaymentTerm, c.pkCompanyId, i.InvoiceDate, i.InvoiceYear, i.InvoiceMonth, i.InvoiceDueDate, i.InvoiceNumber, i.pkInvoiceId, c.CompanyName }).ToList();









                            //var emails = (from i in myDB.tblInvoices
                            //              join c in myDB.tblCompanies on i.fkCompanyId equals c.pkCompanyId
                            //              where (string.IsNullOrEmpty(Convert.ToString(i.InvoiceDate)) && (i.InvoiceMonth == month && i.InvoiceYear == year)) && ids.Contains(i.InvoiceTypeId) && i.fkCompanyId.Equals(selectedcompanyId)
                            //              select new { c.BillingEmailId, c.PaymentTerm, c.pkCompanyId, i.InvoiceDate, i.InvoiceYear, i.InvoiceMonth, i.InvoiceDueDate, i.InvoiceNumber, i.pkInvoiceId, c.CompanyName }).ToList();

                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller : " + emails, "Quikbook");

                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller in done email   ", "Quikbook");
                            foreach (var s in emails)
                            {
                                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller : " + "data in ", "Quikbook");
                                if (s.InvoiceDate == null)
                                {
                                    VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller  s.InvoiceDate: " + s.InvoiceDate, "Quikbook");
                                    var obj = myDB.tblInvoices.Where(u => u.fkCompanyId.ToString() == s.pkCompanyId.ToString() && ids.Contains(u.InvoiceTypeId) && u.fkCompanyId.Equals(selectedcompanyId) && u.InvoiceMonth.Equals(month) && u.InvoiceYear.Equals(year)).ToList(); //To edit user that matches the Username
                                    VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller : " + obj, "Quikbook");
                                    foreach (var a in obj)
                                    {
                                        if (a.InvoiceDate == null)
                                        {
                                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller : a.InvoiceDate == null", "Quikbook");

                                            //DateTime now = DateTime.Now;
                                            //DateTime date = new DateTime(now.Year, now.Month, 1);
                                            //a.InvoiceDate = date;
                                            //a.InvoiceDueDate = date.Add(new TimeSpan(countDays, 0, 0, 0));

                                            DateTime date = System.DateTime.Now;
                                            a.InvoiceDate = date;
                                            //a.InvoiceDueDate = date.Add(new TimeSpan(countDays, 0, 0, 0));

                                            a.InvoiceDueDate = date.AddDays(countDays);


                                            // myDB.SubmitChanges();
                                            EmailID = s.BillingEmailId;
                                            bool b = sendalert.SendMailForReportReminder(Invobj, EmailID, Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceCreationEmailTemplate"]), "Invoice Created");
                                            if (b == true)
                                            {
                                                return Json(new { Result = "Succeded" });
                                            }

                                            QuickBookRequest objQCRequest = new QuickBookRequest();
                                            objQCRequest.InvoiceID = s.pkInvoiceId;
                                            objQCRequest.CompanyName = s.CompanyName;
                                            objQCRequest.CompanyID = s.pkCompanyId;
                                            objQCRequest.Type = "AddInvoice";
                                            objQCRequest.InvoiceMonth = Convert.ToInt32(s.InvoiceMonth); ;
                                            objQCRequest.InvoiceYear = Convert.ToInt32(s.InvoiceYear);

                                            myDB.QuickBookRequests.InsertOnSubmit(objQCRequest);
                                            // myDB.SubmitChanges();

                                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n  invoice controller : success", "Quikbook");
                                        }
                                    }
                                }
                                if (s.InvoiceYear == year && s.InvoiceMonth == month && s.InvoiceDate != null)
                                {
                                    Invobj.InvoiceNumber = s.InvoiceNumber;
                                    //Invobj.InvoiceDate = s.InvoiceDate;
                                    Invobj.InvoiceDueDate = (s.InvoiceDate).Value.Add(new TimeSpan(countDays, 0, 0, 0));
                                    EmailID = s.BillingEmailId;
                                    bool b = sendalert.SendMailForReportReminder(Invobj, EmailID, Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceCreationEmailTemplate"]), "Invoice Created");
                                    b = true;
                                    if (b == true)
                                    {
                                        return Json(new { Result = "Succeded" });
                                    }
                                }
                            }
                        }
                        //SENDING due date EMAIL reminder for invoices
                        //query for due date reminder
                        var email = (from i in myDB.tblInvoices
                                     join c in myDB.tblCompanies on i.fkCompanyId equals c.pkCompanyId
                                     where i.CreatedDateTime < System.DateTime.Now && i.InvoiceDueDate != null && ids.Contains(i.InvoiceTypeId) && i.fkCompanyId.Equals(selectedcompanyId)
                                     select new { c.BillingEmailId, c.pkCompanyId, c.PaymentTerm, i.InvoiceNumber, i.InvoiceDate, i.InvoiceDueDate }).ToList();
                        foreach (var s in email)
                        {
                            Invobj.InvoiceNumber = s.InvoiceNumber;
                            Invobj.InvoiceDate = s.InvoiceDate;
                            Invobj.InvoiceDueDate = s.InvoiceDueDate;
                            if (Invobj.InvoiceDueDate < System.DateTime.Now)
                            {
                                EmailID = s.BillingEmailId;
                                bool b = sendalert.SendMailForReportReminder(Invobj, EmailID, Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceDueDateExpiredTemplate"]), "Due Date Expired");

                                if (b == true)
                                {
                                    return Json(new { Result = "Due date alert sent" });
                                }
                            }

                        }
                        var idlock = (from i in myDB.tblInvoices
                                      join c in myDB.tblCompanies on i.fkCompanyId equals c.pkCompanyId
                                      where ids.Contains(i.InvoiceTypeId) && i.fkCompanyId.Equals(selectedcompanyId)
                                      select new { i.fkCompanyId, i.InvoiceDueDate, c.BillingEmailId, c.NonPaymentLockDate }).ToList();
                        foreach (var d in idlock)
                        {
                            if (Invobj.InvoiceDueDate < System.DateTime.Now.AddDays(-5))
                            {
                                tblCompany cmp = myDB.tblCompanies.Single(u => u.pkCompanyId.ToString() == d.fkCompanyId.ToString()); //To edit user that matches the Username
                                cmp.NonPaymentLockDate = System.DateTime.Now.AddDays(5);
                                myDB.SubmitChanges();

                            }
                            if (d.NonPaymentLockDate < System.DateTime.Now)
                            {
                                //mail for account lock notification
                                EmailID = d.BillingEmailId;
                                bool b = sendalert.SendMailForReportReminder(Invobj, EmailID, Convert.ToInt32(ConfigurationManager.AppSettings["InvoiceAccountLockTemplate"]), "Account Lock");
                                if (b == true)
                                {
                                    return Json(new { Result = "Account Lock Email sent successfully" });
                                }
                            }
                        }
                    }
                }
            }
            #endregion
            return Json(new { Result = "Blank" });
        }

        public bool SendMailForReportReminder(tblInvoice tblinvoice, string Email, int template, string subject)
        {
            bool mailsent = false;
            BALGeneral objBALGeneral = new BALGeneral();
            try
            {
                string emailText = "";
                //BALGeneral ObjBALGeneral = new BALGeneral();
              //  string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;
                var emailContent = objBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, template);//Use dummy templete.

                string FromMail = ConfigurationManager.AppSettings["EmergeAdmin"].ToString();


                emailText = emailContent.TemplateContent;
                emailText = emailText.Replace("%invoicenumber%", tblinvoice.InvoiceNumber.ToString());
                emailText = emailText.Replace("%invoicedate%", Convert.ToDateTime(tblinvoice.InvoiceDate).ToString("MM-dd-yyyy", CultureInfo.CreateSpecificCulture("en-US")));
                emailText = emailText.Replace("%invoiceduedate%", Convert.ToDateTime(tblinvoice.InvoiceDueDate).ToString("MM-dd-yyyy", CultureInfo.CreateSpecificCulture("en-US")));
                emailText = emailText.Replace("%invoiceamount%", tblinvoice.InvoiceAmount.ToString());
                return Utility.SendMail(Email, ConfigurationManager.AppSettings["BillingCCName"].ToString(), string.Empty, FromMail, emailText, subject);
            }
            catch// (Exception ex)
            {
                return mailsent;
               // throw ex;
            }
            finally
            {
                objBALGeneral = null;
            }
        }


        public ActionResult DailySendMailSection(string companyid = null, string Test = null)
        {
            //InvoiceController sendalert = new InvoiceController();
            //EmergeDALDataContext myDB = new EmergeDALDataContext();
            //tblInvoice Invobj = new tblInvoice();
            //tblCompany Cmpobj = new tblCompany();
            //string EmailID = string.Empty;


            var BillingInfor = new List<Proc_BillingMailSectionWithInvoiceDetailsResult>();
            using (EmergeDALDataContext dbObj = new EmergeDALDataContext())
            {

                if (companyid != null)
                {

                    if (companyid == "")
                    {
                        companyid = "411";
                    }
                }

                else
                {
                    companyid = "0";
                }

                BillingInfor = dbObj.Proc_BillingMailSectionWithInvoiceDetails(companyid).ToList();


            }
            //string success = string.Empty;
            string ChooseTemplate = string.Empty;
            string ChooseSubject = string.Empty;
            foreach (var InvoiceMail in BillingInfor)
            {

                try
                {



                    int LeftDays = 0;
                    if (InvoiceMail.InvoiceAmount > 0)
                    {

                        DateTime currecnt = System.DateTime.Now;
                        DateTime InvoiceDueDate = Convert.ToDateTime(InvoiceMail.InvoiceDueDate);
                        TimeSpan span = currecnt.Subtract(InvoiceDueDate);
                        LeftDays = Convert.ToInt32(span.Days);

                        // level from 5 to 0 days and send invoice for paying due date 

                        if (LeftDays > 0 && LeftDays <= 3)
                        {
                            //before due days for a invoice 
                            ChooseTemplate = ConfigurationManager.AppSettings["InvoiceBeforeDueDateExpiredTemplate"];
                            ChooseSubject = "Before Due Date";
                        }
                        else if (LeftDays == 0)
                        {
                            //Today is due date day a invoice 
                            ChooseTemplate = ConfigurationManager.AppSettings["InvoiceDueDateExpiredTemplate"];
                            ChooseSubject = "Before Due Date";
                        }

                        else if (LeftDays < 0 && LeftDays >= -3)
                        {
                            ChooseTemplate = ConfigurationManager.AppSettings["InvoiceAfterDueDateExpiredTemplate"];
                            // after due date 
                            ChooseSubject = "After Due Date";
                        }

                        else if (LeftDays < -3 && LeftDays <= -4)
                        {
                            ChooseTemplate = ConfigurationManager.AppSettings["InvoiceBeforeLockTemplate"];

                            //account is locked
                            ChooseSubject = "Before Account Locking";

                        }

                        else if (LeftDays == -5)
                        {
                            //Grace Period MAIL
                            ChooseTemplate = ConfigurationManager.AppSettings["InvoiceLockTemplate"];
                            ChooseSubject = "Account is  Locked";
                            //account is locked

                        }

                        else if (LeftDays < -6 && LeftDays <= -9)
                        {
                            ChooseTemplate = ConfigurationManager.AppSettings["InvoiceAfterLockTemplate"];
                            ChooseSubject = "After Account is Locked";
                            // ACCOUNT IS LOCKED 
                        }

                        else
                        {
                            ChooseTemplate = ConfigurationManager.AppSettings["InvoiceCreationEmailTemplate"];
                            ChooseSubject = "Invoice Created";
                            // ACCOUNT IS LOCKED 
                        }


                        BALGeneral objBALGeneral = new BALGeneral();
                        try
                        {
                            string emailText = "";
                            //BALGeneral ObjBALGeneral = new BALGeneral();
                           // string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;
                            var emailContent = objBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, Convert.ToInt32(ChooseTemplate));//Use dummy templete.

                            string FromMail = ConfigurationManager.AppSettings["EmergeAdmin"].ToString();


                            emailText = emailContent.TemplateContent;
                            emailText = emailText.Replace("%invoicenumber%", InvoiceMail.InvoiceNumber.ToString());
                            emailText = emailText.Replace("%invoicedate%", Convert.ToDateTime(InvoiceMail.InvoiceDate).ToString("MM-dd-yyyy", CultureInfo.CreateSpecificCulture("en-US")));
                            emailText = emailText.Replace("%invoiceduedate%", Convert.ToDateTime(InvoiceMail.InvoiceDueDate).ToString("MM-dd-yyyy", CultureInfo.CreateSpecificCulture("en-US")));
                            emailText = emailText.Replace("%invoiceamount%", InvoiceMail.InvoiceAmount.ToString());

                            emailText = emailText.Replace("%CompanyCode%", InvoiceMail.CompanyCode.ToString());
                            emailText = emailText.Replace("%CompanyName%", InvoiceMail.CompanyName.ToString());
                            emailText = emailText.Replace("%MainContactPersonName%", InvoiceMail.MainContactPersonName.ToString());
                            emailText = emailText.Replace("%MainEmailId%", InvoiceMail.MainEmailId.ToString());
                            emailText = emailText.Replace("%BillingEmailId%", InvoiceMail.BillingEmailId.ToString());
                            emailText = emailText.Replace("%CompanyAccountNumber%", InvoiceMail.CompanyAccountNumber.ToString());


                            emailText = emailText.Replace("%PaymentTerm%", InvoiceMail.PaymentTerm.ToString());

                            emailText = emailText.Replace("%pkInvoiceId%", InvoiceMail.pkInvoiceId.ToString());

                            emailText = emailText.Replace("%InvoiceMonth%", InvoiceMail.InvoiceMonth.ToString());
                            emailText = emailText.Replace("%InvoiceYear%", InvoiceMail.InvoiceYear.ToString());
                            emailText = emailText.Replace("%InvoiceNumber%", InvoiceMail.InvoiceNumber.ToString());


                            //bool b = Utility.SendMail(InvoiceMail.BillingEmailId, ConfigurationManager.AppSettings["BillingCCName"].ToString(), string.Empty, FromMail, emailText, ChooseSubject);

                            bool b = Utility.SendMail(ConfigurationManager.AppSettings["BillingEmailId"].ToString(), ConfigurationManager.AppSettings["BillingCCName"].ToString(), string.Empty, FromMail, emailText, ChooseSubject);
                            if (b == true)
                            {

                            }
                        }
                        catch (Exception ex)
                        {

                            throw ex;
                        }
                        finally
                        {
                            objBALGeneral = null;
                        }



                    }

                }

                catch //(Exception asda)
                {

                }


            } return null;
        }



    }
}
