﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Data;
using Emerge.Services;
using System.Web.Services;
using System.Text;
using Emerge.Common;
using System.Web.Security;
using System.IO;

namespace Emerge.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public void Index()
        {

            Response.Redirect(ApplicationPath.GetApplicationPath() + "Default");
        }

        public string DisplayUserInformation()
        {
            //MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
            Guid UserId = Utility.GetUID(User.Identity.Name);// new Guid("fdd976e2-222f-47c9-b181-68e5e15ea358");
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            StringBuilder sb = new StringBuilder();
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                Response.Redirect(ApplicationPath.GetApplicationPath() + "Default");
            }
            else
            {

                try
                {
                    List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjData = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(UserId);
                    if (ObjData.Count > 0)
                    {
                        Session["pkCompanyId"] = ObjData.FirstOrDefault().pkCompanyId;
                        Session["pkLocationId"] = ObjData.FirstOrDefault().pkLocationId;
                        Session["IsDuplicateSSNReportAlertEnabled"] = ObjData.FirstOrDefault().IsDuplicateSSNReportAlertEnabled;



                        // sb.Append("<td align='center' width='7%' valign='middle' class='img_user'>");
                        // sb.Append("<div>");


                        //string PagePath = string.Empty;
                        string role = ObjData.FirstOrDefault().RoleName;
                        string username = ObjData.First().FirstName + ' ' + ObjData.First().LastName;
                        if (role.ToLower() == "supporttechnician" || role.ToLower() == "supportmanager" || role.ToLower() == "salesmanager")
                        {
                            username = ObjData.First().FirstName + "(Support)";
                        }

                        //PagePath = "../Corporate/MyAccount";





                        ViewBag.CompanyName = ObjData.First().CompanyName;
                        ViewBag.CurrentUserMailId = ObjData.First().UserName;


                        ViewBag.CurrentUserFullName = ObjData.First().FirstName + " " + ObjData.First().LastName;
                        ViewBag.CurrentUserNameMain = ObjData.First().UserName;
                        ViewBag.CurrentUserpkCompanyUserId = ObjData.First().pkCompanyUserId;




                        //   sb.Append("<div class=\"fleft right-margin10\">");

                        if (ObjData.First().UserImage == string.Empty || !System.IO.File.Exists(Server.MapPath(@"~\Resources\Upload\Images\" + ObjData.First().UserImage)))
                        {
                            Session["UserImagepathforHome"] = "../Content/themes/base/Images/NotAvail.png";

                            //  sb.Append("<a href='" + PagePath + "'><img style='cursor: pointer; height: 44px; width: 37px; alt='User Image' src=" + @Url.Content("~/Content/themes/base/Images/NotAvail.png") + " title='User Image' /></a>");


                        }
                        else
                        {
                            Session["UserImagepathforHome"] = "../Resources/Upload/Images/" + ObjData.First().UserImage;
                            //   sb.Append("<a href='" + PagePath + "'><img style='height: 44px; width: 37px;  alt='User Image' src=" + @Url.Content("~/Resources/Upload/Images/" + ObjData.First().UserImage) + " title='User Image' /></a>");

                        }
                        // sb.Append("<div style='float: right;margin-left: -14px; top: -71px; height: 3px;'>");
                        // sb.Append("<a href='" + PagePath + "'><img title='Edit Image' id='imgEdit' src=" + @Url.Content("~/Content/themes/base/Images/button_edit.png") + " style='cursor: pointer;' /></a>");
                        //  sb.Append("</div>");

                        //  sb.Append("<div");





                        sb.Append("<div class=\"fleft left-company-name\"><h6><b> " + ObjData.First().CompanyName + "</b></h6><span class=\"navbar-text\">Account #" + ObjData.First().CompanyAccountNumber + ",<br/>" + ObjData.First().CompanyLocation + " <input id='senderID' type='hidden' value=" + UserId + " /><input id='senderName' type='hidden' value='" + username + "' /></span> </div>");




                        //sb.Append("</div>");
                        //sb.Append("</td>");
                        //sb.Append("<td align='left' width='15%' valign='top'>");
                        //sb.Append("<table cellspacing='2' cellpadding='2' border='0' width='100%'><tbody><tr><td align='left'><span style='font-weight: bold;' id='nr_LblUserInfo'>");

                        //sb.Append("<span style='font-size:14px;font-family:Lucida Grande,Arial,Helvetica,sans-serif;'>" + ObjData.First().FullName + "</span><br/>" +
                        //                   "<span style='font-size:11px;font-family:Lucida Grande,Arial,Helvetica,sans-serif;'>Account #" + ObjData.First().CompanyAccountNumber + "</span><br/>" +
                        //                   "<span style='font-size:11px;font-family:Lucida Grande,Arial,Helvetica,sans-serif;'>" + ObjData.First().CompanyName + "</span><br/>" +
                        //                   "<span style='font-size:11px;font-family:Lucida Grande,Arial,Helvetica,sans-serif;' id='CurrentUserAddress'>" + ObjData.First().CompanyLocation + "</span><br/>" +
                        //                   "<span id='CurrentUserEmailId' style='font-size:11px;font-family:Lucida Grande,Arial,Helvetica,sans-serif;' class='email'>" + ObjData.First().UserName + "</span>");

                        //sb.Append("</span><input id='senderID' type='hidden' value=" + UserId + " /><input id='senderName' type='hidden' value='" + username + "' /></td></tr></tbody></table><input type='hidden' id='hdnpkCompanyUserId'  value='" + ObjData.First().pkCompanyUserId + "'  /> <input type='hidden' id='hdnpkLocationId'  value='" + ObjData.FirstOrDefault().pkLocationId + "'  /></td>");

                        //Session["UserImagepathforHome"] = "~/Resources/Upload/Images/" + ObjData.First().UserImage;

                        sb.Append("<input id='senderID' type='hidden' value=" + UserId + " /><input id='senderName' type='hidden' value='" + username + "' /><input type='hidden' id='hdnpkCompanyUserId'  value='" + ObjData.First().pkCompanyUserId + "'  /><input type='hidden' id='hdnpkLocationId'  value='" + ObjData.FirstOrDefault().pkLocationId + "'  /></td>");
                        Session["CurrentUserCompanyName"] = ObjData.First().CompanyName;
                        Session["UserFullName"] = ObjData.First().FirstName + " " + ObjData.First().LastName;
                        Session["CurrentUserMailId"] = ObjData.First().UserName;
                        Session["CurrentUserId"] = ObjData.First().pkCompanyUserId;

                    }
                }
                finally
                {
                    ObjBALCompanyUsers = null;
                }
            }
            return sb.ToString();
        }


        public string UserinfoImage()
        {
            //MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
            Guid UserId = Utility.GetUID(User.Identity.Name);// new Guid("fdd976e2-222f-47c9-b181-68e5e15ea358");
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            StringBuilder sb = new StringBuilder();
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                Response.Redirect(ApplicationPath.GetApplicationPath() + "Default");
            }
            else
            {

                try
                {
                    List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjData = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(UserId);
                    if (ObjData.Count > 0)
                    {
                        Session["pkCompanyId"] = ObjData.FirstOrDefault().pkCompanyId;
                        Session["pkLocationId"] = ObjData.FirstOrDefault().pkLocationId;
                        Session["IsDuplicateSSNReportAlertEnabled"] = ObjData.FirstOrDefault().IsDuplicateSSNReportAlertEnabled;



                        // sb.Append("<td align='center' width='7%' valign='middle' class='img_user'>");
                        // sb.Append("<div>");


                        string PagePath = string.Empty;
                        string role = ObjData.FirstOrDefault().RoleName;
                        string username = ObjData.First().FirstName + ' ' + ObjData.First().LastName;
                        if (role.ToLower() == "supporttechnician" || role.ToLower() == "supportmanager" || role.ToLower() == "salesmanager")
                        {
                            username = ObjData.First().FirstName + "(Support)";
                        }

                        PagePath = "../Corporate/MyAccount";

                        sb.Append("<div class=\"fleft right-margin10\">");

                        if (ObjData.First().UserImage == string.Empty || !System.IO.File.Exists(Server.MapPath(@"~\Resources\Upload\Images\" + ObjData.First().UserImage)))
                        {

                            sb.Append("<a href='" + PagePath + "'><img style='cursor: pointer; height: 44px; width: 37px; alt='User Image' src=" + @Url.Content("~/Content/themes/base/Images/NotAvail.png") + " title='User Image' /></a>");


                        }
                        else
                        {

                            sb.Append("<a href='" + PagePath + "'><img style='height: 44px; width: 37px;  alt='User Image' src=" + @Url.Content("~/Resources/Upload/Images/" + ObjData.First().UserImage) + " title='User Image' /></a>");

                        }
                        sb.Append("<div style='float: right;margin-left: -14px; top: -71px; height: 3px;'>");
                        sb.Append("<a href='" + PagePath + "'><img title='Edit Image' id='imgEdit' src=" + @Url.Content("~/Content/themes/base/Images/button_edit.png") + " style='cursor: pointer;' /></a>");
                        sb.Append("</div>");

                        sb.Append("<div");



                        ViewBag.CompanyName = ObjData.First().CompanyName;
                        ViewBag.CurrentUserMailId = ObjData.First().UserName;


                        ViewBag.CurrentUserFullName = ObjData.First().FirstName + " " + ObjData.First().LastName;
                        ViewBag.CurrentUserNameMain = ObjData.First().UserName;
                        ViewBag.CurrentUserpkCompanyUserId = ObjData.First().pkCompanyUserId;
                    }
                }

                finally
                {
                    ObjBALCompanyUsers = null;
                }

            }
            return sb.ToString();
        }




        public string UserInformationOfSalesAdmin()
        {
           // MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
            Guid UserId = Utility.GetUID(User.Identity.Name);// new Guid("fdd976e2-222f-47c9-b181-68e5e15ea358");
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            StringBuilder sb = new StringBuilder();
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                Response.Redirect(ApplicationPath.GetApplicationPath() + "Office/Default");
            }
            else
            {
                //if (Session["LoggedUser"] == null)
                //{
                //    #region User LogOut
                //    System.Web.Security.FormsAuthentication.SignOut();

                //    if (ObjMembershipUser != null)
                //    {
                //        BLLMembership ObjBLLMembership = new BLLMembership();
                //        ObjBLLMembership.SignOut(ObjMembershipUser.UserName, DateTime.Now.ToUniversalTime().AddMinutes(-(Membership.UserIsOnlineTimeWindow)));
                //    }

                //    Response.Redirect(ApplicationPath.GetApplicationPath() + "Office/Default");
                //    #endregion
                //}
                try
                {
                    List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjData = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(UserId);
                    Session["pkCompanyId"] = ObjData.FirstOrDefault().pkCompanyId;
                    Session["pkLocationId"] = ObjData.FirstOrDefault().pkLocationId;
                    string UserRole = "";
                    if (Roles.IsUserInRole("SystemAdmin"))
                    {
                        Session["LoggedUser"] = "SystemAdmin";
                        Session["Admin"] = "SystemAdmin";
                    }
                    if (Roles.IsUserInRole("SalesAdmin"))
                    {
                        Session["LoggedUser"] = "SalesAdmin";
                        Session["Admin"] = "SystemAdmin";
                    }
                    if (Session["LoggedUser"] != null)
                    {
                        if (Session["LoggedUser"].ToString() == "SalesAdmin")
                        {
                            UserRole = "Sales Admin";
                        }
                        if (Session["LoggedUser"].ToString() == "SystemAdmin")
                        {
                            UserRole = "SystemAdmin";
                        }
                        if (Session["LoggedUser"].ToString() == "SalesManager")
                        {
                            UserRole = "Sales Manager";
                        }
                        if (Session["LoggedUser"].ToString() == "SalesRep")
                        {
                            UserRole = "Sales Associate";
                        }
                    }
                    else
                    {
                        Response.Redirect(ApplicationPath.GetApplicationPath() + "Office/Default");
                    }
                    if (ObjData.Count > 0)
                    {
                        sb.Append("<td align='center' width='7%' valign='middle' class='img_user'>");
                        sb.Append("<div>");
                        string PagePath = string.Empty;
                        //string role = ObjData.FirstOrDefault().RoleName;
                        //if (role.ToLower() == "systemadmin")
                        //{
                        PagePath = "../Office/MyAccount";
                        //}
                        //else if (role.ToLower() == "corporate")
                        //{
                        //    PagePath = "../Corporate/MyReports";
                        //}
                        if (ObjData.First().UserImage == string.Empty || !System.IO.File.Exists(Server.MapPath(@"~\Resources\Upload\Images\" + ObjData.First().UserImage)))
                        {

                            sb.Append("<a href='" + PagePath + "'><img style='cursor: pointer; height: 68px; width: 60px; border-width: 0px;' alt='User Image' src=" + @Url.Content("~/Content/themes/base/Images/NotAvail.png") + " title='User Image' /></a>");


                        }
                        else
                        {

                            sb.Append("<a href='" + PagePath + "'><img style='height: 68px; width: 60px; border-width: 0px;' alt='User Image' src=" + @Url.Content("~/Resources/Upload/Images/" + ObjData.First().UserImage) + " title='User Image' /></a>");

                        }
                        sb.Append("<div style='float: right;margin-left: -15px; top: -71px; height: 3px;'>");
                        sb.Append("<a href='" + PagePath + "'><img title='Edit Image' id='imgEdit' src=" + @Url.Content("~/Content/themes/base/Images/button_edit.png") + " style='cursor: pointer;' /></a>");

                        // sb.Append("<img title='Edit Image' id='imgEdit' src=" + @Url.Content("~/Content/themes/base/Images/button_edit.png") + " style='cursor: pointer;' />");
                        sb.Append("</div>");
                        sb.Append("</div>");
                        sb.Append("</td>");
                        sb.Append("<td align='left' width='40%' valign='top'>");
                        sb.Append("<table cellspacing='2' cellpadding='2' border='0' width='100%'><tbody><tr><td align='left'><span style='font-weight: bold;' id='nr_LblUserInfo'>");

                        sb.Append("<span style='font-size:14px;font-family:Lucida Grande,Arial,Helvetica,sans-serif;'>" + ObjData.First().FullName + "</span><br/>" +
                                       "<span style='font-size:11px;font-family:Lucida Grande,Arial,Helvetica,sans-serif;'>" + UserRole + "</span><br/>" +
                                       "<span style='font-size:11px;font-family:Lucida Grande,Arial,Helvetica,sans-serif;'>" + ObjData.First().CompanyName + "</span><br/>" +
                                       "<span style='font-size:11px;font-family:Lucida Grande,Arial,Helvetica,sans-serif;'>" + ObjData.First().CompanyLocation + "</span><br/>" +
                                       "<span style='font-size:11px;font-family:Lucida Grande,Arial,Helvetica,sans-serif;' class='email'>" + ObjData.First().UserName + "</span>");

                        sb.Append("</span></td></tr></tbody></table></td>");

                        //ViewBag.CompanyName = ObjData.First().CompanyName;
                        //ViewBag.CurrentUserMailId = ObjData.First().UserName;
                        Session["CurrentUserCompanyName"] = ObjData.First().CompanyName;
                        Session["UserFullName"] = ObjData.First().FirstName + " " + ObjData.First().LastName;
                        Session["CurrentUserMailId"] = ObjData.First().UserName;
                        Session["CurrentUserId"] = ObjData.First().pkCompanyUserId;
                    }
                }
                finally
                {
                    ObjBALCompanyUsers = null;
                }
            }
            return sb.ToString();
        }


        public string LoadAllAlerts()
        {
            Guid UserId = Utility.GetUID(User.Identity.Name);

            //get password expiry alert
            //get password duration from configuration
            //string password_expiration_duration = System.Configuration.ConfigurationManager.AppSettings["PasswordExpiryDuration"].ToString();
            //int int_password_expiration_duration = 90; //set default to 90, but pick from webconfig file
            //if (!int.TryParse(password_expiration_duration, out  int_password_expiration_duration))
            //{ int_password_expiration_duration = 90; }

            //MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
            //get the last password update date 
            //DateTime _last_password_update = ObjMembershipUser.LastPasswordChangedDate.Date;
            //TimeSpan _TimeSpan = DateTime.UtcNow.Subtract(_last_password_update);

            BALGeneral objGeneral = new BALGeneral();
            List<Proc_GetAllAlertsByUserIdResult> AllAlert = null;
            AllAlert = objGeneral.GetAllAlertsByUserId(UserId);
            bool IsAdmin = false;
            if (Session["Admin"] != null)
            {
                string role = Session["Admin"].ToString();
                if (role == "SystemAdmin" || role == "SupportManager" || role == "SupportTechnician")
                {
                    IsAdmin = true;
                }
                else
                {
                    IsAdmin = false;
                }
            }
            var alertsCollec = objGeneral.GetCompletedManualReports(UserId, IsAdmin);
            StringBuilder sb = new StringBuilder();
            sb.Append("<table id='divresAlert' Width='250px'>");

            //if ((int_password_expiration_duration - _TimeSpan.Days) < 8) 
            //{
            //    sb.Append("<tr class=\"GridRowtop\" onclick=\"window.location='../Corporate/MyAccount'\">");
            //    sb.Append("<td>");
            //    sb.Append("<table style='border-bottom:1px solid #e5e5e5; width: 100%' class='divalerts_Text'>");
            //    sb.Append("<tr style='height: 35px;'>");
            //    sb.Append("<td align='center' valign='middle' width='25%' class='prCode'>");
            //    sb.Append("<img alt='' src=" + @Url.Content("~/Content/themes/base/Images/icon-newfeature.png") + " />");
            //    sb.Append("</td>");
            //    sb.Append("<td align='left' valign='middle' width='75%' class='prName' style='color:Red;'> Update your password. Will expire in " + (int_password_expiration_duration - _TimeSpan.Days).ToString() + " days.");
            //    sb.Append("</td>");
            //    sb.Append("</tr>");
            //    sb.Append("</table>");
            //    sb.Append("</td>");
            //    sb.Append("</tr>");
            //}





            if (AllAlert.Count > 0)
            {
                Session["TotalAlerts"] = AllAlert.Count;
                for (int i = 0; i <= AllAlert.Count - 1; i++)
                {
                    sb.Append("<tr class='GridRowtop' onclick=\"ShowAlertspopup('" + AllAlert.ElementAt(i).AlertType + "','" + AllAlert.ElementAt(i).AlertTypeId + "','" + AllAlert.ElementAt(i).pkAlertId + "')\">");// onclick=\"ShowAlertPopUp('" + AllAlert.ElementAt(i).AlertType.ToString() + "##" + AllAlert.ElementAt(i).AlertTypeId.ToString() + "##" + AllAlert.ElementAt(i).pkAlertId.ToString() + "');\"
                    sb.Append("<td>");
                    //sb.Append("<a href='javascript:void(0);' onclick=\"ShowAlertPopUp('" + AllAlert.ElementAt(i).AlertType.ToString() + "','" + AllAlert.ElementAt(i).AlertTypeId.ToString() + "','" + AllAlert.ElementAt(i).pkAlertId.ToString() + "');\">");
                    sb.Append("<table style='border-bottom:1px solid #e5e5e5; width: 100%' class='divalerts_Text'>");
                    sb.Append("<tr style='height: 35px;'>");

                    if (AllAlert.ElementAt(i).AlertType == 1)
                    {
                        sb.Append("<td align='center' valign='middle' width='25%' class='prCode'>");
                        sb.Append("<img alt='' src=" + @Url.Content("~/Content/themes/base/Images/icon-notice.png") + " />");
                        sb.Append("</td>");
                        sb.Append("<td align='left' valign='middle' width='75%' class='prName'>" + GetSmall(AllAlert.ElementAt(i).Title, 25) + "<br />");
                        sb.Append("EMERGE NOTICE");
                        sb.Append("</td>");
                    }
                    else if (AllAlert.ElementAt(i).AlertType == 2)
                    {
                        sb.Append("<td align='center' valign='middle' width='25%' class='prCode'>");
                        sb.Append("<img alt='' src=" + @Url.Content("~/Content/themes/base/Images/letter-icon.png") + " />");
                        sb.Append("</td>");
                        sb.Append("<td align='left' valign='middle' width='75%' class='prName'>" + GetSmall(AllAlert.ElementAt(i).Title, 25) + "<br />");
                        sb.Append("EMERGE NEWSLETTER");
                        sb.Append("</td>");
                    }
                    else if (AllAlert.ElementAt(i).AlertType == 3)
                    {
                        sb.Append("<td align='center' valign='middle' width='25%' class='prCode'>");
                        sb.Append("<img alt='' src=" + @Url.Content("~/Content/themes/base/Images/icon-newfeature.png") + " />");
                        sb.Append("</td>");
                        sb.Append("<td align='left' valign='middle' width='75%' class='prName'>" + GetSmall(AllAlert.ElementAt(i).Title, 25) + "<br />");
                        sb.Append("NEW FEATURE");
                        sb.Append("</td>");
                    }
                    else if (AllAlert.ElementAt(i).AlertType == 4)
                    {
                        sb.Append("<td align='center' valign='middle' width='25%' class='prCode'>");
                        sb.Append("<img alt='' src=" + @Url.Content("~/Content/themes/base/Images/emerge-review-button-small.png") + " />");
                        sb.Append("</td>");
                        sb.Append("<td align='left' valign='middle' width='75%' class='prName'>" + GetSmall(AllAlert.ElementAt(i).Title, 25) + "<br />");
                        sb.Append("EMERGE REVIEW COMPLETED");
                        sb.Append("</td>");
                    }
                    else if (AllAlert.ElementAt(i).AlertType == 5)
                    {
                        sb.Append("<td align='center' valign='middle' width='25%' class='prCode'>");
                        sb.Append("<a href='../Corporate/BillingStatements'><img alt='' src=" + @Url.Content("~/Content/themes/base/Images/invoice_alert.png") + " /></a>");
                        sb.Append("</td>");
                        sb.Append("<td align='left' valign='middle' width='75%' class='prName'>" + GetSmall(AllAlert.ElementAt(i).Title, 25) + "<br />");
                        sb.Append("" + Convert.ToDateTime(AllAlert.ElementAt(i).CreatedDate).ToShortDateString());
                        sb.Append("</td>");
                    }
                    else if (AllAlert.ElementAt(i).AlertType == 7)// added to show extra billing information 
                    {// change on 11 feb 2016 
                        // show extra alertts for billing as due date , locking , before and after invoice cration 
                        sb.Append("<td align='center' valign='middle' width='25%' class='prCode'>");
                        sb.Append("<a href='../Corporate/BillingStatements'><img alt='' src=" + @Url.Content("~/Content/themes/base/Images/invoice_alert.png") + " /></a>");
                        sb.Append("</td>");
                        sb.Append("<td align='left' valign='middle' width='75%' class='prName'>" + GetSmall(AllAlert.ElementAt(i).Title, 25) + "<br />");
                        sb.Append("" + Convert.ToString(AllAlert.ElementAt(i).CreatedDate));
                        sb.Append("</td>");
                    }


                    sb.Append("</tr>");
                    sb.Append("</table>");
                    //sb.Append("</a>");
                    sb.Append("</td>");
                    sb.Append("</tr>");
                }
            }
            if (alertsCollec.Count > 0)
            {
                for (int i = 0; i <= alertsCollec.Count - 1; i++)
                {
                    sb.Append("<tr class=\"GridRowtop\" onclick=\"window.location='ViewReports?_OId=" + alertsCollec.ElementAt(i).fkOrderId.ToString() + "'\">");
                    sb.Append("<td>");
                    sb.Append("<table style='border-bottom:1px solid #e5e5e5; width: 100%' class='divalerts_Text'>");
                    sb.Append("<tr style='height: 35px;'>");
                    sb.Append("<td align='center' width='25%' valign='middle' class='prCode'> " + alertsCollec.ElementAt(i).ProductCode.ToString() + "</td>");
                    sb.Append("<td align='left' width='75%' valign='middle' class='prName'>" + alertsCollec.ElementAt(i).ProductCode.ToString() + " &nbsp;REPORT IS READY<br />" + alertsCollec.ElementAt(i).FullName.ToString() + "</td>");
                    sb.Append("</tr>");
                    sb.Append("</table>");
                    sb.Append("</td>");
                    sb.Append("</tr>");
                }
            }




            if (AllAlert.Count == 0 && alertsCollec.Count == 0)
            {
                sb.Append("<tr><td>There are no new alerts.</td></tr>");
            }
            sb.Append("</table>");
            return sb.ToString();
        }


        public string LoadAllAlertsOffice()
        {
            Guid UserId = Utility.GetUID(User.Identity.Name);
            //BALGeneral objGeneral = new BALGeneral();

            BALOffice objBALOffice = new BALOffice();
            int PageNum = 1;
            int PageSize = 0;
            string SortingColumn = "CreatedDate";
            string SortingDirection = "asc";
            var alertsCollec = objBALOffice.GetAlertsForOfficeUser(UserId, PageNum, PageSize, SortingColumn, SortingDirection);

            StringBuilder sb = new StringBuilder();
            sb.Append("<table cellspacing=0 border=0 style=background-color:#F1EEEF;width:250px;border-collapse:collapse; >");
            if (alertsCollec.Count > 0)
            {
                for (int i = 0; i <= alertsCollec.Count - 1; i++)
                {
                    string[] stringSeparators = new string[] { "##" };
                    string[] StatusArr = alertsCollec.ElementAt(i).LeadComments.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

                    sb.Append("<tr valign='middle' align='left' class='GridRowAlert'>");
                    sb.Append("<td>");
                    sb.Append("<table width='100%' cellspacing='0' cellpadding='0' border='0' class='divalerts_Text'>");
                    sb.Append("<tr style='height: 35px;'>");
                    if (StatusArr.Length > 0)
                    {
                        string Status = StatusArr[0];

                        if (Status == "5")
                        {
                            sb.Append("<td width='40%' valign='middle' align='left'>");
                            sb.Append("<div style='border: 1px solid gray; padding: 1px; width: 90px; height: 16px'>");
                            sb.Append("<img style='height:16px;width:90px;border-width:0px;' alt='Account Active' title='Account Active' src=" + @Url.Content("~/Content/themes/base/Images/emerge-office-milestone-5-on.png") + " />");
                            sb.Append("</div>");
                            sb.Append("</td>");
                            sb.Append("<td width='60%' valign='middle' align='left'>" + alertsCollec.ElementAt(i).CompanyName);
                            sb.Append("</td>");


                        }
                        else if (Status == "12")
                        {
                            sb.Append("<td width='40%' valign='middle' align='left'>");
                            sb.Append("<div style='border: 1px solid gray; padding: 1px; width: 90px; height: 16px'>");
                            sb.Append("<img style='height:16px;width:90px;border-width:0px;' alt='Quality Control Survey' title='Quality Control Survey Completed' src=" + @Url.Content("~/Content/themes/base/Images/QualityControlSurvey.png") + " />");
                            sb.Append("</div>");
                            sb.Append("</td>");
                            sb.Append("<td width='60%' valign='middle' align='left'>" + alertsCollec.ElementAt(i).CompanyName);
                            sb.Append("</td>");
                        }
                        else if (Status == "13")
                        {

                            sb.Append("<td width='40%' valign='middle' align='left'>");
                            sb.Append("<div style='border: 1px solid gray; padding: 1px; width: 90px; height: 16px'>");
                            sb.Append("<img style='height:16px;width:90px;border-width:0px;'  alt='Demo Completed'  title='Demo Completed'  src=" + @Url.Content("~/Content/themes/base/Images/Demo_Completed.png") + " />");
                            sb.Append("</div>");
                            sb.Append("</td>");
                            sb.Append("<td width='60%' valign='middle' align='left'>" + alertsCollec.ElementAt(i).CompanyName);
                            sb.Append("</td>");

                        }
                        else if (Status == "6")
                        {

                            sb.Append("<td width='40%' valign='middle' align='left'>");
                            sb.Append("<div style='border: 1px solid gray; padding: 1px; width: 90px; height: 16px'>");
                            sb.Append("<img style='height:16px;width:90px;border-width:0px;'  alt='Account Inactive' title='Account Inactive' src=" + @Url.Content("~/Content/themes/base/Images/emerge-office-milestone-6-on.png") + " />");
                            sb.Append("</div>");
                            sb.Append("</td>");
                            sb.Append("<td width='60%' valign='middle' align='left'>" + alertsCollec.ElementAt(i).CompanyName);
                            sb.Append("</td>");

                        }
                        else if (Status == "7")
                        {
                            sb.Append("<td width='40%' valign='middle' align='left'>");
                            sb.Append("<div style='border: 1px solid gray; padding: 1px; width: 90px; height: 16px'>");
                            sb.Append("<img style='height:16px;width:90px;border-width:0px;'  alt='Account Locked' title='Account Locked' src=" + @Url.Content("~/Content/themes/base/Images/emerge-office-milestone-7-on.png") + " />");
                            sb.Append("</div>");
                            sb.Append("</td>");
                            sb.Append("<td width='60%' valign='middle' align='left'>" + alertsCollec.ElementAt(i).CompanyName);
                            sb.Append("</td>");

                        }


                        else if (Status == "8")
                        {
                            sb.Append("<td width='40%' valign='middle' align='left'>");
                            sb.Append("<div style='border: 1px solid gray; padding: 1px; width: 90px; height: 16px'>");
                            sb.Append("<img style='height:16px;width:90px;border-width:0px;'  alt='Account Past Due' title='Account Past Due' src=" + @Url.Content("~/Content/themes/base/Images/emerge-office-milestone-8-on.png") + " />");
                            sb.Append("</div>");
                            sb.Append("</td>");
                            sb.Append("<td width='60%' valign='middle' align='left'>" + alertsCollec.ElementAt(i).CompanyName);
                            sb.Append("</td>");



                        }
                        else if (Status == "9")
                        {
                            sb.Append("<td width='40%' valign='middle' align='left'>");
                            sb.Append("<div style='border: 1px solid gray; padding: 1px; width: 90px; height: 16px'>");
                            sb.Append("<img style='height:16px;width:90px;border-width:0px;'  alt='Account In Collections' title='Account In Collections' src=" + @Url.Content("~/Content/themes/base/Images/emerge-office-milestone-9-on.png") + " />");
                            sb.Append("</div>");
                            sb.Append("</td>");
                            sb.Append("<td width='60%' valign='middle' align='left'>" + alertsCollec.ElementAt(i).CompanyName);
                            sb.Append("</td>");






                        }

                        sb.Append("</tr>");
                        sb.Append("</table>");
                        //sb.Append("</a>");
                        sb.Append("</td>");
                        sb.Append("</tr>");





                    }
                }
            }
            if (alertsCollec.Count == 0 && alertsCollec.Count == 0)
            {
                sb.Append("<tr><td>There are no new alerts.</td></tr>");
            }
            sb.Append("</table>");
            return sb.ToString();

        }

        private string ConvertToMonthText(int BillingMonth)
        {
            string sMonth = string.Empty;

            switch (BillingMonth)
            {
                case 1:
                    sMonth = "January";
                    break;
                case 2:
                    sMonth = "February";
                    break;
                case 3:
                    sMonth = "March";
                    break;
                case 4:
                    sMonth = "April";
                    break;
                case 5:
                    sMonth = "May";
                    break;
                case 6:
                    sMonth = "June";
                    break;
                case 7:
                    sMonth = "July";
                    break;
                case 8:
                    sMonth = "August";
                    break;
                case 9:
                    sMonth = "September";
                    break;
                case 10:
                    sMonth = "October";
                    break;
                case 11:
                    sMonth = "November";
                    break;
                case 12:
                    sMonth = "December";
                    break;
            }
            return sMonth;
        }

        public string GetSmall(object Obj, int len)
        {
            string retstr = string.Empty;
            retstr = Obj.ToString();
            if (retstr.Length > len)
            {
                retstr = retstr.Substring(0, len);
                retstr += "...";
            }
            return retstr;
        }

        [HttpPost]
        public JsonResult ShowAlertPopUp(string AType, string ATypeId, string pkAlertId)
        {
            int AlertId = Convert.ToInt32(ATypeId);
            BALGeneral ObjBALGeneral = new BALGeneral();
            ObjBALGeneral.DeleteAlerts(Int32.Parse(pkAlertId));
            ObjBALGeneral = null;
            string PopupBody;
            string PopupHeader;
            GetPopUpHeaders(AlertId, out PopupBody, out PopupHeader);
            return Json(new { Header = PopupHeader, Msgbody = PopupBody }, JsonRequestBehavior.AllowGet);
            //else if (AType == "5")
            //{
            //    string AlertId = ATypeId.ToString();
            //    if (AlertId != null)
            //    {
            //        //BALGeneral ObjBALGeneral = new BALGeneral();
            //        //ObjBALGeneral.DeleteAlerts(new Guid(CArgs[2].ToString()));
            //        //ObjBALGeneral = null;
            //        //IsHover.Value = "0";
            //        //Response.Redirect("~/Corporate/BillingStatement?AlertInvId=" + AlertId);
            //    }
            //}
        }

        [HttpPost]
        public JsonResult RemoveentryfromAlerts(string pkAlertId)
        {
            BALGeneral ObjBALGeneral = new BALGeneral();
            ObjBALGeneral.DeleteAlerts(Int32.Parse(pkAlertId));
            ObjBALGeneral = null;
            return Json(new { Result = "true" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult LoadNoticeNewsletter(int PageId)
        {
            string PopupBody;
            string PopupHeader;
            GetPopUpHeaders(PageId, out PopupBody, out PopupHeader);
            return Json(new { Header = PopupHeader, Msgbody = PopupBody }, JsonRequestBehavior.AllowGet);
        }


        private static void GetPopUpHeaders(int PageId, out string PopupBody, out string PopupHeader)
        {
            BALEmailSystemTemplates ObjEmailTemplates = new BALEmailSystemTemplates();
            PopupBody = string.Empty;
            PopupHeader = string.Empty;
            if (PageId != 0)
            {
                var Records = ObjEmailTemplates.GetEmailTemplates(PageId, Utility.SiteApplicationId, false, 0);
                if (Records.Count > 0)
                {
                    #region New way Bookmark
                    string MessageBody;
                    BALGeneral ObjBALGeneral = new BALGeneral();
                    string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;

                    BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                    Bookmarks objBookmark = new Bookmarks();

                    objBookmark.EmailContent = Convert.ToString(Records.ElementAt(0).TemplateContent);
                    objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                    objBookmark.LoginLink = "<a style='color: #87ceea;' href='" + ApplicationPath.GetApplicationPath() + "'> Click here </a>";

                    MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Utility.GetUID(System.Web.HttpContext.Current.User.Identity.Name.ToString() != null ? System.Web.HttpContext.Current.User.Identity.Name.ToString() : "Admin Admin"));

                    #endregion
                    PopupBody = MessageBody;
                    PopupHeader = Records.ElementAt(0).TemplateSubject;
                }
            }
        }

        #region Get Left Menu
        public PartialViewResult GetLeftMenu()
        {

            string Menu = string.Empty;
            if (Roles.IsUserInRole(User.Identity.Name, "BranchManager") || Roles.IsUserInRole(User.Identity.Name, "BasicUser"))
            {
                Menu = "PartialBranchLeftMenu";
            }
            if (Roles.IsUserInRole(User.Identity.Name, "SystemAdmin") || Roles.IsUserInRole(User.Identity.Name, "SupportManager") || Roles.IsUserInRole(User.Identity.Name, "SupportTechnician"))//UserRole == "Admin" ||
            {
                if (Request.Url.AbsoluteUri.ToLower().Contains("/office/myclients") || Request.Url.AbsoluteUri.ToLower().Contains("/office/myleads") || Request.Url.AbsoluteUri.ToLower().Contains("/office/salesreport") || Request.Url.AbsoluteUri.ToLower().Contains("/office/myaccount") || Request.Url.AbsoluteUri.ToLower().Contains("/office/alerts"))
                {
                    Menu = "PartialSalesAccountLeftMenu";
                }
                else if (Request.Url.AbsoluteUri.ToLower().Contains("/office/"))
                {
                    Menu = "PartialSalesAdminLeftMenu";
                }

                else
                {
                    Menu = "PartialAdminLeftMenu";
                }
            }
            if (Roles.IsUserInRole(User.Identity.Name, "CorporateManager"))
            {
                if (Request.Url.AbsoluteUri.ToLower().Contains("/office/myclients") || Request.Url.AbsoluteUri.ToLower().Contains("/office/myleads") || Request.Url.AbsoluteUri.ToLower().Contains("/office/salesreport") || Request.Url.AbsoluteUri.ToLower().Contains("/office/myaccount") || Request.Url.AbsoluteUri.ToLower().Contains("/office/alerts"))
                {
                    Menu = "PartialSalesAccountLeftMenu";
                }
                else if (Request.Url.AbsoluteUri.ToLower().Contains("/control/"))
                {
                    Menu = "PartialAdminLeftMenu";
                }
                else if (Request.Url.AbsoluteUri.ToLower().Contains("/office/"))
                {
                    Menu = "PartialSalesAdminLeftMenu";
                }
                else
                {
                    Menu = "PartialCorporateLeftMenu";
                }

            }
            if (Roles.IsUserInRole(User.Identity.Name, "DataEntry") || Roles.IsUserInRole(User.Identity.Name, "ViewOnlyUser"))
            {
                Menu = "PartialDataEntryLeftMenu";
            }
            if (Roles.IsUserInRole(User.Identity.Name, "SalesAdmin"))
            {
                if (Request.Url.AbsoluteUri.ToLower().Contains("/office/"))
                {
                    Menu = "PartialSalesAdminLeftMenu";
                }
            }

            if (Roles.IsUserInRole(User.Identity.Name, "SalesRep") || Roles.IsUserInRole(User.Identity.Name, "SalesManager"))
            {
                if (Request.Url.AbsoluteUri.ToLower().Contains("/office/"))
                {
                    Menu = "PartialSalesAccountLeftMenu";
                }
            }
            if (Roles.IsUserInRole(User.Identity.Name, "BillingUser"))
            {
                if (Request.Url.AbsoluteUri.ToLower().Contains("/corporate/"))
                {
                    Menu = "PartialBillingLeftMenu";
                }
            }

            return PartialView(Menu);
        }
        #endregion

        public ActionResult PSPVendorCall()
        {
            string xmlText = "";
            try
            {
                using (ServiceWebClient objSer = new ServiceWebClient())
                {
                    Dot.Psp.Common.RetrieveDriverRecordReportDataType test = new Dot.Psp.Common.RetrieveDriverRecordReportDataType();
                    //test.DOTNumber = 0;
                    test.AuthCode = "bf10f87d-2f4c-48b6-bb23-10666e7b7e19";
                    test.DriverConsent = true;
                    test.DriverDOB = "07-Jul-1974";
                    test.DriverFirstName = "Gary";
                    test.DriverLastName = "Testone";
                    // test.ExtensionData = null;
                    test.InternalRefId = "Internal Reference";
                    test.MotorCarrierId = "1234";
                    test.Password = "usaintel!123";
                    test.UserName = "Uistagews";
                    Dot.Psp.Common.DriverQuery[] test1 = new Dot.Psp.Common.DriverQuery[1];
                    if (test1.Length > 0)
                    {
                        test1[0] = new Dot.Psp.Common.DriverQuery();
                        test1[0].DlLastName = "Testone";
                        test1[0].DlNum = "123456";
                        test1[0].DlState = "GA";
                        //test12.DlLastName = "Testone";
                        //test12.DlNum = "123456";
                        //test12.DlState = "GA";
                        //test1. = test12;
                        test.DriverQueries = test1;
                        Dot.Psp.Common.DriverReportResponseType result = new Dot.Psp.Common.DriverReportResponseType();
                        result = objSer.GetDriverData(test);

                        System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(result.GetType());
                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(HttpContext.Server.MapPath("~/Resources/Upload/PSPVendorCall/Response.xml")))
                        {
                            writer.Serialize(file, result);

                            file.Close();
                        }
                        xmlText = "Please Check file at path : ~/Resources/Upload/PSPVendorCall/Response.xml";
                        //MemoryStream memoryStream = new MemoryStream();
                        //using (XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8) { Formatting = Formatting.Indented })
                        //{
                        //    //convert the object to xml data
                        //    xmlSerializer.Serialize(xmlTextWriter, result);
                        //    //Get reference to memory stream
                        //    memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                        //    //Convert memory byte array into xml text
                        //    xmlText = new UTF8Encoding().GetString(memoryStream.ToArray());
                        //    //clean up memory stream
                        //    memoryStream.Dispose();
                        //    return xmlText;
                        //}

                        return Content(result.ToString());
                    }
                }
            }
            catch (Exception Ex)
            {
                using (TextWriter tw = System.IO.File.CreateText(HttpContext.Server.MapPath("~/Resources/Upload/PSPVendorCall/Error.txt")))
                {
                    tw.WriteLine(Ex.ToString());
                    tw.Close();
                }
                xmlText = "Please Check file at path : ~/Resources/Upload/PSPVendorCall/Error.txt";
                return Content(Ex.ToString());
            }

            return Content(xmlText);
        }
    }
}
