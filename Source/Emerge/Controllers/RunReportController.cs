﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Emerge.Models;
using System.Web.Security;
using Emerge.Data;
using Emerge.Common;
using System.Text;
using System.Globalization;

namespace Emerge.Controllers
{
    //This [RequireHttps] should be enabled before creating any build on Live or testProd4.
    //If team is working in local environment, then it will be . 
    //[RequireHttps]
    public class RunReportController : Controller
    {
        public ActionResult Index()
        {
          
            BALGeneral ObjBALGeneral = new BALGeneral();
            NewReportModel ObjModel = new NewReportModel();
            int UserLocationId = 0;
            int pkCompanyId = 0;
            ObjModel.Urlpath = Request.Url.AbsoluteUri.ToString();
            string url = Request.Url.AbsoluteUri.ToString().Substring(Request.Url.AbsoluteUri.ToString().LastIndexOf("/") + 1);
            url = url.Replace("/", string.Empty);
            ObjModel.ApiUrl = url;
            BALCompany Obj_BALCompany = new BALCompany();
            var Result = Obj_BALCompany.GetCurrentCompanyName(url);
            string Companyname = Result.Item1;
            ObjModel.pkCompanyId = Result.Item2;
            ObjModel.CompanyLogo = Result.Item3;
            ObjModel.CompanyDisplayName = Companyname.ToUpper();
            ObjModel.CompanyName = Companyname.ToUpper();
            #region Get Company Id
            BALCompany ObjBALCompany = new BALCompany();
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            MembershipUser Member = Membership.GetUser(User.Identity.Name);
            ObjModel.IsAuthenicated = false;
            if (Member != null)
            {
                if (Session["success"] == "true")
                {
                    ObjModel.IsAuthenicated = true;
                }
                var dbCollection = ObjBALCompanyUsers.GetUserCompanyInfo(new Guid(Member.ProviderUserKey.ToString()));
                if (dbCollection.Count > 0)
                {
                    int fkLocationId = Convert.ToInt32(dbCollection.First().pkLocationId.ToString());
                    UserLocationId = fkLocationId;
                    ObjModel.fkLocationId = fkLocationId;
                    ObjModel.pkCompanyUserId = Convert.ToInt32(dbCollection.First().pkCompanyUserId.ToString());
                    pkCompanyId = dbCollection.First().pkCompanyId;
                    ObjModel.pkCompanyId = pkCompanyId;
                    ObjModel.OrderCompanyName = dbCollection.First().CompanyName;
                    ObjModel.CompanyAccountNo = dbCollection.First().CompanyAccountNumber;
                    ObjModel.PermissiblePurposeId = dbCollection.First().PermissiblePurpose;
                    ObjModel.CompanyDisplayName = dbCollection.First().CompanyName;
                    ObjModel.IsturnoffSSN = dbCollection.First().IsturnoffSSN;
                    //ObjModel.CompanyLogo = string.Empty;
                    ObjModel.IsWCVAcknowledged = dbCollection.First().IsWCVAcknowledged;
                    ObjModel.IsLiveRunnerEnabled = dbCollection.First().IsLiveRunnerEnabled;
                    try
                    {
                        if (dbCollection.First().CompanyLogo != string.Empty)
                        {  if (System.IO.File.Exists(Server.MapPath("~/Resources/Upload/CompanyLogo/") + dbCollection.First().CompanyLogo))
                            {
                                ObjModel.CompanyLogo = ApplicationPath.GetApplicationPath() + "Resources/Upload/CompanyLogo/" + dbCollection.First().CompanyLogo;
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }
                    ObjModel.CompanyAddress = dbCollection.First().Address1;

                }

                //coded by chetu.
                #region Left Panel Categories

                List<Proc_Get_ReportCategoriesResult> ObjDataCategories = ObjBALGeneral.GetReportCategories().Where(t => t.pkReportCategoryId != 9).ToList();
                ObjModel.ReportCategoriesList = ObjDataCategories;

                Proc_Get_ReportCategoriesResult ObjDynamicColl = new Proc_Get_ReportCategoriesResult();
                //List<string> ObjCollRcxProducts = new List<string>();
                ObjDynamicColl.pkReportCategoryId = 0;
                ObjDynamicColl.CategoryName = "Packages";
                ObjDynamicColl.IsEnabled = true;
                ObjDataCategories.Insert(0, ObjDynamicColl);

                #endregion

                #region Categories Report List

                Dictionary<int, List<Proc_Get_NewReportsByCategoryIdResult>> DictCategory = new Dictionary<int, List<Proc_Get_NewReportsByCategoryIdResult>>();
                for (int i = 0; i < ObjDataCategories.Count; i++)
                {

                    var NewReportsByCategory1 = ObjBALGeneral.GetNewReportsByCategoryId(UserLocationId, ObjDataCategories.ElementAt(i).pkReportCategoryId);
                    DictCategory.Add(i, NewReportsByCategory1);
                }
                ObjModel.DictCategory = DictCategory;

                #endregion

                //#region Left Panel Categories
                //List<Proc_Get_NewReportsForLandingPageResult> ObjDataCategories = ObjBALGeneral.GetNewReportsForLandingPage(UserLocationId).ToList();
                //ObjModel.LandingReports = ObjDataCategories.Where(p => p.CategoryName != "eScreen").ToList();
                //#endregion

                #region Package List
                List<Proc_Get_EmergeProductPackagesResult> ObjReports = ObjBALGeneral.GetEmergeProductPackages(UserLocationId);
                ObjModel.EmergeProductPackagesList = ObjReports;
                //ObjModel.EmergeProductPackagesList = ObjReports.Where(t => t.IsTiered2!="1").ToList(); //commented by chetu.
                
                #endregion

                using (Corporate.CorporateController ObjController = new Corporate.CorporateController())
                {
                    ObjModel = ObjController.BindNewReportDropdowns(ObjBALGeneral, ObjModel, pkCompanyId, ObjBALCompany);
                }
                ObjModel.OrderType = 2; //Landing Page
                ObjModel.UserName = "dummy";
                ObjModel.Password = "dummy";
                ObjModel.hdnIsSCRLiveRunnerLocation = "0";

            }


            #endregion
            string username;
            string password;
            string UserRole;
            bool Remembered;

            Utility.GetCookie(out username, out password, out UserRole, out Remembered);
            if (Remembered)
            {
                ObjModel.UserName = username;
                ViewBag.Password = password;
                ObjModel.RemeberMe = true;
                ObjModel.UserFistLastName = string.Empty;
                BALContentManagement objContentsMgt = new BALContentManagement();
                var list = objContentsMgt.GetCompanyNameByEmailId(username);
                if (list.UserImage != "" && list.UserImage != null) {  ObjModel.UserImage = list.UserImage; }
                else { ObjModel.UserImage = null; }
                //var list2 = objContentsMgt.GetCompanyNameByUserId(username);
                var FirstName = list.FirstName;
                var LastName = list.LastName;
                var firstandlastname = FirstName + "  " + LastName;
                CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
                TextInfo textInfo = cultureInfo.TextInfo;
                string Searchedfirstname = firstandlastname != null ? textInfo.ToTitleCase(firstandlastname) : string.Empty;
                ObjModel.UserFistLastName = Searchedfirstname;
                ViewBag.tdTakeMeTo = "true";
                ViewBag.tdDDLDefaultPage = "true";
                ViewBag.DDLDefaultPage = "true";
                ViewBag.lblTakeMeTo = "true";
                ViewBag.loginbackcolor = "#d4021b";
                ObjModel.ApiUrl = url;
            }

            return View(ObjModel);
        }
       
    }
}
