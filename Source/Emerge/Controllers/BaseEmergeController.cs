﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Emerge.Controllers
{
    public class BaseEmergeController : Controller
    {
        public BaseEmergeController()
        {
            MembershipUser Member = Membership.GetUser();
            if (Member == null)
            {
                System.Web.HttpContext.Current.Response.Redirect("../default");
            }          
        }
    }
}
