﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.Web.Mvc.UI;
using System.IO;

namespace Emerge.Controllers
{
    public class ImageBrowserController : EditorFileBrowserController
    {
        private const string contentFolderRoot = "~/Resources/Upload";
        private const string prettyName = "Images/";
        //private static readonly string[] foldersToCopy = new[] { "~/Resources/Images" };

        /// <summary>
        /// Gets the base paths from which content will be served.
        /// </summary>
        public override string[] ContentPaths
        {
            get
            {
                return new[] { CreateUserFolder() };
            }
        }

        private string UserID
        {
            get
            {
                var obj = Session["ADUserLoginId"] == null ? "Images" : Session["ADUserLoginId"].ToString();
                //if (obj == null)
                //{
                //    Session["ADUserLoginId"] = obj = "Images";//DateTime.Now.Ticks.ToString();
                //}
                return (string)obj;
            }
        }

        private string CreateUserFolder()
        {
            var userFolder = Path.Combine("EditorImages", UserID);
            var virtualPath = Path.Combine(contentFolderRoot, userFolder);

            var path = Server.MapPath(virtualPath);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                //foreach (var sourceFolder in foldersToCopy)
                //{
                //    CopyFolder(Server.MapPath(sourceFolder), path);
                //}
            }
            return virtualPath;
        }

        private void CopyFolder(string source, string destination)
        {
            if (!Directory.Exists(destination))
            {
                Directory.CreateDirectory(destination);
            }

            foreach (var file in Directory.EnumerateFiles(source))
            {
                var dest = Path.Combine(destination, Path.GetFileName(file));
                System.IO.File.Copy(file, dest);
            }

            foreach (var folder in Directory.EnumerateDirectories(source))
            {
                var dest = Path.Combine(destination, Path.GetFileName(folder));
                CopyFolder(folder, dest);
            }
        }

    }
}
