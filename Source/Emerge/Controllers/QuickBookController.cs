﻿using System;
using System.Linq;
using System.Web.Mvc;


using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;


using Emerge.Service;
using Emerge.Data;
using System.Collections.Generic;



namespace Emerge.Controllers
{
    public class QuickBookController : Controller
    {
        //
        // GET: /QuickBook/

        public ActionResult Index()
        {
            return View();
        }
        public string QuickBooktesting()
        {
            return "1";
        }


        public JsonResult GetRequest()
        {            
            List<QuickBookRequest> getRequest = new List<QuickBookRequest>();
            try
            {
                using (EmergeDALDataContext myDB = new EmergeDALDataContext())
                {
                    getRequest = (from m in myDB.QuickBookRequests
                                  where m.Status.Equals(0)
                                  select m).OrderBy(m => m.ID).ToList();

                    //getRequest = (from m in myDB.QuickBookRequests
                    //              where m.Status.Equals(0)
                    //              select m).Take(1).OrderBy(m => m.ID).FirstOrDefault();

                }
            }
            catch //(Exception ex)
            {

            }

            return Json(getRequest, JsonRequestBehavior.AllowGet);
        }




        //public string GetRequest()
        //{
        //    EmergeDALDataContext myDB = new EmergeDALDataContext();
        //    QuickBookRequest getRequest = null;
        //    try
        //    {
        //        getRequest = (from m in myDB.QuickBookRequests
        //                      where m.Status.Equals(0)
        //                      select m).Take(1).OrderBy(m => m.ID).FirstOrDefault();

        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(getRequest);
        //    Console.WriteLine(jsonString);

        //    return jsonString;


        //}

        public JsonResult QuickBookGetRecordForPayment(int invoiceID, string bankTaxID, int companyID)
        {
            dynamic data = 0;
            try
            {
                using (Emerge.Services.BALInvoiceQuickBook a = new Services.BALInvoiceQuickBook())
                {
                    data = a.GetRecordForPayment(invoiceID, bankTaxID, companyID);
                }
                //jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(data);
            }
            catch
            {
            }

            return Json(data, JsonRequestBehavior.AllowGet);

        }


        public JsonResult QuickBookUpdatedRecordsData(string Tranid, string ListID, string companyName)
        {
            dynamic data = 0;
            try
            {
                using (Emerge.Services.BALInvoiceQuickBook a = new Services.BALInvoiceQuickBook())
                {
                    data = a.QuickBookUpdatedRecordsData(Tranid, ListID, companyName);
                }
                //jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(data);
            }
            catch
            {
            }

            return Json(data, JsonRequestBehavior.AllowGet);

        }




        //public string QuickBookGetRecordForPayment(int invoiceID, string bankTaxID, int companyID)
        //{
        //    string jsonString = string.Empty;
        //    try
        //    {
        //        Emerge.Services.BALInvoiceQuickBook a = new Services.BALInvoiceQuickBook();
        //        dynamic data = a.GetRecordForPayment(invoiceID, bankTaxID, companyID);

        //        jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(data);
        //    }
        //    catch
        //    {
        //    }

        //    return jsonString;

        //}



        public JsonResult QuickBookGetRecordForPayment_discount(long invoiceID, int companyID)
        {
            dynamic data = 0;
            try
            {
                using (Emerge.Services.BALInvoiceQuickBook a = new Services.BALInvoiceQuickBook())
                {
                    data = a.GetRecordForPayment_discount(invoiceID, companyID);
                }
                //jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(data);
            }
            catch
            {
            }

            return Json(data, JsonRequestBehavior.AllowGet);

        }




        //public string QuickBookUpdateRequestByID(int requestID, string ListID, string updateType, bool status, string txnID, string refNo)
        //{
        //    string jsonString = string.Empty;
        //    try
        //    {
        //        Emerge.Services.BALInvoiceQuickBook a = new Services.BALInvoiceQuickBook();
        //        dynamic data = a.updateRequestByID(requestID, ListID, updateType, status, txnID, refNo);

        //        jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(data);
        //    }
        //    catch
        //    {
        //    }

        //    return jsonString;

        //}



        public JsonResult QuickBookUpdateRequestByID(int requestID, string ListID, string updateType, bool status, string txnID, string refNo)
        {
            bool users=false;
            using (Emerge.Services.BALInvoiceQuickBook a = new Services.BALInvoiceQuickBook())
            {
                users = a.updateRequestByID(requestID, ListID, updateType, status, txnID, refNo);                
            }
            return Json(users, JsonRequestBehavior.AllowGet);
        }


        public JsonResult QuickBookGetRecord_discountPerOrder(int companyID, int month, int year, int invoiceID)
        {
            dynamic data = 0;
            try
            {
                using (Emerge.Services.BALInvoiceQuickBook a = new Services.BALInvoiceQuickBook())
                {
                    data = a.GetRecord_discountPerOrder(companyID, month, year, invoiceID);
                }
                // jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(data);
            }
            catch
            {
            }

            return Json(data, JsonRequestBehavior.AllowGet);

        }
        public JsonResult InvoiceLevelDiscountQuickBook(int fkInvoiceId)
        {
            dynamic DATA = 0;

            try
            {

                using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
                {
                    DATA = ObjDALDataContext.InvoiceLevelDiscountQuickBook(fkInvoiceId).ToList<InvoiceLevelDiscountQuickBookResult>();

                }



            }
            catch //(Exception ex)
            {


            }


            return Json(DATA, JsonRequestBehavior.AllowGet);

        }

        public JsonResult QuickBookGetAllRecord(int CompanyId, int BillingMonth, int BillingYear, int invoiceID)
        {
            dynamic data = 0;
            try
            {
                using (Emerge.Services.BALInvoiceQuickBook a = new Services.BALInvoiceQuickBook())
                {
                    data = a.getAllRecord(CompanyId, BillingMonth, BillingYear, invoiceID);
                }
                //jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(data);
            }
            catch
            {
            }

            return Json(data, JsonRequestBehavior.AllowGet);

        }


        public JsonResult QuickBookCreateInvoiecGetRecord(int invoiceID, int companyID)
        {
            dynamic DATA = 0;

            try
            {

                using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
                {
                    DATA = ObjDALDataContext.InvoiceRecordForQuickBook(invoiceID, companyID).ToList<InvoiceRecordForQuickBookResult>();

                    // jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(DATA);
                }



            }
            catch// (Exception ex)
            {


            }


            return Json(DATA, JsonRequestBehavior.AllowGet);

        }

        public JsonResult QuickBookCompanyInformation(string companyName)
        {
            dynamic DATA = 0;

            try
            {

                using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
                {
                    DATA = ObjDALDataContext.QuickBookCompanyInformation(companyName).ToList<Proc_QuickBookCompanyInformationResult>();

                    // jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(DATA);
                }



            }
            catch// (Exception ex)
            {


            }


            return Json(DATA, JsonRequestBehavior.AllowGet);

        }


        public JsonResult QuickBookErrorInsertion(string Error)
        {
            dynamic DATA = 0;

            try
            {

                using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
                {
                    DATA = ObjDALDataContext.QuickBookAddError(Error);

                    // jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(DATA);
                }



            }
            catch// (Exception ex)
            {


            }


            return Json(DATA, JsonRequestBehavior.AllowGet);

        }



     










    }
}
