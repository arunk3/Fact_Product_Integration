﻿using Emerge.Data;
using Emerge.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Emerge.Controllers
{
    public class Service613NoticeController : Controller
    {
        //
        // GET: /NoticePdf613/

        public string Index()
        {
            GetAll613NoticeOrder();
            return "Success";
        }

        private void GetAll613NoticeOrder()
        {
            try
            {
                VendorServices.GeneralService.WriteLog("Sending 613 Notice Pdf Started at: " + DateTime.Now.ToString(), "NoticePdf613");
                using (EmergeDALDataContext DataContext = new EmergeDALDataContext())
                {
                    var AllorderList = DataContext.get613noticeorder().ToList();
                    if (AllorderList != null)
                    {
                        if (AllorderList.Count > 0)
                        {
                            VendorServices.GeneralService.WriteLog("Sending 613 Notice Pdf find at this time total " + AllorderList.Count + " records.", "NoticePdf613");
                            foreach (proc_get613noticeorderResult objSigleOrder in AllorderList)
                            {




                                ExecuteOrder(objSigleOrder);

                            }

                        }
                    }


                }
                VendorServices.GeneralService.WriteLog("Sending 613 NoticePdf End at: " + DateTime.Now.ToString(), "NoticePdf613");
                VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "NoticePdf613");
            }
            catch (Exception ex)
            {
                VendorServices.GeneralService.WriteLog("Sending 613 Notice Pdf End : " + DateTime.Now.ToString(), "NoticePdf613");
                VendorServices.GeneralService.WriteLog("Exceptions: " + ex.ToString(), "NoticePdf613");
                VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "NoticePdf613");
            }

        }


        private int ExecuteOrder(proc_get613noticeorderResult objSigleOrder)
        {


            int resultstatus = -1;
            string response = string.Empty;
            int? pkOrderId;
            int? pkOrderDetailid;
            string filteredReponse = string.Empty;
            string productCode = string.Empty;
            BALGeneral ObjBALGeneral = new BALGeneral();
            using (EmergeDALDataContext DataContext = new EmergeDALDataContext())
            {
                pkOrderId = objSigleOrder.pkOrderId;
                pkOrderDetailid = objSigleOrder.pkOrderDetailId;
                response = objSigleOrder.ResponseData;
                var ObjData_product = DataContext.tblProductPerApplications.FirstOrDefault(x => x.pkProductApplicationId == objSigleOrder.fkProductApplicationId);
                if (ObjData_product != null)
                {
                    productCode = ObjData_product.ProductCode;
                }

                filteredReponse = ObjBALGeneral.GetFilteredXMLDuringSending613Notice(productCode, response, pkOrderDetailid, DateTime.Now, false, false);

                VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "NoticePdf613");
                VendorServices.GeneralService.WriteLog("ExecuteOrder In : " + DateTime.Now.ToString(), "NoticePdf613");


                ObjBALGeneral.CheckIsNCRHit(pkOrderId, pkOrderDetailid, productCode, filteredReponse, true);
            }
            VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "NoticePdf613");
            VendorServices.GeneralService.WriteLog("ExecuteOrder Done : " + DateTime.Now.ToString(), "NoticePdf613");

            resultstatus = 0;
            return resultstatus;

        }

    }
}
