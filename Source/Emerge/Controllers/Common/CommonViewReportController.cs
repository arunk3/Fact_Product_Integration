﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using System.Web.Security;
using Emerge.Data;
using System.Xml.Linq;
using System.Text;
using Emerge.Common;
using System.Configuration;
using Emerge.Models;
using System.IO;
using Newtonsoft.Json;

namespace Emerge.Controllers.Common
{
    public class CommonViewReportController : Controller
    {
        public ActionResult EmergeReviewForOther(string Note, string Id)
        {
            BALEmergeReview ObjReview = new BALEmergeReview();
            int pkOrderResponseId = 0;
            int Result = 0;

            if (Id.Trim() != "")
            {
                if (Id.Trim() == "NRF")
                {
                    //ProductCode = HDNReportName.Value.Trim();
                    //Dictionary<string, string> dictCodepkOrderResponseId = (Dictionary<string, string>)Session["dictCodepkOrderResponseId"];
                    //string ResponseId_OrderDetailId = dictCodepkOrderResponseId.Where(kv => kv.Key == ProductCode).FirstOrDefault().Value.ToString();
                    //string[] ResponseId_OrderDetailIdArr = ResponseId_OrderDetailId.Split('_');
                    //pkOrderResponseId = new Guid(ResponseId_OrderDetailIdArr[0]);
                    //Guid pkOrderDetails = new Guid(ResponseId_OrderDetailIdArr[1]);
                }
                else
                {
                    pkOrderResponseId = Convert.ToInt32(Id);
                }
            }
            if (pkOrderResponseId != 0)
            {
                Result = ObjReview.UpdateOtherReviewedReport(pkOrderResponseId, Note);

            }
            return Json(Result);
        }

        public ActionResult UpdateReportReview(string myString)
        {
            try
            {
                List<CLSEmergeReview> myDeserializedObjList = (List<CLSEmergeReview>)Newtonsoft.Json.JsonConvert.DeserializeObject(myString, typeof(List<CLSEmergeReview>));
                if (myDeserializedObjList.Count > 0)
                {
                    List<string> ResponseIds = myDeserializedObjList.Select(x => x.OrderResponseId).Distinct().ToList();
                    List<CLSEmergeReview> reportData = null;
                    foreach (var item in ResponseIds)
                    {
                        reportData = myDeserializedObjList.Where(x => x.OrderResponseId == item).Distinct().ToList();
                        if (reportData.Count > 0)
                        {
                            for (int i = 0; i < reportData.Count; i++)
                            {
                                string OrderDetailId = reportData.ElementAt(i).OrderDetailId;
                                string ProductCode = reportData.ElementAt(i).ProductCode;
                                if (ProductCode == "NCRPLUS")
                                {
                                    ProductCode = "NCR+";
                                }
                                string cRIDs = reportData.ElementAt(i).CRIDs;// int 108 changes 
                                string possibleMatches = reportData.ElementAt(i).PossibleMatches;// int 108 changes 
                                string IsDeleted = reportData.ElementAt(i).IsDeleted ?? "off";
                                string NodeToEdit = reportData.ElementAt(i).NodeToEdit;
                                string ReviewNote = reportData.ElementAt(i).ReviewNote;
                                string OrderResponseId = reportData.ElementAt(i).OrderResponseId;
                                string Is6MonthOld_Str = reportData.ElementAt(i).IsSixMonthOld;
                                bool Is6MonthOld = Convert.ToBoolean(Is6MonthOld_Str);
                                int pkOrderDetailId = Convert.ToInt32(OrderDetailId);
                                int pkOrderResponseId = Convert.ToInt32(OrderResponseId);
                                string ElementToEdit = "";
                                if (ProductCode.ToLower() != "tdr")
                                    ElementToEdit = "";
                                if (IsDeleted.ToLower() == "on")
                                    IsDeleted = "1";
                                else
                                    IsDeleted = "0";
                                string[] ProductsWithTextForReview = new string[] { "BS", "BR", "CDLIS", "ER", "DB", "DLS", "OIG", "PL", "RAL", "RPL", "MVR", "MVS", "EEI", "EBP", "ECR", "UCC", "EDV", "EMV", "WCV", "PLV", "PRV", "NCR4", "SNS", "NCR2" };

                                if (ProductsWithTextForReview.Contains(ProductCode))
                                {
                                    UpdateResponseAfterReview_4Text(pkOrderResponseId, ReviewNote);
                                }
                                else
                                {
                                    UpdateResponseAfterReviewOptimized(Convert.ToInt32(NodeToEdit), ReviewNote, IsDeleted, ProductCode, pkOrderDetailId, pkOrderResponseId, ElementToEdit, Is6MonthOld, cRIDs, possibleMatches);
                                    // int 108 changes 
                                }
                            }
                            if (reportData.FirstOrDefault().ProductCode == "NCRPLUS")
                            {
                                reportData.FirstOrDefault().ProductCode = "NCR+";
                            }
                            if (Session["" + reportData.FirstOrDefault().ProductCode + "_" + reportData.FirstOrDefault().OrderDetailId + ""] != null)
                            {
                                string FinalResponse = Convert.ToString(Session["" + reportData.FirstOrDefault().ProductCode + "_" + reportData.FirstOrDefault().OrderDetailId + ""]);
                                BALEmergeReview ObjReview = new BALEmergeReview();
                                ObjReview.UpdateFinalResponse(Convert.ToInt32(reportData.FirstOrDefault().OrderResponseId), FinalResponse);
                                ObjReview.UpdateReportReviewStatus(Convert.ToInt32(reportData.FirstOrDefault().OrderDetailId), 1, string.Empty, Convert.ToBoolean(reportData.FirstOrDefault().IsSixMonthOld));
                            }
                        }
                    }
                }
            }
            catch// (Exception x)
            {
            }
            return Json(string.Empty);
        }
        public int UpdateResponseAfterReviewOptimized(int ReviewedNode, string ReviewNote, string IsDeleted, string ProductCode, int pkOrderDetailId, int pkOrderResponseId, string ElementToEdit, bool IsOld, string cRIDs, string possibleMatches)
        {
            string strLog = string.Empty;//For Report Log.
            string LogFilename = "XMLFilteration_XMLCaseDeletionLog_" + DateTime.Now.ToString("yyMMdd");
            strLog += DateTime.Now.ToString() + " Method Started : UpdateResponseAfterReviewOptimized for OrderDetailId " + Convert.ToString(pkOrderDetailId);
            int Status = 0;
            bool IsShowRawData;
            byte WatchListStatus = 0;
            BALEmergeReview ObjReview = new BALEmergeReview();
            XMLFilteration ObjXMLFilteration = new XMLFilteration();
            string ResponseData = "";
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    if (Session["" + ProductCode + "_" + pkOrderDetailId + ""] == null)
                    {
                        if (DX.tblOrderResponseDatas.Where(db => db.pkOrderResponseId == pkOrderResponseId).Count() > 0)
                        {
                            tblOrderResponseData ObjResponse = DX.tblOrderResponseDatas.Where(db => db.pkOrderResponseId == pkOrderResponseId).FirstOrDefault();
                            if (ObjResponse != null)
                            {
                                strLog += DateTime.Now.ToString() + "  Line No ----------------------140\n";
                                ResponseData = ObjResponse.ResponseData;
                                string FilteredResponse = ObjXMLFilteration.GetRemovedDismissedChargeXML(ResponseData, ProductCode, pkOrderDetailId);
                                strLog += DateTime.Now.ToString() + " GetRemovedDismissedChargeXML executed Line No ----------------------143\n";
                                XDocument HidedXML = ObjReview.GetHidedResponse(FilteredResponse, pkOrderDetailId, WatchListStatus, ProductCode, DateTime.Now, out IsShowRawData);
                                strLog += DateTime.Now.ToString() + " GetHidedResponse executed Line No ----------------------146\n";
                                string HidedXMLResponse = Convert.ToString(HidedXML);
                                FilteredResponse = ObjReview.CompleteFinalResponse(ResponseData, HidedXMLResponse, pkOrderResponseId, ProductCode);
                                ResponseData = FilteredResponse;
                                Session["" + ProductCode + "_" + pkOrderDetailId + ""] = FilteredResponse;
                            }
                        }
                        else
                        {
                            tblOrderResponseData_Archieve ObjResponse = DX.tblOrderResponseData_Archieves.Where(db => db.pkOrderResponseId == pkOrderResponseId).FirstOrDefault();
                            if (ObjResponse != null)
                            {
                                ResponseData = ObjResponse.ResponseData;
                                string FilteredResponse = ObjXMLFilteration.GetRemovedDismissedChargeXML(ResponseData, ProductCode, pkOrderDetailId);
                                XDocument HidedXML = ObjReview.GetHidedResponse(FilteredResponse, pkOrderDetailId, WatchListStatus, ProductCode, DateTime.Now, out IsShowRawData);
                                string HidedXMLResponse = Convert.ToString(HidedXML);
                                FilteredResponse = ObjReview.CompleteFinalResponseOptimized(ResponseData, HidedXMLResponse, pkOrderResponseId, ProductCode);
                                ResponseData = FilteredResponse;
                                Session["" + ProductCode + "_" + pkOrderDetailId + ""] = FilteredResponse;
                                //Session["" + ProductCode + "_" + pkOrderDetailId + ""] = ResponseData;
                            }
                        }
                    }
                    else
                        ResponseData = Convert.ToString(Session["" + ProductCode + "_" + pkOrderDetailId + ""]);
                    strLog += DateTime.Now.ToString() + " SelectNodeToUpdate executed Line No ----------------------170\n";
                    XDocument UpdatedXML = ObjReview.SelectNodeToUpdate(ReviewedNode, ProductCode, pkOrderResponseId, ResponseData, IsDeleted, ReviewNote, ElementToEdit, cRIDs, possibleMatches);
                    string UpdatedResponse = Convert.ToString(UpdatedXML);
                    string FinalResponse = ObjReview.CompleteFinalResponseOptimized(ResponseData, UpdatedResponse, pkOrderResponseId, ProductCode);
                    strLog += DateTime.Now.ToString() + " CompleteFinalResponse executed Line No ----------------------174\n";
                    Session["" + ProductCode + "_" + pkOrderDetailId + ""] = FinalResponse;
                    Status = 1;
                    BALGeneral.WriteLogForDeletion(strLog, LogFilename);
                }
            }
            catch (Exception ex)
            {
                strLog += DateTime.Now.ToString() + " Error--------------" + ex.InnerException + "\n" + ex.StackTrace + "\n";
                BALGeneral.WriteLogForDeletion(strLog, LogFilename);
            }
            return Status;

        }
        public ActionResult ProcessReportReview()
        {
            try
            {
                string OrderDetailId = Request.QueryString["OrderDetailId"];
                string ProductCode = Request.QueryString["ProductCode"];
                if (ProductCode == "NCRPLUS")
                {
                    ProductCode = "NCR+";
                }
                string IsDeleted = Request.QueryString["IsDeleted"];
                string NodeToEdit = Request.QueryString["NodeToEdit"];
                string ReviewNote = Request.Params["CurrentReviewNote"];
                string OrderResponseId = Request.Params["pkOrderResponseId"];
                string Is6MonthOld_Str = Request.QueryString["Is6MonthOld"];
                bool Is6MonthOld = Convert.ToBoolean(Is6MonthOld_Str);
                int pkOrderDetailId = Convert.ToInt32(OrderDetailId);
                int pkOrderResponseId = Convert.ToInt32(OrderResponseId);
                string ElementToEdit = Request.QueryString["ElementName"];
                if (ProductCode.ToLower() != "tdr")
                    ElementToEdit = "";
                if (IsDeleted.ToLower() == "on")
                    IsDeleted = "1";
                else
                    IsDeleted = "0";
                string[] ProductsWithTextForReview = new string[] { "BS", "BR", "CDLIS", "ER", "DB", "DLS", "OIG", "PL", "RAL", "RPL", "MVR", "MVS", "EEI", "EBP", "ECR", "UCC", "EDV", "EMV", "WCV", "PLV", "PRV", "NCR4", "SNS", "NCR2" };

                if (ProductsWithTextForReview.Contains(ProductCode))
                {
                    UpdateResponseAfterReview_4Text(pkOrderResponseId, ReviewNote);
                }
                else
                {
                    UpdateResponseAfterReview(Convert.ToInt32(NodeToEdit), ReviewNote, IsDeleted, ProductCode, pkOrderDetailId, pkOrderResponseId, ElementToEdit, Is6MonthOld);
                }
                //Response.Write("");
                //Response.End();
            }
            catch// (Exception x)
            {
            }
            return Json(string.Empty);
        }

        public int UpdateResponseAfterReview_4Text(int pkOrderResponseId, string Note)
        {
            int Status = 0;
            BALEmergeReview ObjReview = new BALEmergeReview();
            try
            {
                Status = ObjReview.UpdateOtherReviewedReport(pkOrderResponseId, Note);
            }
            catch
            { }
            return Status;
        }

        public int UpdateResponseAfterReview(int ReviewedNode, string ReviewNote, string IsDeleted, string ProductCode, int pkOrderDetailId, int pkOrderResponseId, string ElementToEdit, bool IsOld)
        {
            string strLog = string.Empty;//For Report Log.
            string LogFilename = "XMLFilteration_XMLCaseDeletionLog_" + DateTime.Now.ToString("yyMMdd");
            strLog += DateTime.Now.ToString() + " Method Started : UpdateResponseAfterReview for OrderDetailId " + Convert.ToString(pkOrderDetailId);
            int Status = 0;
            bool IsShowRawData;
            byte WatchListStatus = 0;

            BALEmergeReview ObjReview = new BALEmergeReview();
            XMLFilteration ObjXMLFilteration = new XMLFilteration();
            string ResponseData = "";
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    if (Session["" + ProductCode + "_" + pkOrderDetailId + ""] == null)
                    {
                        if (DX.tblOrderResponseDatas.Where(db => db.pkOrderResponseId == pkOrderResponseId).Count() > 0)
                        {
                            tblOrderResponseData ObjResponse = DX.tblOrderResponseDatas.Where(db => db.pkOrderResponseId == pkOrderResponseId).FirstOrDefault();
                            if (ObjResponse != null)
                            {
                                strLog += DateTime.Now.ToString() + "  Line No ----------------------258\n";
                                ResponseData = ObjResponse.ResponseData;
                                string FilteredResponse = ObjXMLFilteration.GetRemovedDismissedChargeXML(ResponseData, ProductCode, pkOrderDetailId);
                                strLog += DateTime.Now.ToString() + " GetRemovedDismissedChargeXML executed Line No ----------------------261\n";
                                XDocument HidedXML = ObjReview.GetHidedResponse(FilteredResponse, pkOrderDetailId, WatchListStatus, ProductCode, DateTime.Now, out IsShowRawData);
                                strLog += DateTime.Now.ToString() + " GetHidedResponse executed Line No ----------------------263\n";
                                string HidedXMLResponse = Convert.ToString(HidedXML);
                                FilteredResponse = ObjReview.CompleteFinalResponse(ResponseData, HidedXMLResponse, pkOrderResponseId, ProductCode);
                                ResponseData = FilteredResponse;
                                Session["" + ProductCode + "_" + pkOrderDetailId + ""] = FilteredResponse;

                            }
                        }
                        else
                        {
                            tblOrderResponseData_Archieve ObjResponse = DX.tblOrderResponseData_Archieves.Where(db => db.pkOrderResponseId == pkOrderResponseId).FirstOrDefault();
                            if (ObjResponse != null)
                            {
                                ResponseData = ObjResponse.ResponseData;
                                string FilteredResponse = ObjXMLFilteration.GetRemovedDismissedChargeXML(ResponseData, ProductCode, pkOrderDetailId);
                                XDocument HidedXML = ObjReview.GetHidedResponse(FilteredResponse, pkOrderDetailId, WatchListStatus, ProductCode, DateTime.Now, out IsShowRawData);
                                string HidedXMLResponse = Convert.ToString(HidedXML);
                                FilteredResponse = ObjReview.CompleteFinalResponse(ResponseData, HidedXMLResponse, pkOrderResponseId, ProductCode);
                                ResponseData = FilteredResponse;
                                Session["" + ProductCode + "_" + pkOrderDetailId + ""] = FilteredResponse;
                                //Session["" + ProductCode + "_" + pkOrderDetailId + ""] = ResponseData;
                            }
                        }
                    }
                    else
                        ResponseData = Convert.ToString(Session["" + ProductCode + "_" + pkOrderDetailId + ""]);

                    strLog += DateTime.Now.ToString() + " SelectNodeToUpdate executed Line No ----------------------290\n";
                    XDocument UpdatedXML = ObjReview.SelectNodeToUpdate(ReviewedNode, ProductCode, pkOrderResponseId, ResponseData, IsDeleted, ReviewNote, ElementToEdit, "0", "0");
                    string UpdatedResponse = Convert.ToString(UpdatedXML);
                    string FinalResponse = ObjReview.CompleteFinalResponse(ResponseData, UpdatedResponse, pkOrderResponseId, ProductCode);
                    strLog += DateTime.Now.ToString() + " CompleteFinalResponse executed Line No ----------------------294\n";
                    Session["" + ProductCode + "_" + pkOrderDetailId + ""] = FinalResponse;
                    ObjReview.UpdateReportReviewStatus(pkOrderDetailId, 1, string.Empty, IsOld);

                    Status = 1;
                    BALGeneral.WriteLogForDeletion(strLog, LogFilename);
                }
            }
            catch
            {
            }
            return Status;

        }

        #region Add Review and Help Comments

        public ActionResult SendEMEditMailRR(int pkOrderDetailId, string txtSuggestionsRR, string hdnIs6MonthOld, string HDNReviewDiffer,
           string HDNReviewSubject, string HDNReportName, string OrderNo, string ApplicantNameForEmail, string MiddleName, string RequestRefPage = null, string OrderApplicantEmail = null)
        {

            if (OrderApplicantEmail != null)
            {
                OrderApplicantEmail = Convert.ToString(OrderApplicantEmail.Replace("\n", "").TrimStart().TrimEnd());

            }

            string fileName = "Reviewmail_" + DateTime.Now.ToString("yyMMdd");
            string sentMailLog = "CommonViewReportController---->start SendEMEditMailRR--------Order No " + OrderNo;
            BALGeneral.WriteLogForDeletion(sentMailLog, fileName);
            int UpdatedStatus = 0;
            byte Status = 1;
            string Middle_Name = string.Empty;
            string UMiddle_Name = string.Empty;
            if (string.IsNullOrEmpty(MiddleName) == false)
            {
                Middle_Name = ", This applicant has a middle name or initial on file : " + MiddleName;
                UMiddle_Name = "Middle_Name: " + MiddleName;
            }
            else
            {
                Middle_Name = MiddleName;
                UMiddle_Name = MiddleName;
            }




            BALEmergeReview ObjReview = new BALEmergeReview();
            if (HDNReviewDiffer == "0")
            {
                sentMailLog = "if (HDNReviewDiffer == 0) for " + OrderNo;
                BALGeneral.WriteLogForDeletion(sentMailLog, fileName);

                UpdatedStatus = ObjReview.UpdateHelpWithOrder(pkOrderDetailId, 1, txtSuggestionsRR.Trim() + UMiddle_Name, Convert.ToBoolean(hdnIs6MonthOld));

                if (UpdatedStatus == 1)
                {
                    sentMailLog = "if (UpdatedStatus == 1) and SendMailRequestingReview method call for " + OrderNo;
                    BALGeneral.WriteLogForDeletion(sentMailLog, fileName);

                    Status = SendMailRequestingReview(HDNReviewDiffer, txtSuggestionsRR + Middle_Name, HDNReviewSubject, HDNReportName, OrderNo, ApplicantNameForEmail.Replace("_", " "), RequestRefPage, OrderApplicantEmail);
                    sentMailLog = "if (UpdatedStatus == 1) and back into the parent SendMailRequestingReview method call for " + OrderNo;
                    BALGeneral.WriteLogForDeletion(sentMailLog, fileName);
                }
            }
            else
            {
                UpdatedStatus = ObjReview.UpdateReportReviewStatus(pkOrderDetailId, 1, txtSuggestionsRR.Trim() + UMiddle_Name, Convert.ToBoolean(hdnIs6MonthOld));
                if (UpdatedStatus == 1)
                {
                    sentMailLog = "if HDNReviewDiffer <> 0  SendMailRequestingReview method call for " + OrderNo;
                    BALGeneral.WriteLogForDeletion(sentMailLog, fileName);
                    Status = SendMailRequestingReview(HDNReviewDiffer, txtSuggestionsRR + Middle_Name, HDNReviewSubject, HDNReportName, OrderNo, ApplicantNameForEmail.Replace("_", " "), RequestRefPage, OrderApplicantEmail);
                }
            }
            string Message = string.Empty;
            if (Status == 1)
            {
                sentMailLog = "if (Status == 1) then submitted successfully  for " + OrderNo;
                BALGeneral.WriteLogForDeletion(sentMailLog, fileName);

                Message = "<span class='SuccessMsg'>Your Emerge Review has been submitted successfully.</span>";
            }
            else
            {
                sentMailLog = "if (Status <> 1) then Some Error Occurred. Please Try Again. for " + OrderNo;
                BALGeneral.WriteLogForDeletion(sentMailLog, fileName);

                Message = "<span class='ErrorMsg'>Some Error Occurred. Please Try Again.</span>";
            }
            return Json(Message);
        }

        private byte SendMailRequestingReview(string HDNReviewDiffer, string txtSuggestionsRR, string HDNReviewSubject, string HDNReportName, string OrderNo, string ApplicantNameForEmail, string RequestRefPage = null, string OrderApplicantEmail = null)
        {
            string fileName = "Reviewmail_" + DateTime.Now.ToString("yyMMdd");
            string sentMailLog = "started SendMailRequestingReview method call with OrderNo " + OrderNo;
            BALGeneral.WriteLogForDeletion(sentMailLog, fileName);
            string emailText = "";
            string MessageBody = "";
            int PkTemplateId = 0;
            StringBuilder Comments = new StringBuilder();
            Comments.Append(" <table width=\"97%\"><tr><td>Report ID : </td><td>" + OrderNo + "</td>"
                + "</tr> <tr><td>Report Name : </td><td>" + HDNReportName + "</td></tr>"
       + "<tr><td>User : </td><td>" + Session["UserFullName"].ToString() + " (<b>UserName : </b>" + User.Identity.Name + ") " + "</td></tr>"
       + " <tr><td valign=\"top\"  style=\"width:150px;\">Additional Comments :</td><td>" + txtSuggestionsRR.Trim() + "</td></tr>  </table>");
            string BoxHeaderText = string.Empty;
            BoxHeaderText = HDNReviewSubject;
            BALGeneral ObjBALGeneral = new BALGeneral();
            //  string WebsiteLogo = "https://www.intelifi.com/emerge/Resources/Upload/Images/emerge-logo--no-shadows-(small).gif";
            string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/emerge-logo--no-shadows-(small).gif";
            if (HDNReviewDiffer == "0")
            {
                PkTemplateId = Convert.ToInt32(EmailTemplates.HelpWithOrder);
            }
            else
            {
                PkTemplateId = Convert.ToInt32(EmailTemplates.EmergeReview);

            }
            //For email status//
           // BALGeneral objbalgenral = new BALGeneral();
            //MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
            //Guid UserId = Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
            //int pktemplateid = PkTemplateId;
           // Guid user_ID = UserId;
           // int fkcompanyuserid = 0;
            //bool email_status = true;//objbalgenral.UserEmailStatusCheck(user_ID, pktemplateid, fkcompanyuserid);


            var emailContent = ObjBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, PkTemplateId);// "Emerge Suggestions"
            emailText = emailContent.TemplateContent;

            BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
            #region  For New Style SystemTemplate
            string Bookmarkfile = string.Empty;
            string bookmarkfilepath = string.Empty;

            Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
            Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
            Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
            emailText = Bookmarkfile;
            #endregion


            #region New way Bookmark


            Bookmarks objBookmark = new Bookmarks();

            objBookmark.EmailContent = emailText;

            objBookmark.Subject = BoxHeaderText;
            if (ApplicantNameForEmail != null)
            {
                objBookmark.ApplicantName = ApplicantNameForEmail;/* Session["UserFullName"].ToString();*/
            }
            objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
            objBookmark.UserCompany = ApplicantNameForEmail;

            if (RequestRefPage != null && OrderApplicantEmail != null)
            {
                objBookmark.UserEmail = Session["UserFullName"].ToString() + " (<b>UserName : </b>" + Convert.ToString(OrderApplicantEmail) + ") ";
            }
            else
            {
                objBookmark.UserEmail = Session["UserFullName"].ToString() + " (<b>UserName : </b>" + User.Identity.Name + ") ";
            }





            objBookmark.EmergeAssistComment = Comments.ToString();
            objBookmark.EmergeReviewComment = Comments.ToString();
            objBookmark.Suggestion = Comments.ToString();

            if (RequestRefPage != null && OrderApplicantEmail != null)
            {
                MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Utility.GetUID(Convert.ToString(OrderApplicantEmail)));

            }
            else
            {
                MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Utility.GetUID(User.Identity.Name.ToString()));
            }



            #endregion
            //For email status//
            string MsgBeforeEmail = "Emerge Reviewed for Order No:-" + OrderNo;

            string UserIdentityName = string.Empty;
            if (RequestRefPage != null && OrderApplicantEmail != null)
            {
                UserIdentityName = OrderApplicantEmail;
            }
            else
            {
                UserIdentityName = User.Identity.Name;
            }




            if (HDNReviewDiffer != "0")
            {
                WriteReviewStatusInTextFile(MsgBeforeEmail, UserIdentityName);
            }

            string successAdmin = string.Empty;

            sentMailLog = "started BALEmergeReview.SendMail method call with OrderNo " + OrderNo;
            BALGeneral.WriteLogForDeletion(sentMailLog, fileName);
            successAdmin = BALEmergeReview.SendMail(ConfigurationManager.AppSettings["ReportReviewMail"].ToString(), UserIdentityName, string.Empty, ConfigurationManager.AppSettings["ReportReviewFromAddress"].ToString(), MessageBody, BoxHeaderText);
            sentMailLog = "Back to parent method BALEmergeReview.SendMail method call with OrderNo " + OrderNo;
            BALGeneral.WriteLogForDeletion(sentMailLog, fileName);

            sentMailLog = "HDNReviewDiffer : " + Convert.ToString(HDNReviewDiffer);
            BALGeneral.WriteLogForDeletion(sentMailLog, fileName);

            if (HDNReviewDiffer == "0")
            {
                if (successAdmin == "Success")
                {
                    sentMailLog = "HDNReviewDiffer  = 0 andm successAdmin = Success then return for " + OrderNo;
                    BALGeneral.WriteLogForDeletion(sentMailLog, fileName);
                    return 1;
                }
                else
                {
                    sentMailLog = "HDNReviewDiffer  = 0 andm successAdmin <> Success then return 2 for " + OrderNo;
                    BALGeneral.WriteLogForDeletion(sentMailLog, fileName);
                    return 2;
                }
            }
            else
            {
                if (successAdmin == "Success")
                {
                    sentMailLog = "HDNReviewDiffer  <> 0 andm successAdmin = Success alert message Order No : " + Convert.ToString(OrderNo) + " Message has been sent";
                    BALGeneral.WriteLogForDeletion(sentMailLog, fileName);
                    string SucessMsg = "Email Successfully Sent for Order No:-" + OrderNo;
                    WriteReviewStatusInTextFile(SucessMsg, ConfigurationManager.AppSettings["ReportReviewMail"].ToString());
                    return 1;
                }
                else
                {
                    sentMailLog = "HDNReviewDiffer  <> 0 andm successAdmin <> Success alert message Order No : " + Convert.ToString(OrderNo) + " Mail sending Fail";
                    BALGeneral.WriteLogForDeletion(sentMailLog, fileName);
                    string UnSucessMsg = "Email Sending Failed for Order No:-" + OrderNo;
                    UnSucessMsg = UnSucessMsg + " Error:" + successAdmin;
                    WriteReviewStatusInTextFile(UnSucessMsg, ConfigurationManager.AppSettings["ReportReviewMail"].ToString());
                    return 2;
                }
            }
        }


        #endregion

        #region Send Email to activate Report

        public JsonResult SendRequestToActivateReport(string hidInActiveReport, string lblSubject, string lblMailFrom)
        {
            string emailText = "";
            string MessageBody = "";
            string CurrentUserCompanyName = string.Empty;
            string Message = string.Empty;
            try
            {
                BALGeneral ObjBALGeneral = new BALGeneral();
                string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;

                var emailContent = ObjBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, Convert.ToInt32(EmailTemplates.ReportRequest));//"Report Request"
                emailText = emailContent.TemplateContent;
                #region New way Bookmark

                BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                Bookmarks objBookmark = new Bookmarks();

                if (Session["CurrentUserCompanyName"] != null)
                {
                    CurrentUserCompanyName = Session["CurrentUserCompanyName"].ToString();
                }


                #region For New Style SystemTemplate
                string Bookmarkfile = string.Empty;
                string bookmarkfilepath = string.Empty;

                Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                emailText = Bookmarkfile;
                #endregion

                objBookmark.EmailContent = emailText;

                objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                objBookmark.UserCompany = CurrentUserCompanyName;
                objBookmark.UserEmail = Session["UserFullName"].ToString() + " (<b>UserName : </b>" + User.Identity.Name + ") ";
                objBookmark.Suggestion = hidInActiveReport;

                MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Utility.GetUID(User.Identity.Name.ToString()));

                #endregion

                MessageBody = emailText.
                            Replace("%WebsiteLogo%", "<img src='" + WebsiteLogo + "' />").
                            Replace("%UserCompany%", CurrentUserCompanyName).
                            Replace("%UserEmail%", Session["UserFullName"].ToString() + " (<b>UserName : </b>" + User.Identity.Name + ") ").
                            Replace("%report%", hidInActiveReport);



                bool success = Utility.SendMail(BALGeneral.GetGeneralSettings().SupportEmailId, string.Empty, string.Empty, lblMailFrom, MessageBody, lblSubject);
                if (success == true)
                {
                    Message = "<span class='SuccessMsg'>Your request to activate the Emerge Review feature for " + hidInActiveReport + " is sent successfully.</span>";
                }
                else
                {
                    Message = "<span class='ErrorMsg'>Some error occured , please try again.</span>";
                }
            }
            catch (Exception)
            {
                Message = "<span class='ErrorMsg'>Error occured , please try again.</span>";
            }
            return Json(Message);
        }

        #endregion

        #region Consent

        public FilePathResult DownloadConsent_FeaturedFile(string FPathTitle)
        {

            string FileName = FPathTitle;
            string Title = "ConsentForm";
            string FilePath = Request.PhysicalApplicationPath + "Resources/Upload/ConsentForm/" + FileName;
            string fileext = System.IO.Path.GetExtension(FileName);
            string CType = "application/" + fileext.Replace(".", "");
            //string fileName = Path.GetFileName(FileName);

            string strFileTitle = Title + fileext;

            return File(FilePath, CType, strFileTitle);


        }

        public ActionResult btnContinue_Click(FormCollection Form, string C)
        {
            string[] farray = new string[] { };
            string StrResult = "";
            List<tblOrderDetail> ObjCollOrderDetail = new List<tblOrderDetail>();
            tblOrderDetail ObjOrderDetail = new tblOrderDetail();
            var ConsentFilename = Request.Form["ConsentFilename"];
            var pkOrderDetailId = Request.Form["pkOrderDetailId"];
            var OrderId = Request.Form["OrderId"];
            farray = ConsentFilename.Split(',');
            var garray = pkOrderDetailId.Split(',');
            int J = 0;
            for (int i = 0; i < farray.Length; i++)
            {

                ObjOrderDetail = new tblOrderDetail();
                if (farray[i] == "")
                {

                    if (Request.Files != null)
                    {

                        HttpPostedFileBase hpf = Request.Files[J];

                        if (hpf.ContentLength > 0)
                        {

                            string sExt = Path.GetExtension(hpf.FileName).ToLower();
                            string sFileName = DateTime.Now.ToFileTime().ToString() + sExt;

                            //string sFilePath = "";
                            //sFilePath = HttpContext.Server.MapPath("~/Resources/Upload/ConsentForm/");
                            if (System.IO.File.Exists(HttpContext.Server.MapPath(@"~\Resources\Upload\ConsentForm\") + sFileName))
                            {
                                try
                                {
                                    System.IO.File.Delete(HttpContext.Server.MapPath(@"~\Resources\Upload\ConsentForm\") + sFileName);
                                }
                                catch { }
                            }


                            hpf.SaveAs(HttpContext.Server.MapPath(@"~\Resources\Upload\ConsentForm\") + sFileName);
                            //string fPath = Server.MapPath("~/Resources/Upload/ConsentForm/") + sFileName;

                            ObjOrderDetail.pkOrderDetailId = Convert.ToInt32(garray[i]);
                            ObjOrderDetail.ConsentFileName = sFileName;
                            ObjOrderDetail.IsConsentEmailorFax = false;
                            ObjOrderDetail.IsConsentRequired = true;
                            ObjCollOrderDetail.Add(ObjOrderDetail);
                            J = J + 1;
                        }

                    }
                    else
                    {
                        ObjOrderDetail.pkOrderDetailId = Convert.ToInt32(pkOrderDetailId);
                        ObjOrderDetail.ConsentFileName = string.Empty;
                        ObjOrderDetail.IsConsentEmailorFax = true;
                        ObjOrderDetail.IsConsentRequired = true;
                        ObjCollOrderDetail.Add(ObjOrderDetail);
                    }
                }
                else
                {

                }

            }

            BALOrders ObjBalOrders = new BALOrders();
            int iResult = ObjBalOrders.UpdateConsentInfo(ObjCollOrderDetail);
            if (iResult == 1)
            {
                StrResult = "<span class='successmsg'>Information Updated Successfully</span>";


            }
            else
            {
                StrResult = "<span class='errormsg'>Problem while saving. Please try again</span>";
            }

            TempData["messages"] = StrResult;
            TempData["AdminConsent"] = "AdminConsent";
            if (Request.Url.ToString().ToLower().Contains("control"))
            {
                return RedirectToAction("../Control/ViewReports", new { _OId = OrderId });
            }
            else
            {
                return RedirectToAction("../Corporate/ViewReports", new { _OId = OrderId });
            }
        }
        #endregion

        public ActionResult AutoEmeregeSbmtMiddleName(int pkOrderId, string txtSuggestionsRR, string OrderNo, string ApplicantNameForEmail, string MiddleName)
        {
            string fileName = "Reviewmail_" + DateTime.Now.ToString("yyMMdd");
            string sentMailLog = "AutoEmeregeSbmtMiddleName for OrderNo " + OrderNo;
            BALGeneral.WriteLogForDeletion(sentMailLog, fileName);

            BALEmergeReview ObjReview = new BALEmergeReview();
            int UpdatedStatus = 0;
            byte Status = 1;
            string Middle_Name = string.Empty;
            string UMiddle_Name = string.Empty;
            string Message = string.Empty;
            if (string.IsNullOrEmpty(MiddleName) == false)
            {
                Middle_Name = " This applicant has a middle name or initial on file : " + MiddleName;
                UMiddle_Name = "Middle_Name: " + MiddleName;
            }
            else
            {
                Middle_Name = MiddleName;
                UMiddle_Name = MiddleName;
            }
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                tblOrderDetail ObjOrderDetails = DX.tblOrderDetails.Where(db => db.fkOrderId == pkOrderId && db.ReviewStatus == 1).FirstOrDefault();
                tblProductPerApplication objtblProductPerApplication = DX.tblProductPerApplications.Where(db => db.pkProductApplicationId == ObjOrderDetails.fkProductApplicationId).FirstOrDefault();
                if (ObjOrderDetails != null)
                {
                    UpdatedStatus = ObjReview.UpdateReportReviewStatus(ObjOrderDetails.pkOrderDetailId, 1, txtSuggestionsRR.Trim() + UMiddle_Name, false);
                    if (UpdatedStatus == 1)
                    {
                        sentMailLog = "AutoEmeregeSbmtMiddleName   if (UpdatedStatus == 1) for OrderNo " + OrderNo;
                        BALGeneral.WriteLogForDeletion(sentMailLog, fileName);
                        Status = SendAutoMailForReview(txtSuggestionsRR + Middle_Name, objtblProductPerApplication.ProductDisplayName, objtblProductPerApplication.ProductCode, OrderNo, ApplicantNameForEmail.Replace("_", " "));
                    }
                    if (Status == 1)
                    {
                        sentMailLog = "AutoEmeregeSbmtMiddleName    if (Status == 1) Your Middle Name has been submitted successfully. for OrderNo " + OrderNo;
                        BALGeneral.WriteLogForDeletion(sentMailLog, fileName);
                        Message = "<span class='SuccessMsg'>Your Middle Name has been submitted successfully.</span>";
                    }
                    else
                    {
                        sentMailLog = "AutoEmeregeSbmtMiddleName    if (Status == 1) Some Error Occurred. Please Try Again. for OrderNo " + OrderNo;
                        BALGeneral.WriteLogForDeletion(sentMailLog, fileName);
                        Message = "<span class='ErrorMsg'>Some Error Occurred. Please Try Again.</span>";
                    }
                }
            }
            return Json(Message);
        }

        private byte SendAutoMailForReview(string txtSuggestionsRR, string HDNReportName, string HDNReportCode, string OrderNo, string ApplicantNameForEmail)
        {
            string emailText = "";
            string MessageBody = "";
            int PkTemplateId = 0;
            string fileName = "Reviewmail_" + DateTime.Now.ToString("yyMMdd");
            string sentMailLog = "SendAutoMailForReview for OrderNo " + OrderNo;
            BALGeneral.WriteLogForDeletion(sentMailLog, fileName);

            StringBuilder Comments = new StringBuilder();
            Comments.Append(" <table width=\"97%\"><tr><td>Report ID : </td><td>" + OrderNo + "</td>"
                + "</tr> <tr><td>Report Name : </td><td>" + HDNReportName + "</td></tr>"
       + "<tr><td>User : </td><td>" + Session["UserFullName"].ToString() + " (<b>UserName : </b>" + User.Identity.Name + ") " + "</td></tr>"
       + " <tr><td valign=\"top\"  style=\"width:150px;\">Additional Comments :</td><td>Auto request for " + HDNReportCode + " report review." + txtSuggestionsRR.Trim() + "</td></tr>  </table>");
            string BoxHeaderText = string.Empty;
            BoxHeaderText = HDNReportCode + " Auto-Review Request - " + ApplicantNameForEmail;
            BALGeneral ObjBALGeneral = new BALGeneral();
            string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/emerge-logo--no-shadows-(small).gif";
            PkTemplateId = Convert.ToInt32(EmailTemplates.EmergeReview);
            //For email status//
            //BALGeneral objbalgenral = new BALGeneral();
            //MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
            //Guid UserId = Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
           // int pktemplateid = PkTemplateId;
           // Guid user_ID = UserId;
            //int fkcompanyuserid = 0;
            //bool email_status = true; // objbalgenral.UserEmailStatusCheck(user_ID, pktemplateid, fkcompanyuserid); 


            var emailContent = ObjBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, PkTemplateId);
            emailText = emailContent.TemplateContent;

            BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
            #region  For New Style SystemTemplate
            string Bookmarkfile = string.Empty;
            string bookmarkfilepath = string.Empty;

            Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
            Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
            Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
            emailText = Bookmarkfile;
            #endregion


            #region New way Bookmark


            Bookmarks objBookmark = new Bookmarks();

            objBookmark.EmailContent = emailText;

            objBookmark.Subject = BoxHeaderText;
            if (ApplicantNameForEmail != null)
            {
                objBookmark.ApplicantName = ApplicantNameForEmail;/* Session["UserFullName"].ToString();*/
            }
            objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
            objBookmark.UserCompany = ApplicantNameForEmail;
            objBookmark.UserEmail = Session["UserFullName"].ToString() + " (<b>UserName : </b>" + User.Identity.Name + ") ";
            objBookmark.EmergeAssistComment = Comments.ToString();
            objBookmark.EmergeReviewComment = Comments.ToString();
            objBookmark.Suggestion = Comments.ToString();

            MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Utility.GetUID(User.Identity.Name.ToString()));

            #endregion
            //For email status//
            string MsgBeforeEmail = "Emerge Reviewed for Order No:-" + OrderNo;
            string UserIdentityName = User.Identity.Name;
            WriteReviewStatusInTextFile(MsgBeforeEmail, UserIdentityName);

            string successAdmin = string.Empty;


            sentMailLog = "BALEmergeReview.SendMail for OrderNo " + OrderNo;
            BALGeneral.WriteLogForDeletion(sentMailLog, fileName);
            successAdmin = BALEmergeReview.SendMail(ConfigurationManager.AppSettings["ReportReviewMail"].ToString(), UserIdentityName, string.Empty, ConfigurationManager.AppSettings["ReportReviewFromAddress"].ToString(), MessageBody, BoxHeaderText);

            if (successAdmin == "Success")
            {
                sentMailLog = "BALEmergeReview.SendMail for Success OrderNo " + OrderNo;
                BALGeneral.WriteLogForDeletion(sentMailLog, fileName);
                string SucessMsg = "Email Successfully Sent for Order No:-" + OrderNo;
                WriteReviewStatusInTextFile(SucessMsg, ConfigurationManager.AppSettings["ReportReviewMail"].ToString());
                return 1;
            }
            else
            {
                sentMailLog = "BALEmergeReview.SendMail for Failed OrderNo " + OrderNo;
                BALGeneral.WriteLogForDeletion(sentMailLog, fileName);
                string UnSucessMsg = "Email Sending Failed for Order No:-" + OrderNo;
                UnSucessMsg = UnSucessMsg + " Error:" + successAdmin;
                WriteReviewStatusInTextFile(UnSucessMsg, ConfigurationManager.AppSettings["ReportReviewMail"].ToString());
                return 2;
            }
        }


        public void WriteReviewStatusInTextFile(string ReviewMsg, string UserIdentityName)
        {
            try
            {
                //List<string> strlst = new List<string>();
                StreamWriter SW;
                string savedPath = string.Empty;
                string fileName = string.Empty;
                DateTime dt = DateTime.Now;
                fileName = dt.ToString("yyMMdd");
                savedPath = HttpContext.Server.MapPath("~/Resources/Upload/EmergeReviewLog/" + fileName + ".txt");
                if (!System.IO.File.Exists(savedPath))
                {
                    savedPath = HttpContext.Server.MapPath("~/Resources/Upload/EmergeReviewLog/" + fileName + ".txt");
                    using(SW = System.IO.File.CreateText(savedPath))
                    {
                        SW.WriteLine("\n");
                        SW.WriteLine("****************************************");
                        SW.WriteLine(ReviewMsg);
                        SW.WriteLine("Emerge Review Order Identity:- " + UserIdentityName);
                        SW.WriteLine("Time of Event :- " + DateTime.Now.ToString());
                        SW.WriteLine("****************************************");
                        SW.Close();
                    }
                }
                else
                {
                    using(SW = System.IO.File.AppendText(savedPath))
                    {
                        SW.WriteLine("\n");
                        SW.WriteLine("****************************************");
                        SW.WriteLine(ReviewMsg);
                        SW.WriteLine("Emerge Review Order Identity:- " + UserIdentityName);
                        SW.WriteLine("Time of Event :- " + DateTime.Now.ToString());
                        SW.WriteLine("****************************************");
                        SW.Close();
                    }
                }               

            }
            catch { }
        }

        public ActionResult AddNullDOBReport(string hdnNullDOBAdd4NCR1)
        {
            int Result = 0;
            try
            {
                BALEmergeReview ObjBALEmergeReview = new BALEmergeReview();
                string[] AddedValue = hdnNullDOBAdd4NCR1.Split('_');
                string IsAdded = "true";
                List<int> SelectedNodes = new List<int>();
                for (int irow = 1; irow < AddedValue.Length; irow++)
                {
                    SelectedNodes.Add(Convert.ToInt32(AddedValue[irow]));
                }

                string ProductCode = "NCR1";
                int pkOrderDetails = Convert.ToInt32(AddedValue[0]);
                Result = ObjBALEmergeReview.AddNullDOBReport(SelectedNodes, IsAdded, ProductCode, pkOrderDetails);

            }
            catch (Exception)
            {

                throw;
            }
            return Json(Result);
        }

        public ActionResult AddEmailForViewBlockRecords(string orderId, string email)
        {
            bool IsCheckViewReport = false;
            int OrderId = 0;
            BALViewReport objBalViewReport = new BALViewReport();
            OrderId = Convert.ToInt32(orderId);
            IsCheckViewReport = objBalViewReport.AddEmailDetailINDBForViewBlockRecords(OrderId, email);
            return Json(new { IsCheckViewReportForBlock = IsCheckViewReport });
        }

        /// <summary>
        /// To Check entered email id is logged on user id.
        /// </summary>
        /// <param name="emailId"></param>
        /// <returns></returns>
        public ActionResult CheckEnteredEmailIdWithLoggedOnUser(string emailId)
        {
            return Json(new { CheckEmailId = User.Identity.Name.Equals(emailId) });
        }


    }
    public class CLSEmergeReview
    {
        public string ProductCode { get; set; }
        public string OrderDetailId { get; set; }
        public string IsSixMonthOld { get; set; }
        public string OrderResponseId { get; set; }
        public string IsDeleted { get; set; }
        public string NodeToEdit { get; set; }
        public string ReviewNote { get; set; }
        public string CRIDs { get; set; }// int 108 changes 
        public string PossibleMatches { get; set; }// int 108 changes 
    }
}
