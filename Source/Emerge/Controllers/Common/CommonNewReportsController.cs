﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Emerge.Data;
using Emerge.Models;
using System.Xml;
using Emerge.Common;
using System.Xml.Serialization;
using System.IO;
using System.Configuration;
using System.Text;
using System.Xml.Linq;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web.Services.Protocols;
using System.Data;

namespace Emerge.Controllers.Common
{
    public partial class CommonNewReportController : Controller
    { //
        // GET: /CommonNewReports/

        //Test
        //#region Save New Report

        //public ActionResult SaveNewReport()
        //{
        //    OrderInfo ObjOrderInfo = new OrderInfo();
        //    Xml ObjXml = new Xml();
        //    XDocument ObjXDocument = new XDocument();
        //    StringBuilder response = new StringBuilder();
        //    StringBuilder responseOptional = new StringBuilder();
        //    StringBuilder TieredHeadText = new StringBuilder();
        //    List<tblState> tblStateColl = new List<tblState>();

        //    XmlSerializer serializer = new XmlSerializer(typeof(EmergeReport));
        //    string FilePath = Server.MapPath(@"~\Resources\Upload\NewReportXML\" + Convert.ToString(Session["NewReportXMLPath"]));
        //    TextReader tr = new StreamReader(FilePath);
        //    EmergeReport ObjEmergeReport = new EmergeReport();
        //    using (TextReader textWriter = new StreamReader(FilePath))
        //    {
        //        ObjEmergeReport = (EmergeReport)serializer.Deserialize(textWriter);
        //    }
        //    serializer = null;

        //    Guid OrderId = Guid.Empty;
        //    Guid LocationId = (Session["pkLocationId"] == null) ? Guid.Empty : new Guid(Session["pkLocationId"].ToString());
        //    Guid CompanyUserId = (Session["CurrentUserId"] == null) ? Guid.Empty : new Guid(Session["CurrentUserId"].ToString());
        //    BALOrders ObjBALOrders = new BALOrders();

        //    List<tblOrderSearchedAliasPersonalInfo> ObjCollOrderSearchedAliasPersonalInfo = new List<tblOrderSearchedAliasPersonalInfo>();
        //    List<tblOrderSearchedAliasLocationInfo> ObjCollOrderSearchedAliasLocationInfo = new List<tblOrderSearchedAliasLocationInfo>();
        //    List<tblOrderSearchedAliasWorkInfo> ObjCollOrderSearchedAliasWorkInfo = new List<tblOrderSearchedAliasWorkInfo>();
        //    Dictionary<string, Guid> ObjCompnyProductInfo = new Dictionary<string, Guid>();

        //    // EmergeReport ObjEmergeReport = (EmergeReport)Session["EmergeReport"];
        //    if (Session["newapi"] == "fromapi")
        //    {
        //        ObjEmergeReport = ObjBALOrders.AddOrdersApi(ObjEmergeReport.ObjtblOrder,
        //        ObjEmergeReport.ObjCollProducts,
        //        ObjEmergeReport.ObjtblOrderSearchedData,
        //        ObjEmergeReport.ObjCollOrderSearchedEmploymentInfo,
        //        ObjEmergeReport.ObjCollOrderSearchedEducationInfo,
        //        ObjEmergeReport.ObjCollOrderSearchedPersonalInfo,
        //        ObjEmergeReport.objColltblOrderPersonalQtnInfo,
        //        ObjEmergeReport.ObjCollOrderSearchedLicenseInfo,
        //        ObjCollOrderSearchedAliasPersonalInfo,
        //        ObjCollOrderSearchedAliasLocationInfo,
        //        ObjCollOrderSearchedAliasWorkInfo,
        //        ObjCompnyProductInfo,
        //        ObjEmergeReport.ObjColltblOrderSearchedLiveRunnerInfo, ObjEmergeReport.ObjCollCLSConsentInfo,
        //        Utility.SiteApplicationId, ObjEmergeReport.IsNullDOB,
        //        ObjEmergeReport.IsLiveRunnerState, ObjEmergeReport.ObjTieredPackage, ObjEmergeReport.ObjOrderInfo, ObjEmergeReport.CompanyAccountNo, ObjEmergeReport.OrderCompanyName, ObjEmergeReport.PermissiblePurpose);
        //    }
        //    else
        //    {
        //        ObjEmergeReport = ObjBALOrders.AddOrders(ObjEmergeReport.ObjtblOrder,
        //        ObjEmergeReport.ObjCollProducts,
        //        ObjEmergeReport.ObjtblOrderSearchedData,
        //        ObjEmergeReport.ObjCollOrderSearchedEmploymentInfo,
        //        ObjEmergeReport.ObjCollOrderSearchedEducationInfo,
        //        ObjEmergeReport.ObjCollOrderSearchedPersonalInfo,
        //        ObjEmergeReport.objColltblOrderPersonalQtnInfo,
        //        ObjEmergeReport.ObjCollOrderSearchedLicenseInfo,
        //        ObjCollOrderSearchedAliasPersonalInfo,
        //        ObjCollOrderSearchedAliasLocationInfo,
        //        ObjCollOrderSearchedAliasWorkInfo,
        //        ObjCompnyProductInfo,
        //        ObjEmergeReport.ObjColltblOrderSearchedLiveRunnerInfo, ObjEmergeReport.ObjCollCLSConsentInfo,
        //        Utility.SiteApplicationId, ObjEmergeReport.IsNullDOB,
        //        ObjEmergeReport.IsLiveRunnerState, ObjEmergeReport.ObjTieredPackage, ObjEmergeReport.ObjOrderInfo, ObjEmergeReport.CompanyAccountNo, ObjEmergeReport.OrderCompanyName, ObjEmergeReport.PermissiblePurpose);
        //    }
        //    Session["newapi"] = null;
        //    if (ObjEmergeReport.ObjOrderInfo.OrderId != null)
        //    {
        //        OrderId = ObjEmergeReport.ObjOrderInfo.OrderId;
        //        using (EmergeDALDataContext dx = new EmergeDALDataContext())
        //        {
        //            ObjEmergeReport.ObjtblOrder = dx.tblOrders.Where(d => d.pkOrderId == OrderId).FirstOrDefault();
        //            ObjEmergeReport.ObjtblOrderSearchedData = dx.tblOrderSearchedDatas.Where(d => d.fkOrderId == OrderId).FirstOrDefault();
        //        }
        //    }

        //    if (!ObjEmergeReport.ObjOrderInfo.IsAutomatic)
        //    {
        //        #region Is Automatic False

        //        XmlDocument ObjXmlDocument = new XmlDocument();
        //        XDocument ObjXmlDoc = new XDocument();
        //        if (ObjEmergeReport.ObjTieredPackage != null && ObjEmergeReport.ObjOrderInfo.IsTiered && ObjEmergeReport.ObjOrderInfo.Tiered2Response != null && ObjEmergeReport.ObjOrderInfo.Tiered2Response != "")
        //        {

        //            ObjXmlDoc = XDocument.Parse(ObjEmergeReport.ObjOrderInfo.Tiered2Response);
        //            string XML_Value = GetRequiredXmlConditional(ObjXmlDoc.Document.ToString(), ObjEmergeReport.ObjOrderInfo.HostProductCode);
        //            ObjXDocument = XDocument.Parse(XML_Value);
        //            Dictionary<string, string> DictCounty = new Dictionary<string, string>();

        //            if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode != null)
        //            {
        //                ObjEmergeReport.ObjCollProducts.Add(ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode);
        //            }

        //            if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode != null)
        //            {
        //                ObjEmergeReport.ObjCollProducts.Add(ObjEmergeReport.ObjOrderInfo.OptionalProductCode);
        //            }

        //            #region Header Text

        //            List<tblProductPerApplication> ObjtblPPA = new List<tblProductPerApplication>();
        //            tblProductPerApplication ObjtblPPAs = new tblProductPerApplication();

        //            using (EmergeDALDataContext dx = new EmergeDALDataContext())
        //            {
        //                ObjtblPPA = dx.tblProductPerApplications.ToList<tblProductPerApplication>();
        //                string sHostname = "";
        //                string sTiered2 = "";
        //                string sOptional = "";
        //                ObjtblPPAs = ObjtblPPA.Where(d => d.pkProductApplicationId == ObjEmergeReport.ObjTieredPackage.Host).FirstOrDefault();
        //                if (ObjtblPPAs != null)
        //                {
        //                    sHostname = "[" + ObjtblPPAs.ProductCode + "] " + ObjtblPPAs.ProductDisplayName;
        //                }
        //                ObjtblPPAs = null;
        //                ObjtblPPAs = ObjtblPPA.Where(d => d.pkProductApplicationId == ObjEmergeReport.ObjTieredPackage.Tired2).FirstOrDefault();
        //                if (ObjtblPPAs != null)
        //                {
        //                    sTiered2 = "[" + ObjtblPPAs.ProductCode + "] " + ObjtblPPAs.ProductDisplayName;
        //                } ObjtblPPAs = null;
        //                ObjtblPPAs = ObjtblPPA.Where(d => d.pkProductApplicationId == ObjEmergeReport.ObjTieredPackage.TiredOptional).FirstOrDefault();
        //                if (ObjtblPPAs != null)
        //                {
        //                    sOptional = "[" + ObjtblPPAs.ProductCode + "] " + ObjtblPPAs.ProductDisplayName;
        //                }
        //                TieredHeadText.Append("<table>");
        //                TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Host Report:</td><td>" + sHostname + "</td><tr>");
        //                TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Tiered 2 Report:</td><td>" + sTiered2 + "</td><tr>");
        //                TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Tiered 2 Optional Report:</td><td>" + sOptional + "</td><tr>");
        //                TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Timeframe: </td><td>" + ObjEmergeReport.ObjTieredPackage.TimeFrame + " Year(s)</td><tr>");
        //                TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Conditional Variable:</td><td>" + ObjEmergeReport.ObjTieredPackage.Variable + "</td><tr>");
        //                TieredHeadText.Append("</table>");
        //            }
        //            #endregion

        //            #region HOST Response
        //            EmergeDALDataContext dx1 = new EmergeDALDataContext();
        //            if (ObjEmergeReport.ObjOrderInfo.HostProductCode == "PS")
        //            {
        //                try
        //                {
        //                    string SearchedStateName = string.Empty;
        //                    string SearchedStateCode = string.Empty;
        //                    string IsStateFound = string.Empty;
        //                    if (ObjEmergeReport.ObjtblOrderSearchedData.SearchedStateId != null)
        //                    {
        //                        SearchedStateName = (from d in dx1.tblStates
        //                                             where (d.pkStateId == ObjEmergeReport.ObjtblOrderSearchedData.SearchedStateId)
        //                                             select d.StateName).FirstOrDefault().ToString();
        //                        SearchedStateCode = (from d in dx1.tblStates
        //                                             where (d.pkStateId == ObjEmergeReport.ObjtblOrderSearchedData.SearchedStateId)
        //                                             select d.StateCode).FirstOrDefault().ToString();
        //                    }

        //                    int DictCount = 0;

        //                    #region Tiered CCR1

        //                    List<XElement> CreditFile = ObjXDocument.Descendants("CreditFile").ToList();
        //                    if (CreditFile != null && CreditFile.Count > 0)
        //                    {
        //                        List<XElement> PersonalData = CreditFile.Descendants("PersonalData").ToList();
        //                        if (PersonalData != null && PersonalData.Count > 0)
        //                        {
        //                            foreach (XElement xe in PersonalData)
        //                            {
        //                                XElement Region = xe.Descendants("Region").FirstOrDefault();
        //                                XElement County = xe.Descendants("County").FirstOrDefault();
        //                                if (Region != null && County != null)
        //                                {
        //                                    if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
        //                                        DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
        //                                }
        //                            }
        //                            if (DictCounty.Count > 0)
        //                            {
        //                                response.Append("<table cellpadding=\"3\" cellspacing=\"0\" width=\"100%\">");
        //                                response.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"3\">Counties (" + DictCounty.Count + ") found for " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + " " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + ":</td></tr>");
        //                                response.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"33%\"><td width=\"40%\">County[State]</td><td width=\"40%\">(ReportType)</td> <td width=\"20%\"><input type=\"checkbox\" onclick=\"selectunselectCounty();\" id=\"chk_SelectAllCounty\"  />Select All</td></tr>");
        //                                for (int i = 0; i < DictCounty.Count; i++)
        //                                {
        //                                    string CountyName = DictCounty.ElementAt(i).Key;
        //                                    BALCompanyType Objbalcompanytype = new BALCompanyType();
        //                                    string fee = Objbalcompanytype.Get_County_Fee(CountyName);
        //                                    if (fee == "0" || fee == "0.00")
        //                                    {
        //                                        fee = string.Empty;
        //                                    }
        //                                    else
        //                                    {
        //                                        fee = " County Fee" + " $" + fee;
        //                                    }
        //                                    string sDispState = "";
        //                                    response.Append("<tr>");
        //                                    response.Append("<td>" + DictCounty.ElementAt(i).Key + ", [" + DictCounty.ElementAt(i).Value + "]" + "<label style='color:red'>" + fee + "</label>" + "</td>");
        //                                    if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode != null)
        //                                    {
        //                                        if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Contains("_"))
        //                                            sDispState = "(" + ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Split('_').First() + ")";
        //                                        else
        //                                            sDispState = DictCounty.ElementAt(i).Value;
        //                                    }
        //                                    else
        //                                    {
        //                                        sDispState = DictCounty.ElementAt(i).Value;
        //                                    }




        //                                    response.Append("<td>" + sDispState + "</td>");
        //                                    response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk\" type=\"checkbox\" id=\"chk_" + DictCounty.ElementAt(i).Key + "_" + DictCounty.ElementAt(i).Value + "_County_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
        //                                    response.Append("<tr>");
        //                                }
        //                                if (DictCounty.ContainsKey("DALLAS"))
        //                                {
        //                                    int count = DictCounty.Count + 1;
        //                                    string CountyName = "DALLAS METRO";
        //                                    BALCompanyType Objbalcompanytype = new BALCompanyType();
        //                                    string fee = Objbalcompanytype.Get_County_Fee(CountyName);
        //                                    if (fee == "0" || fee == "0.00")
        //                                    {
        //                                        fee = string.Empty;
        //                                    }
        //                                    else
        //                                    {
        //                                        fee = " County Fee" + " $" + fee;
        //                                    }
        //                                    response.Append("<tr width=\"33%\">");
        //                                    response.Append("<td width=\"40%\">DALLAS METRO, [TX]" + "<label style='color:red'>" + fee + "</label>" + "</td>");
        //                                    response.Append("<td width=\"40%\">(CCR1)</td>");
        //                                    response.Append("<td width=\"20%\"><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk\" type=\"checkbox\" id=\"chk_DALLAS METRO_TX_County_" + count.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
        //                                    response.Append("<tr>");
        //                                }
        //                            }
        //                            response.Append("</table>");
        //                        }
        //                    }
        //                    #endregion

        //                    if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode != null)
        //                    {
        //                        if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("SCR"))
        //                        {
        //                            #region Optional SCR

        //                            if (CreditFile != null && CreditFile.Count > 0)
        //                            {
        //                                List<XElement> PersonalData = CreditFile.Descendants("PersonalData").ToList();
        //                                if (PersonalData != null && PersonalData.Count > 0)
        //                                {
        //                                    foreach (XElement xe in PersonalData)
        //                                    {
        //                                        XElement Region = xe.Descendants("Region").FirstOrDefault();
        //                                        XElement County = xe.Descendants("County").FirstOrDefault();
        //                                        if (Region != null && County != null)
        //                                        {
        //                                            if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
        //                                                DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
        //                                        }
        //                                    }
        //                                    if (DictCounty.Count > 0)
        //                                    {
        //                                        DictCount = DictCounty.Count;
        //                                        string RecordCount = "RecordFound";
        //                                        responseOptional.Append("<table cellpadding=\"3\" cellspacing=\"0\" width=\"100%\">");
        //                                        responseOptional.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"3\">States (" + RecordCount + ") found for " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + " " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + ":</td></tr>");
        //                                        responseOptional.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"33%\"><td width=\"40%\">State</td><td width=\"40%\"> (ReportType)</td> <td width=\"20%\"><input type=\"checkbox\" onclick=\"selectunselectState();\" id=\"chk_SelectAllStates\"  />Select All</td></tr>");
        //                                        for (int i = 0; i < DictCounty.Count; i++)
        //                                        {
        //                                            string StateName = (from d in dx1.tblStates
        //                                                                where (d.StateCode.ToLower() == DictCounty.ElementAt(i).Value.ToLower())
        //                                                                select d.StateName).FirstOrDefault().ToString();

        //                                            string stringToCheck = StateName;
        //                                            string check = string.Empty;
        //                                            BALGeneral objGeneral = new BALGeneral();
        //                                            string[] StateArray = objGeneral.GetDisabledStates().Select(d => d.StateName).ToArray();//{ "California", "Louisiana", "Nevada", "Ohio", "Vermont", "West Virginia", "Wyoming" };
        //                                            foreach (string x in StateArray)
        //                                            {
        //                                                if (x.ToLower().Contains(stringToCheck.ToLower()))
        //                                                {
        //                                                    check = "exist";
        //                                                }

        //                                            }

        //                                            if (check != "exist")
        //                                            {
        //                                                string sDispState = "";
        //                                                responseOptional.Append("<tr>");
        //                                                //responseOptional.Append("<td>" + DictCounty.ElementAt(i).Key + "</td>");
        //                                                if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode != null)
        //                                                {
        //                                                    if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("_"))
        //                                                        sDispState = "(" + ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Split('_').First() + ")";
        //                                                }
        //                                                else
        //                                                {
        //                                                    sDispState = "NA";// DictCounty.ElementAt(i).Value;
        //                                                }
        //                                                //string StateName = (from d in dx1.tblStates
        //                                                //            where (d.StateCode.ToLower() == DictCounty.ElementAt(i).Value.ToLower())
        //                                                //            select d.StateName).FirstOrDefault().ToString();
        //                                                if (StateName.ToLower() == SearchedStateName.ToLower())
        //                                                {
        //                                                    IsStateFound = "yes";
        //                                                }

        //                                                BALCompanyType Objbalcompanytype = new BALCompanyType();
        //                                                string StateFee = Objbalcompanytype.Get_State_Fee(StateName);
        //                                                if (StateFee == "0")
        //                                                {
        //                                                    StateFee = string.Empty;
        //                                                }
        //                                                else
        //                                                {
        //                                                    StateFee = " State Fee" + " $" + StateFee;
        //                                                }
        //                                                responseOptional.Append("<td>" + StateName + "<label style='color:red'>" + StateFee + "</label>" + "</td>");
        //                                                responseOptional.Append("<td>" + sDispState + "</td>");
        //                                                responseOptional.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_" + DictCounty.ElementAt(i).Key + "_" + DictCounty.ElementAt(i).Value + "_State_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
        //                                                responseOptional.Append("<tr>");
        //                                            }
        //                                        }
        //                                        if (IsStateFound == string.Empty)
        //                                        {
        //                                            string check = string.Empty;
        //                                            BALGeneral objGeneral = new BALGeneral();
        //                                            string[] StateArray = objGeneral.GetDisabledStates().Select(d => d.StateName).ToArray();//{ "California", "Louisiana", "Nevada", "Ohio", "Vermont", "West Virginia", "Wyoming" };
        //                                            foreach (string x in StateArray)
        //                                            {
        //                                                if (x.ToLower().Contains(SearchedStateName.ToLower()))
        //                                                {
        //                                                    check = "exist";
        //                                                }

        //                                            }
        //                                            if (check != "exist")
        //                                            {
        //                                                DictCount = DictCount + 1;
        //                                                string sDispState = "";
        //                                                responseOptional.Append("<tr>");
        //                                                sDispState = "(SCR)";
        //                                                BALCompanyType Objbalcompanytype = new BALCompanyType();
        //                                                string StateFee = Objbalcompanytype.Get_State_Fee(SearchedStateName);
        //                                                if (StateFee == "0" || StateFee == "0.00")
        //                                                {
        //                                                    StateFee = string.Empty;
        //                                                }
        //                                                else
        //                                                {
        //                                                    StateFee = " State Fee" + " $" + StateFee;
        //                                                }
        //                                                responseOptional.Append("<td>" + SearchedStateName + "<label style='color:red'>" + StateFee + "</label>" + "</td>");
        //                                                responseOptional.Append("<td>" + sDispState + "</td>");
        //                                                responseOptional.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_NoRecord_" + SearchedStateCode + "_State_" + DictCount + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
        //                                                responseOptional.Append("<tr>");
        //                                            }
        //                                        }
        //                                    }
        //                                    if (DictCounty.Count == 0)
        //                                    {
        //                                        responseOptional.Append("<table cellpadding=\"3\" cellspacing=\"0\" width=\"100%\">");
        //                                        responseOptional.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"3\">States (1) found for " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + " " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + ":</td></tr>");
        //                                        responseOptional.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"33%\"><td width=\"40%\">State</td><td width=\"40%\"> (ReportType)</td> <td width=\"20%\"><input type=\"checkbox\" onclick=\"selectunselectState();\" id=\"chk_SelectAllStates\"  />Select All</td></tr>");
        //                                        string sDispState = "";
        //                                        responseOptional.Append("<tr>");
        //                                        sDispState = "(SCR)";
        //                                        BALCompanyType Objbalcompanytype = new BALCompanyType();
        //                                        string StateFee = Objbalcompanytype.Get_State_Fee(SearchedStateName);
        //                                        if (StateFee == "0" || StateFee == "0.00")
        //                                        {
        //                                            StateFee = string.Empty;
        //                                        }
        //                                        else
        //                                        {
        //                                            StateFee = " State Fee" + " $" + StateFee;
        //                                        }
        //                                        responseOptional.Append("<td>" + SearchedStateName + "<label style='color:red'>" + StateFee + "</label>" + "</td>");
        //                                        responseOptional.Append("<td>" + sDispState + "</td>");
        //                                        responseOptional.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_NoRecord_" + SearchedStateCode + "_State_1\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
        //                                        responseOptional.Append("<tr>");
        //                                    }
        //                                    responseOptional.Append("</table>").Replace("RecordFound", DictCount.ToString());
        //                                }
        //                            }

        //                            #endregion
        //                        }
        //                        if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("NCR1"))
        //                        {
        //                            #region Optional NCR1


        //                            if (CreditFile != null && CreditFile.Count > 0)
        //                            {
        //                                List<XElement> PersonalData = CreditFile.Descendants("PersonalData").ToList();
        //                                if (PersonalData != null && PersonalData.Count > 0)
        //                                {
        //                                    if (DictCounty.Count > 0)
        //                                    {
        //                                        DictCount = DictCounty.Count;
        //                                        responseOptional.Append("<table cellpadding=\"3\" cellspacing=\"0\" width=\"100%\">");
        //                                        responseOptional.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"3\">Optional Report</td></tr>");
        //                                        responseOptional.Append("<tr width=\"33%\">");
        //                                        responseOptional.Append("<td width=\"40%\">National Criminal Report</td>");
        //                                        responseOptional.Append("<td width=\"40%\">(NCR1)</td>");
        //                                        responseOptional.Append("<td width=\"20%\"><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_NoRecord_" + SearchedStateCode + "_State_1\"  checked=\"checked\" /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
        //                                        responseOptional.Append("<tr>");
        //                                    }
        //                                    responseOptional.Append("</table>");
        //                                }
        //                            }

        //                            #endregion
        //                        }
        //                    }
        //                }
        //                catch
        //                {
        //                    response = new StringBuilder();
        //                    responseOptional = new StringBuilder();
        //                }
        //            }
        //            else if (ObjEmergeReport.ObjOrderInfo.HostProductCode == "CCR1")
        //            {
        //                string SearchedStateName = string.Empty;
        //                string SearchedStateCode = string.Empty;
        //                if (ObjEmergeReport.ObjtblOrderSearchedData.SearchedStateId != null)
        //                {
        //                    SearchedStateName = (from d in dx1.tblStates
        //                                         where (d.pkStateId == ObjEmergeReport.ObjtblOrderSearchedData.SearchedStateId)
        //                                         select d.StateName).FirstOrDefault().ToString();
        //                    SearchedStateCode = (from d in dx1.tblStates
        //                                         where (d.pkStateId == ObjEmergeReport.ObjtblOrderSearchedData.SearchedStateId)
        //                                         select d.StateCode).FirstOrDefault().ToString();
        //                }
        //                try
        //                {
        //                    List<XElement> stateregion = null;
        //                    List<XElement> reportrequested = null;
        //                    stateregion = ObjXDocument.Descendants("Region").ToList();
        //                    if (stateregion != null)
        //                    {
        //                        if (stateregion.Count == 0)
        //                        {
        //                            stateregion = ObjXDocument.Descendants("State").ToList();
        //                        }
        //                        if (stateregion.Count == 0)
        //                        {
        //                            reportrequested = ObjXDocument.Descendants("report-requested").ToList();
        //                            if (reportrequested != null && reportrequested.Count() > 0)
        //                            {
        //                                stateregion = reportrequested.Descendants("state").ToList();
        //                            }
        //                        }
        //                    }

        //                    if (stateregion != null && stateregion.Count > 0)
        //                    {

        //                        #region Tiered SCR

        //                        if (stateregion != null && stateregion.Count > 0)
        //                        {
        //                            foreach (XElement xe in stateregion)
        //                            {
        //                                if (xe != null)
        //                                {
        //                                    if (DictCounty.Where(d => d.Key == xe.Value).Count() == 0)
        //                                        DictCounty.Add(xe.Value.ToString(), xe.Value.ToString());
        //                                }
        //                            }
        //                            if (DictCounty.Count > 0)
        //                            {
        //                                response.Append("<table cellpadding=\"3\" cellspacing=\"0\" width=\"100%\">");
        //                                response.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"3\">States (&variable) found for " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + " " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + ":</td></tr>");
        //                                response.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"40%\"><td>State</td><td width=\"40%\"> (ReportType)</td> <td width=\"20%\"><input type=\"checkbox\" onclick=\"selectunselectState();\" id=\"chk_SelectAllStates\"  />Select All</td></tr>");
        //                                for (int i = 0; i < DictCounty.Count; i++)
        //                                {
        //                                    string StateName = (from d in dx1.tblStates
        //                                                        where (d.StateCode.ToLower() == DictCounty.ElementAt(i).Value.ToLower())
        //                                                        select d.StateName).FirstOrDefault().ToString();
        //                                    string check_second = string.Empty;
        //                                    string stringToCheck = StateName;
        //                                    BALGeneral objGeneral = new BALGeneral();
        //                                    string[] StateArray = objGeneral.GetDisabledStates().Select(d => d.StateName).ToArray();//{ "California", "Louisiana", "Nevada", "Ohio", "Vermont", "West Virginia", "Wyoming" };
        //                                    foreach (string x in StateArray)
        //                                    {
        //                                        if (x.ToLower().Contains(stringToCheck.ToLower()))
        //                                        {
        //                                            check_second = "exist";
        //                                        }
        //                                    }
        //                                    if (check_second != "exist")
        //                                    {
        //                                        string sDispState = "";
        //                                        response.Append("<tr>");
        //                                        if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode != null)
        //                                        {
        //                                            if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("_"))
        //                                                sDispState = "(" + ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Split('_').First() + ")";
        //                                        }
        //                                        else
        //                                        {
        //                                            sDispState = "NA";// DictCounty.ElementAt(i).Value;
        //                                        }
        //                                        //string StateName = (from d in dx1.tblStates
        //                                        //                    where (d.StateCode.ToLower() == DictCounty.ElementAt(i).Value.ToLower())
        //                                        //                    select d.StateName).FirstOrDefault().ToString();

        //                                        BALCompanyType Objbalcompanytype = new BALCompanyType();
        //                                        string StateFee = Objbalcompanytype.Get_State_Fee(StateName);
        //                                        if (StateFee == "0" || StateFee == "0.00")
        //                                        {
        //                                            StateFee = string.Empty;
        //                                        }
        //                                        else
        //                                        {
        //                                            StateFee = " State Fee" + "$" + StateFee;
        //                                        }



        //                                        response.Append("<td>" + StateName + "<label style='color:red'>" + StateFee + "</label>" + "</td>");
        //                                        response.Append("<td>" + sDispState + "</td>");
        //                                        response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk\" type=\"checkbox\"  id=\"chk_" + DictCounty.ElementAt(i).Key + "_" + DictCounty.ElementAt(i).Value + "_State_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
        //                                        response.Append("<tr>");
        //                                    }
        //                                }
        //                            }
        //                            response.Append("</table>");

        //                        }
        //                        #endregion

        //                        if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode != null)
        //                        {
        //                            if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("PS"))
        //                            {
        //                                #region Optional PS

        //                                if (stateregion != null && stateregion.Count > 0)
        //                                {
        //                                    foreach (XElement xe in stateregion)
        //                                    {
        //                                        if (xe != null)
        //                                        {
        //                                            if (DictCounty.Where(d => d.Key == xe.Value).Count() == 0)
        //                                                DictCounty.Add(xe.Value.ToString(), xe.Value.ToString());
        //                                        }
        //                                    }
        //                                    if (DictCounty.Count > 0)
        //                                    {
        //                                        responseOptional.Append("<table cellpadding=\"3\" cellspacing=\"0\" width=\"100%\">");
        //                                        responseOptional.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"3\">Counties (" + DictCounty.Count + ") found for " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + " " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + ":</td></tr>");
        //                                        responseOptional.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"40%\"><td>State</td><td width=\"40%\"> (ReportType)</td> <td width=\"20%\"><input type=\"checkbox\" onclick=\"selectunselectCounty();\" id=\"chk_SelectAllCounty\"  />Select All</td></tr>");
        //                                        for (int i = 0; i < DictCounty.Count; i++)
        //                                        {
        //                                            string sDispState = "";
        //                                            responseOptional.Append("<tr>");
        //                                            response.Append("<td>" + DictCounty.ElementAt(i).Key + ", [" + DictCounty.ElementAt(i).Value + "]</td>");
        //                                            if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode != null)
        //                                            {
        //                                                if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("_"))
        //                                                    sDispState = " (" + ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Split('_').First() + ")";
        //                                            }
        //                                            else
        //                                            {
        //                                                sDispState = "NA";// DictCounty.ElementAt(i).Value;
        //                                            }
        //                                            responseOptional.Append("<td>" + sDispState + "</td>");
        //                                            responseOptional.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk\" type=\"checkbox\"  id=\"chk_" + DictCounty.ElementAt(i).Key + "_" + DictCounty.ElementAt(i).Value + "_State_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
        //                                            responseOptional.Append("<tr>");
        //                                        }
        //                                    }
        //                                    responseOptional.Append("</table>");
        //                                }

        //                                #endregion
        //                            }

        //                            if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("NCR1"))
        //                            {
        //                                #region Optional NCR1


        //                                if (stateregion != null && stateregion.Count > 0)
        //                                {
        //                                    foreach (XElement xe in stateregion)
        //                                    {
        //                                        if (xe != null)
        //                                        {
        //                                            if (DictCounty.Where(d => d.Key == xe.Value).Count() == 0)
        //                                                DictCounty.Add(xe.Value.ToString(), xe.Value.ToString());
        //                                        }
        //                                    }
        //                                    if (DictCounty.Count > 0)
        //                                    {
        //                                        responseOptional.Append("<table cellpadding=\"3\" cellspacing=\"0\" width=\"100%\">");
        //                                        responseOptional.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"3\">Optional Report</td></tr>");
        //                                        responseOptional.Append("<tr>");
        //                                        responseOptional.Append("<td>National Criminal Report</td>");
        //                                        responseOptional.Append("<td>(NCR1)</td>");
        //                                        responseOptional.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_NoRecord_" + SearchedStateCode + "_State_1\" checked=\"checked\" /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
        //                                        responseOptional.Append("<tr>");
        //                                    }
        //                                    responseOptional.Append("</table>");
        //                                }

        //                                #endregion
        //                            }
        //                        }
        //                    }
        //                }
        //                catch
        //                {
        //                    response = new StringBuilder();
        //                    responseOptional = new StringBuilder();
        //                }
        //            }

        //            #endregion

        //            #region OrderInfo Region

        //            ObjEmergeReport.ObjOrderInfo.OrderId = OrderId;
        //            ObjEmergeReport.ObjOrderInfo.Tiered2Response = response.ToString();
        //            if (ObjEmergeReport.ObjTieredPackage.TiredOptional != null)
        //                ObjEmergeReport.ObjOrderInfo.OptionalResponse = responseOptional.ToString();
        //            else
        //                ObjEmergeReport.ObjOrderInfo.OptionalResponse = "";
        //            ObjEmergeReport.ObjOrderInfo.TieredTopInfo = TieredHeadText.ToString();

        //            #endregion

        //            Session["EmergeReport"] = ObjEmergeReport;
        //        }
        //        #endregion
        //    }
        //    else
        //    {
        //        ///
        //        /// Just Create a collection and call the tiered 2nd function SearchingTieredOrders().
        //        ///
        //        #region Is Automatic True

        //        XmlDocument ObjXmlDocument = new XmlDocument();
        //        XDocument ObjXmlDoc = new XDocument();
        //        string COUNTIES = "";
        //        string STATES = "";
        //        if (ObjEmergeReport.ObjTieredPackage != null && ObjEmergeReport.ObjOrderInfo.IsTiered && ObjEmergeReport.ObjOrderInfo.Tiered2Response != null && ObjEmergeReport.ObjOrderInfo.Tiered2Response != "")
        //        {

        //            ObjXmlDoc = XDocument.Parse(ObjEmergeReport.ObjOrderInfo.Tiered2Response);
        //            string XML_Value = GetRequiredXmlConditional(ObjXmlDoc.Document.ToString(), ObjEmergeReport.ObjOrderInfo.HostProductCode);
        //            ObjXDocument = XDocument.Parse(XML_Value);
        //            Dictionary<string, string> DictCounty = new Dictionary<string, string>();

        //            if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode != null)
        //            {
        //                ObjEmergeReport.ObjCollProducts.Add(ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode);
        //            }

        //            if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode != null)
        //            {
        //                ObjEmergeReport.ObjCollProducts.Add(ObjEmergeReport.ObjOrderInfo.OptionalProductCode);
        //            }

        //            #region Header Text

        //            //List<tblProductPerApplication> ObjtblPPA = new List<tblProductPerApplication>();
        //            //tblProductPerApplication ObjtblPPAs = new tblProductPerApplication();

        //            //using (DALDataContext dx = new DALDataContext())
        //            //{
        //            //    ObjtblPPA = dx.tblProductPerApplications.ToList<tblProductPerApplication>();
        //            //    string sHostname = "";
        //            //    string sTiered2 = "";
        //            //    string sOptional = "";
        //            //    ObjtblPPAs = ObjtblPPA.Where(d => d.pkProductApplicationId == ObjEmergeReport.ObjTieredPackage.Host).FirstOrDefault();
        //            //    if (ObjtblPPAs != null)
        //            //    {
        //            //        sHostname = ObjtblPPAs.ProductDisplayName + "(" + ObjtblPPAs.ProductCode + ")";
        //            //    }
        //            //    ObjtblPPAs = null;
        //            //    ObjtblPPAs = ObjtblPPA.Where(d => d.pkProductApplicationId == ObjEmergeReport.ObjTieredPackage.Tired2).FirstOrDefault();
        //            //    if (ObjtblPPAs != null)
        //            //    {
        //            //        sTiered2 = ObjtblPPAs.ProductDisplayName + "(" + ObjtblPPAs.ProductCode + ")";
        //            //    } ObjtblPPAs = null;
        //            //    ObjtblPPAs = ObjtblPPA.Where(d => d.pkProductApplicationId == ObjEmergeReport.ObjTieredPackage.TiredOptional).FirstOrDefault();
        //            //    if (ObjtblPPAs != null)
        //            //    {
        //            //        sOptional = ObjtblPPAs.ProductDisplayName + "(" + ObjtblPPAs.ProductCode + ")";
        //            //    }
        //            //    TieredHeadText.Append("<table>");
        //            //    TieredHeadText.Append("<tr><td>Host Report:</td><td>" + sHostname + "</td><tr>");
        //            //    TieredHeadText.Append("<tr><td>Tiered 2 Report:</td><td>" + sTiered2 + "</td><tr>");
        //            //    TieredHeadText.Append("<tr><td>Tiered 2 Optional Report:</td><td>" + sOptional + "</td><tr>");
        //            //    TieredHeadText.Append("<tr><td>Timeframe: </td><td></td><tr>");
        //            //    TieredHeadText.Append("<tr><td>Conditional Variable:</td><td>" + ObjEmergeReport.ObjTieredPackage.Variable + "</td><tr>");
        //            //    TieredHeadText.Append("</table>");
        //            //}
        //            #endregion

        //            #region HOST Response

        //            if (ObjEmergeReport.ObjOrderInfo.HostProductCode == "PS")
        //            {
        //                try
        //                {

        //                    #region Tiered CCR1

        //                    List<XElement> CreditFile = ObjXDocument.Descendants("CreditFile").ToList();
        //                    if (CreditFile != null && CreditFile.Count > 0)
        //                    {
        //                        List<XElement> PersonalData = CreditFile.Descendants("PersonalData").ToList();
        //                        if (PersonalData != null && PersonalData.Count > 0)
        //                        {
        //                            foreach (XElement xe in PersonalData)
        //                            {
        //                                XElement Region = xe.Descendants("Region").FirstOrDefault();
        //                                XElement County = xe.Descendants("County").FirstOrDefault();
        //                                if (Region != null && County != null)
        //                                {
        //                                    if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
        //                                        DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
        //                                }
        //                            }
        //                            if (DictCounty.Count > 0)
        //                            {
        //                                COUNTIES = COUNTIES + "##chk_" + DictCounty.First().Key + "_" + DictCounty.First().Value + "_County_0";
        //                            }
        //                        }
        //                    }
        //                    #endregion

        //                    #region Optional SCR

        //                    if (CreditFile != null && CreditFile.Count > 0)
        //                    {
        //                        List<XElement> PersonalData = CreditFile.Descendants("PersonalData").ToList();
        //                        if (PersonalData != null && PersonalData.Count > 0)
        //                        {
        //                            foreach (XElement xe in PersonalData)
        //                            {
        //                                XElement Region = xe.Descendants("Region").FirstOrDefault();
        //                                XElement County = xe.Descendants("County").FirstOrDefault();
        //                                if (Region != null && County != null)
        //                                {
        //                                    if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
        //                                        DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
        //                                }
        //                            }
        //                            if (DictCounty.Count > 0)
        //                            {
        //                                STATES = STATES + "##chk_" + DictCounty.First().Key + "_" + DictCounty.First().Value + "_State_" + "0".ToString();
        //                            }
        //                        }
        //                    }

        //                    #endregion

        //                }
        //                catch
        //                {
        //                    response = new StringBuilder();
        //                    responseOptional = new StringBuilder();
        //                }
        //            }
        //            else if (ObjEmergeReport.ObjOrderInfo.HostProductCode == "CCR1")
        //            {
        //                try
        //                {
        //                    List<XElement> stateregion = null;
        //                    List<XElement> reportrequested = null;
        //                    stateregion = ObjXDocument.Descendants("Region").ToList();
        //                    if (stateregion != null)
        //                    {
        //                        if (stateregion.Count == 0)
        //                        {
        //                            stateregion = ObjXDocument.Descendants("State").ToList();
        //                        }
        //                        if (stateregion.Count == 0)
        //                        {
        //                            reportrequested = ObjXDocument.Descendants("report-requested").ToList();
        //                            if (reportrequested != null && reportrequested.Count() > 0)
        //                            {
        //                                stateregion = reportrequested.Descendants("state").ToList();
        //                            }
        //                        }
        //                    }

        //                    if (stateregion != null && stateregion.Count > 0)
        //                    {

        //                        #region Tiered SCR

        //                        if (stateregion != null && stateregion.Count > 0)
        //                        {
        //                            foreach (XElement xe in stateregion)
        //                            {
        //                                if (xe != null)
        //                                {
        //                                    if (DictCounty.Where(d => d.Key == xe.Value).Count() == 0)
        //                                        DictCounty.Add(xe.Value.ToString(), xe.Value.ToString());
        //                                }
        //                            }
        //                            if (DictCounty.Count > 0)
        //                            {
        //                                COUNTIES = COUNTIES + "##chk_" + DictCounty.First().Key + "_" + DictCounty.First().Value + "_State_" + "0".ToString();
        //                            }
        //                            response.Append("</table>");

        //                        }
        //                        #endregion

        //                        #region Optional PS

        //                        if (stateregion != null && stateregion.Count > 0)
        //                        {
        //                            foreach (XElement xe in stateregion)
        //                            {
        //                                if (xe != null)
        //                                {
        //                                    if (DictCounty.Where(d => d.Key == xe.Value).Count() == 0)
        //                                        DictCounty.Add(xe.Value.ToString(), xe.Value.ToString());
        //                                }
        //                            }
        //                            if (DictCounty.Count > 0)
        //                            {
        //                                STATES = STATES + "##chk_" + DictCounty.First().Key + "_" + DictCounty.First().Value + "_State_" + "0".ToString();
        //                            }
        //                        }

        //                        #endregion
        //                    }
        //                }
        //                catch
        //                {
        //                    response = new StringBuilder();
        //                    responseOptional = new StringBuilder();
        //                }
        //            }

        //            #endregion

        //        }

        //        #endregion
        //    }
        //    try
        //    {
        //        string IsDataEntryOnly = "false";
        //        #region Check Data Entry User


        //        if (Session["UserRoles"] != null)
        //        {
        //            string[] role = (string[])(Session["UserRoles"]);
        //            if (role.Contains("DataEntry"))
        //            {
        //                ObjEmergeReport.ObjOrderInfo.IsDataEntry_Enable = true;
        //            }
        //            else
        //            {
        //                IsDataEntryOnly = GetDataEntryCheck();
        //                if (!string.IsNullOrEmpty(IsDataEntryOnly))
        //                {
        //                    ObjEmergeReport.ObjOrderInfo.IsDataEntry_Enable = Convert.ToBoolean(IsDataEntryOnly);
        //                }
        //            }
        //        }
        //        else
        //        {
        //            #region If Session expire check from membership
        //            MembershipUser ObjMembership = Membership.GetUser(User.Identity.Name);
        //            if (ObjMembership != null)
        //            {
        //                string[] role = Roles.GetRolesForUser(User.Identity.Name);
        //                Session["UserRoles"] = role;
        //                if (role.Contains("DataEntry"))
        //                {
        //                    ObjEmergeReport.ObjOrderInfo.IsDataEntry_Enable = true;
        //                }
        //            }
        //            #endregion

        //        }
        //        #endregion
        //    }
        //    catch
        //    {
        //    }
        //    return Json(new { ReportPage = "", Coll = ObjEmergeReport });
        //    //byte OrderType = ObjEmergeReport.ObjtblOrder.OrderType;
        //    //if (OrderType == 1)
        //    //{
        //    //    return Json(new { ReportPage = "", Coll = ObjEmergeReport });
        //    //}
        //    //else
        //    //{
        //    //    return Json(new { ReportPage = "runreport", Coll = ObjEmergeReport });//Landing Page
        //    //}
        //}

        //public string GetDataEntryCheck()
        //{
        //    string IsDataEntryOnly = "";
        //    try
        //    {
        //        string username = Convert.ToString(Session["CurrentUserMailId"]);
        //        if (username == string.Empty)
        //        {
        //            username = User.Identity.Name;
        //        }
        //        ProfileCommon Profile = new ProfileCommon();
        //        ProfileModel userProfile = Profile.GetProfile(username);
        //        IsDataEntryOnly = userProfile.IsDataEntryOnly_Enable;
        //        //if (!string.IsNullOrEmpty(Profile.SearchParams.IsDataEntryOnly_Enable))
        //        //{
        //        //    IsDataEntryOnly = Profile.SearchParams.IsDataEntryOnly_Enable.ToLower();
        //        //}
        //    }
        //    catch
        //    {
        //    }
        //    return IsDataEntryOnly;
        //}



        //public static string GetRequiredXmlConditional(string XmlResponse, string ProductCode)
        //{
        //    string XmlResponse_Local = XmlResponse;
        //    switch (ProductCode)
        //    {
        //        case "PS":
        //        case "NCR1":
        //        case "NCR+":
        //        case "RCX":
        //        case "CCR1":
        //            {
        //                if (XmlResponse_Local.IndexOf("<BackgroundReportPackage>") > 0 && XmlResponse_Local.IndexOf("</BackgroundReports>") > 0)
        //                {
        //                    XmlResponse_Local = XmlResponse_Local.Replace("</BackgroundReports>", "");
        //                    int xmlstartindex = XmlResponse_Local.IndexOf("<BackgroundReportPackage>");
        //                    XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
        //                } break;
        //            }
        //        case "SCR":
        //            {
        //                if (XmlResponse_Local.IndexOf("<BackgroundReportPackage>") != -1)
        //                {
        //                    XmlResponse_Local = XmlResponse_Local.Replace("</BackgroundReports>", "");
        //                    int xmlstartindex = XmlResponse_Local.IndexOf("<BackgroundReportPackage>");
        //                    XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
        //                } break;
        //            }
        //        default:
        //            break;
        //    }


        //    return XmlResponse_Local;
        //}


        //#endregion

    }
}
