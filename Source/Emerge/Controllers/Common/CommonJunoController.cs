﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Emerge.Models;
using System.Web.Security;

namespace Emerge.Controllers.Common
{
    public class CommonJunoController : Controller
    {
        //
        // GET: /CommonJuno/

        public PartialViewResult Index(string LeadStatus)
        {
            ViewBag.LeadStatus = LeadStatus;
            return PartialView("../Common/PartialJuno");
        }

        [HttpPost]
        public ActionResult GetNewJunos(int page, int pageSize, string group, List<GridSort> sort, List<GridFilter> filters)
        {
            BALJuno ObjBALJuno = new BALJuno();
           
            
            #region Sorting
            int iTotalRecords = 0;
            int iPageNo = page;
            int PageSize = pageSize;
            string ColumnName = "CreatedDate";
            string Direction = "DESC";
            if (sort != null)
            {
                Direction = sort[0].Dir;
                ColumnName = sort[0].Field;
            }

            #endregion

            #region Filtering
            byte LeadStatus = 0;
            if (filters != null)
            {
                LeadStatus = !string.IsNullOrEmpty(filters[0].Value) ? byte.Parse(filters[0].Value) : byte.Parse("0");
            }
            #endregion

            var ObjDbCollection = ObjBALJuno.GetNewJunos(ColumnName, Direction, true, iPageNo, PageSize, LeadStatus);

            if (ObjDbCollection.Count > 0)
            {
                iTotalRecords = int.Parse(ObjDbCollection.ElementAt(0).TotalRec.ToString());
            }
            // return Json(ObjDbCollection);
            return Json(new { Products = ObjDbCollection, TotalCount = iTotalRecords });
        }

    }
}
