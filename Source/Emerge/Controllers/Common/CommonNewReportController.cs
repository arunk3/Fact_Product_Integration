﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Emerge.Data;
using Emerge.Models;
using System.Xml;
using Emerge.Common;
using System.Xml.Serialization;
using System.IO;
using System.Configuration;
using System.Text;
using System.Xml.Linq;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web.Services.Protocols;
using System.Data;

namespace Emerge.Controllers.Common
{
    public partial class CommonNewReportController : Controller
    {
        string XMLFilePath = Utility.GetProductXMLFilePath();

        #region Submit New Report

        public ActionResult SubmitReport(NewReportModel ObjModel, FormCollection frm, string[] CompanyName, string[] AliasName, string[] SchoolName, string[] InstituteAlias, string[] ReferenceName, string[] ReferenceAlias, string[] LicenseType, string[] LicenseAlias)
        {
            if (ModelState.IsValid)
            {

                int LocationId = ObjModel.fkLocationId;
                int CompanyUserId = ObjModel.pkCompanyUserId;
                int fkCompanyId = ObjModel.pkCompanyId;

                EmergeReport ObjEmergeReport = new EmergeReport();
                OrderInfo ObjOrderInfo = new OrderInfo();
                tblTieredPackage ObjtblTieredPackage = new tblTieredPackage();
                // Dictionary<string, Guid> ObjCompnyProductInfo = new Dictionary<string, Guid>();
                //int TieredPackageids = 0;//INT-194
                int TieredPackageid = 0;
                //check 7 year filter is auto selected
                if (ObjModel.Is7YearAuto)
                {
                    ObjOrderInfo.Is7YearFilterApplyForCriminal = ObjModel.Is7YearAuto;
                }
                else
                {
                    ObjOrderInfo.Is7YearFilterApplyForCriminal = ObjModel.Is7YearFilterApplyForAllCriminalResult;
                }
                // string eror = string.Empty;
                //var NewProductCode = ObjModel.ProductsSelected.Split('_');
                //var VNewProductCode = NewProductCode[0].ToString().Split('#');

                XmlDocument ObjXmlDocument = new XmlDocument();
                ObjXmlDocument.Load(Server.MapPath("~/Content/ProductXml/ProductRequireData.xml"));
                // XmlNode ObjXmlNode = ObjXmlDocument.SelectSingleNode("/Products/Product[@Code='" + VNewProductCode[1] + "']");
                // XmlNodeList xnList = ObjXmlDocument.SelectNodes("/Products/Product[@Code='" + VNewProductCode[1] + "']/RequiredData");
                #region Product Selected
                int CheckTieredPackageiD = 0;
                List<string> ObjCollProducts = ProductsSelectedList(ObjModel.ProductsSelected);
                ObjCollProducts = FindControlsinSelectedReports(ObjCollProducts);
                for (int i = 0; i < ObjCollProducts.Count; i++)
                {

                    string[] ProductFullName = ObjCollProducts.ElementAt(i).Split('_');


                    int.TryParse(ProductFullName[1], out TieredPackageid);
                    using (EmergeDALDataContext dx = new EmergeDALDataContext())
                    {
                        if (Convert.ToInt32(ProductFullName[1]) != 0) //edit for INT-194
                        {
                            ObjtblTieredPackage = dx.tblTieredPackages.Where(d => d.fkPackageId == Convert.ToInt32(ProductFullName[1])).FirstOrDefault();
                            if (ObjtblTieredPackage != null)
                            {

                                TieredPackageid = Convert.ToInt32(ProductFullName[1]);
                                CheckTieredPackageiD = TieredPackageid;
                            }
                        }
                    }
                    if (ProductFullName[0] == "NCRPLUS")
                    {
                        ObjCollProducts.RemoveAt(i);
                        ObjCollProducts.Add("NCR+_" + ProductFullName[1]);
                    }
                    if (ProductFullName[0] == "NCR2")//Ticket #740: Incomplete report on NCR2 and error log is not shwing #17
                    {
                        if (string.IsNullOrEmpty(ObjModel.Social) == true) //# 408 Ticket
                        {
                            ObjModel.Social = "999-99-9999";
                        }
                    }
                }

                #endregion

                #region Tiered Section
                using (EmergeDALDataContext dx = new EmergeDALDataContext())
                {
                    if (CheckTieredPackageiD != 0) //edit for INT-194
                    {
                        TieredPackageid = CheckTieredPackageiD;// ADD FOR TAKE NON ZERO PACKAGE ID 

                        ObjtblTieredPackage = dx.tblTieredPackages.Where(d => d.fkPackageId == TieredPackageid).FirstOrDefault();
                        if (ObjtblTieredPackage != null)
                        {
                            ObjOrderInfo.IsTiered = true;
                            ObjOrderInfo.IsAutomatic = ObjtblTieredPackage.IsAutomaticRun;
                            if (ObjtblTieredPackage.Variable == "County State")
                            {
                                ObjOrderInfo.Variable = "1";
                            }
                            else if (ObjtblTieredPackage.Variable == "State")
                            {
                                ObjOrderInfo.Variable = "2";
                            }
                        }
                    }

                }

                #endregion
                #region Insertion in main order table
                DateTime Dated = DateTime.Now;
                tblOrder ObjtblOrder = new tblOrder();
                ObjtblOrder.fkLocationId = LocationId;
                ObjtblOrder.fkCompanyUserId = CompanyUserId;
                string OrderNo = Dated.ToString("yyMMddHHmmssfff");
                ObjtblOrder.OrderNo = OrderNo;
                ObjtblOrder.OrderDt = Dated;
                ObjtblOrder.OrderStatus = (byte)OrderStatus.Pending;
                ObjtblOrder.CreatedDate = Dated;
                ObjtblOrder.CreatedById = CompanyUserId;
                ObjtblOrder.SyncpodImage = ObjModel.hdnSyncpodImage;
                ObjtblOrder.FaceImage = ObjModel.hdnFaceImage;
                ObjtblOrder.OrderType = ObjModel.OrderType;
                ObjtblOrder.ReferenceCode = string.Empty;
                ObjtblOrder.SearchPersonHTML = Request.ServerVariables["HTTP_USER_AGENT"] + " OS- " + Environment.OSVersion.Version.ToString() + " Report Run From New Report Page.";
                if (ObjModel.hdnIsSyncpod == "true")
                {
                    ObjtblOrder.IsSyncpod = true;
                }
                else
                {
                    ObjtblOrder.IsSyncpod = false;
                }

                #endregion

                #region Insert new ReferenceCode for company

                int ReferenceCodeId = 0;
                string ReferenceCode = string.Empty;
                if (Convert.ToInt32(ObjModel.TrackingRefCode) > 0)
                {
                    ReferenceCodeId = Convert.ToInt32(ObjModel.TrackingRefCode);
                    ReferenceCode = ObjModel.TrackingRefCodeText;
                }
                else if (ObjModel.TrackingRefCode == -3 && ObjModel.TrackingRefCodeValue != "")
                {
                    if (ObjModel.TrackingRefCodeValue != null)
                    {
                        ReferenceCode = ObjModel.TrackingRefCodeValue.Trim();
                    }
                    else
                    {
                        ReferenceCode = ObjModel.TrackingNotes;
                    }
                    tblReferenceCode ObjtblReferenceCode = new tblReferenceCode();
                    BALReferenceCode ObjBALReferenceCode = new BALReferenceCode();
                    // DateTime CreatedDate = DateTime.Now;

                    ObjtblReferenceCode.fkCompanyId = fkCompanyId;
                    ObjtblReferenceCode.ReferenceCode = ReferenceCode;
                    ObjtblReferenceCode.ReferenceNote = ObjModel.TrackingNotes;
                    int result = ObjBALReferenceCode.AddReference(ObjtblReferenceCode, fkCompanyId, true);
                    if (result > 0)
                    {
                        ReferenceCodeId = result;
                    }
                }

                #endregion

                #region Insertion in order searched table
                tblOrderSearchedData ObjtblOrderSearchedData = new tblOrderSearchedData();
                ObjtblOrderSearchedData.SearchedLastName = (ObjModel.LastName != null ? ObjModel.LastName.Trim() : string.Empty);
                ObjtblOrderSearchedData.SearchedFirstName = (ObjModel.FirstName != null ? ObjModel.FirstName.Trim() : string.Empty);
                ObjtblOrderSearchedData.SearchedMiddleInitial = ObjModel.MI ?? string.Empty;
                ObjtblOrderSearchedData.SearchedSuffix = (ObjModel.SuffixValue != "-1") ? ObjModel.SuffixValue : string.Empty;
                ObjtblOrderSearchedData.SearchedSsn = ObjModel.Social ?? string.Empty;
                ObjtblOrderSearchedData.SearchedDob = ObjModel.DOB ?? string.Empty;
                ObjtblOrderSearchedData.SearchedPhoneNumber = ObjModel.Phone ?? string.Empty;
                ObjtblOrderSearchedData.SearchedApplicantEmail = ObjModel.ApplicantEmail ?? string.Empty;
                ObjtblOrderSearchedData.SearchedStreetAddress = ObjModel.Address ?? string.Empty;
                ObjtblOrderSearchedData.SearchedDirection = (ObjModel.DirectionValue != "-1") ? ObjModel.DirectionValue : string.Empty;
                ObjtblOrderSearchedData.SearchedStreetName = ObjModel.Street ?? string.Empty;
                ObjtblOrderSearchedData.SearchedStreetType = (ObjModel.TypeValue != "-1" && ObjModel.TypeValue != null) ? ObjModel.TypeValue : string.Empty;
                ObjtblOrderSearchedData.SearchedApt = ObjModel.Apt ?? string.Empty;
                ObjtblOrderSearchedData.SearchedCity = ObjModel.City ?? string.Empty;
                ObjtblOrderSearchedData.best_SearchedStreetAddress = ObjModel.Address ?? string.Empty;
                ObjtblOrderSearchedData.best_SearchedStreetName = ObjModel.Street ?? string.Empty;
                ObjtblOrderSearchedData.best_SearchedCity = ObjModel.City ?? string.Empty;
                bool IsLiveRunnerState = false;

                if (ObjModel.State != "-1")
                {
                    string[] SelectedStateValues = ObjModel.State.ToString().Split('_');
                    string SelectedState = SelectedStateValues[0].Trim();
                    if (SelectedStateValues[1].Trim().ToLower() == "true" && ObjModel.hdnIsSCRLiveRunnerLocation == "1")
                    {
                        if (ObjModel.IsLiveRunnerEnabled == true)
                        {
                            IsLiveRunnerState = true;
                        }
                    }
                    ObjtblOrderSearchedData.SearchedStateId = int.Parse(SelectedState);
                }

                ObjtblOrderSearchedData.SearchedZipCode = (ObjModel.Zip != null ? ObjModel.Zip.Trim() : string.Empty);
                ObjtblOrderSearchedData.best_SearchedZipCode = (ObjModel.Zip != null ? ObjModel.Zip.Trim() : string.Empty);

                //Multiple jursdiction apply for FCR
                List<tblOrderSearchedDataJurdictionInfo> ObjCollJurdistiction = new List<tblOrderSearchedDataJurdictionInfo>();
                if (ObjModel.Jurisdiction != "-1")
                {
                    string[] Jurisdiction = frm.GetValue("Jurisdiction").AttemptedValue.Split(',');
                    for (int index = 0; index < Jurisdiction.Count(); index++)
                    {
                        tblOrderSearchedDataJurdictionInfo ObjtblOrderSearchedDataJurisdiction = new tblOrderSearchedDataJurdictionInfo();
                        string[] jurisdictionDate = Jurisdiction[index].Split('_');
                        int JurisdictionId = Convert.ToInt32(jurisdictionDate[0]);
                        string JurisdictionName = jurisdictionDate[2];
                        if (JurisdictionId != -1)
                        {
                            ObjtblOrderSearchedDataJurisdiction.SearchedCountyId = JurisdictionId;
                            ObjtblOrderSearchedDataJurisdiction.SearchedCountyName = JurisdictionName;
                            ObjCollJurdistiction.Add(ObjtblOrderSearchedDataJurisdiction);
                        }
                    }
                }
                if (ObjModel.Jurisdiction != "-1")
                {
                    ObjtblOrderSearchedData.search_county = ObjModel.JurisdictionText;
                    ObjtblOrderSearchedData.best_search_county = ObjModel.JurisdictionText;
                    ObjtblOrderSearchedData.SearchedCountyId = Convert.ToInt32(ObjModel.Jurisdiction.ToString().Split('_').FirstOrDefault());
                }

                ObjtblOrderSearchedData.SearchedDriverLicense = (ObjModel.DriverLicense != null ? ObjModel.DriverLicense.Trim() : string.Empty);
                ObjtblOrderSearchedData.SearchedVehicleVin = (ObjModel.VehicleVin != null ? ObjModel.VehicleVin.Trim() : string.Empty);

                if (ObjModel.DLState != "-1")
                {
                    ObjtblOrderSearchedData.SearchedDLStateId = int.Parse(ObjModel.DLState.ToString().Split('_').FirstOrDefault());
                }

                ObjtblOrderSearchedData.SearchedSex = (ObjModel.Sex != "-1") ? ObjModel.Sex : string.Empty;
                ObjtblOrderSearchedData.SearchedBusinessName = (ObjModel.BusinessName != null ? ObjModel.BusinessName.Trim() : string.Empty);
                ObjtblOrderSearchedData.SearchedBusinessCity = (ObjModel.BusinessCity != null ? ObjModel.BusinessCity.Trim() : string.Empty);
                if (ObjModel.BusinessState != -1)
                {
                    ObjtblOrderSearchedData.SearchedBusinessStateId = Convert.ToInt32(ObjModel.BusinessState);
                }
                ObjtblOrderSearchedData.SearchedTrackingRef = ReferenceCode;
                if (ReferenceCodeId != 0)
                {
                    ObjtblOrderSearchedData.SearchedTrackingRefId = ReferenceCodeId;
                }
                ObjtblOrderSearchedData.SearchedTrackingNotes = (ObjModel.TrackingNotes != null ? ObjModel.TrackingNotes.Trim() : string.Empty);
                ObjtblOrderSearchedData.SearchedPermissibleId = ObjModel.PermissiblePurposeId; //ND-5 add this value to tblordersearched data

                #endregion

                #region Insertion in employment table

                List<tblOrderSearchedEmploymentInfo> ObjCollOrderSearchedEmploymentInfo = new List<tblOrderSearchedEmploymentInfo>();
                if (ObjCollProducts.Any(d => d.Contains("EMV")))
                {

                    //string[] CompanyName = frm.GetValue("CompanyName").AttemptedValue.Split(',');
                    string[] CompanyCity = frm.GetValue("CompanyCity").AttemptedValue.Split(',');
                    string[] CompanyPhone = frm.GetValue("CompanyPhone").AttemptedValue.Split(',');
                    //string[] AliasName = frm.GetValue("AliasName").AttemptedValue.Split(',');
                    string[] CompanyState = frm.GetValue("CompanyState").AttemptedValue.Split(',');
                    for (int iRow = 0; iRow < CompanyName.Count(); iRow++)
                    {
                        tblOrderSearchedEmploymentInfo ObjtblOrderSearchedEmploymentInfo = new tblOrderSearchedEmploymentInfo();
                        ObjtblOrderSearchedEmploymentInfo.SearchedAlias = AliasName[iRow];
                        ObjtblOrderSearchedEmploymentInfo.SearchedName = CompanyName[iRow];
                        ObjtblOrderSearchedEmploymentInfo.SearchedCity = CompanyCity[iRow];
                        ObjtblOrderSearchedEmploymentInfo.SearchedState = CompanyState[iRow];
                        ObjtblOrderSearchedEmploymentInfo.SearchedPhone = CompanyPhone[iRow] == "(___) ___-____" ? "" : CompanyPhone[iRow];

                        ObjtblOrderSearchedEmploymentInfo.SearchedContact = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedStarted = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedEnded = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedEmail = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedTitleStart = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedTitleEnd = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedSupervisorContact = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedReason = string.Empty;

                        ObjCollOrderSearchedEmploymentInfo.Add(ObjtblOrderSearchedEmploymentInfo);


                        if (iRow > 0)
                        {
                            ObjCollProducts.Add("EMV_0");
                        }
                    }
                }

                #endregion

                #region Insertion in education table

                List<tblOrderSearchedEducationInfo> ObjCollOrderSearchedEducationInfo = new List<tblOrderSearchedEducationInfo>();
                if (ObjCollProducts.Any(d => d.Contains("EDV")))
                {
                    //string[] SchoolName = frm.GetValue("SchoolName").AttemptedValue.Split(',');
                    string[] InstituteState = frm.GetValue("InstituteState").AttemptedValue.Split(',');
                    string[] SchoolCity = frm.GetValue("SchoolCity").AttemptedValue.Split(',');
                    string[] SchoolPhone = frm.GetValue("SchoolPhone").AttemptedValue.Split(',');
                    string[] GraduationDt = frm.GetValue("SearchedGraduationDt").AttemptedValue.Split(',');//INT336 GraduationDt

                    for (int iRow = 0; iRow < SchoolName.Count(); iRow++)
                    {

                        tblOrderSearchedEducationInfo ObjtblOrderSearchedEducationInfo = new tblOrderSearchedEducationInfo();
                        ObjtblOrderSearchedEducationInfo.SearchedSchoolName = SchoolName[iRow];
                        ObjtblOrderSearchedEducationInfo.SearchedCity = SchoolCity[iRow];
                        ObjtblOrderSearchedEducationInfo.SearchedPhone = SchoolPhone[iRow] == "(___) ___-____" ? "" : SchoolPhone[iRow];
                        ObjtblOrderSearchedEducationInfo.SearchedAlias = InstituteAlias[iRow];
                        ObjtblOrderSearchedEducationInfo.SearchedState = InstituteState[iRow];
                        ObjtblOrderSearchedEducationInfo.SearchedGraduationDt = GraduationDt[iRow];//INT336 GraduationDt
                        ObjtblOrderSearchedEducationInfo.SearchedStarted = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedDegree = string.Empty;

                        ObjtblOrderSearchedEducationInfo.SearchedEnded = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedMajor = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedFax = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedGrade = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedCredit = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedEmail = string.Empty;

                        ObjCollOrderSearchedEducationInfo.Add(ObjtblOrderSearchedEducationInfo);
                        if (iRow > 0)
                        {
                            ObjCollProducts.Add("EDV_0");
                        }
                        // change for INT 328 BILLING ISSue on 10 march 2016
                        //According to greg ,we have to find add new amount in BILLING AND IN ITEMISED REPPORT SECTION 

                    }
                }

                #endregion

                #region Insertion in Personal table and Question table

                List<tblOrderSearchedPersonalInfo> ObjCollOrderSearchedPersonalInfo = new List<tblOrderSearchedPersonalInfo>();
                List<tblOrderPersonalQtnInfo> objColltblOrderPersonalQtnInfo = new List<tblOrderPersonalQtnInfo>();
                if (ObjCollProducts.Any(d => d.Contains("PRV")))
                {
                    //string[] ReferenceName = frm.GetValue("ReferenceName").AttemptedValue.Split(',');
                    string[] ReferencePhone = frm.GetValue("ReferencePhone").AttemptedValue.Split(',');
                    //string[] ReferenceAlias = frm.GetValue("ReferenceAlias").AttemptedValue.Split(',');
                    for (int iRow = 0; iRow < ReferenceName.Count(); iRow++)
                    {
                        //Guid pkOrderSearchedPersnlId = Guid.NewGuid();

                        #region For other grid Controls

                        tblOrderSearchedPersonalInfo ObjtblOrderSearchedPersonalInfo = new tblOrderSearchedPersonalInfo();
                        ObjtblOrderSearchedPersonalInfo.SearchedName = ReferenceName[iRow];
                        ObjtblOrderSearchedPersonalInfo.SearchedPhone = ReferencePhone[iRow] == "(___) ___-____" ? "" : ReferencePhone[iRow];
                        //ObjtblOrderSearchedPersonalInfo.pkOrderSearchedPersnlId = pkOrderSearchedPersnlId;
                        ObjtblOrderSearchedPersonalInfo.SearchedAlias = ReferenceAlias[iRow];

                        ObjtblOrderSearchedPersonalInfo.SearchedCity = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedState = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedAltPhone = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedRelation = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedEmail = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedSalary = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedTitle = string.Empty;

                        ObjCollOrderSearchedPersonalInfo.Add(ObjtblOrderSearchedPersonalInfo);

                        #endregion


                        if (iRow > 0)
                        {
                            ObjCollProducts.Add("PRV_0");
                        }

                        // change for INT 328 BILLING ISSue on 10 march 2016
                        //According to greg ,we have to find add new amount in BILLING AND IN ITEMISED REPPORT SECTION 
                        #region For questions

                        tblOrderPersonalQtnInfo objtblOrderPersonalQtnInfo = new tblOrderPersonalQtnInfo();
                        objColltblOrderPersonalQtnInfo.Add(objtblOrderPersonalQtnInfo);

                        #endregion
                    }

                }

                #endregion

                #region Insertion in Profession table and Question table //INT336 //INT336 Professional Reference.

                List<tblOrderSearchedProfessionalInfo> ObjCollOrderSearchedProfessionalInfo = new List<tblOrderSearchedProfessionalInfo>();

                if (ObjCollProducts.Any(d => d.Contains("PRV2")))
                {
                    string[] PReferenceName = frm.GetValue("PReferenceName").AttemptedValue.Split(',');
                    string[] PReferencePhone = frm.GetValue("PReferencePhone").AttemptedValue.Split(',');
                    string[] PReferenceAlias = frm.GetValue("PReferenceAlias").AttemptedValue.Split(',');
                    for (int iRow = 0; iRow < PReferenceName.Count(); iRow++)
                    {

                        #region For other grid Controls

                        tblOrderSearchedProfessionalInfo OrderSearchedProfessionalInfo = new tblOrderSearchedProfessionalInfo();
                        OrderSearchedProfessionalInfo.SearchedName = PReferenceName[iRow];
                        OrderSearchedProfessionalInfo.SearchedPhone = PReferencePhone[iRow] == "(___) ___-____" ? "" : PReferencePhone[iRow];
                        OrderSearchedProfessionalInfo.SearchedAlias = PReferenceAlias[iRow];

                        OrderSearchedProfessionalInfo.SearchedCity = string.Empty;
                        OrderSearchedProfessionalInfo.SearchedState = string.Empty;
                        OrderSearchedProfessionalInfo.SearchedAltPhone = string.Empty;
                        OrderSearchedProfessionalInfo.SearchedRelation = string.Empty;
                        OrderSearchedProfessionalInfo.SearchedEmail = string.Empty;
                        OrderSearchedProfessionalInfo.SearchedSalary = string.Empty;
                        OrderSearchedProfessionalInfo.SearchedTitle = string.Empty;

                        ObjCollOrderSearchedProfessionalInfo.Add(OrderSearchedProfessionalInfo);

                        #endregion


                        if (iRow > 0)
                        {
                            ObjCollProducts.Add("PRV2_0");
                        }

                        // change for INT 328 BILLING ISSue on 10 march 2016
                        //According to greg ,we have to find add new amount in BILLING AND IN ITEMISED REPPORT SECTION 
                    }

                }

                #endregion

                #region Insertion in License verification table

                List<tblOrderSearchedLicenseInfo> ObjCollOrderSearchedLicenseInfo = new List<tblOrderSearchedLicenseInfo>();

                if (ObjCollProducts.Any(d => d.Contains("PLV")))
                {
                    //string[] LicenseType = frm.GetValue("LicenseType").AttemptedValue.Split(',');
                    string[] LicenseState = frm.GetValue("LicenseState").AttemptedValue.Split(',');
                    string[] LicenseNumber = frm.GetValue("LicenseNumber").AttemptedValue.Split(',');
                    string[] LicenseIssueDate = frm.GetValue("LicenseIssueDate").AttemptedValue.Split(',');
                    //string[] LicenseAlias = frm.GetValue("LicenseAlias").AttemptedValue.Split(',');
                    for (int iRow = 0; iRow < LicenseType.Count(); iRow++)
                    {
                        tblOrderSearchedLicenseInfo ObjtblOrderSearchedLicenseInfo = new tblOrderSearchedLicenseInfo();
                        //ObjtblOrderSearchedLicenseInfo.pkOrderLicenseVerifyId = Guid.NewGuid();
                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseType = LicenseType[iRow];
                        ObjtblOrderSearchedLicenseInfo.SearchedIssueDate = LicenseIssueDate[iRow];
                        ObjtblOrderSearchedLicenseInfo.SearchedIssuingState = LicenseState[iRow];
                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseNo = LicenseNumber[iRow];
                        ObjtblOrderSearchedLicenseInfo.SearchedAlias = LicenseAlias[iRow];

                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseName = string.Empty;
                        //ObjtblOrderSearchedLicenseInfo.SearchedLicenseType = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedAuthority = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedPhone = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedExpiryDate = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedStatus = string.Empty;
                        ObjCollOrderSearchedLicenseInfo.Add(ObjtblOrderSearchedLicenseInfo);
                        // change for INT 328 BILLING ISSue on 10 march 2016
                        //According to greg ,we have to find add new amount in BILLING AND IN ITEMISED REPPORT SECTION 
                        if (iRow > 0)
                        {
                            ObjCollProducts.Add("PLV_0");
                        }

                    }
                }

                if (ObjCollProducts.Any(d => d.Contains("WCV")))
                {
                    //string[] LicenseType = frm.GetValue("LicenseType").AttemptedValue.Split(',');
                    string[] LicenseState = frm.GetValue("LicenseState").AttemptedValue.Split(',');
                    string[] LicenseNumber = frm.GetValue("LicenseNumber").AttemptedValue.Split(',');
                    string[] LicenseIssueDate = frm.GetValue("LicenseIssueDate").AttemptedValue.Split(',');
                    //string[] LicenseAlias = frm.GetValue("LicenseAlias").AttemptedValue.Split(',');
                    for (int iRow = 0; iRow < LicenseType.Count(); iRow++)
                    {
                        tblOrderSearchedLicenseInfo ObjtblOrderSearchedLicenseInfo = new tblOrderSearchedLicenseInfo();
                        //ObjtblOrderSearchedLicenseInfo.pkOrderLicenseVerifyId = Guid.NewGuid();
                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseType = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedIssueDate = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedIssuingState = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseNo = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedAlias = LicenseAlias[iRow];

                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseName = string.Empty;
                        //ObjtblOrderSearchedLicenseInfo.SearchedLicenseType = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedAuthority = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedPhone = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedExpiryDate = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedStatus = string.Empty;
                        ObjCollOrderSearchedLicenseInfo.Add(ObjtblOrderSearchedLicenseInfo);


                        // change for INT 328 BILLING ISSue on 10 march 2016
                        //According to greg ,we have to find add new amount in BILLING AND IN ITEMISED REPPORT SECTION 
                        if (iRow > 0)
                        {
                            ObjCollProducts.Add("WCV_0");
                        }
                    }
                }

                #endregion

                #region For Social Diligence

                #region Insertion in Alias PersonalInfo table

                //List<tblOrderSearchedAliasPersonalInfo> ObjCollOrderSearchedAliasPersonalInfo = new List<tblOrderSearchedAliasPersonalInfo>();

                #endregion

                #region Insertion in Alias Location Info table

                // List<tblOrderSearchedAliasLocationInfo> ObjCollOrderSearchedAliasLocationInfo = new List<tblOrderSearchedAliasLocationInfo>();

                #endregion

                #region Insertion in Alias Work Info table

                //List<tblOrderSearchedAliasWorkInfo> ObjCollOrderSearchedAliasWorkInfo = new List<tblOrderSearchedAliasWorkInfo>();

                #endregion

                #endregion

                #region Insertion in LiveRunnerInfo table For CCR's Reports

                List<tblOrderSearchedLiveRunnerInfo> ObjColltblOrderSearchedLiveRunnerInfo = new List<tblOrderSearchedLiveRunnerInfo>();
                if (ObjCollProducts.Any(d => d.Contains("CCR1")) || ObjCollProducts.Any(d => d.Contains("CCR2")) || ObjCollProducts.Any(d => d.Contains("RCX")))
                {
                    // List of the Live Runner Products      
                    string[] CountyInfoState = frm.GetValue("CountyInfoState").AttemptedValue.Split(',');
                    string[] CountyInfoCounty = frm.GetValue("CountyInfoCounty").AttemptedValue.Split(',');

                    for (int iRow = 0; iRow < CountyInfoCounty.Count(); iRow++)
                    {
                        string[] CountyValueData = CountyInfoCounty[iRow].Split('_');
                        int CountyId = Convert.ToInt32(CountyValueData[0]);
                        if (CountyId != -1)
                        {
                            string IsRcxCounty = string.Empty;
                            IsRcxCounty = "False";
                            if (ObjModel.IsLiveRunnerEnabled == true)
                            {
                                IsRcxCounty = CountyValueData[1];
                            }
                            tblOrderSearchedLiveRunnerInfo ObjBALtblOrderSearchedLiveRunnerInfo = new tblOrderSearchedLiveRunnerInfo();
                            ObjBALtblOrderSearchedLiveRunnerInfo.LiveRunnerCounty = CountyValueData[5] + "_" + IsRcxCounty; //ddlCountyInfoCounty.SelectedItem.Text.Trim() + "_" + IsRcxCounty;
                            ObjBALtblOrderSearchedLiveRunnerInfo.LiveRunnerState = CountyInfoState[iRow];
                            ObjBALtblOrderSearchedLiveRunnerInfo.SearchedCountyId = CountyId;
                            ObjColltblOrderSearchedLiveRunnerInfo.Add(ObjBALtblOrderSearchedLiveRunnerInfo);
                        }
                    }
                }

                #endregion

                #region Live Runner Information For BAL Prcocessing
                // Dictionary<string, string> dictRcxProducts = new Dictionary<string, string>();
                #endregion

                #region Null DOB
                int IsNullDOB = 0;
                if (ObjModel.chkIsNullDOB)
                    IsNullDOB = 1;
                #endregion

                #region Insert In OrderDetails For Consent Form

                string ConsentEmailPDFPath = Server.MapPath("~/Resources/Upload/ConsentForm/usaintelConsentForm.pdf");
                List<CLSConsentInfo> ObjCollConsentInfo = new List<CLSConsentInfo>();

                if (frm.GetValue("ConsentReport") != null)
                {
                    string[] ConsentFaxEmail = new string[0];
                    string[] ConsentReport = frm.GetValue("ConsentReport").AttemptedValue.Split(',');
                    string[] ConsentState = frm.GetValue("ConsentState").AttemptedValue.Split(',');
                    if (frm.GetValue("ConsentFaxEmail") != null)
                    {
                        ConsentFaxEmail = frm.GetValue("ConsentFaxEmail").AttemptedValue.Split(',');
                    }
                    string FileName = string.Empty;
                    string sFilePath = "";

                    for (int i = 0; i < ConsentReport.Length; i++)
                    {

                        bool FAXEmail = false;
                        if (ConsentFaxEmail.Contains(i.ToString()))
                        {
                            FAXEmail = true;
                        }
                        HttpPostedFileBase ObjHttpPostedFile = Request.Files[i];
                        if (ObjHttpPostedFile.FileName != "")
                        {
                            string sExt = Path.GetExtension(ObjHttpPostedFile.FileName).ToLower();
                            FileName = DateTime.Now.ToFileTime().ToString() + sExt;
                            sFilePath = Server.MapPath("~/Resources/Upload/ConsentForm/") + FileName;
                            ObjHttpPostedFile.SaveAs(sFilePath);
                        }
                        else
                        {
                            FileName = string.Empty;
                        }


                        CLSConsentInfo ObjConsentInfo = new CLSConsentInfo();
                        ObjConsentInfo.pkConsentId = i + 1;
                        ObjConsentInfo.ConsentFileName = FileName;
                        ObjConsentInfo.IsConsentEmailOrFax = FAXEmail;
                        ObjConsentInfo.IsConsentRequired = true;
                        ObjConsentInfo.ConsentReportType = ConsentReport[i];
                        ObjConsentInfo.ConsentState = ConsentState[i];
                        ObjConsentInfo.PDFAttachFilePath = ConsentEmailPDFPath;
                        ObjCollConsentInfo.Add(ObjConsentInfo);
                    }
                }
                #endregion Insert In OrderDetails For Consent Form

                #region Calling Business Layer Method for insertion in db

                ObjEmergeReport.ObjCollOrderSearchedEducationInfo = ObjCollOrderSearchedEducationInfo;
                ObjEmergeReport.ObjCollOrderSearchedEmploymentInfo = ObjCollOrderSearchedEmploymentInfo;
                ObjEmergeReport.ObjCollOrderSearchedLicenseInfo = ObjCollOrderSearchedLicenseInfo;
                ObjEmergeReport.ObjCollOrderSearchedPersonalInfo = ObjCollOrderSearchedPersonalInfo;
                ObjEmergeReport.ObjCollOrderSearchedProfessionalInfo = ObjCollOrderSearchedProfessionalInfo;//INT336
                ObjEmergeReport.ObjCollProducts = ObjCollProducts;
                ObjEmergeReport.objColltblOrderPersonalQtnInfo = objColltblOrderPersonalQtnInfo;
                ObjEmergeReport.ObjtblOrder = ObjtblOrder;
                ObjEmergeReport.ObjtblOrderSearchedData = ObjtblOrderSearchedData;
                ObjEmergeReport.ObjColltblOrderSearchedLiveRunnerInfo = ObjColltblOrderSearchedLiveRunnerInfo;
                ObjEmergeReport.IsNullDOB = IsNullDOB;
                ObjEmergeReport.IsLiveRunnerState = IsLiveRunnerState;
                ObjEmergeReport.ObjCollCLSConsentInfo = ObjCollConsentInfo;
                ObjEmergeReport.ObjTieredPackage = ObjtblTieredPackage;
                ObjEmergeReport.ObjOrderInfo = ObjOrderInfo;
                ObjEmergeReport.CompanyAccountNo = ObjModel.CompanyAccountNo;
                ObjEmergeReport.OrderCompanyName = ObjModel.OrderCompanyName;
                ObjEmergeReport.PermissiblePurpose = ObjModel.PermissiblePurposeValue;
                ObjEmergeReport.ObjJurisdictionList = ObjCollJurdistiction;//used for multiple jurisdiction
                string sCurrentUserXMLFile = OrderNo.ToString() + ".xml";
                XmlSerializer serializer = new XmlSerializer(typeof(EmergeReport));
                Session["NewReportXMLPath"] = sCurrentUserXMLFile;
                Session["EmergeReport"] = ObjEmergeReport;
                using (TextWriter textWriter = new StreamWriter(Server.MapPath(@"~\Resources\Upload\NewReportXML\" + sCurrentUserXMLFile)))
                {
                    serializer.Serialize(textWriter, ObjEmergeReport);
                }
                serializer = null;
                #endregion


            }

            if (frm.GetValue("category_name").AttemptedValue == "eScreen")
            {
                try
                {
                    var ESProductCode = ObjModel.ProductsSelected.Split('_');
                    var ESPNewProductCode = ESProductCode[0].ToString().Split('#');
                    string eSProductCode = string.Empty;
                    if (ESPNewProductCode.Length == 2) { eSProductCode = ESPNewProductCode[1]; }
                    StringBuilder Request = EscreenRequest(ObjModel);
                    if (Request.Length == 0)
                    {
                        return RedirectToAction("NewReport", "Corporate");
                    }
                    else
                    {
                        string responseurl = InsertOrderForEscreen(eSProductCode, Request.ToString());

                        if (responseurl.Contains("popup"))
                        {
                            var element = responseurl.ToString().Split(new[] { "popup" }, StringSplitOptions.None);
                            TempData["popupdata"] = element[1].ToString();
                            TempData["popup"] = "1";
                        }

                        TempData["info"] = "eScreenRequest";
                        TempData["url"] = responseurl;
                        return RedirectToAction("NewReport", "Corporate");
                    }

                }
                catch (Exception ex)
                {
                    return Content(ex.ToString());
                }

            }
            else
            {
                if (ModelState.IsValid)
                {
                    return RedirectToAction("Searching", "Corporate");
                }
            }
            return View();
        }
        #endregion


        //INT-219 Duplicate report alert for SSN within 30 days.

        public int CheckDuplicateReportdata(string Social, int CompanyId)
        {
            BALGeneral obj = new BALGeneral();
            // retrieve the alert message dispaly value with SSN.
            int SSNReport = obj.SSNReportcount(Social, CompanyId);
            return SSNReport;
        }



        #region Add Additional Report
        public ActionResult SubmitAdditionalReport(NewReportModel ObjModel, FormCollection frm, string[] CompanyName, string[] AliasName, string[] SchoolName, string[] InstituteAlias, string[] ReferenceName, string[] ReferenceAlias, string[] LicenseType, string[] LicenseAlias)
        {
            var TieredPackageid1 = 0;
            int LocationId = 0;
            int CompanyUserId = 0;
            int fkCompanyId = 0;
            if (ModelState.IsValid)
            {
                if (frm.GetValue("chkSubmitAsUser") != null)
                {
                    //if additional report submitted by admin in behalf of normal user.
                    Dictionary<string, int> dic = new Dictionary<string, int>();
                    dic = new BALOrders().getOrderDetialsWithUserDetailsByOrderId(Convert.ToInt32(frm.GetValue("ParentOrderId").AttemptedValue));
                    fkCompanyId = Convert.ToInt32(dic["fkCompanyId"]);
                    LocationId = Convert.ToInt32(dic["fkLocationId"]);
                    CompanyUserId = Convert.ToInt32(dic["fkCompanyUserId"]);
                }
                else
                {
                    //if additional report submitted by normal user account.
                    LocationId = ObjModel.fkLocationId;
                    CompanyUserId = ObjModel.pkCompanyUserId;
                    fkCompanyId = ObjModel.pkCompanyId;
                }

                OrderInfo ObjOrderInfo = new OrderInfo();
                tblTieredPackage ObjtblTieredPackage = new tblTieredPackage();
                //Dictionary<string, Guid> ObjCompnyProductInfo = new Dictionary<string, Guid>();


                //string eror = string.Empty;
                //var NewProductCode = ObjModel.ProductsSelected.Split('_');
                //var VNewProductCode = NewProductCode[0].ToString().Split('#');
                XmlDocument ObjXmlDocument = new XmlDocument();
                ObjXmlDocument.Load(Server.MapPath("~/Content/ProductXml/ProductRequireData.xml"));
                // XmlNode ObjXmlNode = ObjXmlDocument.SelectSingleNode("/Products/Product[@Code='" + VNewProductCode[1] + "']");
                // XmlNodeList xnList = ObjXmlDocument.SelectNodes("/Products/Product[@Code='" + VNewProductCode[1] + "']/RequiredData");

                #region Product Selected
                List<string> ObjCollProducts = ProductsSelectedList(ObjModel.ProductsSelected);

                ObjCollProducts = FindControlsinSelectedReports(ObjCollProducts);
                for (int i = 0; i < ObjCollProducts.Count; i++)
                {
                    string[] ProductFullName = ObjCollProducts.ElementAt(i).Split('_');
                    int.TryParse(ProductFullName[1], out TieredPackageid1);
                    if (ProductFullName[0] == "NCRPLUS")
                    {
                        ObjCollProducts.RemoveAt(i);
                        ObjCollProducts.Add("NCR+_" + ProductFullName[1]);
                    }
                    if (ProductFullName[0] == "NCR2")//Ticket #740: Incomplete report on NCR2 and error log is not shwing #17
                    {
                        if (ObjModel.Social.Trim() == "")
                        {
                            ObjModel.Social = "999-99-9999";
                        }
                    }
                }

                #endregion

                #region Tiered Section


                if (TieredPackageid1 != 0)
                {
                    using (EmergeDALDataContext dx = new EmergeDALDataContext())
                    {
                        ObjtblTieredPackage = dx.tblTieredPackages.Where(d => d.fkPackageId == TieredPackageid1).FirstOrDefault();
                    }
                    if (ObjtblTieredPackage != null)
                    {
                        ObjOrderInfo.IsTiered = true;
                        ObjOrderInfo.IsAutomatic = ObjtblTieredPackage.IsAutomaticRun;
                        if (ObjtblTieredPackage.Variable == "County State")
                        {
                            ObjOrderInfo.Variable = "1";
                        }
                        else if (ObjtblTieredPackage.Variable == "State")
                        {
                            ObjOrderInfo.Variable = "2";
                        }
                    }
                }


                #endregion

                #region Insertion in main order table

                DateTime Dated = DateTime.Now;
                //int ParentOrderId = 0;

                string GetParentOrderId = frm.GetValue("ParentOrderId").AttemptedValue;

                tblOrder ObjtblOrder = new tblOrder();
                ObjtblOrder.fkLocationId = LocationId;
                ObjtblOrder.fkCompanyUserId = CompanyUserId;
                string OrderNo = Dated.ToString("yyMMddHHmmssfff");
                ObjtblOrder.OrderNo = OrderNo;
                ObjtblOrder.OrderDt = Dated;
                ObjtblOrder.OrderStatus = (byte)OrderStatus.Pending;
                ObjtblOrder.CreatedDate = Dated;
                ObjtblOrder.CreatedById = CompanyUserId;
                ObjtblOrder.SyncpodImage = ObjModel.hdnSyncpodImage;
                ObjtblOrder.FaceImage = ObjModel.hdnFaceImage;
                ObjtblOrder.OrderType = ObjModel.OrderType;
                ObjtblOrder.ReferenceCode = string.Empty;
                ObjtblOrder.ParentOrderId = Int32.Parse(GetParentOrderId);
                ObjtblOrder.SearchPersonHTML = Request.ServerVariables["HTTP_USER_AGENT"] + " OS- " + Environment.OSVersion.Version.ToString() + " Report Run From Additional Report Page."; ;
                if (ObjModel.hdnIsSyncpod == "true")
                {
                    ObjtblOrder.IsSyncpod = true;
                }
                else
                {
                    ObjtblOrder.IsSyncpod = false;
                }

                #endregion

                #region Insert new ReferenceCode for company

                int ReferenceCodeId = 0;
                string ReferenceCode = string.Empty;
                if (Convert.ToInt32(ObjModel.TrackingRefCode) > 0)
                {
                    ReferenceCodeId = Convert.ToInt32(ObjModel.TrackingRefCode);
                    ReferenceCode = ObjModel.TrackingRefCodeText;
                }
                else if (ObjModel.TrackingRefCode == -3 && ObjModel.TrackingRefCodeValue != "")
                {
                    if (ObjModel.TrackingRefCodeValue != null)
                    {
                        ReferenceCode = ObjModel.TrackingRefCodeValue.Trim();
                    }
                    else
                    {
                        ReferenceCode = ObjModel.TrackingNotes;
                    }
                    tblReferenceCode ObjtblReferenceCode = new tblReferenceCode();
                    BALReferenceCode ObjBALReferenceCode = new BALReferenceCode();
                    //DateTime CreatedDate = DateTime.Now;

                    ObjtblReferenceCode.fkCompanyId = fkCompanyId;
                    ObjtblReferenceCode.ReferenceCode = ReferenceCode;
                    ObjtblReferenceCode.ReferenceNote = ObjModel.TrackingNotes;
                    int result = ObjBALReferenceCode.AddReference(ObjtblReferenceCode, fkCompanyId, true);
                    if (result > 0)
                    {
                        ReferenceCodeId = result;
                    }
                }

                #endregion

                #region Insertion in order searched table

                tblOrderSearchedData ObjtblOrderSearchedData = new tblOrderSearchedData();
                ObjtblOrderSearchedData.SearchedLastName = (ObjModel.LastName != null ? ObjModel.LastName.Trim() : string.Empty);
                ObjtblOrderSearchedData.SearchedFirstName = (ObjModel.FirstName != null ? ObjModel.FirstName.Trim() : string.Empty);
                ObjtblOrderSearchedData.SearchedMiddleInitial = ObjModel.MI ?? string.Empty;
                ObjtblOrderSearchedData.SearchedSuffix = (ObjModel.SuffixValue != "-1") ? ObjModel.SuffixValue : string.Empty;
                ObjtblOrderSearchedData.SearchedSsn = ObjModel.SocialMask ?? string.Empty;
                ObjtblOrderSearchedData.SearchedDob = ObjModel.DOB ?? string.Empty;
                ObjtblOrderSearchedData.SearchedPhoneNumber = ObjModel.Phone ?? string.Empty;
                ObjtblOrderSearchedData.SearchedApplicantEmail = ObjModel.ApplicantEmail ?? string.Empty;
                ObjtblOrderSearchedData.SearchedStreetAddress = ObjModel.Address ?? string.Empty;
                ObjtblOrderSearchedData.SearchedDirection = (ObjModel.DirectionValue != "-1") ? ObjModel.DirectionValue : string.Empty;
                ObjtblOrderSearchedData.SearchedStreetName = ObjModel.Street ?? string.Empty;
                ObjtblOrderSearchedData.SearchedStreetType = (ObjModel.TypeValue != "-1" && ObjModel.TypeValue != null) ? ObjModel.TypeValue : string.Empty;
                ObjtblOrderSearchedData.SearchedApt = ObjModel.Apt ?? string.Empty;
                ObjtblOrderSearchedData.SearchedCity = ObjModel.City ?? string.Empty;
                bool IsLiveRunnerState = false;
                if (ObjModel.State != "-1")
                {
                    string[] SelectedStateValues = ObjModel.State.ToString().Split('_');
                    string SelectedState = SelectedStateValues[0].Trim();
                    BALCompanyUsers objBALCompanyUsers = new BALCompanyUsers();
                    bool result = objBALCompanyUsers.StateCheckStatusLiveRunner(Convert.ToInt32(SelectedState));
                    IsLiveRunnerState = result;
                    ObjtblOrderSearchedData.SearchedStateId = int.Parse(SelectedState);
                }

                ObjtblOrderSearchedData.SearchedZipCode = (ObjModel.Zip != null ? ObjModel.Zip.Trim() : string.Empty);
                ObjtblOrderSearchedData.best_SearchedZipCode = (ObjModel.Zip != null ? ObjModel.Zip.Trim() : string.Empty);

                //Multiple jursdiction apply for FCR
                List<tblOrderSearchedDataJurdictionInfo> ObjCollJurdistiction = new List<tblOrderSearchedDataJurdictionInfo>();
                if (ObjModel.Jurisdiction != "-1")
                {
                    string[] Jurisdiction = frm.GetValue("Jurisdiction").AttemptedValue.Split(',');
                    for (int index = 0; index < Jurisdiction.Count(); index++)
                    {
                        tblOrderSearchedDataJurdictionInfo ObjtblOrderSearchedDataJurisdiction = new tblOrderSearchedDataJurdictionInfo();
                        string[] jurisdictionDate = Jurisdiction[index].Split('_');
                        int JurisdictionId = Convert.ToInt32(jurisdictionDate[0]);
                        string JurisdictionName = jurisdictionDate[2];
                        if (JurisdictionId != -1)
                        {
                            ObjtblOrderSearchedDataJurisdiction.SearchedCountyId = JurisdictionId;
                            ObjtblOrderSearchedDataJurisdiction.SearchedCountyName = JurisdictionName;
                            ObjCollJurdistiction.Add(ObjtblOrderSearchedDataJurisdiction);
                        }
                    }
                }
                if (ObjModel.Jurisdiction != "-1")
                {
                    ObjtblOrderSearchedData.search_county = ObjModel.JurisdictionText;
                    ObjtblOrderSearchedData.best_search_county = ObjModel.JurisdictionText;
                    ObjtblOrderSearchedData.SearchedCountyId = Convert.ToInt32(ObjModel.Jurisdiction.ToString().Split('_').FirstOrDefault());
                }

                ObjtblOrderSearchedData.SearchedDriverLicense = (ObjModel.DriverLicense != null ? ObjModel.DriverLicense.Trim() : string.Empty);
                ObjtblOrderSearchedData.SearchedVehicleVin = (ObjModel.VehicleVin != null ? ObjModel.VehicleVin.Trim() : string.Empty);

                if (ObjModel.DLState != "-1")
                {
                    ObjtblOrderSearchedData.SearchedDLStateId = int.Parse(ObjModel.DLState.ToString().Split('_').FirstOrDefault());
                }

                ObjtblOrderSearchedData.SearchedSex = (ObjModel.Sex != "-1") ? ObjModel.Sex : string.Empty;
                ObjtblOrderSearchedData.SearchedBusinessName = (ObjModel.BusinessName != null ? ObjModel.BusinessName.Trim() : string.Empty);
                ObjtblOrderSearchedData.SearchedBusinessCity = (ObjModel.BusinessCity != null ? ObjModel.BusinessCity.Trim() : string.Empty);
                if (ObjModel.BusinessState != -1)
                {
                    ObjtblOrderSearchedData.SearchedBusinessStateId = Convert.ToInt32(ObjModel.BusinessState);
                }
                ObjtblOrderSearchedData.SearchedTrackingRef = ReferenceCode;
                if (ReferenceCodeId != 0)
                {
                    ObjtblOrderSearchedData.SearchedTrackingRefId = ReferenceCodeId;
                }
                ObjtblOrderSearchedData.SearchedTrackingNotes = (ObjModel.TrackingNotes != null ? ObjModel.TrackingNotes.Trim() : string.Empty);
                ObjtblOrderSearchedData.SearchedPermissibleId = ObjModel.PermissiblePurposeId; //ND-5 add this value to tblordersearched data

                #endregion

                #region Insertion in employment table

                List<tblOrderSearchedEmploymentInfo> ObjCollOrderSearchedEmploymentInfo = new List<tblOrderSearchedEmploymentInfo>();
                if (ObjCollProducts.Any(d => d.Contains("EMV")))
                {

                    //string[] CompanyName = frm.GetValue("CompanyName").AttemptedValue.Split(',');
                    string[] CompanyCity = frm.GetValue("CompanyCity").AttemptedValue.Split(',');
                    string[] CompanyPhone = frm.GetValue("CompanyPhone").AttemptedValue.Split(',');
                    //string[] AliasName = frm.GetValue("AliasName").AttemptedValue.Split(',');
                    string[] CompanyState = frm.GetValue("CompanyState").AttemptedValue.Split(',');
                    for (int iRow = 0; iRow < CompanyName.Count(); iRow++)
                    {
                        tblOrderSearchedEmploymentInfo ObjtblOrderSearchedEmploymentInfo = new tblOrderSearchedEmploymentInfo();
                        ObjtblOrderSearchedEmploymentInfo.SearchedAlias = AliasName[iRow];
                        ObjtblOrderSearchedEmploymentInfo.SearchedName = CompanyName[iRow];
                        ObjtblOrderSearchedEmploymentInfo.SearchedCity = CompanyCity[iRow];
                        ObjtblOrderSearchedEmploymentInfo.SearchedState = CompanyState[iRow];
                        ObjtblOrderSearchedEmploymentInfo.SearchedPhone = CompanyPhone[iRow] == "(___) ___-____" ? "" : CompanyPhone[iRow];

                        ObjtblOrderSearchedEmploymentInfo.SearchedContact = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedStarted = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedEnded = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedEmail = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedTitleStart = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedTitleEnd = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedSupervisorContact = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedReason = string.Empty;

                        ObjCollOrderSearchedEmploymentInfo.Add(ObjtblOrderSearchedEmploymentInfo);
                        if (iRow > 0)
                        {
                            ObjCollProducts.Add("EMV_0");
                        }
                    }
                }

                #endregion

                #region Insertion in education table

                List<tblOrderSearchedEducationInfo> ObjCollOrderSearchedEducationInfo = new List<tblOrderSearchedEducationInfo>();
                if (ObjCollProducts.Any(d => d.Contains("EDV")))
                {
                    //string[] SchoolName = frm.GetValue("SchoolName").AttemptedValue.Split(',');
                    string[] InstituteState = frm.GetValue("InstituteState").AttemptedValue.Split(',');
                    string[] SchoolCity = frm.GetValue("SchoolCity").AttemptedValue.Split(',');
                    string[] SchoolPhone = frm.GetValue("SchoolPhone").AttemptedValue.Split(',');
                    string[] GraduationDt = frm.GetValue("SearchedGraduationDt").AttemptedValue.Split(',');//INT336 GraduationDt

                    for (int iRow = 0; iRow < SchoolName.Count(); iRow++)
                    {

                        tblOrderSearchedEducationInfo ObjtblOrderSearchedEducationInfo = new tblOrderSearchedEducationInfo();
                        ObjtblOrderSearchedEducationInfo.SearchedSchoolName = SchoolName[iRow];
                        ObjtblOrderSearchedEducationInfo.SearchedCity = SchoolCity[iRow];
                        ObjtblOrderSearchedEducationInfo.SearchedPhone = SchoolPhone[iRow] == "(___) ___-____" ? "" : SchoolPhone[iRow];
                        ObjtblOrderSearchedEducationInfo.SearchedGraduationDt = GraduationDt[iRow];//INT336 GraduationDt
                        ObjtblOrderSearchedEducationInfo.SearchedAlias = InstituteAlias[iRow];
                        ObjtblOrderSearchedEducationInfo.SearchedState = InstituteState[iRow];

                        ObjtblOrderSearchedEducationInfo.SearchedStarted = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedDegree = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedEnded = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedMajor = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedFax = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedGrade = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedCredit = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedEmail = string.Empty;

                        ObjCollOrderSearchedEducationInfo.Add(ObjtblOrderSearchedEducationInfo);
                        if (iRow > 0)
                        {
                            ObjCollProducts.Add("EDV_0");
                        }
                        // change for INT 328 BILLING ISSue on 10 march 2016
                        //According to greg ,we have to find add new amount in BILLING AND IN ITEMISED REPPORT SECTION
                    }
                }

                #endregion

                #region Insertion in Personal table and Question table

                List<tblOrderSearchedPersonalInfo> ObjCollOrderSearchedPersonalInfo = new List<tblOrderSearchedPersonalInfo>();
                List<tblOrderPersonalQtnInfo> objColltblOrderPersonalQtnInfo = new List<tblOrderPersonalQtnInfo>();
                if (ObjCollProducts.Any(d => d.Contains("PRV")))
                {
                    //string[] ReferenceName = frm.GetValue("ReferenceName").AttemptedValue.Split(',');
                    string[] ReferencePhone = frm.GetValue("ReferencePhone").AttemptedValue.Split(',');
                    //string[] ReferenceAlias = frm.GetValue("ReferenceAlias").AttemptedValue.Split(',');
                    for (int iRow = 0; iRow < ReferenceName.Count(); iRow++)
                    {
                        //Guid pkOrderSearchedPersnlId = Guid.NewGuid();

                        #region For other grid Controls

                        tblOrderSearchedPersonalInfo ObjtblOrderSearchedPersonalInfo = new tblOrderSearchedPersonalInfo();
                        ObjtblOrderSearchedPersonalInfo.SearchedName = ReferenceName[iRow];
                        ObjtblOrderSearchedPersonalInfo.SearchedPhone = ReferencePhone[iRow] == "(___) ___-____" ? "" : ReferencePhone[iRow];
                        //ObjtblOrderSearchedPersonalInfo.pkOrderSearchedPersnlId = pkOrderSearchedPersnlId;
                        ObjtblOrderSearchedPersonalInfo.SearchedAlias = ReferenceAlias[iRow];

                        ObjtblOrderSearchedPersonalInfo.SearchedCity = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedState = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedAltPhone = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedRelation = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedEmail = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedSalary = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedTitle = string.Empty;

                        ObjCollOrderSearchedPersonalInfo.Add(ObjtblOrderSearchedPersonalInfo);
                        if (iRow > 0)
                        {
                            ObjCollProducts.Add("PRV_0");
                        }

                        // change for INT 328 BILLING ISSue on 10 march 2016
                        //According to greg ,we have to find add new amount in BILLING AND IN ITEMISED REPPORT SECTION 
                        #endregion

                        #region For questions

                        tblOrderPersonalQtnInfo objtblOrderPersonalQtnInfo = new tblOrderPersonalQtnInfo();
                        objColltblOrderPersonalQtnInfo.Add(objtblOrderPersonalQtnInfo);

                        #endregion
                    }

                }

                #endregion

                #region Insertion in License verification table

                List<tblOrderSearchedLicenseInfo> ObjCollOrderSearchedLicenseInfo = new List<tblOrderSearchedLicenseInfo>();

                if (ObjCollProducts.Any(d => d.Contains("PLV")))
                {
                    //string[] LicenseType = frm.GetValue("LicenseType").AttemptedValue.Split(',');
                    string[] LicenseState = frm.GetValue("LicenseState").AttemptedValue.Split(',');
                    string[] LicenseNumber = frm.GetValue("LicenseNumber").AttemptedValue.Split(',');
                    string[] LicenseIssueDate = frm.GetValue("LicenseIssueDate").AttemptedValue.Split(',');
                    //string[] LicenseAlias = frm.GetValue("LicenseAlias").AttemptedValue.Split(',');
                    for (int iRow = 0; iRow < LicenseType.Count(); iRow++)
                    {
                        tblOrderSearchedLicenseInfo ObjtblOrderSearchedLicenseInfo = new tblOrderSearchedLicenseInfo();
                        //ObjtblOrderSearchedLicenseInfo.pkOrderLicenseVerifyId = Guid.NewGuid();
                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseType = LicenseType[iRow];
                        ObjtblOrderSearchedLicenseInfo.SearchedIssueDate = LicenseIssueDate[iRow];
                        ObjtblOrderSearchedLicenseInfo.SearchedIssuingState = LicenseState[iRow];
                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseNo = LicenseNumber[iRow];
                        ObjtblOrderSearchedLicenseInfo.SearchedAlias = LicenseAlias[iRow];

                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseName = string.Empty;
                        //ObjtblOrderSearchedLicenseInfo.SearchedLicenseType = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedAuthority = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedPhone = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedExpiryDate = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedStatus = string.Empty;
                        ObjCollOrderSearchedLicenseInfo.Add(ObjtblOrderSearchedLicenseInfo);
                        // change for INT 328 BILLING ISSue on 10 march 2016
                        //According to greg ,we have to find add new amount in BILLING AND IN ITEMISED REPPORT SECTION 
                        if (iRow > 0)
                        {
                            ObjCollProducts.Add("PLV_0");
                        }
                    }
                }

                if (ObjCollProducts.Any(d => d.Contains("WCV")))
                {
                    //string[] LicenseType = frm.GetValue("LicenseType").AttemptedValue.Split(',');
                    string[] LicenseState = frm.GetValue("LicenseState").AttemptedValue.Split(',');
                    string[] LicenseNumber = frm.GetValue("LicenseNumber").AttemptedValue.Split(',');
                    string[] LicenseIssueDate = frm.GetValue("LicenseIssueDate").AttemptedValue.Split(',');
                    //string[] LicenseAlias = frm.GetValue("LicenseAlias").AttemptedValue.Split(',');
                    for (int iRow = 0; iRow < LicenseType.Count(); iRow++)
                    {
                        tblOrderSearchedLicenseInfo ObjtblOrderSearchedLicenseInfo = new tblOrderSearchedLicenseInfo();
                        //ObjtblOrderSearchedLicenseInfo.pkOrderLicenseVerifyId = Guid.NewGuid();
                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseType = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedIssueDate = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedIssuingState = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseNo = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedAlias = LicenseAlias[iRow];

                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseName = string.Empty;
                        //ObjtblOrderSearchedLicenseInfo.SearchedLicenseType = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedAuthority = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedPhone = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedExpiryDate = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedStatus = string.Empty;
                        ObjCollOrderSearchedLicenseInfo.Add(ObjtblOrderSearchedLicenseInfo);

                        // change for INT 328 BILLING ISSue on 10 march 2016
                        //According to greg ,we have to find add new amount in BILLING AND IN ITEMISED REPPORT SECTION 
                        if (iRow > 0)
                        {
                            ObjCollProducts.Add("WCV_0");
                        }
                    }
                }

                #endregion

                #region For Social Diligence

                #region Insertion in Alias PersonalInfo table

                // List<tblOrderSearchedAliasPersonalInfo> ObjCollOrderSearchedAliasPersonalInfo = new List<tblOrderSearchedAliasPersonalInfo>();

                #endregion

                #region Insertion in Alias Location Info table

                //List<tblOrderSearchedAliasLocationInfo> ObjCollOrderSearchedAliasLocationInfo = new List<tblOrderSearchedAliasLocationInfo>();

                #endregion

                #region Insertion in Alias Work Info table

                //List<tblOrderSearchedAliasWorkInfo> ObjCollOrderSearchedAliasWorkInfo = new List<tblOrderSearchedAliasWorkInfo>();

                #endregion

                #endregion

                #region Insertion in LiveRunnerInfo table For CCR's Reports

                List<tblOrderSearchedLiveRunnerInfo> ObjColltblOrderSearchedLiveRunnerInfo = new List<tblOrderSearchedLiveRunnerInfo>();
                if (ObjCollProducts.Any(d => d.Contains("CCR1")) || ObjCollProducts.Any(d => d.Contains("CCR2")) || ObjCollProducts.Any(d => d.Contains("RCX")))
                {
                    // List of the Live Runner Products      
                    string[] CountyInfoState = frm.GetValue("CountyInfoState").AttemptedValue.Split(',');
                    string[] CountyInfoCounty = frm.GetValue("CountyInfoCounty").AttemptedValue.Split(',');

                    for (int iRow = 0; iRow < CountyInfoCounty.Count(); iRow++)
                    {
                        string[] CountyValueData = CountyInfoCounty[iRow].Split('_');
                        int CountyId = Convert.ToInt32(CountyValueData[0]);
                        if (CountyId != -1)
                        {
                            string IsRcxCounty = CountyValueData[1];
                            tblOrderSearchedLiveRunnerInfo ObjBALtblOrderSearchedLiveRunnerInfo = new tblOrderSearchedLiveRunnerInfo();
                            ObjBALtblOrderSearchedLiveRunnerInfo.LiveRunnerCounty = CountyValueData[5] + "_" + IsRcxCounty; //ddlCountyInfoCounty.SelectedItem.Text.Trim() + "_" + IsRcxCounty;
                            ObjBALtblOrderSearchedLiveRunnerInfo.LiveRunnerState = CountyInfoState[iRow];
                            ObjBALtblOrderSearchedLiveRunnerInfo.SearchedCountyId = CountyId;
                            ObjColltblOrderSearchedLiveRunnerInfo.Add(ObjBALtblOrderSearchedLiveRunnerInfo);
                        }
                    }
                }

                #endregion

                #region Live Runner Information For BAL Prcocessing
                // Dictionary<string, string> dictRcxProducts = new Dictionary<string, string>();
                #endregion

                #region Null DOB
                int IsNullDOB = 0;
                if (ObjModel.chkIsNullDOB)
                    IsNullDOB = 1;
                #endregion

                #region Insert In OrderDetails For Consent Form

                string ConsentEmailPDFPath = Server.MapPath("~/Resources/Upload/ConsentForm/IntelifiConsentForm.pdf");
                List<CLSConsentInfo> ObjCollConsentInfo = new List<CLSConsentInfo>();

                if (frm.GetValue("ConsentReport") != null)
                {
                    string[] ConsentFaxEmail = new string[0];
                    string[] ConsentReport = frm.GetValue("ConsentReport").AttemptedValue.Split(',');
                    string[] ConsentState = frm.GetValue("ConsentState").AttemptedValue.Split(',');
                    // string[] ConsentFile = frm.GetValue("ConsentFile").AttemptedValue.Split(',');
                    if (frm.GetValue("ConsentFaxEmail") != null)
                    {
                        ConsentFaxEmail = frm.GetValue("ConsentFaxEmail").AttemptedValue.Split(',');
                    }
                    string FileName = string.Empty;
                    string sFilePath = "";

                    for (int i = 0; i < ConsentReport.Length; i++)
                    {

                        bool FAXEmail = false;
                        if (ConsentFaxEmail.Contains(i.ToString()))
                        {
                            FAXEmail = true;
                        }
                        HttpPostedFileBase ObjHttpPostedFile = Request.Files[i];
                        if (ObjHttpPostedFile.FileName != "")
                        {
                            string sExt = Path.GetExtension(ObjHttpPostedFile.FileName).ToLower();
                            FileName = DateTime.Now.ToFileTime().ToString() + sExt;
                            sFilePath = Server.MapPath("~/Resources/Upload/ConsentForm/") + FileName;
                            ObjHttpPostedFile.SaveAs(sFilePath);
                        }
                        else
                        {
                            FileName = string.Empty;
                        }


                        CLSConsentInfo ObjConsentInfo = new CLSConsentInfo();
                        ObjConsentInfo.pkConsentId = i + 1;
                        ObjConsentInfo.ConsentFileName = FileName;
                        ObjConsentInfo.IsConsentEmailOrFax = FAXEmail;
                        ObjConsentInfo.IsConsentRequired = true;
                        ObjConsentInfo.ConsentReportType = ConsentReport[i];
                        ObjConsentInfo.ConsentState = ConsentState[i];
                        ObjConsentInfo.PDFAttachFilePath = ConsentEmailPDFPath;
                        ObjCollConsentInfo.Add(ObjConsentInfo);
                    }
                }
                #endregion Insert In OrderDetails For Consent Form

                #region Calling Business Layer Method for insertion in db
                EmergeReport ObjEmergeReport = new EmergeReport();
                ObjEmergeReport.ObjCollOrderSearchedEducationInfo = ObjCollOrderSearchedEducationInfo;
                ObjEmergeReport.ObjCollOrderSearchedEmploymentInfo = ObjCollOrderSearchedEmploymentInfo;
                ObjEmergeReport.ObjCollOrderSearchedLicenseInfo = ObjCollOrderSearchedLicenseInfo;
                ObjEmergeReport.ObjCollOrderSearchedPersonalInfo = ObjCollOrderSearchedPersonalInfo;
                ObjEmergeReport.ObjCollProducts = ObjCollProducts;
                ObjEmergeReport.objColltblOrderPersonalQtnInfo = objColltblOrderPersonalQtnInfo;
                ObjEmergeReport.ObjtblOrder = ObjtblOrder;
                ObjEmergeReport.ObjtblOrderSearchedData = ObjtblOrderSearchedData;
                ObjEmergeReport.ObjColltblOrderSearchedLiveRunnerInfo = ObjColltblOrderSearchedLiveRunnerInfo;
                ObjEmergeReport.IsNullDOB = IsNullDOB;
                ObjEmergeReport.IsLiveRunnerState = IsLiveRunnerState;
                ObjEmergeReport.ObjCollCLSConsentInfo = ObjCollConsentInfo;
                ObjEmergeReport.ObjTieredPackage = ObjtblTieredPackage;
                ObjEmergeReport.ObjOrderInfo = ObjOrderInfo;
                ObjEmergeReport.CompanyAccountNo = ObjModel.CompanyAccountNo;
                ObjEmergeReport.OrderCompanyName = ObjModel.OrderCompanyName;
                ObjEmergeReport.PermissiblePurpose = ObjModel.PermissiblePurposeValue;
                ObjEmergeReport.ObjJurisdictionList = ObjCollJurdistiction;
                string sCurrentUserXMLFile = DateTime.Now.ToFileTime().ToString() + ".xml";

                Session["NewReportXMLPath"] = sCurrentUserXMLFile;
                Session["EmergeReport"] = ObjEmergeReport;


                XmlSerializer serializer = new XmlSerializer(typeof(EmergeReport));

                using (TextWriter textWriter = new StreamWriter(Server.MapPath(@"~\Resources\Upload\NewReportXML\" + sCurrentUserXMLFile)))
                {
                    serializer.Serialize(textWriter, ObjEmergeReport);
                }


                #endregion
                serializer = null;
                TempData["IsErrorAdditionalRptRan"] = "true";
            }

            if (frm.GetValue("category_name").AttemptedValue == "eScreen")
            {

                try
                {
                    var ESProductCode = ObjModel.ProductsSelected.Split('_');
                    var ESPNewProductCode = ESProductCode[0].ToString().Split('#');
                    string eSProductCode = string.Empty;
                    if (ESPNewProductCode.Length == 2) { eSProductCode = ESPNewProductCode[1]; }
                    StringBuilder Request = EscreenRequest(ObjModel);
                    if (Request.Length == 0)
                    {
                        return RedirectToAction("NewReport", "Corporate");
                    }

                    else
                    {
                        string responseurl = InsertOrderForEscreen(eSProductCode, Request.ToString());
                        TempData["info"] = "eScreenRequest";
                        TempData["url"] = responseurl;
                        return RedirectToAction("NewReport", "Corporate");
                    }

                }
                catch (Exception ex)
                {
                    return Content(ex.ToString());
                }

            }
            else
            {

                if (ModelState.IsValid)
                {
                    return RedirectToAction("Searching", "Corporate");
                }

            }
            return View();
        }


        #endregion





        #region Submit all run county Report
        public ActionResult SubmitRunCounty(NewReportModel ObjModel, FormCollection frm, string[] CompanyName, string[] AliasName, string[] SchoolName, string[] InstituteAlias, string[] ReferenceName, string[] ReferenceAlias, string[] LicenseType, string[] LicenseAlias)
        {
            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n " + System.DateTime.Now + "  CompanyId:" + ObjModel.pkCompanyId + ", ModelState:" + ModelState.IsValid + "FromOrderID:" + ObjModel.OrderId, "RunAllCountyLogIssue");

            var TieredPackageid1 = 0;
            if (ModelState.IsValid)
            {

                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n " + System.DateTime.Now + "  CompanyId" + ObjModel.pkCompanyId + " LOG for runall ModelState.IsValid is true county information" + ModelState.IsValid, "RunAllCountyLogIssue");

                int LocationId = ObjModel.fkLocationId;
                int CompanyUserId = ObjModel.pkCompanyUserId;
                int fkCompanyId = ObjModel.pkCompanyId;
                OrderInfo ObjOrderInfo = new OrderInfo();
                tblTieredPackage ObjtblTieredPackage = new tblTieredPackage();
                //Dictionary<string, Guid> ObjCompnyProductInfo = new Dictionary<string, Guid>();
                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n " + System.DateTime.Now + "  CompanyId" + ObjModel.pkCompanyId + " LOG for runall ObjCompnyProductInfo county information" + ModelState.IsValid, "RunAllCountyLogIssue");

                //string eror = string.Empty;
                //var NewProductCode = ObjModel.ProductsSelected.Split('_');
                //var VNewProductCode = NewProductCode[0].ToString().Split('#');
                XmlDocument ObjXmlDocument = new XmlDocument();
                ObjXmlDocument.Load(Server.MapPath("~/Content/ProductXml/ProductRequireData.xml"));
                //XmlNode ObjXmlNode = ObjXmlDocument.SelectSingleNode("/Products/Product[@Code='" + VNewProductCode[1] + "']");
                // XmlNodeList xnList = ObjXmlDocument.SelectNodes("/Products/Product[@Code='" + VNewProductCode[1] + "']/RequiredData");


                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n " + System.DateTime.Now + "  CompanyId" + ObjModel.pkCompanyId + " LOG for runall xnList" + ModelState.IsValid, "RunAllCountyLogIssue");

                //for (int i = 0; i < xnList.Count; i++)
                //{
                //    string ReqColumn = "";
                //    ReqColumn = xnList[i].InnerXml;
                //    switch (ReqColumn)
                //    {
                //        case "LastName": { if (ObjModel.LastName == null) { eror = "eror"; } break; }
                //        case "FirstName": { if (ObjModel.FirstName == null) { eror = "eror"; } break; }
                //        case "Social": { if (ObjModel.Social == null) { eror = "eror"; } break; }
                //        case "State": { if (ObjModel.State == null) { eror = "eror"; } break; }
                //        case "Jurisdiction": { if (ObjModel.Jurisdiction == null) { eror = "eror"; } break; }
                //        case "LicenseState": { if (ObjModel.LicenseState == null) { eror = "eror"; } break; }
                //        case "LicenseType": { if (ObjModel.LicenseType == null) { eror = "eror"; } break; }
                //        case "ReferenceName": { if (ObjModel.ReferenceName == null) { eror = "eror"; } break; }
                //        case "ReferencePhone": { if (ObjModel.ReferencePhone == null) { eror = "eror"; } break; }
                //        case "SchoolCity": { if (ObjModel.SchoolCity == null) { eror = "eror"; } break; }
                //        case "InstituteState": { if (ObjModel.InstituteState == null) { eror = "eror"; } break; }
                //        case "SchoolName": { if (ObjModel.SchoolName == null) { eror = "eror"; } break; }
                //        case "DOB": { if (ObjModel.DOB == null) { eror = "eror"; }
                //                      else{ bool datecheck = true;if (ObjModel.DOB != null)
                //                          { datecheck = isValidDate(ObjModel.DOB);}
                //                          if (datecheck == false)
                //                          { eror = "eror"; }
                //                      }
                //                break;
                //            }
                //        case "CompanyName": { if (ObjModel.CompanyName == null) { eror = "eror"; } break; }
                //        case "CompanyState": { if (ObjModel.CompanyState == null) { eror = "eror"; } break; }
                //        case "CompanyCity": { if (ObjModel.CompanyCity == null) { eror = "eror"; } break; }
                //        case "CompanyPhone": { if (ObjModel.CompanyPhone == null) { eror = "eror"; } break; }
                //        case "VehicleVin": { if (ObjModel.VehicleVin== null) { eror = "eror"; } break; }
                //        case "BusinessName": { if (ObjModel.BusinessName == null) { eror = "eror"; } break; }
                //        case "BusinessCity": { if (ObjModel.BusinessCity == null) { eror = "eror"; } break; }
                //        case "DLState": { if (ObjModel.DLState == null) { eror = "eror"; } break; }
                //        case "DriverLicense": { if (ObjModel.DriverLicense == null) { eror = "eror"; } break; }
                //        case "CountyInfoCounty": { if (ObjModel.CountyInfoCounty == null) { eror = "eror"; } break; }
                //        case "CountyInfoState": { if (ObjModel.CountyInfoState == null) { eror = "eror"; } break; }
                //        case "Address": { if (ObjModel.Address == null) { eror = "eror"; } break; }
                //        case "Street": { if (ObjModel.Street == null) { eror = "eror"; } break; }
                //        case "Zip": { if (ObjModel.Zip == null) { eror = "eror"; } break; }
                //        default:
                //            break;
                //    }

                //}



                //if (eror != "eror")
                //{

                #region Product Selected
                List<string> ObjCollProducts = ProductsSelectedList(ObjModel.ProductsSelected);

                ObjCollProducts = FindControlsinSelectedReports(ObjCollProducts);
                for (int i = 0; i < ObjCollProducts.Count; i++)
                {
                    string[] ProductFullName = ObjCollProducts.ElementAt(i).Split('_');
                    int.TryParse(ProductFullName[1], out TieredPackageid1);
                    if (ProductFullName[0] == "NCRPLUS")
                    {
                        ObjCollProducts.RemoveAt(i);
                        ObjCollProducts.Add("NCR+_" + ProductFullName[1]);
                    }
                    if (ProductFullName[0] == "NCR2")//Ticket #740: Incomplete report on NCR2 and error log is not shwing #17
                    {
                        if (ObjModel.Social.Trim() == "")
                        {
                            ObjModel.Social = "999-99-9999";
                        }
                    }
                }

                #endregion

                #region Tiered Section
                if (TieredPackageid1 != 0)
                {
                    using (EmergeDALDataContext dx = new EmergeDALDataContext())
                    {
                        ObjtblTieredPackage = dx.tblTieredPackages.Where(d => d.fkPackageId == TieredPackageid1).FirstOrDefault();
                    }
                    if (ObjtblTieredPackage != null)
                    {
                        ObjOrderInfo.IsTiered = true;
                        ObjOrderInfo.IsAutomatic = ObjtblTieredPackage.IsAutomaticRun;
                        if (ObjtblTieredPackage.Variable == "County State")
                        {
                            ObjOrderInfo.Variable = "1";
                        }
                        else if (ObjtblTieredPackage.Variable == "State")
                        {
                            ObjOrderInfo.Variable = "2";
                        }
                    }
                }


                #endregion

                #region Insertion in main order table

                DateTime Dated = DateTime.Now;
                //int ParentOrderId = 0;

                string GetParentOrderId = frm.GetValue("ParentOrderId").AttemptedValue;

                tblOrder ObjtblOrder = new tblOrder();
                ObjtblOrder.fkLocationId = LocationId;
                ObjtblOrder.fkCompanyUserId = CompanyUserId;
                string OrderNo = Dated.ToString("yyMMddHHmmssfff");
                ObjtblOrder.OrderNo = OrderNo;
                ObjtblOrder.OrderDt = Dated;
                ObjtblOrder.OrderStatus = (byte)OrderStatus.Pending;
                ObjtblOrder.CreatedDate = Dated;
                ObjtblOrder.CreatedById = CompanyUserId;
                ObjtblOrder.SyncpodImage = ObjModel.hdnSyncpodImage;
                ObjtblOrder.FaceImage = ObjModel.hdnFaceImage;
                ObjtblOrder.OrderType = ObjModel.OrderType;
                ObjtblOrder.ReferenceCode = string.Empty;
                ObjtblOrder.ParentOrderId = Int32.Parse(GetParentOrderId);
                ObjtblOrder.SearchPersonHTML = Request.ServerVariables["HTTP_USER_AGENT"] + " OS- " + Environment.OSVersion.Version.ToString() + " Report Run From Run All County Page."; ;
                if (ObjModel.hdnIsSyncpod == "true")
                {
                    ObjtblOrder.IsSyncpod = true;
                }
                else
                {
                    ObjtblOrder.IsSyncpod = false;
                }

                #endregion

                #region Insert new ReferenceCode for company

                int ReferenceCodeId = 0;
                string ReferenceCode = string.Empty;
                if (Convert.ToInt32(ObjModel.TrackingRefCode) > 0)
                {
                    ReferenceCodeId = Convert.ToInt32(ObjModel.TrackingRefCode);
                    ReferenceCode = ObjModel.TrackingRefCodeText;
                }
                else if (ObjModel.TrackingRefCode == -3 && ObjModel.TrackingRefCodeValue != "")
                {
                    if (ObjModel.TrackingRefCodeValue != null)
                    {
                        ReferenceCode = ObjModel.TrackingRefCodeValue.Trim();
                    }
                    else
                    {
                        ReferenceCode = ObjModel.TrackingNotes;
                    }
                    tblReferenceCode ObjtblReferenceCode = new tblReferenceCode();
                    BALReferenceCode ObjBALReferenceCode = new BALReferenceCode();
                    // DateTime CreatedDate = DateTime.Now;

                    ObjtblReferenceCode.fkCompanyId = fkCompanyId;
                    ObjtblReferenceCode.ReferenceCode = ReferenceCode;
                    ObjtblReferenceCode.ReferenceNote = ObjModel.TrackingNotes;
                    int result = ObjBALReferenceCode.AddReference(ObjtblReferenceCode, fkCompanyId, true);
                    if (result > 0)
                    {
                        ReferenceCodeId = result;
                    }
                }

                #endregion

                #region Insertion in order searched table

                tblOrderSearchedData ObjtblOrderSearchedData = new tblOrderSearchedData();

                ObjtblOrderSearchedData.SearchedLastName = (ObjModel.LastName != null ? ObjModel.LastName.Trim() : string.Empty);
                ObjtblOrderSearchedData.SearchedFirstName = (ObjModel.FirstName != null ? ObjModel.FirstName.Trim() : string.Empty);
                ObjtblOrderSearchedData.SearchedMiddleInitial = ObjModel.MI ?? string.Empty;
                ObjtblOrderSearchedData.SearchedSuffix = (ObjModel.SuffixValue != "-1") ? ObjModel.SuffixValue : string.Empty;
                ObjtblOrderSearchedData.SearchedSsn = ObjModel.SocialMask ?? string.Empty;
                ObjtblOrderSearchedData.SearchedDob = ObjModel.DOB ?? string.Empty;
                ObjtblOrderSearchedData.SearchedPhoneNumber = ObjModel.Phone ?? string.Empty;
                ObjtblOrderSearchedData.SearchedApplicantEmail = ObjModel.ApplicantEmail ?? string.Empty;
                ObjtblOrderSearchedData.SearchedStreetAddress = ObjModel.Address ?? string.Empty;
                ObjtblOrderSearchedData.SearchedDirection = (ObjModel.DirectionValue != "-1") ? ObjModel.DirectionValue : string.Empty;
                ObjtblOrderSearchedData.SearchedStreetName = ObjModel.Street ?? string.Empty;
                ObjtblOrderSearchedData.SearchedStreetType = (ObjModel.TypeValue != "-1" && ObjModel.TypeValue != null) ? ObjModel.TypeValue : string.Empty;
                ObjtblOrderSearchedData.SearchedApt = ObjModel.Apt ?? string.Empty;
                ObjtblOrderSearchedData.SearchedCity = ObjModel.City ?? string.Empty;

                bool IsLiveRunnerState = false;

                if (ObjModel.State != "-1")
                {
                    string[] SelectedStateValues = ObjModel.State.ToString().Split('_');
                    string SelectedState = SelectedStateValues[0].Trim();
                    BALCompanyUsers objBALCompanyUsers = new BALCompanyUsers();
                    bool result = objBALCompanyUsers.StateCheckStatusLiveRunner(Convert.ToInt32(SelectedState));
                    IsLiveRunnerState = result;
                    ObjtblOrderSearchedData.SearchedStateId = int.Parse(SelectedState);
                }

                ObjtblOrderSearchedData.SearchedZipCode = (ObjModel.Zip != null ? ObjModel.Zip.Trim() : string.Empty);
                ObjtblOrderSearchedData.best_SearchedZipCode = (ObjModel.Zip != null ? ObjModel.Zip.Trim() : string.Empty);

                if (ObjModel.Jurisdiction != "-1")
                {
                    ObjtblOrderSearchedData.search_county = ObjModel.JurisdictionText;
                    ObjtblOrderSearchedData.best_search_county = ObjModel.JurisdictionText;
                    ObjtblOrderSearchedData.SearchedCountyId = Convert.ToInt32(ObjModel.Jurisdiction.ToString().Split('_').FirstOrDefault());
                }

                ObjtblOrderSearchedData.SearchedDriverLicense = (ObjModel.DriverLicense != null ? ObjModel.DriverLicense.Trim() : string.Empty);
                ObjtblOrderSearchedData.SearchedVehicleVin = (ObjModel.VehicleVin != null ? ObjModel.VehicleVin.Trim() : string.Empty);

                if (ObjModel.DLState != "-1")
                {
                    ObjtblOrderSearchedData.SearchedDLStateId = int.Parse(ObjModel.DLState.ToString().Split('_').FirstOrDefault());
                }

                ObjtblOrderSearchedData.SearchedSex = (ObjModel.Sex != "-1") ? ObjModel.Sex : string.Empty;
                ObjtblOrderSearchedData.SearchedBusinessName = (ObjModel.BusinessName != null ? ObjModel.BusinessName.Trim() : string.Empty);
                ObjtblOrderSearchedData.SearchedBusinessCity = (ObjModel.BusinessCity != null ? ObjModel.BusinessCity.Trim() : string.Empty);
                if (ObjModel.BusinessState != -1)
                {
                    ObjtblOrderSearchedData.SearchedBusinessStateId = Convert.ToInt32(ObjModel.BusinessState);
                }
                ObjtblOrderSearchedData.SearchedTrackingRef = ReferenceCode;
                if (ReferenceCodeId != 0)
                {
                    ObjtblOrderSearchedData.SearchedTrackingRefId = ReferenceCodeId;
                }
                ObjtblOrderSearchedData.SearchedTrackingNotes = (ObjModel.TrackingNotes != null ? ObjModel.TrackingNotes.Trim() : string.Empty);
                ObjtblOrderSearchedData.SearchedPermissibleId = ObjModel.PermissiblePurposeId; //ND-5 add this value to tblordersearched data

                #endregion

                #region Insertion in employment table

                List<tblOrderSearchedEmploymentInfo> ObjCollOrderSearchedEmploymentInfo = new List<tblOrderSearchedEmploymentInfo>();
                if (ObjCollProducts.Any(d => d.Contains("EMV")))
                {

                    //string[] CompanyName = frm.GetValue("CompanyName").AttemptedValue.Split(',');
                    string[] CompanyCity = frm.GetValue("CompanyCity").AttemptedValue.Split(',');
                    string[] CompanyPhone = frm.GetValue("CompanyPhone").AttemptedValue.Split(',');
                    //string[] AliasName = frm.GetValue("AliasName").AttemptedValue.Split(',');
                    string[] CompanyState = frm.GetValue("CompanyState").AttemptedValue.Split(',');
                    for (int iRow = 0; iRow < CompanyName.Count(); iRow++)
                    {
                        tblOrderSearchedEmploymentInfo ObjtblOrderSearchedEmploymentInfo = new tblOrderSearchedEmploymentInfo();
                        ObjtblOrderSearchedEmploymentInfo.SearchedAlias = AliasName[iRow];
                        ObjtblOrderSearchedEmploymentInfo.SearchedName = CompanyName[iRow];
                        ObjtblOrderSearchedEmploymentInfo.SearchedCity = CompanyCity[iRow];
                        ObjtblOrderSearchedEmploymentInfo.SearchedState = CompanyState[iRow];
                        ObjtblOrderSearchedEmploymentInfo.SearchedPhone = CompanyPhone[iRow] == "(___) ___-____" ? "" : CompanyPhone[iRow];

                        ObjtblOrderSearchedEmploymentInfo.SearchedContact = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedStarted = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedEnded = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedEmail = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedTitleStart = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedTitleEnd = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedSupervisorContact = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedReason = string.Empty;

                        ObjCollOrderSearchedEmploymentInfo.Add(ObjtblOrderSearchedEmploymentInfo);
                        if (iRow > 0)
                        {
                            ObjCollProducts.Add("EMV_0");
                        }
                    }
                }

                #endregion

                #region Insertion in education table

                List<tblOrderSearchedEducationInfo> ObjCollOrderSearchedEducationInfo = new List<tblOrderSearchedEducationInfo>();
                if (ObjCollProducts.Any(d => d.Contains("EDV")))
                {
                    //string[] SchoolName = frm.GetValue("SchoolName").AttemptedValue.Split(',');
                    string[] InstituteState = frm.GetValue("InstituteState").AttemptedValue.Split(',');
                    string[] SchoolCity = frm.GetValue("SchoolCity").AttemptedValue.Split(',');
                    string[] SchoolPhone = frm.GetValue("SchoolPhone").AttemptedValue.Split(',');
                    string[] GraduationDt = frm.GetValue("SearchedGraduationDt").AttemptedValue.Split(',');//INT336 GraduationDt

                    for (int iRow = 0; iRow < SchoolName.Count(); iRow++)
                    {

                        tblOrderSearchedEducationInfo ObjtblOrderSearchedEducationInfo = new tblOrderSearchedEducationInfo();
                        ObjtblOrderSearchedEducationInfo.SearchedSchoolName = SchoolName[iRow];
                        ObjtblOrderSearchedEducationInfo.SearchedCity = SchoolCity[iRow];
                        ObjtblOrderSearchedEducationInfo.SearchedPhone = SchoolPhone[iRow] == "(___) ___-____" ? "" : SchoolPhone[iRow];
                        ObjtblOrderSearchedEducationInfo.SearchedGraduationDt = GraduationDt[iRow];//INT336 GraduationDt
                        ObjtblOrderSearchedEducationInfo.SearchedAlias = InstituteAlias[iRow];
                        ObjtblOrderSearchedEducationInfo.SearchedState = InstituteState[iRow];

                        ObjtblOrderSearchedEducationInfo.SearchedStarted = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedDegree = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedEnded = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedMajor = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedFax = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedGrade = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedCredit = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedEmail = string.Empty;

                        ObjCollOrderSearchedEducationInfo.Add(ObjtblOrderSearchedEducationInfo);
                        if (iRow > 0)
                        {
                            ObjCollProducts.Add("EDV_0");
                        }
                        // change for INT 328 BILLING ISSue on 10 march 2016
                        //According to greg ,we have to find add new amount in BILLING AND IN ITEMISED REPPORT SECTION
                    }
                }

                #endregion

                #region Insertion in Personal table and Question table

                List<tblOrderSearchedPersonalInfo> ObjCollOrderSearchedPersonalInfo = new List<tblOrderSearchedPersonalInfo>();
                List<tblOrderPersonalQtnInfo> objColltblOrderPersonalQtnInfo = new List<tblOrderPersonalQtnInfo>();
                if (ObjCollProducts.Any(d => d.Contains("PRV")))
                {
                    //string[] ReferenceName = frm.GetValue("ReferenceName").AttemptedValue.Split(',');
                    string[] ReferencePhone = frm.GetValue("ReferencePhone").AttemptedValue.Split(',');
                    //string[] ReferenceAlias = frm.GetValue("ReferenceAlias").AttemptedValue.Split(',');
                    for (int iRow = 0; iRow < ReferenceName.Count(); iRow++)
                    {
                        //Guid pkOrderSearchedPersnlId = Guid.NewGuid();

                        #region For other grid Controls

                        tblOrderSearchedPersonalInfo ObjtblOrderSearchedPersonalInfo = new tblOrderSearchedPersonalInfo();
                        ObjtblOrderSearchedPersonalInfo.SearchedName = ReferenceName[iRow];
                        ObjtblOrderSearchedPersonalInfo.SearchedPhone = ReferencePhone[iRow] == "(___) ___-____" ? "" : ReferencePhone[iRow];
                        //ObjtblOrderSearchedPersonalInfo.pkOrderSearchedPersnlId = pkOrderSearchedPersnlId;
                        ObjtblOrderSearchedPersonalInfo.SearchedAlias = ReferenceAlias[iRow];

                        ObjtblOrderSearchedPersonalInfo.SearchedCity = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedState = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedAltPhone = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedRelation = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedEmail = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedSalary = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedTitle = string.Empty;

                        ObjCollOrderSearchedPersonalInfo.Add(ObjtblOrderSearchedPersonalInfo);
                        if (iRow > 0)
                        {
                            ObjCollProducts.Add("PRV_0");
                        }

                        // change for INT 328 BILLING ISSue on 10 march 2016
                        //According to greg ,we have to find add new amount in BILLING AND IN ITEMISED REPPORT SECTION 
                        #endregion

                        #region For questions

                        tblOrderPersonalQtnInfo objtblOrderPersonalQtnInfo = new tblOrderPersonalQtnInfo();
                        objColltblOrderPersonalQtnInfo.Add(objtblOrderPersonalQtnInfo);

                        #endregion
                    }

                }

                #endregion

                #region Insertion in License verification table

                List<tblOrderSearchedLicenseInfo> ObjCollOrderSearchedLicenseInfo = new List<tblOrderSearchedLicenseInfo>();

                if (ObjCollProducts.Any(d => d.Contains("PLV")))
                {
                    //string[] LicenseType = frm.GetValue("LicenseType").AttemptedValue.Split(',');
                    string[] LicenseState = frm.GetValue("LicenseState").AttemptedValue.Split(',');
                    string[] LicenseNumber = frm.GetValue("LicenseNumber").AttemptedValue.Split(',');
                    string[] LicenseIssueDate = frm.GetValue("LicenseIssueDate").AttemptedValue.Split(',');
                    //string[] LicenseAlias = frm.GetValue("LicenseAlias").AttemptedValue.Split(',');
                    for (int iRow = 0; iRow < LicenseType.Count(); iRow++)
                    {
                        tblOrderSearchedLicenseInfo ObjtblOrderSearchedLicenseInfo = new tblOrderSearchedLicenseInfo();
                        // ObjtblOrderSearchedLicenseInfo.pkOrderLicenseVerifyId = Guid.NewGuid();
                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseType = LicenseType[iRow];
                        ObjtblOrderSearchedLicenseInfo.SearchedIssueDate = LicenseIssueDate[iRow];
                        ObjtblOrderSearchedLicenseInfo.SearchedIssuingState = LicenseState[iRow];
                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseNo = LicenseNumber[iRow];
                        ObjtblOrderSearchedLicenseInfo.SearchedAlias = LicenseAlias[iRow];

                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseName = string.Empty;
                        //ObjtblOrderSearchedLicenseInfo.SearchedLicenseType = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedAuthority = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedPhone = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedExpiryDate = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedStatus = string.Empty;
                        ObjCollOrderSearchedLicenseInfo.Add(ObjtblOrderSearchedLicenseInfo);
                        // change for INT 328 BILLING ISSue on 10 march 2016
                        //According to greg ,we have to find add new amount in BILLING AND IN ITEMISED REPPORT SECTION 
                        if (iRow > 0)
                        {
                            ObjCollProducts.Add("PLV_0");
                        }
                    }
                }

                if (ObjCollProducts.Any(d => d.Contains("WCV")))
                {
                    //string[] LicenseType = frm.GetValue("LicenseType").AttemptedValue.Split(',');
                    string[] LicenseState = frm.GetValue("LicenseState").AttemptedValue.Split(',');
                    string[] LicenseNumber = frm.GetValue("LicenseNumber").AttemptedValue.Split(',');
                    string[] LicenseIssueDate = frm.GetValue("LicenseIssueDate").AttemptedValue.Split(',');
                    //string[] LicenseAlias = frm.GetValue("LicenseAlias").AttemptedValue.Split(',');
                    for (int iRow = 0; iRow < LicenseType.Count(); iRow++)
                    {
                        tblOrderSearchedLicenseInfo ObjtblOrderSearchedLicenseInfo = new tblOrderSearchedLicenseInfo();
                        //ObjtblOrderSearchedLicenseInfo.pkOrderLicenseVerifyId = Guid.NewGuid();
                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseType = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedIssueDate = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedIssuingState = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseNo = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedAlias = LicenseAlias[iRow];

                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseName = string.Empty;
                        //ObjtblOrderSearchedLicenseInfo.SearchedLicenseType = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedAuthority = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedPhone = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedExpiryDate = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedStatus = string.Empty;
                        ObjCollOrderSearchedLicenseInfo.Add(ObjtblOrderSearchedLicenseInfo);
                        // change for INT 328 BILLING ISSue on 10 march 2016
                        //According to greg ,we have to find add new amount in BILLING AND IN ITEMISED REPPORT SECTION 
                        if (iRow > 0)
                        {
                            ObjCollProducts.Add("WCV_0");
                        }
                    }
                }

                #endregion

                #region For Social Diligence

                #region Insertion in Alias PersonalInfo table

                // List<tblOrderSearchedAliasPersonalInfo> ObjCollOrderSearchedAliasPersonalInfo = new List<tblOrderSearchedAliasPersonalInfo>();

                #endregion

                #region Insertion in Alias Location Info table

                //List<tblOrderSearchedAliasLocationInfo> ObjCollOrderSearchedAliasLocationInfo = new List<tblOrderSearchedAliasLocationInfo>();

                #endregion

                #region Insertion in Alias Work Info table

                //List<tblOrderSearchedAliasWorkInfo> ObjCollOrderSearchedAliasWorkInfo = new List<tblOrderSearchedAliasWorkInfo>();

                #endregion

                #endregion

                #region Insertion in LiveRunnerInfo table For CCR's Reports
                List<tblOrderSearchedLiveRunnerInfo> ObjColltblOrderSearchedLiveRunnerInfo = new List<tblOrderSearchedLiveRunnerInfo>();
                string Counties = string.Empty;
                string States = string.Empty;
                var element = frm.GetValue("listcounty").AttemptedValue.Split(new[] { "countycollection" }, StringSplitOptions.None);

                for (int i = 0; i < element.Length; i++)
                {
                    if (string.IsNullOrEmpty(element[i].ToString()) == false)
                    {
                        var County_State = element[i].ToString().Split(new Char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);

                        string County_str = County_State[1].ToString();
                        string State_str = County_State[2].ToString();
                        if (string.IsNullOrEmpty(County_str) == false && string.IsNullOrEmpty(State_str) == false) { Counties = Counties + County_str + "_" + State_str + "#"; }
                        if (string.IsNullOrEmpty(State_str) == false) { States = States + State_str + "#"; }


                    }
                }

                ObjColltblOrderSearchedLiveRunnerInfo = ListOrderSearchedLiveRunnerInfoBind(Counties, States, ObjModel);

                #endregion

                #region Live Runner Information For BAL Prcocessing
                //Dictionary<string, string> dictRcxProducts = new Dictionary<string, string>();
                #endregion

                #region Null DOB
                int IsNullDOB = 0;
                if (ObjModel.chkIsNullDOB)
                    IsNullDOB = 1;
                #endregion

                #region Insert In OrderDetails For Consent Form

                string ConsentEmailPDFPath = Server.MapPath("~/Resources/Upload/ConsentForm/usaintelConsentForm.pdf");
                List<CLSConsentInfo> ObjCollConsentInfo = new List<CLSConsentInfo>();

                if (frm.GetValue("ConsentReport") != null)
                {

                    VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n " + System.DateTime.Now + "  CompanyId" + ObjModel.pkCompanyId + " frm.GetValue(ConsentReport) is not null ", "RunAllCountyLogIssue");

                    string[] ConsentFaxEmail = new string[0];
                    string[] ConsentReport = frm.GetValue("ConsentReport").AttemptedValue.Split(',');
                    string[] ConsentState = frm.GetValue("ConsentState").AttemptedValue.Split(',');
                    // string[] ConsentFile = frm.GetValue("ConsentFile").AttemptedValue.Split(',');
                    if (frm.GetValue("ConsentFaxEmail") != null)
                    {
                        ConsentFaxEmail = frm.GetValue("ConsentFaxEmail").AttemptedValue.Split(',');
                    }
                    string FileName = string.Empty;
                    string sFilePath = "";

                    for (int i = 0; i < ConsentReport.Length; i++)
                    {

                        bool FAXEmail = false;
                        if (ConsentFaxEmail.Contains(i.ToString()))
                        {
                            FAXEmail = true;
                        }
                        HttpPostedFileBase ObjHttpPostedFile = Request.Files[i];
                        if (ObjHttpPostedFile.FileName != "")
                        {
                            string sExt = Path.GetExtension(ObjHttpPostedFile.FileName).ToLower();
                            FileName = DateTime.Now.ToFileTime().ToString() + sExt;
                            sFilePath = Server.MapPath("~/Resources/Upload/ConsentForm/") + FileName;
                            ObjHttpPostedFile.SaveAs(sFilePath);
                        }
                        else
                        {
                            FileName = string.Empty;
                        }


                        CLSConsentInfo ObjConsentInfo = new CLSConsentInfo();
                        ObjConsentInfo.pkConsentId = i + 1;
                        ObjConsentInfo.ConsentFileName = FileName;
                        ObjConsentInfo.IsConsentEmailOrFax = FAXEmail;
                        ObjConsentInfo.IsConsentRequired = true;
                        ObjConsentInfo.ConsentReportType = ConsentReport[i];
                        ObjConsentInfo.ConsentState = ConsentState[i];
                        ObjConsentInfo.PDFAttachFilePath = ConsentEmailPDFPath;
                        ObjCollConsentInfo.Add(ObjConsentInfo);
                    }
                }
                #endregion Insert In OrderDetails For Consent Form

                #region Calling Business Layer Method for insertion in db

                EmergeReport ObjEmergeReport = new EmergeReport();
                //ObjEmergeReport.ObjCollOrderSearchedAliasLocationInfo = ObjCollOrderSearchedAliasLocationInfo;
                //ObjEmergeReport.ObjCollOrderSearchedAliasPersonalInfo = ObjCollOrderSearchedAliasPersonalInfo;
                //ObjEmergeReport.ObjCollOrderSearchedAliasWorkInfo = ObjCollOrderSearchedAliasWorkInfo;
                ObjEmergeReport.ObjCollOrderSearchedEducationInfo = ObjCollOrderSearchedEducationInfo;
                ObjEmergeReport.ObjCollOrderSearchedEmploymentInfo = ObjCollOrderSearchedEmploymentInfo;
                ObjEmergeReport.ObjCollOrderSearchedLicenseInfo = ObjCollOrderSearchedLicenseInfo;
                ObjEmergeReport.ObjCollOrderSearchedPersonalInfo = ObjCollOrderSearchedPersonalInfo;
                ObjEmergeReport.ObjCollProducts = ObjCollProducts;
                ObjEmergeReport.objColltblOrderPersonalQtnInfo = objColltblOrderPersonalQtnInfo;
                ObjEmergeReport.ObjtblOrder = ObjtblOrder;
                ObjEmergeReport.ObjtblOrderSearchedData = ObjtblOrderSearchedData;
                ObjEmergeReport.ObjColltblOrderSearchedLiveRunnerInfo = ObjColltblOrderSearchedLiveRunnerInfo;
                //  ObjEmergeReport.ObjCompnyProductInfo = ObjCompnyProductInfo;
                ObjEmergeReport.IsNullDOB = IsNullDOB;
                ObjEmergeReport.IsLiveRunnerState = IsLiveRunnerState;
                ObjEmergeReport.ObjCollCLSConsentInfo = ObjCollConsentInfo;
                ObjEmergeReport.ObjTieredPackage = ObjtblTieredPackage;
                ObjEmergeReport.ObjOrderInfo = ObjOrderInfo;
                ObjEmergeReport.CompanyAccountNo = ObjModel.CompanyAccountNo;
                ObjEmergeReport.OrderCompanyName = ObjModel.OrderCompanyName;
                ObjEmergeReport.PermissiblePurpose = ObjModel.PermissiblePurposeValue;
                string sCurrentUserXMLFile = DateTime.Now.ToFileTime().ToString() + ".xml";

                Session["NewReportXMLPath"] = sCurrentUserXMLFile;
                Session["EmergeReport"] = ObjEmergeReport;


                XmlSerializer serializer = new XmlSerializer(typeof(EmergeReport));

                using (TextWriter textWriter = new StreamWriter(Server.MapPath(@"~\Resources\Upload\NewReportXML\" + sCurrentUserXMLFile)))
                {
                    serializer.Serialize(textWriter, ObjEmergeReport);
                }


                #endregion

                serializer = null;

                TempData["IsErrorAdditionalRptRan"] = "true";
                //return RedirectToAction("Searching", "Corporate");
                //From Searching Page we call SaveNewReport method and it will save report in db and call vendor
                //}

                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n " + System.DateTime.Now + "  CompanyId" + ObjModel.pkCompanyId + " Work fine", "RunAllCountyLogIssue");
                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n " + System.DateTime.Now + "  CompanyId" + ObjModel.pkCompanyId + " Work fine", "RunAllCountyLogIssue");

            }


            if (frm.GetValue("category_name").AttemptedValue == "eScreen")
            {

                StringBuilder Request = EscreenRequest(ObjModel);
                try
                {
                    using (SingleSignOn oSSO = new SingleSignOn())
                    {
                        string certPath = ConfigurationManager.AppSettings["eScreenCertificatePath"].ToString();
                        // string certPath = @"C:\inetpub\vhosts\test.intelifi.com\emergeMVC\certs\intelifi.pfx";//"C:\inetpub\vhosts\emerge.intelifi.com\certs\intelifi.pfx";"D:\Tickets\25eScreen\intelifi.pfx";
                        oSSO.Url = @"https://services.escreen.com/SingleSignOn/SingleSignOn.asmx";

                        if (certPath != "")
                        {
                            X509Certificate2 cert = new X509Certificate2(certPath, "B@u9*xaEns", X509KeyStorageFlags.MachineKeySet);
                            oSSO.ClientCertificates.Add(cert);
                        }

                        string ResponseXML = oSSO.RequestTicket(Request.ToString());
                        XDocument ObjXDocumentresponse = new XDocument();
                        ObjXDocumentresponse = XDocument.Parse(ResponseXML);
                        var TicketResponse = ObjXDocumentresponse.Descendants("TicketResponse").ToList();
                        if (TicketResponse.Count > 0)
                        {
                            var Result = TicketResponse.Descendants("Result").FirstOrDefault();
                            var Action = Result.Descendants("Action").FirstOrDefault();
                            if (Action != null)
                            {
                                string responseurl = Action.Value.ToString();
                                TempData["info"] = "eScreenRequest";
                                TempData["url"] = responseurl;
                                return RedirectToAction("NewReport", "Corporate");
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n " + System.DateTime.Now + "  CompanyId" + ObjModel.pkCompanyId + " Error Information" + ex.Message.ToString(), "RunAllCountyLogIssue");

                }



            }
            else
            {
                if (ModelState.IsValid)
                {
                    return RedirectToAction("Searching", "Corporate");
                }


            }
            return View();
        }
        #endregion









        public ActionResult SubmitAdditionalReport1()
        {
            return View();
        }

        #region Submit New Report Log





        // int 194 changes 
        public ActionResult SubmitNewReportLog(NewReportModel ObjModel, FormCollection frm)
        {
            if (ModelState.IsValid)
            {

                int LocationId = ObjModel.fkLocationId;
                int CompanyUserId = ObjModel.pkCompanyUserId;
                int fkCompanyId = ObjModel.pkCompanyId;

                OrderInfo ObjOrderInfo = new OrderInfo();
                tblTieredPackage ObjtblTieredPackage = new tblTieredPackage();
                // Dictionary<string, Guid> ObjCompnyProductInfo = new Dictionary<string, Guid>();
                int TieredPackageid = 0;

                #region Product Selected
                int CheckTieredPackageiD = 0;
                //This is basically for CCR1, CCR2 and RCX check. when we have more than 1 CCR1 checkbox selected then we differntiate with pkOrderDetailId in collproducts
                // we don't need pkOrderdetail id for sending to BALOrder so we removed id from here.
                List<string> ObjCollProducts = ProductsLogSelectedList(ObjModel.ProductsSelected);

                List<string> ObjCollProductsNew = ProductsSelectedList(ObjModel.ProductsSelected);

                ObjCollProductsNew = FindControlsinSelectedReports(ObjCollProductsNew);

                for (int i = 0; i < ObjCollProductsNew.Count; i++)
                {
                    string[] ProductFullName = ObjCollProductsNew.ElementAt(i).Split('_');
                    int.TryParse(ProductFullName[1], out TieredPackageid);

                    using (EmergeDALDataContext dx1 = new EmergeDALDataContext())
                    {

                        //if (TieredPackageid != 0)

                        if (Convert.ToInt32(ProductFullName[1]) != 0) //edit for INT-194
                        {
                            ObjtblTieredPackage = dx1.tblTieredPackages.Where(d => d.fkPackageId == Convert.ToInt32(ProductFullName[1])).FirstOrDefault();
                            if (ObjtblTieredPackage != null)
                            {

                                TieredPackageid = Convert.ToInt32(ProductFullName[1]);
                                CheckTieredPackageiD = TieredPackageid;
                            }
                        }
                    }


                    if (ProductFullName[0] == "NCRPLUS")
                    {
                        ObjCollProductsNew.RemoveAt(i);
                        ObjCollProductsNew.Add("NCR+_" + ProductFullName[1]);
                    }
                    if (ProductFullName[0] == "NCR2")//Ticket #740: Incomplete report on NCR2 and error log is not shwing #17
                    {
                        if (ObjModel.Social.Trim() == "")
                        {
                            ObjModel.Social = "999-99-9999";
                        }
                    }
                }

                #endregion

                #region Tiered Section
                //  if (TieredPackageid != 0)
                //{
                if (CheckTieredPackageiD != 0) //edit for INT-194
                {
                    TieredPackageid = CheckTieredPackageiD;// ADD FOR TAKE NON ZERO PACKAGE ID 
                    using (EmergeDALDataContext dx = new EmergeDALDataContext())
                    {
                        ObjtblTieredPackage = dx.tblTieredPackages.Where(d => d.fkPackageId == TieredPackageid).FirstOrDefault();
                    }
                    if (ObjtblTieredPackage != null)
                    {
                        ObjOrderInfo.IsTiered = true;
                        ObjOrderInfo.IsAutomatic = ObjtblTieredPackage.IsAutomaticRun;
                        if (ObjtblTieredPackage.Variable == "County State")
                        {
                            ObjOrderInfo.Variable = "1";
                        }
                        else if (ObjtblTieredPackage.Variable == "State")
                        {
                            ObjOrderInfo.Variable = "2";
                        }
                    }
                }


                #endregion

                #region Insertion in main order table

                DateTime Dated = DateTime.Now;


                tblOrder ObjtblOrder = new tblOrder();
                ObjtblOrder.fkLocationId = LocationId;
                ObjtblOrder.fkCompanyUserId = CompanyUserId;
                string OrderNo = Dated.ToString("yyMMddHHmmssfff");
                ObjtblOrder.OrderNo = OrderNo;
                ObjtblOrder.OrderDt = Dated;
                ObjtblOrder.OrderStatus = (byte)OrderStatus.Pending;
                ObjtblOrder.CreatedDate = Dated;
                ObjtblOrder.CreatedById = CompanyUserId;
                ObjtblOrder.SyncpodImage = ObjModel.hdnSyncpodImage;
                ObjtblOrder.FaceImage = ObjModel.hdnFaceImage;
                ObjtblOrder.OrderType = ObjModel.OrderType;
                ObjtblOrder.ReferenceCode = string.Empty;
                ObjtblOrder.SearchPersonHTML = Request.ServerVariables["HTTP_USER_AGENT"] + " OS- " + Environment.OSVersion.Version.ToString() + " Report Run From New Report Log Page."; ;
                if (ObjModel.hdnIsSyncpod == "true")
                {
                    ObjtblOrder.IsSyncpod = true;
                }
                else
                {
                    ObjtblOrder.IsSyncpod = false;
                }

                #endregion

                #region Insert new ReferenceCode for company

                int ReferenceCodeId = 0;
                string ReferenceCode = string.Empty;
                if (Convert.ToInt32(ObjModel.TrackingRefCode) > 0)
                {
                    ReferenceCodeId = Convert.ToInt32(ObjModel.TrackingRefCode);
                    ReferenceCode = ObjModel.TrackingRefCodeText;
                }
                else if (ObjModel.TrackingRefCode == -3 && ObjModel.TrackingRefCodeValue != "")
                {
                    ReferenceCode = ObjModel.TrackingRefCodeValue.Trim();
                    tblReferenceCode ObjtblReferenceCode = new tblReferenceCode();
                    BALReferenceCode ObjBALReferenceCode = new BALReferenceCode();
                    //DateTime CreatedDate = DateTime.Now;

                    ObjtblReferenceCode.fkCompanyId = fkCompanyId;
                    ObjtblReferenceCode.ReferenceCode = ObjModel.TrackingRefCodeValue;
                    ObjtblReferenceCode.ReferenceNote = ObjModel.TrackingNotes;
                    int result = ObjBALReferenceCode.AddReference(ObjtblReferenceCode, fkCompanyId, true);
                    if (result > 0)
                    {
                        ReferenceCodeId = result;
                    }
                }

                #endregion

                #region Insertion in order searched table

                tblOrderSearchedData ObjtblOrderSearchedData = new tblOrderSearchedData();

                ObjtblOrderSearchedData.SearchedLastName = (ObjModel.LastName != null ? ObjModel.LastName.Trim() : string.Empty);
                ObjtblOrderSearchedData.SearchedFirstName = (ObjModel.FirstName != null ? ObjModel.FirstName.Trim() : string.Empty);
                ObjtblOrderSearchedData.SearchedMiddleInitial = ObjModel.MI ?? string.Empty;
                ObjtblOrderSearchedData.SearchedSuffix = (ObjModel.SuffixValue != "-1") ? ObjModel.SuffixValue : string.Empty;
                ObjtblOrderSearchedData.SearchedSsn = ObjModel.Social ?? string.Empty;
                ObjtblOrderSearchedData.SearchedDob = ObjModel.DOB ?? string.Empty;
                ObjtblOrderSearchedData.SearchedPhoneNumber = ObjModel.Phone ?? string.Empty;
                ObjtblOrderSearchedData.SearchedApplicantEmail = ObjModel.ApplicantEmail ?? string.Empty;
                ObjtblOrderSearchedData.SearchedStreetAddress = ObjModel.Address ?? string.Empty;
                ObjtblOrderSearchedData.SearchedDirection = (ObjModel.DirectionValue != "-1") ? ObjModel.DirectionValue : string.Empty;
                ObjtblOrderSearchedData.SearchedStreetName = ObjModel.Street ?? string.Empty;
                ObjtblOrderSearchedData.SearchedStreetType = (ObjModel.TypeValue != "-1" && ObjModel.TypeValue != null) ? ObjModel.TypeValue : string.Empty;
                ObjtblOrderSearchedData.SearchedApt = ObjModel.Apt ?? string.Empty;
                ObjtblOrderSearchedData.SearchedCity = ObjModel.City ?? string.Empty;

                bool IsLiveRunnerState = false;

                if (ObjModel.State != "-1")
                {
                    string[] SelectedStateValues = ObjModel.State.ToString().Split('_');
                    string SelectedState = SelectedStateValues[0].Trim();
                    BALCompanyUsers objBALCompanyUsers = new BALCompanyUsers();
                    bool result = objBALCompanyUsers.StateCheckStatusLiveRunner(Convert.ToInt32(SelectedState));
                    IsLiveRunnerState = result;
                    ObjtblOrderSearchedData.SearchedStateId = int.Parse(SelectedState);
                }

                ObjtblOrderSearchedData.SearchedZipCode = (ObjModel.Zip != null ? ObjModel.Zip.Trim() : string.Empty);
                ObjtblOrderSearchedData.best_SearchedZipCode = (ObjModel.Zip != null ? ObjModel.Zip.Trim() : string.Empty);

                if (ObjModel.Jurisdiction != "-1")
                {
                    ObjtblOrderSearchedData.search_county = ObjModel.JurisdictionText;
                    ObjtblOrderSearchedData.best_search_county = ObjModel.JurisdictionText;
                    ObjtblOrderSearchedData.SearchedCountyId = Convert.ToInt32(ObjModel.Jurisdiction.ToString().Split('_').FirstOrDefault());
                }

                ObjtblOrderSearchedData.SearchedDriverLicense = (ObjModel.DriverLicense != null ? ObjModel.DriverLicense.Trim() : string.Empty);
                ObjtblOrderSearchedData.SearchedVehicleVin = (ObjModel.VehicleVin != null ? ObjModel.VehicleVin.Trim() : string.Empty);

                if (ObjModel.DLState != "-1")
                {
                    ObjtblOrderSearchedData.SearchedDLStateId = int.Parse(ObjModel.DLState.ToString().Split('_').FirstOrDefault());
                }

                ObjtblOrderSearchedData.SearchedSex = (ObjModel.Sex != "-1") ? ObjModel.Sex : string.Empty;
                ObjtblOrderSearchedData.SearchedBusinessName = (ObjModel.BusinessName != null ? ObjModel.BusinessName.Trim() : string.Empty);
                ObjtblOrderSearchedData.SearchedBusinessCity = (ObjModel.BusinessCity != null ? ObjModel.BusinessCity.Trim() : string.Empty);
                if (ObjModel.BusinessState != -1)
                {
                    ObjtblOrderSearchedData.SearchedBusinessStateId = Convert.ToInt32(ObjModel.BusinessState);
                }
                ObjtblOrderSearchedData.SearchedTrackingRef = ReferenceCode;
                if (ReferenceCodeId != 0)
                {
                    ObjtblOrderSearchedData.SearchedTrackingRefId = ReferenceCodeId;
                }
                ObjtblOrderSearchedData.SearchedTrackingNotes = (ObjModel.TrackingNotes != null ? ObjModel.TrackingNotes.Trim() : string.Empty);
                ObjtblOrderSearchedData.SearchedPermissibleId = ObjModel.PermissiblePurposeId; //ND-5 add this value to tblordersearched data

                #endregion

                #region Insertion in employment table

                List<tblOrderSearchedEmploymentInfo> ObjCollOrderSearchedEmploymentInfo = new List<tblOrderSearchedEmploymentInfo>();
                if (ObjCollProducts.Any(d => d.Contains("EMV")))
                {

                    string[] CompanyName = frm.GetValue("CompanyName").AttemptedValue.Split(',');
                    string[] CompanyCity = frm.GetValue("CompanyCity").AttemptedValue.Split(',');
                    string[] CompanyPhone = frm.GetValue("CompanyPhone").AttemptedValue.Split(',');
                    string[] AliasName = frm.GetValue("AliasName").AttemptedValue.Split(',');
                    string[] CompanyState = frm.GetValue("CompanyState").AttemptedValue.Split(',');
                    for (int iRow = 0; iRow < CompanyName.Count(); iRow++)
                    {
                        tblOrderSearchedEmploymentInfo ObjtblOrderSearchedEmploymentInfo = new tblOrderSearchedEmploymentInfo();
                        ObjtblOrderSearchedEmploymentInfo.SearchedAlias = AliasName[iRow];
                        ObjtblOrderSearchedEmploymentInfo.SearchedName = CompanyName[iRow];
                        ObjtblOrderSearchedEmploymentInfo.SearchedCity = CompanyCity[iRow];
                        ObjtblOrderSearchedEmploymentInfo.SearchedState = CompanyState[iRow];
                        ObjtblOrderSearchedEmploymentInfo.SearchedPhone = CompanyPhone[iRow] == "(___) ___-____" ? "" : CompanyPhone[iRow];

                        ObjtblOrderSearchedEmploymentInfo.SearchedContact = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedStarted = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedEnded = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedEmail = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedTitleStart = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedTitleEnd = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedSupervisorContact = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedReason = string.Empty;

                        ObjCollOrderSearchedEmploymentInfo.Add(ObjtblOrderSearchedEmploymentInfo);
                        if (iRow > 0)
                        {
                            ObjCollProducts.Add("EMV_0");
                        }
                    }
                }

                #endregion

                #region Insertion in education table

                List<tblOrderSearchedEducationInfo> ObjCollOrderSearchedEducationInfo = new List<tblOrderSearchedEducationInfo>();
                if (ObjCollProducts.Any(d => d.Contains("EDV")))
                {
                    string[] SchoolName = frm.GetValue("SchoolName").AttemptedValue.Split(',');
                    string[] InstituteState = frm.GetValue("InstituteState").AttemptedValue.Split(',');
                    string[] SchoolCity = frm.GetValue("SchoolCity").AttemptedValue.Split(',');
                    string[] SchoolPhone = frm.GetValue("SchoolPhone").AttemptedValue.Split(',');
                    string[] GraduationDt = frm.GetValue("SearchedGraduationDt").AttemptedValue.Split(',');//INT336 GraduationDt
                    string[] InstituteAlias = frm.GetValue("InstituteAlias").AttemptedValue.Split(',');

                    for (int iRow = 0; iRow < SchoolName.Count(); iRow++)
                    {

                        tblOrderSearchedEducationInfo ObjtblOrderSearchedEducationInfo = new tblOrderSearchedEducationInfo();
                        ObjtblOrderSearchedEducationInfo.SearchedSchoolName = SchoolName[iRow];
                        ObjtblOrderSearchedEducationInfo.SearchedCity = SchoolCity[iRow];
                        ObjtblOrderSearchedEducationInfo.SearchedPhone = SchoolPhone[iRow] == "(___) ___-____" ? "" : SchoolPhone[iRow];
                        ObjtblOrderSearchedEducationInfo.SearchedGraduationDt = GraduationDt[iRow];//INT336 GraduationDt
                        ObjtblOrderSearchedEducationInfo.SearchedAlias = InstituteAlias[iRow];
                        ObjtblOrderSearchedEducationInfo.SearchedState = InstituteState[iRow];

                        ObjtblOrderSearchedEducationInfo.SearchedStarted = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedDegree = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedEnded = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedMajor = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedFax = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedGrade = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedCredit = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedEmail = string.Empty;

                        ObjCollOrderSearchedEducationInfo.Add(ObjtblOrderSearchedEducationInfo);

                        if (iRow > 0)
                        {
                            ObjCollProducts.Add("EDV_0");
                        }
                        // change for INT 328 BILLING ISSue on 10 march 2016
                        //According to greg ,we have to find add new amount in BILLING AND IN ITEMISED REPPORT SECTION
                    }
                }

                #endregion

                #region Insertion in Personal table and Question table

                List<tblOrderSearchedPersonalInfo> ObjCollOrderSearchedPersonalInfo = new List<tblOrderSearchedPersonalInfo>();
                List<tblOrderPersonalQtnInfo> objColltblOrderPersonalQtnInfo = new List<tblOrderPersonalQtnInfo>();
                if (ObjCollProducts.Any(d => d.Contains("PRV")))
                {
                    string[] ReferenceName = frm.GetValue("ReferenceName").AttemptedValue.Split(',');
                    string[] ReferencePhone = frm.GetValue("ReferencePhone").AttemptedValue.Split(',');
                    for (int iRow = 0; iRow < ReferenceName.Count(); iRow++)
                    {
                        //Guid pkOrderSearchedPersnlId = Guid.NewGuid();

                        #region For other grid Controls

                        tblOrderSearchedPersonalInfo ObjtblOrderSearchedPersonalInfo = new tblOrderSearchedPersonalInfo();
                        ObjtblOrderSearchedPersonalInfo.SearchedName = ReferenceName[iRow];
                        ObjtblOrderSearchedPersonalInfo.SearchedPhone = ReferencePhone[iRow] == "(___) ___-____" ? "" : ReferencePhone[iRow];
                        //ObjtblOrderSearchedPersonalInfo.pkOrderSearchedPersnlId = pkOrderSearchedPersnlId;

                        ObjtblOrderSearchedPersonalInfo.SearchedCity = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedState = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedAltPhone = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedRelation = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedEmail = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedSalary = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedTitle = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedAlias = string.Empty;

                        ObjCollOrderSearchedPersonalInfo.Add(ObjtblOrderSearchedPersonalInfo);
                        if (iRow > 0)
                        {
                            ObjCollProducts.Add("PRV_0");
                        }

                        // change for INT 328 BILLING ISSue on 10 march 2016
                        //According to greg ,we have to find add new amount in BILLING AND IN ITEMISED REPPORT SECTION 
                        #endregion

                        #region For questions

                        tblOrderPersonalQtnInfo objtblOrderPersonalQtnInfo = new tblOrderPersonalQtnInfo();
                        objColltblOrderPersonalQtnInfo.Add(objtblOrderPersonalQtnInfo);

                        #endregion
                    }

                }

                #endregion

                #region Insertion in License verification table

                List<tblOrderSearchedLicenseInfo> ObjCollOrderSearchedLicenseInfo = new List<tblOrderSearchedLicenseInfo>();

                if (ObjCollProducts.Any(d => d.Contains("PLV")))
                {
                    string[] LicenseType = frm.GetValue("LicenseType").AttemptedValue.Split(',');
                    string[] LicenseState = frm.GetValue("LicenseState").AttemptedValue.Split(',');
                    string[] LicenseNumber = frm.GetValue("LicenseNumber").AttemptedValue.Split(',');
                    string[] LicenseIssueDate = frm.GetValue("LicenseIssueDate").AttemptedValue.Split(',');
                    string[] LicenseAlias = frm.GetValue("LicenseAlias").AttemptedValue.Split(',');
                    for (int iRow = 0; iRow < LicenseType.Count(); iRow++)
                    {
                        tblOrderSearchedLicenseInfo ObjtblOrderSearchedLicenseInfo = new tblOrderSearchedLicenseInfo();
                        //ObjtblOrderSearchedLicenseInfo.pkOrderLicenseVerifyId = Guid.NewGuid();
                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseType = LicenseType[iRow];
                        ObjtblOrderSearchedLicenseInfo.SearchedIssueDate = LicenseIssueDate[iRow];
                        ObjtblOrderSearchedLicenseInfo.SearchedIssuingState = LicenseState[iRow];
                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseNo = LicenseNumber[iRow];
                        ObjtblOrderSearchedLicenseInfo.SearchedAlias = LicenseAlias[iRow];

                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseName = string.Empty;
                        //ObjtblOrderSearchedLicenseInfo.SearchedLicenseType = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedAuthority = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedPhone = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedExpiryDate = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedStatus = string.Empty;
                        ObjCollOrderSearchedLicenseInfo.Add(ObjtblOrderSearchedLicenseInfo);
                        // change for INT 328 BILLING ISSue on 10 march 2016
                        //According to greg ,we have to find add new amount in BILLING AND IN ITEMISED REPPORT SECTION 
                        if (iRow > 0)
                        {
                            ObjCollProducts.Add("PLV_0");
                        }
                    }
                }

                #endregion

                #region For Social Diligence

                #region Insertion in Alias PersonalInfo table

                //List<tblOrderSearchedAliasPersonalInfo> ObjCollOrderSearchedAliasPersonalInfo = new List<tblOrderSearchedAliasPersonalInfo>();

                #endregion

                #region Insertion in Alias Location Info table

                //List<tblOrderSearchedAliasLocationInfo> ObjCollOrderSearchedAliasLocationInfo = new List<tblOrderSearchedAliasLocationInfo>();

                #endregion

                #region Insertion in Alias Work Info table

                // List<tblOrderSearchedAliasWorkInfo> ObjCollOrderSearchedAliasWorkInfo = new List<tblOrderSearchedAliasWorkInfo>();

                #endregion

                #endregion

                #region Insertion in LiveRunnerInfo table For CCR's Reports

                List<tblOrderSearchedLiveRunnerInfo> ObjColltblOrderSearchedLiveRunnerInfo = new List<tblOrderSearchedLiveRunnerInfo>();
                int CCRCount = (from db in ObjCollProductsNew where db.Contains("CCR1") || db.Contains("CCR2") || db.Contains("RCX") select db).Count();
                if (CCRCount > 0)
                {
                    string[] CountyInfoOrderDetailId = frm.GetValue("CountyInfoOrderDetailId").AttemptedValue.Split(',');

                    for (int c = 0; c < CCRCount; c++)
                    {

                        int pkOrderDetailId = Convert.ToInt32(ObjCollProductsNew.ElementAt(c).Split('_')[2]);
                        int index = (from d in CountyInfoOrderDetailId where d.Contains(pkOrderDetailId.ToString()) select CountyInfoOrderDetailId.ToList().IndexOf(d)).Last();

                        // List of the Live Runner Products      
                        string[] CountyInfoState = frm.GetValue("CountyInfoState").AttemptedValue.Split(',');
                        string[] CountyInfoCounty = frm.GetValue("CountyInfoCounty").AttemptedValue.Split(',');

                        string[] CountyValueData = CountyInfoCounty[index].Split('_');
                        int CountyId = Convert.ToInt32(CountyValueData[0]);
                        if (CountyId != -1)
                        {
                            string IsRcxCounty = CountyValueData[1];
                            tblOrderSearchedLiveRunnerInfo ObjBALtblOrderSearchedLiveRunnerInfo = new tblOrderSearchedLiveRunnerInfo();
                            ObjBALtblOrderSearchedLiveRunnerInfo.LiveRunnerCounty = CountyValueData[5] + "_" + IsRcxCounty; //ddlCountyInfoCounty.SelectedItem.Text.Trim() + "_" + IsRcxCounty;
                            ObjBALtblOrderSearchedLiveRunnerInfo.LiveRunnerState = CountyInfoState[index];
                            ObjBALtblOrderSearchedLiveRunnerInfo.SearchedCountyId = CountyId;
                            ObjColltblOrderSearchedLiveRunnerInfo.Add(ObjBALtblOrderSearchedLiveRunnerInfo);
                        }

                    }
                }

                #endregion

                #region Live Runner Information For BAL Prcocessing
                // Dictionary<string, string> dictRcxProducts = new Dictionary<string, string>();
                #endregion

                #region Null DOB
                int IsNullDOB = 0;
                if (ObjModel.chkIsNullDOB)
                    IsNullDOB = 1;
                #endregion

                #region Insert In OrderDetails For Consent Form

                string ConsentEmailPDFPath = Server.MapPath("~/Resources/Upload/ConsentForm/usaintelConsentForm.pdf");
                List<CLSConsentInfo> ObjCollConsentInfo = new List<CLSConsentInfo>();

                if (frm.GetValue("ConsentReport") != null)
                {
                    string[] ConsentFaxEmail = new string[0];
                    string[] ConsentReport = frm.GetValue("ConsentReport").AttemptedValue.Split(',');
                    string[] ConsentState = frm.GetValue("ConsentState").AttemptedValue.Split(',');
                    // string[] ConsentFile = frm.GetValue("ConsentFile").AttemptedValue.Split(',');
                    if (frm.GetValue("ConsentFaxEmail") != null)
                    {
                        ConsentFaxEmail = frm.GetValue("ConsentFaxEmail").AttemptedValue.Split(',');
                    }
                    string FileName = string.Empty;
                    string sFilePath = "";

                    for (int i = 0; i < ConsentReport.Length; i++)
                    {

                        bool FAXEmail = false;
                        if (ConsentFaxEmail.Contains(i.ToString()))
                        {
                            FAXEmail = true;
                        }
                        HttpPostedFileBase ObjHttpPostedFile = Request.Files[i];
                        if (ObjHttpPostedFile.FileName != "")
                        {
                            string sExt = Path.GetExtension(ObjHttpPostedFile.FileName).ToLower();
                            FileName = DateTime.Now.ToFileTime().ToString() + sExt;
                            sFilePath = Server.MapPath("~/Resources/Upload/ConsentForm/") + FileName;
                            ObjHttpPostedFile.SaveAs(sFilePath);
                        }
                        else
                        {
                            FileName = string.Empty;
                        }


                        CLSConsentInfo ObjConsentInfo = new CLSConsentInfo();
                        ObjConsentInfo.pkConsentId = i + 1;
                        ObjConsentInfo.ConsentFileName = FileName;
                        ObjConsentInfo.IsConsentEmailOrFax = FAXEmail;
                        ObjConsentInfo.IsConsentRequired = true;
                        ObjConsentInfo.ConsentReportType = ConsentReport[i];
                        ObjConsentInfo.ConsentState = ConsentState[i];
                        ObjConsentInfo.PDFAttachFilePath = ConsentEmailPDFPath;
                        ObjCollConsentInfo.Add(ObjConsentInfo);
                    }
                }
                #endregion Insert In OrderDetails For Consent Form

                #region Calling Business Layer Method for insertion in db

                EmergeReport ObjEmergeReport = new EmergeReport();
                //ObjEmergeReport.ObjCollOrderSearchedAliasLocationInfo = ObjCollOrderSearchedAliasLocationInfo;
                //ObjEmergeReport.ObjCollOrderSearchedAliasPersonalInfo = ObjCollOrderSearchedAliasPersonalInfo;
                //ObjEmergeReport.ObjCollOrderSearchedAliasWorkInfo = ObjCollOrderSearchedAliasWorkInfo;
                ObjEmergeReport.ObjCollOrderSearchedEducationInfo = ObjCollOrderSearchedEducationInfo;
                ObjEmergeReport.ObjCollOrderSearchedEmploymentInfo = ObjCollOrderSearchedEmploymentInfo;
                ObjEmergeReport.ObjCollOrderSearchedLicenseInfo = ObjCollOrderSearchedLicenseInfo;
                ObjEmergeReport.ObjCollOrderSearchedPersonalInfo = ObjCollOrderSearchedPersonalInfo;
                ObjEmergeReport.ObjCollProducts = ObjCollProductsNew;
                ObjEmergeReport.objColltblOrderPersonalQtnInfo = objColltblOrderPersonalQtnInfo;
                ObjEmergeReport.ObjtblOrder = ObjtblOrder;
                ObjEmergeReport.ObjtblOrderSearchedData = ObjtblOrderSearchedData;
                ObjEmergeReport.ObjColltblOrderSearchedLiveRunnerInfo = ObjColltblOrderSearchedLiveRunnerInfo;
                //  ObjEmergeReport.ObjCompnyProductInfo = ObjCompnyProductInfo;
                ObjEmergeReport.IsNullDOB = IsNullDOB;
                ObjEmergeReport.IsLiveRunnerState = IsLiveRunnerState;
                ObjEmergeReport.ObjCollCLSConsentInfo = ObjCollConsentInfo;
                ObjEmergeReport.ObjTieredPackage = ObjtblTieredPackage;
                ObjEmergeReport.ObjOrderInfo = ObjOrderInfo;
                ObjEmergeReport.CompanyAccountNo = ObjModel.CompanyAccountNo;
                ObjEmergeReport.OrderCompanyName = ObjModel.OrderCompanyName;
                ObjEmergeReport.PermissiblePurpose = ObjModel.PermissiblePurposeValue;
                string sCurrentUserXMLFile = DateTime.Now.ToFileTime().ToString() + ".xml";

                Session["NewReportXMLPath"] = sCurrentUserXMLFile;
                Session["EmergeReport"] = ObjEmergeReport;


                XmlSerializer serializer = new XmlSerializer(typeof(EmergeReport));

                using (TextWriter textWriter = new StreamWriter(Server.MapPath(@"~\Resources\Upload\NewReportXML\" + sCurrentUserXMLFile)))
                {
                    serializer.Serialize(textWriter, ObjEmergeReport);
                }


                #endregion

                serializer = null;

                TempData["IsErrorLogRan"] = "true";
                return RedirectToAction("NewReportLog", "Corporate", new { OrderId = 0 });
                //From Searching Page we call SaveNewReport method and it will save report in db and call vendor
            }
            return View();
        }
        public ActionResult SubmitNewReportLogApi(NewReportModel ObjModel, FormCollection frm)
        {
            if (ModelState.IsValid)
            {
                EmergeReport ObjEmergeReport = new EmergeReport();

                //int LocationId = ObjModel.fkLocationId;
                // int CompanyUserId = ObjModel.pkCompanyUserId;
                int fkCompanyId = ObjModel.pkCompanyId;

                OrderInfo ObjOrderInfo = new OrderInfo();
                ObjOrderInfo.IsDataEntry_Enable = false;
                ObjOrderInfo.IsTiered = false;
                ObjOrderInfo.Tiered2Response = string.Empty;
                ObjOrderInfo.OptionalResponse = string.Empty;
                ObjOrderInfo.TieredTopInfo = string.Empty;
                ObjOrderInfo.Variable = string.Empty;
                ObjOrderInfo.OrderId = ObjModel.OrderId;
                BALOrders ObjBALOrders = new BALOrders();
                using (EmergeDALDataContext dx = new EmergeDALDataContext())
                {
                    tblTieredPackage ObjtblTieredPackage = new tblTieredPackage();
                    //Dictionary<string, Guid> ObjCompnyProductInfo = new Dictionary<string, Guid>();
                    int TieredPackageid = 0;
                    int Order_Ids = ObjModel.OrderId;
                    #region Product Selected
                    List<string> ObjCollProducts = ProductsLogSelectedList(ObjModel.ProductsSelected);
                    Session["ProductSelected"] = ObjModel.ProductsSelected;
                    int CheckTieredPackageiD = 0;

                    List<string> ObjCollProductsNew = ProductsSelectedList(ObjModel.ProductsSelected);
                    ObjCollProductsNew = FindControlsinSelectedReports(ObjCollProductsNew);

                    for (int i = 0; i < ObjCollProductsNew.Count; i++)
                    {
                        string[] ProductFullName = ObjCollProductsNew.ElementAt(i).Split('_');

                        int.TryParse(ProductFullName[1], out TieredPackageid);
                        using (EmergeDALDataContext dx1 = new EmergeDALDataContext())
                        {

                            //if (TieredPackageid != 0)

                            if (Convert.ToInt32(ProductFullName[1]) != 0) //edit for INT-194
                            {
                                ObjtblTieredPackage = dx1.tblTieredPackages.Where(d => d.fkPackageId == Convert.ToInt32(ProductFullName[1])).FirstOrDefault();
                                if (ObjtblTieredPackage != null)
                                {

                                    TieredPackageid = Convert.ToInt32(ProductFullName[1]);
                                    CheckTieredPackageiD = TieredPackageid;
                                }
                            }
                        }
                        if (ProductFullName[0] == "NCRPLUS")
                        {
                            ObjCollProductsNew.RemoveAt(i);
                            ObjCollProductsNew.Add("NCR+_" + ProductFullName[1]);
                        }
                        if (ProductFullName[0] == "NCR2")
                        {
                            if (ObjModel.Social.Trim() == "")
                            {
                                ObjModel.Social = "999-99-9999";
                            }
                        }
                    }

                    #endregion

                    #region Tiered Section

                    TieredPackageid = dx.tblOrderDetails.Where(d => d.fkOrderId == Order_Ids).Select(d => d.fkPackageId).FirstOrDefault();

                    //if (TieredPackageid != 0)
                    //{
                    if (CheckTieredPackageiD != 0) //edit for INT-194
                    {
                        TieredPackageid = CheckTieredPackageiD;// ADD FOR TAKE NON ZERO PACKAGE ID 

                        ObjtblTieredPackage = dx.tblTieredPackages.Where(d => d.fkPackageId == TieredPackageid).FirstOrDefault();
                        if (ObjtblTieredPackage != null)
                        {
                            ObjOrderInfo.IsTiered = true;
                            ObjOrderInfo.IsAutomatic = ObjtblTieredPackage.IsAutomaticRun;
                            if (ObjtblTieredPackage.Variable == "County State")
                            {
                                ObjOrderInfo.Variable = "1";
                            }
                            else if (ObjtblTieredPackage.Variable == "State")
                            {
                                ObjOrderInfo.Variable = "2";
                            }
                        }
                    }



                    #endregion


                    #region Insert new ReferenceCode for company

                    int ReferenceCodeId = 0;
                    string ReferenceCode = string.Empty;
                    if (Convert.ToInt32(ObjModel.TrackingRefCode) > 0)
                    {
                        ReferenceCodeId = Convert.ToInt32(ObjModel.TrackingRefCode);
                        ReferenceCode = new BALCompany().GetReferenceValueByRefCode(ReferenceCodeId);
                    }
                    else if (ObjModel.TrackingRefCode == -3 && ObjModel.TrackingRefCodeValue != "")
                    {
                        ReferenceCode = ObjModel.TrackingRefCodeValue.Trim();
                        tblReferenceCode ObjtblReferenceCode = new tblReferenceCode();
                        BALReferenceCode ObjBALReferenceCode = new BALReferenceCode();
                        //DateTime CreatedDate = DateTime.Now;

                        ObjtblReferenceCode.fkCompanyId = fkCompanyId;
                        ObjtblReferenceCode.ReferenceCode = ObjModel.TrackingRefCodeValue;
                        ObjtblReferenceCode.ReferenceNote = ObjModel.TrackingNotes;
                        int result = ObjBALReferenceCode.AddReference(ObjtblReferenceCode, fkCompanyId, true);
                        if (result > 0)
                        {
                            ReferenceCodeId = result;
                        }
                    }

                    #endregion

                    #region update in order searched table

                    UpdatetblOrderSearchedData(ReferenceCodeId, ObjModel, ReferenceCode, Order_Ids);

                    #endregion
                    #region delete in employment table
                    List<tblOrderSearchedEmploymentInfo> ObjCollOrderSearchedEmploymentInfo = new List<tblOrderSearchedEmploymentInfo>();
                    ObjCollOrderSearchedEmploymentInfo = ObjBALOrders.GetOrderSearchedEmploymentInfo(ObjModel.OrderId);
                    BALCompany ObjBALCompany = new BALCompany();
                    ObjBALCompany.DeleteOrderSearchedEmploymentInfo(ObjCollOrderSearchedEmploymentInfo);

                    #endregion

                    #region insert in employment table
                    if (ObjCollProducts.Any(d => d.Contains("EMV")))
                    {

                        string[] CompanyName = frm.GetValue("CompanyName").AttemptedValue.Split(',');
                        string[] CompanyCity = frm.GetValue("CompanyCity").AttemptedValue.Split(',');
                        string[] CompanyPhone = frm.GetValue("CompanyPhone").AttemptedValue.Split(',');
                        string[] AliasName = frm.GetValue("AliasName").AttemptedValue.Split(',');
                        string[] CompanyState = frm.GetValue("CompanyState").AttemptedValue.Split(',');
                        for (int iRow = 0; iRow < CompanyName.Count(); iRow++)
                        {
                            tblOrderSearchedEmploymentInfo ObjtblOrderSearchedEmploymentInfo = new tblOrderSearchedEmploymentInfo();
                            ObjtblOrderSearchedEmploymentInfo.fkOrderId = Order_Ids;
                            ObjtblOrderSearchedEmploymentInfo.SearchedAlias = AliasName[iRow];
                            ObjtblOrderSearchedEmploymentInfo.SearchedName = CompanyName[iRow];
                            ObjtblOrderSearchedEmploymentInfo.SearchedCity = CompanyCity[iRow];
                            ObjtblOrderSearchedEmploymentInfo.SearchedState = CompanyState[iRow];
                            ObjtblOrderSearchedEmploymentInfo.SearchedPhone = CompanyPhone[iRow] == "(___) ___-____" ? "" : CompanyPhone[iRow];

                            ObjtblOrderSearchedEmploymentInfo.SearchedContact = string.Empty;
                            ObjtblOrderSearchedEmploymentInfo.SearchedStarted = string.Empty;
                            ObjtblOrderSearchedEmploymentInfo.SearchedEnded = string.Empty;
                            ObjtblOrderSearchedEmploymentInfo.SearchedEmail = string.Empty;
                            ObjtblOrderSearchedEmploymentInfo.SearchedTitleStart = string.Empty;
                            ObjtblOrderSearchedEmploymentInfo.SearchedTitleEnd = string.Empty;
                            ObjtblOrderSearchedEmploymentInfo.SearchedSupervisorContact = string.Empty;
                            ObjtblOrderSearchedEmploymentInfo.SearchedReason = string.Empty;

                            ObjCollOrderSearchedEmploymentInfo.Add(ObjtblOrderSearchedEmploymentInfo);
                            if (iRow > 0)
                            {
                                ObjCollProducts.Add("EMV_0");
                            }
                        }
                    }
                    ObjBALCompany.CreateSearchedEmploymentInfo(ObjCollOrderSearchedEmploymentInfo);
                    #endregion
                    #region delete in Education table
                    List<tblOrderSearchedEducationInfo> ObjCollOrderSearchedEducationInfo = new List<tblOrderSearchedEducationInfo>();
                    ObjCollOrderSearchedEducationInfo = ObjBALOrders.GetOrderSearchedEducationInfo(ObjModel.OrderId);

                    ObjBALCompany.DeleteOrderSearchedEducationInfo(ObjCollOrderSearchedEducationInfo);
                    #endregion
                    #region insert in Education table

                    if (ObjCollProducts.Any(d => d.Contains("EDV")))
                    {
                        string[] SchoolName = frm.GetValue("SchoolName").AttemptedValue.Split(',');
                        string[] InstituteState = frm.GetValue("InstituteState").AttemptedValue.Split(',');
                        string[] SchoolCity = frm.GetValue("SchoolCity").AttemptedValue.Split(',');
                        string[] SchoolPhone = frm.GetValue("SchoolPhone").AttemptedValue.Split(',');
                        string[] GraduationDt = frm.GetValue("SearchedGraduationDt").AttemptedValue.Split(',');//INT336 GraduationDt
                        string[] InstituteAlias = frm.GetValue("InstituteAlias").AttemptedValue.Split(',');

                        for (int iRow = 0; iRow < SchoolName.Count(); iRow++)
                        {

                            tblOrderSearchedEducationInfo ObjtblOrderSearchedEducationInfo = new tblOrderSearchedEducationInfo();
                            ObjtblOrderSearchedEducationInfo.SearchedSchoolName = SchoolName[iRow];
                            ObjtblOrderSearchedEducationInfo.SearchedCity = SchoolCity[iRow];
                            ObjtblOrderSearchedEducationInfo.SearchedPhone = SchoolPhone[iRow] == "(___) ___-____" ? "" : SchoolPhone[iRow];
                            ObjtblOrderSearchedEducationInfo.SearchedGraduationDt = GraduationDt[iRow];//INT336 GraduationDt
                            ObjtblOrderSearchedEducationInfo.SearchedAlias = InstituteAlias[iRow];
                            ObjtblOrderSearchedEducationInfo.SearchedState = InstituteState[iRow];

                            ObjtblOrderSearchedEducationInfo.SearchedStarted = string.Empty;
                            ObjtblOrderSearchedEducationInfo.SearchedDegree = string.Empty;
                            ObjtblOrderSearchedEducationInfo.SearchedEnded = string.Empty;
                            ObjtblOrderSearchedEducationInfo.SearchedMajor = string.Empty;
                            ObjtblOrderSearchedEducationInfo.SearchedFax = string.Empty;
                            ObjtblOrderSearchedEducationInfo.SearchedGrade = string.Empty;
                            ObjtblOrderSearchedEducationInfo.SearchedCredit = string.Empty;
                            ObjtblOrderSearchedEducationInfo.SearchedEmail = string.Empty;

                            ObjCollOrderSearchedEducationInfo.Add(ObjtblOrderSearchedEducationInfo);
                            if (iRow > 0)
                            {
                                ObjCollProducts.Add("EDV_0");
                            }
                            // change for INT 328 BILLING ISSue on 10 march 2016
                            //According to greg ,we have to find add new amount in BILLING AND IN ITEMISED REPPORT SECTION
                        }
                    }
                    ObjBALCompany.CreateSearchedEducationInfo(ObjCollOrderSearchedEducationInfo);

                    #endregion


                    #region delete in OrderSearchedPersonalInfo
                    List<tblOrderSearchedPersonalInfo> ObjCollOrderSearchedPersonalInfo = new List<tblOrderSearchedPersonalInfo>();
                    ObjCollOrderSearchedPersonalInfo = ObjBALOrders.GetOrderSearchedPersonalInfo(ObjModel.OrderId);

                    ObjBALCompany.DeleteOrderSearchedPersonalInfo(ObjCollOrderSearchedPersonalInfo);
                    #endregion

                    #region Insert in tblOrderSearchedPersonalInfo

                    if (ObjCollProducts.Any(d => d.Contains("PRV")))
                    {
                        string[] ReferenceName = frm.GetValue("ReferenceName").AttemptedValue.Split(',');
                        string[] ReferencePhone = frm.GetValue("ReferencePhone").AttemptedValue.Split(',');
                        for (int iRow = 0; iRow < ReferenceName.Count(); iRow++)
                        {
                            //Guid pkOrderSearchedPersnlId = Guid.NewGuid();

                            #region For other grid Controls

                            tblOrderSearchedPersonalInfo ObjtblOrderSearchedPersonalInfo = new tblOrderSearchedPersonalInfo();
                            ObjtblOrderSearchedPersonalInfo.SearchedName = ReferenceName[iRow];
                            ObjtblOrderSearchedPersonalInfo.SearchedPhone = ReferencePhone[iRow] == "(___) ___-____" ? "" : ReferencePhone[iRow];
                            //ObjtblOrderSearchedPersonalInfo.pkOrderSearchedPersnlId = pkOrderSearchedPersnlId;

                            ObjtblOrderSearchedPersonalInfo.SearchedCity = string.Empty;
                            ObjtblOrderSearchedPersonalInfo.SearchedState = string.Empty;
                            ObjtblOrderSearchedPersonalInfo.SearchedAltPhone = string.Empty;
                            ObjtblOrderSearchedPersonalInfo.SearchedRelation = string.Empty;
                            ObjtblOrderSearchedPersonalInfo.SearchedEmail = string.Empty;
                            ObjtblOrderSearchedPersonalInfo.SearchedSalary = string.Empty;
                            ObjtblOrderSearchedPersonalInfo.SearchedTitle = string.Empty;
                            ObjtblOrderSearchedPersonalInfo.SearchedAlias = string.Empty;

                            ObjCollOrderSearchedPersonalInfo.Add(ObjtblOrderSearchedPersonalInfo);
                            if (iRow > 0)
                            {
                                ObjCollProducts.Add("PRV_0");
                            }

                            // change for INT 328 BILLING ISSue on 10 march 2016
                            //According to greg ,we have to find add new amount in BILLING AND IN ITEMISED REPPORT SECTION 
                            #endregion


                        }

                    }

                    ObjBALCompany.CreateSearchedPersonalInfo(ObjCollOrderSearchedPersonalInfo);










                    #endregion

                    #region delete in OrderSearchedLicenseInfo
                    List<tblOrderSearchedLicenseInfo> ObjCollOrderSearchedLicenseInfo = new List<tblOrderSearchedLicenseInfo>();
                    ObjCollOrderSearchedLicenseInfo = ObjBALOrders.GetOrderSearchedLicenseInfo(ObjModel.OrderId);
                    ObjBALCompany.DeleteOrderSearchedLicenseInfo(ObjCollOrderSearchedLicenseInfo);

                    #endregion
                    #region Insertion in License verification table


                    if (ObjCollProducts.Any(d => d.Contains("PLV")))
                    {
                        string[] LicenseType = frm.GetValue("LicenseType").AttemptedValue.Split(',');
                        string[] LicenseState = frm.GetValue("LicenseState").AttemptedValue.Split(',');
                        string[] LicenseNumber = frm.GetValue("LicenseNumber").AttemptedValue.Split(',');
                        string[] LicenseIssueDate = frm.GetValue("LicenseIssueDate").AttemptedValue.Split(',');
                        string[] LicenseAlias = frm.GetValue("LicenseAlias").AttemptedValue.Split(',');
                        for (int iRow = 0; iRow < LicenseType.Count(); iRow++)
                        {
                            tblOrderSearchedLicenseInfo ObjtblOrderSearchedLicenseInfo = new tblOrderSearchedLicenseInfo();
                            // ObjtblOrderSearchedLicenseInfo.pkOrderLicenseVerifyId = Guid.NewGuid();
                            ObjtblOrderSearchedLicenseInfo.SearchedLicenseType = LicenseType[iRow];
                            ObjtblOrderSearchedLicenseInfo.SearchedIssueDate = LicenseIssueDate[iRow];
                            ObjtblOrderSearchedLicenseInfo.SearchedIssuingState = LicenseState[iRow];
                            ObjtblOrderSearchedLicenseInfo.SearchedLicenseNo = LicenseNumber[iRow];
                            ObjtblOrderSearchedLicenseInfo.SearchedAlias = LicenseAlias[iRow];

                            ObjtblOrderSearchedLicenseInfo.SearchedLicenseName = string.Empty;
                            //ObjtblOrderSearchedLicenseInfo.SearchedLicenseType = string.Empty;
                            ObjtblOrderSearchedLicenseInfo.SearchedAuthority = string.Empty;
                            ObjtblOrderSearchedLicenseInfo.SearchedPhone = string.Empty;
                            ObjtblOrderSearchedLicenseInfo.SearchedExpiryDate = string.Empty;
                            ObjtblOrderSearchedLicenseInfo.SearchedStatus = string.Empty;
                            ObjCollOrderSearchedLicenseInfo.Add(ObjtblOrderSearchedLicenseInfo);
                            // change for INT 328 BILLING ISSue on 10 march 2016
                            //According to greg ,we have to find add new amount in BILLING AND IN ITEMISED REPPORT SECTION 
                            if (iRow > 0)
                            {
                                ObjCollProducts.Add("PLV_0");
                            }
                        }
                    }
                    ObjBALCompany.CreateOrderSearchedLicenseInfo(ObjCollOrderSearchedLicenseInfo);
                    #endregion
                    #region delete in LiveRunnerInfo table For CCR's Reports
                    List<tblOrderSearchedLiveRunnerInfo> ObjColltblOrderSearchedLiveRunnerInfo = new List<tblOrderSearchedLiveRunnerInfo>();
                    ObjColltblOrderSearchedLiveRunnerInfo = ObjBALOrders.GetOrderSearchedLiveRunnerInfo(ObjModel.OrderId);
                    ObjBALCompany.DeleteOrderSearchedLiveRunnerInfo(ObjColltblOrderSearchedLiveRunnerInfo);

                    #endregion
                    #region Insertion in LiveRunnerInfo table For CCR's Reports

                    int CCRCount = (from db in ObjCollProductsNew where db.Contains("CCR1") || db.Contains("CCR2") || db.Contains("RCX") select db).Count();
                    if (CCRCount > 0)
                    {
                        string[] CountyInfoOrderDetailId = frm.GetValue("CountyInfoOrderDetailId").AttemptedValue.Split(',');

                        for (int c = 0; c < CCRCount; c++)
                        {
                            int pkOrderDetailId = Convert.ToInt32(ObjCollProductsNew.ElementAt(c).Split('_')[2]);

                            int index = (from d in CountyInfoOrderDetailId where d.Contains(pkOrderDetailId.ToString()) select CountyInfoOrderDetailId.ToList().IndexOf(d)).LastOrDefault();

                            // List of the Live Runner Products      
                            string[] CountyInfoState = frm.GetValue("CountyInfoState").AttemptedValue.Split(',');
                            string[] CountyInfoCounty = frm.GetValue("CountyInfoCounty").AttemptedValue.Split(',');

                            string[] CountyValueData = CountyInfoCounty[index].Split('_');
                            int CountyId = Convert.ToInt32(CountyValueData[0]);
                            if (CountyId != -1)
                            {
                                string IsRcxCounty = CountyValueData[1];
                                tblOrderSearchedLiveRunnerInfo ObjBALtblOrderSearchedLiveRunnerInfo = new tblOrderSearchedLiveRunnerInfo();
                                ObjBALtblOrderSearchedLiveRunnerInfo.LiveRunnerCounty = CountyValueData[5] + "_" + IsRcxCounty; //ddlCountyInfoCounty.SelectedItem.Text.Trim() + "_" + IsRcxCounty;
                                ObjBALtblOrderSearchedLiveRunnerInfo.LiveRunnerState = CountyInfoState[index];
                                ObjBALtblOrderSearchedLiveRunnerInfo.SearchedCountyId = CountyId;
                                ObjColltblOrderSearchedLiveRunnerInfo.Add(ObjBALtblOrderSearchedLiveRunnerInfo);
                            }

                        }

                    }

                    ObjBALCompany.CreateOrderSearchedLiveRunnerInfo(ObjColltblOrderSearchedLiveRunnerInfo);

                    #endregion

                    //List<tblOrderSearchedEducationInfo> ObjCollOrderSearchedEducationInfo = new List<tblOrderSearchedEducationInfo>();
                    ObjCollOrderSearchedEducationInfo = ObjBALOrders.GetOrderSearchedEducationInfo(ObjModel.OrderId);
                    //List<tblOrderSearchedEmploymentInfo> ObjCollOrderSearchedEmploymentInfo = new List<tblOrderSearchedEmploymentInfo>();
                    ObjCollOrderSearchedEmploymentInfo = ObjBALOrders.GetOrderSearchedEmploymentInfo(ObjModel.OrderId);
                    //List<tblOrderSearchedPersonalInfo> ObjCollOrderSearchedPersonalInfo = new List<tblOrderSearchedPersonalInfo>();
                    ObjCollOrderSearchedPersonalInfo = ObjBALOrders.GetOrderSearchedPersonalInfo(ObjModel.OrderId);

                    //List<tblOrderSearchedLicenseInfo> ObjCollOrderSearchedLicenseInfo = new List<tblOrderSearchedLicenseInfo>();
                    ObjCollOrderSearchedLicenseInfo = ObjBALOrders.GetOrderSearchedLicenseInfo(ObjModel.OrderId);
                    ObjCollProducts = ProductsLogSelectedList(ObjModel.ProductsSelected);
                    List<tblOrderPersonalQtnInfo> objColltblOrderPersonalQtnInfo = new List<tblOrderPersonalQtnInfo>();
                    objColltblOrderPersonalQtnInfo = ObjBALOrders.GetOrderSearchedPersonalQtnInfo(ObjModel.OrderId);
                    tblOrder ObjtblOrder = new tblOrder();
                    ObjtblOrder = ObjBALOrders.GetOrder(ObjModel.OrderId);

                    tblOrderSearchedData ObjtblOrderSearchedData = new tblOrderSearchedData();
                    ObjtblOrderSearchedData = ObjBALOrders.GetOrderSearchedData(ObjModel.OrderId);

                    //List<tblOrderSearchedLiveRunnerInfo> ObjColltblOrderSearchedLiveRunnerInfo = new List<tblOrderSearchedLiveRunnerInfo>();
                    ObjColltblOrderSearchedLiveRunnerInfo = ObjBALOrders.GetOrderSearchedLiveRunnerInfo(ObjModel.OrderId);


                    #region Null DOB
                    int IsNullDOB = 0;
                    if (ObjModel.chkIsNullDOB)
                        IsNullDOB = 1;
                    #endregion

                    bool IsLiveRunnerState = false;

                    if (ObjModel.State != "-1")
                    {
                        string[] SelectedStateValues = ObjModel.State.ToString().Split('_');
                        string SelectedState = SelectedStateValues[0].Trim();
                        BALCompanyUsers objBALCompanyUsers = new BALCompanyUsers();
                        bool result = objBALCompanyUsers.StateCheckStatusLiveRunner(Convert.ToInt32(SelectedState));
                        IsLiveRunnerState = result;



                    }
                    string ConsentEmailPDFPath = Server.MapPath("~/Resources/Upload/ConsentForm/usaintelConsentForm.pdf");
                    List<CLSConsentInfo> ObjCollConsentInfo = new List<CLSConsentInfo>();

                    if (frm.GetValue("ConsentReport") != null)
                    {
                        string[] ConsentFaxEmail = new string[0];
                        string[] ConsentReport = frm.GetValue("ConsentReport").AttemptedValue.Split(',');
                        string[] ConsentState = frm.GetValue("ConsentState").AttemptedValue.Split(',');
                        // string[] ConsentFile = frm.GetValue("ConsentFile").AttemptedValue.Split(',');
                        if (frm.GetValue("ConsentFaxEmail") != null)
                        {
                            ConsentFaxEmail = frm.GetValue("ConsentFaxEmail").AttemptedValue.Split(',');
                        }
                        string FileName = string.Empty;
                        string sFilePath = "";

                        for (int i = 0; i < ConsentReport.Length; i++)
                        {

                            bool FAXEmail = false;
                            if (ConsentFaxEmail.Contains(i.ToString()))
                            {
                                FAXEmail = true;
                            }
                            HttpPostedFileBase ObjHttpPostedFile = Request.Files[i];
                            if (ObjHttpPostedFile.FileName != "")
                            {
                                string sExt = Path.GetExtension(ObjHttpPostedFile.FileName).ToLower();
                                FileName = DateTime.Now.ToFileTime().ToString() + sExt;
                                sFilePath = Server.MapPath("~/Resources/Upload/ConsentForm/") + FileName;
                                ObjHttpPostedFile.SaveAs(sFilePath);
                            }
                            else
                            {
                                FileName = string.Empty;
                            }


                            CLSConsentInfo ObjConsentInfo = new CLSConsentInfo();
                            ObjConsentInfo.pkConsentId = i + 1;
                            ObjConsentInfo.ConsentFileName = FileName;
                            ObjConsentInfo.IsConsentEmailOrFax = FAXEmail;
                            ObjConsentInfo.IsConsentRequired = true;
                            ObjConsentInfo.ConsentReportType = ConsentReport[i];
                            ObjConsentInfo.ConsentState = ConsentState[i];
                            ObjConsentInfo.PDFAttachFilePath = ConsentEmailPDFPath;
                            ObjCollConsentInfo.Add(ObjConsentInfo);
                        }
                    }


                    ObjEmergeReport.ObjCollOrderSearchedEducationInfo = ObjCollOrderSearchedEducationInfo;
                    ObjEmergeReport.ObjCollOrderSearchedEmploymentInfo = ObjCollOrderSearchedEmploymentInfo;
                    ObjEmergeReport.ObjCollOrderSearchedLicenseInfo = ObjCollOrderSearchedLicenseInfo;
                    ObjEmergeReport.ObjCollOrderSearchedPersonalInfo = ObjCollOrderSearchedPersonalInfo;
                    ObjEmergeReport.ObjCollProducts = ObjCollProductsNew;
                    ObjEmergeReport.objColltblOrderPersonalQtnInfo = objColltblOrderPersonalQtnInfo;
                    ObjEmergeReport.ObjtblOrder = ObjtblOrder;
                    ObjEmergeReport.ObjtblOrderSearchedData = ObjtblOrderSearchedData;
                    ObjEmergeReport.ObjColltblOrderSearchedLiveRunnerInfo = ObjColltblOrderSearchedLiveRunnerInfo;
                    ObjEmergeReport.IsNullDOB = IsNullDOB;
                    ObjEmergeReport.IsLiveRunnerState = IsLiveRunnerState;
                    ObjEmergeReport.ObjCollCLSConsentInfo = ObjCollConsentInfo;
                    ObjEmergeReport.ObjTieredPackage = ObjtblTieredPackage;
                    ObjEmergeReport.ObjOrderInfo = ObjOrderInfo;
                    ObjEmergeReport.CompanyAccountNo = ObjModel.CompanyAccountNo;
                    ObjEmergeReport.OrderCompanyName = ObjModel.OrderCompanyName;
                    ObjEmergeReport.PermissiblePurpose = ObjModel.PermissiblePurposeValue;
                    // string sCurrentUserXMLFile = DateTime.Now.ToFileTime().ToString() + ".xml";
                    string sCurrentUserXMLFile = ObjEmergeReport.ObjtblOrder.OrderNo.ToString() + ".xml";
                    Session["NewReportXMLPath"] = sCurrentUserXMLFile;
                    Session["EmergeReport"] = ObjEmergeReport;

                    Session["newapi"] = "fromapi";

                    //XmlSerializer serializer = new XmlSerializer(typeof(EmergeReport));

                    //using (TextWriter textWriter = new StreamWriter(Server.MapPath(@"~\Resources\Upload\NewReportXML\" + sCurrentUserXMLFile)))
                    //{
                    //    serializer.Serialize(textWriter, ObjEmergeReport);
                    //}
        #endregion
                }
                //serializer = null;
                TempData["IsErrorLogRan"] = "true";
                return RedirectToAction("NewReportLogApi", "Corporate", new { OrderId = ObjModel.OrderId });

            }
            return View();

        }
        private static List<string> ProductsLogSelectedList(string ProductsSelected)
        {
            string ProductSelected = ProductsSelected.Substring(0, (ProductsSelected.Length - 1));
            string[] stringSeparator = new string[] { "##" };
            List<string> ObjCollProducts = ProductSelected.Substring(1, (ProductSelected.Length - 1)).Split(stringSeparator, StringSplitOptions.RemoveEmptyEntries).Distinct().ToList<string>();
            List<string> NewCollProducts = new List<string>();
            for (int i = 0; i < ObjCollProducts.Count; i++)
            {
                string[] arr = ObjCollProducts.ElementAt(i).Split('_');
                NewCollProducts.Add(arr[0] + "_" + arr[1] + "_" + arr[2]);
            }
            return NewCollProducts.Distinct().ToList();
        }

        private static List<string> ProductsSelectedList(string ProductsSelected)
        {
            string ProductSelected = ProductsSelected.Substring(0, (ProductsSelected.Length - 1));
            string[] stringSeparator = new string[] { "##" };
            List<string> ObjCollProducts = ProductSelected.Substring(1, (ProductSelected.Length - 1)).Split(stringSeparator, StringSplitOptions.RemoveEmptyEntries).Distinct().ToList<string>();
            var psProduct = ObjCollProducts.FirstOrDefault(n => n.StartsWith("PS_"));
            if (psProduct != null)
            {
                ObjCollProducts.Remove(psProduct);
                ObjCollProducts.Insert(0, psProduct);
            }
            return ObjCollProducts;
        }

        public ActionResult ForgetPassword(string Email)
        {
            string Message = string.Empty;
            string Css = string.Empty;
            string username = "";
            string password = "";
            MembershipUser objMembershipUser = Membership.GetUser(Email.Trim());
            if (objMembershipUser != null)
            {
                password = objMembershipUser.GetPassword();
                username = objMembershipUser.UserName;
                if (ForgetPassword_Email(username, password))
                {
                    Message = "Your login details have been sent to your email address.";
                    Css = "successmsg";
                }
                else
                {
                    Message = "Some error ocurred while sending an email. Please try again later.";
                    Css = "errormsgSMALL";
                }
            }
            else
            {
                Message = "Invalid User Name";
                Css = "errormsgSMALL";
            }
            return Json(new { Msg = Message, css = Css });
        }
        public bool ForgetPassword_Email(string user_Name, string password)
        {
            bool mailsent = false;
            BALGeneral objBALGeneral = new BALGeneral();

            try
            {
                string emailText = "";
                string MessageBody = "";

                BALGeneral ObjBALGeneral = new BALGeneral();
                string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;

                var emailContent = objBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, Convert.ToInt32(EmailTemplates.PasswordRecovery));//"Password Recovery"
                emailText = emailContent.TemplateContent;
                BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                #region New way Bookmark


                Bookmarks objBookmark = new Bookmarks();

                objBookmark.EmailContent = emailText;

                objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                objBookmark.CompanyName = ObjBALGeneral.GetSettings(1).CompanyName;
                objBookmark.UserEmail = user_Name;
                objBookmark.UserPassword = password;
                objBookmark.LoginLink = "<a style='color: #87ceea;' href='" + ApplicationPath.GetApplicationPath() + "'>Login Now !</a>";

                MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Utility.GetUID(HttpContext.User.Identity.Name.ToString()));

                #endregion
                return Utility.SendMail(user_Name, string.Empty, string.Empty, BALGeneral.GetGeneralSettings().SupportEmailId, MessageBody, "Forget User Name and Password");
            }
            catch// (Exception ex)
            {
                return mailsent;
                //throw ex;
            }
            finally
            {
                objBALGeneral = null;
            }

        }
        #region Find Controls and XML

        private List<string> FindControlsinSelectedReports(List<string> ObjCollection)
        {
            List<string> ObjXmlData = new List<string>();

            for (int iRow = 0; iRow < ObjCollection.Count; iRow++)
            {
                List<string> ObjXmlControls = GetXMLData(ObjCollection[iRow].ToString());
                if (ObjXmlControls.Count > 0)
                {
                    ObjXmlData.Add(ObjCollection[iRow].ToString());
                }
            }
            return ObjXmlData;
        }

        private List<string> GetXMLData(string ProductCode)
        {
            List<string> ObjCollection = new List<string>();
            if (ProductCode.Contains("_"))
            {
                string[] Pcode = ProductCode.Split('_');
                ProductCode = Pcode[0].ToString();
            }
            XmlDocument ObjXmlDocument = new XmlDocument();
            ObjXmlDocument.Load(Server.MapPath(XMLFilePath));

            XmlNode ObjXmlNode = ObjXmlDocument.SelectSingleNode("/Products/Product[@Code='" + ProductCode + "']");

            if (ObjXmlNode != null)
            {
                for (int iRow = 0; iRow < ObjXmlNode.ChildNodes.Count; iRow++)
                {
                    ObjCollection.Add(ObjXmlNode.ChildNodes[iRow].InnerText);
                }
            }
            return ObjCollection;
        }

        #endregion

        #region Bind County

        public ActionResult BindCounty(int StateId)
        {
            BALGeneral ObjBALGeneral = new BALGeneral();
            List<clsCounty> ObjCollection = ObjBALGeneral.GetStatesCountiesWithConsent(StateId); //.GetStatesCountiesNR(StateId);
            //ObjCollection.Insert(0, new clsCounty { pkCountyId = "-1", CountyName = "" });            
            return Json(new { CountryList = ObjCollection }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Bind State

        public ActionResult BindState()
        {
            BALGeneral ObjGeneral = new BALGeneral();
            List<tblState> ObjStateCollection = ObjGeneral.GetStates();
            ObjStateCollection = (from db in ObjStateCollection select new tblState { pkStateId = db.pkStateId, StateName = db.StateName }).ToList();
            return Json(ObjStateCollection);

        }

        #endregion

        #region Get Reference Note

        public ActionResult GetReferenceNote(int ReferenceCodeId)
        {
            BALCompany ObjBALCompany = new BALCompany();
            string Notes = ObjBALCompany.GetReferenceNote(ReferenceCodeId);
            return Json(Notes);
        }

        #endregion

        #region  IsConsent

        public ActionResult IsConsentActivated(string ProductsSelected, string JSelValue, string JSelText, string DLvalue, string DLtext, string SValue,
                                                string SText, string Cvalue, string Ctext, string C1value, string C1text, string C2value, string C2text)
        {
            List<Consent> ObjConsent = new List<Consent>();
            if (ProductsSelected != "")
            {
                List<string> ProductList = ProductsSelectedList(ProductsSelected);
                string SelectedText = string.Empty;
                // string PkJurisdictionId = SelectedValue.ToString().Split('_').FirstOrDefault();

                for (int i = 0; i < ProductList.Count; i++)
                {
                    string UpdProductList = ProductList.ElementAt(i).Split('_').FirstOrDefault();
                    string IsConsentActivated = "false";
                    if (UpdProductList.Contains("SCR") || UpdProductList.Contains("MVR") || UpdProductList.Contains("WCV") || UpdProductList.Contains("CCR1") || UpdProductList.Contains("CCR2") || UpdProductList.Contains("RCX") || UpdProductList.Contains("FCR"))
                    {
                        if (UpdProductList.Contains("FCR"))
                        {
                            #region Check from Juridiction Dropdown values

                            if (JSelValue != "-1")
                            {
                                IsConsentActivated = JSelValue.Split('_').ElementAt(1);
                                SelectedText = JSelText;
                                if (IsConsentActivated == "True")
                                {
                                    ObjConsent.Add(new Consent { ReportType = UpdProductList, StateCounty = SelectedText });
                                }
                            }

                            #endregion

                        }
                        else if (UpdProductList.Contains("MVR"))
                        {
                            #region Check from Automotive Information Dropdown values

                            if (DLvalue != "-1")
                            {
                                IsConsentActivated = DLvalue.Split('_').ElementAt(1);
                                SelectedText = DLtext;
                                if (IsConsentActivated == "True")
                                {
                                    ObjConsent.Add(new Consent { ReportType = UpdProductList, StateCounty = SelectedText });
                                }
                            }

                            #endregion
                        }
                        else if (UpdProductList.Contains("SCR") || UpdProductList.Contains("WCV"))
                        {
                            #region  Check from State Dropdown values

                            int Iindex = 2;
                            if (UpdProductList.Contains("WCV"))
                            {
                                Iindex = 3;
                            }

                            IsConsentActivated = SValue != "-1" ? SValue.Split('_').ElementAt(Iindex) : "";
                            SelectedText = SText;
                            if (IsConsentActivated == "1")
                            {
                                ObjConsent.Add(new Consent { ReportType = UpdProductList, StateCounty = SelectedText });
                            }

                            #endregion

                        }
                        else if (UpdProductList.Contains("CCR1") || UpdProductList.Contains("CCR2") || UpdProductList.Contains("RCX"))
                        {
                            #region  Check from County Dropdown values

                            //int index = 2;//commented by chetu
                            int index = 0;//coded by chetu
                            if (UpdProductList.Contains("CCR2"))
                            {
                                index = 3;
                            }
                            else if (UpdProductList.Contains("RCX"))
                            {
                                index = 4;
                            }
                            if (Cvalue != "-1")//First County Dropdown
                            {
                                IsConsentActivated = Cvalue.Split('_').ElementAt(index);
                                SelectedText = Ctext;
                                if (IsConsentActivated == "1")
                                {
                                    ObjConsent.Add(new Consent { ReportType = UpdProductList, StateCounty = SelectedText });
                                }
                            }
                            if (C1value != "")//Second County Dropdown
                            {
                                IsConsentActivated = C1value.Split('_').ElementAt(index);
                                SelectedText = C1text;
                                if (IsConsentActivated == "1")
                                {
                                    ObjConsent.Add(new Consent { ReportType = UpdProductList, StateCounty = SelectedText });
                                }
                            }
                            if (C2value != "")//Third County Dropdown
                            {
                                IsConsentActivated = C2value.Split('_').ElementAt(index);
                                SelectedText = C2text;
                                if (IsConsentActivated == "1")
                                {
                                    ObjConsent.Add(new Consent { ReportType = UpdProductList, StateCounty = SelectedText });
                                }
                            }

                            #endregion
                        }

                    }
                }

            }
            return Json(ObjConsent);
        }
        #endregion

        #region Suggestions Email

        public ActionResult SendSuggestionEmail(string Suggestions, string From, string SuggestionSubject)
        {
            string emailText = "";
            string MessageBody = "";
            string CurrentUserCompanyName = string.Empty;
            string Message = string.Empty;

            try
            {
                BALGeneral ObjBALGeneral = new BALGeneral();
                var settings = ObjBALGeneral.GetSettings(1);
                string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;
                var emailContent = ObjBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, Convert.ToInt32(EmailTemplates.EmergeEdit));// "Emerge Suggestions"
                emailText = emailContent.TemplateContent;

                #region New way Bookmark

                BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();

                #region For New Style SystemTemplate test
                string Bookmarkfile = string.Empty;
                string bookmarkfilepath = string.Empty;

                Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                emailText = Bookmarkfile;
                #endregion
                Bookmarks objBookmark = new Bookmarks();
                if (Session["CurrentUserCompanyName"] != null)
                {
                    CurrentUserCompanyName = Session["CurrentUserCompanyName"].ToString();
                }
                objBookmark.EmailContent = emailText;

                objBookmark.Subject = SuggestionSubject;
                objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                objBookmark.UserCompany = CurrentUserCompanyName;
                objBookmark.UserEmail = Session["UserFullName"].ToString() + " (<b>UserName : </b>" + User.Identity.Name + ") ";
                objBookmark.Suggestion = Suggestions;

                MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Utility.GetUID(User.Identity.Name.ToString()));

                #endregion

                //bool success = Utility.SendMail(ConfigurationManager.AppSettings["EMEditMailTo"].ToString(), From.Trim(), string.Empty, From.Trim(), MessageBody, SuggestionSubject);
                bool success = Utility.SendMail(settings.SupportEmailId, From.Trim(), string.Empty, settings.SupportEmailId, MessageBody, SuggestionSubject);
                if (success == true)
                {
                    Message = "<span class='SuccessMsg'>Your suggestion is submitted successfully.</span>";
                }
                else
                {
                    Message = "<span class='ErrorMsg'>Some error occured , please try again.</span>";
                }
            }
            catch (Exception)
            {
                Message = "<span class='ErrorMsg'>Error occured , please try again.</span>";
            }
            return Json(Message);
        }

        #endregion

        #region Load Searching Content For Searching page

        public ActionResult LoadSearchingContent()
        {
            Guid SiteAppId = Utility.SiteApplicationId;
            BALContentManagement ObjBALContentManagement = new BALContentManagement();
            List<tblContentManagement> ObjList = new List<tblContentManagement>();
            ObjList = ObjBALContentManagement.GetRandomContentsAll(SiteAppId);
            //var rnd = new Random();
            // var result = ObjList.OrderBy(item => rnd.Next());
            StringBuilder st = new StringBuilder();
            StringBuilder stTitle = new StringBuilder();
            if (ObjList.Count() > 0)
            {
                for (int irow = 0; irow < ObjList.Count(); irow++)
                {
                    if (irow == ObjList.Count() - 1)
                    {
                        st.Append(ObjList.ElementAt(irow).Comment);
                        stTitle.Append(ObjList.ElementAt(irow).Title);

                    }
                    else
                    {
                        st.Append(ObjList.ElementAt(irow).Comment + "## ");
                        stTitle.Append(ObjList.ElementAt(irow).Title + "## ");
                    }
                }
            }

            return Json(new { Title = stTitle.ToString(), Desription = st.ToString() });
            // Response.End();
        }

        #endregion



        [HttpPost]
        [ValidateInput(false)]
        /// <summary>
        /// This method is used to send product ordered email to support and client
        /// </summary>
        /// 
        public ActionResult SendMailForProductOrdered(string productcode, string productname, string productAppid, string qty, string unitprice, string pkCompanyUserId, string pkLocationId, string shipToAddress)
        {
            string CssClass = "";
            string Message = "";
            int orderid = 0;
            string sendShipAddress = string.Empty;
            try
            {
                orderid = SaveDrugTestOrder(productcode, productname, productAppid, qty, unitprice, pkCompanyUserId, pkLocationId);
                if (orderid != 0)
                {
                    BALGeneral ObjBALGeneral = new BALGeneral();
                    ShipToAddress objShipToAddress = new ShipToAddress();
                    Dictionary<string, string> ObjDictDrugTestOrderInfo = (Dictionary<string, string>)Session["vwDrugTestOrderInfo"];
                    string[] shipToAddressList = shipToAddress.Split('_');
                    if (shipToAddressList.Count() > 0)
                    {
                        objShipToAddress.LocationId = Convert.ToInt32(pkLocationId);
                        objShipToAddress.CompanyUserId = Convert.ToInt32(pkCompanyUserId);
                        objShipToAddress.CompanyName = shipToAddressList[0];
                        objShipToAddress.Address1 = shipToAddressList[1];
                        objShipToAddress.Address2 = shipToAddressList[2];
                        objShipToAddress.StateCode = shipToAddressList[3];
                        objShipToAddress.City = shipToAddressList[4];
                        objShipToAddress.ZipCode = shipToAddressList[5];
                        objShipToAddress.fkOrderId = orderid;
                        //To save the custom shipping address.
                        ObjBALGeneral.SaveShipToAddressInDB(objShipToAddress);
                        if (!string.IsNullOrEmpty(shipToAddressList[2]))
                        {
                            sendShipAddress = shipToAddressList[0] + "<br/>" + shipToAddressList[1] + "" + shipToAddressList[2] + "<br/>" + shipToAddressList[4] + "," + shipToAddressList[3] + "-" + shipToAddressList[5];
                        }
                        else
                        {
                            sendShipAddress = shipToAddressList[0] + "<br/>" + shipToAddressList[1] + "<br/>" + shipToAddressList[4] + "," + shipToAddressList[3] + "-" + shipToAddressList[5];
                        }
                        ObjDictDrugTestOrderInfo.Add("key3", sendShipAddress);//For Ship to address;
                    }
                    var emailContent = ObjBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, Convert.ToInt32(EmailTemplates.ProductOrdered));
                    if (emailContent != null)
                    {
                        string emailText = string.Empty;
                        string MessageBody = string.Empty;
                        string MailSubject = "Product Ordered";
                        // string WebsiteLogo = "https://www.intelifi.com/emerge/Resources/Upload/Images/emerge-logo--no-shadows-(small).gif";
                        string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/emerge-logo--no-shadows-(small).gif";
                        string EmergeAdminAddress = string.Empty;
                        if (ConfigurationManager.AppSettings["EmergeAdmin"] != null)
                        {
                            EmergeAdminAddress = ConfigurationManager.AppSettings["EmergeAdmin"].ToString();// BALGeneral.GetGeneralSettings().SupportEmailId;

                        }
                        string MailToAddress = BALGeneral.GetGeneralSettings().SupportEmailId;
                        string ClientMailAddress = (Session["CurrentUserMailId"] == null) ? string.Empty : Session["CurrentUserMailId"].ToString();
                        emailText = emailContent.TemplateContent;
                        #region New way Bookmark
                        BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                        #region For New Style SystemTemplate
                        string Bookmarkfile = string.Empty;
                        string bookmarkfilepath = string.Empty;

                        Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                        Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                        Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                        emailText = Bookmarkfile;
                        #endregion
                        Bookmarks objBookmark = new Bookmarks();
                        objBookmark.EmailContent = emailText;
                        objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                        objBookmark.OrderNumber = ObjDictDrugTestOrderInfo["key1"];
                        objBookmark.OrderDescription = ObjDictDrugTestOrderInfo["key2"];
                        objBookmark.LoginLink = "<a style='color: #87ceea;' href='" + ApplicationPath.GetApplicationPath() + "'>Login Now !</a>";
                        MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Utility.GetUID(!string.IsNullOrEmpty(System.Web.HttpContext.Current.User.Identity.Name.ToString()) ? System.Web.HttpContext.Current.User.Identity.Name.ToString() : "admin@intelifi.com"));
                        MessageBody = MessageBody.Replace("%ShipToAddress%", ObjDictDrugTestOrderInfo["key3"]);
                        #endregion
                        bool success = Utility.SendMail(EmergeAdminAddress,
                                                        ClientMailAddress,
                                                        string.Empty,
                                                        MailToAddress,
                                                        MessageBody,
                                                        MailSubject);
                        if (success == true)
                        {
                            //CssClass = "successmsg";
                            Message = "<span class='successmsg'>Your order has been placed successfully.</span>";
                        }
                        else
                        {
                            //CssClass = "errormsg";
                            Message = "<span class='errormsg'>Some problem occurred while sending product ordered mail.</span>";
                        }
                    }
                }
                else
                {
                    //CssClass = "errormsg";
                    Message = "<span class='errormsg'>Some problem occured while ordering this item.</span>";
                }
                return Json(new { Msg = Message, Css = CssClass });
            }
            catch
            {
                return Json(new { Msg = Message, Css = CssClass });
            }
        }


        /// <summary>
        /// Call action for getting ship to address for ticket #17
        /// </summary>
        /// <param name="pkLocationId"></param>
        /// <param name="pkCompanyUserId"></param>
        /// <returns></returns>
        public ActionResult GetShipToAddress(string pkLocationId, string pkCompanyUserId)
        {
            ShipToAddress objShipToAddress = new ShipToAddress();
            try
            {
                int locationid = Convert.ToInt32(pkLocationId);
                int UserId = Convert.ToInt32(pkCompanyUserId);
                objShipToAddress = new BALGeneral().GetShipToAddressDetails(locationid, UserId);
            }
            catch
            {

            }
            return Json(new { ShipAddress = objShipToAddress });
        }






        /// <summary>
        /// This method is used to save drug test order
        /// </summary>
        /// <param name="GV"></param>
        /// <returns></returns>
        public int SaveDrugTestOrder(string productcode, string productname, string productAppid, string qty, string unitprice, string pkCompanyUserId, string pkLocationId)
        {
            int OrderId = 0;
            // int LocationId = Convert.ToInt32(pkLocationId);
            int CompanyUserId = Convert.ToInt32(pkCompanyUserId);
            BALOrders ObjBALOrders = new BALOrders();
            Dictionary<string, string> ObjDictDrugTestOrderInfo = new Dictionary<string, string>();

            try
            {

                string ProductCode = productcode;
                string ProductName = productname;
                int ProductAppId = Convert.ToInt32(productAppid);
                short ProductQty = short.Parse(qty);
                string ProductPanel = "0"; /*(ddlPanels.Items.Count == 0) ? "0" : ddlPanels.SelectedValue;*/

                List<string> ObjCollProducts = new List<string>();

                ObjCollProducts.Add(ProductCode);

                DateTime Dated = DateTime.Now;

                #region Insertion in main order table

                tblOrder ObjtblOrder = new tblOrder();
                //ObjtblOrder.fkCompanyUserId = (Session["CurrentUserId"] == null) ? Guid.Empty : new Guid(Session["CurrentUserId"].ToString());
                //ObjtblOrder.fkLocationId = (Session["pkLocationId"] == null) ? Guid.Empty : new Guid(Session["pkLocationId"].ToString());
                ObjtblOrder.fkCompanyUserId = Convert.ToInt32(pkCompanyUserId);
                ObjtblOrder.fkLocationId = Convert.ToInt32(pkLocationId);

                string OrderNo = Dated.ToString("yyMMddHHmmssfff");
                ObjtblOrder.OrderNo = OrderNo;

                ObjDictDrugTestOrderInfo.Add("key1", OrderNo); /* Maintain for order details */

                ObjtblOrder.OrderDt = Dated;
                ObjtblOrder.OrderStatus = (byte)OrderStatus.Pending;/* (OrderStatus.Completed) This was changed from 21 Sept 2011 according to client demand on Ticket 631*/
                ObjtblOrder.ReportIncludes = ObjCollProducts[0];
                ObjtblOrder.CreatedDate = Dated;
                ObjtblOrder.CreatedById = CompanyUserId;
                ObjtblOrder.IsDrugTestingOrder = true;
                ObjtblOrder.SearchPersonHTML = Request.ServerVariables["HTTP_USER_AGENT"] + " OS- " + Environment.OSVersion.Version.ToString() + " Report Run From New Report(Drug Testing) Page."; ;

                #endregion

                #region Insertion in order searched table

                tblOrderSearchedData ObjtblOrderSearchedData = new tblOrderSearchedData();

                ObjtblOrderSearchedData.SearchedLastName = string.Empty;
                ObjtblOrderSearchedData.SearchedFirstName = ProductName + " X " + ProductQty.ToString();

                ObjDictDrugTestOrderInfo.Add("key2", ObjtblOrderSearchedData.SearchedFirstName); /* Maintain for order details */

                ObjtblOrderSearchedData.SearchedMiddleInitial = string.Empty;
                ObjtblOrderSearchedData.SearchedSuffix = string.Empty;
                ObjtblOrderSearchedData.SearchedSsn = string.Empty;
                ObjtblOrderSearchedData.SearchedDob = string.Empty;
                ObjtblOrderSearchedData.SearchedPhoneNumber = string.Empty;
                ObjtblOrderSearchedData.SearchedApplicantEmail = string.Empty;
                ObjtblOrderSearchedData.SearchedStreetAddress = string.Empty;
                ObjtblOrderSearchedData.SearchedDirection = string.Empty;
                ObjtblOrderSearchedData.SearchedStreetName = string.Empty;
                ObjtblOrderSearchedData.SearchedStreetType = string.Empty;
                ObjtblOrderSearchedData.SearchedApt = string.Empty;
                ObjtblOrderSearchedData.SearchedCity = string.Empty;
                ObjtblOrderSearchedData.SearchedStateId = -1;
                ObjtblOrderSearchedData.SearchedZipCode = string.Empty;
                ObjtblOrderSearchedData.best_SearchedZipCode = string.Empty;
                ObjtblOrderSearchedData.SearchedCountyId = 0;
                ObjtblOrderSearchedData.SearchedDriverLicense = string.Empty;
                ObjtblOrderSearchedData.SearchedVehicleVin = string.Empty;
                ObjtblOrderSearchedData.SearchedDLStateId = -1;
                ObjtblOrderSearchedData.SearchedSex = string.Empty;
                ObjtblOrderSearchedData.SearchedBusinessName = string.Empty;
                ObjtblOrderSearchedData.SearchedBusinessCity = string.Empty;
                ObjtblOrderSearchedData.SearchedBusinessStateId = -1;
                ObjtblOrderSearchedData.SearchedTrackingRef = string.Empty;
                ObjtblOrderSearchedData.SearchedTrackingRefId = 0;
                ObjtblOrderSearchedData.SearchedTrackingNotes = string.Empty;

                #endregion

                #region Insertion in Order Details

                List<tblOrderDetail> ObjCollOrderDetail = new List<tblOrderDetail>();
                tblOrderDetail ObjtblOrderDetail = new tblOrderDetail();

                ObjtblOrderDetail.fkProductApplicationId = ProductAppId;
                ObjtblOrderDetail.ProductQty = ProductQty;
                ObjtblOrderDetail.ProductPanel = ProductPanel;

                decimal ReportAmount;
                decimal.TryParse(unitprice, out ReportAmount);
                ObjtblOrderDetail.ReportAmount = ReportAmount;

                ObjCollOrderDetail.Add(ObjtblOrderDetail);

                #endregion

                #region Calling Business Layer Method for insertion in db

                OrderId = (int)ObjBALOrders.AddDrugTestOrders(ObjtblOrder,
                                                 ObjCollOrderDetail,
                                                 ObjtblOrderSearchedData,
                                                 Utility.SiteApplicationId);

                #endregion
            }
            catch { }
            finally
            {
                ObjBALOrders = null;
                Session["vwDrugTestOrderInfo"] = ObjDictDrugTestOrderInfo;
            }
            return OrderId;
        }

        public JsonResult GetAllReports(int fkLocationId, int PackageId)
        {
            string ReportName = string.Empty;
            try
            {
                BALGeneral ObjGeneral = new BALGeneral();
                ReportName = ObjGeneral.GetEmergeProductPackageReportsNew(fkLocationId, PackageId);
            }
            catch (Exception)
            {
            }
            return Json(ReportName, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetHostReport(string TieredPackageid)
        {
            string ReportName = string.Empty;
            try
            {
                using (EmergeDALDataContext dx = new EmergeDALDataContext())
                {
                    tblTieredPackage ObjtblTieredPackage = new tblTieredPackage();
                    ObjtblTieredPackage = dx.tblTieredPackages.Where(d => d.fkPackageId == Convert.ToInt32(TieredPackageid)).FirstOrDefault();
                    if (ObjtblTieredPackage != null)
                    {
                        List<tblProductPerApplication> ObjtblPPA = new List<tblProductPerApplication>();
                        tblProductPerApplication ObjtblPPAs = new tblProductPerApplication();
                        ObjtblPPA = dx.tblProductPerApplications.ToList<tblProductPerApplication>();
                        ObjtblPPAs = ObjtblPPA.Where(d => d.pkProductApplicationId == ObjtblTieredPackage.Host).FirstOrDefault();
                        if (ObjtblPPAs != null)
                        {
                            ReportName = ObjtblPPAs.ProductCode;
                        }
                        ObjtblPPAs = null;
                        ObjtblPPA = null;
                    }
                }
            }
            catch (Exception)
            {
            }
            return Json(new { Data = ReportName });
        }

        public bool IsValidEmail(string email)
        {
            try
            {
                // var addr = new System.Net.Mail.MailAddress(email);
                return true;
            }
            catch
            {
                return false;
            }
        }



        public bool isValidDate(string datePart)
        {
            bool result = true;
            try
            {
                // DateTime dateTime = DateTime.Now;
                var Datefilled = datePart.Split('/');

                if (Datefilled.Count() > 2)
                {
                    //var CurrntDate = DateTime.Now.ToString();
                    var TodayDate = int.Parse(DateTime.Now.Month.ToString()) + 1 + "/" + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Year.ToString();
                    string[] newDatefilled = new string[3];
                    newDatefilled[0] = Datefilled[2];
                    newDatefilled[1] = Convert.ToString(int.Parse(Datefilled[1]) - 1);
                    newDatefilled[2] = Datefilled[0];
                    var temp = newDatefilled[2] + "/" + newDatefilled[1] + "/" + newDatefilled[0];

                    var dtDatefilled = Convert.ToDateTime(temp);
                    var dtToday = DateTime.Parse(TodayDate);

                    if (dtDatefilled > dtToday || int.Parse(Datefilled[2]) > int.Parse(DateTime.Now.Year.ToString()) - 18 || int.Parse(Datefilled[0]) > 12 || int.Parse(Datefilled[1]) > 31)
                    {

                        result = false;
                    }

                    else
                    {

                        result = true;

                    }

                }
            }
            catch
            {
                result = false;
            }

            return result;

        }


        // int 194 changes 

        public ActionResult SubmitReportForApi(NewReportModel ObjModel, FormCollection frm, string[] CompanyName, string[] AliasName, string[] SchoolName, string[] InstituteAlias, string[] ReferenceName, string[] ReferenceAlias, string[] LicenseType, string[] LicenseAlias)
        {
            if (ModelState.IsValid)
            {

                int LocationId = ObjModel.fkLocationId;
                int CompanyUserId = ObjModel.pkCompanyUserId;
                int fkCompanyId = ObjModel.pkCompanyId;

                EmergeReport ObjEmergeReport = new EmergeReport();
                OrderInfo ObjOrderInfo = new OrderInfo();
                tblTieredPackage ObjtblTieredPackage = new tblTieredPackage();
                // Dictionary<string, Guid> ObjCompnyProductInfo = new Dictionary<string, Guid>();
                //int TieredPackageids = 0;//INT-194
                int TieredPackageid = 0;
                //check 7 year filter is auto selected
                if (ObjModel.Is7YearAuto)
                {
                    ObjOrderInfo.Is7YearFilterApplyForCriminal = ObjModel.Is7YearAuto;
                }
                else
                {
                    ObjOrderInfo.Is7YearFilterApplyForCriminal = ObjModel.Is7YearFilterApplyForAllCriminalResult;
                }
                //string eror = string.Empty;
                //var NewProductCode = ObjModel.ProductsSelected.Split('_');
                //var VNewProductCode = NewProductCode[0].ToString().Split('#');

                XmlDocument ObjXmlDocument = new XmlDocument();
                ObjXmlDocument.Load(Server.MapPath("~/Content/ProductXml/ProductRequireData.xml"));
                //XmlNode ObjXmlNode = ObjXmlDocument.SelectSingleNode("/Products/Product[@Code='" + VNewProductCode[1] + "']");
                //XmlNodeList xnList = ObjXmlDocument.SelectNodes("/Products/Product[@Code='" + VNewProductCode[1] + "']/RequiredData");
                //edit for INT-194
                #region Product Selected
                int CheckTieredPackageiD = 0;
                List<string> ObjCollProducts = ProductsSelectedList(ObjModel.ProductsSelected);
                ObjCollProducts = FindControlsinSelectedReports(ObjCollProducts);
                for (int i = 0; i < ObjCollProducts.Count; i++)
                {
                    string[] ProductFullName = ObjCollProducts.ElementAt(i).Split('_');
                    int.TryParse(ProductFullName[1], out TieredPackageid);
                    using (EmergeDALDataContext dx = new EmergeDALDataContext())
                    {
                        if (Convert.ToInt32(ProductFullName[1]) != 0) //edit for INT-194
                        {
                            ObjtblTieredPackage = dx.tblTieredPackages.Where(d => d.fkPackageId == Convert.ToInt32(ProductFullName[1])).FirstOrDefault();
                            if (ObjtblTieredPackage != null)
                            {

                                TieredPackageid = Convert.ToInt32(ProductFullName[1]);
                                CheckTieredPackageiD = TieredPackageid;
                            }
                        }
                    }
                    if (ProductFullName[0] == "NCRPLUS")
                    {
                        ObjCollProducts.RemoveAt(i);
                        ObjCollProducts.Add("NCR+_" + ProductFullName[1]);
                    }
                    if (ProductFullName[0] == "NCR2")//Ticket #740: Incomplete report on NCR2 and error log is not shwing #17
                    {
                        if (string.IsNullOrEmpty(ObjModel.Social) == true) //# 408 Ticket
                        {
                            ObjModel.Social = "999-99-9999";

                        }
                    }
                }

                #endregion

                #region Tiered Section
                using (EmergeDALDataContext dx = new EmergeDALDataContext())
                {
                    if (CheckTieredPackageiD != 0) //edit for INT-194
                    {
                        TieredPackageid = CheckTieredPackageiD;// ADD FOR TAKE NON ZERO PACKAGE ID 

                        ObjtblTieredPackage = dx.tblTieredPackages.Where(d => d.fkPackageId == TieredPackageid).FirstOrDefault();
                        if (ObjtblTieredPackage != null)
                        {
                            ObjOrderInfo.IsTiered = true;
                            ObjOrderInfo.IsAutomatic = ObjtblTieredPackage.IsAutomaticRun;
                            if (ObjtblTieredPackage.Variable == "County State")
                            {
                                ObjOrderInfo.Variable = "1";
                            }
                            else if (ObjtblTieredPackage.Variable == "State")
                            {
                                ObjOrderInfo.Variable = "2";
                            }
                        }
                    }
                }
                #endregion
                #region Insertion in main order table
                DateTime Dated = DateTime.Now;
                tblOrder ObjtblOrder = new tblOrder();
                ObjtblOrder.fkLocationId = LocationId;
                ObjtblOrder.fkCompanyUserId = CompanyUserId;
                string OrderNo = Dated.ToString("yyMMddHHmmssfff");
                ObjtblOrder.OrderNo = OrderNo;
                ObjtblOrder.OrderDt = Dated;
                ObjtblOrder.OrderStatus = (byte)OrderStatus.Pending;
                ObjtblOrder.CreatedDate = Dated;
                ObjtblOrder.CreatedById = CompanyUserId;
                ObjtblOrder.SyncpodImage = ObjModel.hdnSyncpodImage;
                ObjtblOrder.FaceImage = ObjModel.hdnFaceImage;
                ObjtblOrder.OrderType = ObjModel.OrderType;
                ObjtblOrder.ReferenceCode = string.Empty;
                ObjtblOrder.SearchPersonHTML = Request.ServerVariables["HTTP_USER_AGENT"] + " OS- " + Environment.OSVersion.Version.ToString() + " Report Run From New Report Page.";
                if (ObjModel.hdnIsSyncpod == "true")
                {
                    ObjtblOrder.IsSyncpod = true;
                }
                else
                {
                    ObjtblOrder.IsSyncpod = false;
                }

                #endregion

                #region Insert new ReferenceCode for company

                int ReferenceCodeId = 0;
                string ReferenceCode = string.Empty;
                if (Convert.ToInt32(ObjModel.TrackingRefCode) > 0)
                {
                    ReferenceCodeId = Convert.ToInt32(ObjModel.TrackingRefCode);
                    ReferenceCode = ObjModel.TrackingRefCodeText;
                }
                else if (ObjModel.TrackingRefCode == -3 && ObjModel.TrackingRefCodeValue != "")
                {
                    if (ObjModel.TrackingRefCodeValue != null)
                    {
                        ReferenceCode = ObjModel.TrackingRefCodeValue.Trim();
                    }
                    else
                    {
                        ReferenceCode = ObjModel.TrackingNotes;
                    }
                    tblReferenceCode ObjtblReferenceCode = new tblReferenceCode();
                    BALReferenceCode ObjBALReferenceCode = new BALReferenceCode();
                    // DateTime CreatedDate = DateTime.Now;

                    ObjtblReferenceCode.fkCompanyId = fkCompanyId;
                    ObjtblReferenceCode.ReferenceCode = ReferenceCode;
                    ObjtblReferenceCode.ReferenceNote = ObjModel.TrackingNotes;
                    int result = ObjBALReferenceCode.AddReference(ObjtblReferenceCode, fkCompanyId, true);
                    if (result > 0)
                    {
                        ReferenceCodeId = result;
                    }
                }

                #endregion

                #region Insertion in order searched table
                tblOrderSearchedData ObjtblOrderSearchedData = new tblOrderSearchedData();
                ObjtblOrderSearchedData.SearchedLastName = (ObjModel.LastName != null ? ObjModel.LastName.Trim() : string.Empty);
                ObjtblOrderSearchedData.SearchedFirstName = (ObjModel.FirstName != null ? ObjModel.FirstName.Trim() : string.Empty);
                ObjtblOrderSearchedData.SearchedMiddleInitial = ObjModel.MI ?? string.Empty;
                ObjtblOrderSearchedData.SearchedSuffix = (ObjModel.SuffixValue != "-1") ? ObjModel.SuffixValue : string.Empty;
                ObjtblOrderSearchedData.SearchedSsn = ObjModel.Social ?? string.Empty;
                ObjtblOrderSearchedData.SearchedDob = ObjModel.DOB ?? string.Empty;
                ObjtblOrderSearchedData.SearchedPhoneNumber = ObjModel.Phone ?? string.Empty;
                ObjtblOrderSearchedData.SearchedApplicantEmail = ObjModel.ApplicantEmail ?? string.Empty;
                ObjtblOrderSearchedData.SearchedStreetAddress = ObjModel.Address ?? string.Empty;
                ObjtblOrderSearchedData.SearchedDirection = (ObjModel.DirectionValue != "-1") ? ObjModel.DirectionValue : string.Empty;
                ObjtblOrderSearchedData.SearchedStreetName = ObjModel.Street ?? string.Empty;
                ObjtblOrderSearchedData.SearchedStreetType = (ObjModel.TypeValue != "-1" && ObjModel.TypeValue != null) ? ObjModel.TypeValue : string.Empty;
                ObjtblOrderSearchedData.SearchedApt = ObjModel.Apt ?? string.Empty;
                ObjtblOrderSearchedData.SearchedCity = ObjModel.City ?? string.Empty;
                ObjtblOrderSearchedData.best_SearchedStreetAddress = ObjModel.Address ?? string.Empty;
                ObjtblOrderSearchedData.best_SearchedStreetName = ObjModel.Street ?? string.Empty;
                ObjtblOrderSearchedData.best_SearchedCity = ObjModel.City ?? string.Empty;
                bool IsLiveRunnerState = false;

                if (ObjModel.State != "-1")
                {
                    string[] SelectedStateValues = ObjModel.State.ToString().Split('_');
                    string SelectedState = SelectedStateValues[0].Trim();
                    if (SelectedStateValues[1].Trim().ToLower() == "true" && ObjModel.hdnIsSCRLiveRunnerLocation == "1")
                    {
                        if (ObjModel.IsLiveRunnerEnabled == true)
                        {
                            IsLiveRunnerState = true;
                        }
                    }
                    ObjtblOrderSearchedData.SearchedStateId = int.Parse(SelectedState);
                }

                ObjtblOrderSearchedData.SearchedZipCode = (ObjModel.Zip != null ? ObjModel.Zip.Trim() : string.Empty);
                ObjtblOrderSearchedData.best_SearchedZipCode = (ObjModel.Zip != null ? ObjModel.Zip.Trim() : string.Empty);

                //Multiple jursdiction apply for FCR
                List<tblOrderSearchedDataJurdictionInfo> ObjCollJurdistiction = new List<tblOrderSearchedDataJurdictionInfo>();
                if (ObjModel.Jurisdiction != "-1")
                {
                    string[] Jurisdiction = frm.GetValue("Jurisdiction").AttemptedValue.Split(',');
                    for (int index = 0; index < Jurisdiction.Count(); index++)
                    {
                        tblOrderSearchedDataJurdictionInfo ObjtblOrderSearchedDataJurisdiction = new tblOrderSearchedDataJurdictionInfo();
                        string[] jurisdictionDate = Jurisdiction[index].Split('_');
                        int JurisdictionId = Convert.ToInt32(jurisdictionDate[0]);
                        string JurisdictionName = jurisdictionDate[2];
                        if (JurisdictionId != -1)
                        {
                            ObjtblOrderSearchedDataJurisdiction.SearchedCountyId = JurisdictionId;
                            ObjtblOrderSearchedDataJurisdiction.SearchedCountyName = JurisdictionName;
                            ObjCollJurdistiction.Add(ObjtblOrderSearchedDataJurisdiction);
                        }
                    }
                }
                if (ObjModel.Jurisdiction != "-1")
                {
                    ObjtblOrderSearchedData.search_county = ObjModel.JurisdictionText;
                    ObjtblOrderSearchedData.best_search_county = ObjModel.JurisdictionText;
                    ObjtblOrderSearchedData.SearchedCountyId = Convert.ToInt32(ObjModel.Jurisdiction.ToString().Split('_').FirstOrDefault());
                }

                ObjtblOrderSearchedData.SearchedDriverLicense = (ObjModel.DriverLicense != null ? ObjModel.DriverLicense.Trim() : string.Empty);
                ObjtblOrderSearchedData.SearchedVehicleVin = (ObjModel.VehicleVin != null ? ObjModel.VehicleVin.Trim() : string.Empty);

                if (ObjModel.DLState != "-1")
                {
                    ObjtblOrderSearchedData.SearchedDLStateId = int.Parse(ObjModel.DLState.ToString().Split('_').FirstOrDefault());
                }

                ObjtblOrderSearchedData.SearchedSex = (ObjModel.Sex != "-1") ? ObjModel.Sex : string.Empty;
                ObjtblOrderSearchedData.SearchedBusinessName = (ObjModel.BusinessName != null ? ObjModel.BusinessName.Trim() : string.Empty);
                ObjtblOrderSearchedData.SearchedBusinessCity = (ObjModel.BusinessCity != null ? ObjModel.BusinessCity.Trim() : string.Empty);
                if (ObjModel.BusinessState != -1)
                {
                    ObjtblOrderSearchedData.SearchedBusinessStateId = Convert.ToInt32(ObjModel.BusinessState);
                }
                ObjtblOrderSearchedData.SearchedTrackingRef = ReferenceCode;
                if (ReferenceCodeId != 0)
                {
                    ObjtblOrderSearchedData.SearchedTrackingRefId = ReferenceCodeId;
                }
                ObjtblOrderSearchedData.SearchedTrackingNotes = (ObjModel.TrackingNotes != null ? ObjModel.TrackingNotes.Trim() : string.Empty);
                ObjtblOrderSearchedData.SearchedPermissibleId = ObjModel.PermissiblePurposeId; //ND-5 add this value to tblordersearched data

                #endregion

                #region Insertion in employment table

                List<tblOrderSearchedEmploymentInfo> ObjCollOrderSearchedEmploymentInfo = new List<tblOrderSearchedEmploymentInfo>();
                if (ObjCollProducts.Any(d => d.Contains("EMV")))
                {

                    //string[] CompanyName = frm.GetValue("CompanyName").AttemptedValue.Split(',');
                    string[] CompanyCity = frm.GetValue("CompanyCity").AttemptedValue.Split(',');
                    string[] CompanyPhone = frm.GetValue("CompanyPhone").AttemptedValue.Split(',');
                    //string[] AliasName = frm.GetValue("AliasName").AttemptedValue.Split(',');
                    string[] CompanyState = frm.GetValue("CompanyState").AttemptedValue.Split(',');
                    for (int iRow = 0; iRow < CompanyName.Count(); iRow++)
                    {
                        tblOrderSearchedEmploymentInfo ObjtblOrderSearchedEmploymentInfo = new tblOrderSearchedEmploymentInfo();
                        ObjtblOrderSearchedEmploymentInfo.SearchedAlias = AliasName[iRow];
                        ObjtblOrderSearchedEmploymentInfo.SearchedName = CompanyName[iRow];
                        ObjtblOrderSearchedEmploymentInfo.SearchedCity = CompanyCity[iRow];
                        ObjtblOrderSearchedEmploymentInfo.SearchedState = CompanyState[iRow];
                        ObjtblOrderSearchedEmploymentInfo.SearchedPhone = CompanyPhone[iRow] == "(___) ___-____" ? "" : CompanyPhone[iRow];

                        ObjtblOrderSearchedEmploymentInfo.SearchedContact = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedStarted = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedEnded = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedEmail = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedTitleStart = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedTitleEnd = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedSupervisorContact = string.Empty;
                        ObjtblOrderSearchedEmploymentInfo.SearchedReason = string.Empty;

                        ObjCollOrderSearchedEmploymentInfo.Add(ObjtblOrderSearchedEmploymentInfo);
                        if (iRow > 0)
                        {
                            ObjCollProducts.Add("EMV_0");
                        }
                    }
                }

                #endregion

                #region Insertion in education table

                List<tblOrderSearchedEducationInfo> ObjCollOrderSearchedEducationInfo = new List<tblOrderSearchedEducationInfo>();
                if (ObjCollProducts.Any(d => d.Contains("EDV")))
                {
                    string[] InstituteState = frm.GetValue("InstituteState").AttemptedValue.Split(',');
                    string[] SchoolCity = frm.GetValue("SchoolCity").AttemptedValue.Split(',');
                    string[] SchoolPhone = frm.GetValue("SchoolPhone").AttemptedValue.Split(',');
                    string[] GraduationDt = frm.GetValue("SearchedGraduationDt").AttemptedValue.Split(',');//INT336 GraduationDt
                    for (int iRow = 0; iRow < SchoolName.Count(); iRow++)
                    {

                        tblOrderSearchedEducationInfo ObjtblOrderSearchedEducationInfo = new tblOrderSearchedEducationInfo();
                        ObjtblOrderSearchedEducationInfo.SearchedSchoolName = SchoolName[iRow];
                        ObjtblOrderSearchedEducationInfo.SearchedCity = SchoolCity[iRow];
                        ObjtblOrderSearchedEducationInfo.SearchedPhone = SchoolPhone[iRow] == "(___) ___-____" ? "" : SchoolPhone[iRow];
                        ObjtblOrderSearchedEducationInfo.SearchedGraduationDt = GraduationDt[iRow];//INT336 GraduationDt
                        ObjtblOrderSearchedEducationInfo.SearchedAlias = InstituteAlias[iRow];
                        ObjtblOrderSearchedEducationInfo.SearchedState = InstituteState[iRow];

                        ObjtblOrderSearchedEducationInfo.SearchedStarted = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedDegree = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedEnded = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedMajor = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedFax = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedGrade = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedCredit = string.Empty;
                        ObjtblOrderSearchedEducationInfo.SearchedEmail = string.Empty;

                        ObjCollOrderSearchedEducationInfo.Add(ObjtblOrderSearchedEducationInfo);

                        if (iRow > 0)
                        {
                            ObjCollProducts.Add("EDV_0");
                        }
                        // change for INT 328 BILLING ISSue on 10 march 2016
                        //According to greg ,we have to find add new amount in BILLING AND IN ITEMISED REPPORT SECTION
                    }
                }

                #endregion

                #region Insertion in Personal table and Question table

                List<tblOrderSearchedPersonalInfo> ObjCollOrderSearchedPersonalInfo = new List<tblOrderSearchedPersonalInfo>();
                List<tblOrderPersonalQtnInfo> objColltblOrderPersonalQtnInfo = new List<tblOrderPersonalQtnInfo>();
                if (ObjCollProducts.Any(d => d.Contains("PRV")))
                {
                    string[] ReferencePhone = frm.GetValue("ReferencePhone").AttemptedValue.Split(',');
                    for (int iRow = 0; iRow < ReferenceName.Count(); iRow++)
                    {
                        #region For other grid Controls
                        tblOrderSearchedPersonalInfo ObjtblOrderSearchedPersonalInfo = new tblOrderSearchedPersonalInfo();
                        ObjtblOrderSearchedPersonalInfo.SearchedName = ReferenceName[iRow];
                        ObjtblOrderSearchedPersonalInfo.SearchedPhone = ReferencePhone[iRow] == "(___) ___-____" ? "" : ReferencePhone[iRow];
                        //ObjtblOrderSearchedPersonalInfo.pkOrderSearchedPersnlId = pkOrderSearchedPersnlId;
                        ObjtblOrderSearchedPersonalInfo.SearchedAlias = ReferenceAlias[iRow];

                        ObjtblOrderSearchedPersonalInfo.SearchedCity = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedState = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedAltPhone = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedRelation = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedEmail = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedSalary = string.Empty;
                        ObjtblOrderSearchedPersonalInfo.SearchedTitle = string.Empty;

                        ObjCollOrderSearchedPersonalInfo.Add(ObjtblOrderSearchedPersonalInfo);
                        if (iRow > 0)
                        {
                            ObjCollProducts.Add("PRV_0");
                        }

                        // change for INT 328 BILLING ISSue on 10 march 2016
                        //According to greg ,we have to find add new amount in BILLING AND IN ITEMISED REPPORT SECTION 
                        #endregion

                        #region For questions

                        tblOrderPersonalQtnInfo objtblOrderPersonalQtnInfo = new tblOrderPersonalQtnInfo();
                        objColltblOrderPersonalQtnInfo.Add(objtblOrderPersonalQtnInfo);

                        #endregion
                    }

                }

                #endregion


                #region Insertion in Profession table and Question table //INT336 //INT336 Professional Reference.

                List<tblOrderSearchedProfessionalInfo> ObjCollOrderSearchedProfessionalInfo = new List<tblOrderSearchedProfessionalInfo>();

                if (ObjCollProducts.Any(d => d.Contains("PRV2")))
                {
                    string[] PReferenceName = frm.GetValue("PReferenceName").AttemptedValue.Split(',');
                    string[] PReferencePhone = frm.GetValue("PReferencePhone").AttemptedValue.Split(',');
                    string[] PReferenceAlias = frm.GetValue("PReferenceAlias").AttemptedValue.Split(',');
                    for (int iRow = 0; iRow < PReferenceName.Count(); iRow++)
                    {

                        #region For other grid Controls

                        tblOrderSearchedProfessionalInfo OrderSearchedProfessionalInfo = new tblOrderSearchedProfessionalInfo();
                        OrderSearchedProfessionalInfo.SearchedName = PReferenceName[iRow];
                        OrderSearchedProfessionalInfo.SearchedPhone = PReferencePhone[iRow] == "(___) ___-____" ? "" : PReferencePhone[iRow];
                        OrderSearchedProfessionalInfo.SearchedAlias = PReferenceAlias[iRow];

                        OrderSearchedProfessionalInfo.SearchedCity = string.Empty;
                        OrderSearchedProfessionalInfo.SearchedState = string.Empty;
                        OrderSearchedProfessionalInfo.SearchedAltPhone = string.Empty;
                        OrderSearchedProfessionalInfo.SearchedRelation = string.Empty;
                        OrderSearchedProfessionalInfo.SearchedEmail = string.Empty;
                        OrderSearchedProfessionalInfo.SearchedSalary = string.Empty;
                        OrderSearchedProfessionalInfo.SearchedTitle = string.Empty;

                        ObjCollOrderSearchedProfessionalInfo.Add(OrderSearchedProfessionalInfo);

                        #endregion


                        if (iRow > 0)
                        {
                            ObjCollProducts.Add("PRV2_0");
                        }

                        // change for INT 328 BILLING ISSue on 10 march 2016
                        //According to greg ,we have to find add new amount in BILLING AND IN ITEMISED REPPORT SECTION 
                    }

                }

                #endregion



                #region Insertion in License verification table

                List<tblOrderSearchedLicenseInfo> ObjCollOrderSearchedLicenseInfo = new List<tblOrderSearchedLicenseInfo>();

                if (ObjCollProducts.Any(d => d.Contains("PLV")))
                {
                    //string[] LicenseType = frm.GetValue("LicenseType").AttemptedValue.Split(',');
                    string[] LicenseState = frm.GetValue("LicenseState").AttemptedValue.Split(',');
                    string[] LicenseNumber = frm.GetValue("LicenseNumber").AttemptedValue.Split(',');
                    string[] LicenseIssueDate = frm.GetValue("LicenseIssueDate").AttemptedValue.Split(',');
                    //string[] LicenseAlias = frm.GetValue("LicenseAlias").AttemptedValue.Split(',');
                    for (int iRow = 0; iRow < LicenseType.Count(); iRow++)
                    {
                        tblOrderSearchedLicenseInfo ObjtblOrderSearchedLicenseInfo = new tblOrderSearchedLicenseInfo();
                        //ObjtblOrderSearchedLicenseInfo.pkOrderLicenseVerifyId = Guid.NewGuid();
                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseType = LicenseType[iRow];
                        ObjtblOrderSearchedLicenseInfo.SearchedIssueDate = LicenseIssueDate[iRow];
                        ObjtblOrderSearchedLicenseInfo.SearchedIssuingState = LicenseState[iRow];
                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseNo = LicenseNumber[iRow];
                        ObjtblOrderSearchedLicenseInfo.SearchedAlias = LicenseAlias[iRow];

                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseName = string.Empty;
                        //ObjtblOrderSearchedLicenseInfo.SearchedLicenseType = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedAuthority = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedPhone = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedExpiryDate = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedStatus = string.Empty;
                        ObjCollOrderSearchedLicenseInfo.Add(ObjtblOrderSearchedLicenseInfo);
                        // change for INT 328 BILLING ISSue on 10 march 2016
                        //According to greg ,we have to find add new amount in BILLING AND IN ITEMISED REPPORT SECTION 
                        if (iRow > 0)
                        {
                            ObjCollProducts.Add("PLV_0");
                        }
                    }
                }

                if (ObjCollProducts.Any(d => d.Contains("WCV")))
                {
                    //string[] LicenseType = frm.GetValue("LicenseType").AttemptedValue.Split(',');
                    string[] LicenseState = frm.GetValue("LicenseState").AttemptedValue.Split(',');
                    string[] LicenseNumber = frm.GetValue("LicenseNumber").AttemptedValue.Split(',');
                    string[] LicenseIssueDate = frm.GetValue("LicenseIssueDate").AttemptedValue.Split(',');
                    //string[] LicenseAlias = frm.GetValue("LicenseAlias").AttemptedValue.Split(',');
                    for (int iRow = 0; iRow < LicenseType.Count(); iRow++)
                    {
                        tblOrderSearchedLicenseInfo ObjtblOrderSearchedLicenseInfo = new tblOrderSearchedLicenseInfo();
                        //ObjtblOrderSearchedLicenseInfo.pkOrderLicenseVerifyId = Guid.NewGuid();
                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseType = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedIssueDate = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedIssuingState = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseNo = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedAlias = LicenseAlias[iRow];

                        ObjtblOrderSearchedLicenseInfo.SearchedLicenseName = string.Empty;
                        //ObjtblOrderSearchedLicenseInfo.SearchedLicenseType = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedAuthority = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedPhone = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedExpiryDate = string.Empty;
                        ObjtblOrderSearchedLicenseInfo.SearchedStatus = string.Empty;
                        ObjCollOrderSearchedLicenseInfo.Add(ObjtblOrderSearchedLicenseInfo);
                        // change for INT 328 BILLING ISSue on 10 march 2016
                        //According to greg ,we have to find add new amount in BILLING AND IN ITEMISED REPPORT SECTION 
                        if (iRow > 0)
                        {
                            ObjCollProducts.Add("WCV_0");
                        }
                    }
                }

                #endregion

                #region For Social Diligence

                #region Insertion in Alias PersonalInfo table

                //List<tblOrderSearchedAliasPersonalInfo> ObjCollOrderSearchedAliasPersonalInfo = new List<tblOrderSearchedAliasPersonalInfo>();

                #endregion

                #region Insertion in Alias Location Info table

                //List<tblOrderSearchedAliasLocationInfo> ObjCollOrderSearchedAliasLocationInfo = new List<tblOrderSearchedAliasLocationInfo>();

                #endregion

                #region Insertion in Alias Work Info table

                //List<tblOrderSearchedAliasWorkInfo> ObjCollOrderSearchedAliasWorkInfo = new List<tblOrderSearchedAliasWorkInfo>();

                #endregion

                #endregion

                #region Insertion in LiveRunnerInfo table For CCR's Reports

                List<tblOrderSearchedLiveRunnerInfo> ObjColltblOrderSearchedLiveRunnerInfo = new List<tblOrderSearchedLiveRunnerInfo>();
                if (ObjCollProducts.Any(d => d.Contains("CCR1")) || ObjCollProducts.Any(d => d.Contains("CCR2")) || ObjCollProducts.Any(d => d.Contains("RCX")))
                {
                    // List of the Live Runner Products      
                    string[] CountyInfoState = frm.GetValue("CountyInfoState").AttemptedValue.Split(',');
                    string[] CountyInfoCounty = frm.GetValue("CountyInfoCounty").AttemptedValue.Split(',');

                    for (int iRow = 0; iRow < CountyInfoCounty.Count(); iRow++)
                    {
                        string[] CountyValueData = CountyInfoCounty[iRow].Split('_');
                        int CountyId = Convert.ToInt32(CountyValueData[0]);
                        if (CountyId != -1)
                        {
                            string IsRcxCounty = string.Empty;
                            IsRcxCounty = "False";
                            if (ObjModel.IsLiveRunnerEnabled == true)
                            {
                                IsRcxCounty = CountyValueData[1];
                            }
                            tblOrderSearchedLiveRunnerInfo ObjBALtblOrderSearchedLiveRunnerInfo = new tblOrderSearchedLiveRunnerInfo();
                            ObjBALtblOrderSearchedLiveRunnerInfo.LiveRunnerCounty = CountyValueData[5] + "_" + IsRcxCounty; //ddlCountyInfoCounty.SelectedItem.Text.Trim() + "_" + IsRcxCounty;
                            ObjBALtblOrderSearchedLiveRunnerInfo.LiveRunnerState = CountyInfoState[iRow];
                            ObjBALtblOrderSearchedLiveRunnerInfo.SearchedCountyId = CountyId;
                            ObjColltblOrderSearchedLiveRunnerInfo.Add(ObjBALtblOrderSearchedLiveRunnerInfo);
                        }
                    }
                }

                #endregion

                #region Live Runner Information For BAL Prcocessing
                //Dictionary<string, string> dictRcxProducts = new Dictionary<string, string>();
                #endregion

                #region Null DOB
                int IsNullDOB = 0;
                if (ObjModel.chkIsNullDOB)
                    IsNullDOB = 1;
                #endregion

                #region Insert In OrderDetails For Consent Form

                string ConsentEmailPDFPath = Server.MapPath("~/Resources/Upload/ConsentForm/usaintelConsentForm.pdf");
                List<CLSConsentInfo> ObjCollConsentInfo = new List<CLSConsentInfo>();

                if (frm.GetValue("ConsentReport") != null)
                {
                    string[] ConsentFaxEmail = new string[0];
                    string[] ConsentReport = frm.GetValue("ConsentReport").AttemptedValue.Split(',');
                    string[] ConsentState = frm.GetValue("ConsentState").AttemptedValue.Split(',');
                    // string[] ConsentFile = frm.GetValue("ConsentFile").AttemptedValue.Split(',');
                    if (frm.GetValue("ConsentFaxEmail") != null)
                    {
                        ConsentFaxEmail = frm.GetValue("ConsentFaxEmail").AttemptedValue.Split(',');
                    }
                    string FileName = string.Empty;
                    string sFilePath = "";

                    for (int i = 0; i < ConsentReport.Length; i++)
                    {

                        bool FAXEmail = false;
                        if (ConsentFaxEmail.Contains(i.ToString()))
                        {
                            FAXEmail = true;
                        }
                        HttpPostedFileBase ObjHttpPostedFile = Request.Files[i];
                        if (ObjHttpPostedFile.FileName != "")
                        {
                            string sExt = Path.GetExtension(ObjHttpPostedFile.FileName).ToLower();
                            FileName = DateTime.Now.ToFileTime().ToString() + sExt;
                            sFilePath = Server.MapPath("~/Resources/Upload/ConsentForm/") + FileName;
                            ObjHttpPostedFile.SaveAs(sFilePath);
                        }
                        else
                        {
                            FileName = string.Empty;
                        }


                        CLSConsentInfo ObjConsentInfo = new CLSConsentInfo();
                        ObjConsentInfo.pkConsentId = i + 1;
                        ObjConsentInfo.ConsentFileName = FileName;
                        ObjConsentInfo.IsConsentEmailOrFax = FAXEmail;
                        ObjConsentInfo.IsConsentRequired = true;
                        ObjConsentInfo.ConsentReportType = ConsentReport[i];
                        ObjConsentInfo.ConsentState = ConsentState[i];
                        ObjConsentInfo.PDFAttachFilePath = ConsentEmailPDFPath;
                        ObjCollConsentInfo.Add(ObjConsentInfo);
                    }
                }
                #endregion Insert In OrderDetails For Consent Form

                #region Calling Business Layer Method for insertion in db

                ObjEmergeReport.ObjCollOrderSearchedEducationInfo = ObjCollOrderSearchedEducationInfo;
                ObjEmergeReport.ObjCollOrderSearchedEmploymentInfo = ObjCollOrderSearchedEmploymentInfo;
                ObjEmergeReport.ObjCollOrderSearchedLicenseInfo = ObjCollOrderSearchedLicenseInfo;
                ObjEmergeReport.ObjCollOrderSearchedPersonalInfo = ObjCollOrderSearchedPersonalInfo;
                ObjEmergeReport.ObjCollOrderSearchedProfessionalInfo = ObjCollOrderSearchedProfessionalInfo;
                ObjEmergeReport.ObjCollProducts = ObjCollProducts;
                ObjEmergeReport.objColltblOrderPersonalQtnInfo = objColltblOrderPersonalQtnInfo;
                ObjEmergeReport.ObjtblOrder = ObjtblOrder;
                ObjEmergeReport.ObjtblOrderSearchedData = ObjtblOrderSearchedData;
                ObjEmergeReport.ObjColltblOrderSearchedLiveRunnerInfo = ObjColltblOrderSearchedLiveRunnerInfo;
                ObjEmergeReport.IsNullDOB = IsNullDOB;
                ObjEmergeReport.IsLiveRunnerState = IsLiveRunnerState;
                ObjEmergeReport.ObjCollCLSConsentInfo = ObjCollConsentInfo;
                ObjEmergeReport.ObjTieredPackage = ObjtblTieredPackage;
                ObjEmergeReport.ObjOrderInfo = ObjOrderInfo;
                ObjEmergeReport.CompanyAccountNo = ObjModel.CompanyAccountNo;
                ObjEmergeReport.OrderCompanyName = ObjModel.OrderCompanyName;
                ObjEmergeReport.PermissiblePurpose = ObjModel.PermissiblePurposeValue;
                ObjEmergeReport.ObjJurisdictionList = ObjCollJurdistiction;//used for multiple jurisdiction
                string sCurrentUserXMLFile = OrderNo.ToString() + ".xml";
                XmlSerializer serializer = new XmlSerializer(typeof(EmergeReport));
                Session["NewReportXMLPath"] = sCurrentUserXMLFile;
                Session["EmergeReport"] = ObjEmergeReport;
                using (TextWriter textWriter = new StreamWriter(Server.MapPath(@"~\Resources\Upload\NewReportXML\" + sCurrentUserXMLFile)))
                {
                    serializer.Serialize(textWriter, ObjEmergeReport);
                }
                serializer = null;
                #endregion
                int OrderId = SaveNewReportForApi();
                //int OrderId = SaveNewReportLandingPage();

                if (OrderId != 0)
                {
                    //string Company_Name = ObjModel.CompanyName;

                    return Redirect(ObjModel.Urlpath + "?OrderId=" + OrderId + "");
                }
            }
            return View();
        }
        public int SaveNewReportForApi()
        {
            // OrderInfo ObjOrderInfo = new OrderInfo();
            //Xml ObjXml = new Xml();
            //XDocument ObjXDocument = new XDocument();
            //StringBuilder response = new StringBuilder();
            //StringBuilder responseOptional = new StringBuilder();
            // StringBuilder TieredHeadText = new StringBuilder();
            //List<tblState> tblStateColl = new List<tblState>();

            XmlSerializer serializer = new XmlSerializer(typeof(EmergeReport));
            string FilePath = Server.MapPath(@"~\Resources\Upload\NewReportXML\" + Convert.ToString(Session["NewReportXMLPath"]));

            BALGeneral.WriteLogForDeletion(FilePath, "LandingPage_" + DateTime.Now.ToString("yyMMdd"));

            //TextReader tr = new StreamReader(FilePath);
            EmergeReport ObjEmergeReport = new EmergeReport();
            using (TextReader textWriter = new StreamReader(FilePath))
            {
                ObjEmergeReport = (EmergeReport)serializer.Deserialize(textWriter);
            }
            serializer = null;

            BALGeneral.WriteLogForDeletion("serializer", "LandingPage_" + DateTime.Now.ToString("yyMMdd"));
            int OrderId = 0;
            //int LocationId = (Session["pkLocationId"] == null) ? 0 : Convert.ToInt32(Session["pkLocationId"].ToString());
            //int CompanyUserId = (Session["CurrentUserId"] == null) ? 0 : Convert.ToInt32(Session["CurrentUserId"].ToString());
            BALOrders ObjBALOrders = new BALOrders();

            List<tblOrderSearchedAliasPersonalInfo> ObjCollOrderSearchedAliasPersonalInfo = new List<tblOrderSearchedAliasPersonalInfo>();
            List<tblOrderSearchedAliasLocationInfo> ObjCollOrderSearchedAliasLocationInfo = new List<tblOrderSearchedAliasLocationInfo>();
            List<tblOrderSearchedAliasWorkInfo> ObjCollOrderSearchedAliasWorkInfo = new List<tblOrderSearchedAliasWorkInfo>();
            Dictionary<string, Guid> ObjCompnyProductInfo = new Dictionary<string, Guid>();

            if (ObjEmergeReport.ObjJurisdictionList.Count() == 0)
            {
                tblOrderSearchedDataJurdictionInfo jur = new tblOrderSearchedDataJurdictionInfo();
                jur.SearchedCountyId = Convert.ToInt32(ObjEmergeReport.ObjtblOrderSearchedData.SearchedCountyId);
                jur.SearchedCountyName = ObjEmergeReport.ObjtblOrderSearchedData.search_county;
                ObjEmergeReport.ObjJurisdictionList.Add(jur);
            }

            if (ObjEmergeReport.ObjOrderInfo.IsTiered == true)
            {
                ObjEmergeReport.ObjOrderInfo.CheckTieredForApi = true;
            }

            ObjEmergeReport = ObjBALOrders.AddOrders(ObjEmergeReport.ObjtblOrder,
            ObjEmergeReport.ObjCollProducts,
            ObjEmergeReport.ObjtblOrderSearchedData,
            ObjEmergeReport.ObjCollOrderSearchedEmploymentInfo,
            ObjEmergeReport.ObjCollOrderSearchedEducationInfo,
            ObjEmergeReport.ObjCollOrderSearchedPersonalInfo,
            ObjEmergeReport.ObjCollOrderSearchedProfessionalInfo,
            ObjEmergeReport.objColltblOrderPersonalQtnInfo,
            ObjEmergeReport.ObjCollOrderSearchedLicenseInfo,
            ObjCollOrderSearchedAliasPersonalInfo,
            ObjCollOrderSearchedAliasLocationInfo,
            ObjCollOrderSearchedAliasWorkInfo,
            ObjCompnyProductInfo,
            ObjEmergeReport.ObjColltblOrderSearchedLiveRunnerInfo, ObjEmergeReport.ObjCollCLSConsentInfo,
            Utility.SiteApplicationId, ObjEmergeReport.IsNullDOB,
            ObjEmergeReport.IsLiveRunnerState, ObjEmergeReport.ObjTieredPackage, ObjEmergeReport.ObjOrderInfo, ObjEmergeReport.CompanyAccountNo, ObjEmergeReport.OrderCompanyName, ObjEmergeReport.PermissiblePurpose, ObjEmergeReport.ObjJurisdictionList);
            if (ObjEmergeReport.ObjOrderInfo.OrderId != null)
            {
                OrderId = (int)ObjEmergeReport.ObjOrderInfo.OrderId;

            }
            return OrderId;
        }



        public int SaveNewReportLandingPage()
        {
            decimal GrandTotal = 0;
            string COUNTIES = "";
            string STATES = "";
            Boolean flagAutopopup = false;
            List<JurisdictionListColl> ObjJurisdictionListColl = new List<JurisdictionListColl>();
            BALCompanyType Objbalcompanytype = new BALCompanyType();
            BALCompany Objbalcompany = new BALCompany();
            //OrderInfo ObjOrderInfo = new OrderInfo();
            //Xml ObjXml = new Xml();
            XDocument ObjXDocument = new XDocument();
            StringBuilder response = new StringBuilder();
            StringBuilder responseOptional = new StringBuilder();
            StringBuilder TieredHeadText = new StringBuilder();
            //List<tblState> tblStateColl = new List<tblState>();

            XmlSerializer serializer = new XmlSerializer(typeof(EmergeReport));
            string FilePath = Server.MapPath(@"~\Resources\Upload\NewReportXML\" + Convert.ToString(Session["NewReportXMLPath"]));
            //TextReader tr = new StreamReader(FilePath);
            EmergeReport ObjEmergeReport = new EmergeReport();
            using (TextReader textWriter = new StreamReader(FilePath))
            {
                ObjEmergeReport = (EmergeReport)serializer.Deserialize(textWriter);
            }
            serializer = null;

            int? OrderId = 0;
            int LocationId = (Session["pkLocationId"] == null) ? 0 : Convert.ToInt32(Session["pkLocationId"].ToString());
            int CompanyUserId = (Session["CurrentUserId"] == null) ? 0 : Convert.ToInt32(Session["CurrentUserId"].ToString());
            BALOrders ObjBALOrders = new BALOrders();

            List<tblOrderSearchedAliasPersonalInfo> ObjCollOrderSearchedAliasPersonalInfo = new List<tblOrderSearchedAliasPersonalInfo>();
            List<tblOrderSearchedAliasLocationInfo> ObjCollOrderSearchedAliasLocationInfo = new List<tblOrderSearchedAliasLocationInfo>();
            List<tblOrderSearchedAliasWorkInfo> ObjCollOrderSearchedAliasWorkInfo = new List<tblOrderSearchedAliasWorkInfo>();
            Dictionary<string, Guid> ObjCompnyProductInfo = new Dictionary<string, Guid>();
            ObjEmergeReport.ObjOrderInfo.CheckTieredForApi = true;
            ObjEmergeReport = ObjBALOrders.AddOrders(ObjEmergeReport.ObjtblOrder,
                ObjEmergeReport.ObjCollProducts,
                ObjEmergeReport.ObjtblOrderSearchedData,
                ObjEmergeReport.ObjCollOrderSearchedEmploymentInfo,
                ObjEmergeReport.ObjCollOrderSearchedEducationInfo,
                ObjEmergeReport.ObjCollOrderSearchedPersonalInfo,
                ObjEmergeReport.ObjCollOrderSearchedProfessionalInfo,//INT336
                ObjEmergeReport.objColltblOrderPersonalQtnInfo,
                ObjEmergeReport.ObjCollOrderSearchedLicenseInfo,
                ObjCollOrderSearchedAliasPersonalInfo,
                ObjCollOrderSearchedAliasLocationInfo,
                ObjCollOrderSearchedAliasWorkInfo,
                ObjCompnyProductInfo,
                ObjEmergeReport.ObjColltblOrderSearchedLiveRunnerInfo, ObjEmergeReport.ObjCollCLSConsentInfo,
                Utility.SiteApplicationId, ObjEmergeReport.IsNullDOB,
                ObjEmergeReport.IsLiveRunnerState, ObjEmergeReport.ObjTieredPackage, ObjEmergeReport.ObjOrderInfo, ObjEmergeReport.CompanyAccountNo, ObjEmergeReport.OrderCompanyName, ObjEmergeReport.PermissiblePurpose, ObjEmergeReport.ObjJurisdictionList);

            if (ObjEmergeReport.ObjOrderInfo.OrderId != null)
            {
                OrderId = ObjEmergeReport.ObjOrderInfo.OrderId;
                using (EmergeDALDataContext dx = new EmergeDALDataContext())
                {
                    ObjEmergeReport.ObjtblOrder = dx.tblOrders.Where(d => d.pkOrderId == OrderId).FirstOrDefault();
                    ObjEmergeReport.ObjtblOrderSearchedData = dx.tblOrderSearchedDatas.Where(d => d.fkOrderId == OrderId).FirstOrDefault();
                }
            }

            using (EmergeDALDataContext dx1 = new EmergeDALDataContext())
            {
                if (!ObjEmergeReport.ObjOrderInfo.IsAutomatic)
                {
                    #region Is Automatic False
                    List<TiredSearchedData> searchedDataList1 = new List<TiredSearchedData>();
                    // XmlDocument ObjXmlDocument = new XmlDocument();
                    XDocument ObjXmlDoc = new XDocument();
                    if (ObjEmergeReport.ObjTieredPackage != null && ObjEmergeReport.ObjOrderInfo.IsTiered && ObjEmergeReport.ObjOrderInfo.Tiered2Response != null)
                    {
                        string sHostname = "";
                        string sTiered2 = "";
                        string sTiered3 = "";
                        string sOptional = "";

                        int TimeFrame = ObjEmergeReport.ObjTieredPackage.TimeFrame;
                        int TimeFrameYear = TimeFrame * (-1);
                        DateTime TimeFrameDate = DateTime.Now.AddYears(TimeFrameYear);
                        ObjXmlDoc = XDocument.Parse(ObjEmergeReport.ObjOrderInfo.Tiered2Response);
                        string XML_Value = GetRequiredXmlConditional(ObjXmlDoc.Document.ToString(), ObjEmergeReport.ObjOrderInfo.HostProductCode);
                        ObjXDocument = XDocument.Parse(XML_Value);
                        Dictionary<string, string> DictCounty = new Dictionary<string, string>();

                        if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode != null)
                        {
                            ObjEmergeReport.ObjCollProducts.Add(ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode);
                        }
                        if (ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode != null)
                        {
                            ObjEmergeReport.ObjCollProducts.Add(ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode);
                        }

                        if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode != null)
                        {
                            ObjEmergeReport.ObjCollProducts.Add(ObjEmergeReport.ObjOrderInfo.OptionalProductCode);
                        }

                        #region Header Text

                        List<tblProductPerApplication> ObjtblPPA = new List<tblProductPerApplication>();
                        tblProductPerApplication ObjtblPPAs = new tblProductPerApplication();

                        using (EmergeDALDataContext dx = new EmergeDALDataContext())
                        {
                            ObjtblPPA = dx.tblProductPerApplications.ToList<tblProductPerApplication>();

                            ObjtblPPAs = ObjtblPPA.Where(d => d.pkProductApplicationId == ObjEmergeReport.ObjTieredPackage.Host).FirstOrDefault();


                            if (ObjtblPPAs != null)
                            {
                                sHostname = "[" + ObjtblPPAs.ProductCode + "] " + ObjtblPPAs.ProductDisplayName;
                            }
                            ObjtblPPAs = null;
                            ObjtblPPAs = ObjtblPPA.Where(d => d.pkProductApplicationId == ObjEmergeReport.ObjTieredPackage.Tired2).FirstOrDefault();
                            if (ObjtblPPAs != null)
                            {
                                sTiered2 = "[" + ObjtblPPAs.ProductCode + "] " + ObjtblPPAs.ProductDisplayName;
                            }
                            ObjtblPPAs = null;
                            ObjtblPPAs = ObjtblPPA.Where(d => d.pkProductApplicationId == ObjEmergeReport.ObjTieredPackage.Tired3).FirstOrDefault();
                            if (ObjtblPPAs != null)
                            {
                                sTiered3 = "[" + ObjtblPPAs.ProductCode + "] " + ObjtblPPAs.ProductDisplayName;
                            }
                            ObjtblPPAs = null;
                            ObjtblPPAs = ObjtblPPA.Where(d => d.pkProductApplicationId == ObjEmergeReport.ObjTieredPackage.TiredOptional).FirstOrDefault();
                            if (ObjtblPPAs != null)
                            {
                                sOptional = "[" + ObjtblPPAs.ProductCode + "] " + ObjtblPPAs.ProductDisplayName;
                            }

                            if (!string.IsNullOrEmpty(sTiered2) && !string.IsNullOrEmpty(sTiered3))
                            {
                                TieredHeadText.Append("<table style=\"width:100%;\">");
                                TieredHeadText.Append("<tr>");

                                if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Contains("FCR"))
                                {
                                    TieredHeadText.Append("<td style=\"width:49.8%;border-left: 1px solid gray;border-right: 1px solid gray;\">");
                                    TieredHeadText.Append("<table>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Host Report:</td><td>" + sHostname + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Tiered 2 Report:</td><td>" + sTiered2 + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Tiered 2 Optional Report:</td><td>" + sOptional + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Timeframe: </td><td>" + ObjEmergeReport.ObjTieredPackage.TimeFrame + " Year(s)</td><tr>");
                                    // TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Conditional Variable:</td><td>" + ObjEmergeReport.ObjTieredPackage.Variable + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'></td><td></td><tr>");
                                    TieredHeadText.Append("</table>");
                                    TieredHeadText.Append("</td>");
                                    TieredHeadText.Append("<td style=\"border-right: 1px solid gray;\">");
                                    TieredHeadText.Append("<table>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Host Report:</td><td>" + sHostname + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Tiered 2 Report:</td><td>" + sTiered3 + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Tiered 2 Optional Report:</td><td>" + sOptional + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Timeframe: </td><td>" + ObjEmergeReport.ObjTieredPackage.TimeFrame + " Year(s)</td><tr>");
                                    //TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Conditional Variable:</td><td>" + ObjEmergeReport.ObjTieredPackage.Variable + "</td><tr>");
                                    TieredHeadText.Append("</table>");
                                    TieredHeadText.Append("</td>");
                                }
                                else
                                {
                                    TieredHeadText.Append("<td style=\"width:49.8%;border-left: 1px solid gray;border-right: 1px solid gray;\">");
                                    TieredHeadText.Append("<table>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Host Report:</td><td>" + sHostname + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Tiered 2 Report:</td><td>" + sTiered3 + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Tiered 2 Optional Report:</td><td>" + sOptional + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Timeframe: </td><td>" + ObjEmergeReport.ObjTieredPackage.TimeFrame + " Year(s)</td><tr>");
                                    // TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Conditional Variable:</td><td>" + ObjEmergeReport.ObjTieredPackage.Variable + "</td><tr>");
                                    TieredHeadText.Append("</table>");
                                    TieredHeadText.Append("</td>");
                                    TieredHeadText.Append("<td style=\"border-right: 1px solid gray;\">");
                                    TieredHeadText.Append("<table>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Host Report:</td><td>" + sHostname + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Tiered 2 Report:</td><td>" + sTiered2 + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Tiered 2 Optional Report:</td><td>" + sOptional + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Timeframe: </td><td>" + ObjEmergeReport.ObjTieredPackage.TimeFrame + " Year(s)</td><tr>");
                                    // TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Conditional Variable:</td><td>" + ObjEmergeReport.ObjTieredPackage.Variable + "</td><tr>");
                                    TieredHeadText.Append("</table>");
                                    TieredHeadText.Append("</td>");
                                }

                                TieredHeadText.Append("</tr>");
                                TieredHeadText.Append("</table>");
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(sTiered2))
                                {
                                    TieredHeadText.Append("<table>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Host Report:</td><td>" + sHostname + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Tiered 2 Report:</td><td>" + sTiered2 + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Tiered 2 Optional Report:</td><td>" + sOptional + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Timeframe: </td><td>" + ObjEmergeReport.ObjTieredPackage.TimeFrame + " Year(s)</td><tr>");
                                    // TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Conditional Variable:</td><td>" + ObjEmergeReport.ObjTieredPackage.Variable + "</td><tr>");
                                    TieredHeadText.Append("</table>");
                                }
                                else
                                {
                                    TieredHeadText.Append("<table>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Host Report:</td><td>" + sHostname + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Tiered 2 Report:</td><td>" + sTiered3 + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Tiered 2 Optional Report:</td><td>" + sOptional + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Timeframe: </td><td>" + ObjEmergeReport.ObjTieredPackage.TimeFrame + " Year(s)</td><tr>");
                                    //TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Conditional Variable:</td><td>" + ObjEmergeReport.ObjTieredPackage.Variable + "</td><tr>");
                                    TieredHeadText.Append("</table>");
                                }

                            }
                        }
                        #endregion

                        #region HOST Response
                        // int subTotalRowCount = 0;

                        if (ObjEmergeReport.ObjOrderInfo.HostProductCode == "PS")
                        {
                            #region PS HostReport
                            try
                            {
                                string SearchedStateName = string.Empty;
                                string SearchedStateCode = string.Empty;
                                string IsStateFound = string.Empty;
                                if (ObjEmergeReport.ObjtblOrderSearchedData.SearchedStateId != null)
                                {
                                    SearchedStateName = (from d in dx1.tblStates
                                                         where (d.pkStateId == ObjEmergeReport.ObjtblOrderSearchedData.SearchedStateId)
                                                         select d.StateName).FirstOrDefault().ToString();
                                    SearchedStateCode = (from d in dx1.tblStates
                                                         where (d.pkStateId == ObjEmergeReport.ObjtblOrderSearchedData.SearchedStateId)
                                                         select d.StateCode).FirstOrDefault().ToString();
                                }

                                int DictCount = 0;
                                string Tiered2ReportType = string.Empty;
                                string Tiered3ReportType = string.Empty;
                                List<XElement> Credit_File = ObjXDocument.Descendants("records").ToList();
                                List<XElement> CreditFile = ObjXDocument.Descendants("records").ToList();

                                //if (!string.IsNullOrEmpty(ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode) && !string.IsNullOrEmpty(ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode))
                                if (!string.IsNullOrEmpty(sTiered2) && !string.IsNullOrEmpty(sTiered3))
                                {
                                    #region Tiered FCR and Tiered CCR1

                                    if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Contains("FCR"))
                                    {


                                        Tiered2ReportType = "Tiered2";

                                        response.Append("<table style=\"width:100%;\">");
                                        response.Append("<tr>");
                                        response.Append("<td style=\"width:50%;border-left: 1px solid gray;border-right: 1px solid gray;\">");

                                        //Changed By Himesh Kumar
                                        #region Tiered FCR

                                        if (Credit_File != null && Credit_File.Count > 0)
                                        {

                                            List<string> lstJFCR = new List<string>();

                                            List<XElement> Personal_Data = Credit_File.Descendants("record").ToList();

                                            List<Region_Name> ObjRegion_Name = new List<Region_Name>();
                                            if (Personal_Data != null && Personal_Data.Count > 0)
                                            {
                                                foreach (XElement xe in Personal_Data)
                                                {
                                                    XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                    XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                    XElement Region = Address.Descendants("state").FirstOrDefault();
                                                    XElement County = Address.Descendants("county").FirstOrDefault();


                                                    XElement FName = xe.Descendants("firstname").FirstOrDefault();
                                                    XElement MName = xe.Descendants("middlename").FirstOrDefault();
                                                    XElement LName = xe.Descendants("lastname").FirstOrDefault();
                                                    XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                    if (EffectiveDate != null)
                                                    {
                                                        string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";
                                                        TiredSearchedData objTiered = new TiredSearchedData();
                                                        if (Region != null && County != null != null && LName != null && YearMonth != null)
                                                        {
                                                            var region_value = Region.Value.ToString();
                                                            int check_item = 1;
                                                            for (int i = 0; i < ObjRegion_Name.Count; i++)
                                                            {
                                                                if (ObjRegion_Name.ElementAt(i).Region.ToString() == region_value)
                                                                {
                                                                    check_item = 2;
                                                                }
                                                            }
                                                            if (check_item != 2)
                                                            {
                                                                string FirstName = string.Empty;
                                                                string LastName = string.Empty;
                                                                string MiddleName = string.Empty;
                                                                if (FName != null) { FirstName = FName.Value.ToString(); }
                                                                if (LName != null) { LastName = LName.Value.ToString(); }
                                                                if (MName != null) { MiddleName = MName.Value.ToString(); }




                                                                ObjRegion_Name.Add(new Region_Name() { Region = Region.Value.ToString(), Fname = FirstName, Lname = LastName, Mname = MiddleName });
                                                            }

                                                            var ArrayCollection = YearMonth.Split('-');
                                                            int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                            //int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                            if (TimeFrame == 0)//ticket #87
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                            else if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                        }
                                                    }
                                                }
                                                var duplicatesRemoved = ObjRegion_Name.Distinct().ToList();

                                                string firstname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                                                string lastname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;
                                                string middlename_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                List<State_Name> CollectionStateName = Objbalcompany.GetStateList(duplicatesRemoved, SearchedStateName, firstname_item, lastname_item, middlename_item);

                                                Guid pkProductIdfcr = Guid.Parse("0f0016cf-c214-44d6-9b41-cda20dee2251");
                                                List<JurisdictionColl> CollectionJudistration = Objbalcompany.GetJurisdictionList(CollectionStateName, pkProductIdfcr);




                                                if (CollectionJudistration != null && CollectionJudistration.Count > 0)
                                                {
                                                    var lst = searchedDataList1.ToList();
                                                    lst.ForEach(n => searchedDataList1.Add(new TiredSearchedData
                                                    {
                                                        county = n.county,
                                                        state = n.state,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName
                                                    }));
                                                    var searchedDataList = searchedDataList1.Select(n => new
                                                    {
                                                        county = n.county,
                                                        state = n.state,
                                                        mName = n.mName,
                                                        fName = n.fName,
                                                        lName = n.lName,
                                                        MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                    }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                    var lst_fcr = CollectionJudistration.ToList();
                                                    lst_fcr.ForEach(n => CollectionJudistration.Add(new JurisdictionColl
                                                    {
                                                        Jurisdiction_Name = n.Jurisdiction_Name,
                                                        pkJurisdictionId = n.pkJurisdictionId,
                                                        AdditionalFee = n.AdditionalFee,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName

                                                    }));
                                                    var CollectionJudistration_item = CollectionJudistration.Select(n => new
                                                    {
                                                        Jurisdiction_Name = n.Jurisdiction_Name,
                                                        pkJurisdictionId = n.pkJurisdictionId,
                                                        AdditionalFee = n.AdditionalFee,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName,
                                                        MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                    }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                    if (CollectionJudistration_item.Count > 0)
                                                    {
                                                        bool flagDisabled = false;
                                                        decimal ReportFee = GetReportFee(LocationId, CompanyUserId, "FCR", Tiered2ReportType, ObjEmergeReport.ObjTieredPackage.fkPackageId);
                                                        response.Append("<table id=\"tieredTable\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">");
                                                        response.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">Record(s) found : RecordCount" +
                                                            "</td></tr>");
                                                        response.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"100%\"><td width=\"20%\">Full Name</td><td width=\"20%\">Jurisdictions</td><td width=\"15%\">(ReportType)</td><td width=\"20%\">Cost</td> <td width=\"10%\"><input type=\"checkbox\" onclick=\"selectunselectCountyForFCR();\" id=\"chk_SelectAllCounty\"  />Select All</td><td width=\"15%\">Sub Total</td></tr>");
                                                        for (int i = 0; i < CollectionJudistration_item.Count; i++)
                                                        {
                                                            string JudistrationName = CollectionJudistration_item.ElementAt(i).Jurisdiction_Name;
                                                            var Judistrationfee = CollectionJudistration_item.ElementAt(i).AdditionalFee;
                                                            string fee = "$" + ReportFee.ToString(); ;

                                                            if (Judistrationfee == 0 || Judistrationfee == Convert.ToDecimal(0.00))
                                                            {
                                                            }
                                                            else
                                                            {
                                                                fee = fee + " <br /> Jurisdiction Fee" + " $" + Judistrationfee;
                                                            }
                                                            // string MatchMeterIcon = MatchMeterResult(searchedDataList.ElementAt(i).fName, searchedDataList.ElementAt(i).mName, searchedDataList.ElementAt(i).lName,
                                                            //                         ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName, ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial, ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName);

                                                            string MatchMeterIcon = CollectionJudistration_item.ElementAt(i).MatchingStatus == 1 ? "ico_match_green.jpg" : (CollectionJudistration_item.ElementAt(i).MatchingStatus == 2 ? "ico_red_yellow.png" : "ico_red_match.png");
                                                            decimal subTotal = ReportFee + Judistrationfee;
                                                            //string fullName = searchedDataList.ElementAt(i).mName == "" ? searchedDataList.ElementAt(i).fName : searchedDataList.ElementAt(i).fName + " " + searchedDataList.ElementAt(i).mName.Substring(0, 1);
                                                            // fullName = fullName + " " + searchedDataList.ElementAt(i).lName;

                                                            string fullName = CollectionJudistration_item.ElementAt(i).mName == "" ? CollectionJudistration_item.ElementAt(i).fName : CollectionJudistration_item.ElementAt(i).fName + " " + CollectionJudistration_item.ElementAt(i).mName.Substring(0, 1);
                                                            fullName = fullName + " " + CollectionJudistration_item.ElementAt(i).lName;




                                                            string sDispState = "";
                                                            response.Append("<tr><td><img title=\"Match Meter\" src=\"../Content/themes/base/images/" + MatchMeterIcon + "\">" + fullName + "</td>");
                                                            response.Append("<td>" + JudistrationName + "</td>");
                                                            //if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode != null)
                                                            //{
                                                            //    if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Contains("_"))
                                                            //        sDispState = "(" + ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Split('_').First() + ")";
                                                            //    else
                                                            //        sDispState = searchedDataList.ElementAt(i).state;
                                                            //}
                                                            //else
                                                            //{
                                                            //    sDispState = searchedDataList.ElementAt(i).state;
                                                            //}

                                                            // string MI = CollectionJudistration_item.ElementAt(i).mName != "" ? CollectionJudistration_item.ElementAt(i).mName : "*";
                                                            //INT-126
                                                            string MI = middlename_item != "" ? middlename_item : "*";
                                                            response.Append("<td>(FCR)</td>");
                                                            response.Append("<td>" + fee + "</td>");
                                                            if (i < searchedDataList.Count())
                                                            {

                                                                if (lstJFCR.Contains(JudistrationName))
                                                                {
                                                                    response.Append("<td>&nbsp;&nbsp;&nbsp;<span style=\"color:red;\">-</span></td>");
                                                                }
                                                                else
                                                                {
                                                                    response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk1\" type=\"checkbox\" id=\"chk_" + JudistrationName + "_County_" + firstname_item + "_" + MI + "_" + lastname_item + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                                    lstJFCR.Add(JudistrationName);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk1\" type=\"checkbox\" id=\"chk_" + JudistrationName + "_County_" + firstname_item + "_" + MI + "_" + lastname_item + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                            }
                                                            response.Append("<td><span class=\"subtotalClass\" id=\"subTotal_" + i.ToString() + "\">$" + subTotal + "</span></td>");
                                                            response.Append("</tr>");
                                                            ObjJurisdictionListColl.Add(new JurisdictionListColl() { AdditionalFee = decimal.Parse(CollectionJudistration_item.ElementAt(i).AdditionalFee.ToString()), pkJurisdictionId = int.Parse(CollectionJudistration_item.ElementAt(i).pkJurisdictionId.ToString()), Jurisdiction_Name = CollectionJudistration_item.ElementAt(i).Jurisdiction_Name.ToString(), fName = CollectionJudistration_item.ElementAt(i).fName, mName = CollectionJudistration_item.ElementAt(i).mName, lName = CollectionJudistration_item.ElementAt(i).lName });

                                                        }
                                                    }

                                                    response.Append("</table>").Replace("RecordCount", CollectionJudistration.Count.ToString());
                                                }
                                            }
                                        }
                                        #endregion

                                        response.Append("</td>");
                                        response.Append("<td style=\"border-right: 1px solid gray;vertical-align:top;\">");
                                        Tiered3ReportType = "Tiered3";

                                        if (sTiered3.Contains("CCR"))
                                        {
                                            //Changed By Himesh Kumar
                                            #region Tiered CCR1
                                            if (CreditFile != null && CreditFile.Count > 0)
                                            {

                                                List<string> lstCountyStateCCR = new List<string>();
                                                List<XElement> PersonalData = CreditFile.Descendants("record").ToList();


                                                if (PersonalData != null && PersonalData.Count > 0)
                                                {
                                                    foreach (XElement xe in PersonalData)
                                                    {
                                                        XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                        XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                        XElement Region = Address.Descendants("state").FirstOrDefault();
                                                        XElement County = Address.Descendants("county").FirstOrDefault();


                                                        XElement FName = xe.Descendants("firstname").FirstOrDefault();
                                                        XElement MName = xe.Descendants("middlename").FirstOrDefault();
                                                        XElement LName = xe.Descendants("lastname").FirstOrDefault();
                                                        XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                        if (EffectiveDate != null)
                                                        {
                                                            string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";

                                                            TiredSearchedData objTiered = new TiredSearchedData();
                                                            if (Region != null && County != null != null && LName != null && YearMonth != null)
                                                            {
                                                                var ArrayCollection = YearMonth.Split('-');
                                                                int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                                //int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                                if (TimeFrame == 0)//ticket #87
                                                                {
                                                                    objTiered.county = County.Value;
                                                                    objTiered.state = Region.Value;
                                                                    objTiered.fName = FName != null ? FName.Value : "";
                                                                    objTiered.mName = MName != null ? MName.Value : "";
                                                                    objTiered.lName = LName.Value;
                                                                    searchedDataList1.Add(objTiered);
                                                                }
                                                                else if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                                {
                                                                    objTiered.county = County.Value;
                                                                    objTiered.state = Region.Value;
                                                                    objTiered.fName = FName != null ? FName.Value : "";
                                                                    objTiered.mName = MName != null ? MName.Value : "";
                                                                    objTiered.lName = LName.Value;
                                                                    searchedDataList1.Add(objTiered);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    if (searchedDataList1 != null && searchedDataList1.Count > 0)
                                                    {
                                                        var lst = searchedDataList1.Where(n => n.county.Contains("DALLAS")).ToList();
                                                        lst.ForEach(n => searchedDataList1.Add(new TiredSearchedData
                                                        {
                                                            county = "DALLAS METRO",
                                                            state = "TX",
                                                            fName = n.fName,
                                                            mName = n.mName,
                                                            lName = n.lName
                                                        }));

                                                        var searchedDataList = searchedDataList1.Select(n => new
                                                        {
                                                            county = n.county,
                                                            state = n.state,
                                                            mName = n.mName,
                                                            fName = n.fName,
                                                            lName = n.lName,
                                                            MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                        }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                        if (searchedDataList.Count > 0)
                                                        {
                                                            bool flagDisabled = false;
                                                            decimal ReportFee = GetReportFee(LocationId, CompanyUserId, "CCR1", Tiered3ReportType, ObjEmergeReport.ObjTieredPackage.fkPackageId);
                                                            response.Append("<table id=\"tieredTable\" cellpadding=\"3\" cellspacing=\"0\" width=\"100%\">");
                                                            response.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">Record(s) found : RecordCount" +
                                                                "</td></tr>");
                                                            response.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"100%\"><td width=\"20%\">Full Name</td><td width=\"20%\">County[State]</td><td width=\"15%\">(ReportType)</td><td width=\"20%\">Cost</td> <td width=\"10%\"><input type=\"checkbox\" onclick=\"selectunselectCountyCCRTiered();\" id=\"chk_SelectAllCountyCCRTiered\"  />Select All</td><td width=\"15%\">Sub Total</td></tr>");
                                                            for (int i = 0; i < searchedDataList.Count; i++)
                                                            {
                                                                string CountyName = searchedDataList.ElementAt(i).county.ToLower().Replace("st.", "saint");
                                                                var Countyfee = Objbalcompanytype.Get_County_Fee(CountyName, searchedDataList.ElementAt(i).state, LocationId, "CCR1");
                                                                string fee = "$" + ReportFee.ToString(); ;

                                                                if (Countyfee == 0 || Countyfee == Convert.ToDecimal(0.00))
                                                                {
                                                                }
                                                                else
                                                                {
                                                                    fee = fee + " <br /> County Fee" + " $" + Countyfee;
                                                                }


                                                                string MatchMeterIcon = searchedDataList.ElementAt(i).MatchingStatus == 1 ? "ico_match_green.jpg" : (searchedDataList.ElementAt(i).MatchingStatus == 2 ? "ico_red_yellow.png" : "ico_red_match.png");
                                                                decimal subTotal = ReportFee + Countyfee;
                                                                string fullName = searchedDataList.ElementAt(i).mName == "" ? searchedDataList.ElementAt(i).fName : searchedDataList.ElementAt(i).fName + " " + searchedDataList.ElementAt(i).mName.Substring(0, 1);
                                                                fullName = fullName + " " + searchedDataList.ElementAt(i).lName;
                                                                string sDispState = "";
                                                                response.Append("<tr><td><img title=\"Match Meter\" src=\"../Content/themes/base/images/" + MatchMeterIcon + "\">" + fullName + "</td>");
                                                                response.Append("<td>" + searchedDataList.ElementAt(i).county + ", [" + searchedDataList.ElementAt(i).state + "]" +
                                                                    "</td>");
                                                                if (ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode != null)
                                                                {
                                                                    if (ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode.Contains("_"))
                                                                        sDispState = "(" + ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode.Split('_').First() + ")";
                                                                    else
                                                                        sDispState = searchedDataList.ElementAt(i).state;
                                                                }
                                                                else
                                                                {
                                                                    sDispState = searchedDataList.ElementAt(i).state;
                                                                }

                                                                //INT-126
                                                                //INT-126 See Comments on Jira
                                                                string firstname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                                                                string lastname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;
                                                                string middlename_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                                //string MI = searchedDataList.ElementAt(i).mName != "" ? searchedDataList.ElementAt(i).mName : "*";
                                                                string MI = middlename_item != "" ? middlename_item : "*";
                                                                response.Append("<td>" + sDispState + "</td>");
                                                                response.Append("<td>" + fee + "</td>");
                                                                if (i < searchedDataList.Count())
                                                                {
                                                                    if (lstCountyStateCCR.Contains(searchedDataList.ElementAt(i).county + ", [" + searchedDataList.ElementAt(i).state + "]"))
                                                                    {
                                                                        response.Append("<td>&nbsp;&nbsp;&nbsp;<span style=\"color:red;\">-</span></td>");
                                                                    }
                                                                    else
                                                                    {
                                                                        flagDisabled = false;
                                                                        response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk\" type=\"checkbox\" id=\"chk_" + CountyName + "_" + searchedDataList.ElementAt(i).state + "_County_" + firstname_item + "_" + MI + "_" + lastname_item + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                                        lstCountyStateCCR.Add(searchedDataList.ElementAt(i).county + ", [" + searchedDataList.ElementAt(i).state + "]");
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk\" type=\"checkbox\" id=\"chk_" + CountyName + "_" + searchedDataList.ElementAt(i).state + "_County_" + firstname_item + "_" + MI + "_" + lastname_item + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                                }
                                                                response.Append("<td><span class=\"subtotalClass\" id=\"subTotal_" + i.ToString() + "\">$" + subTotal + "</span></td>");
                                                                response.Append("</tr>");
                                                            }

                                                        }

                                                        response.Append("</table>").Replace("RecordCount", searchedDataList.Count.ToString());
                                                    }
                                                }
                                            }
                                            #endregion
                                        }
                                        else if (sTiered3.Contains("SCR"))
                                        {
                                            //Changed By Himesh Kumar
                                            #region Tiered SCR
                                            if (CreditFile != null && CreditFile.Count > 0)
                                            {
                                                List<XElement> PersonalData = CreditFile.Descendants("record").ToList();

                                                List<string> lstStateSCR = new List<string>();
                                                if (PersonalData != null && PersonalData.Count > 0)
                                                {
                                                    foreach (XElement xe in PersonalData)
                                                    {
                                                        XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                        XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                        XElement Region = Address.Descendants("state").FirstOrDefault();
                                                        XElement County = Address.Descendants("county").FirstOrDefault();


                                                        XElement FName = xe.Descendants("firstname").FirstOrDefault();
                                                        XElement MName = xe.Descendants("middlename").FirstOrDefault();
                                                        XElement LName = xe.Descendants("lastname").FirstOrDefault();
                                                        XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                        if (EffectiveDate != null)
                                                        {
                                                            string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";

                                                            TiredSearchedData objTiered = new TiredSearchedData();
                                                            if (Region != null && County != null != null && LName != null && YearMonth != null)
                                                            {
                                                                var ArrayCollection = YearMonth.Split('-');
                                                                int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                                //int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                                if (TimeFrame == 0)//ticket #87
                                                                {
                                                                    objTiered.county = County.Value;
                                                                    objTiered.state = Region.Value;
                                                                    objTiered.fName = FName != null ? FName.Value : "";
                                                                    objTiered.mName = MName != null ? MName.Value : "";
                                                                    objTiered.lName = LName.Value;
                                                                    searchedDataList1.Add(objTiered);
                                                                }
                                                                else if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                                {
                                                                    objTiered.county = County.Value;
                                                                    objTiered.state = Region.Value;
                                                                    objTiered.fName = FName != null ? FName.Value : "";
                                                                    objTiered.mName = MName != null ? MName.Value : "";
                                                                    objTiered.lName = LName.Value;
                                                                    searchedDataList1.Add(objTiered);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    if (searchedDataList1 != null && searchedDataList1.Count > 0)
                                                    {
                                                        var lst = searchedDataList1.Where(n => n.county.Contains("DALLAS")).ToList();
                                                        lst.ForEach(n => searchedDataList1.Add(new TiredSearchedData
                                                        {
                                                            county = "DALLAS METRO",
                                                            state = "TX",
                                                            fName = n.fName,
                                                            mName = n.mName,
                                                            lName = n.lName
                                                        }));

                                                        var searchedDataList = searchedDataList1.Select(n => new
                                                        {
                                                            county = n.county,
                                                            state = n.state,
                                                            mName = n.mName,
                                                            fName = n.fName,
                                                            lName = n.lName,
                                                            MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                        }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();

                                                        string[] StateArray = new BALGeneral().GetDisabledStates().Select(d => d.StateCode).ToArray();

                                                        searchedDataList = searchedDataList.Where(s => !StateArray.Contains(s.state)).ToList();
                                                        if (searchedDataList.Count > 0)
                                                        {
                                                            bool flagDisabled = false;
                                                            decimal ReportFee = GetReportFee(LocationId, CompanyUserId, "SCR", "Tiered", ObjEmergeReport.ObjTieredPackage.fkPackageId);
                                                            response.Append("<table id=\"tieredTable\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">");
                                                            response.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">Record(s) found : RecordCount" +
                                                                "</td></tr>");
                                                            response.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"100%\"><td width=\"20%\">Full Name</td><td width=\"20%\">State</td><td width=\"15%\">(ReportType)</td><td width=\"20%\">Cost</td> <td width=\"10%\"><input type=\"checkbox\" onclick=\"selectunselectCountySCR();\" id=\"chk_SelectAllStates\"  />Select All</td><td width=\"15%\">Sub Total</td></tr>");
                                                            for (int i = 0; i < searchedDataList.Count; i++)
                                                            {
                                                                string CountyName = searchedDataList.ElementAt(i).county.ToLower().Replace("st.", "saint");

                                                                List<tblState> lstStateCode = new BALGeneral().GetStates();
                                                                string StateNameval = lstStateCode.Where(st => st.StateCode == searchedDataList.ElementAt(i).state).Select(st => st.StateName).FirstOrDefault();
                                                                var Countyfee = 0;
                                                                int stateFees = 0;
                                                                if (!string.IsNullOrEmpty(StateNameval))
                                                                {
                                                                    string stFee = Objbalcompanytype.Get_State_Fee(StateNameval);

                                                                    int.TryParse(stFee, out stateFees);
                                                                }
                                                                Countyfee = stateFees;

                                                                string fee = "$" + ReportFee.ToString(); ;

                                                                if (Countyfee == 0 || Countyfee == Convert.ToDecimal(0.00))
                                                                {
                                                                }
                                                                else
                                                                {
                                                                    fee = fee + " <br />State Fee" + " $" + Countyfee;
                                                                }
                                                                // string MatchMeterIcon = MatchMeterResult(searchedDataList.ElementAt(i).fName, searchedDataList.ElementAt(i).mName, searchedDataList.ElementAt(i).lName,
                                                                //                         ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName, ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial, ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName);

                                                                string MatchMeterIcon = searchedDataList.ElementAt(i).MatchingStatus == 1 ? "ico_match_green.jpg" : (searchedDataList.ElementAt(i).MatchingStatus == 2 ? "ico_red_yellow.png" : "ico_red_match.png");
                                                                decimal subTotal = ReportFee + Countyfee;
                                                                string fullName = searchedDataList.ElementAt(i).mName == "" ? searchedDataList.ElementAt(i).fName : searchedDataList.ElementAt(i).fName + " " + searchedDataList.ElementAt(i).mName.Substring(0, 1);
                                                                fullName = fullName + " " + searchedDataList.ElementAt(i).lName;
                                                                if (lstStateSCR.Contains(searchedDataList.ElementAt(i).state))
                                                                {
                                                                    flagDisabled = true;
                                                                }
                                                                else
                                                                {
                                                                    flagDisabled = false;
                                                                    lstStateSCR.Add(searchedDataList.ElementAt(i).state);
                                                                }
                                                                response.Append("<tr><td><img title=\"Match Meter\" src=\"../Content/themes/base/images/" + MatchMeterIcon + "\">" + fullName + "</td>");

                                                                string sDispState = "";

                                                                response.Append("<td>" + searchedDataList.ElementAt(i).state + "</td>");

                                                                sDispState = "(SCR)";
                                                                //INT-126 See Comments on Jira
                                                                string firstname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                                                                string lastname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;
                                                                string middlename_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                                string MI = middlename_item != "" ? middlename_item : "*";

                                                                //string MI = searchedDataList.ElementAt(i).mName != "" ? searchedDataList.ElementAt(i).mName : "*";
                                                                response.Append("<td>" + sDispState + "</td>");
                                                                response.Append("<td>" + fee + "</td>");
                                                                //Ticket#179
                                                                if (flagDisabled)
                                                                {
                                                                    response.Append("<td>&nbsp;&nbsp;&nbsp;<span style=\"color:red;\">-</span></td>");
                                                                }
                                                                else
                                                                {
                                                                    response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk\" type=\"checkbox\" id=\"chk_" + CountyName + "_" + searchedDataList.ElementAt(i).state + "_County_" + firstname_item + "_" + MI + "_" + lastname_item + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                                }

                                                                // subTotal = 10;//Just for testing
                                                                response.Append("<td><span class=\"subtotalClass\" id=\"subTotal_" + i.ToString() + "\">$" + subTotal + "</span></td>");
                                                                response.Append("</tr>");
                                                            }

                                                        }
                                                        if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode == null)
                                                        {
                                                            response.Append("</table>").Replace("RecordCount", searchedDataList.Count.ToString());
                                                        }

                                                    }
                                                }
                                            }
                                            #endregion
                                        }

                                        response.Append("</td>");
                                        response.Append("</tr>");
                                        response.Append("</table>");


                                    }
                                    else
                                    {
                                        //Changed By Himesh Kumar
                                        Tiered2ReportType = "Tiered2";

                                        response.Append("<table style=\"width:100%;\">");
                                        response.Append("<tr>");
                                        response.Append("<td style=\"width:50%;border-left: 1px solid gray;border-right: 1px solid gray;\">");

                                        #region Tiered FCR
                                        Tiered3ReportType = "Tiered3";

                                        if (Credit_File != null && Credit_File.Count > 0)
                                        {
                                            List<string> lstJFCRT = new List<string>();
                                            List<XElement> Personal_Data = Credit_File.Descendants("record").ToList();

                                            List<Region_Name> ObjRegion_Name = new List<Region_Name>();
                                            if (Personal_Data != null && Personal_Data.Count > 0)
                                            {
                                                foreach (XElement xe in Personal_Data)
                                                {
                                                    XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                    XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                    XElement Region = Address.Descendants("state").FirstOrDefault();
                                                    XElement County = Address.Descendants("county").FirstOrDefault();


                                                    XElement FName = xe.Descendants("firstname").FirstOrDefault();
                                                    XElement MName = xe.Descendants("middlename").FirstOrDefault();
                                                    XElement LName = xe.Descendants("lastname").FirstOrDefault();
                                                    XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                    if (EffectiveDate != null)
                                                    {
                                                        string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";
                                                        TiredSearchedData objTiered = new TiredSearchedData();
                                                        if (Region != null && County != null != null && LName != null && YearMonth != null)
                                                        {
                                                            var region_value = Region.Value.ToString();
                                                            int check_item = 1;
                                                            for (int i = 0; i < ObjRegion_Name.Count; i++)
                                                            {
                                                                if (ObjRegion_Name.ElementAt(i).Region.ToString() == region_value)
                                                                {
                                                                    check_item = 2;
                                                                }
                                                            }
                                                            if (check_item != 2)
                                                            {
                                                                string FirstName = string.Empty;
                                                                string LastName = string.Empty;
                                                                string MiddleName = string.Empty;
                                                                if (FName != null) { FirstName = FName.Value.ToString(); }
                                                                if (LName != null) { LastName = LName.Value.ToString(); }
                                                                if (MName != null) { MiddleName = MName.Value.ToString(); }




                                                                ObjRegion_Name.Add(new Region_Name() { Region = Region.Value.ToString(), Fname = FirstName, Lname = LastName, Mname = MiddleName });
                                                            }

                                                            var ArrayCollection = YearMonth.Split('-');
                                                            int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                            //int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                            if (TimeFrame == 0)//ticket #87
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                            else if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                        }
                                                    }
                                                }
                                                var duplicatesRemoved = ObjRegion_Name.Distinct().ToList();

                                                string firstname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                                                string lastname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;
                                                string middlename_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                List<State_Name> CollectionStateName = Objbalcompany.GetStateList(duplicatesRemoved, SearchedStateName, firstname_item, lastname_item, middlename_item);

                                                Guid pkProductIdfcr = Guid.Parse("0f0016cf-c214-44d6-9b41-cda20dee2251");
                                                List<JurisdictionColl> CollectionJudistration = Objbalcompany.GetJurisdictionList(CollectionStateName, pkProductIdfcr);




                                                if (CollectionJudistration != null && CollectionJudistration.Count > 0)
                                                {
                                                    var lst = searchedDataList1.ToList();
                                                    lst.ForEach(n => searchedDataList1.Add(new TiredSearchedData
                                                    {
                                                        county = n.county,
                                                        state = n.state,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName
                                                    }));
                                                    var searchedDataList = searchedDataList1.Select(n => new
                                                    {
                                                        county = n.county,
                                                        state = n.state,
                                                        mName = n.mName,
                                                        fName = n.fName,
                                                        lName = n.lName,
                                                        MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                    }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                    var lst_fcr = CollectionJudistration.ToList();
                                                    lst_fcr.ForEach(n => CollectionJudistration.Add(new JurisdictionColl
                                                    {
                                                        Jurisdiction_Name = n.Jurisdiction_Name,
                                                        pkJurisdictionId = n.pkJurisdictionId,
                                                        AdditionalFee = n.AdditionalFee,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName

                                                    }));
                                                    var CollectionJudistration_item = CollectionJudistration.Select(n => new
                                                    {
                                                        Jurisdiction_Name = n.Jurisdiction_Name,
                                                        pkJurisdictionId = n.pkJurisdictionId,
                                                        AdditionalFee = n.AdditionalFee,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName,
                                                        MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                    }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                    if (CollectionJudistration_item.Count > 0)
                                                    {
                                                        bool flagDisabled = false;
                                                        decimal ReportFee = GetReportFee(LocationId, CompanyUserId, "FCR", Tiered3ReportType, ObjEmergeReport.ObjTieredPackage.fkPackageId);
                                                        response.Append("<table id=\"tieredTable\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">");
                                                        response.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">Record(s) found : RecordCount" +
                                                            "</td></tr>");
                                                        response.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"100%\"><td width=\"20%\">Full Name</td><td width=\"20%\">Jurisdictions</td><td width=\"15%\">(ReportType)</td><td width=\"20%\">Cost</td> <td width=\"10%\"><input type=\"checkbox\" onclick=\"selectunselectCountyForFCR();\" id=\"chk_SelectAllCounty\"  />Select All</td><td width=\"15%\">Sub Total</td></tr>");
                                                        for (int i = 0; i < CollectionJudistration_item.Count; i++)
                                                        {
                                                            string JudistrationName = CollectionJudistration_item.ElementAt(i).Jurisdiction_Name;
                                                            var Judistrationfee = CollectionJudistration_item.ElementAt(i).AdditionalFee;
                                                            string fee = "$" + ReportFee.ToString(); ;

                                                            if (Judistrationfee == 0 || Judistrationfee == Convert.ToDecimal(0.00))
                                                            {
                                                            }
                                                            else
                                                            {
                                                                fee = fee + " <br /> Jurisdiction Fee" + " $" + Judistrationfee;
                                                            }
                                                            // string MatchMeterIcon = MatchMeterResult(searchedDataList.ElementAt(i).fName, searchedDataList.ElementAt(i).mName, searchedDataList.ElementAt(i).lName,
                                                            //                         ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName, ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial, ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName);

                                                            string MatchMeterIcon = CollectionJudistration_item.ElementAt(i).MatchingStatus == 1 ? "ico_match_green.jpg" : (CollectionJudistration_item.ElementAt(i).MatchingStatus == 2 ? "ico_red_yellow.png" : "ico_red_match.png");
                                                            decimal subTotal = ReportFee + Judistrationfee;
                                                            //string fullName = searchedDataList.ElementAt(i).mName == "" ? searchedDataList.ElementAt(i).fName : searchedDataList.ElementAt(i).fName + " " + searchedDataList.ElementAt(i).mName.Substring(0, 1);
                                                            // fullName = fullName + " " + searchedDataList.ElementAt(i).lName;

                                                            string fullName = CollectionJudistration_item.ElementAt(i).mName == "" ? CollectionJudistration_item.ElementAt(i).fName : CollectionJudistration_item.ElementAt(i).fName + " " + CollectionJudistration_item.ElementAt(i).mName.Substring(0, 1);
                                                            fullName = fullName + " " + CollectionJudistration_item.ElementAt(i).lName;




                                                            string sDispState = "";
                                                            response.Append("<tr><td><img title=\"Match Meter\" src=\"../Content/themes/base/images/" + MatchMeterIcon + "\">" + fullName + "</td>");
                                                            response.Append("<td>" + JudistrationName + "</td>");
                                                            if (ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode != null)
                                                            {
                                                                if (ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode.Contains("_"))
                                                                    sDispState = "(" + ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode.Split('_').First() + ")";
                                                                else
                                                                    sDispState = searchedDataList.ElementAt(i).state;
                                                            }
                                                            else
                                                            {
                                                                sDispState = searchedDataList.ElementAt(i).state;
                                                            }
                                                            //string MI = CollectionJudistration_item.ElementAt(i).mName != "" ? CollectionJudistration_item.ElementAt(i).mName : "*";
                                                            //INT-126 See Comments on Jira
                                                            //string MI = searchedDataList.ElementAt(i).mName != "" ? searchedDataList.ElementAt(i).mName : "*";
                                                            string MI = middlename_item != "" ? middlename_item : "*";
                                                            response.Append("<td>" + sDispState + "</td>");
                                                            response.Append("<td>" + fee + "</td>");
                                                            if (i < searchedDataList.Count())
                                                            {

                                                                if (lstJFCRT.Contains(JudistrationName))
                                                                {
                                                                    response.Append("<td>&nbsp;&nbsp;&nbsp;<span style=\"color:red;\">-</span></td>");
                                                                }
                                                                else
                                                                {
                                                                    response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk1\" type=\"checkbox\" id=\"chk_" + JudistrationName + "_County_" + firstname_item + "_" + MI + "_" + lastname_item + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                                    lstJFCRT.Add(JudistrationName);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk1\" type=\"checkbox\" id=\"chk_" + JudistrationName + "_County_" + firstname_item + "_" + MI + "_" + lastname_item + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                            }
                                                            response.Append("<td><span class=\"subtotalClass\" id=\"subTotal_" + i.ToString() + "\">$" + subTotal + "</span></td>");
                                                            response.Append("</tr>");
                                                            ObjJurisdictionListColl.Add(new JurisdictionListColl() { AdditionalFee = decimal.Parse(CollectionJudistration_item.ElementAt(i).AdditionalFee.ToString()), pkJurisdictionId = int.Parse(CollectionJudistration_item.ElementAt(i).pkJurisdictionId.ToString()), Jurisdiction_Name = CollectionJudistration_item.ElementAt(i).Jurisdiction_Name.ToString(), fName = CollectionJudistration_item.ElementAt(i).fName, mName = CollectionJudistration_item.ElementAt(i).mName, lName = CollectionJudistration_item.ElementAt(i).lName });

                                                        }
                                                    }

                                                    response.Append("</table>").Replace("RecordCount", CollectionJudistration.Count.ToString());
                                                }
                                            }
                                        }

                                        #endregion

                                        response.Append("</td>");
                                        response.Append("<td style=\"border-right: 1px solid gray;vertical-align:top;\">");

                                        //Changed By Himesh Kumar
                                        #region Tiered CCR1


                                        if (CreditFile != null && CreditFile.Count > 0)
                                        {
                                            List<XElement> PersonalData = CreditFile.Descendants("record").ToList();
                                            List<string> lstCountyState = new List<string>();
                                            if (PersonalData != null && PersonalData.Count > 0)
                                            {
                                                foreach (XElement xe in PersonalData)
                                                {
                                                    XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                    XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                    XElement Region = Address.Descendants("state").FirstOrDefault();
                                                    XElement County = Address.Descendants("county").FirstOrDefault();


                                                    XElement FName = xe.Descendants("firstname").FirstOrDefault();
                                                    XElement MName = xe.Descendants("middlename").FirstOrDefault();
                                                    XElement LName = xe.Descendants("lastname").FirstOrDefault();
                                                    XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                    if (EffectiveDate != null)
                                                    {
                                                        string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";

                                                        TiredSearchedData objTiered = new TiredSearchedData();
                                                        if (Region != null && County != null != null && LName != null && YearMonth != null)
                                                        {
                                                            var ArrayCollection = YearMonth.Split('-');
                                                            int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                            //int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                            if (TimeFrame == 0)//ticket #87
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                            else if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                        }
                                                    }
                                                }
                                                if (searchedDataList1 != null && searchedDataList1.Count > 0)
                                                {
                                                    var lst = searchedDataList1.Where(n => n.county.Contains("DALLAS")).ToList();
                                                    lst.ForEach(n => searchedDataList1.Add(new TiredSearchedData
                                                    {
                                                        county = "DALLAS METRO",
                                                        state = "TX",
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName
                                                    }));

                                                    var searchedDataList = searchedDataList1.Select(n => new
                                                    {
                                                        county = n.county,
                                                        state = n.state,
                                                        mName = n.mName,
                                                        fName = n.fName,
                                                        lName = n.lName,
                                                        MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                    }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                    if (searchedDataList.Count > 0)
                                                    {
                                                        bool flagDisabled = false;
                                                        decimal ReportFee = GetReportFee(LocationId, CompanyUserId, "CCR1", Tiered2ReportType, ObjEmergeReport.ObjTieredPackage.fkPackageId);
                                                        response.Append("<table id=\"tieredTableCCr\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">");
                                                        response.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">Record(s) found : RecordCount" +
                                                            "</td></tr>");
                                                        response.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"100%\"><td width=\"20%\">Full Name</td><td width=\"20%\">County[State]</td><td width=\"15%\">(ReportType)</td><td width=\"20%\">Cost</td> <td width=\"10%\"><input type=\"checkbox\" onclick=\"selectunselectCountyCCR();\" id=\"chk_SelectAllCountyCCR1\"  />Select All</td><td width=\"15%\">Sub Total</td></tr>");
                                                        for (int i = 0; i < searchedDataList.Count; i++)
                                                        {
                                                            string CountyName = searchedDataList.ElementAt(i).county.ToLower().Replace("st.", "saint");
                                                            var Countyfee = Objbalcompanytype.Get_County_Fee(CountyName, searchedDataList.ElementAt(i).state, LocationId, "CCR1");
                                                            string fee = "$" + ReportFee.ToString(); ;

                                                            if (Countyfee == 0 || Countyfee == Convert.ToDecimal(0.00))
                                                            {
                                                            }
                                                            else
                                                            {
                                                                fee = fee + " <br /> County Fee" + " $" + Countyfee;
                                                            }


                                                            string MatchMeterIcon = searchedDataList.ElementAt(i).MatchingStatus == 1 ? "ico_match_green.jpg" : (searchedDataList.ElementAt(i).MatchingStatus == 2 ? "ico_red_yellow.png" : "ico_red_match.png");
                                                            decimal subTotal = ReportFee + Countyfee;
                                                            string fullName = searchedDataList.ElementAt(i).mName == "" ? searchedDataList.ElementAt(i).fName : searchedDataList.ElementAt(i).fName + " " + searchedDataList.ElementAt(i).mName.Substring(0, 1);
                                                            fullName = fullName + " " + searchedDataList.ElementAt(i).lName;
                                                            string sDispState = "";
                                                            response.Append("<tr><td><img title=\"Match Meter\" src=\"../Content/themes/base/images/" + MatchMeterIcon + "\">" + fullName + "</td>");
                                                            response.Append("<td>" + searchedDataList.ElementAt(i).county + ", [" + searchedDataList.ElementAt(i).state + "]" +
                                                                "</td>");
                                                            if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode != null)
                                                            {
                                                                if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Contains("_"))
                                                                    sDispState = "(" + ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Split('_').First() + ")";
                                                                else
                                                                    sDispState = searchedDataList.ElementAt(i).state;
                                                            }
                                                            else
                                                            {
                                                                sDispState = searchedDataList.ElementAt(i).state;
                                                            }

                                                            //INT-126 See Comments on Jira
                                                            string firstname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                                                            string lastname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;
                                                            string middlename_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                            string MI = middlename_item != "" ? middlename_item : "*";

                                                            //string MI = searchedDataList.ElementAt(i).mName != "" ? searchedDataList.ElementAt(i).mName : "*";
                                                            response.Append("<td>" + sDispState + "</td>");
                                                            response.Append("<td>" + fee + "</td>");

                                                            if (i < searchedDataList.Count())
                                                            {
                                                                if (lstCountyState.Contains(searchedDataList.ElementAt(i).county + ", [" + searchedDataList.ElementAt(i).state + "]"))
                                                                {
                                                                    response.Append("<td>&nbsp;&nbsp;&nbsp;<span style=\"color:red;\">-</span></td>");
                                                                }
                                                                else
                                                                {
                                                                    response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk\" type=\"checkbox\" id=\"chk_" + CountyName + "_" + searchedDataList.ElementAt(i).state + "_County_" + firstname_item + "_" + MI + "_" + lastname_item + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                                    lstCountyState.Add(searchedDataList.ElementAt(i).county + ", [" + searchedDataList.ElementAt(i).state + "]");
                                                                }

                                                            }
                                                            else
                                                            {
                                                                response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk\" type=\"checkbox\" id=\"chk_" + CountyName + "_" + searchedDataList.ElementAt(i).state + "_County_" + firstname_item + "_" + MI + "_" + lastname_item + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                            }
                                                            response.Append("<td><span class=\"subtotalClass\" id=\"subTotalCCR_" + i.ToString() + "\">$" + subTotal + "</span></td>");
                                                            response.Append("</tr>");
                                                        }

                                                    }

                                                    response.Append("</table>").Replace("RecordCount", searchedDataList.Count.ToString());
                                                }
                                            }
                                        }
                                        #endregion

                                        response.Append("</td>");
                                        response.Append("</tr>");
                                        response.Append("</table>");
                                    }

                                    #endregion

                                }
                                else
                                {
                                    if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Contains("FCR"))
                                    {
                                        //Changed By Himesh Kumar
                                        #region Tiered FCR

                                        if (Credit_File != null && Credit_File.Count > 0)
                                        {
                                            List<XElement> Personal_Data = Credit_File.Descendants("record").ToList();
                                            List<string> lstJT2 = new List<string>();
                                            List<Region_Name> ObjRegion_Name = new List<Region_Name>();
                                            if (Personal_Data != null && Personal_Data.Count > 0)
                                            {
                                                foreach (XElement xe in Personal_Data)
                                                {
                                                    XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                    XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                    XElement Region = Address.Descendants("state").FirstOrDefault();
                                                    XElement County = Address.Descendants("county").FirstOrDefault();


                                                    XElement FName = xe.Descendants("firstname").FirstOrDefault();
                                                    XElement MName = xe.Descendants("middlename").FirstOrDefault();
                                                    XElement LName = xe.Descendants("lastname").FirstOrDefault();
                                                    XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                    if (EffectiveDate != null)
                                                    {
                                                        string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";
                                                        TiredSearchedData objTiered = new TiredSearchedData();
                                                        if (Region != null && County != null != null && LName != null && YearMonth != null)
                                                        {
                                                            var region_value = Region.Value.ToString();
                                                            int check_item = 1;
                                                            for (int i = 0; i < ObjRegion_Name.Count; i++)
                                                            {
                                                                if (ObjRegion_Name.ElementAt(i).Region.ToString() == region_value)
                                                                {
                                                                    check_item = 2;
                                                                }
                                                            }
                                                            if (check_item != 2)
                                                            {
                                                                string FirstName = string.Empty;
                                                                string LastName = string.Empty;
                                                                string MiddleName = string.Empty;
                                                                if (FName != null) { FirstName = FName.Value.ToString(); }
                                                                if (LName != null) { LastName = LName.Value.ToString(); }
                                                                if (MName != null) { MiddleName = MName.Value.ToString(); }




                                                                ObjRegion_Name.Add(new Region_Name() { Region = Region.Value.ToString(), Fname = FirstName, Lname = LastName, Mname = MiddleName });
                                                            }

                                                            var ArrayCollection = YearMonth.Split('-');
                                                            int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                            // int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                            if (TimeFrame == 0)//ticket #87
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                            else if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                        }
                                                    }
                                                }
                                                var duplicatesRemoved = ObjRegion_Name.Distinct().ToList();

                                                string firstname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                                                string lastname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;
                                                string middlename_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                List<State_Name> CollectionStateName = Objbalcompany.GetStateList(duplicatesRemoved, SearchedStateName, firstname_item, lastname_item, middlename_item);

                                                Guid pkProductIdfcr = Guid.Parse("0f0016cf-c214-44d6-9b41-cda20dee2251");
                                                List<JurisdictionColl> CollectionJudistration = Objbalcompany.GetJurisdictionList(CollectionStateName, pkProductIdfcr);




                                                if (CollectionJudistration != null && CollectionJudistration.Count > 0)
                                                {
                                                    var lst = searchedDataList1.ToList();
                                                    lst.ForEach(n => searchedDataList1.Add(new TiredSearchedData
                                                    {
                                                        county = n.county,
                                                        state = n.state,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName
                                                    }));
                                                    var searchedDataList = searchedDataList1.Select(n => new
                                                    {
                                                        county = n.county,
                                                        state = n.state,
                                                        mName = n.mName,
                                                        fName = n.fName,
                                                        lName = n.lName,
                                                        MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                    }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                    var lst_fcr = CollectionJudistration.ToList();
                                                    lst_fcr.ForEach(n => CollectionJudistration.Add(new JurisdictionColl
                                                    {
                                                        Jurisdiction_Name = n.Jurisdiction_Name,
                                                        pkJurisdictionId = n.pkJurisdictionId,
                                                        AdditionalFee = n.AdditionalFee,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName

                                                    }));
                                                    var CollectionJudistration_item = CollectionJudistration.Select(n => new
                                                    {
                                                        Jurisdiction_Name = n.Jurisdiction_Name,
                                                        pkJurisdictionId = n.pkJurisdictionId,
                                                        AdditionalFee = n.AdditionalFee,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName,
                                                        MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                    }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                    if (CollectionJudistration_item.Count > 0)
                                                    {
                                                        bool flagDisabled = false;
                                                        decimal ReportFee = GetReportFee(LocationId, CompanyUserId, "FCR", "Tiered", ObjEmergeReport.ObjTieredPackage.fkPackageId);
                                                        response.Append("<table id=\"tieredTable\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">");
                                                        response.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">Record(s) found : RecordCount" +
                                                            "</td></tr>");
                                                        response.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"100%\"><td width=\"20%\">Full Name</td><td width=\"20%\">Jurisdictions</td><td width=\"15%\">(ReportType)</td><td width=\"20%\">Cost</td> <td width=\"10%\"><input type=\"checkbox\" onclick=\"selectunselectCountyForFCR();\" id=\"chk_SelectAllCounty\"  />Select All</td><td width=\"15%\">Sub Total</td></tr>");
                                                        for (int i = 0; i < CollectionJudistration_item.Count; i++)
                                                        {
                                                            string JudistrationName = CollectionJudistration_item.ElementAt(i).Jurisdiction_Name;
                                                            var Judistrationfee = CollectionJudistration_item.ElementAt(i).AdditionalFee;
                                                            string fee = "$" + ReportFee.ToString(); ;

                                                            if (Judistrationfee == 0 || Judistrationfee == Convert.ToDecimal(0.00))
                                                            {
                                                            }
                                                            else
                                                            {
                                                                fee = fee + " <br /> Jurisdiction Fee" + " $" + Judistrationfee;
                                                            }
                                                            // string MatchMeterIcon = MatchMeterResult(searchedDataList.ElementAt(i).fName, searchedDataList.ElementAt(i).mName, searchedDataList.ElementAt(i).lName,
                                                            //                         ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName, ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial, ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName);

                                                            string MatchMeterIcon = CollectionJudistration_item.ElementAt(i).MatchingStatus == 1 ? "ico_match_green.jpg" : (CollectionJudistration_item.ElementAt(i).MatchingStatus == 2 ? "ico_red_yellow.png" : "ico_red_match.png");
                                                            decimal subTotal = ReportFee + Judistrationfee;
                                                            string fullName = CollectionJudistration_item.ElementAt(i).mName == "" ? CollectionJudistration_item.ElementAt(i).fName : CollectionJudistration_item.ElementAt(i).fName + " " + CollectionJudistration_item.ElementAt(i).mName.Substring(0, 1);
                                                            fullName = fullName + " " + CollectionJudistration_item.ElementAt(i).lName;
                                                            response.Append("<tr><td><img title=\"Match Meter\" src=\"../Content/themes/base/images/" + MatchMeterIcon + "\">" + fullName + "</td>");
                                                            response.Append("<td>" + JudistrationName + "</td>");
                                                            //INT-126 See Comments on Jira
                                                            string MI = middlename_item != "" ? middlename_item : "*";
                                                            // string MI = CollectionJudistration_item.ElementAt(i).mName != "" ? CollectionJudistration_item.ElementAt(i).mName : "*";
                                                            response.Append("<td>(FCR)</td>");
                                                            response.Append("<td>" + fee + "</td>");
                                                            if (i < searchedDataList.Count())
                                                            {
                                                                //if (lstJT2.Contains(searchedDataList.ElementAt(i).county + ", [" + searchedDataList.ElementAt(i).state + "]"))
                                                                if (lstJT2.Contains(JudistrationName))
                                                                {
                                                                    response.Append("<td>&nbsp;&nbsp;&nbsp;<span style=\"color:red;\">-</span></td>");
                                                                }
                                                                else
                                                                {
                                                                    response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk1\" type=\"checkbox\" id=\"chk_" + JudistrationName + "_County_" + firstname_item + "_" + MI + "_" + lastname_item + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                                    // lstJT2.Add(searchedDataList.ElementAt(i).county + ", [" + searchedDataList.ElementAt(i).state + "]");
                                                                    lstJT2.Add(JudistrationName);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk1\" type=\"checkbox\" id=\"chk_" + JudistrationName + "_County_" + firstname_item + "_" + MI + "_" + lastname_item + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                            }

                                                            response.Append("<td><span class=\"subtotalClass\" id=\"subTotal_" + i.ToString() + "\">$" + subTotal + "</span></td>");
                                                            response.Append("</tr>");
                                                            ObjJurisdictionListColl.Add(new JurisdictionListColl() { AdditionalFee = decimal.Parse(CollectionJudistration_item.ElementAt(i).AdditionalFee.ToString()), pkJurisdictionId = int.Parse(CollectionJudistration_item.ElementAt(i).pkJurisdictionId.ToString()), Jurisdiction_Name = CollectionJudistration_item.ElementAt(i).Jurisdiction_Name.ToString(), fName = CollectionJudistration_item.ElementAt(i).fName, mName = CollectionJudistration_item.ElementAt(i).mName, lName = CollectionJudistration_item.ElementAt(i).lName });
                                                        }
                                                    }

                                                    response.Append("</table>").Replace("RecordCount", CollectionJudistration.Count.ToString());
                                                }
                                            }
                                        }

                                        #endregion

                                    }
                                    else if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Contains("CCR1"))
                                    {
                                        //Changed By Himesh Kumar
                                        #region Tiered CCR1



                                        if (CreditFile != null && CreditFile.Count > 0)
                                        {
                                            List<XElement> PersonalData = CreditFile.Descendants("record").ToList();
                                            List<string> lstCountyCCR = new List<string>();

                                            if (PersonalData != null && PersonalData.Count > 0)
                                            {
                                                foreach (XElement xe in PersonalData)
                                                {
                                                    XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                    XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                    XElement Region = Address.Descendants("state").FirstOrDefault();
                                                    XElement County = Address.Descendants("county").FirstOrDefault();


                                                    XElement FName = xe.Descendants("firstname").FirstOrDefault();
                                                    XElement MName = xe.Descendants("middlename").FirstOrDefault();
                                                    XElement LName = xe.Descendants("lastname").FirstOrDefault();
                                                    XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                    if (EffectiveDate != null)
                                                    {
                                                        string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";

                                                        TiredSearchedData objTiered = new TiredSearchedData();
                                                        if (Region != null && County != null && LName != null && YearMonth != null)
                                                        {
                                                            var ArrayCollection = YearMonth.Split('-');
                                                            int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                            // int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                            if (TimeFrame == 0)//ticket #87
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                            else if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                        }
                                                    }
                                                }
                                                if (searchedDataList1 != null && searchedDataList1.Count > 0)
                                                {
                                                    var lst = searchedDataList1.Where(n => n.county.Contains("DALLAS")).ToList();
                                                    lst.ForEach(n => searchedDataList1.Add(new TiredSearchedData
                                                    {
                                                        county = "DALLAS METRO",
                                                        state = "TX",
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName
                                                    }));

                                                    var searchedDataList = searchedDataList1.Select(n => new
                                                    {
                                                        county = n.county,
                                                        state = n.state,
                                                        mName = n.mName,
                                                        fName = n.fName,
                                                        lName = n.lName,
                                                        MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                    }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                    if (searchedDataList.Count > 0)
                                                    {
                                                        bool flagDisabled = false;
                                                        decimal ReportFee = GetReportFee(LocationId, CompanyUserId, "CCR1", "Tiered", ObjEmergeReport.ObjTieredPackage.fkPackageId);
                                                        response.Append("<table id=\"tieredTable\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">");
                                                        response.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">Record(s) found : RecordCount" +
                                                            "</td></tr>");
                                                        response.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"100%\"><td width=\"20%\">Full Name</td><td width=\"20%\">County[State]</td><td width=\"15%\">(ReportType)</td><td width=\"20%\">Cost</td> <td width=\"10%\"><input type=\"checkbox\" onclick=\"selectunselectCounty();\" id=\"chk_SelectAllCounty\"  />Select All</td><td width=\"15%\">Sub Total</td></tr>");
                                                        for (int i = 0; i < searchedDataList.Count; i++)
                                                        {
                                                            string CountyName = searchedDataList.ElementAt(i).county.ToLower().Replace("st.", "saint");
                                                            var Countyfee = Objbalcompanytype.Get_County_Fee(CountyName, searchedDataList.ElementAt(i).state, LocationId, "CCR1");
                                                            string fee = "$" + ReportFee.ToString(); ;

                                                            if (Countyfee == 0 || Countyfee == Convert.ToDecimal(0.00))
                                                            {
                                                            }
                                                            else
                                                            {
                                                                fee = fee + " <br /> County Fee" + " $" + Countyfee;
                                                            }
                                                            // string MatchMeterIcon = MatchMeterResult(searchedDataList.ElementAt(i).fName, searchedDataList.ElementAt(i).mName, searchedDataList.ElementAt(i).lName,
                                                            //                         ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName, ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial, ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName);

                                                            string MatchMeterIcon = searchedDataList.ElementAt(i).MatchingStatus == 1 ? "ico_match_green.jpg" : (searchedDataList.ElementAt(i).MatchingStatus == 2 ? "ico_red_yellow.png" : "ico_red_match.png");
                                                            decimal subTotal = ReportFee + Countyfee;
                                                            string fullName = searchedDataList.ElementAt(i).mName == "" ? searchedDataList.ElementAt(i).fName : searchedDataList.ElementAt(i).fName + " " + searchedDataList.ElementAt(i).mName.Substring(0, 1);
                                                            fullName = fullName + " " + searchedDataList.ElementAt(i).lName;

                                                            //Ticket#179
                                                            if (lstCountyCCR.Contains(searchedDataList.ElementAt(i).county + ", [" + searchedDataList.ElementAt(i).state + "]"))
                                                            {
                                                                flagDisabled = true;
                                                            }
                                                            else
                                                            {
                                                                flagDisabled = false;
                                                                lstCountyCCR.Add(searchedDataList.ElementAt(i).county + ", [" + searchedDataList.ElementAt(i).state + "]");
                                                            }
                                                            response.Append("<tr><td><img title=\"Match Meter\" src=\"../Content/themes/base/images/" + MatchMeterIcon + "\">" + fullName + "</td>");

                                                            string sDispState = "";

                                                            response.Append("<td>" + searchedDataList.ElementAt(i).county + ", [" + searchedDataList.ElementAt(i).state + "]" +
                                                                "</td>");
                                                            if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode != null)
                                                            {
                                                                if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Contains("_"))
                                                                    sDispState = "(" + ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Split('_').First() + ")";
                                                                else
                                                                    sDispState = searchedDataList.ElementAt(i).state;
                                                            }
                                                            else
                                                            {
                                                                sDispState = searchedDataList.ElementAt(i).state;
                                                            }

                                                            //INT-126 See Comments on Jira
                                                            string firstname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                                                            string lastname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;
                                                            string middlename_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                            string MI = middlename_item != "" ? middlename_item : "*";

                                                            //string MI = searchedDataList.ElementAt(i).mName != "" ? searchedDataList.ElementAt(i).mName : "*";
                                                            response.Append("<td>" + sDispState + "</td>");
                                                            response.Append("<td>" + fee + "</td>");
                                                            //Ticket#179
                                                            if (flagDisabled)
                                                            {
                                                                response.Append("<td>&nbsp;&nbsp;&nbsp;<span style=\"color:red;\">-</span></td>");
                                                            }
                                                            else
                                                            {
                                                                response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk\" type=\"checkbox\" id=\"chk_" + CountyName + "_" + searchedDataList.ElementAt(i).state + "_County_" + firstname_item + "_" + MI + "_" + lastname_item + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                            }
                                                            response.Append("<td><span class=\"subtotalClass\" id=\"subTotal_" + i.ToString() + "\">$" + subTotal + "</span></td>");
                                                            response.Append("</tr>");
                                                        }

                                                    }

                                                    response.Append("</table>").Replace("RecordCount", searchedDataList.Count.ToString());
                                                }
                                            }
                                        }

                                        #region Tiered FCR

                                        if (Credit_File != null && Credit_File.Count > 0)
                                        {
                                            List<XElement> Personal_Data = Credit_File.Descendants("PersonalData").ToList();

                                            List<Region_Name> ObjRegion_Name = new List<Region_Name>();
                                            if (Personal_Data != null && Personal_Data.Count > 0)
                                            {
                                                foreach (XElement xe in Personal_Data)
                                                {
                                                    XElement Region = xe.Descendants("Region").FirstOrDefault();
                                                    XElement County = xe.Descendants("County").FirstOrDefault();
                                                    XElement PersonName = xe.Descendants("PersonName").FirstOrDefault();
                                                    XElement FName = PersonName.Descendants("GivenName").FirstOrDefault();
                                                    XElement MName = PersonName.Descendants("MiddleName").FirstOrDefault();
                                                    XElement LName = PersonName.Descendants("FamilyName").FirstOrDefault();
                                                    XElement EffectiveDate = xe.Descendants("EffectiveDate").FirstOrDefault();
                                                    if (EffectiveDate != null)
                                                    {
                                                        XElement EndDate = EffectiveDate.Descendants("EndDate").FirstOrDefault();
                                                        XElement YearMonth = EndDate.Descendants("YearMonth").FirstOrDefault();
                                                        TiredSearchedData objTiered = new TiredSearchedData();
                                                        if (Region != null && County != null && PersonName != null && LName != null && YearMonth != null)
                                                        {
                                                            var region_value = Region.Value.ToString();
                                                            int check_item = 1;
                                                            for (int i = 0; i < ObjRegion_Name.Count; i++)
                                                            {
                                                                if (ObjRegion_Name.ElementAt(i).Region.ToString() == region_value)
                                                                {
                                                                    check_item = 2;
                                                                }
                                                            }
                                                            if (check_item != 2)
                                                            {
                                                                string FirstName = string.Empty;
                                                                string LastName = string.Empty;
                                                                string MiddleName = string.Empty;
                                                                if (FName != null) { FirstName = FName.Value.ToString(); }
                                                                if (LName != null) { LastName = LName.Value.ToString(); }
                                                                if (MName != null) { MiddleName = MName.Value.ToString(); }




                                                                ObjRegion_Name.Add(new Region_Name() { Region = Region.Value.ToString(), Fname = FirstName, Lname = LastName, Mname = MiddleName });
                                                            }

                                                            var ArrayCollection = YearMonth.Value.Split('-');
                                                            int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                            //int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                            if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                        }
                                                    }
                                                }
                                                var duplicatesRemoved = ObjRegion_Name.Distinct().ToList();
                                                string firstname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                                                string lastname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;
                                                string middlename_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                List<State_Name> CollectionStateName = Objbalcompany.GetStateList(duplicatesRemoved, SearchedStateName, firstname_item, lastname_item, middlename_item);

                                                Guid pkProductIdfcr = Guid.Parse("0f0016cf-c214-44d6-9b41-cda20dee2251");
                                                List<JurisdictionColl> CollectionJudistration = Objbalcompany.GetJurisdictionList(CollectionStateName, pkProductIdfcr);
                                                if (CollectionJudistration != null && CollectionJudistration.Count > 0)
                                                {
                                                    var lst = searchedDataList1.ToList();
                                                    lst.ForEach(n => searchedDataList1.Add(new TiredSearchedData
                                                    {
                                                        county = n.county,
                                                        state = n.state,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName
                                                    }));
                                                    var searchedDataList = searchedDataList1.Select(n => new
                                                    {
                                                        county = n.county,
                                                        state = n.state,
                                                        mName = n.mName,
                                                        fName = n.fName,
                                                        lName = n.lName,
                                                        MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                    }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                    var lst_fcr = CollectionJudistration.ToList();
                                                    lst_fcr.ForEach(n => CollectionJudistration.Add(new JurisdictionColl
                                                    {
                                                        Jurisdiction_Name = n.Jurisdiction_Name,
                                                        pkJurisdictionId = n.pkJurisdictionId,
                                                        AdditionalFee = n.AdditionalFee,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName

                                                    }));
                                                    var CollectionJudistration_item = CollectionJudistration.Select(n => new
                                                    {
                                                        Jurisdiction_Name = n.Jurisdiction_Name,
                                                        pkJurisdictionId = n.pkJurisdictionId,
                                                        AdditionalFee = n.AdditionalFee,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName,
                                                        MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                    }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                    if (CollectionJudistration_item.Count > 0)
                                                    {
                                                        for (int i = 0; i < CollectionJudistration_item.Count; i++)
                                                        {
                                                            ObjJurisdictionListColl.Add(new JurisdictionListColl() { AdditionalFee = decimal.Parse(CollectionJudistration_item.ElementAt(i).AdditionalFee.ToString()), pkJurisdictionId = int.Parse(CollectionJudistration_item.ElementAt(i).pkJurisdictionId.ToString()), Jurisdiction_Name = CollectionJudistration_item.ElementAt(i).Jurisdiction_Name.ToString(), fName = CollectionJudistration_item.ElementAt(i).fName, mName = CollectionJudistration_item.ElementAt(i).mName, lName = CollectionJudistration_item.ElementAt(i).lName });
                                                        }

                                                    }
                                                }
                                            }
                                        }

                                        #endregion

                                        #endregion
                                    }
                                    else if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Contains("SCR"))
                                    {
                                        //Changed By Himesh Kumar
                                        #region Tiered SCR
                                        if (CreditFile != null && CreditFile.Count > 0)
                                        {
                                            List<XElement> PersonalData = CreditFile.Descendants("record").ToList();

                                            List<string> lstStateSCR = new List<string>();
                                            if (PersonalData != null && PersonalData.Count > 0)
                                            {
                                                foreach (XElement xe in PersonalData)
                                                {
                                                    XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                    XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                    XElement Region = Address.Descendants("state").FirstOrDefault();
                                                    XElement County = Address.Descendants("county").FirstOrDefault();


                                                    XElement FName = xe.Descendants("firstname").FirstOrDefault();
                                                    XElement MName = xe.Descendants("middlename").FirstOrDefault();
                                                    XElement LName = xe.Descendants("lastname").FirstOrDefault();
                                                    XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                    if (EffectiveDate != null)
                                                    {
                                                        string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";

                                                        TiredSearchedData objTiered = new TiredSearchedData();
                                                        if (Region != null && County != null && LName != null && YearMonth != null)
                                                        {
                                                            var ArrayCollection = YearMonth.Split('-');
                                                            int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                            //int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                            if (TimeFrame == 0)//ticket #87
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                            else if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                        }
                                                    }
                                                }
                                                string[] StateArray = new BALGeneral().GetDisabledStates().Select(d => d.StateCode).ToArray();

                                                if (searchedDataList1 != null && searchedDataList1.Count > 0)
                                                {
                                                    var lst = searchedDataList1.Where(n => n.county.Contains("DALLAS")).ToList();
                                                    lst.ForEach(n => searchedDataList1.Add(new TiredSearchedData
                                                    {
                                                        county = "DALLAS METRO",
                                                        state = "TX",
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName
                                                    }));

                                                    var searchedDataList = searchedDataList1.Select(n => new
                                                    {
                                                        county = n.county,
                                                        state = n.state,
                                                        mName = n.mName,
                                                        fName = n.fName,
                                                        lName = n.lName,
                                                        MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                    }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();

                                                    searchedDataList = searchedDataList.Where(s => !StateArray.Contains(s.state)).ToList();
                                                    if (searchedDataList.Count > 0)
                                                    {
                                                        bool flagDisabled = false;
                                                        decimal ReportFee = GetReportFee(LocationId, CompanyUserId, "SCR", "Tiered", ObjEmergeReport.ObjTieredPackage.fkPackageId);
                                                        response.Append("<table id=\"tieredTable\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">");
                                                        response.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">Record(s) found : RecordCount" +
                                                            "</td></tr>");
                                                        response.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"100%\"><td width=\"20%\">Full Name</td><td width=\"20%\">State</td><td width=\"15%\">(ReportType)</td><td width=\"20%\">Cost</td> <td width=\"10%\"><input type=\"checkbox\" onclick=\"selectunselectCounty();\" id=\"chk_SelectAllCounty\"  />Select All</td><td width=\"15%\">Sub Total</td></tr>");
                                                        for (int i = 0; i < searchedDataList.Count; i++)
                                                        {
                                                            string CountyName = searchedDataList.ElementAt(i).county.ToLower().Replace("st.", "saint");

                                                            List<tblState> lstStateCode = new BALGeneral().GetStates();
                                                            string StateNameval = lstStateCode.Where(st => st.StateCode == searchedDataList.ElementAt(i).state).Select(st => st.StateName).FirstOrDefault();
                                                            var Countyfee = 0;
                                                            int stateFees = 0;
                                                            if (!string.IsNullOrEmpty(StateNameval))
                                                            {
                                                                string stFee = Objbalcompanytype.Get_State_Fee(StateNameval);

                                                                int.TryParse(stFee, out stateFees);
                                                            }
                                                            Countyfee = stateFees;

                                                            string fee = "$" + ReportFee.ToString(); ;

                                                            if (Countyfee == 0 || Countyfee == Convert.ToDecimal(0.00))
                                                            {
                                                            }
                                                            else
                                                            {
                                                                fee = fee + " <br />State Fee" + " $" + Countyfee;
                                                            }
                                                            // string MatchMeterIcon = MatchMeterResult(searchedDataList.ElementAt(i).fName, searchedDataList.ElementAt(i).mName, searchedDataList.ElementAt(i).lName,
                                                            //                         ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName, ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial, ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName);

                                                            string MatchMeterIcon = searchedDataList.ElementAt(i).MatchingStatus == 1 ? "ico_match_green.jpg" : (searchedDataList.ElementAt(i).MatchingStatus == 2 ? "ico_red_yellow.png" : "ico_red_match.png");
                                                            decimal subTotal = ReportFee + Countyfee;
                                                            string fullName = searchedDataList.ElementAt(i).mName == "" ? searchedDataList.ElementAt(i).fName : searchedDataList.ElementAt(i).fName + " " + searchedDataList.ElementAt(i).mName.Substring(0, 1);
                                                            fullName = fullName + " " + searchedDataList.ElementAt(i).lName;

                                                            response.Append("<tr><td><img title=\"Match Meter\" src=\"../Content/themes/base/images/" + MatchMeterIcon + "\">" + fullName + "</td>");

                                                            string sDispState = "";

                                                            response.Append("<td>" + searchedDataList.ElementAt(i).state + "</td>");
                                                            if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode != null)
                                                            {
                                                                if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Contains("_"))
                                                                    sDispState = "(" + ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Split('_').First() + ")";
                                                                else
                                                                    sDispState = searchedDataList.ElementAt(i).state;
                                                            }
                                                            else
                                                            {
                                                                sDispState = searchedDataList.ElementAt(i).state;
                                                            }

                                                            //INT-126 See Comments on Jira
                                                            string firstname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                                                            string lastname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;
                                                            string middlename_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                            string MI = middlename_item != "" ? middlename_item : "*";

                                                            //string MI = searchedDataList.ElementAt(i).mName != "" ? searchedDataList.ElementAt(i).mName : "*";
                                                            response.Append("<td>" + sDispState + "</td>");
                                                            response.Append("<td>" + fee + "</td>");

                                                            if (lstStateSCR.Contains(searchedDataList.ElementAt(i).state))
                                                            {
                                                                response.Append("<td>&nbsp;&nbsp;&nbsp;<span style=\"color:red;\">-</span></td>");
                                                            }
                                                            else
                                                            {
                                                                response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk\" type=\"checkbox\" id=\"chk_" + CountyName + "_" + searchedDataList.ElementAt(i).state + "_County_" + firstname_item + "_" + MI + "_" + lastname_item + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                                lstStateSCR.Add(searchedDataList.ElementAt(i).state);
                                                            }
                                                            response.Append("<td><span class=\"subtotalClass\" id=\"subTotal_" + i.ToString() + "\">$" + subTotal + "</span></td>");
                                                            response.Append("</tr>");
                                                        }

                                                    }
                                                    if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode == null)
                                                    {
                                                        response.Append("</table>").Replace("RecordCount", searchedDataList.Count.ToString());
                                                    }

                                                }
                                            }
                                        }
                                        #endregion

                                    }
                                }


                                if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode != null)
                                {
                                    response.Append("</td>");
                                    response.Append("<td style=\"border-right: 1px solid gray;vertical-align:top;\">");

                                    if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("SCR"))
                                    {
                                        #region Optional SCR
                                        #region Optional SCR new
                                        List<string> lstStateSCR = new List<string>();
                                        if (CreditFile != null && CreditFile.Count > 0)
                                        {
                                            int RecordNumber = 0;
                                            decimal SCRFee = GetReportFee(LocationId, CompanyUserId, "SCR", "Optional", ObjEmergeReport.ObjTieredPackage.fkPackageId);
                                            List<XElement> PersonalData = CreditFile.Descendants("PersonalData").ToList();
                                            if (PersonalData != null && PersonalData.Count > 0)
                                            {
                                                foreach (XElement xe in PersonalData)
                                                {
                                                    XElement Region = xe.Descendants("Region").FirstOrDefault();
                                                    XElement County = xe.Descendants("County").FirstOrDefault();
                                                    if (Region != null && County != null)
                                                    {
                                                        if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                            DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                    }
                                                }
                                                var distinctOptionalSCRData = searchedDataList1.Select(n => new
                                                {
                                                    state = n.state,
                                                    mName = n.mName,
                                                    fName = n.fName,
                                                    lName = n.lName,
                                                    MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())

                                                }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();

                                                if (distinctOptionalSCRData.Count > 0)
                                                {

                                                    DictCount = distinctOptionalSCRData.Count;
                                                    string RecordCount = "RecordFound";
                                                    responseOptional.Append("<table id=\"OptionTbl\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">");
                                                    responseOptional.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">States (" + RecordCount + ")</td></tr>");
                                                    responseOptional.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"100%\"><td width=\"20%\">Full Name</td><td width=\"20%\">State</td><td width=\"15%\">(ReportType)</td><td width=\"20%\">Cost</td> <td width=\"10%\"><input type=\"checkbox\" onclick=\"selectunselectState();\" id=\"chk_SelectAllStates\"  />Select All</td><td width=\"15%\">Sub Total</td></tr>");

                                                    BALGeneral objGeneral = new BALGeneral();
                                                    string[] StateArray = objGeneral.GetDisabledStates().Select(d => d.StateName).ToArray();//{ "California", "Louisiana", "Nevada", "Ohio", "Vermont", "West Virginia", "Wyoming" };
                                                    for (int i = 0; i < distinctOptionalSCRData.Count; i++)
                                                    {
                                                        string fullName = distinctOptionalSCRData.ElementAt(i).mName == "" ? distinctOptionalSCRData.ElementAt(i).fName : distinctOptionalSCRData.ElementAt(i).fName + " " + distinctOptionalSCRData.ElementAt(i).mName.Substring(0, 1);
                                                        fullName = fullName + " " + distinctOptionalSCRData.ElementAt(i).lName;

                                                        string StateName = (from d in dx1.tblStates
                                                                            where (d.StateCode.ToLower() == distinctOptionalSCRData.ElementAt(i).state.ToLower())
                                                                            select d.StateName).FirstOrDefault().ToString();

                                                        string stringToCheck = StateName;
                                                        string check = string.Empty;
                                                        foreach (string x in StateArray)
                                                        {
                                                            if (x.ToLower().Contains(stringToCheck.ToLower()))
                                                            {
                                                                check = "exist";
                                                            }

                                                        }

                                                        if (check != "exist")
                                                        {
                                                            string sDispState = "";
                                                            responseOptional.Append("<tr>");
                                                            if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode != null)
                                                            {
                                                                if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("_"))
                                                                    sDispState = "(" + ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Split('_').First() + ")";
                                                            }
                                                            else
                                                            {
                                                                sDispState = "NA";// DictCounty.ElementAt(i).Value;
                                                            }
                                                            if (StateName.ToLower() == SearchedStateName.ToLower())
                                                            {
                                                                IsStateFound = "yes";
                                                            }
                                                            string StateFee = Objbalcompanytype.Get_State_Fee(StateName);
                                                            decimal stateFees = 0;
                                                            decimal.TryParse(StateFee, out stateFees);
                                                            if (StateFee == "0")
                                                            {
                                                                StateFee = string.Empty;
                                                            }
                                                            else
                                                            {
                                                                StateFee = "<br /> State Fee" + " $" + StateFee;
                                                            }
                                                            decimal subTotal = stateFees + SCRFee;
                                                            //  string MatchMeterIcon = MatchMeterResult(distinctOptionalSCRData.ElementAt(i).fName, distinctOptionalSCRData.ElementAt(i).mName, distinctOptionalSCRData.ElementAt(i).lName,
                                                            //             ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName, ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial, ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName);

                                                            string MatchMeterIcon = distinctOptionalSCRData.ElementAt(i).MatchingStatus == 1 ? "ico_match_green.jpg" : (distinctOptionalSCRData.ElementAt(i).MatchingStatus == 2 ? "ico_red_yellow.png" : "ico_red_match.png");

                                                            string MI = distinctOptionalSCRData.ElementAt(i).mName != "" ? distinctOptionalSCRData.ElementAt(i).mName : "*";
                                                            responseOptional.Append("<td><img title=\"Match Meter\" src=\"../Content/themes/base/images/" + MatchMeterIcon + "\">" + fullName + "</td>");
                                                            responseOptional.Append("<td>" + StateName + "</td>");
                                                            responseOptional.Append("<td>" + sDispState + "</td>");
                                                            responseOptional.Append("<td>$" + SCRFee + StateFee + "</td>");

                                                            if (lstStateSCR.Contains(StateName))
                                                            {
                                                                responseOptional.Append("<td>&nbsp;&nbsp;&nbsp;<span style=\"color:red;\">-</span></td>");
                                                            }
                                                            else
                                                            {
                                                                responseOptional.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_NoRecord_" + distinctOptionalSCRData.ElementAt(i).state + "_State_" + distinctOptionalSCRData.ElementAt(i).fName + "_" + MI + "_" + distinctOptionalSCRData.ElementAt(i).lName + "_name_" + i + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                                lstStateSCR.Add(StateName);
                                                            }



                                                            responseOptional.Append("<td><span class=\"subtotalClass\" id=\"OptionalSubTotal_" + i + "\">$" + subTotal + "</span></td>");
                                                            responseOptional.Append("<tr>");
                                                            RecordNumber = RecordNumber + 1;
                                                        }

                                                    }
                                                    if (IsStateFound == string.Empty)
                                                    {
                                                        string check = string.Empty;
                                                        foreach (string x in StateArray)
                                                        {
                                                            if (x.ToLower().Contains(SearchedStateName.ToLower()))
                                                            {
                                                                check = "exist";
                                                            }

                                                        }
                                                        if (check != "exist")
                                                        {
                                                            string sDispState = "";
                                                            responseOptional.Append("<tr>");
                                                            sDispState = "(SCR)";
                                                            string StateFee = Objbalcompanytype.Get_State_Fee(SearchedStateName);
                                                            decimal stateFees = 0;
                                                            decimal.TryParse(StateFee, out stateFees);
                                                            if (StateFee == "0" || StateFee == "0.00")
                                                            {
                                                                StateFee = string.Empty;
                                                            }
                                                            else
                                                            {
                                                                StateFee = "<br /> State Fee" + " $" + StateFee;
                                                            }
                                                            decimal subTotal = stateFees + SCRFee;

                                                            string MI = (ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial == null || ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial == "") ? "*" : ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                            responseOptional.Append("<td>" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + " " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + "</td>");
                                                            responseOptional.Append("<td>" + SearchedStateName + "</td>");
                                                            responseOptional.Append("<td>" + sDispState + "</td>");
                                                            responseOptional.Append("<td>$" + SCRFee + StateFee + "</td>");
                                                            responseOptional.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_NoRecord_" + SearchedStateCode + "_State_" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + "_" + MI + "_" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + "_name_" + DictCount + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                            responseOptional.Append("<td><span class=\"subtotalClass\" id=\"OptionalSubTotal_" + DictCount + "\">$" + subTotal + "</span></td>");
                                                            responseOptional.Append("<tr>");
                                                            RecordNumber = RecordNumber + 1;
                                                        }
                                                    }
                                                }
                                                if (DictCounty.Count == 0)
                                                {
                                                    string MI = (ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial == null || ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial == "") ? "*" : ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                    responseOptional.Append("<table id=\"OptionTbl\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">");
                                                    responseOptional.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">State (1) found </td></tr>");
                                                    responseOptional.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"100%\"><td width=\"20%\">Full Name</td><td width=\"20%\">State</td><td width=\"15%\">(ReportType)</td><td width=\"20%\">Cost</td> <td width=\"10%\"><input type=\"checkbox\" onclick=\"selectunselectState();\" id=\"chk_SelectAllStates\"  />Select All</td><td width=\"15%\">Sub Total</td></tr>");
                                                    string sDispState = "";
                                                    responseOptional.Append("<tr>");
                                                    sDispState = "(SCR)";
                                                    string StateFee = Objbalcompanytype.Get_State_Fee(SearchedStateName);
                                                    decimal stateFees = 0;
                                                    decimal.TryParse(StateFee, out stateFees);
                                                    if (StateFee == "0" || StateFee == "0.00")
                                                    {
                                                        StateFee = string.Empty;
                                                    }
                                                    else
                                                    {
                                                        StateFee = "<br /> State Fee" + " $" + StateFee;
                                                    }
                                                    decimal subTotal = stateFees + SCRFee;
                                                    responseOptional.Append("<td>" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + " " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + "</td>");
                                                    responseOptional.Append("<td>" + SearchedStateName + "</td>");
                                                    responseOptional.Append("<td>" + sDispState + "</td>");
                                                    responseOptional.Append("<td>$" + SCRFee + StateFee + "</td>");
                                                    //responseOptional.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_NoRecord_" + SearchedStateCode + "_State_1\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                    responseOptional.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_NoRecord_" + SearchedStateCode + "_State_" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + "_" + MI + "_" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + "_name_" + DictCount + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                    responseOptional.Append("<td><span class=\"subtotalClass\" id=\"OptionalSubTotal_1\">$" + subTotal + "</span></td>");
                                                    responseOptional.Append("<tr>");
                                                    RecordNumber = 1;
                                                }
                                                responseOptional.Append("</table>").Replace("RecordFound", RecordNumber.ToString());
                                            }
                                        }

                                        #endregion

                                        #region Optional SCR Old
                                        //if (CreditFile != null && CreditFile.Count > 0)
                                        //{
                                        //    List<XElement> PersonalData = CreditFile.Descendants("PersonalData").ToList();
                                        //    if (PersonalData != null && PersonalData.Count > 0)
                                        //    {
                                        //        foreach (XElement xe in PersonalData)
                                        //        {
                                        //            XElement Region = xe.Descendants("Region").FirstOrDefault();
                                        //            XElement County = xe.Descendants("County").FirstOrDefault();
                                        //            if (Region != null && County != null)
                                        //            {
                                        //                if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                        //                    DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                        //            }
                                        //        }
                                        //        if (DictCounty.Count > 0)
                                        //        {
                                        //            DictCount = DictCounty.Count;
                                        //            string RecordCount = "RecordFound";
                                        //            responseOptional.Append("<table cellpadding=\"3\" cellspacing=\"0\" width=\"100%\">");
                                        //            responseOptional.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"3\">States (" + RecordCount + ") found for " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + " " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + ":</td></tr>");
                                        //            responseOptional.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"33%\"><td width=\"40%\">State</td><td width=\"40%\"> (ReportType)</td> <td width=\"20%\"><input type=\"checkbox\" onclick=\"selectunselectState();\" id=\"chk_SelectAllStates\"  />Select All</td></tr>");
                                        //            for (int i = 0; i < DictCounty.Count; i++)
                                        //            {
                                        //                string StateName = (from d in dx1.tblStates
                                        //                                    where (d.StateCode.ToLower() == DictCounty.ElementAt(i).Value.ToLower())
                                        //                                    select d.StateName).FirstOrDefault().ToString();

                                        //                string stringToCheck = StateName;
                                        //                string check = string.Empty;
                                        //                BALGeneral objGeneral = new BALGeneral();
                                        //                string[] StateArray = objGeneral.GetDisabledStates().Select(d => d.StateName).ToArray();//{ "California", "Louisiana", "Nevada", "Ohio", "Vermont", "West Virginia", "Wyoming" };
                                        //                foreach (string x in StateArray)
                                        //                {
                                        //                    if (x.ToLower().Contains(stringToCheck.ToLower()))
                                        //                    {
                                        //                        check = "exist";
                                        //                    }

                                        //                }

                                        //                if (check != "exist")
                                        //                {
                                        //                    string sDispState = "";
                                        //                    responseOptional.Append("<tr>");
                                        //                    //responseOptional.Append("<td>" + DictCounty.ElementAt(i).Key + "</td>");
                                        //                    if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode != null)
                                        //                    {
                                        //                        if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("_"))
                                        //                            sDispState = "(" + ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Split('_').First() + ")";
                                        //                    }
                                        //                    else
                                        //                    {
                                        //                        sDispState = "NA";// DictCounty.ElementAt(i).Value;
                                        //                    }
                                        //                    //string StateName = (from d in dx1.tblStates
                                        //                    //            where (d.StateCode.ToLower() == DictCounty.ElementAt(i).Value.ToLower())
                                        //                    //            select d.StateName).FirstOrDefault().ToString();
                                        //                    if (StateName.ToLower() == SearchedStateName.ToLower())
                                        //                    {
                                        //                        IsStateFound = "yes";
                                        //                    }

                                        //                    BALCompanyType Objbalcompanytype = new BALCompanyType();
                                        //                    string StateFee = Objbalcompanytype.Get_State_Fee(StateName);
                                        //                    if (StateFee == "0")
                                        //                    {
                                        //                        StateFee = string.Empty;
                                        //                    }
                                        //                    else
                                        //                    {
                                        //                        StateFee = " State Fee" + " $" + StateFee;
                                        //                    }
                                        //                    responseOptional.Append("<td>" + StateName + "<label style='color:red'>" + StateFee + "</label>" + "</td>");
                                        //                    responseOptional.Append("<td>" + sDispState + "</td>");
                                        //                    responseOptional.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_" + DictCounty.ElementAt(i).Key + "_" + DictCounty.ElementAt(i).Value + "_State_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                        //                    responseOptional.Append("<tr>");
                                        //                }
                                        //            }
                                        //            if (IsStateFound == string.Empty)
                                        //            {
                                        //                string check = string.Empty;
                                        //                BALGeneral objGeneral = new BALGeneral();
                                        //                string[] StateArray = objGeneral.GetDisabledStates().Select(d => d.StateName).ToArray();//{ "California", "Louisiana", "Nevada", "Ohio", "Vermont", "West Virginia", "Wyoming" };
                                        //                foreach (string x in StateArray)
                                        //                {
                                        //                    if (x.ToLower().Contains(SearchedStateName.ToLower()))
                                        //                    {
                                        //                        check = "exist";
                                        //                    }

                                        //                }
                                        //                if (check != "exist")
                                        //                {
                                        //                    DictCount = DictCount + 1;
                                        //                    string sDispState = "";
                                        //                    responseOptional.Append("<tr>");
                                        //                    sDispState = "(SCR)";
                                        //                    BALCompanyType Objbalcompanytype = new BALCompanyType();
                                        //                    string StateFee = Objbalcompanytype.Get_State_Fee(SearchedStateName);
                                        //                    if (StateFee == "0" || StateFee == "0.00")
                                        //                    {
                                        //                        StateFee = string.Empty;
                                        //                    }
                                        //                    else
                                        //                    {
                                        //                        StateFee = " State Fee" + " $" + StateFee;
                                        //                    }
                                        //                    responseOptional.Append("<td>" + SearchedStateName + "<label style='color:red'>" + StateFee + "</label>" + "</td>");
                                        //                    responseOptional.Append("<td>" + sDispState + "</td>");
                                        //                    responseOptional.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_NoRecord_" + SearchedStateCode + "_State_" + DictCount + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                        //                    responseOptional.Append("<tr>");
                                        //                }
                                        //            }
                                        //        }
                                        //        if (DictCounty.Count == 0)
                                        //        {
                                        //            responseOptional.Append("<table cellpadding=\"3\" cellspacing=\"0\" width=\"100%\">");
                                        //            responseOptional.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"3\">States (1) found for " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + " " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + ":</td></tr>");
                                        //            responseOptional.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"33%\"><td width=\"40%\">State</td><td width=\"40%\"> (ReportType)</td> <td width=\"20%\"><input type=\"checkbox\" onclick=\"selectunselectState();\" id=\"chk_SelectAllStates\"  />Select All</td></tr>");
                                        //            string sDispState = "";
                                        //            responseOptional.Append("<tr>");
                                        //            sDispState = "(SCR)";
                                        //            BALCompanyType Objbalcompanytype = new BALCompanyType();
                                        //            string StateFee = Objbalcompanytype.Get_State_Fee(SearchedStateName);
                                        //            if (StateFee == "0" || StateFee == "0.00")
                                        //            {
                                        //                StateFee = string.Empty;
                                        //            }
                                        //            else
                                        //            {
                                        //                StateFee = " State Fee" + " $" + StateFee;
                                        //            }
                                        //            responseOptional.Append("<td>" + SearchedStateName + "<label style='color:red'>" + StateFee + "</label>" + "</td>");
                                        //            responseOptional.Append("<td>" + sDispState + "</td>");
                                        //            responseOptional.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_NoRecord_" + SearchedStateCode + "_State_1\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                        //            responseOptional.Append("<tr>");
                                        //        }
                                        //        responseOptional.Append("</table>").Replace("RecordFound", DictCount.ToString());
                                        //    }
                                        //}
                                        #endregion
                                        if (!ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("NCR1"))
                                            response.Append("</table>");
                                        #endregion
                                    }
                                    if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("NCR1"))
                                    {
                                        #region Optional NCR1
                                        decimal NCRReportfee = GetReportFee(LocationId, CompanyUserId, "NCR1", "Optional", ObjEmergeReport.ObjTieredPackage.fkPackageId); ;
                                        GrandTotal = NCRReportfee;
                                        if (CreditFile != null && CreditFile.Count > 0)
                                        {
                                            List<XElement> PersonalData = CreditFile.Descendants("PersonalData").ToList();
                                            if (PersonalData != null && PersonalData.Count > 0)
                                            {
                                                if (searchedDataList1.Count > 0)
                                                {
                                                    DictCount = DictCounty.Count;
                                                    string MI = (ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial == null || ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial == "") ? "*" : ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                    responseOptional.Append("<table id=\"OptionTbl\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">");
                                                    responseOptional.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td>Optional Report</td><td></td><td></td><td></td><td width=\"15%\"><input type=\"checkbox\" onclick=\"selectunselectState();\" id=\"chk_SelectAllStates\"  />Select All</td><td></td></tr>");
                                                    // ("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"100%\"><td width=\"20%\">Full Name</td><td width=\"20%\">State</td><td width=\"15%\">(ReportType)</td><td width=\"20%\">Cost</td> <td width=\"10%\"><input type=\"checkbox\" onclick=\"selectunselectState();\" id=\"chk_SelectAllStates\"  />Select All</td><td width=\"15%\">Sub Total</td></tr>");
                                                    responseOptional.Append("<tr width=\"100%\">");
                                                    responseOptional.Append("<td>" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + " " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + "</td>");
                                                    responseOptional.Append("<td></td>");
                                                    responseOptional.Append("<td>(NCR1)</td>");
                                                    responseOptional.Append("<td>$" + NCRReportfee + "</td>");
                                                    responseOptional.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_NoRecord_" + SearchedStateCode + "_State_" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + "_" + MI + "_" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + "_1\"  checked=\"checked\" /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                    responseOptional.Append("<td><span class=\"subtotalClass\" id=\"OptionalSubTotal_1\">$" + NCRReportfee + "</span></td>");
                                                    responseOptional.Append("<tr>");
                                                }
                                                responseOptional.Append("</table>");
                                            }
                                        }

                                        #endregion
                                    }
                                    response.Append("</td>");
                                    response.Append("</tr>");
                                    response.Append("</table>");
                                }
                            }
                            catch //(Exception ex)
                            {
                                response = new StringBuilder();
                                responseOptional = new StringBuilder();
                            }
                            #endregion
                        }
                        else if (ObjEmergeReport.ObjOrderInfo.HostProductCode == "CCR1")
                        {

                            #region CCR1 HostReport New
                            List<TiredSearchedData> searchedDataListForCCRHostCode = new List<TiredSearchedData>();
                            string SearchedStateName = string.Empty;
                            string SearchedStateCode = string.Empty;
                            if (ObjEmergeReport.ObjtblOrderSearchedData.SearchedStateId != null)
                            {
                                SearchedStateName = (from d in dx1.tblStates
                                                     where (d.pkStateId == ObjEmergeReport.ObjtblOrderSearchedData.SearchedStateId)
                                                     select d.StateName).FirstOrDefault().ToString();
                                SearchedStateCode = (from d in dx1.tblStates
                                                     where (d.pkStateId == ObjEmergeReport.ObjtblOrderSearchedData.SearchedStateId)
                                                     select d.StateCode).FirstOrDefault().ToString();
                            }
                            try
                            {
                                List<XElement> CriminalReport = ObjXDocument.Descendants("CriminalReport").ToList();
                                if (CriminalReport != null && CriminalReport.Count > 0)
                                {
                                    // List<XElement> CriminalCase = CriminalReport.Descendants("CriminalCase").ToList();
                                    List<XElement> SubjectIdentification = CriminalReport.Descendants("SubjectIdentification").ToList();
                                    if (SubjectIdentification != null && SubjectIdentification.Count > 0)
                                    {
                                        foreach (XElement xe in SubjectIdentification)
                                        {

                                            XElement PostalAddress = xe.Descendants("PostalAddress").FirstOrDefault();
                                            XElement Region = PostalAddress.Descendants("Region").FirstOrDefault();
                                            XElement PersonName = xe.Descendants("PersonName").FirstOrDefault();
                                            XElement FName = PersonName.Descendants("GivenName").FirstOrDefault();
                                            XElement MName = PersonName.Descendants("MiddleName").FirstOrDefault();
                                            XElement LName = PersonName.Descendants("FamilyName").FirstOrDefault();
                                            TiredSearchedData objTiered = new TiredSearchedData();
                                            if (Region != null && LName != null)
                                            {
                                                objTiered.state = Region.Value;
                                                objTiered.fName = FName != null ? FName.Value : "";
                                                objTiered.mName = MName != null ? MName.Value : "";
                                                objTiered.lName = LName.Value;
                                                searchedDataListForCCRHostCode.Add(objTiered);
                                            }
                                        }

                                    }
                                }
                                int RecordCount = 0;
                                if (searchedDataListForCCRHostCode != null && searchedDataListForCCRHostCode.Count > 0)
                                {
                                    #region Tiered SCR

                                    if (searchedDataListForCCRHostCode != null && searchedDataListForCCRHostCode.Count > 0)
                                    {
                                        var searchedDataListForCCRHostCodeCCR = searchedDataListForCCRHostCode.Select(n => new
                                        {
                                            state = n.state,
                                            mName = n.mName.Substring(0, 1),
                                            fName = n.fName,
                                            lName = n.lName
                                        }).Distinct().OrderBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();

                                        if (searchedDataListForCCRHostCodeCCR.Count > 0)
                                            response.Append("<table id=\"tieredTable\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">");
                                        response.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">Record(s) found: RecordCount" +
                                            "</td></tr>");
                                        response.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"100%\"><td width=\"20%\">Full Name</td><td width=\"20%\">County</td><td width=\"15%\">(ReportType)</td><td width=\"20%\">Cost</td> <td width=\"10%\"><input type=\"checkbox\" onclick=\"selectunselectCounty();\" id=\"chk_SelectAllCounty\"  />Select All</td><td width=\"15%\">Sub Total</td></tr>");

                                        for (int i = 0; i < searchedDataListForCCRHostCodeCCR.Count; i++)
                                        {
                                            string StateName = (from d in dx1.tblStates
                                                                where (d.StateCode.ToLower() == searchedDataListForCCRHostCodeCCR.ElementAt(i).state.ToLower())
                                                                select d.StateName).FirstOrDefault().ToString();
                                            string check_second = string.Empty;
                                            string stringToCheck = StateName;
                                            BALGeneral objGeneral = new BALGeneral();
                                            string[] StateArray = objGeneral.GetDisabledStates().Select(d => d.StateName).ToArray();//{ "California", "Louisiana", "Nevada", "Ohio", "Vermont", "West Virginia", "Wyoming" };
                                            foreach (string x in StateArray)
                                            {
                                                if (x.ToLower().Contains(stringToCheck.ToLower()))
                                                {
                                                    check_second = "exist";
                                                }
                                            }
                                            if (check_second != "exist")
                                            {
                                                string sDispState = "";
                                                response.Append("<tr>");
                                                if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode != null)
                                                {
                                                    if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("_"))
                                                        sDispState = "(" + ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Split('_').First() + ")";
                                                }
                                                else
                                                {
                                                    sDispState = "NA";// DictCounty.ElementAt(i).Value;
                                                }
                                                string StateFee = Objbalcompanytype.Get_State_Fee(StateName);
                                                string fullName = searchedDataListForCCRHostCodeCCR.ElementAt(i).mName == "" ? searchedDataListForCCRHostCodeCCR.ElementAt(i).fName : searchedDataListForCCRHostCodeCCR.ElementAt(i).fName + " " + searchedDataListForCCRHostCodeCCR.ElementAt(i).mName.Substring(0, 1);
                                                fullName = fullName + " " + searchedDataListForCCRHostCodeCCR.ElementAt(i).lName;
                                                decimal SCRFee = GetReportFee(LocationId, CompanyUserId, "SCR", "Tiered", ObjEmergeReport.ObjTieredPackage.fkPackageId);
                                                decimal stateFees = 0;
                                                decimal.TryParse(StateFee, out stateFees);

                                                if (StateFee == "0")
                                                {
                                                    StateFee = string.Empty;
                                                }
                                                else
                                                {
                                                    StateFee = "<br /> State Fee" + " $" + StateFee;
                                                }
                                                decimal subTotal = stateFees + SCRFee;
                                                string MI = searchedDataListForCCRHostCodeCCR.ElementAt(i).mName == "" ? "*" : searchedDataListForCCRHostCodeCCR.ElementAt(i).mName;
                                                response.Append("<td>" + fullName + "</td>");
                                                response.Append("<td>" + StateName + "</td>");
                                                response.Append("<td>" + sDispState + "</td>");
                                                response.Append("<td>$" + SCRFee + StateFee + "</td>");//chk_" + DictCounty.ElementAt(i).Key + "_" + DictCounty.ElementAt(i).Value + "_State_" + i.ToString()
                                                response.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_NoRecord_" + searchedDataListForCCRHostCodeCCR.ElementAt(i).state + "_State_" + searchedDataListForCCRHostCodeCCR.ElementAt(i).fName + "_" + MI + "_" + searchedDataListForCCRHostCodeCCR.ElementAt(i).lName + "_name_" + i + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                response.Append("<td><span class=\"subtotalClass\" id=\"subTotal_" + i + "\">$" + subTotal + "</span></td>");
                                                response.Append("<tr>");
                                                RecordCount = RecordCount + 1;
                                            }
                                        }
                                    }
                                    response.Append("</table>").Replace("RecordCount", RecordCount.ToString());

                                    #endregion
                                }


                                if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode != null)
                                {
                                    if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("PS"))
                                    {
                                        #region Optional PS


                                        if (searchedDataListForCCRHostCode != null && searchedDataListForCCRHostCode.Count > 0)
                                        {
                                            var searchedDataListForCCRHostCodePS = searchedDataListForCCRHostCode.Select(n => new
                                            {
                                                state = n.state,
                                                county = n.county,
                                            }).Distinct().ToList();
                                            if (searchedDataListForCCRHostCodePS.Count > 0)
                                            {
                                                responseOptional.Append("<table id=\"OptionTbl\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">");
                                                responseOptional.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">Optional Report</td></tr>");
                                                for (int i = 0; i < searchedDataListForCCRHostCodePS.Count; i++)
                                                {
                                                    string sDispState = "";
                                                    responseOptional.Append("<tr>");
                                                    if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode != null)
                                                    {
                                                        if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("_"))
                                                            sDispState = " (" + ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Split('_').First() + ")";
                                                    }
                                                    else
                                                    {
                                                        sDispState = "NA";// DictCounty.ElementAt(i).Value;
                                                    }
                                                    decimal PSReportfee = GetReportFee(LocationId, CompanyUserId, "PS", "Optional", ObjEmergeReport.ObjTieredPackage.fkPackageId); ;
                                                    string MI = (ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial == null || ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial == "") ? "*" : ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                    responseOptional.Append("<td>" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + " " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + "</td>");
                                                    responseOptional.Append("<td></td>");
                                                    responseOptional.Append("<td>" + sDispState + "</td>");
                                                    responseOptional.Append("<td>$" + PSReportfee + "</td>");
                                                    responseOptional.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_NoRecord_" + SearchedStateCode + "_State_" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + "_" + MI + "_" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + "_name_" + i + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                    responseOptional.Append("<td><span class=\"subtotalClass\" id=\"OptionalSubTotal_" + i + "\">$" + PSReportfee + "</span></td>");
                                                    responseOptional.Append("<tr>");
                                                }
                                            }
                                            responseOptional.Append("</table>");
                                        }

                                        #endregion
                                    }

                                    if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("NCR1"))
                                    {
                                        #region Optional NCR1 New
                                        decimal NCRReportfee = GetReportFee(LocationId, CompanyUserId, "NCR1", "Optional", ObjEmergeReport.ObjTieredPackage.fkPackageId); ;

                                        if (CriminalReport != null && CriminalReport.Count > 0)
                                        {
                                            if (searchedDataListForCCRHostCode.Count > 0)
                                            {
                                                GrandTotal = NCRReportfee;
                                                string MI = (ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial == null || ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial == "") ? "*" : ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                responseOptional.Append("<table id=\"OptionTbl\" cellpadding=\"3\" cellspacing=\"0\" width=\"100%\">");
                                                //  responseOptional.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">Optional Report</td></tr>");
                                                responseOptional.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td >Optional Report</td><td></td><td></td><td></td><td width=\"15%\"><input type=\"checkbox\" onclick=\"selectunselectState();\" id=\"chk_SelectAllStates\"  />Select All</td><td></td></tr>");
                                                responseOptional.Append("<tr width=\"100%\">");
                                                responseOptional.Append("<td>" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + " " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + "</td>");
                                                responseOptional.Append("<td></td>");
                                                responseOptional.Append("<td>(NCR1)</td>");
                                                responseOptional.Append("<td>$" + NCRReportfee + "</td>");
                                                //responseOptional.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_NoRecord_" + SearchedStateCode + "_State_1\"  checked=\"checked\" /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                responseOptional.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_NoRecord_" + SearchedStateCode + "_State_" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + "_" + MI + "_" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + "_name_1\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                responseOptional.Append("<td><span class=\"subtotalClass\" id=\"OptionalSubTotal_1\">$" + NCRReportfee + "</span></td>");
                                                responseOptional.Append("<tr>");
                                            }
                                            responseOptional.Append("</table>");
                                        }

                                        #endregion

                                    }
                                }

                            }
                            catch
                            {
                                response = new StringBuilder();
                                responseOptional = new StringBuilder();
                            }
                            #endregion
                        }

                        #endregion

                        #region OrderInfo Region

                        ObjEmergeReport.ObjOrderInfo.OrderId = OrderId;
                        ObjEmergeReport.ObjOrderInfo.Tiered2Response = response.ToString();
                        if (ObjEmergeReport.ObjTieredPackage.TiredOptional != null)
                            ObjEmergeReport.ObjOrderInfo.OptionalResponse = responseOptional.ToString();
                        else
                            ObjEmergeReport.ObjOrderInfo.OptionalResponse = "";
                        ObjEmergeReport.ObjOrderInfo.TieredTopInfo = TieredHeadText.ToString();

                        #endregion

                        Session["EmergeReport"] = ObjEmergeReport;
                    }
                    #endregion
                }
                else
                {                ///
                    /// Just Create a collection and call the tiered 2nd function SearchingTieredOrders().
                    ///
                    #region Is Automatic True

                    // XmlDocument ObjXmlDocument = new XmlDocument();
                    XDocument ObjXmlDoc = new XDocument();

                    if (ObjEmergeReport.ObjTieredPackage != null && ObjEmergeReport.ObjOrderInfo.IsTiered && ObjEmergeReport.ObjOrderInfo.Tiered2Response != null)
                    {

                        ObjXmlDoc = XDocument.Parse(ObjEmergeReport.ObjOrderInfo.Tiered2Response);
                        string XML_Value = GetRequiredXmlConditional(ObjXmlDoc.Document.ToString(), ObjEmergeReport.ObjOrderInfo.HostProductCode);
                        ObjXDocument = XDocument.Parse(XML_Value);
                        Dictionary<string, string> DictCounty = new Dictionary<string, string>();

                        if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode != null)
                        {
                            ObjEmergeReport.ObjCollProducts.Add(ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode);
                        }
                        if (ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode != null)
                        {
                            ObjEmergeReport.ObjCollProducts.Add(ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode);
                        }

                        if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode != null)
                        {
                            ObjEmergeReport.ObjCollProducts.Add(ObjEmergeReport.ObjOrderInfo.OptionalProductCode);
                        }

                        #region HOST Response

                        string GivenFname = string.Empty;
                        string GivenMname = string.Empty;
                        string givenLname = string.Empty;

                        if (ObjEmergeReport.ObjOrderInfo.HostProductCode == "PS")
                        {
                            #region HostedResponse for PS
                            try
                            {
                                string SearchedStateName = string.Empty;
                                string SearchedStateCode = string.Empty;
                                string IsStateFound = string.Empty;


                                if (ObjEmergeReport.ObjtblOrderSearchedData.SearchedStateId != null)
                                {
                                    SearchedStateName = (from d in dx1.tblStates
                                                         where (d.pkStateId == ObjEmergeReport.ObjtblOrderSearchedData.SearchedStateId)
                                                         select d.StateName).FirstOrDefault().ToString();
                                    SearchedStateCode = (from d in dx1.tblStates
                                                         where (d.pkStateId == ObjEmergeReport.ObjtblOrderSearchedData.SearchedStateId)
                                                         select d.StateCode).FirstOrDefault().ToString();

                                    GivenFname = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                                    GivenMname = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                    givenLname = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;


                                }

                                //Tiered2 Package functionality.
                                List<XElement> CreditFile = ObjXDocument.Descendants("records").ToList();
                                int TimeFrame = ObjEmergeReport.ObjTieredPackage.TimeFrame;
                                int TimeFrameYear = TimeFrame * (-1);
                                DateTime TimeFrameDate = DateTime.Now.AddYears(TimeFrameYear);
                                if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Contains("CCR"))
                                {
                                    //Changed By Himesh Kumar
                                    #region Tiered CCR1
                                    if (CreditFile != null && CreditFile.Count > 0)
                                    {
                                        List<XElement> PersonalData = CreditFile.Descendants("record").ToList();
                                        if (PersonalData != null && PersonalData.Count > 0)
                                        {

                                            foreach (XElement xe in PersonalData)
                                            {
                                                #region----New code
                                                XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                XElement Region = Address.Descendants("state").FirstOrDefault();
                                                XElement County = Address.Descendants("county").FirstOrDefault();

                                                //XElement Region = xe.Descendants("Region").FirstOrDefault();
                                                //XElement County = xe.Descendants("County").FirstOrDefault();
                                                XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                if (EffectiveDate != null)
                                                {
                                                    string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";
                                                    //XElement YearMonth = EndDate.Descendants("YearMonth").FirstOrDefault();

                                                    TiredSearchedData objTiered = new TiredSearchedData();
                                                    if (Region != null && County != null && YearMonth != null)
                                                    {
                                                        var ArrayCollection = YearMonth.Split('-');
                                                        int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                        int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                        if (TimeFrame == 0) //ticket #87
                                                        {
                                                            if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                            {
                                                                DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                            }
                                                        }
                                                        else if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                        {
                                                            if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                            {
                                                                DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion
                                            }
                                            List<string> lstAutoCounty = new List<string>();
                                            List<string> lstcounty = new BALCompany().GetCountyList();

                                            foreach (var DictObj in DictCounty)
                                            {
                                                if (!lstAutoCounty.Contains(DictObj.Key))
                                                {
                                                    if (!lstcounty.Contains(DictObj.Key.ToUpper()))
                                                    {
                                                        flagAutopopup = true;
                                                    }
                                                    COUNTIES = COUNTIES + "##chk_" + DictObj.Key + "_" + DictObj.Value + "_County" + "_" + GivenFname + "_" + GivenMname + "_" + givenLname + "_name_0";
                                                    lstAutoCounty.Add(DictObj.Key);
                                                }
                                                else
                                                {
                                                    continue;
                                                }
                                            }

                                        }
                                    }
                                    #endregion
                                }
                                else if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Contains("FCR"))
                                {

                                    //Changed By Himesh Kumar
                                    #region Tiered FCR
                                    List<XElement> Credit_File = ObjXDocument.Descendants("records").ToList();
                                    List<TiredSearchedData> searchedDataList1 = new List<TiredSearchedData>();

                                    if (Credit_File != null && Credit_File.Count > 0)
                                    {
                                        List<XElement> Personal_Data = Credit_File.Descendants("record").ToList();

                                        List<Region_Name> ObjRegion_Name = new List<Region_Name>();
                                        if (Personal_Data != null && Personal_Data.Count > 0)
                                        {
                                            foreach (XElement xe in Personal_Data)
                                            {
                                                XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                XElement Region = Address.Descendants("state").FirstOrDefault();
                                                XElement County = Address.Descendants("county").FirstOrDefault();


                                                XElement FName = xe.Descendants("firstname").FirstOrDefault();
                                                XElement MName = xe.Descendants("middlename").FirstOrDefault();
                                                XElement LName = xe.Descendants("lastname").FirstOrDefault();
                                                XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                if (EffectiveDate != null)
                                                {
                                                    string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";
                                                    TiredSearchedData objTiered = new TiredSearchedData();
                                                    if (Region != null && County != null && LName != null && YearMonth != null)
                                                    {
                                                        var region_value = Region.Value.ToString();
                                                        int check_item = 1;
                                                        for (int i = 0; i < ObjRegion_Name.Count; i++)
                                                        {
                                                            if (ObjRegion_Name.ElementAt(i).Region.ToString() == region_value)
                                                            {
                                                                check_item = 2;
                                                            }
                                                        }
                                                        if (check_item != 2)
                                                        {
                                                            string FirstName = string.Empty;
                                                            string LastName = string.Empty;
                                                            string MiddleName = string.Empty;
                                                            if (FName != null) { FirstName = FName.Value.ToString(); }
                                                            if (LName != null) { LastName = LName.Value.ToString(); }
                                                            if (MName != null) { MiddleName = MName.Value.ToString(); }




                                                            ObjRegion_Name.Add(new Region_Name() { Region = Region.Value.ToString(), Fname = FirstName, Lname = LastName, Mname = MiddleName });
                                                        }

                                                        var ArrayCollection = YearMonth.Split('-');
                                                        int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                        int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                        if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                        {
                                                            objTiered.county = County.Value;
                                                            objTiered.state = Region.Value;
                                                            objTiered.fName = FName != null ? FName.Value : "";
                                                            objTiered.mName = MName != null ? MName.Value : "";
                                                            objTiered.lName = LName.Value;
                                                            searchedDataList1.Add(objTiered);
                                                        }
                                                    }
                                                }
                                            }
                                            var duplicatesRemoved = ObjRegion_Name.Distinct().ToList();
                                            string firstname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                                            string lastname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;
                                            string middlename_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                            List<State_Name> CollectionStateName = Objbalcompany.GetStateList(duplicatesRemoved, SearchedStateName, firstname_item, lastname_item, middlename_item);

                                            Guid pkProductIdfcr = Guid.Parse("0f0016cf-c214-44d6-9b41-cda20dee2251");
                                            List<JurisdictionColl> CollectionJudistration = Objbalcompany.GetJurisdictionList(CollectionStateName, pkProductIdfcr);
                                            if (CollectionJudistration != null && CollectionJudistration.Count > 0)
                                            {
                                                var lst = searchedDataList1.ToList();
                                                lst.ForEach(n => searchedDataList1.Add(new TiredSearchedData
                                                {
                                                    county = n.county,
                                                    state = n.state,
                                                    fName = n.fName,
                                                    mName = n.mName,
                                                    lName = n.lName
                                                }));
                                                var searchedDataList = searchedDataList1.Select(n => new
                                                {
                                                    county = n.county,
                                                    state = n.state,
                                                    mName = n.mName,
                                                    fName = n.fName,
                                                    lName = n.lName,
                                                    MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                var lst_fcr = CollectionJudistration.ToList();
                                                lst_fcr.ForEach(n => CollectionJudistration.Add(new JurisdictionColl
                                                {
                                                    Jurisdiction_Name = n.Jurisdiction_Name,
                                                    pkJurisdictionId = n.pkJurisdictionId,
                                                    AdditionalFee = n.AdditionalFee,
                                                    fName = n.fName,
                                                    mName = n.mName,
                                                    lName = n.lName

                                                }));
                                                var CollectionJudistration_item = CollectionJudistration.Select(n => new
                                                {
                                                    Jurisdiction_Name = n.Jurisdiction_Name,
                                                    pkJurisdictionId = n.pkJurisdictionId,
                                                    AdditionalFee = n.AdditionalFee,
                                                    fName = n.fName,
                                                    mName = n.mName,
                                                    lName = n.lName,
                                                    MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                if (CollectionJudistration_item.Count > 0)
                                                {
                                                    for (int i = 0; i < CollectionJudistration_item.Count; i++)
                                                    {
                                                        ObjJurisdictionListColl.Add(new JurisdictionListColl() { AdditionalFee = decimal.Parse(CollectionJudistration_item.ElementAt(i).AdditionalFee.ToString()), pkJurisdictionId = int.Parse(CollectionJudistration_item.ElementAt(i).pkJurisdictionId.ToString()), Jurisdiction_Name = CollectionJudistration_item.ElementAt(i).Jurisdiction_Name.ToString(), fName = CollectionJudistration_item.ElementAt(i).fName, mName = CollectionJudistration_item.ElementAt(i).mName, lName = CollectionJudistration_item.ElementAt(i).lName });
                                                    }

                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                else if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Contains("SCR"))
                                {
                                    #region Optional SCR

                                    if (CreditFile != null && CreditFile.Count > 0)
                                    {
                                        List<XElement> PersonalData = CreditFile.Descendants("record").ToList();
                                        if (PersonalData != null && PersonalData.Count > 0)
                                        {
                                            foreach (XElement xe in PersonalData)
                                            {
                                                #region---Old Code
                                                //XElement Region = xe.Descendants("Region").FirstOrDefault();
                                                //XElement County = xe.Descendants("County").FirstOrDefault();
                                                //if (Region != null && County != null)
                                                //{
                                                //    if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                //        DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                //}
                                                #endregion

                                                //Changed By Himesh Kumar
                                                #region---New Code For 7 year filter
                                                XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                XElement Region = Address.Descendants("state").FirstOrDefault();
                                                XElement County = Address.Descendants("county").FirstOrDefault();

                                                //XElement Region = xe.Descendants("Region").FirstOrDefault();
                                                //XElement County = xe.Descendants("County").FirstOrDefault();
                                                XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                if (EffectiveDate != null)
                                                {
                                                    string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";

                                                    TiredSearchedData objTiered = new TiredSearchedData();
                                                    if (Region != null && County != null && YearMonth != null)
                                                    {
                                                        var ArrayCollection = YearMonth.Split('-');
                                                        int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                        int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                        if (TimeFrame == 0) //ticket #87
                                                        {
                                                            if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                                DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                        }
                                                        else if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                        {
                                                            if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                                DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                        }
                                                    }
                                                }
                                                #endregion
                                            }
                                            string[] StateArray = new BALGeneral().GetDisabledStates().Select(d => d.StateCode).ToArray();

                                            List<string> lstAutoState = new List<string>();
                                            if (DictCounty.Count > 0)
                                            {
                                                foreach (var GetState in DictCounty)
                                                {
                                                    if (!lstAutoState.Contains(GetState.Value))
                                                    {
                                                        if (!StateArray.Contains(GetState.Value))
                                                        {
                                                            STATES = STATES + "##chk_" + GetState.Key + "_" + GetState.Value + "_State" + "_" + GivenFname + "_" + GivenMname + "_" + givenLname + "_name_0";
                                                            lstAutoState.Add(GetState.Value);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        continue;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    #endregion
                                }


                                if (!string.IsNullOrEmpty(ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode))
                                {
                                    //Tiered3 Package functionality.
                                    if (ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode.Contains("CCR"))
                                    {
                                        #region Tiered CCR1
                                        if (CreditFile != null && CreditFile.Count > 0)
                                        {
                                            List<XElement> PersonalData = CreditFile.Descendants("record").ToList();
                                            if (PersonalData != null && PersonalData.Count > 0)
                                            {

                                                foreach (XElement xe in PersonalData)
                                                {
                                                    #region---Old Code
                                                    //XElement Region = xe.Descendants("Region").FirstOrDefault();
                                                    //XElement County = xe.Descendants("County").FirstOrDefault();
                                                    //if (Region != null && County != null)
                                                    //{
                                                    //    if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                    //        DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                    //}
                                                    #endregion
                                                    #region----New code
                                                    XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                    XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                    XElement Region = Address.Descendants("state").FirstOrDefault();
                                                    XElement County = Address.Descendants("county").FirstOrDefault();

                                                    //XElement Region = xe.Descendants("Region").FirstOrDefault();
                                                    //XElement County = xe.Descendants("County").FirstOrDefault();
                                                    XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                    if (EffectiveDate != null)
                                                    {
                                                        string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";

                                                        TiredSearchedData objTiered = new TiredSearchedData();
                                                        if (Region != null && County != null && YearMonth != null)
                                                        {
                                                            var ArrayCollection = YearMonth.Split('-');
                                                            int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                            int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                            if (TimeFrame == 0) //ticket #87
                                                            {
                                                                if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                                {
                                                                    DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                                }
                                                            }
                                                            else if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                            {
                                                                if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                                {
                                                                    DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                List<string> lstAutoCounty = new List<string>();
                                                List<string> lstcounty = new BALCompany().GetCountyList();

                                                foreach (var DictObj in DictCounty)
                                                {
                                                    if (!lstAutoCounty.Contains(DictObj.Key))
                                                    {
                                                        if (!lstcounty.Contains(DictObj.Key.ToUpper()))
                                                        {
                                                            flagAutopopup = true;
                                                        }
                                                        COUNTIES = COUNTIES + "##chk_" + DictObj.Key + "_" + DictObj.Value + "_County" + "_" + GivenFname + "_" + GivenMname + "_" + givenLname + "_name_0";
                                                        lstAutoCounty.Add(DictObj.Key);
                                                    }
                                                    else
                                                    {
                                                        continue;
                                                    }
                                                }

                                            }
                                        }
                                        #endregion
                                    }
                                    else if (ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode.Contains("FCR"))
                                    {
                                        //Changed By Himesh Kumar
                                        #region Tiered FCR
                                        List<XElement> Credit_File = ObjXDocument.Descendants("records").ToList();
                                        List<TiredSearchedData> searchedDataList1 = new List<TiredSearchedData>();

                                        if (Credit_File != null && Credit_File.Count > 0)
                                        {
                                            List<XElement> Personal_Data = Credit_File.Descendants("record").ToList();

                                            List<Region_Name> ObjRegion_Name = new List<Region_Name>();
                                            if (Personal_Data != null && Personal_Data.Count > 0)
                                            {
                                                foreach (XElement xe in Personal_Data)
                                                {
                                                    XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                    XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                    XElement Region = Address.Descendants("state").FirstOrDefault();
                                                    XElement County = Address.Descendants("county").FirstOrDefault();


                                                    XElement FName = xe.Descendants("firstname").FirstOrDefault();
                                                    XElement MName = xe.Descendants("middlename").FirstOrDefault();
                                                    XElement LName = xe.Descendants("lastname").FirstOrDefault();
                                                    XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                    if (EffectiveDate != null)
                                                    {
                                                        string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";
                                                        TiredSearchedData objTiered = new TiredSearchedData();
                                                        if (Region != null && County != null && LName != null && YearMonth != null)
                                                        {
                                                            var region_value = Region.Value.ToString();
                                                            int check_item = 1;
                                                            for (int i = 0; i < ObjRegion_Name.Count; i++)
                                                            {
                                                                if (ObjRegion_Name.ElementAt(i).Region.ToString() == region_value)
                                                                {
                                                                    check_item = 2;
                                                                }
                                                            }
                                                            if (check_item != 2)
                                                            {
                                                                string FirstName = string.Empty;
                                                                string LastName = string.Empty;
                                                                string MiddleName = string.Empty;
                                                                if (FName != null) { FirstName = FName.Value.ToString(); }
                                                                if (LName != null) { LastName = LName.Value.ToString(); }
                                                                if (MName != null) { MiddleName = MName.Value.ToString(); }




                                                                ObjRegion_Name.Add(new Region_Name() { Region = Region.Value.ToString(), Fname = FirstName, Lname = LastName, Mname = MiddleName });
                                                            }

                                                            var ArrayCollection = YearMonth.Split('-');
                                                            int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                            int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                            if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                        }
                                                    }
                                                }
                                                var duplicatesRemoved = ObjRegion_Name.Distinct().ToList();
                                                string firstname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                                                string lastname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;
                                                string middlename_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                List<State_Name> CollectionStateName = Objbalcompany.GetStateList(duplicatesRemoved, SearchedStateName, firstname_item, lastname_item, middlename_item);

                                                Guid pkProductIdfcr = Guid.Parse("0f0016cf-c214-44d6-9b41-cda20dee2251");
                                                List<JurisdictionColl> CollectionJudistration = Objbalcompany.GetJurisdictionList(CollectionStateName, pkProductIdfcr);
                                                if (CollectionJudistration != null && CollectionJudistration.Count > 0)
                                                {
                                                    var lst = searchedDataList1.ToList();
                                                    lst.ForEach(n => searchedDataList1.Add(new TiredSearchedData
                                                    {
                                                        county = n.county,
                                                        state = n.state,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName
                                                    }));
                                                    var searchedDataList = searchedDataList1.Select(n => new
                                                    {
                                                        county = n.county,
                                                        state = n.state,
                                                        mName = n.mName,
                                                        fName = n.fName,
                                                        lName = n.lName,
                                                        MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                    }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                    var lst_fcr = CollectionJudistration.ToList();
                                                    lst_fcr.ForEach(n => CollectionJudistration.Add(new JurisdictionColl
                                                    {
                                                        Jurisdiction_Name = n.Jurisdiction_Name,
                                                        pkJurisdictionId = n.pkJurisdictionId,
                                                        AdditionalFee = n.AdditionalFee,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName

                                                    }));
                                                    var CollectionJudistration_item = CollectionJudistration.Select(n => new
                                                    {
                                                        Jurisdiction_Name = n.Jurisdiction_Name,
                                                        pkJurisdictionId = n.pkJurisdictionId,
                                                        AdditionalFee = n.AdditionalFee,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName,
                                                        MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                    }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                    if (CollectionJudistration_item.Count > 0)
                                                    {
                                                        for (int i = 0; i < CollectionJudistration_item.Count; i++)
                                                        {
                                                            ObjJurisdictionListColl.Add(new JurisdictionListColl() { AdditionalFee = decimal.Parse(CollectionJudistration_item.ElementAt(i).AdditionalFee.ToString()), pkJurisdictionId = int.Parse(CollectionJudistration_item.ElementAt(i).pkJurisdictionId.ToString()), Jurisdiction_Name = CollectionJudistration_item.ElementAt(i).Jurisdiction_Name.ToString(), fName = CollectionJudistration_item.ElementAt(i).fName, mName = CollectionJudistration_item.ElementAt(i).mName, lName = CollectionJudistration_item.ElementAt(i).lName });
                                                        }

                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                    else if (ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode.Contains("SCR"))
                                    {
                                        #region Optional SCR

                                        if (CreditFile != null && CreditFile.Count > 0)
                                        {
                                            List<XElement> PersonalData = CreditFile.Descendants("record").ToList();
                                            if (PersonalData != null && PersonalData.Count > 0)
                                            {
                                                foreach (XElement xe in PersonalData)
                                                {
                                                    #region---Old Code
                                                    //XElement Region = xe.Descendants("Region").FirstOrDefault();
                                                    //XElement County = xe.Descendants("County").FirstOrDefault();
                                                    //if (Region != null && County != null)
                                                    //{
                                                    //    if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                    //        DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                    //}
                                                    #endregion

                                                    //Changed By Himesh Kumar
                                                    #region---New Code For 7 year filter
                                                    XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                    XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                    XElement Region = Address.Descendants("state").FirstOrDefault();
                                                    XElement County = Address.Descendants("county").FirstOrDefault();

                                                    //XElement Region = xe.Descendants("Region").FirstOrDefault();
                                                    //XElement County = xe.Descendants("County").FirstOrDefault();
                                                    XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                    if (EffectiveDate != null)
                                                    {
                                                        string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";

                                                        TiredSearchedData objTiered = new TiredSearchedData();
                                                        if (Region != null && County != null && YearMonth != null)
                                                        {
                                                            var ArrayCollection = YearMonth.Split('-');
                                                            int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                            int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                            if (TimeFrame == 0) //ticket #87
                                                            {
                                                                if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                                    DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                            }
                                                            else if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                            {
                                                                if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                                    DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                string[] StateArray = new BALGeneral().GetDisabledStates().Select(d => d.StateCode).ToArray();

                                                List<string> lstAutoState = new List<string>();
                                                if (DictCounty.Count > 0)
                                                {
                                                    foreach (var GetState in DictCounty)
                                                    {
                                                        if (!lstAutoState.Contains(GetState.Value))
                                                        {
                                                            if (!StateArray.Contains(GetState.Value))
                                                            {
                                                                STATES = STATES + "##chk_" + GetState.Key + "_" + GetState.Value + "_State" + "_" + GivenFname + "_" + GivenMname + "_" + givenLname + "_name_0";
                                                                lstAutoState.Add(GetState.Value);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            continue;
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        #endregion
                                    }
                                }

                                if (!string.IsNullOrEmpty(ObjEmergeReport.ObjOrderInfo.OptionalProductCode))
                                {
                                    if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("SCR"))
                                    {
                                        #region Optional SCR

                                        if (CreditFile != null && CreditFile.Count > 0)
                                        {
                                            List<XElement> PersonalData = CreditFile.Descendants("record").ToList();
                                            if (PersonalData != null && PersonalData.Count > 0)
                                            {
                                                foreach (XElement xe in PersonalData)
                                                {
                                                    #region---Old Code
                                                    //XElement Region = xe.Descendants("Region").FirstOrDefault();
                                                    //XElement County = xe.Descendants("County").FirstOrDefault();
                                                    //if (Region != null && County != null)
                                                    //{
                                                    //    if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                    //        DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                    //}
                                                    #endregion

                                                    //Changed By Himesh Kumar
                                                    #region---New Code For 7 year filter
                                                    XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                    XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                    XElement Region = Address.Descendants("state").FirstOrDefault();
                                                    XElement County = Address.Descendants("county").FirstOrDefault();

                                                    //XElement Region = xe.Descendants("Region").FirstOrDefault();
                                                    //XElement County = xe.Descendants("County").FirstOrDefault();
                                                    XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                    if (EffectiveDate != null)
                                                    {
                                                        string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";

                                                        TiredSearchedData objTiered = new TiredSearchedData();
                                                        if (Region != null && County != null && YearMonth != null)
                                                        {
                                                            var ArrayCollection = YearMonth.Split('-');
                                                            int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                            int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                            if (TimeFrame == 0) //ticket #87
                                                            {
                                                                if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                                    DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                            }
                                                            else if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                            {
                                                                if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                                    DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                string[] StateArray = new BALGeneral().GetDisabledStates().Select(d => d.StateCode).ToArray();

                                                List<string> lstAutoState = new List<string>();
                                                if (DictCounty.Count > 0)
                                                {
                                                    foreach (var GetState in DictCounty)
                                                    {
                                                        if (!lstAutoState.Contains(GetState.Value))
                                                        {
                                                            if (!StateArray.Contains(GetState.Value))
                                                            {
                                                                STATES = STATES + "##chk_" + GetState.Key + "_" + GetState.Value + "_State" + "_" + GivenFname + "_" + GivenMname + "_" + givenLname + "_name_0";
                                                                lstAutoState.Add(GetState.Value);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            continue;
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        #endregion
                                    }
                                }
                            }
                            catch
                            {
                                response = new StringBuilder();
                                responseOptional = new StringBuilder();
                            }
                            #endregion
                        }
                        else if (ObjEmergeReport.ObjOrderInfo.HostProductCode == "CCR1")
                        {

                            GivenFname = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                            GivenMname = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                            givenLname = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;

                            #region HostedResponse for CCR
                            try
                            {
                                List<XElement> stateregion = null;
                                List<XElement> reportrequested = null;
                                stateregion = ObjXDocument.Descendants("Region").ToList();
                                if (stateregion != null)
                                {
                                    if (stateregion.Count == 0)
                                    {
                                        stateregion = ObjXDocument.Descendants("State").ToList();
                                    }
                                    if (stateregion.Count == 0)
                                    {
                                        reportrequested = ObjXDocument.Descendants("report-requested").ToList();
                                        if (reportrequested != null && reportrequested.Count() > 0)
                                        {
                                            stateregion = reportrequested.Descendants("state").ToList();
                                        }
                                    }
                                }

                                if (stateregion != null && stateregion.Count > 0)
                                {

                                    #region Tiered SCR

                                    if (stateregion != null && stateregion.Count > 0)
                                    {
                                        foreach (XElement xe in stateregion)
                                        {
                                            if (xe != null)
                                            {
                                                if (DictCounty.Where(d => d.Key == xe.Value).Count() == 0)
                                                    DictCounty.Add(xe.Value.ToString(), xe.Value.ToString());
                                            }
                                        }

                                        //foreach (var DictObj in DictCounty)
                                        //{
                                        //    COUNTIES = COUNTIES + "##chk_" + DictObj.Key + "_" + DictObj.Value + "_County" + "_" + GivenFname + "_" + GivenMname + "_" + givenLname + "_name_0";
                                        //}

                                        if (DictCounty.Count > 0)
                                        {
                                            COUNTIES = COUNTIES + "##chk_" + DictCounty.First().Key + "_" + DictCounty.First().Value + "_State_" + "0".ToString();
                                        }
                                        response.Append("</table>");

                                    }
                                    #endregion

                                    #region Optional PS

                                    if (stateregion != null && stateregion.Count > 0)
                                    {
                                        foreach (XElement xe in stateregion)
                                        {
                                            if (xe != null)
                                            {
                                                if (DictCounty.Where(d => d.Key == xe.Value).Count() == 0)
                                                    DictCounty.Add(xe.Value.ToString(), xe.Value.ToString());
                                            }
                                        }

                                        //foreach (var GetState in DictCounty)
                                        //{
                                        //    STATES = STATES + "##chk_" + GetState.Key + "_" + GetState.Value + "_State" + "_" + GivenFname + "_" + GivenMname + "_" + givenLname + "_name_0";
                                        //}

                                        if (DictCounty.Count > 0)
                                        {
                                            STATES = STATES + "##chk_" + DictCounty.First().Key + "_" + DictCounty.First().Value + "_State_" + "0".ToString();
                                        }


                                    }

                                    #endregion
                                }
                            }
                            catch
                            {
                                response = new StringBuilder();
                                responseOptional = new StringBuilder();
                            }
                            #endregion
                        }

                        #endregion

                        #region OrderInfo Region

                        ObjEmergeReport.ObjOrderInfo.OrderId = OrderId;
                        ObjEmergeReport.ObjOrderInfo.Tiered2Response = response.ToString();
                        if (ObjEmergeReport.ObjTieredPackage.TiredOptional != null)
                            ObjEmergeReport.ObjOrderInfo.OptionalResponse = responseOptional.ToString();
                        else
                            ObjEmergeReport.ObjOrderInfo.OptionalResponse = "";
                        ObjEmergeReport.ObjOrderInfo.TieredTopInfo = TieredHeadText.ToString();

                        #endregion

                        Session["EmergeReport"] = ObjEmergeReport;

                    }

                    #endregion
                }
            }
            try
            {
                string IsDataEntryOnly = "false";
                #region Check Data Entry User


                if (Session["UserRoles"] != null)
                {
                    string[] role = (string[])(Session["UserRoles"]);
                    if (role.Contains("DataEntry"))
                    {
                        ObjEmergeReport.ObjOrderInfo.IsDataEntry_Enable = true;
                    }
                    else
                    {
                        IsDataEntryOnly = GetDataEntryCheck();
                        if (!string.IsNullOrEmpty(IsDataEntryOnly))
                        {
                            ObjEmergeReport.ObjOrderInfo.IsDataEntry_Enable = Convert.ToBoolean(IsDataEntryOnly);
                        }
                    }
                }
                else
                {
                    #region If Session expire check from membership
                    MembershipUser ObjMembership = Membership.GetUser(User.Identity.Name);
                    if (ObjMembership != null)
                    {
                        string[] role = Roles.GetRolesForUser(User.Identity.Name);
                        Session["UserRoles"] = role;
                        if (role.Contains("DataEntry"))
                        {
                            ObjEmergeReport.ObjOrderInfo.IsDataEntry_Enable = true;
                        }
                    }
                    #endregion

                }
                #endregion

            }
            catch// (Exception ex)
            {
            }


            SearchingTieredOrdersLandingPage(ObjEmergeReport.ObjtblOrder.pkOrderId, COUNTIES, STATES, ObjJurisdictionListColl);
            return ObjEmergeReport.ObjtblOrder.pkOrderId;

        }














        public void UpdatetblOrderSearchedData(int ReferenceCodeId, NewReportModel ObjModel, string ReferenceCode, int Order_Ids)
        {
            tblOrderSearchedData ObjtblOrderSearchedData = new tblOrderSearchedData();
            BALCompany ObjBALCompany = new BALCompany();
            ObjtblOrderSearchedData.SearchedLastName = (ObjModel.LastName != null ? ObjModel.LastName.Trim() : string.Empty);
            ObjtblOrderSearchedData.SearchedFirstName = (ObjModel.FirstName != null ? ObjModel.FirstName.Trim() : string.Empty);
            ObjtblOrderSearchedData.SearchedMiddleInitial = ObjModel.MI ?? string.Empty;
            ObjtblOrderSearchedData.SearchedSuffix = (ObjModel.SuffixValue != "-1") ? ObjModel.SuffixValue : string.Empty;
            ObjtblOrderSearchedData.SearchedSsn = ObjModel.Social ?? string.Empty;
            ObjtblOrderSearchedData.SearchedDob = ObjModel.DOB ?? string.Empty;
            ObjtblOrderSearchedData.SearchedPhoneNumber = ObjModel.Phone ?? string.Empty;
            ObjtblOrderSearchedData.SearchedApplicantEmail = ObjModel.ApplicantEmail ?? string.Empty;
            ObjtblOrderSearchedData.SearchedStreetAddress = ObjModel.Address ?? string.Empty;
            ObjtblOrderSearchedData.SearchedDirection = (ObjModel.DirectionValue != "-1") ? ObjModel.DirectionValue : string.Empty;
            ObjtblOrderSearchedData.SearchedStreetName = ObjModel.Street ?? string.Empty;
            ObjtblOrderSearchedData.SearchedStreetType = (ObjModel.TypeValue != "-1" && ObjModel.TypeValue != null) ? ObjModel.TypeValue : string.Empty;
            ObjtblOrderSearchedData.SearchedApt = ObjModel.Apt ?? string.Empty;
            ObjtblOrderSearchedData.SearchedCity = ObjModel.City ?? string.Empty;
            bool IsLiveRunnerState = false;
            if (ObjModel.State != "-1")
            {
                string[] SelectedStateValues = ObjModel.State.ToString().Split('_');
                string SelectedState = SelectedStateValues[0].Trim();
                BALCompanyUsers objBALCompanyUsers = new BALCompanyUsers();
                bool result = objBALCompanyUsers.StateCheckStatusLiveRunner(Convert.ToInt32(SelectedState));
                IsLiveRunnerState = result;
                ObjtblOrderSearchedData.SearchedStateId = int.Parse(SelectedState);
            }

            ObjtblOrderSearchedData.SearchedZipCode = (ObjModel.Zip != null ? ObjModel.Zip.Trim() : string.Empty);
            ObjtblOrderSearchedData.best_SearchedZipCode = (ObjModel.Zip != null ? ObjModel.Zip.Trim() : string.Empty);
            if (ObjModel.Jurisdiction != "-1")
            {
                ObjtblOrderSearchedData.search_county = ObjModel.JurisdictionText;
                ObjtblOrderSearchedData.best_search_county = ObjModel.JurisdictionText;
                ObjtblOrderSearchedData.SearchedCountyId = Convert.ToInt32(ObjModel.Jurisdiction.ToString().Split('_').FirstOrDefault());
            }

            ObjtblOrderSearchedData.SearchedDriverLicense = (ObjModel.DriverLicense != null ? ObjModel.DriverLicense.Trim() : string.Empty);
            ObjtblOrderSearchedData.SearchedVehicleVin = (ObjModel.VehicleVin != null ? ObjModel.VehicleVin.Trim() : string.Empty);
            if (ObjModel.DLState != "-1")
            {
                ObjtblOrderSearchedData.SearchedDLStateId = int.Parse(ObjModel.DLState.ToString().Split('_').FirstOrDefault());
            }

            ObjtblOrderSearchedData.SearchedSex = (ObjModel.Sex != "-1") ? ObjModel.Sex : string.Empty;
            ObjtblOrderSearchedData.SearchedBusinessName = (ObjModel.BusinessName != null ? ObjModel.BusinessName.Trim() : string.Empty);
            ObjtblOrderSearchedData.SearchedBusinessCity = (ObjModel.BusinessCity != null ? ObjModel.BusinessCity.Trim() : string.Empty);
            if (ObjModel.BusinessState != -1)
            {
                ObjtblOrderSearchedData.SearchedBusinessStateId = Convert.ToInt32(ObjModel.BusinessState);
            }
            ObjtblOrderSearchedData.SearchedTrackingRef = ReferenceCode;
            if (ReferenceCodeId != 0)
            {
                ObjtblOrderSearchedData.SearchedTrackingRefId = ReferenceCodeId;
            }
            ObjtblOrderSearchedData.SearchedTrackingNotes = (ObjModel.TrackingNotes != null ? ObjModel.TrackingNotes.Trim() : string.Empty);
            ObjtblOrderSearchedData.SearchedPermissibleId = ObjModel.PermissiblePurposeId; //ND-5 add this value to tblordersearched data
            ObjBALCompany.UpdatetblOrderSearchedData(ObjtblOrderSearchedData, Order_Ids);
        }

        public void UpdatetblOrderSearchedEmploymentInfo(List<string> ObjCollProducts, FormCollection frm)
        {
            List<tblOrderSearchedEmploymentInfo> ObjCollOrderSearchedEmploymentInfo = new List<tblOrderSearchedEmploymentInfo>();
            if (ObjCollProducts.Any(d => d.Contains("EMV")))
            {
                string[] CompanyName = frm.GetValue("CompanyName").AttemptedValue.Split(',');
                string[] CompanyCity = frm.GetValue("CompanyCity").AttemptedValue.Split(',');
                string[] CompanyPhone = frm.GetValue("CompanyPhone").AttemptedValue.Split(',');
                string[] AliasName = frm.GetValue("AliasName").AttemptedValue.Split(',');
                string[] CompanyState = frm.GetValue("CompanyState").AttemptedValue.Split(',');
                for (int iRow = 0; iRow < CompanyName.Count(); iRow++)
                {
                    tblOrderSearchedEmploymentInfo ObjtblOrderSearchedEmploymentInfo = new tblOrderSearchedEmploymentInfo();
                    ObjtblOrderSearchedEmploymentInfo.SearchedAlias = AliasName[iRow];
                    ObjtblOrderSearchedEmploymentInfo.SearchedName = CompanyName[iRow];
                    ObjtblOrderSearchedEmploymentInfo.SearchedCity = CompanyCity[iRow];
                    ObjtblOrderSearchedEmploymentInfo.SearchedState = CompanyState[iRow];
                    ObjtblOrderSearchedEmploymentInfo.SearchedPhone = CompanyPhone[iRow] == "(___) ___-____" ? "" : CompanyPhone[iRow];
                    ObjtblOrderSearchedEmploymentInfo.SearchedContact = string.Empty;
                    ObjtblOrderSearchedEmploymentInfo.SearchedStarted = string.Empty;
                    ObjtblOrderSearchedEmploymentInfo.SearchedEnded = string.Empty;
                    ObjtblOrderSearchedEmploymentInfo.SearchedEmail = string.Empty;
                    ObjtblOrderSearchedEmploymentInfo.SearchedTitleStart = string.Empty;
                    ObjtblOrderSearchedEmploymentInfo.SearchedTitleEnd = string.Empty;
                    ObjtblOrderSearchedEmploymentInfo.SearchedSupervisorContact = string.Empty;
                    ObjtblOrderSearchedEmploymentInfo.SearchedReason = string.Empty;
                    ObjCollOrderSearchedEmploymentInfo.Add(ObjtblOrderSearchedEmploymentInfo);
                }
            }
        }

        public List<tblOrderSearchedLiveRunnerInfo> ListOrderSearchedLiveRunnerInfoBind(string Counties, string States, NewReportModel ObjModel)
        {
            List<tblCounty> ColltblCounty = new List<tblCounty>();
            List<tblState> ColltblState = new List<tblState>();
            List<tblOrderSearchedLiveRunnerInfo> ColltblOrderSearchedLiveRunnerInfo = new List<tblOrderSearchedLiveRunnerInfo>();
            // List<string> CollStateCode = new List<string>();
            //Dictionary<string, string> DicStateColl = new Dictionary<string, string>();
            //   List<TiredSearchedData> DicStateColl = new List<TiredSearchedData>();
            string sCounty = "", sStates = "";
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                ColltblCounty = dx.tblCounties.ToList<tblCounty>();
                ColltblState = dx.tblStates.ToList<tblState>();
            }
            sCounty = Counties;
            sStates = States;

            try
            {
                // BALOrders ObjBALOrders = new BALOrders();
                string[] CountyColl = sCounty.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries);

                if (CountyColl.Count() > 0)
                {
                    ColltblOrderSearchedLiveRunnerInfo.Clear();
                    for (int i = 0; i < CountyColl.Count(); i++)
                    {
                        tblCounty SingletblCounty = new tblCounty();
                        tblState SingletblState = new tblState();

                        string Region = "", County = "";
                        string[] Cnty = CountyColl.ElementAt(i).Split('_');
                        //if (Cnty.Count() > 5)
                        // {
                        Region = Cnty[1];
                        County = Cnty[0];
                        // string fName = (ObjModel.FirstName != null ? ObjModel.FirstName.Trim() : string.Empty);

                        //string lName = (ObjModel.LastName != null ? ObjModel.LastName.Trim() : string.Empty);
                        //string mName = ObjModel.MI == "*" ? "" : ObjModel.MI;

                        SingletblState = ColltblState.Where(d => d.StateCode.ToLower() == Region.ToLower().Trim()).FirstOrDefault();
                        int pkStateId = 0;
                        if (string.IsNullOrEmpty(SingletblState.pkStateId.ToString()) == false) { pkStateId = int.Parse(SingletblState.pkStateId.ToString()); }

                        if (pkStateId == 11 || pkStateId == 21)
                        {
                            SingletblCounty = ColltblCounty.Where(p => (p.CountyName.ToLower().Contains(County.ToLower()) && p.fkStateId == pkStateId)).FirstOrDefault();

                        }

                        else
                        {

                            SingletblCounty = ColltblCounty.Where(p => (p.CountyName.ToLower().Trim() == County.ToLower().Trim() && p.fkStateId == pkStateId)).FirstOrDefault();
                        }


                        if (SingletblCounty != null && SingletblState != null)
                        {
                            tblOrderSearchedLiveRunnerInfo ObjBALtblOrderSearchedLiveRunnerInfo = new tblOrderSearchedLiveRunnerInfo();
                            string IsRcxCounty = (SingletblCounty.IsRcxCounty) ? "True" : "False";

                            ObjBALtblOrderSearchedLiveRunnerInfo.LiveRunnerCounty = SingletblCounty.CountyName + "_" + IsRcxCounty;   // ddlCountyInfoCounty.SelectedItem.Text.Trim() + "_" + IsRcxCounty;
                            ObjBALtblOrderSearchedLiveRunnerInfo.LiveRunnerState = SingletblState.pkStateId.ToString();  //ddlCountyInfoState.SelectedValue;
                            ObjBALtblOrderSearchedLiveRunnerInfo.SearchedCountyId = SingletblCounty.pkCountyId; //CountyId;
                            ObjBALtblOrderSearchedLiveRunnerInfo.SearchedFirstName = (ObjModel.FirstName != null ? ObjModel.FirstName.Trim() : string.Empty);  // FirstName 
                            ObjBALtblOrderSearchedLiveRunnerInfo.SearchedLastName = (ObjModel.LastName != null ? ObjModel.LastName.Trim() : string.Empty);//LastName
                            ObjBALtblOrderSearchedLiveRunnerInfo.SearchedMiddleName = ObjModel.MI == "*" ? "" : ObjModel.MI; //MiddleName
                            if (ColltblOrderSearchedLiveRunnerInfo.Where(d =>
                                d.LiveRunnerCounty == ObjBALtblOrderSearchedLiveRunnerInfo.LiveRunnerCounty &&
                                d.LiveRunnerState == ObjBALtblOrderSearchedLiveRunnerInfo.LiveRunnerState &&
                                d.SearchedCountyId == ObjBALtblOrderSearchedLiveRunnerInfo.SearchedCountyId &&
                                d.SearchedFirstName == ObjBALtblOrderSearchedLiveRunnerInfo.SearchedFirstName &&
                                d.SearchedLastName == ObjBALtblOrderSearchedLiveRunnerInfo.SearchedLastName &&
                                d.SearchedMiddleName == ObjBALtblOrderSearchedLiveRunnerInfo.SearchedMiddleName
                                ).Count() == 0)
                                ColltblOrderSearchedLiveRunnerInfo.Add(ObjBALtblOrderSearchedLiveRunnerInfo);
                        }
                        //}
                    }



                }
            }

            catch { }
            return ColltblOrderSearchedLiveRunnerInfo;



        }


        public string InsertOrderForEscreen(string eSProductCode, string Request)
        {
            //OrderInfo ObjOrderInfo = new OrderInfo();
            string responseurl = "";
            using (Xml ObjXml = new Xml())
            {
                //XDocument ObjXDocument = new XDocument();
                //StringBuilder response = new StringBuilder();
                // StringBuilder responseOptional = new StringBuilder();
                //StringBuilder TieredHeadText = new StringBuilder();
                //List<tblState> tblStateColl = new List<tblState>();

                XmlSerializer serializer = new XmlSerializer(typeof(EmergeReport));
                string FilePath = Server.MapPath(@"~\Resources\Upload\NewReportXML\" + Convert.ToString(Session["NewReportXMLPath"]));
                //TextReader tr = new StreamReader(FilePath);
                EmergeReport ObjEmergeReport = new EmergeReport();
                using (TextReader textWriter = new StreamReader(FilePath))
                {
                    ObjEmergeReport = (EmergeReport)serializer.Deserialize(textWriter);
                }
                serializer = null;

                //int OrderId = 0;
                //int LocationId = Convert.ToInt32(Session["pkLocationId"]) == 0 ? 0 : Convert.ToInt32(Session["pkLocationId"]);
                //int CompanyUserId = Convert.ToInt32(Session["CurrentUserId"]) == 0 ? 0 : Convert.ToInt32(Session["CurrentUserId"]);
                BALOrders ObjBALOrders = new BALOrders();

                //List<tblOrderSearchedAliasPersonalInfo> ObjCollOrderSearchedAliasPersonalInfo = new List<tblOrderSearchedAliasPersonalInfo>();
                //List<tblOrderSearchedAliasLocationInfo> ObjCollOrderSearchedAliasLocationInfo = new List<tblOrderSearchedAliasLocationInfo>();
                // List<tblOrderSearchedAliasWorkInfo> ObjCollOrderSearchedAliasWorkInfo = new List<tblOrderSearchedAliasWorkInfo>();
                //Dictionary<string, Guid> ObjCompnyProductInfo = new Dictionary<string, Guid>();


                List<tblOrderDetail> ObjCollOrderDetail = new List<tblOrderDetail>();
                tblOrderDetail ObjtblOrderDetail = new tblOrderDetail();

                ObjtblOrderDetail.fkProductApplicationId = 0;
                ObjtblOrderDetail.ProductQty = 1;
                ObjtblOrderDetail.ProductPanel = "0";
                ObjCollOrderDetail.Add(ObjtblOrderDetail);


                responseurl = ObjBALOrders.AddOrdersEscreen(ObjEmergeReport.ObjtblOrder,
                                                            ObjEmergeReport.ObjCollProducts,
                                                            ObjEmergeReport.ObjtblOrderSearchedData,
                                                            ObjCollOrderDetail, eSProductCode, Request);
            }
            return responseurl;
        }







        public StringBuilder EscreenRequest(NewReportModel ObjModel)
        {
            // BALCompany ObjbalCompany = new BALCompany();
            StringBuilder ObjXML = new StringBuilder();
            string SearchedStateCode = string.Empty;
            if (ObjModel.State != "-1")
            {
                string[] SelectedStateValues = ObjModel.State.ToString().Split('_');
                string SelectedState = SelectedStateValues[0].Trim();
                int SearchedStateId = int.Parse(SelectedState);
                using (EmergeDALDataContext dx = new EmergeDALDataContext())
                {
                    SearchedStateCode = (from d in dx.tblStates
                                         where (d.pkStateId == SearchedStateId)
                                         select d.StateCode).FirstOrDefault().ToString();
                }
            }
            var ListItems = GetEscreenRequestAccount(ObjModel);
            if (ListItems.Item1 == string.Empty && ListItems.Item2 == string.Empty && ListItems.Item3 == string.Empty && ListItems.Item4 == string.Empty)
            {
                ObjXML.Append((String)null);
            }
            else
            {
                string CompleteAddress = string.Empty;
                if (string.IsNullOrEmpty(ObjModel.Street) == false) { CompleteAddress = ObjModel.Address + "," + ObjModel.Street; }
                else { CompleteAddress = ObjModel.Address; }
                string CompletePhone = string.Empty;
                //CompletePhone = RemoveSpecialsChars(ObjModel.Phone);

                ObjXML.Append("<?xml version='1.0' encoding='UTF-8'?>");
                ObjXML.Append("<TicketRequest>");
                ObjXML.Append("<Version>1.0</Version>");
                ObjXML.Append("<Mode>Prod</Mode>");
                ObjXML.Append("<CommitAction>Commit</CommitAction>");
                ObjXML.Append("<PartnerInfo>");
                ObjXML.Append("<UserName>" + ListItems.Item1 + "</UserName>");
                ObjXML.Append("<Password>" + ListItems.Item2 + "</Password>");
                ObjXML.Append("</PartnerInfo>");
                ObjXML.Append("<CustomerIdentification>");
                ObjXML.Append("<IPAddress />");
                ObjXML.Append("<ClientAccount>" + ListItems.Item3 + "</ClientAccount>");
                ObjXML.Append("<ClientSubAccount>" + ListItems.Item4 + "</ClientSubAccount>");
                ObjXML.Append("</CustomerIdentification>");
                ObjXML.Append("<TicketAction>");
                ObjXML.Append("<Type>Sched</Type>");
                ObjXML.Append("<Params>");
                ObjXML.Append("<Param>");
                ObjXML.Append("<ID>FirstName</ID>");
                ObjXML.Append("<Value>" + ObjModel.FirstName + "</Value>");
                ObjXML.Append("</Param>");
                ObjXML.Append("<Param>");
                ObjXML.Append("<ID>LastName</ID>");
                ObjXML.Append("<Value>" + ObjModel.LastName + "</Value>");
                ObjXML.Append("</Param>");
                ObjXML.Append("<Param>");
                ObjXML.Append("<ID>SSN</ID>");
                ObjXML.Append("<Value>" + ObjModel.Social + "</Value>");
                ObjXML.Append("</Param>");
                ObjXML.Append("<Param>");
                ObjXML.Append("<ID>ExternalUserID</ID>");
                ObjXML.Append("<Value>1234</Value>");
                ObjXML.Append("</Param>");
                ObjXML.Append("<Param>");
                ObjXML.Append("<ID>DOB</ID>");
                ObjXML.Append("<Value>" + ObjModel.DOB + "</Value>");
                ObjXML.Append("</Param>");
                ObjXML.Append("<Param>");
                ObjXML.Append("<ID>DayPhone</ID>");
                ObjXML.Append("<Value>" + CompletePhone + "</Value>");
                ObjXML.Append("</Param>");
                ObjXML.Append("<Param>");
                ObjXML.Append("<ID>Address</ID>");
                ObjXML.Append("<Value>" + CompleteAddress + "</Value>");
                ObjXML.Append("</Param>");
                ObjXML.Append("<Param>");
                ObjXML.Append("<ID>City</ID>");
                ObjXML.Append("<Value>" + ObjModel.City + "</Value>");
                ObjXML.Append("</Param>");
                ObjXML.Append("<Param>");
                ObjXML.Append("<ID>State</ID>");
                ObjXML.Append("<Value>" + SearchedStateCode + "</Value>");
                ObjXML.Append("</Param>");
                ObjXML.Append("<Param>");
                ObjXML.Append("<ID>PostalCode</ID>");
                ObjXML.Append("<Value>" + ObjModel.Zip + "</Value>");
                ObjXML.Append("</Param>");
                ObjXML.Append("<Param>");
                ObjXML.Append("<ID>Email</ID>");
                ObjXML.Append("<Value>" + ObjModel.ApplicantEmail + "</Value>");
                ObjXML.Append("</Param>");
                ObjXML.Append("<Param>");
                ObjXML.Append("<ID>ExternalDonorID</ID>");
                ObjXML.Append("<Value>%CurrentOrderId%</Value>");
                ObjXML.Append("</Param>");
                ObjXML.Append("</Params>");
                ObjXML.Append("<CompletionAction>");
                ObjXML.Append("<Action>");
                //NotifyURL send by vendor.
                ObjXML.Append("<Type>URL</Type>");//url


                // ObjXML.Append("<Type>NOTIFURL</Type>");//url

                // ObjXML.Append("<Value>https://emerge.intelifi.com/get-escreen-response.aspx?OrderId=%CurrentOrderId%</Value>");
                // ObjXML.Append("<Value>https://test.intelifi.com/testProd3/get-escreen-response.aspx?OrderId=%CurrentOrderId%</Value>");
                // ObjXML.Append("<Value>https://localhost:44301/get-escreen-response.aspx?OrderId=%CurrentOrderId%</Value>");

                ObjXML.Append("<Value>" + ConfigurationManager.AppSettings["eScreenTestUrl"].ToString() + "</Value>");
                // ObjXML.Append("<Type>NOTIFURL</Type>");
                ////ObjXML.Append("<Value>" + ConfigurationManager.AppSettings["eScreenTestUrlEventID"].ToString() + "</Value>");
                //ObjXML.Append("<Value>https://test.intelifi.com/testProd3/get-escreen-response.aspx?OrderId=%CurrentOrderId%</Value>");
                //ObjXML.Append("<Value>https://localhost:44300/get-escreen-response.aspx?OrderId=%CurrentOrderId%</Value>");
                ObjXML.Append("<Condition>Success</Condition>");
                ObjXML.Append("</Action>");
                ObjXML.Append("</CompletionAction>");
                ObjXML.Append("</TicketAction>");
                ObjXML.Append("</TicketRequest>");
            }

            return ObjXML;
        }
        public static string RemoveSpecialsChars(string str)
        {

            string[] chars = new string[] { ",", ".", "/", "!", "@", "#", "$", "%", "^", "&", "*", "'", "\"", ";", "_", "(", ")", ":", "|", "[", "]", " ", "-" };
            for (int i = 0; i < chars.Length; i++)
                if (str.Contains(chars[i]))
                {
                    str = str.Replace(chars[i], "");
                }
            return str;
        }
        public Tuple<string, string, string, string> GetEscreenRequestAccount(NewReportModel ObjModel)
        {
            using (EmergeDALDataContext ObjEmergeDALDataContext = new EmergeDALDataContext())
            {
                string Username = string.Empty;
                string Password = string.Empty;
                string ClientAccount = string.Empty;
                string ClientSubAccount = string.Empty;
                var Obj_thirdpartycredentialdata = ObjEmergeDALDataContext.tblthirdpartycredentials.Where(db => db.fkCompanyId == ObjModel.pkCompanyId).FirstOrDefault();
                if (Obj_thirdpartycredentialdata != null)
                {
                    Username = Obj_thirdpartycredentialdata.username;
                    Password = Obj_thirdpartycredentialdata.password;
                    ClientAccount = Obj_thirdpartycredentialdata.accountno;
                    ClientSubAccount = Obj_thirdpartycredentialdata.subaccountno;
                }
                return new Tuple<string, string, string, string>(Username, Password, ClientAccount, ClientSubAccount);

            }
        }
        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("WebMatrix", "0.6.0.0")]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Web.Services.WebServiceBindingAttribute(Name = "SingleSignOnSoap", Namespace = "http://services.escreen.com/")]
        public partial class SingleSignOn : System.Web.Services.Protocols.SoapHttpClientProtocol
        {

            /// <remarks/>
            public SingleSignOn()
            {
                this.Url = "https://services.escreen.com/singlesignon/singlesignon.asmx";
            }

            /// <remarks/>
            [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://services.escreen.com/RequestTicket", RequestNamespace = "http://services.escreen.com/", ResponseNamespace = "http://services.escreen.com/", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
            public string RequestTicket(string sRequestXML)
            {
                object[] results = this.Invoke("RequestTicket", new object[] {
                    sRequestXML});
                return ((string)(results[0]));
            }

            /// <remarks/>
            public System.IAsyncResult BeginRequestTicket(string sRequestXML, System.AsyncCallback callback, object asyncState)
            {
                return this.BeginInvoke("RequestTicket", new object[] {
                    sRequestXML}, callback, asyncState);
            }

            /// <remarks/>
            public string EndRequestTicket(System.IAsyncResult asyncResult)
            {
                object[] results = this.EndInvoke(asyncResult);
                return ((string)(results[0]));
            }
        }


    }
}
