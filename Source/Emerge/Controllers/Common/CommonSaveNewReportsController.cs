﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Emerge.Data;
using Emerge.Models;
using System.Xml;
using Emerge.Common;
using System.Xml.Serialization;
using System.IO;
using System.Configuration;
using System.Text;
using System.Xml.Linq;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web.Services.Protocols;
using System.Data;

namespace Emerge.Controllers.Common
{
    public partial class CommonNewReportController : Controller
    {
        #region Save New Report

        public ActionResult SaveNewReport()
        {
            decimal GrandTotal = 0;
            string COUNTIES = "";
            string STATES = "";
            Boolean flagAutopopup = false;
            List<JurisdictionListColl> ObjJurisdictionListColl = new List<JurisdictionListColl>();
            BALCompanyType Objbalcompanytype = new BALCompanyType();
            BALCompany Objbalcompany = new BALCompany();
           // OrderInfo ObjOrderInfo = new OrderInfo();
            //Xml ObjXml = new Xml();
            XDocument ObjXDocument = new XDocument();
            StringBuilder response = new StringBuilder();
            StringBuilder responseOptional = new StringBuilder();
            StringBuilder TieredHeadText = new StringBuilder();
            //List<tblState> tblStateColl = new List<tblState>();

            XmlSerializer serializer = new XmlSerializer(typeof(EmergeReport));
            string FilePath = Server.MapPath(@"~\Resources\Upload\NewReportXML\" + Convert.ToString(Session["NewReportXMLPath"]));
            //TextReader tr = new StreamReader(FilePath);
            EmergeReport ObjEmergeReport = new EmergeReport();

            using (TextReader textWriter = new StreamReader(FilePath))
            {
                ObjEmergeReport = (EmergeReport)serializer.Deserialize(textWriter);
            }
            serializer = null;

            int? OrderId = 0;
            int LocationId = (Session["pkLocationId"] == null) ? 0 : Convert.ToInt32(Session["pkLocationId"].ToString());
            int CompanyUserId = (Session["CurrentUserId"] == null) ? 0 : Convert.ToInt32(Session["CurrentUserId"].ToString());

            BALOrders ObjBALOrders = new BALOrders();
            bool flagtoarchive = false;
            List<tblOrderSearchedAliasPersonalInfo> ObjCollOrderSearchedAliasPersonalInfo = new List<tblOrderSearchedAliasPersonalInfo>();
            List<tblOrderSearchedAliasLocationInfo> ObjCollOrderSearchedAliasLocationInfo = new List<tblOrderSearchedAliasLocationInfo>();
            List<tblOrderSearchedAliasWorkInfo> ObjCollOrderSearchedAliasWorkInfo = new List<tblOrderSearchedAliasWorkInfo>();
            Dictionary<string, Guid> ObjCompnyProductInfo = new Dictionary<string, Guid>();
            Session["CheckApiStatus"] = Session["newapi"];
            // int 414 on 06 july 2016 
            if (Convert.ToString(Session["newapi"]) == "fromapi")
            {
                ObjEmergeReport.ObjtblOrder.CreatedById = CompanyUserId;
                List<string> ObjCollProducts = ProductsLogSelectedList(Convert.ToString(Session["ProductSelected"]));
                ObjEmergeReport.ObjCollProducts = ObjCollProducts;
                ObjEmergeReport = ObjBALOrders.AddOrdersApi(ObjEmergeReport.ObjtblOrder,
                ObjEmergeReport.ObjCollProducts,
                ObjEmergeReport.ObjtblOrderSearchedData,
                ObjEmergeReport.ObjCollOrderSearchedEmploymentInfo,
                ObjEmergeReport.ObjCollOrderSearchedEducationInfo,
                ObjEmergeReport.ObjCollOrderSearchedPersonalInfo,
                ObjEmergeReport.ObjCollOrderSearchedProfessionalInfo,
                ObjEmergeReport.objColltblOrderPersonalQtnInfo,
                ObjEmergeReport.ObjCollOrderSearchedLicenseInfo,
                ObjCollOrderSearchedAliasPersonalInfo,
                ObjCollOrderSearchedAliasLocationInfo,
                ObjCollOrderSearchedAliasWorkInfo,
                ObjCompnyProductInfo,
                ObjEmergeReport.ObjColltblOrderSearchedLiveRunnerInfo, ObjEmergeReport.ObjCollCLSConsentInfo,
                Utility.SiteApplicationId, ObjEmergeReport.IsNullDOB,
                ObjEmergeReport.IsLiveRunnerState, ObjEmergeReport.ObjTieredPackage, ObjEmergeReport.ObjOrderInfo, ObjEmergeReport.CompanyAccountNo, ObjEmergeReport.OrderCompanyName, ObjEmergeReport.PermissiblePurpose);

            }
            else
            {
                ObjEmergeReport = ObjBALOrders.AddOrders(ObjEmergeReport.ObjtblOrder,
                ObjEmergeReport.ObjCollProducts,
                ObjEmergeReport.ObjtblOrderSearchedData,
                ObjEmergeReport.ObjCollOrderSearchedEmploymentInfo,
                ObjEmergeReport.ObjCollOrderSearchedEducationInfo,
                ObjEmergeReport.ObjCollOrderSearchedPersonalInfo,
                ObjEmergeReport.ObjCollOrderSearchedProfessionalInfo,
                ObjEmergeReport.objColltblOrderPersonalQtnInfo,
                ObjEmergeReport.ObjCollOrderSearchedLicenseInfo,
                ObjCollOrderSearchedAliasPersonalInfo,
                ObjCollOrderSearchedAliasLocationInfo,
                ObjCollOrderSearchedAliasWorkInfo,
                ObjCompnyProductInfo,
                ObjEmergeReport.ObjColltblOrderSearchedLiveRunnerInfo, ObjEmergeReport.ObjCollCLSConsentInfo,
                Utility.SiteApplicationId, ObjEmergeReport.IsNullDOB,
                ObjEmergeReport.IsLiveRunnerState, ObjEmergeReport.ObjTieredPackage, ObjEmergeReport.ObjOrderInfo, ObjEmergeReport.CompanyAccountNo, ObjEmergeReport.OrderCompanyName, ObjEmergeReport.PermissiblePurpose, ObjEmergeReport.ObjJurisdictionList);
            }
            Session["newapi"] = null;
            if (ObjEmergeReport.ObjOrderInfo.OrderId != null)
            {
                OrderId = ObjEmergeReport.ObjOrderInfo.OrderId;
                using (EmergeDALDataContext dx = new EmergeDALDataContext())
                {
                    ObjEmergeReport.ObjtblOrder = dx.tblOrders.Where(d => d.pkOrderId == OrderId).FirstOrDefault();
                    ObjEmergeReport.ObjtblOrderSearchedData = dx.tblOrderSearchedDatas.Where(d => d.fkOrderId == OrderId).FirstOrDefault();
                }
            }

            using (EmergeDALDataContext dx1 = new EmergeDALDataContext())
            {
                if (!ObjEmergeReport.ObjOrderInfo.IsAutomatic)
                {
                    #region Is Automatic False
                    List<TiredSearchedData> searchedDataList1 = new List<TiredSearchedData>();
                    //XmlDocument ObjXmlDocument = new XmlDocument();
                    XDocument ObjXmlDoc = new XDocument();
                    if (ObjEmergeReport.ObjTieredPackage != null && ObjEmergeReport.ObjOrderInfo.IsTiered && ObjEmergeReport.ObjOrderInfo.Tiered2Response != null)
                    {
                        string sHostname = "";
                        string sTiered2 = "";
                        string sTiered3 = "";
                        string sOptional = "";

                        int TimeFrame = ObjEmergeReport.ObjTieredPackage.TimeFrame;
                        int TimeFrameYear = TimeFrame * (-1);
                        DateTime TimeFrameDate = DateTime.Now.AddYears(TimeFrameYear);
                        ObjXmlDoc = XDocument.Parse(ObjEmergeReport.ObjOrderInfo.Tiered2Response);
                        string XML_Value = GetRequiredXmlConditional(ObjXmlDoc.Document.ToString(), ObjEmergeReport.ObjOrderInfo.HostProductCode);
                        ObjXDocument = XDocument.Parse(XML_Value);
                        Dictionary<string, string> DictCounty = new Dictionary<string, string>();

                        if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode != null)
                        {
                            ObjEmergeReport.ObjCollProducts.Add(ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode);
                        }
                        if (ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode != null)
                        {
                            if (Convert.ToString(Session["CheckApiStatus"]) == "fromapi")
                            {
                                // changes for landing page issue int 414 on 06 july 2016
                                string[] Tiered3ProductCodeValueapi = Convert.ToString(ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode).Split('_');

                                if (Convert.ToString(Tiered3ProductCodeValueapi[0]) != "")
                                {
                                    ObjEmergeReport.ObjCollProducts.Add(ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode);
                                }


                            }// changes for landing page issue int 414 on 06 july 2016
                            else
                            {
                                ObjEmergeReport.ObjCollProducts.Add(ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode);
                            }
                        }

                        if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode != null)
                        {
                            ObjEmergeReport.ObjCollProducts.Add(ObjEmergeReport.ObjOrderInfo.OptionalProductCode);
                        }

                        #region Header Text

                        List<tblProductPerApplication> ObjtblPPA = new List<tblProductPerApplication>();
                        tblProductPerApplication ObjtblPPAs = new tblProductPerApplication();

                        using (EmergeDALDataContext dx = new EmergeDALDataContext())
                        {
                            ObjtblPPA = dx.tblProductPerApplications.ToList<tblProductPerApplication>();

                            ObjtblPPAs = ObjtblPPA.Where(d => d.pkProductApplicationId == ObjEmergeReport.ObjTieredPackage.Host).FirstOrDefault();


                            if (ObjtblPPAs != null)
                            {
                                sHostname = "[" + ObjtblPPAs.ProductCode + "] " + ObjtblPPAs.ProductDisplayName;
                            }
                            ObjtblPPAs = null;
                            ObjtblPPAs = ObjtblPPA.Where(d => d.pkProductApplicationId == ObjEmergeReport.ObjTieredPackage.Tired2).FirstOrDefault();
                            if (ObjtblPPAs != null)
                            {
                                sTiered2 = "[" + ObjtblPPAs.ProductCode + "] " + ObjtblPPAs.ProductDisplayName;
                            }
                            ObjtblPPAs = null;
                            ObjtblPPAs = ObjtblPPA.Where(d => d.pkProductApplicationId == ObjEmergeReport.ObjTieredPackage.Tired3).FirstOrDefault();
                            if (ObjtblPPAs != null)
                            {
                                sTiered3 = "[" + ObjtblPPAs.ProductCode + "] " + ObjtblPPAs.ProductDisplayName;
                            }
                            ObjtblPPAs = null;
                            ObjtblPPAs = ObjtblPPA.Where(d => d.pkProductApplicationId == ObjEmergeReport.ObjTieredPackage.TiredOptional).FirstOrDefault();
                            if (ObjtblPPAs != null)
                            {
                                sOptional = "[" + ObjtblPPAs.ProductCode + "] " + ObjtblPPAs.ProductDisplayName;
                            }

                            if (!string.IsNullOrEmpty(sTiered2) && !string.IsNullOrEmpty(sTiered3))
                            {
                                TieredHeadText.Append("<table style=\"width:100%;\">");
                                TieredHeadText.Append("<tr>");

                                if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Contains("FCR"))
                                {
                                    TieredHeadText.Append("<td style=\"width:49.8%;border-left: 1px solid gray;border-right: 1px solid gray;\">");
                                    TieredHeadText.Append("<table>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Host Report:</td><td>" + sHostname + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Tiered 2 Report:</td><td>" + sTiered2 + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Tiered 2 Optional Report:</td><td>" + sOptional + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Timeframe: </td><td>" + ObjEmergeReport.ObjTieredPackage.TimeFrame + " Year(s)</td><tr>");
                                    // TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Conditional Variable:</td><td>" + ObjEmergeReport.ObjTieredPackage.Variable + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'></td><td></td><tr>");
                                    TieredHeadText.Append("</table>");
                                    TieredHeadText.Append("</td>");
                                    TieredHeadText.Append("<td style=\"border-right: 1px solid gray;\">");
                                    TieredHeadText.Append("<table>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Host Report:</td><td>" + sHostname + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Tiered 2 Report:</td><td>" + sTiered3 + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Tiered 2 Optional Report:</td><td>" + sOptional + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Timeframe: </td><td>" + ObjEmergeReport.ObjTieredPackage.TimeFrame + " Year(s)</td><tr>");
                                    //TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Conditional Variable:</td><td>" + ObjEmergeReport.ObjTieredPackage.Variable + "</td><tr>");
                                    TieredHeadText.Append("</table>");
                                    TieredHeadText.Append("</td>");
                                }
                                else
                                {
                                    TieredHeadText.Append("<td style=\"width:49.8%;border-left: 1px solid gray;border-right: 1px solid gray;\">");
                                    TieredHeadText.Append("<table>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Host Report:</td><td>" + sHostname + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Tiered 2 Report:</td><td>" + sTiered3 + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Tiered 2 Optional Report:</td><td>" + sOptional + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Timeframe: </td><td>" + ObjEmergeReport.ObjTieredPackage.TimeFrame + " Year(s)</td><tr>");
                                    // TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Conditional Variable:</td><td>" + ObjEmergeReport.ObjTieredPackage.Variable + "</td><tr>");
                                    TieredHeadText.Append("</table>");
                                    TieredHeadText.Append("</td>");
                                    TieredHeadText.Append("<td style=\"border-right: 1px solid gray;\">");
                                    TieredHeadText.Append("<table>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Host Report:</td><td>" + sHostname + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Tiered 2 Report:</td><td>" + sTiered2 + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Tiered 2 Optional Report:</td><td>" + sOptional + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Timeframe: </td><td>" + ObjEmergeReport.ObjTieredPackage.TimeFrame + " Year(s)</td><tr>");
                                    // TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Conditional Variable:</td><td>" + ObjEmergeReport.ObjTieredPackage.Variable + "</td><tr>");
                                    TieredHeadText.Append("</table>");
                                    TieredHeadText.Append("</td>");
                                }

                                TieredHeadText.Append("</tr>");
                                TieredHeadText.Append("</table>");
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(sTiered2))
                                {
                                    TieredHeadText.Append("<table>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Host Report:</td><td>" + sHostname + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Tiered 2 Report:</td><td>" + sTiered2 + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Tiered 2 Optional Report:</td><td>" + sOptional + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Timeframe: </td><td>" + ObjEmergeReport.ObjTieredPackage.TimeFrame + " Year(s)</td><tr>");
                                    // TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Conditional Variable:</td><td>" + ObjEmergeReport.ObjTieredPackage.Variable + "</td><tr>");
                                    TieredHeadText.Append("</table>");
                                }
                                else
                                {
                                    TieredHeadText.Append("<table>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Host Report:</td><td>" + sHostname + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Tiered 2 Report:</td><td>" + sTiered3 + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Tiered 2 Optional Report:</td><td>" + sOptional + "</td><tr>");
                                    TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Timeframe: </td><td>" + ObjEmergeReport.ObjTieredPackage.TimeFrame + " Year(s)</td><tr>");
                                    //TieredHeadText.Append("<tr><td style='font-size: 13px; font-weight: bold; font-family: Arial;'>Conditional Variable:</td><td>" + ObjEmergeReport.ObjTieredPackage.Variable + "</td><tr>");
                                    TieredHeadText.Append("</table>");
                                }

                            }
                        }
                        #endregion

                        #region HOST Response
                       // int subTotalRowCount = 0;

                        if (ObjEmergeReport.ObjOrderInfo.HostProductCode == "PS")
                        {
                            #region PS HostReport
                            try
                            {
                                string SearchedStateName = string.Empty;
                                string SearchedStateCode = string.Empty;
                                string IsStateFound = string.Empty;
                                if (ObjEmergeReport.ObjtblOrderSearchedData.SearchedStateId != null)
                                {
                                    SearchedStateName = (from d in dx1.tblStates
                                                         where (d.pkStateId == ObjEmergeReport.ObjtblOrderSearchedData.SearchedStateId)
                                                         select d.StateName).FirstOrDefault().ToString();
                                    SearchedStateCode = (from d in dx1.tblStates
                                                         where (d.pkStateId == ObjEmergeReport.ObjtblOrderSearchedData.SearchedStateId)
                                                         select d.StateCode).FirstOrDefault().ToString();
                                }

                                int DictCount = 0;
                                string Tiered2ReportType = string.Empty;
                                string Tiered3ReportType = string.Empty;
                                List<XElement> Credit_File = ObjXDocument.Descendants("records").ToList();
                                List<XElement> CreditFile = ObjXDocument.Descendants("records").ToList();

                                //if (!string.IsNullOrEmpty(ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode) && !string.IsNullOrEmpty(ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode))
                                if (!string.IsNullOrEmpty(sTiered2) && !string.IsNullOrEmpty(sTiered3))
                                {
                                    #region Tiered FCR and Tiered CCR1

                                    if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Contains("FCR"))
                                    {


                                        Tiered2ReportType = "Tiered2";

                                        response.Append("<table style=\"width:100%;\">");
                                        response.Append("<tr>");
                                        response.Append("<td style=\"width:50%;border-left: 1px solid gray;border-right: 1px solid gray;\">");
                                        
                                        //Changed By Himesh Kumar
                                        #region Tiered FCR

                                        if (Credit_File != null && Credit_File.Count > 0)
                                        {

                                            List<string> lstJFCR = new List<string>();

                                            List<XElement> Personal_Data = Credit_File.Descendants("record").ToList();

                                            List<Region_Name> ObjRegion_Name = new List<Region_Name>();
                                            if (Personal_Data != null && Personal_Data.Count > 0)
                                            {
                                                foreach (XElement xe in Personal_Data)
                                                {
                                                    XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                    XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                    XElement Region = Address.Descendants("state").FirstOrDefault();
                                                    XElement County = Address.Descendants("county").FirstOrDefault();


                                                    XElement FName = xe.Descendants("firstname").FirstOrDefault();
                                                    XElement MName = xe.Descendants("middlename").FirstOrDefault();
                                                    XElement LName = xe.Descendants("lastname").FirstOrDefault();
                                                    XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                    if (EffectiveDate != null)
                                                    {
                                                        string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";
                                                        TiredSearchedData objTiered = new TiredSearchedData();
                                                        if (Region != null && County != null && LName != null && YearMonth != null)
                                                        {
                                                            var region_value = Region.Value.ToString();
                                                            int check_item = 1;
                                                            for (int i = 0; i < ObjRegion_Name.Count; i++)
                                                            {
                                                                if (ObjRegion_Name.ElementAt(i).Region.ToString() == region_value)
                                                                {
                                                                    check_item = 2;
                                                                }
                                                            }
                                                            if (check_item != 2)
                                                            {
                                                                string FirstName = string.Empty;
                                                                string LastName = string.Empty;
                                                                string MiddleName = string.Empty;
                                                                if (FName != null) { FirstName = FName.Value.ToString(); }
                                                                if (LName != null) { LastName = LName.Value.ToString(); }
                                                                if (MName != null) { MiddleName = MName.Value.ToString(); }




                                                                ObjRegion_Name.Add(new Region_Name() { Region = Region.Value.ToString(), Fname = FirstName, Lname = LastName, Mname = MiddleName });
                                                            }

                                                            var ArrayCollection = YearMonth.Split('-');
                                                            int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                           // int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                            if (TimeFrame == 0)//ticket #87
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                            else if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                        }
                                                    }
                                                }
                                                var duplicatesRemoved = ObjRegion_Name.Distinct().ToList();

                                                string firstname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                                                string lastname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;
                                                string middlename_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                List<State_Name> CollectionStateName = Objbalcompany.GetStateList(duplicatesRemoved, SearchedStateName, firstname_item, lastname_item, middlename_item);

                                                Guid pkProductIdfcr = Guid.Parse("0f0016cf-c214-44d6-9b41-cda20dee2251");
                                                List<JurisdictionColl> CollectionJudistration = Objbalcompany.GetJurisdictionList(CollectionStateName, pkProductIdfcr);




                                                if (CollectionJudistration != null && CollectionJudistration.Count > 0)
                                                {
                                                    var lst = searchedDataList1.ToList();
                                                    lst.ForEach(n => searchedDataList1.Add(new TiredSearchedData
                                                    {
                                                        county = n.county,
                                                        state = n.state,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName
                                                    }));
                                                    var searchedDataList = searchedDataList1.Select(n => new
                                                    {
                                                        county = n.county,
                                                        state = n.state,
                                                        mName = n.mName,
                                                        fName = n.fName,
                                                        lName = n.lName,
                                                        MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                    }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                    var lst_fcr = CollectionJudistration.ToList();
                                                    lst_fcr.ForEach(n => CollectionJudistration.Add(new JurisdictionColl
                                                    {
                                                        Jurisdiction_Name = n.Jurisdiction_Name,
                                                        pkJurisdictionId = n.pkJurisdictionId,
                                                        AdditionalFee = n.AdditionalFee,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName

                                                    }));
                                                    var CollectionJudistration_item = CollectionJudistration.Select(n => new
                                                    {
                                                        Jurisdiction_Name = n.Jurisdiction_Name,
                                                        pkJurisdictionId = n.pkJurisdictionId,
                                                        AdditionalFee = n.AdditionalFee,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName,
                                                        MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                    }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                    if (CollectionJudistration_item.Count > 0)
                                                    {
                                                        bool flagDisabled = false;
                                                        decimal ReportFee = GetReportFee(LocationId, CompanyUserId, "FCR", Tiered2ReportType, ObjEmergeReport.ObjTieredPackage.fkPackageId);
                                                        response.Append("<table id=\"tieredTable\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">");
                                                        response.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">Record(s) found : RecordCount" +
                                                            "</td></tr>");
                                                        response.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"100%\"><td width=\"20%\">Full Name</td><td width=\"20%\">Jurisdictions</td><td width=\"15%\">(ReportType)</td><td width=\"20%\">Cost</td> <td width=\"10%\"><input type=\"checkbox\" onclick=\"selectunselectCountyForFCR();\" id=\"chk_SelectAllCounty\"  />Select All</td><td width=\"15%\">Sub Total</td></tr>");
                                                        for (int i = 0; i < CollectionJudistration_item.Count; i++)
                                                        {
                                                            string JudistrationName = CollectionJudistration_item.ElementAt(i).Jurisdiction_Name;
                                                            var Judistrationfee = CollectionJudistration_item.ElementAt(i).AdditionalFee;
                                                            string fee = "$" + ReportFee.ToString(); ;

                                                            if (Judistrationfee == 0 || Judistrationfee == Convert.ToDecimal(0.00))
                                                            {
                                                            }
                                                            else
                                                            {
                                                                fee = fee + " <br /> Jurisdiction Fee" + " $" + Judistrationfee;
                                                            }
                                                            // string MatchMeterIcon = MatchMeterResult(searchedDataList.ElementAt(i).fName, searchedDataList.ElementAt(i).mName, searchedDataList.ElementAt(i).lName,
                                                            //                         ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName, ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial, ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName);

                                                            string MatchMeterIcon = CollectionJudistration_item.ElementAt(i).MatchingStatus == 1 ? "ico_match_green.jpg" : (CollectionJudistration_item.ElementAt(i).MatchingStatus == 2 ? "ico_red_yellow.png" : "ico_red_match.png");
                                                            decimal subTotal = ReportFee + Judistrationfee;
                                                            //string fullName = searchedDataList.ElementAt(i).mName == "" ? searchedDataList.ElementAt(i).fName : searchedDataList.ElementAt(i).fName + " " + searchedDataList.ElementAt(i).mName.Substring(0, 1);
                                                            // fullName = fullName + " " + searchedDataList.ElementAt(i).lName;

                                                            string fullName = CollectionJudistration_item.ElementAt(i).mName == "" ? CollectionJudistration_item.ElementAt(i).fName : CollectionJudistration_item.ElementAt(i).fName + " " + CollectionJudistration_item.ElementAt(i).mName.Substring(0, 1);
                                                            fullName = fullName + " " + CollectionJudistration_item.ElementAt(i).lName;




                                                            string sDispState = "";
                                                            response.Append("<tr><td><img title=\"Match Meter\" src=\"../Content/themes/base/images/" + MatchMeterIcon + "\">" + fullName + "</td>");
                                                            response.Append("<td>" + JudistrationName + "</td>");
                                                            //if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode != null)
                                                            //{
                                                            //    if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Contains("_"))
                                                            //        sDispState = "(" + ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Split('_').First() + ")";
                                                            //    else
                                                            //        sDispState = searchedDataList.ElementAt(i).state;
                                                            //}
                                                            //else
                                                            //{
                                                            //    sDispState = searchedDataList.ElementAt(i).state;
                                                            //}

                                                            // string MI = CollectionJudistration_item.ElementAt(i).mName != "" ? CollectionJudistration_item.ElementAt(i).mName : "*";
                                                            //INT-126
                                                            string MI = middlename_item != "" ? middlename_item : "*";
                                                            response.Append("<td>(FCR)</td>");
                                                            response.Append("<td>" + fee + "</td>");
                                                            if (i < searchedDataList.Count())
                                                            {

                                                                if (lstJFCR.Contains(JudistrationName))
                                                                {
                                                                    response.Append("<td>&nbsp;&nbsp;&nbsp;<span style=\"color:red;\">-</span></td>");
                                                                }
                                                                else
                                                                {
                                                                    response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk1\" type=\"checkbox\" id=\"chk_" + JudistrationName + "_County_" + firstname_item + "_" + MI + "_" + lastname_item + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                                    lstJFCR.Add(JudistrationName);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk1\" type=\"checkbox\" id=\"chk_" + JudistrationName + "_County_" + firstname_item + "_" + MI + "_" + lastname_item + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                            }
                                                            response.Append("<td><span class=\"subtotalClass\" id=\"subTotal_" + i.ToString() + "\">$" + subTotal + "</span></td>");
                                                            response.Append("</tr>");
                                                            ObjJurisdictionListColl.Add(new JurisdictionListColl() { AdditionalFee = decimal.Parse(CollectionJudistration_item.ElementAt(i).AdditionalFee.ToString()), pkJurisdictionId = int.Parse(CollectionJudistration_item.ElementAt(i).pkJurisdictionId.ToString()), Jurisdiction_Name = CollectionJudistration_item.ElementAt(i).Jurisdiction_Name.ToString(), fName = CollectionJudistration_item.ElementAt(i).fName, mName = CollectionJudistration_item.ElementAt(i).mName, lName = CollectionJudistration_item.ElementAt(i).lName });

                                                        }
                                                    }

                                                    response.Append("</table>").Replace("RecordCount", CollectionJudistration.Count.ToString());
                                                }
                                            }
                                        }
                                        #endregion

                                        response.Append("</td>");
                                        response.Append("<td style=\"border-right: 1px solid gray;vertical-align:top;\">");
                                        Tiered3ReportType = "Tiered3";

                                        if (sTiered3.Contains("CCR"))
                                        {
                                            //Changed By Himesh Kumar
                                            #region Tiered CCR1
                                            if (CreditFile != null && CreditFile.Count > 0)
                                            {

                                                List<string> lstCountyStateCCR = new List<string>();
                                                List<XElement> PersonalData = CreditFile.Descendants("record").ToList();


                                                if (PersonalData != null && PersonalData.Count > 0)
                                                {
                                                    foreach (XElement xe in PersonalData)
                                                    {
                                                        XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                        XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                        XElement Region = Address.Descendants("state").FirstOrDefault();
                                                        XElement County = Address.Descendants("county").FirstOrDefault();


                                                        XElement FName = xe.Descendants("firstname").FirstOrDefault();
                                                        XElement MName = xe.Descendants("middlename").FirstOrDefault();
                                                        XElement LName = xe.Descendants("lastname").FirstOrDefault();
                                                        XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                        if (EffectiveDate != null)
                                                        {
                                                            string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";

                                                            TiredSearchedData objTiered = new TiredSearchedData();
                                                            if (Region != null && County != null && LName != null && YearMonth != null)
                                                            {
                                                                var ArrayCollection = YearMonth.Split('-');
                                                                int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                               // int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                                if (TimeFrame == 0)//ticket #87
                                                                {
                                                                    objTiered.county = County.Value;
                                                                    objTiered.state = Region.Value;
                                                                    objTiered.fName = FName != null ? FName.Value : "";
                                                                    objTiered.mName = MName != null ? MName.Value : "";
                                                                    objTiered.lName = LName.Value;
                                                                    searchedDataList1.Add(objTiered);
                                                                }
                                                                else if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                                {
                                                                    objTiered.county = County.Value;
                                                                    objTiered.state = Region.Value;
                                                                    objTiered.fName = FName != null ? FName.Value : "";
                                                                    objTiered.mName = MName != null ? MName.Value : "";
                                                                    objTiered.lName = LName.Value;
                                                                    searchedDataList1.Add(objTiered);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    if (searchedDataList1 != null && searchedDataList1.Count > 0)
                                                    {
                                                        var lst = searchedDataList1.Where(n => n.county.Contains("DALLAS")).ToList();
                                                        lst.ForEach(n => searchedDataList1.Add(new TiredSearchedData
                                                        {
                                                            county = "DALLAS METRO",
                                                            state = "TX",
                                                            fName = n.fName,
                                                            mName = n.mName,
                                                            lName = n.lName
                                                        }));

                                                        var searchedDataList = searchedDataList1.Select(n => new
                                                        {
                                                            county = n.county,
                                                            state = n.state,
                                                            mName = n.mName,
                                                            fName = n.fName,
                                                            lName = n.lName,
                                                            MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                        }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                        if (searchedDataList.Count > 0)
                                                        {
                                                            bool flagDisabled = false;
                                                            decimal ReportFee = GetReportFee(LocationId, CompanyUserId, "CCR1", Tiered3ReportType, ObjEmergeReport.ObjTieredPackage.fkPackageId);
                                                            response.Append("<table id=\"tieredTable\" cellpadding=\"3\" cellspacing=\"0\" width=\"100%\">");
                                                            response.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">Record(s) found : RecordCount" +
                                                                "</td></tr>");
                                                            response.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"100%\"><td width=\"20%\">Full Name</td><td width=\"20%\">County[State]</td><td width=\"15%\">(ReportType)</td><td width=\"20%\">Cost</td> <td width=\"10%\"><input type=\"checkbox\" onclick=\"selectunselectCountyCCRTiered();\" id=\"chk_SelectAllCountyCCRTiered\"  />Select All</td><td width=\"15%\">Sub Total</td></tr>");
                                                            for (int i = 0; i < searchedDataList.Count; i++)
                                                            {
                                                                string CountyName = searchedDataList.ElementAt(i).county.ToLower().Replace("st.", "saint");
                                                                var Countyfee = Objbalcompanytype.Get_County_Fee(CountyName, searchedDataList.ElementAt(i).state, LocationId, "CCR1");
                                                                string fee = "$" + ReportFee.ToString(); ;

                                                                if (Countyfee == 0 || Countyfee == Convert.ToDecimal(0.00))
                                                                {
                                                                }
                                                                else
                                                                {
                                                                    fee = fee + " <br /> County Fee" + " $" + Countyfee;
                                                                }


                                                                string MatchMeterIcon = searchedDataList.ElementAt(i).MatchingStatus == 1 ? "ico_match_green.jpg" : (searchedDataList.ElementAt(i).MatchingStatus == 2 ? "ico_red_yellow.png" : "ico_red_match.png");
                                                                decimal subTotal = ReportFee + Countyfee;
                                                                string fullName = searchedDataList.ElementAt(i).mName == "" ? searchedDataList.ElementAt(i).fName : searchedDataList.ElementAt(i).fName + " " + searchedDataList.ElementAt(i).mName.Substring(0, 1);
                                                                fullName = fullName + " " + searchedDataList.ElementAt(i).lName;
                                                                string sDispState = "";
                                                                response.Append("<tr><td><img title=\"Match Meter\" src=\"../Content/themes/base/images/" + MatchMeterIcon + "\">" + fullName + "</td>");
                                                                response.Append("<td>" + searchedDataList.ElementAt(i).county + ", [" + searchedDataList.ElementAt(i).state + "]" +
                                                                    "</td>");
                                                                if (ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode != null)
                                                                {
                                                                    if (ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode.Contains("_"))
                                                                        sDispState = "(" + ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode.Split('_').First() + ")";
                                                                    else
                                                                        sDispState = searchedDataList.ElementAt(i).state;
                                                                }
                                                                else
                                                                {
                                                                    sDispState = searchedDataList.ElementAt(i).state;
                                                                }

                                                                //INT-126
                                                                //INT-126 See Comments on Jira
                                                                string firstname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                                                                string lastname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;
                                                                string middlename_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                                //string MI = searchedDataList.ElementAt(i).mName != "" ? searchedDataList.ElementAt(i).mName : "*";
                                                                string MI = middlename_item != "" ? middlename_item : "*";
                                                                response.Append("<td>" + sDispState + "</td>");
                                                                response.Append("<td>" + fee + "</td>");
                                                                if (i < searchedDataList.Count())
                                                                {
                                                                    if (lstCountyStateCCR.Contains(searchedDataList.ElementAt(i).county + ", [" + searchedDataList.ElementAt(i).state + "]"))
                                                                    {
                                                                        response.Append("<td>&nbsp;&nbsp;&nbsp;<span style=\"color:red;\">-</span></td>");
                                                                    }
                                                                    else
                                                                    {
                                                                        flagDisabled = false;
                                                                        response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk\" type=\"checkbox\" id=\"chk_" + CountyName + "_" + searchedDataList.ElementAt(i).state + "_County_" + firstname_item + "_" + MI + "_" + lastname_item + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                                        lstCountyStateCCR.Add(searchedDataList.ElementAt(i).county + ", [" + searchedDataList.ElementAt(i).state + "]");
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk\" type=\"checkbox\" id=\"chk_" + CountyName + "_" + searchedDataList.ElementAt(i).state + "_County_" + firstname_item + "_" + MI + "_" + lastname_item + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                                }
                                                                response.Append("<td><span class=\"subtotalClass\" id=\"subTotal_" + i.ToString() + "\">$" + subTotal + "</span></td>");
                                                                response.Append("</tr>");
                                                            }

                                                        }

                                                        response.Append("</table>").Replace("RecordCount", searchedDataList.Count.ToString());
                                                    }
                                                }
                                            }
                                            #endregion
                                        }
                                        else if (sTiered3.Contains("SCR"))
                                        {
                                            //Changed By Himesh Kumar
                                            #region Tiered SCR
                                            if (CreditFile != null && CreditFile.Count > 0)
                                            {
                                                List<XElement> PersonalData = CreditFile.Descendants("record").ToList();

                                                List<string> lstStateSCR = new List<string>();
                                                if (PersonalData != null && PersonalData.Count > 0)
                                                {
                                                    foreach (XElement xe in PersonalData)
                                                    {
                                                        XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                        XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                        XElement Region = Address.Descendants("state").FirstOrDefault();
                                                        XElement County = Address.Descendants("county").FirstOrDefault();


                                                        XElement FName = xe.Descendants("firstname").FirstOrDefault();
                                                        XElement MName = xe.Descendants("middlename").FirstOrDefault();
                                                        XElement LName = xe.Descendants("lastname").FirstOrDefault();
                                                        XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                        if (EffectiveDate != null)
                                                        {
                                                            string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";

                                                            TiredSearchedData objTiered = new TiredSearchedData();
                                                            if (Region != null && County != null && LName != null && YearMonth != null)
                                                            {
                                                                var ArrayCollection = YearMonth.Split('-');
                                                                int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                                //int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                                if (TimeFrame == 0)//ticket #87
                                                                {
                                                                    objTiered.county = County.Value;
                                                                    objTiered.state = Region.Value;
                                                                    objTiered.fName = FName != null ? FName.Value : "";
                                                                    objTiered.mName = MName != null ? MName.Value : "";
                                                                    objTiered.lName = LName.Value;
                                                                    searchedDataList1.Add(objTiered);
                                                                }
                                                                else if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                                {
                                                                    objTiered.county = County.Value;
                                                                    objTiered.state = Region.Value;
                                                                    objTiered.fName = FName != null ? FName.Value : "";
                                                                    objTiered.mName = MName != null ? MName.Value : "";
                                                                    objTiered.lName = LName.Value;
                                                                    searchedDataList1.Add(objTiered);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    if (searchedDataList1 != null && searchedDataList1.Count > 0)
                                                    {
                                                        var lst = searchedDataList1.Where(n => n.county.Contains("DALLAS")).ToList();
                                                        lst.ForEach(n => searchedDataList1.Add(new TiredSearchedData
                                                        {
                                                            county = "DALLAS METRO",
                                                            state = "TX",
                                                            fName = n.fName,
                                                            mName = n.mName,
                                                            lName = n.lName
                                                        }));

                                                        var searchedDataList = searchedDataList1.Select(n => new
                                                        {
                                                            county = n.county,
                                                            state = n.state,
                                                            mName = n.mName,
                                                            fName = n.fName,
                                                            lName = n.lName,
                                                            MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                        }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();

                                                        string[] StateArray = new BALGeneral().GetDisabledStates().Select(d => d.StateCode).ToArray();

                                                        searchedDataList = searchedDataList.Where(s => !StateArray.Contains(s.state)).ToList();
                                                        if (searchedDataList.Count > 0)
                                                        {
                                                            bool flagDisabled = false;
                                                            decimal ReportFee = GetReportFee(LocationId, CompanyUserId, "SCR", "Tiered", ObjEmergeReport.ObjTieredPackage.fkPackageId);
                                                            response.Append("<table id=\"tieredTable\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">");
                                                            response.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">Record(s) found : RecordCount" +
                                                                "</td></tr>");
                                                            response.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"100%\"><td width=\"20%\">Full Name</td><td width=\"20%\">State</td><td width=\"15%\">(ReportType)</td><td width=\"20%\">Cost</td> <td width=\"10%\"><input type=\"checkbox\" onclick=\"selectunselectCountySCR();\" id=\"chk_SelectAllStates\"  />Select All</td><td width=\"15%\">Sub Total</td></tr>");
                                                            for (int i = 0; i < searchedDataList.Count; i++)
                                                            {
                                                                string CountyName = searchedDataList.ElementAt(i).county.ToLower().Replace("st.", "saint");

                                                                List<tblState> lstStateCode = new BALGeneral().GetStates();
                                                                string StateNameval = lstStateCode.Where(st => st.StateCode == searchedDataList.ElementAt(i).state).Select(st => st.StateName).FirstOrDefault();
                                                                var Countyfee = 0;
                                                                int stateFees = 0;
                                                                if (!string.IsNullOrEmpty(StateNameval))
                                                                {
                                                                    string stFee = Objbalcompanytype.Get_State_Fee(StateNameval);

                                                                    int.TryParse(stFee, out stateFees);
                                                                }
                                                                Countyfee = stateFees;

                                                                string fee = "$" + ReportFee.ToString(); ;

                                                                if (Countyfee == 0 || Countyfee == Convert.ToDecimal(0.00))
                                                                {
                                                                }
                                                                else
                                                                {
                                                                    fee = fee + " <br />State Fee" + " $" + Countyfee;
                                                                }
                                                                // string MatchMeterIcon = MatchMeterResult(searchedDataList.ElementAt(i).fName, searchedDataList.ElementAt(i).mName, searchedDataList.ElementAt(i).lName,
                                                                //                         ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName, ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial, ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName);

                                                                string MatchMeterIcon = searchedDataList.ElementAt(i).MatchingStatus == 1 ? "ico_match_green.jpg" : (searchedDataList.ElementAt(i).MatchingStatus == 2 ? "ico_red_yellow.png" : "ico_red_match.png");
                                                                decimal subTotal = ReportFee + Countyfee;
                                                                string fullName = searchedDataList.ElementAt(i).mName == "" ? searchedDataList.ElementAt(i).fName : searchedDataList.ElementAt(i).fName + " " + searchedDataList.ElementAt(i).mName.Substring(0, 1);
                                                                fullName = fullName + " " + searchedDataList.ElementAt(i).lName;
                                                                if (lstStateSCR.Contains(searchedDataList.ElementAt(i).state))
                                                                {
                                                                    flagDisabled = true;
                                                                }
                                                                else
                                                                {
                                                                    flagDisabled = false;
                                                                    lstStateSCR.Add(searchedDataList.ElementAt(i).state);
                                                                }
                                                                response.Append("<tr><td><img title=\"Match Meter\" src=\"../Content/themes/base/images/" + MatchMeterIcon + "\">" + fullName + "</td>");

                                                                string sDispState = "";

                                                                response.Append("<td>" + searchedDataList.ElementAt(i).state + "</td>");

                                                                sDispState = "(SCR)";
                                                                //INT-126 See Comments on Jira
                                                                string firstname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                                                                string lastname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;
                                                                string middlename_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                                string MI = middlename_item != "" ? middlename_item : "*";

                                                                //string MI = searchedDataList.ElementAt(i).mName != "" ? searchedDataList.ElementAt(i).mName : "*";
                                                                response.Append("<td>" + sDispState + "</td>");
                                                                response.Append("<td>" + fee + "</td>");
                                                                //Ticket#179
                                                                if (flagDisabled)
                                                                {
                                                                    response.Append("<td>&nbsp;&nbsp;&nbsp;<span style=\"color:red;\">-</span></td>");
                                                                }
                                                                else
                                                                {
                                                                    response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk\" type=\"checkbox\" id=\"chk_" + CountyName + "_" + searchedDataList.ElementAt(i).state + "_County_" + firstname_item + "_" + MI + "_" + lastname_item + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                                }

                                                                // subTotal = 10;//Just for testing
                                                                response.Append("<td><span class=\"subtotalClass\" id=\"subTotal_" + i.ToString() + "\">$" + subTotal + "</span></td>");
                                                                response.Append("</tr>");
                                                            }

                                                        }
                                                        if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode == null)
                                                        {
                                                            response.Append("</table>").Replace("RecordCount", searchedDataList.Count.ToString());
                                                        }

                                                    }
                                                }
                                            }
                                            #endregion
                                        }

                                        response.Append("</td>");
                                        response.Append("</tr>");
                                        response.Append("</table>");


                                    }
                                    else
                                    {
                                        Tiered2ReportType = "Tiered2";

                                        response.Append("<table style=\"width:100%;\">");
                                        response.Append("<tr>");
                                        response.Append("<td style=\"width:50%;border-left: 1px solid gray;border-right: 1px solid gray;\">");

                                        //Changed By Himesh Kumar
                                        #region Tiered FCR
                                        Tiered3ReportType = "Tiered3";

                                        if (Credit_File != null && Credit_File.Count > 0)
                                        {
                                            List<string> lstJFCRT = new List<string>();
                                            List<XElement> Personal_Data = Credit_File.Descendants("record").ToList();

                                            List<Region_Name> ObjRegion_Name = new List<Region_Name>();
                                            if (Personal_Data != null && Personal_Data.Count > 0)
                                            {
                                                foreach (XElement xe in Personal_Data)
                                                {
                                                    XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                    XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                    XElement Region = Address.Descendants("state").FirstOrDefault();
                                                    XElement County = Address.Descendants("county").FirstOrDefault();


                                                    XElement FName = xe.Descendants("firstname").FirstOrDefault();
                                                    XElement MName = xe.Descendants("middlename").FirstOrDefault();
                                                    XElement LName = xe.Descendants("lastname").FirstOrDefault();
                                                    XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                    if (EffectiveDate != null)
                                                    {
                                                        string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";
                                                        TiredSearchedData objTiered = new TiredSearchedData();
                                                        if (Region != null && County != null && LName != null && YearMonth != null)
                                                        {
                                                            var region_value = Region.Value.ToString();
                                                            int check_item = 1;
                                                            for (int i = 0; i < ObjRegion_Name.Count; i++)
                                                            {
                                                                if (ObjRegion_Name.ElementAt(i).Region.ToString() == region_value)
                                                                {
                                                                    check_item = 2;
                                                                }
                                                            }
                                                            if (check_item != 2)
                                                            {
                                                                string FirstName = string.Empty;
                                                                string LastName = string.Empty;
                                                                string MiddleName = string.Empty;
                                                                if (FName != null) { FirstName = FName.Value.ToString(); }
                                                                if (LName != null) { LastName = LName.Value.ToString(); }
                                                                if (MName != null) { MiddleName = MName.Value.ToString(); }




                                                                ObjRegion_Name.Add(new Region_Name() { Region = Region.Value.ToString(), Fname = FirstName, Lname = LastName, Mname = MiddleName });
                                                            }

                                                            var ArrayCollection = YearMonth.Split('-');
                                                            int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                            //int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                            if (TimeFrame == 0)//ticket #87
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                            else if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                        }
                                                    }
                                                }
                                                var duplicatesRemoved = ObjRegion_Name.Distinct().ToList();

                                                string firstname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                                                string lastname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;
                                                string middlename_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                List<State_Name> CollectionStateName = Objbalcompany.GetStateList(duplicatesRemoved, SearchedStateName, firstname_item, lastname_item, middlename_item);

                                                Guid pkProductIdfcr = Guid.Parse("0f0016cf-c214-44d6-9b41-cda20dee2251");
                                                List<JurisdictionColl> CollectionJudistration = Objbalcompany.GetJurisdictionList(CollectionStateName, pkProductIdfcr);




                                                if (CollectionJudistration != null && CollectionJudistration.Count > 0)
                                                {
                                                    var lst = searchedDataList1.ToList();
                                                    lst.ForEach(n => searchedDataList1.Add(new TiredSearchedData
                                                    {
                                                        county = n.county,
                                                        state = n.state,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName
                                                    }));
                                                    var searchedDataList = searchedDataList1.Select(n => new
                                                    {
                                                        county = n.county,
                                                        state = n.state,
                                                        mName = n.mName,
                                                        fName = n.fName,
                                                        lName = n.lName,
                                                        MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                    }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                    var lst_fcr = CollectionJudistration.ToList();
                                                    lst_fcr.ForEach(n => CollectionJudistration.Add(new JurisdictionColl
                                                    {
                                                        Jurisdiction_Name = n.Jurisdiction_Name,
                                                        pkJurisdictionId = n.pkJurisdictionId,
                                                        AdditionalFee = n.AdditionalFee,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName

                                                    }));
                                                    var CollectionJudistration_item = CollectionJudistration.Select(n => new
                                                    {
                                                        Jurisdiction_Name = n.Jurisdiction_Name,
                                                        pkJurisdictionId = n.pkJurisdictionId,
                                                        AdditionalFee = n.AdditionalFee,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName,
                                                        MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                    }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                    if (CollectionJudistration_item.Count > 0)
                                                    {
                                                        bool flagDisabled = false;
                                                        decimal ReportFee = GetReportFee(LocationId, CompanyUserId, "FCR", Tiered3ReportType, ObjEmergeReport.ObjTieredPackage.fkPackageId);
                                                        response.Append("<table id=\"tieredTable\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">");
                                                        response.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">Record(s) found : RecordCount" +
                                                            "</td></tr>");
                                                        response.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"100%\"><td width=\"20%\">Full Name</td><td width=\"20%\">Jurisdictions</td><td width=\"15%\">(ReportType)</td><td width=\"20%\">Cost</td> <td width=\"10%\"><input type=\"checkbox\" onclick=\"selectunselectCountyForFCR();\" id=\"chk_SelectAllCounty\"  />Select All</td><td width=\"15%\">Sub Total</td></tr>");
                                                        for (int i = 0; i < CollectionJudistration_item.Count; i++)
                                                        {
                                                            string JudistrationName = CollectionJudistration_item.ElementAt(i).Jurisdiction_Name;
                                                            var Judistrationfee = CollectionJudistration_item.ElementAt(i).AdditionalFee;
                                                            string fee = "$" + ReportFee.ToString(); ;

                                                            if (Judistrationfee == 0 || Judistrationfee == Convert.ToDecimal(0.00))
                                                            {
                                                            }
                                                            else
                                                            {
                                                                fee = fee + " <br /> Jurisdiction Fee" + " $" + Judistrationfee;
                                                            }
                                                            // string MatchMeterIcon = MatchMeterResult(searchedDataList.ElementAt(i).fName, searchedDataList.ElementAt(i).mName, searchedDataList.ElementAt(i).lName,
                                                            //                         ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName, ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial, ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName);

                                                            string MatchMeterIcon = CollectionJudistration_item.ElementAt(i).MatchingStatus == 1 ? "ico_match_green.jpg" : (CollectionJudistration_item.ElementAt(i).MatchingStatus == 2 ? "ico_red_yellow.png" : "ico_red_match.png");
                                                            decimal subTotal = ReportFee + Judistrationfee;
                                                            //string fullName = searchedDataList.ElementAt(i).mName == "" ? searchedDataList.ElementAt(i).fName : searchedDataList.ElementAt(i).fName + " " + searchedDataList.ElementAt(i).mName.Substring(0, 1);
                                                            // fullName = fullName + " " + searchedDataList.ElementAt(i).lName;

                                                            string fullName = CollectionJudistration_item.ElementAt(i).mName == "" ? CollectionJudistration_item.ElementAt(i).fName : CollectionJudistration_item.ElementAt(i).fName + " " + CollectionJudistration_item.ElementAt(i).mName.Substring(0, 1);
                                                            fullName = fullName + " " + CollectionJudistration_item.ElementAt(i).lName;




                                                            string sDispState = "";
                                                            response.Append("<tr><td><img title=\"Match Meter\" src=\"../Content/themes/base/images/" + MatchMeterIcon + "\">" + fullName + "</td>");
                                                            response.Append("<td>" + JudistrationName + "</td>");
                                                            if (ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode != null)
                                                            {
                                                                if (ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode.Contains("_"))
                                                                    sDispState = "(" + ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode.Split('_').First() + ")";
                                                                else
                                                                    sDispState = searchedDataList.ElementAt(i).state;
                                                            }
                                                            else
                                                            {
                                                                sDispState = searchedDataList.ElementAt(i).state;
                                                            }
                                                            //string MI = CollectionJudistration_item.ElementAt(i).mName != "" ? CollectionJudistration_item.ElementAt(i).mName : "*";
                                                            //INT-126 See Comments on Jira
                                                            //string MI = searchedDataList.ElementAt(i).mName != "" ? searchedDataList.ElementAt(i).mName : "*";
                                                            string MI = middlename_item != "" ? middlename_item : "*";
                                                            response.Append("<td>" + sDispState + "</td>");
                                                            response.Append("<td>" + fee + "</td>");
                                                            if (i < searchedDataList.Count())
                                                            {

                                                                if (lstJFCRT.Contains(JudistrationName))
                                                                {
                                                                    response.Append("<td>&nbsp;&nbsp;&nbsp;<span style=\"color:red;\">-</span></td>");
                                                                }
                                                                else
                                                                {
                                                                    response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk1\" type=\"checkbox\" id=\"chk_" + JudistrationName + "_County_" + firstname_item + "_" + MI + "_" + lastname_item + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                                    lstJFCRT.Add(JudistrationName);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk1\" type=\"checkbox\" id=\"chk_" + JudistrationName + "_County_" + firstname_item + "_" + MI + "_" + lastname_item + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                            }
                                                            response.Append("<td><span class=\"subtotalClass\" id=\"subTotal_" + i.ToString() + "\">$" + subTotal + "</span></td>");
                                                            response.Append("</tr>");
                                                            ObjJurisdictionListColl.Add(new JurisdictionListColl() { AdditionalFee = decimal.Parse(CollectionJudistration_item.ElementAt(i).AdditionalFee.ToString()), pkJurisdictionId = int.Parse(CollectionJudistration_item.ElementAt(i).pkJurisdictionId.ToString()), Jurisdiction_Name = CollectionJudistration_item.ElementAt(i).Jurisdiction_Name.ToString(), fName = CollectionJudistration_item.ElementAt(i).fName, mName = CollectionJudistration_item.ElementAt(i).mName, lName = CollectionJudistration_item.ElementAt(i).lName });

                                                        }
                                                    }

                                                    response.Append("</table>").Replace("RecordCount", CollectionJudistration.Count.ToString());
                                                }
                                            }
                                        }

                                        #endregion

                                        response.Append("</td>");
                                        response.Append("<td style=\"border-right: 1px solid gray;vertical-align:top;\">");


                                        //Changed By Himesh Kumar
                                        #region Tiered CCR1


                                        if (CreditFile != null && CreditFile.Count > 0)
                                        {
                                            List<XElement> PersonalData = CreditFile.Descendants("record").ToList();
                                            List<string> lstCountyState = new List<string>();
                                            if (PersonalData != null && PersonalData.Count > 0)
                                            {
                                                foreach (XElement xe in PersonalData)
                                                {
                                                    XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                    XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                    XElement Region = Address.Descendants("state").FirstOrDefault();
                                                    XElement County = Address.Descendants("county").FirstOrDefault();


                                                    XElement FName = xe.Descendants("firstname").FirstOrDefault();
                                                    XElement MName = xe.Descendants("middlename").FirstOrDefault();
                                                    XElement LName = xe.Descendants("lastname").FirstOrDefault();
                                                    XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                    if (EffectiveDate != null)
                                                    {
                                                        string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";

                                                        TiredSearchedData objTiered = new TiredSearchedData();
                                                        if (Region != null && County != null && LName != null && YearMonth != null)
                                                        {
                                                            var ArrayCollection = YearMonth.Split('-');
                                                            int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                            //int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                            if (TimeFrame == 0)//ticket #87
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                            else if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                        }
                                                    }
                                                }
                                                if (searchedDataList1 != null && searchedDataList1.Count > 0)
                                                {
                                                    var lst = searchedDataList1.Where(n => n.county.Contains("DALLAS")).ToList();
                                                    lst.ForEach(n => searchedDataList1.Add(new TiredSearchedData
                                                    {
                                                        county = "DALLAS METRO",
                                                        state = "TX",
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName
                                                    }));

                                                    var searchedDataList = searchedDataList1.Select(n => new
                                                    {
                                                        county = n.county,
                                                        state = n.state,
                                                        mName = n.mName,
                                                        fName = n.fName,
                                                        lName = n.lName,
                                                        MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                    }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                    if (searchedDataList.Count > 0)
                                                    {
                                                        bool flagDisabled = false;
                                                        decimal ReportFee = GetReportFee(LocationId, CompanyUserId, "CCR1", Tiered2ReportType, ObjEmergeReport.ObjTieredPackage.fkPackageId);
                                                        response.Append("<table id=\"tieredTableCCr\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">");
                                                        response.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">Record(s) found : RecordCount" +
                                                            "</td></tr>");
                                                        response.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"100%\"><td width=\"20%\">Full Name</td><td width=\"20%\">County[State]</td><td width=\"15%\">(ReportType)</td><td width=\"20%\">Cost</td> <td width=\"10%\"><input type=\"checkbox\" onclick=\"selectunselectCountyCCR();\" id=\"chk_SelectAllCountyCCR1\"  />Select All</td><td width=\"15%\">Sub Total</td></tr>");
                                                        for (int i = 0; i < searchedDataList.Count; i++)
                                                        {
                                                            string CountyName = searchedDataList.ElementAt(i).county.ToLower().Replace("st.", "saint");
                                                            var Countyfee = Objbalcompanytype.Get_County_Fee(CountyName, searchedDataList.ElementAt(i).state, LocationId, "CCR1");
                                                            string fee = "$" + ReportFee.ToString(); ;

                                                            if (Countyfee == 0 || Countyfee == Convert.ToDecimal(0.00))
                                                            {
                                                            }
                                                            else
                                                            {
                                                                fee = fee + " <br /> County Fee" + " $" + Countyfee;
                                                            }


                                                            string MatchMeterIcon = searchedDataList.ElementAt(i).MatchingStatus == 1 ? "ico_match_green.jpg" : (searchedDataList.ElementAt(i).MatchingStatus == 2 ? "ico_red_yellow.png" : "ico_red_match.png");
                                                            decimal subTotal = ReportFee + Countyfee;
                                                            string fullName = searchedDataList.ElementAt(i).mName == "" ? searchedDataList.ElementAt(i).fName : searchedDataList.ElementAt(i).fName + " " + searchedDataList.ElementAt(i).mName.Substring(0, 1);
                                                            fullName = fullName + " " + searchedDataList.ElementAt(i).lName;
                                                            string sDispState = "";
                                                            response.Append("<tr><td><img title=\"Match Meter\" src=\"../Content/themes/base/images/" + MatchMeterIcon + "\">" + fullName + "</td>");
                                                            response.Append("<td>" + searchedDataList.ElementAt(i).county + ", [" + searchedDataList.ElementAt(i).state + "]" +
                                                                "</td>");
                                                            if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode != null)
                                                            {
                                                                if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Contains("_"))
                                                                    sDispState = "(" + ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Split('_').First() + ")";
                                                                else
                                                                    sDispState = searchedDataList.ElementAt(i).state;
                                                            }
                                                            else
                                                            {
                                                                sDispState = searchedDataList.ElementAt(i).state;
                                                            }

                                                            //INT-126 See Comments on Jira
                                                            string firstname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                                                            string lastname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;
                                                            string middlename_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                            string MI = middlename_item != "" ? middlename_item : "*";

                                                            //string MI = searchedDataList.ElementAt(i).mName != "" ? searchedDataList.ElementAt(i).mName : "*";
                                                            response.Append("<td>" + sDispState + "</td>");
                                                            response.Append("<td>" + fee + "</td>");

                                                            if (i < searchedDataList.Count())
                                                            {
                                                                if (lstCountyState.Contains(searchedDataList.ElementAt(i).county + ", [" + searchedDataList.ElementAt(i).state + "]"))
                                                                {
                                                                    response.Append("<td>&nbsp;&nbsp;&nbsp;<span style=\"color:red;\">-</span></td>");
                                                                }
                                                                else
                                                                {
                                                                    response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk\" type=\"checkbox\" id=\"chk_" + CountyName + "_" + searchedDataList.ElementAt(i).state + "_County_" + firstname_item + "_" + MI + "_" + lastname_item + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                                    lstCountyState.Add(searchedDataList.ElementAt(i).county + ", [" + searchedDataList.ElementAt(i).state + "]");
                                                                }

                                                            }
                                                            else
                                                            {
                                                                response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk\" type=\"checkbox\" id=\"chk_" + CountyName + "_" + searchedDataList.ElementAt(i).state + "_County_" + firstname_item + "_" + MI + "_" + lastname_item + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                            }
                                                            response.Append("<td><span class=\"subtotalClass\" id=\"subTotalCCR_" + i.ToString() + "\">$" + subTotal + "</span></td>");
                                                            response.Append("</tr>");
                                                        }

                                                    }

                                                    response.Append("</table>").Replace("RecordCount", searchedDataList.Count.ToString());
                                                }
                                            }
                                        }
                                        #endregion

                                        response.Append("</td>");
                                        response.Append("</tr>");
                                        response.Append("</table>");
                                    }

                                    #endregion

                                }
                                else
                                {
                                    if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Contains("FCR"))
                                    {
                                        //Changed By Himesh Kumar
                                        #region Tiered FCR

                                        if (Credit_File != null && Credit_File.Count > 0)
                                        {
                                            List<XElement> Personal_Data = Credit_File.Descendants("record").ToList();
                                            List<string> lstJT2 = new List<string>();
                                            List<Region_Name> ObjRegion_Name = new List<Region_Name>();
                                            if (Personal_Data != null && Personal_Data.Count > 0)
                                            {
                                                foreach (XElement xe in Personal_Data)
                                                {
                                                    XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                    XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                    XElement Region = Address.Descendants("state").FirstOrDefault();
                                                    XElement County = Address.Descendants("county").FirstOrDefault();


                                                    XElement FName = xe.Descendants("firstname").FirstOrDefault();
                                                    XElement MName = xe.Descendants("middlename").FirstOrDefault();
                                                    XElement LName = xe.Descendants("lastname").FirstOrDefault();
                                                    XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                    if (EffectiveDate != null)
                                                    {
                                                        string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";
                                                        TiredSearchedData objTiered = new TiredSearchedData();
                                                        if (Region != null && County != null && LName != null && YearMonth != null)
                                                        {
                                                            var region_value = Region.Value.ToString();
                                                            int check_item = 1;
                                                            for (int i = 0; i < ObjRegion_Name.Count; i++)
                                                            {
                                                                if (ObjRegion_Name.ElementAt(i).Region.ToString() == region_value)
                                                                {
                                                                    check_item = 2;
                                                                }
                                                            }
                                                            if (check_item != 2)
                                                            {
                                                                string FirstName = string.Empty;
                                                                string LastName = string.Empty;
                                                                string MiddleName = string.Empty;
                                                                if (FName != null) { FirstName = FName.Value.ToString(); }
                                                                if (LName != null) { LastName = LName.Value.ToString(); }
                                                                if (MName != null) { MiddleName = MName.Value.ToString(); }




                                                                ObjRegion_Name.Add(new Region_Name() { Region = Region.Value.ToString(), Fname = FirstName, Lname = LastName, Mname = MiddleName });
                                                            }

                                                            var ArrayCollection = YearMonth.Split('-');
                                                            int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                           // int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                            if (TimeFrame == 0)//ticket #87
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                            else if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                        }
                                                    }
                                                }
                                                var duplicatesRemoved = ObjRegion_Name.Distinct().ToList();

                                                string firstname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                                                string lastname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;
                                                string middlename_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                List<State_Name> CollectionStateName = Objbalcompany.GetStateList(duplicatesRemoved, SearchedStateName, firstname_item, lastname_item, middlename_item);

                                                Guid pkProductIdfcr = Guid.Parse("0f0016cf-c214-44d6-9b41-cda20dee2251");
                                                List<JurisdictionColl> CollectionJudistration = Objbalcompany.GetJurisdictionList(CollectionStateName, pkProductIdfcr);




                                                if (CollectionJudistration != null && CollectionJudistration.Count > 0)
                                                {
                                                    var lst = searchedDataList1.ToList();
                                                    lst.ForEach(n => searchedDataList1.Add(new TiredSearchedData
                                                    {
                                                        county = n.county,
                                                        state = n.state,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName
                                                    }));
                                                    var searchedDataList = searchedDataList1.Select(n => new
                                                    {
                                                        county = n.county,
                                                        state = n.state,
                                                        mName = n.mName,
                                                        fName = n.fName,
                                                        lName = n.lName,
                                                        MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                    }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                    var lst_fcr = CollectionJudistration.ToList();
                                                    lst_fcr.ForEach(n => CollectionJudistration.Add(new JurisdictionColl
                                                    {
                                                        Jurisdiction_Name = n.Jurisdiction_Name,
                                                        pkJurisdictionId = n.pkJurisdictionId,
                                                        AdditionalFee = n.AdditionalFee,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName

                                                    }));
                                                    var CollectionJudistration_item = CollectionJudistration.Select(n => new
                                                    {
                                                        Jurisdiction_Name = n.Jurisdiction_Name,
                                                        pkJurisdictionId = n.pkJurisdictionId,
                                                        AdditionalFee = n.AdditionalFee,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName,
                                                        MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                    }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                    if (CollectionJudistration_item.Count > 0)
                                                    {
                                                        bool flagDisabled = false;
                                                        decimal ReportFee = GetReportFee(LocationId, CompanyUserId, "FCR", "Tiered", ObjEmergeReport.ObjTieredPackage.fkPackageId);
                                                        response.Append("<table id=\"tieredTable\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">");
                                                        response.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">Record(s) found : RecordCount" +
                                                            "</td></tr>");
                                                        response.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"100%\"><td width=\"20%\">Full Name</td><td width=\"20%\">Jurisdictions</td><td width=\"15%\">(ReportType)</td><td width=\"20%\">Cost</td> <td width=\"10%\"><input type=\"checkbox\" onclick=\"selectunselectCountyForFCR();\" id=\"chk_SelectAllCounty\"  />Select All</td><td width=\"15%\">Sub Total</td></tr>");
                                                        for (int i = 0; i < CollectionJudistration_item.Count; i++)
                                                        {
                                                            string JudistrationName = CollectionJudistration_item.ElementAt(i).Jurisdiction_Name;
                                                            var Judistrationfee = CollectionJudistration_item.ElementAt(i).AdditionalFee;
                                                            string fee = "$" + ReportFee.ToString(); ;

                                                            if (Judistrationfee == 0 || Judistrationfee == Convert.ToDecimal(0.00))
                                                            {
                                                            }
                                                            else
                                                            {
                                                                fee = fee + " <br /> Jurisdiction Fee" + " $" + Judistrationfee;
                                                            }
                                                            // string MatchMeterIcon = MatchMeterResult(searchedDataList.ElementAt(i).fName, searchedDataList.ElementAt(i).mName, searchedDataList.ElementAt(i).lName,
                                                            //                         ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName, ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial, ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName);

                                                            string MatchMeterIcon = CollectionJudistration_item.ElementAt(i).MatchingStatus == 1 ? "ico_match_green.jpg" : (CollectionJudistration_item.ElementAt(i).MatchingStatus == 2 ? "ico_red_yellow.png" : "ico_red_match.png");
                                                            decimal subTotal = ReportFee + Judistrationfee;
                                                            string fullName = CollectionJudistration_item.ElementAt(i).mName == "" ? CollectionJudistration_item.ElementAt(i).fName : CollectionJudistration_item.ElementAt(i).fName + " " + CollectionJudistration_item.ElementAt(i).mName.Substring(0, 1);
                                                            fullName = fullName + " " + CollectionJudistration_item.ElementAt(i).lName;
                                                            response.Append("<tr><td><img title=\"Match Meter\" src=\"../Content/themes/base/images/" + MatchMeterIcon + "\">" + fullName + "</td>");
                                                            response.Append("<td>" + JudistrationName + "</td>");
                                                            //INT-126 See Comments on Jira
                                                            string MI = middlename_item != "" ? middlename_item : "*";
                                                            // string MI = CollectionJudistration_item.ElementAt(i).mName != "" ? CollectionJudistration_item.ElementAt(i).mName : "*";
                                                            response.Append("<td>(FCR)</td>");
                                                            response.Append("<td>" + fee + "</td>");
                                                            if (i < searchedDataList.Count())
                                                            {
                                                                //if (lstJT2.Contains(searchedDataList.ElementAt(i).county + ", [" + searchedDataList.ElementAt(i).state + "]"))
                                                                if (lstJT2.Contains(JudistrationName))
                                                                {
                                                                    response.Append("<td>&nbsp;&nbsp;&nbsp;<span style=\"color:red;\">-</span></td>");
                                                                }
                                                                else
                                                                {
                                                                    response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk1\" type=\"checkbox\" id=\"chk_" + JudistrationName + "_County_" + firstname_item + "_" + MI + "_" + lastname_item + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                                    // lstJT2.Add(searchedDataList.ElementAt(i).county + ", [" + searchedDataList.ElementAt(i).state + "]");
                                                                    lstJT2.Add(JudistrationName);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk1\" type=\"checkbox\" id=\"chk_" + JudistrationName + "_County_" + firstname_item + "_" + MI + "_" + lastname_item + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                            }

                                                            response.Append("<td><span class=\"subtotalClass\" id=\"subTotal_" + i.ToString() + "\">$" + subTotal + "</span></td>");
                                                            response.Append("</tr>");
                                                            ObjJurisdictionListColl.Add(new JurisdictionListColl() { AdditionalFee = decimal.Parse(CollectionJudistration_item.ElementAt(i).AdditionalFee.ToString()), pkJurisdictionId = int.Parse(CollectionJudistration_item.ElementAt(i).pkJurisdictionId.ToString()), Jurisdiction_Name = CollectionJudistration_item.ElementAt(i).Jurisdiction_Name.ToString(), fName = CollectionJudistration_item.ElementAt(i).fName, mName = CollectionJudistration_item.ElementAt(i).mName, lName = CollectionJudistration_item.ElementAt(i).lName });
                                                        }
                                                    }

                                                    response.Append("</table>").Replace("RecordCount", CollectionJudistration.Count.ToString());
                                                }
                                            }
                                        }

                                        #endregion

                                    }
                                    else if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Contains("CCR1"))
                                    {
                                        //Changed By Himesh Kumar
                                        #region Tiered CCR1
                                        
                                        if (CreditFile != null && CreditFile.Count > 0)
                                        {
                                            List<XElement> PersonalData = CreditFile.Descendants("record").ToList();
                                            List<string> lstCountyCCR = new List<string>();

                                            if (PersonalData != null && PersonalData.Count > 0)
                                            {
                                                foreach (XElement xe in PersonalData)
                                                {
                                                    XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                    XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                    XElement Region = Address.Descendants("state").FirstOrDefault();
                                                    XElement County = Address.Descendants("county").FirstOrDefault();


                                                    XElement FName = xe.Descendants("firstname").FirstOrDefault();
                                                    XElement MName = xe.Descendants("middlename").FirstOrDefault();
                                                    XElement LName = xe.Descendants("lastname").FirstOrDefault();
                                                    XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                    if (EffectiveDate != null)
                                                    {
                                                        string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";

                                                        TiredSearchedData objTiered = new TiredSearchedData();
                                                        if (Region != null && County != null && LName != null && YearMonth != null)
                                                        {
                                                            var ArrayCollection = YearMonth.Split('-');
                                                            int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                           // int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                            if (TimeFrame == 0)//ticket #87
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                            else if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                        }
                                                    }
                                                }
                                                if (searchedDataList1 != null && searchedDataList1.Count > 0)
                                                {
                                                    var lst = searchedDataList1.Where(n => n.county.Contains("DALLAS")).ToList();
                                                    lst.ForEach(n => searchedDataList1.Add(new TiredSearchedData
                                                    {
                                                        county = "DALLAS METRO",
                                                        state = "TX",
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName
                                                    }));

                                                    var searchedDataList = searchedDataList1.Select(n => new
                                                    {
                                                        county = n.county,
                                                        state = n.state,
                                                        mName = n.mName,
                                                        fName = n.fName,
                                                        lName = n.lName,
                                                        MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                    }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                    if (searchedDataList.Count > 0)
                                                    {
                                                        bool flagDisabled = false;
                                                        decimal ReportFee = GetReportFee(LocationId, CompanyUserId, "CCR1", "Tiered", ObjEmergeReport.ObjTieredPackage.fkPackageId);
                                                        response.Append("<table id=\"tieredTable\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">");
                                                        response.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">Record(s) found : RecordCount" +
                                                            "</td></tr>");
                                                        response.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"100%\"><td width=\"20%\">Full Name</td><td width=\"20%\">County[State]</td><td width=\"15%\">(ReportType)</td><td width=\"20%\">Cost</td> <td width=\"10%\"><input type=\"checkbox\" onclick=\"selectunselectCounty();\" id=\"chk_SelectAllCounty\"  />Select All</td><td width=\"15%\">Sub Total</td></tr>");
                                                        for (int i = 0; i < searchedDataList.Count; i++)
                                                        {
                                                            string CountyName = searchedDataList.ElementAt(i).county.ToLower().Replace("st.", "saint");
                                                            var Countyfee = Objbalcompanytype.Get_County_Fee(CountyName, searchedDataList.ElementAt(i).state, LocationId, "CCR1");
                                                            string fee = "$" + ReportFee.ToString(); ;

                                                            if (Countyfee == 0 || Countyfee == Convert.ToDecimal(0.00))
                                                            {
                                                            }
                                                            else
                                                            {
                                                                fee = fee + " <br /> County Fee" + " $" + Countyfee;
                                                            }
                                                            // string MatchMeterIcon = MatchMeterResult(searchedDataList.ElementAt(i).fName, searchedDataList.ElementAt(i).mName, searchedDataList.ElementAt(i).lName,
                                                            //                         ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName, ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial, ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName);

                                                            string MatchMeterIcon = searchedDataList.ElementAt(i).MatchingStatus == 1 ? "ico_match_green.jpg" : (searchedDataList.ElementAt(i).MatchingStatus == 2 ? "ico_red_yellow.png" : "ico_red_match.png");
                                                            decimal subTotal = ReportFee + Countyfee;
                                                            string fullName = searchedDataList.ElementAt(i).mName == "" ? searchedDataList.ElementAt(i).fName : searchedDataList.ElementAt(i).fName + " " + searchedDataList.ElementAt(i).mName.Substring(0, 1);
                                                            fullName = fullName + " " + searchedDataList.ElementAt(i).lName;

                                                            //Ticket#179
                                                            if (lstCountyCCR.Contains(searchedDataList.ElementAt(i).county + ", [" + searchedDataList.ElementAt(i).state + "]"))
                                                            {
                                                                flagDisabled = true;
                                                            }
                                                            else
                                                            {
                                                                flagDisabled = false;
                                                                lstCountyCCR.Add(searchedDataList.ElementAt(i).county + ", [" + searchedDataList.ElementAt(i).state + "]");
                                                            }
                                                            response.Append("<tr><td><img title=\"Match Meter\" src=\"../Content/themes/base/images/" + MatchMeterIcon + "\">" + fullName + "</td>");

                                                            string sDispState = "";

                                                            response.Append("<td>" + searchedDataList.ElementAt(i).county + ", [" + searchedDataList.ElementAt(i).state + "]" +
                                                                "</td>");
                                                            if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode != null)
                                                            {
                                                                if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Contains("_"))
                                                                    sDispState = "(" + ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Split('_').First() + ")";
                                                                else
                                                                    sDispState = searchedDataList.ElementAt(i).state;
                                                            }
                                                            else
                                                            {
                                                                sDispState = searchedDataList.ElementAt(i).state;
                                                            }

                                                            //INT-126 See Comments on Jira
                                                            string firstname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                                                            string lastname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;
                                                            string middlename_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                            string MI = middlename_item != "" ? middlename_item : "*";

                                                            //string MI = searchedDataList.ElementAt(i).mName != "" ? searchedDataList.ElementAt(i).mName : "*";
                                                            response.Append("<td>" + sDispState + "</td>");
                                                            response.Append("<td>" + fee + "</td>");
                                                            //Ticket#179
                                                            if (flagDisabled)
                                                            {
                                                                response.Append("<td>&nbsp;&nbsp;&nbsp;<span style=\"color:red;\">-</span></td>");
                                                            }
                                                            else
                                                            {
                                                                response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk\" type=\"checkbox\" id=\"chk_" + CountyName + "_" + searchedDataList.ElementAt(i).state + "_County_" + firstname_item + "_" + MI + "_" + lastname_item + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                            }
                                                            response.Append("<td><span class=\"subtotalClass\" id=\"subTotal_" + i.ToString() + "\">$" + subTotal + "</span></td>");
                                                            response.Append("</tr>");
                                                        }

                                                    }

                                                    response.Append("</table>").Replace("RecordCount", searchedDataList.Count.ToString());
                                                }
                                            }
                                        }

                                        #region Tiered FCR

                                        if (Credit_File != null && Credit_File.Count > 0)
                                        {
                                            List<XElement> Personal_Data = Credit_File.Descendants("PersonalData").ToList();

                                            List<Region_Name> ObjRegion_Name = new List<Region_Name>();
                                            if (Personal_Data != null && Personal_Data.Count > 0)
                                            {
                                                foreach (XElement xe in Personal_Data)
                                                {
                                                    XElement Region = xe.Descendants("Region").FirstOrDefault();
                                                    XElement County = xe.Descendants("County").FirstOrDefault();
                                                    XElement PersonName = xe.Descendants("PersonName").FirstOrDefault();
                                                    XElement FName = PersonName.Descendants("GivenName").FirstOrDefault();
                                                    XElement MName = PersonName.Descendants("MiddleName").FirstOrDefault();
                                                    XElement LName = PersonName.Descendants("FamilyName").FirstOrDefault();
                                                    XElement EffectiveDate = xe.Descendants("EffectiveDate").FirstOrDefault();
                                                    if (EffectiveDate != null)
                                                    {
                                                        XElement EndDate = EffectiveDate.Descendants("EndDate").FirstOrDefault();
                                                        XElement YearMonth = EndDate.Descendants("YearMonth").FirstOrDefault();
                                                        TiredSearchedData objTiered = new TiredSearchedData();
                                                        if (Region != null && County != null && PersonName != null && LName != null && YearMonth != null)
                                                        {
                                                            var region_value = Region.Value.ToString();
                                                            int check_item = 1;
                                                            for (int i = 0; i < ObjRegion_Name.Count; i++)
                                                            {
                                                                if (ObjRegion_Name.ElementAt(i).Region.ToString() == region_value)
                                                                {
                                                                    check_item = 2;
                                                                }
                                                            }
                                                            if (check_item != 2)
                                                            {
                                                                string FirstName = string.Empty;
                                                                string LastName = string.Empty;
                                                                string MiddleName = string.Empty;
                                                                if (FName != null) { FirstName = FName.Value.ToString(); }
                                                                if (LName != null) { LastName = LName.Value.ToString(); }
                                                                if (MName != null) { MiddleName = MName.Value.ToString(); }




                                                                ObjRegion_Name.Add(new Region_Name() { Region = Region.Value.ToString(), Fname = FirstName, Lname = LastName, Mname = MiddleName });
                                                            }

                                                            var ArrayCollection = YearMonth.Value.Split('-');
                                                            int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                           // int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                            if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                        }
                                                    }
                                                }
                                                var duplicatesRemoved = ObjRegion_Name.Distinct().ToList();
                                                string firstname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                                                string lastname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;
                                                string middlename_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                List<State_Name> CollectionStateName = Objbalcompany.GetStateList(duplicatesRemoved, SearchedStateName, firstname_item, lastname_item, middlename_item);

                                                Guid pkProductIdfcr = Guid.Parse("0f0016cf-c214-44d6-9b41-cda20dee2251");
                                                List<JurisdictionColl> CollectionJudistration = Objbalcompany.GetJurisdictionList(CollectionStateName, pkProductIdfcr);
                                                if (CollectionJudistration != null && CollectionJudistration.Count > 0)
                                                {
                                                    var lst = searchedDataList1.ToList();
                                                    lst.ForEach(n => searchedDataList1.Add(new TiredSearchedData
                                                    {
                                                        county = n.county,
                                                        state = n.state,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName
                                                    }));
                                                    var searchedDataList = searchedDataList1.Select(n => new
                                                    {
                                                        county = n.county,
                                                        state = n.state,
                                                        mName = n.mName,
                                                        fName = n.fName,
                                                        lName = n.lName,
                                                        MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                    }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                    var lst_fcr = CollectionJudistration.ToList();
                                                    lst_fcr.ForEach(n => CollectionJudistration.Add(new JurisdictionColl
                                                    {
                                                        Jurisdiction_Name = n.Jurisdiction_Name,
                                                        pkJurisdictionId = n.pkJurisdictionId,
                                                        AdditionalFee = n.AdditionalFee,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName

                                                    }));
                                                    var CollectionJudistration_item = CollectionJudistration.Select(n => new
                                                    {
                                                        Jurisdiction_Name = n.Jurisdiction_Name,
                                                        pkJurisdictionId = n.pkJurisdictionId,
                                                        AdditionalFee = n.AdditionalFee,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName,
                                                        MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                    }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                    if (CollectionJudistration_item.Count > 0)
                                                    {
                                                        for (int i = 0; i < CollectionJudistration_item.Count; i++)
                                                        {
                                                            ObjJurisdictionListColl.Add(new JurisdictionListColl() { AdditionalFee = decimal.Parse(CollectionJudistration_item.ElementAt(i).AdditionalFee.ToString()), pkJurisdictionId = int.Parse(CollectionJudistration_item.ElementAt(i).pkJurisdictionId.ToString()), Jurisdiction_Name = CollectionJudistration_item.ElementAt(i).Jurisdiction_Name.ToString(), fName = CollectionJudistration_item.ElementAt(i).fName, mName = CollectionJudistration_item.ElementAt(i).mName, lName = CollectionJudistration_item.ElementAt(i).lName });
                                                        }

                                                    }
                                                }
                                            }
                                        }

                                        #endregion

                                        #endregion
                                    }
                                    else if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Contains("SCR"))
                                    {
                                        //Changed By Himesh Kumar
                                        #region Tiered SCR
                                        if (CreditFile != null && CreditFile.Count > 0)
                                        {
                                            List<XElement> PersonalData = CreditFile.Descendants("record").ToList();

                                            List<string> lstStateSCR = new List<string>();
                                            if (PersonalData != null && PersonalData.Count > 0)
                                            {
                                                foreach (XElement xe in PersonalData)
                                                {
                                                    XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                    XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                    XElement Region = Address.Descendants("state").FirstOrDefault();
                                                    XElement County = Address.Descendants("county").FirstOrDefault();


                                                    XElement FName = xe.Descendants("firstname").FirstOrDefault();
                                                    XElement MName = xe.Descendants("middlename").FirstOrDefault();
                                                    XElement LName = xe.Descendants("lastname").FirstOrDefault();
                                                    XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                    if (EffectiveDate != null)
                                                    {
                                                        string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";

                                                        TiredSearchedData objTiered = new TiredSearchedData();
                                                        if (Region != null && County != null && LName != null && YearMonth != null)
                                                        {
                                                            var ArrayCollection = YearMonth.Split('-');
                                                            int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                            //int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                            if (TimeFrame == 0)//ticket #87
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                            else if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                        }
                                                    }
                                                }
                                                string[] StateArray = new BALGeneral().GetDisabledStates().Select(d => d.StateCode).ToArray();

                                                if (searchedDataList1 != null && searchedDataList1.Count > 0)
                                                {
                                                    var lst = searchedDataList1.Where(n => n.county.Contains("DALLAS")).ToList();
                                                    lst.ForEach(n => searchedDataList1.Add(new TiredSearchedData
                                                    {
                                                        county = "DALLAS METRO",
                                                        state = "TX",
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName
                                                    }));

                                                    var searchedDataList = searchedDataList1.Select(n => new
                                                    {
                                                        county = n.county,
                                                        state = n.state,
                                                        mName = n.mName,
                                                        fName = n.fName,
                                                        lName = n.lName,
                                                        MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                    }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();

                                                    searchedDataList = searchedDataList.Where(s => !StateArray.Contains(s.state)).ToList();
                                                    if (searchedDataList.Count > 0)
                                                    {
                                                        bool flagDisabled = false;
                                                        decimal ReportFee = GetReportFee(LocationId, CompanyUserId, "SCR", "Tiered", ObjEmergeReport.ObjTieredPackage.fkPackageId);
                                                        response.Append("<table id=\"tieredTable\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">");
                                                        response.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">Record(s) found : RecordCount" +
                                                            "</td></tr>");
                                                        response.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"100%\"><td width=\"20%\">Full Name</td><td width=\"20%\">State</td><td width=\"15%\">(ReportType)</td><td width=\"20%\">Cost</td> <td width=\"10%\"><input type=\"checkbox\" onclick=\"selectunselectCounty();\" id=\"chk_SelectAllCounty\"  />Select All</td><td width=\"15%\">Sub Total</td></tr>");
                                                        for (int i = 0; i < searchedDataList.Count; i++)
                                                        {
                                                            string CountyName = searchedDataList.ElementAt(i).county.ToLower().Replace("st.", "saint");

                                                            List<tblState> lstStateCode = new BALGeneral().GetStates();
                                                            string StateNameval = lstStateCode.Where(st => st.StateCode == searchedDataList.ElementAt(i).state).Select(st => st.StateName).FirstOrDefault();
                                                            var Countyfee = 0;
                                                            int stateFees = 0;
                                                            if (!string.IsNullOrEmpty(StateNameval))
                                                            {
                                                                string stFee = Objbalcompanytype.Get_State_Fee(StateNameval);

                                                                int.TryParse(stFee, out stateFees);
                                                            }
                                                            Countyfee = stateFees;

                                                            string fee = "$" + ReportFee.ToString(); ;

                                                            if (Countyfee == 0 || Countyfee == Convert.ToDecimal(0.00))
                                                            {
                                                            }
                                                            else
                                                            {
                                                                fee = fee + " <br />State Fee" + " $" + Countyfee;
                                                            }
                                                            // string MatchMeterIcon = MatchMeterResult(searchedDataList.ElementAt(i).fName, searchedDataList.ElementAt(i).mName, searchedDataList.ElementAt(i).lName,
                                                            //                         ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName, ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial, ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName);

                                                            string MatchMeterIcon = searchedDataList.ElementAt(i).MatchingStatus == 1 ? "ico_match_green.jpg" : (searchedDataList.ElementAt(i).MatchingStatus == 2 ? "ico_red_yellow.png" : "ico_red_match.png");
                                                            decimal subTotal = ReportFee + Countyfee;
                                                            string fullName = searchedDataList.ElementAt(i).mName == "" ? searchedDataList.ElementAt(i).fName : searchedDataList.ElementAt(i).fName + " " + searchedDataList.ElementAt(i).mName.Substring(0, 1);
                                                            fullName = fullName + " " + searchedDataList.ElementAt(i).lName;

                                                            response.Append("<tr><td><img title=\"Match Meter\" src=\"../Content/themes/base/images/" + MatchMeterIcon + "\">" + fullName + "</td>");

                                                            string sDispState = "";

                                                            response.Append("<td>" + searchedDataList.ElementAt(i).state + "</td>");
                                                            if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode != null)
                                                            {
                                                                if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Contains("_"))
                                                                    sDispState = "(" + ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Split('_').First() + ")";
                                                                else
                                                                    sDispState = searchedDataList.ElementAt(i).state;
                                                            }
                                                            else
                                                            {
                                                                sDispState = searchedDataList.ElementAt(i).state;
                                                            }

                                                            //INT-126 See Comments on Jira
                                                            string firstname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                                                            string lastname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;
                                                            string middlename_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                            string MI = middlename_item != "" ? middlename_item : "*";

                                                            //string MI = searchedDataList.ElementAt(i).mName != "" ? searchedDataList.ElementAt(i).mName : "*";
                                                            response.Append("<td>" + sDispState + "</td>");
                                                            response.Append("<td>" + fee + "</td>");

                                                            if (lstStateSCR.Contains(searchedDataList.ElementAt(i).state))
                                                            {
                                                                response.Append("<td>&nbsp;&nbsp;&nbsp;<span style=\"color:red;\">-</span></td>");
                                                            }
                                                            else
                                                            {
                                                                response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk\" type=\"checkbox\" id=\"chk_" + CountyName + "_" + searchedDataList.ElementAt(i).state + "_County_" + firstname_item + "_" + MI + "_" + lastname_item + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                                lstStateSCR.Add(searchedDataList.ElementAt(i).state);
                                                            }
                                                            response.Append("<td><span class=\"subtotalClass\" id=\"subTotal_" + i.ToString() + "\">$" + subTotal + "</span></td>");
                                                            response.Append("</tr>");
                                                        }

                                                    }
                                                    if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode == null)
                                                    {
                                                        response.Append("</table>").Replace("RecordCount", searchedDataList.Count.ToString());
                                                    }

                                                }
                                            }
                                        }
                                        #endregion

                                    }
                                }


                                if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode != null)
                                {
                                    response.Append("</td>");
                                    response.Append("<td style=\"border-right: 1px solid gray;vertical-align:top;\">");

                                    if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("SCR"))
                                    {
                                        //Changed By Himesh Kumar
                                        #region Optional SCR
                                        #region Optional SCR new
                                        List<string> lstStateSCR = new List<string>();
                                        if (CreditFile != null && CreditFile.Count > 0)
                                        {
                                            int RecordNumber = 0;
                                            decimal SCRFee = GetReportFee(LocationId, CompanyUserId, "SCR", "Optional", ObjEmergeReport.ObjTieredPackage.fkPackageId);
                                            List<XElement> PersonalData = CreditFile.Descendants("record").ToList();
                                            if (PersonalData != null && PersonalData.Count > 0)
                                            {
                                                foreach (XElement xe in PersonalData)
                                                {
                                                    //XElement Region = xe.Descendants("Region").FirstOrDefault();
                                                    //XElement County = xe.Descendants("County").FirstOrDefault();

                                                    XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                    XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                    XElement Region = Address.Descendants("state").FirstOrDefault();
                                                    XElement County = Address.Descendants("county").FirstOrDefault();
                                                    if (Region != null && County != null)
                                                    {
                                                        if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                            DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                    }
                                                }
                                                var distinctOptionalSCRData = searchedDataList1.Select(n => new
                                                {
                                                    state = n.state,
                                                    mName = n.mName,
                                                    fName = n.fName,
                                                    lName = n.lName,
                                                    MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())

                                                }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();

                                                if (distinctOptionalSCRData.Count > 0)
                                                {

                                                    DictCount = distinctOptionalSCRData.Count;
                                                    string RecordCount = "RecordFound";
                                                    responseOptional.Append("<table id=\"OptionTbl\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">");
                                                    responseOptional.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">States (" + RecordCount + ")</td></tr>");
                                                    responseOptional.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"100%\"><td width=\"20%\">Full Name</td><td width=\"20%\">State</td><td width=\"15%\">(ReportType)</td><td width=\"20%\">Cost</td> <td width=\"10%\"><input type=\"checkbox\" onclick=\"selectunselectState();\" id=\"chk_SelectAllStates\"  />Select All</td><td width=\"15%\">Sub Total</td></tr>");

                                                    BALGeneral objGeneral = new BALGeneral();
                                                    string[] StateArray = objGeneral.GetDisabledStates().Select(d => d.StateName).ToArray();//{ "California", "Louisiana", "Nevada", "Ohio", "Vermont", "West Virginia", "Wyoming" };
                                                    for (int i = 0; i < distinctOptionalSCRData.Count; i++)
                                                    {
                                                        string fullName = distinctOptionalSCRData.ElementAt(i).mName == "" ? distinctOptionalSCRData.ElementAt(i).fName : distinctOptionalSCRData.ElementAt(i).fName + " " + distinctOptionalSCRData.ElementAt(i).mName.Substring(0, 1);
                                                        fullName = fullName + " " + distinctOptionalSCRData.ElementAt(i).lName;

                                                        string StateName = (from d in dx1.tblStates
                                                                            where (d.StateCode.ToLower() == distinctOptionalSCRData.ElementAt(i).state.ToLower())
                                                                            select d.StateName).FirstOrDefault().ToString();

                                                        string stringToCheck = StateName;
                                                        string check = string.Empty;
                                                        foreach (string x in StateArray)
                                                        {
                                                            if (x.ToLower().Contains(stringToCheck.ToLower()))
                                                            {
                                                                check = "exist";
                                                            }

                                                        }

                                                        if (check != "exist")
                                                        {
                                                            string sDispState = "";
                                                            responseOptional.Append("<tr>");
                                                            if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode != null)
                                                            {
                                                                if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("_"))
                                                                    sDispState = "(" + ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Split('_').First() + ")";
                                                            }
                                                            else
                                                            {
                                                                sDispState = "NA";// DictCounty.ElementAt(i).Value;
                                                            }
                                                            if (StateName.ToLower() == SearchedStateName.ToLower())
                                                            {
                                                                IsStateFound = "yes";
                                                            }
                                                            string StateFee = Objbalcompanytype.Get_State_Fee(StateName);
                                                            decimal stateFees = 0;
                                                            decimal.TryParse(StateFee, out stateFees);
                                                            if (StateFee == "0")
                                                            {
                                                                StateFee = string.Empty;
                                                            }
                                                            else
                                                            {
                                                                StateFee = "<br /> State Fee" + " $" + StateFee;
                                                            }
                                                            decimal subTotal = stateFees + SCRFee;
                                                            //  string MatchMeterIcon = MatchMeterResult(distinctOptionalSCRData.ElementAt(i).fName, distinctOptionalSCRData.ElementAt(i).mName, distinctOptionalSCRData.ElementAt(i).lName,
                                                            //             ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName, ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial, ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName);

                                                            string MatchMeterIcon = distinctOptionalSCRData.ElementAt(i).MatchingStatus == 1 ? "ico_match_green.jpg" : (distinctOptionalSCRData.ElementAt(i).MatchingStatus == 2 ? "ico_red_yellow.png" : "ico_red_match.png");

                                                            string MI = distinctOptionalSCRData.ElementAt(i).mName != "" ? distinctOptionalSCRData.ElementAt(i).mName : "*";
                                                            responseOptional.Append("<td><img title=\"Match Meter\" src=\"../Content/themes/base/images/" + MatchMeterIcon + "\">" + fullName + "</td>");
                                                            responseOptional.Append("<td>" + StateName + "</td>");
                                                            responseOptional.Append("<td>" + sDispState + "</td>");
                                                            responseOptional.Append("<td>$" + SCRFee + StateFee + "</td>");

                                                            if (lstStateSCR.Contains(StateName))
                                                            {
                                                                responseOptional.Append("<td>&nbsp;&nbsp;&nbsp;<span style=\"color:red;\">-</span></td>");
                                                            }
                                                            else
                                                            {
                                                                responseOptional.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_NoRecord_" + distinctOptionalSCRData.ElementAt(i).state + "_State_" + distinctOptionalSCRData.ElementAt(i).fName + "_" + MI + "_" + distinctOptionalSCRData.ElementAt(i).lName + "_name_" + i + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                                lstStateSCR.Add(StateName);
                                                            }



                                                            responseOptional.Append("<td><span class=\"subtotalClass\" id=\"OptionalSubTotal_" + i + "\">$" + subTotal + "</span></td>");
                                                            responseOptional.Append("<tr>");
                                                            RecordNumber = RecordNumber + 1;
                                                        }

                                                    }
                                                    if (IsStateFound == string.Empty)
                                                    {
                                                        string check = string.Empty;
                                                        foreach (string x in StateArray)
                                                        {
                                                            if (x.ToLower().Contains(SearchedStateName.ToLower()))
                                                            {
                                                                check = "exist";
                                                            }

                                                        }
                                                        if (check != "exist")
                                                        {
                                                            string sDispState = "";
                                                            responseOptional.Append("<tr>");
                                                            sDispState = "(SCR)";
                                                            string StateFee = Objbalcompanytype.Get_State_Fee(SearchedStateName);
                                                            decimal stateFees = 0;
                                                            decimal.TryParse(StateFee, out stateFees);
                                                            if (StateFee == "0" || StateFee == "0.00")
                                                            {
                                                                StateFee = string.Empty;
                                                            }
                                                            else
                                                            {
                                                                StateFee = "<br /> State Fee" + " $" + StateFee;
                                                            }
                                                            decimal subTotal = stateFees + SCRFee;

                                                            string MI = (ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial == null || ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial == "") ? "*" : ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                            responseOptional.Append("<td>" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + " " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + "</td>");
                                                            responseOptional.Append("<td>" + SearchedStateName + "</td>");
                                                            responseOptional.Append("<td>" + sDispState + "</td>");
                                                            responseOptional.Append("<td>$" + SCRFee + StateFee + "</td>");
                                                            responseOptional.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_NoRecord_" + SearchedStateCode + "_State_" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + "_" + MI + "_" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + "_name_" + DictCount + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                            responseOptional.Append("<td><span class=\"subtotalClass\" id=\"OptionalSubTotal_" + DictCount + "\">$" + subTotal + "</span></td>");
                                                            responseOptional.Append("<tr>");
                                                            RecordNumber = RecordNumber + 1;
                                                        }
                                                    }
                                                }
                                                if (DictCounty.Count == 0)
                                                {
                                                    string MI = (ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial == null || ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial == "") ? "*" : ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                    responseOptional.Append("<table id=\"OptionTbl\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">");
                                                    responseOptional.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">State (1) found </td></tr>");
                                                    responseOptional.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"100%\"><td width=\"20%\">Full Name</td><td width=\"20%\">State</td><td width=\"15%\">(ReportType)</td><td width=\"20%\">Cost</td> <td width=\"10%\"><input type=\"checkbox\" onclick=\"selectunselectState();\" id=\"chk_SelectAllStates\"  />Select All</td><td width=\"15%\">Sub Total</td></tr>");
                                                    string sDispState = "";
                                                    responseOptional.Append("<tr>");
                                                    sDispState = "(SCR)";
                                                    string StateFee = Objbalcompanytype.Get_State_Fee(SearchedStateName);
                                                    decimal stateFees = 0;
                                                    decimal.TryParse(StateFee, out stateFees);
                                                    if (StateFee == "0" || StateFee == "0.00")
                                                    {
                                                        StateFee = string.Empty;
                                                    }
                                                    else
                                                    {
                                                        StateFee = "<br /> State Fee" + " $" + StateFee;
                                                    }
                                                    decimal subTotal = stateFees + SCRFee;
                                                    responseOptional.Append("<td>" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + " " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + "</td>");
                                                    responseOptional.Append("<td>" + SearchedStateName + "</td>");
                                                    responseOptional.Append("<td>" + sDispState + "</td>");
                                                    responseOptional.Append("<td>$" + SCRFee + StateFee + "</td>");
                                                    //responseOptional.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_NoRecord_" + SearchedStateCode + "_State_1\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                    responseOptional.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_NoRecord_" + SearchedStateCode + "_State_" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + "_" + MI + "_" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + "_name_" + DictCount + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                    responseOptional.Append("<td><span class=\"subtotalClass\" id=\"OptionalSubTotal_1\">$" + subTotal + "</span></td>");
                                                    responseOptional.Append("<tr>");
                                                    RecordNumber = 1;
                                                }
                                                responseOptional.Append("</table>").Replace("RecordFound", RecordNumber.ToString());
                                            }
                                        }

                                        #endregion

                                        #region Optional SCR Old
                                        //if (CreditFile != null && CreditFile.Count > 0)
                                        //{
                                        //    List<XElement> PersonalData = CreditFile.Descendants("PersonalData").ToList();
                                        //    if (PersonalData != null && PersonalData.Count > 0)
                                        //    {
                                        //        foreach (XElement xe in PersonalData)
                                        //        {
                                        //            XElement Region = xe.Descendants("Region").FirstOrDefault();
                                        //            XElement County = xe.Descendants("County").FirstOrDefault();
                                        //            if (Region != null && County != null)
                                        //            {
                                        //                if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                        //                    DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                        //            }
                                        //        }
                                        //        if (DictCounty.Count > 0)
                                        //        {
                                        //            DictCount = DictCounty.Count;
                                        //            string RecordCount = "RecordFound";
                                        //            responseOptional.Append("<table cellpadding=\"3\" cellspacing=\"0\" width=\"100%\">");
                                        //            responseOptional.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"3\">States (" + RecordCount + ") found for " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + " " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + ":</td></tr>");
                                        //            responseOptional.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"33%\"><td width=\"40%\">State</td><td width=\"40%\"> (ReportType)</td> <td width=\"20%\"><input type=\"checkbox\" onclick=\"selectunselectState();\" id=\"chk_SelectAllStates\"  />Select All</td></tr>");
                                        //            for (int i = 0; i < DictCounty.Count; i++)
                                        //            {
                                        //                string StateName = (from d in dx1.tblStates
                                        //                                    where (d.StateCode.ToLower() == DictCounty.ElementAt(i).Value.ToLower())
                                        //                                    select d.StateName).FirstOrDefault().ToString();

                                        //                string stringToCheck = StateName;
                                        //                string check = string.Empty;
                                        //                BALGeneral objGeneral = new BALGeneral();
                                        //                string[] StateArray = objGeneral.GetDisabledStates().Select(d => d.StateName).ToArray();//{ "California", "Louisiana", "Nevada", "Ohio", "Vermont", "West Virginia", "Wyoming" };
                                        //                foreach (string x in StateArray)
                                        //                {
                                        //                    if (x.ToLower().Contains(stringToCheck.ToLower()))
                                        //                    {
                                        //                        check = "exist";
                                        //                    }

                                        //                }

                                        //                if (check != "exist")
                                        //                {
                                        //                    string sDispState = "";
                                        //                    responseOptional.Append("<tr>");
                                        //                    //responseOptional.Append("<td>" + DictCounty.ElementAt(i).Key + "</td>");
                                        //                    if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode != null)
                                        //                    {
                                        //                        if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("_"))
                                        //                            sDispState = "(" + ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Split('_').First() + ")";
                                        //                    }
                                        //                    else
                                        //                    {
                                        //                        sDispState = "NA";// DictCounty.ElementAt(i).Value;
                                        //                    }
                                        //                    //string StateName = (from d in dx1.tblStates
                                        //                    //            where (d.StateCode.ToLower() == DictCounty.ElementAt(i).Value.ToLower())
                                        //                    //            select d.StateName).FirstOrDefault().ToString();
                                        //                    if (StateName.ToLower() == SearchedStateName.ToLower())
                                        //                    {
                                        //                        IsStateFound = "yes";
                                        //                    }

                                        //                    BALCompanyType Objbalcompanytype = new BALCompanyType();
                                        //                    string StateFee = Objbalcompanytype.Get_State_Fee(StateName);
                                        //                    if (StateFee == "0")
                                        //                    {
                                        //                        StateFee = string.Empty;
                                        //                    }
                                        //                    else
                                        //                    {
                                        //                        StateFee = " State Fee" + " $" + StateFee;
                                        //                    }
                                        //                    responseOptional.Append("<td>" + StateName + "<label style='color:red'>" + StateFee + "</label>" + "</td>");
                                        //                    responseOptional.Append("<td>" + sDispState + "</td>");
                                        //                    responseOptional.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_" + DictCounty.ElementAt(i).Key + "_" + DictCounty.ElementAt(i).Value + "_State_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                        //                    responseOptional.Append("<tr>");
                                        //                }
                                        //            }
                                        //            if (IsStateFound == string.Empty)
                                        //            {
                                        //                string check = string.Empty;
                                        //                BALGeneral objGeneral = new BALGeneral();
                                        //                string[] StateArray = objGeneral.GetDisabledStates().Select(d => d.StateName).ToArray();//{ "California", "Louisiana", "Nevada", "Ohio", "Vermont", "West Virginia", "Wyoming" };
                                        //                foreach (string x in StateArray)
                                        //                {
                                        //                    if (x.ToLower().Contains(SearchedStateName.ToLower()))
                                        //                    {
                                        //                        check = "exist";
                                        //                    }

                                        //                }
                                        //                if (check != "exist")
                                        //                {
                                        //                    DictCount = DictCount + 1;
                                        //                    string sDispState = "";
                                        //                    responseOptional.Append("<tr>");
                                        //                    sDispState = "(SCR)";
                                        //                    BALCompanyType Objbalcompanytype = new BALCompanyType();
                                        //                    string StateFee = Objbalcompanytype.Get_State_Fee(SearchedStateName);
                                        //                    if (StateFee == "0" || StateFee == "0.00")
                                        //                    {
                                        //                        StateFee = string.Empty;
                                        //                    }
                                        //                    else
                                        //                    {
                                        //                        StateFee = " State Fee" + " $" + StateFee;
                                        //                    }
                                        //                    responseOptional.Append("<td>" + SearchedStateName + "<label style='color:red'>" + StateFee + "</label>" + "</td>");
                                        //                    responseOptional.Append("<td>" + sDispState + "</td>");
                                        //                    responseOptional.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_NoRecord_" + SearchedStateCode + "_State_" + DictCount + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                        //                    responseOptional.Append("<tr>");
                                        //                }
                                        //            }
                                        //        }
                                        //        if (DictCounty.Count == 0)
                                        //        {
                                        //            responseOptional.Append("<table cellpadding=\"3\" cellspacing=\"0\" width=\"100%\">");
                                        //            responseOptional.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"3\">States (1) found for " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + " " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + ":</td></tr>");
                                        //            responseOptional.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"33%\"><td width=\"40%\">State</td><td width=\"40%\"> (ReportType)</td> <td width=\"20%\"><input type=\"checkbox\" onclick=\"selectunselectState();\" id=\"chk_SelectAllStates\"  />Select All</td></tr>");
                                        //            string sDispState = "";
                                        //            responseOptional.Append("<tr>");
                                        //            sDispState = "(SCR)";
                                        //            BALCompanyType Objbalcompanytype = new BALCompanyType();
                                        //            string StateFee = Objbalcompanytype.Get_State_Fee(SearchedStateName);
                                        //            if (StateFee == "0" || StateFee == "0.00")
                                        //            {
                                        //                StateFee = string.Empty;
                                        //            }
                                        //            else
                                        //            {
                                        //                StateFee = " State Fee" + " $" + StateFee;
                                        //            }
                                        //            responseOptional.Append("<td>" + SearchedStateName + "<label style='color:red'>" + StateFee + "</label>" + "</td>");
                                        //            responseOptional.Append("<td>" + sDispState + "</td>");
                                        //            responseOptional.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_NoRecord_" + SearchedStateCode + "_State_1\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                        //            responseOptional.Append("<tr>");
                                        //        }
                                        //        responseOptional.Append("</table>").Replace("RecordFound", DictCount.ToString());
                                        //    }
                                        //}
                                        #endregion
                                        if (!ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("NCR1"))
                                            response.Append("</table>");
                                        #endregion
                                    }
                                    if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("NCR1"))
                                    {
                                        #region Optional NCR1
                                        decimal NCRReportfee = GetReportFee(LocationId, CompanyUserId, "NCR1", "Optional", ObjEmergeReport.ObjTieredPackage.fkPackageId); ;
                                        GrandTotal = NCRReportfee;
                                        if (CreditFile != null && CreditFile.Count > 0)
                                        {
                                            //Changed By Himesh Kumar
                                            List<XElement> PersonalData = CreditFile.Descendants("record").ToList();
                                            if (PersonalData != null && PersonalData.Count > 0)
                                            {
                                                if (searchedDataList1.Count > 0)
                                                {
                                                    DictCount = DictCounty.Count;
                                                    string MI = (ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial == null || ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial == "") ? "*" : ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                    responseOptional.Append("<table id=\"OptionTbl\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">");
                                                    responseOptional.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td>Optional Report</td><td></td><td></td><td></td><td width=\"15%\"><input type=\"checkbox\" onclick=\"selectunselectState();\" id=\"chk_SelectAllStates\"  />Select All</td><td></td></tr>");
                                                    // ("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"100%\"><td width=\"20%\">Full Name</td><td width=\"20%\">State</td><td width=\"15%\">(ReportType)</td><td width=\"20%\">Cost</td> <td width=\"10%\"><input type=\"checkbox\" onclick=\"selectunselectState();\" id=\"chk_SelectAllStates\"  />Select All</td><td width=\"15%\">Sub Total</td></tr>");
                                                    responseOptional.Append("<tr width=\"100%\">");
                                                    responseOptional.Append("<td>" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + " " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + "</td>");
                                                    responseOptional.Append("<td></td>");
                                                    responseOptional.Append("<td>(NCR1)</td>");
                                                    responseOptional.Append("<td>$" + NCRReportfee + "</td>");
                                                    responseOptional.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_NoRecord_" + SearchedStateCode + "_State_" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + "_" + MI + "_" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + "_1\"  checked=\"checked\" /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                    responseOptional.Append("<td><span class=\"subtotalClass\" id=\"OptionalSubTotal_1\">$" + NCRReportfee + "</span></td>");
                                                    responseOptional.Append("<tr>");
                                                }
                                                responseOptional.Append("</table>");
                                            }
                                        }

                                        #endregion
                                    }
                                    response.Append("</td>");
                                    response.Append("</tr>");
                                    response.Append("</table>");
                                }
                            }
                            catch// (Exception ex)
                            {
                                response = new StringBuilder();
                                responseOptional = new StringBuilder();
                            }
                            #endregion
                        }
                        else if (ObjEmergeReport.ObjOrderInfo.HostProductCode == "CCR1")
                        {

                            #region CCR1 HostReport New
                            List<TiredSearchedData> searchedDataListForCCRHostCode = new List<TiredSearchedData>();
                            string SearchedStateName = string.Empty;
                            string SearchedStateCode = string.Empty;
                            if (ObjEmergeReport.ObjtblOrderSearchedData.SearchedStateId != null)
                            {
                                SearchedStateName = (from d in dx1.tblStates
                                                     where (d.pkStateId == ObjEmergeReport.ObjtblOrderSearchedData.SearchedStateId)
                                                     select d.StateName).FirstOrDefault().ToString();
                                SearchedStateCode = (from d in dx1.tblStates
                                                     where (d.pkStateId == ObjEmergeReport.ObjtblOrderSearchedData.SearchedStateId)
                                                     select d.StateCode).FirstOrDefault().ToString();
                            }
                            try
                            {
                                List<XElement> CriminalReport = ObjXDocument.Descendants("CriminalReport").ToList();
                                if (CriminalReport != null && CriminalReport.Count > 0)
                                {
                                   // List<XElement> CriminalCase = CriminalReport.Descendants("CriminalCase").ToList();
                                    List<XElement> SubjectIdentification = CriminalReport.Descendants("SubjectIdentification").ToList();
                                    if (SubjectIdentification != null && SubjectIdentification.Count > 0)
                                    {
                                        foreach (XElement xe in SubjectIdentification)
                                        {

                                            XElement PostalAddress = xe.Descendants("PostalAddress").FirstOrDefault();
                                            XElement Region = PostalAddress.Descendants("Region").FirstOrDefault();
                                            XElement PersonName = xe.Descendants("PersonName").FirstOrDefault();
                                            XElement FName = PersonName.Descendants("GivenName").FirstOrDefault();
                                            XElement MName = PersonName.Descendants("MiddleName").FirstOrDefault();
                                            XElement LName = PersonName.Descendants("FamilyName").FirstOrDefault();
                                            TiredSearchedData objTiered = new TiredSearchedData();
                                            if (Region != null && LName != null)
                                            {
                                                objTiered.state = Region.Value;
                                                objTiered.fName = FName != null ? FName.Value : "";
                                                objTiered.mName = MName != null ? MName.Value : "";
                                                objTiered.lName = LName.Value;
                                                searchedDataListForCCRHostCode.Add(objTiered);
                                            }
                                        }

                                    }
                                }
                                int RecordCount = 0;
                                if (searchedDataListForCCRHostCode != null && searchedDataListForCCRHostCode.Count > 0)
                                {
                                    #region Tiered SCR

                                    if (searchedDataListForCCRHostCode != null && searchedDataListForCCRHostCode.Count > 0)
                                    {
                                        var searchedDataListForCCRHostCodeCCR = searchedDataListForCCRHostCode.Select(n => new
                                        {
                                            state = n.state,
                                            mName = n.mName.Substring(0, 1),
                                            fName = n.fName,
                                            lName = n.lName
                                        }).Distinct().OrderBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();

                                        if (searchedDataListForCCRHostCodeCCR.Count > 0)
                                            response.Append("<table id=\"tieredTable\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">");
                                        response.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">Record(s) found: RecordCount" +
                                            "</td></tr>");
                                        response.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"100%\"><td width=\"20%\">Full Name</td><td width=\"20%\">County</td><td width=\"15%\">(ReportType)</td><td width=\"20%\">Cost</td> <td width=\"10%\"><input type=\"checkbox\" onclick=\"selectunselectCounty();\" id=\"chk_SelectAllCounty\"  />Select All</td><td width=\"15%\">Sub Total</td></tr>");

                                        for (int i = 0; i < searchedDataListForCCRHostCodeCCR.Count; i++)
                                        {
                                            string StateName = (from d in dx1.tblStates
                                                                where (d.StateCode.ToLower() == searchedDataListForCCRHostCodeCCR.ElementAt(i).state.ToLower())
                                                                select d.StateName).FirstOrDefault().ToString();
                                            string check_second = string.Empty;
                                            string stringToCheck = StateName;
                                            BALGeneral objGeneral = new BALGeneral();
                                            string[] StateArray = objGeneral.GetDisabledStates().Select(d => d.StateName).ToArray();//{ "California", "Louisiana", "Nevada", "Ohio", "Vermont", "West Virginia", "Wyoming" };
                                            foreach (string x in StateArray)
                                            {
                                                if (x.ToLower().Contains(stringToCheck.ToLower()))
                                                {
                                                    check_second = "exist";
                                                }
                                            }
                                            if (check_second != "exist")
                                            {
                                                string sDispState = "";
                                                response.Append("<tr>");
                                                if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode != null)
                                                {
                                                    if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("_"))
                                                        sDispState = "(" + ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Split('_').First() + ")";
                                                }
                                                else
                                                {
                                                    sDispState = "NA";// DictCounty.ElementAt(i).Value;
                                                }
                                                string StateFee = Objbalcompanytype.Get_State_Fee(StateName);
                                                string fullName = searchedDataListForCCRHostCodeCCR.ElementAt(i).mName == "" ? searchedDataListForCCRHostCodeCCR.ElementAt(i).fName : searchedDataListForCCRHostCodeCCR.ElementAt(i).fName + " " + searchedDataListForCCRHostCodeCCR.ElementAt(i).mName.Substring(0, 1);
                                                fullName = fullName + " " + searchedDataListForCCRHostCodeCCR.ElementAt(i).lName;
                                                decimal SCRFee = GetReportFee(LocationId, CompanyUserId, "SCR", "Tiered", ObjEmergeReport.ObjTieredPackage.fkPackageId);
                                                decimal stateFees = 0;
                                                decimal.TryParse(StateFee, out stateFees);

                                                if (StateFee == "0")
                                                {
                                                    StateFee = string.Empty;
                                                }
                                                else
                                                {
                                                    StateFee = "<br /> State Fee" + " $" + StateFee;
                                                }
                                                decimal subTotal = stateFees + SCRFee;
                                                string MI = searchedDataListForCCRHostCodeCCR.ElementAt(i).mName == "" ? "*" : searchedDataListForCCRHostCodeCCR.ElementAt(i).mName;
                                                response.Append("<td>" + fullName + "</td>");
                                                response.Append("<td>" + StateName + "</td>");
                                                response.Append("<td>" + sDispState + "</td>");
                                                response.Append("<td>$" + SCRFee + StateFee + "</td>");//chk_" + DictCounty.ElementAt(i).Key + "_" + DictCounty.ElementAt(i).Value + "_State_" + i.ToString()
                                                response.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_NoRecord_" + searchedDataListForCCRHostCodeCCR.ElementAt(i).state + "_State_" + searchedDataListForCCRHostCodeCCR.ElementAt(i).fName + "_" + MI + "_" + searchedDataListForCCRHostCodeCCR.ElementAt(i).lName + "_name_" + i + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                response.Append("<td><span class=\"subtotalClass\" id=\"subTotal_" + i + "\">$" + subTotal + "</span></td>");
                                                response.Append("<tr>");
                                                RecordCount = RecordCount + 1;
                                            }
                                        }
                                    }
                                    response.Append("</table>").Replace("RecordCount", RecordCount.ToString());

                                    #endregion
                                }


                                if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode != null)
                                {
                                    if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("PS"))
                                    {
                                        #region Optional PS


                                        if (searchedDataListForCCRHostCode != null && searchedDataListForCCRHostCode.Count > 0)
                                        {
                                            var searchedDataListForCCRHostCodePS = searchedDataListForCCRHostCode.Select(n => new
                                            {
                                                state = n.state,
                                                county = n.county,
                                            }).Distinct().ToList();
                                            if (searchedDataListForCCRHostCodePS.Count > 0)
                                            {
                                                responseOptional.Append("<table id=\"OptionTbl\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">");
                                                responseOptional.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">Optional Report</td></tr>");
                                                for (int i = 0; i < searchedDataListForCCRHostCodePS.Count; i++)
                                                {
                                                    string sDispState = "";
                                                    responseOptional.Append("<tr>");
                                                    if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode != null)
                                                    {
                                                        if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("_"))
                                                            sDispState = " (" + ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Split('_').First() + ")";
                                                    }
                                                    else
                                                    {
                                                        sDispState = "NA";// DictCounty.ElementAt(i).Value;
                                                    }
                                                    decimal PSReportfee = GetReportFee(LocationId, CompanyUserId, "PS", "Optional", ObjEmergeReport.ObjTieredPackage.fkPackageId); ;
                                                    string MI = (ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial == null || ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial == "") ? "*" : ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                    responseOptional.Append("<td>" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + " " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + "</td>");
                                                    responseOptional.Append("<td></td>");
                                                    responseOptional.Append("<td>" + sDispState + "</td>");
                                                    responseOptional.Append("<td>$" + PSReportfee + "</td>");
                                                    responseOptional.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_NoRecord_" + SearchedStateCode + "_State_" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + "_" + MI + "_" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + "_name_" + i + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                    responseOptional.Append("<td><span class=\"subtotalClass\" id=\"OptionalSubTotal_" + i + "\">$" + PSReportfee + "</span></td>");
                                                    responseOptional.Append("<tr>");
                                                }
                                            }
                                            responseOptional.Append("</table>");
                                        }

                                        #endregion
                                    }

                                    if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("NCR1"))
                                    {
                                        #region Optional NCR1 New
                                        decimal NCRReportfee = GetReportFee(LocationId, CompanyUserId, "NCR1", "Optional", ObjEmergeReport.ObjTieredPackage.fkPackageId); ;

                                        if (CriminalReport != null && CriminalReport.Count > 0)
                                        {
                                            if (searchedDataListForCCRHostCode.Count > 0)
                                            {
                                                GrandTotal = NCRReportfee;
                                                string MI = (ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial == null || ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial == "") ? "*" : ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                responseOptional.Append("<table id=\"OptionTbl\" cellpadding=\"3\" cellspacing=\"0\" width=\"100%\">");
                                                //  responseOptional.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">Optional Report</td></tr>");
                                                responseOptional.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td >Optional Report</td><td></td><td></td><td></td><td width=\"15%\"><input type=\"checkbox\" onclick=\"selectunselectState();\" id=\"chk_SelectAllStates\"  />Select All</td><td></td></tr>");
                                                responseOptional.Append("<tr width=\"100%\">");
                                                responseOptional.Append("<td>" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + " " + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + "</td>");
                                                responseOptional.Append("<td></td>");
                                                responseOptional.Append("<td>(NCR1)</td>");
                                                responseOptional.Append("<td>$" + NCRReportfee + "</td>");
                                                //responseOptional.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_NoRecord_" + SearchedStateCode + "_State_1\"  checked=\"checked\" /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                responseOptional.Append("<td><input type=\"checkbox\" class=\"chkunchk\" onclick=\"ChkSingleSelect(this.id);\"  id=\"chk_NoRecord_" + SearchedStateCode + "_State_" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName + "_" + MI + "_" + ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName + "_name_1\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                                                responseOptional.Append("<td><span class=\"subtotalClass\" id=\"OptionalSubTotal_1\">$" + NCRReportfee + "</span></td>");
                                                responseOptional.Append("<tr>");
                                            }
                                            responseOptional.Append("</table>");
                                        }

                                        #endregion

                                    }
                                }

                            }
                            catch
                            {
                                response = new StringBuilder();
                                responseOptional = new StringBuilder();
                            }
                            #endregion
                        }

                        #endregion

                        #region OrderInfo Region

                        ObjEmergeReport.ObjOrderInfo.OrderId = OrderId;
                        ObjEmergeReport.ObjOrderInfo.Tiered2Response = response.ToString();
                        if (ObjEmergeReport.ObjTieredPackage.TiredOptional != null)
                            ObjEmergeReport.ObjOrderInfo.OptionalResponse = responseOptional.ToString();
                        else
                            ObjEmergeReport.ObjOrderInfo.OptionalResponse = "";
                        ObjEmergeReport.ObjOrderInfo.TieredTopInfo = TieredHeadText.ToString();

                        #endregion

                        Session["EmergeReport"] = ObjEmergeReport;
                    }
                    #endregion
                }
                else
                {                ///
                    /// Just Create a collection and call the tiered 2nd function SearchingTieredOrders().
                    ///
                    #region Is Automatic True

                    //XmlDocument ObjXmlDocument = new XmlDocument();
                    XDocument ObjXmlDoc = new XDocument();

                    if (ObjEmergeReport.ObjTieredPackage != null && ObjEmergeReport.ObjOrderInfo.IsTiered && ObjEmergeReport.ObjOrderInfo.Tiered2Response != null)
                    {

                        ObjXmlDoc = XDocument.Parse(ObjEmergeReport.ObjOrderInfo.Tiered2Response);
                        string XML_Value = GetRequiredXmlConditional(ObjXmlDoc.Document.ToString(), ObjEmergeReport.ObjOrderInfo.HostProductCode);
                        ObjXDocument = XDocument.Parse(XML_Value);
                        Dictionary<string, string> DictCounty = new Dictionary<string, string>();

                        if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode != null)
                        {
                            ObjEmergeReport.ObjCollProducts.Add(ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode);
                        }
                        if (ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode != null)
                        {
                            ObjEmergeReport.ObjCollProducts.Add(ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode);
                        }

                        if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode != null)
                        {
                            ObjEmergeReport.ObjCollProducts.Add(ObjEmergeReport.ObjOrderInfo.OptionalProductCode);
                        }

                        #region HOST Response

                        string GivenFname = string.Empty;
                        string GivenMname = string.Empty;
                        string givenLname = string.Empty;

                        if (ObjEmergeReport.ObjOrderInfo.HostProductCode == "PS")
                        {
                            #region HostedResponse for PS
                            try
                            {
                                string SearchedStateName = string.Empty;
                                string SearchedStateCode = string.Empty;
                                string IsStateFound = string.Empty;


                                if (ObjEmergeReport.ObjtblOrderSearchedData.SearchedStateId != null)
                                {
                                    SearchedStateName = (from d in dx1.tblStates
                                                         where (d.pkStateId == ObjEmergeReport.ObjtblOrderSearchedData.SearchedStateId)
                                                         select d.StateName).FirstOrDefault().ToString();
                                    SearchedStateCode = (from d in dx1.tblStates
                                                         where (d.pkStateId == ObjEmergeReport.ObjtblOrderSearchedData.SearchedStateId)
                                                         select d.StateCode).FirstOrDefault().ToString();

                                    GivenFname = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                                    GivenMname = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                    givenLname = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;


                                }

                                //Tiered2 Package functionality.
                                List<XElement> CreditFile = ObjXDocument.Descendants("records").ToList();
                                int TimeFrame = ObjEmergeReport.ObjTieredPackage.TimeFrame;
                                int TimeFrameYear = TimeFrame * (-1);
                                DateTime TimeFrameDate = DateTime.Now.AddYears(TimeFrameYear);
                                if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Contains("CCR"))
                                {
                                    //Changed By Himesh Kumar
                                    #region Tiered CCR1
                                    if (CreditFile != null && CreditFile.Count > 0)
                                    {
                                        List<XElement> PersonalData = CreditFile.Descendants("record").ToList();
                                        if (PersonalData != null && PersonalData.Count > 0)
                                        {

                                            foreach (XElement xe in PersonalData)
                                            {
                                                #region---Old Code
                                                //XElement Region = xe.Descendants("Region").FirstOrDefault();
                                                //XElement County = xe.Descendants("County").FirstOrDefault();
                                                //if (Region != null && County != null)
                                                //{
                                                //    if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                //        DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                //}
                                                #endregion
                                                #region----New code
                                                //XElement Region = xe.Descendants("Region").FirstOrDefault();
                                                //XElement County = xe.Descendants("County").FirstOrDefault();
                                                //XElement EffectiveDate = xe.Descendants("EffectiveDate").FirstOrDefault();

                                                XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                XElement Region = Address.Descendants("state").FirstOrDefault();
                                                XElement County = Address.Descendants("county").FirstOrDefault();
                                                XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                if (EffectiveDate != null)
                                                {
                                                    //XElement EndDate = EffectiveDate.Descendants("EndDate").FirstOrDefault();
                                                    //XElement YearMonth = EndDate.Descendants("YearMonth").FirstOrDefault();

                                                    string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";
                                                    TiredSearchedData objTiered = new TiredSearchedData();
                                                    if (Region != null && County != null && YearMonth != null)
                                                    {
                                                        var ArrayCollection = YearMonth.Split('-');
                                                        int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                        //int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                        if (TimeFrame == 0) //ticket #87
                                                        {
                                                            if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                            {
                                                                DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                            }
                                                        }
                                                        else if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                        {
                                                            if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                            {
                                                                DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                            }
                                                        }
                                                    }
                                                }

                                                else
                                                {  //changes for int 389
                                                    if (TimeFrame == 0) //ticket #87
                                                    {
                                                        if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                        {
                                                            DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                        }
                                                    }

                                                }

                                                #endregion
                                            }
                                            List<string> lstAutoCounty = new List<string>();
                                            List<string> lstcounty = new BALCompany().GetCountyList();

                                            foreach (var DictObj in DictCounty)
                                            {
                                                if (!lstAutoCounty.Contains(DictObj.Key))
                                                {
                                                    if (!lstcounty.Contains(DictObj.Key.ToUpper()))
                                                    {
                                                        flagAutopopup = true;
                                                    }
                                                    COUNTIES = COUNTIES + "##chk_" + DictObj.Key + "_" + DictObj.Value + "_County" + "_" + GivenFname + "_" + GivenMname + "_" + givenLname + "_name_0";
                                                    lstAutoCounty.Add(DictObj.Key);
                                                }
                                                else
                                                {
                                                    continue;
                                                }
                                            }

                                        }
                                    }
                                    #endregion
                                }
                                else if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Contains("FCR"))
                                {
                                    //Changed By Himesh Kumar
                                    #region Tiered FCR
                                    List<XElement> Credit_File = ObjXDocument.Descendants("records").ToList();
                                    List<TiredSearchedData> searchedDataList1 = new List<TiredSearchedData>();

                                    if (Credit_File != null && Credit_File.Count > 0)
                                    {
                                        List<XElement> Personal_Data = Credit_File.Descendants("record").ToList();

                                        List<Region_Name> ObjRegion_Name = new List<Region_Name>();
                                        if (Personal_Data != null && Personal_Data.Count > 0)
                                        {
                                            foreach (XElement xe in Personal_Data)
                                            {
                                                XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                XElement Region = Address.Descendants("state").FirstOrDefault();
                                                XElement County = Address.Descendants("county").FirstOrDefault();


                                                XElement FName = xe.Descendants("firstname").FirstOrDefault();
                                                XElement MName = xe.Descendants("middlename").FirstOrDefault();
                                                XElement LName = xe.Descendants("lastname").FirstOrDefault();
                                                XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                if (EffectiveDate != null)
                                                {
                                                    string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";
                                                    TiredSearchedData objTiered = new TiredSearchedData();
                                                    if (Region != null && County != null && LName != null && YearMonth != null)
                                                    {
                                                        var region_value = Region.Value.ToString();
                                                        int check_item = 1;
                                                        for (int i = 0; i < ObjRegion_Name.Count; i++)
                                                        {
                                                            if (ObjRegion_Name.ElementAt(i).Region.ToString() == region_value)
                                                            {
                                                                check_item = 2;
                                                            }
                                                        }
                                                        if (check_item != 2)
                                                        {
                                                            string FirstName = string.Empty;
                                                            string LastName = string.Empty;
                                                            string MiddleName = string.Empty;
                                                            if (FName != null) { FirstName = FName.Value.ToString(); }
                                                            if (LName != null) { LastName = LName.Value.ToString(); }
                                                            if (MName != null) { MiddleName = MName.Value.ToString(); }




                                                            ObjRegion_Name.Add(new Region_Name() { Region = Region.Value.ToString(), Fname = FirstName, Lname = LastName, Mname = MiddleName });
                                                        }

                                                        var ArrayCollection = YearMonth.Split('-');
                                                        int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                        int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                        if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                        {
                                                            objTiered.county = County.Value;
                                                            objTiered.state = Region.Value;
                                                            objTiered.fName = FName != null ? FName.Value : "";
                                                            objTiered.mName = MName != null ? MName.Value : "";
                                                            objTiered.lName = LName.Value;
                                                            searchedDataList1.Add(objTiered);
                                                        }
                                                    }
                                                }
                                            }
                                            var duplicatesRemoved = ObjRegion_Name.Distinct().ToList();
                                            string firstname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                                            string lastname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;
                                            string middlename_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                            List<State_Name> CollectionStateName = Objbalcompany.GetStateList(duplicatesRemoved, SearchedStateName, firstname_item, lastname_item, middlename_item);

                                            Guid pkProductIdfcr = Guid.Parse("0f0016cf-c214-44d6-9b41-cda20dee2251");
                                            List<JurisdictionColl> CollectionJudistration = Objbalcompany.GetJurisdictionList(CollectionStateName, pkProductIdfcr);
                                            if (CollectionJudistration != null && CollectionJudistration.Count > 0)
                                            {
                                                var lst = searchedDataList1.ToList();
                                                lst.ForEach(n => searchedDataList1.Add(new TiredSearchedData
                                                {
                                                    county = n.county,
                                                    state = n.state,
                                                    fName = n.fName,
                                                    mName = n.mName,
                                                    lName = n.lName
                                                }));
                                                var searchedDataList = searchedDataList1.Select(n => new
                                                {
                                                    county = n.county,
                                                    state = n.state,
                                                    mName = n.mName,
                                                    fName = n.fName,
                                                    lName = n.lName,
                                                    MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                var lst_fcr = CollectionJudistration.ToList();
                                                lst_fcr.ForEach(n => CollectionJudistration.Add(new JurisdictionColl
                                                {
                                                    Jurisdiction_Name = n.Jurisdiction_Name,
                                                    pkJurisdictionId = n.pkJurisdictionId,
                                                    AdditionalFee = n.AdditionalFee,
                                                    fName = n.fName,
                                                    mName = n.mName,
                                                    lName = n.lName

                                                }));
                                                var CollectionJudistration_item = CollectionJudistration.Select(n => new
                                                {
                                                    Jurisdiction_Name = n.Jurisdiction_Name,
                                                    pkJurisdictionId = n.pkJurisdictionId,
                                                    AdditionalFee = n.AdditionalFee,
                                                    fName = n.fName,
                                                    mName = n.mName,
                                                    lName = n.lName,
                                                    MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                if (CollectionJudistration_item.Count > 0)
                                                {
                                                    for (int i = 0; i < CollectionJudistration_item.Count; i++)
                                                    {
                                                        ObjJurisdictionListColl.Add(new JurisdictionListColl() { AdditionalFee = decimal.Parse(CollectionJudistration_item.ElementAt(i).AdditionalFee.ToString()), pkJurisdictionId = int.Parse(CollectionJudistration_item.ElementAt(i).pkJurisdictionId.ToString()), Jurisdiction_Name = CollectionJudistration_item.ElementAt(i).Jurisdiction_Name.ToString(), fName = CollectionJudistration_item.ElementAt(i).fName, mName = CollectionJudistration_item.ElementAt(i).mName, lName = CollectionJudistration_item.ElementAt(i).lName });
                                                    }

                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                else if (ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Contains("SCR"))
                                {

                                    //Changed By Himesh Kumar
                                    #region Optional SCR

                                    if (CreditFile != null && CreditFile.Count > 0)
                                    {
                                        List<XElement> PersonalData = CreditFile.Descendants("record").ToList();
                                        if (PersonalData != null && PersonalData.Count > 0)
                                        {
                                            foreach (XElement xe in PersonalData)
                                            {
                                                #region---Old Code
                                                //XElement Region = xe.Descendants("Region").FirstOrDefault();
                                                //XElement County = xe.Descendants("County").FirstOrDefault();
                                                //if (Region != null && County != null)
                                                //{
                                                //    if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                //        DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                //}
                                                #endregion

                                                #region---New Code For 7 year filter
                                                XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                XElement Region = Address.Descendants("state").FirstOrDefault();
                                                XElement County = Address.Descendants("county").FirstOrDefault();
                                                XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                if (EffectiveDate != null)
                                                {
                                                    string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";

                                                    TiredSearchedData objTiered = new TiredSearchedData();
                                                    if (Region != null && County != null && YearMonth != null)
                                                    {
                                                        var ArrayCollection = YearMonth.Split('-');
                                                        int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                        int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                        if (TimeFrame == 0) //ticket #87
                                                        {
                                                            if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                                DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                        }
                                                        else if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                        {
                                                            if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                                DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                        }
                                                    }
                                                }
                                                #endregion
                                            }
                                            string[] StateArray = new BALGeneral().GetDisabledStates().Select(d => d.StateCode).ToArray();

                                            List<string> lstAutoState = new List<string>();
                                            if (DictCounty.Count > 0)
                                            {
                                                foreach (var GetState in DictCounty)
                                                {
                                                    if (!lstAutoState.Contains(GetState.Value))
                                                    {
                                                        if (!StateArray.Contains(GetState.Value))
                                                        {
                                                            STATES = STATES + "##chk_" + GetState.Key + "_" + GetState.Value + "_State" + "_" + GivenFname + "_" + GivenMname + "_" + givenLname + "_name_0";
                                                            lstAutoState.Add(GetState.Value);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        continue;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    #endregion
                                }


                                if (!string.IsNullOrEmpty(ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode))
                                {
                                    //Tiered3 Package functionality.
                                    if (ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode.Contains("CCR"))
                                    {
                                        //Changed By Himesh Kumar
                                        #region Tiered CCR1
                                        if (CreditFile != null && CreditFile.Count > 0)
                                        {
                                            List<XElement> PersonalData = CreditFile.Descendants("record").ToList();
                                            if (PersonalData != null && PersonalData.Count > 0)
                                            {

                                                foreach (XElement xe in PersonalData)
                                                {
                                                    #region---Old Code
                                                    //XElement Region = xe.Descendants("Region").FirstOrDefault();
                                                    //XElement County = xe.Descendants("County").FirstOrDefault();
                                                    //if (Region != null && County != null)
                                                    //{
                                                    //    if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                    //        DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                    //}
                                                    #endregion
                                                    #region----New code
                                                    XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                    XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                    XElement Region = Address.Descendants("state").FirstOrDefault();
                                                    XElement County = Address.Descendants("county").FirstOrDefault();
                                                    XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                    if (EffectiveDate != null)
                                                    {
                                                        string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";

                                                        TiredSearchedData objTiered = new TiredSearchedData();
                                                        if (Region != null && County != null && YearMonth != null)
                                                        {
                                                            var ArrayCollection = YearMonth.Split('-');
                                                            int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                            int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                            if (TimeFrame == 0) //ticket #87
                                                            {
                                                                if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                                {
                                                                    DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                                }
                                                            }
                                                            else if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                            {
                                                                if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                                {
                                                                    DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                List<string> lstAutoCounty = new List<string>();
                                                List<string> lstcounty = new BALCompany().GetCountyList();

                                                foreach (var DictObj in DictCounty)
                                                {
                                                    if (!lstAutoCounty.Contains(DictObj.Key))
                                                    {
                                                        if (!lstcounty.Contains(DictObj.Key.ToUpper()))
                                                        {
                                                            flagAutopopup = true;
                                                        }
                                                        COUNTIES = COUNTIES + "##chk_" + DictObj.Key + "_" + DictObj.Value + "_County" + "_" + GivenFname + "_" + GivenMname + "_" + givenLname + "_name_0";
                                                        lstAutoCounty.Add(DictObj.Key);
                                                    }
                                                    else
                                                    {
                                                        continue;
                                                    }
                                                }

                                            }
                                        }
                                        #endregion
                                    }
                                    else if (ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode.Contains("FCR"))
                                    {
                                        //Changed By Himesh Kumar
                                        #region Tiered FCR
                                        List<XElement> Credit_File = ObjXDocument.Descendants("records").ToList();
                                        List<TiredSearchedData> searchedDataList1 = new List<TiredSearchedData>();

                                        if (Credit_File != null && Credit_File.Count > 0)
                                        {
                                            List<XElement> Personal_Data = Credit_File.Descendants("record").ToList();

                                            List<Region_Name> ObjRegion_Name = new List<Region_Name>();
                                            if (Personal_Data != null && Personal_Data.Count > 0)
                                            {
                                                foreach (XElement xe in Personal_Data)
                                                {
                                                    XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                    XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                    XElement Region = Address.Descendants("state").FirstOrDefault();
                                                    XElement County = Address.Descendants("county").FirstOrDefault();


                                                    XElement FName = xe.Descendants("firstname").FirstOrDefault();
                                                    XElement MName = xe.Descendants("middlename").FirstOrDefault();
                                                    XElement LName = xe.Descendants("lastname").FirstOrDefault();
                                                    XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                    if (EffectiveDate != null)
                                                    {
                                                        string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";
                                                        TiredSearchedData objTiered = new TiredSearchedData();
                                                        if (Region != null && County != null && LName != null && YearMonth != null)
                                                        {
                                                            var region_value = Region.Value.ToString();
                                                            int check_item = 1;
                                                            for (int i = 0; i < ObjRegion_Name.Count; i++)
                                                            {
                                                                if (ObjRegion_Name.ElementAt(i).Region.ToString() == region_value)
                                                                {
                                                                    check_item = 2;
                                                                }
                                                            }
                                                            if (check_item != 2)
                                                            {
                                                                string FirstName = string.Empty;
                                                                string LastName = string.Empty;
                                                                string MiddleName = string.Empty;
                                                                if (FName != null) { FirstName = FName.Value.ToString(); }
                                                                if (LName != null) { LastName = LName.Value.ToString(); }
                                                                if (MName != null) { MiddleName = MName.Value.ToString(); }




                                                                ObjRegion_Name.Add(new Region_Name() { Region = Region.Value.ToString(), Fname = FirstName, Lname = LastName, Mname = MiddleName });
                                                            }

                                                            var ArrayCollection = YearMonth.Split('-');
                                                            int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                            int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                            if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                            {
                                                                objTiered.county = County.Value;
                                                                objTiered.state = Region.Value;
                                                                objTiered.fName = FName != null ? FName.Value : "";
                                                                objTiered.mName = MName != null ? MName.Value : "";
                                                                objTiered.lName = LName.Value;
                                                                searchedDataList1.Add(objTiered);
                                                            }
                                                        }
                                                    }
                                                }
                                                var duplicatesRemoved = ObjRegion_Name.Distinct().ToList();
                                                string firstname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                                                string lastname_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;
                                                string middlename_item = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                                                List<State_Name> CollectionStateName = Objbalcompany.GetStateList(duplicatesRemoved, SearchedStateName, firstname_item, lastname_item, middlename_item);

                                                Guid pkProductIdfcr = Guid.Parse("0f0016cf-c214-44d6-9b41-cda20dee2251");
                                                List<JurisdictionColl> CollectionJudistration = Objbalcompany.GetJurisdictionList(CollectionStateName, pkProductIdfcr);
                                                if (CollectionJudistration != null && CollectionJudistration.Count > 0)
                                                {
                                                    var lst = searchedDataList1.ToList();
                                                    lst.ForEach(n => searchedDataList1.Add(new TiredSearchedData
                                                    {
                                                        county = n.county,
                                                        state = n.state,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName
                                                    }));
                                                    var searchedDataList = searchedDataList1.Select(n => new
                                                    {
                                                        county = n.county,
                                                        state = n.state,
                                                        mName = n.mName,
                                                        fName = n.fName,
                                                        lName = n.lName,
                                                        MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                    }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                    var lst_fcr = CollectionJudistration.ToList();
                                                    lst_fcr.ForEach(n => CollectionJudistration.Add(new JurisdictionColl
                                                    {
                                                        Jurisdiction_Name = n.Jurisdiction_Name,
                                                        pkJurisdictionId = n.pkJurisdictionId,
                                                        AdditionalFee = n.AdditionalFee,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName

                                                    }));
                                                    var CollectionJudistration_item = CollectionJudistration.Select(n => new
                                                    {
                                                        Jurisdiction_Name = n.Jurisdiction_Name,
                                                        pkJurisdictionId = n.pkJurisdictionId,
                                                        AdditionalFee = n.AdditionalFee,
                                                        fName = n.fName,
                                                        mName = n.mName,
                                                        lName = n.lName,
                                                        MatchingStatus = MatchMeterResult(n.fName.ToLower(), n.mName.ToLower(), n.lName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial.ToLower(), ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToLower())
                                                    }).Distinct().OrderBy(n => n.MatchingStatus).ThenBy(n => n.fName).ThenBy(n => n.mName).ThenBy(n => n.lName).ToList();


                                                    if (CollectionJudistration_item.Count > 0)
                                                    {
                                                        for (int i = 0; i < CollectionJudistration_item.Count; i++)
                                                        {
                                                            ObjJurisdictionListColl.Add(new JurisdictionListColl() { AdditionalFee = decimal.Parse(CollectionJudistration_item.ElementAt(i).AdditionalFee.ToString()), pkJurisdictionId = int.Parse(CollectionJudistration_item.ElementAt(i).pkJurisdictionId.ToString()), Jurisdiction_Name = CollectionJudistration_item.ElementAt(i).Jurisdiction_Name.ToString(), fName = CollectionJudistration_item.ElementAt(i).fName, mName = CollectionJudistration_item.ElementAt(i).mName, lName = CollectionJudistration_item.ElementAt(i).lName });
                                                        }

                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                    else if (ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode.Contains("SCR"))
                                    {
                                        //Changed By Himesh Kumar
                                        #region Optional SCR

                                        if (CreditFile != null && CreditFile.Count > 0)
                                        {
                                            List<XElement> PersonalData = CreditFile.Descendants("record").ToList();
                                            if (PersonalData != null && PersonalData.Count > 0)
                                            {
                                                foreach (XElement xe in PersonalData)
                                                {
                                                    #region---Old Code
                                                    //XElement Region = xe.Descendants("Region").FirstOrDefault();
                                                    //XElement County = xe.Descendants("County").FirstOrDefault();
                                                    //if (Region != null && County != null)
                                                    //{
                                                    //    if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                    //        DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                    //}
                                                    #endregion

                                                    #region---New Code For 7 year filter
                                                    XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                    XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                    XElement Region = Address.Descendants("state").FirstOrDefault();
                                                    XElement County = Address.Descendants("county").FirstOrDefault();
                                                    XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                    if (EffectiveDate != null)
                                                    {
                                                        string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";

                                                        TiredSearchedData objTiered = new TiredSearchedData();
                                                        if (Region != null && County != null && YearMonth != null)
                                                        {
                                                            var ArrayCollection = YearMonth.Split('-');
                                                            int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                            int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                            if (TimeFrame == 0) //ticket #87
                                                            {
                                                                if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                                    DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                            }
                                                            else if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                            {
                                                                if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                                    DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                string[] StateArray = new BALGeneral().GetDisabledStates().Select(d => d.StateCode).ToArray();

                                                List<string> lstAutoState = new List<string>();
                                                if (DictCounty.Count > 0)
                                                {
                                                    foreach (var GetState in DictCounty)
                                                    {
                                                        if (!lstAutoState.Contains(GetState.Value))
                                                        {
                                                            if (!StateArray.Contains(GetState.Value))
                                                            {
                                                                STATES = STATES + "##chk_" + GetState.Key + "_" + GetState.Value + "_State" + "_" + GivenFname + "_" + GivenMname + "_" + givenLname + "_name_0";
                                                                lstAutoState.Add(GetState.Value);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            continue;
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        #endregion
                                    }
                                }

                                if (!string.IsNullOrEmpty(ObjEmergeReport.ObjOrderInfo.OptionalProductCode))
                                {
                                    if (ObjEmergeReport.ObjOrderInfo.OptionalProductCode.Contains("SCR"))
                                    {
                                        //Changed By Himesh Kumar
                                        #region Optional SCR

                                        if (CreditFile != null && CreditFile.Count > 0)
                                        {
                                            List<XElement> PersonalData = CreditFile.Descendants("record").ToList();
                                            if (PersonalData != null && PersonalData.Count > 0)
                                            {
                                                foreach (XElement xe in PersonalData)
                                                {
                                                    #region---Old Code
                                                    //XElement Region = xe.Descendants("Region").FirstOrDefault();
                                                    //XElement County = xe.Descendants("County").FirstOrDefault();
                                                    //if (Region != null && County != null)
                                                    //{
                                                    //    if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                    //        DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                    //}
                                                    #endregion

                                                    #region---New Code For 7 year filter
                                                    XElement Addresses = xe.Descendants("addresses").FirstOrDefault();
                                                    XElement Address = Addresses.Descendants("address").FirstOrDefault();
                                                    XElement Region = Address.Descendants("state").FirstOrDefault();
                                                    XElement County = Address.Descendants("county").FirstOrDefault();
                                                    XElement EffectiveDate = Address.Descendants("to-date").FirstOrDefault();
                                                    if (EffectiveDate != null)
                                                    {
                                                        string YearMonth = Convert.ToString(EffectiveDate.Attribute("year").Value) + "-0";

                                                        TiredSearchedData objTiered = new TiredSearchedData();
                                                        if (Region != null && County != null && YearMonth != null)
                                                        {
                                                            var ArrayCollection = YearMonth.Split('-');
                                                            int StartYear = Convert.ToInt32(ArrayCollection[0]);
                                                            int StartMonth = Convert.ToInt32(ArrayCollection[1]);
                                                            if (TimeFrame == 0) //ticket #87
                                                            {
                                                                if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                                    DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                            }
                                                            else if (StartYear >= TimeFrameDate.Year) //&& StartMonth >= TimeFrameDate.Month
                                                            {
                                                                if (DictCounty.Where(d => d.Key == County.Value).Count() == 0)
                                                                    DictCounty.Add(County.Value.ToString(), Region.Value.ToString());
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                string[] StateArray = new BALGeneral().GetDisabledStates().Select(d => d.StateCode).ToArray();

                                                List<string> lstAutoState = new List<string>();
                                                if (DictCounty.Count > 0)
                                                {
                                                    foreach (var GetState in DictCounty)
                                                    {
                                                        if (!lstAutoState.Contains(GetState.Value))
                                                        {
                                                            if (!StateArray.Contains(GetState.Value))
                                                            {
                                                                STATES = STATES + "##chk_" + GetState.Key + "_" + GetState.Value + "_State" + "_" + GivenFname + "_" + GivenMname + "_" + givenLname + "_name_0";
                                                                lstAutoState.Add(GetState.Value);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            continue;
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        #endregion
                                    }
                                }
                            }
                            catch
                            {
                                response = new StringBuilder();
                                responseOptional = new StringBuilder();
                            }
                            #endregion
                        }
                        else if (ObjEmergeReport.ObjOrderInfo.HostProductCode == "CCR1")
                        {

                            GivenFname = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                            GivenMname = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial;
                            givenLname = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;

                            #region HostedResponse for CCR
                            try
                            {
                                List<XElement> stateregion = null;
                                List<XElement> reportrequested = null;
                                stateregion = ObjXDocument.Descendants("Region").ToList();
                                if (stateregion != null)
                                {
                                    if (stateregion.Count == 0)
                                    {
                                        stateregion = ObjXDocument.Descendants("State").ToList();
                                    }
                                    if (stateregion.Count == 0)
                                    {
                                        reportrequested = ObjXDocument.Descendants("report-requested").ToList();
                                        if (reportrequested != null && reportrequested.Count() > 0)
                                        {
                                            stateregion = reportrequested.Descendants("state").ToList();
                                        }
                                    }
                                }

                                if (stateregion != null && stateregion.Count > 0)
                                {

                                    #region Tiered SCR

                                    if (stateregion != null && stateregion.Count > 0)
                                    {
                                        foreach (XElement xe in stateregion)
                                        {
                                            if (xe != null)
                                            {
                                                if (DictCounty.Where(d => d.Key == xe.Value).Count() == 0)
                                                    DictCounty.Add(xe.Value.ToString(), xe.Value.ToString());
                                            }
                                        }

                                        //foreach (var DictObj in DictCounty)
                                        //{
                                        //    COUNTIES = COUNTIES + "##chk_" + DictObj.Key + "_" + DictObj.Value + "_County" + "_" + GivenFname + "_" + GivenMname + "_" + givenLname + "_name_0";
                                        //}

                                        if (DictCounty.Count > 0)
                                        {
                                            COUNTIES = COUNTIES + "##chk_" + DictCounty.First().Key + "_" + DictCounty.First().Value + "_State_" + "0".ToString();
                                        }
                                        response.Append("</table>");

                                    }
                                    #endregion

                                    #region Optional PS

                                    if (stateregion != null && stateregion.Count > 0)
                                    {
                                        foreach (XElement xe in stateregion)
                                        {
                                            if (xe != null)
                                            {
                                                if (DictCounty.Where(d => d.Key == xe.Value).Count() == 0)
                                                    DictCounty.Add(xe.Value.ToString(), xe.Value.ToString());
                                            }
                                        }

                                        //foreach (var GetState in DictCounty)
                                        //{
                                        //    STATES = STATES + "##chk_" + GetState.Key + "_" + GetState.Value + "_State" + "_" + GivenFname + "_" + GivenMname + "_" + givenLname + "_name_0";
                                        //}

                                        if (DictCounty.Count > 0)
                                        {
                                            STATES = STATES + "##chk_" + DictCounty.First().Key + "_" + DictCounty.First().Value + "_State_" + "0".ToString();
                                        }


                                    }

                                    #endregion
                                }
                            }
                            catch
                            {
                                response = new StringBuilder();
                                responseOptional = new StringBuilder();
                            }
                            #endregion
                        }

                        #endregion

                        #region OrderInfo Region

                        ObjEmergeReport.ObjOrderInfo.OrderId = OrderId;
                        ObjEmergeReport.ObjOrderInfo.Tiered2Response = response.ToString();
                        if (ObjEmergeReport.ObjTieredPackage.TiredOptional != null)
                            ObjEmergeReport.ObjOrderInfo.OptionalResponse = responseOptional.ToString();
                        else
                            ObjEmergeReport.ObjOrderInfo.OptionalResponse = "";
                        ObjEmergeReport.ObjOrderInfo.TieredTopInfo = TieredHeadText.ToString();

                        #endregion

                        Session["EmergeReport"] = ObjEmergeReport;

                    }

                    #endregion
                }
            }
            try
            {
                string IsDataEntryOnly = "false";
                #region Check Data Entry User


                if (Session["UserRoles"] != null)
                {
                    string[] role = (string[])(Session["UserRoles"]);
                    if (role.Contains("DataEntry"))
                    {
                        ObjEmergeReport.ObjOrderInfo.IsDataEntry_Enable = true;
                    }
                    else
                    {
                        IsDataEntryOnly = GetDataEntryCheck();
                        if (!string.IsNullOrEmpty(IsDataEntryOnly))
                        {
                            ObjEmergeReport.ObjOrderInfo.IsDataEntry_Enable = Convert.ToBoolean(IsDataEntryOnly);
                        }
                    }
                }
                else
                {
                    #region If Session expire check from membership
                    MembershipUser ObjMembership = Membership.GetUser(User.Identity.Name);
                    if (ObjMembership != null)
                    {
                        string[] role = Roles.GetRolesForUser(User.Identity.Name);
                        Session["UserRoles"] = role;
                        if (role.Contains("DataEntry"))
                        {
                            ObjEmergeReport.ObjOrderInfo.IsDataEntry_Enable = true;
                        }
                    }
                    #endregion

                }
                #endregion

                if (ObjEmergeReport.ObjOrderInfo.OrderId != null)
                {
                    //As per client requirment, they need a notification during the tiered package if package name contains FACIS and send notification to support@intelifi.com
                    ObjBALOrders.verifyPackageFACISAndSendNotification(Convert.ToInt32(OrderId));
                }

            }
            catch// (Exception ex)
            {
            }

            return Json(new { ReportPage = flagAutopopup, Goarchive = flagtoarchive, OrderNo = ObjEmergeReport.ObjOrderInfo.OrderId, Coll = ObjEmergeReport, counties = COUNTIES, states = STATES, GrandTotal = GrandTotal, JurisdictionColl = ObjJurisdictionListColl });

        }






        public string GetDataEntryCheck()
        {
            string IsDataEntryOnly = "";
            try
            {
                string username = Convert.ToString(Session["CurrentUserMailId"]);
                if (username == string.Empty)
                {
                    username = User.Identity.Name;
                }
                ProfileCommon Profile = new ProfileCommon();
                ProfileModel userProfile = Profile.GetProfile(username);
                IsDataEntryOnly = userProfile.IsDataEntryOnly_Enable;
                //if (!string.IsNullOrEmpty(Profile.SearchParams.IsDataEntryOnly_Enable))
                //{
                //    IsDataEntryOnly = Profile.SearchParams.IsDataEntryOnly_Enable.ToLower();
                //}
            }
            catch
            {
            }
            return IsDataEntryOnly;
        }

        public static string GetRequiredXmlConditional(string XmlResponse, string ProductCode)
        {
            string XmlResponse_Local = XmlResponse;
            switch (ProductCode)
            {
                    //Removed PS product case because its is not required to get substring of response for this product --Changed By Himesh Kumar
                case "NCR1":
                case "NCR+":
                case "RCX":
                case "CCR1":
                    {
                        if (XmlResponse_Local.IndexOf("<BackgroundReportPackage>") > 0 && XmlResponse_Local.IndexOf("</BackgroundReports>") > 0)
                        {
                            XmlResponse_Local = XmlResponse_Local.Replace("</BackgroundReports>", "");
                            int xmlstartindex = XmlResponse_Local.IndexOf("<BackgroundReportPackage>");
                            XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                        } break;
                    }
                case "SCR":
                    {
                        if (XmlResponse_Local.IndexOf("<BackgroundReportPackage>") != -1)
                        {
                            XmlResponse_Local = XmlResponse_Local.Replace("</BackgroundReports>", "");
                            int xmlstartindex = XmlResponse_Local.IndexOf("<BackgroundReportPackage>");
                            XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
                        } break;
                    }
                
                default:
                    break;
            }


            return XmlResponse_Local;
        }

        private decimal GetReportFee(int LocationId, int CompanyUserId, string ReportCode, string ReportType, int packageId)
        {
            decimal fee = 0;

            if (ReportType == "Tiered2")
            {
                using (EmergeDALDataContext db = new EmergeDALDataContext())
                {
                    var feeVar2 = db.tblProductPerApplications.Join(db.tblTieredPackages, tblProduct => tblProduct.pkProductApplicationId,
                                                             tblTiered => tblTiered.Tired2, (tblProduct, tblTiered) => new
                                                             {
                                                                 fkPackageId = tblTiered.fkPackageId,
                                                                 reportcode = tblProduct.ProductCode,
                                                                 fee = tblTiered.Tired2Price
                                                             }).Where(d => d.fkPackageId == packageId && d.reportcode == ReportCode).FirstOrDefault();

                    if (feeVar2 != null)
                    {
                        fee = Convert.ToInt32(feeVar2.fee);
                    }
                    else
                    {
                        var feeIfNull = db.tblProductAccesses.Join(db.tblProductPerApplications, ProductAccesses => ProductAccesses.fkProductApplicationId,
                                                                           ProductPerApp => ProductPerApp.pkProductApplicationId, (ProductAccesses, ProductPerApp) => new
                                                                           {
                                                                               fkLocationId = ProductAccesses.fkLocationId,
                                                                               reportcode = ProductPerApp.ProductCode,
                                                                               fee = ProductAccesses.LocationProductPrice
                                                                           }).Where(d => d.fkLocationId == LocationId && d.reportcode == ReportCode).FirstOrDefault();
                        if (feeIfNull != null)
                        {
                            fee = Convert.ToInt32(feeIfNull.fee);
                        }
                        else
                        {
                            fee = 0;
                        }
                    }
                }

            }
            else if (ReportType == "Tiered3")
            {
                using (EmergeDALDataContext db = new EmergeDALDataContext())
                {

                    decimal? fee2 = 0;

                    var fee2Val = db.tblProductPerApplications.Join(db.tblTieredPackages, tblProduct => tblProduct.pkProductApplicationId,
                                                             tblTiered => tblTiered.Tired3, (tblProduct, tblTiered) => new
                                                             {
                                                                 fkPackageId = tblTiered.fkPackageId,
                                                                 reportcode = tblProduct.ProductCode,
                                                                 fee = tblTiered.Tired3Price
                                                             }).Where(d => d.fkPackageId == packageId && d.reportcode == ReportCode).FirstOrDefault();
                    if (fee2Val != null)
                    {
                        fee2 = Convert.ToInt32(fee2Val.fee);
                    }
                    else
                    {
                        var feeaccess = db.tblProductAccesses.Join(db.tblProductPerApplications, ProductAccesses => ProductAccesses.fkProductApplicationId,
                                                                           ProductPerApp => ProductPerApp.pkProductApplicationId, (ProductAccesses, ProductPerApp) => new
                                                                           {
                                                                               fkLocationId = ProductAccesses.fkLocationId,
                                                                               reportcode = ProductPerApp.ProductCode,
                                                                               fee = ProductAccesses.LocationProductPrice
                                                                           }).Where(d => d.fkLocationId == LocationId && d.reportcode == ReportCode).FirstOrDefault();
                        if (feeaccess != null)
                        {
                            fee = Convert.ToInt32(feeaccess.fee);
                        }
                        else
                        {
                            fee = 0;
                        }

                    }
                }

            }
            else if (ReportType == "Optional")
            {
                using (EmergeDALDataContext db = new EmergeDALDataContext())
                {
                    decimal? fee2 = db.tblProductPerApplications.Join(db.tblTieredPackages, tblProduct => tblProduct.pkProductApplicationId,
                                                            tblTiered => tblTiered.TiredOptional, (tblProduct, tblTiered) => new
                                                            {
                                                                fkPackageId = tblTiered.fkPackageId,
                                                                reportcode = tblProduct.ProductCode,
                                                                fee = tblTiered.OptionalPrice
                                                            }).Where(d => d.fkPackageId == packageId && d.reportcode == ReportCode).FirstOrDefault().fee;
                    if (fee2 == null)
                    {
                        fee = db.tblProductAccesses.Join(db.tblProductPerApplications, ProductAccesses => ProductAccesses.fkProductApplicationId,
                                                        ProductPerApp => ProductPerApp.pkProductApplicationId, (ProductAccesses, ProductPerApp) => new
                                                        {
                                                            fkLocationId = ProductAccesses.fkLocationId,
                                                            reportcode = ProductPerApp.ProductCode,
                                                            fee = ProductAccesses.LocationProductPrice
                                                        }).Where(d => d.fkLocationId == LocationId && d.reportcode == ReportCode).FirstOrDefault().fee;
                    }
                    else
                    {
                        fee = fee2.Value;
                    }

                }
            }

            return fee;
        }

        private int MatchMeterResult(string oldFName, string oldMName, string oldLName, string newFName, string newMName, string newLName)
        {

            int result = 10;
            if ((oldFName == newFName) && (oldMName == newMName) && (oldLName == newLName))
            {
                //result = "ico_match_green.jpg";
                result = 1;
            }
            else if (oldMName != "" || newMName != "")
            {
                if ((oldFName == newFName) || (oldMName == newMName) || (oldLName == newLName))
                {
                    //result = "ico_red_yellow.png";
                    result = 2;
                }
                else
                {
                    //result = "ico_red_match.png";
                    result = 3;
                }
            }
            else
            {
                if ((oldFName == newFName) || (oldLName == newLName))
                {
                    //result = "ico_red_yellow.png";
                    result = 2;
                }
                else
                {
                    //result = "ico_red_match.png";
                    result = 3;
                }
            }
            return result;
        }

        #endregion

        #region Searching Tiered Orders Test
        [HttpPost]
        public ActionResult SearchingTieredOrders(int OrderId, string Counties, string States, List<JurisdictionListColl> JurisdictionList)
        {
            VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "TieredPackageThread");
            VendorServices.GeneralService.WriteLog("1/16 Package thread start: " + DateTime.Now.ToString(), "TieredPackageThread");
            EmergeReport ObjEmergeReport = new EmergeReport();
            if (Session["EmergeReport"] != null)
            {
                VendorServices.GeneralService.WriteLog("2 Session['EmergeReport'] Value has been get : " + DateTime.Now.ToString(), "TieredPackageThread");

                ObjEmergeReport = (EmergeReport)Session["EmergeReport"];

                VendorServices.GeneralService.WriteLog("3 Session['EmergeReport'] Value has been set : " + DateTime.Now.ToString(), "TieredPackageThread");
            }

            int CompanyUserId = (Session["CurrentUserId"] == null) ? 0 : Convert.ToInt32(Session["CurrentUserId"].ToString());
            ObjEmergeReport.ObjtblOrder.CreatedById = CompanyUserId;
            HttpContext context = System.Web.HttpContext.Current;
            System.Threading.Thread tieredThread = new System.Threading.Thread(() =>
            {
                System.Web.HttpContext.Current = context;
                ObjEmergeReport = CallThreadForTiered(OrderId, Counties, States, JurisdictionList, ObjEmergeReport);
            });
            tieredThread.Start();
            tieredThread.Priority = System.Threading.ThreadPriority.AboveNormal;
            Session["EmergeReport"] = ObjEmergeReport;
            return Json(0);
        }


        public void SearchingTieredOrdersLandingPage(int OrderId, string Counties, string States, List<JurisdictionListColl> JurisdictionList)
        {
            VendorServices.GeneralService.WriteLog("---------------------------------------------------------------------------------------", "TieredPackageThread");
            VendorServices.GeneralService.WriteLog("1/16 Package thread start: " + DateTime.Now.ToString(), "TieredPackageThread");
            EmergeReport ObjEmergeReport = new EmergeReport();
            if (Session["EmergeReport"] != null)
            {
                VendorServices.GeneralService.WriteLog("2 Session['EmergeReport'] Value has been get : " + DateTime.Now.ToString(), "TieredPackageThread");

                ObjEmergeReport = (EmergeReport)Session["EmergeReport"];

                VendorServices.GeneralService.WriteLog("3 Session['EmergeReport'] Value has been set : " + DateTime.Now.ToString(), "TieredPackageThread");
            }


            HttpContext context = System.Web.HttpContext.Current;
            System.Threading.Thread tieredThread = new System.Threading.Thread(() =>
            {
                System.Web.HttpContext.Current = context;
                ObjEmergeReport = CallThreadForTiered(OrderId, Counties, States, JurisdictionList, ObjEmergeReport);
            });
            tieredThread.Start();
            tieredThread.Priority = System.Threading.ThreadPriority.AboveNormal;
            Session["EmergeReport"] = ObjEmergeReport;
        }



        public EmergeReport CallThreadForTiered(int OrderId, string Counties, string States, List<JurisdictionListColl> JurisdictionList, EmergeReport ObjEmergeReport)
        {

            VendorServices.GeneralService.WriteLog("4 Function calling start : " + DateTime.Now.ToString(), "TieredPackageThread");
            List<tblCounty> ColltblCounty = new List<tblCounty>();
            List<tblState> ColltblState = new List<tblState>();
            List<tblOrderSearchedLiveRunnerInfo> ColltblOrderSearchedLiveRunnerInfo = new List<tblOrderSearchedLiveRunnerInfo>();
            //List<string> CollStateCode = new List<string>();
            //Dictionary<string, string> DicStateColl = new Dictionary<string, string>();
            List<TiredSearchedData> DicStateColl = new List<TiredSearchedData>();

            List<TiredSearchedDataJurisdiction> DicJurisdictionColl = new List<TiredSearchedDataJurisdiction>();
            string sCounty = "", sStates = "";
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                ColltblCounty = dx.tblCounties.ToList<tblCounty>();
                ColltblState = dx.tblStates.ToList<tblState>();
            }
            sCounty = Counties;
            sStates = States;

            try
            {
                BALOrders ObjBALOrders = new BALOrders();
                if (ObjEmergeReport != null)
                {
                    VendorServices.GeneralService.WriteLog("5 Try block : " + DateTime.Now.ToString(), "TieredPackageThread");


                    #region CCR1 (County Based)
                    //To get the distinct county list 
                    var removeDuplicateCounties = (sCounty.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries)).Distinct().ToList();
                    //To create a county array.
                    string[] CountyCollFirst = new string[removeDuplicateCounties.Count()];

                    //string[] CountyCollMain = new string[removeDuplicateCounties.Count()];

                    //To get county from the list and add in array.
                    for (int strCountiesVal = 0; strCountiesVal < removeDuplicateCounties.Count(); strCountiesVal++)
                    {
                        CountyCollFirst[strCountiesVal] = removeDuplicateCounties[strCountiesVal].Substring(0, (removeDuplicateCounties[strCountiesVal]).Length - 1);//Adding counties in array.
                    }
                    var mainCountyList = CountyCollFirst.Distinct().ToList();
                    string[] CountyColl = new string[mainCountyList.Count()];
                    for (int strCountiesVal = 0; strCountiesVal < mainCountyList.Count(); strCountiesVal++)
                    {
                        CountyColl[strCountiesVal] = mainCountyList[strCountiesVal];//Adding counties in array.
                    }
                    if (CountyColl.Count() > 0)
                    {
                        ColltblOrderSearchedLiveRunnerInfo.Clear();
                        VendorServices.GeneralService.WriteLog("6 Loop start : " + DateTime.Now.ToString(), "TieredPackageThread");
                        for (int i = 0; i < CountyColl.Count(); i++)
                        {
                            tblCounty SingletblCounty = new tblCounty();
                            tblState SingletblState = new tblState();

                            string Region = "", County = "";
                            string[] Cnty = CountyColl.ElementAt(i).Split('_');
                            if (Cnty.Count() > 5)
                            {
                                Region = Cnty[2];
                                County = Cnty[1];
                               // string fName = Cnty[4];
                                //string lName = Cnty[6];
                               // string mName = Cnty[5] == "*" ? "" : Cnty[5];

                                SingletblState = ColltblState.Where(d => d.StateCode.ToLower() == Region.ToLower()).FirstOrDefault();
                                int pkStateId = 0;
                                if (SingletblState == null) { pkStateId = 0; }
                                else
                                {
                                    pkStateId = SingletblState.pkStateId == null ? 0 : SingletblState.pkStateId;
                                }
                                //SingletblCounty = ColltblCounty.Where(p => (p.CountyName.ToLower() == County.ToLower() && p.fkStateId == pkStateId)).FirstOrDefault();
                                if (pkStateId == 11 || pkStateId == 21)
                                {
                                    SingletblCounty = ColltblCounty.Where(p => (p.CountyName.ToLower().Contains(County.ToLower()) && p.fkStateId == pkStateId)).FirstOrDefault();
                                }
                                else
                                {
                                    SingletblCounty = ColltblCounty.Where(p => (p.CountyName.ToLower() == County.ToLower() && p.fkStateId == pkStateId)).FirstOrDefault();
                                }
                                if (SingletblCounty != null && SingletblState != null)
                                {
                                    tblOrderSearchedLiveRunnerInfo ObjBALtblOrderSearchedLiveRunnerInfo = new tblOrderSearchedLiveRunnerInfo();
                                    string IsRcxCounty = (SingletblCounty.IsRcxCounty) ? "True" : "False";

                                    ObjBALtblOrderSearchedLiveRunnerInfo.LiveRunnerCounty = SingletblCounty.CountyName + "_" + IsRcxCounty;   // ddlCountyInfoCounty.SelectedItem.Text.Trim() + "_" + IsRcxCounty;
                                    ObjBALtblOrderSearchedLiveRunnerInfo.LiveRunnerState = SingletblState.pkStateId.ToString();  //ddlCountyInfoState.SelectedValue;
                                    ObjBALtblOrderSearchedLiveRunnerInfo.SearchedCountyId = SingletblCounty.pkCountyId; //CountyId;
                                    ObjBALtblOrderSearchedLiveRunnerInfo.SearchedFirstName = Cnty[4];   // FirstName 
                                    ObjBALtblOrderSearchedLiveRunnerInfo.SearchedLastName = Cnty[6];    //LastName
                                    ObjBALtblOrderSearchedLiveRunnerInfo.SearchedMiddleName = Cnty[5];  //MiddleName
                                    if (ObjBALtblOrderSearchedLiveRunnerInfo.SearchedCountyId > 0 && ObjBALtblOrderSearchedLiveRunnerInfo.LiveRunnerCounty.Trim().Length > 0)
                                    {
                                        if (ColltblOrderSearchedLiveRunnerInfo.Where(d =>
                                            d.LiveRunnerCounty == ObjBALtblOrderSearchedLiveRunnerInfo.LiveRunnerCounty &&
                                            d.LiveRunnerState == ObjBALtblOrderSearchedLiveRunnerInfo.LiveRunnerState &&
                                            d.SearchedCountyId == ObjBALtblOrderSearchedLiveRunnerInfo.SearchedCountyId &&
                                            d.SearchedFirstName == ObjBALtblOrderSearchedLiveRunnerInfo.SearchedFirstName &&
                                            d.SearchedLastName == ObjBALtblOrderSearchedLiveRunnerInfo.SearchedLastName &&
                                            d.SearchedMiddleName == ObjBALtblOrderSearchedLiveRunnerInfo.SearchedMiddleName
                                            ).Count() == 0)
                                            ColltblOrderSearchedLiveRunnerInfo.Add(ObjBALtblOrderSearchedLiveRunnerInfo);
                                    }
                                }
                            }
                        }
                        VendorServices.GeneralService.WriteLog("7 Loop end : " + DateTime.Now.ToString(), "TieredPackageThread");
                    }

                    #endregion

                    #region SCR (State based)
                    VendorServices.GeneralService.WriteLog("8 SCR Start : " + DateTime.Now.ToString(), "TieredPackageThread");
                    //To get the distinct state list 
                    var removeDuplicateStates = (States.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries)).Distinct().ToList();
                    //To create a state array.
                    string[] StateColl = new string[removeDuplicateStates.Count()];
                    //To get state from the list and add in array.
                    for (int strStatesVal = 0; strStatesVal < removeDuplicateStates.Count(); strStatesVal++)
                    {
                        StateColl[strStatesVal] = removeDuplicateStates[strStatesVal];//Adding states in array.
                    }
                    if (StateColl.Count() > 0)
                    {

                        for (int i = 0; i < StateColl.Count(); i++)
                        {
                            tblCounty SingletblCounty = new tblCounty();
                            tblState SingletblState = new tblState();

                           // string Region = "";
                            string[] Stat = StateColl.ElementAt(i).Split('_');
                            if (Stat.Count() > 5)
                            {
                                //Region = Stat[2];
                                //string fName = Stat[4];
                                //string lName = Stat[6];
                                //string mName = Stat[5] == "*" ? "" : Stat[5];

                                TiredSearchedData objTiredSearchedData = new TiredSearchedData();
                                objTiredSearchedData.fName = Stat[4];
                                objTiredSearchedData.lName = Stat[6];
                                objTiredSearchedData.mName = Stat[5] == "*" ? "" : Stat[5];
                                objTiredSearchedData.state = Stat[2];

                                if (DicStateColl.Where(n => n.state.ToLower() == objTiredSearchedData.state.ToLower() &&
                                                      n.fName == objTiredSearchedData.fName &&
                                                      n.mName == objTiredSearchedData.lName &&
                                                      n.lName == objTiredSearchedData.mName).Count() == 0)
                                    DicStateColl.Add(objTiredSearchedData);
                                //if (DicStateColl.Where(d => d.Key == Region).Count() == 0)
                                //{
                                //    DicStateColl.Add(Region, ObjEmergeReport.ObjOrderInfo.OptionalProductCode);
                                //}

                            }
                        }

                    }
                    VendorServices.GeneralService.WriteLog("9 SCR End : " + DateTime.Now.ToString(), "TieredPackageThread");
                    #endregion
                    VendorServices.GeneralService.WriteLog("10 FCR start : " + DateTime.Now.ToString(), "TieredPackageThread");
                    #region FCR (Juridiction based)
                    if (JurisdictionList != null)
                    {

                        if (JurisdictionList.Count() > 0)
                        {

                            for (int i = 0; i < JurisdictionList.Count(); i++)
                            {
                                //tblJurisdiction SingletblCounty = new tblJurisdiction();
                                TiredSearchedDataJurisdiction objTiredSearchedData = new TiredSearchedDataJurisdiction();
                                objTiredSearchedData.fName = JurisdictionList.ElementAt(i).fName;
                                objTiredSearchedData.lName = JurisdictionList.ElementAt(i).lName;
                                objTiredSearchedData.mName = JurisdictionList.ElementAt(i).mName == "*" ? "" : JurisdictionList.ElementAt(i).mName;
                                objTiredSearchedData.Jurisdiction = JurisdictionList.ElementAt(i).Jurisdiction_Name;
                                DicJurisdictionColl.Add(objTiredSearchedData);

                            }

                        }

                    }

                    VendorServices.GeneralService.WriteLog("11 FCR End : " + DateTime.Now.ToString(), "TieredPackageThread");
                    #endregion

                    #region Tiered Reports Insertion

                    if (ColltblOrderSearchedLiveRunnerInfo.Count() > 0)
                    {
                        ObjEmergeReport.ObjColltblOrderSearchedLiveRunnerInfo = ColltblOrderSearchedLiveRunnerInfo;
                    }
                    if (!string.IsNullOrEmpty(ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode) && !string.IsNullOrEmpty(ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode))
                    {
                        ObjEmergeReport.ObjCollProducts.Remove(ObjEmergeReport.ObjOrderInfo.HostProductCode + "_" + ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Split('_').Last() + "_" + ObjEmergeReport.ObjOrderInfo.Tiered3ProductCode.Split('_').Last());
                    }
                    else
                    {
                        ObjEmergeReport.ObjCollProducts.Remove(ObjEmergeReport.ObjOrderInfo.HostProductCode + "_" + ObjEmergeReport.ObjOrderInfo.Tiered2ProductCode.Split('_').Last());
                    }
                    foreach (string ReportCode in ObjEmergeReport.ObjOrderInfo.NormalReport)
                    {
                        ObjEmergeReport.ObjCollProducts.Remove(ReportCode);
                    }

                    /// Here we need to add more counties to ObjColltblOrderSearchedLiveRunnerInfo.
                    /// That are selected by user.

                    VendorServices.GeneralService.WriteLog("12 UpdateOrders4Tiered start : " + DateTime.Now.ToString(), "TieredPackageThread");
                    OrderId = ObjBALOrders.UpdateOrders4Tiered(ObjEmergeReport.ObjtblOrder,
                                ObjEmergeReport.ObjCollProducts,
                                ObjEmergeReport.ObjtblOrderSearchedData,
                                ObjEmergeReport.ObjCollOrderSearchedEmploymentInfo,
                                ObjEmergeReport.ObjCollOrderSearchedEducationInfo,
                                ObjEmergeReport.ObjCollOrderSearchedPersonalInfo,
                                ObjEmergeReport.ObjCollOrderSearchedProfessionalInfo,
                                ObjEmergeReport.objColltblOrderPersonalQtnInfo,
                                ObjEmergeReport.ObjCollOrderSearchedLicenseInfo,
                                ObjEmergeReport.ObjColltblOrderSearchedLiveRunnerInfo,
                                ObjEmergeReport.ObjCollCLSConsentInfo,
                                Utility.SiteApplicationId, ObjEmergeReport.IsNullDOB,
                                ObjEmergeReport.IsLiveRunnerState, DicStateColl, DicJurisdictionColl, ObjEmergeReport.CompanyAccountNo, ObjEmergeReport.OrderCompanyName, ObjEmergeReport.PermissiblePurpose);
                    ObjEmergeReport.ObjColltblOrderSearchedLiveRunnerInfo.Clear();
                    VendorServices.GeneralService.WriteLog("13 UpdateOrders4Tiered end : " + DateTime.Now.ToString(), "TieredPackageThread");
                    VendorServices.GeneralService.WriteLog("14 Session value set : " + DateTime.Now.ToString(), "TieredPackageThread");

                    VendorServices.GeneralService.WriteLog("15 Session value set : " + DateTime.Now.ToString(), "TieredPackageThread");
                    #endregion
                    int CompanyUserId = (ObjEmergeReport.ObjtblOrder.CreatedById == null) ? 0 : Convert.ToInt32(ObjEmergeReport.ObjtblOrder.CreatedById);
                    ObjBALOrders.updateOrderDtForLandingPageOrders(ObjEmergeReport.ObjtblOrder.pkOrderId, CompanyUserId);
                }

            }
            catch (Exception ex)
            {
                VendorServices.GeneralService.WriteLog("16 Error in thread package: " + DateTime.Now.ToString(), "TieredPackageThread");
                VendorServices.GeneralService.WriteLog("StackTrace : " + ex.StackTrace, "TieredPackageThread");
                VendorServices.GeneralService.WriteLog("InnerException : " + ex.InnerException, "TieredPackageThread");
                VendorServices.GeneralService.WriteLog("16 Error in thread package: " + DateTime.Now.ToString(), "TieredPackageThread");
                //  throw ex;
            }
            return ObjEmergeReport;
        }


        #endregion








        public ActionResult TestMethod()
        {
            BALCompanyType obj = new BALCompanyType();
            var fee = obj.Get_County_Fee("Harris", "TX", 8917, "CCR1");
            return Content(fee.ToString());
        }


        [HttpPost]
        public ActionResult UpdateOrderStatusFcr(int OrderId)
        {
            string strMessage = "";
            BALCompany ObjBALCompany = new BALCompany();
            ObjBALCompany.UpdateOrderStatusFcr(OrderId);

            return Json(new { UpdateMessage = strMessage });
        }


        [HttpPost]
        public ActionResult RunAllCountyPopUp(string OrderId, string CountyState)
        {

            int Order_Id = Convert.ToInt32(OrderId);
            StringBuilder response = new StringBuilder();
            BALOrders ObjBALOrders = new BALOrders();
            BALCompanyType Objbalcompanytype = new BALCompanyType();
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            var Collection = ObjBALOrders.GetOrderSearchedData(Order_Id);
            string FirstName = Collection.SearchedFirstName;
            string LastName = Collection.SearchedLastName;
            string fullName = FirstName + " " + LastName;
            string MDI = Collection.SearchedMiddleInitial;
            int LocationId = 0;
            int CompanyUserId = 0;

            List<SelectListItem> objLIstItem = new List<SelectListItem>();
            string StateCode = string.Empty;
            int index = 1;

            MembershipUser Member = Membership.GetUser(User.Identity.Name);
            if (Member != null)
            {
                var dbCollection = ObjBALCompanyUsers.GetUserCompanyInfo(new Guid(Member.ProviderUserKey.ToString()));
                if (dbCollection.Count > 0)
                {
                    LocationId = Convert.ToInt32(dbCollection.First().pkLocationId.ToString());
                    CompanyUserId = Convert.ToInt32(dbCollection.First().pkCompanyUserId.ToString());
                }

            }
            decimal ReportFee = Get_Report_Fee(LocationId, CompanyUserId, "CCR1", "Tiered", 1);
           // List<CountyState> ObjCountyState = new List<CountyState>();

            var element = CountyState.ToString().Split(new[] { "countystate" }, StringSplitOptions.None);

            response.Append("<table id=\"tieredTable\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">");
            response.Append("<tr style=\"background-color:#fff;font-weight:bold;\"><td Colspan=\"6\">Record(s) found : RecordCount" +
                "</td></tr>");
            response.Append("<tr style=\"background-color:#CCCCCC;font-weight:bold;\" width=\"100%\"><td width=\"20%\">Full Name</td><td width=\"10%\">State</td><td width=\"10%\">County</td><td width=\"15%\">(ReportType)</td><td width=\"20%\">Cost</td> <td width=\"15%\"><input type=\"checkbox\" onclick=\"selectunselectCounty(this);\" id=\"chk_SelectAllCounty\"  />Select All</td><td width=\"15%\">Sub Total</td></tr>");
            for (int i = 0; i < element.Length; i++)
            {

                if (string.IsNullOrEmpty(element[i].ToString()) == false)
                {
                    bool IsAllRunCountyMatch = false;
                    var County_State = element[i].ToString().Split(new Char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    string County = County_State[0].ToString();
                    string State = County_State[1].ToString();

                    using (EmergeDALDataContext dx = new EmergeDALDataContext())
                    {
                        var objTblCount = (from tblCountyObj in dx.tblCounties
                                           join tblStateObj in dx.tblStates
                                           on
                                           tblCountyObj.fkStateId equals tblStateObj.pkStateId
                                           where tblStateObj.StateCode.ToUpper().Trim() == State.ToUpper().Trim()
                                           select tblCountyObj).ToList();

                        bool HasCount = objTblCount.Select(sd => sd.CountyName.ToUpper().Trim()).Contains(County.ToUpper().Trim());

                        if (!HasCount)
                        {
                            objLIstItem = objTblCount.Select(x => new SelectListItem { Text = x.CountyName, Value = x.pkCountyId.ToString() }).ToList();
                            StateCode = State;
                            IsAllRunCountyMatch = true;
                        }
                    }

                    string CountyName = County.ToLower().Replace("st.", "saint");
                    var Countyfee = Objbalcompanytype.Get_County_Fee(CountyName, State, LocationId, "CCR1");
                    string fee = "$" + ReportFee.ToString(); ;
                    if (Countyfee == 0 || Countyfee == Convert.ToDecimal(0.00))
                    {
                    }
                    else
                    {
                        fee = fee + " <br /> County Fee" + " $" + Countyfee;
                    }

                    decimal subTotal = ReportFee + Countyfee;

                    response.Append("<tr><td>" + fullName + "</td>");
                    if (IsAllRunCountyMatch)
                    {
                        response.Append("<td><select class='select_State' id='selectStateId_" + index + "' disabled style='width:80px;'><option selected='selected'>" + StateCode + "</option></select></td>");

                        response.Append("<td><select class='select_County' id='selectedCountyId_" + index + "' style='width:175px;' onchange='countyOnChanges(this);' ><option selected='selected'>--Select County--</option>");
                        foreach (var countyNameIndex in objLIstItem)
                        {
                            response.Append("<option>" + countyNameIndex.Text + "</option>");
                        }
                        response.Append("</select> &nbsp; <span style='color:red;'>" + County + "</span> </td>");

                    }
                    else
                    {
                        response.Append("<td>" + State + "</td>");
                        response.Append("<td>" + CountyName + "</td>");
                    }

                    string MI = MDI != "" ? MDI : "*";
                    response.Append("<td>CCR1</td>");
                    response.Append("<td>" + fee + "</td>");
                    response.Append("<td><input onclick=\"ChkSingleSelect(this.id);\" class=\"chkunchk\" type=\"checkbox\" id=\"chk_" + CountyName + "_" + State + "_County_" + FirstName + "_" + MI + "_" + LastName + "_name_" + i.ToString() + "\"  /> &nbsp;&nbsp;<span style=\"color:red;\">Add</span></td>");
                    response.Append("<td><span class=\"subtotalClass\" id=\"subTotal_" + i.ToString() + "\">$" + subTotal + "</span></td>");
                    response.Append("</tr>");

                    index++;

                }

            }
            int j = int.Parse(element.Length.ToString()) - 1;
            response.Append("</table>").Replace("RecordCount", Convert.ToString(j));
            return Json(new { Msg = response.ToString() });
        }


        private decimal Get_Report_Fee(int LocationId, int CompanyUserId, string ReportCode, string ReportType, int packageId)
        {
            decimal fee = 0;
            try
            {
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    #region----Old code

                    //var Obj = (from t1 in DX.tblProductPerApplications
                    //           join t2 in DX.tblProductAccesses on t1.pkProductApplicationId equals t2.fkProductApplicationId
                    //           join t3 in DX.tblCompany_Prices on t2.fkProductApplicationId equals t3.fkProductApplicationId
                    //           where t2.fkLocationId == LocationId && t1.ProductCode == ReportCode
                    //           select new { t1, t2, t3 }).First();

                    //if (Obj != null)
                    //{

                    //    if (Obj.t2.pkProductAccessId != null) { fee = Obj.t2.LocationProductPrice; }
                    //    else { if (Obj.t3.CompanyPrice != null) { fee = (decimal)Obj.t3.CompanyPrice; } else { fee = (decimal)Obj.t1.ProductPerApplicationPrice; } }


                    //}
                    #endregion


                   var Obj1 = DX.Proc_GetReportFee(LocationId, ReportCode).FirstOrDefault();
                    if (Obj1 != null)
                    {
                        if (Obj1.pkProductAccessId > 0) { fee = Obj1.LocationProductPrice; }
                        else { if (Obj1.CompanyPrice != null) { fee = (decimal)Obj1.CompanyPrice; } else { fee = (decimal)Obj1.ProductPerApplicationPrice; } }
                    }
                }
            }
            catch
            {
                //Do nothing.
            }
            return fee;

        }

        #region codded by chetu-Validate countystate for ticket #22

        [HttpPost]
        public ActionResult ValidatCountyState(string CountyState)
        {
            bool IsAllRunCountyMatch = false;
            List<SelectListItem> objLIstItem = new List<SelectListItem>();
            string StateCode = string.Empty;
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                var element = CountyState.ToString().Split(new[] { "countystate" }, StringSplitOptions.None);
                for (int i = 0; i < element.Length; i++)
                {
                    if (string.IsNullOrEmpty(element[i].ToString()) == false)
                    {
                        var County_State = element[i].ToString().Split(new Char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        string County = County_State[0].ToString();
                        string State = County_State[1].ToString();

                        //string County ="Newport News City";
                        //string State = "VA";

                        var objTblCount = (from tblCountyObj in dx.tblCounties
                                           join tblStateObj in dx.tblStates
                                           on
                                           tblCountyObj.fkStateId equals tblStateObj.pkStateId
                                           where tblStateObj.StateCode.ToUpper().Trim() == State.ToUpper().Trim()
                                           select tblCountyObj).ToList();

                        bool HasCount = objTblCount.Select(sd => sd.CountyName.ToUpper().Trim()).Contains(County.ToUpper().Trim());

                        if (!HasCount)
                        {
                            objLIstItem = objTblCount.Select(x => new SelectListItem { Text = x.CountyName, Value = x.pkCountyId.ToString() }).ToList();
                            StateCode = State;
                            IsAllRunCountyMatch = false;
                            break;
                        }
                        else
                        {
                            IsAllRunCountyMatch = true;
                        }
                    }
                }
            }

            return Json(new { IsUnMatch = IsAllRunCountyMatch, StateCode = StateCode, objLIstItem = objLIstItem });
        }


        #endregion


    }

}
