﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Emerge.Services;

namespace Emerge.Controllers.Common
{
    public class CommonAPIController : ApiController
    {
        public string GetAllReports(string id)
        {
            string ReportName = string.Empty;
            try
            {
                string[] arr = id.Split('_');
                BALGeneral ObjGeneral = new BALGeneral();
                ReportName = ObjGeneral.GetEmergeProductPackageReportsNew(Convert.ToInt32(arr[0]), Convert.ToInt32(arr[1]));
            }
            catch (Exception)
            {
            }
            return ReportName;
        }
    }
}
