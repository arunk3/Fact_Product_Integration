﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Emerge.Common;
using System.Web.Security;
using Emerge.Models;
using System.Configuration;
using Emerge.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Web.UI;
using System.Net.Mail;
using iTextSharp.text;
using iTextSharp.text.pdf;
//using RazorEngine;

namespace Emerge.Controllers.Common
{
    public class CommonController : Controller
    {
        BALManageFolder objBALManageFolder = new BALManageFolder();
        BALBillingData ObjBALBillingData = new BALBillingData();
        Guid SiteApplicationId = Emerge.Common.Utility.SiteApplicationId;// new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852");

        public PartialViewResult PartialDropDownsData(string PType)
        {
            //int CompanyId = 0;
            //int LocationId = 0;
            string RoleName = "SystemAdmin";
            ViewData["listMonth"] = FillMonth();
            ViewData["listYear"] = FillYear();

            #region Company List
            List<SelectListItem> listCompany = new List<SelectListItem>();
            var companies = ObjBALBillingData.GetAllCompany(SiteApplicationId, true, false);
            if (companies.Count() > 0)
            {
                //listCompany = companies.Select(x => new SelectListItem { Text = x.CompanyName, Value = x.pkCompanyId.ToString() }).ToList();
                //CompanyId = companies.ElementAt(0).pkCompanyId;
                ViewData["CompanyId"] = companies.ElementAt(0).pkCompanyId;
            }
            ViewData["PType"] = "";
            if (!string.IsNullOrEmpty(PType))
            {
                ViewData["PType"] = PType;
                listCompany.Insert(0, new SelectListItem { Text = "---All Companies---", Value = "" });
            }
            if (Session["fkcmpId1"] != null)
            {
                ViewData["CompanyId"] = Session["fkcmpId1"].ToString();
                Session["pkCompanyId"] = Session["fkcmpId1"].ToString();
            }
            #endregion

            #region Locations List
            //List<SelectListItem> listLocations = new List<SelectListItem>();
            //var Locations = ObjBALBillingData.GetLocationsByCompanyId(CompanyId, SiteApplicationId);
            //if (Locations.Count() > 0)
            //{
            //    listLocations = Locations.Select(x => new SelectListItem { Text = x.DisplayLocation, Value = x.LocationId.ToString() }).ToList();
            //    LocationId = Locations.ElementAt(0).LocationId;
            //}
            //listLocations.Insert(0, new SelectListItem { Text = "---All Locations---", Value = "" });
            #endregion

            #region Users List
            //List<SelectListItem> listUsers = new List<SelectListItem>();
            //var Users = ObjBALBillingData.GetCompanyUsersByLocationId(CompanyId, LocationId, SiteApplicationId);
            //if (Users.Count() > 0)
            //{
            //    listUsers = Users.Select(x => new SelectListItem { Text = x.UserEmail, Value = x.CompanyUserId.ToString() }).ToList();
            //}
            //listUsers.Insert(0, new SelectListItem { Text = "---All Users---", Value = "" });
            #endregion

            #region ReferenceCode List
            //List<SelectListItem> listReferenceCode = new List<SelectListItem>();
            //var ReferenceCode = ObjBALBillingData.GetAllReferenceCodes(CompanyId);
            //if (ReferenceCode.Count() > 0)
            //{
            //    listReferenceCode = ReferenceCode.Select(x => new SelectListItem { Text = x.ReferenceCode, Value = x.pkReferenceCodeId.ToString() }).ToList();
            //}
            //listReferenceCode.Insert(0, new SelectListItem { Text = "---All Reference---", Value = "" });
            #endregion

            #region salesRep List
            RoleName = "salesmanager";//salesrep
            List<SelectListItem> listsalesRep = new List<SelectListItem>();
            var salesRep = ObjBALBillingData.GetSalesRep(RoleName);
            if (salesRep.Count() > 0)
            {
                listsalesRep = salesRep.Select(x => new SelectListItem { Text = x.FullName, Value = x.pkSalesRepId.ToString() }).ToList();
            }
            listsalesRep.Insert(0, new SelectListItem { Text = "---Select Sales Rep---", Value = "" });
            #endregion
            List<SelectListItem> listUser = new List<SelectListItem>();

            //listsalesRep = salesRep.Select(x => new SelectListItem { Text = x.FullName, Value = x.pkSalesRepId.ToString() }).ToList();
            listUser.Insert(0, new SelectListItem { Text = "---All Users---", Value = "-1" });

            ViewBag.stateList = listUser;
            //ViewData["listCompany"] = listCompany;
            //ViewData["listLocation"] = listLocations;
            //ViewData["listUsers"] = listUsers;
            ViewData["listSalesRep"] = listsalesRep;
            //ViewData["listReferenceNo"] = listReferenceCode;
            return PartialView("../Common/PartialDropDownsData");
        }

        public PartialViewResult PartialVendorDeopdownsData()
        {
            int VendorId = 0;
            ViewData["listMonth"] = FillMonth();
            ViewData["listYear"] = FillYear();

            #region Vendors List
            List<SelectListItem> listVendors = new List<SelectListItem>();
            var Vendors = ObjBALBillingData.GetVendorDetails();
            if (Vendors.Count() > 0)
            {
                listVendors = Vendors.Select(x => new SelectListItem { Text = x.VendorName, Value = x.pkVendorId.ToString() }).ToList();
            }
            listVendors.Insert(0, new SelectListItem { Text = "---All Vendors---", Value = "" });
            #endregion

            #region Report Type List
            List<SelectListItem> listReportType = new List<SelectListItem>();
            var ReportTypes = ObjBALBillingData.GetReportsByVendorId(VendorId);
            if (ReportTypes.Count() > 0)
            {
                listReportType = ReportTypes.Select(x => new SelectListItem { Text = x.ReportName, Value = x.pkReportId.ToString() }).ToList();
            }
            listReportType.Insert(0, new SelectListItem { Text = "---Reports---", Value = "" });
            #endregion


            ViewData["listVendors"] = listVendors;
            ViewData["listReportType"] = listReportType;

            return PartialView("../Common/PartialDropDownsForVendorReport");
        }
        public PartialViewResult PartialSalesDropDownsData()
        {
            int CompanyId = 0;
            //int LocationId = 0;
            //string RoleName = "SystemAdmin";
            ViewData["listMonth"] = FillMonth();
            ViewData["listYear"] = FillYear();
            ViewData["listAccountStatus"] = GetAccountStatusList();

            #region Company List
           // List<SelectListItem> listCompany = new List<SelectListItem>();
            var companies = ObjBALBillingData.GetAllCompany(SiteApplicationId, true, false);
            if (companies.Count() > 0)
            {
                //listCompany = companies.Select(x => new SelectListItem { Text = x.CompanyName, Value = x.pkCompanyId.ToString() }).ToList();
                //CompanyId = companies.ElementAt(0).pkCompanyId;
                ViewData["CompanyId"] = companies.ElementAt(0).pkCompanyId;
            }
            //listCompany.Insert(0, new SelectListItem { Text = "---All Companies---", Value = "" });
            #endregion

            #region Locations List
            //List<SelectListItem> listLocations = new List<SelectListItem>();
            //var Locations = ObjBALBillingData.GetLocationsByCompanyId(CompanyId, SiteApplicationId);
            //if (Locations.Count() > 0)
            //{
            //    listLocations = Locations.Select(x => new SelectListItem { Text = x.DisplayLocation, Value = x.LocationId.ToString() }).ToList();
            //    LocationId = Locations.ElementAt(0).LocationId;
            //}
            //listLocations.Insert(0, new SelectListItem { Text = "---All Locations---", Value = "" });
            #endregion

            #region Users List
            //List<SelectListItem> listUsers = new List<SelectListItem>();
            //var Users = ObjBALBillingData.GetCompanyUsersByLocationId(CompanyId, LocationId, SiteApplicationId);
            //if (Users.Count() > 0)
            //{
            //    listUsers = Users.Select(x => new SelectListItem { Text = x.UserEmail, Value = x.CompanyUserId.ToString() }).ToList();
            //}
            //listUsers.Insert(0, new SelectListItem { Text = "---All Users---", Value = "" });
            #endregion

            #region ReferenceCode List
            List<SelectListItem> listReferenceCode = new List<SelectListItem>();
            var ReferenceCode = ObjBALBillingData.GetAllReferenceCodes(CompanyId);
            if (ReferenceCode.Count() > 0)
            {
                listReferenceCode = ReferenceCode.Select(x => new SelectListItem { Text = x.ReferenceCode, Value = x.pkReferenceCodeId.ToString() }).ToList();
            }
            listReferenceCode.Insert(0, new SelectListItem { Text = "---All Reference---", Value = "" });
            #endregion

            #region salesRep List
            List<SelectListItem> listAccManager = new List<SelectListItem>();
            BALSalesRep ObjBALSalesRep = new BALSalesRep();
            string Role_Name = "salesmanager";//salesrep
            var accManager = ObjBALSalesRep.GetSalesManagers(Role_Name);
            if (accManager.Count() > 0)
            {
                listAccManager = accManager.Select(x => new SelectListItem { Text = x.FullName, Value = x.fkUserId.ToString() }).ToList();
            }
            listAccManager.Insert(0, new SelectListItem { Text = "---Select Account Manager---", Value = "" });
            #endregion


            //ViewData["listCompany"] = listCompany;
            //ViewData["listLocation"] = listLocations;
            //ViewData["listUsers"] = listUsers;
            ViewData["listAccManager"] = listAccManager;
            ViewData["listReferenceNo"] = listReferenceCode;
            return PartialView("../Common/PartialSalesDropDownsData");
        }

        private List<SelectListItem> FillMonth()
        {
            List<SelectListItem> listMonth = new List<SelectListItem>();
            listMonth.Add(new SelectListItem { Text = "January", Value = "1" });
            listMonth.Add(new SelectListItem { Text = "February", Value = "2" });
            listMonth.Add(new SelectListItem { Text = "March", Value = "3" });
            listMonth.Add(new SelectListItem { Text = "April", Value = "4" });
            listMonth.Add(new SelectListItem { Text = "May", Value = "5" });
            listMonth.Add(new SelectListItem { Text = "June", Value = "6" });
            listMonth.Add(new SelectListItem { Text = "July", Value = "7" });
            listMonth.Add(new SelectListItem { Text = "August", Value = "8" });
            listMonth.Add(new SelectListItem { Text = "September", Value = "9" });
            listMonth.Add(new SelectListItem { Text = "October", Value = "10" });
            listMonth.Add(new SelectListItem { Text = "November", Value = "11" });
            listMonth.Add(new SelectListItem { Text = "December", Value = "12" });
            DateTime BillingDate = DateTime.Now;
            if (Session["BillingDate"] != null)
            {
                BillingDate = Convert.ToDateTime(Session["BillingDate"]);
            }

            listMonth = listMonth.Select(x => new SelectListItem { Text = x.Text, Value = x.Value, Selected = (x.Value == BillingDate.Month.ToString()) }).ToList();
            return listMonth;
        }

        private List<SelectListItem> FillYear()
        {
            List<SelectListItem> listYear = new List<SelectListItem>();
            for (int i = 2008; i <= DateTime.Now.Year; i++)
            {
                listYear.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }
            DateTime BillingDate = DateTime.Now;
            if (Session["BillingDate"] != null)
            {
                BillingDate = Convert.ToDateTime(Session["BillingDate"]);
            }
            listYear = listYear.Select(x => new SelectListItem { Text = x.Text, Value = x.Value, Selected = (x.Value == BillingDate.Year.ToString()) }).ToList();
            return listYear;
        }

        public ActionResult CheckDuplicateCompanyName(string NewCompanyName)
        {
            //string saleManger = string.Empty;
            string CompanyName = "DuplicateName";
            BALCompany ObjBALCompany = new BALCompany();
            try
            {

                var list = ObjBALCompany.CheckDuplicasebyComapnyName(NewCompanyName);
                if (list != null)
                {
                    CompanyName = list.CompanyName;

                }

            }
            catch //(Exception ex)
            {

            }
            finally
            {
                ObjBALCompany = null;
            }
            return Json(new { CompanyName = CompanyName });
        }

        /// <summary>
        /// Check company name during the add new lead if company name exists then get sales manager name
        /// ticket #154
        /// </summary>
        /// <param name="NewCompanyName"></param>
        /// <returns></returns>
        public ActionResult CheckCompanyNameDuringAddNewLead(string NewCompanyName)
        {
            string saleManger = string.Empty;
            string CompanyName = "DuplicateName";
            BALCompany ObjBALCompany = new BALCompany();
            try
            {
                //commented for ticket #154
                //var list = ObjBALCompany.CheckDuplicasebyComapnyName(NewCompanyName);
                saleManger = ObjBALCompany.CheckDuplicateComapnyName(NewCompanyName);
                if (!string.IsNullOrEmpty(saleManger))
                {
                    CompanyName = saleManger;
                }

                //commented for ticket #154
                //if (list != null)
                //{
                //    CompanyName = list.CompanyName;

                //}

            }
            catch //(Exception ex)
            {

            }
            finally
            {
                ObjBALCompany = null;
            }
            return Json(new { CompanyName = CompanyName });
        }

        /// <summary>
        /// /// Check Email during the add new lead if email exists then get sales manager name
        /// ticket #154
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        public ActionResult CheckEmailDuringAddNewLead(string Email, string CompanyName)
        {
            string saleManger = string.Empty;
            string cType = string.Empty;
            int count = 0;
            BALCompany ObjBALCompany = new BALCompany();
            try
            {
                if (CompanyName != string.Empty)
                {
                    var list = ObjBALCompany.CheckDuplicateEmail(Email, CompanyName);
                    count = list.Count();
                    if (list.Count() > 0)
                    {
                        saleManger = list.ElementAt(0).SalesManager;
                        cType = list.ElementAt(0).CType;//User for flag: Same Company and Other Company.
                    }
                }
            }
            catch //(Exception ex)
            {

            }
            finally
            {
                ObjBALCompany = null;
            }
            return Json(new { saleManger = saleManger, cType = cType, count = count });
        }



        public ActionResult GetCompanies(string PType)
        {
            MembershipUser Member = Membership.GetUser();//User.Identity.Name
            List<SelectListItem> listCompany = new List<SelectListItem>();



            if (Member != null)
            {
                string[] Role = Roles.GetRolesForUser(Member.Email);
                if (Role.Contains("SystemAdmin") || Role.Contains("SupportManager") || Role.Contains("SupportTechnician"))
                {
                    var companies = ObjBALBillingData.GetAllCompany(SiteApplicationId, true, false);
                    if (companies.Count() > 0)
                    {
                        listCompany = companies.Select(x => new SelectListItem { Text = x.CompanyName, Value = x.pkCompanyId.ToString() }).ToList();
                    }
                    if (!string.IsNullOrEmpty(PType))
                    {
                        listCompany.Insert(0, new SelectListItem { Text = "---All Companies---", Value = "" });
                    }
                }
                else
                {

                    //MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
                    Guid UserId = Utility.GetUID(User.Identity.Name);
                    BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
                    List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjData = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(UserId);
                    var companies = GetCompanyInfoByCompanyId(SiteApplicationId, true, false, ObjData.First().pkCompanyId);

                    listCompany = companies.Select(x => new SelectListItem { Text = x.CompanyName, Value = x.pkCompanyId.ToString() }).ToList();


                    //if (Session["pkCompanyId"] != null)
                    //{
                    //    Guid pkCompanyId = Guid.Parse(Session["pkCompanyId"].ToString());
                    //    var CompanyName = Session["CurrentUserCompanyName"].ToString();
                    //    listCompany.Insert(0, new SelectListItem { Text = CompanyName, Value = pkCompanyId.ToString() });
                    //}
                    //else
                    //{
                    //    MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
                    //    Guid UserId = Utility.GetUID(User.Identity.Name);// new Guid("fdd976e2-222f-47c9-b181-68e5e15ea358");
                    //    BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
                    //    List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjData = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(UserId);
                    //     if (ObjData.Count > 0)
                    //     {
                    //         var CompanyName = ObjData.First().CompanyName.ToLower() + "  [" + ObjData.First().City + ", " + ObjData.First().StateCode + "]";
                    //         listCompany.Insert(0, new SelectListItem { Text = CompanyName, Value = ObjData.First().pkCompanyId.ToString() });
                    //     }

                    //}




                }
            }
            return Json(listCompany);
        }

        public List<CompanyList> GetCompanyInfoByCompanyId(Guid ApplicationId, bool Enabled, bool Deleted, int pkCompanyId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                IQueryable<CompanyList> ObjData = from c in DX.tblCompanies
                                                  join l in DX.tblLocations on c.pkCompanyId equals l.fkCompanyId
                                                  join s in DX.tblStates on l.fkStateID equals s.pkStateId

                                                  where c.fkApplicationId == ApplicationId && c.IsDeleted == Deleted && l.IsHome == true && c.pkCompanyId == pkCompanyId
                                                  orderby c.CompanyName ascending
                                                  select new CompanyList
                                                  {
                                                      CompanyName = c.CompanyName + "  [" + l.City + ", " + s.StateCode + "]",
                                                      pkCompanyId = c.pkCompanyId
                                                  };
                return ObjData.ToList();
            }
        }


        public ActionResult FillCompanies()
        {
            Guid GlobalMemberUserId = Utility.GetUID(User.Identity.Name);
            List<SelectListItem> listCompany = new List<SelectListItem>();
            if (Roles.IsUserInRole(User.Identity.Name, "SalesManager"))
            {
                var companies = ObjBALBillingData.GetAllCompanyBySalesMgr(Utility.SiteApplicationId, true, false, GlobalMemberUserId);
                if (companies.Count() > 0)
                {
                    listCompany = companies.Select(x => new SelectListItem { Text = x.CompanyName, Value = x.pkCompanyId.ToString() }).ToList();
                }
            }
            else
            {
                var companies = ObjBALBillingData.GetAllCompany(SiteApplicationId, true, false);
                if (companies.Count() > 0)
                {
                    listCompany = companies.Select(x => new SelectListItem { Text = x.CompanyName, Value = x.pkCompanyId.ToString() }).ToList();
                }
            }
            listCompany.Insert(0, new SelectListItem { Text = "--All Companies--", Value = "--All Companies--" });

            return Json(listCompany);
        }


        [HttpPost]
        public JsonResult GetReferenceNumbers(int CompanyId)
        {
            int CmpId = CompanyId != 0 ? CompanyId : 0;
            List<SelectListItem> listReferenceCode = new List<SelectListItem>();
            var ReferenceCode = ObjBALBillingData.GetAllReferenceCodes(CmpId);
            if (ReferenceCode.Count() > 0)
            {
                listReferenceCode = ReferenceCode.Select(x => new SelectListItem { Text = x.ReferenceCode, Value = x.pkReferenceCodeId.ToString() }).ToList();
            }
            listReferenceCode.Insert(0, new SelectListItem { Text = "---All Reference---", Value = "" });

            return Json(new { Data = listReferenceCode });
        }

        public ActionResult GetReferences(int CompanyId)
        {
            int CmpId = CompanyId != 0 ? CompanyId : 0;
            List<SelectListItem> listReferenceCode = new List<SelectListItem>();
            var ReferenceCode = ObjBALBillingData.GetAllReferenceCodes(CmpId);
            if (ReferenceCode.Count() > 0)
            {
                listReferenceCode = ReferenceCode.Select(x => new SelectListItem { Text = x.ReferenceCode, Value = x.pkReferenceCodeId.ToString() }).ToList();
            }
            listReferenceCode.Insert(0, new SelectListItem { Text = "---All Reference---", Value = "" });
            string a = Request.UrlReferrer.ToString();
            if (a.ToLower().Contains("billingstatements"))
            {
                //listReferenceCode.Insert(1, new SelectListItem { Text = "All Reference Codes", Value = "1" });//ND-29

                listReferenceCode.Insert(1, new SelectListItem { Text = "All Reference Codes", Value = "-1" });//ND-29
                // change on 10 aug 2015

            }
            return Json(listReferenceCode);
        }


        //INT-371
        [HttpPost]
        #region(SELECT MANAGE FOLDER NAME LIST FOR INT-371)
        public ActionResult GetManageFolders()
        {
            int CompanyId = Convert.ToInt16(Session["pkCompanyId"]);
            int CmpId = CompanyId != 0 ? CompanyId : 0;
            List<SelectListItem> listManageFolder = new List<SelectListItem>();
            var ManageFolder = objBALManageFolder.GetAllManageFolders(CmpId);
            if (ManageFolder.Count() > 0)
            {
                listManageFolder = ManageFolder.Select(x => new SelectListItem { Text = x.FolderName, Value = x.pkManageFolderId.ToString() }).ToList();
            }
            //listManageFolder.Insert(0, new SelectListItem { Text = "---Select Folder---", Value = "" });
            return Json(listManageFolder);
        }
        #endregion



        [HttpPost]
        public JsonResult GetLocations(int CompanyId)
        {
            int CmpId = CompanyId != 0 ? CompanyId : 0;
            List<SelectListItem> listLocations = new List<SelectListItem>();
            var Locations = ObjBALBillingData.GetLocationsByCompanyId(CmpId, SiteApplicationId);
            if (Locations.Count() > 0)
            {
                listLocations = Locations.Select(x => new SelectListItem { Text = x.DisplayLocation, Value = x.LocationId.ToString() }).ToList();
            }
            listLocations.Insert(0, new SelectListItem { Text = "---All Locations---", Value = "" });

            return Json(new { Data = listLocations });
        }

        public ActionResult GetLocationsList(string CompanyId)
        {
            int CmpId = 0;
            if (!string.IsNullOrEmpty(CompanyId))
            {
                CmpId = Convert.ToInt32(CompanyId);
            }
            List<SelectListItem> listLocations = new List<SelectListItem>();
            if (CmpId != 0)
            {
                var Locations = ObjBALBillingData.GetLocationsByCompanyId(CmpId, SiteApplicationId);
                if (Locations.Count() > 0)
                {
                    listLocations = Locations.Select(x => new SelectListItem { Text = x.DisplayLocation, Value = x.LocationId.ToString() }).ToList();
                }
            }
            listLocations.Insert(0, new SelectListItem { Text = "---All Locations---", Value = "" });
            string a = Request.UrlReferrer.ToString();
            if (a.ToLower().Contains("billingstatements"))
            {
                listLocations.Insert(1, new SelectListItem { Text = "All Locations", Value = "-1" }); ;//INT=230
            }
            return Json(listLocations);

        }

        [HttpPost]
        public JsonResult GetCompanyUsers(int CompanyId, int LocationId)
        {
            int CmpId = CompanyId != 0 ? CompanyId : 0;
            int LcId = LocationId != 0 ? LocationId : 0;
            List<SelectListItem> listUsers = new List<SelectListItem>();
            var Users = ObjBALBillingData.GetCompanyUsersByLocationId(CmpId, LcId, SiteApplicationId);
            if (Users.Count() > 0)
            {
                listUsers = Users.Select(x => new SelectListItem { Text = x.UserEmail, Value = x.CompanyUserId.ToString() }).ToList();
            }
            listUsers.Insert(0, new SelectListItem { Text = "---All Users---", Value = "" });

            return Json(new { Data = listUsers });
        }

        public ActionResult GetUsers(string CompanyId, string LocationId)
        {
            int CmpId = 0;
            if (!string.IsNullOrEmpty(CompanyId))
            {
                CmpId = Convert.ToInt32(CompanyId);
            }

            int LcId = 0;
            if (!string.IsNullOrEmpty(LocationId))
            {
                LcId = Convert.ToInt32(LocationId);
            }
            List<SelectListItem> listUsers = new List<SelectListItem>();
            if (CmpId != 0)
            {
                var Users = ObjBALBillingData.GetCompanyUsersByLocationId(CmpId, LcId, SiteApplicationId);
                if (Users.Count() > 0)
                {
                    listUsers = Users.Select(x => new SelectListItem { Text = x.UserEmail, Value = x.CompanyUserId.ToString() }).ToList();
                }
            }
            listUsers.Insert(0, new SelectListItem { Text = "---All Users---", Value = "" });

            string a = Request.UrlReferrer.ToString();
            if (a.ToLower().Contains("billingstatements"))
            {
                listUsers.Insert(1, new SelectListItem { Text = "All Users", Value = "-1" });//ND-29
                // change on 30 may 2016.
                // int-230 add user in billing statement 

            }
            return Json(listUsers);
        }

        [HttpPost]
        public JsonResult GetReportTypes(string VId)
        {
            int VendorId = !string.IsNullOrEmpty(VId) ? int.Parse(VId) : 0;
            List<SelectListItem> listReportType = new List<SelectListItem>();
            var ReportTypes = ObjBALBillingData.GetReportsByVendorId(VendorId);
            if (ReportTypes.Count() > 0)
            {
                listReportType = ReportTypes.Select(x => new SelectListItem { Text = x.ReportName, Value = x.pkReportId.ToString() }).ToList();
            }
            listReportType.Insert(0, new SelectListItem { Text = "---Reports---", Value = "" });

            return Json(new { Data = listReportType });
        }

        private List<SelectListItem> GetAccountStatusList()
        {
            List<SelectListItem> listAccountStatus = new List<SelectListItem>();
            listAccountStatus.Add(new SelectListItem { Text = "--All--", Value = "-1" });
            listAccountStatus.Add(new SelectListItem { Text = "Active", Value = "Active" });
            listAccountStatus.Add(new SelectListItem { Text = "InActive", Value = "InActive" });
            listAccountStatus.Add(new SelectListItem { Text = "Locked", Value = "Locked" });
            listAccountStatus.Add(new SelectListItem { Text = "PastDue", Value = "PastDue" });
            listAccountStatus.Add(new SelectListItem { Text = "InCollections", Value = "InCollections" });
            return listAccountStatus;
        }

        public PartialViewResult PartialReport(string Month, string Year, string ReferenceNo, int? CompanyUserId, int? LocationId, string SalesRep, int? CompanyId, string IsAdmin)
        {
            ViewData["appId"] = "65d1e085-eb17-4f29-96d0-247b0c1ee852";
            ViewData["Month"] = Month;
            ViewData["Year"] = Year;
            //ViewData["CompanyId"] = "7dc87d77-6bb5-4bb3-9837-fa861f36f281";
            ViewData["CompanyId"] = CompanyId != 0 ? CompanyId : 0;
            ViewData["UserId"] = CompanyUserId != 0 ? CompanyUserId : 0;
            ViewData["LocationId"] = LocationId != 0 ? LocationId : 0;
            ViewData["SalesRep"] = !string.IsNullOrEmpty(SalesRep) ? SalesRep : Guid.Empty.ToString();
            ViewData["RefCode"] = !string.IsNullOrEmpty(ReferenceNo) ? ReferenceNo : "0";
            ViewData["IsAdmin"] = !string.IsNullOrEmpty(IsAdmin) ? IsAdmin : "false";

            return PartialView("PartialReport");
        }

        #region Forget Password
        /// <summary>
        /// update expired Password
        /// </summary>
        [HttpPost]
        public ActionResult UpdateExpiredPassword(LoginModel Model)
        {
            string Message = string.Empty;
            string Css = string.Empty;
            string username = (string)Session["UserNamePasswordExpired"];
            //string password = string.Empty;
            MembershipUser objMembershipUser = Membership.GetUser(username);

            if (objMembershipUser != null)
            {
                if (!string.IsNullOrEmpty(Model.NewPassword) && !string.IsNullOrEmpty(Model.ConfirmNewPassword))
                {
                    Membership.GetUser(objMembershipUser.UserName).ChangePassword(objMembershipUser.GetPassword(), Model.NewPassword);
                    Message = "Password updated. Please use new password to login.";
                    Css = "successmsg";
                }
            }
            else
            {
                Message = "Invalid User";
                Css = "errormsgSMALL";
            }
            return Json(new { Msg = Message, css = Css });
        }

        [HttpPost]
        public ActionResult ForgetPassword(LoginModel Model)
        {
            string Message = string.Empty;
            string Css = string.Empty;
            string username = "";
            string password = "";
            MembershipUser objMembershipUser = Membership.GetUser(Model.Email.Trim());
            if (objMembershipUser != null)
            {
                password = objMembershipUser.GetPassword();
                username = objMembershipUser.UserName;
                if (ForgetPassword_Email(username, password))
                {
                    Message = "Your login details have been sent to your email address.";
                    Css = "successmsg";
                }
                else
                {
                    Message = "Some error ocurred while sending an email. Please try again later.";
                    Css = "errormsgSMALL";
                }
            }
            else
            {
                Message = "Invalid User Name";
                Css = "errormsgSMALL";
            }
            return Json(new { Msg = Message, css = Css });
        }

        public bool ForgetPassword_Email(string user_Name, string password)
        {
            bool mailsent = false;
            BALGeneral objBALGeneral = new BALGeneral();
            try
            {
                string emailText = "";
                string MessageBody = "";

                BALGeneral ObjBALGeneral = new BALGeneral();
                string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;

                var emailContent = objBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, Convert.ToInt32(EmailTemplates.PasswordRecovery));//"Password Recovery"
                emailText = emailContent.TemplateContent;
                BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                #region For New Style SystemTemplate
                string Bookmarkfile = string.Empty;
                string bookmarkfilepath = string.Empty;

                Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                //string logopth = ApplicationPath.GetApplicationPath() + "Content/themes/base/images/emerge_login.png";
                //proc_Get_UserAllInfoResult userinfo = objTemplateBookmarks.GetUserInfo(Utility.GetUID(User.Identity.Name));
                Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                emailText = Bookmarkfile;
                #endregion



                #region New way Bookmark


                Bookmarks objBookmark = new Bookmarks();

                objBookmark.EmailContent = emailText;

                objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                objBookmark.CompanyName = ObjBALGeneral.GetSettings(1).CompanyName;
                objBookmark.UserEmail = user_Name;
                objBookmark.UserPassword = password;
                objBookmark.LoginLink = "<a style='color: #87ceea;' href='" + ApplicationPath.GetApplicationPath() + "'>Login Now !</a>";

                MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Utility.GetUID(HttpContext.User.Identity.Name.ToString()));

                #endregion

                //MessageBody = emailText.
                //            Replace("%WebsiteLogo%", "<img src='" + WebsiteLogo + "' />").
                //            Replace("%%CompanyName%%", ObjBALGeneral.GetSettings(1).CompanyName).
                //            Replace("%UserName%", user_Name).
                //            Replace("%Password%", password).
                //           Replace("%%Link%%", "<a href=" + ApplicationPath.GetApplicationPath() + "Default.aspx" + ">Login Now !</a>"); 

                return Utility.SendMail(user_Name, string.Empty, string.Empty, BALGeneral.GetGeneralSettings().SupportEmailId, MessageBody, "Forget User Name and Password");
            }
            catch //(Exception ex)
            {
                return mailsent;
                //throw ex;
            }
            finally
            {
                objBALGeneral = null;
            }

        }
        #endregion

        #region Get Email Template to send Emerge Edit Email
        [HttpPost]
        public JsonResult GetEmergeEditEmailTemplate()
        {
            BALGeneral ObjBALGeneral = new BALGeneral();
            byte settingId = 1;
            var settings = ObjBALGeneral.GetSettings(settingId);
            var emailContent = ObjBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, Convert.ToInt32(EmailTemplates.EmergeEdit));// "EmergeEdit"
            string Company = (Session["CurrentUserCompanyName"] == null) ? string.Empty : Session["CurrentUserCompanyName"].ToString();
            string FromAddress = (Session["CurrentUserMailId"] == null) ? string.Empty : Session["CurrentUserMailId"].ToString();
            string Mailto = settings.SupportEmailId;

            #region For New Style SystemTemplate
            string Bookmarkfile = string.Empty;
            string bookmarkfilepath = string.Empty;
            BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
            Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
            Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
            Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
            emailContent.TemplateContent = Bookmarkfile;
            #endregion

            string EmailContent = emailContent.TemplateContent;

            return Json(new { From = FromAddress, To = Mailto, EContent = EmailContent, Cmp = Company });
        }
        #endregion

        #region Send Emerge Edit Email
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult SendEmail(string Company, string MailFrom, string Subject, string EContent, string Suggestion)
        {

            string emailText = "";
            string MessageBody = "";
            string CssClass = "";
            string Message = "";

            BALGeneral ObjBALGeneral = new BALGeneral();
            var settings = ObjBALGeneral.GetSettings(1);
            string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + settings.WebSiteLogo;

            emailText = string.IsNullOrEmpty(EContent) ? string.Empty : EContent;

            #region New way Bookmark

            BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
            Bookmarks objBookmark = new Bookmarks();

            objBookmark.EmailContent = emailText;

            objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
            objBookmark.UserCompany = string.IsNullOrEmpty(Company) ? string.Empty : Company;
            objBookmark.UserEmail = string.IsNullOrEmpty(MailFrom) ? string.Empty : MailFrom;
            objBookmark.Subject = string.IsNullOrEmpty(Subject) ? string.Empty : Subject;
            objBookmark.Suggestion = string.IsNullOrEmpty(Suggestion) ? string.Empty : Suggestion;
            MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Utility.GetUID(!string.IsNullOrEmpty(System.Web.HttpContext.Current.User.Identity.Name.ToString()) ? System.Web.HttpContext.Current.User.Identity.Name.ToString() : "admin@intelifi.com"));
            #endregion

            // bool success = Utility.SendMail("support@dotnetoutsourcing.com", string.Empty, string.Empty, objBookmark.UserEmail, MessageBody, Subject);//ConfigurationManager.AppSettings["EMEditMailTo"].ToString(),
            bool success = Utility.SendMail(settings.SupportEmailId, string.Empty, string.Empty, settings.SupportEmailId, MessageBody, Subject);//ConfigurationManager.AppSettings["EMEditMailTo"].ToString(),

            if (success == true)
            {
                CssClass = "successmsg";
                Message = "Your suggestion is submitted successfully.";
            }
            else
            {
                CssClass = "errormsg";
                Message = "Some error occured,please try again.";

            }
            return Json(new { Msg = Message, Css = CssClass });
        }
        #endregion

        [HttpPost]
        public ActionResult SendEmailToSupprtAndUser(int ddlpkCompanyId, int ddlCompanyUser, string txtComments, string CompanyName)
        {
            string emailText = "";
            string CssClass = "";
            string Message = "";
            string MessageBody = "";
            BALGeneral ObjBALGeneral = new BALGeneral();
            string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;
            List<Proc_Get_CompanyUsersByCompanyIdResult> userlist = new List<Proc_Get_CompanyUsersByCompanyIdResult>();
            if (System.Web.HttpContext.Current.Session["ddlCompanyUsers"] != null)
            {
                userlist = (List<Proc_Get_CompanyUsersByCompanyIdResult>)Session["ddlCompanyUsers"];
            }
            List<Proc_Get_CompanyUsersByCompanyIdResult> AllItems = userlist;
            var Record = AllItems.Where(m => m.CompanyUserId == ddlCompanyUser).FirstOrDefault();
            var UserEmail = Record.UserEmail;
            string subject = "Support Ticket";
            MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));
            var CurrentUserEmail = ObjMembershipUser.Email;
            int PkTemplateId = 33;

            var emailContent = ObjBALGeneral.GetEmailTemplate(SiteApplicationId, PkTemplateId);// "Emerge Suggestions"
            emailText = emailContent.TemplateContent;
            #region New way Bookmark
            BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();


            #region For New Style SystemTemplate
            string Bookmarkfile = string.Empty;
            string bookmarkfilepath = string.Empty;
            Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
            Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
            Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
            emailText = Bookmarkfile;
            #endregion

            Bookmarks objBookmark = new Bookmarks();
            objBookmark.EmailContent = emailText;// txtComments;
            objBookmark.CompanyStatus = txtComments;
            objBookmark.Date = DateTime.Now.ToShortDateString();
            objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
            objBookmark.CompanyName = string.IsNullOrEmpty(CompanyName) ? string.Empty : CompanyName;
            objBookmark.UserEmail = string.IsNullOrEmpty(CurrentUserEmail) ? string.Empty : CurrentUserEmail;
            objBookmark.Subject = string.IsNullOrEmpty(subject) ? string.Empty : subject;



            MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Utility.GetUID(!string.IsNullOrEmpty(System.Web.HttpContext.Current.User.Identity.Name.ToString()) ? System.Web.HttpContext.Current.User.Identity.Name.ToString() : "admin@intelifi.com"));

            #endregion


            bool success = Utility.SendMail("support@intelifisupport.zendesk.com", UserEmail, string.Empty, CurrentUserEmail, MessageBody, subject);//"support@intelifisupport.zendesk.com"
            if (success == true)
            {
                CssClass = "successmsg";
                Message = "E-Mail Sent Successfully";
            }
            else
            {
                CssClass = "errormsg";
                Message = "Some error occured,please try again.";

            }
            return Json(new { Msg = Message, Css = CssClass });
        }

        [HttpPost]
        public ActionResult GetCompanyUserByCompanyId(string CompanyName, string PageType)
        {
            BALCompany ObjBALCompany = new BALCompany();
            BALLeads ObjBALLeads = new BALLeads();
            int CompanyId = 0;
            string CompanyNme = CompanyName.Split('[')[0];
            var CompanyRec = ObjBALLeads.GetCompanyIdByComapnyName(CompanyNme);
            int tempCompanyId = 0;
            tempCompanyId = AutoCompleteCompany_id(CompanyNme, PageType, CompanyName);
            if (CompanyRec != null)
            {
                CompanyId = CompanyRec.pkCompanyId;
            }

            if (tempCompanyId != 0)
            {
                CompanyId = tempCompanyId;

            }

            var Users = ObjBALCompany.GetCompanyUserByPkCompanyId(CompanyId);
            var GetComapnyStatus = ObjBALCompany.GetCompanyStatausByCompanyId(CompanyId);
            string RtnCompanyStatus = string.Empty;
            if (PageType == "office")
            {
                if (GetComapnyStatus.Count > 0)
                {
                    RtnCompanyStatus = GetComapnyStatus.ElementAt(0).LeadStatus.ToString();
                }
                else
                {
                    RtnCompanyStatus = Guid.Empty.ToString();
                }
            }
            else
            {
                if (GetComapnyStatus.Count() > 0)
                {
                    if (GetComapnyStatus.ElementAt(0).LeadStatus.ToString() == "8")
                    {
                        RtnCompanyStatus = "8";
                    }
                    else if (GetComapnyStatus.ElementAt(0).LeadStatus.ToString() == "9")
                    {
                        RtnCompanyStatus = "9";
                    }
                    else if (GetComapnyStatus.ElementAt(0).IsEnabled == false)
                    {
                        RtnCompanyStatus = "7";
                    }
                    else if (GetComapnyStatus.ElementAt(0).IsEnabled == true && GetComapnyStatus.ElementAt(0).IsActive.ToString() == "1")
                    {
                        RtnCompanyStatus = "6";
                    }
                    else if (GetComapnyStatus.ElementAt(0).IsEnabled == true && GetComapnyStatus.ElementAt(0).IsActive.ToString() == "0")
                    {
                        RtnCompanyStatus = "10";
                    }
                }
                else
                {
                    RtnCompanyStatus = "-1";
                }

            }
            List<SelectListItem> listUsers = new List<SelectListItem>();
            if (Users.Count() > 0)
            {
                listUsers = Users.Select(x => new SelectListItem { Text = x.UserEmail, Value = x.CompanyUserId.ToString() }).ToList();
            }
            listUsers.Insert(0, new SelectListItem { Text = "---All Users---", Value = "-1" });
            Session["ddlCompanyUsers"] = Users;
           // List<Proc_Get_CompanyUsersByCompanyIdResult> AllCompanyUser = Users;
            return Json(new { Data = listUsers, CompanyStatus = RtnCompanyStatus, pkCompanyId = CompanyId });
        }


        #region MyPreferences


        public ActionResult GetReferenceCode()
        {
            BALReferenceCode objBALReferenceCode = new BALReferenceCode();
            int fkcompanyid = 0;
            if (!string.IsNullOrEmpty(Session["pkCompanyId"].ToString()))
            {
                fkcompanyid = Convert.ToInt32(Session["pkCompanyId"].ToString());
            }
            var referencelist = objBALReferenceCode.GetReferences(fkcompanyid).Select(c => new ReferenceCodeModel
            {
                pkReferenceCodeId = c.pkReferenceCodeId,
                ReferenceCode = c.ReferenceCode,
                ReferenceNote = c.ReferenceNote
            }).ToList();
            return Json(referencelist);
        }


        [HttpPost]
        public ActionResult AddReferenceCode(IEnumerable<ReferenceCodeModel> referencecodes)
        {
            var result = new List<tblReferenceCode>();
            int fkcompanyid = 0;
            if (!string.IsNullOrEmpty(Session["pkCompanyId"].ToString()))
            {
                fkcompanyid = Convert.ToInt32(Session["pkCompanyId"].ToString());
                foreach (var referencecodeModel in referencecodes)
                {
                    var ReferenceCode = new tblReferenceCode
                    {
                        pkReferenceCodeId = referencecodeModel.pkReferenceCodeId,
                        fkCompanyId = fkcompanyid,
                        ReferenceCode = referencecodeModel.ReferenceCode,
                        ReferenceNote = referencecodeModel.ReferenceNote
                    };

                    BALReferenceCode ObjBALReferenceCode = new BALReferenceCode();
                    //int success = 0;
                    //success = 
                        ObjBALReferenceCode.AddReference(ReferenceCode, fkcompanyid);
                }
            }


            return Json(result.Select(c => new ReferenceCodeModel
            {
                pkReferenceCodeId = c.pkReferenceCodeId,
                ReferenceCode = c.ReferenceCode,
                ReferenceNote = c.ReferenceNote
            }).ToList());
        }

        [HttpPost]
        public ActionResult UpdateReferenceCode(IEnumerable<ReferenceCodeModel> referencecodes)
        {
            BALReferenceCode ObjBALReferenceCode = new BALReferenceCode();
            foreach (var referencecodeModel in referencecodes)
            {
                var ReferenceCode = new tblReferenceCode
                {
                    pkReferenceCodeId = referencecodeModel.pkReferenceCodeId,
                    ReferenceCode = referencecodeModel.ReferenceCode,
                    ReferenceNote = referencecodeModel.ReferenceNote
                };
                //int Success = 
                    ObjBALReferenceCode.UpdateReference(ReferenceCode);
            }

            return Json(null);
        }

        [HttpPost]
        public ActionResult DeleteReferenceCode(IEnumerable<ReferenceCodeModel> referencecodes)
        {
            int fkcompanyid = 0;
            if (!string.IsNullOrEmpty(Session["pkCompanyId"].ToString()))
            {
                fkcompanyid = Convert.ToInt32(Session["pkCompanyId"].ToString());
                foreach (var referencecodeModel in referencecodes)
                {
                    BALReferenceCode ObjBALReferenceCode = new BALReferenceCode();
                    //int success = 0;
                    //success = 
                        ObjBALReferenceCode.DeleteReference(referencecodeModel.pkReferenceCodeId, fkcompanyid);
                }
            }
            //Return emtpy result
            return Json(null);
        }


        public ActionResult GetNotificationEmailAlert()
        {
            BALReferenceCode objBALReferenceCode = new BALReferenceCode();
            //int fkcompanyid = 0;
            int fkCompanyUserId = 0;
            if (!string.IsNullOrEmpty(Session["pkCompanyId"].ToString()))
            {
                //fkcompanyid = Convert.ToInt32(Session["pkCompanyId"].ToString());
                string username = (Session["CurrentUserMailId"] == null) ? string.Empty : Session["CurrentUserMailId"].ToString();
                BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
                var dbCollection = ObjBALCompanyUsers.GetUserInformationByMembershipUserId(new Guid(Membership.GetUser(username).ProviderUserKey.ToString()));
                if (dbCollection.Count > 0)
                {
                    fkCompanyUserId = dbCollection.First().pkCompanyUserId;

                }
            }
            var lstNotificationEmailAlertModel = objBALReferenceCode.GetEmailAlerts(fkCompanyUserId).Select(c => new NotificationEmailAlertModel
            {
                pkEmailAlertId = Convert.ToInt32(c.pkEmailNotificationAlertId),
                fkTemplateId = c.pkTemplateId,
                fkCompanyUserId = c.fkCompanyUserId,
                TemplateName = c.TemplateName,
                IsSendEmail = c.SendEmail,
                IsSendAlert = c.SendAlert
            }).ToList();
            return Json(lstNotificationEmailAlertModel);
        }

        [HttpPost]
        public ActionResult UpdateNotificationEmailAlert(IEnumerable<NotificationEmailAlertModel> emailalerts)
        {
            BALReferenceCode ObjBALReferenceCode = new BALReferenceCode();
            foreach (var EmailAlertModel in emailalerts)
            {
                var EmailNotificationAlert = new tblEmailNotificationAlert
                {
                    pkEmailNotificationAlertId = EmailAlertModel.pkEmailAlertId,
                    fkTemplateId = EmailAlertModel.fkTemplateId,
                    fkCompanyUserId = EmailAlertModel.fkCompanyUserId ?? 0,
                    IsSendAlert = EmailAlertModel.IsSendAlert,
                    IsSendEmail = EmailAlertModel.IsSendEmail
                };
                //int Success = 
                    ObjBALReferenceCode.UpdateEmailAlertInfo(EmailNotificationAlert);
            }

            return Json(null);
        }

        [HttpPost]
        public ActionResult UpdatePreferences(string DefaultPage, string SortColumn, string RecordSize, string IsDataEntryOnly)
        {
            string strResult = "";
           // int fkcompanyid = 0;
            if (!string.IsNullOrEmpty(Session["pkCompanyId"].ToString()))
            {
                //fkcompanyid = Convert.ToInt32(Session["pkCompanyId"].ToString());
                string username = (Session["CurrentUserMailId"] == null) ? string.Empty : Session["CurrentUserMailId"].ToString();
                ProfileCommon Profile = new ProfileCommon();
                Profile.SavePreferencesPageProfile(username, DefaultPage, SortColumn, RecordSize, IsDataEntryOnly);
                strResult = "<span class='successmsg'>Info updated Successfully</span>";
            }
            return Json(new { Message = strResult });

        }

        #endregion

        #region(MANAGE FOLDER)

        #region(GET MANAGE FOLDER)

        public ActionResult GetManageFolder()
        {
            BALManageFolder objBALManageFolder = new BALManageFolder();
            int fkcompanyid = 0;
            if (!string.IsNullOrEmpty(Session["pkCompanyId"].ToString()))
            {
                fkcompanyid = Convert.ToInt32(Session["pkCompanyId"].ToString());
            }
            var folderlist = objBALManageFolder.GetManageFolder(fkcompanyid).Select(c => new ManageFolderModel
            {
                pkManageFolderId = c.pkManageFolderId,
                FolderName = c.FolderName,
                FolderDescription = c.FolderDescription
            }).ToList();
            return Json(folderlist);
        }

        #endregion

        #region(ADD MANAGE FOLDER)

        [HttpPost]
        public ActionResult AddManageFolder(IEnumerable<ManageFolderModel> managefolder)
        {

            string strSuccess = string.Empty;
           // var result = new List<tblManageFolder>();
            int fkcompanyid = 0;
            int success = 0;
            if (!string.IsNullOrEmpty(Session["pkCompanyId"].ToString()))
            {
                fkcompanyid = Convert.ToInt32(Session["pkCompanyId"].ToString());
                foreach (var managefolderModel in managefolder)
                {
                    var ManageFolder = new tblManageFolder
                    {
                        pkManageFolderId = managefolderModel.pkManageFolderId,
                        fkCompanyId = fkcompanyid,
                        FolderName = managefolderModel.FolderName,
                        FolderDescription = managefolderModel.FolderDescription
                    };

                    BALManageFolder ObjBALManageFolder = new BALManageFolder();

                    success = ObjBALManageFolder.AddManageFolder(ManageFolder, fkcompanyid);
                    //if (success == -1)
                    //{
                    //   ScriptManager.RegisterStartupScript(this.page, this.Page.GetType(), "alert('Duplicate Value');", true);
                    //}
                }


            }
            if (success == 1)
            {

                strSuccess = "<span class='successmsgafd'>Settings updated successfully.</span>";
            }
            else
            {

                strSuccess = "<span class='errormsg'>Some error occured while updation.</span>";

            }


            return Json(new { Message = strSuccess });


            //return Json(result.Select(c => new ManageFolderModel
            //{
            //    pkManageFolderId = c.pkManageFolderId,
            //    FolderName = c.FolderName,
            //    FolderDescription = c.FolderDescription
            //}).ToList());
        }

        #endregion

        #region(UPDATE MANAGE FOLDER)

        [HttpPost]
        public ActionResult UpdateManageFolder(IEnumerable<ManageFolderModel> managefolder)
        {
            BALManageFolder ObjBALManageFolder = new BALManageFolder();
            foreach (var managefolderModel in managefolder)
            {
                var ManageFolder = new tblManageFolder
                {
                    pkManageFolderId = managefolderModel.pkManageFolderId,
                    FolderName = managefolderModel.FolderName,
                    FolderDescription = managefolderModel.FolderDescription
                };
                //int Success = 
                    ObjBALManageFolder.UpdateManageFolder(ManageFolder);
            }

            return Json(null);
        }

        #endregion

        #region(DELETE MANAGE FOLDER)

        [HttpPost]
        public ActionResult DeleteManageFolder(IEnumerable<ManageFolderModel> managefolder)
        {
            int fkcompanyid = 0;
            if (!string.IsNullOrEmpty(Session["pkCompanyId"].ToString()))
            {
                fkcompanyid = Convert.ToInt32(Session["pkCompanyId"].ToString());
                foreach (var managefolderModel in managefolder)
                {
                    BALManageFolder ObjBALManageFolder = new BALManageFolder();
                    //int success = 0;
                    //success = 
                        ObjBALManageFolder.DeleteFolder(managefolderModel.pkManageFolderId, fkcompanyid);
                }
            }
            //Return emtpy result
            return Json(null);
        }

        #endregion

        #endregion




        #region callLog

        public PartialViewResult LoadCallLog(string PageType, string pkCompanyId)
        {
            CallLogModel Model = new CallLogModel();

            BALCompany ObjBALCompany = new BALCompany();
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            Guid UserId = Utility.GetUID(User.Identity.Name);
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                Response.Redirect(ApplicationPath.GetApplicationPath() + "Default");
            }
            else
            {
                try
                {
                    List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjData = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(UserId);
                    Session["pkCompanyId"] = ObjData.FirstOrDefault().pkCompanyId;
                    Session["pkLocationId"] = ObjData.FirstOrDefault().pkLocationId;
                    Session["CurrentUserCompanyName"] = ObjData.First().CompanyName;
                    Session["UserFullName"] = ObjData.First().FirstName + " " + ObjData.First().LastName;
                    Session["CurrentUserMailId"] = ObjData.First().UserName;
                    Session["CurrentUserId"] = ObjData.First().pkCompanyUserId;
                    //Guid fkcompanyid = Guid.Empty;
                    //Guid calllogCompanyId = Guid.Empty;
                    Model.ComapnyStatus = -1;
                    List<SelectListItem> CompanyList = new List<SelectListItem>();
                    List<SelectListItem> Milestonelist = new List<SelectListItem>();
                    Milestonelist.Insert(0, new SelectListItem { Text = "---SelectMileStone---", Value = "-1" });
                    //if (string.IsNullOrEmpty(PageType))
                    //{
                    //    var companies = ObjBALCompany.GetAllCompanyForEmerge(SiteApplicationId, true, false);
                    //    if (companies.Count() > 0)
                    //    {
                    //        CompanyList = companies.Select(x => new SelectListItem { Text = x.CompanyName, Value = x.pkCompanyId.ToString() }).ToList();
                    //        CompanyList.Insert(0, new SelectListItem { Text = "---Select Company---", Value = Guid.Empty.ToString() });
                    //    }
                    //}
                    //else
                    //{
                    //    List<CompanyList> companies = new List<CompanyList>();
                    //    if (PageType == "Clients_1")
                    //    {
                    //        BALOffice ObjBALOffice = new BALOffice();
                    //        companies = ObjBALCompany.GetAllCompanyForEmerge(SiteApplicationId, true, false);
                    //    }
                    //    else
                    //    {
                    //        BALOffice ObjBALOffice = new BALOffice();
                    //         companies = ObjBALOffice.GetAllLeadsForEmergeOffice(Utility.SiteApplicationId, true, false);
                    //    }
                    //    if (companies.Count() > 0)
                    //    {
                    //        CompanyList = companies.Select(x => new SelectListItem { Text = x.CompanyName, Value = x.pkCompanyId.ToString() }).ToList();
                    //        CompanyList.Insert(0, new SelectListItem { Text = "---Select Company---", Value = Guid.Empty.ToString() });
                    //    }
                    //}
                    if (Request.Url.ToString().ToLower().Contains("office"))
                    {
                        List<CompanyList> companies = new List<CompanyList>();

                        Model.PageType = "office";
                        if (Request.Url.ToString().ToLower().Contains("leads") || Request.Url.ToString().ToLower().Contains("addeditleads"))
                        {
                            BALOffice ObjBALOffice = new BALOffice();
                            companies = ObjBALOffice.GetAllLeadsForEmergeOffice(Utility.SiteApplicationId, true, false);
                            Milestonelist.Insert(1, new SelectListItem { Text = "File Started", Value = "1" });
                            Milestonelist.Insert(2, new SelectListItem { Text = "Proposal Out", Value = "2" });
                            // Milestonelist.Insert(3, new SelectListItem { Text = "Proposal In", Value = "3" });
                            Milestonelist.Insert(3, new SelectListItem { Text = "Agreement Out", Value = "3" });
                            Milestonelist.Insert(4, new SelectListItem { Text = "Agreement In", Value = "4" });
                            if (Session["pkcompanyidforChangeCallLog"] != null)
                            {
                                string pkcompanyidforCallLog = Session["pkcompanyidforChangeCallLog"].ToString();
                                if (pkcompanyidforCallLog != "-1")
                                {
                                    var ComapnyStaus = ObjBALCompany.GetCompanyStatausByCompanyId(Int32.Parse(pkcompanyidforCallLog));
                                    int Status = ComapnyStaus.ElementAt(0).LeadStatus;
                                    if (Status == 1 || Status == 2 || Status == 3 || Status == 4)
                                    {
                                        Model.ComapnyStatus = ComapnyStaus.ElementAt(0).LeadStatus;
                                    }
                                }
                            }



                        }

                        else
                        {

                            BALOffice ObjBALOffice = new BALOffice();
                            companies = ObjBALCompany.GetAllCompanyForEmerge(SiteApplicationId, true, false);
                            Milestonelist.Insert(1, new SelectListItem { Text = "Account InActive", Value = "6" });
                            Milestonelist.Insert(2, new SelectListItem { Text = "Locked", Value = "7" });
                            Milestonelist.Insert(3, new SelectListItem { Text = "Past Due", Value = "8" });
                            Milestonelist.Insert(4, new SelectListItem { Text = "In Collection", Value = "9" });
                            Milestonelist.Insert(5, new SelectListItem { Text = "Active", Value = "10" });
                            Model.PageType = "MainPage";
                            if (Session["pkcompanyidforChangeCallLog"] != null)
                            {
                                string pkcompanyidforCallLog = Session["pkcompanyidforChangeCallLog"].ToString();
                                if (pkcompanyidforCallLog != "-1")
                                {
                                    var ComapnyStaus = ObjBALCompany.GetCompanyStatausByCompanyId(Int32.Parse(pkcompanyidforCallLog));
                                    int Status = ComapnyStaus.ElementAt(0).LeadStatus;
                                    if (Status == 0 || Status == 5 || Status == 6 || Status == 7 || Status == 8 || Status == 9 || Status == 10)
                                    {
                                        Model.ComapnyStatus = ComapnyStaus.ElementAt(0).LeadStatus;
                                    }
                                }
                            }
                        }
                        if (companies.Count() > 0)
                        {
                            companies.Insert(0, new CompanyList { CompanyName = "---Select Company---", pkCompanyId = 0 });
                            CompanyList = companies.Select(x => new SelectListItem { Text = x.CompanyName, Value = x.pkCompanyId.ToString() }).ToList();
                            // CompanyList.Insert(0, new SelectListItem { Text = "---Select Company---", Value = Guid.Empty.ToString() });
                        }

                        //  CompanyUserlist.Insert(0, new SelectListItem { Text = "---Select User---", Value = "-1" });

                    }
                    else if (string.IsNullOrEmpty(PageType))
                    {
                        Model.PageType = "MainPage";
                        var companies = ObjBALCompany.GetAllCompanyForEmerge(SiteApplicationId, true, false);

                        if (companies.Count() > 0)
                        {
                            CompanyList = companies.Select(x => new SelectListItem { Text = x.CompanyName, Value = x.pkCompanyId.ToString() }).ToList();
                            CompanyList.Insert(0, new SelectListItem { Text = "---Select Company---", Value = Guid.Empty.ToString() });
                        }
                        Milestonelist.Insert(1, new SelectListItem { Text = "Account InActive", Value = "6" });
                        Milestonelist.Insert(2, new SelectListItem { Text = "Locked", Value = "7" });
                        Milestonelist.Insert(3, new SelectListItem { Text = "Past Due", Value = "8" });
                        Milestonelist.Insert(4, new SelectListItem { Text = "In Collection", Value = "9" });
                        Milestonelist.Insert(5, new SelectListItem { Text = "Active", Value = "10" });
                        if (pkCompanyId == null)
                        {
                            Model.ComapnyStatus = -1;
                            if (Session["pkcompanyidforChangeCallLog"] != null)
                            {
                                string pkcompanyidforCallLog = Session["pkcompanyidforChangeCallLog"].ToString();
                                if (pkcompanyidforCallLog != "-1")
                                {
                                    var ComapnyStaus = ObjBALCompany.GetCompanyStatausByCompanyId(Int32.Parse(pkcompanyidforCallLog));
                                    int Status = ComapnyStaus.ElementAt(0).LeadStatus;
                                    if (Status == 0 || Status == 5 || Status == 6 || Status == 7 || Status == 8 || Status == 9 || Status == 10)
                                    {
                                        Model.ComapnyStatus = ComapnyStaus.ElementAt(0).LeadStatus;
                                    }
                                }
                            }

                        }
                        else
                        {
                            var ComapnyStaus = ObjBALCompany.GetCompanyStatausByCompanyId(Int32.Parse(pkCompanyId));
                            if (ComapnyStaus.ElementAt(0).LeadStatus == 8 || ComapnyStaus.ElementAt(0).LeadStatus == 9)
                            {
                                Model.ComapnyStatus = ComapnyStaus.ElementAt(0).LeadStatus;
                            }
                            else if (ComapnyStaus.ElementAt(0).IsEnabled == false)
                            {
                                Model.ComapnyStatus = 7;
                            }
                            else
                            {
                                if (ComapnyStaus.ElementAt(0).IsEnabled == true || ComapnyStaus.ElementAt(0).LeadStatus == 8 || ComapnyStaus.ElementAt(0).IsActive == 0)
                                {
                                    Model.ComapnyStatus = 10;
                                }
                                else
                                {
                                    Model.ComapnyStatus = 6;
                                }
                            }
                        }
                    }
                    else if (!string.IsNullOrEmpty(PageType))
                    {
                        List<CompanyList> companies = new List<CompanyList>();
                        if (PageType == "Clients_1")
                        {
                            BALOffice ObjBALOffice = new BALOffice();

                            companies = ObjBALCompany.GetAllCompanyForEmerge(SiteApplicationId, true, false);
                            companies.Insert(0, new CompanyList { CompanyName = "---Select Company---", pkCompanyId = 0 });
                            Milestonelist.Insert(1, new SelectListItem { Text = "Account InActive", Value = "6" });
                            Milestonelist.Insert(2, new SelectListItem { Text = "Locked", Value = "7" });
                            Milestonelist.Insert(3, new SelectListItem { Text = "PastDue", Value = "8" });
                            Milestonelist.Insert(4, new SelectListItem { Text = "In Collection", Value = "9" });
                            Milestonelist.Insert(5, new SelectListItem { Text = "Active", Value = "10" });
                            var ComapnyStaus = ObjBALCompany.GetCompanyStatausByCompanyId(Int32.Parse(pkCompanyId));
                            if (ComapnyStaus.ElementAt(0).LeadStatus == 8 || ComapnyStaus.ElementAt(0).LeadStatus == 9)
                            {
                                Model.ComapnyStatus = ComapnyStaus.ElementAt(0).LeadStatus;
                            }
                            else if (ComapnyStaus.ElementAt(0).IsEnabled == false)
                            {
                                Model.ComapnyStatus = 7;
                            }
                            else
                            {
                                if (ComapnyStaus.ElementAt(0).IsEnabled == true || ComapnyStaus.ElementAt(0).LeadStatus == 8 || ComapnyStaus.ElementAt(0).IsActive == 0)
                                {
                                    Model.ComapnyStatus = 10;
                                }
                                else
                                {
                                    Model.ComapnyStatus = 6;
                                }
                            }
                        }
                        else
                        {
                            Model.PageType = "office";
                            BALOffice ObjBALOffice = new BALOffice();
                            companies = ObjBALOffice.GetAllLeadsForEmergeOffice(Utility.SiteApplicationId, true, false);
                            Milestonelist.Insert(1, new SelectListItem { Text = "File Started", Value = "1" });
                            Milestonelist.Insert(2, new SelectListItem { Text = "Proposal Out", Value = "2" });
                            //  Milestonelist.Insert(3, new SelectListItem { Text = "Proposal In", Value = "3" });
                            Milestonelist.Insert(3, new SelectListItem { Text = "Agreement Out", Value = "3" });
                            Milestonelist.Insert(4, new SelectListItem { Text = "Agreement In", Value = "4" });
                            var ComapnyStaus = ObjBALCompany.GetCompanyStatausByCompanyId(Int32.Parse(pkCompanyId));
                            Model.ComapnyStatus = ComapnyStaus.ElementAt(0).LeadStatus;
                        }
                        if (companies.Count() > 0)
                        {

                            CompanyList = companies.Select(x => new SelectListItem { Text = x.CompanyName, Value = x.pkCompanyId.ToString() }).ToList();

                        }
                        //  CompanyUserlist.Insert(0, new SelectListItem { Text = "---Select User---", Value = "-1" });
                    }
                    else
                    {
                        var companies = ObjBALCompany.GetAllCompanyForEmerge(SiteApplicationId, true, false);
                        if (companies.Count() > 0)
                        {
                            CompanyList = companies.Select(x => new SelectListItem { Text = x.CompanyName, Value = x.pkCompanyId.ToString() }).ToList();
                            CompanyList.Insert(0, new SelectListItem { Text = "---Select Company---", Value = Guid.Empty.ToString() });
                        }
                        Milestonelist.Insert(1, new SelectListItem { Text = "Account InActive", Value = "6" });
                        Milestonelist.Insert(2, new SelectListItem { Text = "Locked", Value = "7" });
                        Milestonelist.Insert(3, new SelectListItem { Text = "Past Due", Value = "8" });
                        Milestonelist.Insert(4, new SelectListItem { Text = "In Collection", Value = "9" });
                        Milestonelist.Insert(5, new SelectListItem { Text = "Active", Value = "10" });
                    }
                    Model.ddlCompanyUsers = Milestonelist;
                    Model.CompanyList = CompanyList;
                    Model.CallLogCompany = string.Empty;
                    Model.CallLogHeader = Server.HtmlDecode("<span class='imgCallLog'>Call Log</span>");
                    Model.IsCallLogIcon = false;
                    Model.IsCallLogOpen = false;
                    if (!string.IsNullOrEmpty(Session["pkCompanyId"].ToString()))
                    {
                        //fkcompanyid = new Guid(Session["pkCompanyId"].ToString());

                        string username = (Session["CurrentUserMailId"] == null) ? string.Empty : Session["CurrentUserMailId"].ToString();
                       // string UserRole = Roles.GetRolesForUser().FirstOrDefault();
                        ProfileCommon Profile = new ProfileCommon();
                        ProfileModel userProfile = Profile.GetProfile(username);
                        if (Session["Admin"] != null)
                        {
                            string role = Session["Admin"].ToString();
                            if (role == "SystemAdmin" || role == "SupportManager" || role == "SupportTechnician")
                            {
                                Model.IsCallLogIcon = true;
                                int FinalCompanyStatus = Model.ComapnyStatus;
                                if (!string.IsNullOrEmpty(userProfile.CallLogCompany) && userProfile.CallLogCompany != "0" && FinalCompanyStatus != -1)
                                {
                                    Model.CompanyList = Model.CompanyList.Select(x => new SelectListItem { Text = x.Text, Value = x.Value, Selected = (x.Value == userProfile.CallLogCompany) }).ToList();
                                    Model.IsCallLogOpen = true;
                                    Model.CallLogCompany = userProfile.CallLogCompany;
                                }
                            }
                        }
                        if (Session["LoggedUser"] != null)
                        {
                            Model.IsCallLogIcon = true;
                            int FinalCompanyStatus = Model.ComapnyStatus;
                            if (!string.IsNullOrEmpty(userProfile.CallLogCompany) && userProfile.CallLogCompany != "0" && FinalCompanyStatus != -1)
                            {
                                Model.CompanyList = Model.CompanyList.Select(x => new SelectListItem { Text = x.Text, Value = x.Value, Selected = (x.Value == userProfile.CallLogCompany) }).ToList();
                                Model.IsCallLogOpen = true;
                                Model.CallLogCompany = userProfile.CallLogCompany;

                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(PageType))
                    {
                        ViewBag.PageType = Request.QueryString["PageType"];
                    }
                }
                catch { }
            }
            // 
            return PartialView("PartialCallLog", Model);
        }


        public PartialViewResult LoadCallLogComment(string CallLogCompanyId)
        {
            CallLogModel Model = new CallLogModel();
            BALCompany ObjBALCompany = new BALCompany();
            //BALOrders ObjBALOrders = new BALOrders();
            BALLeads ObjBALLeads = new BALLeads();
            string Url_Path = Request.Url.AbsoluteUri.ToString();
            string PageType = string.Empty; ;
            PageType = "main";
            if (Url_Path.IndexOf("Leads") != -1 || Url_Path.IndexOf("Leads") != -1)
            {
                PageType = "office";
            }
            {
                try
                {

                    string CompanyNme = CallLogCompanyId.Split('[')[0];
                    string NewCompanyId = string.Empty;
                    var CompanyRec = ObjBALLeads.GetCompanyIdByComapnyName(CompanyNme);

                    int tempCompanyId = 0;
                    if (string.IsNullOrEmpty(CallLogCompanyId) == false)
                    {
                        tempCompanyId = AutoCompleteCompany_id(CompanyNme, PageType, CallLogCompanyId);

                    }
                    if (CompanyRec != null)
                    {
                        NewCompanyId = CompanyRec.pkCompanyId.ToString();
                        if (tempCompanyId != 0) { NewCompanyId = tempCompanyId.ToString(); }
                    }
                    else
                    {
                        NewCompanyId = CallLogCompanyId;
                        if (tempCompanyId != 0) { NewCompanyId = tempCompanyId.ToString(); }
                    }

                    //int fkcompanyid = 0;
                    string calllogCompanyId = string.Empty;
                    string CompanyName = string.Empty;
                    Model.SelectedComapnyName = string.Empty;
                    if (!string.IsNullOrEmpty(NewCompanyId))
                    {
                        calllogCompanyId = NewCompanyId;
                        Session["pkcompanyidforChangeCallLog"] = NewCompanyId;
                        if (Session["CallLohCompanyName"] != null)
                        {
                            Model.SelectedComapnyName = Session["CallLohCompanyName"].ToString();
                        }
                        else
                        {
                            var CompanyInfo = ObjBALCompany.GetCompanyNameForCallLogById(Int32.Parse(NewCompanyId));
                            if (CompanyInfo != null && CompanyInfo.Count() != 0)
                            {
                                CompanyName = CompanyInfo.ElementAt(0).CompanyName;
                                Model.SelectedComapnyName = CompanyName;
                            }
                        }

                    }
                    List<Proc_Get_CommentsForLeadResult> CallLogCommentList = null;
                    CallLogCommentList = ObjBALCompany.GetCommentsForCompany(Convert.ToInt32(calllogCompanyId));
                    Model.CallLogCommentList = CallLogCommentList;

                    if (!string.IsNullOrEmpty(Session["pkCompanyId"].ToString()))
                    {
                        //fkcompanyid = Convert.ToInt32(Session["pkCompanyId"].ToString());
                        string username = (Session["CurrentUserMailId"] == null) ? string.Empty : Session["CurrentUserMailId"].ToString();
                        //string UserRole = Roles.GetRolesForUser().FirstOrDefault();
                        ProfileCommon Profile = new ProfileCommon();
                        Profile.SetCallLogCompanyProfile(username, calllogCompanyId.ToString());
                        ProfileModel userProfile = Profile.GetProfile(username);
                        if (Session["Admin"] != null)
                        {
                            string role = Session["Admin"].ToString();
                            if (role == "SystemAdmin" || role == "SupportManager" || role == "SupportTechnician")
                            {
                                if (!string.IsNullOrEmpty(userProfile.CallLogCompany))
                                {
                                    CallLogCommentList = ObjBALCompany.GetCommentsForCompany(Convert.ToInt32(userProfile.CallLogCompany));
                                    Model.CallLogCommentList = CallLogCommentList;
                                }
                            }
                        }
                    }
                }
                catch { }
            }

            return PartialView("PartialCallLogComment", Model);
        }


        public PartialViewResult LoadUpdatedCallLogComment(string CompanyName, string PageType)
        {

            CallLogModel Model = new CallLogModel();
            string CallLogCompanyId = string.Empty;
            BALCompany ObjBALCompany = new BALCompany();
            BALLeads ObjBALLeads = new BALLeads();
            {
                try
                {
                    //int fkcompanyid = 0;
                    string calllogCompanyId = string.Empty;
                    //BALOrders ObjBALOrders = new BALOrders();
                    Session["CallLohCompanyName"] = CompanyName;
                    string CompanyNme = CompanyName.Split('[')[0];
                    //int CompanyId = 0;
                    var CompanyRec = ObjBALLeads.GetCompanyIdByComapnyName(CompanyNme);

                    int tempCompanyId = 0;
                    tempCompanyId = AutoCompleteCompany_id(CompanyNme, PageType, CompanyName);

                    if (CompanyRec != null)
                    {
                        CallLogCompanyId = CompanyRec.pkCompanyId.ToString();
                    }

                    if (tempCompanyId != 0)
                    {

                        CallLogCompanyId = tempCompanyId.ToString();
                    }




                    if (!string.IsNullOrEmpty(CallLogCompanyId))
                    {
                        calllogCompanyId = CallLogCompanyId;
                        Session["pkcompanyidforChangeCallLog"] = CallLogCompanyId;
                    }
                    List<Proc_Get_CommentsForLeadResult> CallLogCommentList = null;
                    CallLogCommentList = ObjBALCompany.GetCommentsForCompany(Convert.ToInt32(calllogCompanyId));
                    Model.CallLogCommentList = CallLogCommentList;
                    if (!string.IsNullOrEmpty(Session["pkCompanyId"].ToString()))
                    {
                        //fkcompanyid = Convert.ToInt32(Session["pkCompanyId"].ToString());
                        string username = (Session["CurrentUserMailId"] == null) ? string.Empty : Session["CurrentUserMailId"].ToString();
                        //string UserRole = Roles.GetRolesForUser().FirstOrDefault();
                        ProfileCommon Profile = new ProfileCommon();
                        Profile.SetCallLogCompanyProfile(username, calllogCompanyId.ToString());
                        ProfileModel userProfile = Profile.GetProfile(username);
                        if (Session["Admin"] != null)
                        {
                            string role = Session["Admin"].ToString();
                            if (role == "SystemAdmin" || role == "SupportManager" || role == "SupportTechnician")
                            {
                                if (!string.IsNullOrEmpty(userProfile.CallLogCompany))
                                {
                                    CallLogCommentList = ObjBALCompany.GetCommentsForCompany(Convert.ToInt32(userProfile.CallLogCompany));
                                    Model.CallLogCommentList = CallLogCommentList;
                                }
                            }
                        }
                    }
                }
                catch { }
            }

            return PartialView("PartialCallLogComment", Model);
        }

        [HttpPost]
        public ActionResult UpdateCompanyStatusByCallLog(string IsEnable, string Status, string CompanyId, string ButtonId, string PageType)
        {

            BALLeads ObjBALLeads = new BALLeads();
            string CompanyNme = CompanyId.Split('[')[0];
            string NewCompanyId = string.Empty;
            var CompanyRec = ObjBALLeads.GetCompanyIdByComapnyName(CompanyNme);

            int tempCompanyId = 0;
            tempCompanyId = AutoCompleteCompany_id(CompanyNme, PageType, CompanyId);

            if (CompanyRec != null)
            {
                NewCompanyId = CompanyRec.pkCompanyId.ToString();
            }

            if (tempCompanyId != 0)
            {
                NewCompanyId = tempCompanyId.ToString();

            }



            BALCompany ObjBALCompany = new BALCompany();
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            string Message = string.Empty;
            string CSS = string.Empty;
            string Result = string.Empty;
            ObjBALCompanyUsers = new BALCompanyUsers();
            var dbCollection = ObjBALCompanyUsers.GetUserInformationByMembershipUserId(new Guid(Membership.GetUser(HttpContext.User.Identity.Name).ProviderUserKey.ToString()));
            //byte Button_Id = Convert.ToByte(ButtonId);
            byte LeadStatus = Convert.ToByte(Status);
            if (!string.IsNullOrEmpty(NewCompanyId))
            {
                ObjBALCompany = new BALCompany();
                tblCompany ObjtblCompany = new tblCompany();

                ObjtblCompany.pkCompanyId = Convert.ToInt32(NewCompanyId);
                ObjtblCompany.LeadStatus = LeadStatus;
                ObjtblCompany.IsEnabled = bool.Parse(IsEnable);
                ObjtblCompany.LastModifiedDate = DateTime.Now;
                ObjtblCompany.LastModifiedById = dbCollection.ElementAt(0).fkUserId;
                int success = ObjBALCompany.UpdateCompanyLeadStatus(ObjtblCompany);
                if (success > 0)
                {
                }
                else
                {
                    Message = "Some error is occured";
                    CSS = "errormsg";
                }
            }

            string Comments = string.Empty;
            switch (ButtonId)
            {
                case "5":
                    Comments = "##5##";
                    break;
                case "6":
                    Comments = "##6##";
                    break;
                case "7":
                    Comments = "##7##";
                    break;
                case "8":
                    Comments = "##8##";
                    break;
                case "9":
                    Comments = "##9##";
                    break;
            }

            if (Comments != string.Empty)
            {
                Guid UserId = new Guid(dbCollection.ElementAt(0).fkUserId.ToString());
                int pkCompanyId = Convert.ToInt32(NewCompanyId.ToString());
                DateTime CreatedDate = System.DateTime.Now;
                ObjBALCompany.AddStatusToCallLog(pkCompanyId, UserId, Comments, CreatedDate);
            }
            return Json(new { Msg = Message, css = CSS, rslt = Result });

        }

        public ActionResult CallLogAutoCompleteResult(string searchText, string PageType)
        {

            BALOrders ObjOrder = new BALOrders();
           // BALOffice objbalOffice = new BALOffice();
           // MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
            Guid UserId = Utility.GetUID(User.Identity.Name);
            bool IsAdmin = true;
            if (Roles.IsUserInRole("SystemAdmin"))
            {
                IsAdmin = true;
            }
            else
            {
                IsAdmin = false;
            }

            List<string> Coll = new List<string>();

            Coll = ObjOrder.GetCallLogTop10ComapnyRecords(searchText, UserId, IsAdmin, PageType).Select(d => d.Item).ToList();
            Coll.RemoveAll(item => item == null);

           // List<AutoCompany> ObjAutoCompany = new List<AutoCompany>();
            List<string> tempColl = new List<string>();
            if (Coll.Count > 0)
            {
                for (int i = 0; i < Coll.Count; i++)
                {
                    var element = Coll[i].ToString().Split(new[] { "CompId" }, StringSplitOptions.None);
                    string Company_Name = element[1].ToString();

                    tempColl.Add(Company_Name);
                }
                Coll = tempColl;
            }

            if (Coll.Count() == 0)
            {
                Coll.Insert(0, "No Record Found");
            }
            return Json(Coll, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult AddCommentToCallLog(string CompanyId, string Comments, string SelectedMilestone, string PageType)
        {
            int Result = 0;
            BALCompany ObjBALCompany = new BALCompany();
            //string MileStone = "1";

           // BALOrders ObjBALOrders = new BALOrders();
            BALLeads ObjBALLeads = new BALLeads();
            string CompanyNme = CompanyId.Split('[')[0];
            string NewCompanyId = string.Empty;
            int pkCompanyId = 0;
            var CompanyRec = ObjBALLeads.GetCompanyIdByComapnyName(CompanyNme);
            int tempCompanyId = 0;
            tempCompanyId = AutoCompleteCompany_id(CompanyNme, PageType, CompanyId);





            if (CompanyRec != null)
            {
                NewCompanyId = CompanyRec.pkCompanyId.ToString();
                if (tempCompanyId != 0) { NewCompanyId = tempCompanyId.ToString(); }
                pkCompanyId = Convert.ToInt32(NewCompanyId);
            }
            else
            {
                NewCompanyId = CompanyId;
                if (tempCompanyId != 0) { NewCompanyId = tempCompanyId.ToString(); }
                pkCompanyId = Int32.Parse(NewCompanyId);
            }




            if (Session["CurrentUserId"] != null)
            {
                tblCompany objtblCompany = new tblCompany();
                objtblCompany.pkCompanyId = Int32.Parse(NewCompanyId);
                int Islead = 0;
                if (SelectedMilestone == "1")//past due
                {
                    Islead = 1;
                    objtblCompany.LeadStatus = 1;
                }
                else if (SelectedMilestone == "2")//past due
                {
                    Islead = 1;
                    objtblCompany.LeadStatus = 2;
                }
                else if (SelectedMilestone == "3")//past due
                {
                    Islead = 1;
                    objtblCompany.LeadStatus = 3;
                }
                else if (SelectedMilestone == "4")//past due
                {
                    Islead = 1;
                    objtblCompany.LeadStatus = 4;
                }
                else if (SelectedMilestone == "5")//past due
                {
                    Islead = 1;
                    objtblCompany.LeadStatus = 5;
                }

                else if (SelectedMilestone == "8")//past due
                {
                    Islead = 1;
                    objtblCompany.LeadStatus = 8;
                }
                else if (SelectedMilestone == "9")//InColection
                {
                    Islead = 1;
                    objtblCompany.LeadStatus = 9;
                }


                else if (SelectedMilestone == "6")//ACCOUNT in active set is enabled 1 and isactive 0
                {
                    Islead = 2;
                    objtblCompany.IsEnabled = true;
                    objtblCompany.IsActive = 1;
                }
                else if (SelectedMilestone == "7")//locked set is enabled false
                {
                    Islead = 3;
                    objtblCompany.IsEnabled = false;
                }
                else if (SelectedMilestone == "10")//ACCOUNT Active set is enabled 1 and isactive 1
                {
                    Islead = 2;
                    objtblCompany.IsEnabled = true;
                    objtblCompany.IsActive = 0;
                }

                Guid UserId = Utility.GetUID(User.Identity.Name);
                try
                {
                    Result = ObjBALCompany.AddCommentsToCallLog(pkCompanyId, UserId, Comments, DateTime.Now);
                    if (Islead == 1)
                    {
                        // int result = ObjBALCompany.UpdateCompanyLeadStatus(objtblCompany);
                    }
                    else if (Islead == 2)
                    {
                        // ObjBALCompany.SetCompanyEnableDisable(objtblCompany);
                    }
                    else
                    {
                        //ObjBALCompany.UpdateCompanyStatus(objtblCompany);
                    }

                }
                catch (Exception)
                {

                }
                finally
                {
                    ObjBALCompany = null;
                }
            }
            return Json(new { Message = Result.ToString() });

        }


        public ActionResult setProfileCallLogCompany(string CallLogCompanyId)
        {
            Session["CallLohCompanyName"] = null;
            string strResult = "";
           // int fkcompanyid = 0;
            if (!string.IsNullOrEmpty(Session["pkCompanyId"].ToString()))
            {
                //fkcompanyid = Convert.ToInt32(Session["pkCompanyId"].ToString());
                string username = (Session["CurrentUserMailId"] == null) ? string.Empty : Session["CurrentUserMailId"].ToString();
                ProfileCommon Profile = new ProfileCommon();
                Profile.SetCallLogCompanyProfile(username, CallLogCompanyId);
                strResult = "<span class='successmsg'>Info updated Successfully</span>";
            }
            return Json(new { Message = strResult });

        }

        #endregion

        #region SystemNotification

        public PartialViewResult PartialSystemNotifiication()
        {

            SystemNotificationModel ObjSystemNotificationModel = new SystemNotificationModel();
            ObjSystemNotificationModel.CollTemplateContents = GetTemplateContentsById(Convert.ToInt32(EmailTemplates.ReportsNotGenerating), true);
            if (ObjSystemNotificationModel.CollTemplateContents != null)
            {
                ObjSystemNotificationModel.SystemNotificationStatus = ObjSystemNotificationModel.CollTemplateContents.ElementAt(0).IsEnabled;
                if (ObjSystemNotificationModel.SystemNotificationStatus == true)
                {
                    //BALGeneral ObjBALGeneral = new BALGeneral();
                    ObjSystemNotificationModel.SystemNotificationContent = ObjSystemNotificationModel.CollTemplateContents.ElementAt(0).TemplateContent;

                    if (ObjSystemNotificationModel.CollTemplateContents.ElementAt(0).TemplateSubject.Trim() != "")
                    {
                        ObjSystemNotificationModel.SystemNotificationTitle = ObjSystemNotificationModel.CollTemplateContents.ElementAt(0).TemplateSubject + ":";
                    }
                    else
                    {
                        ObjSystemNotificationModel.SystemNotificationTitle = ObjSystemNotificationModel.CollTemplateContents.ElementAt(0).TemplateSubject;
                    }
                }
            }

            return PartialView("PartialSystemNotifiication", ObjSystemNotificationModel);
        }

        #endregion

        #region Email/Save/Print
        [ValidateInput(false)]
        public ActionResult SendMailForViewReport(string OrderId, string EmailTo, string EmailCC, string contents)
        {
            string Message = string.Empty;
            string CSS = string.Empty;
            bool EmailSent = false;
            string PageRequest = "";
            string MessageBody = "", emailText = "", Subject = "", Name = "", LinkLocation = "";
            string FileName = GetFileName(OrderId);
            Name = FileName.Contains('_') ? FileName.Replace('_', ' ') : FileName;
            try
            {
                BALGeneral ObjBALGeneral = new BALGeneral();
                string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;
                var emailContent = ObjBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, Convert.ToInt32(EmailTemplates.EmailedReportRequest));//"Emailed Report Request"



                Subject = emailContent.TemplateSubject.Replace("%Name%", Name);

                emailText = emailContent.TemplateContent;
                BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                #region For New Style SystemTemplate

                string Bookmarkfile = string.Empty;
                string bookmarkfilepath = string.Empty;
                Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                emailText = Bookmarkfile;
                #endregion

                string Substring = Request.Url.AbsoluteUri.Contains("&Message") ? Request.Url.AbsoluteUri.Substring(Request.Url.AbsoluteUri.IndexOf("&Message"), (Request.Url.AbsoluteUri.Length - Request.Url.AbsoluteUri.IndexOf("&Message"))) : "";

                PageRequest = Request.Url.AbsoluteUri.Contains("&Message") ? Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.Length - Substring.Length) : Request.Url.AbsoluteUri;

                #region New way Bookmark


                Bookmarks objBookmark = new Bookmarks();

                objBookmark.EmailContent = emailText;

                objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                objBookmark.Name = Name;
                objBookmark.LoginLink = "<a style='color: #87ceea;' href='" + PageRequest + "'> Click here </a>";

                MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Utility.GetUID(System.Web.HttpContext.Current.User.Identity.Name.ToString()));

                #endregion

                //MessageBody = emailText.
                //            Replace("%WebsiteLogo%", "<img src='" + WebsiteLogo + "' />").
                //            Replace("%Name%", Name).
                //            Replace("%%Link%%", "<a href='" + PageRequest + "'> Click here </a>");

                byte[] byteArray = Encoding.Default.GetBytes(GetReportHtml(contents).Replace("?", " "));
                using (MemoryStream stream = new MemoryStream(byteArray))
                {
                    EmailSent = Utility.SendMail(EmailTo.ToLower(), EmailCC.ToLower(), string.Empty, BALGeneral.GetGeneralSettings().SupportEmailId, MessageBody, Subject, new Attachment(stream, FileName + ".htm"));

                    if (EmailSent == true)
                    {
                        //ScriptManager.RegisterStartupScript(this, typeof(string), "Request", "document.location.href='" + PageRequest + "&Message=Email Sent Successfully&Type=s';", true);
                        Message = "Email Sent Successfully";
                        CSS = "successmsg";
                    }
                    else
                    {
                        // ScriptManager.RegisterStartupScript(this, typeof(string), "Request", "document.location.href='" + PageRequest + "&Message=Some error occured,please try again&Type=e';", true);
                        Message = "Some error occured,please try again";
                        CSS = "errormsg";
                    }
                }
            }
            catch// (Exception ex)
            {
                //ScriptManager.RegisterStartupScript(this, typeof(string), "Request", "document.location.href='" + PageRequest + "&Message=Some error occured,please try again&Type=e';", true);
                Message = "Some error occured,please try again";
                CSS = "errormsg";
            }

            return Json(new { Msg = Message, css = CSS });

        }

        private string GetFileName(string OrderId)
        {
            BALOrders ObjBALOrders = new BALOrders();
            List<Proc_Get_RequestAndResponseResult> ObjProc_Get_RequestAndResponseResult = ObjBALOrders.GetRequestAndResponseByOrderId(Convert.ToInt32(OrderId));
            string FirstName = ObjProc_Get_RequestAndResponseResult.First().SearchedFirstName;
            string LastName = ObjProc_Get_RequestAndResponseResult.First().SearchedLastName;
            string FileName = "";
            if (FirstName != "" && LastName != "")
            {
                FileName = FirstName + "_" + LastName;
            }
            else if (FirstName == "" && LastName != "")
            {
                FileName = LastName;
            }
            else if (FirstName != "" && LastName == "")
            {
                FileName = FirstName;
            }
            else if (FirstName == "" && LastName == "")
            {
                FileName = ObjProc_Get_RequestAndResponseResult.First().OrderNo.ToString();
            }
            return FileName;
        }

        public string GetReportHtml(string contents)
        {
            StringBuilder sb = new StringBuilder();

            try
            {
                //StringWriter stw = new StringWriter();
                //HtmlTextWriter htextw = new HtmlTextWriter(stw);
                Response.ContentEncoding = Encoding.Default;
                //test.RenderControl(htextw);


                sb.Append("<html xmlns='http://www.w3.org/1999/xhtml'>");
                sb.Append("<head>");
                sb.Append("<style type='text/css'>");
                sb.Append(GetCSS());
                sb.Append("</style>");
                sb.Append("</head>");
                sb.Append("<body>");
                string oldStr1 = "<span class=\"facetip\">";
                string oldStr2 = "<span class=\"tip\">";
                sb.Append(contents.Replace(oldStr1, "<span class=\"facetip\" style=\"display:none\"> ").Replace(oldStr2, "<span class=\"tip\" style=\"display:none\"> "));
                //sb.Append(stw.ToString().Replace("<div id=\"ncr1prestyle\">", "<pre><div id=\"ncr1prestyle\">"));

                /*sb.Append("<span id='Hiddenncr' style='display:none;'>");
                sb.Append("<pre>" + Session["STR"].ToString() + "</pre>");
                sb.Append("<span>");

                sb.Append("<script language='javascript' type='text/javascript'>");
                sb.Append("document.getElementById('report_ncr1').innerHTML = document.getElementById('Hiddenncr').innerHTML;");
                sb.Append("</script>");*/

                sb.Append("</body>");
                sb.Append("</html>");
                sb.Append("<script type='text/javascript'> if(document.getElementById('imgBigFaceimage')!=null){ document.getElementById('imgBigFaceimage').style.visibility = 'hidden';} if(document.getElementById('imgBigimage')!=null){ document.getElementById('imgBigimage').style.visibility = 'hidden';}</script>");



                string ZipRegex = @"^\{?}$";
                if (Regex.IsMatch(sb.ToString(), ZipRegex))
                {
                    // lbl.Text = "ZIP is valid!";
                }
                else
                {
                    // lbl.Text = "ZIP is invalid!";
                }
                sb = sb.Replace("/emerge/Resources/images/", ApplicationPath.GetApplicationPath() + "Resources/images/");
                sb = sb.Replace("url('../", "url('" + ApplicationPath.GetApplicationPath() + "Resources/");


                //sb = sb.Replace("&#194;", "");
                // sb = sb.Replace("&Acirc;", "");

                // string textFile = string.Format("{0}.txt", DateTime.Now.ToString("MM-dd-yyyy_HH-mm-ss"));
                // System.IO.StreamWriter streamWriter = System.IO.File.CreateText(string.Format(@"{0}{1}", System.Web.HttpContext.Current.Request.PhysicalApplicationPath, textFile));
                // streamWriter.WriteLine(sb.ToString());
                // streamWriter.Close();
            }
            catch (Exception)
            {

                throw;
            }


            return sb.ToString();

        }


        [ValidateInput(false)]
        public ActionResult AutoEmeregeSbmtMiddleName(int pkOrderId, string txtSuggestionsRR, string OrderNo, string ApplicantNameForEmail, string MiddleName, string contents)
        {
            BALEmergeReview ObjReview = new BALEmergeReview();
            int UpdatedStatus = 0;
            byte Status = 1;
            string Middle_Name = string.Empty;
            string UMiddle_Name = string.Empty;
            string Message = string.Empty;
            if (string.IsNullOrEmpty(MiddleName) == false)
            {
                Middle_Name = " This applicant has a middle name or initial on file : " + MiddleName;
                UMiddle_Name = "Middle_Name: " + MiddleName;
            }
            else
            {
                Middle_Name = MiddleName;
                UMiddle_Name = MiddleName;
            }
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                tblOrderDetail ObjOrderDetails = DX.tblOrderDetails.Where(db => db.fkOrderId == pkOrderId && db.ReviewStatus == 1).FirstOrDefault();
                tblProductPerApplication objtblProductPerApplication = DX.tblProductPerApplications.Where(db => db.pkProductApplicationId == ObjOrderDetails.fkProductApplicationId).FirstOrDefault();
                if (ObjOrderDetails != null)
                {
                    UpdatedStatus = ObjReview.UpdateReportReviewStatus(ObjOrderDetails.pkOrderDetailId, 1, txtSuggestionsRR.Trim() + UMiddle_Name, false);
                    if (UpdatedStatus == 1)
                    {

                        Status = SendAutoMailForReview(txtSuggestionsRR + Middle_Name, objtblProductPerApplication.ProductDisplayName, objtblProductPerApplication.ProductCode, OrderNo, ApplicantNameForEmail.Replace("_", " "), contents);
                    }
                    if (Status == 1)
                    {
                        Message = "<span class='SuccessMsg'>Your Middle Name has been submitted successfully.</span>";
                    }
                    else
                    {
                        Message = "<span class='ErrorMsg'>Some Error Occurred. Please Try Again.</span>";
                    }
                }
            }
            return Json(Message);
        }

        private byte SendAutoMailForReview(string txtSuggestionsRR, string HDNReportName, string HDNReportCode, string OrderNo, string ApplicantNameForEmail, string contents)
        {
            string fileName = "Reviewmail_" + DateTime.Now.ToString("yyMMdd");
            string sentMailLog = "SendAutoMailForReview for OrderNo " + OrderNo;
            BALGeneral.WriteLogForDeletion(sentMailLog, fileName);

            string emailText = "";
            string MessageBody = "";
            int PkTemplateId = 0;
            //string FileName = OrderNo + "_ReportHtml";

            StringBuilder Comments = new StringBuilder();
            Comments.Append(" <table width=\"97%\"><tr><td>Report ID : </td><td>" + OrderNo + "</td>"
                + "</tr> <tr><td>Report Name : </td><td>" + HDNReportName + "</td></tr>"
       + "<tr><td>User : </td><td>" + Session["UserFullName"].ToString() + " (<b>UserName : </b>" + User.Identity.Name + ") " + "</td></tr>"
       + " <tr><td valign=\"top\"  style=\"width:150px;\">Additional Comments :</td><td>Auto request for " + HDNReportCode + " report review." + txtSuggestionsRR.Trim() + "</td></tr>  </table>");
            string BoxHeaderText = string.Empty;
            BoxHeaderText = HDNReportCode + " Auto-Review Request - " + ApplicantNameForEmail;
            BALGeneral ObjBALGeneral = new BALGeneral();
            string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/emerge-logo--no-shadows-(small).gif";
            PkTemplateId = Convert.ToInt32(EmailTemplates.EmergeReview);
            //For email status//
           // BALGeneral objbalgenral = new BALGeneral();
           // MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
            //Guid UserId = Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
           // int pktemplateid = PkTemplateId;
            //Guid user_ID = UserId;
            int? fkcompanyuserid = null;
           // bool email_status = true; // objbalgenral.UserEmailStatusCheck(user_ID, pktemplateid, fkcompanyuserid); 


            var emailContent = ObjBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, PkTemplateId);
            emailText = emailContent.TemplateContent;

            BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
            #region  For New Style SystemTemplate
            string Bookmarkfile = string.Empty;
            string bookmarkfilepath = string.Empty;

            Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
            Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
            Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
            emailText = Bookmarkfile;
            #endregion


            #region New way Bookmark


            Bookmarks objBookmark = new Bookmarks();

            objBookmark.EmailContent = emailText;

            objBookmark.Subject = BoxHeaderText;
            if (ApplicantNameForEmail != null)
            {
                objBookmark.ApplicantName = ApplicantNameForEmail;/* Session["UserFullName"].ToString();*/
            }
            objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
            objBookmark.UserCompany = ApplicantNameForEmail;
            objBookmark.UserEmail = Session["UserFullName"].ToString() + " (<b>UserName : </b>" + User.Identity.Name + ") ";
            objBookmark.EmergeAssistComment = Comments.ToString();
            objBookmark.EmergeReviewComment = Comments.ToString();
            objBookmark.Suggestion = Comments.ToString();

            MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Utility.GetUID(User.Identity.Name.ToString()));

            #endregion
            //For email status//
            string MsgBeforeEmail = "Emerge Reviewed for Order No:-" + OrderNo;
            string UserIdentityName = User.Identity.Name;
            WriteReviewStatusInTextFile(MsgBeforeEmail, UserIdentityName);

            string successAdmin = string.Empty;
            sentMailLog = "BALEmergeReview.SendMail for OrderNo " + OrderNo;
            BALGeneral.WriteLogForDeletion(sentMailLog, fileName);


            successAdmin = BALEmergeReview.SendMail(ConfigurationManager.AppSettings["ReportReviewMail"].ToString(), UserIdentityName, string.Empty, ConfigurationManager.AppSettings["ReportReviewFromAddress"].ToString(), MessageBody, BoxHeaderText);

            if (successAdmin == "Success")
            {
                sentMailLog = "BALEmergeReview.SendMail for Success OrderNo " + OrderNo;
                BALGeneral.WriteLogForDeletion(sentMailLog, fileName);
                string SucessMsg = "Email Successfully Sent for Order No:-" + OrderNo;
                WriteReviewStatusInTextFile(SucessMsg, ConfigurationManager.AppSettings["ReportReviewMail"].ToString());
                return 1;
            }
            else
            {
                sentMailLog = "BALEmergeReview.SendMail for Failed OrderNo " + OrderNo;
                BALGeneral.WriteLogForDeletion(sentMailLog, fileName);
                string UnSucessMsg = "Email Sending Failed for Order No:-" + OrderNo;
                UnSucessMsg = UnSucessMsg + " Error:" + successAdmin;
                WriteReviewStatusInTextFile(UnSucessMsg, ConfigurationManager.AppSettings["ReportReviewMail"].ToString());
                return 2;
            }
        }

        public void WriteReviewStatusInTextFile(string ReviewMsg, string UserIdentityName)
        {
            try
            {
               // List<string> strlst = new List<string>();
                StreamWriter SW;
                string savedPath = string.Empty;
                string fileName = string.Empty;
                DateTime dt = DateTime.Now;
                fileName = dt.ToString("yyMMdd");
                savedPath = HttpContext.Server.MapPath("~/Resources/Upload/EmergeReviewLog/" + fileName + ".txt");
                if (!System.IO.File.Exists(savedPath))
                {
                    savedPath = HttpContext.Server.MapPath("~/Resources/Upload/EmergeReviewLog/" + fileName + ".txt");
                    using(SW = System.IO.File.CreateText(savedPath))
                    {
                        SW.WriteLine("\n");
                        SW.WriteLine("****************************************");
                        SW.WriteLine(ReviewMsg);
                        SW.WriteLine("Emerge Review Order Identity:- " + UserIdentityName);
                        SW.WriteLine("Time of Event :- " + DateTime.Now.ToString());
                        SW.WriteLine("****************************************");
                        SW.Close();
                    }
                }
                else
                {
                    using(SW = System.IO.File.AppendText(savedPath))
                    {
                        SW.WriteLine("\n");
                        SW.WriteLine("****************************************");
                        SW.WriteLine(ReviewMsg);
                        SW.WriteLine("Emerge Review Order Identity:- " + UserIdentityName);
                        SW.WriteLine("Time of Event :- " + DateTime.Now.ToString());
                        SW.WriteLine("****************************************");
                        SW.Close();
                    }
                }               
            }
            catch { }
        }



        public string GetCSS()
        {
            string Css_Code = "";
            string CssPath = Server.MapPath("~/Content/Css/Report.css");
            string CssPath_Style = Server.MapPath("~/Content/Css/Report_Style.css");
            if (System.IO.File.Exists(CssPath))
            {
                Css_Code = System.IO.File.OpenText(CssPath).ReadToEnd();
                //if (hdnIsNullDobHide.Value == "1")
                //{
                //    Css_Code += "#trNullDOBHeading{ display:none;} #trNullDOBRecords { display:none;} ";
                //}
            }
            if (System.IO.File.Exists(CssPath_Style))
            {
                Css_Code = Css_Code + System.IO.File.OpenText(CssPath_Style).ReadToEnd();
            }
            return Css_Code;

        }
        [ValidateInput(false)]
        public ActionResult SaveViewReport(string OrderId, string ReportName, string contents)
        {
            try
            {
                string filename = "";
                if (!string.IsNullOrEmpty(ReportName))
                {
                    filename = ReportName;
                }
                else
                {
                    filename = "Report";
                }
                System.Text.Encoding enc = System.Text.Encoding.ASCII;
                byte[] myByteArray = enc.GetBytes(GetReportHtml(contents));
                //string attachment = "attachment; filename=" + filename + ".html";
                //System.Web.HttpContext.Current.Response.ClearContent();
                //System.Web.HttpContext.Current.Response.AddHeader("content-disposition", attachment);
                //System.Web.HttpContext.Current.Response.ContentType = "application/html";
                //System.Web.HttpContext.Current.Response.Write(GetReportHtml());
                //System.Web.HttpContext.Current.Response.End();
                //string fileext = System.IO.Path.GetExtension(FilePath);
                System.Web.HttpContext.Current.Response.ContentType = "application/html";
                System.Web.HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename + ".html");
                System.Web.HttpContext.Current.Response.BinaryWrite(myByteArray);
                System.Web.HttpContext.Current.Response.End();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Json(new { });
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SaveFile(FormCollection frm)
        {
            string contents = frm["hdData"];
            string filename = "";
            byte[] file = { };
            try
            {
                if (!string.IsNullOrEmpty(frm["hdFile"]))
                {
                    filename = frm["hdFile"] != null ? frm["hdFile"].Replace(" ", "_") : string.Empty;
                }
                else
                {
                    filename = "Report";
                }
                System.Text.Encoding enc = System.Text.Encoding.ASCII;
                //contents = contents.Replace("/emerge/Resources/images/", ApplicationPath.GetApplicationPath() + "Content/themes/base/images/");
                byte[] myByteArray = enc.GetBytes(GetReportHtml(contents));
                file = myByteArray;
            }
            catch //(Exception ex)
            {
            }

            return File(file, "application/html", filename + ".html");

        }



        [HttpPost]
        [ValidateInput(false)]
        public ActionResult GetFileForPrint(string OID, string Contents)
        {
            string filename = "";
            //byte[] file = { };
            try
            {
                System.Text.Encoding enc = System.Text.Encoding.ASCII;
                byte[] myByteArray = enc.GetBytes(GetReportHtml(Contents));
                filename = DateTime.Now.ToFileTime() + ".htm";
                string path = Server.MapPath("~/Resources/Upload/Temp/" + filename);
                System.IO.File.WriteAllBytes(path, myByteArray);
                filename = ApplicationPath.GetApplicationPath() + "Resources/Upload/Temp/" + filename;


            }
            catch //(Exception ex)
            {
            }
            return Json(new { File = filename });
        }

        public void RedirectBack(string RR, string bck)
        {

            int IsReportForReview = 0, ComeFromDashBoard = 0;
            if (!string.IsNullOrEmpty(Request.QueryString["RR"]))
            {
                IsReportForReview = Convert.ToInt32(Request.QueryString["RR"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["_bck"]))
            {
                ComeFromDashBoard = Convert.ToInt32(Request.QueryString["_bck"]);
            }
            if (Request.Url.AbsoluteUri.Contains("EndUser"))
            {
                Response.Redirect("~/EndUser/SavedReports?_bck=1");
            }
            else if (Request.Url.AbsoluteUri.Contains("Branch"))
            {
                Response.Redirect("~/Branch/SavedReports?_bck=1");
            }
            else if (Request.Url.AbsoluteUri.Contains("Corporate"))
            {
                Response.Redirect("~/Corporate/SavedReports?_bck=1");
            }
            else if (Request.Url.AbsoluteUri.Contains("Control") && IsReportForReview != 1)
            {
                if (ComeFromDashBoard == 2)
                { Response.Redirect("~/Control/AdminSearch?_bck=1"); }
                Response.Redirect("~/Control/SavedReports?_bck=1");
            }
            else if (Request.Url.AbsoluteUri.Contains("Control") && IsReportForReview == 1)
            {
                Response.Redirect("~/Control/AdminSearch?_bck=1");
            }
            else if (Request.Url.AbsoluteUri.Contains("Control") && ComeFromDashBoard == 2)
            {
                Response.Redirect("~/Control/AdminSearch?_bck=1");
            }
            else
            {
                Response.Redirect("~/Control/SavedReports?_bck=1");
            }
        }
        #endregion

        #region AdverdeLetterWriteToPdf
        //[ValidateInput(false)]
        public ActionResult SendMailWithAdverdeLetter(string OrderId, string EmailTo, string EmailCC, string EnteredCompany)
        {
            string Message = string.Empty;
            string CSS = string.Empty;
            bool EmailSent = false;
            string MessageBody = "", emailText = "", Subject = "", Name = "";
            string FileName = GetFileName(OrderId);
            Name = FileName.Contains('_') ? FileName.Replace('_', ' ') : FileName;
            try
            {
                BALGeneral ObjBALGeneral = new BALGeneral();
                //string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;
                var emailContent = ObjBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, Convert.ToInt32(EmailTemplates.AdverseLetter));//"Emailed Report Request"


                #region For New Style SystemTemplate
                string Bookmarkfile = string.Empty;
                string bookmarkfilepath = string.Empty;
                BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
               // string logopth = ApplicationPath.GetApplicationPath() + "Content/themes/base/images/emerge_login.png";
               // proc_Get_UserAllInfoResult userinfo = objTemplateBookmarks.GetUserInfo(Utility.GetUID(User.Identity.Name));
                Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                emailContent.TemplateContent = Bookmarkfile;
                #endregion
                // Subject = emailContent.TemplateSubject.Replace("%Name%", Name);
                Subject = "Adverse Action Letter";
                emailText = emailContent.TemplateContent.Replace("%Suggestion%", "Hello,<p>Attached is an Adverse Action Letter. Please download and review the documents. If you have any questions, please contact Intelifi at 888.409.1819. Thank you.</p>").Replace("%Subject%", "Adverse Action Letter");//.Replace("%WebsiteLogo%", "<img src='" + WebsiteLogo + "' />");
                MessageBody = emailText;
                string PdfFile = GeneratePdfOfAdverdeLetter(OrderId, EnteredCompany);
                byte[] byteArray = System.IO.File.ReadAllBytes(Server.MapPath("~/Resources/Upload/AdversePDF/" + PdfFile));// Encoding.Default.GetBytes("");
                using (MemoryStream stream = new MemoryStream(byteArray))
                {
                    EmailSent = Utility.SendMail(EmailTo.ToLower(), EmailCC.ToLower(), string.Empty, BALGeneral.GetGeneralSettings().SupportEmailId, MessageBody, Subject, new Attachment(stream, PdfFile));
                }
                if (EmailSent == true)
                {
                    //ScriptManager.RegisterStartupScript(this, typeof(string), "Request", "document.location.href='" + PageRequest + "&Message=Email Sent Successfully&Type=s';", true);
                    Message = "Email Sent Successfully";
                    CSS = "successmsg";
                }
                else
                {
                    // ScriptManager.RegisterStartupScript(this, typeof(string), "Request", "document.location.href='" + PageRequest + "&Message=Some error occured,please try again&Type=e';", true);
                    Message = "Some error occured,please try again";
                    CSS = "errormsg";
                }
            }
            catch //(Exception ex)
            {
                //ScriptManager.RegisterStartupScript(this, typeof(string), "Request", "document.location.href='" + PageRequest + "&Message=Some error occured,please try again&Type=e';", true);
                Message = "Some error occured,please try again";
                CSS = "errormsg";
            }

            return Json(new { Msg = Message, css = CSS });

        }


        public ActionResult SendMailWithPreAdverdeLetter(string OrderId, string ApplicantName, string EmailTo, string EmailCC, string EnteredCompany)
        {
            string Message = string.Empty;
            string CSS = string.Empty;
            bool EmailSent = false;
            string MessageBody = "", emailText = "", Subject = "", Name = "";
            string FileName = GetFileName(OrderId);
            Name = FileName.Contains('_') ? FileName.Replace('_', ' ') : FileName;
            try
            {
                BALGeneral ObjBALGeneral = new BALGeneral();
                //string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;
                var emailContent = ObjBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, Convert.ToInt32(EmailTemplates.AdverseLetter));//"Emailed Report Request"
                emailContent.TemplateName = "Pre Adverse Letter";
                emailContent.TemplateSubject = "Pre Adverse Letter";

                #region For New Style SystemTemplate
                string Bookmarkfile = string.Empty;
                string bookmarkfilepath = string.Empty;
                BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
               // string logopth = ApplicationPath.GetApplicationPath() + "Content/themes/base/images/emerge_login.png";
                //proc_Get_UserAllInfoResult userinfo = objTemplateBookmarks.GetUserInfo(Utility.GetUID(User.Identity.Name));
                Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                emailContent.TemplateContent = Bookmarkfile;
                #endregion
                // Subject = emailContent.TemplateSubject.Replace("%Name%", Name);
                Subject = "Pre Adverse Action Letter";
                emailText = emailContent.TemplateContent.Replace("%Suggestion%", "Hello,<p>Attached is Pre Adverse Action Letter. Please download and review the documents. If you have any questions, please contact Intelifi at 888.409.1819. Thank you.</p>").Replace("%Subject%", "Pre Adverse Action Letter");//.Replace("%WebsiteLogo%", "<img src='" + WebsiteLogo + "' />");
                MessageBody = emailText;
                //BALCompany ObjBALCompany = new BALCompany();
                BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
                MembershipUser Member = Membership.GetUser(User.Identity.Name);
                string Company_name = string.Empty;
                string Address = string.Empty;
                string contactNo = string.Empty;
                if (Member != null)
                {
                    var dbCollection = ObjBALCompanyUsers.GetUserCompanyInfo(new Guid(Member.ProviderUserKey.ToString()));
                    if (dbCollection.Count > 0)
                    {
                        Company_name = dbCollection.First().CompanyName.ToString();
                        Address = dbCollection.First().CompanyName.ToString();
                        contactNo = dbCollection.First().PhoneNo1.ToString();
                    }
                }
                string pdfFileName = string.Empty;
                string CompanyName = EnteredCompany != "" ? EnteredCompany : Company_name;
                string Recipient = ApplicantName;
                CompanyName = CompanyName + "_" + contactNo;
                pdfFileName = ObjBALGeneral.Confidentialform(CompanyName, Recipient, Address);
                string PdfFile = pdfFileName;
                byte[] byteArray = System.IO.File.ReadAllBytes(Server.MapPath("~/Resources/Upload/AdversePDF/" + PdfFile));// Encoding.Default.GetBytes("");
                using (MemoryStream stream = new MemoryStream(byteArray))
                {
                    EmailSent = Utility.SendMail(EmailTo.ToLower(), EmailCC.ToLower(), string.Empty, BALGeneral.GetGeneralSettings().SupportEmailId, MessageBody, Subject, new Attachment(stream, PdfFile));

                    if (EmailSent == true)
                    {
                        //ScriptManager.RegisterStartupScript(this, typeof(string), "Request", "document.location.href='" + PageRequest + "&Message=Email Sent Successfully&Type=s';", true);
                        Message = "Email Sent Successfully";
                        CSS = "successmsg";
                    }
                    else
                    {
                        // ScriptManager.RegisterStartupScript(this, typeof(string), "Request", "document.location.href='" + PageRequest + "&Message=Some error occured,please try again&Type=e';", true);
                        Message = "Some error occured,please try again";
                        CSS = "errormsg";
                    }
                }
            }
            catch //(Exception ex)
            {
                //ScriptManager.RegisterStartupScript(this, typeof(string), "Request", "document.location.href='" + PageRequest + "&Message=Some error occured,please try again&Type=e';", true);
                Message = "Some error occured,please try again";
                CSS = "errormsg";
            }

            return Json(new { Msg = Message, css = CSS });

        }



        [HttpPost]
        public ActionResult SaveAdverdeLetter(FormCollection frm)
        {
            string Applicantname = string.Empty;
            string PdfFile = string.Empty;
            string filename = !string.IsNullOrEmpty(frm["hdAdverseLtrFile"]) ? frm["hdAdverseLtrFile"].Replace(" ", "_") : string.Empty;
            Applicantname = frm["hdAppname"];

            byte[] file = { };
            if (string.IsNullOrEmpty(Applicantname) == false)
            {
                PdfFile = Generate_Confidential_form(Applicantname, frm["hdAdverseLtrCompany"]);
            }
            else
            {
                PdfFile = GeneratePdfOfAdverdeLetter(frm["hdAdverseLtrOID"], frm["hdAdverseLtrCompany"]);
            }
            try
            {
                //System.Text.Encoding enc = System.Text.Encoding.ASCII;
                byte[] myByteArray = System.IO.File.ReadAllBytes(Server.MapPath("~/Resources/Upload/AdversePDF/" + PdfFile));
                file = myByteArray;
            }
            catch //(Exception ex)
            {
            }

            return File(file, "application/pdf", filename + ".pdf");

        }
        #endregion

        #region AdverdeLetterWriteToPdf
        public ActionResult AdverdeLetterWriteToPdf(string OrderId, string EnteredCompany)
        {
            string pdfFileName = string.Empty;
            pdfFileName = GeneratePdfOfAdverdeLetter(OrderId, EnteredCompany);
            return Json(new { FileName = pdfFileName });
        }

        public string GeneratePdfOfAdverdeLetter(string OrderId, string EnteredCompany)
        {
            BALCompany ObjBALCompany = new BALCompany();
            BALOrders ObjBALOrders = new BALOrders();
           // string UserName = string.Empty;
            string CompanyName = string.Empty;
           // string Address = string.Empty;
            string ApplicantName = string.Empty;
            string contactNo = string.Empty;
            List<Proc_Get_RequestAndResponseResult> ObjProc_Get_RequestAndResponseResult = new List<Proc_Get_RequestAndResponseResult>();
            Proc_Get_RequestAndResponseResult ObjRequestAndResponseResult = new Proc_Get_RequestAndResponseResult();
            List<Proc_Get_CompanyDetailByMemberShipIdResult> ObjCompanyDetails = new List<Proc_Get_CompanyDetailByMemberShipIdResult>();
            Proc_Get_CompanyDetailByMemberShipIdResult ObjCompanyDetailByMemberShipIdResult = new Proc_Get_CompanyDetailByMemberShipIdResult();

            if (!string.IsNullOrEmpty(OrderId))
            {
                ObjProc_Get_RequestAndResponseResult = ObjBALOrders.GetRequestAndResponseByOrderId(Convert.ToInt32(OrderId));
                if (ObjProc_Get_RequestAndResponseResult.Count > 0)
                {
                    ObjRequestAndResponseResult = ObjProc_Get_RequestAndResponseResult.First();
                    ObjCompanyDetails = ObjBALCompany.GetCompanyDetailByMemberShipIdResult(ObjRequestAndResponseResult.fkUserId);
                    if (ObjCompanyDetails.Count > 0)
                    {
                        ObjCompanyDetailByMemberShipIdResult = ObjCompanyDetails.First();

                        //UserName = ObjCompanyDetailByMemberShipIdResult.lastname + ", " + ObjCompanyDetailByMemberShipIdResult.firstname;
                        CompanyName = ObjCompanyDetailByMemberShipIdResult.CompanyName;
                       // Address = ObjCompanyDetailByMemberShipIdResult.city + ", " + ObjCompanyDetailByMemberShipIdResult.statecode;
                        ApplicantName = GenerateApplicantName(ObjRequestAndResponseResult.SearchedLastName, ObjRequestAndResponseResult.SearchedSuffix, ObjRequestAndResponseResult.SearchedMiddleInitial, ObjRequestAndResponseResult.SearchedFirstName); ;

                        var dbCollection = new BALCompanyUsers().GetUserCompanyInfo(ObjRequestAndResponseResult.fkUserId);
                        if (dbCollection.Count > 0)
                        {
                            contactNo = dbCollection.First().PhoneNo1.ToString();
                        }
                    }
                }
            }
            if (EnteredCompany != "")
            {
                CompanyName = EnteredCompany;
            }

            string pdfFileName = string.Empty;
            //MemoryStream ObjMemoryStream = new MemoryStream();
            iTextSharp.text.Rectangle ObjPageSize = new iTextSharp.text.Rectangle(PageSize.A4);
            Document ObjDocument = new Document(ObjPageSize, 55, 55, 15, 12);
            float Leading = 13;
            DateTime DateToBePrint = DateTime.Now;

            try
            {

                pdfFileName = "Adverse_File" + DateTime.Now.ToFileTime().ToString() + ".pdf";
                //ViewState["pdfFileName"] = pdfFileName;
                DirectoryInfo dir = null;
                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\AdversePDF")))
                {
                    Directory.CreateDirectory(Server.MapPath(@"~\Resources\Upload\AdversePDF"));
                }
                else
                {

                    dir = new DirectoryInfo(Server.MapPath(@"~\Resources\Upload\AdversePDF"));
                    FileSystemInfo[] info = dir.GetFiles();
                    if (info.Length > 0)
                    {
                        foreach (FileInfo fi in info)
                        {
                            if (fi.Name.Contains(""))
                            {
                                string FilePath = Server.MapPath(@"~\Resources\Upload\AdversePDF\") + fi.Name;
                                if (System.IO.File.Exists(FilePath))
                                {
                                    try
                                    {
                                        System.IO.File.Delete(FilePath);
                                    }
                                    catch //(Exception ex)
                                    {
                                        // ListErrors.Add("File Used By Another Process : " + ex.Message.ToString());
                                    }

                                }
                            }
                        }
                    }
                }

                string path = Request.PhysicalApplicationPath + "Resources\\Upload\\AdversePDF\\" + pdfFileName;
                PdfWriter.GetInstance(ObjDocument, new FileStream(path, FileMode.Create));


                iTextSharp.text.Font FontInfoHeader = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 10, iTextSharp.text.Font.BOLD, iTextSharp.text.Color.BLACK);
                iTextSharp.text.Font FontInfoNormal = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.NORMAL, iTextSharp.text.Color.BLACK);
                iTextSharp.text.Font FontInfoHeadings = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 12, iTextSharp.text.Font.BOLD, iTextSharp.text.Color.BLACK);
                iTextSharp.text.Font FontInfoTotal = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 10, iTextSharp.text.Font.BOLD, iTextSharp.text.Color.BLACK);

                #region for blankspace
                Chunk spaceChunk = new Chunk("                                                                                                                                                     ");
                #endregion
                HeaderFooter ObjPdfFooter = new HeaderFooter(new Phrase(" ", FontInfoNormal), new Phrase(spaceChunk + DateToBePrint.ToString("MMMM dd, yyyy hh:mm tt"), FontInfoNormal));
                ObjDocument.Footer = ObjPdfFooter;
                ObjPdfFooter.Alignment = Element.ALIGN_LEFT;
                ObjDocument.Open();
                iTextSharp.text.Table TblMain = new iTextSharp.text.Table(1);
                TblMain.Width = 100;
                TblMain.Padding = 0;
                TblMain.Cellpadding = 2;
                TblMain.Cellspacing = 1;
                TblMain.DefaultCell.Border = Rectangle.NO_BORDER;
                TblMain.CellsFitPage = true;
                TblMain.TableFitsPage = true;

                iTextSharp.text.Table TblMain1 = new iTextSharp.text.Table(1);
                TblMain1.Width = 100;
                TblMain1.Padding = 0;
                TblMain1.Cellpadding = 2;
                TblMain1.Cellspacing = 1;
                TblMain1.DefaultCell.Border = Rectangle.NO_BORDER;
                TblMain1.CellsFitPage = true;
                TblMain1.TableFitsPage = true;
                Phrase op = new Phrase();
                Cell ocell = new Cell(op);
                //Chunk ock = new Chunk();

                //string imageFilePath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Content\\themes\\base\\images\\usaintelPowerToKnow.jpg";
                //iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imageFilePath);
                //Chunk ch = new Chunk(image, 0, 0);
                //ocell = new Cell();
                //op = new Phrase(Leading, ch);
                //ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                //ocell.BackgroundColor = new Color(255, 255, 255);
                //ocell.Add(op); TblMain.AddCell(ocell);

                Cell newLineSpace1 = new Cell();
                Phrase phraseLineSpace1 = new Phrase(Leading, "\n\n\n", FontInfoNormal);
                newLineSpace1.HorizontalAlignment = Element.ALIGN_CENTER;
                newLineSpace1.BackgroundColor = new Color(255, 255, 255);
                newLineSpace1.Add(phraseLineSpace1);
                TblMain.AddCell(newLineSpace1);


                Cell ocellHeading = new Cell();
                Phrase opHeading = new Phrase(Leading, "Adverse Action Letter", FontInfoHeader);
                ocellHeading.HorizontalAlignment = Element.ALIGN_CENTER;
                ocellHeading.BackgroundColor = new Color(255, 255, 255);
                ocellHeading.Add(opHeading);
                TblMain.AddCell(ocellHeading);


                Cell newLineSpace2 = new Cell();
                Phrase phraseLineSpace2 = new Phrase(Leading, "\n\n", FontInfoNormal);
                newLineSpace2.HorizontalAlignment = Element.ALIGN_CENTER;
                newLineSpace2.BackgroundColor = new Color(255, 255, 255);
                newLineSpace2.Add(phraseLineSpace2);
                TblMain.AddCell(newLineSpace2);


                Cell ocellDate = new Cell();
                Phrase opDate = new Phrase(Leading, DateTime.Now.ToString("dd-MMM-yyyy"), FontInfoNormal);
                ocellDate.HorizontalAlignment = Element.ALIGN_LEFT;
                ocellDate.BackgroundColor = new Color(255, 255, 255);
                ocellDate.Add(opDate);
                TblMain.AddCell(ocellDate);



                ocell = new Cell();
                op = new Phrase(Leading, "Dear " + ApplicantName + ":", FontInfoNormal);
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain.AddCell(ocell);

                Cell newLineSpace3 = new Cell();
                Phrase phraseLineSpace3 = new Phrase(Leading, "\n", FontInfoNormal);
                newLineSpace3.HorizontalAlignment = Element.ALIGN_CENTER;
                newLineSpace3.BackgroundColor = new Color(255, 255, 255);
                newLineSpace3.Add(phraseLineSpace3);
                TblMain.AddCell(newLineSpace3);


                string paragraph = string.Empty;
                #region Main Paragraph
                paragraph = "";

                paragraph = "       No less than five (5) business days prior to " + DateTime.Now.ToString("dd-MMM-yyyy") + ", we sent you a notice of our "
                + "intent to take adverse action against you based, in whole or in part, on information furnished "
                + "in a consumer report by Intelifi, Inc.  We have now taken that adverse action.\n\n"
                + "       You may contact Intelifi to dispute the contents of your file or this report. "
                + "Intelifi is located at: 8730 Wilshire Blvd., 4th Floor, Ste. 412, Beverly Hills, CA  90211. "
                + "Their telephone number for consumers is:  (888) 409-1819.   Intelifi cannot tell you why the adverse "
                + "action was made.    Although you have been provided a copy of your report, you are still entitled "
                + "to another free copy of your report within 60 days of the receipt of this notice upon which we based our decision. "
                + "You also have the right to contact the agency which furnished the report and dispute the accuracy or completeness "
                + "of the report and the agency will investigate your dispute.\n\n"
                + "       We had previously provided to you a copy of the report in question along with a copy of your rights under the Fair "
                + "Credit Reporting Act, the federal law that governs this report. "
                + " If you have lost either or both of these, please contact our office and we will provide a free replacement copy.\n\n";

                #endregion


                iTextSharp.text.Font FontInfoNormal1 = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.NORMAL, iTextSharp.text.Color.BLACK);


                ocell = new Cell();
                op = new Phrase(Leading, paragraph, FontInfoNormal1);
                ocell.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain.AddCell(ocell);


                ocell = new Cell();
                op = new Phrase(Leading, "\n\n", FontInfoNormal);
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain.AddCell(ocell);


                ocell = new Cell();
                op = new Phrase(Leading, "                                                                                                          Very truly yours,", FontInfoNormal);

                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain.AddCell(ocell);


                string companyAndcontact = CompanyName + " | " + contactNo;

                ocell = new Cell();
                op = new Phrase(Leading, "                                                                                                          " + companyAndcontact, FontInfoNormal);
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain.AddCell(ocell);

                Cell newLineSpace4 = new Cell();
                Phrase phraseLineSpace4 = new Phrase(Leading, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", FontInfoNormal);
                newLineSpace4.HorizontalAlignment = Element.ALIGN_CENTER;
                newLineSpace4.BackgroundColor = new Color(255, 255, 255);
                newLineSpace4.Add(phraseLineSpace4);
                TblMain.AddCell(newLineSpace4);


               // string str1 = string.Empty;

                #region Para1

                //str1 = "Para informacion en espanol, visite www.consumerfinance.gov/learnmore o escribe a "
                //    + "la\nConsumer Financial Protection Bureau, 1700 G Street N.W., "
                //    + "Washington, D.C. 20006. \n";

                #endregion
                iTextSharp.text.Font FontInfoItalic = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.ITALIC, iTextSharp.text.Color.BLACK);
                iTextSharp.text.Font FontInfoBoldUnderLine = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.UNDERLINE, iTextSharp.text.Color.BLACK);
                iTextSharp.text.Font FontInfoBold = new iTextSharp.text.Font(iTextSharp.text.Font.HELVETICA, 9, iTextSharp.text.Font.BOLD, iTextSharp.text.Color.BLACK);

                //ocell = new Cell();
                //op = new Phrase(Leading, str1, FontInfoItalic);
                //ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                //ocell.BackgroundColor = new Color(255, 255, 255);
                //ocell.Add(op);
                //TblMain.AddCell(ocell);



                //ocell = new Cell();
                //op = new Phrase(Leading, "\n\n\n\n\n\n\n\n\n", FontInfoBoldUnderLine);
                //ocell.HorizontalAlignment = Element.ALIGN_CENTER;
                //ocell.BackgroundColor = new Color(255, 255, 255);
                //ocell.Add(op);
                //TblMain.AddCell(ocell);



                ocell = new Cell();
                op = new Phrase(Leading, "A Summary of Your Rights Under the Fair Credit Reporting Act", FontInfoBoldUnderLine);
                ocell.HorizontalAlignment = Element.ALIGN_CENTER;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain1.AddCell(ocell);



                ocell = new Cell();
                op = new Phrase(Leading, str21(), FontInfoNormal);
                op.Leading = 13;
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain1.AddCell(ocell);

                ocell = new Cell();
                op = new Phrase(Leading, str22(), FontInfoNormal);
                op.Leading = 13;
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain1.AddCell(ocell);

                ocell = new Cell();
                op = new Phrase(Leading, str23(), FontInfoNormal);
                op.Leading = 13;
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain1.AddCell(ocell);

                ocell = new Cell();
                op = new Phrase(Leading, str3(), FontInfoNormal);
                op.Leading = 15;
                ocell.HorizontalAlignment = Element.ALIGN_LEFT;
                ocell.BackgroundColor = new Color(255, 255, 255);
                ocell.Add(op);
                TblMain1.AddCell(ocell);



                iTextSharp.text.Table ObjPdfTblMainBottom = new iTextSharp.text.Table(2);
                ObjPdfTblMainBottom.SetWidths(new int[2] { 60, 40 });
                ObjPdfTblMainBottom.BorderColor = new Color(191, 217, 235);
                ObjPdfTblMainBottom.Border = 2;
                ObjPdfTblMainBottom.BorderColor = new Color(System.Drawing.Color.Red);

                Phrase TPhrase = new Phrase();
                Cell TCell = new Cell(TPhrase);

                TCell = new Cell();
                TPhrase = new Phrase(Leading, "TYPE OF BUSINESS:", FontInfoBold);
                TPhrase.Leading = 15;
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                TCell = new Cell();
                TPhrase = new Phrase(Leading, "CONTACT:", FontInfoBold);
                TPhrase.Leading = 15;
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTL1 = string.Empty;
                strTL1 = "1.a. Banks, saving associations, and credit unions with total assets of over $10 billion and their"
                + " affiliates."
                + "\nb. Such affiliates that are not banks,saving associations, or credit unions also should list, in"
                + " addition to the Bureau:";
                TCell = new Cell();
                TPhrase = new Phrase(13, strTL1, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTR1 = string.Empty;
                strTR1 = "a. Bureau of Consumer Financial Protection"
                + "\n1700 G Street NW"
                + "\nWashington, DC 20006"
                + "\nb. Federal Trade Commission: ConsumerResponse Center-"
                + "\nFCRA"
                + "\nWashington, DC 20580"
                + "\n(877)382-4857";

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, strTR1, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTL2 = string.Empty;
                strTL2 = "2. To the extent not included in item 1 above:"
                + "\na. National Banks, federal saving associations, and federal branches ad federal agencies of"
                + "foreign banks"
                + "\nb. State member banks, branches and agencies of foreign banks( other than federal branches,"
                + " federal agencies and insured state branches of foreign banks), commercial leading companies"
                + " owned or controlled by foreign banks, and organizations operating under section 25 or 25A of"
                + " the Federal Reserve Act"
                + "\nc. Nonmember Insured Banks, Insured State Branches of Foreign banks and insured state"
                + " saving organizations"
                + "\nd. Federal Credit Unions";
                TCell = new Cell();
                TPhrase = new Phrase(13, strTL2, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTR2 = string.Empty;
                strTR2 = "a. Office of Comptroller of the Currency"
                + "\nCustomer Assistance Group"
                + "\n1301 McKinnney Street, Suite 3450"
                + "\nHouston, TX 77010-9050"
                + "\nb. Federal Reserve Consumer Help Center"
                + "\nPO Box 1200"
                + "\nMinneapolis, MN55480"
                + "\nc. FDIC Consumer Response Center"
                + "\n1100 Walnut Street, Box #11"
                + "\nKansas City, MO 64106"
                + "\nd. National Credit Union Administrator"
                + "\nOffice of Consumer Protection(OCP)"
                + "\nDivision of Consumer Compliance and Outreach(DCCO)"
                + "\n1775 Duke Street"
                + "\nAlexandria, VA 22314";

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, strTR2, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                TCell = new Cell();
                TPhrase = new Phrase(13, "3. Air carriers", FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTR3 = string.Empty;
                strTR3 = "Asst.General Counsel for Aviation Enforcement & Proceeding"
                + "\n Department ofTransportation"
                + "\n400 Seventh Street SW"
                + "\nWashington, DC 20590";

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, strTR3, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);


                TCell = new Cell();
                TPhrase = new Phrase(13, "4. Creditors Subject to Surface Transportation Board", FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTR4 = string.Empty;
                strTR4 = "Office of Proceedings, Surface Transportation Board"
                + "\nDepartment of Transportation"
                + "\n1925 K Street NW"
                + "\nWashington, DC 20423";

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, strTR4, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);


                TCell = new Cell();
                TPhrase = new Phrase(13, "5. Creditors Subject to Packers and Stockyards Act", FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTR5 = string.Empty;
                strTR5 = "Nearest packers and Stockyards Administration area supervisor";

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, strTR5, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                TCell = new Cell();
                TPhrase = new Phrase(13, "6. Small Business Investment Companies", FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTR6 = string.Empty;
                strTR6 = "Associate Deputy Administrator for CapitalAccess"
                + " United States Small Business Administration"
                + " 406 Third Street, SW, 8TH Floor"
                + " Washington, DC 20416";

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, strTR6, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                TCell = new Cell();
                TPhrase = new Phrase(13, "7. Brokers and Dealers", FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTR7 = string.Empty;
                strTR7 = "Securities and Exchange Commission"
                + "\n100 F St NE"
                + "\nWashington, DC 20549";

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, strTR7, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTL8 = string.Empty;
                strTL8 = "8. Federal Land Banks, Federal Land Bank Associations, Federal Intermediate Credit Banks,"
                + " and production Credit Associations";
                TCell = new Cell();
                TPhrase = new Phrase(13, strTL8, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                string strTR8 = string.Empty;
                strTR8 = "Farm Credit Administration"
                + "\n1501 Farm Credit Drive"
                + "\nMcLean, VA 22102-5090";

                TCell = new Cell();
                TPhrase = new Phrase(13, strTR8, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, "9. Retailers, Finance Companies, and All Other Creditors Not Listed Above", FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);


                string strTR9 = string.Empty;
                strTR9 = "FTC Regional Office for region in which the creditor operates or"
                + "\nFederal Trade Commission: Consumer Response Center- FCRA"
                + "\nWashington, DC 20580"
                + "\n(877)382-4357";

                TCell = new Cell();
                TPhrase = new Phrase();
                TPhrase = new Phrase(13, strTR9, FontInfoNormal);
                TCell.HorizontalAlignment = Element.ALIGN_LEFT;
                TCell.BackgroundColor = new Color(255, 255, 255);
                TCell.Add(TPhrase);
                ObjPdfTblMainBottom.AddCell(TCell);

                //ObjPdfTblMainBottom.InsertTable(ObjPdfTblL);
                //ObjPdfTblMainBottom.AddCell("");
                //ObjPdfTblMainBottom.InsertTable(ObjPdfTblR);

                TblMain1.InsertTable(ObjPdfTblMainBottom);

                ObjDocument.Add(TblMain);
                ObjDocument.NewPage();
                ObjDocument.Add(TblMain1);

            }
            catch //(Exception ex)
            {

                // string err = "Error Msg : " + ex.Message.ToString() + ", " + ex.InnerException.ToString() + ", " + ex.Source.ToString();

            }
            finally
            {
                ObjDocument.Close();
            }

            return pdfFileName;
        }

        public string GenerateApplicantName(string sLastname, string sSufix, string sMiddle, string sFirstName)
        {
            string applicantName = string.Empty;
            int getstr = 0, ilen = 0;
            //applicantName = (sLastname != "") ? sLastname + ", " : ""; //for ticket 82
            applicantName = (sFirstName != "") ? sFirstName + " " : "";
            applicantName += (sSufix != "") ? sSufix + " " : "";
            applicantName += (sMiddle != "") ? sMiddle + " " : "";
            //applicantName += (sFirstName != "") ? sFirstName + ", " : "";
            applicantName += (sLastname != "") ? sLastname + " " : "";


            ilen = applicantName.LastIndexOf(",");
            getstr = ilen + 2;
            if (getstr == applicantName.Length)
            {
                applicantName = applicantName.Substring(0, ilen);
            }
            return applicantName;
        }

        public string str21()
        {
            string str = string.Empty;

            str = "The federal Fair Credit Reporting Act (FCRA) promotes the accuracy, fairness, and "
               + "privacy of information in the files of consumer reporting agencies. There are many "
               + "types of consumer reporting agencies, including credit bureaus and specialty agencies "
               + "(such as agencies that sell information about check writing histories, medical "
               + "records, and rental history records). Here is a summary of your major "
               + "rights under the FCRA. For more information, including information about additional "
               + "rights, go to www.consumerfinance.gov/learnmore or write to: Consumer Financial Protection Bureau, "
               + "1700 G Street N.W., Washington, D.C. 20006. "
               + "\nYou may have additional rights under Maine's FCRA, Me. Rev. Stat. Ann. 10, Sec 1311 et"
               + "seq.";
            return str;
        }

        public string str22()
        {
            string str = string.Empty;
            str = "\n•	You must be told if information in your file has been used against you. Anyone who"
                        + "uses a credit report or another type of consumer report to deny your application for"
                        + "credit, insurance, or employment – or to take another adverse action against you –"
                        + "must tell you, and must give you the name, address, and phone number of the agency"
                        + "that provided the information."
                + "\n•	You have the right to know what is in your file. You may request and obtain all "
                        + "the information about you in the files of a consumer reporting agency (your “file"
                        + "disclosure”). You will be required to provide proper identification, which may "
                        + "include your Social Security number. In many cases, the disclosure will be free."
                        + "You are entitled to a free file disclosure";
            return str;
        }

        public string str23()
        {
            string str = string.Empty;
            str = "if:"
                + "\n\t\t\t\t\t\t\t\t\t\t•	A person has taken adverse action against you because of information in your"
                + "credit report;"
                + "\n\t\t\t\t\t\t\t\t\t\t•	You are the victim of identify theft and place a fraud alert in your file;"
                + "\n\t\t\t\t\t\t\t\t\t\t•	Your file contains inaccurate information as a result of fraud;"
                + "\n\t\t\t\t\t\t\t\t\t\t•	You are on public assistance;"
                + "\n\t\t\t\t\t\t\t\t\t\t•	You are unemployed but expect to apply for employment within 60 days."
                + "\n\t\t\t\t\tIn addition, by September 2005 all consumers will be entitled to one free disclosure"
                + "\n\t\t\t\t\tevery 12 months upon request from each nationwide credit bureau and from nationwide"
                + "\n\t\t\t\t\tspecialty consumer reporting agencies. See www.consumerfinance.gov/learnmore for additional "
                + "\n\t\t\t\t\tinformation."
                + "\n•	You have the right to ask for a credit score. Credit scores are numerical"
                + "summaries of your credit-worthiness based on information from credit bureaus."
                + "You may request a credit score from consumer reporting agencies that create scores"
                + "or distribute scores used in residential real property loans, but you will have to"
                + "pay for it. In some mortgage transactions, you will receive credit score information "
                + "for free from the mortgage lender."
                + "\n•	You have the right to dispute incomplete or inaccurate information. If you identify"
                + "information in your file that is incomplete or inaccurate, and report it to the"
                + "consumer reporting agency, the agency must investigate unless your dispute is "
                + "frivolous. See www.consumerfinance.gov/learnmore for an explanation of dispute procedures."
                + "\n•	Consumer reporting agencies must correct or delete inaccurate, incomplete,"
                + "or unverifiable information. Inaccurate, incomplete or unverifiable information"
                + "must be removed or corrected, usually within 30 days. However, a consumer reporting"
                + "agency may continue to report information it has verified as accurate."
                + "\n•	Consumer reporting agencies may not report outdated negative information."
                + "In most cases, a consumer reporting agency may not report negative information"
                + "that is more than seven years old, or bankruptcies that are more than 10 years old."
                + "\n•	Access to your file is limited. A consumer reporting agency may provide"
                + "information about you only to people with a valid need -- usually to consider "
                + "an application with a creditor, insurer, employer, landlord, or other business."
                + "The FCRA specifies those with a valid need for access."
                + "\n•	You must give your consent for reports to be provided to employers. A "
                + "consumer reporting agency may not give out information about you to your"
                + "employer, or a potential employer, without your written consent given to"
                + "the employer. Written consent generally is not required in the trucking industry. "
                + "For more information, go to www.consumerfinance.gov/learnmore."
                + "\n•	You may limit “prescreened” offers of credit and insurance you get based on information in "
                + "your credit report. Unsolicited “prescreened” offers for credit and insurance must"
                + "include a toll-free phone number you can call if you choose to remove your name"
                + "and address from the lists these offers are based on. You may opt-out with the"
                + "nationwide credit bureaus at 1-888-5-OPTOUT (1-888-567-8688)."
                + "\n•	You may seek damages from violators. If a consumer reporting agency, or,"
                + "in some cases, a user of consumer reports or a furnisher of information to "
                + "a consumer reporting agency violates the FCRA, you may be able to sue in state"
                + "or federal court.\n•	Identity theft victims and active duty military personnel have"
                + "additional rights. For more information, visit www.consumerfinance.gov/learnmore.";

            return str;
        }

        public string str3()
        {
            string str = string.Empty;
            str = "States may enforce the FCRA, and many states have their own consumer reporting "
                + "laws. In some cases, you may have more rights under state law. For more information,"
                + "contact your state or local consumer protection agency or your state Attorney"
                + "General. For information about your federal rights, contact:";

            return str;
        }

        public string ButtomLeft1()
        {
            string str = string.Empty;
            str = "1.a. Banks, saving associations, and credit unions with total assets of over $10 billion and their"
                + "\n affiliates."
                + "\nb. Such affiliates that are not banks,saving associations, or credit unions also should list, in"
                + "\n addition to the Bureau:"
                + "\n2. To the extent not included in item 1 above:"
                + "\na. National Banks, federal saving associations, and federal branches ad federal agencies of"
                + "\nforeign banks"
                + "\nb. State member banks, branches and agencies of foreign banks( other than federal branches,"
                + "\n federal agencies and insured state branches of foreign banks), commercial leading companies"
                + "\nowned or controlled by foreign banks, and organizations operating under section 25 or 25A of"
                + "\nthe Federal Reserve Act"
                + "\nc. Nonmember Insured Banks, Insured State Branches of Foreign banks and insured state"
                + "\nsaving organizations"
                + "\nd. Federal Credit Unions"
                + "\n3. Air carriers"
                + "\n4. Creditors Subject to Surface Transportation Board"
                + "\n5. Creditors Subject to Packers and Stockyards Act"
                + "\n6. Small Business Investment Companies"
                + "\n7. Brokers and Dealers"
                + "\n8. Federal Land Banks, Federal Land Bank Associations, Federal Intermediate Credit Banks,"
                + "\nand production Credit Associations"
                + "\n9. Retailers, Finance Companies, and All Other Creditors Not Listed Above";


            return str;
        }

        public string ButtomLeft2()
        {
            string str = string.Empty;
            str = "P O Box 1200\n "
            + "Minneapolis, MN 55480\n "
            + "Telephone: 888-851-1920\n "
            + "Website Address: www.federalreserveconsumerhelp.gov\n "
            + "Email Address: ConsumerHelp@FederalReserve.gov\n\n "
            + "Savings associations and federally chartered savings banks (word "
            + "\"Federal\" or initials \"F.S.B.\" appear in federal institution's name) "
            + "Office of Thrift Supervision "
            + "Consumer Complaints\n "
            + "Washington, DC 20552 800-842-6929\n ";

            return str;
        }

        public string ButtomRight1()
        {
            string str = string.Empty;
            str = "a. Bureau of Consumer Financial Protection"
                + "\n1700 G Street NW"
                + "\nWashington, DC 20006"
                + "\nb. Federal Trade Commission: ConsumerResponse Center-"
                + "\nFCRA"
                + "\nWashington, DC 20580"
                + "\n(877)382-4857"
                + "\na. Office of Comptroller of the Currency"
                + "\nCustomer Assistance Group"
                + "\n1301 McKinnney Street, Suite 3450"
                + "\nHouston, TX 77010-9050"
                + "\nb. Federal Reserve Consumer Help Center"
                + "\nPO Box 1200"
                + "\nMinneapolis, MN55480"
                + "\nc. FDIC Consumer Response Center"
                + "\n1100 Walnut Street, Box #11"
                + "\nKansas City, MO 64106"
                + "\nd. National Credit Union Administrator"
                + "\nOffice of Consumer Protection(OCP)"
                + "\nDivision of Consumer Compliance and Outreach(DCCO)"
                + "\n1775 Duke Street"
                + "\nAlexandria, VA 22314"
                + "\nAsst.General Counsel for Aviation Enforcement & Proceeding"
                + "\n Department ofTransportation"
                + "\n400 Seventh Street SW"
                + "\nWashington, DC 20590"
                + "\nOffice of Proceedings, Surface Transportation Board"
                + "\nDepartment of Transportation"
                + "\n1925 K Street NW"
                + "\nWashington, DC 20423"
                + "\nNearest packers and Stockyards Administration area supervisor"
                + "\nAssociate Deputy Administrator for CapitalAccess"
                + "\nUnited States Small Business Administration"
                + "\n406 Third Street, SW, 8TH Floor"
                + "\nWashington, DC 20416"
                + "\nSecurities and Exchange Commission"
                + "\n100 F St NE"
                + "\nWashington, DC 20549"
                + "\nFarm Credit Administration"
                + "\n1501 Farm Credit Drive"
                + "\nMcLean, VA 22102-5090"
                + "\nFTC Regional Office for region in which the creditor operates or"
                + "\nFederal Trade Commission: Consumer Response Center- FCRA"
                + "\nWashington, DC 20580"
                + "\n(877)382-4357";


            return str;
        }

        public string ButtomRight2()
        {
            string str = string.Empty;
            str = "Aeronautics Board or Interstate Commerce Commission "
            + "Department of Transportation, Office of Financial Management\n "
            + "Washington, DC 20590 202-366-1306\n\n "
            + "Activities subject to the Packers and Stockyards Act, 1921 Department of Agriculture\n\n "
            + "Office of Deputy Administrator - GIPSA "
            + "Washington, DC 20250 202-720-7051\n ";

            return str;
        }
        #endregion

        //public string LoadCSS()
        //{
        //    Response.ContentType = "text/css";
        //    return Razor.Parse(System.IO.File.ReadAllText(Server.MapPath("/Content/CSS/Dynamic.css")));
        //}

        public PartialViewResult PartialFeatureList()
        {
            MarketingContentModel ObjModel = new MarketingContentModel();

            BALContentManagement ObjBALContentManagement = new BALContentManagement();
            var DBContent = ObjBALContentManagement.GetAllFeatures();
            ObjModel.ObjFeature = DBContent;
            ObjModel.Message = string.Empty;
            if (Request.QueryString["Status"] != null)
            {
                int status = Convert.ToInt32(Request.QueryString["Status"]);
                if (status == 1)
                {
                    ObjModel.Message = "<span class='successmsg'>Feature Updated Successfully</span>";
                }
                else if (status == -1)
                {
                    ObjModel.Message = "<span class='errormsg'>Title name already exist</span>";
                }
                else
                {
                    ObjModel.Message = "<span class='errormsg'>error is occured while updating content</span>";
                }
            }
            return PartialView(ObjModel);

        }

        public JsonResult GetStateNameByStateCode(string statecode)
        {
            string sName = "";
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                var vStateName = dx.tblStates.Where(d => d.StateCode == statecode).Select(d => new { d.StateName }).FirstOrDefault();
                if (vStateName != null)
                {
                    sName = vStateName.StateName;
                }
            }
            return Json(new { Data = sName });
        }


        #region NewFeature

        public List<Proc_USA_LoadEmailTemplatesResult> GetTemplateContentsById(int pkTemplateId, bool IsSystemTemplate)
        {
            BALEmailSystemTemplates ObjTemplate = new BALEmailSystemTemplates();
            List<Proc_USA_LoadEmailTemplatesResult> ObjRecords = ObjTemplate.GetEmailTemplates(pkTemplateId, Utility.SiteApplicationId, IsSystemTemplate, 0);
            return ObjRecords;
        }

        public PartialViewResult OpenNewFeatureScreen()
        {

            NewFeatureOpenModel Model = new NewFeatureOpenModel();
            BALContentManagement ObjBALContentManagement = new BALContentManagement();


            try
            {
                MembershipUser Member = Membership.GetUser(User.Identity.Name);
                if (Member != null)
                {
                    Guid UserId = new Guid(Member.ProviderUserKey.ToString());
                    BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
                    //ProfileCommon Profile = new ProfileCommon();
                    //ProfileModel userProfile = Profile.GetProfile(Member.Email);
                    tblCompanyUser ObjTest = ObjBALCompanyUsers.GetUserInformationByMembershipUserId(UserId).First();
                    if (ObjTest.IsSignedFcra == true)
                    {
                        var Obj = ObjBALContentManagement.GetEnabledFeatures();
                        DateTime FeatureCreatedDate = DateTime.Now;
                        if (Obj.Count > 0)
                        {
                            FeatureCreatedDate = Obj.Max(d => d.FeatureCreatedDate);
                            if (FeatureCreatedDate != DateTime.Now && Session["LastLoginDate"] != null)
                            {
                                if (Convert.ToDateTime(Session["LastLoginDate"]) < FeatureCreatedDate)
                                {
                                    Model.IsOpenFeature = true;
                                }
                                else
                                {
                                    Model.IsOpenFeature = false;
                                }
                            }
                            else
                            {
                                Model.IsOpenFeature = false;
                            }
                        }
                        else
                        {
                            Model.IsOpenFeature = false;
                        }

                    }
                    else
                    {
                        Model.IsOpenFeature = false;
                    }
                }
            }
            catch
            {

            }
            return PartialView("PartialOpenNewFeature", Model);
        }




        public PartialViewResult Open_NewFeatureScreen()
        {
            //test
            LoginModel Model = new LoginModel();
            BALContentManagement ObjBALContentManagement = new BALContentManagement();
            bool Feature_count = false;

            try
            {
                // MembershipUser Member = Membership.GetUser(User.Identity.Name);
                Guid UserId = new Guid(Session["user_feature_id"].ToString());
                BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
                //ProfileCommon Profile = new ProfileCommon();

                tblCompanyUser ObjTest = ObjBALCompanyUsers.GetUserInformationByMembershipUserId(UserId).First();
                if (ObjTest.IsSignedFcra == true)
                {
                    var Obj = ObjBALContentManagement.GetEnabledFeatures();
                    DateTime FeatureCreatedDate = DateTime.Now;
                    if (Obj.Count > 0)
                    {
                        FeatureCreatedDate = Obj.Max(d => d.FeatureCreatedDate);
                        if (FeatureCreatedDate != DateTime.Now && Session["LastLoginDate"] != null)
                        {
                            if (Convert.ToDateTime(Session["LastLoginDate"]) < FeatureCreatedDate)
                            {
                                Model.IsOpenFeature = true;
                                Feature_count = Model.IsOpenFeature;
                            }
                            else
                            {
                                Model.IsOpenFeature = false;
                                Feature_count = Model.IsOpenFeature;
                            }
                        }
                        else
                        {
                            Model.IsOpenFeature = false;
                            Feature_count = Model.IsOpenFeature;
                        }
                    }
                    else
                    {
                        Model.IsOpenFeature = false;
                        Feature_count = Model.IsOpenFeature;
                    }

                }
                else
                {
                    Model.IsOpenFeature = false;
                    Feature_count = Model.IsOpenFeature;
                }


            }
            catch
            {

            }
            return PartialView("Open_NewFeatureScreen", Model);

        }


        public string SetImageHandler(object obj, string path)
        {
            return ApplicationPath.GetApplicationPath() + "GetImage.ashx?ImagePath=" + path + obj.ToString() + "&Width=540&Height=380";
        }

        public PartialViewResult LoadFeature(int CurrentFeature, bool IsNext)
        {
            NewFeatureOpenModel ObjNewFeatureModel = new NewFeatureOpenModel();
            BALContentManagement ObjBALContentManagement = new BALContentManagement();
            Proc_GetFeaturesForUserResult ObjDb = new Proc_GetFeaturesForUserResult();
            List<Proc_GetFeaturesForUserResult> ObjDbCollection = new List<Proc_GetFeaturesForUserResult>();
           // Guid UserId = Utility.GetUID(User.Identity.Name);
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                Response.Redirect(ApplicationPath.GetApplicationPath() + "Default");
            }
            else
            {
                try
                {
                    if (Session["LastLoginDate"] != null)
                    {
                        if (Session["NewFeatureColl"] != null)
                        {
                            ObjDbCollection = (List<Proc_GetFeaturesForUserResult>)Session["NewFeatureColl"];

                            ObjDb = ObjDbCollection.ElementAt(CurrentFeature);
                            if (ObjDbCollection.Count == 1)
                            {
                                ObjNewFeatureModel.IsNext = false;
                                ObjNewFeatureModel.IsPrevious = false;
                                ObjNewFeatureModel.CurrentFeature = 1;
                            }
                            else if (CurrentFeature == ObjDbCollection.Count)
                            {
                                ObjNewFeatureModel.IsNext = false;
                                ObjNewFeatureModel.IsPrevious = true;
                                ObjNewFeatureModel.CurrentFeature = IsNext == true ? CurrentFeature + 1 : CurrentFeature;
                            }
                            else if (CurrentFeature < ObjDbCollection.Count && CurrentFeature != 0 && CurrentFeature + 1 < ObjDbCollection.Count)
                            {
                                ObjNewFeatureModel.IsNext = true;
                                ObjNewFeatureModel.IsPrevious = true;
                                ObjNewFeatureModel.CurrentFeature = IsNext == true ? CurrentFeature + 1 : CurrentFeature;
                            }
                            else if (CurrentFeature != 0 && CurrentFeature + 1 == ObjDbCollection.Count)
                            {
                                ObjNewFeatureModel.IsNext = false;
                                ObjNewFeatureModel.IsPrevious = true;
                                ObjNewFeatureModel.CurrentFeature = IsNext == true ? CurrentFeature + 1 : CurrentFeature;
                            }
                            else if (CurrentFeature == 0)
                            {
                                ObjNewFeatureModel.IsNext = true;
                                ObjNewFeatureModel.IsPrevious = false;
                                ObjNewFeatureModel.CurrentFeature = 1;
                            }
                            ObjNewFeatureModel.FeatureId = ObjDb.FeatureId;
                            ObjNewFeatureModel.Title = ObjDb.Title;
                            ObjNewFeatureModel.Description = ObjDb.Description;
                            ObjNewFeatureModel.UploadedFile = ObjDb.UploadedFile;
                            ObjNewFeatureModel.TitleImage = ObjDb.UploadedTitleImage;
                            string UploadedFile = ObjDb.UploadedFile;
                            string Ext = UploadedFile.Split('.')[1];
                            if (Ext == "flv" || Ext == "swf")
                            {
                                ObjNewFeatureModel.Image = "";
                            }
                            else
                            {
                                ObjNewFeatureModel.Image = SetImageHandler(ObjDb.UploadedFile, "Resources/Upload/FeatureFiles/");
                            }
                        }
                        else
                        {
                            ObjDbCollection = ObjBALContentManagement.GetFeaturesForUser("FeatureCreatedDate", "asc", true, 1, 25, Convert.ToDateTime(Session["LastLoginDate"]));
                            if (ObjDbCollection.Count > 0)
                            {
                                Session["NewFeatureColl"] = ObjDbCollection;
                                ObjDb = ObjDbCollection.ElementAt(0);
                                if (ObjDbCollection.Count == 1)
                                {
                                    ObjNewFeatureModel.IsNext = false;
                                    ObjNewFeatureModel.IsPrevious = false;
                                    ObjNewFeatureModel.CurrentFeature = 1;
                                }
                                else
                                {
                                    ObjNewFeatureModel.IsNext = true;
                                    ObjNewFeatureModel.IsPrevious = false;
                                    ObjNewFeatureModel.CurrentFeature = 1;
                                }
                                ObjNewFeatureModel.FeatureId = ObjDb.FeatureId;
                                ObjNewFeatureModel.Title = ObjDb.Title;
                                ObjNewFeatureModel.Description = ObjDb.Description;
                                ObjNewFeatureModel.UploadedFile = ObjDb.UploadedFile;
                                ObjNewFeatureModel.TitleImage = ObjDb.UploadedTitleImage;
                                string UploadedFile = ObjDb.UploadedFile;
                                string Ext = UploadedFile.Split('.')[1];
                                if (Ext == "flv" || Ext == "swf")
                                {
                                    ObjNewFeatureModel.Image = "";
                                }
                                else
                                {
                                    ObjNewFeatureModel.Image = SetImageHandler(ObjDb.UploadedFile, "Resources/Upload/FeatureFiles/");
                                }
                            }
                        }
                        ObjNewFeatureModel.Featurecount = ObjDbCollection.Count;
                    }
                }
                catch { }
            }

            return PartialView("PartialCurrentFeature", ObjNewFeatureModel);
        }

        public PartialViewResult LoadFeaturenew(int CurrentFeature, bool IsNext)
        {
            LoginModel ObjNewFeatureModel = new LoginModel();
            BALContentManagement ObjBALContentManagement = new BALContentManagement();
            Proc_GetFeaturesForUserResult ObjDb = new Proc_GetFeaturesForUserResult();
            List<Proc_GetFeaturesForUserResult> ObjDbCollection = new List<Proc_GetFeaturesForUserResult>();
            Guid UserId = new Guid(Session["user_feature_id"].ToString());
            if (UserId != null)
            {

                try
                {
                    if (Session["LastLoginDate"] != null)
                    {
                        if (Session["NewFeatureColl"] != null)
                        {
                            ObjDbCollection = (List<Proc_GetFeaturesForUserResult>)Session["NewFeatureColl"];

                            ObjDb = ObjDbCollection.ElementAt(CurrentFeature);
                            if (ObjDbCollection.Count == 1)
                            {
                                ObjNewFeatureModel.IsNext = false;
                                ObjNewFeatureModel.IsPrevious = false;
                                ObjNewFeatureModel.CurrentFeature = 1;
                            }
                            else if (CurrentFeature == ObjDbCollection.Count)
                            {
                                ObjNewFeatureModel.IsNext = false;
                                ObjNewFeatureModel.IsPrevious = true;
                                ObjNewFeatureModel.CurrentFeature = IsNext == true ? CurrentFeature + 1 : CurrentFeature;
                            }
                            else if (CurrentFeature < ObjDbCollection.Count && CurrentFeature != 0 && CurrentFeature + 1 < ObjDbCollection.Count)
                            {
                                ObjNewFeatureModel.IsNext = true;
                                ObjNewFeatureModel.IsPrevious = true;
                                ObjNewFeatureModel.CurrentFeature = IsNext == true ? CurrentFeature + 1 : CurrentFeature;
                            }
                            else if (CurrentFeature != 0 && CurrentFeature + 1 == ObjDbCollection.Count)
                            {
                                ObjNewFeatureModel.IsNext = false;
                                ObjNewFeatureModel.IsPrevious = true;
                                ObjNewFeatureModel.CurrentFeature = IsNext == true ? CurrentFeature + 1 : CurrentFeature;
                            }
                            else if (CurrentFeature == 0)
                            {
                                ObjNewFeatureModel.IsNext = true;
                                ObjNewFeatureModel.IsPrevious = false;
                                ObjNewFeatureModel.CurrentFeature = 1;
                            }
                            ObjNewFeatureModel.FeatureId = ObjDb.FeatureId;
                            ObjNewFeatureModel.Title = ObjDb.Title;
                            ObjNewFeatureModel.Description = ObjDb.Description;
                            ObjNewFeatureModel.UploadedFile = ObjDb.UploadedFile;
                            //ApplicationPath.GetApplicationPath() + "GetImage.ashx?ImagePath=~/Resources/Upload/FeatureFiles/" + ObjtblFeature.UploadedTitleImage + "&Height=40&Width=500
                            if (string.IsNullOrEmpty(ObjDb.UploadedTitleImage) == false)
                            {
                                ObjNewFeatureModel.TitleImage = ApplicationPath.GetApplicationPath() + "GetImage.ashx?ImagePath=~/Resources/Upload/FeatureFiles/" + ObjDb.UploadedTitleImage + "&Height=40&Width=500";
                            }


                            string UploadedFile = ObjDb.UploadedFile;
                            string Ext = UploadedFile.Split('.')[1];
                            if (Ext == "flv" || Ext == "swf")
                            {
                                ObjNewFeatureModel.Image = "";
                            }
                            else
                            {
                                //ObjNewFeatureModel.Image = SetImageHandler(ObjDb.UploadedFile, "Resources/Upload/FeatureFiles/");
                                ObjNewFeatureModel.Image = ApplicationPath.GetApplicationPath() + "GetImage.ashx?ImagePath=~/Resources/Upload/FeatureFiles/" + ObjDb.UploadedFile + "&Height=350&Width=650";
                                ObjNewFeatureModel.UploadedFile = ApplicationPath.GetApplicationPath() + "GetImage.ashx?ImagePath=~/Resources/Upload/FeatureFiles/" + ObjDb.UploadedFile + "&Height=350&Width=650";

                            }
                        }
                        else
                        {
                            ObjDbCollection = ObjBALContentManagement.GetFeaturesForUser("FeatureCreatedDate", "asc", true, 1, 25, Convert.ToDateTime(Session["LastLoginDate"]));
                            if (ObjDbCollection.Count > 0)
                            {
                                Session["NewFeatureColl"] = ObjDbCollection;
                                ObjDb = ObjDbCollection.ElementAt(0);
                                if (ObjDbCollection.Count == 1)
                                {
                                    ObjNewFeatureModel.IsNext = false;
                                    ObjNewFeatureModel.IsPrevious = false;
                                    ObjNewFeatureModel.CurrentFeature = 1;
                                }
                                else
                                {
                                    ObjNewFeatureModel.IsNext = true;
                                    ObjNewFeatureModel.IsPrevious = false;
                                    ObjNewFeatureModel.CurrentFeature = 1;
                                }
                                ObjNewFeatureModel.FeatureId = ObjDb.FeatureId;
                                ObjNewFeatureModel.Title = ObjDb.Title;
                                ObjNewFeatureModel.Description = ObjDb.Description;
                                ObjNewFeatureModel.UploadedFile = ObjDb.UploadedFile;
                                ObjNewFeatureModel.TitleImage = ObjDb.UploadedTitleImage;
                                string UploadedFile = ObjDb.UploadedFile;
                                string Ext = UploadedFile.Split('.')[1];
                                if (Ext == "flv" || Ext == "swf")
                                {
                                    ObjNewFeatureModel.Image = "";
                                }
                                else
                                {
                                    ObjNewFeatureModel.Image = SetImageHandler(ObjDb.UploadedFile, "Resources/Upload/FeatureFiles/");
                                }
                            }
                        }
                        ObjNewFeatureModel.Featurecount = ObjDbCollection.Count;
                    }
                }
                catch { }
            }

            return PartialView("PartialCurrentFeaturenew", ObjNewFeatureModel);
        }


        public PartialViewResult PartialCurrentFeaturenew()
        {
            int CurrentFeature = 0;
            bool IsNext = true;

            LoginModel ObjNewFeatureModel = new LoginModel();
            BALContentManagement ObjBALContentManagement = new BALContentManagement();
            Proc_GetFeaturesForUserResult ObjDb = new Proc_GetFeaturesForUserResult();
            List<Proc_GetFeaturesForUserResult> ObjDbCollection = new List<Proc_GetFeaturesForUserResult>();
            Guid UserId = new Guid(Session["user_feature_id"].ToString());

            if (UserId != null)
            {
                try
                {
                    if (Session["LastLoginDate"] != null)
                    {
                        if (Session["NewFeatureColl"] != null)
                        {
                            ObjDbCollection = (List<Proc_GetFeaturesForUserResult>)Session["NewFeatureColl"];

                            ObjDb = ObjDbCollection.ElementAt(CurrentFeature);
                            if (ObjDbCollection.Count == 1)
                            {
                                ObjNewFeatureModel.IsNext = false;
                                ObjNewFeatureModel.IsPrevious = false;
                                ObjNewFeatureModel.CurrentFeature = 1;
                            }
                            else if (CurrentFeature == ObjDbCollection.Count)
                            {
                                ObjNewFeatureModel.IsNext = false;
                                ObjNewFeatureModel.IsPrevious = true;
                                ObjNewFeatureModel.CurrentFeature = IsNext == true ? CurrentFeature + 1 : CurrentFeature;
                            }
                            else if (CurrentFeature < ObjDbCollection.Count && CurrentFeature != 0 && CurrentFeature + 1 < ObjDbCollection.Count)
                            {
                                ObjNewFeatureModel.IsNext = true;
                                ObjNewFeatureModel.IsPrevious = true;
                                ObjNewFeatureModel.CurrentFeature = IsNext == true ? CurrentFeature + 1 : CurrentFeature;
                            }
                            else if (CurrentFeature != 0 && CurrentFeature + 1 == ObjDbCollection.Count)
                            {
                                ObjNewFeatureModel.IsNext = false;
                                ObjNewFeatureModel.IsPrevious = true;
                                ObjNewFeatureModel.CurrentFeature = IsNext == true ? CurrentFeature + 1 : CurrentFeature;
                            }
                            else if (CurrentFeature == 0)
                            {
                                ObjNewFeatureModel.IsNext = true;
                                ObjNewFeatureModel.IsPrevious = false;
                                ObjNewFeatureModel.CurrentFeature = 1;
                            }
                            ObjNewFeatureModel.FeatureId = ObjDb.FeatureId;
                            ObjNewFeatureModel.Title = ObjDb.Title;
                            ObjNewFeatureModel.Description = ObjDb.Description;
                            ObjNewFeatureModel.UploadedFile = ObjDb.UploadedFile;
                            // ObjNewFeatureModel.TitleImage = ObjDb.UploadedTitleImage;
                            if (string.IsNullOrEmpty(ObjDb.UploadedTitleImage) == false)
                            {
                                ObjNewFeatureModel.TitleImage = ApplicationPath.GetApplicationPath() + "GetImage.ashx?ImagePath=~/Resources/Upload/FeatureFiles/" + ObjDb.UploadedTitleImage + "&Height=40&Width=500";
                            }
                            string UploadedFile = ObjDb.UploadedFile;
                            string Ext = UploadedFile.Split('.')[1];
                            if (Ext == "flv" || Ext == "swf")
                            {
                                ObjNewFeatureModel.Image = "";
                            }
                            else
                            {
                                //  ObjNewFeatureModel.Image = SetImageHandler(ObjDb.UploadedFile, "Resources/Upload/FeatureFiles/");
                                ObjNewFeatureModel.Image = ApplicationPath.GetApplicationPath() + "GetImage.ashx?ImagePath=~/Resources/Upload/FeatureFiles/" + ObjDb.UploadedFile + "&Height=350&Width=650";
                                ObjNewFeatureModel.UploadedFile = ApplicationPath.GetApplicationPath() + "GetImage.ashx?ImagePath=~/Resources/Upload/FeatureFiles/" + ObjDb.UploadedFile + "&Height=350&Width=650";

                            }
                        }
                        else
                        {
                            ObjDbCollection = ObjBALContentManagement.GetFeaturesForUser("FeatureCreatedDate", "asc", true, 1, 25, Convert.ToDateTime(Session["LastLoginDate"]));
                            if (ObjDbCollection.Count > 0)
                            {
                                Session["NewFeatureColl"] = ObjDbCollection;
                                ObjDb = ObjDbCollection.ElementAt(0);
                                if (ObjDbCollection.Count == 1)
                                {
                                    ObjNewFeatureModel.IsNext = false;
                                    ObjNewFeatureModel.IsPrevious = false;
                                    ObjNewFeatureModel.CurrentFeature = 1;
                                }
                                else
                                {
                                    ObjNewFeatureModel.IsNext = true;
                                    ObjNewFeatureModel.IsPrevious = false;
                                    ObjNewFeatureModel.CurrentFeature = 1;
                                }
                                ObjNewFeatureModel.FeatureId = ObjDb.FeatureId;
                                ObjNewFeatureModel.Title = ObjDb.Title;
                                ObjNewFeatureModel.Description = ObjDb.Description;
                                ObjNewFeatureModel.UploadedFile = ObjDb.UploadedFile;
                                // ObjNewFeatureModel.TitleImage = ObjDb.UploadedTitleImage;
                                if (string.IsNullOrEmpty(ObjDb.UploadedTitleImage) == false)
                                {
                                    ObjNewFeatureModel.TitleImage = ApplicationPath.GetApplicationPath() + "GetImage.ashx?ImagePath=~/Resources/Upload/FeatureFiles/" + ObjDb.UploadedTitleImage + "&Height=40&Width=500";
                                }
                                string UploadedFile = ObjDb.UploadedFile;
                                string Ext = UploadedFile.Split('.')[1];
                                if (Ext == "flv" || Ext == "swf")
                                {
                                    ObjNewFeatureModel.Image = "";
                                }
                                else
                                {
                                    //    ObjNewFeatureModel.Image = SetImageHandler(ObjDb.UploadedFile, "Resources/Upload/FeatureFiles/");
                                    ObjNewFeatureModel.Image = ApplicationPath.GetApplicationPath() + "GetImage.ashx?ImagePath=~/Resources/Upload/FeatureFiles/" + ObjDb.UploadedFile + "&Height=350&Width=650";
                                    ObjNewFeatureModel.UploadedFile = ApplicationPath.GetApplicationPath() + "GetImage.ashx?ImagePath=~/Resources/Upload/FeatureFiles/" + ObjDb.UploadedFile + "&Height=350&Width=650";
                                }
                            }
                        }
                        ObjNewFeatureModel.Featurecount = ObjDbCollection.Count;
                    }
                }
                catch { }
            }

            return PartialView("PartialCurrentFeaturenew", ObjNewFeatureModel);
        }


        public ActionResult UpdateViewCount()
        {
            BALContentManagement ObjBALContentManagement = new BALContentManagement();
            List<Proc_GetFeaturesForUserResult> ObjDbCollection = new List<Proc_GetFeaturesForUserResult>();
            if (Session["LastLoginDate"] != null)
            {
                if (Session["NewFeatureColl"] != null)
                {
                    ObjDbCollection = (List<Proc_GetFeaturesForUserResult>)Session["NewFeatureColl"];
                    foreach (Proc_GetFeaturesForUserResult data in ObjDbCollection)
                    {
                        //int Status = 
                            ObjBALContentManagement.UpdateFeature(Convert.ToInt32(data.FeatureId), Convert.ToInt32(data.ViewCount) + 1);

                    }
                    Session["LastLoginDate"] = null;
                }
            }
            return Json(null);
        }

        [HttpPost]
        public ActionResult GetNewFeatures(int page, int pageSize)
        {
            BALContentManagement ObjBALContentManagement = new BALContentManagement();
            List<Proc_GetFeaturesForUserResult> ObjDbCollection = new List<Proc_GetFeaturesForUserResult>();
            ObjDbCollection = ObjBALContentManagement.GetFeaturesForUser("FeatureCreatedDate", "asc", true, 1, 25, Convert.ToDateTime(Session["LastLoginDate"]));
            if (ObjDbCollection.Count > 0)
            {
                return Json(new { Reclist = ObjDbCollection, TotalRec = ObjDbCollection.ElementAt(0).TotalRec });
            }
            else
            {
                return Json(new { Reclist = ObjDbCollection, TotalRec = ObjDbCollection.Count });
            }
        }

        #endregion


        #region Chat

        public ActionResult GetOnLineUserList()
        {
            //var num = Membership.GetNumberOfUsersOnline().ToString();

            MembershipUser user = Membership.GetUser();
            List<ChatUserList> onlineUser = null;
            try
            {
                onlineUser = GetChatUserListbyCompanyUserId(Guid.Parse(user.ProviderUserKey.ToString()));
            }
            catch { }

            return Json(onlineUser, JsonRequestBehavior.AllowGet);
        }


        public ActionResult sendMessage(Guid to, string message, Guid from)
        {
            try
            {
                using (EmergeDALDataContext db = new EmergeDALDataContext())
                {
                    tblChatMessage messageEntry = new tblChatMessage();
                    messageEntry.fkReceiverID = to;
                    messageEntry.message = message;
                    messageEntry.time = DateTime.Now;
                    messageEntry.fkSenderID = from;
                    //messageEntry.pkMessageID = Guid.NewGuid();
                    messageEntry.ReadStatus = false;
                    db.tblChatMessages.InsertOnSubmit(messageEntry);
                    db.SubmitChanges();
                    return Json("");
                }
            }
            catch { }
            return Json("");
        }


        public ActionResult ChatHeartBeat(Guid senderID, Guid receiverID)
        {
            List<Proc_GetChatMessageUpdateResult> data = null;
            try
            {
                using (EmergeDALDataContext db = new EmergeDALDataContext())
                {
                    data = db.GetChatMessageUpdate(senderID, receiverID).ToList();
                }
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch { }
            return Json(data, JsonRequestBehavior.AllowGet);
        }


        public ActionResult StartChatSession()
        {
            //MembershipUser user = Membership.GetUser();
            //List<getFirstChatMessage_Result> chatMessageDetails = null;
            List<Proc_getListWhoMessagedYouResult> userMessagedMe = new List<Proc_getListWhoMessagedYouResult>();
            try
            {
                MembershipUser user = Membership.GetUser();
                using (EmergeDALDataContext db = new EmergeDALDataContext())
                {
                    userMessagedMe = db.getListWhoMessagedYou(Guid.Parse(user.ProviderUserKey.ToString())).ToList();
                    return Json(userMessagedMe, JsonRequestBehavior.AllowGet);
                }
            }
            catch { }
            return Json(userMessagedMe, JsonRequestBehavior.AllowGet);
        }

        public List<ChatUserList> GetChatUserListbyCompanyUserId(Guid UserId)
        {
            List<ChatUserList> objChatUserList = new List<ChatUserList>();
            List<Proc_GetOnLineuserCountResult> FilterChatlist, onlineUser = null;
            try
            {
                using (EmergeDALDataContext db = new EmergeDALDataContext())
                {
                    BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
                    List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjData = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(UserId);
                    if (ObjData.Count > 0)
                    {
                        Guid CUserId = ObjData.First().UserId;
                        Guid ChatUserId = Guid.Empty;
                        BALChat ObjBALChat = new BALChat();
                        List<Proc_GetRoleNameByCompanyUserIdResult> GetRoleName = ObjBALChat.GetRoleName(CUserId);
                        string RoleName = GetRoleName.ElementAt(0).UserRole;


                        if ((RoleName == "SupportTechnician") || (RoleName == "SupportManager") || (RoleName == "SalesManager"))//(RoleName == "Admin") ||
                        {
                            onlineUser = db.GetOnLineuserCount(UserId).ToList();
                            if (onlineUser != null)
                            {
                                for (int i = 0; i < onlineUser.Count; i++)
                                {
                                    ChatUserList objChatUser = new ChatUserList();
                                    objChatUser.UserId = onlineUser.ElementAt(i).UserId;
                                    objChatUser.UserName = onlineUser.ElementAt(i).FirstName + " " + onlineUser.ElementAt(i).LastName;
                                    objChatUser.imageName = onlineUser.ElementAt(i).imageName;
                                    if (objChatUserList.Contains(objChatUser) == false)
                                    {
                                        if (ChatUserId != objChatUser.UserId)
                                        {
                                            objChatUserList.Add(objChatUser);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            proc_CheckUserUnderSameCompanyResult UserInfo = new proc_CheckUserUnderSameCompanyResult();
                            UserInfo = CheckUserCompany(UserId);
                            if (UserInfo != null)
                            {
                                CompanyId = UserInfo.pkcompanyid;
                            }

                            FilterChatlist = db.GetOnLineuserCount(UserId).ToList();
                            foreach (Proc_GetOnLineuserCountResult kvp in FilterChatlist)
                            {
                                proc_CheckUserUnderSameCompanyResult OtherUserInfo = new proc_CheckUserUnderSameCompanyResult();
                                try
                                {
                                    ChatUserList objOtherChatUser = new ChatUserList();
                                    Guid OtherUid = kvp.UserId;
                                    OtherUserInfo = CheckUserCompany(OtherUid);
                                    objOtherChatUser.UserId = kvp.UserId;
                                    objOtherChatUser.imageName = kvp.imageName;
                                    if (OtherUserInfo != null)
                                    {
                                        if (CompanyId == OtherUserInfo.pkcompanyid)
                                        {
                                            objOtherChatUser.UserName = kvp.FirstName + " " + kvp.LastName;
                                            objChatUserList.Add(objOtherChatUser);
                                        }
                                        else
                                        {
                                            List<Proc_GetRoleNameByCompanyUserIdResult> GetRoleName1 = ObjBALChat.GetRoleName(OtherUid);
                                            string RoleName1 = GetRoleName1.ElementAt(0).UserRole;
                                            if ((RoleName1 == "SupportTechnician") || (RoleName1 == "SupportManager") || (RoleName1 == "SalesManager"))//(RoleName1 == "Admin") ||
                                            {
                                                if (objChatUserList.Contains(objOtherChatUser) == false)
                                                {
                                                    if (CUserId != objOtherChatUser.UserId)
                                                    {
                                                        objOtherChatUser.UserName = kvp.FirstName + " " + kvp.LastName;
                                                        objChatUserList.Add(objOtherChatUser);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        List<Proc_GetRoleNameByCompanyUserIdResult> GetRoleName1 = ObjBALChat.GetRoleName(OtherUid);
                                        string RoleName1 = GetRoleName1.ElementAt(0).UserRole;
                                        if ((RoleName1 == "SupportTechnician") || (RoleName1 == "SupportManager") || (RoleName1 == "SalesManager")) //(RoleName1 == "Admin") ||
                                        {
                                            if (objChatUserList.Contains(objOtherChatUser) == false)
                                            {
                                                if (CUserId != objOtherChatUser.UserId)
                                                {
                                                    objOtherChatUser.UserName = kvp.FirstName + " " + kvp.LastName;
                                                    objChatUserList.Add(objOtherChatUser);
                                                }
                                            }
                                        }
                                    }
                                }
                                catch
                                {
                                    if (Utility.IsGuid(kvp.UserId.ToString()))
                                    {
                                        Guid OtherUid = new Guid(UserId.ToString());
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch { }
            return objChatUserList;
        }
        public ActionResult LoadFeaturePreview(string FeatureId)
        {
            int fId = 0;
            BALContentManagement ObjBALContentManagement = new BALContentManagement();
            tblFeature ObjtblFeature = new tblFeature();
            try
            {
                if (!string.IsNullOrEmpty(FeatureId))
                {
                    fId = Int32.Parse(FeatureId);
                }
                else
                {
                    fId = 0;
                }
                ObjtblFeature = ObjBALContentManagement.GetFeatureListById(fId);
                if (System.IO.File.Exists(Server.MapPath(@"~/Resources/Upload/FeatureFiles/" + ObjtblFeature.UploadedTitleImage)))
                {
                    ObjtblFeature.UploadedTitleImage = ApplicationPath.GetApplicationPath() + "GetImage.ashx?ImagePath=~/Resources/Upload/FeatureFiles/" + ObjtblFeature.UploadedTitleImage + "&Height=40&Width=200";
                }

                if (!System.IO.File.Exists(Server.MapPath(@"~/Resources/Upload/FeatureFiles/" + ObjtblFeature.UploadedFile)))
                {
                    ObjtblFeature.UploadedFile = "/Resources/Upload/FeatureFiles/no_image.jpg";
                }
                else if (!(ObjtblFeature.UploadedFile.ToLower().Contains(".flv") || ObjtblFeature.UploadedFile.ToLower().Contains(".swf")))
                {
                    ObjtblFeature.UploadedFile = ApplicationPath.GetApplicationPath() + "GetImage.ashx?ImagePath=~/Resources/Upload/FeatureFiles/" + ObjtblFeature.UploadedFile + "&Height=350&Width=650";
                }

            }
            catch (Exception)
            {

            }
            finally
            {
                ObjBALContentManagement = null;
            }
            return Json(ObjtblFeature, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadFeaturePreviewForEnlarge(string FeatureId)
        {
            int fId = 0;
            BALContentManagement ObjBALContentManagement = new BALContentManagement();
            tblFeature ObjtblFeature = new tblFeature();
            try
            {
                if (!string.IsNullOrEmpty(FeatureId))
                {
                    fId = Int32.Parse(FeatureId);
                }
                else
                {
                    fId = 0;
                }


                ObjtblFeature = ObjBALContentManagement.GetFeatureListById(fId);
                if (System.IO.File.Exists(Server.MapPath(@"~/Resources/Upload/FeatureFiles/" + ObjtblFeature.UploadedTitleImage)))
                {
                    ObjtblFeature.UploadedTitleImage = ApplicationPath.GetApplicationPath() + "GetImage.ashx?ImagePath=~/Resources/Upload/FeatureFiles/" + ObjtblFeature.UploadedTitleImage + "&Height=40&Width=200";
                }

                if (!System.IO.File.Exists(Server.MapPath(@"~/Resources/Upload/FeatureFiles/" + ObjtblFeature.UploadedFile)))
                {
                    ObjtblFeature.UploadedFile = "/Resources/Upload/FeatureFiles/no_image.jpg";
                }
                else if (!(ObjtblFeature.UploadedFile.ToLower().Contains(".flv") || ObjtblFeature.UploadedFile.ToLower().Contains(".swf")))
                {
                    ObjtblFeature.UploadedFile = ApplicationPath.GetApplicationPath() + "GetImage.ashx?ImagePath=~/Resources/Upload/FeatureFiles/" + ObjtblFeature.UploadedFile + "&Height=600&Width=850";
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                ObjBALContentManagement = null;
            }
            return Json(ObjtblFeature, JsonRequestBehavior.AllowGet);
        }

        public proc_CheckUserUnderSameCompanyResult CheckUserCompany(Guid UserId)
        {
            //fdfee5c5-ca52-4499-8233-7dcb7fadaace
            //7dc87d77-6bb5-4bb3-9837-fa861f36f281
            BALChat ObjBALChat = new BALChat();
            proc_CheckUserUnderSameCompanyResult UserInfo = new proc_CheckUserUnderSameCompanyResult();
            UserInfo = ObjBALChat.GetUserCompany(UserId);
            return UserInfo;
        }

        public Guid CompanyId { get; set; }

        #endregion

        public PartialViewResult PartialReportViewer()
        {
            return PartialView("PartialReportViewer");
        }



        public ActionResult GenerateConfidentialform(string ApplicantName, string EnteredCompany)
        {
            string pdfFileName = string.Empty;
            pdfFileName = Generate_Confidential_form(ApplicantName, EnteredCompany);


            return Json(new { FileName = pdfFileName });
            //string FilePath = HttpContext.Server.MapPath(@"~/Resources/Upload/613NoticePdf/disclosureandauthorization.pdf");
            //string fileext = System.IO.Path.GetExtension("613NoticePdf.pdf");
            //string CType = "application/" + fileext.Replace(".", "");
            //string strFileTitle = pdfFileName;

            //return File(FilePath, CType, strFileTitle);


        }

        public string Generate_Confidential_form(string ApplicantName, string EnteredCompany)
        {
            string pdfFileName = string.Empty;
            //BALCompany ObjBALCompany = new BALCompany();
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            MembershipUser Member = Membership.GetUser(User.Identity.Name);
            string Company_name = string.Empty;
            string Address = string.Empty;
            string contactNo = string.Empty;
            if (Member != null)
            {
                var dbCollection = ObjBALCompanyUsers.GetUserCompanyInfo(new Guid(Member.ProviderUserKey.ToString()));
                if (dbCollection.Count > 0)
                {
                    Company_name = dbCollection.First().CompanyName.ToString();
                    Address = dbCollection.First().CompanyName.ToString();
                    contactNo = dbCollection.First().PhoneNo1.ToString();

                }
            }


            string CompanyName = EnteredCompany != "" ? EnteredCompany : Company_name;
            CompanyName = CompanyName + "_" + contactNo;
            string Recipient = ApplicantName;
            BALGeneral ObjBALGeneral = new BALGeneral();
            pdfFileName = ObjBALGeneral.Confidentialform(CompanyName, Recipient, Address);
            return pdfFileName;


        }
        public int AutoCompleteCompany_id(string CompanyNme, string PageType, string CompanyName)
        {
            BALOrders ObjOrder = new BALOrders();

            bool IsAdmin = true;
            if (Roles.IsUserInRole("SystemAdmin"))
            {
                IsAdmin = true;
            }
            else
            {
                IsAdmin = false;
            }


            Guid UserId = Utility.GetUID(User.Identity.Name);

            List<string> collection = ObjOrder.GetCallLogTop10ComapnyRecords(CompanyNme.Trim(), UserId, IsAdmin, PageType).Select(d => d.Item).ToList();
            int pkCompanyId = 0;
            List<AutoCompany> ObjAutoCompany = new List<AutoCompany>();
            if (collection.Count > 0)
            {
                for (int i = 0; i < collection.Count; i++)
                {
                    var element = collection[i].ToString().Split(new[] { "CompId" }, StringSplitOptions.None);
                    int Company_Id = Convert.ToInt32(element[0].ToString());
                    string Company_Name = element[1].ToString();
                    ObjAutoCompany.Add(new AutoCompany() { CompanyName = Company_Name, pkCompanyId = Company_Id });
                }

                if (ObjAutoCompany.Count > 0)
                {
                    for (int Y = 0; Y < ObjAutoCompany.Count; Y++)
                    {

                        if (ObjAutoCompany.ElementAt(Y).CompanyName == CompanyName)
                        {

                            pkCompanyId = ObjAutoCompany.ElementAt(Y).pkCompanyId;
                        }

                    }


                }
            }


            return pkCompanyId;
        }
    }

    public class AutoCompany
    {

        public int pkCompanyId { get; set; }
        public string CompanyName { get; set; }

    }

}
