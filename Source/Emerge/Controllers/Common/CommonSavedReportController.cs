﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Data;
using Emerge.Services;
using Emerge.Models;
using Emerge.Common;
using System.Text;
using System.Web.Security;
using System.IO;

namespace Emerge.Controllers.Common
{
    public class CommonSavedReportController : Controller
    {
        public ActionResult GetSavedReports(int page, int pageSize, string group, List<GridSort> sort, List<GridFilter> filters, int pkCompanyId, int LocationId, int CompanyUserId, string Keyword, string SearchdOntable, int pkProductApplicationId, bool ShowIncomplete)
        {

            List<Proc_GetSavedReportsForMVCResult> ObjData = null;
            BALOrders ObjBALOrders = new BALOrders();
            List<proc_GetSavedReportsCountResult> ObjCountStatus1 = new List<proc_GetSavedReportsCountResult>();
            List<proc_GetSavedReportsCountResult> ObjCountStatus = new List<proc_GetSavedReportsCountResult>();
            //static values
            string ColumnName = "OrderDt";
            string Direction = "DESC";
            string ReferenceCode = string.Empty;
            bool SavedReport6Month = false;
            #region Get Columns from Profile
            //int pkCompanyId1 = 0;
            int pkLocationId1 = 0;


            try
            {

                MembershipUser Member = Membership.GetUser(User.Identity.Name);
                string[] UserRole = Roles.GetRolesForUser(Member.Email);
                if (UserRole.Contains("SystemAdmin"))
                {
                    SavedReport6Month = true;
                }
                else
                {
                    //SavedReport6Month = false;
                    SavedReport6Month = true; ////add code in ticket #152
                    MembershipUser ObjMembership = Membership.GetUser(User.Identity.Name);
                    if (ObjMembership != null)
                    {
                        Guid UserId = new Guid(ObjMembership.ProviderUserKey.ToString());
                        List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjCompanyinfo = GetCompanyInfo(UserId);
                        if (ObjCompanyinfo != null)
                        {
                            //add code in ticket #152 for see all saved report by user
                            CompanyUserId = ObjCompanyinfo.ElementAt(0).pkCompanyUserId;
                            //commented for ticket #152
                            pkCompanyId = ObjCompanyinfo.ElementAt(0).pkCompanyId;
                            //LocationId = ObjCompanyinfo.ElementAt(0).pkLocationId;
                        }
                    }
                }

                if (Member != null)
                {
                    ProfileCommon Profile = new ProfileCommon();
                    ProfileModel userProfile = Profile.GetProfile(Member.Email);
                    if (userProfile.SortColumn != string.Empty)
                    {
                        ColumnName = userProfile.SortColumn;

                        if (ColumnName == "ApplicantName" || ColumnName == "OrderNo" || ColumnName == "ReportIncludes" || ColumnName == "CompanyName" || ColumnName == "Location" || ColumnName == "User")
                        {
                            Direction = "ASC";
                        }
                        else
                        {
                            Direction = "DESC";
                        }

                    }
                    if (TempData["PageSize"] != null)
                    {
                        pageSize = Convert.ToInt32(TempData["PageSize"]);
                    }
                    if (TempData["PageNo"] != null)
                    {
                        page = Convert.ToInt32(TempData["PageNo"]);
                    }
                }

            #endregion

                #region Sorting

                if (sort != null)
                {
                    Direction = sort[0].Dir;
                    ColumnName = sort[0].Field;
                }

                #endregion

                #region Filtering

                string ToDate = "";
                string FromDate = "";
                string ReportName = "AllReports";

                if (filters != null)
                {
                    int pkOrderDateId = Convert.ToInt32(filters[0].Value);
                    if (pkOrderDateId != -5) //-5 Means Reset Filters
                    {
                        #region Set Values

                        pkProductApplicationId = Convert.ToInt32(filters[1].Value);
                        if (UserRole.Contains("BasicUser"))
                        {
                            CompanyUserId = (filters[2].Value == null ? 0 : Convert.ToInt32(filters[2].Value));//Allan:for 306 ticket.
                        }
                        else
                        {
                            CompanyUserId = (filters[2].Value == null ? 0 : Convert.ToInt32(filters[2].Value));
                        }
                        pkCompanyId = (filters[3].Value == null ? 0 : Convert.ToInt32(filters[3].Value));
                        if (UserRole.Contains("SystemAdmin"))
                        {
                            LocationId = (filters[4].Value == null ? 0 : Convert.ToInt32(filters[4].Value));
                        }
                        else
                        {
                            pkLocationId1 = (filters[4].Value == null ? 0 : Convert.ToInt32(filters[4].Value));
                            if (pkLocationId1 != 0)
                            {
                                LocationId = pkLocationId1;
                            }
                            if (UserRole.Contains("CorporateManager")) { LocationId = pkLocationId1; }


                        }

                        Keyword = filters[5].Value ?? string.Empty;

                        if (string.IsNullOrEmpty(Keyword) == false) { Keyword = Keyword.Replace(",", ""); }

                        int Month = Convert.ToInt32(filters[6].Value);
                        int Year = Convert.ToInt32(filters[7].Value);

                        DateTime today = DateTime.Today;
                        if (pkOrderDateId == 1)
                        {
                            ToDate = Convert.ToString(today.ToShortDateString());
                            FromDate = Convert.ToString(today.ToShortDateString());
                        }
                        else if (pkOrderDateId == 2)
                        {
                            FromDate = new DateTime(today.Year, today.Month, 1).ToShortDateString();
                            ToDate = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month)).ToShortDateString();
                        }
                        else if (pkOrderDateId == 3)
                        {
                            FromDate = new DateTime(today.Year, today.Month, 1).AddMonths(-1).ToShortDateString();
                            ToDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(-1).ToShortDateString();
                        }
                        else if (pkOrderDateId == 4)
                        {
                            FromDate = new DateTime(Year, Month, 1).ToShortDateString();
                            ToDate = new DateTime(Year, Month, DateTime.DaysInMonth(Year, Month)).ToShortDateString();
                        }
                        else if (pkOrderDateId == 5)
                        {
                            FromDate = filters[8].Value;
                            ToDate = filters[8].Value;
                        }
                        else if (pkOrderDateId == 6)
                        {
                            FromDate = filters[9].Value;
                            ToDate = filters[10].Value;
                        }
                        ReportName = filters[11].Value;
                        ShowIncomplete = Convert.ToBoolean(filters[13].Value);
                        string pkReferenceCode = filters[14].Value;
                        if (pkReferenceCode == "--All--")//ND-2
                        {
                            ReferenceCode = null;
                        }
                        else if (pkReferenceCode != "Reference")
                        {
                            ReferenceCode = filters[14].Value;
                        }

                        #endregion

                    }
                    else //Reset filters
                    {
                        #region Reset Filters

                        string[] Role = Roles.GetRolesForUser(Member.Email);
                        if (Role.Contains("SystemAdmin"))
                        {
                            pkCompanyId = 0;
                        }
                        if (Role.Contains("SystemAdmin") || Role.Contains("CorporateManager") || Role.Contains("SupportManager") || Role.Contains("SupportTechnician"))
                        {
                            LocationId = 0;
                            CompanyUserId = 0;
                        }

                        //If reset all button than compnay user id will be all(0) based on selected location.
                        if (Role.Contains("BasicUser") || Role.Contains("BranchManager"))
                        {
                            CompanyUserId = 0;
                        }

                        Keyword = string.Empty;
                        pkProductApplicationId = 0;
                        ShowIncomplete = false;
                        #endregion
                    }
                }
                #endregion

                #region Save Profile

                if (Member != null)
                {
                    try
                    {
                        ProfileCommon Profile = new ProfileCommon();
                        ProfileModel userProfile = Profile.GetProfile(Member.Email);
                        Profile.SetGeneralProfile(Member.Email, "PageNo", page.ToString());
                        Profile.SetGeneralProfile(Member.Email, "RecordSize", pageSize.ToString());

                        Profile.SetGeneralProfile(Member.Email, "SearchedText", Keyword);
                        Profile.SetGeneralProfile(Member.Email, "DataSource", pkProductApplicationId.ToString());
                        Profile.SetGeneralProfile(Member.Email, "Company", pkCompanyId.ToString());
                        Profile.SetGeneralProfile(Member.Email, "Location", LocationId.ToString());
                        Profile.SetGeneralProfile(Member.Email, "User", CompanyUserId.ToString());
                    }
                    catch (Exception)
                    {
                    }
                }

                #endregion

                #region Call DB
                #region Save  Important Id for CSV.
                //For Billing User.
                if (UserRole.Contains("BillingUser"))
                {
                    CompanyUserId = 0;
                }
                Session["pkProductApplicationId_CSV"] = pkProductApplicationId;
                Session["CompanyUserId_CSV"] = CompanyUserId;
                Session["pkCompanyId_CSV"] = pkCompanyId;
                Session["LocationId_CSV"] = LocationId;
                Session["ReportName_CSV"] = ReportName;
                Session["FromDate_CSV"] = FromDate;
                Session["ToDate_CSV"] = ToDate;

                #endregion

                if (string.IsNullOrEmpty(Keyword) == false) { Keyword = Keyword.Replace(",", ""); }
                Session["Keyword_CSV"] = Keyword;
                ObjData = ObjBALOrders.GetSavedReportsMVC(pkProductApplicationId,
                                                          CompanyUserId,
                                                          pkCompanyId,
                                                          LocationId,
                                                          ColumnName,
                                                          Direction,
                                                          true,
                                                          page,
                                                          pageSize,
                                                          FromDate,
                                                          ToDate,
                                                          Keyword.Replace("'", "''"),
                                                          0,
                                                          ReportName,
                                                          ShowIncomplete, ReferenceCode, SavedReport6Month, SearchdOntable).Select(p => new Proc_GetSavedReportsForMVCResult
                                                          {
                                                              OrderDt = p.OrderDt,
                                                              IsSyncpod = p.IsSyncpod,
                                                              InReviewed = p.InReviewed,
                                                              IsDrugTestingOrder = p.IsDrugTestingOrder,
                                                              TotalRec = p.TotalRec,
                                                              OrderId = p.OrderId,
                                                              PendingOrderUpdateNote = p.PendingOrderUpdateNote,
                                                              OrderNo = p.OrderNo,
                                                              OrderStatus = p.OrderStatus,
                                                              ApplicantDOB = p.ApplicantDOB,
                                                              fkLocationId = p.fkLocationId,
                                                              ApplicantName = p.ApplicantName,
                                                              CompanyName = p.CompanyName,
                                                              Days = p.Days,
                                                              FirstName = p.FirstName,
                                                              IsViewed = p.IsViewed,
                                                              TrackingReferenceCode = p.TrackingReferenceCode,
                                                              LastName = p.LastName,
                                                              Location = p.Location,
                                                              OrderStatusDisplay = p.OrderStatusDisplay,
                                                              OrderType = p.OrderType,
                                                              ReportsWithStatus = ReportsWithStatus(p.ReportsWithStatus, Int32.Parse(p.OrderId.ToString())), //
                                                              ReportIncludes = p.ReportIncludes,
                                                              RowNo = p.RowNo,
                                                              StateCode = p.StateCode,
                                                              UserEmail = p.UserEmail,
                                                              IsSubmitted = p.IsSubmitted,
                                                              HitStatus = p.HitStatus,
                                                              pkCompanyId = p.pkCompanyId,
                                                              pkCompanyUserId = p.pkCompanyUserId,
                                                              SearchedSsn = p.SearchedSsn,
                                                              IsEscreenOrder = p.IsEscreenOrder,
                                                              ReportStatus = p.ReportStatus,
                                                              IsReportStatus = p.IsReportStatus
                                                          }).ToList();
                int TotalRec = 0;
                if (ObjData.Count() > 0)
                {
                    TotalRec = (int)ObjData.First().TotalRec;
                }

                #region for SSN Search by lakhvinder larry Ticket #359
                int n;
                bool isNumeric = int.TryParse(Keyword, out n);
                int length = Keyword.Length;
                if (isNumeric == true && length == 4)
                {
                    ObjData = ObjData.Where(rec => rec.SearchedSsn.EndsWith(Keyword)).ToList();
                    TotalRec = (int)ObjData.Count();
                }
                #endregion
                #endregion

                #region Get Count Mails

                string ReportCounts = string.Empty;

                if (ReportName == "AllReports")
                {
                    ReportCounts = GetReportCounter(ObjCountStatus1);
                }
                if (ReportName == "IncompleteArchive")
                {
                    ReportCounts = GetArchiveReportCounter(ObjCountStatus);
                }

                // INT 371 ISSUE ON 11MAY2016
                if (ReportName != "AllReports" || ReportName != "PendingReports" || ReportName != "InReviewReports" || ReportName != "IncompleteReports" || ReportName != "Submit" || ReportName != "CompleteReports")
                {
                    ReportName = "AllReports";
                }




                #endregion





                Session["TotalReportCount"] = TotalRec;

                return Json(new { Products = ObjData, TotalCount = TotalRec, ReportCounts = ReportCounts, ReportName = ReportName });
            }
            catch// (Exception excep)
            {
                return Json(new { Products = ObjData, TotalCount = 0, ReportCounts = 0, ReportName = string.Empty });
            }
            finally
            {
                ObjData = null;
                ObjBALOrders = null;
                ObjCountStatus1 = null;
                ObjCountStatus = null;
            }

        }


        public FileResult ExportSelectedOrderToCSV()
        {
            //BALChat objBalChat = new BALChat();
            int PageNum = 1;
            int PageSize = Convert.ToInt32(Session["TotalReportCount"]);
            string FromDate = string.Empty;
            string ToDate = string.Empty;
            bool ShowIncomplete = true;
            string ReferenceCode = string.Empty;
            bool SavedReport6Month = true;
            string SearchdOntable = string.Empty;

            string SortingColumn = "OrderDt";
            string Keyword = string.Empty;
            string ReportName = string.Empty;
            string SortingDirection = "DESC";
           // int TotalRec = 0;
            //bool IsPaging = true;
            int LocationId = 0;
            int FkCompanyId = 0;
            int CompanyUserId = 0;
            int pkProductApplicationId = 0;

            pkProductApplicationId = Int32.Parse(Session["pkProductApplicationId_CSV"].ToString());
            CompanyUserId = Int32.Parse(Session["CompanyUserId_CSV"].ToString());
            FkCompanyId = Int32.Parse(Session["pkCompanyId_CSV"].ToString());
            LocationId = Int32.Parse(Session["LocationId_CSV"].ToString());
            ReportName = Session["ReportName_CSV"].ToString();
            FromDate = Session["FromDate_CSV"].ToString();
            ToDate = Session["ToDate_CSV"].ToString();
            //Int16 CompanyType = -1;
            //BALCompany ObjBALCompany = new BALCompany();
            BALOrders ObjBALOrders = new BALOrders();
            var ObjData = ObjBALOrders.GetSavedReportsMVC(pkProductApplicationId,
                                                           CompanyUserId,
                                                           FkCompanyId,
                                                           LocationId,
                                                           SortingColumn,
                                                           SortingDirection,
                                                           true,
                                                           PageNum,
                                                           PageSize,
                                                           FromDate,
                                                           ToDate,
                                                           Keyword.Replace("'", "''"),
                                                           0,
                                                           ReportName,
                                                           ShowIncomplete, ReferenceCode, SavedReport6Month, SearchdOntable).ToList();
            MemoryStream output = new MemoryStream();
            StreamWriter writer = new StreamWriter(output, Encoding.UTF8);
            try
            {                
                writer.Write("Applicant Name,");
                writer.Write("DOB ,");
                writer.Write("Order No ,");

                writer.Write("Reference  , ");
                writer.Write("Data Sources , ");
                writer.Write("Company Name ,");
                writer.Write("Location  ,");

                writer.Write("User    ,");
                writer.Write("Order Date    ,");

                writer.Write("Status");
                writer.WriteLine();

                for (int i = 0; i < ObjData.Count(); i++)
                {
                    writer.Write(ObjData.ElementAt(i).ApplicantName == null ? string.Empty : ObjData.ElementAt(i).ApplicantName.Replace(",", " ").Replace("\n", " "));
                    writer.Write(",");
                    writer.Write("\"");
                    writer.Write(ObjData.ElementAt(i).ApplicantDOB == null ? string.Empty : ObjData.ElementAt(i).ApplicantDOB.Replace(",", " "));
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");
                    writer.Write(ObjData.ElementAt(i).OrderNo == null ? string.Empty : ObjData.ElementAt(i).OrderNo.ToString());
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");
                    writer.Write(ObjData.ElementAt(i).TrackingReferenceCode == null ? string.Empty : ObjData.ElementAt(i).TrackingReferenceCode.Replace(",", " "));//Reference
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");
                    writer.Write(ObjData.ElementAt(i).ReportsWithStatus == null ? string.Empty : ObjData.ElementAt(i).ReportsWithStatus.Replace("_0_0", " ").Replace("_0_1", " ").Replace("_1_0", " ").Replace("_1_1", " "));//Data Sources
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");
                    writer.Write(ObjData.ElementAt(i).CompanyName == null ? string.Empty : ObjData.ElementAt(i).CompanyName.Replace(",", " "));
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");
                    writer.Write(ObjData.ElementAt(i).Location == null ? string.Empty : ObjData.ElementAt(i).Location.Replace(",", " "));
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");
                    writer.Write(ObjData.ElementAt(i).UserEmail == null ? string.Empty : ObjData.ElementAt(i).UserEmail.Replace(",", " "));
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");
                    writer.Write(ObjData.ElementAt(i).OrderDt == null ? string.Empty : ObjData.ElementAt(i).OrderDt.ToString().Replace(",", " "));
                    writer.Write("\"");
                    writer.Write(",");
                    writer.Write("\"");
                    writer.Write(ObjData.ElementAt(i).OrderStatusDisplay == null ? string.Empty : ObjData.ElementAt(i).OrderStatusDisplay.ToString().Replace(",", " "));
                    writer.Write("\"");
                    writer.WriteLine();
                }
                writer.Flush();

                output.Position = 0;                
            }
            catch //(Exception)
            {
                throw;
            }
            return File(output, "text/comma-separated-values", "ArchiveOrders.csv");
        }

        private List<Proc_Get_UserInfoByMemberShipUserIdResult> GetCompanyInfo(Guid UserId)
        {
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            try
            {
                List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjDbCollection = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(UserId);
                if (ObjDbCollection.Count > 0)
                {
                    return ObjDbCollection;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }

        public void SavedReportPageLoad(SavedReportModel ObjModel, int CompanyId, int pkLocationId, int pkCompanyUserId, string Keyword, bool ShowIncomplete, string[] Role)
        {
            #region Date
            Dictionary<int, string> ObjDate = new Dictionary<int, string>();
            ObjDate.Add(0, "--All--");
            ObjDate.Add(1, "Today");
            ObjDate.Add(2, "This Month");
            ObjDate.Add(3, "Last Month");
            ObjDate.Add(4, "Specific Month");
            ObjDate.Add(5, "Specific Date");
            ObjDate.Add(6, "Specific Date Range");
            ObjModel.OrderDateColl = ObjDate;
            #endregion
            #region Year
            Dictionary<int, string> ObjYear = new Dictionary<int, string>();
            for (int i = 2008; i <= DateTime.Now.Year; i++)
            {
                ObjYear.Add(i, i.ToString());
            }
            ObjModel.DictYear = ObjYear;
            #endregion
            #region DataSource
            BALProducts ObjBALProducts = new BALProducts();
            List<vwProductsEmerge> ObjProductFirst = ObjBALProducts.GetProducts().Where(t => t.ReportCategoryId != Convert.ToByte(9)).ToList();
            List<vwProductsEmerge> ObjProduct = new List<vwProductsEmerge>();
            ObjProduct = ObjProductFirst.Select(d => new vwProductsEmerge { ProductDisplayName = d.ProductDisplayName + " - " + d.ProductCode, pkProductApplicationId = d.pkProductApplicationId }).ToList();
            //INT-151 
            ObjProduct.Insert(0, new vwProductsEmerge { ProductDisplayName = "--All--", pkProductApplicationId = 0 });
            ObjProduct.Add(new vwProductsEmerge { ProductDisplayName = "      ", pkProductApplicationId = -2 });
            ObjProduct.Add(new vwProductsEmerge { ProductDisplayName = "<< Drug Testing Kits >>", pkProductApplicationId = -4 });
            List<vwProductsEmerge> ObjProductNew = ObjBALProducts.GetProducts().Where(t => t.ReportCategoryId == 9 && !t.IsDeleted).ToList();
            for (int i = 0; i < ObjProductNew.Count; i++)
            {
                string ProductDisplay = ObjProductNew.ElementAt(i).ProductDisplayName + " - " + ObjProductNew.ElementAt(i).ProductCode;
                ObjProduct.Add(new vwProductsEmerge { ProductDisplayName = ProductDisplay, pkProductApplicationId = ObjProductNew.ElementAt(i).pkProductApplicationId });
            }
            ObjModel.ObjProductsEmerge = ObjProduct;
            #endregion
            #region Company
            BALCompany ObjBALCompany = new BALCompany();
            List<tblUsersLocation> listtblCompanyOtherLocation1 = new List<tblUsersLocation>();
            if (Role.Contains("SystemAdmin"))
            {
                var ObjCompany = ObjBALCompany.GetAllCompany(Utility.SiteApplicationId, true, false);
                ObjCompany.Insert(0, new CompanyList { CompanyName = "--All--", pkCompanyId = 0 });
                ObjModel.ObjCompanyList = ObjCompany;
            }
            else
            {
               // MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
                Guid UserId = Utility.GetUID(User.Identity.Name);
                BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
                List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjData = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(UserId);
                var companies = GetCompanyInfoByCompanyId(Utility.SiteApplicationId, true, false, ObjData.First().pkCompanyId);
                ObjModel.ObjCompanyList = companies.ToList();
            }
            ObjModel.pkCompanyId = CompanyId;
            BALLocation ObjBALLocation2 = new BALLocation();
            #endregion
            #region Location
            // ObjLocations Used Mainly for Looping Purpose
            List<Proc_Get_CompanyLocationsByCompanyIdResult> ObjLocations = ObjBALCompany.GetLocationsByCompanyId(CompanyId, Utility.SiteApplicationId);
            //ObjLocations2 Used for finally Return List
            List<Proc_Get_CompanyLocationsByCompanyIdResult> ObjLocations2 = ObjBALCompany.GetLocationsByCompanyId(CompanyId, Utility.SiteApplicationId);
            bool SavedReport6Month = false;
            if (ObjModel.UserRoles.Contains("SystemAdmin"))
            {
                SavedReport6Month = true;
            }
            // Get List From tblOtherLocation
            if (ObjModel.UserRoles.Contains("BasicUser") || ObjModel.UserRoles.Contains("BranchManager"))
            {
                try
                {
                    listtblCompanyOtherLocation1 = ObjBALLocation2.GetLocationsByPkCompanyUserId(pkCompanyUserId);
                    if (listtblCompanyOtherLocation1 != null && listtblCompanyOtherLocation1.Count > 0)
                    {
                        // Clear Previous List For Add Selected Location
                        ObjLocations2.Clear();
                        for (int i = 0; i <= ObjLocations.Count; i++)
                        {
                            // Get single Records On Base of Pklocation For check this location is Checked or Unchecked for Current User.
                            var list = listtblCompanyOtherLocation1.Where(item => item.fklocationId == ObjLocations.ElementAt(i).LocationId).FirstOrDefault(); //  listtblCompanyOtherLocation1.ElementAt(i).fkCompanyUserId).FirstOrDefault();
                            if (list != null)
                            {
                                if (((list.IsHome == true) && (list.fklocationId == ObjLocations.ElementAt(i).LocationId)) || (ObjLocations.ElementAt(i).LocationId == pkLocationId)) // || listtblCompanyOtherLocation1.ElementAt(i).fklocationId != listtblCompanyOtherLocation1.ElementAt(i).fklocationId))
                                {
                                    ObjLocations2.Add(ObjLocations.ElementAt(i));
                                }
                            }
                        }
                    }
                    else
                    {
                        // When Not have any Other Location Only Master Location Will Display In Location Drop-down.
                        ObjLocations2.RemoveAll(item => item.LocationId != pkLocationId);
                    }
                }
                catch { }
            }
            if (!(ObjModel.UserRoles.Contains("ViewOnlyUser")))
            {
                ObjLocations2.Insert(0, new Proc_Get_CompanyLocationsByCompanyIdResult { DisplayLocation = "--All--", LocationId = 0 });
            }
            // Finally Assign Location List On base of different Role's.
            ObjModel.LocationList = ObjLocations2;
            ObjModel.LocationId = pkLocationId;
            #endregion
            #region User List
            BALCompanyUsers ObjBALCompanyUsers1 = new BALCompanyUsers();
            List<Proc_Get_CompanyUsersByLocationIdResult> ObjUsers = ObjBALCompanyUsers1.GetCompanyUsersByLocationId(CompanyId, pkLocationId, Utility.SiteApplicationId);
            ObjUsers.Insert(0, new Proc_Get_CompanyUsersByLocationIdResult { UserEmail = "--All--", CompanyUserId = 0 });
            ObjModel.UserList = ObjUsers;
            ObjModel.CompanyUserId = pkCompanyUserId;
            #endregion
            #region Reference Code List
            List<Proc_GetReferenceCodebyCompanyIdResult> ListRefenceCode = ObjBALCompany.GetReferenceCodeListbypkCompanyId(CompanyId).ToList();
            ListRefenceCode.Insert(0, new Proc_GetReferenceCodebyCompanyIdResult { ReferenceCode = "--All--", pkReferenceCodeId = -1 });
            ObjModel.RefenceCodeList = ListRefenceCode;
            #endregion
            #region Get Count Mails
            #endregion
        }
        private int GetUserInfo(Guid UserId)
        {
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            try
            {
                List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjDbCollection = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(UserId);
                if (ObjDbCollection.Count > 0)
                {
                    return ObjDbCollection.First().pkCompanyUserId;
                }
                else
                {
                    return 0;
                }
            }
            catch
            {
                return 0;
            }
            finally
            {
                ObjBALCompanyUsers = null;
            }
        }

        public List<CompanyList> GetCompanyInfoByCompanyId(Guid ApplicationId, bool Enabled, bool Deleted, int pkCompanyId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                IQueryable<CompanyList> ObjData = from c in DX.tblCompanies
                                                  join l in DX.tblLocations on c.pkCompanyId equals l.fkCompanyId
                                                  join s in DX.tblStates on l.fkStateID equals s.pkStateId

                                                  where c.fkApplicationId == ApplicationId && c.IsDeleted == Deleted && l.IsHome == true && c.pkCompanyId == pkCompanyId
                                                  orderby c.CompanyName ascending
                                                  select new CompanyList
                                                  {
                                                      CompanyName = c.CompanyName + "  [" + l.City + ", " + s.StateCode + "]",
                                                      pkCompanyId = c.pkCompanyId
                                                  };
                return ObjData.ToList();
            }
        }

        public string ReportsWithStatus(string ReportsWithStatus, int OrderId)
        {
            string FormattedProductCode = "";
            try
            {
                if (ReportsWithStatus != null)
                {
                    if (ReportsWithStatus.Trim() != "")
                    {
                        FormattedProductCode = ReportWithImage(ReportsWithStatus.Trim());
                        if (FormattedProductCode == "")
                        {
                            FormattedProductCode = GetProductForReview(OrderId);
                        }
                    }
                    else
                    {
                        FormattedProductCode = GetProductForReview(OrderId);
                    }
                }
                else { }
            }
            catch
            {
            }
            return FormattedProductCode;
        }

        private string ReportWithImage(string Status)
        {
            string ReportNames = Status;
            string Reportwithstatus = "";
            string[] strSplit = ReportNames.Split(',');
            Reportwithstatus += "";
            for (int Irow = 0; Irow < strSplit.Length; Irow++)
            {
                if (strSplit[Irow] != "")
                {
                    string[] subSplit = strSplit[Irow].Split('_');
                    if (subSplit.Count() > 2)
                    {
                        //this for live runner
                        if (subSplit[1] == "1")
                        {
                            Reportwithstatus += subSplit[0] + "<img src=" + @Url.Content("~/Content/themes/base/Images/LiveRunnerSmallLogo.png") + " height='12px' width='12px'/>,";
                        }
                        else
                        {
                            Reportwithstatus += subSplit[0] + ",";
                        }
                    }
                }
            }

            Reportwithstatus += "";
            int CommaIndex = Reportwithstatus.LastIndexOf(',');
            //char[] FormattedProductCodesChar = Reportwithstatus.ToCharArray();

            string FinalString = Reportwithstatus.Remove(CommaIndex, 1);
            return FinalString; // Reportwithstatus;
        }
        private string GetProductForReview(int OrderId)
        {
            string FormattedProductCodes = "";
            BALEmergeReview ObjBALEmergeReview = new BALEmergeReview();
            FormattedProductCodes = "";
            Dictionary<string, string> ObjLiveRunnerProduct = ObjBALEmergeReview.GetProductsForFormatting(OrderId, "liverunner");
            if (ObjLiveRunnerProduct.Count != 0)
            {
                foreach (KeyValuePair<string, string> KV in ObjLiveRunnerProduct)
                {
                    string[] KeyArr = KV.Key.Split('_');
                    string LiveRunner = KeyArr[0].Trim();
                    string ProductCode = KV.Value.Trim();
                    if (LiveRunner.ToLower() == "true" && ProductCode.ToLower() != "rcx")
                    {
                        FormattedProductCodes += KV.Value + "<img src='../Resources/Images/LiveRunnerSmallLogo.png' height='11px' width='11px'/>,";
                    }
                    else
                        FormattedProductCodes += KV.Value + ", ";
                }
            }
            FormattedProductCodes += "";
            int CommaIndex = FormattedProductCodes.LastIndexOf(',');
           // char[] FormattedProductCodesChar = FormattedProductCodes.ToCharArray();

            string FinalString = FormattedProductCodes.Remove(CommaIndex, 1);
            // string FinalString = string.Empty;
            return FinalString;
        }

        #region For TEST Site GetReportCounter
        public string GetReportCounter(List<proc_GetSavedReportsCountResult> ObjCountStatus)
        {
            StringBuilder st = new StringBuilder();
            st.Append("<Select id='kendodropdownlist' style='width:168px;' onchange='onChanged()'>");
            st.Append("<option  id='AllReports'  onclick='BindGrid(0,this.id)'>All </option>");
            st.Append("<option  id='ReadReports' onclick='BindGrid(0,this.id)'>Read </option>");
            st.Append("<option  id='Submitted'  onclick='BindGrid(0,this.id)'>Submit </option>");
            st.Append("<option  id='UnreadReports'  onclick='BindGrid(0,this.id)'>Unread </option>");
            st.Append("<option  id='PendingReports'  onclick='BindGrid(0,this.id)'>Pending </option>");
            st.Append("<option  id='InReviewReports'  onclick='BindGrid(0,this.id)'>In Review </option>");
            st.Append("<option id='IncompleteReports'  onclick='BindGrid(0,this.id)'>Incomplete </option>");
            st.Append("<option id='CompleteReports'   onclick='BindGrid(0,this.id)'>Complete </option>");
            st.Append("</Select>");
            return st.ToString();
        }
        #endregion

        public string GetArchiveReportCounter(List<proc_GetSavedReportsCountResult> ObjCountStatus)
        {
            StringBuilder st = new StringBuilder();
            st.Append("<table style='font-size: 12px;' class='RoundBorder' width='100%'>");
            st.Append("<tr><td><a name='anchorfolder' id='IncompleteReports'>Incomplete </a></td></tr>");
            st.Append("</table>");
            return st.ToString();
        }
        #region Location
        public ActionResult GetCompanyLocationsByCompanyId(int CompanyId)
        {
            BALCompany ObjBALCompany = new BALCompany();
            List<Proc_Get_CompanyLocationsByCompanyIdResult> ObjLocations = ObjBALCompany.GetLocationsByCompanyId(CompanyId, Utility.SiteApplicationId);
            ObjLocations.Insert(0, new Proc_Get_CompanyLocationsByCompanyIdResult { DisplayLocation = "--All--", LocationId = 0 });
            var Location = (from db in ObjLocations select new { DisplayLocation = db.DisplayLocation, LocationId = db.LocationId }).ToList();
            return Json(Location, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region User List
        public JsonResult GetUserAsPerLocation(int CompanyId, int pkLocationId)
        {

            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            List<Proc_Get_CompanyUsersByLocationIdResult> ObjUsers = ObjBALCompanyUsers.GetCompanyUsersByLocationId(CompanyId, pkLocationId, Utility.SiteApplicationId);
            ObjUsers.Insert(0, new Proc_Get_CompanyUsersByLocationIdResult { UserEmail = "--All--", CompanyUserId = 0 });
            return Json(ObjUsers, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Auto Complete

        public ActionResult AutoCompleteResult(string searchText)
        {
            BALOrders ObjOrder = new BALOrders();
            //MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
            Guid UserId = Utility.GetUID(User.Identity.Name);
            bool IsAdmin = true;
            if (Roles.IsUserInRole("SystemAdmin"))
            {
                IsAdmin = true;
            }
            else
            {
                IsAdmin = false;
            }

            var List = ObjOrder.GetAutoCompleteSearchRecords(searchText.Trim(), UserId, IsAdmin).ToList();
            var Coll = List.Select(r => new { label = r.Item, value = r.label });
            return Json(Coll, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SavedReportsAutoComplete(string searchText)
        {
            BALGeneral ObjBALGeneral = new BALGeneral();
            //MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
            Guid UserId = Utility.GetUID(User.Identity.Name);
            bool IsAdmin = true;
            if (Roles.IsUserInRole("SystemAdmin"))
            {
                IsAdmin = true;
            }
            else
            {
                IsAdmin = false;
            }

            var Coll = ObjBALGeneral.GetAutoCompleteForArchive(searchText.Trim(), UserId, IsAdmin).Select(d => d.Item).ToList();

            if (Coll.Count() == 0)
            {
                Coll.Insert(0, "No Record Found");
            }
            return Json(Coll, JsonRequestBehavior.AllowGet);
        }
        public ActionResult checkstateenable(string pkStateId)
        {
            try
            {
                BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
                bool result = ObjBALCompanyUsers.StateCheckStatus(Convert.ToInt32(pkStateId));
                return Json(result);
            }
            catch
            {
                bool result = true;
                return Json(result);
            }
        }
        public ActionResult GetStateCounty(int Zip)
        {
            BALCompanyType ObjBALCompanyType = new BALCompanyType();
            var Result = ObjBALCompanyType.GetCountyState(Zip);
            return Json(new
            {
                CountyName = Result.Item2,
                StateName = Result.Item1
            }
             , JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}
