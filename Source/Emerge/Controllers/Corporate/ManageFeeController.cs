﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Emerge.Data;

namespace Emerge.Controllers.Corporate
{
    public partial class CorporateController : BaseEmergeController
    {
        BALGeneral objGeneral;
        //
        // GET: /StateFee/
        #region StateFee
        public ActionResult StateFee()
        {
            objGeneral = new BALGeneral();
            string Mode = string.Empty;
            if (!string.IsNullOrEmpty(Request.QueryString["cd"]))
            {
                string code = Request.QueryString["cd"].ToString();
                if (code == "SCR")
                {
                    ViewBag.Heading = "SCR Fee";
                    Mode = "1";
                    ViewData["FeeData"] = objGeneral.GetAllStatesByCode("SCR");
                }
                if (code == "MVR")
                {
                    ViewBag.Heading = "MVR Fee";
                    Mode = "2";
                    ViewData["FeeData"] = objGeneral.GetAllStatesByCode("MVR");
                }
            }
            ViewBag.Type = Mode;
            //ViewData["FeeData"] = GetFee();
            return View();
        }

        private List<tblState> GetFee()
        {
            objGeneral = new BALGeneral();
            List<tblState> states = objGeneral.GetAllStates();
            return states;
        }

        #endregion

        #region StateCountyFee
        public ActionResult StateCountyFee()
        {
            string ProductCode = string.Empty;
            objGeneral = new BALGeneral();
            if (!string.IsNullOrEmpty(Request.QueryString["cd"]))
            {
                ProductCode = Convert.ToString(Request.QueryString["cd"]);
            }
            ViewBag.ProductCode = ProductCode;
            List<tblState> states = objGeneral.GetStates();
            List<SelectListItem> listStates = states.Select(x => new SelectListItem { Text = x.StateName, Value = x.pkStateId.ToString() }).ToList();
            ViewData["states"] = listStates;
            ViewBag.Page = !string.IsNullOrEmpty(Request.QueryString["Page"]) ? Request.QueryString["Page"] : string.Empty;
            return View();
        }

        [HttpPost]
        public JsonResult GetStateCounty(string StateId, string Page, string Code, string PageType)
        {
            if (PageType == "ManageReports")
            {
                return Json(new { Data = BuildStringOfStateCounty(StateId, Page, Code) });
            }
            else
            {
                return Json(new { Data = BuildStringOfStateCountyMyReports(StateId, Page, Code) });
            }
        }


        public string BuildStringOfStateCountyMyReports(string StateId, string Page, string Code)
        {

            objGeneral = new BALGeneral();
           // string ProductCode = string.Empty;
            string Content = string.Empty;
            List<proc_GetStateCountyForRCXResult> ObjDataRCX = new List<proc_GetStateCountyForRCXResult>();
            List<proc_GetStateCountyForCCRResult> ObjData = new List<proc_GetStateCountyForCCRResult>();

            //string FolderPath = HttpContext.Server.MapPath("abc");
            //string Substring = Request.Url.AbsoluteUri;

            if (string.IsNullOrEmpty(Page))
            {
                /***************For Corporate******************/
                /***************CCR Page**************/
                Content += "<table border='0' class='tbl_layout' cellspacing='0' style='width:40%'><tr class='ProductsTd'><td align='left' width='200px'>&nbsp;County Name</td>";
                if (!(Code != string.Empty && Code.ToLower() == "rcx"))
                {
                    Content += "<td align='left' id='thFees' runat='server'>&nbsp;Fees</td>";
                }
                if (!(Code != string.Empty && Code.ToLower() == "rcx"))
                {
                    Content += "<td align='center' id='thLiveRunner' runat='server' style='padding-left: 10px; width: 40px'>&nbsp;<img src='" + Emerge.Common.ApplicationPath.GetApplicationPath() + "Content/themes/base/images/LiveRunnerSmallLogo.png' alt='Live Runner' /></td>";
                }
                Content += "<td align='right' id='thRcxFee' runat='server'>&nbsp; Fees</td>";
             //   Content += "<td align='right' id='thdisclaimer' runat='server' ><span style='margin-right:180px'>&nbsp;Disclaimer Data</span></td>";

                Content += "</tr>";
                if (!(Code != string.Empty && Code.ToLower() == "rcx"))
                {
                    ObjData = objGeneral.GetStateCountyForCCR(Convert.ToInt32(StateId), Code);
                    if (ObjData.Count > 0)
                    {
                        foreach (var i in ObjData)
                        {
                            Content += "<tr>";
                            Content += "<td align='left'>" + i.CountyName + "</td>";
                            Content += "<td align='left' id='tdFees' runat='server'>" + i.AdditionalFee + "</td>";
                            Content += "<td align='center' id='tdLiveRunner' runat='server' style='padding-left: 10px;'>";
                            if (i.IsRcxCounty)
                            {
                                Content += "<input type='checkbox' id='chkIsRcxCounty' checked='checked' disabled='disabled' />";
                            }
                            else
                            {
                                Content += "<input type='checkbox' id='chkIsRcxCounty' disabled='disabled' />";
                            }
                            Content += "</td>";
                            Content += "<td align='center' id='tdRcxFee' runat='server' style='padding-left: 15px;text-align: right;'>" + i.RCXFee + "</td>";
                          //  Content += "<td align='center' id='tddisclaimerData' runat='server' style='padding-left: 15px;text-align: right;'>" + i.DisclaimerData + "</td>";
                            Content += "</tr>";
                        }
                    }
                }
                else
                {
                    ObjDataRCX = objGeneral.GetStateCountyForRCX(Convert.ToInt32(StateId));
                    if (ObjDataRCX.Count > 0)
                    {
                        foreach (var i in ObjDataRCX)
                        {
                            Content += "<tr>";
                            Content += "<td align='left'>" + i.CountyName + "</td>";

                            Content += "<td align='center' id='tdRcxFee' runat='server' style='padding-left: 15px;text-align: right;'>" + i.AdditionalFee + "</td>";
                            Content += "</tr>";
                        }
                    }
                }
                Content += "</table>";
            }
            else
            {
                Content += "<table border='0' class='tbl_layout' cellspacing='0'><tr class='ProductsTd'><td align='left' width='200px'>&nbsp;County Name</td>";
                if (!(Code != string.Empty && Code.ToLower() == "rcx"))
                {
                    Content += "<td align='left' id='thFees' runat='server'>&nbsp;Fees</td>";
                }
                if (!(Code != string.Empty && Code.ToLower() == "rcx"))
                {
                    Content += "<td align='center' id='thLiveRunner' runat='server' style='padding-left: 10px;'><img src='" + Emerge.Common.ApplicationPath.GetApplicationPath() + "Content/themes/base/images/LiveRunnerSmallLogo.png' title='Is Live Runner'alt='Live Runner Logo' /></td>";
                }
                //if (Code != string.Empty && Code.ToLower() == "rcx")
                //{
                Content += "<td align='left' id='thRcxFee' runat='server' style='padding-left: 10px;'>&nbsp; Fees</td>";
                //}


                Content += "<td align='center' id='thConsentForm' runat='server' style='padding-left: 10px;'>&nbsp;Consent Form</td></tr>";
              //  Content += "<td align='right' id='thdisclaimer' runat='server'><span style='margin-right:180px'>&nbsp;Disclaimer Data</span></td>";
                int iRow = 0;
                if (!(Code != string.Empty && Code.ToLower() == "rcx"))
                {
                    /**********CCR Page**************/
                    ObjData = objGeneral.GetStateCountyForCCR(Convert.ToInt32(StateId), Code);
                    if (ObjData.Count > 0)
                    {
                        foreach (var i in ObjData)
                        {
                            Content += "<tr><td align='left' width='200px'>" + i.CountyName + "<input type='hidden' name='fkProductId_" + iRow + "' id='fkProductId_" + iRow + "' value='" + i.pkCountyFeeId + "' /> <input type='hidden' name='lblpkCountyId_" + iRow + "' id='lblpkCountyId_" + iRow + "' value='" + i.fkCountyId + "' /> <input type='hidden' name='lblfkProdId_" + iRow + "' id='lblfkProdId_" + iRow + "' value='" + i.fkProductId + "' /></td>";

                            Content += "<td align='left' name='tdFees_" + iRow + "' runat='server'>";
                            Content += "<input type='text'  name='txtCountyFee_" + iRow + "' value='" + i.AdditionalFee + "'  style='width: 80px;' /></td>";


                            Content += "<td align='center' id='tdLiveRunner_" + iRow + "' runat='server' style='padding-left: 10px;'>";
                            if (i.IsRcxCounty)
                            {
                                Content += "<input type='checkbox' name='chkIsRcxCounty_" + iRow + "' runat='server' checked='checked' />";
                            }
                            else
                            {
                                Content += "<input type='checkbox' name='chkIsRcxCounty_" + iRow + "' runat='server' />";
                            }
                            Content += "</td>";

                            Content += "<td align='left' id='tdRcxFee_" + iRow + "' runat='server' style='padding-left: 10px;'>";
                            Content += "<span> " + i.RCXFee + "</span>";
                            Content += "</td>";

                            //}
                            Content += "<td align='center' name='tdConsentForm_" + iRow + "' runat='server' style='padding-left: 10px;'>";

                            if (i.IsConsent)
                            {
                                Content += "<input type='checkbox' name='chkConsentFormCCR_" + iRow + "' checked='checked'  />";
                            }
                            else
                            {
                                Content += "<input type='checkbox' name='chkConsentFormCCR_" + iRow + "'  />";
                            }
                            Content += "</td>";
                            Content += "<td align='left' id='tdDisclaimer_" + iRow + "' runat='server' style='padding-left: 10px;'>";
                        //    Content += "<input type='text'  name='txtDisclaimerData_" + iRow + "' value='" + i.DisclaimerData + "'  style='width: 400px' /></td>";
                            Content += "</tr>";
                            iRow++;
                        }
                    }
                }
                else
                {
                    /**********RCX Page**************/
                    ObjDataRCX = objGeneral.GetStateCountyForRCX(Convert.ToInt32(StateId));
                    if (ObjDataRCX.Count > 0)
                    {
                        foreach (var i in ObjDataRCX)
                        {
                            Content += "<tr><td align='left' width='200px'>" + i.CountyName + "<input type='hidden' name='fkProductId_" + iRow + "' id='fkProductId_" + iRow + "' value='" + i.pkCountyFeeId + "' /> <input type='hidden' name='lblpkCountyId_" + iRow + "' id='lblpkCountyId_" + iRow + "' value='" + i.fkCountyId + "' /><input type='hidden' name='lblfkProdId_" + iRow + "' id='lblfkProdId_" + iRow + "' value='" + i.fkProductId + "' /></td>";

                            Content += "<td align='left' id='tdFees_" + iRow + "' runat='server' style='display:none;'>&nbsp;</td>";


                            Content += "<td align='center' id='tdLiveRunner_" + iRow + "' runat='server' style='padding-left: 10px;display:none;'>&nbsp;</td>";
                            Content += "<td align='left' id='tdRcxFee_" + iRow + "' runat='server' style='padding-left: 10px;'>";

                            Content += "<input type='text' style='width: 80px;' name='txtRcxFees_" + iRow + "' value='" + i.AdditionalFee + "'  />";

                            Content += "</td>";

                            //}
                            Content += "<td align='center' name='tdConsentForm_" + iRow + "' runat='server' style='padding-left: 10px;'>";

                            if (i.IsConsent)
                            {
                                Content += "<input type='checkbox' name='chkConsentFormRCX_" + iRow + "'   checked='checked'  />";
                            }
                            else
                            {
                                Content += "<input type='checkbox' name='chkConsentFormRCX_" + iRow + "'  />";
                            }

                            Content += "</td>";
                            Content += "<td align='left' id='tdDisclaimer_" + iRow + "' runat='server' style='padding-left: 10px;'>";

                           // Content += "<input type='text' style='width: 400px' name='txtDisclaimerData_" + iRow + "' value='" + i.DisclaimerData + "'  />";

                            Content += "</td>";

                            Content += "</tr>";
                            iRow++;
                        }
                    }
                }

                Content += "</table>";
            }

            return Content;
        }
        public string BuildStringOfStateCounty(string StateId, string Page, string Code)
        {
            objGeneral = new BALGeneral();
           // string ProductCode = string.Empty;
            string Content = string.Empty;
            List<proc_GetStateCountyForRCXResult> ObjDataRCX = new List<proc_GetStateCountyForRCXResult>();
            List<proc_GetStateCountyForCCRResult> ObjData = new List<proc_GetStateCountyForCCRResult>();

           // string FolderPath = HttpContext.Server.MapPath("abc");
            //string Substring = Request.Url.AbsoluteUri;

            if (string.IsNullOrEmpty(Page))
            {
                /***************For Corporate******************/
                /***************CCR Page**************/
                Content += "<table border='0' class='tbl_layout' cellspacing='0' style='width:40%'><tr class='ProductsTd'><td align='left' width='200px'>&nbsp;County Name</td>";
                if (!(Code != string.Empty && Code.ToLower() == "rcx"))
                {
                    Content += "<td align='left' id='thFees' runat='server'>&nbsp;Fees</td>";
                }
                if (!(Code != string.Empty && Code.ToLower() == "rcx"))
                {
                    Content += "<td align='center' id='thLiveRunner' runat='server' style='padding-left: 10px; width: 40px'>&nbsp;<img src='"+Emerge.Common.ApplicationPath.GetApplicationPath()+"Content/themes/base/images/LiveRunnerSmallLogo.png' alt='Live Runner' /></td>";
                }
                Content += "<td align='right' id='thRcxFee' runat='server'>&nbsp; Fees</td>";
                Content += "<td align='right' id='thdisclaimer' runat='server' ><span style='margin-right:180px'>&nbsp;Disclaimer Data</span></td>";

                Content += "</tr>";
                if (!(Code != string.Empty && Code.ToLower() == "rcx"))
                {
                    ObjData = objGeneral.GetStateCountyForCCR(Convert.ToInt32(StateId),Code);
                    if (ObjData.Count > 0)
                    {
                        foreach (var i in ObjData)
                        {
                            Content += "<tr>";
                            Content += "<td align='left'>" + i.CountyName + "</td>";
                            Content += "<td align='left' id='tdFees' runat='server'>" + i.AdditionalFee + "</td>";
                            Content += "<td align='center' id='tdLiveRunner' runat='server' style='padding-left: 10px;'>";
                            if (i.IsRcxCounty)
                            {
                                Content += "<input type='checkbox' id='chkIsRcxCounty' checked='checked' disabled='disabled' />";
                            }
                            else
                            {
                                Content += "<input type='checkbox' id='chkIsRcxCounty' disabled='disabled' />";
                            }
                            Content += "</td>";
                            Content += "<td align='center' id='tdRcxFee' runat='server' style='padding-left: 15px;text-align: right;'>" + i.RCXFee + "</td>";
                            Content += "<td align='center' id='tddisclaimerData' runat='server' style='padding-left: 15px;text-align: right;'>" + i.DisclaimerData + "</td>";
                            Content += "</tr>";
                        }
                    }
                }
                else
                {
                    ObjDataRCX = objGeneral.GetStateCountyForRCX(Convert.ToInt32(StateId));
                    if (ObjDataRCX.Count > 0)
                    {
                        foreach (var i in ObjDataRCX)
                        {
                            Content += "<tr>";
                            Content += "<td align='left'>" + i.CountyName + "</td>";

                            Content += "<td align='center' id='tdRcxFee' runat='server' style='padding-left: 15px;text-align: right;'>" + i.AdditionalFee + "</td>";
                            Content += "</tr>";
                        }
                    }
                }
                Content += "</table>";
            }
            else
            {
                Content += "<table border='0' class='tbl_layout' cellspacing='0'><tr class='ProductsTd'><td align='left' width='200px'>&nbsp;County Name</td>";
                if (!(Code != string.Empty && Code.ToLower() == "rcx"))
                {
                    Content += "<td align='left' id='thFees' runat='server'>&nbsp;Fees</td>";
                }
                if (!(Code != string.Empty && Code.ToLower() == "rcx"))
                {
                    Content += "<td align='center' id='thLiveRunner' runat='server' style='padding-left: 10px;'><img src='"+Emerge.Common.ApplicationPath.GetApplicationPath()+"Content/themes/base/images/LiveRunnerSmallLogo.png' title='Is Live Runner'alt='Live Runner Logo' /></td>";
                }
                //if (Code != string.Empty && Code.ToLower() == "rcx")
                //{
                Content += "<td align='left' id='thRcxFee' runat='server' style='padding-left: 10px;'>&nbsp; Fees</td>";
                //}


                Content += "<td align='center' id='thConsentForm' runat='server' style='padding-left: 10px;'>&nbsp;Consent Form</td>";
                Content += "<td align='right' id='thdisclaimer' runat='server'><span style='margin-right:180px'>&nbsp;Disclaimer Data</span></td></tr>";
                int iRow = 0;
                if (!(Code != string.Empty && Code.ToLower() == "rcx"))
                {
                    /**********CCR Page**************/
                    ObjData = objGeneral.GetStateCountyForCCR(Convert.ToInt32(StateId), Code);
                    if (ObjData.Count > 0)
                    {
                        foreach (var i in ObjData)
                        {
                            Content += "<tr><td align='left' width='200px'>" + i.CountyName + "<input type='hidden' name='fkProductId_" + iRow + "' id='fkProductId_" + iRow + "' value='" + i.pkCountyFeeId + "' /> <input type='hidden' name='lblpkCountyId_" + iRow + "' id='lblpkCountyId_" + iRow + "' value='" + i.fkCountyId + "' /> <input type='hidden' name='lblfkProdId_" + iRow + "' id='lblfkProdId_" + iRow + "' value='" + i.fkProductId + "' /></td>";

                            Content += "<td align='left' name='tdFees_" + iRow + "' runat='server'>";
                            Content += "<input type='text'  name='txtCountyFee_" + iRow + "' value='" + i.AdditionalFee + "'  style='width: 80px;' /></td>";


                            Content += "<td align='center' id='tdLiveRunner_" + iRow + "' runat='server' style='padding-left: 10px;'>";
                            if (i.IsRcxCounty)
                            {
                                Content += "<input type='checkbox' name='chkIsRcxCounty_" + iRow + "' runat='server' checked='checked' />";
                            }
                            else
                            {
                                Content += "<input type='checkbox' name='chkIsRcxCounty_" + iRow + "' runat='server' />";
                            }
                            Content += "</td>";

                            Content += "<td align='left' id='tdRcxFee_" + iRow + "' runat='server' style='padding-left: 10px;'>";
                            Content += "<span> " + i.RCXFee + "</span>";
                            Content += "</td>";

                            //}
                            Content += "<td align='center' name='tdConsentForm_" + iRow + "' runat='server' style='padding-left: 10px;'>";
                            
                                if (i.IsConsent)
                                {
                                    Content += "<input type='checkbox' name='chkConsentFormCCR_" + iRow + "' checked='checked'  />";
                                }
                                else
                                {
                                    Content += "<input type='checkbox' name='chkConsentFormCCR_" + iRow + "'  />";
                                }
                            Content += "</td>";
                            Content += "<td align='left' id='tdDisclaimer_" + iRow + "' runat='server' style='padding-left: 10px;'>";
                            Content += "<input type='text'  name='txtDisclaimerData_" + iRow + "' value='" + i.DisclaimerData + "'  style='width: 400px' /></td>"; 
                            Content += "</tr>";
                            iRow++;
                        }
                    }
                }
                else
                {
                    /**********RCX Page**************/
                    ObjDataRCX = objGeneral.GetStateCountyForRCX(Convert.ToInt32(StateId));
                    if (ObjDataRCX.Count > 0)
                    {
                        foreach (var i in ObjDataRCX)
                        {
                            Content += "<tr><td align='left' width='200px'>" + i.CountyName + "<input type='hidden' name='fkProductId_" + iRow + "' id='fkProductId_" + iRow + "' value='" + i.pkCountyFeeId + "' /> <input type='hidden' name='lblpkCountyId_" + iRow + "' id='lblpkCountyId_" + iRow + "' value='" + i.fkCountyId + "' /><input type='hidden' name='lblfkProdId_" + iRow + "' id='lblfkProdId_" + iRow + "' value='" + i.fkProductId + "' /></td>";

                            Content += "<td align='left' id='tdFees_" + iRow + "' runat='server' style='display:none;'>&nbsp;</td>";


                            Content += "<td align='center' id='tdLiveRunner_" + iRow + "' runat='server' style='padding-left: 10px;display:none;'>&nbsp;</td>";
                            Content += "<td align='left' id='tdRcxFee_" + iRow + "' runat='server' style='padding-left: 10px;'>";

                            Content += "<input type='text' style='width: 80px;' name='txtRcxFees_" + iRow + "' value='" + i.AdditionalFee + "'  />";
                           
                            Content += "</td>";

                            //}
                            Content += "<td align='center' name='tdConsentForm_" + iRow + "' runat='server' style='padding-left: 10px;'>";

                            if (i.IsConsent)
                            {
                                Content += "<input type='checkbox' name='chkConsentFormRCX_" + iRow + "'   checked='checked'  />";
                            }
                            else
                            {
                                Content += "<input type='checkbox' name='chkConsentFormRCX_" + iRow + "'  />";
                            }

                            Content += "</td>";
                            Content += "<td align='left' id='tdDisclaimer_" + iRow + "' runat='server' style='padding-left: 10px;'>";

                            Content += "<input type='text' style='width: 400px' name='txtDisclaimerData_" + iRow + "' value='" + i.DisclaimerData + "'  />";

                            Content += "</td>";

                            Content += "</tr>";
                            iRow++;
                        }
                    }
                }

                Content += "</table>";
            }

            return Content;
        }

        private List<proc_GetStateCountyForRCXResult> GetStateCountry(string pkId)
        {
            objGeneral = new BALGeneral();
            List<proc_GetStateCountyForRCXResult> collec = objGeneral.GetStateCountyForRCX(Convert.ToInt32(pkId));
            return collec;

        }
        #endregion
    }
}
