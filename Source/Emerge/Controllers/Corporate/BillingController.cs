﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Emerge.Models;
using Emerge.Data;
using Emerge.Common;
using System.Web.Security;
using System.Globalization;

namespace Emerge.Controllers.Corporate
{
    public partial class CorporateController : BaseEmergeController
    {
        //BALBillingData ObjBALBillingData = new BALBillingData();
        Guid SiteApplicationId = Emerge.Common.Utility.SiteApplicationId;// new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852");

        public ActionResult GetOrderDetailsbySelectedOrderNo(string orderNo, string AdjustmentType)
        {
            List<proc_GetOrderDetailsByOrderNoResult1> lst = new List<proc_GetOrderDetailsByOrderNoResult1>();
            try
            {
                lst = new BALInvoice().getOrderDetailsByOrderNo(orderNo, AdjustmentType);
            }
            catch
            {
            }
            return Json(lst);
        }




        public ActionResult BillingStatements()
        {
            if (Request.QueryString["Type"] != null)
            {
                int Id = Convert.ToInt32(Request.QueryString["Type"]);
                ViewBag.OpenPopUp = Id;
            }
            else
            {
                ViewBag.OpenPopUp = -2;
            }
            ViewBag.PageFor = "Corporate";
            return View();
        }

        /// <summary>
        /// To get the Open Invoice Details.
        /// </summary>
        /// <param name="CompanyId"></param>
        /// <param name="PageType"></param>
        /// <returns></returns>
        public ActionResult GetOpenInvoiceAndRemainingBalance(string CompanyId, string PageType)
        {
            string content = string.Empty;
            string[] role = new string[0];
            bool flagInvoiceDate = false;
            bool flagInvoiceDueDate = false;
            //AdCompanyModel objmodel = new AdCompanyModel();
            List<proc_LoadOpenInvoiceByCompanyIdResult> list = new List<proc_LoadOpenInvoiceByCompanyIdResult>();


            List<proc_LoadOpenInvoiceByInvoiceIdDetailsResult1> list_InvoiceDetails = new List<proc_LoadOpenInvoiceByInvoiceIdDetailsResult1>();



            List<string> chkStatementBalance = new List<string>();
            decimal RemainingBalance = 0;
            if (string.IsNullOrEmpty(CompanyId) || CompanyId == null)
            {
                content = "";
            }
            else
            {
                //AdCompanyModel ObjModel = new AdCompanyModel();
                BALCompany serObj = new BALCompany();
                int CID = int.Parse(CompanyId);
                int chkPaymentTerm = serObj.GetCompanyPaymentType(CID);
                ViewBag.chkPaymentTerm = chkPaymentTerm;
                if (chkPaymentTerm > 1)//If it is normal Invoice Process (Net10, Net30 etc).
                {
                    list = serObj.LoadOpenInVoice(CID).ToList();





                    if (list.Count > 0)
                    {
                        if (Session["UserRoles"] != null)
                        {
                            role = (string[])(Session["UserRoles"]);
                            if (role.Contains("SystemAdmin"))
                            {
                                //content += "<div style='float:right;'><img href><a onclick=\"OpenAdjustmentDiscount()\" ><img src='" + ApplicationPath.GetApplicationPath() + "Content/themes/base/Images/discount_button.png' alt='' /> </a></div>";

                                content += "<div style='float:right;'><a onclick=\"OpenAdjustment()\" ><img src='" + ApplicationPath.GetApplicationPath() + "Content/themes/base/Images/adjustment_per_order.png' alt='' /> </a> &nbsp;&nbsp;<a onclick=\"OpenDiscount()\" ><img src='" + ApplicationPath.GetApplicationPath() + "Content/themes/base/Images/discount_per_order.png' alt='' /> </a> &nbsp;&nbsp;<a onclick=\"OpenAdjustmentDiscount()\" ><img src='" + ApplicationPath.GetApplicationPath() + "Content/themes/base/Images/discount_button.png' alt='' /> </a></div>";
                            }
                        }
                        content += "</br><table align='Center' cellspacing='0' cellpadding='4' border='1' style='background-color: White;width:100%; border-collapse: collapse;float: left;' id='gvOpenInvoice' rules='all' class='Grid GriddataFinal'>";
                        content += "<tbody><tr class='GridHeader' align='center' style='font-weight: bold;color: #0B55C4;'><th scope='col' style='width: 24px;height: 32px;'> </th><th scope='col'>Invoice #</th><th scope='col' style='width: 230px;'>Invoice Date</th><th scope='col'>Due Date</th><th style='width: 127px;' scope='col'>Invoice Amount</th><th style='width: 125px;' scope='col'>Additional Charges</th><th style='width:110px;' scope='col'>Credit </th><th style='width:110px;' scope='col'>Amount Paid</th><th style='width: 127px;' scope='col'>Remaining Balance</th><th scope='col'> Action </th></tr>";
                        content += "</tr>";
                        for (var i = 0; i < list.Count; i++)
                        {
                            list_InvoiceDetails = serObj.LoadOpenInVoice_PerDate(CID, Convert.ToInt32(list.ElementAt(i).InvoiceNumber.ToString())).ToList();
                            string DisplayText = "";
                            content += "<tr valign='middle' align='left' style='border-color: #BBBBBB; border-width: 1px;border-style: solid;' class='GridRow'>";
                            content += "<td>" + (i + 1) + "</td>";
                            if (list.ElementAt(i).InvoiceDueDate == null)
                            {
                                content += "<td>" + "Current" + "</td>";
                            }
                            else
                            {
                                content += "<td>" + list.ElementAt(i).InvoiceNumber + "</td>";
                            }


                            //If Invoice Date Exists.

                            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();


                           // string strMonthName = mfi.GetMonthName(Convert.ToInt32(list.ElementAt(i).InvoiceMonth)).ToString();

                            if (list.ElementAt(i).InvoiceDate != null)
                            {
                                content += "<td align='left'>" + mfi.GetMonthName(Convert.ToInt32(list.ElementAt(i).InvoiceMonth)).ToString() + " " + list.ElementAt(i).InvoiceYear.ToString() + "<br>" + "(" + list_InvoiceDetails.ElementAt(0).MinInvoiceDate.ToString() + " To " + list_InvoiceDetails.ElementAt(0).MaxInvoiceDate.ToString() + ")" + "</td>";
                                flagInvoiceDate = true;
                            }
                            else//If Invoice Date not exists.
                            {
                                //content += "<td>  -  </td>";Removed Month and Year column from the Grid.
                                //content += "<td align='left'>     </td>";

                                content += "<td align='left'>" + mfi.GetMonthName(Convert.ToInt32(list.ElementAt(i).InvoiceMonth)).ToString() + " " + list.ElementAt(i).InvoiceYear.ToString() + "<br>" + "(" + list_InvoiceDetails.ElementAt(0).MinInvoiceDate.ToString() + " To " + list_InvoiceDetails.ElementAt(0).MaxInvoiceDate.ToString() + ")" + "</td>";
                                flagInvoiceDate = false;
                            }

                            //If due date exists.
                            if (list.ElementAt(i).InvoiceDueDate != null)
                            {
                                //if Invoice Date less than current date, means Due Date Payment.
                                if (list.ElementAt(i).InvoiceDueDate < DateTime.Now)
                                {
                                    content += "<td align='left'>" + string.Format("{0:MMM d, yyyy}", list.ElementAt(i).InvoiceDueDate) + "&nbsp;<span style='color: Red;' >[Past Due]</span></td>";
                                    DisplayText = "Statement Balance (#" + list.ElementAt(i).InvoiceNumber + "): <b>" + String.Format("{0:C}", list.ElementAt(i).RemainingBalance) + "</b>  due " + list.ElementAt(i).InvoiceDueDate.Value.ToShortDateString() + "<span style='color:red;'>[Past Due]</span>";
                                }
                                else
                                {
                                    content += "<td align='left'>" + string.Format("{0:MMM d, yyyy}", list.ElementAt(i).InvoiceDueDate) + "</td>";
                                    DisplayText = "Statement Balance (#" + list.ElementAt(i).InvoiceNumber + "): <b>" + String.Format("{0:C}", list.ElementAt(i).RemainingBalance) + "</b>  due " + list.ElementAt(i).InvoiceDueDate.Value.ToShortDateString() + "";
                                }
                                flagInvoiceDueDate = true;
                            }//If due date not exists.
                            else
                            {
                                content += "<td align='left'>    </td>";
                                DisplayText = " ";
                                flagInvoiceDueDate = false;
                            }
                            content += "<td align='right'>$" + Convert.ToDecimal(list.ElementAt(i).InvoiceAmount).ToString("#,##0.00") + "</td>";
                            content += "<td align='right'>$" + Convert.ToDecimal(list.ElementAt(i).InvoiceAdjustAmount).ToString("#,##0.00") + "</td>";
                            content += "<td align='right'>$" + Convert.ToDecimal(list.ElementAt(i).InvoiceDiscount).ToString("#,##0.00") + "</td>";
                            content += "<td align='right'>$" + Convert.ToDecimal(list.ElementAt(i).AmountPaid).ToString("#,##0.00") + "</td>";
                            content += "<td align='right'>$" + Convert.ToDecimal(list.ElementAt(i).RemainingBalance).ToString("#,##0.00") + "</td>";
                            if (list.ElementAt(i).InvoiceDate != null && list.ElementAt(i).InvoiceDueDate != null)
                            {
                                content += "<td align='center'><a onclick=\"OpenPayInvoice_info('" + list.ElementAt(i).InvoiceNumber + "')\" ><img src='" + ApplicationPath.GetApplicationPath() + "Content/themes/base/Images/paymentamountreport.jpg'  width= 25 height=25 alt='' /> </a><a onclick=\"OpenPayInvoice('" + i + "')\" ><img src='" + ApplicationPath.GetApplicationPath() + "Content/themes/base/Images/Payinvoice1.png' alt='' /> </a></td>";
                            }
                            else
                            {
                                content += "<td align='left'> </td>";
                            }
                            content += "</tr>";
                            //
                            chkStatementBalance.Add(list.ElementAt(i).pkInvoiceId.ToString() + "_" + DisplayText);
                        }
                        RemainingBalance = Convert.ToDecimal(list.Sum(x => x.RemainingBalance));
                        content += "<tr align='left' style='color: #0B55C4; background-color: #FFFFDD; font-weight: bold;'><td>&nbsp;</td><td>Total</td><td>&nbsp;</td><td>&nbsp;</td><td ><span >$" + Convert.ToDecimal(list.Sum(x => x.InvoiceAmount)).ToString("#,##0.00") + "</span></td><td ><span >$" + Convert.ToDecimal(list.Sum(x => x.InvoiceAdjustAmount)).ToString("#,##0.00") + "</span></td><td ><span >$" + Convert.ToDecimal(list.Sum(x => x.InvoiceDiscount)).ToString("#,##0.00") + "</span></td><td ><span >$" + Convert.ToDecimal(list.Sum(x => x.AmountPaid)).ToString("#,##0.00") + "</span></td><td ><span >$" + Convert.ToDecimal(list.Sum(x => x.RemainingBalance)).ToString("#,##0.00") + "</span></td>";
                        if (flagInvoiceDate == true && flagInvoiceDueDate == true)
                        {
                            //content += "<td align='center'><a onclick=\"OpenPayInvoice('-1')\" ><img src='" + ApplicationPath.GetApplicationPath() + "Content/themes/base/Images/Payallinvoice1.png' alt='' /> </a></td>";

                            content += "<td align='left'></td>";
                        }
                        else
                        {
                            content += "<td align='left'></td>";
                        }
                        content += "</tr>";
                        content += "</tbody>";
                        content += "</table>";

                    }
                    else
                    {
                        content = " <div id='nullrecord' style='margin-left: 37%'><h4><span class='successmsg'> There is no open invoice avaliable for this company</span></h4></div>";
                    }

                }
                else if (chkPaymentTerm == 1)//for Prepayment process
                {
                    decimal preValue = new BALPayment().GetPrePaymentAmountByCompanyId(CID);
                    content += "<table><tr><td><b>Current Balance: </td><td><span id='spanRemainingBalance'><b>$" + Convert.ToString(preValue) + "</b></span></td><td colspan='2'><input type='Button' value='Add to balance' onclick='OpenPrePayment();'></td></tr></table></div>";
                }
                else if (chkPaymentTerm == 0)//for Due on Receipt 
                {
                    list = serObj.LoadOpenInVoice(CID).ToList();
                    if (list.Count > 0)
                    {
                        if (Session["UserRoles"] != null)
                        {
                            role = (string[])(Session["UserRoles"]);
                            if (role.Contains("SystemAdmin"))
                            {
                                //On Due On Receipt, There will not be any process for Adjustment and discount.
                                // content += "<div style='float:right;'><img href><a onclick=\"OpenAdjustmentDiscount()\" ><img src='" + ApplicationPath.GetApplicationPath() + "Content/themes/base/Images/discount_button.png' alt='' /> </a></div>";
                            }
                        }
                        content += "<table align='Center' cellspacing='0' cellpadding='4' border='1' style='background-color: White;width:100%; border-collapse: collapse;float: left;' id='gvOpenInvoice' rules='all' class='Grid'>";
                        content += "<tbody><tr align='center' style='font-weight: bold;color: #0B55C4;' class='GridHeader'><th scope='col'>&nbsp;</th><th scope='col'>Invoice #</th><th scope='col'>Invoice Date</th><th scope='col'>Due Date</th><th scope='col' style='width:110px;text-align:right;'>Invoice Amount</th><th scope='col' style='width:110px;text-align:right;'>Amount Paid</th><th scope='col' style='width:110px;text-align:right;'>Remaining Balance</th>";
                        content += "<th scope='col'> Action </th>";
                        content += "</tr>";
                        for (var i = 0; i < list.Count; i++)
                        {
                            string DisplayText = "";
                            content += "<tr valign='middle' align='left' style='border-color: #BBBBBB; border-width: 1px;border-style: solid;' class='GridRow'>";
                            content += "<td>" + (i + 1) + "</td>";
                            content += "<td>" + list.ElementAt(i).InvoiceNumber + "</td>";
                            //If Invoice Date Exists.

                            System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();

                            if (list.ElementAt(i).InvoiceDate != null)
                            {
                                content += "<td align='left'>" + mfi.GetMonthName(Convert.ToInt32(list.ElementAt(i).InvoiceMonth)).ToString() + " " + list.ElementAt(i).InvoiceYear.ToString() + "</td>";
                                flagInvoiceDate = true;
                            }
                            else//If Invoice Date not exists.
                            {
                                //content += "<td>  -  </td>";Removed Month and Year column from the Grid.
                                content += "<td align='left'>     </td>";
                                flagInvoiceDate = false;
                            }

                            //If due date exists.
                            if (list.ElementAt(i).InvoiceDueDate != null)
                            {
                                //if Invoice Date less than current date, means Due Date Payment.
                                if (list.ElementAt(i).InvoiceDueDate <= DateTime.Now)
                                {
                                    content += "<td align='left'>" + string.Format("{0:MMM d, yyyy}", list.ElementAt(i).InvoiceDueDate) + "&nbsp;<span style='color: Red;' >[Past Due]</span></td>";
                                    DisplayText = "Statement Balance (#" + list.ElementAt(i).InvoiceNumber + "): <b>" + String.Format("{0:C}", list.ElementAt(i).RemainingBalance) + "</b>  due " + list.ElementAt(i).InvoiceDueDate.Value.ToShortDateString() + "<span style='color:red;'>[Past Due]</span>";
                                }
                                else
                                {
                                    content += "<td align='left'>" + string.Format("{0:MMM d, yyyy}", list.ElementAt(i).InvoiceDueDate) + "</td>";
                                    DisplayText = "Statement Balance (#" + list.ElementAt(i).InvoiceNumber + "): <b>" + String.Format("{0:C}", list.ElementAt(i).RemainingBalance) + "</b>  due " + list.ElementAt(i).InvoiceDueDate.Value.ToShortDateString() + "";
                                }
                                flagInvoiceDueDate = true;
                            }//If due date not exists.
                            else
                            {
                                content += "<td align='left'>    </td>";
                                DisplayText = " ";
                                flagInvoiceDueDate = false;
                            }
                            content += "<td align='right'>$" + Convert.ToDecimal(list.ElementAt(i).InvoiceAmount).ToString("#,##0.00") + "</td>";
                            content += "<td align='right'>$" + Convert.ToDecimal(list.ElementAt(i).AmountPaid).ToString("#,##0.00") + "</td>";
                            content += "<td align='right'>$" + Convert.ToDecimal(list.ElementAt(i).RemainingBalance).ToString("#,##0.00") + "</td>";
                            if (list.ElementAt(i).InvoiceDate != null && list.ElementAt(i).InvoiceDueDate != null)
                            {
                                content += "<td align='center'><a onclick=\"OpenPayInvoice_info('" + list.ElementAt(i).InvoiceNumber + "')\" ><img src='" + ApplicationPath.GetApplicationPath() + "Content/themes/base/Images/paymentamountreport.jpg'  width= 25 height=25 alt='' /> </a><a onclick=\"OpenPayInvoice('" + i + "')\" ><img src='" + ApplicationPath.GetApplicationPath() + "Content/themes/base/Images/Payinvoice1.png' alt='' /> </a></td>";
                            }
                            else
                            {
                                content += "<td align='left'> </td>";
                            }
                            content += "</tr>";
                            //
                            chkStatementBalance.Add(list.ElementAt(i).pkInvoiceId.ToString() + "_" + DisplayText);
                        }
                        RemainingBalance = Convert.ToDecimal(list.Sum(x => x.RemainingBalance));

                        content += "<tr align='left' style='color: #0B55C4; background-color: #FFFFDD; font-weight: bold;'><td>&nbsp;</td><td>Total</td><td>&nbsp;</td><td>&nbsp;</td><td style='text-align:right;'><span >$" + Convert.ToDecimal(list.Sum(x => x.InvoiceAmount)).ToString("#,##0.00") + "</span></td><td style='text-align: right;'><span >$" + Convert.ToDecimal(list.Sum(x => x.AmountPaid)).ToString("#,##0.00") + "</span></td><td style='text-align:right;'><span >$" + Convert.ToDecimal(list.Sum(x => x.RemainingBalance)).ToString("#,##0.00") + "</span></td>";
                        if (flagInvoiceDate == true && flagInvoiceDueDate == true)
                        {
                            //content += "<td align='center'><a onclick=\"OpenPayInvoice('-1')\" ><img src='" + ApplicationPath.GetApplicationPath() + "Content/themes/base/Images/Payallinvoice1.png' alt='' /> </a></td>";
                            content += "<td align='left'></td>";
                        }
                        else
                        {
                            content += "<td align='left'></td>";
                        }
                        content += "</tr>";
                        content += "</tbody>";
                        content += "</table>";

                    }
                    else
                    {
                        content = " <div id='nullrecord' style='margin-left: 37%'><h4><span class='successmsg'> There is no open invoice avaliable for this company</span></h4></div>";
                    }
                }

            }


            return Json(new { content = content, list = list, chkStatementBalance = chkStatementBalance, RemainingBalance = RemainingBalance });
        }
        /// <summary>
        /// To Get invoice Ids based on the Company.
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public ActionResult FillInvoicesByCompanyId(string companyId)
        {
            List<SelectListItem> lstInvoice = new List<SelectListItem>();
            try
            {
                int CID = Int32.Parse(companyId);
                var Invoice = new BALInvoice().LoadInvoiceNumberByCompanyId(CID);
                if (Invoice.Count() > 0)
                {
                    lstInvoice.Add(new SelectListItem { Text = "--Select--", Value = "0" });
                    foreach (var ch in Invoice)
                    {

                        lstInvoice.Add(new SelectListItem { Text = ch.InvoiceNumber, Value = ch.pkInvoiceId.ToString() });
                    }
                }
            }
            catch
            {
            }
            return Json(lstInvoice);

        }


        public ActionResult FillInvoicesByCompanyIdforPayment(string companyId)
        {
            List<SelectListItem> lstInvoice = new List<SelectListItem>();
            try
            {
                int CID = Int32.Parse(companyId);
                var Invoice = new BALInvoice().LoadInvoiceNumberbyFillInvoicesByCompanyIdforPayment(CID);
                if (Invoice.Count() > 0)
                {
                    lstInvoice.Add(new SelectListItem { Text = "--Select--", Value = "0" });
                    foreach (var ch in Invoice)
                    {

                        lstInvoice.Add(new SelectListItem { Text = ch.InvoiceNumber, Value = ch.pkInvoiceId.ToString() });
                    }
                }
            }
            catch
            {
            }
            return Json(lstInvoice);

        }









        /// <summary>
        /// To Lock User after Due Date.
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public ActionResult lockUserAfterDueDate(string companyId)
        {
            bool LockAccount = false;
            try
            {
                int CID = Int32.Parse(companyId);
                LockAccount = new BALInvoice().LockUserAccountBasedOnPayment(CID);
            }
            catch
            {
            }
            return Json(new { Result = LockAccount }, JsonRequestBehavior.AllowGet);

        }
        /// <summary>
        /// To Get Due Payement Date and Amount
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public ActionResult DuePaymentDetails(int companyId)
        {
            List<string> lst = new List<string>();
            try
            {
                int CID = companyId;
                lst = new BALInvoice().GetDuePaymentDetails(CID);
            }
            catch
            {
            }
            return Json(new { Result = lst }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// To Update the Invoice based on Discount given by Admin
        /// </summary>
        /// <param name="pkInvoiceId"></param>
        /// <param name="DiscountAmount"></param>
        /// <param name="DiscountDesc"></param>
        /// <returns></returns>

        public ActionResult UpdateInvoiceDiscount(string pkInvoiceId, decimal DiscountAmount, string DiscountDesc)
        {
            int retVal = 0;
            try
            {
                long invoiceId = Convert.ToInt64(pkInvoiceId);
                retVal = new BALInvoice().UpdateInvoiceDiscount(invoiceId, Convert.ToDecimal(DiscountAmount), DiscountDesc, DateTime.Now);

            }
            catch
            {
            }
            return Json(new { Result = retVal }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// To Get Invoice amount based on Invoice Id.
        /// </summary>
        /// <param name="pkInvoiceId"></param>
        /// <returns></returns>
        public ActionResult GetInvoiceAmountById(string pkInvoiceId)
        {
            decimal retVal = 0;

            try
            {
                long invoiceId = Convert.ToInt64(pkInvoiceId);
                retVal = new BALInvoice().GetSelectedInvoiceAmount(invoiceId);
            }
            catch
            {
            }
            return Json(new { Result = retVal }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// To Get the Data Source. 
        /// </summary>
        /// <returns></returns>
        public ActionResult GetDataSources()
        {
            List<vwProductsEmerge> ObjProduct = new List<vwProductsEmerge>();
            List<SelectListItem> lstDataSource = new List<SelectListItem>();
            BALProducts ObjBALProducts = new BALProducts();
            try
            {
                List<vwProductsEmerge> ObjProductFirst = ObjBALProducts.GetProducts().Where(t => t.ReportCategoryId != Convert.ToByte(9)).ToList();
                ObjProduct = ObjProductFirst.Select(d => new vwProductsEmerge { ProductDisplayName = d.ProductDisplayName + " - " + d.ProductCode, pkProductApplicationId = d.pkProductApplicationId }).ToList();
                ObjProduct.Insert(0, new vwProductsEmerge { ProductDisplayName = "--All--", pkProductApplicationId = 0 });
                ObjProduct.Add(new vwProductsEmerge { ProductDisplayName = "      ", pkProductApplicationId = 0 });
                ObjProduct.Add(new vwProductsEmerge { ProductDisplayName = "<< Drug Testing Kits >>", pkProductApplicationId = -4 });
                List<vwProductsEmerge> ObjProductNew = ObjBALProducts.GetProducts().Where(t => t.ReportCategoryId == 9 && !t.IsDeleted).ToList();
                for (int i = 0; i < ObjProductNew.Count; i++)
                {
                    string ProductDisplay = ObjProductNew.ElementAt(i).ProductDisplayName + " - " + ObjProductNew.ElementAt(i).ProductCode;
                    ObjProduct.Add(new vwProductsEmerge { ProductDisplayName = ProductDisplay, pkProductApplicationId = ObjProductNew.ElementAt(i).pkProductApplicationId });
                }

                lstDataSource = ObjProduct.Select(x => new SelectListItem { Text = x.ProductDisplayName.ToString(), Value = x.pkProductApplicationId.ToString() }).ToList();
            }
            catch
            {
            }
            return Json(lstDataSource);
        }

        /// <summary>
        /// To Get the Product default price based on product id.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public ActionResult GetDefaultPricePerProduct(int companyId, int productId, long pkInvoiceId)
        {
            decimal pppVal = 0;
            int pQty = 0;
            try
            {
                int pkProductId = productId;
                int pkcompanytId = companyId;
                pppVal = new BALInvoice().GetDefaultPrice(pkcompanytId, pkProductId, pkInvoiceId);
                pQty = new BALInvoice().GetRemainingQtyofProduct(pkcompanytId, pkProductId, pkInvoiceId);
            }
            catch { }
            return Json(new { PricePerProduct = pppVal, ProductQty = pQty });
        }


        public ActionResult UpdateDiscountAmountPerInvoice(long companyId, decimal priceValue, long pkInvoiceId, long pkorderdetailid, string Notes, string AdjustmentType)
        {
            decimal Result = 0;

            try
            {

                int check = 0;
                Result = new BALInvoice().UpdateCustomPricePerInvoice(companyId, priceValue, pkInvoiceId, pkorderdetailid, Notes, check, AdjustmentType);

            }
            catch { }
            return Json(new { ResultData = Result });
        }

        public ActionResult SaveAdjustment(string companyid, string invoice, int PaymentProcess, int adjustmentProductId, int Qty, decimal AdjustmentAmount, decimal DiscountAmount, string DiscountDes)
        {
            try
            {
                int company = Int32.Parse(companyid);
                Guid UserId = Guid.Parse("00000000-0000-0000-0000-000000000000");
                MembershipUser Obj = Membership.GetUser(User.Identity.Name);
                if (Obj != null)
                {
                    UserId = new Guid(Obj.ProviderUserKey.ToString());
                }
                new BALInvoice().SaveAdjustmentBasedonProduct(company, Convert.ToInt64(invoice), PaymentProcess, adjustmentProductId, Qty, AdjustmentAmount, DiscountAmount, UserId, DiscountDes);
            }
            catch
            {
            }
            return Json(string.Empty);
        }



    }
}
