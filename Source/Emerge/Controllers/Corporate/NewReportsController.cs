﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Data;
using Emerge.Models;
using Emerge.Services;
using Emerge.Common;
using System.Configuration;
using System.Web.UI;
using System.Web.Security;
using System.IO;
using System.Text;
using System.Xml;

namespace Emerge.Controllers.Corporate
{
    public partial class CorporateController : BaseEmergeController
    {
        //
        // GET: /NewReports/

        public ActionResult NewReportXP()
        {
            BALGeneral ObjBALGeneral = new BALGeneral();
            NewReportModel ObjModel = new NewReportModel();
            int UserLocationId = 0;
            int pkCompanyId = 0;

            #region Get Company Id

            BALCompany ObjBALCompany = new BALCompany();
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            MembershipUser Member = Membership.GetUser(User.Identity.Name);
            if (Member != null)
            {
                var dbCollection = ObjBALCompanyUsers.GetUserCompanyInfo(new Guid(Member.ProviderUserKey.ToString()));
                if (dbCollection.Count > 0)
                {
                    Session["UserFullName"] = dbCollection.First().FirstName + " " + dbCollection.First().LastName;


                    int fkLocationId = Int32.Parse(dbCollection.First().pkLocationId.ToString());
                    UserLocationId = fkLocationId;
                    ObjModel.fkLocationId = fkLocationId;
                    ObjModel.pkCompanyUserId = Int32.Parse(dbCollection.First().pkCompanyUserId.ToString());
                    ObjModel.OrderCompanyName = dbCollection.First().CompanyName;
                    ObjModel.CompanyAccountNo = dbCollection.First().CompanyAccountNumber;
                    ObjModel.PermissiblePurposeId = dbCollection.First().PermissiblePurpose;
                    ObjModel.IsMIEnable = dbCollection.First().IsMIEnable;
                    pkCompanyId = dbCollection.First().pkCompanyId;
                    ObjModel.pkCompanyId = pkCompanyId;
                    ObjModel.IsSyncpodforcompany = dbCollection.First().IsSyncpod;
                    ObjModel.UserName = "dummy";
                    ObjModel.Password = "dummy";
                    
                  
                }
            }


            #endregion

            #region Left Panel Categories

            List<Proc_Get_ReportCategoriesResult> ObjDataCategories = ObjBALGeneral.GetReportCategories().Where(t => t.pkReportCategoryId != 9).ToList();
            ObjModel.ReportCategoriesList = ObjDataCategories;

            Proc_Get_ReportCategoriesResult ObjDynamicColl = new Proc_Get_ReportCategoriesResult();
            //List<string> ObjCollRcxProducts = new List<string>();
            ObjDynamicColl.pkReportCategoryId = 0;
            ObjDynamicColl.CategoryName = "Packages";
            ObjDynamicColl.IsEnabled = true;
            ObjDataCategories.Insert(0, ObjDynamicColl);

            #endregion

            #region Package List

            List<Proc_Get_EmergeProductPackagesResult> ObjReports = ObjBALGeneral.GetEmergeProductPackages(UserLocationId);
            ObjModel.EmergeProductPackagesList = ObjReports;

            #endregion

            #region Categories Report List

            Dictionary<int, List<Proc_Get_NewReportsByCategoryIdResult>> DictCategory = new Dictionary<int, List<Proc_Get_NewReportsByCategoryIdResult>>();
            for (int i = 0; i < ObjDataCategories.Count; i++)
            {

                var NewReportsByCategory1 = ObjBALGeneral.GetNewReportsByCategoryId(UserLocationId, ObjDataCategories.ElementAt(i).pkReportCategoryId);
                DictCategory.Add(i, NewReportsByCategory1);
            }
            ObjModel.DictCategory = DictCategory;

            #endregion

            #region Drug Testing

            BALProducts ObjBALProducts = new BALProducts();
            List<Proc_GetDrugTestingProductsResult> ObjData = ObjBALProducts.GetDrugTestingProducts(UserLocationId);
            ObjModel.DrugTestingList = ObjData;

            #endregion

            ObjModel = BindNewReportDropdowns(ObjBALGeneral, ObjModel, pkCompanyId, ObjBALCompany);

            #region FcraAgreement

            ObjModel = PartialFcraAgreement(ObjModel);

            #endregion FcraAgreement

            #region Report Not Generating

            ObjModel = PartialReportNotGenerating(ObjModel);
            #endregion Report Not Generating

            ObjModel.OrderType = 1;//Normal Report
            ObjModel.hdnIsSCRLiveRunnerLocation = "0";

            return View(ObjModel);
        }

    }
}
