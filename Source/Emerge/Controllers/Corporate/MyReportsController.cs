﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Data;
using Emerge.Services;
using Emerge.Common;

namespace Emerge.Controllers.Corporate
{
    public partial class CorporateController : BaseEmergeController
    {
        BALGeneral ObjBALGeneral;
        BALProductPackages ObjProductPackagesService;
        BALCompany ObjBALCompany;

        #region MyReports
        public ActionResult MyReports()
        {
            ObjBALGeneral = new BALGeneral();
            ObjBALCompany = new BALCompany();
            ObjProductPackagesService = new BALProductPackages();
          //  List<Proc_Get_ReportCategoriesResult> ObjCategories = new List<Proc_Get_ReportCategoriesResult>();
            List<Proc_GetPackagesWithProductsResult> ObjPackages = new List<Proc_GetPackagesWithProductsResult>();
            int LocationId = 0;// new Guid("2e37dd1e-f60e-40cf-82e3-de973d56f5df");
            if (Session["pkLocationId"] != null)
            {
                LocationId = Int32.Parse(Session["pkLocationId"].ToString());// new Guid("2e37dd1e-f60e-40cf-82e3-de973d56f5df");

            }
            int CompanyId = 0;
            if (Session["pkCompanyId"] != null)
            {
                CompanyId = Int32.Parse(Session["pkCompanyId"].ToString());// new Guid("2e37dd1e-f60e-40cf-82e3-de973d56f5df");

            }
            //ObjCategories = ObjBALGeneral.GetReportCategories();
            List<Proc_GetAllReportsByLocationIdResult> Reports = ObjBALCompany.GetAllReportsLocationId(LocationId);
            List<Proc_GetAllReportsByLocationIdResult> DrugTestReports = Reports.Where(i => i.CategoryName == "Drug Testing").ToList();
            List<Proc_GetAllReportsByLocationIdResult> Reports1 = Reports.Where(i => i.CategoryName != "Drug Testing").ToList();
            //ObjPackages = ObjProductPackagesService.GetEmergeProductPackagesForAdmin(LocationId, Utility.SiteApplicationId, CompanyId);
            ObjPackages = ObjProductPackagesService.GetPackagesWithProducts(LocationId, Utility.SiteApplicationId, CompanyId);
            ViewData["Reports"] = Reports1;
            ViewData["DrugTestReports"] = DrugTestReports;
            //ViewData["Categories"] = ObjCategories;
            ViewData["Packages"] = ObjPackages;
            return View();
        }
        #endregion

        #region Get Products
        public string GetProducts(string CategoryId)
        {
            string Content = string.Empty;
            string pdesc = "";
            ObjBALGeneral = new BALGeneral();
            byte CId = byte.Parse(CategoryId);
            int LocationId = 0;// new Guid("2e37dd1e-f60e-40cf-82e3-de973d56f5df");
            if (Session["pkLocationId"] != null)
            {
                LocationId = Int32.Parse(Session["pkLocationId"].ToString());// new Guid("2e37dd1e-f60e-40cf-82e3-de973d56f5df");

            }
            int CompanyId = 0;
            if (Session["pkCompanyId"] != null)
            {
                CompanyId = Int32.Parse(Session["pkCompanyId"].ToString());// new Guid("2e37dd1e-f60e-40cf-82e3-de973d56f5df");

            }
            List<Proc_GetReportProductsByIdResult> ObjProducts = new List<Proc_GetReportProductsByIdResult>();
            ObjProducts = ObjBALGeneral.GetReportProductsById(LocationId, CId, CompanyId);
            #region Build string of Products
            if (ObjProducts.Count > 0)
            {
                foreach (var i in ObjProducts)
                {
                    Content += "<table width='100%' border='0' class='product_Border'>";
                    Content += "<tr>";
                    Content += "<td style='text-align:left;'><input type='checkbox' disabled='disabled' checked='checked' /></td>";
                    Content += "<td align='left' width='2%'><input type='checkbox' disabled='disabled' checked='checked' style='display:none;' /></td>";
                    Content += "<td align='left' width='21%'>" + i.ProductDisplayName + "</td>";
                    Content += "<td align='left' width='10%'>" + i.CompanyPrice + "</td>";
                    Content += "<td align='left' width='7%'>" + i.ProductCode + "</td>";
                    
                    /*********DESCRIPTION*********/
                    if (String.IsNullOrEmpty(i.ProductDescription))
                    {
                        pdesc = "N/A";
                    }
                    else
                    {
                        pdesc = i.ProductDescription;
                    }
                    Content += "<td align='center' width='8%'><img src='../../Content/themes/base/images/PDescription.png' alt='' onmouseover=\"tooltipLiveRunner1('" + i.ProductDescription + "','" + pdesc + "');\" onmouseout='exit()' /></td>";
                    Content += "<td align='center' width='6%'><a href='javascript:void(0)' onclick=\"SampleReport('" + i.ProductCode + "','" + i.ProductDisplayName + "')\"> <img src='../../Content/themes/base/images/PSample.png' alt='' /></td>";
                    Content += "<td align='center' width='6%'>";
                    
                    /*********COVERAGE PDF*********/
                    if (string.IsNullOrEmpty(i.CoveragePDFPath))
                    {
                        Content += "<img src='../../Content/themes/base/images/PCoverage.png' alt='' onclick='alert('~/Resources/Upload/CoveragePDF/" + i.CoveragePDFPath + "')' />";
                    }
                    Content += "</td>";
                    Content += "<td align='center' width='12%' style='min-width: 110px;'>";
                    
                    /*********ACTIVE REPORTS*********/
                    if (i.ProductAccessId != 0)
                    {
                        Content += "<img src='../../Content/themes/base/images/tick.png' alt='' />";
                    }
                    else
                    {
                        Content += "<a href='javascript:void(0)'  onclick=\"GetEmailTemplate('" + i.ProductDisplayName + "');\" ><img src='../../Content/themes/base/images/activate_report.png' alt='' /></a>";
                    }
                    Content += "</td>";
                    Content += "<td align='center' width='9%'>";
                    
                    /*********FEE*********/
                    if (i.IsThirdPartyFee)
                    {
                        if (i.ProductCode == "SCR" || i.ProductCode == "MVR")
                        {
                            Content += "<a href='../Corporate/StateFee?cd=" + i.ProductCode+ "'><img src='../../Content/themes/base/images/icon-finance.png' alt='' /></td>";
                        }
                        if (i.ProductCode == "CCR1" || i.ProductCode == "CCR2")
                        {
                            Content += "<a href='../Corporate/StateCountyFee'><img src='../../Content/themes/base/images/icon-finance.png' alt='' /></td>";
                        }
                    }
                    else if (i.ProductCode.ToLower() == "rcx")
                    {
                        Content += "<a href='../Corporate/StateCountyFee?ShowFee=RCX'><img src='../../Content/themes/base/images/icon-finance.png' alt='' /></td>";
                    }
                    Content += "</td>";
                    Content += "<td width='4%' valign='top' style='min-width: 20px;' align='center'>";
                    
                    /*********REVIEW*********/
                    if (i.AutoReviewStatusForLocation != 0)
                    {
                        Content += "<input type='checkbox' disabled='disabled' checked='checked' />";
                    }
                    else
                    {
                        Content += "<input type='checkbox' disabled='disabled' />";
                    }
                    Content += "</td>";
                    Content += "<td align='center' width='4%'>";
                    
                    /*********IS LIVE RUNNER*********/
                    if (i.IsLiveRunnerProduct && bool.Parse(i.IsLiveRunnerLocation.ToString()))
                    {
                        Content += "<input type='checkbox' disabled='disabled' checked='checked' />";
                    }
                    else
                    {
                        Content += "<input type='checkbox' disabled='disabled' />";
                    }
                    Content += "</td>";
                    Content += "</tr>";
                    Content += "</table>";
                }
            }
            #endregion

            return Content;
        }
        #endregion

        #region Get Reports in Package
        public string GetReportsInPackage(string PackageId)
        {
            string Content = string.Empty;
            ObjProductPackagesService = new BALProductPackages();
            int PId = int.Parse(PackageId);
            List<proc_Get_ProductInPackage_ByPckgIdResult> ObjProducts = new List<proc_Get_ProductInPackage_ByPckgIdResult>();
            ObjProducts = ObjProductPackagesService.GetReportsInPackage(PId);
            if (ObjProducts.Count > 0)
            {
                foreach (var i in ObjProducts)
                {
                    Content += i.ProductDisplayName + "</br>";
                }
            }
            return Content;
        }
        #endregion

        #region ShowSampleReport
        [HttpPost]
        public JsonResult ShowSampleReport(string SampleCode, string ReportName)
        {
            //string SampleReportText = "<link rel=\"stylesheet\" type=\"text/css\" href=\"../Resources/Css/Report.css\"/>";
            string SampleReportText = "<link rel=\"stylesheet\" type=\"text/css\" href=\"" + ApplicationPath.GetApplicationPath() + "Resources/Css/Report.css?v1" + "\"/>";
            string FileContent = "";
            FileContent = Utility.GetSampleReportContent(ReportName + "_SampleReport");
            if (FileContent == "")
            {
                FileContent = Utility.GetSampleReportContent("Common_SampleReport");
                FileContent = FileContent.Replace("%%ReportType%%", SampleCode).Replace("%%ReportName%%", ReportName);
            }
            SampleReportText += FileContent;

            return Json(new { Data = SampleReportText });
        }
        #endregion

        #region Send Active Report Email

        #region Get Email Template to send Active Report Email
        [HttpPost]
        public JsonResult GetEmailTemplate(string ReportName)
        {
            ObjBALGeneral = new BALGeneral();
            byte settingId = 1;
            var settings = ObjBALGeneral.GetSettings(settingId);
            var emailContent = ObjBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, Convert.ToInt32(EmailTemplates.PackageRequest));// "Package Request"
            string Company = (Session["CurrentUserCompanyName"] == null) ? string.Empty : Session["CurrentUserCompanyName"].ToString();
            string FromAddress = (Session["CurrentUserMailId"] == null) ? string.Empty : Session["CurrentUserMailId"].ToString();
            string PopHeader = " Request New Package";
            string Mailto = settings.SupportEmailId;
            string Subject = emailContent.TemplateSubject;
            string Message = FromAddress + " from " + Company + " is requesting to activate the " + ReportName + " package.";

            string EmailContent = emailContent.TemplateContent;
            //ViewState["ReportName"] = e.CommandArgument.ToString();

            //mwActivateReq.Style["Display"] = "Block";
            //divPopActivateReq.Style["Display"] = "Block";

            return Json(new { Header = PopHeader, From = FromAddress, To = Mailto, Subj = Subject, Msg = Message, EContent = EmailContent, Cmp = Company });
        }
        #endregion

        #region Send Active Report Email
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult SendEmail(FormCollection frm, string Company, string MailFrom, string ReportName, string Subject,  string EContent)
        {

            string emailText = "";
            string MessageBody = "";
            string CssClass = "";
            string Message = "";

            BALGeneral ObjBALGeneral = new BALGeneral();
            string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;

            emailText = string.IsNullOrEmpty(EContent) ? string.Empty : EContent;

            #region New way Bookmark

            BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
            Bookmarks objBookmark = new Bookmarks();

            objBookmark.EmailContent = emailText;

            objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
            objBookmark.CompanyName = string.IsNullOrEmpty(Company) ? string.Empty : Company;
            objBookmark.UserEmail = string.IsNullOrEmpty(MailFrom) ? string.Empty : MailFrom;
            objBookmark.ReportName = string.IsNullOrEmpty(ReportName) ? string.Empty : ReportName;

            //MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Utility.GetUID(System.Web.HttpContext.Current.User.Identity.Name.ToString()));
            MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Utility.GetUID(!string.IsNullOrEmpty(System.Web.HttpContext.Current.User.Identity.Name.ToString()) ? System.Web.HttpContext.Current.User.Identity.Name.ToString() : "admin@intelifi.com"));

            #endregion

            bool success = Utility.SendMail("support@dotnetoutsourcing.com", string.Empty, string.Empty, objBookmark.UserEmail, MessageBody, Subject);

            if (success == true)
            {
                CssClass = "successmsg";
                Message = "Your request for product/package " + objBookmark.ReportName + " is sent successfully.";
            }
            else
            {
                CssClass = "errormsg";
                Message = "Some error occured,please try again.";

            }
            return Json(new { Msg = Message, Css = CssClass });
        }
        #endregion

        #endregion  
    }
}
