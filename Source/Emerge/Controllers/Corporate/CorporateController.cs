﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;

namespace Emerge.Controllers.Corporate
{
    public partial class CorporateController : BaseEmergeController
    {
        //
        // GET: /Corporate/

        public ActionResult BillingStatement()
        {
            List<SelectListItem> EmptyList = new List<SelectListItem>();
            EmptyList.Add(new SelectListItem { Text = "---Select---", Value = string.Empty });
            ViewData["listData"] = EmptyList;
            return View();
        }
        public ActionResult LoadFeaturePreview(int FeatureId)
        {
            BALContentManagement ObjBALContentManagement = new BALContentManagement();
            tblFeature ObjtblFeature = new tblFeature();
            try
            {
                ObjtblFeature = ObjBALContentManagement.GetFeatureListById(FeatureId);
                //if (!System.IO.File.Exists(Server.MapPath(@"~/Resources/Upload/FeatureFiles/" + ObjtblFeature.UploadedFile)))
                //{
                //    ObjtblFeature.UploadedFile = "no_image.jpg";
                //}

                if (!System.IO.File.Exists(Server.MapPath(@"~/Resources/Upload/FeatureFiles/" + ObjtblFeature.UploadedFile)))
                {
                    ObjtblFeature.UploadedFile = "../Resources/Upload/FeatureFiles/no_image.jpg";
                }
                else if (!(ObjtblFeature.UploadedFile.ToLower().Contains(".flv") || ObjtblFeature.UploadedFile.ToLower().Contains(".swf")))
                {
                    ObjtblFeature.UploadedFile = ApplicationPath.GetApplicationPath() + "GetImage.ashx?ImagePath=~/Resources/Upload/FeatureFiles/" + ObjtblFeature.UploadedFile + "&Height=350&Width=650";
                }

                if (System.IO.File.Exists(Server.MapPath(@"~/Resources/Upload/FeatureFiles/" + ObjtblFeature.UploadedTitleImage)))
                {
                    //"Content/Handler/Photogallery.ashx?PhotoGallery=~/Resources/Upload/FeatureFiles/"
                    ObjtblFeature.UploadedTitleImage = ApplicationPath.GetApplicationPath() + "GetImage.ashx?ImagePath=~/Resources/Upload/FeatureFiles/" + ObjtblFeature.UploadedTitleImage + "&Height=40&Width=500";
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in LoadFeaturePreviewForEnlarge(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            finally
            {
                ObjBALContentManagement = null;
            }
            return Json(ObjtblFeature);
        }

        public ActionResult LoadFeaturePreviewForEnlarge(int FeatureId)
        {
            BALContentManagement ObjBALContentManagement = new BALContentManagement();
            tblFeature ObjtblFeature = new tblFeature();
            try
            {
                ObjtblFeature = ObjBALContentManagement.GetFeatureListById(FeatureId);
                //if (!System.IO.File.Exists(Server.MapPath(@"~/Resources/Upload/FeatureFiles/" + ObjtblFeature.UploadedFile)))
                //{
                //    ObjtblFeature.UploadedFile = "no_image.jpg";
                //}

                if (!System.IO.File.Exists(Server.MapPath(@"~/Resources/Upload/FeatureFiles/" + ObjtblFeature.UploadedFile)))
                {
                    ObjtblFeature.UploadedFile = "../Resources/Upload/FeatureFiles/no_image.jpg";
                }
                else if (!(ObjtblFeature.UploadedFile.ToLower().Contains(".flv") || ObjtblFeature.UploadedFile.ToLower().Contains(".swf")))
                {
                    ObjtblFeature.UploadedFile = ApplicationPath.GetApplicationPath() + "GetImage.ashx?ImagePath=~/Resources/Upload/FeatureFiles/" + ObjtblFeature.UploadedFile + "&Height=600&Width=850";
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in LoadFeaturePreviewForEnlarge(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            finally
            {
                ObjBALContentManagement = null;
            }
            return Json(ObjtblFeature);
        }

        public ActionResult TestViewSavedReport()
        {
            return View();
        }
    }
}
