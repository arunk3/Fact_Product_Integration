﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using System.Web.Security;
using Emerge.Controllers.Common;

namespace Emerge.Controllers.Corporate
{
    public partial class CorporateController : BaseEmergeController
    {

        #region Saved Report Page Load

        public ActionResult SavedReports()
        {
            int pkCompanyId = 0;
            int pkLocationId = 0;
            int pkCompanyUserId = 0;
            SavedReportModel ObjModel = new SavedReportModel();
            ObjModel.pkProductApplicationId = 0;
            MembershipUser ObjMembership = Membership.GetUser(User.Identity.Name);
            string strqryst = string.Empty;
            string strqryst2 = string.Empty;
            bool ShowIncomplete = false;
            string[] Role;
            Role = new string[0];
            ObjModel.IsReportStatus = new BALCompany().GetPassFailReportStatus(ObjMembership.Email);
            if (ObjMembership != null)
            {
                Guid UserId = new Guid(ObjMembership.ProviderUserKey.ToString());

                #region Get Company and Location Info
                List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjCompanyinfo = GetCompanyInfo(UserId);
                if (ObjCompanyinfo != null)
                {
                    pkCompanyId = ObjCompanyinfo.ElementAt(0).pkCompanyId;
                    pkLocationId = ObjCompanyinfo.ElementAt(0).pkLocationId;
                }

                #endregion

                #region get report status
                //if (UserId != null && UserId!=Guid.Empty)
                //{
                //    List<Proc_Get_CompanyDetailByMemberShipIdResult> ObjCompanyDetails = ObjBALCompany.GetCompanyDetailByMemberShipIdResult(UserId);
                //    int CountValue = ObjCompanyDetails.Count();
                //    if (CountValue > 0)
                //    {
                //        ObjModel.IsReportStatus = ObjCompanyDetails.Select(r => r.IsReportStatus).FirstOrDefault();
                //    }
                //}

                #endregion

                Role = Roles.GetRolesForUser(ObjMembership.Email);


                ObjModel.UserRoles = Role;

                if (Role.Contains("BasicUser") || Role.Contains("ViewOnlyUser"))
                {
                    ObjModel.Role = 5;
                    #region GetUserInfo
                    pkCompanyUserId = GetUserInfo(UserId);
                    ObjModel.pkCompanyUserId = pkCompanyUserId;
                    ObjModel.CompanyUserId = pkCompanyUserId;
                    #endregion
                }
                else if (Role.Contains("SystemAdmin"))
                {
                    ObjModel.Role = 1;
                    pkCompanyId = 0;
                    pkLocationId = 0;
                }
                else if (Role.Contains("CorporateManager") || Role.Contains("SupportManager"))
                {
                    ObjModel.Role = 2;
                }
                else if (Role.Contains("BranchManager"))
                {
                    pkCompanyUserId = GetUserInfo(UserId);
                    ObjModel.pkCompanyUserId = pkCompanyUserId;
                    ObjModel.CompanyUserId = pkCompanyUserId;
                    ObjModel.Role = 3;
                }

                #region Query String

                if (!string.IsNullOrEmpty(Request.QueryString["qryst"]))
                {
                    strqryst = Request.QueryString["qryst"];
                    ObjModel.Keyword = strqryst;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["qryst2"]))
                {
                    strqryst2 = Request.QueryString["qryst2"];
                    ObjModel.SearchedOntbl = strqryst2;

                }

                #endregion

                #region Get Columns from Profile

                try
                {
                    MembershipUser Member = Membership.GetUser(User.Identity.Name);
                    if (Member != null)
                    {
                        ProfileCommon Profile = new ProfileCommon();
                        ProfileModel userProfile = Profile.GetProfile(Member.Email);
                        if (userProfile.RecordSize != string.Empty)
                        {
                            TempData["PageSize"] = userProfile.RecordSize;
                            ObjModel.PageSize = Convert.ToInt32(userProfile.RecordSize);
                        }
                        else
                        {
                            ObjModel.PageSize = 25;
                        }
                        if (userProfile.PageNo != string.Empty)
                        {
                            TempData["PageNo"] = userProfile.PageNo;
                            ObjModel.PageNo = Convert.ToInt32(userProfile.PageNo);
                        }
                        else
                        {
                            ObjModel.PageNo = 1;
                        }
                        if (Request.QueryString["_bck"] != null)
                        {
                            ObjModel.Keyword = userProfile.SearchedText;
                            strqryst = userProfile.SearchedText;
                            pkCompanyId = Convert.ToInt32(userProfile.Company);
                            pkLocationId = Convert.ToInt32(userProfile.Location);
                            pkCompanyUserId = Convert.ToInt32(userProfile.User);
                            ObjModel.pkCompanyUserId = Convert.ToInt32(userProfile.User);
                            ObjModel.pkProductApplicationId = Convert.ToInt32(userProfile.DataSource);
                        }

                    }
                }
                catch (Exception)
                {
                }
                #endregion
            }
            SavedReportPageLoad(ObjModel, pkCompanyId, pkLocationId, pkCompanyUserId, strqryst, ShowIncomplete, Role);
            return View(ObjModel);
        }

        public ActionResult PartialArchieveFilter()
        {
            int pkCompanyId = 0;
            int pkLocationId = 0;
            int pkCompanyUserId = 0;
            SavedReportModel ObjModel = new SavedReportModel();
            ObjModel.pkProductApplicationId = 0;
            MembershipUser ObjMembership = Membership.GetUser(User.Identity.Name);
            string strqryst = string.Empty;
            bool ShowIncomplete = false;
            string[] Role;
            Role = new string[0];

            if (ObjMembership != null)
            {
                Guid UserId = new Guid(ObjMembership.ProviderUserKey.ToString());

                #region Get Company and Location Info
                List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjCompanyinfo = GetCompanyInfo(UserId);
                if (ObjCompanyinfo != null)
                {
                    pkCompanyId = ObjCompanyinfo.ElementAt(0).pkCompanyId;
                    pkLocationId = ObjCompanyinfo.ElementAt(0).pkLocationId;

                }

                #endregion

                Role = Roles.GetRolesForUser(ObjMembership.Email);


                ObjModel.UserRoles = Role;

                if (Role.Contains("BasicUser") || Role.Contains("ViewOnlyUser"))
                {
                    ObjModel.Role = 5;
                    #region GetUserInfo
                    pkCompanyUserId = GetUserInfo(UserId);
                    ObjModel.pkCompanyUserId = pkCompanyUserId;
                    ObjModel.CompanyUserId = pkCompanyUserId;
                    #endregion
                }
                else if (Role.Contains("SystemAdmin"))
                {
                    ObjModel.Role = 1;
                    pkCompanyId = 0;
                    pkLocationId = 0;
                }
                else if (Role.Contains("CorporateManager") || Role.Contains("SupportManager"))
                {
                    ObjModel.Role = 2;
                }
                else if (Role.Contains("BranchManager"))
                {
                    pkCompanyUserId = GetUserInfo(UserId);
                    ObjModel.pkCompanyUserId = pkCompanyUserId;
                    ObjModel.CompanyUserId = pkCompanyUserId;
                    ObjModel.Role = 3;
                }

                #region Query String

                if (!string.IsNullOrEmpty(Request.QueryString["qryst"]))
                {
                    strqryst = Request.QueryString["qryst"];
                    ObjModel.Keyword = strqryst;
                }

                #endregion

                #region Get Columns from Profile

                try
                {
                    MembershipUser Member = Membership.GetUser(User.Identity.Name);
                    if (Member != null)
                    {
                        ProfileCommon Profile = new ProfileCommon();
                        ProfileModel userProfile = Profile.GetProfile(Member.Email);
                        if (userProfile.RecordSize != string.Empty)
                        {
                            TempData["PageSize"] = userProfile.RecordSize;
                            ObjModel.PageSize = Convert.ToInt32(userProfile.RecordSize);
                        }
                        else
                        {
                            ObjModel.PageSize = 25;
                        }
                        if (userProfile.PageNo != string.Empty)
                        {
                            TempData["PageNo"] = userProfile.PageNo;
                            ObjModel.PageNo = Convert.ToInt32(userProfile.PageNo);
                        }
                        else
                        {
                            ObjModel.PageNo = 1;
                        }
                        if (Request.QueryString["_bck"] != null)
                        {
                            ObjModel.Keyword = userProfile.SearchedText;
                            strqryst = userProfile.SearchedText;
                            pkCompanyId = Convert.ToInt32(userProfile.Company);
                            pkLocationId = Convert.ToInt32(userProfile.Location);
                            pkCompanyUserId = Convert.ToInt32(userProfile.User);
                            ObjModel.pkCompanyUserId = Convert.ToInt32(userProfile.User);
                            ObjModel.pkProductApplicationId = Convert.ToInt32(userProfile.DataSource);
                        }

                    }
                }
                catch (Exception)
                {
                }
                #endregion
            }
            SavedReportPageDropdownLoad(ObjModel, pkCompanyId, pkLocationId, pkCompanyUserId, strqryst, ShowIncomplete, Role);
            return PartialView("PartialArchieveFilter", ObjModel);
        }

        public void SavedReportPageLoad(SavedReportModel ObjModel, int CompanyId, int pkLocationId, int pkCompanyUserId, string Keyword, bool ShowIncomplete, string[] Role)
        {
            #region Date


            Dictionary<int, string> ObjDate = new Dictionary<int, string>();
            ObjDate.Add(0, "--All--");
            ObjDate.Add(1, "Today");
            ObjDate.Add(2, "This Month");
            ObjDate.Add(3, "Last Month");
            ObjDate.Add(4, "Specific Month");
            ObjDate.Add(5, "Specific Date");
            ObjDate.Add(6, "Specific Date Range");


            ObjModel.OrderDateColl = ObjDate;


            #endregion

            #region Year

            Dictionary<int, string> ObjYear = new Dictionary<int, string>();
            for (int i = 2008; i <= DateTime.Now.Year; i++)
            {
                ObjYear.Add(i, i.ToString());
            }
            ObjModel.DictYear = ObjYear;

            #endregion

            #region DataSource

            BALProducts ObjBALProducts = new BALProducts();
            List<vwProductsEmerge> ObjProductFirst = ObjBALProducts.GetProducts().Where(t => t.ReportCategoryId != Convert.ToByte(9)).ToList();
            List<vwProductsEmerge> ObjProduct = new List<vwProductsEmerge>();

            ObjProduct = ObjProductFirst.Select(d => new vwProductsEmerge { ProductDisplayName = d.ProductDisplayName + " - " + d.ProductCode, pkProductApplicationId = d.pkProductApplicationId }).ToList();

            //INT-151 
            ObjProduct.Insert(0, new vwProductsEmerge { ProductDisplayName = "--All--", pkProductApplicationId = 0 });

            ObjProduct.Add(new vwProductsEmerge { ProductDisplayName = "      ", pkProductApplicationId = -2 });
            ObjProduct.Add(new vwProductsEmerge { ProductDisplayName = "<< Drug Testing Kits >>", pkProductApplicationId = -4 });

            List<vwProductsEmerge> ObjProductNew = ObjBALProducts.GetProducts().Where(t => t.ReportCategoryId == 9 && !t.IsDeleted).ToList();

            for (int i = 0; i < ObjProductNew.Count; i++)
            {
                string ProductDisplay = ObjProductNew.ElementAt(i).ProductDisplayName + " - " + ObjProductNew.ElementAt(i).ProductCode;
                ObjProduct.Add(new vwProductsEmerge { ProductDisplayName = ProductDisplay, pkProductApplicationId = ObjProductNew.ElementAt(i).pkProductApplicationId });
            }
            ObjModel.ObjProductsEmerge = ObjProduct;

            #endregion

            #region Company

            BALCompany ObjBALCompany = new BALCompany();
            List<tblUsersLocation> listtblCompanyOtherLocation1 = new List<tblUsersLocation>();
            if (Role.Contains("SystemAdmin"))
            {
                var ObjCompany = ObjBALCompany.GetAllCompany(Utility.SiteApplicationId, true, false);
                ObjCompany.Insert(0, new CompanyList { CompanyName = "--All--", pkCompanyId = 0 });
                ObjModel.ObjCompanyList = ObjCompany;
            }
            else
            {
               // MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
                Guid UserId = Utility.GetUID(User.Identity.Name);
                BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
                List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjData = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(UserId);
                var companies = GetCompanyInfoByCompanyId(SiteApplicationId, true, false, ObjData.First().pkCompanyId);
                ObjModel.ObjCompanyList = companies.ToList();
                //ObjModel.ObjCompanyList = companies.Select(x => new SelectListItem { Text = x.CompanyName, Value = x.pkCompanyId.ToString() }).ToList();

            }

            ObjModel.pkCompanyId = CompanyId;
            BALLocation ObjBALLocation2 = new BALLocation();

            #endregion

            #region Location
            // ObjLocations Used Mainly for Looping Purpose
            List<Proc_Get_CompanyLocationsByCompanyIdResult> ObjLocations = ObjBALCompany.GetLocationsByCompanyId(CompanyId, Utility.SiteApplicationId);
            //ObjLocations2 Used for finally Return List
            List<Proc_Get_CompanyLocationsByCompanyIdResult> ObjLocations2 = ObjBALCompany.GetLocationsByCompanyId(CompanyId, Utility.SiteApplicationId);

            bool SavedReport6Month = false;



            if (ObjModel.UserRoles.Contains("SystemAdmin"))
            {
                SavedReport6Month = true;
            }

            // Get List From tblOtherLocation
            if (ObjModel.UserRoles.Contains("BasicUser") || ObjModel.UserRoles.Contains("BranchManager"))
            {
                try
                {
                    // listtblCompanyOtherLocation1 Used For Check selected other location on base of pkCompanyUserId.
                    listtblCompanyOtherLocation1 = ObjBALLocation2.GetLocationsByPkCompanyUserId(pkCompanyUserId);


                    if (listtblCompanyOtherLocation1 != null && listtblCompanyOtherLocation1.Count > 0)
                    {
                        // Clear Previous List For Add Selected Location
                        ObjLocations2.Clear();
                        for (int i = 0; i <= ObjLocations.Count; i++)
                        {
                            // Get single Records On Base of Pklocation For check this location is Checked or Unchecked for Current User.
                            var list = listtblCompanyOtherLocation1.Where(item => item.fklocationId == ObjLocations.ElementAt(i).LocationId).FirstOrDefault(); //  listtblCompanyOtherLocation1.ElementAt(i).fkCompanyUserId).FirstOrDefault();
                            if (list != null)
                            {
                                if (((list.IsHome == true) && (list.fklocationId == ObjLocations.ElementAt(i).LocationId)) || (ObjLocations.ElementAt(i).LocationId == pkLocationId)) // || listtblCompanyOtherLocation1.ElementAt(i).fklocationId != listtblCompanyOtherLocation1.ElementAt(i).fklocationId))
                                {
                                    ObjLocations2.Add(ObjLocations.ElementAt(i));
                                }
                                else
                                {

                                }
                            }
                            else
                            {

                            }

                        }

                    }
                    else
                    {
                        // When Not have any Other Location Only Master Location Will Display In Location Drop-down.
                        ObjLocations2.RemoveAll(item => item.LocationId != pkLocationId);
                    }
                }


                catch
                {

                }
            }
            // Set "All" at 0(zero) Index In location Dropdown For Get All Records In Grid.
            // if (ObjModel.UserRoles.Contains("ViewOnlyUser"))
            //{
            //ObjLocations2.RemoveAll(item => item.LocationId != pkLocationId);

            //}
            if (!(ObjModel.UserRoles.Contains("ViewOnlyUser")))
            {
                ObjLocations2.Insert(0, new Proc_Get_CompanyLocationsByCompanyIdResult { DisplayLocation = "--All--", LocationId = 0 });
            }
            // Finally Assign Location List On base of different Role's.
            ObjModel.LocationList = ObjLocations2;
            ObjModel.LocationId = pkLocationId;
            #endregion

            #region User List

            BALCompanyUsers ObjBALCompanyUsers1 = new BALCompanyUsers();

            List<Proc_Get_CompanyUsersByLocationIdResult> ObjUsers = new List<Proc_Get_CompanyUsersByLocationIdResult>();
            //if Admin or Corporate Manager Then All Location, All Users of selected Company.
            if (ObjModel.UserRoles.Contains("SystemAdmin"))
            {
                ObjUsers = ObjBALCompanyUsers1.GetCompanyUsersByLocationId(0, 0, Utility.SiteApplicationId);
            }
            else if (ObjModel.UserRoles.Contains("CorporateManager") || ObjModel.UserRoles.Contains("SupportManager") || ObjModel.UserRoles.Contains("SupportTechnician"))
            {
                ObjUsers = ObjBALCompanyUsers1.GetCompanyUsersByLocationId(CompanyId, 0, Utility.SiteApplicationId);
            }
            else if (ObjModel.UserRoles.Contains("BranchManager") || ObjModel.UserRoles.Contains("BasicUser") || ObjModel.UserRoles.Contains("ViewOnlyUser"))//All users belongs to his location with seletected company.
            {
                ObjUsers = ObjBALCompanyUsers1.GetCompanyUsersByLocationId(CompanyId, pkLocationId, Utility.SiteApplicationId);
            }



            ObjUsers.Insert(0, new Proc_Get_CompanyUsersByLocationIdResult { UserEmail = "--All--", CompanyUserId = 0 });
            ObjModel.UserList = ObjUsers;
            ObjModel.CompanyUserId = pkCompanyUserId;
            #endregion

            #region Reference Code List
            List<Proc_GetReferenceCodebyCompanyIdResult> ListRefenceCode = ObjBALCompany.GetReferenceCodeListbypkCompanyId(CompanyId).ToList();
            ListRefenceCode.Insert(0, new Proc_GetReferenceCodebyCompanyIdResult { ReferenceCode = "--All--", pkReferenceCodeId = -1 });
            ObjModel.RefenceCodeList = ListRefenceCode;
            #endregion

            #region Get Count Mails

            //BALOrders ObjBALOrders = new BALOrders();

            List<proc_GetSavedReportsCountResult> ObjCountStatus = new List<proc_GetSavedReportsCountResult>();

            //ObjCountStatus = ObjBALOrders.GetSavedReportsCount(ObjModel.pkProductApplicationId,
            //                                                                                                     pkCompanyUserId,
            //                                                                                                     CompanyId,
            //                                                                                                     pkLocationId,
            //                                                                                                     string.Empty,
            //                                                                                                     string.Empty,
            //                                                                                                     Keyword,
            //                                                                                                     0,
            //                                                                                                     "AllReports",
            //                                                                                                     ShowIncomplete, SavedReport6Month);

            using (CommonSavedReportController ObjController = new CommonSavedReportController())
            {
                string ReportCounts = ObjController.GetReportCounter(ObjCountStatus);

                ObjModel.ReportCount = ReportCounts;
                ObjModel.TotalRecords = ObjCountStatus.Count();
            }
            #endregion


        }

        public void SavedReportPageDropdownLoad(SavedReportModel ObjModel, int CompanyId, int pkLocationId, int pkCompanyUserId, string Keyword, bool ShowIncomplete, string[] Role)
        {
            #region Date


            Dictionary<int, string> ObjDate = new Dictionary<int, string>();
            ObjDate.Add(0, "--All--");
            ObjDate.Add(1, "Today");
            ObjDate.Add(2, "This Month");
            ObjDate.Add(3, "Last Month");
            ObjDate.Add(4, "Specific Month");
            ObjDate.Add(5, "Specific Date");
            ObjDate.Add(6, "Specific Date Range");


            ObjModel.OrderDateColl = ObjDate;


            #endregion

            #region Year

            Dictionary<int, string> ObjYear = new Dictionary<int, string>();
            for (int i = 2008; i <= DateTime.Now.Year; i++)
            {
                ObjYear.Add(i, i.ToString());
            }
            ObjModel.DictYear = ObjYear;

            #endregion

            #region DataSource

            //BALProducts ObjBALProducts = new BALProducts();
            //List<vwProductsEmerge> ObjProductFirst = ObjBALProducts.GetProducts().Where(t => t.ReportCategoryId != Convert.ToByte(9)).ToList();
            List<vwProductsEmerge> ObjProduct = new List<vwProductsEmerge>();

            //ObjProduct = ObjProductFirst.Select(d => new vwProductsEmerge { ProductDisplayName = d.ProductDisplayName + " - " + d.ProductCode, pkProductApplicationId = d.pkProductApplicationId }).ToList();


            ObjProduct.Insert(0, new vwProductsEmerge { ProductDisplayName = "--All--", pkProductApplicationId = 0 });

            //ObjProduct.Add(new vwProductsEmerge { ProductDisplayName = "      ", pkProductApplicationId = new Guid("00000000-0000-0000-0000-000000000002") });
            //ObjProduct.Add(new vwProductsEmerge { ProductDisplayName = "<< Drug Testing Kits >>", pkProductApplicationId = new Guid("00000000-0000-0000-0000-000000000001") });

            //List<vwProductsEmerge> ObjProductNew = ObjBALProducts.GetProducts().Where(t => t.ReportCategoryId == 9 && !t.IsDeleted).ToList();

            //for (int i = 0; i < ObjProductNew.Count; i++)
            //{
            //string ProductDisplay = ObjProductNew.ElementAt(i).ProductDisplayName + " - " + ObjProductNew.ElementAt(i).ProductCode;
            //ObjProduct.Add(new vwProductsEmerge { ProductDisplayName = ProductDisplay, pkProductApplicationId = ObjProductNew.ElementAt(i).pkProductApplicationId });
            //}
            ObjModel.ObjProductsEmerge = ObjProduct;

            #endregion

            #region Company

            //BALCompany ObjBALCompany = new BALCompany();
            //List<tblUsersLocation> listtblCompanyOtherLocation1 = new List<tblUsersLocation>();
            //if (Role.Contains("SystemAdmin"))
            //{
            //    var ObjCompany = ObjBALCompany.GetAllCompany(Utility.SiteApplicationId, true, false);
            //    ObjCompany.Insert(0, new CompanyList { CompanyName = "--All--", pkCompanyId = Guid.Empty });
            //    ObjModel.ObjCompanyList = ObjCompany;
            //}
            //else
            //{
            //    MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
            //    Guid UserId = Utility.GetUID(User.Identity.Name);
            //    BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            //    List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjData = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(UserId);
            //    var companies = GetCompanyInfoByCompanyId(SiteApplicationId, true, false, ObjData.First().pkCompanyId);
            //    ObjModel.ObjCompanyList = companies.ToList();
            //    //ObjModel.ObjCompanyList = companies.Select(x => new SelectListItem { Text = x.CompanyName, Value = x.pkCompanyId.ToString() }).ToList();

            //}

            List<CompanyList> ObjCompany = new List<CompanyList>();
            ObjCompany.Insert(0, new CompanyList { CompanyName = "--All--", pkCompanyId = 0 });
            ObjModel.ObjCompanyList = ObjCompany;

            ObjModel.pkCompanyId = CompanyId;
            //BALLocation ObjBALLocation2 = new BALLocation();

            #endregion

            #region Location
            // // ObjLocations Used Mainly for Looping Purpose
            // List<Proc_Get_CompanyLocationsByCompanyIdResult> ObjLocations = ObjBALCompany.GetLocationsByCompanyId(CompanyId, Utility.SiteApplicationId);
            // //ObjLocations2 Used for finally Return List
            // List<Proc_Get_CompanyLocationsByCompanyIdResult> ObjLocations2 = ObjBALCompany.GetLocationsByCompanyId(CompanyId, Utility.SiteApplicationId);

            // bool SavedReport6Month = false;




            // if (ObjModel.UserRoles.Contains("SystemAdmin"))
            // {
            //     SavedReport6Month = true;
            // }

            // // Get List From tblOtherLocation
            // if (ObjModel.UserRoles.Contains("BasicUser") || ObjModel.UserRoles.Contains("BranchManager"))
            // {
            //     try
            //     {
            //         // listtblCompanyOtherLocation1 Used For Check selected other location on base of pkCompanyUserId.
            //         listtblCompanyOtherLocation1 = ObjBALLocation2.GetLocationsByPkCompanyUserId(pkCompanyUserId);


            //         if (listtblCompanyOtherLocation1 != null && listtblCompanyOtherLocation1.Count>0)
            //         {
            //             // Clear Previous List For Add Selected Location
            //             ObjLocations2.Clear();
            //             for (int i = 0; i <= ObjLocations.Count; i++)
            //             {
            //                 // Get single Records On Base of Pklocation For check this location is Checked or Unchecked for Current User.
            //                 var list = listtblCompanyOtherLocation1.Where(item => item.fklocationId == ObjLocations.ElementAt(i).LocationId ).FirstOrDefault(); //  listtblCompanyOtherLocation1.ElementAt(i).fkCompanyUserId).FirstOrDefault();
            //                 if (list != null)
            //                 {
            //                     if (((list.IsHome == true) && (list.fklocationId == ObjLocations.ElementAt(i).LocationId)) || (ObjLocations.ElementAt(i).LocationId == pkLocationId)) // || listtblCompanyOtherLocation1.ElementAt(i).fklocationId != listtblCompanyOtherLocation1.ElementAt(i).fklocationId))
            //                     {
            //                         ObjLocations2.Add(ObjLocations.ElementAt(i));
            //                     }
            //                     else
            //                     {

            //                     }
            //                 }
            //                 else
            //                 {

            //                 }

            //             }

            //         }
            //         else
            //         {
            //             // When Not have any Other Location Only Master Location Will Display In Location Drop-down.
            //             ObjLocations2.RemoveAll(item => item.LocationId != pkLocationId);
            //        }
            //     }


            //     catch
            //     {

            //     }
            // }
            //// Set "All" at 0(zero) Index In location Dropdown For Get All Records In Grid.
            // // if (ObjModel.UserRoles.Contains("ViewOnlyUser"))
            // //{
            // //ObjLocations2.RemoveAll(item => item.LocationId != pkLocationId);

            // //}
            // if (!(ObjModel.UserRoles.Contains("ViewOnlyUser")))
            // {
            //     ObjLocations2.Insert(0, new Proc_Get_CompanyLocationsByCompanyIdResult { DisplayLocation = "--All--", LocationId = Guid.Empty });
            // }
            List<Proc_Get_CompanyLocationsByCompanyIdResult> ObjLocations2 = new List<Proc_Get_CompanyLocationsByCompanyIdResult>();
            ObjLocations2.Insert(0, new Proc_Get_CompanyLocationsByCompanyIdResult { DisplayLocation = "--All--", LocationId = 0 });
            // Finally Assign Location List On base of different Role's.
            ObjModel.LocationList = ObjLocations2;
            ObjModel.LocationId = pkLocationId;
            #endregion

            #region User List

            //BALCompanyUsers ObjBALCompanyUsers1 = new BALCompanyUsers();
            List<Proc_Get_CompanyUsersByLocationIdResult> ObjUsers = new List<Proc_Get_CompanyUsersByLocationIdResult>();
            ObjUsers.Insert(0, new Proc_Get_CompanyUsersByLocationIdResult { UserEmail = "--All--", CompanyUserId = 0 });
            ObjModel.UserList = ObjUsers;
            ObjModel.CompanyUserId = pkCompanyUserId;
            #endregion

            #region Reference Code List
            List<Proc_GetReferenceCodebyCompanyIdResult> ListRefenceCode = new List<Proc_GetReferenceCodebyCompanyIdResult>();
            ListRefenceCode.Insert(0, new Proc_GetReferenceCodebyCompanyIdResult { ReferenceCode = "--All--", pkReferenceCodeId = -1 });
            ObjModel.RefenceCodeList = ListRefenceCode;
            #endregion

            #region Get Count Mails

           // BALOrders ObjBALOrders = new BALOrders();

            List<proc_GetSavedReportsCountResult> ObjCountStatus = new List<proc_GetSavedReportsCountResult>();
            string ReportCounts = string.Empty;
            using (CommonSavedReportController ObjController = new CommonSavedReportController())
            {
                ReportCounts = ObjController.GetReportCounter(ObjCountStatus);
            }
            ObjModel.ReportCount = ReportCounts;
            ObjModel.TotalRecords = ObjCountStatus.Count();

            #endregion


        }

        public List<CompanyList> GetCompanyInfoByCompanyId(Guid ApplicationId, bool Enabled, bool Deleted, int pkCompanyId)
        {
            using (EmergeDALDataContext DX = new EmergeDALDataContext())
            {
                IQueryable<CompanyList> ObjData = from c in DX.tblCompanies
                                                  join l in DX.tblLocations on c.pkCompanyId equals l.fkCompanyId
                                                  join s in DX.tblStates on l.fkStateID equals s.pkStateId

                                                  where c.fkApplicationId == ApplicationId && c.IsDeleted == Deleted && l.IsHome == true && c.pkCompanyId == pkCompanyId
                                                  orderby c.CompanyName ascending
                                                  select new CompanyList
                                                  {
                                                      CompanyName = c.CompanyName + "  [" + l.City + ", " + s.StateCode + "]",
                                                      pkCompanyId = c.pkCompanyId
                                                  };
                return ObjData.ToList();
            }
        }

        private List<Proc_Get_UserInfoByMemberShipUserIdResult> GetCompanyInfo(Guid UserId)
        {
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();

            try
            {
                List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjDbCollection = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(UserId);
                if (ObjDbCollection.Count > 0)
                {
                    return ObjDbCollection;
                }
                else
                {
                    return null;
                }

            }
            catch
            {
                return null;
            }
        }

        private int GetUserInfo(Guid UserId)
        {
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();

            try
            {
                List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjDbCollection = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(UserId);
                if (ObjDbCollection.Count > 0)
                {
                    return ObjDbCollection.First().pkCompanyUserId;
                }
                else
                {
                    return 0;
                }
            }
            catch
            {
                return 0;
            }
            finally
            {
                ObjBALCompanyUsers = null;
            }
        }

        #endregion

        #region Mark
        [HttpPost]
        public JsonResult Mark(string pkIds, string MarkAs)
        {
            string Message = string.Empty;
            string CSS = string.Empty;
            string[] Ids = { };
            if (!string.IsNullOrEmpty(pkIds))
            {
                Ids = pkIds.Split(',').Where(x => x != string.Empty).ToArray();
            }
            BALOrders ObjBALOrders = new BALOrders();
            List<MarkAs> lstMarkAs = new List<MarkAs>();

            try
            {
                if (Ids.Count() > 0)
                {
                    for (int i = 0; i < Ids.Count(); i++)
                    {
                        MarkAs objMarkAs = new MarkAs();
                        objMarkAs.OrderId = Convert.ToInt32(Ids[i]);
                        objMarkAs.FolderId = Convert.ToInt32(MarkAs);
                        using (EmergeDALDataContext DX = new EmergeDALDataContext())
                        {

                            string Name = string.Empty;
                            tblManageFolder ObjtbltblManageFolder = new tblManageFolder();
                            ObjtbltblManageFolder = DX.tblManageFolders.Where(d => d.pkManageFolderId == objMarkAs.FolderId).FirstOrDefault();
                            if (ObjtbltblManageFolder != null)
                            {
                                Name = ObjtbltblManageFolder.FolderName;
                                if (Name == "Read")
                                {
                                    objMarkAs.IsRead = true;
                                }
                                else if (Name == "Unread")
                                {
                                    objMarkAs.IsRead = false;
                                }
                                else
                                {
                                    objMarkAs.IsRead = MarkAs == "1" ? true : false;
                                }

                            }
                            else if (ObjtbltblManageFolder == null)
                            {
                                objMarkAs.IsRead = MarkAs == "1" ? true : false;

                            }

                        }

                        
                        lstMarkAs.Add(objMarkAs);
                    }
                    int result = ObjBALOrders.MarkReadUnRead(lstMarkAs);
                    int fkcompanyid = 0;
                    if (!string.IsNullOrEmpty(Session["pkCompanyId"].ToString()))
                    {
                        fkcompanyid = Convert.ToInt32(Session["pkCompanyId"].ToString());
                    }
                    //int folderStatus = ObjBALOrders.AddOrderWiseFolderStatus(lstMarkAs, fkcompanyid);
                    if (result == 1)
                    {
                        Message = "Moved successfully";
                        CSS = "successmsg";
                    }
                    else
                    {
                        Message = "Some error ocurred. Please try again later.";
                        CSS = "Error";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBALOrders = null;
            }

            return Json(new { Msg = Message, css = CSS });
        }
        #endregion





        //#region Mark
        //[HttpPost]
        //public JsonResult Mark(string pkIds, string MarkAs)
        //{
        //    string Message = string.Empty;
        //    string CSS = string.Empty;
        //    string[] Ids = { };
        //    if (!string.IsNullOrEmpty(pkIds))
        //    {
        //        Ids = pkIds.Split(',').Where(x => x != string.Empty).ToArray();
        //    }
        //    BALOrders ObjBALOrders = new BALOrders();
        //    List<MarkAs> lstMarkAs = new List<MarkAs>();

        //    try
        //    {
        //        if (Ids.Count() > 0)
        //        {
        //            for (int i = 0; i < Ids.Count(); i++)
        //            {
        //                MarkAs objMarkAs = new MarkAs();
        //                objMarkAs.OrderId = Convert.ToInt32(Ids[i]);
        //                objMarkAs.IsRead = MarkAs == "1" ? true : false;
        //                lstMarkAs.Add(objMarkAs);
        //            }
        //            int result = ObjBALOrders.MarkReadUnRead(lstMarkAs);
        //            if (result == 1)
        //            {
        //                Message = "Moved successfully";
        //                CSS = "successmsg";
        //            }
        //            else
        //            {
        //                Message = "Some error ocurred. Please try again later.";
        //                CSS = "Error";
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        ObjBALOrders = null;
        //    }

        //    return Json(new { Msg = Message, css = CSS });
        //}
        //#endregion

        #region Delete Orders for ticket #23
        /// <summary>
        /// Delete order ids
        /// </summary>
        /// <param name="pkIds"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteOrders(string pkIds)
        {
            string Message = string.Empty;
            string CSS = string.Empty;
            Guid UserId = Guid.Empty;
            string[] Ids = { };
            if (!string.IsNullOrEmpty(pkIds))
            {
                Ids = pkIds.Split(',').Where(x => x != string.Empty).ToArray();
            }
            BALOrders ObjBALOrders = new BALOrders();
            List<tblOrder> ObjListtblOrders = new List<tblOrder>();


            try
            {
                MembershipUser ObjMembership = Membership.GetUser(User.Identity.Name);
                DateTime CurrentDate = DateTime.UtcNow;
                if (ObjMembership != null)
                {
                    UserId = new Guid(ObjMembership.ProviderUserKey.ToString());
                }
                if (Ids.Count() > 0)
                {
                    for (int i = 0; i < Ids.Count(); i++)
                    {
                        tblOrder objTblOrder = new tblOrder();
                        objTblOrder.pkOrderId = Convert.ToInt32(Ids[i]);
                        objTblOrder.DeletedBy = UserId;
                        objTblOrder.DeletedDate = CurrentDate;
                        ObjListtblOrders.Add(objTblOrder);

                    }
                    int result = ObjBALOrders.DeleteOrders(ObjListtblOrders);
                    if (result == 1)
                    {
                        Message = "Delete successfully";
                        CSS = "successmsg";
                    }
                    else
                    {
                        Message = "Some error ocurred. Please try again later.";
                        CSS = "Error";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBALOrders = null;
            }

            return Json(new { Msg = Message, css = CSS });
        }
        #endregion
        public FilePathResult GenerateAuthorizationform(string OrderId)
        {
            int Order_ID = Convert.ToInt32(OrderId);
            string pdfFileName = string.Empty;
            BALGeneral ObjBALGeneral = new BALGeneral();
            CustomOrderCollection ObjCustomOrderCollection = new CustomOrderCollection();
            BALOrders ObjBALOrders = new BALOrders();
            ObjCustomOrderCollection = ObjBALOrders.GetOrderDetailsByOrderId(Order_ID);
            DateTime Ordereddate = Convert.ToDateTime(ObjCustomOrderCollection.ListOrderDetail.ElementAt(0).OrderDt.ToString());
            string Ordered_date = Convert.ToString(Ordereddate.ToShortDateString());

            string firstname = ObjCustomOrderCollection.ObjOrderSearchedData.SearchedFirstName;
            string ssn = ObjCustomOrderCollection.ObjOrderSearchedData.SearchedSsn.ToString();
            string dob = ObjCustomOrderCollection.ObjOrderSearchedData.SearchedDob.ToString();
            string email = ObjCustomOrderCollection.ObjOrderSearchedData.SearchedApplicantEmail.ToString();
            int CompanyId = Convert.ToInt32(ObjCustomOrderCollection.ListOrderDetail.ElementAt(0).fkCompanyId.ToString());
            string LastName = ObjCustomOrderCollection.ObjOrderSearchedData.SearchedLastName.ToString();
            string FilePath = string.Empty;
            pdfFileName = ObjBALGeneral.GenerateAuthorizationform(firstname, ssn, dob, email, CompanyId, Ordered_date, LastName);
            if (CompanyId == 6587)
            {
                FilePath = HttpContext.Server.MapPath(@"~/Resources/Upload/613NoticePdf/axiomstaffingdisclosureandauthorization.pdf");
            }

            else
            {
                FilePath = HttpContext.Server.MapPath(@"~/Resources/Upload/613NoticePdf/disclosureandauthorization.pdf");
            }
            string fileext = System.IO.Path.GetExtension("613NoticePdf.pdf");
            string CType = "application/" + fileext.Replace(".", "");
            string strFileTitle = pdfFileName;

            return File(FilePath, CType, strFileTitle);


        }
    }
}
