﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using System.Web.Security;
namespace Emerge.Controllers.Corporate
{
    public partial class CorporateController : BaseEmergeController
    {
        //
        // GET: /MyDocuments/
        public ActionResult MyForms()
        {
            MyFormModel ObjModel = new MyFormModel();
            //List<MyFormModelColl> MyFormModel = new List<MyFormModelColl>();
            ObjModel = GetMyFormCollection();
            return View(ObjModel);
        }


        public FilePathResult DownloadMyForms(string FPathTitle)
        {
            string FilePath = Server.MapPath("~/Resources/Upload/CustomForm/" + FPathTitle);
            string fileext = System.IO.Path.GetExtension(FilePath);
            string CType = "application/" + fileext.Replace(".", "");
            string fileName = Path.GetFileName(FilePath);
            return File(FilePath, CType, fileName);
        }

        public FilePathResult DownloadCustomForms(string FPathTitle)
        {
            string FilePath = Server.MapPath("~/Resources/Upload/CustomForm/" + FPathTitle);
            string fileext = System.IO.Path.GetExtension(FilePath);
            string CType = "application/" + fileext.Replace(".", "");
            string fileName = Path.GetFileName(FilePath);
            return File(FilePath, CType, fileName);
        }


        public MyFormModel GetMyFormCollection()
        {
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                Guid UserId = Guid.Empty;
                int pkCompanyId = 0;
                MyFormModel ObjMyFormModel = new MyFormModel();
                List<MyFormModelColl> ObjData = new List<MyFormModelColl>();
                List<CustomFormModelColl> ObjCustomFormData = new List<CustomFormModelColl>();
                //get user information
                MembershipUser Member = Membership.GetUser(User.Identity.Name);
                if (Member != null)
                    UserId = new Guid(Member.ProviderUserKey.ToString());

                //get company information
                List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjCompanyinfo = GetCompanyInfo(UserId);
                if (ObjCompanyinfo != null)
                {
                    pkCompanyId = ObjCompanyinfo.ElementAt(0).pkCompanyId;
                }

                ObjData = (from u in ObjDALDataContext.tblmyforms
                           where u.IsActive == true
                           select new MyFormModelColl
                           {
                               pkformId = u.pkformId,
                               Title = u.Title,
                               Description = u.Description,
                               Path = u.Path,
                               ModifiedLoggedId = u.ModifiedLoggedId != null ? (Guid)u.ModifiedLoggedId : Guid.Empty,
                               ModifiedDate = u.ModifiedDate
                           }).ToList();

                ObjMyFormModel.Collection = ObjData;


                ObjCustomFormData = (from c in ObjDALDataContext.tblCustomForms
                                     where c.IsActive == true && c.FkCompanyId == pkCompanyId
                                     select new CustomFormModelColl
                                     {
                                         PkCustomFormId = c.pkCustomFormId,
                                         Title = c.Title,
                                         Description = c.Description,
                                         Path = c.Path,
                                         ModifiedLoggedId = c.ModifiedLoggedId != null ? (Guid)c.ModifiedLoggedId : Guid.Empty,
                                         UpdatedByUserId = c.UploadedByUserId != null ? (Guid)c.UploadedByUserId : Guid.Empty,
                                         ModifiedDate = c.ModifiedDate
                                     }).ToList();
                ObjMyFormModel.CustomFormCollection = ObjCustomFormData;

                return ObjMyFormModel;

            }
        }


        public int UpdateForms(tblmyform Objtblmyform)
        {
            int Result = 0;
            tblmyform ObjDoc = new tblmyform();
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                try
                {

                    ObjDoc = ObjDALDataContext.tblmyforms.Where(d => d.pkformId == Objtblmyform.pkformId).FirstOrDefault();
                    if (ObjDoc != null)
                    {
                        ObjDoc.Title = Objtblmyform.Title;
                        ObjDoc.Description = Objtblmyform.Description;
                        ObjDoc.Path = Objtblmyform.Path;
                        ObjDoc.ModifiedDate = Objtblmyform.ModifiedDate;
                        ObjDoc.ModifiedLoggedId = Objtblmyform.ModifiedLoggedId;
                        ObjDALDataContext.SubmitChanges();
                        Result = 1;
                    }


                }
                catch (Exception)
                {

                    Result = 0;
                }
            }
            return Result;
        }

        /// <summary>
        /// corportate manager able to Save custom forms
        /// </summary>
        /// <param name="PDFfileName"></param>
        /// <param name="txtDocumentTitle"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SaveCustomFormDetails(HttpPostedFileBase PDFfileName, string txtDocumentTitle, FormCollection formColl)
        {
            string strPagetoRedirect = string.Empty;
            int IntResult = 0;
            string Message = string.Empty;
            Guid UserId = Guid.Empty;
            int pkCompanyId = 0;
            string Description = formColl["CustomFormDescription"];
            BALCustomForm objBalCustomForm = new BALCustomForm();
            MyFormModel ObjMyForm = new MyFormModel();
            try
            {
                if (PDFfileName != null)
                {
                    if (PDFfileName != null && PDFfileName.ContentLength > 0)
                    {
                        if (PDFfileName.FileName != string.Empty)
                        {

                            if (CheckExtensionsForCustomForm(PDFfileName))
                            {
                                //string sExt = Path.GetExtension(PDFfileName.FileName).ToLower();
                                string sFileName = PDFfileName.FileName;
                                string sFilePath = string.Empty;
                                sFilePath = HttpContext.Server.MapPath(@"~/Resources/Upload/CustomForm/") + sFileName;
                                string Title = string.Empty;
                                Title = Path.GetFileNameWithoutExtension(sFilePath);
                                if (System.IO.File.Exists(HttpContext.Server.MapPath(@"~\Resources\Upload\CustomForm\") + sFileName))
                                {
                                    try
                                    {
                                        System.IO.File.Delete(HttpContext.Server.MapPath(@"~\Resources\Upload\CustomForm\") + sFileName);
                                    }
                                    catch { }
                                }

                                PDFfileName.SaveAs(HttpContext.Server.MapPath(@"~\Resources\Upload\CustomForm\") + sFileName);

                                //string fPath = Server.MapPath("~/Resources/Upload/CustomForm/") + sFileName;

                                //get user information
                                MembershipUser Member = Membership.GetUser(User.Identity.Name);
                                if (Member != null)
                                    UserId = new Guid(Member.ProviderUserKey.ToString());
                                //get company information
                                List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjCompanyinfo = GetCompanyInfo(UserId);
                                if (ObjCompanyinfo != null)
                                {
                                    pkCompanyId = ObjCompanyinfo.ElementAt(0).pkCompanyId;
                                }

                                tblCustomForm ObjTblCustomForm = new tblCustomForm();
                                ObjTblCustomForm.Title = Title;
                                ObjTblCustomForm.Path = sFileName;
                                ObjTblCustomForm.ModifiedDate = DateTime.Now;
                                ObjTblCustomForm.ModifiedLoggedId = UserId;
                                ObjTblCustomForm.UploadedByUserId = UserId;
                                ObjTblCustomForm.Description = Description;
                                ObjTblCustomForm.IsActive = true;
                                ObjTblCustomForm.FkCompanyId = pkCompanyId;
                                IntResult = objBalCustomForm.InsertCustomFormDetails(ObjTblCustomForm);

                                if (IntResult == 1)
                                {
                                    Message = "Custom form uploaded successfully.";
                                }
                                else
                                {
                                    Message = "Occured Problem during the upload custom form.";
                                }

                                strPagetoRedirect = "MyForms";
                            }
                            else
                            {
                                strPagetoRedirect = "MyForms";
                                Message = "Only .pdf files are allowed to upload.";
                            }

                        }

                    }
                }
            }
            catch
            {
                strPagetoRedirect = "MyForms";
                Message = "Some error occurred while uploading. Please try again!";
            }
            ObjMyForm.Message = Message;
            return RedirectToAction(strPagetoRedirect);
        }
        public bool CheckExtensionsForCustomForm(HttpPostedFileBase Document)
        {
            string Extension = string.Empty;

            if (Document.FileName != string.Empty)
            {
                Extension = Path.GetExtension(Document.FileName).ToLower();
                if (Document.ContentLength > 0)
                {
                    if (Extension == ".pdf" || Extension == ".doc")
                    {

                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return true;
        }




    }
    public class MyFormColl
    {
        public int pkformId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
        public Guid ModifiedLoggedId { get; set; }
        public DateTime? ModifiedDate { get; set; }

    }
}
