﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using System.Web.Security;
using Emerge.Data;
using System.IO;
using System.Data.OleDb;
using System.Data;
using LumenWorks.Framework.IO.Csv;
using System.Xml;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using Emerge.Common;
using System.Configuration;
using AuthorizeNet;

namespace Emerge.Controllers.Corporate
{
    public partial class CorporateController : BaseEmergeController
    {
        //
        // GET: /AccountHistory/

        public ActionResult AccountHistory()
        {
            ViewBag.DefaultCompany = GetCurrentUserDetail().First().pkCompanyId;
            return View();
        }


        public ActionResult GetAllAccountHistory()
        {
            BALInvoice objbalinvoice = new BALInvoice();
            int pkcompanyid = GetCurrentUserDetail().First().pkCompanyId;
            // Guid pkcompanyid= Guid.Parse(Session["pkCompanyId"].ToString());
            var list = objbalinvoice.LoadAccountHistory(pkcompanyid);
            return Json(new { Reclist = list, TotalRec = list.Count });
        }

        //To Get All Invoice and Payment Detail based on Selected Date.
        public ActionResult GetAllAccountHistoryByDate(string from, string to)
        {
            BALInvoice objbalinvoice = new BALInvoice();
            int pkcompanyid = GetCurrentUserDetail().First().pkCompanyId;
            DateTime fromDD = Convert.ToDateTime(from);
            DateTime toDD = Convert.ToDateTime(to);
            var list = objbalinvoice.LoadAccountHistoryByDate(pkcompanyid, fromDD, toDD);
            return Json(new { Reclist = list, TotalRec = list.Count });
        }
        //To get Invoice details.
        public ActionResult GetInvoiceHistory()
        {
            BALInvoice objBalInvoice = new BALInvoice();
            int pkcompanyid = GetCurrentUserDetail().First().pkCompanyId;
            var list = objBalInvoice.LoadInvoiceHistoryByCompanyId(pkcompanyid);
            return Json(new { Reclist = list, TotalRec = list.Count });
        }
        //To get Invoice details based on selected date.
        public ActionResult GetInvoiceHistoryByDate(string from, string to)
        {
            BALInvoice objBalInvoice = new BALInvoice();
            int pkcompanyid = GetCurrentUserDetail().First().pkCompanyId;
            DateTime fromDD = Convert.ToDateTime(from);
            DateTime toDD = Convert.ToDateTime(to);
            var list = objBalInvoice.LoadInvoiceHistoryByCompanyIdByDate(pkcompanyid, fromDD, toDD);
            return Json(new { Reclist = list, TotalRec = list.Count });
        }
        //To get Payment history details.
        public ActionResult GetPaymentHistory()
        {
            BALPayment objPayment = new BALPayment();
            int pkcompanyid = GetCurrentUserDetail().First().pkCompanyId;
            var list = objPayment.LoadPaymentHistoryByCompanyId(pkcompanyid);
            return Json(new { Reclist = list, TotalRec = list.Count });
        }

        //To get Payment history details based on selected date.
        public ActionResult GetPaymentHistoryByDate(string from, string to)
        {
            BALPayment objPayment = new BALPayment();
            int pkcompanyid = GetCurrentUserDetail().First().pkCompanyId;
            DateTime fromDD = Convert.ToDateTime(from);
            DateTime toDD = Convert.ToDateTime(to);
            var list = objPayment.LoadPaymentHistoryByCompanyIdByDate(pkcompanyid, fromDD, toDD);
            return Json(new { Reclist = list, TotalRec = list.Count });
        }
        /// <summary>
        /// To get prepayment history detail based on logged on user company id.
        /// </summary>
        /// <returns></returns>
        public ActionResult GetPrePaymentHistory()
        {

            BALPayment objPayment = new BALPayment();
            int pkcompanyid = GetCurrentUserDetail().First().pkCompanyId;
            var list = objPayment.LoadPrePaymentHistoryByCompanyId(pkcompanyid);
            return Json(new { Reclist = list, TotalRec = list.Count });
        }
        /// <summary>
        /// To get prepayment history detail based on logged on user company id and selected date.
        /// </summary>
        /// <returns></returns>
        public ActionResult GetPrePaymentHistoryByDate(string from, string to)
        {
            BALPayment objPayment = new BALPayment();
            int pkcompanyid = GetCurrentUserDetail().First().pkCompanyId;
            DateTime fromDD = Convert.ToDateTime(from);
            DateTime toDD = Convert.ToDateTime(to);
            var list = objPayment.LoadPrePaymentHistoryByCompanyIdByDate(pkcompanyid, fromDD, toDD);
            return Json(new { Reclist = list, TotalRec = list.Count });
        }
        /// <summary>
        /// To get prepaymentn recharge history based on logged on user company id.
        /// </summary>
        /// <returns></returns>
        public ActionResult GetPrePaymentRechargeHistory()
        {

            BALPayment objPayment = new BALPayment();
            int pkcompanyid = GetCurrentUserDetail().First().pkCompanyId;
            var list = objPayment.LoadRechargePrePaymentHistory(pkcompanyid);
            return Json(new { Reclist = list, TotalRec = list.Count });
        }

        /// <summary>
        /// To get prepaymentn recharge history based on logged on user company id and date filter.
        /// </summary>
        /// <returns></returns>
        public ActionResult GetPrePaymentRechargeHistoryByDate(string from, string to)
        {

            BALPayment objPayment = new BALPayment();
            int pkcompanyid = GetCurrentUserDetail().First().pkCompanyId;
            DateTime fromDD = Convert.ToDateTime(from);
            DateTime toDD = Convert.ToDateTime(to);
            var list = objPayment.LoadRechargePrePaymentHistoryByDate(pkcompanyid, fromDD, toDD);
            return Json(new { Reclist = list, TotalRec = list.Count });
        }


        /// <summary>
        /// To get all Prepayment information.
        /// </summary>
        /// <returns></returns>
        public ActionResult GetAllPrePaymentDetail()
        {
            BALPayment objPayment = new BALPayment();
            int pkcompanyid = GetCurrentUserDetail().First().pkCompanyId;
            var list = objPayment.GetAllPrePaymentDetails(pkcompanyid);
            return Json(new { Reclist = list, TotalRec = list.Count });
        }

        /// <summary>
        /// To get all Prepayment information by date
        /// </summary>
        /// <returns></returns>
        public ActionResult GetAllPrePaymentDetailByDate(string from, string to)
        {
            BALPayment objPayment = new BALPayment();
            int pkcompanyid = GetCurrentUserDetail().First().pkCompanyId;
            DateTime fromDD = Convert.ToDateTime(from);
            DateTime toDD = Convert.ToDateTime(to);
            var list = objPayment.GetAllPrePaymentDetailsByDate(pkcompanyid, fromDD, toDD);
            return Json(new { Reclist = list, TotalRec = list.Count });
        }

        /// <summary>
        /// To get all Due On Receipt Information based on date filter(if selected)
        /// </summary>
        /// <returns></returns>
        public ActionResult GetAllDueonReceiptHistoryByDate(string from, string to, string reportType)
        {
            BALPayment objPayment = new BALPayment();
            int pkcompanyid = GetCurrentUserDetail().First().pkCompanyId;
            DateTime fromDD;
            DateTime toDD;
            if (from != string.Empty)
            {
                fromDD = Convert.ToDateTime(from);
                toDD = Convert.ToDateTime(to);
            }
            else
            {
                fromDD = Convert.ToDateTime("01/01/1900");
                toDD = Convert.ToDateTime("01/01/1900");
            }
            var list = objPayment.GetAllDueonReceiptHistoryByDate(pkcompanyid, fromDD, toDD, Convert.ToInt32(reportType));
            return Json(new { Reclist = list, TotalRec = list.Count });
        }


        /// <summary>
        /// To get Invoices Due On Receipt Information based on date filter(if selected)
        /// </summary>
        /// <returns></returns>
        public ActionResult GetInvoicesDueonReceiptHistoryByDate(string from, string to, string reportType)
        {
            BALPayment objPayment = new BALPayment();
            int pkcompanyid = GetCurrentUserDetail().First().pkCompanyId;
            DateTime fromDD;
            DateTime toDD;
            if (from != string.Empty)
            {
                fromDD = Convert.ToDateTime(from);
                toDD = Convert.ToDateTime(to);
            }
            else
            {
                fromDD = Convert.ToDateTime("01/01/1900");
                toDD = Convert.ToDateTime("01/01/1900");
            }
            var list = objPayment.GetAllDueonReceiptHistoryByDate(pkcompanyid, fromDD, toDD, Convert.ToInt32(reportType));
            return Json(new { Reclist = list, TotalRec = list.Count });
        }

        /// <summary>
        /// To get Invoices Due On Receipt Information based on date filter(if selected)
        /// </summary>
        /// <returns></returns>
        public ActionResult GetPaymentsDueonReceiptHistoryByDate(string from, string to, string reportType)
        {
            BALPayment objPayment = new BALPayment();
            int pkcompanyid = GetCurrentUserDetail().First().pkCompanyId;
            DateTime fromDD;
            DateTime toDD;
            if (from != string.Empty)
            {
                fromDD = Convert.ToDateTime(from);
                toDD = Convert.ToDateTime(to);
            }
            else
            {
                fromDD = Convert.ToDateTime("01/01/1900");
                toDD = Convert.ToDateTime("01/01/1900");
            }
            var list = objPayment.GetAllDueonReceiptHistoryByDate(pkcompanyid, fromDD, toDD, Convert.ToInt32(reportType));
            return Json(new { Reclist = list, TotalRec = list.Count });
        }

        public ActionResult MakePayment_Information(string CompanyId, string invoiceid)
        {
            //string Message = string.Empty;
            string content = string.Empty;
            BALCompany serObj = new BALCompany();
            //int CID = Convert.ToInt32(CompanyId);
            //int chkPaymentTerm = serObj.GetCompanyPaymentType(CID);


           // AdCompanyModel ObjModel = new AdCompanyModel();

            int locationid = Convert.ToInt32(invoiceid);
            var list = serObj.GetAllInvoicePerInvoice(Convert.ToInt32(CompanyId), locationid).FirstOrDefault();
            int BillingMonth = Convert.ToInt32(list.InvoiceMonth);
            int BillingYear = Convert.ToInt32(list.InvoiceYear);
            int invoiceidmain = Convert.ToInt32(list.pkInvoiceId);

            using (Emerge.Services.BALInvoiceQuickBook a = new Services.BALInvoiceQuickBook())
            {
                var PrdoctData = a.getAllRecord(Convert.ToInt32(CompanyId), BillingMonth, BillingYear, Convert.ToInt32(invoiceidmain));


                List<InvoiceRecordDiscoountPerOrderResult> InvoiceLevelData = new List<InvoiceRecordDiscoountPerOrderResult>();
                List<InvoiceLevelDiscountQuickBookResult> InvoiceLevelDiscountQuickBook = new List<InvoiceLevelDiscountQuickBookResult>();
                using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
                {
                    InvoiceLevelData = ObjDALDataContext.InvoiceRecordDiscoountPerOrder(Convert.ToInt32(CompanyId), BillingMonth, BillingYear, Convert.ToInt32(invoiceidmain)).ToList<InvoiceRecordDiscoountPerOrderResult>();

                    InvoiceLevelDiscountQuickBook = ObjDALDataContext.InvoiceLevelDiscountQuickBook(Convert.ToInt32(invoiceidmain)).ToList<InvoiceLevelDiscountQuickBookResult>();

                }

                // jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(data);
                if (PrdoctData != null)
                {

                    if (PrdoctData.Count > 0)
                    {

                        //EmergeDALDataContext myDB = new EmergeDALDataContext();
                        content = "<div style='float:right;'><img href></div>";
                        content += "</br><table align='Center' cellspacing='0' cellpadding='4' border='1' style='background-color: White;width:100%; border-collapse: collapse; float:left;' id='gvOpenInvoice' rules='all' class='Grid'>";
                        content += "<tbody><tr align='center' style='font-weight: bold;color: #0B55C4;' class='GridHeader'><th scope='col' style='width: 70px;text-align: center; padding: 5px 0px;'>Quantity</th><th scope='col' style='padding: 5px 0px;text-align: center;width: 90px;'>Product</th><th scope='col' style='text-align: center;padding: 5px 0px;'>Description</th><th scope='col' style='width: 50px;text-align: center;padding: 5px 0px;'>Price</th><th scope='col' style='width: 65px;padding: 5px 0px;text-align: center;'>Amount</th>";
                        decimal PrdoctData_Total = 0;
                        if (PrdoctData != null)
                        {
                            for (var i = 0; i < PrdoctData.Count; i++)
                            {
                                content += "<tr>";
                                content += "<td align='right' style='text-align: center;padding: 4px 0px;'>" + (PrdoctData.ElementAt(i).ProductQty).ToString() + "</td>";
                                content += "<td align='right' style='text-align: center;'>" + (PrdoctData.ElementAt(i).ProductDisplayName).ToString() + "</td>";
                                using (EmergeDALDataContext myDB = new EmergeDALDataContext())
                                {
                                    content += "<td align='right' style='text-align: center;'>" + (myDB.tblLocations.Where(m => m.pkLocationId == PrdoctData.ElementAt(i).fkLocationId).SingleOrDefault().City).ToString() + "</td>";
                                }
                                content += "<td align='right' style='text-align: center;'>" + "$" + (PrdoctData.ElementAt(i).ReportAmount).ToString() + "</td>";
                                content += "<td align='right' style='text-align: center;'>" + "$" + ((PrdoctData.ElementAt(i).ReportAmount * PrdoctData.ElementAt(i).ProductQty)).ToString() + "</td>";
                                content += "</tr>";

                                PrdoctData_Total = Convert.ToDecimal(PrdoctData_Total + (PrdoctData.ElementAt(i).ReportAmount * PrdoctData.ElementAt(i).ProductQty));

                            }
                        }


                        decimal InvoiceLevelData_Total = 0;
                        if (InvoiceLevelData != null)
                        {

                            for (var i = 0; i < InvoiceLevelData.Count; i++)
                            {
                                content += "<tr>";
                                content += "<td align='right' style='text-align: center;'>" + (1).ToString() + "</td>";
                                content += "<td align='right' style='text-align: center;'>" + (InvoiceLevelData.ElementAt(i).AdjustmentType).ToString() + "</td>";
                                content += "<td align='right' style='text-align: center;'>" + "Order No:" + InvoiceLevelData.ElementAt(i).OrderNo.ToString() + " Product:" + InvoiceLevelData.ElementAt(i).ProductDisplayName.ToString() + " Date:" + InvoiceLevelData.ElementAt(i).DiscountDate.ToString() + " Note:  " + InvoiceLevelData.ElementAt(i).Note.ToString() + "</td>";

                                if (InvoiceLevelData.ElementAt(i).AdjustmentType == "Discount")
                                {
                                    content += "<td align='right' style='text-align: center;'>" + "$" + "-" + (InvoiceLevelData.ElementAt(i).DiscountAmount).ToString() + "</td>";
                                    content += "<td align='right' style='text-align: center;'>" + "$" + "-" + (InvoiceLevelData.ElementAt(i).DiscountAmount).ToString() + "</td>";
                                    InvoiceLevelData_Total = Convert.ToDecimal(InvoiceLevelData_Total - (InvoiceLevelData.ElementAt(i).DiscountAmount));

                                }
                                else
                                {
                                    content += "<td align='right' style='text-align: center;'>" + "$" + (InvoiceLevelData.ElementAt(i).DiscountAmount).ToString() + "</td>";
                                    content += "<td align='right' style='text-align: center;'>" + "$" + (InvoiceLevelData.ElementAt(i).DiscountAmount).ToString() + "</td>";
                                    InvoiceLevelData_Total = Convert.ToDecimal(InvoiceLevelData_Total + (InvoiceLevelData.ElementAt(i).DiscountAmount));
                                }


                                content += "</tr>";
                            }
                        }

                        decimal InvoiceLevelDiscountQuickBook_Total = 0;
                        if (InvoiceLevelDiscountQuickBook != null)
                        {

                            for (var i = 0; i < InvoiceLevelDiscountQuickBook.Count; i++)
                            {
                                content += "<tr>";
                                content += "<td align='right' style='text-align: center;'>" + (1).ToString() + "</td>";
                                content += "<td align='right' style='text-align: center;'>" + (InvoiceLevelDiscountQuickBook.ElementAt(i).AdjustmentType).ToString() + "</td>";
                                content += "<td align='right' style='text-align: center;'>" + ("Invoice Data").ToString() + "</td>";

                                if (InvoiceLevelDiscountQuickBook.ElementAt(i).AdjustmentType == "Discount")
                                {
                                    content += "<td align='right' style='text-align: center;'>" + "$" + "-" + InvoiceLevelDiscountQuickBook.ElementAt(i).DiscountAmount + "</td>";
                                    content += "<td align='right' style='text-align: center;'>" + "$" + "-" + (InvoiceLevelDiscountQuickBook.ElementAt(i).DiscountAmount).ToString() + "</td>";
                                    InvoiceLevelDiscountQuickBook_Total = Convert.ToDecimal(InvoiceLevelDiscountQuickBook_Total - (InvoiceLevelDiscountQuickBook.ElementAt(i).DiscountAmount));
                                }
                                else
                                {
                                    content += "<td align='right' style='text-align: center;'>" + "$" + InvoiceLevelDiscountQuickBook.ElementAt(i).DiscountAmount + "</td>";
                                    content += "<td align='right' style='text-align: center;'>" + "$" + (InvoiceLevelDiscountQuickBook.ElementAt(i).DiscountAmount).ToString() + "</td>";
                                    InvoiceLevelDiscountQuickBook_Total = Convert.ToDecimal(InvoiceLevelDiscountQuickBook_Total + (InvoiceLevelDiscountQuickBook.ElementAt(i).DiscountAmount));
                                }

                                content += "</tr>";

                            }
                        }

                        content += "<tr>";
                        content += "<tr><td colspan='4' align='right'>Total Amount :</td>";
                        content += "<td align='right' style='text-align: center;'>" + "$" + Convert.ToDecimal(PrdoctData_Total + InvoiceLevelData_Total + InvoiceLevelDiscountQuickBook_Total) + "</td>";
                        content += "</tr>";

                        content += "</tbody>";
                        content += "</table>";


                    }
                }
            }
            //List<proc_LoadOpenInvoiceByCompanyIdResult> list = new List<proc_LoadOpenInvoiceByCompanyIdResult>();
            //list = serObj.LoadOpenInVoice(CID).ToList();
            return Json(new { content = content });
        }
        public ActionResult GetBillingCompanyInfo()
        {

            BALPayment ObjBALPayment = new BALPayment();
            List<proc_GetPaymentBillingInfoPerCompanyIdResult> ObjDbCollection = new List<proc_GetPaymentBillingInfoPerCompanyIdResult>();
            string CardNumber = string.Empty;
            int CompanyId = GetCurrentUserDetail().First().pkCompanyId;

            try
            {
                ObjDbCollection = ObjBALPayment.GetPaymentBillingInfoByCompanyId(CompanyId);
                //if (ObjDbCollection.Count > 0)
                //{
                //    //CardNumber = EncryptDecrypt.Decrypt(ObjDbCollection.ElementAt(0).CardNumber.ToString());
                //}
            }
            finally
            {
                ObjBALPayment = null;
            }

            return Json(new { CardNumber = CardNumber, ObjDbCollection = ObjDbCollection });
        }





        private string GetInvoiceNumber(long pkInvoiceId)
        {
            string InvoiceNumber = "";
            BALInvoice ObjBALInvoice = new BALInvoice();
            try
            {
                List<tblInvoice> ObjList = new List<tblInvoice>();
                ObjList = ObjBALInvoice.GetInvoiceNumber(pkInvoiceId);
                InvoiceNumber = ObjList.ElementAt(0).InvoiceNumber.ToString();
            }
            finally
            {
                ObjBALInvoice = null;
            }
            return InvoiceNumber;
        }

        private string GetInvoiceAmount(long pkInvoiceId)
        {
            string InvoiceBalAmount = "0";
            BALInvoice ObjBALInvoice = new BALInvoice();
            try
            {
                List<tblInvoice> ObjList = new List<tblInvoice>();
                ObjList = ObjBALInvoice.GetInvoiceBalanceAmount(pkInvoiceId);
                decimal Total = 0;// ObjList.ElementAt(0).TotalInvoiceAmount - ObjList.ElementAt(0).TotalPaidAmount;
                InvoiceBalAmount = Total.ToString();
            }
            finally
            {
                ObjBALInvoice = null;
            }
            return InvoiceBalAmount;
        }



        private string MakePay(decimal PayInvoiceAmount, bool PayALLInvoice, long pkInvoiceId, string strInvoiceNumber, FormCollection frm, int TranErrorMsgCount)
        {
            StringBuilder ObjStringBuilder1 = new StringBuilder();
            BALInvoice ObjBALInvoice = new BALInvoice();
            try
            {
                string APILoginId = ConfigurationManager.AppSettings["APILoginID"];
                string TransactionKey = ConfigurationManager.AppSettings["TransactionKey"];
                bool TestMode = Convert.ToBoolean(ConfigurationManager.AppSettings["TestMode"]);
                Gateway gateway = new Gateway(APILoginId, TransactionKey, TestMode);
                decimal PayAmount = PayInvoiceAmount;

                string[] ddlCardExpiryMonth = frm.GetValue("ddlCardExpiryMonth").AttemptedValue.Split(',');
                string[] ddlCardExpiryYear = frm.GetValue("ddlCardExpiryYear").AttemptedValue.Split(',');
                string[] txtCardumber = frm.GetValue("txtCardumber").AttemptedValue.Split(',');

                string[] txtFname = frm.GetValue("txtFname").AttemptedValue.Split(',');
                string[] txtLname = frm.GetValue("txtLname").AttemptedValue.Split(',');
                string[] txtComapny = frm.GetValue("txtComapny").AttemptedValue.Split(',');
                string[] txtAddress = frm.GetValue("txtAddress").AttemptedValue.Split(',');
                string[] txtCity = frm.GetValue("txtCity").AttemptedValue.Split(',');
                string[] ddlState = frm.GetValue("ddlState").AttemptedValue.Split(',');
                string[] ddlCountry = frm.GetValue("ddlCountry").AttemptedValue.Split(',');
                string[] txtEmail = frm.GetValue("txtEmail").AttemptedValue.Split(',');
                string[] txtZip = frm.GetValue("txtZip").AttemptedValue.Split(',');
                //string[] CompanyId = frm.GetValue("CompanyId").AttemptedValue.Split(',');

                int ExpiryMonth = Convert.ToInt32(ddlCardExpiryMonth[0]);
                int ExpityYear = Convert.ToInt32(ddlCardExpiryYear[0]);
                string Monthyear = ExpityYear + "-" + ExpiryMonth;

                //EcheckRequest objeCheck = new EcheckRequest(EcheckType.WEB, PayAmount, "011000015", "999999999", BankAccountType.Savings, "FEDERAL RESERVE BANK OF ABC ", "TestaccountName", "12457802");

                //var Result = gateway.Send(objeCheck);

                AuthorizationRequest obj = new AuthorizationRequest(HttpUtility.HtmlEncode(txtCardumber[0].Trim()), Monthyear, PayAmount, "desc");
                obj.FirstName = HttpUtility.HtmlEncode(txtFname[0].Trim());
                obj.LastName = HttpUtility.HtmlEncode(txtLname[0].Trim());
                obj.Company = HttpUtility.HtmlEncode(txtComapny[0].Trim());
                obj.Address = HttpUtility.HtmlEncode(txtAddress[0].Trim());
                obj.City = HttpUtility.HtmlEncode(txtCity[0].Trim());
                obj.State = ddlState[0].Split('_')[1];
                obj.Zip = HttpUtility.HtmlEncode(txtZip[0].Trim());
                obj.Country = ddlCountry[0];
                obj.Email = HttpUtility.HtmlEncode(txtEmail[0].Trim());
                var Result = gateway.Send(obj);
                if (Result.Approved == true)
                {
                    tblPaymentHistory ObjtblPaymentHistory = new tblPaymentHistory();
                    ObjtblPaymentHistory.Address = txtAddress[0].Trim();
                    ObjtblPaymentHistory.CardExpiryMonth = Convert.ToInt16(ddlCardExpiryMonth[0]);
                    ObjtblPaymentHistory.CardExpiryYear = Convert.ToInt16(ddlCardExpiryYear[0]);
                    ObjtblPaymentHistory.CardNumber = EncryptDecrypt.Encrypt(txtCardumber[0].Trim());
                    ObjtblPaymentHistory.City = txtCity[0].Trim();
                    ObjtblPaymentHistory.CompanyName = txtComapny[0].Trim();
                    ObjtblPaymentHistory.CreatedDateTime = DateTime.Now;
                    ObjtblPaymentHistory.Email = txtEmail[0].Trim();
                    ObjtblPaymentHistory.FirstName = txtFname[0].Trim();
                    ObjtblPaymentHistory.fkCompanyId = GetCurrentUserDetail().First().pkCompanyId;
                    ObjtblPaymentHistory.fkInvoiceId = pkInvoiceId;
                    Guid ObjGuid = GetCurrentUserDetail().First().UserId;
                   // ObjtblPaymentHistory.fkCompanyUserId = 0;
                    ObjtblPaymentHistory.fkCompanyUserId = ObjGuid;
                    ObjtblPaymentHistory.fkCountryId = 1;
                    ObjtblPaymentHistory.fkStateId = Convert.ToInt16(ddlState[0].Split('_')[0]);
                    ObjtblPaymentHistory.LastName = txtLname[0].Trim();
                    ObjtblPaymentHistory.PaidAmount = PayAmount;
                    ObjtblPaymentHistory.PaidDate = DateTime.Now;
                    ObjtblPaymentHistory.Zipcode = txtZip[0].Trim();
                    ObjtblPaymentHistory.TransactionId = Result.TransactionID.ToString();
                    int Success = 0;
                    if (PayALLInvoice == true)
                    {
                        Success = ObjBALInvoice.AddPaymentHistoryDetails(ObjtblPaymentHistory, false, true); /*pay all invoice*/
                    }
                    else
                    {
                        Success = ObjBALInvoice.AddPaymentHistoryDetails(ObjtblPaymentHistory, false, false); /*pay invoice*/
                    }
                    if (Success == 1)
                    {
                        //   LoadOpenInvoice();
                        //  TranSuccMsgCount++;
                        ObjStringBuilder1.Append("<tr><td>");
                        if (PayALLInvoice == true)
                        {
                            ObjStringBuilder1.Append("Your transaction is completed successfully at authorize.net");
                        }
                        else
                        {
                            ObjStringBuilder1.Append("Your transaction is completed successfully at authorize.net for Invoice#: " + strInvoiceNumber + ". Your transaction number is " + Result.TransactionID.ToString());
                        }
                        ObjStringBuilder1.Append("</tr></td>");
                    }
                    else
                    {
                        TranErrorMsgCount++;
                        ObjStringBuilder1.Append("<tr><td>");
                        if (PayALLInvoice == true)
                        {
                            ObjStringBuilder1.Append("Your transaction is completed successfully at authorize.net. Your transaction number is  " + Result.TransactionID.ToString() + ". But an error occurred while saving payment details.");
                        }
                        else
                        {
                            ObjStringBuilder1.Append("Your transaction is completed successfully at authorize.net for Invoice#: " + strInvoiceNumber + ". Your transaction number is " + Result.TransactionID.ToString() + ". But an error occurred while saving payment details.");
                        }
                        ObjStringBuilder1.Append("</tr></td>");
                    }
                    //  MultiViewPayInvoice.ActiveViewIndex = 2;
                    // UPPayInvoice.Update();

                }
                else
                {
                    TranErrorMsgCount++;
                    ObjStringBuilder1.Append("<tr><td>");
                    if (PayALLInvoice == true)
                    {
                        ObjStringBuilder1.Append(Result.Message);
                    }
                    else
                    {
                        ObjStringBuilder1.Append(Result.Message.Replace(".", "") + " for Invoice#:  " + strInvoiceNumber);
                    }
                    ObjStringBuilder1.Append("</tr></td>");
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                ObjBALInvoice = null;
            }
            return ObjStringBuilder1.ToString();
        }




        public ActionResult MakeInvoicePayment(FormCollection frm)
        {
            int TranErrorMsgCount = 0;
            long pkInvoiceId;
            string Message = string.Empty;
            string[] rdbPaymentType = frm.GetValue("rdbPaymentType").AttemptedValue.Split(',');
            string[] OutStandingAmount = frm.GetValue("OutStandingAmount").AttemptedValue.Split(',');
            string[] ddlOpenInvoice = frm.GetValue("ddlOpenInvoice").AttemptedValue.Split(',');
            string[] txtOtherAmount = frm.GetValue("txtOtherAmount").AttemptedValue.Split(',');
            if (rdbPaymentType[0] == "1")
            {
                string[] chckPayment = frm.GetValue("chckPayment").AttemptedValue.Split(',');
                for (int num = 0; num < chckPayment.Length; num++)
                {
                    pkInvoiceId = Convert.ToInt64(chckPayment[num]);
                    Message = MakePay(Convert.ToDecimal(GetInvoiceAmount(pkInvoiceId)), false, pkInvoiceId, GetInvoiceNumber(pkInvoiceId), frm, TranErrorMsgCount);
                }
            }
            if (rdbPaymentType[0] == "2")
            {
                pkInvoiceId = 0;
                Message = MakePay(Convert.ToDecimal(OutStandingAmount[0]), true, pkInvoiceId, "", frm, TranErrorMsgCount);
            }
            if (rdbPaymentType[0] == "3")
            {
                pkInvoiceId = Convert.ToInt64(ddlOpenInvoice[0].ToString());
                Message = MakePay(Convert.ToDecimal(txtOtherAmount[0]), false, pkInvoiceId, GetInvoiceNumber(pkInvoiceId), frm, TranErrorMsgCount);
            }
            return Json(Message);
        }


        //private void GetCurrentUserDetailname()
        //{

        //     ObjBALCompanyUsers = new BALCompanyUsers();
        //    List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjDbCollection;
        //    MembershipUser ObjMembershipUser = Membership.GetUser(Utility.GetUID(User.Identity.Name));

        //    Guid GlobalMemberUserId = Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
        //    try
        //    {
        //        ObjDbCollection = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(GlobalMemberUserId);
        //        Session["pkCompanyId"] = ObjDbCollection.First().pkCompanyId.ToString();

        //    }
        //    finally
        //    {
        //        ObjBALCompanyUsers = null;
        //    }
        //}
    }
}
