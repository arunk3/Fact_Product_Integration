﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Emerge.Common;
using Emerge.Models;
using Emerge.Data;
using System.Text;
using System.Xml;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Web.Security;
using System.Configuration;
using System.Globalization;
using System.IO;
using Emerge.Data;

namespace Emerge.Controllers.Corporate
{


    public partial class CorporateController : BaseEmergeController
    {
        int MicroBiltEndPoint = 1;
        int vwpsStatus = 0;
        string PSResponse = string.Empty;
        #region View Report


        public ActionResult ViewReports()
        {
            Boolean flagOrderId = false;
            #region Get Query String
            BALOrders balObj = new BALOrders();
            ViewReportModel ObjModel = new ViewReportModel();
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["RR"]))
                {
                    ViewBag.RR = Convert.ToInt32(Request.QueryString["RR"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["_bck"]))
                {
                    ViewBag.bck = Convert.ToInt32(Request.QueryString["_bck"]);
                }

                int OrderId = 0;

                #region add and check view report page is open via taleo only

                if (TempData["loginfrom"] != null)
                {

                }
                else
                {
                    TempData["TaleoOrderId"] = null;
                }
                #endregion



                if (Request.QueryString["_OId"] != null || TempData["TaleoOrderId"] != null)
                {
                    #region Check Guid

                    // for taleo view report page to open id for single order by taleo user
                    if (TempData["TaleoOrderId"] != null)
                    {
                        VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n ----ViewReports---------start-----" + TempData["TaleoOrderId"], "TaleoErrorLog");
                        VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n ----ViewReports---------Session[TaleoOrderId]-----" + TempData["TaleoOrderId"], "TaleoErrorLog");
                        //change for unique data fetch using iframe
                        if (Request.QueryString["_OId"] != null)
                        {

                            OrderId = Convert.ToInt32(Request.QueryString["_OId"]);
                        }
                        else
                        {
                            VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n ----ViewReports---------else in Request.QueryString[_OId]-----" + Convert.ToInt32(TempData["TaleoOrderId"]), "TaleoErrorLog");
                            OrderId = Convert.ToInt32(TempData["TaleoOrderId"]);

                            ViewBag.TaleoOrderid = TempData["TaleoOrderId"];

                        }

                        //comment for some session issue when using 2 user on sinle browser 

                    }
                    else
                    {
                        OrderId = Convert.ToInt32(Request.QueryString["_OId"]);
                    }










                    if (Session["Admin"] != null)
                    {
                        string role = Session["Admin"].ToString();
                        if (role == "SystemAdmin" || role == "SupportManager" || role == "SupportTechnician")
                        {
                            //Pass if logged on user is Admin
                        }
                        else
                        {

                            string cmpId = string.Empty;
                            //if (Session["TaleoOrderId"] != null) nd-16 change 
                            if (TempData["TaleoOrderId"] != null)
                            {
                                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                                {
                                    //int Status = -1;

                                    var OrderComapnyid = (from t1 in DX.tblOrderDetails
                                                          where t1.fkOrderId == Convert.ToInt32(TempData["TaleoOrderId"])
                                                          select new { t1.fkCompanyId }).FirstOrDefault();
                                    // cmpId = 

                                    cmpId = Convert.ToString(OrderComapnyid.fkCompanyId);

                                }

                                // for taleo changes 



                            }
                            else
                            {
                                cmpId = Convert.ToString(Session["pkCompanyId"]);
                            }


                            flagOrderId = balObj.CheckOrderCompanyByOrderId(OrderId, Convert.ToInt32(cmpId));
                            if (!flagOrderId)
                            {
                                //return RedirectToAction("Index", "Home");
                                return RedirectToAction("SavedReports", "Corporate");
                            }
                        }
                    }

                    #endregion
                }
                else if (Request.QueryString["_AdditionalOrderId"] != null)
                {
                    OrderId = Convert.ToInt32(Request.QueryString["_AdditionalOrderId"]);
                }
                BALCompany ObjBALCompany = new BALCompany();
                ObjModel.AdditionalReports = ObjBALCompany.GetAdditionalReportsByPkOrderId(OrderId);
                int ReviewQueryString = 0;
                if (!string.IsNullOrEmpty(Request.QueryString["RR"]))
                {
                    ReviewQueryString = Convert.ToInt32(Request.QueryString["RR"]);
                }

                int ViewReportReview = 0;
                if (Request.Url.AbsolutePath.Contains("ViewReportReview"))
                {
                    ViewReportReview = 1;
                }


                ObjModel.OrderId = OrderId;



                #region User FullName

                string Username = string.Empty;

                if (Session["UserFullName"] != null)
                {
                    Username = Convert.ToString(Session["UserFullName"]);
                }
                else
                {
                    ObjBALCompanyUsers = new BALCompanyUsers();
                    MembershipUser Member = Membership.GetUser();//User.Identity.Name
                    if (Member != null)
                    {
                        Guid UserId = new Guid(Member.ProviderUserKey.ToString());
                        List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjData = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(UserId);
                        Session["CurrentUserCompanyName"] = ObjData.First().CompanyName;
                        Session["UserFullName"] = ObjData.First().FirstName + " " + ObjData.First().LastName;
                    }
                }

                #endregion

                if (Roles.IsUserInRole("SystemAdmin") || Roles.IsUserInRole("SupportManager") || Roles.IsUserInRole("SupportTechnician"))
                {
                    ObjModel.IsAdmin = true;
                }
                else
                {
                    ObjModel.IsAdmin = false;
                }

            #endregion

                #region Request/Response
                ViewBag.CountyExist = "3";
                GetViewReport(ObjModel, OrderId, ReviewQueryString, ViewReportReview, Username);
                if (System.Web.HttpContext.Current.Session["CountyExist"] != null)
                {
                    ViewBag.CountyExist = System.Web.HttpContext.Current.Session["CountyExist"];
                    System.Web.HttpContext.Current.Session["CountyExist"] = null;
                }

                #endregion

                if (TempData["messages"] != null)
                {
                    ViewBag.Message = TempData["messages"];
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("was deadlocked on lock resources with another process") || ex.Message.Contains("Value cannot be null") || ex.Message.Contains("Timeout expired"))
                {
                    return RedirectToAction("CustomDeadLockMsg", "Corporate");
                }
            }
            finally
            {

            }
            return View(ObjModel);
        }

        public ActionResult UserView_ViewReport()
        {
            Boolean flagOrderId = false;
            #region Get Query String
            BALOrders balObj = new BALOrders();
            ViewReportModel ObjModel = new ViewReportModel();
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["RR"]))
                {
                    ViewBag.RR = Convert.ToInt32(Request.QueryString["RR"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["_bck"]))
                {
                    ViewBag.bck = Convert.ToInt32(Request.QueryString["_bck"]);
                }
                int OrderId = 0;
                if (Request.QueryString["OrderId"] != null)
                {
                    OrderId = Convert.ToInt32(Request.QueryString["OrderId"]);
                }
                else if (Request.QueryString["_Old"] != null)
                {
                    OrderId = Convert.ToInt32(Request.QueryString["_Old"]);
                }
                else
                {
                    OrderId = Convert.ToInt32(Request.QueryString["_AdditionalOrderId"]);
                }


                if (OrderId != 0)
                {
                    #region Check Guid
                    OrderId = Convert.ToInt32(Request.QueryString["OrderId"]);
                    if (Session["Admin"] != null)
                    {
                        string role = Session["Admin"].ToString();
                        if (role == "SystemAdmin" || role == "SupportManager" || role == "SupportTechnician")
                        {
                            //Pass if logged on user is Admin
                        }
                        else
                        {

                            string cmpId = string.Empty;

                            cmpId = Convert.ToString(Session["pkCompanyId"]);


                            flagOrderId = balObj.CheckOrderCompanyByOrderId(OrderId, Convert.ToInt32(cmpId));
                            if (!flagOrderId)
                            {
                                return RedirectToAction("SavedReports", "Corporate");
                            }
                        }
                    }

                    #endregion
                }
                //else if (Request.QueryString["OrderId"] != null)
                //{
                //    OrderId = Convert.ToInt32(Request.QueryString["OrderId"]);
                //}
                BALCompany ObjBALCompany = new BALCompany();
                ObjModel.AdditionalReports = ObjBALCompany.GetAdditionalReportsByPkOrderId(OrderId);
                int ReviewQueryString = 0;
                if (!string.IsNullOrEmpty(Request.QueryString["RR"]))
                {
                    ReviewQueryString = Convert.ToInt32(Request.QueryString["RR"]);
                }

                int ViewReportReview = 0;
                if (Request.Url.AbsolutePath.Contains("ViewReportReview"))
                {
                    ViewReportReview = 1;
                }


                ObjModel.OrderId = OrderId;



                #region User FullName

                string Username = string.Empty;

                if (Session["UserFullName"] != null)
                {
                    Username = Convert.ToString(Session["UserFullName"]);
                }
                else
                {
                    ObjBALCompanyUsers = new BALCompanyUsers();
                    MembershipUser Member = Membership.GetUser();//User.Identity.Name
                    if (Member != null)
                    {
                        Guid UserId = new Guid(Member.ProviderUserKey.ToString());
                        List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjData = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(UserId);
                        Session["CurrentUserCompanyName"] = ObjData.First().CompanyName;
                        Session["UserFullName"] = ObjData.First().FirstName + " " + ObjData.First().LastName;
                    }
                }

                #endregion

                ObjModel.IsAdmin = false;

            #endregion

                #region Request/Response
                ViewBag.CountyExist = "3";
                UserView_GetViewReport(ObjModel, OrderId, ReviewQueryString, ViewReportReview, Username);
                if (System.Web.HttpContext.Current.Session["CountyExist"] != null)
                {
                    ViewBag.CountyExist = System.Web.HttpContext.Current.Session["CountyExist"];
                    System.Web.HttpContext.Current.Session["CountyExist"] = null;
                }

                #endregion

                if (TempData["messages"] != null)
                {
                    ViewBag.Message = TempData["messages"];
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("was deadlocked on lock resources with another process") || ex.Message.Contains("Value cannot be null") || ex.Message.Contains("Timeout expired"))
                {
                    return RedirectToAction("CustomDeadLockMsg", "Corporate");
                }
            }
            finally
            {

            }
            return View(ObjModel);
        }



        public ActionResult AddUpdateReferenceCode(string SelectedValue, string fkCompanyId, string NewRefenceCode, string RefenceNote, string fkOrderId, string SelectedRefCode, int PermissibleId)
        {
            int result = 0;
            if (SelectedValue == "-3")// Add New Reference
            {


                var ReferenceCode = new tblReferenceCode
                {
                    pkReferenceCodeId = Convert.ToInt16(SelectedValue),
                    fkCompanyId = Convert.ToInt32(fkCompanyId),
                    ReferenceCode = NewRefenceCode,
                    ReferenceNote = RefenceNote
                };

                BALReferenceCode ObjBALReferenceCode = new BALReferenceCode();
                int success = 0;
                success = ObjBALReferenceCode.AddReference(ReferenceCode, Convert.ToInt32(fkCompanyId), true);
                if (success > 0)
                {
                    string pkReferenceCodeId = success.ToString();
                    ObjBALReferenceCode.UpdateRefenceByOrderId(Convert.ToInt32(fkOrderId), NewRefenceCode, pkReferenceCodeId, RefenceNote, PermissibleId);
                }
                result = 1;
            }
            else// Update Current Reference
            {
                var ReferenceCode = new tblReferenceCode
                {
                    pkReferenceCodeId = Convert.ToInt16(SelectedValue),
                    fkCompanyId = Convert.ToInt32(fkCompanyId),
                    ReferenceCode = NewRefenceCode,
                    ReferenceNote = RefenceNote
                };
                BALReferenceCode ObjBALReferenceCode = new BALReferenceCode();
                ObjBALReferenceCode.UpdateRefenceByOrderId(Convert.ToInt32(fkOrderId), SelectedRefCode, SelectedValue, RefenceNote, PermissibleId);
                result = 2;

            }
            return Json(new { result = result });
        }
        public ActionResult GetRefnceNoteByRefenceId(string fkCompanyId, string RefenceCode)
        {
            string RefenceNote = string.Empty;
            BALCompany objBALCompany = new BALCompany();
            RefenceNote = objBALCompany.GetReferenceNote(Convert.ToInt16(RefenceCode));


            return Json(new { RefenceNote = RefenceNote });
        }

        /// <summary>
        /// INT-232 View Report as user, while logged in as Admin
        /// </summary>
        public void UserView_GetViewReport(ViewReportModel ObjModel, int OrderId, int ReviewQueryString, int ViewReportReview, string Username)
        {
            BALOrders ObjOrder = new BALOrders();

            var ObjRequestAndResponseResult = ObjOrder.GetRequestAndResponseByOrderId(OrderId);
            string userIds = string.Empty;
            string userEmailIds = string.Empty;
            using (EmergeDALDataContext dalContext = new Data.EmergeDALDataContext())
            {
                var userDetails = (from odr in (dalContext.tblOrders.Where(o => o.pkOrderId == OrderId))
                                   join cusr in dalContext.tblCompanyUsers
                                   on odr.fkCompanyUserId equals cusr.pkCompanyUserId
                                   join usr in dalContext.aspnet_Users
                                   on cusr.fkUserId equals usr.UserId
                                   select new { UserId = usr.UserId, UserEmail = usr.UserName }).FirstOrDefault();
                userIds = Convert.ToString(userDetails.UserId);
                userEmailIds = userDetails.UserEmail;
            }
            if (ObjRequestAndResponseResult.Count > 0)
            {


                #region Check 16 Month Old

                ObjModel.hdnIs6MonthOld = false;
                BALGeneral ObjBALGeneral = new BALGeneral();
                ObjModel.hdnIs6MonthOld = ObjBALGeneral.Is6MonthOld(OrderId);
                ObjModel.IsShowRawData = false;

                #endregion

                #region Applicant Info

                ObjModel.ApplicantDob = ObjRequestAndResponseResult.ElementAt(0).SearchedDob;
                ObjModel.ApplicantState = ObjRequestAndResponseResult.ElementAt(0).SearchedStateCode;
                ObjModel.OrderNo = ObjRequestAndResponseResult.ElementAt(0).OrderNo;
                if (ObjRequestAndResponseResult.ElementAt(0).CompletionDate != null)
                {
                    ObjModel.CompletionDate = Convert.ToDateTime(ObjRequestAndResponseResult.ElementAt(0).CompletionDate).ToString("MM/dd/yyyy HH:mm:ss");
                }
                ObjModel.OrderDt = ObjRequestAndResponseResult.ElementAt(0).OrderDt.ToString("MM/dd/yyyy HH:mm:ss");
                string SearchedName = GenerateApplicantName(ObjRequestAndResponseResult[0].SearchedLastName, ObjRequestAndResponseResult[0].SearchedSuffix, ObjRequestAndResponseResult[0].SearchedMiddleInitial, ObjRequestAndResponseResult[0].SearchedFirstName);
                string FullSearchedName = SearchedName.Replace("'", "");
                ObjModel.ApplicantName = FullSearchedName;
                ViewBag.UserFullName1 = FullSearchedName;
                ObjModel.ApplicantNameForEmail = ObjRequestAndResponseResult[0].SearchedLastName + "_" + ObjRequestAndResponseResult[0].SearchedFirstName;

                #region Mailing Address
                string mailingAddress = "";

                if (ObjRequestAndResponseResult[0].SearchedStreetAddress != null || ObjRequestAndResponseResult[0].SearchedStreetName != null)
                {
                    mailingAddress = ObjRequestAndResponseResult[0].SearchedStreetAddress != string.Empty ? ObjRequestAndResponseResult[0].SearchedStreetAddress + "\n" : string.Empty;
                    mailingAddress = ObjRequestAndResponseResult[0].SearchedStreetName != string.Empty ? mailingAddress + ObjRequestAndResponseResult[0].SearchedStreetName + "\n" : mailingAddress;
                }
                mailingAddress = ObjRequestAndResponseResult[0].SearchedStreetType != string.Empty ? mailingAddress + ObjRequestAndResponseResult[0].SearchedStreetType + ", " : mailingAddress;
                mailingAddress = ObjRequestAndResponseResult[0].SearchedApt != string.Empty ? mailingAddress + ObjRequestAndResponseResult[0].SearchedApt + ", " : mailingAddress;
                mailingAddress = ObjRequestAndResponseResult[0].SearchedCity != string.Empty ? mailingAddress + ObjRequestAndResponseResult[0].SearchedCity : mailingAddress;

                if (mailingAddress != string.Empty)
                {
                    mailingAddress = ObjRequestAndResponseResult[0].SearchedStateCode != string.Empty ? mailingAddress + "\n" + ObjRequestAndResponseResult[0].SearchedStateCode : mailingAddress;
                    mailingAddress = ObjRequestAndResponseResult[0].SearchedZipCode != string.Empty ? mailingAddress + " " + ObjRequestAndResponseResult[0].SearchedZipCode : mailingAddress;
                }
                ObjModel.ApplicantAddress = mailingAddress;
                ObjModel.IsAddressUpdated = ObjRequestAndResponseResult[0].IsAddressUpdated;
                #endregion
                #endregion

                #region Company Info
                ObjModel.PendingOrderNote = string.Empty;
                if (ObjRequestAndResponseResult.ElementAt(0).OrderStatus == 1)           // When Order Status is pending then show Update Note
                {
                    if (ObjRequestAndResponseResult.ElementAt(0).PendingOrderUpdateNote != string.Empty)
                    {
                        ObjModel.PendingOrderNote = DisplayPendingOrderNote(ObjRequestAndResponseResult[0].PendingOrderUpdateNote);
                    }
                }

                MembershipUser Member = Membership.GetUser();//User.Identity.Name
                if (Member != null)
                {
                    Member.Email = userEmailIds;
                    string[] Role = Roles.GetRolesForUser(Member.Email);

                    BALCompany ObjBALCompany = new BALCompany();

                    List<bool> lst = ObjBALCompany.GetReportStatusData(OrderId);
                    if (lst.Count > 0)
                    {
                        ObjModel.IsPassReport = lst[0];
                        ObjModel.IsAppliedStatus = lst[1];
                    }
                    else
                    {
                        ObjModel.IsPassReport = false;
                        ObjModel.IsAppliedStatus = false;
                    }
                    List<Proc_Get_CompanyDetailByMemberShipIdResult> ObjCompanyDetails = ObjBALCompany.GetCompanyDetailByMemberShipIdResult(ObjRequestAndResponseResult.ElementAt(0).fkUserId);
                    if (ObjCompanyDetails.Count > 0)
                    {
                        if (Role.Contains("SystemAdmin") || Role.Contains("SupportTechnician") || Role.Contains("SupportManager"))
                        {
                            //Do nothing.
                        }
                        else
                        {
                            ObjModel.IsShowRawData = false;
                            ObjModel.CompanyUser = ObjCompanyDetails.First().lastname + ", " + ObjCompanyDetails.First().firstname;
                            ObjModel.CompanyLocation = ObjCompanyDetails.First().city + ", " + ObjCompanyDetails.First().statecode;
                            ObjModel.CompanyName = ObjCompanyDetails.First().CompanyName;
                            ObjModel.CompanyNameAdverse = ObjCompanyDetails.First().CompanyName;
                            ObjModel.IsReportStatus = ObjCompanyDetails.First().IsReportStatus;
                        }
                    }


                    int LoginCompanyId = 0;
                    int OrderCompanyId = 0;
                    Guid UserId = new Guid(userIds);
                    List<Proc_Get_CompanyDetailByMemberShipIdResult> ObjRootCompanyDetails = ObjBALCompany.GetCompanyDetailByMemberShipIdResult(UserId);
                    {
                        //Guid RootCompanyId = new Guid("7DC87D77-6BB5-4BB3-9837-FA861F36F281");
                        //int RootCompanyId = 9514;
                        int RootCompanyId = 9831;
                        #region Emerge review Pop details

                        //email id for emerge review message
                        ObjModel.CurrentUserEmail = Member.Email;
                        ObjModel.lblToRR = ConfigurationManager.AppSettings["ReportReviewMail"].ToString();

                        //email id for asking admin to activate report
                        ObjModel.lblMailto = BALGeneral.GetGeneralSettings().SupportEmailId;
                        ObjModel.lblMailFrom = Member.Email;
                        ObjModel.txtMessage = Username + " is requesting to activate the Emerge Review feature ##report## ";



                        #endregion

                        if (ObjRootCompanyDetails.First().PkCompanyId != 0)
                        {
                            LoginCompanyId = ObjRootCompanyDetails.First().PkCompanyId;
                            OrderCompanyId = (int)ObjRequestAndResponseResult[0].fkCompanyId;

                            if (RootCompanyId == ObjRootCompanyDetails.First().PkCompanyId)
                            {
                                ObjModel.CompanyPhone = ObjCompanyDetails.First().CompanyPhone.ToString();
                                ObjModel.LocationPhone = ObjCompanyDetails.First().LocationPhone.ToString();
                                ObjModel.UserPhone = ObjCompanyDetails.First().UserPhone.ToString();
                                ObjModel.EmailAddress = ObjCompanyDetails.First().EmailAddress.ToString();
                                ObjModel.IsRootCompanyId = true;
                                ObjModel.IsReportStatus = ObjCompanyDetails.First().IsReportStatus;
                            }
                            else
                            {
                                ObjModel.IsRootCompanyId = false;
                            }
                        }
                    }

                #endregion

                    #region Product Code

                    int nameDiff = 0;
                    int IsAutoReviewPopup = 0;
                    int IsAutoReviewSent = 0;
                    string BottomScript = string.Empty;
                    string EmergeAssistScript = string.Empty;
                    string DisclaimerScript = string.Empty;
                    int MVR_Counter = 0; int CountReviewReports = 0;
                    string productcodeforReview = string.Empty;
                    // BALEmergeReview ObjBALEmergeReview = new BALEmergeReview();


                    var ObjProc_Get_RequestAndResponseResultList = ObjRequestAndResponseResult.OrderByDescending(d => d.ProductCode);
                    List<ClsViewReport> ObjViewReport = new List<ClsViewReport>();
                    foreach (Proc_Get_RequestAndResponseResult ObjRequestAndResponseResults in ObjProc_Get_RequestAndResponseResultList)
                    {

                        StringBuilder TopScript = new StringBuilder();
                        ClsViewReport ObjClsViewReport = new ClsViewReport();

                        /* Send an Email For Pending Reports */
                        productcodeforReview = ObjRequestAndResponseResults.ProductCode;
                        try
                        {

                            if (ObjRequestAndResponseResults.ReportStatus == 1 && ObjRequestAndResponseResults.IsSentPendingEmail == false)
                            {
                                SendPendingMessage(ObjRequestAndResponseResults);
                                BALGeneral.UpdateEmailSentStatus(ObjRequestAndResponseResults.pkOrderDetailId);
                            }

                            /* Send an Email For Pending Reports */

                            /* Send an Email For In-Complete Reports */
                            //  if (ObjRequestAndResponseResults.IsSentInCompleteEmail == null) { ObjRequestAndResponseResults.IsSentInCompleteEmail = false; } //for #387  ticket.

                            if (ObjRequestAndResponseResults.ReportStatus == 3 && ObjRequestAndResponseResults.IsSentInCompleteEmail == false)
                            {
                                SendInCompleteMessage(ObjRequestAndResponseResults);
                                BALGeneral.UpdateInCompleteEmailSentStatus(ObjRequestAndResponseResults.pkOrderDetailId);
                            }
                        }
                        catch (Exception)
                        {

                        }
                        /* Send an Email For In-Complete Reports */


                        #region Report Type

                        string DisplyProductCode = "";
                        if (ObjRequestAndResponseResults.IsLiveRunner == true)
                        {
                            DisplyProductCode = ObjRequestAndResponseResults.ProductCode + "*";
                        }
                        else
                        {
                            DisplyProductCode = ObjRequestAndResponseResults.ProductCode;
                        }
                        ObjModel.ReportType += DisplyProductCode + ", ";

                        if ((LoginCompanyId == OrderCompanyId) && (ObjRequestAndResponseResults.IsViewed == false) && (ObjRequestAndResponseResults.ReportStatus == 2))
                        {
                            IsAutoReviewPopup = 1;
                            BALGeneral objGeneral = new BALGeneral();
                            objGeneral.UpdateOrderDetail_IsViewed(ObjRequestAndResponseResults.pkOrderDetailId);     // Update Isviewed for order report
                        }

                        UpdateOrderIsViewed(LoginCompanyId, OrderCompanyId, OrderId); // Update Read Unread

                        #endregion

                        #region Live Runner

                        string LiveRunnerImagePath = "../Content/themes/base/images/LiveRunnerSmall.png";

                        string IsLiveRunnerProvided = "";
                        if (ObjRequestAndResponseResults.IsLiveRunner == true)
                        {
                            IsLiveRunnerProvided = "<table><tr><td style='padding-right:15px;line-height: 44px;' >Provided By</td><td><img src='" + LiveRunnerImagePath + "' alt='Live Runner' height='13' style='padding-right: 30px;border:0px'/></td></tr></table>";
                        }
                        else
                        {
                            IsLiveRunnerProvided = "";
                        }

                        #endregion

                        #region SCR STATE COUNTY

                        string SCRState = "";
                        if (ObjRequestAndResponseResults.ProductCode.ToLower() == "scr")
                        {
                            SCRState = "[" + ObjRequestAndResponseResults.StateName + "]";
                        }
                        //Used for MVR Report INT-83 (SMALL CHANGE: We would like "MVR" reports to show the state that is selected.)
                        if (ObjRequestAndResponseResults.ProductCode.ToLower() == "mvr")
                        {
                            SCRState = "[" + ObjRequestAndResponseResults.StateName + "]";
                        }


                        string StateCounty = "";
                        string StateCode = "";
                        string CountyName = ObjRequestAndResponseResults.CountyCode;
                        if (CountyName != "")
                        {
                            StateCode = ObjRequestAndResponseResults.StateCodeReport;
                            if (StateCode != "")
                            {
                                StateCounty = "[" + CountyName + ", " + StateCode + "]";
                            }
                        }

                        #endregion

                        #region Top Report Header
                        if (ObjRequestAndResponseResults.IsLiveRunner == true)
                        {
                            if (ObjRequestAndResponseResults.ProductCode == "SCR" || ObjRequestAndResponseResults.ProductCode == "CCR1")
                            {
                                if (ObjRequestAndResponseResults.ProductDisplayName == "Manual State Criminal")
                                {
                                    ObjRequestAndResponseResults.ProductDisplayName = "LiveRunner State Criminal Report";

                                }

                                if (ObjRequestAndResponseResults.ProductDisplayName == "Manual County Search")
                                {
                                    ObjRequestAndResponseResults.ProductDisplayName = "LiveRunner County Search";

                                }
                            }
                        }

                        #region check dobvariation count for ticket #83

                        // disable auto emerge review in ncr reports if find dob variation.
                        int RemainingNUllDOB = 0;
                        if (ObjRequestAndResponseResults.IsViewed == false && ObjRequestAndResponseResults.ReviewStatus != 1)
                        {

                            if (ObjRequestAndResponseResults.ProductCode == "NCR1" || ObjRequestAndResponseResults.ProductCode == "NCR+" || ObjRequestAndResponseResults.ProductCode == "NCR4")
                            {

                                if (!string.IsNullOrEmpty(ObjRequestAndResponseResults.ResponseData))
                                {
                                    string XML_ValueCharges = GetRequiredXml_ForRapidCourt(ObjRequestAndResponseResults.ResponseData);
                                    XDocument ObjXDocument = XDocument.Parse(XML_ValueCharges);
                                    var List_Charges = ObjXDocument.Descendants("CriminalCase").ToList();
                                    if (List_Charges.Count > 0)
                                    {
                                        foreach (XElement user in ObjXDocument.Descendants("CriminalCase"))
                                        {
                                            foreach (XElement contact in user.Descendants("DemographicDetail"))
                                            {
                                                var dob = user.Descendants("DateOfBirth");
                                                if (dob.Count() == 0)
                                                { RemainingNUllDOB++; }
                                                else { }
                                            }
                                        }
                                    }
                                }
                            }

                            if (RemainingNUllDOB > 0) //for ticket 83
                            {
                                ObjRequestAndResponseResults.ReviewStatus = 0;
                                ObjRequestAndResponseResults.fkReportCategoryId = 1;
                                BALGeneral objGeneral = new BALGeneral();
                                objGeneral.UpdateOrderDetail_ReviewStatus(ObjRequestAndResponseResults.pkOrderDetailId);
                                ObjModel.CheckNcrReports = true;
                                //ObjRequestAndResponseResults.ReviewStatus =objGeneral.GetOrderDetail_ReviewStatus(ObjRequestAndResponseResults.pkOrderDetailId);//INT-243
                            }
                        }

                        #endregion

                        TopScript.Append("<div class=\"main-box\"><div onclick=\"collapse('" + ObjRequestAndResponseResults.ProductCode + "_" + nameDiff.ToString() + "');\">"
                            + "<table class=\"rv-header\"  width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse\">"
                            + "<tr><td style=\"background-repeat: no-repeat; background-image: url('../content/themes/base/images/" + ObjRequestAndResponseResults.ProductDisplayName + ".png');  background-position: 30px;cursor: pointer;width: 8% !important;color: transparent;  \"  width=\"39px\" height=\"34px\"></td><td class=\"rv-header\" width=\"95%\">"
                            + "<table style='font-weight:bold; width:100%'><tbody><tr><td style=\"vertical-align: middle; width:50%\" align='left'>" + ObjRequestAndResponseResults.ProductDisplayName + " " + SCRState + StateCounty + "</td>"
                            + "<td style=\"vertical-align: middle; width:50%\" align='right'><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>" + IsLiveRunnerProvided + "</td><td></td></tr> </table> </td></tr></tbody></table>"
                            + "</td><td class=\"rv-header\" width=\"9px\"></td></tr></table></div><div id=\"" + ObjRequestAndResponseResults.ProductCode + "_" + nameDiff.ToString() + "_b\" style=\"display: block; margin-left: 12px;\" class=\"pad-bot10\">"
                            + "<table  width=\"98%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td class=\"middle_left\" width=\"14px\"></td><td  width=\"100%\">");

                        if (ObjRequestAndResponseResults.ProductCode.ToLower() == "scr")
                        {
                            TopScript.Append("<div class='people-search-wrap'><div style='font-size:14px;padding-left: 5px; color:#666; font-family:arial'><b>State Searched :</b> " + ObjRequestAndResponseResults.StateName + "</div>");
                            TopScript.Append("<div style='font-size:14px;padding-left: 5px; color:#666; font-family:arial'><b>Name :</b> " + FullSearchedName + "</div></div>");
                        }

                        if (ObjRequestAndResponseResults.ProductCode.ToLower() == "fcr" && (ObjRequestAndResponseResults.IsTieredReport == true || ObjRequestAndResponseResults.IsOptionalReport == true))
                        {
                            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                            {

                                TopScript.Append("<div class='people-search-wrap'><div style='font-size:14px;padding-left: 5px; color:#666; font-family:arial'><b>Name :</b> " + FullSearchedName + " </br> <b>Jurisdiction :</b> " + ObjRequestAndResponseResults.LiveRunnerCounty + "</div></div>");

                                //TopScript.Append("<div style='font-size:14px; color:#666; font-family:arial'><b>Name :</b> " + FullSearchedName + " | <b>Jurisdiction :</b> " + Dx.tblJurisdictions.FirstOrDefault(n => n.pkJurisdictionId == Convert.ToInt32(ObjRequestAndResponseResults.SearchedCountyId)).JurisdictionName + "</div>");
                            }
                        }
                        else if (ObjRequestAndResponseResults.ProductCode.ToLower() == "fcr" && ObjRequestAndResponseResults.IsTieredReport == false && ObjRequestAndResponseResults.IsOptionalReport == false)
                        {

                            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                            {
                                //commented by chetu for ticket #19
                                //TopScript.Append("<div style='font-size:14px; color:#666; font-family:arial'><b>Name :</b> " + FullSearchedName + " | <b>Jurisdiction :</b> " + Dx.tblJurisdictions.FirstOrDefault(n => n.pkJurisdictionId == Convert.ToInt32(ObjRequestAndResponseResults.SearchedCountyId)).JurisdictionName + "</div>");
                                if (ObjRequestAndResponseResults.CountyCode != null)
                                {
                                    TopScript.Append("<div class='people-search-wrap'><div style='font-size:14px;padding-left: 5px; color:#666; font-family:arial'><b>Name :</b> " + FullSearchedName + " </br> <b>Jurisdiction :</b> " + ObjRequestAndResponseResults.CountyCode + "</div></div>");
                                }
                                else
                                {
                                    TopScript.Append("<div class='people-search-wrap'><div style='font-size:14px;padding-left: 5px; color:#666; font-family:arial'><b>Name :</b> " + FullSearchedName + " </br> <b>Jurisdiction :</b> " + ObjRequestAndResponseResults.LiveRunnerCounty + "</div></div>");
                                }
                            }

                        }



                        #endregion

                        #region Fetch Report


                        if (ObjRequestAndResponseResults.ReportStatus.ToString() == "3")
                        {
                            //ObjRequestAndResponseResults
                            if (ObjRequestAndResponseResults.ProductCode == "PSP")
                            {
                                if (ObjRequestAndResponseResults.ResponseData != "")
                                {
                                    if (ObjRequestAndResponseResults.ResponseData != "")
                                    {
                                        XDocument erroXML = XDocument.Parse(ObjRequestAndResponseResults.ErrorLog);
                                        var errorMessage = erroXML.Descendants("StatusDescription").FirstOrDefault().Value.ToString();
                                        TopScript.Append(DisplayInCompleteMessageForPSP(errorMessage));
                                    }
                                    else
                                    {
                                        TopScript.Append(DisplayInCompleteMessage());
                                    }
                                }
                                else
                                {
                                    TopScript.Append(DisplayInCompleteMessage());
                                }
                            }
                            else
                            {
                                TopScript.Append(DisplayInCompleteMessage());
                            }

                            BALOrders ObjBALOrders = new BALOrders();
                            ObjBALOrders.UpdateReportStatus_Ret(ObjRequestAndResponseResults.fkOrderId, ObjRequestAndResponseResults.pkOrderDetailId, 3);
                        }
                        else if (ObjRequestAndResponseResults.fkReportCategoryId == 2 && ObjRequestAndResponseResults.OrderStatus == 4 && ObjModel.IsShowRawData == false && ObjRequestAndResponseResults.ReviewStatus != 2)
                        {//For 282 Ticket
                            TopScript.Append(DisplayNoResponseMessage());

                        }
                        else if (ObjRequestAndResponseResults.ReportStatus.ToString() == "2" && (ObjRequestAndResponseResults.ManualResponse != string.Empty || ObjRequestAndResponseResults.ResponseFileName != string.Empty))
                        {
                            TopScript.Append(DisplayManualResponse(ObjRequestAndResponseResults));
                        }
                        //else if (ObjRequestAndResponseResults.ReportStatus.ToString() == "2" && ObjRequestAndResponseResults.ReviewStatus.ToString() == "1" && ObjModel.IsShowRawData == false && ObjRequestAndResponseResults.ProductCode != "PS")//INT-456 Auto review 
                        else if (ObjRequestAndResponseResults.ReportStatus.ToString() == "2" && ObjRequestAndResponseResults.ReviewStatus.ToString() == "1" && ObjModel.IsShowRawData == false && (Convert.ToInt32(Session["CriminalCaseCountResponse"]) > 0 && ObjRequestAndResponseResults.ProductCode != "PS"))//INT-456 Auto review 
                        {
                            TopScript.Append(DisplayInReviewMessage());
                        }

                        else
                        {
                            #region Check For MVR

                            if (ObjRequestAndResponseResults.ProductCode == "MVR")
                            {
                                int MVR_ReportStatus = ObjRequestAndResponseResults.ReportStatus;
                                if (MVR_ReportStatus != 2)
                                {
                                    List<Proc_Get_RequestAndResponseResult> ObjProc_Get_RequestAndResponseResult_MVR = null;
                                    while (MVR_ReportStatus != 2 && MVR_Counter < 4)
                                    {
                                        MVR_Counter++;
                                        System.Threading.Thread.Sleep(4000);
                                        ObjProc_Get_RequestAndResponseResult_MVR = ObjOrder.GetRequestAndResponseByOrderId(OrderId).Where(data => data.ProductCode.ToLower() == "mvr").ToList<Proc_Get_RequestAndResponseResult>();
                                        MVR_ReportStatus = ObjProc_Get_RequestAndResponseResult_MVR.Where(data => data.ProductCode.ToLower() == "mvr").FirstOrDefault().ReportStatus;
                                    }

                                    GetMvrResponse(ObjRequestAndResponseResults, TopScript);
                                }
                                else
                                {

                                    if (ObjRequestAndResponseResults.ResponseData.Contains("INVALID ACCOUNT NAME AND PASSWORD") || ObjRequestAndResponseResults.ResponseData.Contains("<request_status>1</request_status>") || ObjRequestAndResponseResults.ResponseData == "")
                                    {
                                        TopScript.Append("Your order was completed manually through Intelifi support department. Please contact support for any further assistance.");
                                    }
                                    else
                                    {
                                        TopScript.Append(ObjRequestAndResponseResults.ResponseData);
                                    }


                                }
                                ObjClsViewReport.XDocument = new XmlDocument();
                            }

                            #endregion

                            #region Fetch Response

                            else
                            {
                                string hdnReviewDetail = FetchResponse(ObjRequestAndResponseResults, ObjClsViewReport, TopScript, false, false, Role, ObjModel);
                                ObjModel.hdnReviewDetail += hdnReviewDetail;
                            }

                            #endregion

                        }

                        #endregion

                        ObjClsViewReport.Starting = TopScript.ToString();

                        #region Review Status


                        string ApplicantFirstName = ObjRequestAndResponseResults.SearchedFirstName, ApplicantLastName = ObjRequestAndResponseResults.SearchedLastName;


                        string DisclaimerInfo = string.Empty;
                        //if (ObjRequestAndResponseResults.ReportStatus != 1)
                        //{
                        if (ObjRequestAndResponseResults.ProductCode.ToLower() == "scr" || ObjRequestAndResponseResults.ProductCode.ToLower() == "mvr" || ObjRequestAndResponseResults.ProductCode.ToLower() == "mvr")
                        {
                            int StateId = Convert.ToInt32(ObjRequestAndResponseResults.SearchedStateId);
                            int? fkPerApplicationId = Convert.ToInt32(ObjRequestAndResponseResults.fkProductApplicationId.ToString());
                            BALGeneral objBalGeneral = new BALGeneral();
                            var list = objBalGeneral.GetDisclamierInfoForScrMvrWcb(fkPerApplicationId, StateId);
                            if (list.Count > 0)
                            {
                                DisclaimerInfo = list.ElementAt(0).DisclaimerData;
                            }

                        }


                        if (ObjRequestAndResponseResults.ProductCode.ToLower() == "ccr1" || ObjRequestAndResponseResults.ProductCode.ToLower() == "ccr1" || ObjRequestAndResponseResults.ProductCode.ToLower() == "ccr2")
                        {
                            string CountyCode = ObjRequestAndResponseResults.CountyCode;
                            int pkProductId = Convert.ToInt32(ObjRequestAndResponseResults.pkProductId.ToString());
                            int fkCountyId = Convert.ToInt32(ObjRequestAndResponseResults.fkCountyId);
                            BALGeneral objBalGeneral = new BALGeneral();

                            var list = objBalGeneral.GetDisclamierInfoForCcr1Ccr2Rcx(fkCountyId, pkProductId);
                            if (list.Count > 0)
                            {
                                DisclaimerInfo = list.ElementAt(0).DisclaimerData;
                            }
                        }

                        if (ObjRequestAndResponseResults.ProductCode.ToLower() == "fcr")
                        {
                            string CountyCode = ObjRequestAndResponseResults.CountyCode;
                            BALGeneral objBalGeneral = new BALGeneral();
                            var list = objBalGeneral.GetDisclamierInfoForFCR(CountyCode);
                            if (list.Count > 0)
                            {
                                DisclaimerInfo = list.ElementAt(0).DisclaimerData;
                            }
                        }

                        //}




                        DisclaimerScript = "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" ><tr><td>";

                        //if (ObjRequestAndResponseResults.ProductCode.ToLower() == "scr" && ObjRequestAndResponseResults.StateName.ToLower() == "georgia")
                        //{
                        DisclaimerScript = DisclaimerScript + "<div class=\"sectionsummary\"><h3 style=\"line-height: 1em;\">" + DisclaimerInfo + "</h3></div>";
                        //}

                        DisclaimerScript = DisclaimerScript + "</td></tr></table>";

                        BottomScript = "</td><td class=\"middle_right\" width=\"9px\"></td></tr></table></div><div id=\"" + ObjRequestAndResponseResults.ProductCode + "_f\" style=\"display: block;margin-left:12px;\"></div></div><br/>";

                        string strPS = ObjRequestAndResponseResults.ProductDisplayName.Replace("'", "\\'");//INT-172 As per Greg comments Help Button will not be displayed on PS section. 


                        if (ObjRequestAndResponseResults.fkReportCategoryId != 2 && !strPS.Contains("People Search"))
                        {
                            EmergeAssistScript = "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" style=\"margin-top: 10px;\"><tr><td style=\"padding-left: 0%;\"> <a href=\"javascript:ReportReview('" + ObjRequestAndResponseResults.ProductDisplayName.Replace("'", "\\'") + "','" + ApplicantLastName.Replace("'", "\\'") + "','" + ApplicantFirstName.Replace("'", "\\'") + "' ,0,'" + ObjRequestAndResponseResults.pkOrderDetailId + "');\"><img style=\"height:28px;\" src='" + ApplicationPath.GetApplicationPath() + "content/themes/base/images/help-icon4.png' alt='help'/></a></td><td ><a style=\"font-family: Arial;display:block;font-size:12px;padding-left:10px;text-decoration:none;font-weight:bold;color:#000;\" href=\"javascript:ReportReview('" + ObjRequestAndResponseResults.ProductDisplayName.Replace("'", "\\'") + "','" + ApplicantLastName.Replace("'", "\\'") + "','" + ApplicantFirstName.Replace("'", "\\'") + "' ,0,'" + ObjRequestAndResponseResults.pkOrderDetailId + "');\"  > Help</a></td>";
                        }
                        else
                        {
                            EmergeAssistScript = "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" style=\"margin-top: 10px;\"><tr><td style=\"padding-left: 0%;\"></td>";
                        }


                        #region Check Product review status for location

                        byte ProductReviewStatus = 0;
                        try
                        {
                            BALEmergeReview ObjBALEmergeReview = new BALEmergeReview();
                            ProductReviewStatus = ObjBALEmergeReview.GetProductReviewStatusPerLocation(Convert.ToInt32(ObjRequestAndResponseResults.fkLocationId), Convert.ToInt32(ObjRequestAndResponseResults.fkProductApplicationId));
                            if (ProductReviewStatus == 0 && ObjRequestAndResponseResults.fkReportCategoryId == 2 && (ObjRequestAndResponseResults.ProductCode.ToLower() != "ncr3" || ObjRequestAndResponseResults.ProductCode.ToLower() != "rcx" || ObjRequestAndResponseResults.ProductCode.ToLower() != "sor"))
                            {
                                ProductReviewStatus = 1;
                            }
                        }
                        catch
                        {

                        }
                        //if (ObjRequestAndResponseResults.IsEnableReview && ObjRequestAndResponseResults.IsAutoReview && ProductReviewStatus == 2 && ObjRequestAndResponseResults.ReportStatus == 2 && ObjRequestAndResponseResults.ReviewStatus != 0)
                        if (ObjRequestAndResponseResults.ReportStatus == 2 && ObjRequestAndResponseResults.ReviewStatus != 0)
                        {
                            IsAutoReviewSent = 1;
                        }

                        #endregion
                        if (ObjRequestAndResponseResults.IsEnableReview != null)
                        {
                            //INT-116 for disable emerge review for psp
                            if (ObjRequestAndResponseResults.fkReportCategoryId != 1 && ObjRequestAndResponseResults.ReviewStatus.ToString() != "1" && ObjRequestAndResponseResults.ProductCode != "MVR" && ObjRequestAndResponseResults.ProductCode != "PSP")
                            {
                                if (ObjRequestAndResponseResults.IsEnableReview == true && ProductReviewStatus > 0)
                                {
                                    EmergeAssistScript = EmergeAssistScript + " <td style=\"padding-left: 2%;\"><a href=\"javascript:ReportReview('" + ObjRequestAndResponseResults.ProductDisplayName.Replace("'", "\\'") + "','" + ApplicantLastName.Replace("'", "\\'") + "','" + ApplicantFirstName.Replace("'", "\\'") + "',1,'" + ObjRequestAndResponseResults.pkOrderDetailId + "');\"><img style=\"height:28px;\" src='" + ApplicationPath.GetApplicationPath() + "content/themes/base/images/emerge-review-button.png' title='Emerge Review'/></a></td><td><a style=\"font-family: Arial;display:block;font-size:12px;padding-left:10px;text-decoration:none;color:#000;\" href=\"javascript:ReportReview('" + ObjRequestAndResponseResults.ProductDisplayName.Replace("'", "\\'") + "','" + ApplicantLastName.Replace("'", "\\'") + "','" + ApplicantFirstName.Replace("'", "\\'") + "',1,'" + ObjRequestAndResponseResults.pkOrderDetailId + "');\"  > &nbsp;<b>Emerge Review /</b> Connect with an agent  </a></td><td style='display:none'><textarea id=" + ObjRequestAndResponseResults.pkOrderDetailId + ">" + ObjRequestAndResponseResults.ReviewComments + " </textarea></td>";
                                }
                                else
                                {
                                    EmergeAssistScript = EmergeAssistScript + " <td style=\"padding-left: 2%;\"><a id='" + ObjRequestAndResponseResults.ProductDisplayName.Replace("'", "\\'") + "' href=\"javascript:ActivateReportsVR('" + ObjRequestAndResponseResults.ProductDisplayName.Replace("'", "\\'") + "');\"><img style=\"height:28px;\" src='" + ApplicationPath.GetApplicationPath() + "content/themes/base/images/emerge-review-button.png' alt='Enable Review' title='Send Request for Emerge Review'/></a></td><td><a style=\"font-family: Arial;display:block;font-size:12px;padding-left:10px;text-decoration:none;color:#000;\" href=\"javascript:ActivateReportsVR('" + ObjRequestAndResponseResults.ProductDisplayName.Replace("'", "\\'") + "');\"  > &nbsp;<b>Emerge Review /</b> Connect with an agent</a></td>";
                                }
                            }
                            else
                            {

                                if (ObjModel.CheckNcrReports)
                                {
                                    EmergeAssistScript = EmergeAssistScript + " <td style=\"padding-left: 2%;\"><a href=\"javascript:ReportReview('" + ObjRequestAndResponseResults.ProductDisplayName.Replace("'", "\\'") + "','" + ApplicantLastName.Replace("'", "\\'") + "','" + ApplicantFirstName.Replace("'", "\\'") + "',1,'" + ObjRequestAndResponseResults.pkOrderDetailId + "');\"><img style=\"height:28px;\" src='" + ApplicationPath.GetApplicationPath() + "content/themes/base/images/emerge-review-button.png' title='Emerge Review'/></a></td><td><a style=\"font-family: Arial;display:block;font-size:12px;text-decoration:none;color:#000;\" href=\"javascript:ReportReview('" + ObjRequestAndResponseResults.ProductDisplayName.Replace("'", "\\'") + "','" + ApplicantLastName.Replace("'", "\\'") + "','" + ApplicantFirstName.Replace("'", "\\'") + "',1,'" + ObjRequestAndResponseResults.pkOrderDetailId + "');\"  > &nbsp;<b>Emerge Review /</b> Connect with an agent </a></td><td style='display:none'><textarea id=" + ObjRequestAndResponseResults.pkOrderDetailId + ">" + ObjRequestAndResponseResults.ReviewComments + " </textarea></td>";
                                }
                                else
                                {

                                    EmergeAssistScript = EmergeAssistScript + " <td style=\"padding-left: 2%;\"></td>";
                                }
                            }
                        }
                        EmergeAssistScript = EmergeAssistScript + "</tr></table>";

                        #endregion

                        ObjClsViewReport.Ending = DisclaimerScript + EmergeAssistScript + BottomScript;

                        nameDiff++;

                        ObjViewReport.Add(ObjClsViewReport);


                        if (ObjRequestAndResponseResults.ReviewStatus != 0)
                            CountReviewReports++;
                    }

                    ObjModel.ObjViewReportString = ObjViewReport;

                    if (ObjModel.ReportType.Length > 0)
                    {
                        ObjModel.ReportType = ObjModel.ReportType.Substring(0, ObjModel.ReportType.Length - 2);
                    }
                    #endregion


                    #region Display Alert If Auto Review Is Sent

                    if (ReviewQueryString == 2 && IsAutoReviewSent == 1 && IsAutoReviewPopup == 1)//INT-202
                    {
                        ObjModel.IsAutoReviewSentPopUp = true;                        
                    }


                    if (CountReviewReports > 0)
                        ObjModel.IsReviewedReport = 1;
                    else
                        ObjModel.IsReviewedReport = 0;


                    int IsReportReview = 0;
                    IsReportReview = ReviewQueryString;

                    if (ObjModel.IsReviewedReport != 0 || IsReportReview != 0)
                        ObjModel.AllowReviewReportTab = true;
                    if (IsReportReview == 2 && ObjModel.IsReviewedReport == 0)
                        ObjModel.AllowReviewReportTab = false;
                    if (IsReportReview == 0)
                        ObjModel.AllowReviewReportTab = false;
                    if (ObjModel.IsReviewedReport != 0 || ViewReportReview == 1)// Request.Url.AbsolutePath.Contains("ViewReportReview")
                        ObjModel.AllowReviewReportTab = true;
                    else
                        ObjModel.AllowReviewReportTab = false;

                    #endregion

                    #region Reference Code and Notes


                    List<Proc_GetReferenceCodebyCompanyIdResult> ObjList = new List<Proc_GetReferenceCodebyCompanyIdResult>();
                    ObjList = ObjBALCompany.GetReferenceCodeListbypkCompanyId(Convert.ToInt32(ObjRequestAndResponseResult[0].fkCompanyId.ToString()));

                    ObjList.Insert(0, new Proc_GetReferenceCodebyCompanyIdResult { ReferenceCode = "Select Reference #", pkReferenceCodeId = -1 });
                    ObjList.Add(new Proc_GetReferenceCodebyCompanyIdResult { ReferenceCode = "------------------", pkReferenceCodeId = -2 });
                    ObjList.Add(new Proc_GetReferenceCodebyCompanyIdResult { ReferenceCode = "Add New", pkReferenceCodeId = -3 });
                    ObjModel.ReferenceCodeList = ObjList;
                    ObjModel.TrackingRefCodeValue = "NA";
                    var SearchedTrackingRef = ObjRequestAndResponseResult[0].SearchedTrackingRef == "" ? "NA" : ObjRequestAndResponseResult[0].SearchedTrackingRef;
                    if (SearchedTrackingRef != "NA" && SearchedTrackingRef != null)
                    {
                        var SelectedrefCode = ObjList.Where(rec => rec.ReferenceCode.ToLower().Trim() == SearchedTrackingRef.ToLower().Trim()).FirstOrDefault();
                        if (SelectedrefCode != null)
                        {
                            var TrackingRefCodeValue = SelectedrefCode.pkReferenceCodeId;
                            ObjModel.TrackingRefCodeValue = TrackingRefCodeValue.ToString();
                        }
                    }
                    ObjModel.ReferenceCode = ObjRequestAndResponseResult[0].SearchedTrackingRef == "" ? "NA" : ObjRequestAndResponseResult[0].SearchedTrackingRef;
                    ObjModel.Notes = ObjRequestAndResponseResult[0].SearchedTrackingNotes == "" ? "NA" : ObjRequestAndResponseResult[0].SearchedTrackingNotes;
                    ObjModel.fkCompanyId = ObjRequestAndResponseResult[0].fkCompanyId;
                    #endregion

                    #region Permissible Purpose
                    int? orderpermissibleid = 0;
                    orderpermissibleid = ObjRequestAndResponseResult[0].SearchedPermissibleId == null ? 0 : ObjRequestAndResponseResult[0].SearchedPermissibleId;
                    ObjModel.PermissiblePurposeId = Convert.ToInt32(orderpermissibleid);
                    BALCompanyType serboj = new BALCompanyType();
                    List<tblPermissiblePurpose> PermissiblePurpose = serboj.GetPermissiblePurpose();
                    ObjModel.PermissiblePurposeList = PermissiblePurpose.Select(x => new SelectListItem { Text = x.PermissiblePurpose, Value = x.pkPermissibleId.ToString() }).ToList();
                    ObjModel.PermissiblePurposeList = ObjModel.PermissiblePurposeList.Select(x => new SelectListItem { Text = x.Text, Value = x.Value.ToString(), Selected = (x.Value == ObjModel.PermissiblePurposeId.ToString()) }).ToList();
                    ObjModel.PermissiblePurposeValue = PermissiblePurpose.Where(x => x.pkPermissibleId == (ObjModel.PermissiblePurposeId == 0 ? 1 : ObjModel.PermissiblePurposeId)).Select(x => x.PermissiblePurpose).FirstOrDefault().ToString();
                    #endregion


                    #region SyncPOD

                    ObjModel.IsSyncPOD = ObjRequestAndResponseResult.ElementAt(0).IsSyncpod;
                    if (!string.IsNullOrEmpty(ObjRequestAndResponseResult.ElementAt(0).SyncpodImage))
                    {
                        ObjModel.SyncPODImage = ObjRequestAndResponseResult.ElementAt(0).SyncpodImage.Replace(" ", "");
                    }
                    if (!string.IsNullOrEmpty(ObjRequestAndResponseResult.ElementAt(0).FaceImage))
                    {
                        ObjModel.FaceImage = ObjRequestAndResponseResult.ElementAt(0).FaceImage.Replace(" ", "");
                    }
                    #endregion

                    #region Consent PopUp Code

                    try
                    {
                        List<proc_GetConsentInforByOrderIdResult> lstConsent = ObjOrder.GetConsentPendingFormsByOrderId(OrderId).ToList();
                        ObjModel.CollectionGetConsentInforByOrderId = lstConsent;
                        if (lstConsent.Count > 0)
                        {
                            if (lstConsent.Any(d => d.IsConsentRequired == true))
                            {
                                ObjModel.btnConsentDocsVisible = true;
                            }
                            else
                            {
                                ObjModel.btnConsentDocsVisible = false;
                            }
                        }
                        else
                        {
                            ObjModel.btnConsentDocsVisible = false;
                        }

                    }
                    catch
                    { }

                    #endregion
                }
            }
            else
            {
                List<Proc_GetReferenceCodebyCompanyIdResult> ObjList = new List<Proc_GetReferenceCodebyCompanyIdResult>();
                ObjList.Insert(0, new Proc_GetReferenceCodebyCompanyIdResult { ReferenceCode = "Select Reference #", pkReferenceCodeId = -1 });
                ObjList.Add(new Proc_GetReferenceCodebyCompanyIdResult { ReferenceCode = "------------------", pkReferenceCodeId = -2 });
                ObjList.Add(new Proc_GetReferenceCodebyCompanyIdResult { ReferenceCode = "Add New", pkReferenceCodeId = -3 });
                ObjModel.TrackingRefCodeValue = "NA";
                ObjModel.ReferenceCodeList = ObjList;

                #region Permissible Purpose
                int orderpermissibleid = 0;
                BALCompanyType serboj = new BALCompanyType();
                List<tblPermissiblePurpose> PermissiblePurpose = serboj.GetPermissiblePurpose();
                ObjModel.PermissiblePurposeList = PermissiblePurpose.Select(x => new SelectListItem { Text = x.PermissiblePurpose, Value = x.pkPermissibleId.ToString() }).ToList();
                ObjModel.PermissiblePurposeList = ObjModel.PermissiblePurposeList.Select(x => new SelectListItem { Text = x.Text, Value = x.Value.ToString(), Selected = (x.Value == orderpermissibleid.ToString()) }).ToList();
                ObjModel.PermissiblePurposeValue = PermissiblePurpose.Where(x => x.pkPermissibleId == (ObjModel.PermissiblePurposeId == 0 ? 1 : ObjModel.PermissiblePurposeId)).Select(x => x.PermissiblePurpose).FirstOrDefault().ToString();
                #endregion


            }
            #region Package infomation
            using (EmergeDALDataContext _EmergeDALDataContext = new EmergeDALDataContext())
            {
                string displayPckName = string.Empty;
                bool flagPackages = false;
                char[] trimch = new char[1];
                trimch[0] = ',';
                var list = _EmergeDALDataContext.proc_GetOrderPackage(OrderId).ToList();
                if (list != null)
                {
                    foreach (var ch in list)
                    {
                        displayPckName = displayPckName + ch.tblProductPackages_PackageName + ",";
                        flagPackages = true;
                    }
                    ObjModel.PackageName = displayPckName.Trim(trimch);
                    if (flagPackages)
                    {
                        ObjModel.PackageDisplayNameStatus = true;
                    }
                }
            }
            #endregion
        }

        public void GetViewReport(ViewReportModel ObjModel, int OrderId, int ReviewQueryString, int ViewReportReview, string Username)
        {
            BALOrders ObjOrder = new BALOrders();

            var ObjRequestAndResponseResult = ObjOrder.GetRequestAndResponseByOrderId(OrderId);
            if (ObjRequestAndResponseResult.Count > 0)
            {


                #region Check 16 Month Old

                ObjModel.hdnIs6MonthOld = false;
                BALGeneral ObjBALGeneral = new BALGeneral();
                ObjModel.hdnIs6MonthOld = ObjBALGeneral.Is6MonthOld(OrderId);
                ObjModel.IsShowRawData = false;

                #endregion

                #region Applicant Info

                ObjModel.ApplicantDob = ObjRequestAndResponseResult.ElementAt(0).SearchedDob;
                ObjModel.ApplicantState = ObjRequestAndResponseResult.ElementAt(0).SearchedStateCode;
                ObjModel.OrderNo = ObjRequestAndResponseResult.ElementAt(0).OrderNo;
                if (ObjRequestAndResponseResult.ElementAt(0).CompletionDate != null)
                {
                    ObjModel.CompletionDate = Convert.ToDateTime(ObjRequestAndResponseResult.ElementAt(0).CompletionDate).ToString("MM/dd/yyyy HH:mm:ss");
                }
                ObjModel.OrderDt = ObjRequestAndResponseResult.ElementAt(0).OrderDt.ToString("MM/dd/yyyy HH:mm:ss");
                string SearchedName = GenerateApplicantName(ObjRequestAndResponseResult[0].SearchedLastName, ObjRequestAndResponseResult[0].SearchedSuffix, ObjRequestAndResponseResult[0].SearchedMiddleInitial, ObjRequestAndResponseResult[0].SearchedFirstName);
                string FullSearchedName = SearchedName.Replace("'", "");
                ObjModel.ApplicantName = FullSearchedName;
                ViewBag.UserFullName1 = FullSearchedName;
                ObjModel.ApplicantNameForEmail = ObjRequestAndResponseResult[0].SearchedLastName + "_" + ObjRequestAndResponseResult[0].SearchedFirstName;
                ObjModel.forReportLocation = new BALLocation().GetLocationDetailByLocationId(Convert.ToInt32(ObjRequestAndResponseResult[0].fkLocationId)).Select(loc => loc.City).FirstOrDefault();//INT343

                #region Mailing Address
                string mailingAddress = "";

                if (ObjRequestAndResponseResult[0].SearchedStreetAddress != null || ObjRequestAndResponseResult[0].SearchedStreetName != null)
                {
                    mailingAddress = ObjRequestAndResponseResult[0].SearchedStreetAddress != string.Empty ? ObjRequestAndResponseResult[0].SearchedStreetAddress + "\n" : string.Empty;
                    mailingAddress = ObjRequestAndResponseResult[0].SearchedStreetName != string.Empty ? mailingAddress + ObjRequestAndResponseResult[0].SearchedStreetName + "\n" : mailingAddress;
                }
                mailingAddress = ObjRequestAndResponseResult[0].SearchedStreetType != string.Empty ? mailingAddress + ObjRequestAndResponseResult[0].SearchedStreetType + ", " : mailingAddress;
                mailingAddress = ObjRequestAndResponseResult[0].SearchedApt != string.Empty ? mailingAddress + ObjRequestAndResponseResult[0].SearchedApt + ", " : mailingAddress;
                mailingAddress = ObjRequestAndResponseResult[0].SearchedCity != string.Empty ? mailingAddress + ObjRequestAndResponseResult[0].SearchedCity : mailingAddress;

                if (mailingAddress != string.Empty)
                {
                    mailingAddress = ObjRequestAndResponseResult[0].SearchedStateCode != string.Empty ? mailingAddress + "\n" + ObjRequestAndResponseResult[0].SearchedStateCode : mailingAddress;
                    mailingAddress = ObjRequestAndResponseResult[0].SearchedZipCode != string.Empty ? mailingAddress + " " + ObjRequestAndResponseResult[0].SearchedZipCode : mailingAddress;
                }
                ObjModel.ApplicantAddress = mailingAddress;
                ObjModel.IsAddressUpdated = ObjRequestAndResponseResult[0].IsAddressUpdated;
                #endregion
                #endregion

                #region Company Info
                ObjModel.PendingOrderNote = string.Empty;
                if (ObjRequestAndResponseResult.ElementAt(0).OrderStatus == 1)           // When Order Status is pending then show Update Note
                {
                    if (ObjRequestAndResponseResult.ElementAt(0).PendingOrderUpdateNote != string.Empty)
                    {
                        ObjModel.PendingOrderNote = DisplayPendingOrderNote(ObjRequestAndResponseResult[0].PendingOrderUpdateNote);
                    }
                }

                MembershipUser Member = Membership.GetUser();//User.Identity.Name
                if (Member != null)
                {
                    string[] Role = Roles.GetRolesForUser(Member.Email);

                    BALCompany ObjBALCompany = new BALCompany();

                    List<bool> lst = ObjBALCompany.GetReportStatusData(OrderId);
                    if (lst.Count > 0)
                    {
                        ObjModel.IsPassReport = lst[0];
                        ObjModel.IsAppliedStatus = lst[1];
                    }
                    else
                    {
                        ObjModel.IsPassReport = false;
                        ObjModel.IsAppliedStatus = false;
                    }
                    List<Proc_Get_CompanyDetailByMemberShipIdResult> ObjCompanyDetails = ObjBALCompany.GetCompanyDetailByMemberShipIdResult(ObjRequestAndResponseResult.ElementAt(0).fkUserId);
                    if (ObjCompanyDetails.Count > 0)
                    {
                        if (Role.Contains("SystemAdmin") || Role.Contains("SupportTechnician") || Role.Contains("SupportManager"))
                        {
                            ObjModel.IsShowRawData = true;
                            using (EmergeDALDataContext dx = new EmergeDALDataContext())
                            {
                                //Get home company location based on JIRA ticket INT-325 as per Greg comments.
                                var homeCompanyLocation = dx.tblLocations.Where(x => x.fkCompanyId == ObjRequestAndResponseResult.First().fkCompanyId && x.IsHome == true).FirstOrDefault();
                                string homeLocation = string.Empty;
                                if (homeCompanyLocation != null)
                                {
                                    //Set home location.
                                    homeLocation = Convert.ToString(homeCompanyLocation.pkLocationId);
                                }
                                else
                                {
                                    //If query is not able to get home location than location will be the exact location where selected order has created.
                                    homeLocation = ObjRequestAndResponseResult.First().fkLocationId.ToString();
                                }
                                var v = dx.tblCompanyUsers.Where(d => d.fkUserId == ObjRequestAndResponseResult.First().fkUserId).Select(d => new { d.pkCompanyUserId }).FirstOrDefault();
                                if (v != null)
                                {
                                    ObjModel.CompanyUser = "<a title=\"Click here to edit User\"  href=" + ApplicationPath.GetApplicationPath() + "Control/AddCompanyUser?_a=" + v.pkCompanyUserId.ToString() + ">" + ObjCompanyDetails.First().lastname + ", " + ObjCompanyDetails.First().firstname + "</a>";
                                }
                                else
                                {
                                    ObjModel.CompanyUser = "<a title=\"Click here to edit User\"  href=" + ApplicationPath.GetApplicationPath() + "Control/AddCompanyUser?_a=" + Guid.Empty + ">" + ObjCompanyDetails.First().lastname + ", " + ObjCompanyDetails.First().firstname + "</a>";
                                }
                                ObjModel.CompanyLocation = "<a title=\"Click here to edit Location\"   href=" + ApplicationPath.GetApplicationPath() + "Control/AddCompanyLocation?lid=" + ObjRequestAndResponseResult.First().fkLocationId.ToString() + ">" + ObjCompanyDetails.First().city + ", " + ObjCompanyDetails.First().statecode + "</a>";
                                ObjModel.CompanyName = "<a title=\"Click here to edit Company\"    href=" + ApplicationPath.GetApplicationPath() + "Control/AddCompanies?Id1=" + homeLocation + ">" + ObjCompanyDetails.First().CompanyName + "</a>";
                                ObjModel.CompanyNameAdverse = ObjCompanyDetails.First().CompanyName;
                                ObjModel.IsReportStatus = ObjCompanyDetails.First().IsReportStatus;

                            }
                        }
                        else
                        {
                            ObjModel.IsShowRawData = false;
                            ObjModel.CompanyUser = ObjCompanyDetails.First().lastname + ", " + ObjCompanyDetails.First().firstname;
                            ObjModel.CompanyLocation = ObjCompanyDetails.First().city + ", " + ObjCompanyDetails.First().statecode;
                            ObjModel.CompanyName = ObjCompanyDetails.First().CompanyName;
                            ObjModel.CompanyNameAdverse = ObjCompanyDetails.First().CompanyName;
                            ObjModel.IsReportStatus = ObjCompanyDetails.First().IsReportStatus;
                        }
                    }


                    int LoginCompanyId = 0;
                    int OrderCompanyId = 0;
                    Guid UserId = new Guid(Member.ProviderUserKey.ToString());
                    List<Proc_Get_CompanyDetailByMemberShipIdResult> ObjRootCompanyDetails = ObjBALCompany.GetCompanyDetailByMemberShipIdResult(UserId);
                    {
                        //Guid RootCompanyId = new Guid("7DC87D77-6BB5-4BB3-9837-FA861F36F281");
                        //int RootCompanyId = 9514;
                        int RootCompanyId = 9831;
                        #region Emerge review Pop details

                        //email id for emerge review message
                        ObjModel.CurrentUserEmail = Member.Email;
                        ObjModel.lblToRR = ConfigurationManager.AppSettings["ReportReviewMail"].ToString();

                        //email id for asking admin to activate report
                        ObjModel.lblMailto = BALGeneral.GetGeneralSettings().SupportEmailId;
                        ObjModel.lblMailFrom = Member.Email;
                        ObjModel.txtMessage = Username + " is requesting to activate the Emerge Review feature ##report## ";



                        #endregion

                        if (ObjRootCompanyDetails.First().PkCompanyId != 0)
                        {
                            LoginCompanyId = ObjRootCompanyDetails.First().PkCompanyId;
                            OrderCompanyId = (int)ObjRequestAndResponseResult[0].fkCompanyId;

                            if (RootCompanyId == ObjRootCompanyDetails.First().PkCompanyId)
                            {
                                ObjModel.CompanyPhone = ObjCompanyDetails.First().CompanyPhone.ToString();
                                ObjModel.LocationPhone = ObjCompanyDetails.First().LocationPhone.ToString();
                                ObjModel.UserPhone = ObjCompanyDetails.First().UserPhone.ToString();
                                ObjModel.EmailAddress = ObjCompanyDetails.First().EmailAddress.ToString();
                                ObjModel.IsRootCompanyId = true;
                                ObjModel.IsReportStatus = ObjCompanyDetails.First().IsReportStatus;
                            }
                            else
                            {
                                ObjModel.IsRootCompanyId = false;
                            }
                        }
                    }

                #endregion

                    #region Product Code

                    int nameDiff = 0;
                    int IsAutoReviewPopup = 0;
                    int IsAutoReviewSent = 0;
                    string BottomScript = string.Empty;
                    string EmergeAssistScript = string.Empty;
                    string DisclaimerScript = string.Empty;
                    int MVR_Counter = 0; int CountReviewReports = 0;
                    string productcodeforReview = string.Empty;
                    bool boolLiveRunner = false;
                    // BALEmergeReview ObjBALEmergeReview = new BALEmergeReview();


                    var ObjProc_Get_RequestAndResponseResultList = ObjRequestAndResponseResult.OrderByDescending(d => d.ProductCode);
                    List<ClsViewReport> ObjViewReport = new List<ClsViewReport>();
                    foreach (Proc_Get_RequestAndResponseResult ObjRequestAndResponseResults in ObjProc_Get_RequestAndResponseResultList)
                    {

                        StringBuilder TopScript = new StringBuilder();
                        ClsViewReport ObjClsViewReport = new ClsViewReport();

                        /* Send an Email For Pending Reports */
                        productcodeforReview = ObjRequestAndResponseResults.ProductCode;
                        try
                        {

                            if (ObjRequestAndResponseResults.ReportStatus == 1 && ObjRequestAndResponseResults.IsSentPendingEmail == false)
                            {
                                SendPendingMessage(ObjRequestAndResponseResults);
                                BALGeneral.UpdateEmailSentStatus(ObjRequestAndResponseResults.pkOrderDetailId);
                            }

                            /* Send an Email For Pending Reports */

                            /* Send an Email For In-Complete Reports */
                            //  if (ObjRequestAndResponseResults.IsSentInCompleteEmail == null) { ObjRequestAndResponseResults.IsSentInCompleteEmail = false; } //for #387  ticket.

                            if (ObjRequestAndResponseResults.ReportStatus == 3 && ObjRequestAndResponseResults.IsSentInCompleteEmail == false)
                            {
                                SendInCompleteMessage(ObjRequestAndResponseResults);
                                BALGeneral.UpdateInCompleteEmailSentStatus(ObjRequestAndResponseResults.pkOrderDetailId);
                            }
                        }
                        catch (Exception)
                        {

                        }
                        /* Send an Email For In-Complete Reports */


                        #region Report Type

                        string DisplyProductCode = "";
                        if (ObjRequestAndResponseResults.IsLiveRunner == true)
                        {
                            DisplyProductCode = ObjRequestAndResponseResults.ProductCode + "*";
                        }
                        else
                        {
                            DisplyProductCode = ObjRequestAndResponseResults.ProductCode;
                        }
                        ObjModel.ReportType += DisplyProductCode + ", ";

                        if ((LoginCompanyId == OrderCompanyId) && (ObjRequestAndResponseResults.IsViewed == false) && (ObjRequestAndResponseResults.ReportStatus == 2))
                        {
                            IsAutoReviewPopup = 1;
                            BALGeneral objGeneral = new BALGeneral();
                            objGeneral.UpdateOrderDetail_IsViewed(ObjRequestAndResponseResults.pkOrderDetailId);     // Update Isviewed for order report
                        }

                        UpdateOrderIsViewed(LoginCompanyId, OrderCompanyId, OrderId); // Update Read Unread

                        #endregion

                        #region Live Runner

                        string LiveRunnerImagePath = "../Content/themes/base/images/LiveRunnerSmall.png";

                        string IsLiveRunnerProvided = "";
                        if (ObjRequestAndResponseResults.IsLiveRunner == true)
                        {
                            IsLiveRunnerProvided = "<table><tr><td style='padding-right:15px;line-height: 44px;' >Provided By</td><td><img src='" + LiveRunnerImagePath + "' alt='Live Runner' height='13' style='padding-right: 30px;border:0px'/></td></tr></table>";
                        }
                        else
                        {
                            IsLiveRunnerProvided = "";
                        }

                        #endregion

                        #region SCR STATE COUNTY

                        string SCRState = "";
                        if (ObjRequestAndResponseResults.ProductCode.ToLower() == "scr")
                        {
                            SCRState = "[" + ObjRequestAndResponseResults.StateName + "]";
                        }
                        //Used for MVR Report INT-83 (SMALL CHANGE: We would like "MVR" reports to show the state that is selected.)
                        if (ObjRequestAndResponseResults.ProductCode.ToLower() == "mvr")
                        {
                            SCRState = "[" + ObjRequestAndResponseResults.StateName + "]";
                        }


                        string StateCounty = "";
                        string StateCode = "";
                        string CountyName = ObjRequestAndResponseResults.CountyCode;
                        if (CountyName != "")
                        {
                            StateCode = ObjRequestAndResponseResults.StateCodeReport;
                            if (StateCode != "")
                            {
                                StateCounty = "[" + CountyName + ", " + StateCode + "]";
                            }
                        }

                        #endregion

                        #region Top Report Header
                        if (ObjRequestAndResponseResults.IsLiveRunner == true)
                        {
                            if (ObjRequestAndResponseResults.ProductCode == "SCR" || ObjRequestAndResponseResults.ProductCode == "CCR1")
                            {
                                if (ObjRequestAndResponseResults.ProductDisplayName == "Manual State Criminal")
                                {
                                    ObjRequestAndResponseResults.ProductDisplayName = "LiveRunner State Criminal Report";

                                }

                                if (ObjRequestAndResponseResults.ProductDisplayName == "Manual County Search")
                                {
                                    ObjRequestAndResponseResults.ProductDisplayName = "LiveRunner County Search";

                                }
                            }
                        }

                        #region check dobvariation count for ticket #83

                        // disable auto emerge review in ncr reports if find dob variation.
                        int RemainingNUllDOB = 0;
                        if (ObjRequestAndResponseResults.IsViewed == false && ObjRequestAndResponseResults.ReviewStatus != 1)
                        {

                            if (ObjRequestAndResponseResults.ProductCode == "NCR1" || ObjRequestAndResponseResults.ProductCode == "NCR+" || ObjRequestAndResponseResults.ProductCode == "NCR4")
                            {

                                if (!string.IsNullOrEmpty(ObjRequestAndResponseResults.ResponseData))
                                {
                                    string XML_ValueCharges = GetRequiredXml_ForRapidCourt(ObjRequestAndResponseResults.ResponseData);
                                    XDocument ObjXDocument = XDocument.Parse(XML_ValueCharges);
                                    var List_Charges = ObjXDocument.Descendants("CriminalCase").ToList();
                                    if (List_Charges.Count > 0)
                                    {
                                        foreach (XElement user in ObjXDocument.Descendants("CriminalCase"))
                                        {
                                            foreach (XElement contact in user.Descendants("DemographicDetail"))
                                            {
                                                var dob = user.Descendants("DateOfBirth");
                                                if (dob.Count() == 0)
                                                { RemainingNUllDOB++; }
                                                else { }
                                            }
                                        }
                                    }
                                }
                            }

                            if (RemainingNUllDOB > 0) //for ticket 83
                            {
                                ObjRequestAndResponseResults.ReviewStatus = 0;
                                ObjRequestAndResponseResults.fkReportCategoryId = 1;
                                BALGeneral objGeneral = new BALGeneral();
                                objGeneral.UpdateOrderDetail_ReviewStatus(ObjRequestAndResponseResults.pkOrderDetailId);
                                ObjModel.CheckNcrReports = true;
                                //ObjRequestAndResponseResults.ReviewStatus =objGeneral.GetOrderDetail_ReviewStatus(ObjRequestAndResponseResults.pkOrderDetailId);//INT-243
                            }
                        }

                        #endregion

                        TopScript.Append("<div class=\"main-box\"><div onclick=\"collapse('" + ObjRequestAndResponseResults.ProductCode + "_" + nameDiff.ToString() + "');\">"
                            + "<table class=\"rv-header\"  width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse\">"
                            + "<tr><td style=\"background-repeat: no-repeat; background-image: url('../content/themes/base/images/" + ObjRequestAndResponseResults.ProductDisplayName + ".png');  background-position: 30px;cursor: pointer;width: 8% !important;color: transparent;  \"  width=\"39px\" height=\"34px\"></td><td class=\"rv-header\" width=\"95%\">"
                            + "<table style='font-weight:bold; width:100%'><tbody><tr><td style=\"vertical-align: middle; width:50%\" align='left'>" + ObjRequestAndResponseResults.ProductDisplayName + " " + SCRState + StateCounty + "</td>"
                            + "<td style=\"vertical-align: middle; width:50%\" align='right'><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>" + IsLiveRunnerProvided + "</td><td></td></tr> </table> </td></tr></tbody></table>"
                            + "</td><td class=\"rv-header\" width=\"9px\"></td></tr></table></div><div id=\"" + ObjRequestAndResponseResults.ProductCode + "_" + nameDiff.ToString() + "_b\" style=\"display: block; margin-left: 12px;\" class=\"pad-bot10\">"
                            + "<table  width=\"98%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td class=\"middle_left\" width=\"14px\"></td><td  width=\"100%\">");

                        if (ObjRequestAndResponseResults.ProductCode.ToLower() == "scr")
                        {
                            TopScript.Append("<div class='people-search-wrap'><div style='font-size:14px;padding-left: 5px; color:#666; font-family:arial'><b>State Searched :</b> " + ObjRequestAndResponseResults.StateName + "</div>");
                            TopScript.Append("<div style='font-size:14px;padding-left: 5px; color:#666; font-family:arial'><b>Name :</b> " + FullSearchedName + "</div></div>");
                        }

                        if (ObjRequestAndResponseResults.ProductCode.ToLower() == "fcr" && (ObjRequestAndResponseResults.IsTieredReport == true || ObjRequestAndResponseResults.IsOptionalReport == true))
                        {
                            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                            {

                                TopScript.Append("<div class='people-search-wrap'><div style='font-size:14px;padding-left: 5px; color:#666; font-family:arial'><b>Name :</b> " + FullSearchedName + " </br> <b>Jurisdiction :</b> " + ObjRequestAndResponseResults.LiveRunnerCounty + "</div></div>");

                                //TopScript.Append("<div style='font-size:14px; color:#666; font-family:arial'><b>Name :</b> " + FullSearchedName + " | <b>Jurisdiction :</b> " + Dx.tblJurisdictions.FirstOrDefault(n => n.pkJurisdictionId == Convert.ToInt32(ObjRequestAndResponseResults.SearchedCountyId)).JurisdictionName + "</div>");
                            }
                        }
                        else if (ObjRequestAndResponseResults.ProductCode.ToLower() == "fcr" && ObjRequestAndResponseResults.IsTieredReport == false && ObjRequestAndResponseResults.IsOptionalReport == false)
                        {

                            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                            {
                                //commented by chetu for ticket #19
                                //TopScript.Append("<div style='font-size:14px; color:#666; font-family:arial'><b>Name :</b> " + FullSearchedName + " | <b>Jurisdiction :</b> " + Dx.tblJurisdictions.FirstOrDefault(n => n.pkJurisdictionId == Convert.ToInt32(ObjRequestAndResponseResults.SearchedCountyId)).JurisdictionName + "</div>");
                                if (ObjRequestAndResponseResults.CountyCode != null)
                                {
                                    TopScript.Append("<div class='people-search-wrap'><div style='font-size:14px;padding-left: 5px; color:#666; font-family:arial'><b>Name :</b> " + FullSearchedName + " </br> <b>Jurisdiction :</b> " + ObjRequestAndResponseResults.CountyCode + "</div></div>");
                                }
                                else
                                {
                                    TopScript.Append("<div class='people-search-wrap'><div style='font-size:14px;padding-left: 5px; color:#666; font-family:arial'><b>Name :</b> " + FullSearchedName + " </br> <b>Jurisdiction :</b> " + ObjRequestAndResponseResults.LiveRunnerCounty + "</div></div>");
                                }
                            }

                        }



                        #endregion

                        #region Fetch Report


                        if (ObjRequestAndResponseResults.ReportStatus.ToString() == "3")
                        {
                            //ObjRequestAndResponseResults
                            if (ObjRequestAndResponseResults.ProductCode == "PSP")
                            {
                                if (ObjRequestAndResponseResults.ResponseData != "")
                                {
                                    if (ObjRequestAndResponseResults.ResponseData != "")
                                    {
                                        XDocument erroXML = XDocument.Parse(ObjRequestAndResponseResults.ErrorLog);
                                        var errorMessage = erroXML.Descendants("StatusDescription").FirstOrDefault().Value.ToString();
                                        TopScript.Append(DisplayInCompleteMessageForPSP(errorMessage));
                                    }
                                    else
                                    {
                                        TopScript.Append(DisplayInCompleteMessage());
                                    }
                                }
                                else
                                {
                                    TopScript.Append(DisplayInCompleteMessage());
                                }
                            }
                            else
                            {
                                TopScript.Append(DisplayInCompleteMessage());
                            }

                            BALOrders ObjBALOrders = new BALOrders();
                            ObjBALOrders.UpdateReportStatus_Ret(ObjRequestAndResponseResults.fkOrderId, ObjRequestAndResponseResults.pkOrderDetailId, 3);
                        }
                        else if (ObjRequestAndResponseResults.fkReportCategoryId == 2 && ObjRequestAndResponseResults.OrderStatus == 4 && ObjModel.IsShowRawData == false && ObjRequestAndResponseResults.ReviewStatus != 2)
                        {//For 282 Ticket
                            TopScript.Append(DisplayNoResponseMessage());

                        }
                        else if (ObjRequestAndResponseResults.ReportStatus.ToString() == "2" && (ObjRequestAndResponseResults.ManualResponse != string.Empty || ObjRequestAndResponseResults.ResponseFileName != string.Empty))
                        {
                            TopScript.Append(DisplayManualResponse(ObjRequestAndResponseResults));
                        }
                        //else if (ObjRequestAndResponseResults.ReportStatus.ToString() == "2" && ObjRequestAndResponseResults.ReviewStatus.ToString() == "1" && ObjModel.IsShowRawData == false)
                        else if (ObjRequestAndResponseResults.ReportStatus.ToString() == "2" && ObjRequestAndResponseResults.ReviewStatus.ToString() == "1" && ObjModel.IsShowRawData == false && (Convert.ToInt32(Session["CriminalCaseCountResponse"]) > 0 && ObjRequestAndResponseResults.ProductCode != "PS"))//INT-456 Auto review 
                        {
                            TopScript.Append(DisplayInReviewMessage());
                        }

                        else
                        {
                            #region Check For MVR
                            //This code will not be executed because new xml integration.
                            //if (ObjRequestAndResponseResults.ProductCode == "MVR1")
                            if (ObjRequestAndResponseResults.ProductCode == "This function is not in use.")
                            {
                                int MVR_ReportStatus = ObjRequestAndResponseResults.ReportStatus;
                                if (MVR_ReportStatus != 2)
                                {
                                    List<Proc_Get_RequestAndResponseResult> ObjProc_Get_RequestAndResponseResult_MVR = null;
                                    while (MVR_ReportStatus != 2 && MVR_Counter < 4)
                                    {
                                        MVR_Counter++;
                                        System.Threading.Thread.Sleep(4000);
                                        ObjProc_Get_RequestAndResponseResult_MVR = ObjOrder.GetRequestAndResponseByOrderId(OrderId).Where(data => data.ProductCode.ToLower() == "mvr").ToList<Proc_Get_RequestAndResponseResult>();
                                        MVR_ReportStatus = ObjProc_Get_RequestAndResponseResult_MVR.Where(data => data.ProductCode.ToLower() == "mvr").FirstOrDefault().ReportStatus;
                                    }

                                    GetMvrResponse(ObjRequestAndResponseResults, TopScript);
                                }
                                else
                                {

                                    if (ObjRequestAndResponseResults.ResponseData.Contains("INVALID ACCOUNT NAME AND PASSWORD") || ObjRequestAndResponseResults.ResponseData.Contains("<request_status>1</request_status>") || ObjRequestAndResponseResults.ResponseData == "")
                                    {
                                        TopScript.Append("Your order was completed manually through Intelifi support department. Please contact support for any further assistance.");
                                    }
                                    else
                                    {
                                        TopScript.Append(ObjRequestAndResponseResults.ResponseData);
                                    }


                                }
                                ObjClsViewReport.XDocument = new XmlDocument();
                            }

                            #endregion

                            #region Fetch Response

                            else
                            {
                                string hdnReviewDetail = FetchResponse(ObjRequestAndResponseResults, ObjClsViewReport, TopScript, false, false, Role, ObjModel);
                                ObjModel.hdnReviewDetail += hdnReviewDetail;
                            }

                            #endregion

                        }

                        #endregion

                        ObjClsViewReport.Starting = TopScript.ToString();

                        #region Review Status


                        string ApplicantFirstName = ObjRequestAndResponseResults.SearchedFirstName, ApplicantLastName = ObjRequestAndResponseResults.SearchedLastName;


                        string DisclaimerInfo = string.Empty;
                        //if (ObjRequestAndResponseResults.ReportStatus != 1)
                        //{
                        if (ObjRequestAndResponseResults.ProductCode.ToLower() == "scr" || ObjRequestAndResponseResults.ProductCode.ToLower() == "mvr" || ObjRequestAndResponseResults.ProductCode.ToLower() == "mvr")
                        {
                            int StateId = Convert.ToInt32(ObjRequestAndResponseResults.SearchedStateId);
                            int? fkPerApplicationId = Convert.ToInt32(ObjRequestAndResponseResults.fkProductApplicationId.ToString());
                            BALGeneral objBalGeneral = new BALGeneral();
                            var list = objBalGeneral.GetDisclamierInfoForScrMvrWcb(fkPerApplicationId, StateId);
                            if (list.Count > 0)
                            {
                                DisclaimerInfo = list.ElementAt(0).DisclaimerData;
                            }

                        }


                        if (ObjRequestAndResponseResults.ProductCode.ToLower() == "ccr1" || ObjRequestAndResponseResults.ProductCode.ToLower() == "ccr1" || ObjRequestAndResponseResults.ProductCode.ToLower() == "ccr2")
                        {
                            string CountyCode = ObjRequestAndResponseResults.CountyCode;
                            int pkProductId = Convert.ToInt32(ObjRequestAndResponseResults.pkProductId.ToString());
                            int fkCountyId = Convert.ToInt32(ObjRequestAndResponseResults.fkCountyId);
                            BALGeneral objBalGeneral = new BALGeneral();

                            var list = objBalGeneral.GetDisclamierInfoForCcr1Ccr2Rcx(fkCountyId, pkProductId);
                            if (list.Count > 0)
                            {
                                DisclaimerInfo = list.ElementAt(0).DisclaimerData;
                            }
                        }

                        if (ObjRequestAndResponseResults.ProductCode.ToLower() == "fcr")
                        {
                            string CountyCode = ObjRequestAndResponseResults.CountyCode;
                            BALGeneral objBalGeneral = new BALGeneral();
                            var list = objBalGeneral.GetDisclamierInfoForFCR(CountyCode);
                            if (list.Count > 0)
                            {
                                DisclaimerInfo = list.ElementAt(0).DisclaimerData;
                            }
                        }

                        //}




                        DisclaimerScript = "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" ><tr><td>";

                        //if (ObjRequestAndResponseResults.ProductCode.ToLower() == "scr" && ObjRequestAndResponseResults.StateName.ToLower() == "georgia")
                        //{
                        DisclaimerScript = DisclaimerScript + "<div class=\"sectionsummary\"><h3 style=\"line-height: 1em;\">" + DisclaimerInfo + "</h3></div>";
                        //}

                        DisclaimerScript = DisclaimerScript + "</td></tr></table>";

                        BottomScript = "</td><td class=\"middle_right\" width=\"9px\"></td></tr></table></div><div id=\"" + ObjRequestAndResponseResults.ProductCode + "_f\" style=\"display: block;margin-left:12px;\"></div></div><br/>";

                        string strPS = ObjRequestAndResponseResults.ProductDisplayName.Replace("'", "\\'");//INT-172 As per Greg comments Help Button will not be displayed on PS section. 


                        if (ObjRequestAndResponseResults.fkReportCategoryId != 2 && !strPS.Contains("People Search"))
                        {
                            EmergeAssistScript = "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" style=\"margin-top: 10px;\"><tr><td style=\"padding-left: 0%;\"> <a href=\"javascript:ReportReview('" + ObjRequestAndResponseResults.ProductDisplayName.Replace("'", "\\'") + "','" + ApplicantLastName.Replace("'", "\\'") + "','" + ApplicantFirstName.Replace("'", "\\'") + "' ,0,'" + ObjRequestAndResponseResults.pkOrderDetailId + "');\"><img style=\"height:28px;\" src='" + ApplicationPath.GetApplicationPath() + "content/themes/base/images/help-icon4.png' alt='help'/></a></td><td ><a style=\"font-family: Arial;display:block;font-size:12px;padding-left:10px;text-decoration:none;font-weight:bold;color:#000;\" href=\"javascript:ReportReview('" + ObjRequestAndResponseResults.ProductDisplayName.Replace("'", "\\'") + "','" + ApplicantLastName.Replace("'", "\\'") + "','" + ApplicantFirstName.Replace("'", "\\'") + "' ,0,'" + ObjRequestAndResponseResults.pkOrderDetailId + "');\"  > Help</a></td>";
                        }
                        else
                        {
                            EmergeAssistScript = "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" style=\"margin-top: 10px;\"><tr><td style=\"padding-left: 0%;\"></td>";
                        }


                        #region Check Product review status for location

                        byte ProductReviewStatus = 0;
                        try
                        {
                            BALEmergeReview ObjBALEmergeReview = new BALEmergeReview();
                            ProductReviewStatus = ObjBALEmergeReview.GetProductReviewStatusPerLocation(Convert.ToInt32(ObjRequestAndResponseResults.fkLocationId), Convert.ToInt32(ObjRequestAndResponseResults.fkProductApplicationId));
                            if (ProductReviewStatus == 0 && ObjRequestAndResponseResults.fkReportCategoryId == 2 && (ObjRequestAndResponseResults.ProductCode.ToLower() != "ncr3" || ObjRequestAndResponseResults.ProductCode.ToLower() != "rcx" || ObjRequestAndResponseResults.ProductCode.ToLower() != "sor"))
                            {
                                ProductReviewStatus = 1;
                            }
                        }
                        catch
                        {

                        }
                        //if (ObjRequestAndResponseResults.IsEnableReview && ObjRequestAndResponseResults.IsAutoReview && ProductReviewStatus == 2 && ObjRequestAndResponseResults.ReportStatus == 2 && ObjRequestAndResponseResults.ReviewStatus != 0)
                        if (ObjRequestAndResponseResults.ReportStatus == 2 && ObjRequestAndResponseResults.ReviewStatus != 0)
                        {
                            IsAutoReviewSent = 1;
                        }

                        #endregion
                        if (ObjRequestAndResponseResults.IsEnableReview != null)
                        {
                            //INT-116 for disable emerge review for psp
                            if (ObjRequestAndResponseResults.fkReportCategoryId != 1 && ObjRequestAndResponseResults.ReviewStatus.ToString() != "1" && ObjRequestAndResponseResults.ProductCode != "MVR" && ObjRequestAndResponseResults.ProductCode != "PSP")
                            {
                                if (ObjRequestAndResponseResults.IsEnableReview == true && ProductReviewStatus > 0)
                                {
                                    EmergeAssistScript = EmergeAssistScript + " <td style=\"padding-left: 2%;\"><a href=\"javascript:ReportReview('" + ObjRequestAndResponseResults.ProductDisplayName.Replace("'", "\\'") + "','" + ApplicantLastName.Replace("'", "\\'") + "','" + ApplicantFirstName.Replace("'", "\\'") + "',1,'" + ObjRequestAndResponseResults.pkOrderDetailId + "');\"><img style=\"height:28px;\" src='" + ApplicationPath.GetApplicationPath() + "content/themes/base/images/emerge-review-button.png' title='Emerge Review'/></a></td><td><a style=\"font-family: Arial;display:block;font-size:12px;padding-left:10px;text-decoration:none;color:#000;\" href=\"javascript:ReportReview('" + ObjRequestAndResponseResults.ProductDisplayName.Replace("'", "\\'") + "','" + ApplicantLastName.Replace("'", "\\'") + "','" + ApplicantFirstName.Replace("'", "\\'") + "',1,'" + ObjRequestAndResponseResults.pkOrderDetailId + "');\"  > &nbsp;<b>Emerge Review /</b> Connect with an agent  </a></td><td style='display:none'><textarea id=" + ObjRequestAndResponseResults.pkOrderDetailId + ">" + ObjRequestAndResponseResults.ReviewComments + " </textarea></td>";
                                }
                                else
                                {
                                    EmergeAssistScript = EmergeAssistScript + " <td style=\"padding-left: 2%;\"><a id='" + ObjRequestAndResponseResults.ProductDisplayName.Replace("'", "\\'") + "' href=\"javascript:ActivateReportsVR('" + ObjRequestAndResponseResults.ProductDisplayName.Replace("'", "\\'") + "');\"><img style=\"height:28px;\" src='" + ApplicationPath.GetApplicationPath() + "content/themes/base/images/emerge-review-button.png' alt='Enable Review' title='Send Request for Emerge Review'/></a></td><td><a style=\"font-family: Arial;display:block;font-size:12px;padding-left:10px;text-decoration:none;color:#000;\" href=\"javascript:ActivateReportsVR('" + ObjRequestAndResponseResults.ProductDisplayName.Replace("'", "\\'") + "');\"  > &nbsp;<b>Emerge Review /</b> Connect with an agent</a></td>";
                                }
                            }
                            else
                            {

                                if (ObjModel.CheckNcrReports)
                                {
                                    EmergeAssistScript = EmergeAssistScript + " <td style=\"padding-left: 2%;\"><a href=\"javascript:ReportReview('" + ObjRequestAndResponseResults.ProductDisplayName.Replace("'", "\\'") + "','" + ApplicantLastName.Replace("'", "\\'") + "','" + ApplicantFirstName.Replace("'", "\\'") + "',1,'" + ObjRequestAndResponseResults.pkOrderDetailId + "');\"><img style=\"height:28px;\" src='" + ApplicationPath.GetApplicationPath() + "content/themes/base/images/emerge-review-button.png' title='Emerge Review'/></a></td><td><a style=\"font-family: Arial;display:block;font-size:12px;padding-left:10px;text-decoration:none;color:#000;\" href=\"javascript:ReportReview('" + ObjRequestAndResponseResults.ProductDisplayName.Replace("'", "\\'") + "','" + ApplicantLastName.Replace("'", "\\'") + "','" + ApplicantFirstName.Replace("'", "\\'") + "',1,'" + ObjRequestAndResponseResults.pkOrderDetailId + "');\"  > &nbsp;<b>Emerge Review /</b> Connect with an agent </a></td><td style='display:none'><textarea id=" + ObjRequestAndResponseResults.pkOrderDetailId + ">" + ObjRequestAndResponseResults.ReviewComments + " </textarea></td>";
                                }
                                else
                                {

                                    EmergeAssistScript = EmergeAssistScript + " <td style=\"padding-left: 2%;\"></td>";
                                }
                            }
                        }

                        //if (ObjRequestAndResponseResults.IsEnableReview != null)
                        //{
                        //    if (ObjRequestAndResponseResults.fkReportCategoryId != 1)
                        //    {
                        //        if (ObjRequestAndResponseResults.IsEnableReview == true && ProductReviewStatus > 0)
                        //        {
                        //            EmergeAssistScript = EmergeAssistScript + " <td><a href=\"javascript:ReportReview('" + ObjRequestAndResponseResults.ProductDisplayName.Replace("'", "\\'") + "','" + ApplicantLastName.Replace("'", "\\'") + "','" + ApplicantFirstName.Replace("'", "\\'") + "',1,'" + ObjRequestAndResponseResults.pkOrderDetailId + "');\"><img style=\"height:28px;\" src='" + ApplicationPath.GetApplicationPath() + "content/themes/base/images/emerge-review-button.png' title='Emerge Review'/></a></td><td><a style=\"font-family: Arial;display:block;font-size:12px;text-decoration:none;font-weight:bold;color:#000;\" href=\"javascript:ReportReview('" + ObjRequestAndResponseResults.ProductDisplayName.Replace("'", "\\'") + "','" + ApplicantLastName.Replace("'", "\\'") + "','" + ApplicantFirstName.Replace("'", "\\'") + "',1,'" + ObjRequestAndResponseResults.pkOrderDetailId + "');\"  > Emerge Review</a></td><td style='display:none'><textarea id=" + ObjRequestAndResponseResults.pkOrderDetailId + ">" + ObjRequestAndResponseResults.ReviewComments + " </textarea></td>";
                        //        }
                        //        else
                        //        {
                        //            EmergeAssistScript = EmergeAssistScript + " <td><a id='" + ObjRequestAndResponseResults.ProductDisplayName.Replace("'", "\\'") + "' href=\"javascript:ActivateReportsVR('" + ObjRequestAndResponseResults.ProductDisplayName.Replace("'", "\\'") + "');\"><img style=\"height:28px;\" src='" + ApplicationPath.GetApplicationPath() + "content/themes/base/images/emerge-review-button.png' alt='Enable Review' title='Send Request for Emerge Review'/></a></td><td><a style=\"font-family: Arial;display:block;font-size:12px;text-decoration:none;font-weight:bold;color:#000;\" href=\"javascript:ActivateReportsVR('" + ObjRequestAndResponseResults.ProductDisplayName.Replace("'", "\\'") + "');\"  > Emerge Review</a></td>";
                        //        }
                        //    }
                        //}
                        EmergeAssistScript = EmergeAssistScript + "</tr></table>";

                        #endregion

                        ObjClsViewReport.Ending = DisclaimerScript + EmergeAssistScript + BottomScript;

                        nameDiff++;

                        ObjViewReport.Add(ObjClsViewReport);


                        if (ObjRequestAndResponseResults.ReviewStatus != 0)
                            CountReviewReports++;
                    }

                    ObjModel.ObjViewReportString = ObjViewReport;

                    if (ObjModel.ReportType.Length > 0)
                    {
                        ObjModel.ReportType = ObjModel.ReportType.Substring(0, ObjModel.ReportType.Length - 2);
                    }
                    #endregion


                    #region Display Alert If Auto Review Is Sent                 


                    if (ReviewQueryString == 2 && IsAutoReviewSent == 1 && IsAutoReviewPopup == 1)//INT-202
                    {
                       ObjModel.IsAutoReviewSentPopUp = true;
                    }


                    if (CountReviewReports > 0)
                        ObjModel.IsReviewedReport = 1;
                    else
                        ObjModel.IsReviewedReport = 0;


                    int IsReportReview = 0;
                    IsReportReview = ReviewQueryString;

                    if (ObjModel.IsReviewedReport != 0 || IsReportReview != 0)
                        ObjModel.AllowReviewReportTab = true;
                    if (IsReportReview == 2 && ObjModel.IsReviewedReport == 0)
                        ObjModel.AllowReviewReportTab = false;
                    if (IsReportReview == 0)
                        ObjModel.AllowReviewReportTab = false;
                    if (ObjModel.IsReviewedReport != 0 || ViewReportReview == 1)// Request.Url.AbsolutePath.Contains("ViewReportReview")
                        ObjModel.AllowReviewReportTab = true;
                    else
                        ObjModel.AllowReviewReportTab = false;

                    #endregion

                    #region Reference Code and Notes


                    List<Proc_GetReferenceCodebyCompanyIdResult> ObjList = new List<Proc_GetReferenceCodebyCompanyIdResult>();
                    ObjList = ObjBALCompany.GetReferenceCodeListbypkCompanyId(Convert.ToInt32(ObjRequestAndResponseResult[0].fkCompanyId.ToString()));

                    ObjList.Insert(0, new Proc_GetReferenceCodebyCompanyIdResult { ReferenceCode = "Select Reference #", pkReferenceCodeId = -1 });
                    ObjList.Add(new Proc_GetReferenceCodebyCompanyIdResult { ReferenceCode = "------------------", pkReferenceCodeId = -2 });
                    ObjList.Add(new Proc_GetReferenceCodebyCompanyIdResult { ReferenceCode = "Add New", pkReferenceCodeId = -3 });
                    ObjModel.ReferenceCodeList = ObjList;
                    ObjModel.TrackingRefCodeValue = "NA";
                    var SearchedTrackingRef = ObjRequestAndResponseResult[0].SearchedTrackingRef == "" ? "NA" : ObjRequestAndResponseResult[0].SearchedTrackingRef;
                    if (SearchedTrackingRef != "NA" && SearchedTrackingRef != null)
                    {
                        var SelectedrefCode = ObjList.Where(rec => rec.ReferenceCode.ToLower().Trim() == SearchedTrackingRef.ToLower().Trim()).FirstOrDefault();
                        if (SelectedrefCode != null)
                        {
                            var TrackingRefCodeValue = SelectedrefCode.pkReferenceCodeId;
                            ObjModel.TrackingRefCodeValue = TrackingRefCodeValue.ToString();
                        }
                    }
                    ObjModel.ReferenceCode = ObjRequestAndResponseResult[0].SearchedTrackingRef == "" ? "NA" : ObjRequestAndResponseResult[0].SearchedTrackingRef;
                    ObjModel.Notes = ObjRequestAndResponseResult[0].SearchedTrackingNotes == "" ? "NA" : ObjRequestAndResponseResult[0].SearchedTrackingNotes;
                    ObjModel.fkCompanyId = ObjRequestAndResponseResult[0].fkCompanyId;
                    #endregion

                    #region Permissible Purpose
                    int? orderpermissibleid = 0;
                    orderpermissibleid = ObjRequestAndResponseResult[0].SearchedPermissibleId == null ? 0 : ObjRequestAndResponseResult[0].SearchedPermissibleId;
                    ObjModel.PermissiblePurposeId = Convert.ToInt32(orderpermissibleid);
                    BALCompanyType serboj = new BALCompanyType();
                    List<tblPermissiblePurpose> PermissiblePurpose = serboj.GetPermissiblePurpose();
                    ObjModel.PermissiblePurposeList = PermissiblePurpose.Select(x => new SelectListItem { Text = x.PermissiblePurpose, Value = x.pkPermissibleId.ToString() }).ToList();
                    ObjModel.PermissiblePurposeList = ObjModel.PermissiblePurposeList.Select(x => new SelectListItem { Text = x.Text, Value = x.Value.ToString(), Selected = (x.Value == ObjModel.PermissiblePurposeId.ToString()) }).ToList();
                    ObjModel.PermissiblePurposeValue = PermissiblePurpose.Where(x => x.pkPermissibleId == (ObjModel.PermissiblePurposeId == 0 ? 1 : ObjModel.PermissiblePurposeId)).Select(x => x.PermissiblePurpose).FirstOrDefault().ToString();
                    #endregion


                    #region SyncPOD

                    ObjModel.IsSyncPOD = ObjRequestAndResponseResult.ElementAt(0).IsSyncpod;
                    if (!string.IsNullOrEmpty(ObjRequestAndResponseResult.ElementAt(0).SyncpodImage))
                    {
                        ObjModel.SyncPODImage = ObjRequestAndResponseResult.ElementAt(0).SyncpodImage.Replace(" ", "");
                    }
                    if (!string.IsNullOrEmpty(ObjRequestAndResponseResult.ElementAt(0).FaceImage))
                    {
                        ObjModel.FaceImage = ObjRequestAndResponseResult.ElementAt(0).FaceImage.Replace(" ", "");
                    }
                    #endregion

                    #region Consent PopUp Code

                    try
                    {
                        List<proc_GetConsentInforByOrderIdResult> lstConsent = ObjOrder.GetConsentPendingFormsByOrderId(OrderId).ToList();
                        ObjModel.CollectionGetConsentInforByOrderId = lstConsent;
                        if (lstConsent.Count > 0)
                        {
                            if (lstConsent.Any(d => d.IsConsentRequired == true))
                            {
                                ObjModel.btnConsentDocsVisible = true;
                            }
                            else
                            {
                                ObjModel.btnConsentDocsVisible = false;
                            }
                        }
                        else
                        {
                            ObjModel.btnConsentDocsVisible = false;
                        }

                    }
                    catch
                    { }

                    #endregion
                }
            }
            else
            {
                List<Proc_GetReferenceCodebyCompanyIdResult> ObjList = new List<Proc_GetReferenceCodebyCompanyIdResult>();
                ObjList.Insert(0, new Proc_GetReferenceCodebyCompanyIdResult { ReferenceCode = "Select Reference #", pkReferenceCodeId = -1 });
                ObjList.Add(new Proc_GetReferenceCodebyCompanyIdResult { ReferenceCode = "------------------", pkReferenceCodeId = -2 });
                ObjList.Add(new Proc_GetReferenceCodebyCompanyIdResult { ReferenceCode = "Add New", pkReferenceCodeId = -3 });
                ObjModel.TrackingRefCodeValue = "NA";
                ObjModel.ReferenceCodeList = ObjList;

                #region Permissible Purpose
                int orderpermissibleid = 0;
                BALCompanyType serboj = new BALCompanyType();
                List<tblPermissiblePurpose> PermissiblePurpose = serboj.GetPermissiblePurpose();
                ObjModel.PermissiblePurposeList = PermissiblePurpose.Select(x => new SelectListItem { Text = x.PermissiblePurpose, Value = x.pkPermissibleId.ToString() }).ToList();
                ObjModel.PermissiblePurposeList = ObjModel.PermissiblePurposeList.Select(x => new SelectListItem { Text = x.Text, Value = x.Value.ToString(), Selected = (x.Value == orderpermissibleid.ToString()) }).ToList();
                ObjModel.PermissiblePurposeValue = PermissiblePurpose.Where(x => x.pkPermissibleId == (ObjModel.PermissiblePurposeId == 0 ? 1 : ObjModel.PermissiblePurposeId)).Select(x => x.PermissiblePurpose).FirstOrDefault().ToString();
                #endregion


            }
            #region Package infomation
            using (EmergeDALDataContext _EmergeDALDataContext = new EmergeDALDataContext())
            {
                string displayPckName = string.Empty;
                bool flagPackages = false;
                char[] trimch = new char[1];
                trimch[0] = ',';
                var list = _EmergeDALDataContext.proc_GetOrderPackage(OrderId).ToList();
                if (list != null)
                {
                    foreach (var ch in list)
                    {
                        displayPckName = displayPckName + ch.tblProductPackages_PackageName + ",";
                        flagPackages = true;
                    }
                    ObjModel.PackageName = displayPckName.Trim(trimch);
                    if (flagPackages)
                    {
                        ObjModel.PackageDisplayNameStatus = true;
                    }
                }
            }
            #endregion
        }

        public ActionResult CustomDeadLockMsg()
        {
            return View();
        }
        private void UpdateOrderIsViewed(int LoginCompanyId, int CurrentOrderCompanyId, int ObjOrderID)     // Update Order as read unread by companyid
        {
            BALOrders ObjBalOrders = new BALOrders();
            try
            {
                if (ObjOrderID != 0)
                {
                    using (EmergeDALDataContext DX = new EmergeDALDataContext())
                    {
                        tblOrderDetail ObjOrderDetail = DX.tblOrderDetails.Where(d => d.fkOrderId == ObjOrderID).FirstOrDefault();

                        if (ObjOrderDetail != null)
                        {
                            if (LoginCompanyId == CurrentOrderCompanyId)
                            {
                                int Result = ObjBalOrders.UpdateOrderIsViewed(true, ObjOrderID);        // Update IsViewed = true for Order when the order is viewed.
                                if (Result == 1)
                                {
                                }
                            }
                        }

                    }
                }
            }
            catch
            {
            }
            finally
            {
                ObjBalOrders = null;
            }
        }

        #endregion

        #region View Report Review

        public ActionResult ViewReportReview()
        {
            bool flagOrderId = false;
            #region Get Query String
            ViewReportModel ObjModel = new ViewReportModel();
            BALContentManagement objBALContentManagement = new BALContentManagement();
            try
            {
                int OrderId = 0;
                if (Request.QueryString["_OId"] != null)
                {
                    #region Check Guid
                    //bool IsGuid = CommonHelper.IsGuid(Request.QueryString["_OId"]);
                    //if (!IsGuid)
                    //{
                    //    return RedirectToAction("SavedReports");
                    //}
                    OrderId = Convert.ToInt32(Request.QueryString["_OId"]);
                    //If user place some invalid order ids in URLs
                    if (Session["Admin"] != null)
                    {
                        string role = Session["Admin"].ToString();
                        if (role == "SystemAdmin" || role == "SupportManager" || role == "SupportTechnician")
                        {
                            //Pass if logged on user is Admin
                        }
                        else
                        {
                            BALOrders balObj = new BALOrders();
                            string cmpId = Convert.ToString(Session["pkCompanyId"]);
                            flagOrderId = balObj.CheckOrderCompanyByOrderId(OrderId, Convert.ToInt32(cmpId));
                            if (!flagOrderId)
                            {
                                //return RedirectToAction("Index", "Home");
                                return RedirectToAction("SavedReports", "Corporate");

                            }
                        }
                    }



                    string FullQueryString = Request.Url.Query;
                    ObjModel.hdnFullQString = FullQueryString;
                    ObjModel.ReviewStatusComment = objBALContentManagement.GetReviewStatusComment(OrderId);
                    #endregion
                }
                else if (Request.QueryString["_AdditionalOrderId"] != null)
                {
                    OrderId = Convert.ToInt32(Request.QueryString["_AdditionalOrderId"]);
                }


                if (Roles.IsUserInRole("SystemAdmin") || Roles.IsUserInRole("SupportManager") || Roles.IsUserInRole("SupportTechnician"))
                {
                    ObjModel.IsAdmin = true;
                }
                else
                {
                    ObjModel.IsAdmin = false;
                }
                ObjModel.CustomMsgsList = objBALContentManagement.GetCustomMessageList();
                List<Proc_Get_CustomReviewMsgsListResult> GetCustomMsgsList = objBALContentManagement.GetCustomMessageList();
                GetCustomMsgsList.Insert(0, new Proc_Get_CustomReviewMsgsListResult { LogTitle = "--Select--", pkLogId = -1 });
                ObjModel.CustomMsgsList = GetCustomMsgsList;

                BALCompany ObjBALCompany = new BALCompany();
                ObjModel.AdditionalReports = ObjBALCompany.GetAdditionalReportsByPkOrderId(OrderId);
                int ReviewQueryString = -1;
                if (!string.IsNullOrEmpty(Request.QueryString["RR"]))
                {
                    ReviewQueryString = Convert.ToInt32(Request.QueryString["RR"]);
                }
                ObjModel.OrderId = OrderId;
            #endregion

                GetViewReportReview(ObjModel, OrderId, false, ReviewQueryString);
                ViewBag.CountyExist = "3";
                if (System.Web.HttpContext.Current.Session["CountyExist"] != null)
                {
                    ViewBag.CountyExist = System.Web.HttpContext.Current.Session["CountyExist"];
                    System.Web.HttpContext.Current.Session["CountyExist"] = null;
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("was deadlocked on lock resources with another process") || ex.Message.Contains("Value cannot be null") || ex.Message.Contains("Timeout expired"))
                {
                    return RedirectToAction("CustomDeadLockMsg", "Corporate");
                }
            }
            finally
            {

            }
            return View(ObjModel);
        }

        public ActionResult UserView_ViewReportReview()
        {
            //bool flagOrderId = false;
            #region Get Query String
            ViewReportModel ObjModel = new ViewReportModel();
            BALContentManagement objBALContentManagement = new BALContentManagement();
            try
            {
                int OrderId = 0;
                if (Request.QueryString["OrderId"] != null)
                {
                    OrderId = Convert.ToInt32(Request.QueryString["OrderId"]);
                }

                else if (!string.IsNullOrEmpty(Request.QueryString["_OId"].ToString()))
                {
                    OrderId = Convert.ToInt32(Request.QueryString["_OId"]);
                }
                else
                {
                    OrderId = Convert.ToInt32(Request.QueryString["_AdditionalOrderId"]);
                }


                if (OrderId != 0)
                {
                    //BALOrders balObj = new BALOrders();
                    string FullQueryString = Request.Url.Query;
                    ObjModel.hdnFullQString = FullQueryString;
                    ObjModel.ReviewStatusComment = objBALContentManagement.GetReviewStatusComment(OrderId);

                }
                ObjModel.IsAdmin = false;
                ObjModel.CustomMsgsList = objBALContentManagement.GetCustomMessageList();
                List<Proc_Get_CustomReviewMsgsListResult> GetCustomMsgsList = objBALContentManagement.GetCustomMessageList();
                GetCustomMsgsList.Insert(0, new Proc_Get_CustomReviewMsgsListResult { LogTitle = "--Select--", pkLogId = -1 });
                ObjModel.CustomMsgsList = GetCustomMsgsList;

                BALCompany ObjBALCompany = new BALCompany();
                ObjModel.AdditionalReports = ObjBALCompany.GetAdditionalReportsByPkOrderId(OrderId);
                int ReviewQueryString = -1;
                if (!string.IsNullOrEmpty(Request.QueryString["RR"]))
                {
                    ReviewQueryString = Convert.ToInt32(Request.QueryString["RR"]);
                }
                ObjModel.OrderId = OrderId;
            #endregion

                UserView_GetViewReportReview(ObjModel, OrderId, false, ReviewQueryString);
                ViewBag.CountyExist = "3";
                if (System.Web.HttpContext.Current.Session["CountyExist"] != null)
                {
                    ViewBag.CountyExist = System.Web.HttpContext.Current.Session["CountyExist"];
                    System.Web.HttpContext.Current.Session["CountyExist"] = null;
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("was deadlocked on lock resources with another process") || ex.Message.Contains("Value cannot be null") || ex.Message.Contains("Timeout expired"))
                {
                    return RedirectToAction("CustomDeadLockMsg", "Corporate");
                }
            }
            finally
            {

            }
            return View(ObjModel);
        }


        /// <summary>
        /// INT-232 View Report as user, while logged in as Admin
        /// </summary>
        public void UserView_GetViewReportReview(ViewReportModel ObjModel, int OrderId, bool IsAdmin, int ReviewQueryString)
        {
            #region 6 Months Old

            // VRR1.Orderid = Request.QueryString["_O"].ToString();
            ObjModel.hdnIs6MonthOld = false;
            BALGeneral ObjBALGeneral = new BALGeneral();
            ObjModel.hdnIs6MonthOld = ObjBALGeneral.Is6MonthOld(OrderId);
            ObjModel.IsShowRawData = false;


            #endregion

            #region Request/Response



            BALOrders ObjOrder = new BALOrders();

            var ObjRequestAndResponseResult = ObjOrder.GetRequestAndResponseByOrderId(OrderId);
            string userIds = string.Empty;
            string userEmailIds = string.Empty;
            using (EmergeDALDataContext dalContext = new Data.EmergeDALDataContext())
            {
                var userDetails = (from odr in (dalContext.tblOrders.Where(o => o.pkOrderId == OrderId))
                                   join cusr in dalContext.tblCompanyUsers
                                   on odr.fkCompanyUserId equals cusr.pkCompanyUserId
                                   join usr in dalContext.aspnet_Users
                                   on cusr.fkUserId equals usr.UserId
                                   select new { UserId = usr.UserId, UserEmail = usr.UserName }).FirstOrDefault();
                userIds = Convert.ToString(userDetails.UserId);
                userEmailIds = userDetails.UserEmail;
            }

            if (ObjRequestAndResponseResult.Count > 0)
            {
                #region Applicant Info

                ObjModel.ApplicantDob = ObjRequestAndResponseResult.ElementAt(0).SearchedDob;
                ObjModel.ApplicantState = ObjRequestAndResponseResult.ElementAt(0).SearchedStateCode;
                ObjModel.OrderNo = ObjRequestAndResponseResult.ElementAt(0).OrderNo;
                ObjModel.ApplicantNameForEmail = ObjRequestAndResponseResult[0].SearchedLastName + "_" + ObjRequestAndResponseResult[0].SearchedFirstName;
                if (ObjRequestAndResponseResult.ElementAt(0).CompletionDate != null)
                {
                    ObjModel.CompletionDate = Convert.ToDateTime(ObjRequestAndResponseResult.ElementAt(0).CompletionDate).ToString("MM/dd/yyyy HH:mm:ss");
                }
                ObjModel.OrderDt = ObjRequestAndResponseResult.ElementAt(0).OrderDt.ToString("MM/dd/yyyy HH:mm:ss");
                string SearchedName = GenerateApplicantName(ObjRequestAndResponseResult[0].SearchedLastName, ObjRequestAndResponseResult[0].SearchedSuffix, ObjRequestAndResponseResult[0].SearchedMiddleInitial, ObjRequestAndResponseResult[0].SearchedFirstName);
                string FullSearchedName = SearchedName.Replace("'", "");
                ObjModel.ApplicantName = FullSearchedName;

                #region Mailing Address
                string mailingAddress = "";

                if (ObjRequestAndResponseResult[0].SearchedStreetAddress != null || ObjRequestAndResponseResult[0].SearchedStreetName != null)
                {
                    mailingAddress = ObjRequestAndResponseResult[0].SearchedStreetAddress != string.Empty ? ObjRequestAndResponseResult[0].SearchedStreetAddress + "\n" : string.Empty;
                    mailingAddress = ObjRequestAndResponseResult[0].SearchedStreetName != string.Empty ? mailingAddress + ObjRequestAndResponseResult[0].SearchedStreetName + "\n" : mailingAddress;
                }
                mailingAddress = ObjRequestAndResponseResult[0].SearchedStreetType != string.Empty ? mailingAddress + ObjRequestAndResponseResult[0].SearchedStreetType + ", " : mailingAddress;
                mailingAddress = ObjRequestAndResponseResult[0].SearchedApt != string.Empty ? mailingAddress + ObjRequestAndResponseResult[0].SearchedApt + ", " : mailingAddress;
                mailingAddress = ObjRequestAndResponseResult[0].SearchedCity != string.Empty ? mailingAddress + ObjRequestAndResponseResult[0].SearchedCity : mailingAddress;

                if (mailingAddress != string.Empty)
                {
                    mailingAddress = ObjRequestAndResponseResult[0].SearchedStateCode != string.Empty ? mailingAddress + "\n" + ObjRequestAndResponseResult[0].SearchedStateCode : mailingAddress;
                    mailingAddress = ObjRequestAndResponseResult[0].SearchedZipCode != string.Empty ? mailingAddress + " " + ObjRequestAndResponseResult[0].SearchedZipCode : mailingAddress;
                }
                ObjModel.ApplicantAddress = mailingAddress;
                ObjModel.IsAddressUpdated = ObjRequestAndResponseResult[0].IsAddressUpdated;
                #endregion

                #endregion

                #region Company Info

                BALCompany ObjBALCompany = new BALCompany();
                List<Proc_Get_CompanyDetailByMemberShipIdResult> ObjCompanyDetails = ObjBALCompany.GetCompanyDetailByMemberShipIdResult(ObjRequestAndResponseResult.ElementAt(0).fkUserId);
                if (ObjCompanyDetails.Count > 0)
                {
                    ObjModel.CompanyUser = ObjCompanyDetails.First().lastname + ", " + ObjCompanyDetails.First().firstname;
                    ObjModel.CompanyLocation = ObjCompanyDetails.First().city + ", " + ObjCompanyDetails.First().statecode;
                    ObjModel.CompanyName = ObjCompanyDetails.First().CompanyName;
                    ObjModel.CompanyNameAdverse = ObjCompanyDetails.First().CompanyName;
                    ObjModel.IsReportStatus = ObjCompanyDetails.First().IsReportStatus;
                }


                MembershipUser Member = Membership.GetUser();//User.Identity.Name
                if (Member != null)
                {
                    Member.Email = userEmailIds;
                    string[] Role = Roles.GetRolesForUser(Member.Email);
                    int LoginCompanyId = 0;
                    int OrderCompanyId = 0;

                    Guid UserId = new Guid(userIds);

                    List<Proc_Get_CompanyDetailByMemberShipIdResult> ObjRootCompanyDetails = ObjBALCompany.GetCompanyDetailByMemberShipIdResult(UserId);
                    {
                        //Guid RootCompanyId = new Guid("7DC87D77-6BB5-4BB3-9837-FA861F36F281");
                        //int RootCompanyId = 9514;
                        int RootCompanyId = 9831;
                        #region Emerge review Pop details

                        //email id for emerge review message
                        ObjModel.CurrentUserEmail = Member.Email;
                        ObjModel.lblToRR = ConfigurationManager.AppSettings["ReportReviewMail"].ToString();

                        //email id for asking admin to activate report
                        ObjModel.lblMailto = BALGeneral.GetGeneralSettings().SupportEmailId;
                        ObjModel.lblMailFrom = Member.Email;
                        ObjModel.txtMessage = Member.UserName + " is requesting to activate the Emerge Review feature ##report## ";




                        #endregion
                        if (ObjRootCompanyDetails.First().PkCompanyId != 0)
                        {
                            LoginCompanyId = ObjRootCompanyDetails.First().PkCompanyId;
                            OrderCompanyId = (int)ObjRequestAndResponseResult[0].fkCompanyId;

                            if (RootCompanyId == ObjRootCompanyDetails.First().PkCompanyId)
                            {
                                ObjModel.CompanyPhone = ObjCompanyDetails.First().CompanyPhone.ToString();
                                ObjModel.LocationPhone = ObjCompanyDetails.First().LocationPhone.ToString();
                                ObjModel.UserPhone = ObjCompanyDetails.First().UserPhone.ToString();
                                ObjModel.EmailAddress = ObjCompanyDetails.First().EmailAddress.ToString();
                                ObjModel.IsRootCompanyId = true;
                            }
                            else
                            {
                                ObjModel.IsRootCompanyId = false;
                            }
                        }
                    }

                    if (ObjCompanyDetails.Count > 0)
                    {
                        if (Role.Contains("SystemAdmin") || Role.Contains("SupportTechnician") || Role.Contains("SupportManager"))
                        {
                            //Do nothing.
                        }
                        else
                        {
                            ObjModel.IsShowRawData = false;
                            ObjModel.CompanyUser = ObjCompanyDetails.First().lastname + ", " + ObjCompanyDetails.First().firstname;
                            ObjModel.CompanyLocation = ObjCompanyDetails.First().city + ", " + ObjCompanyDetails.First().statecode;
                            ObjModel.CompanyName = ObjCompanyDetails.First().CompanyName;
                            ObjModel.CompanyNameAdverse = ObjCompanyDetails.First().CompanyName;
                            ObjModel.IsReportStatus = ObjCompanyDetails.First().IsReportStatus;

                            //int-198
                            ObjModel.CompanyNote = string.IsNullOrEmpty(ObjCompanyDetails.First().CompnayNote) ? "NA" : ObjCompanyDetails.First().CompnayNote;
                            ObjModel.LocationNote = string.IsNullOrEmpty(ObjCompanyDetails.First().LocationNote) ? "NA" : ObjCompanyDetails.First().LocationNote;
                            ObjModel.UserNote = string.IsNullOrEmpty(ObjCompanyDetails.First().UserNote) ? "NA" : ObjCompanyDetails.First().UserNote;
                            //--------
                        }
                    }

                #endregion

                    #region Display Disclamer


                    ObjModel.Disclamer = DisplayDisclaimer();


                    #endregion

                    #region Product Code

                    int nameDiff = 0;
                    string BottomScript = string.Empty;
                    string EmergeAssistScript = string.Empty;
                    string DisclaimerScript = string.Empty;
                    int MVR_Counter = 0; int CountReviewReports = 0;
                    bool IsShowViewTab = false;
                    // BALEmergeReview ObjBALEmergeReview = new BALEmergeReview();


                    var ObjProc_Get_RequestAndResponseResultList = ObjRequestAndResponseResult.OrderByDescending(d => d.ProductCode);
                    List<ClsViewReport> ObjViewReport = new List<ClsViewReport>();
                    foreach (Proc_Get_RequestAndResponseResult ObjRequestAndResponseResults in ObjProc_Get_RequestAndResponseResultList)
                    {
                        //ObjRequestAndResponseResults.ResponseData = ObjRequestAndResponseResults.ResponseData.Replace(@"&#10;", "\n");
                        //  ObjRequestAndResponseResults.ResponseData = ObjRequestAndResponseResults.ResponseData;
                        StringBuilder TopScript = new StringBuilder();
                        ClsViewReport ObjClsViewReport = new ClsViewReport();
                        #region Report Type

                        string DisplyProductCode = "";
                        if (ObjRequestAndResponseResults.ProductCode.Contains("CCR") && ObjRequestAndResponseResults.IsLiveRunner == true)
                        {
                            DisplyProductCode = ObjRequestAndResponseResults.ProductCode + "*";
                        }
                        else
                        {
                            DisplyProductCode = ObjRequestAndResponseResults.ProductCode;
                        }
                        ObjModel.ReportType += DisplyProductCode + ", ";

                        if ((LoginCompanyId == OrderCompanyId) && (ObjRequestAndResponseResults.IsViewed == false) && (ObjRequestAndResponseResults.ReportStatus == 2))
                        {
                            BALGeneral objGeneral = new BALGeneral();
                            objGeneral.UpdateOrderDetail_IsViewed(ObjRequestAndResponseResults.pkOrderDetailId);     // Update Isviewed for order report
                        }

                        #endregion

                        #region Live Runner

                        string LiveRunnerImagePath = ApplicationPath.GetApplicationPath() + "Content/themes/base/images/LiveRunnerSmall.png";

                        string IsLiveRunnerProvided = "";
                        if (ObjRequestAndResponseResults.IsLiveRunner == true)
                        {
                            IsLiveRunnerProvided = "<table><tr><td >Provided By</td><td><img src='" + LiveRunnerImagePath + "' alt='Live Runner' height='13' style='border:0px'/></td></tr></table>";
                        }
                        else
                        {
                            IsLiveRunnerProvided = "";
                        }

                        #endregion

                        #region SCR STATE COUNTY

                        string SCRState = "";
                        if (ObjRequestAndResponseResults.ProductCode.ToLower() == "scr")
                        {
                            SCRState = "[" + ObjRequestAndResponseResults.StateName + "]";
                        }

                        string StateCounty = "";
                        string StateCode = "";
                        string CountyName = ObjRequestAndResponseResults.CountyCode;
                        if (CountyName != "")
                        {
                            StateCode = ObjRequestAndResponseResults.StateCodeReport;
                            if (StateCode != "")
                            {
                                StateCounty = "[" + CountyName + ", " + StateCode + "]";
                            }
                        }

                        #endregion


                        #region Top Report Header

                        TopScript.Append("<div class=\"main-box\" ><div onclick=\"collapse('" + ObjRequestAndResponseResults.ProductCode + "_" + nameDiff.ToString() + "');\">"
                       + "<table class=\"rv-header\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse\">"
                      + "<tr><td style=\"background-repeat: no-repeat; background-image: url('../content/themes/base/images/" + ObjRequestAndResponseResults.ProductDisplayName + ".png');  background-position: 30px;cursor: pointer;width: 8% !important;color: transparent;  \"  width=\"39px\" height=\"34px\"></td><td class=\"rv-header\" width=\"95%\">"
                       + "<table style='font-weight:bold; width:100%'><tbody><tr><td style=\"vertical-align: middle; width:50%\" align='left'>" + ObjRequestAndResponseResults.ProductDisplayName + " " + SCRState + StateCounty + "</td>"
                       + "<td style=\"vertical-align: middle; width:50%\" align='right'><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>" + IsLiveRunnerProvided + "</td><td></td></tr> </table> </td></tr></tbody></table>"
                       + "</td><td class=\"rv-header\" width=\"9px\"></td></tr></table></div><div id=\"" + ObjRequestAndResponseResults.ProductCode + "_" + nameDiff.ToString() + "_b\" style=\"display: block; margin-left: 12px;\"  class=\"pad-bot10\">"
                       + "<table  width=\"98%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td class=\"middle_left\" width=\"14px\"></td><td  width=\"100%\">");



                        if (ObjRequestAndResponseResults.ProductCode.ToLower() == "scr")
                        {
                            TopScript.Append("<div style='font-size:14px;padding-left: 2%;; font-family:arial;color:#666;'><b>State Searched :</b> " + ObjRequestAndResponseResults.StateName + "</div>");
                            TopScript.Append("<div style='font-size:14px;padding-left: 2%;; font-family:arial;color:#666;'><b>Name :</b> " + FullSearchedName + "</div>");
                        }
                        if (ObjRequestAndResponseResults.ProductCode.ToLower() == "fcr" && (ObjRequestAndResponseResults.IsTieredReport == true || ObjRequestAndResponseResults.IsOptionalReport == true))
                        {
                            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                            {

                                TopScript.Append("<div class='people-search-wrap'><div style='font-size:14px;padding-left: 5px; color:#666; font-family:arial'><b>Name :</b> " + FullSearchedName + " </br> <b>Jurisdiction :</b> " + ObjRequestAndResponseResults.LiveRunnerCounty + "</div></div>");
                                //TopScript.Append("<div style='font-size:14px; color:#666; font-family:arial'><b>Name :</b> " + FullSearchedName + " | <b>Jurisdiction :</b> " + Dx.tblJurisdictions.FirstOrDefault(n => n.pkJurisdictionId == Convert.ToInt32(ObjRequestAndResponseResults.SearchedCountyId)).JurisdictionName + "</div>");
                            }
                        }
                        else if (ObjRequestAndResponseResults.ProductCode.ToLower() == "fcr" && ObjRequestAndResponseResults.IsTieredReport == false && ObjRequestAndResponseResults.IsOptionalReport == false)
                        {
                            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                            {
                                //commented for ticket #21
                                //TopScript.Append("<div style='font-size:14px; color:#666; font-family:arial'><b>Name :</b> " + FullSearchedName + " | <b>Jurisdiction :</b> " + Dx.tblJurisdictions.FirstOrDefault(n => n.pkJurisdictionId == Convert.ToInt32(ObjRequestAndResponseResults.SearchedCountyId)).JurisdictionName + "</div>");
                                TopScript.Append("<div class='people-search-wrap'><div style='font-size:14px;padding-left: 5px; color:#666; font-family:arial'><b>Name :</b> " + FullSearchedName + " </br> <b>Jurisdiction :</b> " + ObjRequestAndResponseResults.CountyCode + "</div></div>");
                            }
                        }
                        string ClientReviewComments = ObjRequestAndResponseResults.ReviewComments;
                        if (ObjRequestAndResponseResults.ReviewStatus != 0 && ClientReviewComments != "")
                        {
                            if (Role.Contains("SystemAdmin") || Role.Contains("SupportTechnician") || Role.Contains("SupportManager"))
                            {
                                //Do nothing.
                            }
                        }

                        #endregion

                        #region Fetch Report
                        //if (ObjRequestAndResponseResults.OrderStatus == 4 && ObjRequestAndResponseResults.ProductCode == "NCR1")
                        //{
                        //    TopScript.Append(DisplayNoResponseMessage());
                        //}

                        if (ObjRequestAndResponseResults.ReportStatus.ToString() == "3")
                        {
                            if (ObjRequestAndResponseResults.ProductCode == "PSP")
                            {
                                if (ObjRequestAndResponseResults.ResponseData != "")
                                {
                                    XDocument erroXML = XDocument.Parse(ObjRequestAndResponseResults.ErrorLog);
                                    var errorMessage = erroXML.Descendants("StatusDescription").FirstOrDefault().Value.ToString();
                                    TopScript.Append(DisplayInCompleteMessageForPSP(errorMessage));
                                }
                                else
                                {
                                    TopScript.Append(DisplayInCompleteMessage());
                                }
                            }
                            else
                            {
                                TopScript.Append(DisplayInCompleteMessage());
                            }
                            BALOrders ObjBALOrders = new BALOrders();
                            ObjBALOrders.UpdateReportStatus_Ret(ObjRequestAndResponseResults.fkOrderId, ObjRequestAndResponseResults.pkOrderDetailId, 3);
                        }
                        else if (ObjRequestAndResponseResults.fkReportCategoryId == 2 && ObjRequestAndResponseResults.OrderStatus == 4 && ObjModel.IsShowRawData == false && ObjRequestAndResponseResults.ReviewStatus != 2)
                        { // #282
                            TopScript.Append(DisplayNoResponseMessage());
                        }
                        else if (ObjRequestAndResponseResults.ReportStatus.ToString() == "2" && (ObjRequestAndResponseResults.ManualResponse != string.Empty || ObjRequestAndResponseResults.ResponseFileName != string.Empty))
                        {
                            TopScript.Append(DisplayManualResponse(ObjRequestAndResponseResults));
                        }
                        //else if (ObjRequestAndResponseResults.ReportStatus.ToString() == "2" && ObjRequestAndResponseResults.ReviewStatus.ToString() == "1" && ObjModel.IsShowRawData == false)
                        else if (ObjRequestAndResponseResults.ReportStatus.ToString() == "2" && ObjRequestAndResponseResults.ReviewStatus.ToString() == "1" && ObjModel.IsShowRawData == false && (Convert.ToInt32(Session["CriminalCaseCountResponse"]) > 0 && ObjRequestAndResponseResults.ProductCode != "PS"))//INT-456 Auto review
                        {
                            TopScript.Append(DisplayInReviewMessage());
                        }


                        else
                        {
                            #region Check For MVR

                            if (ObjRequestAndResponseResults.ProductCode == "MVR")
                            {
                                if (IsAdmin == true)//Request.Url.AbsoluteUri.ToLower().Contains("control")) //&& ObjRequestAndResponseResult.ReviewStatus != 0 //03Jun2011//Page.Request.Url.AbsoluteUri.ToLower().Contains("admin") ||
                                {
                                    if (ObjModel.hdnReviewDetail == "")
                                    {
                                        ObjModel.hdnReviewDetail = ObjRequestAndResponseResults.pkOrderDetailId + "_" + ObjRequestAndResponseResults.ProductCode + "_" + ObjRequestAndResponseResults.pkOrderResponseId;
                                    }
                                    else
                                    {
                                        ObjModel.hdnReviewDetail = ObjModel.hdnReviewDetail + "," + ObjRequestAndResponseResults.pkOrderDetailId + "_" + ObjRequestAndResponseResults.ProductCode + "_" + ObjRequestAndResponseResults.pkOrderResponseId;
                                    }
                                }

                                int MVR_ReportStatus = ObjRequestAndResponseResults.ReportStatus;
                                if (MVR_ReportStatus != 2)
                                {
                                    List<Proc_Get_RequestAndResponseResult> ObjProc_Get_RequestAndResponseResult_MVR = null;
                                    while (MVR_ReportStatus != 2 && MVR_Counter < 4)
                                    {
                                        MVR_Counter++;
                                        System.Threading.Thread.Sleep(4000);
                                        ObjProc_Get_RequestAndResponseResult_MVR = ObjOrder.GetRequestAndResponseByOrderId(OrderId).Where(data => data.ProductCode.ToLower() == "mvr").ToList<Proc_Get_RequestAndResponseResult>();
                                        MVR_ReportStatus = ObjProc_Get_RequestAndResponseResult_MVR.Where(data => data.ProductCode.ToLower() == "mvr").FirstOrDefault().ReportStatus;
                                    }

                                    GetMvrResponse(ObjRequestAndResponseResults, TopScript);
                                }
                                else
                                {

                                    if (ObjRequestAndResponseResults.ResponseData.Contains("INVALID ACCOUNT NAME AND PASSWORD") || ObjRequestAndResponseResults.ResponseData.Contains("<request_status>1</request_status>") || ObjRequestAndResponseResults.ResponseData == "")
                                    {
                                        TopScript.Append("Your order was completed manually through Intelifi support department. Please contact support for any further assistance.");
                                    }
                                    else
                                    {
                                        TopScript.Append(ObjRequestAndResponseResults.ResponseData);
                                    }


                                }
                                ObjClsViewReport.XDocument = new XmlDocument();
                            }

                            #endregion

                            #region Fetch Response

                            else
                            {
                                string hdnReviewDetail = FetchResponse(ObjRequestAndResponseResults, ObjClsViewReport, TopScript, true, true, Role, ObjModel);


                                if (ObjModel.hdnReviewDetail == string.Empty || ObjModel.hdnReviewDetail == null)
                                {
                                    ObjModel.hdnReviewDetail = hdnReviewDetail;
                                }
                                else
                                {
                                    ObjModel.hdnReviewDetail = ObjModel.hdnReviewDetail + "," + hdnReviewDetail;
                                }
                            }

                            #endregion

                        }

                        #endregion

                        ObjClsViewReport.Starting = TopScript.ToString();

                        #region Review Status


                        string ApplicantFirstName = ObjRequestAndResponseResults.SearchedFirstName, ApplicantLastName = ObjRequestAndResponseResults.SearchedLastName;
                        string DisclaimerInfo = string.Empty;
                        //if (ObjRequestAndResponseResults.ReportStatus != 1)
                        //{
                        if (ObjRequestAndResponseResults.ProductCode.ToLower() == "scr" || ObjRequestAndResponseResults.ProductCode.ToLower() == "mvr" || ObjRequestAndResponseResults.ProductCode.ToLower() == "mvr")
                        {
                            int StateId = Convert.ToInt32(ObjRequestAndResponseResults.SearchedStateId);
                            int fkPerApplicationId = Convert.ToInt32(ObjRequestAndResponseResults.fkProductApplicationId.ToString());
                            BALGeneral objBalGeneral = new BALGeneral();
                            var list = objBalGeneral.GetDisclamierInfoForScrMvrWcb(fkPerApplicationId, StateId);
                            if (list.Count > 0)
                            {
                                DisclaimerInfo = list.ElementAt(0).DisclaimerData;
                            }

                        }


                        if (ObjRequestAndResponseResults.ProductCode.ToLower() == "ccr1" || ObjRequestAndResponseResults.ProductCode.ToLower() == "ccr1" || ObjRequestAndResponseResults.ProductCode.ToLower() == "ccr2")
                        {

                            int pkProductId = Convert.ToInt32(ObjRequestAndResponseResults.pkProductId.ToString());
                            int fkCountyId = Convert.ToInt32(ObjRequestAndResponseResults.fkCountyId);
                            BALGeneral objBalGeneral = new BALGeneral();
                            var list = objBalGeneral.GetDisclamierInfoForCcr1Ccr2Rcx(fkCountyId, pkProductId);
                            if (list.Count > 0)
                            {
                                DisclaimerInfo = list.ElementAt(0).DisclaimerData;
                            }
                        }

                        if (ObjRequestAndResponseResults.ProductCode.ToLower() == "fcr")
                        {
                            string CountyCode = ObjRequestAndResponseResults.CountyCode;
                            BALGeneral objBalGeneral = new BALGeneral();
                            var list = objBalGeneral.GetDisclamierInfoForFCR(CountyCode);
                            if (list.Count > 0)
                            {
                                DisclaimerInfo = list.ElementAt(0).DisclaimerData;
                            }
                        }

                        //}
                        DisclaimerScript = "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\"><tr><td>";


                        DisclaimerScript = DisclaimerScript + "<div class=\"sectionsummary\"><h3 style=\"line-height: 1em;\">" + DisclaimerInfo + "</h3></div>";


                        DisclaimerScript = DisclaimerScript + "</td></tr></table>";
                        BottomScript = "</td><td class=\"middle_right\" width=\"9px\"></td></tr></table></div><div id=\"" + ObjRequestAndResponseResults.ProductCode + "_f\" style=\"display: block;margin-left:12px;\"></div></div><br/>";
                        if (ObjRequestAndResponseResults.fkReportCategoryId != 2 && !ObjRequestAndResponseResults.ProductDisplayName.Contains("People Search"))//INT-172 As per Greg comments Help Button will not be displayed on PS section. 
                        {
                            EmergeAssistScript = "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\"><tr><td style=\"padding-left: 0%;\"> <a href=\"javascript:ReportReview('" + ObjRequestAndResponseResults.ProductDisplayName + "','" + ApplicantLastName + "','" + ApplicantFirstName + "' ,0,'" + ObjRequestAndResponseResults.pkOrderDetailId + "');\"><img style=\"height:28px;\" src='" + ApplicationPath.GetApplicationPath() + "content/themes/base/images/help-icon4.png' alt='help'/></a></td><td ><a style=\"font-family: Arial;display:block;font-size:12px;padding-left:10px;text-decoration:none;font-weight:bold;color:#000;\" href=\"javascript:ReportReview('" + ObjRequestAndResponseResults.ProductDisplayName + "','" + ApplicantLastName + "','" + ApplicantFirstName + "' ,0,'" + ObjRequestAndResponseResults.pkOrderDetailId + "');\"  > Help</a></td>";
                        }
                        else
                        {
                            EmergeAssistScript = "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\"><tr><td style=\"padding-left: 0%;\"></td>";
                        }
                        #region Check Product review status for location


                        byte ProductReviewStatus = 0;
                        try
                        {
                            BALEmergeReview ObjBALEmergeReview = new BALEmergeReview();
                            ProductReviewStatus = ObjBALEmergeReview.GetProductReviewStatusPerLocation(Convert.ToInt32(ObjRequestAndResponseResults.fkLocationId.ToString()), Convert.ToInt32(ObjRequestAndResponseResults.fkProductApplicationId.ToString()));
                            if (ProductReviewStatus == 0 && ObjRequestAndResponseResults.fkReportCategoryId == 2 && (ObjRequestAndResponseResults.ProductCode.ToLower() != "ncr3" || ObjRequestAndResponseResults.ProductCode.ToLower() != "rcx" || ObjRequestAndResponseResults.ProductCode.ToLower() != "sor"))
                            {
                                ProductReviewStatus = 1;
                            }
                        }
                        catch
                        {

                        }

                        #endregion


                        #endregion




                        string[] ProductsWithTextForReview = new string[] { "BS", "BR", "CDLIS", "ER", "DB", "DLS", "OIG", "PL", "RAL", "RPL", "MVR", "MVS", "EEI", "EBP", "ECR", "UCC", "EDV", "EMV", "PRV2", "PLV", "PRV", "NCR4", "SNS", "NCR2" };

                        string EmergeReviewSection = string.Empty;
                        if (ProductsWithTextForReview.Contains(ObjRequestAndResponseResults.ProductCode) && IsAdmin)
                        {
                            if (ObjModel.hdnIsTagOnPage == "")
                            {
                                ObjModel.hdnIsTagOnPage = ObjRequestAndResponseResults.ProductCode;
                            }
                            else
                            {
                                ObjModel.hdnIsTagOnPage = ObjModel.hdnIsTagOnPage + "," + ObjRequestAndResponseResults.ProductCode;
                            }

                            EmergeReviewSection = "<table style='width:100%' border=\"0\" cellpadding=\"2\" cellspacing=\"0\"><tr><td style='width:85%;'><textarea id='txt_" + ObjRequestAndResponseResults.pkOrderResponseId + "' rows=\"2\" style=\"width:100%;height:40px;background-color:#FFFFE1\">" + ObjRequestAndResponseResults.EmergeReviewNote + "</textarea></td><td align=\"right\" valign=\"bottom\" style=\"vertical-align:bottom;width:10%\"><input type=\"button\" id='btn_" + Convert.ToString(ObjRequestAndResponseResults.pkOrderResponseId) + "' value=\"Save Changes\" onclick=\"javascript:SetHiddenFieldsForOtherReviewReportsMVC('" + ObjRequestAndResponseResults.pkOrderResponseId + "')\" wrap=\"off\" style=\" height:30px; width:110px; text-align:center; vertical-align:middle\"/></td></tr></table>";

                        }
                        else if (ProductsWithTextForReview.Contains(ObjRequestAndResponseResults.ProductCode) && ObjRequestAndResponseResults.EmergeReviewNote != "")
                        {
                            EmergeReviewSection = "<table style='width:100%' border=\"0\" cellpadding=\"2\" cellspacing=\"0\"><tr><td class=\"ReviewNote\" style='width:85%;background-color:#FFFFE1'>" + ObjRequestAndResponseResults.EmergeReviewNote + "</td></tr></table>";

                        }

                        if (ObjRequestAndResponseResults.IsEnableReview != null)
                        {
                            //INT-116 for psp disable emerge review
                            if (ObjRequestAndResponseResults.fkReportCategoryId != 1 && ObjRequestAndResponseResults.ReviewStatus.ToString() != "1" && ObjRequestAndResponseResults.ProductCode != "MVR" && ObjRequestAndResponseResults.ProductCode != "PSP")
                            {
                                if (ObjRequestAndResponseResults.IsEnableReview == true && ProductReviewStatus > 0)
                                {
                                    EmergeAssistScript = EmergeAssistScript + " <td style=\"padding-left: 2%;\"><a href=\"javascript:ReportReview('" + ObjRequestAndResponseResults.ProductDisplayName + "','" + ApplicantLastName + "','" + ApplicantFirstName + "',1,'" + ObjRequestAndResponseResults.pkOrderDetailId + "');\"><img style=\"height:28px;\" src='" + ApplicationPath.GetApplicationPath() + "content/themes/base/images/emerge-review-button.png' title='Emerge Review'/></a></td><td><a style=\"font-family: Arial;display:block;font-size:12px;padding-left:10px;text-decoration:none;color:#000;\" href=\"javascript:ReportReview('" + ObjRequestAndResponseResults.ProductDisplayName + "','" + ApplicantLastName + "','" + ApplicantFirstName + "',1,'" + ObjRequestAndResponseResults.pkOrderDetailId + "');\"  > &nbsp;<b>Emerge Review /</b> Connect with an agent </a></td><td style='display:none'><textarea id=" + ObjRequestAndResponseResults.pkOrderDetailId + ">" + ObjRequestAndResponseResults.ReviewComments + " </textarea></td>";
                                }
                                else
                                {
                                    EmergeAssistScript = EmergeAssistScript + " <td style=\"padding-left: 2%;\"><a id='" + ObjRequestAndResponseResults.ProductDisplayName + "' href=\"javascript:ActivateReportsVR('" + ObjRequestAndResponseResults.ProductDisplayName + "');\"><img style=\"height:28px;\" src='" + ApplicationPath.GetApplicationPath() + "content/themes/base/images/emerge-review-button.png' alt='Enable Review' title='Send Request for Emerge Review'/></a></td><td><a style=\"font-family: Arial;display:block;font-size:12px;padding-left:10px;text-decoration:none;color:#000;\" href=\"javascript:ActivateReportsVR('" + ObjRequestAndResponseResults.ProductDisplayName + "');\"  > &nbsp;<b>Emerge Review /</b> Connect with an agent </a></td>";
                                }
                            }
                            else
                            {
                                EmergeAssistScript = EmergeAssistScript + " <td></td>";
                            }
                        }
                        EmergeAssistScript = EmergeAssistScript + "</tr></table>";

                        ObjClsViewReport.Ending = DisclaimerScript + EmergeReviewSection + EmergeAssistScript + BottomScript;


                        nameDiff++;

                        ObjViewReport.Add(ObjClsViewReport);


                        if (ObjRequestAndResponseResults.ReviewStatus != 0)
                        {
                            CountReviewReports++;
                            if (ObjRequestAndResponseResults.ReviewStatus == 1)
                            {
                                IsShowViewTab = true;
                            }
                        }
                    }

                    ObjModel.ObjViewReportString = ObjViewReport;

                    if (ObjModel.ReportType.Length > 0)
                    {
                        ObjModel.ReportType = ObjModel.ReportType.Substring(0, ObjModel.ReportType.Length - 2);
                    }
                    #endregion

                    #region Display Alert If Auto Review Is Sent


                    if (ObjModel.IsShowRawData == true)
                    {
                        ObjModel.AllowReviewReportTab = true;
                    }
                    else
                    {
                        //if (IsShowViewTab == true)
                        //{
                        //    ObjModel.AllowReviewReportTab = true;
                        //}
                        //else
                        //{
                        ObjModel.AllowReviewReportTab = false;
                        //}
                    }

                    #endregion

                    #region Reference Code and Notes

                    List<Proc_GetReferenceCodebyCompanyIdResult> ObjList = new List<Proc_GetReferenceCodebyCompanyIdResult>();
                    ObjList = ObjBALCompany.GetReferenceCodeListbypkCompanyId(Convert.ToInt32(ObjRequestAndResponseResult[0].fkCompanyId.ToString()));

                    ObjList.Insert(0, new Proc_GetReferenceCodebyCompanyIdResult { ReferenceCode = "Select Reference #", pkReferenceCodeId = -1 });
                    ObjList.Add(new Proc_GetReferenceCodebyCompanyIdResult { ReferenceCode = "------------------", pkReferenceCodeId = -2 });
                    ObjList.Add(new Proc_GetReferenceCodebyCompanyIdResult { ReferenceCode = "Add New", pkReferenceCodeId = -3 });
                    ObjModel.ReferenceCodeList = ObjList;
                    ObjModel.TrackingRefCodeValue = "NA";
                    var SearchedTrackingRef = ObjRequestAndResponseResult[0].SearchedTrackingRef == "" ? "NA" : ObjRequestAndResponseResult[0].SearchedTrackingRef;
                    if (SearchedTrackingRef != "NA" && SearchedTrackingRef != null)
                    {
                        var SelectedrefCode = ObjList.Where(rec => rec.ReferenceCode.ToLower().Trim() == SearchedTrackingRef.ToLower().Trim()).FirstOrDefault();
                        if (SelectedrefCode != null)
                        {
                            var TrackingRefCodeValue = SelectedrefCode.pkReferenceCodeId;
                            ObjModel.TrackingRefCodeValue = TrackingRefCodeValue.ToString();
                        }
                    }
                    ObjModel.ReferenceCode = ObjRequestAndResponseResult[0].SearchedTrackingRef == "" ? "NA" : ObjRequestAndResponseResult[0].SearchedTrackingRef;
                    ObjModel.Notes = ObjRequestAndResponseResult[0].SearchedTrackingNotes == "" ? "NA" : ObjRequestAndResponseResult[0].SearchedTrackingNotes;
                    ObjModel.fkCompanyId = ObjRequestAndResponseResult[0].fkCompanyId;
                    #endregion


                    #region Permissible Purpose
                    int? orderpermissibleid = 0;
                    orderpermissibleid = ObjRequestAndResponseResult[0].SearchedPermissibleId == null ? 0 : ObjRequestAndResponseResult[0].SearchedPermissibleId;
                    ObjModel.PermissiblePurposeId = Convert.ToInt32(orderpermissibleid);
                    BALCompanyType serboj = new BALCompanyType();
                    List<tblPermissiblePurpose> PermissiblePurpose = serboj.GetPermissiblePurpose();
                    ObjModel.PermissiblePurposeList = PermissiblePurpose.Select(x => new SelectListItem { Text = x.PermissiblePurpose, Value = x.pkPermissibleId.ToString() }).ToList();
                    ObjModel.PermissiblePurposeList = ObjModel.PermissiblePurposeList.Select(x => new SelectListItem { Text = x.Text, Value = x.Value.ToString(), Selected = (x.Value == ObjModel.PermissiblePurposeId.ToString()) }).ToList();
                    ObjModel.PermissiblePurposeValue = PermissiblePurpose.Where(x => x.pkPermissibleId == (ObjModel.PermissiblePurposeId == 0 ? 1 : ObjModel.PermissiblePurposeId)).Select(x => x.PermissiblePurpose).FirstOrDefault().ToString();
                    #endregion


                    #region Package infomation
                    using (EmergeDALDataContext _EmergeDALDataContext = new EmergeDALDataContext())
                    {
                        string displayPckName = string.Empty;
                        bool flagPackages = false;
                        char[] trimch = new char[1];
                        trimch[0] = ',';

                        var list = _EmergeDALDataContext.proc_GetOrderPackage(OrderId).ToList();
                        if (list != null)
                        {
                            foreach (var ch in list)
                            {
                                displayPckName = displayPckName + ch.tblProductPackages_PackageName + ",";
                                flagPackages = true;
                            }
                            ObjModel.PackageName = displayPckName.Trim(trimch);
                            if (flagPackages)
                            {
                                ObjModel.PackageDisplayNameStatus = true;
                            }
                        }
                    }
                    #endregion
                }

            }
            #endregion
        }


        public void GetViewReportReview(ViewReportModel ObjModel, int OrderId, bool IsAdmin, int ReviewQueryString)
        {
            #region 6 Months Old

            // VRR1.Orderid = Request.QueryString["_O"].ToString();
            ObjModel.hdnIs6MonthOld = false;
            BALGeneral ObjBALGeneral = new BALGeneral();
            ObjModel.hdnIs6MonthOld = ObjBALGeneral.Is6MonthOld(OrderId);
            ObjModel.IsShowRawData = false;


            #endregion

            #region Request/Response



            BALOrders ObjOrder = new BALOrders();

            var ObjRequestAndResponseResult = ObjOrder.GetRequestAndResponseByOrderId(OrderId);
            if (ObjRequestAndResponseResult.Count > 0)
            {
                #region Applicant Info

                ObjModel.ApplicantDob = ObjRequestAndResponseResult.ElementAt(0).SearchedDob;
                ObjModel.ApplicantState = ObjRequestAndResponseResult.ElementAt(0).SearchedStateCode;
                ObjModel.OrderNo = ObjRequestAndResponseResult.ElementAt(0).OrderNo;
                ObjModel.ApplicantNameForEmail = ObjRequestAndResponseResult[0].SearchedLastName + "_" + ObjRequestAndResponseResult[0].SearchedFirstName;
                if (ObjRequestAndResponseResult.ElementAt(0).CompletionDate != null)
                {
                    ObjModel.CompletionDate = Convert.ToDateTime(ObjRequestAndResponseResult.ElementAt(0).CompletionDate).ToString("MM/dd/yyyy HH:mm:ss");
                }
                ObjModel.OrderDt = ObjRequestAndResponseResult.ElementAt(0).OrderDt.ToString("MM/dd/yyyy HH:mm:ss");
                string SearchedName = GenerateApplicantName(ObjRequestAndResponseResult[0].SearchedLastName, ObjRequestAndResponseResult[0].SearchedSuffix, ObjRequestAndResponseResult[0].SearchedMiddleInitial, ObjRequestAndResponseResult[0].SearchedFirstName);
                string FullSearchedName = SearchedName.Replace("'", "");
                ObjModel.ApplicantName = FullSearchedName;
                ObjModel.forReportLocation = new BALLocation().GetLocationDetailByLocationId(Convert.ToInt32(ObjRequestAndResponseResult[0].fkLocationId)).Select(loc => loc.City).FirstOrDefault();//INT343

                #region Mailing Address
                string mailingAddress = "";

                if (ObjRequestAndResponseResult[0].SearchedStreetAddress != null || ObjRequestAndResponseResult[0].SearchedStreetName != null)
                {
                    mailingAddress = ObjRequestAndResponseResult[0].SearchedStreetAddress != string.Empty ? ObjRequestAndResponseResult[0].SearchedStreetAddress + "\n" : string.Empty;
                    mailingAddress = ObjRequestAndResponseResult[0].SearchedStreetName != string.Empty ? mailingAddress + ObjRequestAndResponseResult[0].SearchedStreetName + "\n" : mailingAddress;
                }
                mailingAddress = ObjRequestAndResponseResult[0].SearchedStreetType != string.Empty ? mailingAddress + ObjRequestAndResponseResult[0].SearchedStreetType + ", " : mailingAddress;
                mailingAddress = ObjRequestAndResponseResult[0].SearchedApt != string.Empty ? mailingAddress + ObjRequestAndResponseResult[0].SearchedApt + ", " : mailingAddress;
                mailingAddress = ObjRequestAndResponseResult[0].SearchedCity != string.Empty ? mailingAddress + ObjRequestAndResponseResult[0].SearchedCity : mailingAddress;

                if (mailingAddress != string.Empty)
                {
                    mailingAddress = ObjRequestAndResponseResult[0].SearchedStateCode != string.Empty ? mailingAddress + "\n" + ObjRequestAndResponseResult[0].SearchedStateCode : mailingAddress;
                    mailingAddress = ObjRequestAndResponseResult[0].SearchedZipCode != string.Empty ? mailingAddress + " " + ObjRequestAndResponseResult[0].SearchedZipCode : mailingAddress;
                }
                ObjModel.ApplicantAddress = mailingAddress;
                ObjModel.IsAddressUpdated = ObjRequestAndResponseResult[0].IsAddressUpdated;
                #endregion

                #endregion

                #region Company Info

                BALCompany ObjBALCompany = new BALCompany();
                List<Proc_Get_CompanyDetailByMemberShipIdResult> ObjCompanyDetails = ObjBALCompany.GetCompanyDetailByMemberShipIdResult(ObjRequestAndResponseResult.ElementAt(0).fkUserId);
                if (ObjCompanyDetails.Count > 0)
                {
                    ObjModel.CompanyUser = ObjCompanyDetails.First().lastname + ", " + ObjCompanyDetails.First().firstname;
                    ObjModel.CompanyLocation = ObjCompanyDetails.First().city + ", " + ObjCompanyDetails.First().statecode;
                    ObjModel.CompanyName = ObjCompanyDetails.First().CompanyName;
                    ObjModel.CompanyNameAdverse = ObjCompanyDetails.First().CompanyName;
                    ObjModel.IsReportStatus = ObjCompanyDetails.First().IsReportStatus;
                }


                MembershipUser Member = Membership.GetUser();//User.Identity.Name
                if (Member != null)
                {
                    string[] Role = Roles.GetRolesForUser(Member.Email);
                    int LoginCompanyId = 0;
                    int OrderCompanyId = 0;

                    Guid UserId = new Guid(Member.ProviderUserKey.ToString());
                    // Guid UserId = new Guid("fdd976e2-222f-47c9-b181-68e5e15ea358");
                    List<Proc_Get_CompanyDetailByMemberShipIdResult> ObjRootCompanyDetails = ObjBALCompany.GetCompanyDetailByMemberShipIdResult(UserId);
                    {
                        //Guid RootCompanyId = new Guid("7DC87D77-6BB5-4BB3-9837-FA861F36F281");
                        //int RootCompanyId = 9514;
                        int RootCompanyId = 9831;
                        #region Emerge review Pop details

                        //email id for emerge review message
                        ObjModel.CurrentUserEmail = Member.Email;
                        ObjModel.lblToRR = ConfigurationManager.AppSettings["ReportReviewMail"].ToString();

                        //email id for asking admin to activate report
                        ObjModel.lblMailto = BALGeneral.GetGeneralSettings().SupportEmailId;
                        ObjModel.lblMailFrom = Member.Email;
                        ObjModel.txtMessage = Member.UserName + " is requesting to activate the Emerge Review feature ##report## ";




                        #endregion
                        if (ObjRootCompanyDetails.First().PkCompanyId != 0)
                        {
                            LoginCompanyId = ObjRootCompanyDetails.First().PkCompanyId;
                            OrderCompanyId = (int)ObjRequestAndResponseResult[0].fkCompanyId;

                            if (RootCompanyId == ObjRootCompanyDetails.First().PkCompanyId)
                            {
                                ObjModel.CompanyPhone = ObjCompanyDetails.First().CompanyPhone.ToString();
                                ObjModel.LocationPhone = ObjCompanyDetails.First().LocationPhone.ToString();
                                ObjModel.UserPhone = ObjCompanyDetails.First().UserPhone.ToString();
                                ObjModel.EmailAddress = ObjCompanyDetails.First().EmailAddress.ToString();
                                ObjModel.IsRootCompanyId = true;
                            }
                            else
                            {
                                ObjModel.IsRootCompanyId = false;
                            }
                        }
                    }

                    if (ObjCompanyDetails.Count > 0)
                    {
                        if (Role.Contains("SystemAdmin") || Role.Contains("SupportTechnician") || Role.Contains("SupportManager"))
                        {
                            using (EmergeDALDataContext dx = new EmergeDALDataContext())
                            {
                                ObjModel.IsShowRawData = true;
                                //Get home company location based on JIRA ticket INT-325 as per Greg comments.
                                var homeCompanyLocation = dx.tblLocations.Where(x => x.fkCompanyId == ObjRequestAndResponseResult.First().fkCompanyId && x.IsHome == true).FirstOrDefault();
                                string homeLocation = string.Empty;
                                if (homeCompanyLocation != null)
                                {
                                    //Set home location.
                                    homeLocation = Convert.ToString(homeCompanyLocation.pkLocationId);
                                }
                                else
                                {
                                    //If query is not able to get home location than location will be the exact location where selected order has created.
                                    homeLocation = ObjRequestAndResponseResult.First().fkLocationId.ToString();
                                }
                                var v = dx.tblCompanyUsers.Where(d => d.fkUserId == ObjRequestAndResponseResult.First().fkUserId).Select(d => new { d.pkCompanyUserId }).FirstOrDefault();
                                if (v != null)
                                {
                                    ObjModel.CompanyUser = "<a title=\"Click here to edit User\"  href=" + ApplicationPath.GetApplicationPath() + "Control/AddCompanyUser?_a=" + v.pkCompanyUserId.ToString() + ">" + ObjCompanyDetails.First().lastname + ", " + ObjCompanyDetails.First().firstname + "</a>";
                                }
                                else
                                {
                                    ObjModel.CompanyUser = "<a title=\"Click here to edit User\"  href=" + ApplicationPath.GetApplicationPath() + "Control/AddCompanyUser?_a=" + Guid.Empty + ">" + ObjCompanyDetails.First().lastname + ", " + ObjCompanyDetails.First().firstname + "</a>";
                                }
                                ObjModel.CompanyLocation = "<a title=\"Click here to edit Location\"   href=" + ApplicationPath.GetApplicationPath() + "Control/AddCompanyLocation?lid=" + ObjRequestAndResponseResult.First().fkLocationId.ToString() + ">" + ObjCompanyDetails.First().city + ", " + ObjCompanyDetails.First().statecode + "</a>";
                                ObjModel.CompanyName = "<a title=\"Click here to edit Company\"    href=" + ApplicationPath.GetApplicationPath() + "Control/AddCompanies?Id1=" + homeLocation + ">" + ObjCompanyDetails.First().CompanyName + "</a>";
                                ObjModel.CompanyNameAdverse = ObjCompanyDetails.First().CompanyName;
                                ObjModel.IsReportStatus = ObjCompanyDetails.First().IsReportStatus;

                                //int-198
                                ObjModel.CompanyNote = string.IsNullOrEmpty(ObjCompanyDetails.First().CompnayNote) ? "NA" : ObjCompanyDetails.First().CompnayNote;
                                ObjModel.LocationNote = string.IsNullOrEmpty(ObjCompanyDetails.First().LocationNote) ? "NA" : ObjCompanyDetails.First().LocationNote;
                                ObjModel.UserNote = string.IsNullOrEmpty(ObjCompanyDetails.First().UserNote) ? "NA" : ObjCompanyDetails.First().UserNote;
                                //--------
                            }
                        }
                        else
                        {
                            ObjModel.IsShowRawData = false;
                            ObjModel.CompanyUser = ObjCompanyDetails.First().lastname + ", " + ObjCompanyDetails.First().firstname;
                            ObjModel.CompanyLocation = ObjCompanyDetails.First().city + ", " + ObjCompanyDetails.First().statecode;
                            ObjModel.CompanyName = ObjCompanyDetails.First().CompanyName;
                            ObjModel.CompanyNameAdverse = ObjCompanyDetails.First().CompanyName;
                            ObjModel.IsReportStatus = ObjCompanyDetails.First().IsReportStatus;

                            //int-198
                            ObjModel.CompanyNote = string.IsNullOrEmpty(ObjCompanyDetails.First().CompnayNote) ? "NA" : ObjCompanyDetails.First().CompnayNote;
                            ObjModel.LocationNote = string.IsNullOrEmpty(ObjCompanyDetails.First().LocationNote) ? "NA" : ObjCompanyDetails.First().LocationNote;
                            ObjModel.UserNote = string.IsNullOrEmpty(ObjCompanyDetails.First().UserNote) ? "NA" : ObjCompanyDetails.First().UserNote;
                            //--------
                        }
                    }

                #endregion

                    #region Display Disclamer


                    ObjModel.Disclamer = DisplayDisclaimer();


                    #endregion

                    #region Product Code

                    int nameDiff = 0;
                    string BottomScript = string.Empty;
                    string EmergeAssistScript = string.Empty;
                    string DisclaimerScript = string.Empty;
                    int MVR_Counter = 0; int CountReviewReports = 0;
                    bool IsShowViewTab = false;
                    // BALEmergeReview ObjBALEmergeReview = new BALEmergeReview();


                    var ObjProc_Get_RequestAndResponseResultList = ObjRequestAndResponseResult.OrderByDescending(d => d.ProductCode);
                    List<ClsViewReport> ObjViewReport = new List<ClsViewReport>();
                    foreach (Proc_Get_RequestAndResponseResult ObjRequestAndResponseResults in ObjProc_Get_RequestAndResponseResultList)
                    {
                        //ObjRequestAndResponseResults.ResponseData = ObjRequestAndResponseResults.ResponseData.Replace(@"&#10;", "\n");
                        //  ObjRequestAndResponseResults.ResponseData = ObjRequestAndResponseResults.ResponseData;
                        StringBuilder TopScript = new StringBuilder();
                        ClsViewReport ObjClsViewReport = new ClsViewReport();
                        #region Report Type

                        string DisplyProductCode = "";
                        if (ObjRequestAndResponseResults.ProductCode.Contains("CCR") && ObjRequestAndResponseResults.IsLiveRunner == true)
                        {
                            DisplyProductCode = ObjRequestAndResponseResults.ProductCode + "*";
                        }
                        else
                        {
                            DisplyProductCode = ObjRequestAndResponseResults.ProductCode;
                        }
                        ObjModel.ReportType += DisplyProductCode + ", ";

                        if ((LoginCompanyId == OrderCompanyId) && (ObjRequestAndResponseResults.IsViewed == false) && (ObjRequestAndResponseResults.ReportStatus == 2))
                        {
                            BALGeneral objGeneral = new BALGeneral();
                            objGeneral.UpdateOrderDetail_IsViewed(ObjRequestAndResponseResults.pkOrderDetailId);     // Update Isviewed for order report
                        }

                        #endregion

                        #region Live Runner

                        string LiveRunnerImagePath = ApplicationPath.GetApplicationPath() + "Content/themes/base/images/LiveRunnerSmall.png";

                        string IsLiveRunnerProvided = "";
                        if (ObjRequestAndResponseResults.IsLiveRunner == true)
                        {
                            IsLiveRunnerProvided = "<table><tr><td >Provided By</td><td><img src='" + LiveRunnerImagePath + "' alt='Live Runner' height='13' style='border:0px'/></td></tr></table>";
                        }
                        else
                        {
                            IsLiveRunnerProvided = "";
                        }

                        #endregion

                        #region SCR STATE COUNTY

                        string SCRState = "";
                        if (ObjRequestAndResponseResults.ProductCode.ToLower() == "scr")
                        {
                            SCRState = "[" + ObjRequestAndResponseResults.StateName + "]";
                        }

                        string StateCounty = "";
                        string StateCode = "";
                        string CountyName = ObjRequestAndResponseResults.CountyCode;
                        if (CountyName != "")
                        {
                            StateCode = ObjRequestAndResponseResults.StateCodeReport;
                            if (StateCode != "")
                            {
                                StateCounty = "[" + CountyName + ", " + StateCode + "]";
                            }
                        }

                        #endregion


                        #region Top Report Header

                        TopScript.Append("<div class=\"main-box\" ><div onclick=\"collapse('" + ObjRequestAndResponseResults.ProductCode + "_" + nameDiff.ToString() + "');\">"
                       + "<table class=\"rv-header\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse\">"
                      + "<tr><td style=\"background-repeat: no-repeat; background-image: url('../content/themes/base/images/" + ObjRequestAndResponseResults.ProductDisplayName + ".png');  background-position: 30px;cursor: pointer;width: 8% !important;color: transparent;  \"  width=\"39px\" height=\"34px\"></td><td class=\"rv-header\" width=\"95%\">"
                       + "<table style='font-weight:bold; width:100%'><tbody><tr><td style=\"vertical-align: middle; width:50%\" align='left'>" + ObjRequestAndResponseResults.ProductDisplayName + " " + SCRState + StateCounty + "</td>"
                       + "<td style=\"vertical-align: middle; width:50%\" align='right'><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>" + IsLiveRunnerProvided + "</td><td></td></tr> </table> </td></tr></tbody></table>"
                       + "</td><td class=\"rv-header\" width=\"9px\"></td></tr></table></div><div id=\"" + ObjRequestAndResponseResults.ProductCode + "_" + nameDiff.ToString() + "_b\" style=\"display: block; margin-left: 12px;\"  class=\"pad-bot10\">"
                       + "<table  width=\"98%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td class=\"middle_left\" width=\"14px\"></td><td  width=\"100%\">");



                        if (ObjRequestAndResponseResults.ProductCode.ToLower() == "scr")
                        {
                            TopScript.Append("<div style='font-size:14px;padding-left: 2%;; font-family:arial;color:#666;'><b>State Searched :</b> " + ObjRequestAndResponseResults.StateName + "</div>");
                            TopScript.Append("<div style='font-size:14px;padding-left: 2%;; font-family:arial;color:#666;'><b>Name :</b> " + FullSearchedName + "</div>");
                        }
                        if (ObjRequestAndResponseResults.ProductCode.ToLower() == "fcr" && (ObjRequestAndResponseResults.IsTieredReport == true || ObjRequestAndResponseResults.IsOptionalReport == true))
                        {
                            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                            {

                                TopScript.Append("<div class='people-search-wrap'><div style='font-size:14px;padding-left: 5px; color:#666; font-family:arial'><b>Name :</b> " + FullSearchedName + " </br> <b>Jurisdiction :</b> " + ObjRequestAndResponseResults.LiveRunnerCounty + "</div></div>");
                                //TopScript.Append("<div style='font-size:14px; color:#666; font-family:arial'><b>Name :</b> " + FullSearchedName + " | <b>Jurisdiction :</b> " + Dx.tblJurisdictions.FirstOrDefault(n => n.pkJurisdictionId == Convert.ToInt32(ObjRequestAndResponseResults.SearchedCountyId)).JurisdictionName + "</div>");
                            }
                        }
                        else if (ObjRequestAndResponseResults.ProductCode.ToLower() == "fcr" && ObjRequestAndResponseResults.IsTieredReport == false && ObjRequestAndResponseResults.IsOptionalReport == false)
                        {
                            using (EmergeDALDataContext Dx = new EmergeDALDataContext())
                            {
                                //commented for ticket #21
                                //TopScript.Append("<div style='font-size:14px; color:#666; font-family:arial'><b>Name :</b> " + FullSearchedName + " | <b>Jurisdiction :</b> " + Dx.tblJurisdictions.FirstOrDefault(n => n.pkJurisdictionId == Convert.ToInt32(ObjRequestAndResponseResults.SearchedCountyId)).JurisdictionName + "</div>");
                                TopScript.Append("<div class='people-search-wrap'><div style='font-size:14px;padding-left: 5px; color:#666; font-family:arial'><b>Name :</b> " + FullSearchedName + " </br> <b>Jurisdiction :</b> " + ObjRequestAndResponseResults.CountyCode + "</div></div>");
                            }
                        }
                        string ClientReviewComments = ObjRequestAndResponseResults.ReviewComments;
                        if (ObjRequestAndResponseResults.ReviewStatus != 0 && ClientReviewComments != "")
                        {
                            if (Role.Contains("SystemAdmin") || Role.Contains("SupportTechnician") || Role.Contains("SupportManager"))
                            {
                                TopScript.Append("<div class=\"ReviewNote\"><b>Comments: </b>" + ClientReviewComments + "</div>");
                            }
                        }

                        #endregion

                        #region Fetch Report
                        //if (ObjRequestAndResponseResults.OrderStatus == 4 && ObjRequestAndResponseResults.ProductCode == "NCR1")
                        //{
                        //    TopScript.Append(DisplayNoResponseMessage());
                        //}

                        if (ObjRequestAndResponseResults.ReportStatus.ToString() == "3")
                        {
                            if (ObjRequestAndResponseResults.ProductCode == "PSP")
                            {
                                if (ObjRequestAndResponseResults.ResponseData != "")
                                {
                                    XDocument erroXML = XDocument.Parse(ObjRequestAndResponseResults.ErrorLog);
                                    var errorMessage = erroXML.Descendants("StatusDescription").FirstOrDefault().Value.ToString();
                                    TopScript.Append(DisplayInCompleteMessageForPSP(errorMessage));
                                }
                                else
                                {
                                    TopScript.Append(DisplayInCompleteMessage());
                                }
                            }
                            else
                            {
                                TopScript.Append(DisplayInCompleteMessage());
                            }
                            BALOrders ObjBALOrders = new BALOrders();
                            ObjBALOrders.UpdateReportStatus_Ret(ObjRequestAndResponseResults.fkOrderId, ObjRequestAndResponseResults.pkOrderDetailId, 3);
                        }
                        else if (ObjRequestAndResponseResults.fkReportCategoryId == 2 && ObjRequestAndResponseResults.OrderStatus == 4 && ObjModel.IsShowRawData == false && ObjRequestAndResponseResults.ReviewStatus != 2)
                        { // #282
                            TopScript.Append(DisplayNoResponseMessage());
                        }
                        else if (ObjRequestAndResponseResults.ReportStatus.ToString() == "2" && (ObjRequestAndResponseResults.ManualResponse != string.Empty || ObjRequestAndResponseResults.ResponseFileName != string.Empty))
                        {
                            TopScript.Append(DisplayManualResponse(ObjRequestAndResponseResults));
                        }
                        //else if (ObjRequestAndResponseResults.ReportStatus.ToString() == "2" && ObjRequestAndResponseResults.ReviewStatus.ToString() == "1" && ObjModel.IsShowRawData == false && ObjRequestAndResponseResults.ProductCode != "PS")//INT-456 Auto review 
                        else if (ObjRequestAndResponseResults.ReportStatus.ToString() == "2" && ObjRequestAndResponseResults.ReviewStatus.ToString() == "1" && ObjModel.IsShowRawData == false && (Convert.ToInt32(Session["CriminalCaseCountResponse"]) > 0 && ObjRequestAndResponseResults.ProductCode != "PS"))//INT-456 Auto review 
                        {
                            TopScript.Append(DisplayInReviewMessage());
                        }


                        else
                        {
                            #region Check For MVR

                            //This code will not be executed because new xml integration.
                            //if (ObjRequestAndResponseResults.ProductCode == "MVR1")
                            if (ObjRequestAndResponseResults.ProductCode == "This function is not in use.")
                            {
                                if (IsAdmin == true)//Request.Url.AbsoluteUri.ToLower().Contains("control")) //&& ObjRequestAndResponseResult.ReviewStatus != 0 //03Jun2011//Page.Request.Url.AbsoluteUri.ToLower().Contains("admin") ||
                                {
                                    if (ObjModel.hdnReviewDetail == "")
                                    {
                                        ObjModel.hdnReviewDetail = ObjRequestAndResponseResults.pkOrderDetailId + "_" + ObjRequestAndResponseResults.ProductCode + "_" + ObjRequestAndResponseResults.pkOrderResponseId;
                                    }
                                    else
                                    {
                                        ObjModel.hdnReviewDetail = ObjModel.hdnReviewDetail + "," + ObjRequestAndResponseResults.pkOrderDetailId + "_" + ObjRequestAndResponseResults.ProductCode + "_" + ObjRequestAndResponseResults.pkOrderResponseId;
                                    }
                                }

                                int MVR_ReportStatus = ObjRequestAndResponseResults.ReportStatus;
                                if (MVR_ReportStatus != 2)
                                {
                                    List<Proc_Get_RequestAndResponseResult> ObjProc_Get_RequestAndResponseResult_MVR = null;
                                    while (MVR_ReportStatus != 2 && MVR_Counter < 4)
                                    {
                                        MVR_Counter++;
                                        System.Threading.Thread.Sleep(4000);
                                        ObjProc_Get_RequestAndResponseResult_MVR = ObjOrder.GetRequestAndResponseByOrderId(OrderId).Where(data => data.ProductCode.ToLower() == "mvr").ToList<Proc_Get_RequestAndResponseResult>();
                                        MVR_ReportStatus = ObjProc_Get_RequestAndResponseResult_MVR.Where(data => data.ProductCode.ToLower() == "mvr").FirstOrDefault().ReportStatus;
                                    }

                                    GetMvrResponse(ObjRequestAndResponseResults, TopScript);
                                }
                                else
                                {

                                    if (ObjRequestAndResponseResults.ResponseData.Contains("INVALID ACCOUNT NAME AND PASSWORD") || ObjRequestAndResponseResults.ResponseData.Contains("<request_status>1</request_status>") || ObjRequestAndResponseResults.ResponseData == "")
                                    {
                                        TopScript.Append("Your order was completed manually through Intelifi support department. Please contact support for any further assistance.");
                                    }
                                    else
                                    {
                                        TopScript.Append(ObjRequestAndResponseResults.ResponseData);
                                    }


                                }
                                ObjClsViewReport.XDocument = new XmlDocument();
                            }

                            #endregion

                            #region Fetch Response

                            else
                            {
                                string hdnReviewDetail = FetchResponse(ObjRequestAndResponseResults, ObjClsViewReport, TopScript, true, true, Role, ObjModel);


                                if (ObjModel.hdnReviewDetail == string.Empty || ObjModel.hdnReviewDetail == null)
                                {
                                    ObjModel.hdnReviewDetail = hdnReviewDetail;
                                }
                                else
                                {
                                    ObjModel.hdnReviewDetail = ObjModel.hdnReviewDetail + "," + hdnReviewDetail;
                                }
                            }

                            #endregion

                        }

                        #endregion

                        ObjClsViewReport.Starting = TopScript.ToString();

                        #region Review Status


                        string ApplicantFirstName = ObjRequestAndResponseResults.SearchedFirstName, ApplicantLastName = ObjRequestAndResponseResults.SearchedLastName;
                        string DisclaimerInfo = string.Empty;
                        //if (ObjRequestAndResponseResults.ReportStatus != 1)
                        //{
                        if (ObjRequestAndResponseResults.ProductCode.ToLower() == "scr" || ObjRequestAndResponseResults.ProductCode.ToLower() == "mvr" || ObjRequestAndResponseResults.ProductCode.ToLower() == "mvr")
                        {
                            int StateId = Convert.ToInt32(ObjRequestAndResponseResults.SearchedStateId);
                            int fkPerApplicationId = Convert.ToInt32(ObjRequestAndResponseResults.fkProductApplicationId.ToString());
                            BALGeneral objBalGeneral = new BALGeneral();
                            var list = objBalGeneral.GetDisclamierInfoForScrMvrWcb(fkPerApplicationId, StateId);
                            if (list.Count > 0)
                            {
                                DisclaimerInfo = list.ElementAt(0).DisclaimerData;
                            }

                        }


                        if (ObjRequestAndResponseResults.ProductCode.ToLower() == "ccr1" || ObjRequestAndResponseResults.ProductCode.ToLower() == "ccr1" || ObjRequestAndResponseResults.ProductCode.ToLower() == "ccr2")
                        {

                            int pkProductId = Convert.ToInt32(ObjRequestAndResponseResults.pkProductId.ToString());
                            int fkCountyId = Convert.ToInt32(ObjRequestAndResponseResults.fkCountyId);
                            BALGeneral objBalGeneral = new BALGeneral();
                            var list = objBalGeneral.GetDisclamierInfoForCcr1Ccr2Rcx(fkCountyId, pkProductId);
                            if (list.Count > 0)
                            {
                                DisclaimerInfo = list.ElementAt(0).DisclaimerData;
                            }
                        }

                        if (ObjRequestAndResponseResults.ProductCode.ToLower() == "fcr")
                        {
                            string CountyCode = ObjRequestAndResponseResults.CountyCode;
                            BALGeneral objBalGeneral = new BALGeneral();
                            var list = objBalGeneral.GetDisclamierInfoForFCR(CountyCode);
                            if (list.Count > 0)
                            {
                                DisclaimerInfo = list.ElementAt(0).DisclaimerData;
                            }
                        }

                        //}
                        DisclaimerScript = "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\"><tr><td>";


                        DisclaimerScript = DisclaimerScript + "<div class=\"sectionsummary\"><h3 style=\"line-height: 1em;\">" + DisclaimerInfo + "</h3></div>";


                        DisclaimerScript = DisclaimerScript + "</td></tr></table>";
                        BottomScript = "</td><td class=\"middle_right\" width=\"9px\"></td></tr></table></div><div id=\"" + ObjRequestAndResponseResults.ProductCode + "_f\" style=\"display: block;margin-left:12px;\"></div></div><br/>";
                        if (ObjRequestAndResponseResults.fkReportCategoryId != 2 && !ObjRequestAndResponseResults.ProductDisplayName.Contains("People Search"))//INT-172 As per Greg comments Help Button will not be displayed on PS section. 
                        {
                            EmergeAssistScript = "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\"><tr><td style=\"padding-left: 0%;\"> <a href=\"javascript:ReportReview('" + ObjRequestAndResponseResults.ProductDisplayName + "','" + ApplicantLastName + "','" + ApplicantFirstName + "' ,0,'" + ObjRequestAndResponseResults.pkOrderDetailId + "');\"><img style=\"height:28px;\" src='" + ApplicationPath.GetApplicationPath() + "content/themes/base/images/help-icon4.png' alt='help'/></a></td><td ><a style=\"font-family: Arial;display:block;font-size:12px;padding-left:10px;text-decoration:none;font-weight:bold;color:#000;\" href=\"javascript:ReportReview('" + ObjRequestAndResponseResults.ProductDisplayName + "','" + ApplicantLastName + "','" + ApplicantFirstName + "' ,0,'" + ObjRequestAndResponseResults.pkOrderDetailId + "');\"  > Help</a></td>";
                        }
                        else
                        {
                            EmergeAssistScript = "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\"><tr><td style=\"padding-left: 0%;\"></td>";
                        }
                        #region Check Product review status for location


                        byte ProductReviewStatus = 0;
                        try
                        {
                            BALEmergeReview ObjBALEmergeReview = new BALEmergeReview();
                            ProductReviewStatus = ObjBALEmergeReview.GetProductReviewStatusPerLocation(Convert.ToInt32(ObjRequestAndResponseResults.fkLocationId.ToString()), Convert.ToInt32(ObjRequestAndResponseResults.fkProductApplicationId.ToString()));
                            if (ProductReviewStatus == 0 && ObjRequestAndResponseResults.fkReportCategoryId == 2 && (ObjRequestAndResponseResults.ProductCode.ToLower() != "ncr3" || ObjRequestAndResponseResults.ProductCode.ToLower() != "rcx" || ObjRequestAndResponseResults.ProductCode.ToLower() != "sor"))
                            {
                                ProductReviewStatus = 1;
                            }
                        }
                        catch
                        {

                        }

                        #endregion

                        #endregion




                        string[] ProductsWithTextForReview = new string[] { "BS", "BR", "CDLIS", "ER", "DB", "DLS", "OIG", "PL", "RAL", "RPL", "MVR", "MVS", "EEI", "EBP", "ECR", "UCC", "EDV", "EMV", "PRV2", "PLV", "PRV", "NCR4", "SNS", "NCR2" };

                        string EmergeReviewSection = string.Empty;
                        if (ProductsWithTextForReview.Contains(ObjRequestAndResponseResults.ProductCode) && IsAdmin)
                        {
                            if (ObjModel.hdnIsTagOnPage == "")
                            {
                                ObjModel.hdnIsTagOnPage = ObjRequestAndResponseResults.ProductCode;
                            }
                            else
                            {
                                ObjModel.hdnIsTagOnPage = ObjModel.hdnIsTagOnPage + "," + ObjRequestAndResponseResults.ProductCode;
                            }

                            EmergeReviewSection = "<table style='width:100%' border=\"0\" cellpadding=\"2\" cellspacing=\"0\"><tr><td style='width:85%;'><textarea id='txt_" + ObjRequestAndResponseResults.pkOrderResponseId + "' rows=\"2\" style=\"width:100%;height:40px;background-color:#FFFFE1\">" + ObjRequestAndResponseResults.EmergeReviewNote + "</textarea></td><td align=\"right\" valign=\"bottom\" style=\"vertical-align:bottom;width:10%\"><input type=\"button\" id='btn_" + Convert.ToString(ObjRequestAndResponseResults.pkOrderResponseId) + "' value=\"Save Changes\" onclick=\"javascript:SetHiddenFieldsForOtherReviewReportsMVC('" + ObjRequestAndResponseResults.pkOrderResponseId + "')\" wrap=\"off\" style=\" height:30px; width:110px; text-align:center; vertical-align:middle\"/></td></tr></table>";

                        }
                        else if (ProductsWithTextForReview.Contains(ObjRequestAndResponseResults.ProductCode) && ObjRequestAndResponseResults.EmergeReviewNote != "")
                        {
                            EmergeReviewSection = "<table style='width:100%' border=\"0\" cellpadding=\"2\" cellspacing=\"0\"><tr><td class=\"ReviewNote\" style='width:85%;background-color:#FFFFE1'>" + ObjRequestAndResponseResults.EmergeReviewNote + "</td></tr></table>";

                        }

                        if (ObjRequestAndResponseResults.IsEnableReview != null)
                        {
                            //INT-116 for psp disable emerge review
                            if (ObjRequestAndResponseResults.fkReportCategoryId != 1 && ObjRequestAndResponseResults.ReviewStatus.ToString() != "1" && ObjRequestAndResponseResults.ProductCode != "MVR" && ObjRequestAndResponseResults.ProductCode != "PSP")
                            {
                                if (ObjRequestAndResponseResults.IsEnableReview == true && ProductReviewStatus > 0)
                                {
                                    EmergeAssistScript = EmergeAssistScript + " <td style=\"padding-left: 2%;\"><a href=\"javascript:ReportReview('" + ObjRequestAndResponseResults.ProductDisplayName + "','" + ApplicantLastName + "','" + ApplicantFirstName + "',1,'" + ObjRequestAndResponseResults.pkOrderDetailId + "');\"><img style=\"height:28px;\" src='" + ApplicationPath.GetApplicationPath() + "content/themes/base/images/emerge-review-button.png' title='Emerge Review'/></a></td><td><a style=\"font-family: Arial;display:block;font-size:12px;padding-left:10px;text-decoration:none;color:#000;\" href=\"javascript:ReportReview('" + ObjRequestAndResponseResults.ProductDisplayName + "','" + ApplicantLastName + "','" + ApplicantFirstName + "',1,'" + ObjRequestAndResponseResults.pkOrderDetailId + "');\"  > &nbsp;<b>Emerge Review /</b> Connect with an agent </a></td><td style='display:none'><textarea id=" + ObjRequestAndResponseResults.pkOrderDetailId + ">" + ObjRequestAndResponseResults.ReviewComments + " </textarea></td>";
                                }
                                else
                                {
                                    EmergeAssistScript = EmergeAssistScript + " <td style=\"padding-left: 2%;\"><a id='" + ObjRequestAndResponseResults.ProductDisplayName + "' href=\"javascript:ActivateReportsVR('" + ObjRequestAndResponseResults.ProductDisplayName + "');\"><img style=\"height:28px;\" src='" + ApplicationPath.GetApplicationPath() + "content/themes/base/images/emerge-review-button.png' alt='Enable Review' title='Send Request for Emerge Review'/></a></td><td><a style=\"font-family: Arial;display:block;font-size:12px;padding-left:10px;text-decoration:none;color:#000;\" href=\"javascript:ActivateReportsVR('" + ObjRequestAndResponseResults.ProductDisplayName + "');\"  > &nbsp;<b>Emerge Review /</b> Connect with an agent </a></td>";
                                }
                            }
                            else
                            {
                                EmergeAssistScript = EmergeAssistScript + " <td></td>";
                            }
                        }
                        EmergeAssistScript = EmergeAssistScript + "</tr></table>";

                        ObjClsViewReport.Ending = DisclaimerScript + EmergeReviewSection + EmergeAssistScript + BottomScript;


                        nameDiff++;

                        ObjViewReport.Add(ObjClsViewReport);


                        if (ObjRequestAndResponseResults.ReviewStatus != 0)
                        {
                            CountReviewReports++;
                            if (ObjRequestAndResponseResults.ReviewStatus == 1)
                            {
                                IsShowViewTab = true;
                            }
                        }
                    }

                    ObjModel.ObjViewReportString = ObjViewReport;

                    if (ObjModel.ReportType.Length > 0)
                    {
                        ObjModel.ReportType = ObjModel.ReportType.Substring(0, ObjModel.ReportType.Length - 2);
                    }
                    #endregion

                    #region Display Alert If Auto Review Is Sent


                    if (ObjModel.IsShowRawData == true)
                    {
                        ObjModel.AllowReviewReportTab = true;
                    }
                    else
                    {
                        //if (IsShowViewTab == true)
                        //{
                        //    ObjModel.AllowReviewReportTab = true;
                        //}
                        //else
                        //{
                        ObjModel.AllowReviewReportTab = false;
                        //}
                    }

                    #endregion

                    #region Reference Code and Notes

                    List<Proc_GetReferenceCodebyCompanyIdResult> ObjList = new List<Proc_GetReferenceCodebyCompanyIdResult>();
                    ObjList = ObjBALCompany.GetReferenceCodeListbypkCompanyId(Convert.ToInt32(ObjRequestAndResponseResult[0].fkCompanyId.ToString()));

                    ObjList.Insert(0, new Proc_GetReferenceCodebyCompanyIdResult { ReferenceCode = "Select Reference #", pkReferenceCodeId = -1 });
                    ObjList.Add(new Proc_GetReferenceCodebyCompanyIdResult { ReferenceCode = "------------------", pkReferenceCodeId = -2 });
                    ObjList.Add(new Proc_GetReferenceCodebyCompanyIdResult { ReferenceCode = "Add New", pkReferenceCodeId = -3 });
                    ObjModel.ReferenceCodeList = ObjList;
                    ObjModel.TrackingRefCodeValue = "NA";
                    var SearchedTrackingRef = ObjRequestAndResponseResult[0].SearchedTrackingRef == "" ? "NA" : ObjRequestAndResponseResult[0].SearchedTrackingRef;
                    if (SearchedTrackingRef != "NA" && SearchedTrackingRef != null)
                    {
                        var SelectedrefCode = ObjList.Where(rec => rec.ReferenceCode.ToLower().Trim() == SearchedTrackingRef.ToLower().Trim()).FirstOrDefault();
                        if (SelectedrefCode != null)
                        {
                            var TrackingRefCodeValue = SelectedrefCode.pkReferenceCodeId;
                            ObjModel.TrackingRefCodeValue = TrackingRefCodeValue.ToString();
                        }
                    }
                    ObjModel.ReferenceCode = ObjRequestAndResponseResult[0].SearchedTrackingRef == "" ? "NA" : ObjRequestAndResponseResult[0].SearchedTrackingRef;
                    ObjModel.Notes = ObjRequestAndResponseResult[0].SearchedTrackingNotes == "" ? "NA" : ObjRequestAndResponseResult[0].SearchedTrackingNotes;
                    ObjModel.fkCompanyId = ObjRequestAndResponseResult[0].fkCompanyId;
                    #endregion


                    #region Permissible Purpose
                    int? orderpermissibleid = 0;
                    orderpermissibleid = ObjRequestAndResponseResult[0].SearchedPermissibleId == null ? 0 : ObjRequestAndResponseResult[0].SearchedPermissibleId;
                    ObjModel.PermissiblePurposeId = Convert.ToInt32(orderpermissibleid);
                    BALCompanyType serboj = new BALCompanyType();
                    List<tblPermissiblePurpose> PermissiblePurpose = serboj.GetPermissiblePurpose();
                    ObjModel.PermissiblePurposeList = PermissiblePurpose.Select(x => new SelectListItem { Text = x.PermissiblePurpose, Value = x.pkPermissibleId.ToString() }).ToList();
                    ObjModel.PermissiblePurposeList = ObjModel.PermissiblePurposeList.Select(x => new SelectListItem { Text = x.Text, Value = x.Value.ToString(), Selected = (x.Value == ObjModel.PermissiblePurposeId.ToString()) }).ToList();
                    ObjModel.PermissiblePurposeValue = PermissiblePurpose.Where(x => x.pkPermissibleId == (ObjModel.PermissiblePurposeId == 0 ? 1 : ObjModel.PermissiblePurposeId)).Select(x => x.PermissiblePurpose).FirstOrDefault().ToString();
                    #endregion


                    #region Package infomation
                    using (EmergeDALDataContext _EmergeDALDataContext = new EmergeDALDataContext())
                    {
                        string displayPckName = string.Empty;
                        bool flagPackages = false;
                        char[] trimch = new char[1];
                        trimch[0] = ',';

                        var list = _EmergeDALDataContext.proc_GetOrderPackage(OrderId).ToList();
                        if (list != null)
                        {
                            foreach (var ch in list)
                            {
                                displayPckName = displayPckName + ch.tblProductPackages_PackageName + ",";
                                flagPackages = true;
                            }
                            ObjModel.PackageName = displayPckName.Trim(trimch);
                            if (flagPackages)
                            {
                                ObjModel.PackageDisplayNameStatus = true;
                            }
                        }
                    }
                    #endregion
                }

            }
            #endregion
        }

        #endregion

        #region View Drug Testing Reports

        public ActionResult ViewDrugTestingReports()
        {

            #region Get Query String
            if (!string.IsNullOrEmpty(Request.QueryString["RR"]))
            {
                ViewBag.RR = Convert.ToInt32(Request.QueryString["RR"]);
            }
            if (!string.IsNullOrEmpty(Request.QueryString["_bck"]))
            {
                ViewBag.bck = Convert.ToInt32(Request.QueryString["_bck"]);
            }
            int OrderId = 0;
            if (Request.QueryString["_OId"] != null)
            {
                #region Check Guid
                //bool IsGuid = CommonHelper.IsGuid(Request.QueryString["_OId"]);
                //if (!IsGuid)
                //{
                //    return RedirectToAction("SavedReports");
                //}
                OrderId = Convert.ToInt32(Request.QueryString["_OId"]);
                #endregion
            }

            #endregion

            string ProductImagePath = Server.MapPath("~/Resources/Upload/Images/ProductImages/");
            ViewReportModel ObjModel = new ViewReportModel();
            DrugTestingPageLoad(OrderId, ObjModel, ProductImagePath);

            return View(ObjModel);
        }

        public void DrugTestingPageLoad(int OrderId, ViewReportModel ObjModel, string ProductImagePath)
        {
            BALGeneral objBalGenral = new BALGeneral();
            ShipToAddress objShipToAddress = new ShipToAddress();
            StringBuilder TopScript = new StringBuilder();
            ObjModel.OrderId = OrderId;

            BALOrders ObjBALOrders = new BALOrders();
            List<Proc_Get_DrugTestOrderDetailsResult> ObjData = ObjBALOrders.GetDrugTestOrderDetails(OrderId);
            if (ObjData.Count() > 0)
            {
                ObjModel.OrderNo = ObjData.First().OrderNo;
                ObjModel.OrderDt = ObjData.First().OrderDt.ToString("MM/dd/yyyy HH:mm:ss");
                ObjModel.ReportType = ObjData.First().ProductCode;
                string ActionString = string.Empty;
                string EditActionString = string.Empty;

                BALCompany ObjBALCompany = new BALCompany();
                List<Proc_Get_CompanyDetailByMemberShipIdResult> ObjCompanyDetails = ObjBALCompany.GetCompanyDetailByMemberShipIdResult(ObjData.First().fkUserId);
                using (EmergeDALDataContext dx = new EmergeDALDataContext())
                {
                    MembershipUser Member = Membership.GetUser();//User.Identity.Name
                    var v = dx.tblCompanyUsers.Where(d => d.fkUserId == ObjData.First().fkUserId).Select(d => new { d.pkCompanyUserId }).FirstOrDefault();
                    if (Roles.IsUserInRole(Member.Email, "SystemAdmin") || Roles.IsUserInRole(Member.Email, "SupportTechnician") || Roles.IsUserInRole(Member.Email, "SupportManager"))
                    {
                        //Get home company location based on JIRA ticket INT-325 as per Greg comments.
                        var homeCompanyLocation = dx.tblLocations.Where(x => x.fkCompanyId == ObjData.First().fkCompanyId && x.IsHome == true).FirstOrDefault();
                        string homeLocation = string.Empty;
                        if (homeCompanyLocation != null)
                        {
                            //Set home location.
                            homeLocation = Convert.ToString(homeCompanyLocation.pkLocationId);
                        }
                        else
                        {
                            //If query is not able to get home location than location will be the exact location where selected order has created.
                            homeLocation = ObjData.First().fkLocationId.ToString();
                        }

                        if (v != null)
                        {
                            ObjModel.CompanyUser = "<a title=\"Click here to edit User\" class=\"Anchor\" href=" + ApplicationPath.GetApplicationPath() + "Control/AddCompanyUser?_a=" + v.pkCompanyUserId.ToString() + ">" + ObjCompanyDetails.First().lastname + ", " + ObjCompanyDetails.First().firstname + "</a>";
                        }
                        else
                        {
                            ObjModel.CompanyUser = "<a title=\"Click here to edit User\" class=\"Anchor\" href=" + ApplicationPath.GetApplicationPath() + "Control/AddCompanyUser?_a=" + Guid.Empty.ToString() + ">" + ObjCompanyDetails.First().lastname + ", " + ObjCompanyDetails.First().firstname + "</a>";
                        }
                        ObjModel.CompanyLocation = "<a title=\"Click here to edit Location\"  class=\"Anchor\"  href=" + ApplicationPath.GetApplicationPath() + "Control/AddCompanyLocation?lid=" + ObjData.First().fkLocationId.ToString() + ">" + ObjCompanyDetails.First().city + ", " + ObjCompanyDetails.First().statecode + "</a>";
                        ObjModel.CompanyName = "<a title=\"Click here to edit Company\"  class=\"Anchor\"  href=" + ApplicationPath.GetApplicationPath() + "Control/AddCompanies?Id1=" + homeLocation + ">" + ObjCompanyDetails.First().CompanyName + "</a>";
                        ObjModel.CompanyNameAdverse = ObjCompanyDetails.First().CompanyName;
                        ObjModel.IsReportStatus = ObjCompanyDetails.First().IsReportStatus;
                        if (ObjData.First().OrderStatus == 1)
                        {
                            ActionString = "<div id='CompleteButtonDiv'><input type='Button' value='Complete' onclick='CompleteOrderByOrderId(\"" + OrderId + "\")' </div>";
                            EditActionString = "<div id='EditButtonDiv'><input type='Button' style='width: 70px;' value='Edit' onclick='EditDrugTestingOrder(\"" + OrderId + "\")' </div>";
                        }
                        else if (ObjData.First().OrderStatus == 2)
                        {
                            ActionString = "<div id='CompleteButtonDiv'><span class= 'Complete'>Complete</span></div>";
                        }
                        else if (ObjData.First().OrderStatus == 3)
                        {
                            ActionString = "<div id='CompleteButtonDiv'><span class = 'InComplete'> Incomplete </span> </div>";
                        }

                    }
                    else
                    {
                        ObjModel.CompanyUser = ObjCompanyDetails.First().lastname + ", " + ObjCompanyDetails.First().firstname;
                        ObjModel.CompanyLocation = ObjCompanyDetails.First().city + ", " + ObjCompanyDetails.First().statecode;
                        ObjModel.CompanyName = ObjCompanyDetails.First().CompanyName;
                        ObjModel.CompanyNameAdverse = ObjCompanyDetails.First().CompanyName;
                        ObjModel.IsReportStatus = ObjCompanyDetails.First().IsReportStatus;
                        if (ObjData.First().OrderStatus == 1)
                        {
                            ActionString = "<div id='CompleteButtonDiv'><span class='Pending'>Pending</span></div>";
                        }
                        else if (ObjData.First().OrderStatus == 2)
                        {
                            ActionString = "<div id='CompleteButtonDiv'><span class='Complete'>Complete</span></div>";
                        }
                        else if (ObjData.First().OrderStatus == 3)
                        {
                            ActionString = "<div id='CompleteButtonDiv'><span class='Incomplete'>Incomplete</span></div>";
                        }
                    }
                }
                if (ObjData.First().OrderStatus == 1)           // When Order Status is pending then show Update Note
                {
                    if (ObjData.First().PendingOrderUpdateNote != string.Empty)
                    {
                        ObjModel.PendingOrderNote = DisplayPendingOrderNote(ObjData.First().PendingOrderUpdateNote);
                        ObjModel.tdUpdateOrderNote = true;
                    }
                    else
                    {
                        ObjModel.tdUpdateOrderNote = false;
                    }
                }
                else
                {
                    ObjModel.tdUpdateOrderNote = false;
                }
                ProductImagePath = ProductImagePath + ObjData.First().ProductImage;
                string sImg = string.Empty;

                if (System.IO.File.Exists(ProductImagePath))
                {
                    string ImageVirtualPath = ApplicationPath.GetApplicationPath() + "Resources/Upload/Images/ProductImages/" + ObjData.First().ProductImage;
                    string imgTitle = ObjData.First().LongDescription;

                    System.Drawing.Image ImgProduct = System.Drawing.Image.FromFile(ProductImagePath);
                    int ImgWidth = 0, ImgHeight = 0;

                    if (ImgProduct != null)
                    {
                        ImgWidth = ImgProduct.Width;
                        ImgHeight = ImgProduct.Height;
                    }
                    ImgWidth = (ImgWidth > 250) ? 250 : ImgWidth;
                    ImgHeight = (ImgHeight > 250) ? 250 : ImgHeight;

                    sImg = "<img src='" + ImageVirtualPath + "' width='" + ImgWidth + "px' height='" + ImgHeight + "px' style='border:1px solid black;' title='" + imgTitle + "'/>";
                }

                //if (Role.Contains("SystemAdmin") || Role.Contains("SupportTechnician") || Role.Contains("SupportManager"))
                //{
                //    ActionString = "<div id='CompleteButtonDiv'><input type='Button' value='Complete' onclick='CompleteOrderByOrderId(\"" + OrderId + "\")' </div>"

                //}
                //ObjData.First().CompanyName;
                string Address = string.Empty;
                objShipToAddress = objBalGenral.GetUpdatedShipToAddress(Convert.ToInt32(ObjData.First().fkLocationId.ToString()), Convert.ToInt32(ObjData.First().pkCompanyUserId.ToString()), ObjModel.OrderId);
                if (objShipToAddress != null)
                {
                    Address = "<div><div style='width:100%;'>" + objShipToAddress.CompanyName + ",</div><div style='width:100%;'>" + objShipToAddress.Address1;
                    Address = objShipToAddress.Address2 != "" ? Address + objShipToAddress.Address2 + ",</div>" : Address + "</div>";
                    Address = objShipToAddress.City != "" ? Address + "<div style='width:100%;'>" + objShipToAddress.City + "," : Address + "<div style='width:100%;'>";
                    Address = objShipToAddress.ZipCode != "" ? Address + objShipToAddress.StateCode + "-" + objShipToAddress.ZipCode + "</div><div>" : Address + objShipToAddress.StateName + "</div></div>";
                }
                else
                {
                    Address = "<div><div style='width:100%;'>" + ObjData.First().CompanyName + ",</div><div style='width:100%;'>" + ObjData.First().Address1;
                    Address = ObjData.First().Address2 != "" ? Address + ObjData.First().Address2 + ",</div>" : Address + "</div>";
                    Address = ObjData.First().City != "" ? Address + "<div style='width:100%;'>" + ObjData.First().City + "," : Address + "<div style='width:100%;'>";
                    Address = ObjData.First().ZipCode != "" ? Address + ObjData.First().StateCode + "-" + ObjData.First().ZipCode + "</div><div>" : Address + ObjData.First().StateName + "</div></div>";
                }

                TopScript.Append("<div><div class=\"main-box\" ><div onclick=\"collapse('" + ObjModel.ReportType + "');\">");
                TopScript.Append("<table class=\"rv-header\"  width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;\">");
                TopScript.Append("<tr>");
                TopScript.Append("<td class=\"plus_btn\" width=\"39px\" height=\"34px\">ClickHere<br />ClickHere</td>");
                TopScript.Append("<td class=\"rv-header\" width=\"95%\">");
                TopScript.Append("<table style='font-weight:bold; width:100%'>");
                TopScript.Append("<tbody>");
                TopScript.Append("<tr>");
                TopScript.Append("<td style=\"vertical-align: middle; width:50%\" align='left'>Order Details</td>");
                TopScript.Append("<td style=\"vertical-align: middle; width:50%\" align='right'>&nbsp;</td>");
                TopScript.Append("</tr>");
                TopScript.Append("</tbody>");
                TopScript.Append("</table>");
                TopScript.Append("</td>");
                TopScript.Append("<td class=\"rv-header\" width=\"9px\"></td>");
                TopScript.Append("</tr>");
                TopScript.Append("</table></div>");

                TopScript.Append("<div id=\"" + ObjData.First().ProductCode + "_b\" style=\"display: block;\">");
                TopScript.Append("<center>");
                TopScript.Append("<table border='0' width=\"90%\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin:5px 0px;\">");
                TopScript.Append("<tr>");
                TopScript.Append("<td width='20%' align='left' valign='top' style='border-bottom:solid 1px black;'>Product Image</td>");
                TopScript.Append("<td width='15%' align='left' valign='top' style='border-bottom:solid 1px black;'>Product Name</td>");
                TopScript.Append("<td width='15%' align='left' valign='top' style='border-bottom:solid 1px black;'>Address</td>");
                TopScript.Append("<td width='10%' valign='top' align='right' style='border-bottom:solid 1px black;'>Price</td>");
                TopScript.Append("<td width='10%' valign='top' align='right' style='border-bottom:solid 1px black;'>Quantity</td>");
                TopScript.Append("<td width='10%' valign='top' align='right' style='border-bottom:solid 1px black;'>Shipping Cost</td>");
                TopScript.Append("<td width='10%' valign='top' align='right' style='border-bottom:solid 1px black;'>Order Amount</td>");
                TopScript.Append("<td width='10%' valign='top' align='right' style='border-bottom:solid 1px black;'></td>");
                TopScript.Append("<td width='10%' valign='top' align='right' style='border-bottom:solid 1px black;'></td>");
                TopScript.Append("</tr>");

                TopScript.Append("<tr style='height:10px;'>");
                TopScript.Append("<td colspan='9'></td>");
                TopScript.Append("</tr>");
                decimal ReportAmount;
                decimal.TryParse(ObjData.First().ReportAmount.ToString(), out ReportAmount);

                decimal ProductQty;
                decimal.TryParse(ObjData.First().ProductQty.ToString(), out ProductQty);

                decimal OrderAmount = ReportAmount * ProductQty;

                string ShippingCost = string.Empty;
                if (ObjData.First().AdditionalFee != decimal.MinValue)
                {
                    OrderAmount += ObjData.First().AdditionalFee;
                    ShippingCost = ObjData.First().AdditionalFee.ToString("C");
                }



                TopScript.Append("<tr>");
                TopScript.Append("<td align='left' valign='top'>" + sImg + "</td>");
                TopScript.Append("<td align='left' valign='top'>" + ObjData.First().ProductDisplayName + "</td>");
                TopScript.Append("<td align='left' valign='top'>" + Address + "</td>");
                TopScript.Append("<td valign='top' align='right'>" + ReportAmount.ToString("C") + "</td>");
                TopScript.Append("<td valign='top' align='right'>" + ObjData.First().ProductQty.ToString() + "</td>");

                TopScript.Append("<td valign='top' align='right'>" + ShippingCost + "</td>");
                TopScript.Append("<td valign='top' align='right'>" + OrderAmount.ToString("C") + "</td>");
                TopScript.Append("<td valign='top' align='right'>" + ActionString + "</td>");
                TopScript.Append("<td valign='top' align='right'>" + EditActionString + "</td>");

                TopScript.Append("</tr>");
                TopScript.Append("</table>");
                TopScript.Append("</center>");
                TopScript.Append("</div>");
                TopScript.Append("</div>");

                ObjModel.DrugTestingString = TopScript.ToString();

            }
        }

        #endregion

        #region View eScreen Reports

        public ActionResult ViewEscreenReport()
        {

            #region Get Query String
            if (!string.IsNullOrEmpty(Request.QueryString["_bck"]))
            {
                ViewBag.bck = Convert.ToInt32(Request.QueryString["_bck"]);
            }
            int OrderId = 0;
            if (Request.QueryString["_OId"] != null)
            {
                #region Check Guid
                //bool IsGuid = CommonHelper.IsGuid(Request.QueryString["_OId"]);
                //if (!IsGuid)
                //{
                //    return RedirectToAction("SavedReports");
                //}
                OrderId = Convert.ToInt32(Request.QueryString["_OId"]);
                #endregion
            }

            #endregion
            ViewEscreenReportModel ObjModel = new ViewEscreenReportModel();
            eScreenPageLoad(OrderId, ObjModel);

            return View(ObjModel);
        }

        public void eScreenPageLoad(int pkOrderId, ViewEscreenReportModel ObjModel)
        {



            BALOrders ObjBALOrders = null;
            string responseurl = "";
            try
            {
                var ListItems = GetEscreenRequestAccount(pkOrderId);
                if (ListItems.Item1 == string.Empty && ListItems.Item2 == string.Empty && ListItems.Item3 == string.Empty && ListItems.Item4 == string.Empty)
                {

                }
                else
                {

                    ObjBALOrders = new BALOrders();
                    StringBuilder ObjXML = new StringBuilder();
                    ObjXML.Append("<?xml version='1.0' encoding='UTF-8'?>");
                    ObjXML.Append("<TicketRequest>");
                    ObjXML.Append("<Version>1.0</Version>");
                    ObjXML.Append("<Mode>Prod</Mode>");
                    ObjXML.Append("<CommitAction>Commit</CommitAction>");
                    ObjXML.Append("<PartnerInfo>");
                    ObjXML.Append("<UserName>" + ListItems.Item1 + "</UserName>");
                    ObjXML.Append("<Password>" + ListItems.Item2 + "</Password>");
                    ObjXML.Append("</PartnerInfo>");
                    ObjXML.Append("<CustomerIdentification>");
                    ObjXML.Append("<ClientAccount>" + ListItems.Item3 + "</ClientAccount>");
                    ObjXML.Append("<ClientSubAccount>" + ListItems.Item4 + "</ClientSubAccount>");
                    ObjXML.Append("</CustomerIdentification>");
                    ObjXML.Append("<TicketAction>");
                    ObjXML.Append("<Type>SchedReturn</Type>");
                    ObjXML.Append("<Params>");
                    ObjXML.Append("<Param>");
                    ObjXML.Append("<ID>EventID</ID>");
                    ObjXML.Append("<Value>%CurrentEventID%</Value>");//148953  //  %CurrentEventID%
                    ObjXML.Append("</Param>");
                    ObjXML.Append("<Param>");
                    ObjXML.Append("<ID>ExternalUserID</ID>");
                    ObjXML.Append("<Value>" + pkOrderId.ToString() + "</Value>");
                    ObjXML.Append("</Param>");
                    ObjXML.Append("</Params>");
                    ObjXML.Append("</TicketAction>");
                    ObjXML.Append("</TicketRequest>");
                    responseurl = ObjBALOrders.GetEscreenOrder(pkOrderId, ObjXML.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBALOrders = null;
            }


            ObjModel.SchedReturnURL = responseurl;

        }
        public Tuple<string, string, string, string> GetEscreenRequestAccount(int pkOrderId)
        {
            using (EmergeDALDataContext Objdata = new EmergeDALDataContext())
            {
                string Username = string.Empty;
                string Password = string.Empty;
                string ClientAccount = string.Empty;
                string ClientSubAccount = string.Empty;
                var query = (from u in Objdata.tblOrders
                             join ut in Objdata.tblOrderDetails on u.pkOrderId equals ut.fkOrderId
                             join us in Objdata.tblthirdpartycredentials on ut.fkCompanyId equals us.fkCompanyId
                             where u.pkOrderId == pkOrderId
                             select ut).FirstOrDefault();

                if (query != null)
                {
                    var Obj_thirdpartycredentialdata = Objdata.tblthirdpartycredentials.Where(db => db.fkCompanyId == query.fkCompanyId).FirstOrDefault();
                    if (Obj_thirdpartycredentialdata != null)
                    {
                        Username = Obj_thirdpartycredentialdata.username;
                        Password = Obj_thirdpartycredentialdata.password;
                        ClientAccount = Obj_thirdpartycredentialdata.accountno;
                        ClientSubAccount = Obj_thirdpartycredentialdata.subaccountno;
                    }
                }
                return new Tuple<string, string, string, string>(Username, Password, ClientAccount, ClientSubAccount);
            }
        }
        #endregion

        #region Private Methods

        private string DisplayNoResponseMessage()
        {
            return "<div id='NoResponse_message' align='left' style='padding:5px'> <div style='clear:both;'> STATUS : No Response <hr id='Pending_Message_Hr' align='left'> </div><div>Additional information is needed to complete this report. Multiple attempts have been made to contact <br>the user and no response has been received. Please contact Intelifi to complete this report.<br/> <br/>888.409.1819</div></div>";
        }
        private string DisplayPendingOrderNote(string OrderNote)
        {
            return "<div style='padding-left:10px;padding-right:10px'><b>Pending Report Update:</b> " + OrderNote + "  ";
        }

        private string DisplayDisclaimer()
        {
            return "<div style='padding-left:10px;padding-right:10px'><b>Emerge Review Disclaimer :</b> This report has been manually amended through the Emerge Review feature. Emerge Review simply  acts as an editing tool for efficient clarification and communication between Intelifi's support team and the user. This report is NOT in its original form and the contents of this report are provided \"as is\".  Any editing of this report should not be used in place of the original court record. ";
        }

        private void GetMvrResponse(Proc_Get_RequestAndResponseResult ObjRequestAndResponseResults, StringBuilder TopScript)
        {
            ObjRequestAndResponseResults.ResponseData = ObjRequestAndResponseResults.ResponseData.Trim();
            bool Objdynamic = GetResponse(ObjRequestAndResponseResults.ResponseData);
            if (Objdynamic == true)
            {
                using (Xml ObjXml_MVR = new Xml())
                {
                    ObjXml_MVR.DocumentContent = (ObjRequestAndResponseResults.ResponseData == null ? "" : ObjRequestAndResponseResults.ResponseData.ToString());
                    XmlDocument ObjXmlDocument = new XmlDocument();
                    ObjXmlDocument.LoadXml(ObjXml_MVR.DocumentContent);
                   // XmlNode ObjXmlNode = ObjXmlDocument.DocumentElement;
                    if ((ObjRequestAndResponseResults.ReportStatus != Convert.ToByte("2") && ObjRequestAndResponseResults.ReportStatus != Convert.ToByte("3") && ObjRequestAndResponseResults.ReportStatus != Convert.ToByte("4")))
                    {
                        TopScript.Append(DisplayPendingMessage());
                    }
                    if (ObjRequestAndResponseResults.ReportStatus == Convert.ToByte("3"))
                    {
                        BALOrders objBALOrders = new BALOrders();
                        objBALOrders.UpdateOrderStatus(ObjRequestAndResponseResults.fkOrderId);// ticket #969
                    }
                    if (ObjRequestAndResponseResults.ReportStatus == Convert.ToByte("4"))
                    {
                        //BALOrders objBALOrders = new BALOrders();
                        //objBALOrders.UpdateOrderStatus(ObjRequestAndResponseResults.fkOrderId);// ticket #969
                        string Response = (ObjRequestAndResponseResults.ResponseData == null ? "" : ObjRequestAndResponseResults.ResponseData.ToString());
                        XDocument XResponse = XDocument.Parse(Response);
                        var xElement = XElement.EmptySequence.ToList();
                        xElement = XResponse.Descendants("errordescription").ToList();
                        String ErrorMsg = "";
                        if (xElement.Count != 0)
                        {
                            ErrorMsg = xElement.FirstOrDefault().Value.Trim();
                        }
                        //TopScript.Append("<div id='Pending_message' align='left'><div style='clear:both;'>STATUS : <span style='color:red'>Cancelled</span><hr align='left' id='Pending_Message_Hr' /></div><div>Reason : '" + ErrorMsg + "'<br />  Emerge support Team <span class='Pending_Message_Separator'> |</span> <a href='mailto:support@intelifi.com' class='Pending_Message_Link'>support@intelifi.com</a> <span class='Pending_Message_Separator'> |</span> 888.409.1819 </div></div>");
                        TopScript.Append("<div id='Pending_message' align='left' style='font-size:14px;padding-left: 2%;'><div style='clear:both;'>STATUS : <span style='color:red'>Your report is being processed manually.</span><hr align='left' id='Pending_Message_Hr' /></div><div>Reason : '" + ErrorMsg + "'<br />  Emerge support Team <span class='Pending_Message_Separator'> |</span> <a href='mailto:support@intelifi.com' class='Pending_Message_Link'>support@intelifi.com</a> <span class='Pending_Message_Separator'> |</span> 888.409.1819 </div></div>");
                    }
                    #region Old Code To Get the Formatted MVR Response from the XML

                    //if (ObjRequestAndResponseResults.ReportStatus != Convert.ToByte("4"))
                    //{
                    //    string URL_string = ObjXmlNode.SelectSingleNode("ReportLink").ChildNodes[0].Value;

                    //    string[] str_Uri_Content = URL_string.Replace("_$_", "&").Split('?');

                    //    string Uri_string = str_Uri_Content[0].Replace("mvr.", "ptp.");

                    //    string Content_string = "";

                    //    if (str_Uri_Content[1].Contains("&amp;"))
                    //    {
                    //        Content_string = str_Uri_Content[1].Replace("&amp;", "&");
                    //    }
                    //    else
                    //    {
                    //        Content_string = str_Uri_Content[1];
                    //    }

                    //    string UriResponse = Utility.SendRequest(Uri_string, Content_string);

                    //    int LengthOfResponse = UriResponse.Length;

                    //    int FirstHalfIndex = UriResponse.IndexOf("** END OF RECORD **");

                    //    UriResponse = UriResponse.Substring(0, FirstHalfIndex);

                    //    int startindex = UriResponse.IndexOf("<table border=\"1\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">", 300);

                    //    UriResponse = UriResponse.Substring(startindex);

                    //    UriResponse = "<pre><div style=\"font-family: Arial; font-size: 12px;\">" + UriResponse + "</div></pre><br /><div style=\"font-size: 16px;\"><center>** END OF RECORD ***</center></div>";


                    //    TopScript.Append(UriResponse + (ObjRequestAndResponseResults.ReportStatus != Convert.ToByte("2") && ObjRequestAndResponseResults.ReportStatus != Convert.ToByte("3") ? DisplayPendingMessage() : ""));


                    //}
                    #endregion
                }
            }
            else
            {

                TopScript.Append(ObjRequestAndResponseResults.ResponseData + (ObjRequestAndResponseResults.ReportStatus != Convert.ToByte("2") && ObjRequestAndResponseResults.ReportStatus != Convert.ToByte("3") ? DisplayPendingMessage() : ""));

            }
        }

        #region Fetch response Live Site

        private string FetchResponse(Proc_Get_RequestAndResponseResult ObjRequestAndResponseResults, ClsViewReport ObjClsViewReport, StringBuilder TopScript, bool IsReview, bool IsAdmin, string[] Role, ViewReportModel ObjModel)
        {
            BALOrders ObjBALOrders = new BALOrders();
            int OrderDetailId = Convert.ToInt32(ObjRequestAndResponseResults.pkOrderDetailId.ToString());
            int OrderId = Convert.ToInt32(ObjRequestAndResponseResults.fkOrderId.ToString());
            string hdnReviewDetail = string.Empty;
            #region--------------108 Categories in NCR1 report.

            if (ObjRequestAndResponseResults.ProductCode == "NCR1")
            {
                if (ObjBALOrders.CheckOrderInNCRXML(ObjRequestAndResponseResults.OrderNo) == 0)
                {
                    ObjBALOrders.InsertOrderInNCRXML(ObjRequestAndResponseResults.OrderNo);
                    string strResponse = ObjBALOrders.AddNewTagsInNCRResponses(ObjRequestAndResponseResults.ResponseData);
                    if (strResponse != string.Empty)
                    {
                        ObjRequestAndResponseResults.ResponseData = strResponse;
                        UpdateResponse((int)ObjRequestAndResponseResults.pkOrderResponseId, ObjRequestAndResponseResults.ResponseData);
                    }
                }
            }

            #endregion
            bool Objdynamic = GetResponse(ObjRequestAndResponseResults.ResponseData);
            if (Objdynamic)
            {
                using (Xml ObjXml = new Xml())
                {
                    string ProductCode = "";

                    ObjXml.DocumentContent = ObjRequestAndResponseResults.ResponseData;
                   //int IsReportForReview = 0;
                    // TopScript.Append(DisplayPendingMessage());
                    ObjXml.DocumentContent = (ObjRequestAndResponseResults.ResponseData == null ? "" : ObjRequestAndResponseResults.ResponseData.ToString());
                    if (ObjXml.DocumentContent.ToString() == "<XML_INTERFACE>ZZZ</XML_INTERFACE>" && ObjRequestAndResponseResults.IsFirstPendingRequest == true)
                    {
                        string FullRequest = Utility.GetLastRequest(ObjRequestAndResponseResults.RequestData);
                        string[] Request1 = FullRequest.Split('?');
                        string Uri_string = Request1[0];
                        string Content_string = string.Empty;

                        if (Request1.Length > 1)
                        {
                            Content_string = Request1[1];
                        }
                        string UriResponse = "<XML_INTERFACE>ZZZ</XML_INTERFACE>";
                        while (UriResponse == "<XML_INTERFACE>ZZZ</XML_INTERFACE>" && MicroBiltEndPoint < 4)
                        {
                            UriResponse = Utility.SendRequest(Uri_string, Content_string);
                            MicroBiltEndPoint++;
                            System.Threading.Thread.Sleep(4000);
                        }
                        if (UriResponse.Contains("<XML_INTERFACE>"))
                        {
                            UpdateResponse((int)ObjRequestAndResponseResults.pkOrderResponseId, UriResponse);
                            try
                            {
                                ObjBALOrders.CheckResponseandUpdateStatus(ObjRequestAndResponseResults.ProductCode, ObjRequestAndResponseResults.fkOrderId, UriResponse, ObjRequestAndResponseResults.pkOrderDetailId, false, string.Empty, string.Empty, string.Empty, false);
                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                ObjBALOrders = null;
                            }
                        }
                        if (UriResponse == "<XML_INTERFACE>ZZZ</XML_INTERFACE>")
                        {
                            if (MicroBiltEndPoint == 4)
                            {
                                TopScript.Append(DisplayPendingMessage());
                            }
                        }
                        else
                        {
                            if (GetResponse(UriResponse) == true)
                            {
                                if (ObjRequestAndResponseResults.IsLiveRunner == true)
                                    ProductCode = ObjRequestAndResponseResults.ProductCode.Trim() != "RCX" ? ObjRequestAndResponseResults.ProductCode.Trim() + "LiveRunner" : ObjRequestAndResponseResults.ProductCode.Trim(); /* XSLT for live runner report*/
                                else
                                    ProductCode = ObjRequestAndResponseResults.ProductCode;
                                ObjXml.DocumentContent = UriResponse;
                                ObjClsViewReport.XSLTName = "~/Resources/XSLT/" + (ProductCode == "SNS" && Request.Url.ToString().Contains("Control") ? ProductCode + "_CONTROL" : ProductCode) + ".xslt";

                            }

                        }
                        BALGeneral.UpdateFirstPendingRequestStatus(ObjRequestAndResponseResults.pkOrderDetailId);
                    }
                    else
                    {
                        string pcode = "";
                        if (ObjRequestAndResponseResults.ReportStatus == Convert.ToByte("2"))
                        {
                            string ModifiedResponse = string.Empty;
                            ModifiedResponse = ObjRequestAndResponseResults.ResponseData;

                            if (ObjRequestAndResponseResults.IsLiveRunner == true)
                            {
                                if (ObjRequestAndResponseResults.ProductCode.Trim() == "CCR1" || ObjRequestAndResponseResults.ProductCode.Trim() == "CCR2" || ObjRequestAndResponseResults.ProductCode.Trim() == "SCR")
                                {
                                    pcode = "RCX";
                                }
                                else
                                {
                                    pcode = ObjRequestAndResponseResults.ProductCode.Trim();
                                }
                                ProductCode = ObjRequestAndResponseResults.ProductCode.Trim() != "RCX" ? ObjRequestAndResponseResults.ProductCode.Trim() + "LiveRunner" : ObjRequestAndResponseResults.ProductCode.Trim(); /* XSLT for live runner report*/

                            }
                            else
                            {
                                ProductCode = ObjRequestAndResponseResults.ProductCode;
                                pcode = ProductCode;
                            }
                            /* Match meter for NCR1 Report */


                            string ncr1Response = "";
                            bool IsNCR1Report = false;
                            int pkNCR1OrderResponseId = 0;
                            BALEmergeReview ObjBALEmergeReview = new BALEmergeReview();
                            bool IsMatchMeter = false;

                            if (ObjRequestAndResponseResults.ProductCode.ToLower() == "ps")
                            {
                                if (ObjRequestAndResponseResults.ResponseData != "")
                                {
                                    PSResponse = ObjRequestAndResponseResults.ResponseData;
                                   // vwpsStatus = 1; commented By Himesh Kumar
                                    XmlDocument xmlDoc = new XmlDocument();
                                    xmlDoc.LoadXml(PSResponse);
                                    XmlNodeList filelst = xmlDoc.GetElementsByTagName("county"); //Changed By Himesh Kumar
                                    System.Web.HttpContext.Current.Session["CountyExist"] = "2";
                                    for (int i = 0; i < filelst.Count; i++)
                                    {
                                        XmlElement afile = (XmlElement)filelst[i];
                                        string County = afile.FirstChild.Value;
                                        if (string.IsNullOrEmpty(County) == false) { System.Web.HttpContext.Current.Session["CountyExist"] = "1"; break; }

                                    }



                                }
                            }

                            /* Match meter for NCR1 Report */
                            if (ProductCode == "NCR1" || ProductCode == "SCR" || ProductCode == "NCR+" || ProductCode == "RCX" || ProductCode == "NCR4" || ProductCode == "CCR1LiveRunner" || ProductCode == "CCR2LiveRunner")
                            {

                                ModifiedResponse = AddOrderDetailToXMLResponse_0(ProductCode, ObjXml.DocumentContent, ObjRequestAndResponseResults.pkOrderDetailId, ObjModel);
                                ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);
                                ModifiedResponse = AddOrderDetailToXMLResponse_1(ProductCode, ObjXml.DocumentContent, ObjRequestAndResponseResults.pkOrderDetailId);
                                ObjBALOrders.UpdateOrderResponse(ObjRequestAndResponseResults.pkOrderDetailId, ModifiedResponse);
                                #region MatchMeter

                                if (ObjRequestAndResponseResults.ProductCode.ToLower() == "ncr1")
                                {
                                    //if (Role.Contains("SystemAdmin") && ObjRequestAndResponseResults.pkOrderDetailId == new Guid("05DCACFE-32AE-E211-94A6-047D7BF23350"))//"4bab73ee-96c1-e211-a076-002590742e90"
                                    if (Role.Contains("SystemAdmin") && ObjRequestAndResponseResults.pkOrderDetailId == 0)
                                    {
                                        ncr1Response = ObjRequestAndResponseResults.FilteredResponseData;
                                    }
                                    else
                                    {
                                        ncr1Response = ObjRequestAndResponseResults.ResponseData;
                                    }
                                    IsMatchMeter = ObjRequestAndResponseResults.IsMatchMeter;
                                    if (ncr1Response != "")
                                    {
                                        IsNCR1Report = true;
                                    }
                                    pkNCR1OrderResponseId = ObjRequestAndResponseResults.pkOrderResponseId;
                                }
                                if (IsNCR1Report == true && pkNCR1OrderResponseId != 0)
                                {
                                    if (IsMatchMeter == false)
                                    {
                                        bool isadmin = false;
                                        if (Role.Contains("SystemAdmin") || Role.Contains("SupportTechnician") || Role.Contains("SupportManager"))
                                        {
                                            isadmin = true;
                                        }
                                        else
                                        {
                                            isadmin = false;
                                        }
                                        if (vwpsStatus == 1)
                                        {
                                            string updatedResponse = ObjBALEmergeReview.matchmeter(ObjRequestAndResponseResults.SearchedFirstName, ObjRequestAndResponseResults.SearchedLastName, ObjRequestAndResponseResults.SearchedMiddleInitial, ObjRequestAndResponseResults.SearchedDob, PSResponse, ncr1Response, pkNCR1OrderResponseId, ObjRequestAndResponseResults.ProductCode, OrderDetailId, OrderId, isadmin, ObjRequestAndResponseResults.best_SearchedZipcode, ObjRequestAndResponseResults.best_search_state, ObjRequestAndResponseResults.best_SearchedCity, ObjRequestAndResponseResults.best_SearchedStreetAddress + " " + ObjRequestAndResponseResults.best_SearchedStreetName, ObjRequestAndResponseResults.IsAddressUpdated);
                                            ModifiedResponse = updatedResponse;
                                        }
                                        else
                                        {
                                            string updatedResponse = ObjBALEmergeReview.NCR1matchmeter(ObjRequestAndResponseResults.SearchedFirstName, ObjRequestAndResponseResults.SearchedLastName, ObjRequestAndResponseResults.SearchedMiddleInitial, ObjRequestAndResponseResults.SearchedDob, ObjRequestAndResponseResults.SearchedZipCode, ObjRequestAndResponseResults.SearchedStateCode, ObjRequestAndResponseResults.SearchedCity, ObjRequestAndResponseResults.SearchedStreetAddress + " " + ObjRequestAndResponseResults.SearchedStreetName, ncr1Response, pkNCR1OrderResponseId, ObjRequestAndResponseResults.ProductCode, OrderDetailId, OrderId, isadmin);
                                            ModifiedResponse = updatedResponse;
                                        }
                                    }
                                }
                                #endregion
                                #region Ticket #800: 7 YEAR STATES

                                //bool IsShowRawData = true;
                                //ModifiedResponse = objBALEmergeReview.HideSevenYear(ModifiedResponse, ObjRequestAndResponseResult.pkOrderResponseId, pcode, DateTime.Now, out IsShowRawData);
                                //if (Roles.IsUserInRole(Page.User.Identity.Name, "SystemAdmin") || Roles.IsUserInRole(Page.User.Identity.Name, "SupportTechnician") || Roles.IsUserInRole(Page.User.Identity.Name, "SupportManager"))
                                //{ }
                                //else 
                                //{ 
                                //    LblIsShowRawData.Text = IsShowRawData.ToString(); 
                                //}
                                //ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);
                                #endregion
                            }

                            #region Main XML Filteration

                            bool IsShowRawData = true;
                            XMLFilteration ObjXMLFilteration = new XMLFilteration();
                            try
                            {
                                //if (ObjRequestAndResponseResults.pkOrderDetailId == new Guid("05DCACFE-32AE-E211-94A6-047D7BF23350"))//"4bab73ee-96c1-e211-a076-002590742e90"
                                if (ObjRequestAndResponseResults.pkOrderDetailId == 0)
                                {
                                    if (Role.Contains("SystemAdmin"))
                                    {
                                        IsShowRawData = true;
                                    }
                                    else
                                    {
                                        string UpdatedResponse = ObjXMLFilteration.GetFilteredXML(ProductCode, pcode, ModifiedResponse, ObjRequestAndResponseResults.pkOrderDetailId, ObjRequestAndResponseResults.WatchListStatus, DateTime.Now, ObjRequestAndResponseResults.IsLiveRunner, out IsShowRawData);
                                        ModifiedResponse = UpdatedResponse;
                                    }
                                }
                                else
                                {
                                    string UpdatedResponse = ObjXMLFilteration.GetFilteredXML(ProductCode, pcode, ModifiedResponse, ObjRequestAndResponseResults.pkOrderDetailId, ObjRequestAndResponseResults.WatchListStatus, DateTime.Now, ObjRequestAndResponseResults.IsLiveRunner, out IsShowRawData);
                                    ModifiedResponse = UpdatedResponse;
                                }

                                #region For 7 Year States
                                if (Role.Contains("SystemAdmin") || Role.Contains("SupportTechnician") || Role.Contains("SupportManager"))
                                {
                                    ObjModel.IsShowRawData = true;
                                }
                                else
                                {
                                    ObjModel.IsShowRawData = false;
                                    //ObjModel.IsShowRawData = IsShowRawData;
                                }
                                #endregion For 7 Year States

                                ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);
                                if (ProductCode == "NCR1" || ProductCode == "SCR" || ProductCode == "NCR+" || ProductCode == "RCX" || ProductCode == "NCR4" || ProductCode == "CCR1LiveRunner" || ProductCode == "CCR2LiveRunner")
                                {

                                    ModifiedResponse = AddOrderDetailToXMLResponse_0(ProductCode, ObjXml.DocumentContent, ObjRequestAndResponseResults.pkOrderDetailId, ObjModel);
                                    ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);
                                    #region---INT-143 remove text from court house data
                                    string temprowcourtstring = string.Empty;
                                    temprowcourtstring = RemoveCourtHouseData(ModifiedResponse, ProductCode);
                                    if (!string.IsNullOrEmpty(temprowcourtstring))
                                    {
                                        ModifiedResponse = temprowcourtstring;
                                        ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);
                                    }
                                    #endregion
                                }
                            }
                            catch
                            {
                                ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);
                            }

                            #endregion Main XML Filteration

                            if (IsReview)//New check for mvc
                            {
                                if (Role.Contains("SystemAdmin") || Role.Contains("SupportTechnician") || Role.Contains("SupportManager"))//Request.Url.AbsoluteUri.ToLower().Contains("control")) //&& ObjRequestAndResponseResult.ReviewStatus != 0 //03Jun2011//Page.Request.Url.AbsoluteUri.ToLower().Contains("admin") ||
                                {
                                    if (hdnReviewDetail == "")
                                    {
                                        hdnReviewDetail = ObjRequestAndResponseResults.pkOrderDetailId + "_" + pcode + "_" + ObjRequestAndResponseResults.pkOrderResponseId;
                                    }
                                    else
                                    {
                                        hdnReviewDetail = hdnReviewDetail + "," + ObjRequestAndResponseResults.pkOrderDetailId + "_" + pcode + "_" + ObjRequestAndResponseResults.pkOrderResponseId;
                                    }

                                    ModifiedResponse = AddOrderDetailToXMLResponse(ProductCode, ObjXml.DocumentContent, ObjRequestAndResponseResults.pkOrderDetailId);
                                    ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);

                                    ObjClsViewReport.XSLTName = "~/Resources/AdminXSLT/" + (ProductCode == "SNS" && Request.Url.ToString().Contains("Control") ? ProductCode + "_CONTROL" : ProductCode) + ".xslt";
                                }
                                else
                                {
                                    ModifiedResponse = GetFinalXMLResponse(ProductCode, ObjXml.DocumentContent);
                                    ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);
                                    ObjClsViewReport.XSLTName = "~/Resources/ReviewXSLT/" + (ProductCode == "SNS" && Request.Url.ToString().Contains("Control") ? ProductCode + "_CONTROL" : ProductCode) + ".xslt";
                                }
                            }
                            else
                            {
                                ObjClsViewReport.XSLTName = "~/Resources/XSLT/" + (ProductCode == "SNS" && Request.Url.ToString().Contains("Control") ? ProductCode + "_CONTROL" : ProductCode) + ".xslt";
                            }


                            if (ProductCode == "NCR2")
                            {
                                ModifiedResponse = ModifiedResponse.Replace("&lt;td&gt;&lt;b&gt;Provider:&lt;/b&gt; Fastrax&lt;/td&gt;", "&lt;td&gt;&lt;b&gt;Provider:&lt;/b&gt; Emerge&lt;/td&gt;");

                            }

                            XmlDocument _XmlResponse = new XmlDocument();
                            _XmlResponse.LoadXml(ModifiedResponse);
                            ObjClsViewReport.XDocument = _XmlResponse;

                        }
                        else if (ObjRequestAndResponseResults.ReportStatus == Convert.ToByte("1") || ObjRequestAndResponseResults.ReportStatus == Convert.ToByte("6"))//INT-224 Force Pending changes
                        {
                            TopScript.Append(DisplayPendingMessage());
                        }
                        else if (ObjRequestAndResponseResults.ReportStatus == Convert.ToByte("4") && ObjRequestAndResponseResults.ProductCode == "NCR1")
                        {
                            TopScript.Append(DisplayNoResponseMessage());
                        }
                        else if (ObjRequestAndResponseResults.ReportStatus == Convert.ToByte("3"))
                        {
                            //TopScript.Append(DisplayInCompleteMessage());
                            if (ObjRequestAndResponseResults.ProductCode == "PSP")
                            {
                                XDocument erroXML = XDocument.Parse(ObjRequestAndResponseResults.ErrorLog);
                                var errorMessage = erroXML.Descendants("StatusDescription").FirstOrDefault().Value.ToString();
                                TopScript.Append(DisplayInCompleteMessageForPSP(errorMessage));
                            }
                            else
                            {
                                TopScript.Append(DisplayInCompleteMessage());
                            }
                        }
                        else if (ObjRequestAndResponseResults.ReportStatus == Convert.ToByte("4"))
                        {
                            TopScript.Append(DisplayCancelMessage(ObjRequestAndResponseResults));
                        }
                    }
                }
            }
            else
            {
                try
                {
                    TopScript.Append(ObjRequestAndResponseResults.ReportStatus == Convert.ToByte("1") ? DisplayPendingMessage() : ObjRequestAndResponseResults.ResponseData.ToString());
                }
                catch (Exception)
                {
                }
            }
            return hdnReviewDetail;
        }

        #endregion

        #region Fetch response Test Site

        //private string FetchResponse(Proc_Get_RequestAndResponseResult ObjRequestAndResponseResults, ClsViewReport ObjClsViewReport, StringBuilder TopScript, bool IsReview, bool IsAdmin, string[] Role, ViewReportModel ObjModel)
        //{
        //    Guid OrderDetailId = new Guid(ObjRequestAndResponseResults.pkOrderDetailId.ToString());
        //    int OrderId = new Guid(ObjRequestAndResponseResults.fkOrderId.ToString());
        //    string hdnReviewDetail = string.Empty;
        //    bool Objdynamic = GetResponse(ObjRequestAndResponseResults.ResponseData);
        //    if (Objdynamic == true)
        //    {
        //        Xml ObjXml = new Xml();
        //        string ProductCode = "";

        //        ObjXml.DocumentContent = ObjRequestAndResponseResults.ResponseData;
        //        int IsReportForReview = 0;
        //        // TopScript.Append(DisplayPendingMessage());
        //        ObjXml.DocumentContent = (ObjRequestAndResponseResults.ResponseData == null ? "" : ObjRequestAndResponseResults.ResponseData.ToString());
        //        if (ObjXml.DocumentContent.ToString() == "<XML_INTERFACE>ZZZ</XML_INTERFACE>" && ObjRequestAndResponseResults.IsFirstPendingRequest == true)
        //        {
        //            string FullRequest = Utility.GetLastRequest(ObjRequestAndResponseResults.RequestData);
        //            string[] Request1 = FullRequest.Split('?');
        //            string Uri_string = Request1[0];
        //            string Content_string = string.Empty;

        //            if (Request1.Length > 1)
        //            {
        //                Content_string = Request1[1];
        //            }
        //            string UriResponse = "<XML_INTERFACE>ZZZ</XML_INTERFACE>";
        //            while (UriResponse == "<XML_INTERFACE>ZZZ</XML_INTERFACE>" && MicroBiltEndPoint < 4)
        //            {
        //                UriResponse = Utility.SendRequest(Uri_string, Content_string);
        //                MicroBiltEndPoint++;
        //                System.Threading.Thread.Sleep(4000);
        //            }
        //            if (UriResponse.Contains("<XML_INTERFACE>"))
        //            {
        //                UpdateResponse((Guid)ObjRequestAndResponseResults.pkOrderResponseId, UriResponse);
        //                BALOrders ObjBALOrders = new BALOrders();
        //                try
        //                {
        //                    ObjBALOrders.CheckResponseandUpdateStatus(ObjRequestAndResponseResults.ProductCode, ObjRequestAndResponseResults.fkOrderId, UriResponse, ObjRequestAndResponseResults.pkOrderDetailId, false, string.Empty, string.Empty, string.Empty);
        //                }
        //                catch (Exception)
        //                {

        //                }
        //                finally
        //                {
        //                    ObjBALOrders = null;
        //                }
        //            }
        //            if (UriResponse == "<XML_INTERFACE>ZZZ</XML_INTERFACE>")
        //            {
        //                if (MicroBiltEndPoint == 4)
        //                {
        //                    TopScript.Append(DisplayPendingMessage());
        //                }
        //            }
        //            else
        //            {
        //                if (GetResponse(UriResponse) == true)
        //                {
        //                    if (ObjRequestAndResponseResults.IsLiveRunner == true)
        //                        ProductCode = ObjRequestAndResponseResults.ProductCode.Trim() != "RCX" ? ObjRequestAndResponseResults.ProductCode.Trim() + "LiveRunner" : ObjRequestAndResponseResults.ProductCode.Trim(); /* XSLT for live runner report*/
        //                    else
        //                        ProductCode = ObjRequestAndResponseResults.ProductCode;
        //                    ObjXml.DocumentContent = UriResponse;
        //                    ObjClsViewReport.XSLTName = "~/Resources/XSLT/" + (ProductCode == "SNS" && Request.Url.ToString().Contains("Control") ? ProductCode + "_CONTROL" : ProductCode) + ".xslt";

        //                }

        //            }
        //            BALGeneral.UpdateFirstPendingRequestStatus(ObjRequestAndResponseResults.pkOrderDetailId);
        //        }
        //        else
        //        {
        //            string pcode = "";
        //            if (ObjRequestAndResponseResults.ReportStatus == Convert.ToByte("2"))
        //            {
        //                string ModifiedResponse = string.Empty;
        //                ModifiedResponse = ObjRequestAndResponseResults.ResponseData;

        //                if (ObjRequestAndResponseResults.IsLiveRunner == true)
        //                {
        //                    if (ObjRequestAndResponseResults.ProductCode.Trim() == "CCR1" || ObjRequestAndResponseResults.ProductCode.Trim() == "CCR2" || ObjRequestAndResponseResults.ProductCode.Trim() == "SCR")
        //                    {
        //                        pcode = "RCX";
        //                    }
        //                    else
        //                    {
        //                        pcode = ObjRequestAndResponseResults.ProductCode.Trim();
        //                    }
        //                    ProductCode = ObjRequestAndResponseResults.ProductCode.Trim() != "RCX" ? ObjRequestAndResponseResults.ProductCode.Trim() + "LiveRunner" : ObjRequestAndResponseResults.ProductCode.Trim(); /* XSLT for live runner report*/

        //                }
        //                else
        //                {
        //                    ProductCode = ObjRequestAndResponseResults.ProductCode;
        //                    pcode = ProductCode;
        //                }

        //                #region for live

        //                /* Match meter for NCR1 Report */


        //                string ncr1Response = "";
        //                bool IsNCR1Report = false;
        //                Guid pkNCR1OrderResponseId = Guid.Empty;
        //                BALEmergeReview ObjBALEmergeReview = new BALEmergeReview();
        //                bool IsMatchMeter = false;

        //                if (ObjRequestAndResponseResults.ProductCode.ToLower() == "ps")
        //                {
        //                    if (ObjRequestAndResponseResults.ResponseData != "")
        //                    {
        //                        PSResponse = ObjRequestAndResponseResults.ResponseData;
        //                        vwpsStatus = 1;
        //                        XmlDocument xmlDoc = new XmlDocument();
        //                        xmlDoc.LoadXml(PSResponse);
        //                        XmlNodeList filelst = xmlDoc.GetElementsByTagName("County");
        //                        System.Web.HttpContext.Current.Session["CountyExist"] = "2";
        //                        for (int i = 0; i < filelst.Count; i++)
        //                        {
        //                            XmlElement afile = (XmlElement)filelst[i];
        //                            string County = afile.FirstChild.Value;
        //                            if (string.IsNullOrEmpty(County) == false) { System.Web.HttpContext.Current.Session["CountyExist"] = "1"; break; }

        //                        }



        //                    }
        //                }

        //                /* Match meter for NCR1 Report */
        //                if (ProductCode == "NCR1" || ProductCode == "SCR" || ProductCode == "NCR+" || ProductCode == "RCX" || ProductCode == "NCR4" || ProductCode == "CCR1LiveRunner" || ProductCode == "CCR2LiveRunner")
        //                {

        //                    ModifiedResponse = AddOrderDetailToXMLResponse_0(ProductCode, ObjXml.DocumentContent, ObjRequestAndResponseResults.pkOrderDetailId, ObjModel);
        //                    ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);
        //                    ModifiedResponse = AddOrderDetailToXMLResponse_1(ProductCode, ObjXml.DocumentContent, ObjRequestAndResponseResults.pkOrderDetailId);

        //                    BALOrders ObjBALOrders = new BALOrders();
        //                    ObjBALOrders.UpdateOrderResponse(ObjRequestAndResponseResults.pkOrderDetailId, ModifiedResponse);


        //                    #region MatchMeter

        //                    if (ObjRequestAndResponseResults.ProductCode.ToLower() == "ncr1")
        //                    {
        //                        if (Role.Contains("SystemAdmin") && ObjRequestAndResponseResults.pkOrderDetailId == new Guid("05DCACFE-32AE-E211-94A6-047D7BF23350"))//"4bab73ee-96c1-e211-a076-002590742e90"
        //                        {
        //                            ncr1Response = ObjRequestAndResponseResults.FilteredResponseData;
        //                        }
        //                        else
        //                        {
        //                            ncr1Response = ObjRequestAndResponseResults.ResponseData;
        //                        }
        //                        IsMatchMeter = ObjRequestAndResponseResults.IsMatchMeter;
        //                        if (ncr1Response != "")
        //                        {
        //                            IsNCR1Report = true;
        //                        }
        //                        pkNCR1OrderResponseId = ObjRequestAndResponseResults.pkOrderResponseId;
        //                    }
        //                    if (IsNCR1Report == true && pkNCR1OrderResponseId != Guid.Empty)
        //                    {
        //                        if (IsMatchMeter == false)
        //                        {
        //                            bool isadmin = false;
        //                            if (Role.Contains("SystemAdmin") || Role.Contains("SupportTechnician") || Role.Contains("SupportManager"))
        //                            {
        //                                isadmin = true;
        //                            }
        //                            else
        //                            {
        //                                isadmin = false;
        //                            }
        //                            if (vwpsStatus == 1)
        //                            {
        //                                string updatedResponse = ObjBALEmergeReview.matchmeter(ObjRequestAndResponseResults.SearchedFirstName, ObjRequestAndResponseResults.SearchedLastName, ObjRequestAndResponseResults.SearchedMiddleInitial, ObjRequestAndResponseResults.SearchedDob, PSResponse, ncr1Response, pkNCR1OrderResponseId, ObjRequestAndResponseResults.ProductCode, OrderDetailId, OrderId, isadmin, ObjRequestAndResponseResults.best_SearchedZipcode, ObjRequestAndResponseResults.best_search_state, ObjRequestAndResponseResults.best_SearchedCity, ObjRequestAndResponseResults.best_SearchedStreetAddress + " " + ObjRequestAndResponseResults.best_SearchedStreetName, ObjRequestAndResponseResults.IsAddressUpdated);
        //                                ModifiedResponse = updatedResponse;
        //                            }
        //                            else
        //                            {
        //                                string updatedResponse = ObjBALEmergeReview.NCR1matchmeter(ObjRequestAndResponseResults.SearchedFirstName, ObjRequestAndResponseResults.SearchedLastName, ObjRequestAndResponseResults.SearchedMiddleInitial, ObjRequestAndResponseResults.SearchedDob, ObjRequestAndResponseResults.SearchedZipCode, ObjRequestAndResponseResults.SearchedStateCode, ObjRequestAndResponseResults.SearchedCity, ObjRequestAndResponseResults.SearchedStreetAddress + " " + ObjRequestAndResponseResults.SearchedStreetName, ncr1Response, pkNCR1OrderResponseId, ObjRequestAndResponseResults.ProductCode, OrderDetailId, OrderId, isadmin);
        //                                ModifiedResponse = updatedResponse;
        //                            }
        //                        }
        //                    }
        //                    #endregion

        //                    #region Ticket #800: 7 YEAR STATES

        //                    //bool IsShowRawData = true;
        //                    //ModifiedResponse = objBALEmergeReview.HideSevenYear(ModifiedResponse, ObjRequestAndResponseResult.pkOrderResponseId, pcode, DateTime.Now, out IsShowRawData);
        //                    //if (Roles.IsUserInRole(Page.User.Identity.Name, "SystemAdmin") || Roles.IsUserInRole(Page.User.Identity.Name, "SupportTechnician") || Roles.IsUserInRole(Page.User.Identity.Name, "SupportManager"))
        //                    //{ }
        //                    //else 
        //                    //{ 
        //                    //    LblIsShowRawData.Text = IsShowRawData.ToString(); 
        //                    //}
        //                    //ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);
        //                    #endregion

        //                }


        //                #endregion

        //                #region for test
        //                ///* Match meter for NCR1 Report */


        //                //string ResponseData = "";
        //                //bool IsNCR1Report = false;
        //                //Guid pkOrderResponseId = Guid.Empty;
        //                //BALEmergeReview ObjBALEmergeReview = new BALEmergeReview();
        //                //bool IsMatchMeter = false;

        //                //if (ObjRequestAndResponseResults.ProductCode.ToLower() == "ps")
        //                //{
        //                //    if (ObjRequestAndResponseResults.ResponseData != "")
        //                //    {
        //                //        PSResponse = ObjRequestAndResponseResults.ResponseData;
        //                //        vwpsStatus = 1;
        //                //        XmlDocument xmlDoc = new XmlDocument();
        //                //        xmlDoc.LoadXml(PSResponse);
        //                //        XmlNodeList filelst = xmlDoc.GetElementsByTagName("County");
        //                //        System.Web.HttpContext.Current.Session["CountyExist"] = "2";
        //                //        for (int i = 0; i < filelst.Count; i++)
        //                //        {
        //                //            XmlElement afile = (XmlElement)filelst[i];
        //                //            string County = afile.FirstChild.Value;
        //                //            if (string.IsNullOrEmpty(County) == false) { System.Web.HttpContext.Current.Session["CountyExist"] = "1"; break; }

        //                //        }
        //                //    }
        //                //}

        //                ///* Match meter for NCR1 Report */
        //                //if (ProductCode == "NCR1" || ProductCode == "SCR" || ProductCode == "NCR+" || ProductCode == "RCX" || ProductCode == "NCR4" || ProductCode == "CCR1LiveRunner" || ProductCode == "CCR2LiveRunner")
        //                //{
        //                //    ModifiedResponse = AddOrderDetailToXMLResponse_0(ProductCode, ObjXml.DocumentContent, ObjRequestAndResponseResults.pkOrderDetailId, ObjModel);
        //                //    ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);
        //                //    ModifiedResponse = AddOrderDetailToXMLResponse_1(ProductCode, ObjXml.DocumentContent, ObjRequestAndResponseResults.pkOrderDetailId);

        //                //    BALOrders ObjBALOrders = new BALOrders();
        //                //    ObjBALOrders.UpdateOrderResponse(ObjRequestAndResponseResults.pkOrderDetailId, ModifiedResponse);

        //                //    #region Ticket #800: 7 YEAR STATES

        //                //    //bool IsShowRawData = true;
        //                //    //ModifiedResponse = objBALEmergeReview.HideSevenYear(ModifiedResponse, ObjRequestAndResponseResult.pkOrderResponseId, pcode, DateTime.Now, out IsShowRawData);
        //                //    //if (Roles.IsUserInRole(Page.User.Identity.Name, "SystemAdmin") || Roles.IsUserInRole(Page.User.Identity.Name, "SupportTechnician") || Roles.IsUserInRole(Page.User.Identity.Name, "SupportManager"))
        //                //    //{ }
        //                //    //else 
        //                //    //{ 
        //                //    //    LblIsShowRawData.Text = IsShowRawData.ToString(); 
        //                //    //}
        //                //    //ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);
        //                //    #endregion

        //                //}
        //                //#region MatchMeter

        //                ////if (ObjRequestAndResponseResults.ProductCode.ToLower() == "ncr1")
        //                ////{
        //                ////ResponseData = ObjRequestAndResponseResults.ResponseData;
        //                //if (Role.Contains("SystemAdmin") && ObjRequestAndResponseResults.pkOrderDetailId == new Guid("4bab73ee-96c1-e211-a076-002590742e90"))//"05DCACFE-32AE-E211-94A6-047D7BF23350"
        //                //{
        //                //    ResponseData = ObjRequestAndResponseResults.FilteredResponseData;
        //                //}
        //                //else
        //                //{
        //                //    ResponseData = ObjRequestAndResponseResults.ResponseData;
        //                //}
        //                //IsMatchMeter = ObjRequestAndResponseResults.IsMatchMeter;
        //                ////if (ncr1Response != "")
        //                //// {
        //                ////IsNCR1Report = true;
        //                ////}
        //                //pkOrderResponseId = ObjRequestAndResponseResults.pkOrderResponseId;
        //                ////}
        //                //if (pkOrderResponseId != Guid.Empty)
        //                //{
        //                //    if (IsMatchMeter == false)
        //                //    {
        //                //        bool isadmin = false;
        //                //        if (Role.Contains("SystemAdmin") || Role.Contains("SupportTechnician") || Role.Contains("SupportManager"))
        //                //        {
        //                //            isadmin = true;
        //                //        }
        //                //        else
        //                //        {
        //                //            isadmin = false;
        //                //        }
        //                //        if (vwpsStatus == 1)
        //                //        {
        //                //            string updatedResponse = ObjBALEmergeReview.matchmeterwithps(ObjRequestAndResponseResults.SearchedFirstName, ObjRequestAndResponseResults.SearchedLastName, ObjRequestAndResponseResults.SearchedMiddleInitial, ObjRequestAndResponseResults.SearchedDob, PSResponse, ResponseData, pkOrderResponseId, pcode, OrderDetailId, OrderId, isadmin, ObjRequestAndResponseResults.best_SearchedZipcode, ObjRequestAndResponseResults.best_search_state, ObjRequestAndResponseResults.best_SearchedCity, ObjRequestAndResponseResults.best_SearchedStreetAddress + " " + ObjRequestAndResponseResults.best_SearchedStreetName, ObjRequestAndResponseResults.IsAddressUpdated, ObjRequestAndResponseResults.SearchedSsn, ObjRequestAndResponseResults.SearchedSex);
        //                //            ModifiedResponse = updatedResponse;
        //                //        }
        //                //        else
        //                //        {
        //                //            string updatedResponse = ObjBALEmergeReview.matchmeterwithoutps(ObjRequestAndResponseResults.SearchedFirstName, ObjRequestAndResponseResults.SearchedLastName, ObjRequestAndResponseResults.SearchedMiddleInitial, ObjRequestAndResponseResults.SearchedDob, ObjRequestAndResponseResults.SearchedZipCode, ObjRequestAndResponseResults.SearchedStateCode, ObjRequestAndResponseResults.SearchedCity, ObjRequestAndResponseResults.SearchedStreetAddress + " " + ObjRequestAndResponseResults.SearchedStreetName, ResponseData, pkOrderResponseId, pcode, OrderDetailId, OrderId, isadmin, ObjRequestAndResponseResults.SearchedSsn, ObjRequestAndResponseResults.SearchedSex);
        //                //            ModifiedResponse = updatedResponse;
        //                //        }
        //                //    }
        //                //}
        //                //#endregion

        //                #endregion


        //                #region Main XML Filteration

        //                bool IsShowRawData = true;
        //                XMLFilteration ObjXMLFilteration = new XMLFilteration();
        //                try
        //                {
        //                    if (ObjRequestAndResponseResults.pkOrderDetailId == new Guid("4bab73ee-96c1-e211-a076-002590742e90"))//"05DCACFE-32AE-E211-94A6-047D7BF23350"
        //                    {
        //                        if (Role.Contains("SystemAdmin"))
        //                        {
        //                            IsShowRawData = true;
        //                        }
        //                        else
        //                        {
        //                            string UpdatedResponse = ObjXMLFilteration.GetFilteredXML(ProductCode, pcode, ModifiedResponse, ObjRequestAndResponseResults.pkOrderDetailId, ObjRequestAndResponseResults.WatchListStatus, DateTime.Now, ObjRequestAndResponseResults.IsLiveRunner, out IsShowRawData);
        //                            ModifiedResponse = UpdatedResponse;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        string UpdatedResponse = ObjXMLFilteration.GetFilteredXML(ProductCode, pcode, ModifiedResponse, ObjRequestAndResponseResults.pkOrderDetailId, ObjRequestAndResponseResults.WatchListStatus, DateTime.Now, ObjRequestAndResponseResults.IsLiveRunner, out IsShowRawData);
        //                        ModifiedResponse = UpdatedResponse;
        //                    }

        //                    #region For 7 Year States
        //                    if (Role.Contains("SystemAdmin") || Role.Contains("SupportTechnician") || Role.Contains("SupportManager"))
        //                    {
        //                        ObjModel.IsShowRawData = true;
        //                    }
        //                    else
        //                    {
        //                        ObjModel.IsShowRawData = false;
        //                        //ObjModel.IsShowRawData = IsShowRawData;
        //                    }
        //                    #endregion For 7 Year States

        //                    ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);
        //                    if (ProductCode == "NCR1" || ProductCode == "SCR" || ProductCode == "NCR+" || ProductCode == "RCX" || ProductCode == "NCR4" || ProductCode == "CCR1LiveRunner" || ProductCode == "CCR2LiveRunner")
        //                    {

        //                        ModifiedResponse = AddOrderDetailToXMLResponse_0(ProductCode, ObjXml.DocumentContent, ObjRequestAndResponseResults.pkOrderDetailId, ObjModel);
        //                        ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);
        //                    }
        //                }
        //                catch
        //                {
        //                    ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);
        //                }

        //                #endregion Main XML Filteration

        //                if (IsReview)//New check for mvc
        //                {
        //                    if (Role.Contains("SystemAdmin") || Role.Contains("SupportTechnician") || Role.Contains("SupportManager"))//Request.Url.AbsoluteUri.ToLower().Contains("control")) //&& ObjRequestAndResponseResult.ReviewStatus != 0 //03Jun2011//Page.Request.Url.AbsoluteUri.ToLower().Contains("admin") ||
        //                    {
        //                        if (hdnReviewDetail == "")
        //                        {
        //                            hdnReviewDetail = ObjRequestAndResponseResults.pkOrderDetailId + "_" + pcode + "_" + ObjRequestAndResponseResults.pkOrderResponseId;
        //                        }
        //                        else
        //                        {
        //                            hdnReviewDetail = hdnReviewDetail + "," + ObjRequestAndResponseResults.pkOrderDetailId + "_" + pcode + "_" + ObjRequestAndResponseResults.pkOrderResponseId;
        //                        }

        //                        ModifiedResponse = AddOrderDetailToXMLResponse(ProductCode, ObjXml.DocumentContent, ObjRequestAndResponseResults.pkOrderDetailId);
        //                        ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);

        //                        ObjClsViewReport.XSLTName = "~/Resources/AdminXSLT/" + (ProductCode == "SNS" && Request.Url.ToString().Contains("Control") ? ProductCode + "_CONTROL" : ProductCode) + ".xslt";
        //                    }
        //                    else
        //                    {
        //                        ModifiedResponse = GetFinalXMLResponse(ProductCode, ObjXml.DocumentContent);
        //                        ObjXml.DocumentContent = Convert.ToString(ModifiedResponse);
        //                        ObjClsViewReport.XSLTName = "~/Resources/ReviewXSLT/" + (ProductCode == "SNS" && Request.Url.ToString().Contains("Control") ? ProductCode + "_CONTROL" : ProductCode) + ".xslt";
        //                    }
        //                }
        //                else
        //                {
        //                    ObjClsViewReport.XSLTName = "~/Resources/XSLT/" + (ProductCode == "SNS" && Request.Url.ToString().Contains("Control") ? ProductCode + "_CONTROL" : ProductCode) + ".xslt";
        //                }
        //                if (ProductCode == "NCR2")
        //                {
        //                    ModifiedResponse = ModifiedResponse.Replace("&lt;td&gt;&lt;b&gt;Provider:&lt;/b&gt; Fastrax&lt;/td&gt;", "&lt;td&gt;&lt;b&gt;Provider:&lt;/b&gt; Emerge&lt;/td&gt;");

        //                }
        //                XmlDocument _XmlResponse = new XmlDocument();
        //                _XmlResponse.LoadXml(ModifiedResponse);
        //                ObjClsViewReport.XDocument = _XmlResponse;

        //            }
        //            else if (ObjRequestAndResponseResults.ReportStatus == Convert.ToByte("1"))
        //            {
        //                TopScript.Append(DisplayPendingMessage());
        //            }
        //            else if (ObjRequestAndResponseResults.ReportStatus == Convert.ToByte("4") && ObjRequestAndResponseResults.ProductCode == "NCR1")
        //            {
        //                TopScript.Append(DisplayNoResponseMessage());
        //            }
        //            else if (ObjRequestAndResponseResults.ReportStatus == Convert.ToByte("3"))
        //            {
        //                //TopScript.Append(DisplayInCompleteMessage());
        //                if (ObjRequestAndResponseResults.ProductCode == "PSP")
        //                {
        //                    XDocument erroXML = XDocument.Parse(ObjRequestAndResponseResults.ErrorLog);
        //                    var errorMessage = erroXML.Descendants("StatusDescription").FirstOrDefault().Value.ToString();
        //                    TopScript.Append(DisplayInCompleteMessageForPSP(errorMessage));
        //                }
        //                else
        //                {
        //                    TopScript.Append(DisplayInCompleteMessage());
        //                }
        //            }
        //            else if (ObjRequestAndResponseResults.ReportStatus == Convert.ToByte("4"))
        //            {
        //                TopScript.Append(DisplayCancelMessage(ObjRequestAndResponseResults));
        //            }
        //        }
        //    }
        //    else
        //    {
        //        try
        //        {
        //            TopScript.Append(ObjRequestAndResponseResults.ReportStatus == Convert.ToByte("1") ? DisplayPendingMessage() : ObjRequestAndResponseResults.ResponseData.ToString());
        //        }
        //        catch (Exception)
        //        {
        //        }
        //    }
        //    return hdnReviewDetail;
        //}

        #endregion

        #region Final Response Editing For Users Other Than Admin

        private string GetFinalXMLResponse(string ProductCode, string ResponseXML)
        {
            string FinalResponse = "";
            string RootRecordNode = "";
            if (ProductCode.ToUpper().Trim() == "NCR1" || ProductCode.ToUpper().Trim() == "PS" || ProductCode.ToUpper().Trim() == "RCX" || ProductCode.ToUpper().Trim() == "FCR" || ProductCode.ToUpper().Trim() == "NCR+" || ProductCode.ToUpper().Trim() == "CCR1LIVERUNNER" || ProductCode.ToUpper().Trim() == "CCR2LIVERUNNER" || ProductCode.ToUpper().Trim() == "SCRLIVERUNNER")
            {
                string XML_Value = GetRequiredXml_ForRapidCourt(ResponseXML);
                XDocument xmldoc = XDocument.Parse(XML_Value);
                List<XElement> xElement = new List<XElement>();

                //Changed By Himesh Kumar
                if (ProductCode.ToUpper().Trim() == "PS")
                {
                    RootRecordNode = "record";
                    if (xmldoc.Descendants().Count() > 0)
                    {
                        FinalResponse = ResponseXML;
                    }
                }
                else
                {
                    RootRecordNode = "CriminalCase";
                    if (xmldoc.Descendants().Count() > 0)
                    {
                        var elementToDelete = xmldoc.Descendants(RootRecordNode).Where(x => x.Attribute("Deleted") != null).ToList();
                        if (elementToDelete.Count > 0)
                        {
                            xElement = elementToDelete.Where(x => x.Attribute("Deleted").Value == "1").ToList();
                            xElement.Remove();
                        }
                        int TrimedUpTo = ResponseXML.IndexOf("<BackgroundReportPackage>");
                        StringBuilder CompleteResponse = new StringBuilder();
                        string XMLInitials = ResponseXML.Substring(0, TrimedUpTo);
                        CompleteResponse.Append(XMLInitials);
                        CompleteResponse.Append(Convert.ToString(xmldoc));
                        CompleteResponse.Append("</BackgroundReports>");
                        FinalResponse = Convert.ToString(CompleteResponse);// Convert.ToString(CompleteResponse).Replace("&lt;", "<").Replace("&gt;", ">");
                    }
                    else
                        FinalResponse = ResponseXML;
                }
            }
            else if (ProductCode.ToUpper() == "TDR")
            {
                FinalResponse = ModifyTDRReport(ResponseXML);
            }
            else
            {
                switch (ProductCode.ToUpper())
                {
                    case "SCR":
                        RootRecordNode = "result";
                        break;
                    case "CCR1":
                        RootRecordNode = "report-incident";
                        break;
                    case "CCR2":
                        RootRecordNode = "detail";
                        break;
                    case "OFAC":
                        RootRecordNode = "response_row";
                        break;
                    case "PS2":
                        RootRecordNode = "response_row";
                        break;
                    case "SOR":
                        RootRecordNode = "offender";
                        break;
                    case "SSR":
                        RootRecordNode = "response_row";
                        break;
                }
                FinalResponse = ModifyReport(ResponseXML, RootRecordNode);
            }
            return FinalResponse;
        }

        private string ModifyTDRReport(string ResponseXML)
        {
            string FinalResponse = "";
            XDocument xmldoc = XDocument.Parse(ResponseXML);
            List<XElement> xElement = new List<XElement>();
            if (xmldoc.Descendants("address").Count() > 0 || xmldoc.Descendants("identity").Count() > 0 || xmldoc.Descendants("neighborhood").Count() > 0)
            {
                if (xmldoc.Descendants("address").Count() > 0)
                {
                    var elementToDelete = xmldoc.Descendants("address").Where(x => x.Attribute("Deleted") != null).ToList();
                    if (elementToDelete.Count > 0)
                    {
                        xElement = elementToDelete.Where(x => x.Attribute("Deleted").Value == "1").ToList();
                        xElement.Remove();
                    }
                }
                if (xmldoc.Descendants("identity").Count() > 0)
                {
                    var elementToDelete = xmldoc.Descendants("identity").Where(x => x.Attribute("Deleted") != null).ToList();
                    if (elementToDelete.Count > 0)
                    {
                        xElement = elementToDelete.Where(x => x.Attribute("Deleted").Value == "1").ToList();
                        xElement.Remove();
                    }
                }
                if (xmldoc.Descendants("neighborhood").Count() > 0)
                {
                    var elementToDelete = xmldoc.Descendants("neighborhood").Where(x => x.Attribute("Deleted") != null).ToList();
                    if (elementToDelete.Count > 0)
                    {
                        xElement = elementToDelete.Where(x => x.Attribute("Deleted").Value == "1").ToList();
                        xElement.Remove();
                    }
                }
                FinalResponse = Convert.ToString(xmldoc);
            }
            else
                FinalResponse = ResponseXML;
            return FinalResponse;
        }

        private string ModifyReport(string ResponseXML, string XMLElement)
        {
            string FinalResponse = "";
            XDocument xmldoc = XDocument.Parse(ResponseXML);
            List<XElement> xElement = new List<XElement>();
            if (XMLElement != "")
            {
                if (xmldoc.Descendants(XMLElement).Count() > 0)
                {
                    var elementToDelete = xmldoc.Descendants(XMLElement).Where(x => x.Attribute("Deleted") != null).ToList();
                    if (elementToDelete.Count > 0)
                    {
                        xElement = elementToDelete.Where(x => x.Attribute("Deleted").Value == "1").ToList();
                        xElement.Remove();
                    }
                    FinalResponse = Convert.ToString(xmldoc);
                }
                else
                    FinalResponse = ResponseXML;
            }
            else
                FinalResponse = ResponseXML;

            return FinalResponse;
        }
        #endregion

        private string AddOrderDetailToXMLResponse(string ProductCode, string XmlResponse, int pkOrderDetailId)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(XmlResponse);
            XmlElement root = xmlDoc.DocumentElement;
           // XmlNode rootNode = xmlDoc.SelectSingleNode(root.Name.ToString());
            XmlAttribute newAttribute = xmlDoc.CreateAttribute("OrderDetailId");
            newAttribute.Value = Convert.ToString(pkOrderDetailId);
            root.Attributes.Append(newAttribute);


            return xmlDoc.OuterXml.ToString();

        }

        private string GenerateApplicantName(string sLastname, string sSufix, string sMiddle, string sFirstName)
        {
            string applicantName = string.Empty;
            int getstr = 0, ilen = 0;
            applicantName = (sLastname != "") ? sLastname + ", " : "";
            applicantName += (sSufix != "") ? sSufix + ", " : "";
            applicantName += (sMiddle != "") ? sMiddle + ", " : "";
            applicantName += (sFirstName != "") ? sFirstName + ", " : "";

            ilen = applicantName.LastIndexOf(",");
            getstr = ilen + 2;
            if (getstr == applicantName.Length)
            {
                applicantName = applicantName.Substring(0, ilen);
            }
            return applicantName;
        }

        private bool GetResponse(string p)
        {
            XmlDocument ObjXmlDocument = new XmlDocument();
            try
            {
                ObjXmlDocument.LoadXml(p);
                return true;
            }
            catch// (Exception ex)
            {
                return false;
            }
        }

        private string DisplayPendingMessage()
        {
            return "<div id='Pending_message' align='left' style='font-size:14px;padding-left: 2%;'><div style='clear:both;'>STATUS : PENDING<hr align='left' id='Pending_Message_Hr' /></div  ><div class=\"sectionsummary\">      <h3 style=\"line-height: 1em;\">        Your report is being processed manually.  <br />   An Email Notification will be sent to you when the request is completed.   <br /> Contact our support team if you have any questions. <br/> <span class='Pending_Message_Separator'> </span> <a href='mailto:support@intelifi.com' class='Pending_Message_Link'>support@intelifi.com</a> <span class='Pending_Message_Separator'> |</span> 888.409.1819 Mon - Fri 6am-5pm PST </h3></div></div>";
        }

        private string DisplayInCompleteMessage()
        {
            return "<div id='Pending_message' align='left' style='font-size:14px;padding-left: 2%;'><div style='clear:both;'>STATUS : <span style='color:red'>Incomplete</span> <hr align='left' id='Pending_Message_Hr' /></div><div class=\"sectionsummary\">      <h3 style=\"line-height: 1em;\"> Your order is taking longer than expected to complete and is still being processed.<br/>  Please contact support for more information on this matter.    <br />  Emerge support Team <span class='Pending_Message_Separator'> |</span> <a href='mailto:support@intelifi.com' class='Pending_Message_Link'>support@intelifi.com</a> <span class='Pending_Message_Separator'> |</span> 888.409.1819</h3> </div></div>";
        }

        private string DisplayInCompleteMessageForPSP(String Message)
        {

            string IncompleteMessage = "<div id='Pending_message' align='left' style='font-size:14px;padding-left: 2%;'><div style='clear:both;'>STATUS : <span style='color:red'>Incomplete</span> <hr align='left' id='Pending_Message_Hr' /></div><div>  Your order is not completed due to following reason:<br/> " + Message + "<br/> Please contact support for more information on this matter.    <br />  Emerge support Team <span class='Pending_Message_Separator'> |</span> <a href='mailto:support@intelifi.com' class='Pending_Message_Link'>support@intelifi.com</a> <span class='Pending_Message_Separator'> |</span> 888.409.1819 </div></div>";
            return IncompleteMessage;
        }

        private string DisplayInReviewMessage()
        {
            return "<div style='width:59%;' align='left' style='font-size:14px;padding-left: 2%;'><p align='left' style='font-size:10'><span style='color:#8f9261'>STATUS: IN EMERGE REVIEW</span></p><p align='justify' style='font-size:10'><span style='color:red'>Intelifi’s support team is reviewing your report. This means there have been possible name and/or date of birth variations found in the report, and they must be reviewed for accuracy to determine if they match to the person you are searching.</span></p><p align='justify' style='font-size:10'><span style='color:red'>Results will be posted in less than 24 hours. When your report is ready, you will be notified via email to login and check your report. Please call support at 888-409-1819, Monday through Friday, between the hours of 6:00 AM and 5:30 PM PST if you have any questions or need immediate assistance.</span></p><p align='justify' style='font-size:10'><span style='color:red'><b>NOTE: You may receive an e-mail requesting additional information to expedite your results. Emerge Review has been put in place to assure we provide you the most accurate and up-to-date information available. Thank you for your patience.<b></span></p></div>";
        }




        /// <summary>
        /// Function To Display Response Image for pending report.
        /// </summary>
        /// <returns></returns>
        public string DisplayManualResponse(Proc_Get_RequestAndResponseResult ObjRequestAndResponseResult)
        {

            StringBuilder sbManualResponseData = new StringBuilder();
            string manualresponse = string.Empty;


            if (ObjRequestAndResponseResult.ManualResponse != string.Empty)
            {
                if (ObjRequestAndResponseResult.ManualResponse.Contains("<img alt='ResponseImage'"))
                {
                    sbManualResponseData = sbManualResponseData.Append(ObjRequestAndResponseResult.ManualResponse);
                }
                else
                {
                    sbManualResponseData = sbManualResponseData.Append(ObjRequestAndResponseResult.ManualResponse.Replace("?", " "));
                }
                manualresponse += "<div id='Pending_message' align='left'><div style='clear:both;'></div><div>" + sbManualResponseData + "</div></div>";
            }
            if (ObjRequestAndResponseResult.ResponseFileName != string.Empty)
            {
               // string FilePath = "";
                int checkext = -1;
                using (EmergeDALDataContext DX = new EmergeDALDataContext())
                {
                    tblOrderResponseData ObjOrderresponseData = DX.tblOrderResponseDatas.Where(d => d.pkOrderResponseId == ObjRequestAndResponseResult.pkOrderResponseId).FirstOrDefault();
                    if (ObjOrderresponseData != null)
                    {
                        string FileName = ObjOrderresponseData.ResponseFileName;
                        string Ext = Path.GetExtension(FileName).ToLower();
                        if (Ext == ".htm" || Ext == ".html" || Ext == ".pdf")
                        {
                            checkext = 1;
                        }
                        else
                        {
                            checkext = 2;
                        }
                        if (!string.IsNullOrEmpty(FileName))
                        {
                            manualresponse += "<div align='left'><div style='clear:both;'></div><div><a href=\"DownloadAttachment/?FPathTitle=" + FileName + "," + ObjRequestAndResponseResult.ProductCode + "\">Click here to view attachment..</a></div></div>";
                        }
                        //FilePath = ApplicationPath.GetApplicationPath() + "Resources/Upload/ResponseImages/" + FileName;
                        //manualresponse += "<div align='left'><div style='clear:both;'></div><div><a class='easyui-linkbutton' href='#' onclick=\"addTab('" + ObjRequestAndResponseResult.ProductCode + "','" + FilePath + "','" + checkext + "'); \">Click here to view attachment..</a></div></div>";
                    }
                }
            }


            return manualresponse;
        }

        public FilePathResult DownloadAttachment(string FPathTitle)
        {
            string[] str = FPathTitle.Split(',');
            string FileName = str[0];
            string Title = str[1].Replace(" ", "_");
            string FilePath = Request.PhysicalApplicationPath + "Resources/Upload/ResponseImages/" + FileName;
            string fileext = System.IO.Path.GetExtension(FileName);
            string CType = "application/" + fileext.Replace(".", "");
            //string fileName = Path.GetFileName(FileName);

            string strFileTitle = Title + fileext;



            if (System.IO.File.Exists(FilePath))
            {
                return File(FilePath, CType, strFileTitle);
            }
            else
            {
                FilePath = Request.PhysicalApplicationPath + "Resources/Upload/FeatureFiles/no-image.jpg";
                return File(FilePath, CType, strFileTitle);
            }
        }

        /// <summary>
        /// Function To Display uploaded Response Link for pending report.
        /// </summary>
        /// <returns></returns>
        public string DisplayResponseFileLink(Proc_Get_RequestAndResponseResult ObjRequestAndResponseResult)
        {
            return "<div align='left'><div style='clear:both;'></div><div><a href=\"javascript:OpenReportTab('" + ObjRequestAndResponseResult.pkOrderResponseId + "','" + ObjRequestAndResponseResult.ProductCode + "');\">Click here</a></div></div>";
        }


        private string DisplayCancelMessage(Proc_Get_RequestAndResponseResult ObjRequestAndResponseResult)
        {

            string Reason = "";
            switch (ObjRequestAndResponseResult.ProductCode.ToLower())
            {
                case "ps":
                    Reason = GetRapidCourtReasonToCancelReport(ObjRequestAndResponseResult.ResponseData);
                    break;
                case "NCR+":
                    Reason = GetRapidCourtReasonToCancelReport(ObjRequestAndResponseResult.ResponseData);
                    break;
                case "rcx":
                    Reason = GetRapidCourtReasonToCancelReport(ObjRequestAndResponseResult.ResponseData);
                    break;
                case "fcr":
                    Reason = GetRapidCourtReasonToCancelReport(ObjRequestAndResponseResult.ResponseData);
                    break;
                case "ncr1":
                    Reason = GetRapidCourtReasonToCancelReport(ObjRequestAndResponseResult.ResponseData);
                    break;
                //case "ncr1":
                //    Reason = GetRapidCourtReasonToCancelReport(ObjRequestAndResponseResult.ResponseData);
                //    break;
            }
            return Reason;

        }

        private string GetRapidCourtReasonToCancelReport(string ResponseData)
        {
            string Specific_Reason = "<pre>";
            string XML_Value = GetRequiredXml_ForRapidCourt(ResponseData);
            XDocument ObjXDocument = XDocument.Parse(XML_Value);
            var List_Reason = ObjXDocument.Descendants("Text").ToList();
            foreach (XElement ObjXElement in List_Reason)
            {
                if (ObjXElement != null)
                {
                    Specific_Reason += ObjXElement.Value;
                    break;
                }
            }
            return Specific_Reason + "</pre>";
        }

        public static string GetRequiredXml_ForRapidCourt(string XmlResponse)
        {
            string XmlResponse_Local = XmlResponse;
            XmlResponse_Local = XmlResponse_Local.Replace("</BackgroundReports>", "");
            int xmlstartindex = XmlResponse_Local.IndexOf("<BackgroundReportPackage>");
            if (xmlstartindex > -1)//INT96
            {
                XmlResponse_Local = XmlResponse_Local.Substring(xmlstartindex);
            }
            return XmlResponse_Local;
        }

        private string AddOrderDetailToXMLResponse_0(string ProductCode, string XmlResponse, int pkOrderDetailId, ViewReportModel ObjModel)
        {
            #region Here we set the null DOB at te end of xml doc

            string FinalResponse = "";
            string DeletedRecords = "";
            try
            {
                string XML_Value = GetRequiredXml_ForRapidCourt(XmlResponse);
                XElement yourDoc = XElement.Parse(XML_Value);
                int RemainingNUllDOB = 0;
                int Totalcount = 0;
                List<XElement> elementToDelete = new List<XElement>();
                var emp = yourDoc.Descendants("CriminalCase").Any();
                if (emp)
                {
                    Totalcount = yourDoc.Descendants("CriminalCase").Count();/*Just testing purpose*/
                    foreach (XElement user in yourDoc.Descendants("CriminalCase"))
                    {
                        //foreach (XElement contact in user.Descendants("DemographicDetail"))
                        //{
                        var dob = user.Descendants("DateOfBirth");
                        XElement IsAdded = user.Descendants("DemographicDetail").Where(x => x.HasAttributes && x.Attribute("IsAdded").Value == "true").FirstOrDefault();
                        if (dob.Count() == 0 && IsAdded == null)
                        {
                            RemainingNUllDOB++;
                        }
                        else
                        {
                        }
                        bool hasDOB = (dob.Count() > 0);
                        if (!hasDOB && IsAdded == null)
                        {
                            elementToDelete.Add(user);
                            //user.Remove();
                        }
                        //}
                    }

                    List<XElement> elementToDelete1 = elementToDelete;

                    foreach (XElement XE in elementToDelete1)
                    {
                        DeletedRecords += Convert.ToString(XE);
                        XE.Remove();
                    }
                   // XDocument xmldoc = XDocument.Parse(XML_Value);
                    //List<XElement> xElement = new List<XElement>();

                    if (yourDoc.Descendants().Count() > 0)
                    {
                        var CriminalCaseNode = yourDoc.Descendants("CriminalCase").Any();
                        if (CriminalCaseNode)
                        {
                            XElement Node = yourDoc.Descendants("CriminalCase").Last();
                            //xElement = xmldoc.Descendants(RootRecordNode).Where(x => x.HasAttributes && x.Attribute("Deleted").Value == "1").ToList();
                            //xElement.Remove();  

                            // commented for ticket #151

                            //foreach (XElement XE in elementToDelete1.Descendants("CriminalCase").Reverse())
                            //{
                            //   Node.AddAfterSelf(XE);                            
                            //}

                            //codded for ticket #151

                            for (int index = elementToDelete1.Count - 1; index >= 0; index--)
                            {
                                Node.AddAfterSelf(elementToDelete1[index]);
                            }
                        }
                        else
                        {
                            XElement Node = yourDoc.Descendants("CriminalReport").FirstOrDefault();
                            foreach (XElement XE in elementToDelete1)
                            {
                                var xelement = XE.Descendants("DemographicDetail").FirstOrDefault();
                                if (xelement.Attribute("IsAdded") == null)
                                {
                                    xelement.Add(new XAttribute("IsAdded", "false"));
                                }
                                Node.Add(XE);
                            }
                        }
                        int TrimedUpTo = XmlResponse.IndexOf("<BackgroundReportPackage>");
                        StringBuilder CompleteResponse = new StringBuilder();
                        string XMLInitials = XmlResponse.Substring(0, TrimedUpTo);
                        CompleteResponse.Append(XMLInitials);
                        CompleteResponse.Append(Convert.ToString(yourDoc));
                        CompleteResponse.Append("</BackgroundReports>");
                        FinalResponse = Convert.ToString(CompleteResponse);// Convert.ToString(CompleteResponse).Replace("&lt;", "<").Replace("&gt;", ">");
                    }
                }
                else
                {
                    FinalResponse = XmlResponse;
                }
                ObjModel.CountNullDob = RemainingNUllDOB;
                ObjModel.TotalCount = Totalcount - RemainingNUllDOB;
                System.Web.HttpContext.Current.Session["NCR1TotalCount"] = ObjModel;

            }
            catch
            {
                FinalResponse = XmlResponse;
            }

            #endregion
            return FinalResponse;
        }

        private string AddOrderDetailToXMLResponse_1(string ProductCode, string XmlResponse, int pkOrderDetailId)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(XmlResponse);
            XmlElement root = xmlDoc.DocumentElement;
            //XmlNode rootNode = xmlDoc.SelectSingleNode(root.Name.ToString());
            XmlAttribute newAttribute = xmlDoc.CreateAttribute("OrderDetailId");
            newAttribute.Value = Convert.ToString(pkOrderDetailId);
            root.Attributes.Append(newAttribute);


            return xmlDoc.OuterXml.ToString();

        }

        private void UpdateResponse(int guid, string Response)
        {
            BALOrders ObjBALOrders = new BALOrders();
            try
            {
                ObjBALOrders.UpdateResponse(guid, Response);
            }
            catch (Exception)
            {

            }
            finally
            {
                ObjBALOrders = null;
            }
        }

        /// <summary>
        /// Function To Send Pending Message
        /// </summary>
        /// <param name="ObjRequestAndResponseResult"></param>
        private void SendPendingMessage(Proc_Get_RequestAndResponseResult ObjRequestAndResponseResult)
        {
            //StringBuilder ObjStringBuilder = new StringBuilder();
            //ObjStringBuilder.Append("Hi<br/>");
            ////ObjStringBuilder.Append(ConfigurationManager.AppSettings["ApplicationName"].ToString() + "'s " + ObjRequestAndResponseResult.ProductDisplayName + " is Pending with Order ID " + ObjRequestAndResponseResult.fkOrderId + "<br/>");
            //ObjStringBuilder.Append("<div id='Pending_message' align='left'><div style='clear:both;'>STATUS : PENDING<hr align='left' id='Pending_Message_Hr' /></div><div>        Your report is being processed manually.  <br />   when your report is ready, you will be notified by email and your report will appear in your archived reports.   <br />  Emerge support Team <span class='Pending_Message_Separator'> |</span> <a href='mailto:support@intelifi.com' class='Pending_Message_Link'>support@intelifi.com</a> <span class='Pending_Message_Separator'> |</span> 888.409.1819 </div></div>");
            //string body = ObjStringBuilder.ToString();
            string subject = "";
            //string WebsiteLogo = "https://www.intelifi.com/emerge/Resources/Upload/Images/" + BALGeneral.GetGeneralSettings().WebSiteLogo;
            string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + BALGeneral.GetGeneralSettings().WebSiteLogo;
            CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;
            string firstname = ObjRequestAndResponseResult.SearchedFirstName != null ? textInfo.ToTitleCase(ObjRequestAndResponseResult.SearchedFirstName) : string.Empty;
            string lastname = ObjRequestAndResponseResult.SearchedLastName != null ? textInfo.ToTitleCase(ObjRequestAndResponseResult.SearchedLastName) : string.Empty;
            BALGeneral ObjBALGeneral = new BALGeneral();
            int pkTemplateId = 44;
            var emailContent = ObjBALGeneral.GetEmailTemplate(new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852"), pkTemplateId);//"Pending Report"

            string emailText = emailContent.TemplateContent;
            subject = emailContent.TemplateSubject.Replace("%ApplicantName%", firstname + " " + lastname);

            BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
            Bookmarks objBookmark = new Bookmarks();
            string MessageBody = string.Empty;

            #region For New Style SystemTemplate
            string Bookmarkfile = string.Empty;
            string bookmarkfilepath = string.Empty;

            Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
            Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
            Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
            emailText = Bookmarkfile;
            #endregion


            objBookmark.EmailContent = emailText;
            objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";

            MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Guid.Empty);
            //For email status//
            //BALGeneral objbalgenral = new BALGeneral();
            //int pktemplateid = pkTemplateId;
            //MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
           // Guid user_ID = Guid.Parse(ObjMembershipUser.ProviderUserKey.ToString());
           // int fkcompanyuserid = 0;
            bool email_status = true; // objbalgenral.UserEmailStatusCheck(user_ID, pktemplateid, fkcompanyuserid);
            if (email_status == true)
            {
                Utility.SendMail(User.Identity.Name, string.Empty, string.Empty, ConfigurationManager.AppSettings["ReportReviewFromAddress"].ToString(), MessageBody, subject);
            }
            //
        }

        /// <summary>
        /// Function To Send an Email To Admin in case Report Get In-Complete
        /// </summary>
        /// <param name="ObjRequestAndResponseResult"></param>
        public void SendInCompleteMessage(Proc_Get_RequestAndResponseResult ObjRequestAndResponseResult)
        {
            StringBuilder ObjStringBuilder = new StringBuilder();
            try
            {
                ObjStringBuilder.Append("Hi<br/>");
                ObjStringBuilder.Append("A new <b>" + ObjRequestAndResponseResult.ProductDisplayName + "</b> Under order number : <b>" + ObjRequestAndResponseResult.OrderNo + "</b> is <b>In-Complete</b> in the System.<br/> Please Check <br/><br/>Intelifi Recovery Email<br/>Intelifi System.");
                //string body = ObjStringBuilder.ToString();
               //string success = BALEmergeReview.SendMail(ConfigurationManager.AppSettings["BccAdmin"].ToString(), string.Empty, string.Empty, ConfigurationManager.AppSettings["ReportReviewFromAddress"].ToString(), body, "eMerge In-Complete Report");
            }
            catch
            {
            }
        }

        #endregion

        /// <summary>
        /// To update the report status set to pass or fail.
        /// </summary>
        /// <param name="IsReportStatus"></param>
        /// <param name="sOrderId"></param>
        /// <param name="IsApplied"></param>
        /// <returns></returns>
        [HttpPost]
        public string UpdateReportStatusForPassFail(bool IsReportStatus, string sOrderId, bool IsApplied)
        {
            string retVal = string.Empty;
            try
            {
                int OrderId = 0;
                OrderId = Convert.ToInt32(sOrderId);
                new BALCompany().UpdateReportStatusInDatabase(IsReportStatus, IsApplied, OrderId);
                retVal = "Succeded";
            }
            catch
            {
                retVal = "Error";
            }
            return retVal;
        }

        /// <summary>
        /// To get Order hit status based on order id
        /// </summary>
        /// <param name="sOrderId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetHitStatusOfSelectedOrder(string sOrderId)
        {
            string retVal = "Green";//Clear record
            try
            {
                int OrderId = 0;
                OrderId = Convert.ToInt32(sOrderId);
                int val = new BALOrders().GetOrderHitStatusManually(OrderId);
                if (val == 1)
                {
                    retVal = "Red";//Record found
                }
                else if (val == 0)
                {
                    retVal = "Disabled";
                }
            }
            catch
            {
                retVal = "Error";
            }
            return Json(new { result = retVal });
        }



        /// <summary>
        /// To update Order hit status based on order id
        /// </summary>
        /// <param name="sOrderId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateHitStatusOfSelectedOrderManually(string sOrderId, string hitValue)
        {
            string retVal = "Clear";//Clear record
            try
            {
                int? OrderId = 0;
                OrderId = Convert.ToInt32(sOrderId);
                byte hitVal = Convert.ToByte(hitValue);
                int val = new XMLFilteration().UpdateOrderHitStatusManually(OrderId, hitVal);
                if (val == 1)
                {
                    retVal = "Success";
                }
            }
            catch
            {
                retVal = "Error";
            }
            return Json(new { result = retVal });
        }






        /// <summary>
        /// for ticket #17 Edit ship to address
        /// </summary>
        /// <returns></returns>
        public ActionResult EditDrugTestingOrder()
        {
            int OrderId = 0;
            EditDrugTestingModel objEditDreug = new EditDrugTestingModel();
            //string orderInformation = string.Empty;
            string ProductImagePath = Server.MapPath("~/Resources/Upload/Images/ProductImages/");
            if (!string.IsNullOrEmpty(Request.QueryString["_a"]))
            {
                OrderId = Convert.ToInt32(Request.QueryString["_a"]);
            }
            EditDrugTestingPageLoad(OrderId, objEditDreug, ProductImagePath);
            return View(objEditDreug);
        }

        /// <summary>
        /// post udpated data and save update value in db for ticket #17
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateDrugTestingOrder(FormCollection collection)
        {
            bool CheckShipToAddressExist = false;
            int Result = 0;
            string Message = string.Empty;
            string Css = string.Empty;
            int orderId = 0;
            short productQuantity = 0;
            ShipToAddress objShipTo = new ShipToAddress();
            if (collection.Count > 0 && collection != null)
            {
                BALGeneral objBalGeneral = new BALGeneral();
                //get address details
                objShipTo.CompanyName = collection["CompanyName"];
                objShipTo.Address1 = collection["Address1"];
                objShipTo.Address2 = collection["Address2"] != null ? collection["Address2"] : string.Empty;
                objShipTo.StateCode = collection["StateCode"];
                objShipTo.City = collection["City"];
                objShipTo.ZipCode = collection["ZipCode"];
                //get order and company user information
                orderId = Convert.ToInt32(collection["OrderId"]);
                objShipTo.LocationId = Convert.ToInt32(collection["LocationId"]);
                objShipTo.CompanyUserId = Convert.ToInt32(collection["CompanyUserId"]);
                CheckShipToAddressExist = objBalGeneral.CheckCompanyExistanceForShipAddress(objShipTo.LocationId, objShipTo.CompanyUserId);
                if (CheckShipToAddressExist)
                {
                    objBalGeneral.UpdateShipToAddressInDB(objShipTo);
                }
                else
                {
                    objBalGeneral.SaveShipToAddressInDB(objShipTo);
                }
                //get product information
                decimal ReportAmount = decimal.Parse(collection["ReportAmount"]);
                productQuantity = Convert.ToInt16(collection["ProductQty"]);
                decimal AdditionalFee = decimal.Parse(collection["ShippingCost"]);
                Result = objBalGeneral.UpdateDrugTestProductInDb(orderId, ReportAmount, productQuantity, AdditionalFee);
                if (Result == 1)
                {
                    Message = "Updated Successfully";
                    Css = "SuccessMsg";
                }
                else
                {
                    Message = "Some error occured while update drug testing product information";
                    Css = "ErrorMsg";
                }
            }
            return Json(new { Msg = Message, Css = Css });

        }

        public void EditDrugTestingPageLoad(int OrderId, EditDrugTestingModel objEditDreug, string ProductImagePath)
        {
            BALGeneral objBalGenral = new BALGeneral();
            ShipToAddress objShipToAddress = new ShipToAddress();
            BALOrders ObjBALOrders = new BALOrders();
            List<Proc_Get_DrugTestOrderDetailsResult> ObjData = ObjBALOrders.GetDrugTestOrderDetails(OrderId);
            if (ObjData.Count() > 0)
            {
                ProductImagePath = ProductImagePath + ObjData.First().ProductImage;
                string sImg = string.Empty;

                if (System.IO.File.Exists(ProductImagePath))
                {
                    string ImageVirtualPath = ApplicationPath.GetApplicationPath() + "Resources/Upload/Images/ProductImages/" + ObjData.First().ProductImage;
                    string imgTitle = ObjData.First().LongDescription;

                    System.Drawing.Image ImgProduct = System.Drawing.Image.FromFile(ProductImagePath);
                    int ImgWidth = 0, ImgHeight = 0;

                    if (ImgProduct != null)
                    {
                        ImgWidth = ImgProduct.Width;
                        ImgHeight = ImgProduct.Height;
                    }
                    ImgWidth = (ImgWidth > 130) ? 130 : ImgWidth;
                    ImgHeight = (ImgHeight > 150) ? 150 : ImgHeight;

                    sImg = "<img src='" + ImageVirtualPath + "' width='" + ImgWidth + "px' height='" + ImgHeight + "px' style='border:1px solid black;' title='" + imgTitle + "'/>";
                    objEditDreug.ProductImagePath = sImg;
                }


                //string Address = string.Empty;
                objShipToAddress = objBalGenral.GetUpdatedShipToAddress(Convert.ToInt32(ObjData.First().fkLocationId.ToString()), Convert.ToInt32(ObjData.First().pkCompanyUserId.ToString()), OrderId);
                objEditDreug.LocationId = Convert.ToInt32(ObjData.First().fkLocationId.ToString());
                objEditDreug.CompanyUserId = Convert.ToInt32(ObjData.First().pkCompanyUserId.ToString());
                if (objShipToAddress != null)
                {
                    objEditDreug.CompanyName = objShipToAddress.CompanyName;
                    objEditDreug.Address1 = objShipToAddress.Address1;
                    objEditDreug.Address2 = objShipToAddress.Address2 != null ? objShipToAddress.Address2 : string.Empty;
                    objEditDreug.StateCode = objShipToAddress.StateCode;
                    objEditDreug.City = objShipToAddress.City;
                    objEditDreug.ZipCode = objShipToAddress.ZipCode;
                }
                else
                {
                    objEditDreug.CompanyName = ObjData.First().CompanyName;
                    objEditDreug.Address1 = ObjData.First().Address1;
                    objEditDreug.Address2 = ObjData.First().Address2 != "" ? ObjData.First().Address2 : string.Empty;
                    objEditDreug.StateCode = ObjData.First().StateCode;
                    objEditDreug.City = ObjData.First().City;
                    objEditDreug.ZipCode = ObjData.First().ZipCode;
                }

                decimal ReportAmount;
                decimal.TryParse(ObjData.First().ReportAmount.ToString(), out ReportAmount);
                objEditDreug.ReportAmount = ReportAmount;
                short ProductQty;
                short.TryParse(ObjData.First().ProductQty.ToString(), out ProductQty);
                objEditDreug.ProductQty = ProductQty;
                decimal OrderAmount = ReportAmount * ProductQty;
                objEditDreug.OrderAmount = OrderAmount;

                if (ObjData.First().AdditionalFee != decimal.MinValue)
                {
                    OrderAmount += ObjData.First().AdditionalFee;
                    objEditDreug.ShippingCost = ObjData.First().AdditionalFee;
                }

                objEditDreug.ProductDisplayName = ObjData.First().ProductDisplayName;
                objEditDreug.OrderId = OrderId;
            }
        }

        /// <summary>
        /// To remove the CASE TYPE: J and CASE TYPE: JUVENILE  from the rowCourthData section.        
        /// </summary>
        /// <param name="OriginalResponse"></param>
        /// <param name="productcode"></param>
        /// <returns></returns>
        public string RemoveCourtHouseData(string OriginalResponse, string productcode)
        {
            string result = string.Empty;
            try
            {

                if (productcode.ToUpper() == "NCR1" || productcode.ToUpper() == "RCX" || productcode.ToUpper() == "CCR1LIVERUNNER" || productcode.ToUpper() == "CCR2LIVERUNNER" || productcode.ToUpper() == "NCR+" || productcode.ToUpper() == "FCR")
                {
                    string FinalResponse1_1 = "";
                    string finalresponse_1 = string.Empty;
                    string originalresponse_1 = OriginalResponse;
                   // XDocument ObjXDocumentUpdatedFilteredNCR1_1 = new XDocument();
                    XDocument ObjXDocumentFilteredNCR1_1 = new XDocument();
                    string XML_ResponseNCR1_1 = XMLFilteration.GetRequiredXml(originalresponse_1);
                    ObjXDocumentFilteredNCR1_1 = XDocument.Parse(XML_ResponseNCR1_1);
                    var lstScreenings = ObjXDocumentFilteredNCR1_1.Descendants("Screenings").ToList();
                    var lstCriminalReports = lstScreenings.Descendants("ScreeningResults").FirstOrDefault();
                    var stringdata = lstCriminalReports.Descendants("Text").FirstOrDefault().Value;
                    finalresponse_1 = stringdata.Replace("CASE TYPE: J", "");//Replacing the CASE TYPE: J 
                    finalresponse_1 = finalresponse_1.Replace("UVENILE", ""); //Replacing CASE TYPE: JUVENILE     
                    ObjXDocumentFilteredNCR1_1.Element("BackgroundReportPackage").Element("Screenings").Element("Screening").Element("ScreeningResults").Element("Text").ReplaceWith(new XElement("Text", finalresponse_1));
                    int TrimedUpTo = originalresponse_1.IndexOf("<BackgroundReportPackage>");
                    StringBuilder CompleteResponse_1 = new StringBuilder();
                    string XMLInitials = originalresponse_1.Substring(0, TrimedUpTo);
                    CompleteResponse_1.Append(XMLInitials);
                    CompleteResponse_1.Append(Convert.ToString(ObjXDocumentFilteredNCR1_1));
                    CompleteResponse_1.Append("</BackgroundReports>");
                    FinalResponse1_1 = Convert.ToString(CompleteResponse_1);
                    result = FinalResponse1_1;
                }
            }
            catch //(Exception ex) 
            { }
            return result;
        }


    }
}
