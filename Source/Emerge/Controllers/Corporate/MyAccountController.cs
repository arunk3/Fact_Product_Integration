﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using Emerge.Common;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Configuration;
using System.IO;
namespace Emerge.Controllers.Corporate
{
    public partial class CorporateController : BaseEmergeController
    {
        //
        // GET: /MyAccount/
        string CurrentUserCompanyName = string.Empty;
        public ActionResult MyAccount()
        {
            UsersModel ObjModel = new UsersModel();
            BALCompany ObjBALCompany = new BALCompany();
            var collection = ObjBALCompany.GetAllCompany(Utility.SiteApplicationId, true, false);
            var CompanyList = new List<SelectListItem>();
            CompanyList.Insert(0, new SelectListItem { Text = "Select", Value = "" });
            if (collection.Count > 0)
            {
                for (int item = 0; item < collection.Count; item++)
                {
                    CompanyList.Add(new SelectListItem { Text = collection.ElementAt(item).CompanyName, Value = collection.ElementAt(item).pkCompanyId.ToString() });
                }
            }

            ObjModel.ListCompany_Id = CompanyList;


            if (Session["CurrentUserCompanyName"] != null)
            {
                CurrentUserCompanyName = Session["CurrentUserCompanyName"].ToString();
            }
            List<Proc_Get_UserInfoByMemberShipUserIdResult> UserDetail = GetCurrentUserDetail();

            ObjModel.CurrentUserRole = UserDetail.First().RoleName;
            if (ObjModel.CurrentUserRole == "BasicUser")
            {
                ObjModel.PageTitle = "Edit User";
            }
            else
            {
                ObjModel.PageTitle = "My Profile";
            }
            int ObjGuid = GetCurrentUserDetail().First().pkCompanyUserId;
            TempData["TaleoWebApiObjGuid"] = ObjGuid;  //nd-16 changes
            ObjModel = FetchCurrentUserDetails(ObjGuid, ObjModel);
            BindCompany();
            GetCompanyUserId();
            if (TempData["message"] != null)
            {
                ObjModel.strResult = TempData["message"].ToString();
            }
            return View(ObjModel);
        }
        public UsersModel FetchCurrentUserDetails(int PkID, UsersModel ObjModel)
        {
            ObjBALCompanyUsers = new BALCompanyUsers();
            tblCompanyUser ObjtblCompanyUser = ObjBALCompanyUsers.GetCompanyUserByCompanyUserId(PkID);
           
            try
            {
                ObjModel = BindCurrentUserFields(ObjtblCompanyUser, ObjModel);
            }
            catch (Exception)
            {

            }
            finally
            {
                ObjtblCompanyUser = null;
                ObjBALCompanyUsers = null;
            }
            return ObjModel;
        }
        public UsersModel BindCurrentUserFields(tblCompanyUser ObjtblCompanyUser, UsersModel ObjModel)
        {
          
            #region Membership Info

            MembershipUser ObjMembershipUser = Membership.GetUser(ObjtblCompanyUser.fkUserId);
            ObjModel.fkUserId = ObjtblCompanyUser.fkUserId;
            string[] ObjRoles = Roles.GetRolesForUser(ObjMembershipUser.UserName);

            ObjModel.LastLoginByUser = ObjMembershipUser.LastLoginDate.ToString();

            ObjModel.UserEmail = ObjMembershipUser.UserName;

            //ticket #101
            ObjModel.UserEmailOne = ObjtblCompanyUser.UserEmailOne;
            ObjModel.UserEmailTwo = ObjtblCompanyUser.UserEmailTwo;
           

            string Password = string.Empty;
            Password = ObjMembershipUser.GetPassword();
            ObjModel.UserPassword = Password;
            ObjModel.RetypeUserPassword = Password;


            #endregion


            #region taleo login and its key
            using (EmergeDALDataContext ObjDALDataContext = new EmergeDALDataContext())
            {
                var taleouderkey = (from pd in ObjDALDataContext.tblCompanyUsers
                                    join od in ObjDALDataContext.aspnet_Memberships on pd.fkUserId equals od.UserId
                                    where od.UserId == ObjtblCompanyUser.fkUserId
                                    select od.TaleoApiKey).FirstOrDefault();

                ObjModel.TaleoApiKey = Convert.ToString(taleouderkey);//INT-237
            }
            #endregion

            #region User Personal Info
            ObjModel.FirstName = ObjtblCompanyUser.FirstName;
            ObjModel.LastName = ObjtblCompanyUser.LastName;
            int CompanyId = GetCompanyIdByLocationId((int)ObjtblCompanyUser.fkLocationId);
            if (CompanyId == 9514)
            {
                //ObjModel.CollRoles = BindStatus(true, true);
            }
            else
            {
                //ObjModel.CollRoles = BindStatus(false);
            }
            try
            {
                ObjModel.RoleId = GetRoleId(ObjRoles);
            }
            catch { }

            ObjModel.PkCompanyId = CompanyId;
            ObjModel.fkLocationId = ObjtblCompanyUser.fkLocationId;

            if (ObjModel.CollStates.Count == 0)
            {
                // ObjModel.CollStates = BindState();
            }

            if (ObjtblCompanyUser.fkStateId != null)
            {
                ObjModel.fkStateId = ObjtblCompanyUser.fkStateId;
            }


            int StateId;
            int.TryParse(ObjModel.fkStateId.ToString(), out StateId);
            ObjModel.fkCountyId = ObjtblCompanyUser.fkCountyId;
            ObjModel.UserAddressLine1 = ObjtblCompanyUser.UserAddressLine1;
            ObjModel.UserAddressLine2 = ObjtblCompanyUser.UserAddressLine2;
            ObjModel.PhoneNo = ObjtblCompanyUser.PhoneNo;
            ObjModel.ZipCode = ObjtblCompanyUser.ZipCode;

            ObjModel.IsEnabled = ObjtblCompanyUser.IsEnabled;

            #region User Image

            if (ObjtblCompanyUser.UserImage == string.Empty || !System.IO.File.Exists(Server.MapPath(@"~\Resources\Upload\Images\" + ObjtblCompanyUser.UserImage)))
            {
               
                ObjModel.ViewImage = ApplicationPath.GetApplicationPath() + @"Content/themes/base/images/NotAvail.png";
                ObjModel.OldUserImage = ObjtblCompanyUser.UserImage;
            }
            else
            {
                ObjModel.OldUserImage = ObjtblCompanyUser.UserImage;
                ObjModel.ViewImage = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjtblCompanyUser.UserImage;

            }

            #endregion

            #endregion

            #region ViewState Info

            ObjModel.pkCompanyUserId = ObjtblCompanyUser.pkCompanyUserId;
            ObjModel.fkUserId = ObjtblCompanyUser.fkUserId;
            ObjModel.OldUserPassword = Password;
            ObjModel.OldUserEmail = ObjMembershipUser.UserName.Trim();

            #endregion

            return ObjModel;

        }
        [ValidateInput(false)]
        public ActionResult SaveMyProfileDetailsById(UsersModel ObjModel, HttpPostedFileBase UserImage)
        {
            string strResult = string.Empty;


            string OldUserName = ObjModel.OldUserEmail;
            string NewUserName = ObjModel.UserEmail;
            bool bResult = IsExistsUsername(OldUserName, NewUserName);
            if (bResult)
            {

                strResult = "<span class='errormsg'>This Username already exists</span>";
                TempData["message"] = strResult;
                return RedirectToAction("EditCompanyUser", new { _a = ObjModel.pkCompanyUserId });
            }
            if (UserImage != null)
            {
                if (!CheckExtensions(UserImage, ".jpg, .gif, .bmp, .jpeg, .png"))
                {
                    strResult = "<span class='errormsg'>Only these extensions(.jpg, .gif, .bmp, .jpeg, .png) are allowed to upload image.</span>";
                    TempData["message"] = strResult;
                    return RedirectToAction("MyAccount");
                }
            }

            strResult = UpdateMyProfileinDB(ObjModel, UserImage);

            TempData["message"] = strResult;
            return RedirectToAction("MyAccount");
        }
        /// <summary>
        /// Function To Update User 
        /// </summary>
        public string UpdateMyProfileinDB(UsersModel ObjModel, HttpPostedFileBase UserImage)
        {
            string strResult = string.Empty;
            tblCompanyUser ObjtblCompanyUser = new tblCompanyUser();
            ObjBALCompanyUsers = new BALCompanyUsers();
            //BLLMembership ObjBLLMembership = new BLLMembership();

            try
            {
                ObjtblCompanyUser.pkCompanyUserId = ObjModel.pkCompanyUserId;
                ObjtblCompanyUser.fkLocationId = ObjModel.fkLocationId;
                ObjtblCompanyUser.FirstName = ObjModel.FirstName;
                ObjtblCompanyUser.LastName = ObjModel.LastName;
                ObjtblCompanyUser.UserAddressLine1 = !string.IsNullOrEmpty(ObjModel.UserAddressLine1) ? ObjModel.UserAddressLine1 : string.Empty;
                ObjtblCompanyUser.UserAddressLine2 = !string.IsNullOrEmpty(ObjModel.UserAddressLine2) ? ObjModel.UserAddressLine2 : string.Empty;
                ObjtblCompanyUser.PhoneNo = !string.IsNullOrEmpty(ObjModel.PhoneNo) ? ObjModel.PhoneNo : string.Empty;
                ObjtblCompanyUser.ZipCode = !string.IsNullOrEmpty(ObjModel.ZipCode) ? ObjModel.ZipCode : string.Empty;
                ObjtblCompanyUser.fkStateId = ObjModel.fkStateId;

                if (Convert.ToString(ObjModel.fkCountyId) != "-1")
                {
                    ObjtblCompanyUser.fkCountyId = ObjModel.fkCountyId;
                }
                else
                {
                    ObjtblCompanyUser.fkCountyId = null;
                }


                ObjtblCompanyUser.IsEnabled = ObjModel.IsEnabled;
                ObjtblCompanyUser.LastModifiedDate = System.DateTime.Now;

                ObjtblCompanyUser.UserImage = string.Empty;

                #region User Image

                if (UserImage != null)
                {
                    if (UserImage != null && UserImage.ContentLength > 0)
                    {
                        if (ObjModel.OldUserImage != null)
                        {
                            string oldimage_Path = Server.MapPath(@"~\Resources\Upload\Images\") + ObjModel.OldUserImage;
                            if (System.IO.File.Exists(oldimage_Path))
                            {
                                System.IO.File.Delete(oldimage_Path);
                            }
                        }
                        string UserImageFileName = ObjModel.FirstName + ObjModel.LastName + "_" + DateTime.Now.ToFileTime().ToString() + System.IO.Path.GetExtension(UserImage.FileName);
                        string upload_Path = Server.MapPath(@"~\Resources\Upload\Images\") + UserImageFileName;
                        UserImage.SaveAs(upload_Path);

                        ObjtblCompanyUser.UserImage = UserImageFileName;
                    }

                }
                else
                {
                    if (ObjModel.OldUserImage != null)
                    {
                        ObjtblCompanyUser.UserImage = ObjModel.OldUserImage;
                    }
                    
                }                
                #endregion



                bool Result = ObjBALCompanyUsers.UpdateMyAccount(ObjtblCompanyUser);

                if (Result == true)
                {
                    #region Update User Name and password

                    MembershipUser ObjMembershipUser = Membership.GetUser(ObjModel.OldUserEmail);

                    string OldUserName = ObjModel.OldUserEmail;
                    string NewUserName = ObjModel.UserEmail;

                    string OldPwd = ObjModel.OldUserPassword;

                    if (ObjMembershipUser != null)
                    {
                        if (OldPwd != ObjModel.UserPassword)
                        {
                            Membership.GetUser(ObjMembershipUser.UserName).ChangePassword(OldPwd, ObjModel.UserPassword);
                        }
                    }

                    if (OldUserName.ToLower().Trim() != NewUserName.ToLower())
                    {
                        UpdateUsername(OldUserName, NewUserName);

                        #region Change email address

                        UpdateUserEmail(NewUserName, ObjModel.fkUserId);//For 137 ticket Solution

                        #endregion
                        Utility.Logout(NewUserName);
                    }
                    #endregion
                    Session["Success"] = Result;
                    strResult = "<span class='successmsg'>Record Updated Successfully</span>";

                }
                else
                {
                    Session["Success"] = false;
                    strResult = "<span class='errormsg'>Error Occured While Updating Record</span>";
                }
            }
            catch //(Exception ex)
            {
                strResult = "<span class='errormsg'>Error Occured While Updating Record</span>";
            }
            finally
            {
                ObjtblCompanyUser = null;
                ObjBALCompanyUsers = null;
                //ObjBLLMembership = null;
            }
            return strResult;
        }
        private void UpdateUserEmail(string NewEmail, Guid fkUserId)
        {
            if (fkUserId != null)
            {
                MembershipUser mu = Membership.GetUser(fkUserId);

                if (mu != null)
                {
                    mu.Email = NewEmail;
                    Membership.UpdateUser(mu);
                }

            }
        }
        private bool CheckExtensions(HttpPostedFileBase ObjControl, string sExtensions)
        {
            string Extension = string.Empty;
            string[] sDelimeter = new string[] { "," };
            string[] arrSplit = sExtensions.Split(sDelimeter, StringSplitOptions.None);
            bool bMode = false;

            if (ObjControl.FileName != string.Empty)
            {
                Extension = Path.GetExtension(ObjControl.FileName).ToLower();
                if (ObjControl.ContentLength > 0)
                {
                    for (int iRow = 0; iRow < arrSplit.Length; iRow++)
                    {
                        if (Extension == arrSplit[iRow].Trim().ToLower())
                        {
                            bMode = true;
                            break;
                        }
                    }
                    if (!bMode)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        /// <summary>
        /// Function To Bind the States With Dropdownlist
        /// </summary>
        /// 
        [HttpPost]
        public ActionResult BindStateKendo()
        {
            ObjBALGeneral = new BALGeneral();
            List<tblState> States = new List<tblState>();
            try
            {
                States = ObjBALGeneral.GetStates().Select(p => new tblState
                {
                    pkStateId = p.pkStateId,
                    StateName = p.StateName

                }).ToList();
                //States.Insert(0, new tblState
                //{
                //    pkStateId = -1,
                //    StateName = "Select"
                //});

            }
            catch (Exception)
            {

            }
            finally
            {
                ObjBALGeneral = null;
            }
            return Json(States);
        }
        [HttpPost]
        public ActionResult GetCountiesByStateId(string StateId)
        {
            BALGeneral ObjBALGeneral = new BALGeneral();
            List<clsCounty> ObjCollection = new List<clsCounty>();
            if (StateId != string.Empty)
            {
                ObjCollection = ObjBALGeneral.GetStatesCounties(Convert.ToInt32(StateId)).Select(p => new clsCounty
                {
                    pkCountyId = p.pkCountyId,
                    CountyName = p.CountyName

                }).ToList();
            }

            //ObjCollection.Insert(0, new clsCounty
            //{
            //    pkCountyId = "-1",
            //    CountyName = "Select"
            //});

            return Json(ObjCollection);
        }


        #region nd-16 and its re genrate taleo user key for account section for user
        //nd-16
        public ActionResult TaleoWebApiKeyGenrate(string UserEmail)
        {

            int fkuserid = Convert.ToInt32(TempData["TaleoWebApiObjGuid"]);


            Random _Random = new Random();
            string test_to_encrypt = _Random.Next().ToString();
            byte[] arrbyte = new byte[test_to_encrypt.Length];
            using (System.Security.Cryptography.SHA1CryptoServiceProvider hash = new System.Security.Cryptography.SHA1CryptoServiceProvider())
            {
                arrbyte = hash.ComputeHash(System.Text.Encoding.UTF8.GetBytes(test_to_encrypt));
            }
            string TaleoWebApikEYavalue = System.Web.HttpServerUtility.UrlTokenEncode(arrbyte);


            using (EmergeDALDataContext _EmergeDALDataContext = new EmergeDALDataContext())
            {



                var tmp = (from c in _EmergeDALDataContext.aspnet_Memberships
                           join cop in _EmergeDALDataContext.tblCompanyUsers on c.UserId equals cop.fkUserId
                           where cop.pkCompanyUserId == fkuserid && c.Email == UserEmail
                           select c);


                if (tmp.Count() == 1)
                {
                    aspnet_Membership objaspnet_Membership = tmp.First();

                    objaspnet_Membership.TaleoApiKey = TaleoWebApikEYavalue.ToString();
                    _EmergeDALDataContext.SubmitChanges();
                }
            }

            return Json(new { Fname = "SUCCESS" }, JsonRequestBehavior.AllowGet);
            //return null;
        }


        #endregion










        public ActionResult BindCompanyKendo()
        {
            List<CompanyList> lstCompany = new List<CompanyList>();
            ObjBALCompany = new BALCompany();
            try
            {
                lstCompany = ObjBALCompany.GetAllCompany(Utility.SiteApplicationId, true, false).Select(p => new CompanyList
                {
                    pkCompanyId = p.pkCompanyId,
                    CompanyName = p.CompanyName
                }).ToList();
                //lstCompany=ObjBALCompany.GetAllCompany(Utility.SiteApplicationId))
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ObjBALCompany = null;
            }
            return Json(lstCompany, JsonRequestBehavior.AllowGet);
        }

        public ActionResult BindLocationsByCompanyId(string CompanyId)
        {
            ObjBALLocation = new BALLocation();
            List<tblLocation> lstLocations = new List<tblLocation>();
            try
            {

                lstLocations = ObjBALLocation.GetLocationsByCompanyId(Int32.Parse(CompanyId)).Select(p => new tblLocation
                {
                    pkLocationId = p.pkLocationId,
                    City = p.City

                }).ToList();
                //lstLocations.Insert(0, new tblLocation
                //{
                //    pkLocationId = Guid.Empty,
                //    City = "Select"
                //});
            }
            catch (Exception)
            {
                //throw;
            }
            finally
            {
                ObjBALLocation = null;
            }
            return Json(lstLocations);
        }
        public ActionResult BindStatusKendo()
        {
            ObjBALGeneral = new BALGeneral();
            List<aspnet_Role> allroles = new List<aspnet_Role>();
            try
            {
                Guid UserId = Utility.GetUID(User.Identity.Name);
                MembershipUser ObjMembershipUser = Membership.GetUser(UserId);

                if (Roles.IsUserInRole(ObjMembershipUser.UserName, "Systemadmin"))
                {
                    allroles = ObjBALGeneral.FetchRoles(Utility.SiteApplicationId).Select(p => new aspnet_Role
                    {
                        RoleId = p.RoleId,
                        Description = p.Description

                    }).ToList();
                }
                else
                {
                    allroles = ObjBALGeneral.FetchRolesExceptAdmin(Utility.SiteApplicationId).Select(p => new aspnet_Role
                    {
                        RoleId = p.RoleId,
                        Description = p.Description

                    }).ToList();
                }

                //allroles.Insert(0, new aspnet_Role
                //{
                //    RoleId = Guid.Empty,
                //    Description = "Select"
                //});
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ObjBALGeneral = null;
            }
            return Json(allroles);
        }

    }
}
