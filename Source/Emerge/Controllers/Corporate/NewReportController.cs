﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Data;
using Emerge.Models;
using Emerge.Services;
using Emerge.Common;
using System.Configuration;
using System.Web.UI;
using System.Web.Security;
using System.IO;
using System.Text;
using System.Xml;
using System.Globalization;
using System.Web.Providers.Entities;
using System.Net;
using Newtonsoft.Json.Linq;

namespace Emerge.Controllers.Corporate
{
    public partial class CorporateController : BaseEmergeController
    {
        #region New Report Page Load
        int taleoGloalVariable = 0;
        public ActionResult NewReport()
        {
            BALDrug Objbaldrub = new BALDrug();
            if (TempData["info"] != null)
            {
                ViewBag.Escreen = TempData["info"].ToString();
                ViewBag.EscreenReturn = TempData["url"].ToString();
            }
            else
            {
                ViewBag.Escreen = "Request";

            }

            if (TempData["popup"] != null) { ViewBag.Popup = "1"; } else { ViewBag.Popup = "2"; }
            if (TempData["popupdata"] != null) { ViewBag.PopupData = TempData["popupdata"]; }



            BALGeneral ObjBALGeneral = new BALGeneral();
            NewReportModel ObjModel = new NewReportModel();
            int UserLocationId = 0;
            int pkCompanyId = 0;
            #region taleowork
            if (TempData["loginfrom"] != null)
            {

                if (Convert.ToString(TempData["loginfrom"]) == "taleo")
                {

                    string ouptut = string.Empty;
                    string xmlResponseCookie = Convert.ToString(TempData["xmlResponseCookie"]);
                    Session["xmlResponseCookie"] = Convert.ToString(TempData["xmlResponseCookie"]);
                    string Candidateurl = Convert.ToString(TempData["TaleoDispatcherUrlHit"]) + "object/candidate/" + Convert.ToInt32(TempData["TaleoCandidateId"]);
                    //comment for testing by ak on 06 may 2015


                    HttpWebRequest req1 = (HttpWebRequest)WebRequest.Create(Candidateurl);
                    req1.Method = "get";
                    req1.ContentType = "application/json";
                    req1.Headers.Add("Cookie: authToken=" + xmlResponseCookie.ToString());

                    //Make API call

                    try
                    {
                        HttpWebResponse resp1 = (HttpWebResponse)req1.GetResponse();

                        string xmlResponse2 = null;
                        using (StreamReader sr = new StreamReader(resp1.GetResponseStream()))
                        {
                            xmlResponse2 = sr.ReadToEnd();
                        }
                        xmlResponse2 = xmlResponse2.Substring(12, xmlResponse2.Length - 51);

                        ouptut = "1";
                        // var summonerInfo = JsonConvert.DeserializeObject<candidate>(xmlResponse2);
                        dynamic TaleoCandidateInfo_details = JObject.Parse(xmlResponse2);
                        // var json = resp1.Content.ReadAsStringAsync().Result; 



                        //

                        //Response TaleoCandidateInfo_details = JsonConvert.DeserializeObject<Response>(resp1);



                        #region object value fill in new report form  for takeo ND-16
                        ObjModel.FirstName = (string)TaleoCandidateInfo_details["candidate"]["firstName"];
                        ObjModel.LastName = (string)TaleoCandidateInfo_details["candidate"]["lastName"];
                        ObjModel.MI = (string)TaleoCandidateInfo_details["candidate"]["middleInitial"];
                        ObjModel.DOB = (string)TaleoCandidateInfo_details["candidate"]["birthdate"];
                        ObjModel.Phone = (string)TaleoCandidateInfo_details["candidate"]["cellPhone"];
                        ObjModel.ApplicantEmail = (string)TaleoCandidateInfo_details["candidate"]["email"];
                        ObjModel.Zip = (string)TaleoCandidateInfo_details["candidate"]["zipCode"];
                        ObjModel.City = (string)TaleoCandidateInfo_details["candidate"]["city"];
                        ObjModel.Social = (string)TaleoCandidateInfo_details["candidate"]["Emerge_SSN"];


                        TempData["Emerge_Order_Id"] = (string)TaleoCandidateInfo_details["candidate"]["Emerge_Order_Id"];
                        System.Web.HttpContext.Current.Session["Emerge_Order_Id"] = TempData["Emerge_Order_Id"];



                        #endregion

                    }

                    catch (WebException exx)
                    {

                        //Added log for exception handling.
                        VendorServices.GeneralService.WriteLog("Some Error Occured in NewReport(): " + DateTime.Now.ToString() + "\n Error Message :" + exx.Message + "\n Stack Trace :" + exx.StackTrace, "ManageProduct");
                        //Emerge.Controllers.DefaultController DEFAULTCONTROLLER = new Emerge.Controllers.DefaultController();

                        //string successResponse = DEFAULTCONTROLLER.taleologOut();
                        HttpWebResponse webResponse = (HttpWebResponse)exx.Response;
                        if (webResponse.StatusCode == HttpStatusCode.InternalServerError)
                        {

                            if (taleoGloalVariable <= 3)
                            {
                                taleoGloalVariable++;


                                //DEFAULTCONTROLLER.taleoDispatcherlogin();
                                //DEFAULTCONTROLLER.taleologin();
                                //DEFAULTCONTROLLER.TaleoUserWork();
                                NewReport();
                            }
                            else
                            {
                                //not connected to Taleoe ;

                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        //Added log for exception handling.
                        VendorServices.GeneralService.WriteLog("Some Error Occured in NewReport(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    }

                }

            }

            #endregion
            #region Get Company Id

            BALCompany ObjBALCompany = new BALCompany();
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            MembershipUser Member = Membership.GetUser(User.Identity.Name);
            if (Member != null)
            {
                var dbCollection = ObjBALCompanyUsers.GetUserCompanyInfo(new Guid(Member.ProviderUserKey.ToString()));
                if (dbCollection.Count > 0)
                {
                    Session["UserFullName"] = dbCollection.First().FirstName + " " + dbCollection.First().LastName;


                    int fkLocationId = Int32.Parse(dbCollection.First().pkLocationId.ToString());
                    UserLocationId = fkLocationId;
                    ObjModel.fkLocationId = fkLocationId;
                    ObjModel.pkCompanyUserId = Int32.Parse(dbCollection.First().pkCompanyUserId.ToString());
                    ObjModel.OrderCompanyName = dbCollection.First().CompanyName;
                    ObjModel.CompanyAccountNo = dbCollection.First().CompanyAccountNumber;
                    ObjModel.PermissiblePurposeId = dbCollection.First().PermissiblePurpose;
                    ObjModel.IsMIEnable = dbCollection.First().IsMIEnable;
                    pkCompanyId = dbCollection.First().pkCompanyId;
                    ObjModel.pkCompanyId = pkCompanyId;
                    ObjModel.Is7YearFilter = dbCollection.First().Is7YearFilter;
                    ObjModel.Is7YearAuto = dbCollection.First().Is7YearAutoCheck;
                    //int-9
                    ObjModel.isfcrjurisdictionfee = Convert.ToBoolean(dbCollection.First().isfcrjurisdictionfee);
                    ObjModel.IsSyncpodforcompany = dbCollection.First().IsSyncpod;
                    ObjModel.UserName = "dummy";
                    ObjModel.Password = "dummy";
                    ObjModel.IsWCVAcknowledged = dbCollection.First().IsWCVAcknowledged;
                    ObjModel.IsLiveRunnerEnabled = dbCollection.First().IsLiveRunnerEnabled;
                    ObjModel.IsturnoffSSN = dbCollection.First().IsturnoffSSN;
                    ObjModel.isEnabledBillingLocation = Convert.ToBoolean(dbCollection.First().isEnabledBillingLocation);
                }
            }


            #endregion



            #region Left Panel Categories

            List<Proc_Get_ReportCategoriesResult> ObjDataCategories = ObjBALGeneral.GetReportCategories().Where(t => t.pkReportCategoryId != 9).ToList();
            ObjModel.ReportCategoriesList = ObjDataCategories;

            Proc_Get_ReportCategoriesResult ObjDynamicColl = new Proc_Get_ReportCategoriesResult();
            //List<string> ObjCollRcxProducts = new List<string>();
            ObjDynamicColl.pkReportCategoryId = 0;
            ObjDynamicColl.CategoryName = "Packages";
            ObjDynamicColl.IsEnabled = true;
            ObjDataCategories.Insert(0, ObjDynamicColl);

            #endregion

            #region Package List

            List<Proc_Get_EmergeProductPackagesResult> ObjReports = ObjBALGeneral.GetEmergeProductPackages(UserLocationId);
            ObjModel.EmergeProductPackagesList = ObjReports;

            #endregion

            #region Categories Report List

            Dictionary<int, List<Proc_Get_NewReportsByCategoryIdResult>> DictCategory = new Dictionary<int, List<Proc_Get_NewReportsByCategoryIdResult>>();
            for (int i = 0; i < ObjDataCategories.Count; i++)
            {

                var NewReportsByCategory1 = ObjBALGeneral.GetNewReportsByCategoryId(UserLocationId, ObjDataCategories.ElementAt(i).pkReportCategoryId);
                DictCategory.Add(i, NewReportsByCategory1);
            }
            ObjModel.DictCategory = DictCategory;

            #endregion

            #region Drug Testing

            BALProducts ObjBALProducts = new BALProducts();
            List<Proc_GetDrugTestingProductsResult> ObjData = ObjBALProducts.GetDrugTestingProducts(UserLocationId);
            ObjModel.DrugTestingList = ObjData;

            #endregion

            ObjModel = BindNewReportDropdowns(ObjBALGeneral, ObjModel, pkCompanyId, ObjBALCompany);

            #region FcraAgreement

            ObjModel = PartialFcraAgreement(ObjModel);

            #endregion FcraAgreement

            #region Report Not Generating

            ObjModel = PartialReportNotGenerating(ObjModel);
            #endregion Report Not Generating

            ObjModel.OrderType = 1;//Normal Report
            ObjModel.hdnIsSCRLiveRunnerLocation = "0";

            var item = Objbaldrub.GetDrugtestingkit();
            ObjModel.SalivaPrice = item.Item1;
            ObjModel.EZcupPrice = item.Item2;
            var Itemlists = ObjBALCompany.IsEscreenEnableForCompany(pkCompanyId);
            ObjModel.IsEscreenEnable = Itemlists.Item1;
            #region Enable for Iframe Integration


            // int 260 iframe integration 
            if ((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]) != null)
            {

                if (Convert.ToString(((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["callingfrom"]) != null)
                {
                    if (Convert.ToString(((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["callingfrom"]) == "iframe")
                    {
                        try
                        {
                            // INT 260
                            ObjModel.FirstName = ((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["FirstName"] != null ? Convert.ToString(((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["FirstName"]) : "";

                            ObjModel.MI = ((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["MI"] != null ? Convert.ToString(((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["MI"]) : "";
                            ObjModel.LastName = ((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["LastName"] != null ? Convert.ToString(((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["LastName"]) : "";
                            ObjModel.Social = ((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["Social"] != null ? Convert.ToString(((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["Social"]) : "";
                            ObjModel.DOB = ((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["DOB"] != null ? Convert.ToString(((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["DOB"]) : "";
                            ObjModel.ApplicantEmail = ((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["ApplicantEmail"] != null ? Convert.ToString(((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["ApplicantEmail"]) : "";


                            if (((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["State"] != null)
                            {
                                String state = Convert.ToString(((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["State"]);


                                var IframeState = from StateList in ObjModel.StateList
                                                  where StateList.StateName.Contains(state)
                                                  select new { pkStateId = StateList.pkStateId }.pkStateId.FirstOrDefault();


                                if (IframeState.Count() == 0)
                                {


                                    using (EmergeDALDataContext dbObj1 = new EmergeDALDataContext())
                                    {
                                        string IframeState_code = (from tstate in dbObj1.tblStates
                                                                   where tstate.StateCode == state
                                                                   select tstate.StateName).FirstOrDefault();
                                        var IframeStateCode = from StateList in ObjModel.StateList
                                                              where StateList.StateName.Contains(IframeState_code)
                                                              select new { pkStateId = StateList.pkStateId };
                                        ObjModel.State = IframeStateCode != null ? IframeStateCode.FirstOrDefault().pkStateId.ToString() : "-1";

                                    }








                                }
                                else
                                {
                                    var IframeStateCode = from StateList in ObjModel.StateList
                                                          where StateList.StateName.Contains(state)
                                                          select new { pkStateId = StateList.pkStateId };
                                    ObjModel.State = IframeStateCode != null ? IframeStateCode.FirstOrDefault().pkStateId.ToString() : "-1";
                                }






                            }

                            ObjModel.SocialMask = ((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["SocialMask"] != null ? Convert.ToString(((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["SocialMask"]) : "";
                            ObjModel.Phone = ((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["Phone"] != null ? Convert.ToString(((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["Phone"]) : "";
                            ObjModel.Address = ((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["Address"] != null ? Convert.ToString(((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["Address"]) : "";
                            ObjModel.Street = ((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["Street"] != null ? Convert.ToString(((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["Street"]) : "";
                            ObjModel.Apt = ((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["Apt"] != null ? Convert.ToString(((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["Apt"]) : "";
                            ObjModel.City = ((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["City"] != null ? Convert.ToString(((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["City"]) : "";
                            ObjModel.Zip = ((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["Zip"] != null ? Convert.ToString(((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["Zip"]) : "";
                            ObjModel.Sex = ((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["Sex"] != null ? Convert.ToString(((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["Sex"]) : "";




                            ObjModel.VehicleVin = ((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["VehicleVin"] != null ? Convert.ToString(((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["VehicleVin"]) : "";
                            ObjModel.DriverLicense = ((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["DriverLicense"] != null ? Convert.ToString(((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["DriverLicense"]) : "";


                            if (((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["Jurisdiction"] != null)
                            {
                                String Jurisdiction = Convert.ToString(((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["Jurisdiction"]);

                                var IframeJurisdiction = from student in ObjModel.JurisdictionList
                                                         where student.JurisdictionName.Contains(Jurisdiction)
                                                         select new { pkJurisdictionId = student.pkJurisdictionId };
                                ObjModel.Jurisdiction = IframeJurisdiction != null ? IframeJurisdiction.FirstOrDefault().pkJurisdictionId.ToString() : "-1";


                            }

                            if (((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["DLState"] != null)
                            {
                                String DLState = Convert.ToString(((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["DLState"]);


                                ObjModel.DLState = DLState != null ? Convert.ToString(DLState) : "";

                            }

                            if (((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["SuffixValue"] != null)
                            {
                                String SuffixValue = Convert.ToString(((System.Collections.Specialized.NameValueCollection)(TempData["IframeData"]))["SuffixValue"]);

                                var IframeSuffixValue = from student in ObjModel.SuffixList
                                                        where student.Key.Contains(SuffixValue)
                                                        select new { Value = student.Value };
                                ObjModel.SuffixValue = IframeSuffixValue != null ? IframeSuffixValue.FirstOrDefault().Value.ToString() : "-1";

                            }






                        }




                        catch //(Exception asd)
                        {
                        }


                    }
                }


            }




            #endregion
            return View(ObjModel);
        }
        public ActionResult RunAllCounty(string OrderId, string LastName)
        {
            BALGeneral ObjBALGeneral = new BALGeneral();
            NewReportModel ObjModel = new NewReportModel();
            int UserLocationId = 0;
            int pkCompanyId = 0;
            int ParentOrderId = 0;
            ObjModel.ParentOrderId = 0;
            BALOrders ObjBALOrders = new BALOrders();
            if (OrderId != null)
            {
                ParentOrderId = Int32.Parse(OrderId);
                ObjModel.ParentOrderId = ParentOrderId;
            }
            if (LastName != null)
            {
                ObjModel.LastName = LastName;
            }
            #region Get Company Id

            CustomOrderCollection ObjCustomOrderCollection = new CustomOrderCollection();
            try
            {
                ObjCustomOrderCollection = ObjBALOrders.GetOrderDetailsByOrderId(ParentOrderId);
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in RunAllCounty(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }

            BALCompany ObjBALCompany = new BALCompany();
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            MembershipUser Member = Membership.GetUser(User.Identity.Name);
            if (Member != null)
            {
                var dbCollection = ObjBALCompanyUsers.GetUserCompanyInfo(new Guid(Member.ProviderUserKey.ToString()));
                if (dbCollection.Count > 0)
                {
                    Session["UserFullName"] = dbCollection.First().FirstName + " " + dbCollection.First().LastName;


                    int fkLocationId = Int32.Parse(dbCollection.First().pkLocationId.ToString());
                    UserLocationId = fkLocationId;
                    ObjModel.fkLocationId = fkLocationId;
                    ObjModel.pkCompanyUserId = Int32.Parse(dbCollection.First().pkCompanyUserId.ToString());
                    ObjModel.OrderCompanyName = dbCollection.First().CompanyName;
                    ObjModel.CompanyAccountNo = dbCollection.First().CompanyAccountNumber;
                    ObjModel.PermissiblePurposeId = dbCollection.First().PermissiblePurpose;
                    ObjModel.IsMIEnable = dbCollection.First().IsMIEnable;
                    pkCompanyId = dbCollection.First().pkCompanyId;
                    ObjModel.pkCompanyId = pkCompanyId;
                    ObjModel.IsSyncpodforcompany = dbCollection.First().IsSyncpod;
                    ObjModel.UserName = "dummy";
                    ObjModel.Password = "dummy";
                    ObjModel.IsWCVAcknowledged = dbCollection.First().IsWCVAcknowledged;
                    ObjModel.IsLiveRunnerEnabled = dbCollection.First().IsLiveRunnerEnabled;
                }
            }






            #region Bind Order Searched Data
            try
            {
                if (ObjCustomOrderCollection.ObjOrderSearchedData != null)
                {
                    ObjModel.LastName = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedLastName);
                    ObjModel.FirstName = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedFirstName);
                    ObjModel.MI = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedMiddleInitial);
                    ObjModel.SuffixValue = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedSuffix);
                    ObjModel.Social = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedSsn);
                    var newssn = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedSsn);
                    var SocialWithMask = newssn;
                    if (newssn != null && newssn != string.Empty)
                    {
                        var splitssn = newssn.Split('-').ElementAt(2);
                        SocialWithMask = "***-**" + splitssn;
                    }
                    ObjModel.SocialMask = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedSsn); ;
                    ObjModel.Social = SocialWithMask;
                    ObjModel.DOB = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedDob);
                    ObjModel.Phone = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedPhoneNumber);
                    ObjModel.Address = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedStreetAddress);
                    ObjModel.DirectionValue = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedDirection);
                    ObjModel.Street = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedStreetName);
                    ObjModel.TypeValue = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedStreetType);
                    ObjModel.Apt = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedApt);
                    ObjModel.City = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedCity);
                    ObjModel.ApplicantEmail = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedApplicantEmail);
                    ObjModel.pkStateId = Convert.ToInt32(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedStateId);
                    string strstate = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedStateId);
                    ObjModel.State = strstate;
                    if (ObjCustomOrderCollection.ObjOrderSearchedData.SearchedZipCode.Length > 5)
                    {
                        ObjCustomOrderCollection.ObjOrderSearchedData.SearchedZipCode = ObjCustomOrderCollection.ObjOrderSearchedData.SearchedZipCode.Substring(0, 5);
                    }
                    ObjModel.Zip = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedZipCode);
                    if (ObjCustomOrderCollection.ObjOrderSearchedData.SearchedCountyId != null)
                    {
                        ObjModel.AllowJurisdiction = true;
                        ObjModel.Jurisdiction = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedCountyId);
                    }
                    else
                    {
                        ObjModel.AllowJurisdiction = false;
                    }

                    #region Bind Business Info
                    ObjModel.BusinessName = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedBusinessName);
                    ObjModel.BusinessCity = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedBusinessCity);

                    ObjModel.BusinessState = Convert.ToInt32(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedBusinessStateId);

                    #endregion Bind Business Info

                    #region Bind Automative Info

                    string str = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedDLStateId);
                    ObjModel.DLState = str;
                    ObjModel.DriverLicense = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedDriverLicense);
                    ObjModel.VehicleVin = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedVehicleVin);
                    ObjModel.Sex = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedSex);

                    #endregion Bind Automative Info

                    #region Get Jurisdiction For Report Log

                    List<clsJurisdiction> ObjJudCollection = ObjBALGeneral.GetJurisdictionsForNewReportLog();
                    ObjJudCollection.Insert(0, new clsJurisdiction { pkJurisdictionId = "-1", JurisdictionName = "" });
                    ObjModel.JurisdictionList = ObjJudCollection;

                    #endregion
                }
                if (ObjModel.hfReports != null)
                {
                    if (ObjModel.hfReports.Contains("SCR"))
                    {
                        string IsLiveRunner = ObjModel.pkStateId.ToString().Split('_').ElementAt(1);
                        ObjModel.IsLiveRunner = IsLiveRunner;
                    }
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in RunAllCounty(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            #endregion Bind Order Searched Data





            #endregion

            #region Bind Order County Info
            try
            {
                if (ObjCustomOrderCollection.ListOrderSearchedLiveRunnerInfo.Count > 0)
                {
                    ObjModel.ObjOrderSearchedLiveRunnerInfo = ObjCustomOrderCollection.ListOrderSearchedLiveRunnerInfo;
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in RunAllCounty(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            #endregion Bind Order County Info


            #region Bind Order Employment Info
            try
            {
                if (ObjCustomOrderCollection.ListOrderSearchedEmploymentInfo.Count > 0)
                {
                    ObjModel.ObjSearchedEmploymentInfo = ObjCustomOrderCollection.ListOrderSearchedEmploymentInfo;
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in RunAllCounty(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            #endregion Bind Order Employment Info

            #region Bind Order Education Info
            try
            {
                if (ObjCustomOrderCollection.ListOrderSearchedEducationInfo.Count > 0)
                {
                    ObjModel.ObjSearchedEducationInfo = ObjCustomOrderCollection.ListOrderSearchedEducationInfo;
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in RunAllCounty(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            #endregion Bind Order Education Info

            #region Bind Order Personal Info
            try
            {
                if (ObjCustomOrderCollection.ListOrderSearchedPersonalInfo.Count > 0)
                {
                    ObjModel.ObjOrderSearchedPersonalInfo = ObjCustomOrderCollection.ListOrderSearchedPersonalInfo;
                }

            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in RunAllCounty(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }

            #endregion Bind Order Personal Info

            #region Bind Order License Info
            try
            {
                if (ObjCustomOrderCollection.ListOrderSearchedLicenseInfo.Count > 0)
                {
                    ObjModel.ObjOrderSearchedLicenseInfo = ObjCustomOrderCollection.ListOrderSearchedLicenseInfo;
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in RunAllCounty(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }

            #endregion Bind Order License Info

            #region Bind Order Tracking Info
            try
            {
                if (ObjCustomOrderCollection.ObjReferenceCode != null)
                {
                    ObjModel.TrackingNotes = CheckNull(ObjCustomOrderCollection.ObjReferenceCode.pkReferenceCodeId);
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in RunAllCounty(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }

            #endregion Bind Order Tracking Info

            #region Top Blocks
            try
            {
                //ObjModel.ErrorMessage = " Count: " + ObjCustomOrderCollection.ListOrderDetail.Count;
                //if (ObjCustomOrderCollection.ListOrderDetail.Count > 0)
                //{
                BindTopBlocks(ObjCustomOrderCollection.ObjOrder, ObjCustomOrderCollection.ListOrderDetail, ObjModel);
                //}
            }
            catch (Exception ex)
            {
                ObjModel.ErrorMessage += "Top Block: " + ex.Message.ToString();
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in RunAllCounty(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }

            #endregion

            #region Bind Error Log

            try
            {
                if (ObjCustomOrderCollection.ListOrderDetail.Count > 0)
                {
                    if (ObjCustomOrderCollection.ListOrderDetail != null && ObjCustomOrderCollection.ListOrderResponseData != null)
                    {
                        BindErrorLog(ObjCustomOrderCollection.ObjOrder, ObjCustomOrderCollection.ListOrderDetail, ObjCustomOrderCollection.ListOrderResponseData, ObjModel);
                    }
                }
            }
            catch (Exception ex)
            {
                ObjModel.ErrorMessage += "ErrorLog: " + ex.Message.ToString();
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in RunAllCounty(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }


            #endregion



            #region Left Panel Categories

            List<Proc_Get_ReportCategoriesResult> ObjDataCategories = ObjBALGeneral.GetReportCategories().Where(t => t.pkReportCategoryId != 9).ToList();
            ObjModel.ReportCategoriesList = ObjDataCategories;

            Proc_Get_ReportCategoriesResult ObjDynamicColl = new Proc_Get_ReportCategoriesResult();
            //List<string> ObjCollRcxProducts = new List<string>();
            ObjDynamicColl.pkReportCategoryId = 0;
            ObjDynamicColl.CategoryName = "Packages";
            ObjDynamicColl.IsEnabled = true;
            ObjDataCategories.Insert(0, ObjDynamicColl);

            #endregion

            #region Package List

            List<Proc_Get_EmergeProductPackagesResult> ObjReports = ObjBALGeneral.GetEmergeProductPackages(UserLocationId);
            ObjModel.EmergeProductPackagesList = ObjReports;

            #endregion

            #region Categories Report List

            Dictionary<int, List<Proc_Get_NewReportsByCategoryIdResult>> DictCategory = new Dictionary<int, List<Proc_Get_NewReportsByCategoryIdResult>>();
            for (int i = 0; i < ObjDataCategories.Count; i++)
            {

                var NewReportsByCategory1 = ObjBALGeneral.GetNewReportsByCategoryId(UserLocationId, ObjDataCategories.ElementAt(i).pkReportCategoryId);
                DictCategory.Add(i, NewReportsByCategory1);
            }
            ObjModel.DictCategory = DictCategory;

            #endregion

            #region Drug Testing

            BALProducts ObjBALProducts = new BALProducts();
            List<Proc_GetDrugTestingProductsResult> ObjData = ObjBALProducts.GetDrugTestingProducts(UserLocationId);
            ObjModel.DrugTestingList = ObjData;

            #endregion

            ObjModel = BindNewReportDropdowns(ObjBALGeneral, ObjModel, pkCompanyId, ObjBALCompany);

            #region FcraAgreement

            ObjModel = PartialFcraAgreement(ObjModel);

            #endregion FcraAgreement

            #region Report Not Generating

            ObjModel = PartialReportNotGenerating(ObjModel);
            #endregion Report Not Generating

            ObjModel.OrderType = 1;//Normal Report
            ObjModel.hdnIsSCRLiveRunnerLocation = "0";
            return PartialView("RunAllCounty", ObjModel);
        }
        public ActionResult RedirectToPageAddAdditinalReport(string OrderId, string AplicantNme, string Runcounty, string list)
        {
            TempData["additinalreport"] = Runcounty;
            string Collectionlist = string.Empty;
            using (EmergeDALDataContext dx1 = new EmergeDALDataContext())
            {
                if (string.IsNullOrEmpty(list) == false)
                {
                    var Collection = dx1.tblStates.ToList();
                    var element = list.ToString().Split(new[] { "countystate" }, StringSplitOptions.None);
                    for (int i = 0; i < element.Length; i++)
                    {
                        if (string.IsNullOrEmpty(element[i].ToString()) == false)
                        {
                            var County_State = element[i].ToString().Split(new Char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                            string County = County_State[0].ToString().Trim();
                            string State = County_State[1].ToString().Trim();
                            var State_Name = "";
                            if (string.IsNullOrEmpty(State) == false)
                            {
                                State_Name = Collection.Where(t => t.StateCode.ToLower() == State.ToLower()).Select(t => t.StateName).FirstOrDefault();
                            }
                            if (string.IsNullOrEmpty(Collectionlist) == false) { Collectionlist = Collectionlist + "countystate" + County.Replace("st.", "Saint") + "," + State_Name; }
                            else { Collectionlist = "countystate" + County.Replace("St.", "SAINT") + "," + State_Name; }

                        }
                    }
                }
            }
            TempData["list"] = Collectionlist;
            return RedirectToAction("AddAdditinalReport", new { OrderId = OrderId, AplicantNme = AplicantNme });
        }


        public ActionResult AddAdditinalReport()
        {
            BALGeneral ObjBALGeneral = new BALGeneral();
            NewReportModel ObjModel = new NewReportModel();
            int UserLocationId = 0;
            int pkCompanyId = 0;
            int ParentOrderId = 0;
            ObjModel.ParentOrderId = 0;
            BALOrders ObjBALOrders = new BALOrders();
            ViewBag.additinalreport = 0;
            if (Request.QueryString["OrderId"] != null)
            {
                ParentOrderId = Int32.Parse(Request.QueryString["OrderId"].ToString());
                ObjModel.ParentOrderId = ParentOrderId;
            }
            if (Request.QueryString["AplicantNme"] != null)
            {
                ObjModel.LastName = Request.QueryString["AplicantNme"].ToString();
            }
            if (TempData["additinalreport"] != null)
            {
                if (TempData["additinalreport"].ToString() == "2")
                {
                    ViewBag.additinalreport = 1;
                }
            }
            ViewBag.list = string.Empty;
            if (TempData["list"] != null)
            {

                ViewBag.list = TempData["list"].ToString();

            }
            //ViewBag.Dob = string.Empty;
            //if (TempData["Dob"] != null)
            //{
            //    ViewBag.Dob = TempData["Dob"].ToString(); 
            //}

            #region Get Company Id

            CustomOrderCollection ObjCustomOrderCollection = new CustomOrderCollection();
            try
            {
                ObjCustomOrderCollection = ObjBALOrders.GetOrderDetailsByOrderId(ParentOrderId);

                #region---- Employment Information
                if (ObjCustomOrderCollection.ListOrderSearchedEmploymentInfo.Count > 0)
                {
                    ObjModel.CompanyState = Convert.ToInt32(ObjCustomOrderCollection.ListOrderSearchedEmploymentInfo[0].SearchedState);
                    ObjModel.CompanyCity = ObjCustomOrderCollection.ListOrderSearchedEmploymentInfo[0].SearchedCity;
                    ObjModel.CompanyPhone = ObjCustomOrderCollection.ListOrderSearchedEmploymentInfo[0].SearchedPhone;
                    ObjModel.AliasName = ObjCustomOrderCollection.ListOrderSearchedEmploymentInfo[0].SearchedAlias;
                    ObjModel.CompanyName = ObjCustomOrderCollection.ListOrderSearchedEmploymentInfo[0].SearchedName;
                }
                #endregion

                #region---- Educational Information
                if (ObjCustomOrderCollection.ListOrderSearchedEducationInfo.Count > 0)
                {
                    ObjModel.SchoolName = ObjCustomOrderCollection.ListOrderSearchedEducationInfo[0].SearchedSchoolName;
                    ObjModel.InstituteState = Convert.ToInt32(ObjCustomOrderCollection.ListOrderSearchedEducationInfo[0].SearchedState);
                    ObjModel.SchoolCity = ObjCustomOrderCollection.ListOrderSearchedEducationInfo[0].SearchedCity;
                    ObjModel.SchoolPhone = ObjCustomOrderCollection.ListOrderSearchedEducationInfo[0].SearchedPhone;
                    ObjModel.InstituteAlias = ObjCustomOrderCollection.ListOrderSearchedEducationInfo[0].SearchedAlias;
                }
                #endregion

                #region---- Personal Information
                if (ObjCustomOrderCollection.ListOrderSearchedPersonalInfo.Count > 0)
                {
                    ObjModel.ReferenceName = ObjCustomOrderCollection.ListOrderSearchedPersonalInfo[0].SearchedName;
                    ObjModel.ReferencePhone = ObjCustomOrderCollection.ListOrderSearchedPersonalInfo[0].SearchedPhone;
                    ObjModel.ReferenceAlias = ObjCustomOrderCollection.ListOrderSearchedPersonalInfo[0].SearchedAlias;
                }
                #endregion

                #region---- LicenseType Information
                if (ObjCustomOrderCollection.ListOrderSearchedLicenseInfo.Count > 0)
                {
                    ObjModel.LicenseType = ObjCustomOrderCollection.ListOrderSearchedLicenseInfo[0].SearchedLicenseType;
                    ObjModel.LicenseState = Convert.ToInt32(ObjCustomOrderCollection.ListOrderSearchedLicenseInfo[0].SearchedIssuingState);
                    ObjModel.LicenseNumber = ObjCustomOrderCollection.ListOrderSearchedLicenseInfo[0].SearchedLicenseNo;
                    ObjModel.LicenseIssueDate = ObjCustomOrderCollection.ListOrderSearchedLicenseInfo[0].SearchedIssueDate;
                    ObjModel.LicenseAlias = ObjCustomOrderCollection.ListOrderSearchedLicenseInfo[0].SearchedAlias;
                }
                #endregion

            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in AddAdditinalReport(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }

            BALCompany ObjBALCompany = new BALCompany();
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            MembershipUser Member = Membership.GetUser(User.Identity.Name);
            if (Member != null)
            {
                var dbCollection = ObjBALCompanyUsers.GetUserCompanyInfo(new Guid(Member.ProviderUserKey.ToString()));
                if (dbCollection.Count > 0)
                {
                    Session["UserFullName"] = dbCollection.First().FirstName + " " + dbCollection.First().LastName;
                    int fkLocationId = Int32.Parse(dbCollection.First().pkLocationId.ToString());
                    UserLocationId = fkLocationId;
                    ObjModel.fkLocationId = fkLocationId;
                    ObjModel.pkCompanyUserId = Int32.Parse(dbCollection.First().pkCompanyUserId.ToString());
                    ObjModel.OrderCompanyName = dbCollection.First().CompanyName;
                    ObjModel.CompanyAccountNo = dbCollection.First().CompanyAccountNumber;
                    ObjModel.PermissiblePurposeId = dbCollection.First().PermissiblePurpose;
                    ObjModel.IsMIEnable = dbCollection.First().IsMIEnable;
                    pkCompanyId = dbCollection.First().pkCompanyId;
                    ObjModel.pkCompanyId = pkCompanyId;
                    ObjModel.IsSyncpodforcompany = dbCollection.First().IsSyncpod;
                    ObjModel.UserName = "dummy";
                    ObjModel.Password = "dummy";
                    ObjModel.IsWCVAcknowledged = dbCollection.First().IsWCVAcknowledged;
                    ObjModel.IsLiveRunnerEnabled = dbCollection.First().IsLiveRunnerEnabled;
                }
            }






            #region Bind Order Searched Data
            try
            {
                if (ObjCustomOrderCollection.ObjOrderSearchedData != null)
                {
                    ObjModel.LastName = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedLastName);
                    ObjModel.FirstName = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedFirstName);
                    ObjModel.MI = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedMiddleInitial);
                    ObjModel.SuffixValue = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedSuffix);
                    ObjModel.Social = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedSsn);
                    var newssn = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedSsn);
                    var SocialWithMask = newssn;
                    if (newssn != null && newssn != string.Empty)
                    {
                        var splitssn = newssn.Split('-').ElementAt(2);
                        SocialWithMask = "***-**" + splitssn;
                    }
                    ObjModel.SocialMask = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedSsn); ;
                    ObjModel.Social = SocialWithMask;
                    ObjModel.DOB = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedDob);
                    ObjModel.Phone = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedPhoneNumber);
                    ObjModel.Address = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedStreetAddress);
                    ObjModel.DirectionValue = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedDirection);
                    ObjModel.Street = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedStreetName);
                    ObjModel.TypeValue = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedStreetType);
                    ObjModel.Apt = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedApt);
                    ObjModel.City = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedCity);
                    ObjModel.ApplicantEmail = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedApplicantEmail);
                    ObjModel.pkStateId = Convert.ToInt32(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedStateId);
                    string strstate = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedStateId);
                    ObjModel.State = strstate;
                    if (ObjCustomOrderCollection.ObjOrderSearchedData.SearchedZipCode.Length > 5)
                    {
                        ObjCustomOrderCollection.ObjOrderSearchedData.SearchedZipCode = ObjCustomOrderCollection.ObjOrderSearchedData.SearchedZipCode.Substring(0, 5);
                    }

                    ObjModel.Zip = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedZipCode);
                    if (ObjCustomOrderCollection.ObjOrderSearchedData.SearchedCountyId != null)
                    {
                        ObjModel.AllowJurisdiction = true;
                        ObjModel.Jurisdiction = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedCountyId);
                    }
                    else
                    {
                        ObjModel.AllowJurisdiction = false;
                    }

                    #region Bind Business Info
                    ObjModel.BusinessName = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedBusinessName);
                    ObjModel.BusinessCity = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedBusinessCity);

                    ObjModel.BusinessState = Convert.ToInt32(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedBusinessStateId);

                    #endregion Bind Business Info

                    #region Bind Automative Info

                    string str = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedDLStateId);
                    ObjModel.DLState = str;
                    ObjModel.DriverLicense = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedDriverLicense);
                    ObjModel.VehicleVin = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedVehicleVin);
                    ObjModel.Sex = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedSex);

                    #endregion Bind Automative Info

                    #region Get Jurisdiction For Report Log

                    List<clsJurisdiction> ObjJudCollection = ObjBALGeneral.GetJurisdictionsForNewReportLog();
                    ObjJudCollection.Insert(0, new clsJurisdiction { pkJurisdictionId = "-1", JurisdictionName = "" });
                    ObjModel.JurisdictionList = ObjJudCollection;

                    #endregion
                }
                if (ObjModel.hfReports != null)
                {
                    if (ObjModel.hfReports.Contains("SCR"))
                    {
                        string IsLiveRunner = ObjModel.pkStateId.ToString().Split('_').ElementAt(1);
                        ObjModel.IsLiveRunner = IsLiveRunner;
                    }
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in AddAdditinalReport(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            #endregion Bind Order Searched Data





            #endregion

            #region Bind Order County Info
            try
            {
                if (ObjCustomOrderCollection.ListOrderSearchedLiveRunnerInfo.Count > 0)
                {
                    ObjModel.ObjOrderSearchedLiveRunnerInfo = ObjCustomOrderCollection.ListOrderSearchedLiveRunnerInfo;
                    ObjModel.LiveRunnerState = ObjCustomOrderCollection.ListOrderSearchedLiveRunnerInfo[0].LiveRunnerState;

                }
                else
                {
                    ObjModel.LiveRunnerState = "0";
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in AddAdditinalReport(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            #endregion Bind Order County Info


            #region Bind Order Employment Info
            try
            {
                if (ObjCustomOrderCollection.ListOrderSearchedEmploymentInfo.Count > 0)
                {
                    ObjModel.ObjSearchedEmploymentInfo = ObjCustomOrderCollection.ListOrderSearchedEmploymentInfo;
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in AddAdditinalReport(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            #endregion Bind Order Employment Info

            #region Bind Order Education Info
            try
            {
                if (ObjCustomOrderCollection.ListOrderSearchedEducationInfo.Count > 0)
                {
                    ObjModel.ObjSearchedEducationInfo = ObjCustomOrderCollection.ListOrderSearchedEducationInfo;
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in AddAdditinalReport(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            #endregion Bind Order Education Info

            #region Bind Order Personal Info
            try
            {
                if (ObjCustomOrderCollection.ListOrderSearchedPersonalInfo.Count > 0)
                {
                    ObjModel.ObjOrderSearchedPersonalInfo = ObjCustomOrderCollection.ListOrderSearchedPersonalInfo;

                }

            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in AddAdditinalReport(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }

            #endregion Bind Order Personal Info

            #region Bind Order License Info
            try
            {
                if (ObjCustomOrderCollection.ListOrderSearchedLicenseInfo.Count > 0)
                {
                    ObjModel.ObjOrderSearchedLicenseInfo = ObjCustomOrderCollection.ListOrderSearchedLicenseInfo;
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in AddAdditinalReport(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }

            #endregion Bind Order License Info

            #region Bind Order Tracking Info
            try
            {
                if (ObjCustomOrderCollection.ObjReferenceCode != null)
                {
                    ObjModel.TrackingNotes = CheckNull(ObjCustomOrderCollection.ObjReferenceCode.pkReferenceCodeId);
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in AddAdditinalReport(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }

            #endregion Bind Order Tracking Info

            #region Top Blocks
            try
            {
                //ObjModel.ErrorMessage = " Count: " + ObjCustomOrderCollection.ListOrderDetail.Count;
                //if (ObjCustomOrderCollection.ListOrderDetail.Count > 0)
                //{
                BindTopBlocks(ObjCustomOrderCollection.ObjOrder, ObjCustomOrderCollection.ListOrderDetail, ObjModel);
                //}
            }
            catch (Exception ex)
            {
                ObjModel.ErrorMessage += "Top Block: " + ex.Message.ToString();
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in AddAdditinalReport(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }

            #endregion

            #region Bind Error Log

            try
            {
                if (ObjCustomOrderCollection.ListOrderDetail.Count > 0)
                {
                    if (ObjCustomOrderCollection.ListOrderDetail != null && ObjCustomOrderCollection.ListOrderResponseData != null)
                    {
                        BindErrorLog(ObjCustomOrderCollection.ObjOrder, ObjCustomOrderCollection.ListOrderDetail, ObjCustomOrderCollection.ListOrderResponseData, ObjModel);
                    }
                }
            }
            catch (Exception ex)
            {
                ObjModel.ErrorMessage += "ErrorLog: " + ex.Message.ToString();
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in AddAdditinalReport(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }


            #endregion



            #region Left Panel Categories

            List<Proc_Get_ReportCategoriesResult> ObjDataCategories = ObjBALGeneral.GetReportCategories().Where(t => t.pkReportCategoryId != 9).ToList();
            ObjModel.ReportCategoriesList = ObjDataCategories;

            Proc_Get_ReportCategoriesResult ObjDynamicColl = new Proc_Get_ReportCategoriesResult();
            //List<string> ObjCollRcxProducts = new List<string>();
            ObjDynamicColl.pkReportCategoryId = 0;
            ObjDynamicColl.CategoryName = "Packages";
            ObjDynamicColl.IsEnabled = true;
            ObjDataCategories.Insert(0, ObjDynamicColl);

            #endregion

            #region Package List

            List<Proc_Get_EmergeProductPackagesResult> ObjReports = ObjBALGeneral.GetEmergeProductPackages(UserLocationId);
            ObjModel.EmergeProductPackagesList = ObjReports;

            #endregion

            #region Categories Report List

            Dictionary<int, List<Proc_Get_NewReportsByCategoryIdResult>> DictCategory = new Dictionary<int, List<Proc_Get_NewReportsByCategoryIdResult>>();
            for (int i = 0; i < ObjDataCategories.Count; i++)
            {

                var NewReportsByCategory1 = ObjBALGeneral.GetNewReportsByCategoryId(UserLocationId, ObjDataCategories.ElementAt(i).pkReportCategoryId);
                DictCategory.Add(i, NewReportsByCategory1);
            }
            ObjModel.DictCategory = DictCategory;

            #endregion

            #region Drug Testing

            BALProducts ObjBALProducts = new BALProducts();
            List<Proc_GetDrugTestingProductsResult> ObjData = ObjBALProducts.GetDrugTestingProducts(UserLocationId);
            ObjModel.DrugTestingList = ObjData;

            #endregion

            ObjModel = BindNewReportDropdowns(ObjBALGeneral, ObjModel, pkCompanyId, ObjBALCompany);

            string val = string.Empty;
            if (ObjModel.StateList.Count > 3)
            {
                val = ObjModel.pkStateId + "_";
            }
            else
            {
                val = "0_";
            }
            ObjModel.pkStateIdSelected = ObjModel.StateList.Where(u => u.pkStateId.Contains(val)).Select(u => u.pkStateId).FirstOrDefault();




            #region FcraAgreement

            ObjModel = PartialFcraAgreement(ObjModel);

            #endregion FcraAgreement

            #region Report Not Generating

            ObjModel = PartialReportNotGenerating(ObjModel);
            #endregion Report Not Generating

            ObjModel.OrderType = 1;//Normal Report
            ObjModel.hdnIsSCRLiveRunnerLocation = "0";




            return View(ObjModel);
        }

        public ActionResult UpdateAcknowledgeWCV(string pkCompanyUserId)
        {
            BALCompanyUsers objBALCompanyUsers = new BALCompanyUsers();
            int status = objBALCompanyUsers.UpdateWCVAcknowlwdgeInDB(Convert.ToInt32(pkCompanyUserId));
            return Json(new { record = status });
        }

        public NewReportModel BindNewReportDropdowns(BALGeneral ObjBALGeneral, NewReportModel ObjModel, int pkCompanyId, BALCompany ObjBALCompany)
        {
            #region State List SCR/ Application Information

            List<clsStates> ObjCollection = ObjBALGeneral.GetStatesSCR();
            ObjCollection.Insert(0, new clsStates { pkStateId = "-1", StateName = "" });

            ObjModel.StateList = ObjCollection;

            #endregion

            #region State List

            List<tblState> ObjCollectionState = ObjBALGeneral.GetStates();
            ObjCollectionState.Insert(0, new tblState { pkStateId = -1, StateName = "" });
            ObjModel.ObjStateList = ObjCollectionState;

            #endregion

            #region State List MVR

            List<clsStates> ObjCollectionMVR = ObjBALGeneral.GetStatesMVR();
            ObjCollectionMVR.Insert(0, new clsStates { pkStateId = "-1", StateName = "" });
            ObjModel.StateListMVR = ObjCollectionMVR;


            #endregion

            #region County List

            List<clsCounty> ObjCounty = new List<clsCounty>();
            ObjCounty.Add(new clsCounty { pkCountyId = "-1", CountyName = "" });
            ObjModel.ObjCountyList = ObjCounty;

            #endregion

            #region Get All Reference Code


            List<tblReferenceCode> ObjList = new List<tblReferenceCode>();
            ObjList = ObjBALCompany.GetAllReferenceCodes(pkCompanyId);

            ObjList.Insert(0, new tblReferenceCode { ReferenceCode = "Select Reference #", pkReferenceCodeId = -1 });
            ObjList.Add(new tblReferenceCode { ReferenceCode = "------------------", pkReferenceCodeId = -2 });
            ObjList.Add(new tblReferenceCode { ReferenceCode = "Add New", pkReferenceCodeId = -3 });
            ObjModel.ReferenceCodeList = ObjList;

            #endregion

            #region Get Jurisdiction

            List<clsJurisdiction> ObjJudCollection = ObjBALGeneral.GetJurisdictionsForNewReport();
            ObjJudCollection.Insert(0, new clsJurisdiction { pkJurisdictionId = "-1", JurisdictionName = "" });
            ObjModel.JurisdictionList = ObjJudCollection;

            #endregion

            #region SuffixLIst

            Dictionary<string, string> SuffixList = new Dictionary<string, string>();
            SuffixList.Add("-1", "");
            SuffixList.Add("JR", "JR");
            SuffixList.Add("SR", "SR");
            SuffixList.Add("I", "I");
            SuffixList.Add("II", "II");
            SuffixList.Add("III", "III");
            SuffixList.Add("IV", "IV");
            SuffixList.Add("V", "V");
            ObjModel.SuffixList = SuffixList;
            #endregion

            #region Direction

            Dictionary<string, string> DirectionList = new Dictionary<string, string>();
            DirectionList.Add("-1", "");
            DirectionList.Add("E", "E");
            DirectionList.Add("N", "N");
            DirectionList.Add("NE", "NE");
            DirectionList.Add("NW", "NW");
            DirectionList.Add("S", "S");
            DirectionList.Add("SE", "SE");
            DirectionList.Add("SW", "SW");
            DirectionList.Add("W", "W");
            ObjModel.DirectionList = DirectionList;

            #endregion

            #region Type

            Dictionary<string, string> TypeList = new Dictionary<string, string>();
            TypeList.Add("-1", "");
            TypeList.Add("", "None");
            TypeList.Add("AL", "Alley");
            TypeList.Add("AV", "Avenue");
            TypeList.Add("BV", "Boulevard");
            TypeList.Add("BD", "Building");
            TypeList.Add("CN", "Center");
            TypeList.Add("CI", "Circle");
            TypeList.Add("CT", "Court");
            TypeList.Add("CS", "Crescent");
            TypeList.Add("DA", "Dale");
            TypeList.Add("DR", "Drive");
            TypeList.Add("EX", "Expressway");
            TypeList.Add("FY", "Freeway");
            TypeList.Add("GA", "Garden");
            TypeList.Add("GR", "Grove");
            TypeList.Add("HT", "Heights");
            TypeList.Add("HY", "Highway");
            TypeList.Add("HL", "Hill");
            TypeList.Add("KN", "Knoll");
            TypeList.Add("LN", "Lane");
            TypeList.Add("MA", "Mall");
            TypeList.Add("OV", "Oval");
            TypeList.Add("PK", "Park");
            TypeList.Add("PY", "Parkway");
            TypeList.Add("PA", "Path");
            TypeList.Add("PI", "Pike");
            TypeList.Add("PL", "Place");
            TypeList.Add("PZ", "Plaza");
            TypeList.Add("PT", "Point");
            TypeList.Add("RD", "Road");
            TypeList.Add("RT", "Route");
            TypeList.Add("RO", "Row");
            TypeList.Add("RN", "Run");
            TypeList.Add("RR", "Rural Route");
            TypeList.Add("SQ", "Square");
            TypeList.Add("ST", "Street");
            TypeList.Add("TC", "Terrace");
            TypeList.Add("TY", "Thruway");
            TypeList.Add("TR", "Trail");
            TypeList.Add("TP", "Turnpike");
            TypeList.Add("VI", "Viaduct");
            TypeList.Add("VW", "View");
            TypeList.Add("WK", "Walk");
            TypeList.Add("WY", "Way");

            ObjModel.TypeList = TypeList;

            #endregion

            #region Gender

            Dictionary<string, string> GenderList = new Dictionary<string, string>();
            GenderList.Add("-1", "");
            GenderList.Add("Male", "Male");
            GenderList.Add("Female", "Female");
            ObjModel.GenderList = GenderList;

            #endregion

            #region Permissible Purpose

            //Dictionary<string, string> PermissiblePurpose = new Dictionary<string, string>();
            //PermissiblePurpose.Add("Employment", "Employment");
            BALCompanyType serboj = new BALCompanyType();
            List<tblPermissiblePurpose> PermissiblePurpose = serboj.GetPermissiblePurpose();

            ObjModel.PermissiblePurpose = PermissiblePurpose.Select(x => new SelectListItem { Text = x.PermissiblePurpose, Value = x.pkPermissibleId.ToString() }).ToList();

            ObjModel.PermissiblePurpose = ObjModel.PermissiblePurpose.Select(x => new SelectListItem { Text = x.Text, Value = x.Value.ToString(), Selected = (x.Value == ObjModel.PermissiblePurposeId.ToString()) }).ToList();

            ObjModel.PermissiblePurposeValue = PermissiblePurpose.Where(x => x.pkPermissibleId == (ObjModel.PermissiblePurposeId == 0 ? 1 : ObjModel.PermissiblePurposeId)).Select(x => x.PermissiblePurpose).FirstOrDefault().ToString();

            #endregion

            #region chkFCRA

            ObjModel.chkFCRA = true;

            #endregion

            #region Delete Previous Files

            //  string FilePath = Server.MapPath(@"~\Resources\Upload\NewReportXML");
            // CommonHelper.DeleteFile(FilePath);

            #endregion

            #region From and To Email Id
            try
            {
                ObjModel.lblFrom = User.Identity.Name;
                ObjModel.lblTo = ConfigurationManager.AppSettings["EMEditMailTo"].ToString();
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in BindNewReportDropdowns(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            #endregion

            #region---Added drop-down for Cor.Manager and Branch Manager.//INT343
            MembershipUser Member = Membership.GetUser();//User.Identity.Name
            if (Member != null)
            {
                string[] Role = Roles.GetRolesForUser(Member.Email);
                if (Role.Contains("CorporateManager"))
                {
                    #region GetCompany Locationd

                    List<SelectListItem> ObjListLocation = new List<SelectListItem>();

                    ObjListLocation = new BALLocation().GetLocationsByCompanyId(pkCompanyId).Select(p => new SelectListItem
                    {
                        Value = Convert.ToString(p.pkLocationId),
                        Text = p.City,
                        Selected = false


                    }).ToList();
                    ObjModel.CompanyLocations = ObjListLocation;

                    #endregion
                }
                else if (Role.Contains("BranchManager"))
                {
                    #region GetCompany Locationd

                    List<SelectListItem> ObjListLocation = new List<SelectListItem>();

                    ObjListLocation = new BALLocation().GetLocationsByCompanyIdForBranchManager(Member.Email).Select(p => new SelectListItem
                    {
                        Value = Convert.ToString(p.pkLocationId),
                        Text = p.City,
                        Selected = false


                    }).ToList();
                    ObjModel.CompanyLocations = ObjListLocation;
                    #endregion
                }
            }
            #endregion

            return ObjModel;
        }

        #endregion

        #region New Report Log

        #region New Report Page Load

        public PartialViewResult NewReportLog(int OrderId)
        {
            BALGeneral ObjBALGeneral = new BALGeneral();
            NewReportModel ObjModel = new NewReportModel();
            int UserLocationId = 0;
            int pkCompanyId = 0;
            if (TempData["IsErrorLogRan"] != null)
            {
                ObjModel.IsErrorLog = TempData["IsErrorLogRan"].ToString();
            }
            BALOrders ObjBALOrders = new BALOrders();
            BALCompany ObjBALCompany = new BALCompany();
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            MembershipUser Member = Membership.GetUser(User.Identity.Name);
            CustomOrderCollection ObjCustomOrderCollection = new CustomOrderCollection();
            try
            {
                ObjCustomOrderCollection = ObjBALOrders.GetOrderDetailsByOrderId(OrderId);
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in NewReportLog(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            try
            {
                if (ObjCustomOrderCollection.ListOrderDetail.Count > 0)
                {
                    # region Bind OrderCompanyId and LocationId

                   // int hdnOdrCompanyId = Int32.Parse(ObjCustomOrderCollection.ListOrderDetail.FirstOrDefault().fkCompanyId.ToString());
                   // int hdnOdrLocationId = Int32.Parse(ObjCustomOrderCollection.ObjOrder.fkLocationId.ToString());
                    int hdnOdrCompanyUserId = Int32.Parse(ObjCustomOrderCollection.ObjOrder.fkCompanyUserId.ToString());

                    # endregion
                    #region Get Company Id

                    //if (Member != null)
                    //{
                    var dbUserInfo = ObjBALCompanyUsers.SelectCompanyUserByCompanyUserId(hdnOdrCompanyUserId);
                    var dbCollection = ObjBALCompanyUsers.GetUserCompanyInfo(dbUserInfo.fkUserId);
                    if (dbCollection.Count > 0)
                    {
                        int fkLocationId = Int32.Parse(dbCollection.First().pkLocationId.ToString());
                        UserLocationId = fkLocationId;
                        ObjModel.fkLocationId = fkLocationId;
                        ObjModel.pkCompanyUserId = Int32.Parse(dbCollection.First().pkCompanyUserId.ToString());
                        ObjModel.OrderCompanyName = dbCollection.First().CompanyName;

                        ObjModel.CompanyAccountNo = dbCollection.First().CompanyAccountNumber;
                        ObjModel.PermissiblePurposeId = dbCollection.First().PermissiblePurpose;
                        pkCompanyId = dbCollection.First().pkCompanyId;
                        ObjModel.pkCompanyId = pkCompanyId;
                        ObjModel.IsWCVAcknowledged = dbCollection.First().IsWCVAcknowledged;
                        ObjModel.IsLiveRunnerEnabled = dbCollection.First().IsLiveRunnerEnabled;

                    }
                    //}


                    #endregion
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in NewReportLog(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

                if (Member != null)
                {
                    var dbCollection = ObjBALCompanyUsers.GetUserCompanyInfo(new Guid(Member.ProviderUserKey.ToString()));
                    if (dbCollection.Count > 0)
                    {
                        int fkLocationId = Int32.Parse(dbCollection.First().pkLocationId.ToString());
                        UserLocationId = fkLocationId;
                        ObjModel.fkLocationId = fkLocationId;
                        ObjModel.pkCompanyUserId = Int32.Parse(dbCollection.First().pkCompanyUserId.ToString());
                        ObjModel.OrderCompanyName = dbCollection.First().CompanyName;
                        ObjModel.CompanyAccountNo = dbCollection.First().CompanyAccountNumber;
                        ObjModel.PermissiblePurposeId = dbCollection.First().PermissiblePurpose;
                        pkCompanyId = dbCollection.First().pkCompanyId;
                        ObjModel.pkCompanyId = pkCompanyId;
                        ObjModel.IsLiveRunnerEnabled = dbCollection.First().IsLiveRunnerEnabled;
                    }
                }
            }

            ObjModel = BindNewReportDropdowns(ObjBALGeneral, ObjModel, pkCompanyId, ObjBALCompany);


            #region Bind Reports
            try
            {
                if (ObjCustomOrderCollection.ListOrderReports.Count > 0)
                {
                    List<string> ObjCollProducts = ObjCustomOrderCollection.ListOrderReports.ToList();
                    ViewData["ObjCollProducts"] = ObjCollProducts;
                    ViewData["vw_Panels"] = ObjCollProducts;
                    string Codes = string.Empty;
                    string[] lstProductCodes = ObjCustomOrderCollection.ListOrderReports.ToArray();
                    Codes = string.Join("", lstProductCodes);
                    ObjModel.hfReports = Codes.ToString();


                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in NewReportLog(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            #endregion Bind Reports

            #region Bind Order Searched Data
            try
            {
                if (ObjCustomOrderCollection.ObjOrderSearchedData != null)
                {
                    ObjModel.LastName = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedLastName);
                    ObjModel.FirstName = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedFirstName);
                    ObjModel.MI = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedMiddleInitial);
                    ObjModel.SuffixValue = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedSuffix);
                    ObjModel.Social = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedSsn);
                    ObjModel.DOB = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedDob);
                    ObjModel.Phone = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedPhoneNumber);
                    ObjModel.Address = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedStreetAddress);
                    ObjModel.DirectionValue = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedDirection);
                    ObjModel.Street = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedStreetName);
                    ObjModel.TypeValue = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedStreetType);
                    ObjModel.Apt = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedApt);
                    ObjModel.City = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedCity);
                    ObjModel.ApplicantEmail = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedApplicantEmail);
                    ObjModel.pkStateId = Convert.ToInt32(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedStateId);
                    string strstate = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedStateId);
                    ObjModel.State = strstate;
                    if (ObjCustomOrderCollection.ObjOrderSearchedData.SearchedZipCode.Length > 5)
                    {
                        ObjCustomOrderCollection.ObjOrderSearchedData.SearchedZipCode = ObjCustomOrderCollection.ObjOrderSearchedData.SearchedZipCode.Substring(0, 5);
                    }

                    ObjModel.Zip = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedZipCode);
                    if (ObjCustomOrderCollection.ObjOrderSearchedData.SearchedCountyId != null)
                    {
                        ObjModel.AllowJurisdiction = true;
                        ObjModel.Jurisdiction = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedCountyId);
                    }
                    else
                    {
                        ObjModel.AllowJurisdiction = false;
                    }

                    #region Bind Business Info
                    ObjModel.BusinessName = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedBusinessName);
                    ObjModel.BusinessCity = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedBusinessCity);

                    ObjModel.BusinessState = Convert.ToInt32(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedBusinessStateId);

                    #endregion Bind Business Info

                    #region Bind Automative Info

                    string str = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedDLStateId);
                    // changes for INT 321 IN WHICH STATE ID IS NOT BINDING CORRECTLY 
                    // THIS IS FOR NEW REPORT LOG PAGE AND when order type is 1 Date on 29 march 2016 
                    ObjModel.DLState = str + "_" + ObjCustomOrderCollection.ListOrderDetail.FirstOrDefault().IsConsentRequired;
                    ObjModel.DriverLicense = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedDriverLicense);
                    ObjModel.VehicleVin = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedVehicleVin);
                    ObjModel.Sex = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedSex);

                    #endregion Bind Automative Info

                    #region Get Jurisdiction For Report Log

                    List<clsJurisdiction> ObjJudCollection = ObjBALGeneral.GetJurisdictionsForNewReportLog();
                    ObjJudCollection.Insert(0, new clsJurisdiction { pkJurisdictionId = "-1", JurisdictionName = "" });
                    ObjModel.JurisdictionList = ObjJudCollection;

                    #endregion
                }
                if (ObjModel.hfReports != null)
                {
                    if (ObjModel.hfReports.Contains("SCR"))
                    {



                        string IsLiveRunner = ObjModel.pkStateId.ToString().Split('_').ElementAt(0);


                        ObjModel.IsLiveRunner = IsLiveRunner;
                    }
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in NewReportLog(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            #endregion Bind Order Searched Data

            #region Bind Order County Info
            try
            {
                if (ObjCustomOrderCollection.ListOrderSearchedLiveRunnerInfo.Count > 0)
                {
                    ObjModel.ObjOrderSearchedLiveRunnerInfo = ObjCustomOrderCollection.ListOrderSearchedLiveRunnerInfo;
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in NewReportLog(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            #endregion Bind Order County Info


            #region Bind Order Employment Info
            try
            {
                if (ObjCustomOrderCollection.ListOrderSearchedEmploymentInfo.Count > 0)
                {
                    ObjModel.ObjSearchedEmploymentInfo = ObjCustomOrderCollection.ListOrderSearchedEmploymentInfo;
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in NewReportLog(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            #endregion Bind Order Employment Info

            #region Bind Order Education Info
            try
            {
                if (ObjCustomOrderCollection.ListOrderSearchedEducationInfo.Count > 0)
                {
                    ObjModel.ObjSearchedEducationInfo = ObjCustomOrderCollection.ListOrderSearchedEducationInfo;
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in NewReportLog(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            #endregion Bind Order Education Info

            #region Bind Order Personal Info
            try
            {
                if (ObjCustomOrderCollection.ListOrderSearchedPersonalInfo.Count > 0)
                {
                    ObjModel.ObjOrderSearchedPersonalInfo = ObjCustomOrderCollection.ListOrderSearchedPersonalInfo;
                }

            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in NewReportLog(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }

            #endregion Bind Order Personal Info

            #region Bind Order License Info
            try
            {
                if (ObjCustomOrderCollection.ListOrderSearchedLicenseInfo.Count > 0)
                {
                    ObjModel.ObjOrderSearchedLicenseInfo = ObjCustomOrderCollection.ListOrderSearchedLicenseInfo;
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in NewReportLog(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }

            #endregion Bind Order License Info

            #region Bind Order Tracking Info
            try
            {
                if (ObjCustomOrderCollection.ObjReferenceCode != null)
                {
                    ObjModel.TrackingNotes = CheckNull(ObjCustomOrderCollection.ObjReferenceCode.pkReferenceCodeId);
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in NewReportLog(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }

            #endregion Bind Order Tracking Info

            #region Top Blocks
            try
            {
                BindTopBlocks(ObjCustomOrderCollection.ObjOrder, ObjCustomOrderCollection.ListOrderDetail, ObjModel);
            }
            catch (Exception ex)
            {
                ObjModel.ErrorMessage += "Top Block: " + ex.Message.ToString();
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in NewReportLog(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }

            #endregion

            #region Bind Error Log

            try
            {
                if (ObjCustomOrderCollection.ListOrderDetail.Count > 0)
                {
                    if (ObjCustomOrderCollection.ListOrderDetail != null && ObjCustomOrderCollection.ListOrderResponseData != null)
                    {
                        BindErrorLog(ObjCustomOrderCollection.ObjOrder, ObjCustomOrderCollection.ListOrderDetail, ObjCustomOrderCollection.ListOrderResponseData, ObjModel);
                    }
                }
            }
            catch (Exception ex)
            {
                ObjModel.ErrorMessage += "ErrorLog: " + ex.Message.ToString();
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in NewReportLog(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }


            #endregion
            if (pkCompanyId != 0)
            {
                var Itemlists = ObjBALCompany.IsEscreenEnableForCompany(pkCompanyId);
                ObjModel.IsEscreenEnable = Itemlists.Item1;
            }

            ObjModel.UserName = "dummy";
            ObjModel.Password = "dummy";

            return PartialView("NewReportLog", ObjModel);
        }

        public string CheckNull(object Obj)
        {
            string str = string.Empty;
            if (Obj != null)
            {
                str = Convert.ToString(Obj);
            }
            return str;
        }

        #endregion

        #region It will Load Top Order Info and Reports with checkbox

        public void BindTopBlocks(tblOrder ObjtblOrder, List<tblOrderDetail> ObjtblOrderDetail, NewReportModel ObjModel)
        {
            List<tblCompanyUser> CuColl = new List<tblCompanyUser>();
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                CuColl = dx.tblCompanyUsers.ToList<tblCompanyUser>();
                StringBuilder sb = new StringBuilder();
                sb.Append("<table cellspacing='2' cellpadding='2'  border='0'  class='tblRightPanel'>");
                sb.Append("<tr><td colspan='2'><b>User Information</b></td></tr></table>");
                sb.Append("<table  style='width:100%;height:160px;' cellspacing='2' cellpadding='2'  border='0' class='ReportBorderRightViewOpen'>");
                sb.Append("<tr><td style='width:50%;'>Order #: </td><td>" + ObjtblOrder.OrderNo + "</td></tr>");
                sb.Append("<tr><td  style='width:50%;'>Order Date: </td><td>" + ObjtblOrder.OrderDt + "</td></tr>");
                if (ObjtblOrder.fkCompanyUserId != null)
                {
                    try
                    {
                        var vName = CuColl.Where(d => d.pkCompanyUserId == ObjtblOrder.fkCompanyUserId).Select(d => new { Name = d.LastName + " " + d.FirstName }).FirstOrDefault();
                        if (vName != null)
                        {
                            sb.Append("<tr><td>Name: </td><td>" + vName.Name + "</td></tr>");
                        }
                        Guid fkUserid = CuColl.Where(d => d.pkCompanyUserId == ObjtblOrder.fkCompanyUserId).Select(d => d.fkUserId).FirstOrDefault();
                        string UserName = dx.aspnet_Users.Where(d => d.UserId == fkUserid).Select(d => d.UserName).FirstOrDefault();
                        if (UserName != null)
                        {
                            sb.Append("<tr><td>Username: </td><td>" + UserName + "</td></tr>");
                        }

                    }
                    catch (Exception ex)
                    {
                        ObjModel.ErrorMessage += "Name: " + ex.Message.ToString();
                        //Added log for exception handling.
                        VendorServices.GeneralService.WriteLog("Some Error Occured in BindTopBlocks(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    }

                }
                try
                {
                    int? fkLocationId = ObjtblOrderDetail.Where(d => d.fkLocationId != null).Take(1).Select(d => d.fkLocationId).FirstOrDefault();
                    if (fkLocationId != null)
                    {
                        var vLocationDetail = dx.tblLocations.Where(d => d.pkLocationId == fkLocationId).Select(d => new { d.fkCompanyId, d.City, d.fkStateID }).First();
                        string CompanyName = dx.tblCompanies.Where(d => d.pkCompanyId == vLocationDetail.fkCompanyId).Select(d => d.CompanyName).FirstOrDefault();
                        sb.Append("<tr><td>Company Name: </td><td>" + CompanyName + "</td></tr>");
                        var vStateCity = dx.tblStates.Where(d => d.pkStateId == vLocationDetail.fkStateID).Select(d => new { d.StateName }).FirstOrDefault();
                        sb.Append("<tr><td>Location: </td><td>" + vLocationDetail.City + ", " + vStateCity.StateName + "</td></tr>");
                    }
                }
                catch (Exception ex)
                {
                    ObjModel.ErrorMessage += "Company Name: " + ex.Message.ToString();
                    //Added log for exception handling.
                    VendorServices.GeneralService.WriteLog("Some Error Occured in BindTopBlocks(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                }

                sb.Append("</table>");
                ObjModel.TopLeft = sb.ToString();

                //Right Portion

                StringBuilder sbr = new StringBuilder();
                sbr.Append("<table cellspacing='2' cellpadding='2' border='0' style='font-weight:bold;' class='tblRightPanel'>");
                sbr.Append("<tr><td align='left' style='width:170px;  float: left;'>Reports</td>");
                sbr.Append("<td align='left'  style='width:70px;  float: left;'>Status</td>");
                sbr.Append("<td  style='width:60px;  float: left;'>Image</td>");
                sbr.Append("<td  style='width:40px;  float: left;'><img src='../Content/themes/base/images/PendingReport.png' style='height: 16px;' tile='Check Vendor Response' /></td>");
                sbr.Append("<td style='width:185px;  float: left;'>Action</td></table>");
                sbr.Append("<table style='width:100%;height:160px;' cellspacing='1' cellpadding='1'  border='0' class='ReportBorderRightViewOpen'><tr><td style='vertical-align:top;'><table style='vertical-align:top;'>");

                //dx.Log = Console.Out;

                try
                {
                    //INT-328 verification Product sequence changes.
                    var GetReports = ObjtblOrderDetail.Join(dx.tblProductPerApplications,
                            d => d.fkProductApplicationId,
                            p => p.pkProductApplicationId,
                            (d, p) => new { d, p }).Join(dx.tblProducts,
                              g => g.p.fkProductId,
                              a => a.pkProductId,
                              (g, a) => new { g.d.ReportStatus, a.ProductName, g.d.pkOrderDetailId, a.ProductCode, g.p.fkReportCategoryId }).OrderByDescending(a => a.ProductCode);

                    int count = 0;
                    string Id = string.Empty;
                    string SId = string.Empty;
                    string VID = string.Empty;
                    foreach (var v1 in GetReports)
                    {
                        count++;
                        int Category_Id = v1.fkReportCategoryId;
                        string V_Category_Name = CategoryName(Category_Id);
                        Id = "Complete_" + count;
                        SId = "Status_" + count;
                        VID = "VendorIcon_" + count;

                        if (V_Category_Name == "eScreen")
                        {
                            sbr.Append("<tr style='vertical-align:top;'><td style='width:170px;'><input onclick=\"enableDisableValidatorsLog(this.id,0,0,this.checked,'" + v1.pkOrderDetailId + "')\" id='" + v1.ProductCode + "' type='checkbox' class='escreen' disabled='disabled'/>" + v1.ProductName + " </td>");
                        }
                        else
                        {
                            sbr.Append("<tr style='vertical-align:top;'><td style='width:170px;'><input onclick=\"enableDisableValidatorsLog(this.id,0,0,this.checked,'" + v1.pkOrderDetailId + "')\" id='" + v1.ProductCode + "' type='checkbox'/>" + v1.ProductName + " </td>");
                        }
                        sbr.Append("<td  style='width:70px;'><label id=" + SId + ">" + GetStatus(v1.ReportStatus) + "</label></td>");
                        if (v1.ReportStatus == 3)
                        {

                            if (V_Category_Name == "eScreen")
                            {
                                sbr.Append("<td style='width:60px;'> <img onclick=\"DisplayUpload('" + v1.pkOrderDetailId + "');\" Id='upload1'  style='cursor:pointer' src='../Content/themes/base/images/file_upload.png' title='Upload Image/HTML File' disabled='disabled' /></td>");
                                sbr.Append("<td style='width:40px;'><img src='../Content/themes/base/images/Incomplete.gif' style='height: 16px;' title='Not Applicable'  disabled='disabled'/></td>");
                                sbr.Append("<td style='width:200px;'><input  id=" + Id + " type='button' onclick=\"CompleteReport(this.id,'" + ObjtblOrder.pkOrderId + "','" + v1.pkOrderDetailId + "','" + v1.ProductCode + "','" + v1.ProductName + "')\" value='Complete' disabled='disabled' /> <input  id='Pending_" + Id + "' type='button' onclick=\"ChangeReportToPending(this.id,'" + ObjtblOrder.pkOrderId + "','" + v1.pkOrderDetailId + "','" + v1.ProductCode + "','" + v1.ProductName + "')\" value='Pending'  /></td></tr>");
                            }
                            else
                            {
                                sbr.Append("<td style='width:60px;'> <img onclick=\"DisplayUpload('" + v1.pkOrderDetailId + "');\" Id='upload1' style='cursor:pointer' height='20' width='20' src='../Content/themes/base/images/file_upload.png' title='Upload Image/HTML File' /></td>");
                                sbr.Append("<td style='width:40px;'><img src='../Content/themes/base/images/Incomplete.gif' style='height: 16px;' title='Not Applicable' /></td>");
                                sbr.Append("<td style='auto;'><input  id=" + Id + " type='button' onclick=\"CompleteReport(this.id,'" + ObjtblOrder.pkOrderId + "','" + v1.pkOrderDetailId + "','" + v1.ProductCode + "','" + v1.ProductName + "')\" value='Complete' /> <input  id='Pending_" + Id + "' type='button' onclick=\"ChangeReportToPending(this.id,'" + ObjtblOrder.pkOrderId + "','" + v1.pkOrderDetailId + "','" + v1.ProductCode + "','" + v1.ProductName + "')\" value='Pending'  /><input  id='forcePending_" + count + "' type='button' onclick=\"ChangeReportToPending(this.id,'" + ObjtblOrder.pkOrderId + "','" + v1.pkOrderDetailId + "','" + v1.ProductCode + "','" + v1.ProductName + "')\" value='Force Pending'  /></td></tr>");//INT-120
                            }

                        }
                        else
                        {
                            if (V_Category_Name == "eScreen")
                            {
                                sbr.Append("<td style='width:60px;'> <img Id='upload'  onclick=\"DisplayUpload('" + v1.pkOrderDetailId + "');\"  style='cursor:pointer' src='../Content/themes/base/images/file_upload.png' title='Upload Image/HTML File' />&nbsp;&nbsp;<img Id='uploadattach'  onclick=\"DisplayUploadAttach('" + v1.pkOrderDetailId + "');\" style='cursor:pointer'  src='../Content/themes/base/images/file_upload.png' title='Upload Attachment File' disabled='disabled' /></td>");
                                sbr.Append("<td style='width:40px;'><img id=" + VID + " onclick=\"CheckVendorResponse(this.id,'" + v1.pkOrderDetailId + "','" + ObjtblOrder.pkOrderId + "','" + v1.ProductCode + "');\" src='../Content/themes/base/images/PendingReport.png' style='height: 16px;' title='Check Vendor Response' disabled='disabled' /></td>");
                                sbr.Append("<td style='width:200px;'><input id=" + Id + " type='button' onclick=\"CompleteReport(this.id,'" + ObjtblOrder.pkOrderId + "','" + v1.pkOrderDetailId + "','" + v1.ProductCode + "','" + v1.ProductName + "')\"  value='Complete' disabled='disabled'/><input id='Incomplete_" + count + "' type='button' onclick=\"InCompleteReport(this.id,'" + ObjtblOrder.pkOrderId + "','" + v1.pkOrderDetailId + "')\" value='Incomplete' disabled='disabled'/></td></tr>");
                            }
                            else
                            {
                                sbr.Append("<td style='width:60px;'> <img Id='upload'  onclick=\"DisplayUpload('" + v1.pkOrderDetailId + "');\" style='cursor:pointer' height='20' width='20' src='../Content/themes/base/images/file_upload.png' title='Upload Image/HTML File' />&nbsp;&nbsp;<img Id='uploadattach'  onclick=\"DisplayUploadAttach('" + v1.pkOrderDetailId + "');\" style='cursor:pointer' src='../Content/themes/base/images/file_upload.png' title='Upload Attachment File' /></td>");
                                sbr.Append("<td style='width:40px;'><img id=" + VID + " onclick=\"CheckVendorResponse(this.id,'" + v1.pkOrderDetailId + "','" + ObjtblOrder.pkOrderId + "','" + v1.ProductCode + "');\" src='../Content/themes/base/images/PendingReport.png' style='height: 16px;' title='Check Vendor Response' /></td>");
                                sbr.Append("<td style='auto;'><input id=" + Id + " type='button' onclick=\"CompleteReport(this.id,'" + ObjtblOrder.pkOrderId + "','" + v1.pkOrderDetailId + "','" + v1.ProductCode + "','" + v1.ProductName + "')\"  value='Complete'/><input id='Incomplete_" + count + "' type='button' onclick=\"InCompleteReport(this.id,'" + ObjtblOrder.pkOrderId + "','" + v1.pkOrderDetailId + "')\" value='Incomplete'/><input  id='forcePending_" + count + "' type='button' onclick=\"ChangeReportToPending(this.id,'" + ObjtblOrder.pkOrderId + "','" + v1.pkOrderDetailId + "','" + v1.ProductCode + "','" + v1.ProductName + "')\" value='Force Pending'  /></td></tr>");

                            }


                        }
                    }

                }
                catch (Exception ex)
                {
                    //Added log for exception handling.
                    VendorServices.GeneralService.WriteLog("Some Error Occured in BindTopBlocks(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                }

                sbr.Append("</table></td></tr></table>");
                ObjModel.TopRight = sbr.ToString();
            }
        }



        public void BindTopBlocksApi(tblOrder ObjtblOrder, List<tblOrderDetail> ObjtblOrderDetail, NewReportModel ObjModel)
        {

            List<tblCompanyUser> CuColl = new List<tblCompanyUser>();
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                CuColl = dx.tblCompanyUsers.ToList<tblCompanyUser>();
                StringBuilder sb = new StringBuilder();
                sb.Append("<table cellspacing='2' cellpadding='2'  border='0'  class='tblRightPanel'>");
                sb.Append("<tr><td colspan='2'><b>User Information</b></td></tr></table>");
                sb.Append("<table  style='width:100%;height:160px;' cellspacing='2' cellpadding='2'  border='0' class='ReportBorderRightViewOpen'>");
                sb.Append("<tr><td style='width:50%;'>Order #: </td><td>" + ObjtblOrder.OrderNo + "</td></tr>");
                sb.Append("<tr><td  style='width:50%;'>Order Date: </td><td>" + ObjtblOrder.OrderDt + "</td></tr>");
                if (ObjtblOrder.fkCompanyUserId != null)
                {
                    try
                    {
                        var vName = CuColl.Where(d => d.pkCompanyUserId == ObjtblOrder.fkCompanyUserId).Select(d => new { Name = d.LastName + " " + d.FirstName }).FirstOrDefault();
                        if (vName != null)
                        {
                            sb.Append("<tr><td>Name: </td><td>" + vName.Name + "</td></tr>");
                        }
                        Guid fkUserid = CuColl.Where(d => d.pkCompanyUserId == ObjtblOrder.fkCompanyUserId).Select(d => d.fkUserId).FirstOrDefault();
                        string UserName = dx.aspnet_Users.Where(d => d.UserId == fkUserid).Select(d => d.UserName).FirstOrDefault();
                        if (UserName != null)
                        {
                            sb.Append("<tr><td>Username: </td><td>" + UserName + "</td></tr>");
                        }

                    }
                    catch (Exception ex)
                    {
                        ObjModel.ErrorMessage += "Name: " + ex.Message.ToString();
                        //Added log for exception handling.
                        VendorServices.GeneralService.WriteLog("Some Error Occured in BindTopBlocksApi(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    }

                }
                try
                {
                    int? fkLocationId = ObjtblOrderDetail.Where(d => d.fkLocationId != null).Take(1).Select(d => d.fkLocationId).FirstOrDefault();
                    if (fkLocationId != null)
                    {
                        var vLocationDetail = dx.tblLocations.Where(d => d.pkLocationId == fkLocationId).Select(d => new { d.fkCompanyId, d.City, d.fkStateID }).First();
                        string CompanyName = dx.tblCompanies.Where(d => d.pkCompanyId == vLocationDetail.fkCompanyId).Select(d => d.CompanyName).FirstOrDefault();
                        sb.Append("<tr><td>Company Name: </td><td>" + CompanyName + "</td></tr>");
                        var vStateCity = dx.tblStates.Where(d => d.pkStateId == vLocationDetail.fkStateID).Select(d => new { d.StateName }).FirstOrDefault();
                        sb.Append("<tr><td>Location: </td><td>" + vLocationDetail.City + ", " + vStateCity.StateName + "</td></tr>");
                    }
                }
                catch (Exception ex)
                {
                    ObjModel.ErrorMessage += "Company Name: " + ex.Message.ToString();
                    //Added log for exception handling.
                    VendorServices.GeneralService.WriteLog("Some Error Occured in BindTopBlocksApi(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                }

                sb.Append("</table>");
                ObjModel.TopLeft = sb.ToString();

                //Right Portion

                StringBuilder sbr = new StringBuilder();
                sbr.Append("<table cellspacing='2' cellpadding='2' border='0' style='font-weight:bold;' class='tblRightPanel'>");
                sbr.Append("<tr><td align='left' style='width:170px;  float: left;'>Reports</td>");
                sbr.Append("<td align='left'  style='width:70px;  float: left;'></td>");
                sbr.Append("<td  style='width:60px;  float: left;'></td>");
                sbr.Append("<td  style='width:40px;  float: left;'></td>");
                sbr.Append("<td style='width:185px;  float: left;'></td></table>");
                sbr.Append("<table style='width:100%;height:160px;' cellspacing='1' cellpadding='1'  border='0' class='ReportBorderRightViewOpen'><tr><td style='vertical-align:top;'><table style='vertical-align:top;'>");

                try
                {

                    var GetReports = ObjtblOrderDetail.Join(dx.tblProductPerApplications,
                      d => d.fkProductApplicationId,
                      p => p.pkProductApplicationId,
                      (d, p) => new { d, p }).Join(dx.tblProducts,
                        g => g.p.fkProductId,
                        a => a.pkProductId,
                        (g, a) => new { g.d.ReportStatus, a.ProductName, g.d.pkOrderDetailId, a.ProductCode });




                    int count = 0;
                    string Id = string.Empty;
                    string SId = string.Empty;
                    string VID = string.Empty;
                    foreach (var v1 in GetReports)
                    {
                        count++;
                        Id = "Complete_" + count;
                        SId = "Status_" + count;
                        VID = "VendorIcon_" + count;
                        sbr.Append("<tr style='vertical-align:top;'><td style='width:170px;'><input onclick=\"enableDisableValidatorsLog(this.id,0,0,this.checked,'" + v1.pkOrderDetailId + "')\" id='" + v1.ProductCode + "' type='checkbox' class='disable' readonly='readonly'  />" + v1.ProductName + " </td>");
                        sbr.Append("<td  style='width:70px;'></td>");

                        //if (v1.ReportStatus == 2 || v1.ReportStatus == 4)
                        //{
                        //    sbr.Append("<td  style='width:50px;'>&nbsp;</td>");
                        //    sbr.Append("<td style='width:35px;'><img src='../Content/themes/base/images/Done.png' style='height: 16px;' title='Completed from Vendor Side' alt='Completed from Vendor Side' /></td>");
                        //    sbr.Append("<td style='width:85px;'><input type='button' value='Complete' Id='Complete' disabled='disabled'/></td></tr>");
                        //}else
                        if (v1.ReportStatus == 3)
                        {
                            sbr.Append("<td style='width:60px;'> </td>");
                            sbr.Append("<td style='width:40px;'></td>");
                            sbr.Append("<td style='width:200px;'></td></tr>");
                        }
                        else
                        {
                            sbr.Append("<td style='width:60px;'> </td>");
                            sbr.Append("<td style='width:40px;'></td>");
                            sbr.Append("<td style='width:200px;'></td></tr>");
                        }
                    }

                }
                catch (Exception ex)
                {
                    //Added log for exception handling.
                    VendorServices.GeneralService.WriteLog("Some Error Occured in BindTopBlocksApi(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                }

                sbr.Append("</table></td></tr></table>");
                ObjModel.TopRight = sbr.ToString();
            }
        }


        public string CategoryName(int ReportCateGoryId)
        {
            string CategoryName = string.Empty;
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                var ListItems = dx.tblReportCategories.Where(d => d.pkReportCategoryId == ReportCateGoryId).Select(p => p.CategoryName).FirstOrDefault();
                if (ListItems != null) { CategoryName = ListItems; }
            }

            return CategoryName;
        }



        public string GetStatus(int status)
        {
            string retval = "";
            switch (status)
            {
                case 1: { retval = "<span style=\"color:red;\"> Pending </span>"; break; }
                case 2: { retval = "<span style=\"color:green;\"> Completed </span>"; break; }
                case 3: { retval = "<span style=\"color:red;\"> Incompleted </span>"; break; }
                case 4: { retval = "<span style=\"color:green;\"> InHelp </span>"; break; }
                case 5: { retval = "<span style=\"color:green;\"> InReview </span>"; break; }
                case 6: { retval = "<span style=\"color:green;\"> Force Pending </span>"; break; }
                default:
                    break;
            }

            return retval;
        }


        #endregion

        #region Check Vendor Response/ Resend Pending Order

        public int ResendPendingOrderByOrderId(int pkOrderDetailId, int pkOrderId, string ProductCode)
        {
            BALPendingOrders ObjBALPendingOrders = new BALPendingOrders();            
            int ReportStatus = 1;
            try
            {
                using (EmergeDALDataContext dx = new EmergeDALDataContext())
                {
                    //ObjBALPendingOrders.WriteVendorLog();
                    VendorServices.GeneralService.WriteLog(Environment.NewLine + "\nService Method for Single Order Started at:" + DateTime.Now.ToString(), "WindowsService_ResendPendingAllOrders");
                    ObjBALPendingOrders.ResendPendingOrderByOrderId(pkOrderId, ProductCode);
                    VendorServices.GeneralService.WriteLog(Environment.NewLine + "\nService Method for Single Order Completed at:" + DateTime.Now.ToString(), "WindowsService_ResendPendingAllOrders");
                    ReportStatus = dx.tblOrderDetails.Where(d => d.pkOrderDetailId == pkOrderDetailId).Select(d => d.ReportStatus).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\nService Method for Single Order gave error at:" + DateTime.Now.ToString() + Environment.NewLine + ex.Message.ToString(), "WindowsService_ResendPendingAllOrders");
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in ResendPendingOrderByOrderId(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            return ReportStatus;
        }

        //Error on Vendor Response
        public string AddErrorToCollection(string sFilename)
        {
            string retStr = "";
            try
            {
                string str = Server.MapPath("~/Resources/Upload/PendingReportsLog/" + sFilename);
                if (System.IO.File.Exists(str))
                {
                    string stext = string.Empty;
                    //FileInfo fileInfo = new System.IO.FileInfo(str);
                    using (StreamReader sReader = new StreamReader(str))
                    {
                        stext = sReader.ReadToEnd();
                        sReader.Close();
                    }
                    if (stext != "")
                    {
                        string[] FetchContent = stext.Split(',');
                        if (FetchContent.Count() > 1)
                        {
                            //Here i am fetching error 
                            retStr = FetchContent[2];
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in AddErrorToCollection(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            return retStr;
        }

        public string SetFilePath(out string ShortFileName, BALPendingOrders ObjBALPendingOrders)
        {
            string NameCreation = string.Empty;
            NameCreation = DateTime.Now.ToString("yyMMMdd_HHmmss") + ".csv";
            string str = Server.MapPath("~/Resources/Upload/PendingReportsLog/" + NameCreation);
            string rcxstr = Server.MapPath("~/Resources/Upload/ReportsLog/RapidCourt/");
            string rcxrequeststr = Request.PhysicalApplicationPath + "Resources/Upload/ReportsLog/RapidCourt/"; ;
            //BALGeneral ObjBALGeneral = new BALGeneral(rcxstr);
            ObjBALPendingOrders.RCXFilePath = rcxstr;
            ObjBALPendingOrders.RCXRequestPath = rcxrequeststr;
            #region MyRegion

            if (!Directory.Exists(Server.MapPath(@"~\Resources\Upload\PendingReportsLog")))
            {
                Directory.CreateDirectory(Server.MapPath(@"~\Resources\Upload\PendingReportsLog"));
            }

            string savedPath = string.Empty;
            //DateTime dt = DateTime.Now;
            savedPath = str;
            if (!System.IO.File.Exists(savedPath))
            {
                using(System.IO.File.CreateText(savedPath))
                { }
                ObjBALPendingOrders.FilePath = savedPath;
            }

            #endregion

            #region Delete 1 Day Old Files

            DirectoryInfo dir = null;
            dir = new DirectoryInfo(Server.MapPath(@"~\Resources\Upload\PendingReportsLog"));
            FileSystemInfo[] info = dir.GetFiles();
            if (info.Length > 0)
            {
                foreach (FileInfo fi in info)
                {
                    if (fi.CreationTime < DateTime.Now.AddDays(-1))
                    {
                        string FilePath = Server.MapPath(@"~\Resources\Upload\PendingReportsLog\") + fi.Name;
                        if (System.IO.File.Exists(FilePath))
                        {
                            try
                            {
                                System.IO.File.Delete(FilePath);
                            }
                            catch (Exception ex)
                            {
                                //Added log for exception handling.
                                VendorServices.GeneralService.WriteLog("Some Error Occured in SetFilePath(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                                /*ListErrors.Add("File Used By Another Process : " + ex.Message.ToString());*/
                            }

                        }
                    }
                }
            }

            #endregion

            ShortFileName = NameCreation;
            return str;

        }


        #endregion

        #region Bottom Error Log

        public void BindErrorLog(tblOrder ObjtblOrder, List<tblOrderDetail> ObjtblOrderDetailColl, List<tblOrderResponseData> ObjtblOrderResponseDataColl, NewReportModel ObjModel)
        {
            List<tblProductPerApplication> pColl = new List<tblProductPerApplication>();
            BALOrders ObjBALOrders = new BALOrders();
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                pColl = dx.tblProductPerApplications.ToList<tblProductPerApplication>();
            }
            StringBuilder sb = new StringBuilder();
            ObjModel.ErrorLogHeading = "Error Log for Order No : " + ObjtblOrder.OrderNo.ToString();
            sb.Append("<table border=\"0\" width=\"100%\">");
            foreach (tblOrderDetail obj in ObjtblOrderDetailColl)
            {
                tblOrderResponseData ObjtblOrderResponseData = ObjBALOrders.GetResponseByOrderDetailId(obj.pkOrderDetailId);

                string productcode = "";
                productcode = pColl.Where(d => d.pkProductApplicationId == obj.fkProductApplicationId).Select(d => d.ProductCode).FirstOrDefault().ToString();
                string code = obj.pkOrderDetailId + "_" + productcode;
                sb.Append("<tr onclick=\"javascript:ErrorLogShowHide('" + obj.pkOrderDetailId + "');\">"
                 + " <td style=\"color: black; background-color: #cecece;\">"
                 + "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width='100%'>"
                 + "<tr><td><img style=\"height: 20px;\" src=\"../Content/themes/base/images/plus_symbolSmall.png\"></td>"
                 + "<td style='width:70%'>&nbsp;" + productcode + GetStatus(obj.ReportStatus) + "</td>");
                sb.Append("<td style='width:30%; padding-right:8px' align='right'>");
                if (ObjtblOrderResponseData != null && ObjtblOrderResponseData.ResponseData != string.Empty)
                {
                    sb.Append("<a title='Download Response' id='aDownload' href='../Corporate/DownloadFile/?ResponseOrderDetailId=" + code + "'>Download Response</a>");
                }
                else
                {
                    sb.Append("No Response found.");
                }
                sb.Append("</td></tr>");
                sb.Append("</table>"
                 + "</td>  </tr>");



                if ((productcode == "CCR1" || productcode == "SCR") && obj.ReportStatus == 1)
                {

                    /* commented by chetu.
                     
                     * string FirstResponse = ObjtblOrderResponseDataColl.Where(d => d.fkOrderDetailId == obj.pkOrderDetailId).Select(d => d.ResponseData).FirstOrDefault().ToString();
                    if (FirstResponse != "")
                    {
                        sb.Append("<tr id=" + obj.pkOrderDetailId + "><td colspan=\"2\" style=\"border:1px solid #ececec;\">" + FirstResponse + "</td></tr>");
                    }
                    else if (obj.ErrorLog == null || obj.ErrorLog == "")
                    {
                        sb.Append("<tr id=" + obj.pkOrderDetailId + "><td colspan=\"2\" style=\"border:1px solid #ececec;\">NA</td></tr>");
                    }
                    else
                    {
                        sb.Append("<tr id=" + obj.pkOrderDetailId + "><td colspan=\"2\" style=\"border:1px solid #ececec;\">" + obj.ErrorLog + "</td></tr>");
                    } */

                    //codded by chetu.

                    if (ObjtblOrderResponseDataColl.Count() > 0)
                    {
                        string FirstResponse = ObjtblOrderResponseDataColl.Where(d => d.fkOrderDetailId == obj.pkOrderDetailId).Select(d => d.ResponseData).FirstOrDefault().ToString();
                        if (FirstResponse != "")
                        {
                            sb.Append("<tr id=" + obj.pkOrderDetailId + "><td colspan=\"2\" style=\"border:1px solid #ececec;\">" + FirstResponse + "</td></tr>");
                        }
                        else if (obj.ErrorLog == null || obj.ErrorLog == "")
                        {
                            sb.Append("<tr id=" + obj.pkOrderDetailId + "><td colspan=\"2\" style=\"border:1px solid #ececec;\">NA</td></tr>");
                        }
                        else
                        {
                            sb.Append("<tr id=" + obj.pkOrderDetailId + "><td colspan=\"2\" style=\"border:1px solid #ececec;\">" + obj.ErrorLog + "</td></tr>");
                        }
                    }
                    else
                    {
                        ObjtblOrderResponseDataColl = null;

                    }
                }
                else if (obj.ErrorLog == null || obj.ErrorLog == "")
                {
                    sb.Append("<tr id=" + obj.pkOrderDetailId + "><td colspan=\"2\" style=\"border:1px solid #ececec;\">NA</td></tr>");
                }
                else
                {
                    sb.Append("<tr id=" + obj.pkOrderDetailId + "><td colspan=\"2\" style=\"border:1px solid #ececec;\">" + obj.ErrorLog.ToString() + "</td></tr>");
                }


            }
            sb.Append("</table>");
            ObjModel.ErrorLog = sb.ToString();
        }

        #endregion

        #region Download Order Response

        public FilePathResult DownloadFile(string ResponseOrderDetailId)
        {
            string pkOrderDetailId = ResponseOrderDetailId.Split('_').ElementAt(0).ToString();
            string Message = string.Empty;
            try
            {
                BALOrders ObjBALOrders = new BALOrders();

                tblOrderResponseData ObjtblOrderResponseData = ObjBALOrders.GetResponseByOrderDetailId(Int32.Parse(pkOrderDetailId));
                if (ObjtblOrderResponseData != null)
                {
                    if (!string.IsNullOrEmpty(ObjtblOrderResponseData.ResponseData))
                    {
                        //if (IsValidXML(ObjtblOrderResponseData.ResponseData) == true)                   // check if the response is valid xml
                        //{
                        string filename = "Response_" + ResponseOrderDetailId + ".txt";

                        string filepath = Request.PhysicalApplicationPath + @"Resources/Upload/ResponseOrderData/Response_" + ResponseOrderDetailId + ".txt";

                        DeleteOldFiles(filename);  // Delete Old Files

                        System.IO.File.WriteAllText(filepath, ObjtblOrderResponseData.ResponseData);      // Save text file with valid xml response


                        if (System.IO.File.Exists(filepath))
                        {
                            // DownloadWithExtension(filepath, "OrderResponseData");           // download/open/save file which is saved above


                            string fileext = System.IO.Path.GetExtension(filepath);
                            string CType = "application/" + fileext.Replace(".", "");
                            string fileName = "OrderResponseData" + fileext;
                            return File(filepath, CType, fileName);
                        }
                        else
                        {
                            Message = "File not found.";
                        }
                        //}
                        //else
                        //{
                        //    Message = "XML Response not valid.";
                        //}

                    }
                    else
                    {
                        Message = "No Response found.";
                    }

                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in DownloadFile(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            finally
            {
            }

            return null;
        }

        private bool IsValidXML(string xmldata)
        {
            XmlDocument ObjXmlDocument = new XmlDocument();
            try
            {
                ObjXmlDocument.LoadXml(xmldata);
                return true;
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in IsValidXML(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                return false;
            }
        }

        protected void DeleteOldFiles(string filename)
        {
            DirectoryInfo dir = null;
            if (!Directory.Exists(Server.MapPath(@"~\Resources\Upload\ResponseOrderData")))
            {
                Directory.CreateDirectory(Server.MapPath(@"~\Resources\Upload\ResponseOrderData"));
            }
            else
            {
                dir = new DirectoryInfo(Server.MapPath(@"~\Resources\Upload\ResponseOrderData"));
                FileSystemInfo[] info = dir.GetFiles();
                if (info.Length > 0)
                {
                    foreach (FileInfo fi in info)
                    {
                        if (fi.Name.Contains(filename) || fi.CreationTime < DateTime.Now.AddDays(-1))
                        {
                            string FilePath = Server.MapPath(@"~\Resources\Upload\ResponseOrderData\") + fi.Name;
                            if (System.IO.File.Exists(FilePath))
                            {
                                try
                                {
                                    System.IO.File.Delete(FilePath);
                                }
                                catch (Exception ex)
                                {
                                    //Added log for exception handling.
                                    VendorServices.GeneralService.WriteLog("Some Error Occured in DeleteOldFiles(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                                    /*ListErrors.Add("File Used By Another Process : " + ex.Message.ToString());*/
                                }

                            }
                        }
                    }
                }
            }
        }


        #endregion

        #region Update Report Status
        public ActionResult UpdateReportStatus(string OrderId, string OrderDetailId, string ReportStatus)
        {
            int status = 0;
            try
            {
                BALOrders ObjBALOrders = new BALOrders();
                int orderid = Int32.Parse(OrderId);
                int orderdetailid = Int32.Parse(OrderDetailId);
                byte reportstatus = Convert.ToByte(ReportStatus);
                status = ObjBALOrders.UpdateReportStatus_Ret(orderid, orderdetailid, reportstatus);

            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateReportStatus(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                status = -1;
            }

            return Json(status);
        }

        #endregion

        #region Update Report Status and Send Email

        public ActionResult UpdateReportStatusAndSendMail(string OrderId, string OrderDetailId, string ReportStatus, string ProductCode, string ProductName, string FirstName, string LastName)
        {
            int status = 0;
            try
            {
                BALOrders ObjBALOrders = new BALOrders();
                BALGeneral ObjBALGeneral = new BALGeneral();
                int orderid = Int32.Parse(OrderId);
                int orderdetailid = Int32.Parse(OrderDetailId);
                byte reportstatus = Convert.ToByte(ReportStatus);
                status = ObjBALOrders.UpdateReportStatus_Ret(orderid, orderdetailid, reportstatus);
                if ((status == 1) && (ReportStatus == "2" || ReportStatus == "1"))
                {
                    #region Send Email to user when Company Status Complete
                    var GetUserNameFromOrderId = ObjBALGeneral.GetUserNameFromOrderId(orderid);

                    string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + BALGeneral.GetGeneralSettings().WebSiteLogo;


                    int pkTemplateId = 5;
                    var emailContent = ObjBALGeneral.GetEmailTemplate(new Guid("65d1e085-eb17-4f29-96d0-247b0c1ee852"), pkTemplateId);//"Complete Report"
                    string Subject = string.Empty;
                    string emailText = emailContent.TemplateContent;
                    BALTemplateBookmarks objTemplateBookmarks = new BALTemplateBookmarks();
                    #region For New Style SystemTemplate
                    string Bookmarkfile = string.Empty;
                    string bookmarkfilepath = string.Empty;

                    Bookmarkfile = objTemplateBookmarks.getBookMarkFile(bookmarkfilepath);
                    Bookmarkfile = Bookmarkfile.Replace("%EmailHeader%", emailContent.TemplateSubject);
                    Bookmarkfile = Bookmarkfile.Replace("%EmailBody%", emailContent.TemplateContent);
                    emailText = Bookmarkfile;
                    #endregion
                    //Proc_Get_PendingReportsResult ObjReport = new Proc_Get_PendingReportsResult();
                    #region New way Bookmark

                    //CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
                    //TextInfo textInfo = cultureInfo.TextInfo;
                    string firstname = FirstName != null ? FirstName : string.Empty;
                    string lastname = LastName != null ? LastName : string.Empty;
                    string emailAddress = GetUserNameFromOrderId.ElementAt(0).UserName;
                    Bookmarks objBookmark = new Bookmarks();
                    string MessageBody = string.Empty;
                    objBookmark.EmailContent = emailText;
                    objBookmark.EmergeLogo = "<img src='" + WebsiteLogo + "' />";
                    objBookmark.ApplicantName = "Emerge";
                    // objBookmark.ViewReportLink = Linkis;
                    objBookmark.ViewReportLink = string.Empty;
                    objBookmark.ProductName = ProductName;
                    objBookmark.ApplicantName = firstname + " " + lastname;
                    objBookmark.ReportCode = ProductCode;
                    MessageBody = objTemplateBookmarks.ReplaceBookmarks(objBookmark, Guid.Empty);

                    #endregion

                    //Send Email
                    Subject = emailContent.TemplateSubject.Replace("%ApplicantName%", firstname + " " + lastname); //"Your Report on " + ObjReport.SearchedFirstName + " " + ObjReport.SearchedLastName + " is Ready on Emerge";
                    //string Rolename = "";
                    BALPendingOrders objBALPendingOrders = new BALPendingOrders();

                    if (IsSetTrueMailByUser(orderid))
                        objBALPendingOrders.SendEmail(emailAddress, MessageBody, Subject);

                    #endregion
                }


            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in UpdateReportStatusAndSendMail(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                status = -1;
            }

            return Json(status);
        }

        #endregion




        #region Upload Image

        public ActionResult UploadImg(IEnumerable<HttpPostedFileBase> UploadFlePDF, int OrderDetailId)
        {
            string strManualResponse = string.Empty;
            string sFileName = string.Empty;

            try
            {
                string Extension = Path.GetExtension(UploadFlePDF.ElementAt(0).FileName).ToLower();
                if (Extension == ".jpg" || Extension == ".jpeg" || Extension == ".png" || Extension == ".bmp" || Extension == ".gif" || Extension == ".htm" || Extension == ".html")
                {

                    sFileName = OrderDetailId.ToString() + "_" + DateTime.Now.ToFileTime().ToString() + Extension;
                    //string sFilePath = "";

                    if (System.IO.File.Exists(Server.MapPath(@"~\Resources\Upload\ResponseImages\") + sFileName))
                    {
                        try
                        {
                            System.IO.File.Delete(Server.MapPath(@"~\Resources\Upload\ResponseImages\") + sFileName);
                        }
                        catch (Exception ex)
                        {
                            //Added log for exception handling.
                            VendorServices.GeneralService.WriteLog("Some Error Occured in UploadImg(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                        }

                    }
                    HttpPostedFileBase ObjHttpPostedFile = Request.Files[0];
                    ObjHttpPostedFile.SaveAs(Server.MapPath(@"~\Resources\Upload\ResponseImages\") + sFileName);

                    string FilePath = Request.PhysicalApplicationPath + @"Resources\Upload\ResponseImages\" + sFileName;

                    if (Extension == ".jpg" || Extension == ".jpeg" || Extension == ".png" || Extension == ".bmp" || Extension == ".gif")
                    {
                        System.Drawing.Image image1 = System.Drawing.Image.FromFile(FilePath);

                        using (MemoryStream stream = new MemoryStream())
                        {
                            image1.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);

                            string strResponse = ImageToBase64(image1, System.Drawing.Imaging.ImageFormat.Jpeg);

                            strManualResponse = "<img alt='ResponseImage' height='95%' width='95%' src='data:image/jpg;base64," + strResponse + "' />";

                            image1.Dispose();
                            stream.Dispose();
                        }
                    }
                    if (Extension == ".htm" || Extension == ".html")
                    {
                        strManualResponse = System.IO.File.ReadAllText(FilePath).ToString();

                    }
                    try
                    {
                        BALOrders ObjBALOrders = new BALOrders();                                                 // Update Manual Response
                        int Result = ObjBALOrders.UpdateManualResponseByOrderDetailId(OrderDetailId, strManualResponse);

                        if (Result == 1)
                        {
                            //lblMessageUpdate.Visible = true;
                            //lblMessageUpdate.CssClass = "successmsg";
                            //lblMessageUpdate.Text = "Information Updated Successfully";
                        }
                        else
                        {
                            //lblMessageUpdate.Visible = true;
                            //lblMessageUpdate.CssClass = "errormsg";
                            //lblMessageUpdate.Text = "Problem while saving. Please try again";
                        }
                    }
                    catch (Exception ex)
                    {
                        //Added log for exception handling.
                        VendorServices.GeneralService.WriteLog("Some Error Occured in UploadImg(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    }




                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in UploadImg(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }

            finally
            {
                //if (System.IO.File.Exists(Server.MapPath(@"~\Resources\Upload\ResponseImages\") + sFileName))
                //{
                //    try
                //    {
                //        System.IO.File.Delete(Server.MapPath(@"~\Resources\Upload\ResponseImages\") + sFileName);
                //    }
                //    catch { }
                //}
            }
            return Content(string.Empty);
        }

        public string ImageToBase64(System.Drawing.Image image, System.Drawing.Imaging.ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to Base64 String
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }

        public ActionResult UploadAttach(IEnumerable<HttpPostedFileBase> UploadAttachPDF, int OrderDetailId)
        {
            string sFileName = string.Empty;

            try
            {
                string Extension = Path.GetExtension(UploadAttachPDF.ElementAt(0).FileName).ToLower();
                if (Extension == ".png" || Extension == ".bmp" || Extension == ".gif" || Extension == ".jpg" || Extension == ".jpeg" || Extension == ".htm" || Extension == ".html" || Extension == ".pdf")
                {

                    sFileName = OrderDetailId.ToString() + "_" + DateTime.Now.ToFileTime().ToString() + Extension;

                    if (System.IO.File.Exists(Server.MapPath(@"~\Resources\Upload\ResponseImages\") + sFileName))
                    {
                        try
                        {
                            System.IO.File.Delete(Server.MapPath(@"~\Resources\Upload\ResponseImages\") + sFileName);
                        }
                        catch (Exception ex)
                        {
                            //Added log for exception handling.
                            VendorServices.GeneralService.WriteLog("Some Error Occured in UploadAttach(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                        }

                    }
                    HttpPostedFileBase ObjHttpPostedFile = Request.Files[0];
                    ObjHttpPostedFile.SaveAs(Server.MapPath(@"~\Resources\Upload\ResponseImages\") + sFileName);
                    try
                    {
                        BALOrders ObjBALOrders = new BALOrders();                                                 // Update ResponseAttachment
                        int Result = ObjBALOrders.UpdateResponseAttachByOrderDetailId(OrderDetailId, sFileName);

                        if (Result == 1)
                        {
                            //lblMessageUpdate.Visible = true;
                            //lblMessageUpdate.CssClass = "successmsg";
                            //lblMessageUpdate.Text = "Information Updated Successfully";
                        }
                        else
                        {
                            //lblMessageUpdate.Visible = true;
                            //lblMessageUpdate.CssClass = "errormsg";
                            //lblMessageUpdate.Text = "Problem while saving. Please try again";
                        }
                    }
                    catch (Exception ex)
                    {
                        //Added log for exception handling.
                        VendorServices.GeneralService.WriteLog("Some Error Occured in UploadAttach(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
                    }

                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in UploadAttach(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }

            return Content(string.Empty);
        }

        #endregion

        #endregion

        private bool IsSetTrueMailByUser(int fkOrderId)
        {
            bool isSend = false;
            int fkCompanyUserId = 0;
            using (EmergeDALDataContext dx = new EmergeDALDataContext())
            {
                fkCompanyUserId = Int32.Parse((dx.tblOrders.Where(d => d.pkOrderId == fkOrderId).Select(d => d.fkCompanyUserId).FirstOrDefault()).ToString());
                ///
                /// this condition is set if user off email notification from preferences 
                ///
                if (dx.tblEmailNotificationAlerts.Where(d => d.fkCompanyUserId == fkCompanyUserId).Count() > 0)
                {
                    isSend = dx.tblEmailNotificationAlerts.Where(d => d.fkCompanyUserId == fkCompanyUserId && d.fkTemplateId == 5).Select(d => d.IsSendEmail).FirstOrDefault();
                }
                else
                {
                    isSend = true;
                }
            }
            return isSend;
        }

        //ND-17
        public ActionResult CheckCompanyPaymentTerm(string companyId)
        {
            int flag = 0;
            decimal preValue = 0;
            try
            {

                flag = new BALCompany().GetCompanyPaymentType(Convert.ToInt32(companyId));
                preValue = new BALPayment().GetPrePaymentAmountByCompanyId(Convert.ToInt32(companyId));
            }
            catch
            {
            }
            return Json(new { CompanyTerm = flag, Amount = preValue });
        }

        public ActionResult Searching()
        {
            SearchingModel ObjModel = new SearchingModel();

            if (Session["EmergeReport"] != null)
            {

                #region Get User Name from Searched Records

                EmergeReport ObjEmergeReport = (EmergeReport)Session["EmergeReport"];
                if (ObjEmergeReport.ObjtblOrderSearchedData != null)
                {
                    if (!string.IsNullOrEmpty(ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName.ToString()))
                    {
                        string FirstName = ObjEmergeReport.ObjtblOrderSearchedData.SearchedFirstName;
                        string LastName = ObjEmergeReport.ObjtblOrderSearchedData.SearchedLastName;
                        string MiddleName = ObjEmergeReport.ObjtblOrderSearchedData.SearchedMiddleInitial ?? string.Empty;
                        string Prefix = ObjEmergeReport.ObjtblOrderSearchedData.SearchedSuffix;
                        ObjModel.Username = FirstName.ToUpper() + " " + MiddleName.ToUpper() + " " + LastName.ToUpper() + " " + Prefix.ToUpper();

                        BALContentManagement objContents = new BALContentManagement();
                        tblLoginPageSetting objtblLoginPageSetting = new tblLoginPageSetting();
                        objtblLoginPageSetting = objContents.GetLogoImg();
                        ViewBag.loadingbgColor = objtblLoginPageSetting.LoadingImgColor;
                    }
                }
                ObjModel.IsAdditionalReport = "false";
                if (TempData["IsErrorAdditionalRptRan"] != null)
                {
                    ObjModel.IsAdditionalReport = TempData["IsErrorAdditionalRptRan"].ToString();
                }
                #endregion

                //if (Session["Admin"] == null)
                //{
                //    LogOutCurrentUser(ObjMembershipUser);
                //}

            }
            return View(ObjModel);
        }

        public NewReportModel PartialReportNotGenerating(NewReportModel ObjNewReportModel)
        {
            MembershipUser Member = Membership.GetUser(User.Identity.Name);
            if (Member != null)
            {
                ProfileCommon Profile = new ProfileCommon();
                ProfileModel userProfile = Profile.GetProfile(Member.Email);
                if (!string.IsNullOrEmpty(userProfile.IsReportNotGenerating))
                {
                    ObjNewReportModel.ReportNotGeneratingProfile = userProfile.IsReportNotGenerating.ToString();
                }
            }


            ObjNewReportModel.CollTemplateContents = GetTemplateContentsById(Convert.ToInt32(EmailTemplates.ReportsNotGenerating), true);
            if (ObjNewReportModel.CollTemplateContents != null)
            {
                ObjNewReportModel.SystemNotificationStatus = ObjNewReportModel.CollTemplateContents.ElementAt(0).IsEnabled;

                if (ObjNewReportModel.SystemNotificationStatus == true)
                {
                    BALGeneral ObjBALGeneral = new BALGeneral();
                    string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;

                    string FileContent = GetSystemTemplate("SystemNotification.htm");
                    string UpdatedContent = FileContent.
                           Replace("%EmergeLogo%", "<img src='" + WebsiteLogo + "' />").
                          Replace("%NotificationSubject%", ObjNewReportModel.CollTemplateContents.ElementAt(0).TemplateSubject).
                         Replace("%NotificationDetails%", ObjNewReportModel.CollTemplateContents.ElementAt(0).TemplateContent);

                    ObjNewReportModel.SystemNotification = UpdatedContent;

                }
            }

            return ObjNewReportModel;


        }


        protected string GetSystemTemplate(string FileName)
        {
            string FileContent = string.Empty;

            string FilePath = HttpContext.Server.MapPath("~/Resources/Templates/" + FileName);
            if (System.IO.File.Exists(FilePath))
            {
                using (StreamReader sr = System.IO.File.OpenText(FilePath))
                {
                    FileContent = sr.ReadToEnd();
                    sr.Close();
                    sr.Dispose();
                }
            }
            return FileContent;
        }

        public List<Proc_USA_LoadEmailTemplatesResult> GetTemplateContentsById(int pkTemplateId, bool IsSystemTemplate)
        {
            BALEmailSystemTemplates ObjTemplate = new BALEmailSystemTemplates();
            List<Proc_USA_LoadEmailTemplatesResult> ObjRecords = ObjTemplate.GetEmailTemplates(pkTemplateId, Utility.SiteApplicationId, IsSystemTemplate, 0);
            return ObjRecords;
        }

        public ActionResult UpdateProfileWithReportNotGenerating()
        {
            ProfileCommon Profile = new ProfileCommon();
            MembershipUser Member = Membership.GetUser(User.Identity.Name);
            if (Member != null)
            {
                Profile.SetReportNotGeneratingProfile(Member.Email, string.Empty);
            }
            return RedirectToAction("NewReport");
        }

        public NewReportModel PartialFcraAgreement(NewReportModel ObjNewReportModel)
        {
            MembershipUser Member = Membership.GetUser(User.Identity.Name);
            if (Member != null)
            {
                Guid UserId = new Guid(Member.ProviderUserKey.ToString());
                BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();

                tblCompanyUser ObjTest = ObjBALCompanyUsers.GetUserInformationByMembershipUserId(UserId).First();

                if (ObjTest.IsSignedFcra == false || ObjTest.IsSignedFcra == null)
                {
                    ObjNewReportModel.FCRAStatus = false;
                }
                else
                {
                    ObjNewReportModel.FCRAStatus = true;
                }
            }

            return ObjNewReportModel;
        }

        public FilePathResult DownloadFCRA()
        {
            MembershipUser Member = Membership.GetUser(User.Identity.Name);
            if (Member != null)
            {
                Guid UserId = new Guid(Member.ProviderUserKey.ToString());
                BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
                ObjBALCompanyUsers.UpdateSignedFcraStatus(UserId);
            }
            string FilePath = Server.MapPath("~/Resources/Upload/FcraPDF/FCRA_Agreement.pdf");
            string fileext = System.IO.Path.GetExtension(FilePath);
            string CType = "application/" + fileext.Replace(".", "");
            string fileName = Path.GetFileName(FilePath);
            return File(FilePath, CType, fileName);
        }

        public ActionResult UpdateSignedFCRAStatus()
        {
            MembershipUser Member = Membership.GetUser(User.Identity.Name);
            if (Member != null)
            {
                Guid UserId = new Guid(Member.ProviderUserKey.ToString());
                BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
                ObjBALCompanyUsers.UpdateSignedFcraStatus(UserId);
            }
            return Json(new { Result = 1 });

        }

        public PartialViewResult NewReportLogApi(int OrderId)
        {
            BALGeneral ObjBALGeneral = new BALGeneral();
            NewReportModel ObjModel = new NewReportModel();
            int UserLocationId = 0;
            int pkCompanyId = 0;
            if (TempData["IsErrorLogRan"] != null)
            {
                ObjModel.IsErrorLog = TempData["IsErrorLogRan"].ToString();
            }
            BALOrders ObjBALOrders = new BALOrders();
            BALCompany ObjBALCompany = new BALCompany();
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            MembershipUser Member = Membership.GetUser(User.Identity.Name);
            CustomOrderCollection ObjCustomOrderCollection = new CustomOrderCollection();
            try
            {
                ObjCustomOrderCollection = ObjBALOrders.GetOrderDetailsByOrderId(OrderId);
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in NewReportLogApi(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }

            try
            {

                if (ObjCustomOrderCollection.ListOrderDetail.Count > 0)
                {
                    # region Bind OrderCompanyId and LocationId

                   // int hdnOdrCompanyId = Int32.Parse(ObjCustomOrderCollection.ListOrderDetail.FirstOrDefault().fkCompanyId.ToString());
                   // int hdnOdrLocationId = Int32.Parse(ObjCustomOrderCollection.ObjOrder.fkLocationId.ToString());
                    int hdnOdrCompanyUserId = Int32.Parse(ObjCustomOrderCollection.ObjOrder.fkCompanyUserId.ToString());

                    # endregion
                    #region Get Company Id

                    //if (Member != null)
                    //{
                    var dbUserInfo = ObjBALCompanyUsers.SelectCompanyUserByCompanyUserId(hdnOdrCompanyUserId);
                    var dbCollection = ObjBALCompanyUsers.GetUserCompanyInfo(dbUserInfo.fkUserId);
                    if (dbCollection.Count > 0)
                    {
                        int fkLocationId = Int32.Parse(dbCollection.First().pkLocationId.ToString());
                        UserLocationId = fkLocationId;
                        ObjModel.fkLocationId = fkLocationId;
                        ObjModel.pkCompanyUserId = Int32.Parse(dbCollection.First().pkCompanyUserId.ToString());
                        ObjModel.OrderCompanyName = dbCollection.First().CompanyName;

                        ObjModel.CompanyAccountNo = dbCollection.First().CompanyAccountNumber;
                        ObjModel.PermissiblePurposeId = dbCollection.First().PermissiblePurpose;
                        pkCompanyId = dbCollection.First().pkCompanyId;
                        ObjModel.pkCompanyId = pkCompanyId;
                        ObjModel.IsWCVAcknowledged = dbCollection.First().IsWCVAcknowledged;
                        ObjModel.IsLiveRunnerEnabled = dbCollection.First().IsLiveRunnerEnabled;

                    }
                    //}


                    #endregion
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in NewReportLogApi(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");

                if (Member != null)
                {
                    var dbCollection = ObjBALCompanyUsers.GetUserCompanyInfo(new Guid(Member.ProviderUserKey.ToString()));
                    if (dbCollection.Count > 0)
                    {
                        int fkLocationId = Int32.Parse(dbCollection.First().pkLocationId.ToString());
                        UserLocationId = fkLocationId;
                        ObjModel.fkLocationId = fkLocationId;
                        ObjModel.pkCompanyUserId = Int32.Parse(dbCollection.First().pkCompanyUserId.ToString());
                        ObjModel.OrderCompanyName = dbCollection.First().CompanyName;
                        ObjModel.CompanyAccountNo = dbCollection.First().CompanyAccountNumber;
                        ObjModel.PermissiblePurposeId = dbCollection.First().PermissiblePurpose;
                        pkCompanyId = dbCollection.First().pkCompanyId;
                        ObjModel.pkCompanyId = pkCompanyId;
                        ObjModel.IsLiveRunnerEnabled = dbCollection.First().IsLiveRunnerEnabled;
                    }
                }
            }

            ObjModel = BindNewReportDropdowns(ObjBALGeneral, ObjModel, pkCompanyId, ObjBALCompany);


            #region Bind Reports
            try
            {
                if (ObjCustomOrderCollection.ListOrderReports.Count > 0)
                {
                    List<string> ObjCollProducts = ObjCustomOrderCollection.ListOrderReports.ToList();
                    ViewData["ObjCollProducts"] = ObjCollProducts;
                    ViewData["vw_Panels"] = ObjCollProducts;
                    string Codes = string.Empty;
                    string[] lstProductCodes = ObjCustomOrderCollection.ListOrderReports.ToArray();
                    Codes = string.Join("", lstProductCodes);
                    ObjModel.hfReports = Codes.ToString();


                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in NewReportLogApi(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            #endregion Bind Reports

            #region Bind Order Searched Data
            try
            {
                if (ObjCustomOrderCollection.ObjOrderSearchedData != null)
                {
                    ObjModel.LastName = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedLastName);
                    ObjModel.FirstName = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedFirstName);
                    ObjModel.MI = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedMiddleInitial);
                    ObjModel.SuffixValue = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedSuffix);
                    ObjModel.Social = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedSsn);
                    ObjModel.DOB = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedDob);
                    ObjModel.Phone = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedPhoneNumber);
                    ObjModel.Address = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedStreetAddress);
                    ObjModel.DirectionValue = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedDirection);
                    ObjModel.Street = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedStreetName);
                    ObjModel.TypeValue = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedStreetType);
                    ObjModel.Apt = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedApt);
                    ObjModel.City = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedCity);
                    ObjModel.ApplicantEmail = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedApplicantEmail);
                    ObjModel.pkStateId = Convert.ToInt32(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedStateId);
                    string strstate = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedStateId);
                    ObjModel.State = strstate;
                    if (ObjCustomOrderCollection.ObjOrderSearchedData.SearchedZipCode.Length > 5)
                    {
                        ObjCustomOrderCollection.ObjOrderSearchedData.SearchedZipCode = ObjCustomOrderCollection.ObjOrderSearchedData.SearchedZipCode.Substring(0, 5);
                    }

                    ObjModel.Zip = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedZipCode);
                    if (ObjCustomOrderCollection.ObjOrderSearchedData.SearchedCountyId != null)
                    {
                        ObjModel.AllowJurisdiction = true;
                        ObjModel.Jurisdiction = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedCountyId);
                    }
                    else
                    {
                        ObjModel.AllowJurisdiction = false;
                    }

                    #region Bind Business Info
                    ObjModel.BusinessName = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedBusinessName);
                    ObjModel.BusinessCity = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedBusinessCity);

                    ObjModel.BusinessState = Convert.ToInt32(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedBusinessStateId);

                    #endregion Bind Business Info

                    #region Bind Automative Info

                    string str = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedDLStateId);
                    //INT-335
                    //Landing page issue, where state was not binding properly.
                    ObjModel.DLState = str;
                    ObjModel.DriverLicense = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedDriverLicense);
                    ObjModel.VehicleVin = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedVehicleVin);
                    ObjModel.Sex = CheckNull(ObjCustomOrderCollection.ObjOrderSearchedData.SearchedSex);

                    #endregion Bind Automative Info

                    #region Get Jurisdiction For Report Log

                    List<clsJurisdiction> ObjJudCollection = ObjBALGeneral.GetJurisdictionsForNewReportLog();
                    ObjJudCollection.Insert(0, new clsJurisdiction { pkJurisdictionId = "-1", JurisdictionName = "" });
                    ObjModel.JurisdictionList = ObjJudCollection;

                    #endregion
                }
                if (ObjModel.hfReports != null)
                {
                    if (ObjModel.hfReports.Contains("SCR"))
                    {



                        string IsLiveRunner = ObjModel.pkStateId.ToString().Split('_').ElementAt(0);


                        ObjModel.IsLiveRunner = IsLiveRunner;
                    }
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in NewReportLogApi(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            #endregion Bind Order Searched Data

            #region Bind Order County Info
            try
            {
                if (ObjCustomOrderCollection.ListOrderSearchedLiveRunnerInfo.Count > 0)
                {
                    ObjModel.ObjOrderSearchedLiveRunnerInfo = ObjCustomOrderCollection.ListOrderSearchedLiveRunnerInfo;
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in NewReportLogApi(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            #endregion Bind Order County Info


            #region Bind Order Employment Info
            try
            {
                if (ObjCustomOrderCollection.ListOrderSearchedEmploymentInfo.Count > 0)
                {
                    ObjModel.ObjSearchedEmploymentInfo = ObjCustomOrderCollection.ListOrderSearchedEmploymentInfo;
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in NewReportLogApi(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            #endregion Bind Order Employment Info

            #region Bind Order Education Info
            try
            {
                if (ObjCustomOrderCollection.ListOrderSearchedEducationInfo.Count > 0)
                {
                    ObjModel.ObjSearchedEducationInfo = ObjCustomOrderCollection.ListOrderSearchedEducationInfo;
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in NewReportLogApi(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            #endregion Bind Order Education Info

            #region Bind Order Personal Info
            try
            {
                if (ObjCustomOrderCollection.ListOrderSearchedPersonalInfo.Count > 0)
                {
                    ObjModel.ObjOrderSearchedPersonalInfo = ObjCustomOrderCollection.ListOrderSearchedPersonalInfo;
                }

            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in NewReportLogApi(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }

            #endregion Bind Order Personal Info

            #region Bind Order License Info
            try
            {
                if (ObjCustomOrderCollection.ListOrderSearchedLicenseInfo.Count > 0)
                {
                    ObjModel.ObjOrderSearchedLicenseInfo = ObjCustomOrderCollection.ListOrderSearchedLicenseInfo;
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in NewReportLogApi(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }

            #endregion Bind Order License Info

            #region Bind Order Tracking Info
            try
            {
                if (ObjCustomOrderCollection.ObjReferenceCode != null)
                {
                    ObjModel.TrackingNotes = CheckNull(ObjCustomOrderCollection.ObjReferenceCode.pkReferenceCodeId);
                }
            }
            catch (Exception ex)
            {
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in NewReportLogApi(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }

            #endregion Bind Order Tracking Info

            #region Top Blocks
            try
            {
                BindTopBlocksApi(ObjCustomOrderCollection.ObjOrder, ObjCustomOrderCollection.ListOrderDetail, ObjModel);
            }
            catch (Exception ex)
            {
                ObjModel.ErrorMessage += "Top Block: " + ex.Message.ToString();
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in NewReportLogApi(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }
            #endregion

            #region Bind Error Log

            try
            {
                if (ObjCustomOrderCollection.ListOrderDetail.Count > 0)
                {
                    if (ObjCustomOrderCollection.ListOrderDetail != null && ObjCustomOrderCollection.ListOrderResponseData != null)
                    {
                        //BindErrorLog(ObjCustomOrderCollection.ObjOrder, ObjCustomOrderCollection.ListOrderDetail, ObjCustomOrderCollection.ListOrderResponseData, ObjModel);
                    }
                }
            }
            catch (Exception ex)
            {
                ObjModel.ErrorMessage += "ErrorLog: " + ex.Message.ToString();
                //Added log for exception handling.
                VendorServices.GeneralService.WriteLog("Some Error Occured in NewReportLogApi(): " + DateTime.Now.ToString() + "\n Error Message :" + ex.Message + "\n Stack Trace :" + ex.StackTrace, "RapidCourtErrorLog");
            }

            #endregion
            if (pkCompanyId != 0)
            {
                var Itemlists = ObjBALCompany.IsEscreenEnableForCompany(pkCompanyId);
                ObjModel.IsEscreenEnable = Itemlists.Item1;
            }


            ObjModel.UserName = "dummy";
            ObjModel.Password = "dummy";

            return PartialView("NewReportLogApi", ObjModel);
        }

        #region INT-14 check applicant email not same as any useremail
        public ActionResult ApplicantEmailExistence(string emailId)
        {
            int result = 0;
            int comapnyId = Convert.ToInt16(Session["pkCompanyId"]);
            using (EmergeDALDataContext obj = new EmergeDALDataContext())
            {
                var UserList = obj.applicantEmailExistWithUser(comapnyId, emailId).ToList();
                if (UserList != null)
                {
                    if (UserList.Count > 0)
                    {
                        result = 1;
                    }
                    else
                    {
                        result = 0;
                    }
                }
            }
            return Json(new { Status = result });
        }
        #endregion
    }
}
