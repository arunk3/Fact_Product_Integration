﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Common;
using System.Web.Profile;
using System.Configuration;
using System.Web.Security;
using Emerge.Services;
using Emerge.Data;

namespace Emerge.Controllers.Corporate
{
    public partial class CorporateController : BaseEmergeController
    {
        //
        // GET: /MyPreferences/

        public ActionResult MyPreferences()
        {
            MyPreferencesModel Model = new MyPreferencesModel();
            try
            {
               // BALReferenceCode objBALReferenceCode = new BALReferenceCode();
                //int fkcompanyid = 0;
                if (Session["pkCompanyId"] !=null)
                {
                   // fkcompanyid = Int32.Parse(Session["pkCompanyId"].ToString());
                    string username = (Session["CurrentUserMailId"] == null) ? string.Empty : Session["CurrentUserMailId"].ToString();
                    string UserRole = Roles.GetRolesForUser().FirstOrDefault();
                    ProfileCommon Profile = new ProfileCommon();
                    ProfileModel userProfile = Profile.GetProfile(username);
                    Model.listDefaultPages = checkusers(UserRole);
                    Model.listSortColumn = getSortColumnlist();
                    Model.listRecordSize = getRecordSizelist();
                    Model.IsDataEntryOnly = false;
                    if (!string.IsNullOrEmpty(userProfile.DefaultPage))
                    {
                        Model.listDefaultPages = Model.listDefaultPages.Select(x => new SelectListItem { Text = x.Text, Value = x.Value, Selected = (x.Value == userProfile.DefaultPage) }).ToList();
                        Model.DefaultPage = userProfile.DefaultPage;
                    }
                    else
                    {
                    Model.DefaultPage = Model.listDefaultPages.Select(x => new SelectListItem { Text = x.Text, Value = x.Value }).Select(x=>x.Value).FirstOrDefault();
                 
                       
                    }
                    if (!string.IsNullOrEmpty(userProfile.SortColumn))
                    {
                        Model.listSortColumn = Model.listSortColumn.Select(x => new SelectListItem { Text = x.Text, Value = x.Value, Selected = (x.Value == userProfile.SortColumn) }).ToList();
                        Model.SortColumn = userProfile.SortColumn;
                    }
                    else
                    {
                        Model.SortColumn = Model.listSortColumn.Select(x => new SelectListItem { Text = x.Text, Value = x.Value }).Select(x => x.Value).FirstOrDefault();
                    }
                    if (!string.IsNullOrEmpty(userProfile.RecordSize))
                    {
                        Model.listRecordSize = Model.listRecordSize.Select(x => new SelectListItem { Text = x.Text, Value = x.Value, Selected = (x.Value == userProfile.RecordSize) }).ToList();
                        Model.RecordSize = userProfile.RecordSize;
                    }
                    else
                    {
                        Model.RecordSize = Model.listRecordSize.Select(x => new SelectListItem { Text = x.Text, Value = x.Value }).Select(x => x.Value).FirstOrDefault();
                    }
                    if (!string.IsNullOrEmpty(userProfile.IsDataEntryOnly_Enable))
                    {
                        Model.IsDataEntryOnly = Convert.ToBoolean(userProfile.IsDataEntryOnly_Enable.ToString());
                    }
                    BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
                    var dbCollection = ObjBALCompanyUsers.GetUserInformationByMembershipUserId(new Guid(Membership.GetUser(username).ProviderUserKey.ToString()));
                    if (dbCollection.Count > 0)
                    {
                        int fkCompanyUserId = dbCollection.First().pkCompanyUserId;
                        InsertEmailAlerts(fkCompanyUserId);
                    }
                }
            }
            catch { }

            return View(Model);
        }


        private void InsertEmailAlerts(int fkCompanyUserId)
        {
                BALReferenceCode ObjBALReferenceCode = new BALReferenceCode();
                tblEmailNotificationAlert objtblEmailNotificationAlert = new tblEmailNotificationAlert();
                objtblEmailNotificationAlert.fkTemplateId = 5;
                objtblEmailNotificationAlert.fkCompanyUserId = fkCompanyUserId;
                objtblEmailNotificationAlert.IsSendEmail = true;
                objtblEmailNotificationAlert.IsSendAlert = true;
                ObjBALReferenceCode.InsertEmailAlerts(objtblEmailNotificationAlert);
        }


        private List<SelectListItem> checkusers(string UserRole)
        {
            List<SelectListItem> listPages = new List<SelectListItem>();
            if (UserRole == "BranchManager" || UserRole == "BasicUser")
            {
                listPages.Add(new SelectListItem { Value = "NewReport", Text = "New Reports" });
                listPages.Add(new SelectListItem { Value = "SavedReports", Text = "Archives" });
                listPages.Add(new SelectListItem { Value = "MyAccount", Text = "My Profile" });
                listPages.Add(new SelectListItem { Value = "MyReports", Text = "My Reports" });
                listPages.Add(new SelectListItem { Value = "MyForms", Text = "My Forms" });
                listPages.Add(new SelectListItem { Value = "MyPreferences", Text = "My Preferences" });
            }
            if (UserRole == "SystemAdmin" || UserRole == "SupportManager" || UserRole == "SupportTechnician")//UserRole == "Admin" ||
            {
                listPages.Add(new SelectListItem { Value = "NewReport", Text = "New Reports" });
                listPages.Add(new SelectListItem { Value = "SavedReports", Text = "Archives" });
                listPages.Add(new SelectListItem { Value = "MyAccount", Text = "My Profile" });
                listPages.Add(new SelectListItem { Value = "MyReports", Text = "My Reports" });
                listPages.Add(new SelectListItem { Value = "MyForms", Text = "My Forms" });
                listPages.Add(new SelectListItem { Value = "MyPreferences", Text = "My Preferences" });
                listPages.Add(new SelectListItem { Value = "BillingStatement", Text = "My Statements" });

            }
            if (UserRole == "CorporateManager")
            {
                listPages.Add(new SelectListItem { Value = "NewReport", Text = "New Reports" });
                listPages.Add(new SelectListItem { Value = "SavedReports", Text = "Archives" });
                listPages.Add(new SelectListItem { Value = "MyAccount", Text = "My Profile" });
                listPages.Add(new SelectListItem { Value = "MyReports", Text = "My Reports" });
                listPages.Add(new SelectListItem { Value = "MyForms", Text = "My Forms" });
                listPages.Add(new SelectListItem { Value = "MyPreferences", Text = "My Preferences" });
                listPages.Add(new SelectListItem { Value = "BillingStatement", Text = "My Statements" });
                listPages.Add(new SelectListItem { Value = "BillingStatement?invoiceid=-1", Text = "Pay Invoice" });
            }
            if (UserRole == "DataEntry")
            {
                listPages.Add(new SelectListItem { Value = "NewReport", Text = "New Reports" });
                listPages.Add(new SelectListItem { Value = "MyAccount", Text = "My Profile" });
            }

            if (UserRole == "ViewOnlyUser")
            {
                listPages.Add(new SelectListItem { Value = "SavedReports", Text = "Archives" });
                listPages.Add(new SelectListItem { Value = "MyAccount", Text = "My Profile" });
            }


            if (string.IsNullOrEmpty(UserRole))
            {
                listPages.Add(new SelectListItem { Value = "NewReport", Text = "New Reports" });
            }
            return listPages;
        }

        private List<SelectListItem> getSortColumnlist()
        {
            List<SelectListItem> listSortColumn = new List<SelectListItem>();
            listSortColumn.Add(new SelectListItem { Value = "OrderDt", Text = "Order Date" });
            listSortColumn.Add(new SelectListItem { Value = "ApplicantName", Text = "Applicant Name" });
            listSortColumn.Add(new SelectListItem { Value = "OrderNo", Text = "Order No" });
            listSortColumn.Add(new SelectListItem { Value = "ReportIncludes", Text = "Data Sources" });
            listSortColumn.Add(new SelectListItem { Value = "CompanyName", Text = "Company Name" });
            listSortColumn.Add(new SelectListItem { Value = "Location", Text = "Location" });
            listSortColumn.Add(new SelectListItem { Value = "User", Text = "User" });

            return listSortColumn;
        }

        private List<SelectListItem> getRecordSizelist()
        {
            List<SelectListItem> listRecordSize = new List<SelectListItem>();
            listRecordSize.Add(new SelectListItem { Value = "25", Text = "25" });
            listRecordSize.Add(new SelectListItem { Value = "50", Text = "50" });
            listRecordSize.Add(new SelectListItem { Value = "75", Text = "75" });
            listRecordSize.Add(new SelectListItem { Value = "100", Text = "100" });

            return listRecordSize;
        }

    }
}
