﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using System.Web.Security;
using Emerge.Data;
using System.IO;
using System.Data.OleDb;
using System.Data;
using LumenWorks.Framework.IO.Csv;
using System.Xml;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using System.Configuration;

namespace Emerge.Controllers.Corporate
{
    public partial class CorporateController : BaseEmergeController
    {
        public ActionResult NewBatch()
        {
            BALGeneral ObjBALGeneral = new BALGeneral();
            int UserLocationId = 0;
            int pkCompanyId = 0;
            NewBatchModel ObjModel = new NewBatchModel();

            #region Get Company Id

            //BALCompany ObjBALCompany = new BALCompany();
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            MembershipUser Member = Membership.GetUser(User.Identity.Name);
            if (Member != null)
            {
                var dbCollection = ObjBALCompanyUsers.GetUserCompanyInfo(new Guid(Member.ProviderUserKey.ToString()));
                if (dbCollection.Count > 0)
                {
                    int fkLocationId = Int32.Parse(dbCollection.First().pkLocationId.ToString());
                    UserLocationId = fkLocationId;
                    ObjModel.fkLocationId = fkLocationId;
                    ObjModel.pkCompanyUserId = Int32.Parse(dbCollection.First().pkCompanyUserId.ToString());

                    pkCompanyId = dbCollection.First().pkCompanyId;
                    ObjModel.OrderCompanyName = dbCollection.First().CompanyName;
                    ObjModel.CompanyAccountNo = dbCollection.First().CompanyAccountNumber;
                    ObjModel.PermissiblePurposeId = dbCollection.First().PermissiblePurpose;
                    ObjModel.pkCompanyId = pkCompanyId;
                    ObjModel.IsWCVAcknowledged = dbCollection.First().IsWCVAcknowledged;

                }
            }


            #endregion

            #region Left Panel Categories

            List<Proc_Get_ReportCategoriesResult> ObjDataCategories = ObjBALGeneral.GetReportCategories().Where(t => t.pkReportCategoryId != 9).ToList();
            ObjModel.ReportCategoriesList = ObjDataCategories;

            Proc_Get_ReportCategoriesResult ObjDynamicColl = new Proc_Get_ReportCategoriesResult();
           // List<string> ObjCollRcxProducts = new List<string>();
            ObjDynamicColl.pkReportCategoryId = 0;
            ObjDynamicColl.CategoryName = "Packages";
            ObjDynamicColl.IsEnabled = true;
            ObjDataCategories.Insert(0, ObjDynamicColl);

            #endregion

            #region Package List

            List<Proc_Get_EmergeProductPackagesResult> ObjReports = ObjBALGeneral.GetEmergeProductPackages(UserLocationId);
            ObjModel.EmergeProductPackagesList = ObjReports;

            #endregion

            #region Categories Report List

            Dictionary<int, List<Proc_Get_NewReportsByCategoryIdResult>> DictCategory = new Dictionary<int, List<Proc_Get_NewReportsByCategoryIdResult>>();
            for (int i = 0; i < ObjDataCategories.Count; i++)
            {

                var NewReportsByCategory1 = ObjBALGeneral.GetNewReportsByCategoryId(UserLocationId, ObjDataCategories.ElementAt(i).pkReportCategoryId);
                DictCategory.Add(i, NewReportsByCategory1);
            }
            ObjModel.DictCategory = DictCategory;

            #endregion

            #region Drug Testing

            BALProducts ObjBALProducts = new BALProducts();
            List<Proc_GetDrugTestingProductsResult> ObjData = ObjBALProducts.GetDrugTestingProducts(UserLocationId);
            ObjModel.DrugTestingList = ObjData;

            #endregion

            #region Bind Dropdown

            List<clsColumnList> ObjColumnlist = new List<clsColumnList>();
            ObjColumnlist.Add(new clsColumnList { Id = "-1", Text = "--" });
            ObjModel.ColumnList = ObjColumnlist;
            BALCompanyType serboj = new BALCompanyType();
            List<tblPermissiblePurpose> PermissiblePurpose = serboj.GetPermissiblePurpose();

            ObjModel.PermissiblePurpose = PermissiblePurpose.Select(x => new SelectListItem { Text = x.PermissiblePurpose, Value = x.pkPermissibleId.ToString() }).ToList();
            ObjModel.PermissiblePurpose = ObjModel.PermissiblePurpose.Select(x => new SelectListItem { Text = x.Text, Value = x.Value.ToString(), Selected = (x.Value == ObjModel.PermissiblePurposeId.ToString()) }).ToList();
            ObjModel.PermissiblePurposeValue = PermissiblePurpose.Where(x => x.pkPermissibleId == ObjModel.PermissiblePurposeId).Select(x => x.PermissiblePurpose).FirstOrDefault().ToString();
            #endregion
            #region From and To Email Id
            try
            {
                ObjModel.lblFrom = User.Identity.Name;
                byte settingId = 1;
                var settings = ObjBALGeneral.GetSettings(settingId);
                ObjModel.lblTo = settings.SupportEmailId; //ConfigurationManager.AppSettings["EMEditMailTo"].ToString();
            }
            catch (Exception)
            {
            }
            #endregion

            return View(ObjModel);
        }

        public ActionResult UploadBatchFile(IEnumerable<HttpPostedFileBase> UploadBatch)
        {
            string sFileName = string.Empty;
            try
            {
                MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
                Guid UserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
                string Extension = Path.GetExtension(UploadBatch.ElementAt(0).FileName).ToLower();
                if (Extension == ".csv" || Extension == ".xls" || Extension == ".xlsx")
                {
                    DateTime Dated = DateTime.Now;
                    string CreateUniquePathId = Dated.ToString("yyMMddHHmmssfff");
                    sFileName = CreateUniquePathId + "_" + UserId.ToString() + System.IO.Path.GetFileName(UploadBatch.ElementAt(0).FileName);
                    HttpPostedFileBase ObjHttpPostedFile = Request.Files[0];
                    string FilePath = Server.MapPath(@"~/Resources/Upload/TempBatchFiles/") + sFileName;
                    //try
                    //{
                    //    if (System.IO.File.Exists(FilePath))
                    //    {
                    //        System.IO.File.Delete(FilePath);
                    //    }
                    //}
                    //catch (Exception)
                    //{

                    //}
                    ObjHttpPostedFile.SaveAs(FilePath);
                    Session["sFileName"] = sFileName;
                }
            }
            catch (Exception)
            {

            }
            return Content(string.Empty);
        }

        #region Show Headers

        public ActionResult ShowHeaders()
        {
            string sFileName = Convert.ToString(Session["sFileName"]);
            string sFilePath = Server.MapPath(@"~/Resources/Upload/TempBatchFiles/") + sFileName;
            string SheetNames = "";
            if (!sFileName.ToLower().Contains(".csv"))
            {
                string xConnStr = string.Empty;

                if (sFileName.ToLower().Contains(".xlsx"))
                {
                    xConnStr = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=" + sFilePath + ";" + "Extended Properties=Excel 12.0;";
                }
                else if (sFileName.ToLower().Contains(".xls"))
                {
                    xConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + sFilePath + ";" + "Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\"";
                }
                TempData["ExcelConnectionString"] = xConnStr;
                System.Data.DataTable dt = null;
                using (OleDbConnection objXConn = new OleDbConnection(xConnStr))
                {
                    objXConn.Open();
                    dt = objXConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                }
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SheetNames += Convert.ToString(dt.Rows[i]["TABLE_NAME"]) + ",";

                    }
                    if (SheetNames != "")
                    {
                        SheetNames = SheetNames.Substring(0, SheetNames.Length - 1);
                    }
                }
            }

            return Json(SheetNames);
        }


        #endregion

        #region Get File Columns

        public ActionResult GetFileColumns(string SheetName, bool chkIsBatchHeaders, string ProductsSelected)
        {
            #region Declare Collection
            Session["chkIsBatchHeaders"] = chkIsBatchHeaders;
            List<string> ControlList = new List<string>();
            List<string> FileColumns = new List<string>();

            #endregion

            int Status = 1;
            string sFileName = Convert.ToString(Session["sFileName"]);
            if (!sFileName.ToLower().Contains(".csv"))
            {
                string ExcelConnectionString = Convert.ToString(TempData["ExcelConnectionString"]);
                TempData["ExcelConnectionString"] = ExcelConnectionString;
                int ReturnValue = GetExcelHeaders(ExcelConnectionString, SheetName, FileColumns);
                //string Message = string.Empty;
                if (ReturnValue == 0)
                {
                    Status = (chkIsBatchHeaders) ? 0 : -1;
                }

            }
            else
            {
                Status = GetCSVHeaders(FileColumns);
            }

            if (Status == 1)
            {
                if (ProductsSelected.Length > 0)
                {
                    string ProductSelected = ProductsSelected.Substring(0, (ProductsSelected.Length - 1));
                    string[] stringSeparator = new string[] { "##" };
                    List<string> ObjCollection = ProductSelected.Substring(1, (ProductSelected.Length - 1)).Split(stringSeparator, StringSplitOptions.RemoveEmptyEntries).Distinct().ToList<string>();
                    for (int iRow = 0; iRow < ObjCollection.Count; iRow++)
                    {
                        List<string> ObjCollectionControls = GetRequiredColumnsForSelectedReport(ObjCollection[iRow].ToString());
                        for (int iReq = 0; iReq < ObjCollectionControls.Count; iReq++)
                        {
                            if (!ControlList.Contains(ObjCollectionControls.ElementAt(iReq).ToString()))
                            {
                                ControlList.Add(ObjCollectionControls.ElementAt(iReq).ToString());
                            }
                        }
                    }
                }
                ControlList.Add("MI");
                ControlList.Add("Social");
                ControlList.Add("ApplicantEmail");
                ControlList.Add("Direction");
                ControlList.Add("Type");
                ControlList.Add("Apt");
                ControlList.Add("AliasName");
                ControlList.Add("InstituteAlias");
                ControlList.Add("ReferenceAlias");
                ControlList.Add("LicenseAlias");
            }
            return Json(new { Status = Status, ControlList = ControlList, FileColumns = FileColumns });
        }

        private List<string> GetRequiredColumnsForSelectedReport(string ProductCode)
        {
            List<string> ObjCollection = new List<string>();
            if (ProductCode.Contains("_"))
            {
                string[] Pcode = ProductCode.Split('_');
                ProductCode = Pcode[0].ToString();
            }
            XmlDocument ObjXmlDocument = new XmlDocument();
            ObjXmlDocument.Load(Server.MapPath("~/Content/ProductXml/ProductRequireData.xml"));

            XmlNode ObjXmlNode = ObjXmlDocument.SelectSingleNode("/Products/Product[@Code='" + ProductCode + "']");

            if (ObjXmlNode != null)
            {
                for (int iRow = 0; iRow < ObjXmlNode.ChildNodes.Count; iRow++)
                {
                    ObjCollection.Add(ObjXmlNode.ChildNodes[iRow].InnerText);
                }
            }
            return ObjCollection;
        }

        private int GetExcelHeaders(string xConnStr, string SheetName, List<string> FileColumns)
        {
            int ColumnValue = 0;
            using (DataSet objDataSet = new DataSet())
            {
                using (OleDbDataAdapter objDataAdapter = new OleDbDataAdapter())
                {
                    objDataAdapter.SelectCommand = GetExcelFileInformation(xConnStr, SheetName);
                    objDataAdapter.Fill(objDataSet);
                }
                for (int i = 0; i < objDataSet.Tables[0].Columns.Count; i++)
                {


                    if (objDataSet.Tables[0].Columns.Count == 1 && objDataSet.Tables[0].Columns[i].Caption.Trim() == "F1")
                    {
                        FileColumns.Add("");
                        ColumnValue = 0;
                    }
                    else
                    {
                        ColumnValue = 1;
                        FileColumns.Add(objDataSet.Tables[0].Columns[i].Caption.Trim());
                    }
                }
            }
            return ColumnValue;
        }

        protected OleDbCommand GetExcelFileInformation(string xConnStr, string SheetName)
        {
            OleDbCommand objCommand = new OleDbCommand();
            try
            {
                if (xConnStr != string.Empty)
                {
                    System.Data.DataTable dt = null;
                    using (OleDbConnection objXConn = new OleDbConnection(xConnStr))
                    {
                        objXConn.Open();
                        dt = objXConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        if (dt.Rows.Count > 0)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                if (dt.Rows[i]["TABLE_NAME"].ToString() == SheetName)
                                {
                                    SheetName = "[" + SheetName + "]";
                                    objCommand = new OleDbCommand("SELECT * FROM " + SheetName, objXConn);

                                }
                            }
                        }
                    }
                }
            }
            catch
            {
            }

            return objCommand;
        }

        protected int GetCSVHeaders(List<string> FileColumns)
        {
            int ColumnValue = 0;
            string sFileName = Convert.ToString(Session["sFileName"]);
            string sFilepath = Server.MapPath(@"~\Resources/Upload/TempBatchFiles/") + sFileName;
            StreamReader sr = new StreamReader(sFilepath);
            try
            {


                using (CsvReader ObjCSV = new CsvReader(sr, true, ','))
                {
                    string[] arrFields = ObjCSV.GetFieldHeaders();

                    for (int iRow = 0; iRow < arrFields.Count(); iRow++)
                    {
                        FileColumns.Add(arrFields[iRow]);
                    }
                    if (arrFields.Count() == 0)
                    {
                        ColumnValue = 0;
                        FileColumns.Add("");
                    }
                    else { ColumnValue = 1; }
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                sr.Close();
            }
            return ColumnValue;
        }

        #endregion

        #region ValidateFile

        public ActionResult ValidateFile(NewBatchModel ObjModel, FormCollection frm)
        {
            StringBuilder st = new StringBuilder();
            int CountInvalid = 0;
            if (ModelState.IsValid)
            {
                try
                {
                    string SheetName = ObjModel.SheetName;
                    bool chkIsBatchHeaders = ObjModel.chkIsBatchHeaders;
                    string ProductsSelected = ObjModel.ProductsSelected;
                    string sFileName = Convert.ToString(Session["sFileName"]);
                    using (DataTable dtPreviewData = new DataTable())
                    {
                        #region Get Required Columns
                        List<TempBatch> tempBatchArray = new List<TempBatch>();

                        string ProductSelected = ProductsSelected.Substring(0, (ProductsSelected.Length - 1));
                        string[] stringSeparator = new string[] { "##" };
                        List<string> ObjCollection = ProductSelected.Substring(1, (ProductSelected.Length - 1)).Split(stringSeparator, StringSplitOptions.RemoveEmptyEntries).Distinct().ToList<string>();
                        tempBatchArray = GetAllColumnsFromSelectedReportsByUser(ObjCollection, frm);

                        #endregion

                        if (!sFileName.ToLower().Contains(".csv")) //.xls and .xlsx
                        {
                            #region Read Excel
                            using (DataSet objDataSet = new DataSet())
                            {
                                using (OleDbDataAdapter objDataAdapter = new OleDbDataAdapter())
                                {
                                    string Connection = Convert.ToString(TempData["ExcelConnectionString"]);
                                    TempData["ExcelConnectionString"] = Connection;
                                    objDataAdapter.SelectCommand = GetExcelFileInformation(Connection, SheetName);
                                    objDataAdapter.Fill(objDataSet);
                                }
                            #endregion

                                #region Preview Table

                                List<string> lstExcelHeaders = new List<string>();

                                for (int i = 0; i < objDataSet.Tables[0].Columns.Count; i++)
                                {
                                    lstExcelHeaders.Add(objDataSet.Tables[0].Columns[i].Caption.Trim());
                                    dtPreviewData.Columns.Add("Column" + i.ToString(), Type.GetType("System.String"));
                                }

                                if (chkIsBatchHeaders == false)
                                {
                                    DataRow dr = dtPreviewData.NewRow();
                                    for (int iRow = 0; iRow < dtPreviewData.Columns.Count; iRow++)
                                    {
                                        dr[iRow] = objDataSet.Tables[0].Columns[iRow].Caption.Trim();

                                    }
                                    dtPreviewData.Rows.Add(dr);
                                }

                                for (int total = 0; total < objDataSet.Tables[0].Rows.Count; total++)
                                {
                                    DataRow dr = dtPreviewData.NewRow();

                                    for (int iRow = 0; iRow < dtPreviewData.Columns.Count; iRow++)
                                    {
                                        dr[iRow] = (objDataSet.Tables[0].Rows[total][lstExcelHeaders[iRow].ToString()]).ToString().Trim();
                                    }
                                    dtPreviewData.Rows.Add(dr);
                                }
                            }
                                #endregion
                        }
                        else
                        {
                            #region CSV Step 3


                            string sFilepath = Server.MapPath(@"~\Resources/Upload/TempBatchFiles/") + sFileName;
                            StreamReader sr = new StreamReader(sFilepath);
                            try
                            {
                                List<string> FileColumns = new List<string>();
                                GetCSVHeaders(FileColumns);
                                for (int i = 0; i < FileColumns.Count; i++)
                                {
                                    dtPreviewData.Columns.Add("Column" + i.ToString(), Type.GetType("System.String"));

                                }
                                string[] arrFields;
                                bool IsAddHeaders = false;

                                using (CsvReader ObjCSV = new CsvReader(sr, true, ','))
                                {
                                    arrFields = ObjCSV.GetFieldHeaders();

                                    if (chkIsBatchHeaders == false && IsAddHeaders == false)
                                    {
                                        DataRow dr = dtPreviewData.NewRow();
                                        for (int iRow = 0; iRow < arrFields.Count(); iRow++)
                                        {
                                            dr[iRow] = arrFields[iRow].ToString();
                                        }
                                        dtPreviewData.Rows.Add(dr);
                                    }

                                    while (ObjCSV.ReadNextRecord())
                                    {
                                        IsAddHeaders = true;
                                        DataRow dr = dtPreviewData.NewRow();
                                        for (int iRow = 0; iRow < dtPreviewData.Columns.Count; iRow++)
                                        {
                                            dr[iRow] = ObjCSV[iRow].Trim();
                                        }
                                        dtPreviewData.Rows.Add(dr);
                                    }
                                }


                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                            finally
                            {
                                sr.Close();
                            }
                            #endregion CSV Step 3
                        }

                        #region Result

                        DataTable dt = GetCSVPreviewAfter(dtPreviewData, tempBatchArray, frm, ref CountInvalid, ObjModel, ObjCollection);

                        st.Append("<table cellpadding='2' cellspacing='0' border='1' width='565px' style='border:1px solid black;'><tr>");
                        st.Append("<td align='center'><b>Columns</b></td>");
                        st.Append("<td  align='center'><b>Row1</b></td>");
                        st.Append("<td  align='center'><b>Row2</b></td>");
                        st.Append("<td  align='center'><b>.....</b></td>");
                        st.Append("<td  align='center'><b>Invalid cells</b></td>");
                        st.Append("<td  align='center'><b>Validate</b></td></tr>");

                        for (int irow = 0; irow < dt.Rows.Count; irow++)
                        {
                            st.Append("<tr>");
                            for (int icol = 0; icol < dt.Columns.Count; icol++)
                            {
                                st.Append("<td>" + dt.Rows[irow][icol] + "</td>");
                            }
                            st.Append("</tr>");

                        }
                        st.Append("</table>");
                        #endregion
                    }
                }
                catch //(Exception)
                {
                }
            }

            return Json(new { grid = st.ToString(), CountInvalid = CountInvalid });
        }

        protected List<TempBatch> GetAllColumnsFromSelectedReportsByUser(List<string> ProductCodeColl, FormCollection frm)
        {
            string LastName_value = frm.GetValue("LastName").AttemptedValue;
            string temp_LastName = string.Empty;
            string FirstName_value = frm.GetValue("FirstName").AttemptedValue;
            string temp_FirstName = string.Empty;
            string Social_value = frm.GetValue("Social").AttemptedValue;
            string temp_Social = string.Empty;
            string DOB_value = frm.GetValue("DOB").AttemptedValue;
            string temp_DOB = string.Empty;
            string Phone_value = frm.GetValue("Phone").AttemptedValue;
            string temp_Phone = string.Empty;
            string Address_value = frm.GetValue("Address").AttemptedValue;
            string temp_Address = string.Empty;
            string Street_value = frm.GetValue("Street").AttemptedValue;
            string temp_Street = string.Empty;
            string City_value = frm.GetValue("City").AttemptedValue;
            string temp_City = string.Empty;
            string State_value = frm.GetValue("State").AttemptedValue;
            string temp_State = string.Empty;
            string Zip_value = frm.GetValue("Zip").AttemptedValue;
            string temp_Zip = string.Empty;
            List<TempBatch> ObjCollection = new List<TempBatch>();
            try
            {
                foreach (string ProductCode1 in ProductCodeColl)
                {
                    string ProductCode = "";
                    if (ProductCode1.Contains("_"))
                    {
                        string[] Pcode = ProductCode1.Split('_');
                        ProductCode = Pcode[0].ToString();
                    }
                    XmlDocument ObjXmlDocument = new XmlDocument();
                    ObjXmlDocument.Load(Server.MapPath("~/Content/ProductXml/ProductRequireDataForBatch.xml"));


                    XmlNode ObjXmlNode = ObjXmlDocument.SelectSingleNode("/Products/Product[@Code='" + ProductCode + "']");
                    XmlNodeList xnList = ObjXmlDocument.SelectNodes("/Products/Product[@Code='" + ProductCode + "']/RequiredData");
                    for (int i = 0; i < xnList.Count; i++)
                    {
                        string ReqColumn = "";
                        ReqColumn = xnList[i].InnerXml;
                        if (ReqColumn == "LastName")
                        {
                            temp_LastName = "1";
                        }
                        if (ReqColumn == "FirstName")
                        {
                            temp_FirstName = "1";
                        }
                        if (ReqColumn == "Social")
                        {
                            temp_Social = "1";
                        }
                        if (ReqColumn == "DOB")
                        {
                            temp_DOB = "1";
                        }
                        if (ReqColumn == "Phone")
                        {
                            temp_Phone = "1";
                        }
                        if (ReqColumn == "Address")
                        {
                            temp_Address = "1";
                        }
                        if (ReqColumn == "Street")
                        {
                            temp_Street = "1";
                        }
                        if (ReqColumn == "City")
                        {
                            temp_City = "1";
                        }
                        if (ReqColumn == "State")
                        {
                            temp_State = "1";
                        }
                        if (ReqColumn == "Zip")
                        {
                            temp_Zip = "1";
                        }
                    }

                    if (ObjXmlNode != null)
                    {
                        for (int iRow = 0; iRow < ObjXmlNode.ChildNodes.Count; iRow++)
                        {
                            if (ObjCollection.Where(d => d.BatchRequiredFields == ObjXmlNode.ChildNodes[iRow].InnerText).Count() == 0)
                            {
                                ObjCollection.Add(new TempBatch { BatchRequiredFields = ObjXmlNode.ChildNodes[iRow].InnerText, CollaspeId = ObjXmlNode.ChildNodes[iRow].Attributes["Collaspe"].Value.ToString(), RegExp = ObjXmlNode.ChildNodes[iRow].Attributes["ValidationExpression"].Value.ToString() });
                            }
                        }
                    }
                }
                if (LastName_value != "-1")
                {
                    if (temp_LastName != "1")
                    {
                        ObjCollection.Add(new TempBatch { BatchRequiredFields = "LastName", CollaspeId = "ColApplicant", RegExp = @"^[a-zA-Z' ']+$" });
                    }
                }
                if (FirstName_value != "-1")
                {
                    if (temp_FirstName != "1")
                    {
                        ObjCollection.Add(new TempBatch { BatchRequiredFields = "FirstName", CollaspeId = "ColApplicant", RegExp = @"^[a-zA-Z' ']+$" });
                    }
                }
                if (Social_value != "-1")
                {
                    if (temp_Social != "1")
                    {
                        ObjCollection.Add(new TempBatch { BatchRequiredFields = "Social", CollaspeId = "ColApplicant", RegExp = @"^\d{3}-\d{2}-\d{4}$" });
                    }
                }
                if (DOB_value != "-1")
                {
                    if (temp_DOB != "1")
                    {
                        ObjCollection.Add(new TempBatch { BatchRequiredFields = "DOB", CollaspeId = "ColApplicant", RegExp = @"^(((0?[1-9]|1[012])/(0?[1-9]|1\d|2[0-8])|(0?[13456789]|1[012])/(29|30)|(0?[13578]|1[02])/31)/(19|[2-9]\d)\d{2}|0?2/29/((19|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(([2468][048]|[3579][26])00)))$" });
                    }
                }
                if (Phone_value != "-1")
                {
                    if (temp_Phone != "1")
                    {
                        ObjCollection.Add(new TempBatch { BatchRequiredFields = "Phone", CollaspeId = "ColApplicant", RegExp = @"^(\([0-9]{3}\) |\([0-9]{3}\)|[0-9]{3}-)[0-9]{3}-[0-9]{4}$" });
                    }
                } if (Address_value != "-1")
                {
                    if (temp_Address != "1")
                    {
                        ObjCollection.Add(new TempBatch { BatchRequiredFields = "Address", CollaspeId = "ColApplicant", RegExp = @"" });
                    }
                }
                if (Street_value != "-1")
                {
                    if (temp_Street != "1")
                    {
                        ObjCollection.Add(new TempBatch { BatchRequiredFields = "Street", CollaspeId = "ColApplicant", RegExp = @"" });
                    }
                }
                if (City_value != "-1")
                {
                    if (temp_City != "1")
                    {
                        ObjCollection.Add(new TempBatch { BatchRequiredFields = "City", CollaspeId = "ColApplicant", RegExp = @"^[a-zA-Z' ']+$" });
                    }
                }
                if (State_value != "-1")
                {
                    if (temp_State != "1")
                    {
                        ObjCollection.Add(new TempBatch { BatchRequiredFields = "State", CollaspeId = "ColApplicant", RegExp = @"" });
                    }
                }
                if (Zip_value != "-1")
                {
                    if (temp_Zip != "1")
                    {
                        ObjCollection.Add(new TempBatch { BatchRequiredFields = "Zip", CollaspeId = "ColApplicant", RegExp = @"^\d{5}$" });
                    }
                }
                ObjCollection.Add(new TempBatch { BatchRequiredFields = "MI", CollaspeId = "ColApplicant", RegExp = @"^[a-zA-Z' ']+$", allowEmpty = true });
                ObjCollection.Add(new TempBatch { BatchRequiredFields = "Suffix", CollaspeId = "ColApplicant", RegExp = @"^[a-zA-Z' ']+$" });
                ObjCollection.Add(new TempBatch { BatchRequiredFields = "ApplicantEmail", CollaspeId = "ColApplicant", RegExp = @"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$", allowEmpty = true });
                ObjCollection.Add(new TempBatch { BatchRequiredFields = "Direction", CollaspeId = "ColApplicant", RegExp = @"^[a-zA-Z' ']+$", allowEmpty = true });
                ObjCollection.Add(new TempBatch { BatchRequiredFields = "Type", CollaspeId = "ColApplicant", RegExp = @"^[a-zA-Z' ']+$", allowEmpty = true });
                ObjCollection.Add(new TempBatch { BatchRequiredFields = "Apt", CollaspeId = "ColApplicant", RegExp = @"", allowEmpty = true });
                ObjCollection.Add(new TempBatch { BatchRequiredFields = "AliasName", CollaspeId = "ColEmployment", RegExp = @"^[a-zA-Z' ']+$" });
                ObjCollection.Add(new TempBatch { BatchRequiredFields = "InstituteAlias", CollaspeId = "ColEducation", RegExp = @"^[a-zA-Z' ']+$" });
                ObjCollection.Add(new TempBatch { BatchRequiredFields = "ReferenceAlias", CollaspeId = "ColPersonal", RegExp = @"^[a-zA-Z' ']+$" });
                ObjCollection.Add(new TempBatch { BatchRequiredFields = "LicenseAlias", CollaspeId = "ColLicenseVerify", RegExp = @"^[a-zA-Z' ']+$" });
            }
            catch
            {

            }

            return ObjCollection;
        }

        public DataTable GetCSVPreviewAfter(DataTable dt, List<TempBatch> tempBatchArray, FormCollection frm, ref int CountInvalid, NewBatchModel ObjModel, List<string> ObjCollProducts)
        {
            //StringBuilder sHTML;
           // sHTML = new StringBuilder();
            bool Bind = true;
            bool IsRowVlaid = true;
            string CellName = "";
            string invalidColumn = "";
            int iprev = -1, piRow = -1;
            List<ApplicantInfo> APPLICANTINFOCOLL = new List<ApplicantInfo>();
            List<bool> IsAllRowValid = new List<bool>();
            List<string> invalidCells = new List<string>();
            for (int iarr = 0; iarr < tempBatchArray.Count(); iarr++)
            {
                IsAllRowValid.Add(true);
                invalidCells.Add("");
            }

            #region Preview datatable

            DataTable dtPreviewGrd = new DataTable();

            dtPreviewGrd.Columns.Add("column", Type.GetType("System.String"));
            dtPreviewGrd.Columns.Add("r1", Type.GetType("System.String"));
            dtPreviewGrd.Columns.Add("r2", Type.GetType("System.String"));
            dtPreviewGrd.Columns.Add("r3", Type.GetType("System.String"));
            dtPreviewGrd.Columns.Add("Invalid Cells", Type.GetType("System.String"));
            dtPreviewGrd.Columns.Add("Valid", Type.GetType("System.String"));

            #endregion

            #region Collection

            BALGeneral ObjBALGeneral = new BALGeneral();
            List<tblState> tblStateColl = new List<tblState>();
            List<clsCounty> tblCountyColl = new List<clsCounty>();
            List<tblJurisdiction> tblJurisdictionColl = new List<tblJurisdiction>();
            tblStateColl = ObjBALGeneral.GetStates();
            tblCountyColl = ObjBALGeneral.GetStatesCountiesAll();
            tblJurisdictionColl = ObjBALGeneral.GetJurisdictions();

            #endregion

            try
            {
                for (int iRow = 0; iRow < dt.Rows.Count; iRow++)
                {
                    #region Application Info
                    ApplicantInfo APPLICANTINFO;
                    APPLICANTINFO = new ApplicantInfo();
                    APPLICANTINFO.Applicant = new clsBatApplicant();
                    APPLICANTINFO.Automotive = new clsBatAutomotive();
                    APPLICANTINFO.Business = new clsBatBusiness();
                    APPLICANTINFO.County = new List<clsBatCounty>();
                    APPLICANTINFO.Education = new List<clsBatEducation>();
                    APPLICANTINFO.Employment = new List<clsBatEmployment>();
                    APPLICANTINFO.License = new List<clsBatLicense>();
                    APPLICANTINFO.OwnerInfo = new clsBatOwnerInfo();
                    APPLICANTINFO.Personal = new List<clsBatPersonal>();
                    APPLICANTINFO.ProductColl = new List<string>();
                    APPLICANTINFO.Tracking = new clsBatTracking();
                    APPLICANTINFO.ApplicantNumber = iRow.ToString();
                    APPLICANTINFO.IsApplicantProcessed = "0";

                    #endregion

                    #region Check Selected Reports

                    if (ObjCollProducts.Any(d => d.Contains("CCR1")) || ObjCollProducts.Any(d => d.Contains("CCR2")) || ObjCollProducts.Any(d => d.Contains("RCX")))
                    {
                        string[] CountyInfoState = frm.GetValue("CountyInfoState").AttemptedValue.Split(',');
                        for (int i = 0; i < CountyInfoState.Length; i++)
                        {
                            clsBatCounty CountySingle = new clsBatCounty();
                            APPLICANTINFO.County.Add(CountySingle);
                        }
                    }
                    if (ObjCollProducts.Any(d => d.Contains("EMV")))
                    {
                        string[] CompanyName = frm.GetValue("CompanyName").AttemptedValue.Split(',');
                        for (int i = 0; i < CompanyName.Length; i++)
                        {
                            clsBatEmployment EmploymentSingle = new clsBatEmployment();
                            APPLICANTINFO.Employment.Add(EmploymentSingle);
                        }
                    }
                    if (ObjCollProducts.Any(d => d.Contains("EDV")))
                    {
                        string[] SchoolName = frm.GetValue("SchoolName").AttemptedValue.Split(',');
                        for (int i = 0; i < SchoolName.Length; i++)
                        {
                            clsBatEducation EducationSingle = new clsBatEducation();
                            APPLICANTINFO.Education.Add(EducationSingle);
                        }
                    }
                    if (ObjCollProducts.Any(d => d.Contains("PRV")))
                    {
                        string[] ReferenceName = frm.GetValue("ReferenceName").AttemptedValue.Split(',');
                        for (int i = 0; i < ReferenceName.Length; i++)
                        {
                            clsBatPersonal PersonalSingle = new clsBatPersonal();
                            APPLICANTINFO.Personal.Add(PersonalSingle);
                        }
                    }
                    if (ObjCollProducts.Any(d => d.Contains("PLV")))
                    {
                        string[] LicenseType = frm.GetValue("LicenseType").AttemptedValue.Split(',');
                        for (int i = 0; i < LicenseType.Length; i++)
                        {
                            clsBatLicense LicenseSingle = new clsBatLicense();
                            APPLICANTINFO.License.Add(LicenseSingle);
                        }
                    }
                    if (ObjCollProducts.Any(d => d.Contains("WCV")))
                    {
                        string[] LicenseAlias = frm.GetValue("LicenseAlias").AttemptedValue.Split(',');
                        for (int i = 0; i < LicenseAlias.Length; i++)
                        {
                            clsBatLicense LicenseSingle = new clsBatLicense();
                            APPLICANTINFO.License.Add(LicenseSingle);
                        }
                    }
                    #endregion

                    for (int iarr = 0; iarr < tempBatchArray.Count(); iarr++)
                    {
                        string ddlname = tempBatchArray.ElementAt(iarr).BatchRequiredFields.ToString();
                        string PnlName = tempBatchArray.ElementAt(iarr).CollaspeId.ToString().Replace("Col", "");
                        string[] Value = frm.GetValue(ddlname).AttemptedValue.Split(',');
                        int No = 0;
                        if (Value.Length > 0)
                        {
                            for (int i = 0; i < Value.Length; i++) // Multiple check for county/employment
                            {
                                No = Convert.ToInt32(Value[i]);
                                if (No != -1)
                                {
                                    IsRowVlaid = RowValidation(tempBatchArray.ElementAt(iarr).BatchRequiredFields.ToString(), dt.Rows[iRow][No].ToString(), tempBatchArray.ElementAt(iarr).RegExp, tempBatchArray.ElementAt(iarr).allowEmpty, tblStateColl, tblCountyColl, tblJurisdictionColl);
                                    APPLICANTINFO = GetApplicantObject(APPLICANTINFO, tempBatchArray.ElementAt(iarr).BatchRequiredFields.ToString(), dt.Rows[iRow][No].ToString(), i, PnlName);
                                    if (IsRowVlaid && IsAllRowValid[iarr])
                                    {
                                        CellName = "<img src='../Content/themes/base/images/successmsg.png'/>";
                                        invalidColumn = invalidCells[iarr];
                                    }
                                    else
                                    {
                                        if (!IsRowVlaid)
                                        {
                                            invalidCells[iarr] = invalidCells[iarr] == "" ? GetCellAddress(iRow, No) : invalidCells[iarr] + "," + GetCellAddress(iRow, No);
                                        }
                                        IsAllRowValid[iarr] = false;
                                        CountInvalid++;
                                        invalidColumn = invalidCells[iarr];
                                        Bind = false;
                                        CellName = "<img src='../Content/themes/base/images/publish_x.png'/>";  // GetCellAddress(iRow, No);
                                    }
                                    Logic4Preview(dt, dtPreviewGrd, ddlname, ref iprev, ref piRow, iRow, No, CellName, invalidColumn);
                                }
                            }
                        }
                    }

                    if (APPLICANTINFO != null)
                    {
                        APPLICANTINFO.ProductColl = ObjCollProducts;
                        APPLICANTINFO.OwnerInfo.CompanyId = ObjModel.pkCompanyId;
                        APPLICANTINFO.OwnerInfo.CompanyUserId = ObjModel.pkCompanyUserId;
                        APPLICANTINFO.OwnerInfo.LocationId = ObjModel.fkLocationId;
                        APPLICANTINFO.OwnerInfo.PermissiblePurpose = ObjModel.PermissiblePurposeValue;
                        APPLICANTINFO.OwnerInfo.OrderCompanyName = Convert.ToString(ObjModel.OrderCompanyName);
                        APPLICANTINFO.OwnerInfo.CompanyAccountNo = Convert.ToString(ObjModel.CompanyAccountNo);
                        APPLICANTINFOCOLL.Add(APPLICANTINFO);
                    }
                }

                if (Bind)
                {
                    Session["pkCompanyUserId"] = ObjModel.pkCompanyUserId;
                    Session["APPLICANTINFOCOLL"] = APPLICANTINFOCOLL;
                    string SelectedProducts = string.Empty;
                    for (int i = 0; i < ObjCollProducts.Count(); i++)
                    {
                        string[] Pcode = ObjCollProducts.ElementAt(i).Split('_');
                        string SingleProducts = Pcode[0].ToString();
                        if (i == 0)
                        {
                            SelectedProducts += SingleProducts;
                        }
                        else
                        {
                            SelectedProducts += "," + SingleProducts;
                        }
                        SingleProducts = string.Empty;
                    }
                    Session["SelectedProducts"] = SelectedProducts;
                    //XmlSerializer serializer = new XmlSerializer(typeof(List<ApplicantInfo>));
                    //string sCurrentUserXMLFile = GetUserId() + "_" + DateTime.Now.ToFileTime().ToString() + ".xml";
                    //// ViewState["UserBatchXML"] = sCurrentUserXMLFile;
                    //TextWriter tw = new StreamWriter(Server.MapPath(@"~\Resources\Upload\TempBatchFiles\" + sCurrentUserXMLFile));
                    //serializer.Serialize(tw, APPLICANTINFOCOLL);
                    //tw.Close();
                    //string SelectedProducts = string.Empty;
                    //for (int i = 0; i < ObjCollProducts.Count(); i++)
                    //{
                    //    string[] Pcode = ObjCollProducts.ElementAt(i).Split('_');
                    //   string SingleProducts = Pcode[0].ToString();
                    //   if (i == 0)
                    //   {
                    //       SelectedProducts += SingleProducts;
                    //   }
                    //   else
                    //   {
                    //       SelectedProducts += "," + SingleProducts ;
                    //   }
                    //   SingleProducts = string.Empty;
                    //}
                    //BALBatch ObjBALBatch = new BALBatch();
                    //tblBatchOrder objtblBatchOrder = new tblBatchOrder();
                    //objtblBatchOrder.BatchFileName = sCurrentUserXMLFile;
                    //objtblBatchOrder.CreateDate = DateTime.Now;

                    //objtblBatchOrder.fkCompanyUserId = ObjModel.pkCompanyUserId;
                    //objtblBatchOrder.IsProcessed = false;
                    //objtblBatchOrder.UplodedBatchFileName = Convert.ToString(Session["sFileName"]);
                    //objtblBatchOrder.DataSource = SelectedProducts;

                    //if (ObjBALBatch.InsertBatchOrderFile(objtblBatchOrder) == 1)
                    //{
                    //    ///
                    //    /// Logic to delete csv file from website folder.
                    //    ///
                    //    string sFileName = Convert.ToString(Session["sFileName"]);
                    //    //if (sFileName != string.Empty)
                    //    //{
                    //    //    string sFilepath = Server.MapPath(@"~\Resources/Upload/TempBatchFiles/") + sFileName;
                    //    //    if (System.IO.File.Exists(sFilepath))
                    //    //    {
                    //    //        try { System.IO.File.Delete(sFilepath); }
                    //    //        catch { }
                    //    //    }
                    //    //}
                    //}
                }
            }
            catch //(Exception)
            {

            }
            return dtPreviewGrd;
        }

        public string GetCellAddress(int row, int col)
        {
            string[] CharArray = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            string strCellPosition = string.Empty;
            if (col < 26)
            {
                if (Session["chkIsBatchHeaders"].ToString().ToLower() == "true")
                    strCellPosition = CharArray[col].ToString() + (row + 2).ToString();
                else
                    strCellPosition = CharArray[col].ToString() + (row + 1).ToString();
            }
            else
            {
                if (Session["chkIsBatchHeaders"].ToString().ToLower() == "true")
                    strCellPosition = CharArray[(col / 26) - 1].ToString() + CharArray[col % 26].ToString() + (row + 2).ToString();
                else
                    strCellPosition = CharArray[(col / 26) - 1].ToString() + CharArray[col % 26].ToString() + (row + 1).ToString();
            }

            return strCellPosition;
        }

        public string GetUserId()
        {
            MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
            return (ObjMembershipUser.ProviderUserKey.ToString());
        }

        private void Logic4Preview(DataTable dt, DataTable dtPreviewGrd, string ddlname, ref int iprev, ref int piRow, int iRow, int d, string CellName, string invalidColumn)
        {
            if (iRow == 0)
            {
                DataRow dr = dtPreviewGrd.NewRow();
                dr["column"] = ddlname;
                dr["r1"] = dt.Rows[iRow][d].ToString();
                dr["r2"] = "";
                dr["r3"] = "";
                dr["Invalid Cells"] = (invalidColumn != "") ? invalidColumn : "";
                dr["Valid"] = (CellName != "") ? CellName : "";
                dtPreviewGrd.Rows.Add(dr);
            }
            else
            {
                if (piRow != iRow)
                {
                    iprev = 0;
                }
                else
                {
                    iprev++;
                }
                int iiRow = iRow + 1;
                if (iRow < 3)
                {
                    try
                    {
                        dtPreviewGrd.Rows[iprev][iiRow] = dt.Rows[iRow][d].ToString();
                        //string sOldCols = dtPreviewGrd.Rows[iprev]["Valid"].ToString();
                        if (CellName != "")
                        {
                            dtPreviewGrd.Rows[iprev]["Valid"] = CellName;// sOldCols + CellName + ", ";
                            dtPreviewGrd.Rows[iprev]["Invalid Cells"] = invalidColumn;
                        }
                        if (iRow == 2)
                        {
                            dtPreviewGrd.Rows[iprev][iiRow] = ".....";
                        }
                    }
                    catch { }
                }
                else
                {
                    // string sOldCols = dtPreviewGrd.Rows[iprev]["Valid"].ToString();
                    if (CellName != "")
                    {
                        // dtPreviewGrd.Rows[iprev]["Valid"] = sOldCols + CellName + ", ";
                        dtPreviewGrd.Rows[iprev]["Valid"] = CellName;
                        dtPreviewGrd.Rows[iprev]["Invalid Cells"] = invalidColumn;
                    }
                }
                piRow = iRow;
            }
        }

        public ApplicantInfo GetApplicantObject(ApplicantInfo APPLICANTINFO, string PropName, string PropVal, int ObjNO, string PnlName)
        {

            Cell2Property(APPLICANTINFO.Applicant, PropName, PropVal);
            Cell2Property(APPLICANTINFO.Automotive, PropName, PropVal);
            Cell2Property(APPLICANTINFO.Business, PropName, PropVal);
            switch (PnlName)
            {
                case "County":
                    //if (APPLICANTINFO.County.Count >= ObjNO)
                    //{
                    //    if (APPLICANTINFO.County.Count != 0)
                    Cell2Property(APPLICANTINFO.County.ElementAt(ObjNO), PropName, PropVal);
                    //}
                    break;
                case "Employment":
                    //if (APPLICANTINFO.Employment.Count >= ObjNO)
                    //{
                    //    if (APPLICANTINFO.Employment.Count != 0)
                    Cell2Property(APPLICANTINFO.Employment.ElementAt(ObjNO), PropName, PropVal);
                    //}
                    break;
                case "Education":
                    //if (APPLICANTINFO.Education.Count >= ObjNO)
                    //{
                    //    if (APPLICANTINFO.Education.Count != 0)
                    Cell2Property(APPLICANTINFO.Education.ElementAt(ObjNO), PropName, PropVal);
                    //}
                    break;
                case "Personal":
                    //if (APPLICANTINFO.Personal.Count >= ObjNO)
                    //{
                    //    if (APPLICANTINFO.Personal.Count != 0)
                    Cell2Property(APPLICANTINFO.Personal.ElementAt(ObjNO), PropName, PropVal);
                    //}
                    break;
                case "LicenseVerify":
                    //if (APPLICANTINFO.License.Count >= ObjNO)
                    //{
                    //    if (APPLICANTINFO.License.Count != 0)
                    Cell2Property(APPLICANTINFO.License.ElementAt(ObjNO), PropName, PropVal);
                    //}
                    break;
                default:
                    break;
            }
            Cell2Property(APPLICANTINFO.Tracking, PropName, PropVal);
            return APPLICANTINFO;
        }

        public void Cell2Property(object myObject, string ProertyColl, string PropertyValue)
        {
            foreach (var propertyInfo in myObject.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(string))
                {
                    if (propertyInfo.GetValue(myObject, null) == null)
                    {
                        if (ProertyColl.Contains(propertyInfo.Name))
                            propertyInfo.SetValue(myObject, PropertyValue.ToString(), null);
                    }
                }
            }
        }

        private bool RowValidation(string ColumnName, string CellData, string Expr, bool? allowEmpty, List<tblState> StateColl, List<clsCounty> CountyColl, List<tblJurisdiction> JurisdictionColl)
        {
            bool IsCellValid = false;

            if (ColumnName == "DOB")
            {
                try
                {
                    CellData = Convert.ToDateTime(CellData).ToShortDateString();
                }
                catch (Exception)
                {

                }
            }
            if (Expr != "")
            {
                if (allowEmpty != true)
                {
                    Regex reg = new Regex(Expr);
                    IsCellValid = (reg.IsMatch(CellData)) ? true : false;
                }
                else
                {
                    if (CellData.Trim() != "")
                    {
                        Regex reg = new Regex(Expr);
                        IsCellValid = (reg.IsMatch(CellData)) ? true : false;
                    }
                    else
                    {
                        IsCellValid = true;
                    }
                }
            }
            else { IsCellValid = true; }
            if (ColumnName.ToLower().Contains("state"))
            {
                IsCellValid = (StateColl.Where(d => d.StateName.ToLower() == CellData.ToLower()).Count() > 0) ? true : false;
                IsCellValid = IsCellValid == false ? (StateColl.Where(d => d.StateCode.ToLower() == CellData.ToLower()).Count() > 0) ? true : false : IsCellValid;
            }
            else if (ColumnName.ToLower().Contains("county"))
            {
                IsCellValid = (CountyColl.Where(d => d.CountyName.ToLower() == CellData.ToLower()).Count() > 0) ? true : false;
            }
            else if (ColumnName.ToLower().Contains("jurisdiction"))
            {
                IsCellValid = (JurisdictionColl.Where(d => d.JurisdictionName.ToLower() == CellData.ToLower()).Count() > 0) ? true : false;
            }
            //else if (ColumnName.ToLower().Contains("applicant email"))
            //{
            //    Regex reg = new Regex(Expr);
            //    IsCellValid = (reg.IsMatch(@"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$")) ? true : false;
            //}
            return IsCellValid;
        }

        public ActionResult AddBatchFileinDB()
        {
            int Status = 0;
            try
            {
                var APPLICANTINFOCOLL = (List<ApplicantInfo>)Session["APPLICANTINFOCOLL"];
                XmlSerializer serializer = new XmlSerializer(typeof(List<ApplicantInfo>));
                string sCurrentUserXMLFile = GetUserId() + "_" + DateTime.Now.ToFileTime().ToString() + ".xml";
                // ViewState["UserBatchXML"] = sCurrentUserXMLFile;
                using (TextWriter tw = new StreamWriter(Server.MapPath(@"~\Resources\Upload\TempBatchFiles\" + sCurrentUserXMLFile)))
                {
                    serializer.Serialize(tw, APPLICANTINFOCOLL);
                    tw.Close();
                }
                string SelectedProducts = Session["SelectedProducts"].ToString();
                BALBatch ObjBALBatch = new BALBatch();
                tblBatchOrder objtblBatchOrder = new tblBatchOrder();
                objtblBatchOrder.BatchFileName = sCurrentUserXMLFile;
                objtblBatchOrder.CreateDate = DateTime.Now;
                int pkCompanyUserId = Int32.Parse(Session["pkCompanyUserId"].ToString());
                objtblBatchOrder.fkCompanyUserId = pkCompanyUserId;
                objtblBatchOrder.IsProcessed = false;
                objtblBatchOrder.UplodedBatchFileName = Convert.ToString(Session["sFileName"]);
                objtblBatchOrder.DataSource = SelectedProducts;

                if (ObjBALBatch.InsertBatchOrderFile(objtblBatchOrder) == 1)
                {
                    ///
                    /// Logic to delete csv file from website folder.
                    ///
                  //  string sFileName = Convert.ToString(Session["sFileName"]);
                    Status = 1;
                    //if (sFileName != string.Empty)
                    //{
                    //    string sFilepath = Server.MapPath(@"~\Resources/Upload/TempBatchFiles/") + sFileName;
                    //    if (System.IO.File.Exists(sFilepath))
                    //    {
                    //        try { System.IO.File.Delete(sFilepath); }
                    //        catch { }
                    //    }
                    //}
                }
                else
                { Status = -1; }
            }
            catch //(Exception ex)
            {
            }
            return Json(new { Status = Status });
        }

        #endregion

        public FilePathResult DownloadBatchSample()
        {
            string FilePath = Server.MapPath("~/Resources/Upload/TempBatchFiles/BatchSample.csv");
            string fileext = System.IO.Path.GetExtension(FilePath);
            string CType = "application/" + fileext.Replace(".", "");
            string fileName = Path.GetFileName(FilePath);
            return File(FilePath, CType, fileName);
        }
    }
}
