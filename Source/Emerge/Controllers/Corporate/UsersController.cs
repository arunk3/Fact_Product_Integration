﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Services;
using Emerge.Data;
using Emerge.Models;
using Emerge.Common;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace Emerge.Controllers.Corporate
{
    public partial class CorporateController : BaseEmergeController
    {
        //
        // GET: /Users/
        #region Get Users
        public ActionResult GetUsers()
        {
            UsersModel ObjModel = new UsersModel();            
            List<Proc_Get_UserInfoByMemberShipUserIdResult> UserDetail = GetCurrentUserDetail();

            ObjModel.PkCompanyId = UserDetail.First().pkCompanyId;
            ObjModel.CurrentUserLocationId = UserDetail.First().pkLocationId;
            ObjModel.CurrentUserRole = UserDetail.First().RoleName;
            ObjModel.CurrentUserEmail = UserDetail.First().UserEmail;
            return View(ObjModel);
        }
        /// <summary>
        /// Function To Get Loged in User Details
        /// </summary>
        private List<Proc_Get_UserInfoByMemberShipUserIdResult> GetCurrentUserDetail()
        {
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjDbCollection;
            try
            {

                Guid UserId = Utility.GetUID(User.Identity.Name);
                ObjDbCollection = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(UserId);
            }
            finally
            {
                ObjBALCompanyUsers = null;
            }
            return ObjDbCollection;
        }

       // int PageSize = 25;      

        public ActionResult GetUserUnderBranch(int page, int PageSize, string PkLocation)
        {
            int totalCount = 0;
            ObjBALCompanyUsers = new BALCompanyUsers();
            List<Proc_Get_UsersUnderLocationResult> CollGetUsersUnderLocation = new List<Proc_Get_UsersUnderLocationResult>();
            try
            {
                CollGetUsersUnderLocation = ObjBALCompanyUsers.GetUsersUnderLocation(1, PageSize, true, "", Convert.ToInt32(PkLocation)).Select(p => new Proc_Get_UsersUnderLocationResult
                {
                    pkCompanyUserId = p.pkCompanyUserId,
                    FirstName = p.LastName + " , " + p.FirstName,
                    IsEnabledUser = p.IsEnabledUser,
                    StateCode = p.StateCode,
                    UserName = p.UserName,
                    CompanyType = p.CompanyType,
                    CompanyName = p.CompanyName,
                    City = p.City + " , " + p.StateCode,
                    Email = p.Email,
                    LastName = p.LastName,
                    fkUserId = p.fkUserId,
                    RoleName = p.RoleName,
                    Row_Num=p.Row_Num
                }).ToList();
                totalCount = CollGetUsersUnderLocation.Count();
                CollGetUsersUnderLocation = (from user in CollGetUsersUnderLocation
                                           where user.Row_Num >= ((page - 1) * PageSize) + 1 && user.Row_Num <= (page * PageSize)
                                           select user).ToList();
            }
            catch (Exception)
            {

            }
            finally
            {
                ObjBALCompanyUsers = null;
            }
            return Json(new { Users = CollGetUsersUnderLocation, TotalCount = totalCount });
        }

        public ActionResult GetUserUnderCorporate(int page, int PageSize, string PkCompanyId)
        {
            int totalCount = 0;
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            List<Proc_Get_UsersUnderCompanyResult> lstGetUsersUnderCompany = new List<Proc_Get_UsersUnderCompanyResult>();
            try
            {
                lstGetUsersUnderCompany = ObjBALCompanyUsers.GetUsersUnderCompany(page, PageSize, false, "", Convert.ToInt32(PkCompanyId)).Where(data => data.RoleName != "System Admin").Select((p, index) => new Proc_Get_UsersUnderCompanyResult
                {
                    Row_Num = index + 1,
                    pkCompanyUserId = p.pkCompanyUserId,
                    FirstName = p.LastName + " , " + p.FirstName,
                    IsEnabledUser = p.IsEnabledUser,
                    StateCode = p.StateCode,
                    UserName = p.UserName,
                    CompanyType = p.CompanyType,
                    CompanyName = p.CompanyName,
                    City = p.City + " , " + p.StateCode,
                    Email = p.Email,
                    LastName = p.LastName,
                    fkUserId = p.fkUserId,
                    RoleName = p.RoleName
                }).ToList();

                totalCount = lstGetUsersUnderCompany.Count();
                lstGetUsersUnderCompany = (from user in lstGetUsersUnderCompany
                                              where user.Row_Num >=((page - 1) * PageSize) + 1 && user.Row_Num <= (page * PageSize)
                                              select user).ToList();
             
                
            }
            catch (Exception)
            {

            }
            finally
            {
                ObjBALCompanyUsers = null;
            }
            return Json(new { Users = lstGetUsersUnderCompany, TotalCount = totalCount });
        }

        #endregion Get Users

        #region Edit Company User

        BALCompanyUsers ObjBALCompanyUsers = null;
        BALLocation ObjBALLocation = null;
        private int CompanyUserId = 0;

        [ValidateInput(false)]

        public ActionResult EditCompanyUser()
        {
            UsersModel ObjModel = new UsersModel();

            ViewBag.Message = string.Empty;

            if (!string.IsNullOrEmpty(Request.QueryString["_a"]))         //  _a Contains PkUser ID
            {
                int ObjGuid = Convert.ToInt32(Request.QueryString["_a"].ToString());

                //To check user company if any user placed other id in URL.
                if (Session["Admin"] != null)
                {
                    string role = Session["Admin"].ToString();
                    if (role == "SystemAdmin" || role == "SupportManager" || role == "SupportTechnician")
                    {
                        //Pass if logged on user is Admin
                    }
                    else
                    {
                        string cmpId = Convert.ToString(Session["pkCompanyId"]);
                        if (!new BALCompanyUsers().CheckCompanyByUserId(ObjGuid, Convert.ToInt32(cmpId)))
                        {
                            return RedirectToAction("Index", "Home");
                        }
                    }
                }
                //To fetch the user details.
                FetchUserDetails(ObjGuid, ObjModel);
            }
            if (TempData["message"] != null)
            {
                ObjModel.strResult = TempData["message"].ToString();
            }
 
            ObjModel.CompanyUserId = GetCompanyUserId();

            return View(ObjModel);
        }
        /// <summary>
        /// Function To Get User User Id OF Logged in Person 
        /// </summary>
        private int GetCompanyUserId()
        {
            ObjBALCompanyUsers = new BALCompanyUsers();
            try
            {
                Guid UserId = Utility.GetUID(User.Identity.Name);
                var dbCollection = ObjBALCompanyUsers.GetUserInformationByMembershipUserId(UserId);
                if (dbCollection.Count > 0)
                {
                    CompanyUserId = dbCollection.First().pkCompanyUserId;
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                ObjBALCompanyUsers = null;
            }
            return CompanyUserId;
        }
        public UsersModel FetchUserDetails(int PkID, UsersModel ObjModel)
        {
            ObjBALCompanyUsers = new BALCompanyUsers();
            tblCompanyUser ObjtblCompanyUser = ObjBALCompanyUsers.GetCompanyUserByCompanyUserId(PkID);            
            try
            {
                ObjModel = BindFields(ObjtblCompanyUser, ObjModel);
            }
            catch (Exception)
            {

            }
            finally
            {
                ObjtblCompanyUser = null;
                ObjBALCompanyUsers = null;
            }
            return ObjModel;
        }
        /// <summary>
        /// Function To Get Company Id If LocationId is Passed 
        /// </summary>
        /// <param name="PkLocationId"></param>
        /// <returns></returns>
        public int GetCompanyIdByLocationId(int PkLocationId)
        {
            BALCompany ObjBALCompany = new BALCompany();
            int CompanyId = 0;
            try
            {
                List<Proc_Get_CompanyDetailByLocationIdResult> ObjCompanyDetailByLocationId = ObjBALCompany.GetCompanyDetailByLocationId(PkLocationId);
                CompanyId = ObjCompanyDetailByLocationId.First().pkCompanyId;
            }
            catch (Exception)
            {

            }
            finally
            {
                ObjBALCompany = null;
            }
            return CompanyId;
        }
        public UsersModel BindFields(tblCompanyUser ObjtblCompanyUser, UsersModel ObjModel)
        {
            #region Membership Info

            MembershipUser ObjMembershipUser = Membership.GetUser(ObjtblCompanyUser.fkUserId);
            ObjModel.fkUserId = ObjtblCompanyUser.fkUserId;
            string[] ObjRoles = Roles.GetRolesForUser(ObjMembershipUser.UserName);

            ObjModel.LastLoginByUser = ObjMembershipUser.LastLoginDate.ToString();

            ObjModel.UserEmail = ObjMembershipUser.UserName;

            string Password = string.Empty;
            Password = ObjMembershipUser.GetPassword();
            ObjModel.UserPassword = Password;
            //this.txtPassword.Attributes.Add("value", Password);

            #endregion

            #region User Personal Info

            //trLastLogin.Visible = true;
           // trUserImage.Visible = true;

            ObjModel.FirstName = ObjtblCompanyUser.FirstName;
            ObjModel.LastName = ObjtblCompanyUser.LastName;

           // ObjModel.CollCompany = BindCompany();

            int CompanyId = GetCompanyIdByLocationId((int)ObjtblCompanyUser.fkLocationId);
           // ObjModel.CollLocations = BindLocation(CompanyId);

            if (CompanyId == 9514)
            {
              //  ObjModel.CollRoles = BindStatus(true, true);                
            }
            else
            {
               // ObjModel.CollRoles = BindStatus(false);
            }
            try
            {
                ObjModel.RoleId = GetRoleId(ObjRoles);
            }
            catch { }
         
            ObjModel.PkCompanyId = CompanyId;
            ObjModel.fkLocationId = ObjtblCompanyUser.fkLocationId;

            if (ObjModel.CollStates.Count == 0)
            {
             //   ObjModel.CollStates = BindState();
            }

            if (ObjtblCompanyUser.fkStateId != null)
            {
                ObjModel.fkStateId = ObjtblCompanyUser.fkStateId;
            }


            int StateId;
            int.TryParse(ObjModel.fkStateId.ToString(), out StateId);

          //  ObjModel.CollCounties = LoadStateCounties(StateId);

             ObjModel.fkCountyId = ObjtblCompanyUser.fkCountyId;

            ObjModel.UserAddressLine1 = ObjtblCompanyUser.UserAddressLine1;
            ObjModel.UserAddressLine2 = ObjtblCompanyUser.UserAddressLine2;
            ObjModel.PhoneNo = ObjtblCompanyUser.PhoneNo;
            ObjModel.ZipCode = ObjtblCompanyUser.ZipCode;

            ObjModel.IsEnabled = ObjtblCompanyUser.IsEnabled;

            #region User Image

            if (ObjtblCompanyUser.UserImage == string.Empty || !System.IO.File.Exists(Server.MapPath(@"~\Resources\Upload\Images\" + ObjtblCompanyUser.UserImage)))
            {
                ObjModel.ViewImage = ApplicationPath.GetApplicationPath() + @"Content/themes/base/images/NotAvail.png";             
            }
            else
            {
                ObjModel.OldUserImage = ObjtblCompanyUser.UserImage;
                ObjModel.ViewImage = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjtblCompanyUser.UserImage; 
                
            }

            #endregion

            #endregion

            #region ViewState Info

            ObjModel.pkCompanyUserId = ObjtblCompanyUser.pkCompanyUserId;
            ObjModel.fkUserId = ObjtblCompanyUser.fkUserId;
            ObjModel.OldUserPassword = Password;
            ObjModel.OldUserEmail = ObjMembershipUser.UserName.Trim();

            #endregion
            
            return ObjModel;

        }
        /// <summary>
        /// This method is used to load counties according to state
        /// </summary>
        /// <param name="StateId"></param>
        public List<clsCounty> LoadStateCounties(int StateId)
        {
            BALGeneral ObjBALGeneral = new BALGeneral();
            List<clsCounty> ObjCollection = ObjBALGeneral.GetStatesCounties(StateId);
            ObjCollection.Insert(0, new clsCounty
            {
                pkCountyId = "-1",
                CountyName = "Select"               
            });
           
            return ObjCollection;
        }
        /// <summary>
        /// Function To Bind the States With Dropdownlist
        /// </summary>
        public List<tblLocation> BindLocation(int CompanyId)
        {
            ObjBALLocation = new BALLocation();
            List<tblLocation> lstLocations = new List<tblLocation>();
            try
            {
                lstLocations.Clear();
               
                lstLocations = ObjBALLocation.GetLocationsByCompanyId(CompanyId);
                lstLocations.Insert(0, new tblLocation
                {
                    pkLocationId = 0,
                    City = "Select"
                });               
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ObjBALLocation = null;
            }
            return lstLocations;
        }
        /// <summary>
        /// Function To bind the Company DropDownlist
        /// </summary>
        public List<CompanyList> BindCompany()
        {
            List<CompanyList> lstCompany = new List<CompanyList>();
            ObjBALCompany = new BALCompany();
            try
            {
                lstCompany = ObjBALCompany.GetAllCompany(Utility.SiteApplicationId, true, false);               
                lstCompany.Insert(0, new CompanyList
                {
                    pkCompanyId = 0,
                    CompanyName = "Select"
                }); 
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ObjBALCompany = null;
            }
            return lstCompany;
        }

        public ActionResult BindStatusForEditUsers(int PkCompanyId)
        { 
            List<aspnet_Role> allroles = new List<aspnet_Role>();
            if (PkCompanyId == 9514)
            {
                allroles = BindStatus(true, true);                
            }
            else
            {
                allroles = BindStatus(false);
            }
            return Json(allroles);
        }

        /// <summary>
        /// Function To Bind the status Field 
        /// </summary>
        /// 
        public List<aspnet_Role> BindStatus(bool IsAdmin, bool IsUsaIntelCompany)
        {
            ObjBALGeneral = new BALGeneral();
            List<aspnet_Role> RolesColl = new List<aspnet_Role>();

            try
            {

                RolesColl = ObjBALGeneral.FetchRolesAll(Utility.SiteApplicationId);
                if (RolesColl.Count != 0)
                {
                    //This is false means selected company from dropdown list is not INTELIFI
                    //Then remove following role from Dropdown list
                    if (IsUsaIntelCompany == false)
                    {
                        RolesColl = RolesColl.Where(d => d.RoleName != "SalesRep" &&
                                         d.RoleName != "SupportManager" &&
                                         d.RoleName != "SupportTechnician" &&
                                         d.RoleName != "SalesAdmin" &&
                                         d.RoleName != "SalesManager" &&
                            //d.RoleName != "Admin" &&
                                         d.RoleName != "SystemAdmin").Select(p => new aspnet_Role
                                         {
                                             RoleId = p.RoleId,
                                             Description = p.Description

                                         }).ToList();
                    }
                    else
                    {
                        RolesColl = RolesColl.Where(d => d.RoleName != "SalesRep" &&
                                         d.RoleName != "SalesAdmin" &&
                                         d.RoleName != "BasicUser" &&
                                         d.RoleName != "CorporateManager" &&
                                         d.RoleName != "BranchManager" &&
                                         d.RoleName != "SalesManager" &&
                                         d.RoleName != "DataEntry").Select(p => new aspnet_Role
                                         {
                                             RoleId = p.RoleId,
                                             Description = p.Description

                                         }).ToList();

                    }
                 
                    //RolesColl.Insert(0, new aspnet_Role
                    //{
                    //    RoleId = Guid.Empty,
                    //    Description = "Select"
                    //});                    
                  
                }
                else
                {
                  
                }
            }
            catch
            {

            }
            finally
            {
                ObjBALGeneral = null;
            }
            return RolesColl;
        }
        private List<aspnet_Role> BindStatus(bool IsRootCompany)
        {
            ObjBALGeneral = new BALGeneral();
            List<aspnet_Role> allroles = new List<aspnet_Role>();
            try
            {
                //Guid UserId = Utility.GetUID(User.Identity.Name);
               
                //MembershipUser ObjMembershipUser = Membership.GetUser(UserId);

              
                if (IsRootCompany)
                {
                    allroles = ObjBALGeneral.FetchRolesExceptAdmin(Utility.SiteApplicationId).Where(d => d.RoleName != "SalesRep" &&
                                           d.RoleName != "BasicUser" &&
                                           d.RoleName != "CorporateManager" &&
                                           d.RoleName != "BranchManager" &&
                                        d.RoleName != "SalesManager" &&
                                        d.RoleName != "DataEntry").Select(p => new aspnet_Role
                                        {
                                            RoleId = p.RoleId,
                                            Description = p.Description

                                        }).ToList();
                }
                else
                {
                    allroles = ObjBALGeneral.FetchRolesExceptAdmin(Utility.SiteApplicationId).Where(d => d.RoleName != "SalesRep" &&
                                         d.RoleName != "SupportManager" &&
                                         d.RoleName != "SupportTechnician" &&
                                         d.RoleName != "SalesAdmin" &&
                                         d.RoleName != "SalesManager" &&
                        //d.RoleName != "Admin" &&
                                         d.RoleName != "SystemAdmin").Select(p => new aspnet_Role
                                         {
                                             RoleId = p.RoleId,
                                             Description = p.Description

                                         }).ToList();
                }
                allroles.Insert(0, new aspnet_Role
                {
                    RoleId = Guid.Empty,
                    Description = "Select"
                });                 
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ObjBALGeneral = null;
            }
            return allroles;
        }
        /// <summary>
        /// Function To Bind the States With Dropdownlist
        /// </summary>
        public List<tblState> BindState()
        {
            ObjBALGeneral = new BALGeneral();
            List<tblState> States = new List<tblState>();
            try
            {
                States = ObjBALGeneral.GetStates();
                States.Insert(0, new tblState
                {
                    pkStateId = -1,
                    StateName = "Select"
                });                 
            }
            catch (Exception)
            {

            }
            finally
            {
                ObjBALGeneral = null;
            }
            return States;
        }
        /// <summary>
        /// Function to Get Role Id According to Role Name 
        /// </summary>
        /// <param name="ObjRoles"></param>
        /// <returns></returns>
        private Guid GetRoleId(string[] ObjRoles)
        {
            BALGeneral ObjBALGeneral = new BALGeneral();
            Guid StatusId = Guid.Empty;
            try
            {
                List<aspnet_Role> Objtblaspnet_Role = null;
                if (ObjRoles.Contains("SystemAdmin"))
                {

                    Objtblaspnet_Role = ObjBALGeneral.FetchRoles(Utility.SiteApplicationId);
                }
                else
                {
                    Objtblaspnet_Role = ObjBALGeneral.FetchRolesExceptAdmin(Utility.SiteApplicationId);
                }

                foreach (aspnet_Role ObjSingle in Objtblaspnet_Role)
                {
                    if (ObjRoles[0].ToString() == ObjSingle.RoleName)
                    {
                        StatusId = ObjSingle.RoleId;
                        break;
                    }
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                ObjBALGeneral = null;
            }
            return StatusId;
        }


        #region Save User Details
         [ValidateInput(false)]
        public ActionResult SaveUserDetailsById(UsersModel ObjModel, HttpPostedFileBase UserImage)
        {
            string strResult = string.Empty;


            string OldUserName = ObjModel.OldUserEmail;
            string NewUserName = ObjModel.UserEmail;
            bool bResult = IsExistsUsername(OldUserName, NewUserName);

            if (bResult)
            {
                strResult = "<span class='errormsg'>This Username already exists</span>";
                TempData["message"] = strResult;
                return RedirectToAction("EditCompanyUser", new { _a = ObjModel.pkCompanyUserId });               
            }

            strResult = UpdateUserInformationinDB(ObjModel, UserImage);           
            TempData["message"] = strResult;

            return RedirectToAction("EditCompanyUser", new { _a = ObjModel.pkCompanyUserId });          
        }
        /// <summary>
        /// Function To Update User 
        /// </summary>
        public string UpdateUserInformationinDB(UsersModel ObjModel, HttpPostedFileBase UserImage)
        {
            string strResult = string.Empty;
            tblCompanyUser ObjtblCompanyUser = new tblCompanyUser();
            ObjBALCompanyUsers = new BALCompanyUsers();
            BLLMembership ObjBLLMembership = new BLLMembership();

            try
            {
                ObjtblCompanyUser.fkUserId = ObjModel.fkUserId;
                ObjtblCompanyUser.pkCompanyUserId = ObjModel.pkCompanyUserId;
                ObjtblCompanyUser.fkLocationId = ObjModel.fkLocationId;
                ObjtblCompanyUser.FirstName = ObjModel.FirstName;
                ObjtblCompanyUser.LastName = ObjModel.LastName;
                ObjtblCompanyUser.Initials = string.Empty;
                ObjtblCompanyUser.UserAddressLine1 = !string.IsNullOrEmpty(ObjModel.UserAddressLine1) ? ObjModel.UserAddressLine1 : string.Empty;
                ObjtblCompanyUser.UserAddressLine2 = !string.IsNullOrEmpty(ObjModel.UserAddressLine2) ? ObjModel.UserAddressLine2 : string.Empty;
                ObjtblCompanyUser.PhoneNo = !string.IsNullOrEmpty(ObjModel.PhoneNo) ? ObjModel.PhoneNo : string.Empty;
                ObjtblCompanyUser.ZipCode = !string.IsNullOrEmpty(ObjModel.ZipCode) ? ObjModel.ZipCode : string.Empty;
                ObjtblCompanyUser.fkStateId = ObjModel.fkStateId;

                if (Convert.ToString(ObjModel.fkCountyId) != "-1")
                {
                    ObjtblCompanyUser.fkCountyId = ObjModel.fkCountyId;
                }
                else
                {
                    ObjtblCompanyUser.fkCountyId = null;
                }

                ObjtblCompanyUser.IsEnabled = true;
                ObjtblCompanyUser.LastModifiedById = ObjModel.CompanyUserId;
                ObjtblCompanyUser.LastModifiedDate = System.DateTime.Now;

                ObjtblCompanyUser.UserImage = string.Empty;

                if (UserImage != null)
                {
                    if (UserImage != null && UserImage.ContentLength > 0)
                    {
                        if (ObjModel.OldUserImage != null)
                        {
                            string oldimage_Path = Server.MapPath(@"~\Resources\Upload\Images\") + ObjModel.OldUserImage;
                            if (System.IO.File.Exists(oldimage_Path))
                            {
                                System.IO.File.Delete(oldimage_Path);                      
                            }
                        }
                        string UserImageFileName = ObjModel.FirstName + ObjModel.LastName + "_" + DateTime.Now.ToFileTime().ToString() + System.IO.Path.GetExtension(UserImage.FileName);
                        string upload_Path = Server.MapPath(@"~\Resources\Upload\Images\") + UserImageFileName;
                        UserImage.SaveAs(upload_Path);

                        ObjtblCompanyUser.UserImage = UserImageFileName;
                    }
                   
                }
                else
                {
                    if (ObjModel.OldUserImage != null)
                    {
                        ObjtblCompanyUser.UserImage = ObjModel.OldUserImage;
                    }
                }


                int Result = ObjBALCompanyUsers.UpdateCompanyUsers(ObjtblCompanyUser, ObjModel.RoleId);

                if (Result > 0)
                {
                    #region Update User Name and password

                    MembershipUser ObjMembershipUser = Membership.GetUser(ObjModel.OldUserEmail);

                    string OldUserName = ObjModel.OldUserEmail;
                    string NewUserName = ObjModel.UserEmail;

                    if (OldUserName.ToLower().Trim() != NewUserName.ToLower())
                    {
                        UpdateUsername(OldUserName, NewUserName);

                        #region Deduct last activity time

                        if (ObjMembershipUser != null)
                        {
                            ObjBLLMembership.SignOut(ObjMembershipUser.UserName, DateTime.Now.ToUniversalTime().AddMinutes(-(Membership.UserIsOnlineTimeWindow)));
                        }

                        #endregion
                    }

                    string OldPwd = ObjModel.OldUserPassword;

                    if (ObjMembershipUser != null)
                    {
                        if (OldPwd != ObjModel.UserPassword)
                        {
                            Membership.GetUser(ObjModel.OldUserEmail).ChangePassword(OldPwd, ObjModel.UserPassword);
                        }
                    }

                    #endregion

                    strResult = "<span class='successmsg'>Record Updated Successfully</span>";
                }
                else
                {
                    strResult = "<span class='errormsg'>Error Occured While Updating Record</span>";
                }
            }
            catch// (Exception ex)
            {
                strResult = "<span class='errormsg'>Error Occured While Updating Record</span>";               
            }
            finally
            {
                ObjtblCompanyUser = null;
                ObjBALCompanyUsers = null;
                ObjBLLMembership = null;
            }
            return strResult;
        }
     
        /// <summary>
        /// This method is used to update Username
        /// </summary>
        /// <param name="OldUserName"></param>
        /// <param name="NewUserName"></param>
        /// <returns></returns>
        private int UpdateUsername(string OldUserName, string NewUserName)
        {
            int iResult = -1;

            NewUserName = NewUserName.Trim();

            BLLMembership ObjBLLMembership = new BLLMembership();

            try
            {
                iResult = ObjBLLMembership.UpdateUserName(OldUserName, NewUserName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBLLMembership = null;
            }
            return iResult;
        }


        /// <summary>
        /// This method is used to check user name existance
        /// </summary>
        /// <param name="OldUserName"></param>
        /// <param name="NewUserName"></param>
        /// <returns></returns>
        private bool IsExistsUsername(string OldUserName, string NewUserName)
        {
            bool bResult = false;
            NewUserName = NewUserName.Trim();

            BLLMembership ObjBLLMembership = new BLLMembership();

            try
            {
                bResult = ObjBLLMembership.IsExistUserName(OldUserName, NewUserName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ObjBLLMembership = null;
            }
            return bResult;
        }

        #endregion Save User Details

        #endregion Edit Company User
    }
}
