﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Models;
using Emerge.Services;
using Emerge.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Net.Mime;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Security;
using System.Data.Linq;
using System.Web.Security;
using Emerge.Common;
using System.Text;
using System.Configuration;
using AuthorizeNet;
using System.Web.Routing;
using Emerge.CommonClasses;
namespace Emerge.Controllers
{
    public class PaymentController : Controller
    {
        //
        // GET: /Payment/

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// To Make Payment 
        /// /// </summary>
        /// <param name="frm"></param>
        /// <returns></returns>
        public ActionResult MakePayment(FormCollection frm)
        {
            int TranErrorMsgCount = 0;
            long pkInvoiceId;
            string Message = string.Empty;
            //To Get the FormCollection Data.
            string[] rdbPaymentType = frm.GetValue("rdbPaymentType").AttemptedValue.Split(',');
            string[] OutStandingAmount = frm.GetValue("OutStandingAmount").AttemptedValue.Split(',');
            string[] ddlOpenInvoice = frm.GetValue("ddlOpenInvoice").AttemptedValue.Split(',');
            string[] txtOtherAmount = frm.GetValue("txtOtherAmount").AttemptedValue.Split(',');
            string[] chckPayment = frm.GetValue("chckPayment").AttemptedValue.Split(',');

            //string[] CardOption = frm.GetValue("CardOption").AttemptedValue.Split(',');

            //If User selected Statement Balance Option.
            if (rdbPaymentType[0] == "1")
            {
                for (int num = 0; num < chckPayment.Length; num++)
                {
                    pkInvoiceId = Convert.ToInt64(chckPayment[num]);
                    //To Process Payment Gateway.
                    Message = MakePay(Convert.ToDecimal(GetInvoiceAmount(pkInvoiceId)), false, pkInvoiceId, GetInvoiceNumber(pkInvoiceId), frm, TranErrorMsgCount);
                }
            }
            else if (rdbPaymentType[0] == "2")//If User selected OutStanding Option. 
            {
                pkInvoiceId = 0;
                Message = MakePay(Convert.ToDecimal(OutStandingAmount[0]), true, pkInvoiceId, "", frm, TranErrorMsgCount);
            }
            else if (rdbPaymentType[0] == "3")//If User selected Partial Payment Option.
            {

                pkInvoiceId = Convert.ToInt64(ddlOpenInvoice[0].ToString());
                Message = MakePay(Convert.ToDecimal(txtOtherAmount[0]), false, pkInvoiceId, GetInvoiceNumber(pkInvoiceId), frm, TranErrorMsgCount);

            }
            return Json(Message);
        }

        /// <summary>
        /// To Make PrePayment 
        /// /// </summary>
        /// <param name="frm"></param>
        /// <returns></returns>
        public ActionResult MakePrePayment(FormCollection frm)
        {
            string Message = string.Empty;
            Message = PrePayment(frm);
            return Json(Message);
        }


        /// <summary>
        /// To Make Payment through, Payment Gateway.
        /// </summary>
        /// <param name="PayInvoiceAmount"></param>
        /// <param name="PayALLInvoice"></param>
        /// <param name="pkInvoiceId"></param>
        /// <param name="strInvoiceNumber"></param>
        /// <param name="frm"></param>
        /// <param name="TranErrorMsgCount"></param>
        /// <returns></returns>
        private string PrePayment(FormCollection frm)
        {
            StringBuilder ObjStringBuilder1 = new StringBuilder();
            BALInvoice ObjBALInvoice = new BALInvoice();
            try
            {
                //To Getting General Setting from the Web.Config file.
                string APILoginId = ConfigurationManager.AppSettings["AuthorizeNet_APILoginID"];
                string TransactionKey = ConfigurationManager.AppSettings["AuthorizeNet_TransactionKey"];
                bool TestMode = Convert.ToBoolean(ConfigurationManager.AppSettings["AuthorizeNet_TestMode"]);
                //Pass Setting Information into Gateway.
                Gateway gateway = new Gateway(APILoginId, TransactionKey, TestMode);
                //To set user inserted values.
                string[] ddlCardExpiryMonth = frm.GetValue("ddlCardExpiryMonthPrePayment").AttemptedValue.Split(',');
                string[] ddlCardExpiryYear = frm.GetValue("ddlCardExpiryYearPrePayment").AttemptedValue.Split(',');
                string[] ddlAmountRecharge = frm.GetValue("ddlAmountPrePayment").AttemptedValue.Split(',');
                string[] txtCardumber = frm.GetValue("txtCardumberPrePayment").AttemptedValue.Split(',');
                string[] txtFname = frm.GetValue("txtFnamePrePayment").AttemptedValue.Split(',');
                string[] txtLname = frm.GetValue("txtLnamePrePayment").AttemptedValue.Split(',');
                string[] txtComapny = frm.GetValue("txtCompanyPrePayment").AttemptedValue.Split(',');
                string[] txtAddress = frm.GetValue("txtAddressPrePayment").AttemptedValue.Split(',');
                string[] txtCity = frm.GetValue("txtCityPrePayment").AttemptedValue.Split(',');
                string[] ddlState = frm.GetValue("ddlStatePrePayment").AttemptedValue.Split(',');
                string[] ddlCountry = frm.GetValue("ddlCountryPrePayment").AttemptedValue.Split(',');
                string[] txtEmail = frm.GetValue("txtEmailPrePayment").AttemptedValue.Split(',');
                string[] txtZip = frm.GetValue("txtZipPrePayment").AttemptedValue.Split(',');
                string[] CompanyId = frm.GetValue("CompanyIdPrePayment").AttemptedValue.Split(',');
                int ExpiryMonth = Convert.ToInt32(ddlCardExpiryMonth[0]);
                int ExpityYear = Convert.ToInt32(ddlCardExpiryYear[0]);
                string Monthyear = ExpityYear + "-" + ExpiryMonth;
                decimal PayAmount = Convert.ToDecimal(ddlAmountRecharge[0]);
                AuthorizationRequest obj = new AuthorizationRequest(HttpUtility.HtmlEncode(txtCardumber[0].Trim()), Monthyear, PayAmount, "desc");
                obj.FirstName = HttpUtility.HtmlEncode(txtFname[0].Trim());
                obj.LastName = HttpUtility.HtmlEncode(txtLname[0].Trim());
                obj.Company = HttpUtility.HtmlEncode(txtComapny[0].Trim());
                obj.Address = HttpUtility.HtmlEncode(txtAddress[0].Trim());
                obj.City = HttpUtility.HtmlEncode(txtCity[0].Trim());
                obj.State = ddlState[0];
                obj.Zip = HttpUtility.HtmlEncode(txtZip[0].Trim());
                obj.Country = ddlCountry[0];
                obj.Email = HttpUtility.HtmlEncode(txtEmail[0].Trim());
                //To Send information to the gateway for processing.
                var Result = gateway.Send(obj);
                if (Result.Approved == true)
                {
                    //If Geteway approved information regarding payment, then payment information will be saved into paymenthistory table.
                    tblPaymentHistory ObjtblPaymentHistory = new tblPaymentHistory();
                    ObjtblPaymentHistory.Address = txtAddress[0].Trim();
                    ObjtblPaymentHistory.CardExpiryMonth = Convert.ToInt16(ddlCardExpiryMonth[0]);
                    ObjtblPaymentHistory.CardExpiryYear = Convert.ToInt16(ddlCardExpiryYear[0]);
                    ObjtblPaymentHistory.CardNumber = Result.CardNumber;//EncryptDecrypt.Encrypt(txtCardumber[0].Trim());
                    ObjtblPaymentHistory.City = txtCity[0].Trim();
                    ObjtblPaymentHistory.CompanyName = txtComapny[0].Trim();
                    ObjtblPaymentHistory.CreatedDateTime = DateTime.Now;
                    ObjtblPaymentHistory.Email = txtEmail[0].Trim();
                    ObjtblPaymentHistory.FirstName = txtFname[0].Trim();
                    ObjtblPaymentHistory.fkCompanyId = Convert.ToInt32(CompanyId[0].ToString());
                    ObjtblPaymentHistory.fkInvoiceId = 0;
                    Guid ObjGuid = GetCurrentUserDetail().First().UserId;
                    ObjtblPaymentHistory.fkCompanyUserId = ObjGuid;
                    ObjtblPaymentHistory.fkCountryId = 1;
                    //ObjtblPaymentHistory.fkStateId = Convert.ToInt16(ddlState[0].Split('_')[0]);
                    ObjtblPaymentHistory.LastName = txtLname[0].Trim();
                    ObjtblPaymentHistory.PaidAmount = PayAmount;
                    ObjtblPaymentHistory.PaidDate = DateTime.Now;
                    ObjtblPaymentHistory.Zipcode = txtZip[0].Trim();
                    ObjtblPaymentHistory.TransactionId = Result.TransactionID.ToString();
                    ObjtblPaymentHistory.ProcessType = 4;
                    int Success = 0;
                    Success = ObjBALInvoice.AddPrePaymentHistoryDetails(ObjtblPaymentHistory); /*pay invoice*/
                    if (Success == 1)
                    {
                        //If transaction is completed successfully 
                        ObjStringBuilder1.Append("<tr><td>");
                        ObjStringBuilder1.Append("Your transaction number is " + Result.TransactionID.ToString());
                        ObjStringBuilder1.Append("</tr></td>");
                    }
                    else
                    {
                        ObjStringBuilder1.Append("Your transaction number is " + Result.TransactionID.ToString() + ". But an error occurred while saving payment details.");
                        ObjStringBuilder1.Append("</tr></td>");
                    }
                    //To Send Mail after Payment.
                    SendMailAfterPayment(ObjtblPaymentHistory, Result);

                }
                else
                {
                    if (PayAmount == 0)
                    {
                        ObjStringBuilder1.Append("AmountZero");
                    }
                    else
                    {
                        ObjStringBuilder1.Append("InvalidCard");
                    }
                    ObjStringBuilder1.Append("</tr></td>");
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                ObjBALInvoice = null;
            }
            return ObjStringBuilder1.ToString();
        }






        /// <summary>
        /// To Get the Invoive Number based on selected Invoice Id.
        /// </summary>
        /// <param name="pkInvoiceId"></param>
        /// <returns></returns>
        private string GetInvoiceNumber(long pkInvoiceId)
        {
            string InvoiceNumber = "";
            BALInvoice ObjBALInvoice = new BALInvoice();
            try
            {
                List<tblInvoice> ObjList = new List<tblInvoice>();
                ObjList = ObjBALInvoice.GetInvoiceNumber(pkInvoiceId);
                InvoiceNumber = ObjList.ElementAt(0).InvoiceNumber.ToString();
            }
            finally
            {
                ObjBALInvoice = null;
            }
            return InvoiceNumber;
        }

        /// <summary>
        /// To Get the Invoice Amount based on the Selected Invoice Id.
        /// </summary>
        /// <param name="pkInvoiceId"></param>
        /// <returns></returns>
        private string GetInvoiceAmount(long pkInvoiceId)
        {
            string InvoiceBalAmount = "0";
            try
            {
                long invoiceId = Convert.ToInt64(pkInvoiceId);
                decimal retVal = new BALInvoice().GetSelectedInvoiceAmount(invoiceId);
                InvoiceBalAmount = retVal.ToString();
            }
            finally
            {

            }
            return InvoiceBalAmount;
        }





        /// <summary>
        /// To Make Payment through, Payment Gateway.
        /// </summary>
        /// <param name="PayInvoiceAmount"></param>
        /// <param name="PayALLInvoice"></param>
        /// <param name="pkInvoiceId"></param>
        /// <param name="strInvoiceNumber"></param>
        /// <param name="frm"></param>
        /// <param name="TranErrorMsgCount"></param>
        /// <returns></returns>
        private string MakePay(decimal PayInvoiceAmount, bool PayALLInvoice, long pkInvoiceId, string strInvoiceNumber, FormCollection frm, int TranErrorMsgCount)
        {
            StringBuilder ObjStringBuilder1 = new StringBuilder();
            BALInvoice ObjBALInvoice = new BALInvoice();
            try
            {
                // VendorServices.GeneralService.WriteLog(Environment.NewLine + "\n QuickBook_authReturn0_Key :" + QuickBook_authReturn0_Key, "Quikbook");

                //To Getting General Setting from the Web.Config file.
                string APILoginId = ConfigurationManager.AppSettings["AuthorizeNet_APILoginID"];
                string TransactionKey = ConfigurationManager.AppSettings["AuthorizeNet_TransactionKey"];
                bool TestMode = Convert.ToBoolean(ConfigurationManager.AppSettings["AuthorizeNet_TestMode"]);
                //Pass Setting Information into Gateway.
                Gateway gateway = new Gateway(APILoginId, TransactionKey, TestMode);
                decimal PayAmount = PayInvoiceAmount;
                //To set user inserted values.
                string[] ddlCardExpiryMonth = frm.GetValue("ddlCardExpiryMonth").AttemptedValue.Split(',');
                string[] ddlCardExpiryYear = frm.GetValue("ddlCardExpiryYear").AttemptedValue.Split(',');
                string[] txtCardumber = frm.GetValue("txtCardumber").AttemptedValue.Split(',');

                string[] txtFname = frm.GetValue("txtFname").AttemptedValue.Split(',');
                string[] txtLname = frm.GetValue("txtLname").AttemptedValue.Split(',');
                string[] txtComapny = frm.GetValue("txtComapny").AttemptedValue.Split(',');
                string[] txtAddress = frm.GetValue("txtAddress").AttemptedValue.Split(',');
                string[] txtCity = frm.GetValue("txtCity").AttemptedValue.Split(',');
                string[] ddlState = frm.GetValue("ddlState").AttemptedValue.Split(',');
                string[] ddlCountry = frm.GetValue("ddlCountry").AttemptedValue.Split(',');
                string[] txtEmail = frm.GetValue("txtEmail").AttemptedValue.Split(',');
                string[] txtZip = frm.GetValue("txtZip").AttemptedValue.Split(',');
                string[] CompanyId = frm.GetValue("CompanyId").AttemptedValue.Split(',');


                string[] CardOption = frm.GetValue("CardOption").AttemptedValue.Split(',');
                //ND-17 QUICKBOOK IMPLEMENATION 






                int ExpiryMonth = Convert.ToInt32(ddlCardExpiryMonth[0]);
                int ExpityYear = Convert.ToInt32(ddlCardExpiryYear[0]);
                string Monthyear = ExpityYear + "-" + ExpiryMonth;

                AuthorizationRequest obj = new AuthorizationRequest(HttpUtility.HtmlEncode(txtCardumber[0].Trim()), Monthyear, PayAmount, "desc");



                obj.FirstName = HttpUtility.HtmlEncode(txtFname[0].Trim());
                obj.LastName = HttpUtility.HtmlEncode(txtLname[0].Trim());
                obj.Company = HttpUtility.HtmlEncode(txtComapny[0].Trim());
                obj.Address = HttpUtility.HtmlEncode(txtAddress[0].Trim());
                obj.City = HttpUtility.HtmlEncode(txtCity[0].Trim());
                obj.State = ddlState[0];
                obj.Zip = HttpUtility.HtmlEncode(txtZip[0].Trim());
                obj.Country = ddlCountry[0];
                obj.Email = HttpUtility.HtmlEncode(txtEmail[0].Trim());
                //To Send information to the gateway for processing.
                if (CardOption[0].ToString() == "Cheque")
                {

                    //If Geteway approved information regarding payment, then payment information will be saved into paymenthistory table.
                    tblPaymentHistory ObjtblPaymentHistory = new tblPaymentHistory();
                    ObjtblPaymentHistory.Address = txtAddress[0].Trim();
                    ObjtblPaymentHistory.CardExpiryMonth = Convert.ToInt16(ddlCardExpiryMonth[0]);
                    ObjtblPaymentHistory.CardExpiryYear = Convert.ToInt16(ddlCardExpiryYear[0]);
                    //ObjtblPaymentHistory.CardNumber = Result.CardNumber;//EncryptDecrypt.Encrypt(txtCardumber[0].Trim());

                    ObjtblPaymentHistory.CardNumber = txtCardumber[0].ToString();

                    ObjtblPaymentHistory.City = txtCity[0].Trim();
                    ObjtblPaymentHistory.CompanyName = txtComapny[0].Trim();
                    ObjtblPaymentHistory.CreatedDateTime = DateTime.Now;
                    ObjtblPaymentHistory.Email = txtEmail[0].Trim();
                    ObjtblPaymentHistory.FirstName = txtFname[0].Trim();
                    ObjtblPaymentHistory.fkCompanyId = Convert.ToInt32(CompanyId[0].ToString());
                    ObjtblPaymentHistory.fkInvoiceId = pkInvoiceId;
                    Guid ObjGuid = GetCurrentUserDetail().First().UserId;
                    ObjtblPaymentHistory.fkCompanyUserId = ObjGuid;
                    ObjtblPaymentHistory.fkCountryId = 1;
                    //ObjtblPaymentHistory.fkStateId = Convert.ToInt16(ddlState[0].Split('_')[0]);
                    ObjtblPaymentHistory.LastName = txtLname[0].Trim();
                    ObjtblPaymentHistory.PaidAmount = PayAmount;
                    ObjtblPaymentHistory.PaidDate = DateTime.Now;
                    ObjtblPaymentHistory.Zipcode = txtZip[0].Trim();
                    //ObjtblPaymentHistory.TransactionId = Result.TransactionID.ToString();
                    if (CardOption[0].ToString() == "Cheque")
                    {
                        ObjtblPaymentHistory.fkPaymentMethod = 1;
                    }
                    else
                    {
                        ObjtblPaymentHistory.fkPaymentMethod = 2;
                    }

                    int Success = 0;
                    if (PayALLInvoice == true)
                    {
                        Success = ObjBALInvoice.AddPaymentHistoryDetails(ObjtblPaymentHistory, false, true); /*pay all invoice*/
                    }
                    else
                    {
                        Success = ObjBALInvoice.AddPaymentHistoryDetails(ObjtblPaymentHistory, false, false); /*pay invoice*/
                    }
                    if (Success == 1)
                    {
                        //If transaction is completed successfully 
                        ObjStringBuilder1.Append("<tr><td>");
                        if (PayALLInvoice == true)
                        {
                            ObjStringBuilder1.Append("Your transaction is completed successfully.");
                        }
                        else
                        {
                            ObjStringBuilder1.Append("Your check payment has been recorded");

                            ObjStringBuilder1.Append("</tr></td>");


                        }
                        // Qeued in Quickbooks table.
                        using (BALInvoiceQuickBook objpaymentQeue = new BALInvoiceQuickBook())
                        {
                            objpaymentQeue.MakePayement(pkInvoiceId, ObjtblPaymentHistory.CardNumber, Convert.ToInt32(CompanyId[0].ToString()));
                        }
                    }
                    else
                    {
                        //If transaction is completed successfully with transaction number. (If single payment has processed).
                        TranErrorMsgCount++;
                        ObjStringBuilder1.Append("<tr><td>");
                        if (PayALLInvoice == true)
                        {
                            ObjStringBuilder1.Append("Your transaction number is  " + ObjtblPaymentHistory.CardNumber + ". But an error occurred while saving payment details.");
                        }
                        else
                        {
                            ObjStringBuilder1.Append("Your transaction number is " + ObjtblPaymentHistory.CardNumber.ToString() + ". But an error occurred while saving payment details.");
                        }
                        ObjStringBuilder1.Append("</tr></td>");
                    }
                    //To Send Mail after Payment.
                    // SendMailAfterPayment(ObjtblPaymentHistory, Result);


                }
                else
                {
                    var Result = gateway.Send(obj);
                    if (Result.Approved == true)
                    {
                        //If Geteway approved information regarding payment, then payment information will be saved into paymenthistory table.
                        tblPaymentHistory ObjtblPaymentHistory = new tblPaymentHistory();
                        ObjtblPaymentHistory.Address = txtAddress[0].Trim();
                        ObjtblPaymentHistory.CardExpiryMonth = Convert.ToInt16(ddlCardExpiryMonth[0]);
                        ObjtblPaymentHistory.CardExpiryYear = Convert.ToInt16(ddlCardExpiryYear[0]);
                        ObjtblPaymentHistory.CardNumber = Result.CardNumber;//EncryptDecrypt.Encrypt(txtCardumber[0].Trim());
                        ObjtblPaymentHistory.City = txtCity[0].Trim();
                        ObjtblPaymentHistory.CompanyName = txtComapny[0].Trim();
                        ObjtblPaymentHistory.CreatedDateTime = DateTime.Now;
                        ObjtblPaymentHistory.Email = txtEmail[0].Trim();
                        ObjtblPaymentHistory.FirstName = txtFname[0].Trim();
                        ObjtblPaymentHistory.fkCompanyId = Convert.ToInt32(CompanyId[0].ToString());
                        ObjtblPaymentHistory.fkInvoiceId = pkInvoiceId;
                        Guid ObjGuid = GetCurrentUserDetail().First().UserId;
                        ObjtblPaymentHistory.fkCompanyUserId = ObjGuid;
                        ObjtblPaymentHistory.fkCountryId = 1;
                        //ObjtblPaymentHistory.fkStateId = Convert.ToInt16(ddlState[0].Split('_')[0]);
                        ObjtblPaymentHistory.LastName = txtLname[0].Trim();
                        ObjtblPaymentHistory.PaidAmount = PayAmount;
                        ObjtblPaymentHistory.PaidDate = DateTime.Now;
                        ObjtblPaymentHistory.Zipcode = txtZip[0].Trim();
                        ObjtblPaymentHistory.TransactionId = Result.TransactionID.ToString();
                        if (CardOption[0].ToString() == "Cheque")
                        {
                            ObjtblPaymentHistory.fkPaymentMethod = 1;
                        }
                        else
                        {
                            ObjtblPaymentHistory.fkPaymentMethod = 2;
                        }

                        int Success = 0;
                        if (PayALLInvoice == true)
                        {
                            Success = ObjBALInvoice.AddPaymentHistoryDetails(ObjtblPaymentHistory, false, true); /*pay all invoice*/
                        }
                        else
                        {
                            Success = ObjBALInvoice.AddPaymentHistoryDetails(ObjtblPaymentHistory, false, false); /*pay invoice*/
                        }
                        if (Success == 1)
                        {
                            //If transaction is completed successfully 
                            ObjStringBuilder1.Append("<tr><td>");
                            if (PayALLInvoice == true)
                            {
                                ObjStringBuilder1.Append("Your transaction is completed successfully.");
                            }
                            else
                            {
                                ObjStringBuilder1.Append("Your transaction number is " + Result.TransactionID.ToString());

                                ObjStringBuilder1.Append("</tr></td>");


                            }
                            // Qeued in Quickbooks table.
                            using (BALInvoiceQuickBook objpaymentQeue = new BALInvoiceQuickBook())
                            {
                                objpaymentQeue.MakePayement(pkInvoiceId, Result.TransactionID, Convert.ToInt32(CompanyId[0].ToString()));
                            }
                        }
                        else
                        {
                            //If transaction is completed successfully with transaction number. (If single payment has processed).
                            TranErrorMsgCount++;
                            ObjStringBuilder1.Append("<tr><td>");
                            if (PayALLInvoice == true)
                            {
                                ObjStringBuilder1.Append("Your transaction number is  " + Result.TransactionID.ToString() + ". But an error occurred while saving payment details.");
                            }
                            else
                            {
                                ObjStringBuilder1.Append("Your transaction number is " + Result.TransactionID.ToString() + ". But an error occurred while saving payment details.");
                            }
                            ObjStringBuilder1.Append("</tr></td>");
                        }
                        //To Send Mail after Payment.
                        SendMailAfterPayment(ObjtblPaymentHistory, Result);

                    }
                    else
                    {
                        TranErrorMsgCount++;
                        ObjStringBuilder1.Append("<tr><td>");
                        if (PayALLInvoice == true)
                        {
                            ObjStringBuilder1.Append(Result.Message);
                        }
                        else
                        {
                            ObjStringBuilder1.Append("Due to invalid card number. Your transaction has been cancelled.");
                        }
                        ObjStringBuilder1.Append("</tr></td>");
                    }
                }



            }
            catch (Exception)
            {
            }
            finally
            {
                ObjBALInvoice = null;
            }
            return ObjStringBuilder1.ToString();
        }

        /// <summary>
        /// To Get Current Logged On User Information.
        /// </summary>
        /// <returns></returns>
        private List<Proc_Get_UserInfoByMemberShipUserIdResult> GetCurrentUserDetail()
        {
            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
            List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjDbCollection;
            try
            {
                Guid UserId = Guid.Empty;
                MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
                if (ObjMembershipUser != null)
                {
                    UserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());

                }
                ObjDbCollection = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(UserId);
            }
            finally
            {
                ObjBALCompanyUsers = null;
            }
            return ObjDbCollection;
        }

        /// <summary>
        /// To Make Payment 
        /// </summary>
        /// <param name="frm"></param>
        /// <returns></returns>
        public ActionResult MakeInvoicePayment(FormCollection frm)
        {
            int TranErrorMsgCount = 0;
            long pkInvoiceId;
            string Message = string.Empty;
            string[] rdbPaymentType = frm.GetValue("rdbPaymentType").AttemptedValue.Split(',');
            string[] OutStandingAmount = frm.GetValue("OutStandingAmount").AttemptedValue.Split(',');
            string[] ddlOpenInvoice = frm.GetValue("ddlOpenInvoice").AttemptedValue.Split(',');
            string[] txtOtherAmount = frm.GetValue("txtOtherAmount").AttemptedValue.Split(',');
            if (rdbPaymentType[0] == "1")
            {
                string[] chckPayment = frm.GetValue("chckPayment").AttemptedValue.Split(',');
                for (int num = 0; num < chckPayment.Length; num++)
                {
                    pkInvoiceId = Convert.ToInt64(chckPayment[num]);
                    Message = MakePayHistory(Convert.ToDecimal(GetInvoiceAmount(pkInvoiceId)), false, pkInvoiceId, GetInvoiceNumber(pkInvoiceId), frm, TranErrorMsgCount);
                }
            }
            if (rdbPaymentType[0] == "2")
            {
                pkInvoiceId = 0;
                Message = MakePayHistory(Convert.ToDecimal(OutStandingAmount[0]), true, pkInvoiceId, "", frm, TranErrorMsgCount);
            }
            if (rdbPaymentType[0] == "3")
            {
                pkInvoiceId = Convert.ToInt64(ddlOpenInvoice[0].ToString());
                Message = MakePayHistory(Convert.ToDecimal(txtOtherAmount[0]), false, pkInvoiceId, GetInvoiceNumber(pkInvoiceId), frm, TranErrorMsgCount);
            }
            return Json(Message);
        }

        /// <summary>
        /// To Make Payment from the History Section.
        /// /// </summary>
        /// <param name="frm"></param>
        /// <returns></returns>
        private string MakePayHistory(decimal PayInvoiceAmount, bool PayALLInvoice, long pkInvoiceId, string strInvoiceNumber, FormCollection frm, int TranErrorMsgCount)
        {
            StringBuilder ObjStringBuilder1 = new StringBuilder();
            BALInvoice ObjBALInvoice = new BALInvoice();
            try
            {
                //Get appSetting information from the web.Config file.
                string APILoginId = ConfigurationManager.AppSettings["AuthorizeNet_APILoginID"];
                string TransactionKey = ConfigurationManager.AppSettings["AuthorizeNet_TransactionKey"];
                bool TestMode = Convert.ToBoolean(ConfigurationManager.AppSettings["AuthorizeNet_TestMode"]);
                Gateway gateway = new Gateway(APILoginId, TransactionKey, TestMode);
                decimal PayAmount = PayInvoiceAmount;
                //Get User inputed.
                string[] ddlCardExpiryMonth = frm.GetValue("ddlCardExpiryMonth").AttemptedValue.Split(',');
                string[] ddlCardExpiryYear = frm.GetValue("ddlCardExpiryYear").AttemptedValue.Split(',');
                string[] txtCardumber = frm.GetValue("txtCardumber").AttemptedValue.Split(',');

                string[] txtFname = frm.GetValue("txtFname").AttemptedValue.Split(',');
                string[] txtLname = frm.GetValue("txtLname").AttemptedValue.Split(',');
                string[] txtComapny = frm.GetValue("txtComapny").AttemptedValue.Split(',');
                string[] txtAddress = frm.GetValue("txtAddress").AttemptedValue.Split(',');
                string[] txtCity = frm.GetValue("txtCity").AttemptedValue.Split(',');
                string[] ddlState = frm.GetValue("ddlState").AttemptedValue.Split(',');
                string[] ddlCountry = frm.GetValue("ddlCountry").AttemptedValue.Split(',');
                string[] txtEmail = frm.GetValue("txtEmail").AttemptedValue.Split(',');
                string[] txtZip = frm.GetValue("txtZip").AttemptedValue.Split(',');
                //string[] CompanyId = frm.GetValue("CompanyId").AttemptedValue.Split(',');

                int ExpiryMonth = Convert.ToInt32(ddlCardExpiryMonth[0]);
                int ExpityYear = Convert.ToInt32(ddlCardExpiryYear[0]);
                string Monthyear = ExpityYear + "-" + ExpiryMonth;
                AuthorizationRequest obj = new AuthorizationRequest(HttpUtility.HtmlEncode(txtCardumber[0].Trim()), Monthyear, PayAmount, "desc");
                obj.FirstName = HttpUtility.HtmlEncode(txtFname[0].Trim());
                obj.LastName = HttpUtility.HtmlEncode(txtLname[0].Trim());
                obj.Company = HttpUtility.HtmlEncode(txtComapny[0].Trim());
                obj.Address = HttpUtility.HtmlEncode(txtAddress[0].Trim());
                obj.City = HttpUtility.HtmlEncode(txtCity[0].Trim());
                obj.State = ddlState[0];
                obj.Zip = HttpUtility.HtmlEncode(txtZip[0].Trim());
                obj.Country = ddlCountry[0];
                obj.Email = HttpUtility.HtmlEncode(txtEmail[0].Trim());
                var Result = gateway.Send(obj);
                if (Result.Approved == true)
                {
                    //If User Passed from the Gateway, then save information into PaymentHistory table.
                    tblPaymentHistory ObjtblPaymentHistory = new tblPaymentHistory();
                    ObjtblPaymentHistory.Address = txtAddress[0].Trim();
                    ObjtblPaymentHistory.CardExpiryMonth = Convert.ToInt16(ddlCardExpiryMonth[0]);
                    ObjtblPaymentHistory.CardExpiryYear = Convert.ToInt16(ddlCardExpiryYear[0]);
                    ObjtblPaymentHistory.CardNumber = Result.CardNumber; //EncryptDecrypt.Encrypt(txtCardumber[0].Trim());
                    ObjtblPaymentHistory.City = txtCity[0].Trim();
                    ObjtblPaymentHistory.CompanyName = txtComapny[0].Trim();
                    ObjtblPaymentHistory.CreatedDateTime = DateTime.Now;
                    ObjtblPaymentHistory.Email = txtEmail[0].Trim();
                    ObjtblPaymentHistory.FirstName = txtFname[0].Trim();
                    ObjtblPaymentHistory.fkCompanyId = GetCurrentUserDetail().First().pkCompanyId;
                    ObjtblPaymentHistory.fkInvoiceId = pkInvoiceId;
                    Guid ObjGuid = GetCurrentUserDetail().First().UserId;
                    ObjtblPaymentHistory.fkCompanyUserId = ObjGuid;
                    ObjtblPaymentHistory.fkCountryId = 1;
                    //ObjtblPaymentHistory.fkStateId = Convert.ToInt16(ddlState[0].Split('_')[0]);
                    ObjtblPaymentHistory.LastName = txtLname[0].Trim();
                    ObjtblPaymentHistory.PaidAmount = PayAmount;
                    ObjtblPaymentHistory.PaidDate = DateTime.Now;
                    ObjtblPaymentHistory.Zipcode = txtZip[0].Trim();
                    ObjtblPaymentHistory.TransactionId = Result.TransactionID.ToString();
                    int Success = 0;
                    if (PayALLInvoice == true)
                    {
                        Success = ObjBALInvoice.AddPaymentHistoryDetails(ObjtblPaymentHistory, false, true); /*pay all invoice*/
                    }
                    else
                    {
                        Success = ObjBALInvoice.AddPaymentHistoryDetails(ObjtblPaymentHistory, false, false); /*pay invoice*/
                    }
                    if (Success == 1)
                    {
                        //   LoadOpenInvoice();
                        //  TranSuccMsgCount++;
                        ObjStringBuilder1.Append("<tr><td>");
                        if (PayALLInvoice == true)
                        {
                            ObjStringBuilder1.Append("Your transaction is completed successfully.");
                        }
                        else
                        {
                            ObjStringBuilder1.Append("Your transaction number is " + Result.TransactionID.ToString());
                        }
                        ObjStringBuilder1.Append("</tr></td>");
                    }
                    else
                    {
                        TranErrorMsgCount++;
                        ObjStringBuilder1.Append("<tr><td>");
                        if (PayALLInvoice == true)
                        {
                            ObjStringBuilder1.Append("Your transaction number is  " + Result.TransactionID.ToString() + ". But an error occurred while saving payment details.");
                        }
                        else
                        {
                            ObjStringBuilder1.Append("Your transaction number is " + Result.TransactionID.ToString() + ". But an error occurred while saving payment details.");
                        }
                        ObjStringBuilder1.Append("</tr></td>");
                    }
                    //  MultiViewPayInvoice.ActiveViewIndex = 2;
                    // UPPayInvoice.Update();
                    SendMailAfterPayment(ObjtblPaymentHistory, Result);

                }
                else
                {
                    TranErrorMsgCount++;
                    ObjStringBuilder1.Append("<tr><td>");
                    if (PayALLInvoice == true)
                    {
                        ObjStringBuilder1.Append(Result.Message);
                    }
                    else
                    {
                        ObjStringBuilder1.Append("Due to invalid card number. Your transaction has been cancelled.");
                    }
                    ObjStringBuilder1.Append("</tr></td>");
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                ObjBALInvoice = null;
            }
            return ObjStringBuilder1.ToString();
        }

        /// <summary>
        /// to Send email after Payment.
        /// </summary>
        /// <param name="tblObjPaymentHistory"></param>
        /// <param name="Result"></param>
        /// <returns></returns>
        public bool SendMailAfterPayment(tblPaymentHistory tblObjPaymentHistory, IGatewayResponse Result)
        {
            bool mailsent = false;
            BALGeneral objBALGeneral = new BALGeneral();
            try
            {
                string emailText = "";
               // BALGeneral ObjBALGeneral = new BALGeneral();
               // string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;

                //string FromMail = ConfigurationManager.AppSettings["EmergeAdmin"].ToString();

                var emailContent = objBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, Convert.ToInt32(ConfigurationManager.AppSettings["BillingMakePayementTemplateId"]));//Use dummy templete.
                emailText = emailContent.TemplateContent;

                emailText = emailText.Replace("%UAddress%", tblObjPaymentHistory.Address);
                emailText = emailText.Replace("%Zip%", tblObjPaymentHistory.Zipcode);
                //emailText = emailText.Replace("%OrderNo%", "00000000000000");

                emailText = emailText.Replace("%CustomerID%", tblObjPaymentHistory.CompanyName);
                emailText = emailText.Replace("%InvoiceNo%", GetInvoiceNumber(tblObjPaymentHistory.fkInvoiceId));
                emailText = emailText.Replace("%iName%", tblObjPaymentHistory.FirstName + ", " + tblObjPaymentHistory.LastName);
                emailText = emailText.Replace("%iAddress%", tblObjPaymentHistory.Address);
                emailText = emailText.Replace("%iCity%", tblObjPaymentHistory.City);
                emailText = emailText.Replace("%iState%", "");
                emailText = emailText.Replace("%iemail%", tblObjPaymentHistory.Email);
                emailText = emailText.Replace("%Paid%", tblObjPaymentHistory.PaidAmount.ToString());
                emailText = emailText.Replace("%Card%", Result.CardNumber);
                emailText = emailText.Replace("%IDateTime%", tblObjPaymentHistory.PaidDate.Value.ToShortDateString());
                emailText = emailText.Replace("%Transaction%", Result.TransactionID);
                //return Utility.SendMail(tblObjPaymentHistory.Email, "kamleshs@chetu.com,arunk3@chetu.com", string.Empty, FromMail, emailText, "Invoice Mail");
                return Utility.SendMail("ashishs6@chetu.com", ConfigurationManager.AppSettings["BillingCCName"].ToString(), string.Empty, "ashishs6@chetu.com", emailText, "Invoice Mail");
            }
            catch //(Exception ex)
            {
                return mailsent;
                //throw ex;
            }
            finally
            {
                objBALGeneral = null;
            }

        }

    }
}



















//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;
//using Emerge.Models;
//using Emerge.Services;
//using Emerge.Data;
//using System.IO;
//using System.Text.RegularExpressions;
//using System.Net.Mail;
//using System.Net.Mime;
//using System.Web.UI.HtmlControls;
//using System.Web.UI.WebControls;
//using System.Security;
//using System.Data.Linq;
//using System.Web.Security;
//using Emerge.Common;
//using System.Text;
//using System.Configuration;
//using AuthorizeNet;
//using System.Web.Routing;
//using Emerge.CommonClasses;
//namespace Emerge.Controllers
//{
//    public class PaymentController : Controller
//    {
//        //
//        // GET: /Payment/

//        public ActionResult Index()
//        {
//            return View();
//        }

//        /// <summary>
//        /// To Make Payment 
//        /// /// </summary>
//        /// <param name="frm"></param>
//        /// <returns></returns>
//        public ActionResult MakePayment(FormCollection frm)
//        {
//            int TranErrorMsgCount = 0;
//            long pkInvoiceId;
//            string Message = string.Empty;
//            //To Get the FormCollection Data.
//            string[] rdbPaymentType = frm.GetValue("rdbPaymentType").AttemptedValue.Split(',');
//            string[] OutStandingAmount = frm.GetValue("OutStandingAmount").AttemptedValue.Split(',');
//            string[] ddlOpenInvoice = frm.GetValue("ddlOpenInvoice").AttemptedValue.Split(',');
//            string[] txtOtherAmount = frm.GetValue("txtOtherAmount").AttemptedValue.Split(',');
//            string[] chckPayment = frm.GetValue("chckPayment").AttemptedValue.Split(',');
//            //If User selected Statement Balance Option.
//            if (rdbPaymentType[0] == "1")
//            {
//                for (int num = 0; num < chckPayment.Length; num++)
//                {
//                    pkInvoiceId = Convert.ToInt64(chckPayment[num]);
//                    //To Process Payment Gateway.
//                    Message = MakePay(Convert.ToDecimal(GetInvoiceAmount(pkInvoiceId)), false, pkInvoiceId, GetInvoiceNumber(pkInvoiceId), frm, TranErrorMsgCount);
//                }
//            }
//            else if (rdbPaymentType[0] == "2")//If User selected OutStanding Option. 
//            {
//                pkInvoiceId = 0;
//                Message = MakePay(Convert.ToDecimal(OutStandingAmount[0]), true, pkInvoiceId, "", frm, TranErrorMsgCount);
//            }
//            else if (rdbPaymentType[0] == "3")//If User selected Partial Payment Option.
//            {
//                pkInvoiceId = Convert.ToInt64(ddlOpenInvoice[0].ToString());
//                Message = MakePay(Convert.ToDecimal(txtOtherAmount[0]), false, pkInvoiceId, GetInvoiceNumber(pkInvoiceId), frm, TranErrorMsgCount);
//            }
//            return Json(Message);
//        }

//        /// <summary>
//        /// To Get the Invoive Number based on selected Invoice Id.
//        /// </summary>
//        /// <param name="pkInvoiceId"></param>
//        /// <returns></returns>
//        private string GetInvoiceNumber(long pkInvoiceId)
//        {
//            string InvoiceNumber = "";
//            BALInvoice ObjBALInvoice = new BALInvoice();
//            try
//            {
//                List<tblInvoice> ObjList = new List<tblInvoice>();
//                ObjList = ObjBALInvoice.GetInvoiceNumber(pkInvoiceId);
//                InvoiceNumber = ObjList.ElementAt(0).InvoiceNumber.ToString();
//            }
//            finally
//            {
//                ObjBALInvoice = null;
//            }
//            return InvoiceNumber;
//        }

//        /// <summary>
//        /// To Get the Invoice Amount based on the Selected Invoice Id.
//        /// </summary>
//        /// <param name="pkInvoiceId"></param>
//        /// <returns></returns>
//        private string GetInvoiceAmount(long pkInvoiceId)
//        {
//            string InvoiceBalAmount = "0";
//            try
//            {
//                long invoiceId = Convert.ToInt64(pkInvoiceId);
//                decimal retVal = new BALInvoice().GetSelectedInvoiceAmount(invoiceId);
//                InvoiceBalAmount = retVal.ToString();
//            }
//            finally
//            {

//            }
//            return InvoiceBalAmount;
//        }

//        /// <summary>
//        /// To Make Payment through, Payment Gateway.
//        /// </summary>
//        /// <param name="PayInvoiceAmount"></param>
//        /// <param name="PayALLInvoice"></param>
//        /// <param name="pkInvoiceId"></param>
//        /// <param name="strInvoiceNumber"></param>
//        /// <param name="frm"></param>
//        /// <param name="TranErrorMsgCount"></param>
//        /// <returns></returns>
//        private string MakePay(decimal PayInvoiceAmount, bool PayALLInvoice, long pkInvoiceId, string strInvoiceNumber, FormCollection frm, int TranErrorMsgCount)
//        {
//            StringBuilder ObjStringBuilder1 = new StringBuilder();
//            BALInvoice ObjBALInvoice = new BALInvoice();
//            try
//            {
//                //To Getting General Setting from the Web.Config file.
//                string APILoginId = ConfigurationManager.AppSettings["AuthorizeNet_APILoginID"];
//                string TransactionKey = ConfigurationManager.AppSettings["AuthorizeNet_TransactionKey"];
//                bool TestMode = Convert.ToBoolean(ConfigurationManager.AppSettings["AuthorizeNet_TestMode"]);
//                //Pass Setting Information into Gateway.
//                Gateway gateway = new Gateway(APILoginId, TransactionKey, TestMode);
//                decimal PayAmount = PayInvoiceAmount;
//                //To set user inserted values.
//                string[] ddlCardExpiryMonth = frm.GetValue("ddlCardExpiryMonth").AttemptedValue.Split(',');
//                string[] ddlCardExpiryYear = frm.GetValue("ddlCardExpiryYear").AttemptedValue.Split(',');
//                string[] txtCardumber = frm.GetValue("txtCardumber").AttemptedValue.Split(',');

//                string[] txtFname = frm.GetValue("txtFname").AttemptedValue.Split(',');
//                string[] txtLname = frm.GetValue("txtLname").AttemptedValue.Split(',');
//                string[] txtComapny = frm.GetValue("txtComapny").AttemptedValue.Split(',');
//                string[] txtAddress = frm.GetValue("txtAddress").AttemptedValue.Split(',');
//                string[] txtCity = frm.GetValue("txtCity").AttemptedValue.Split(',');
//                string[] ddlState = frm.GetValue("ddlState").AttemptedValue.Split(',');
//                string[] ddlCountry = frm.GetValue("ddlCountry").AttemptedValue.Split(',');
//                string[] txtEmail = frm.GetValue("txtEmail").AttemptedValue.Split(',');
//                string[] txtZip = frm.GetValue("txtZip").AttemptedValue.Split(',');
//                string[] CompanyId = frm.GetValue("CompanyId").AttemptedValue.Split(',');

//                int ExpiryMonth = Convert.ToInt32(ddlCardExpiryMonth[0]);
//                int ExpityYear = Convert.ToInt32(ddlCardExpiryYear[0]);
//                string Monthyear = ExpityYear + "-" + ExpiryMonth;
//                AuthorizationRequest obj = new AuthorizationRequest(HttpUtility.HtmlEncode(txtCardumber[0].Trim()), Monthyear, PayAmount, "desc");
//                obj.FirstName = HttpUtility.HtmlEncode(txtFname[0].Trim());
//                obj.LastName = HttpUtility.HtmlEncode(txtLname[0].Trim());
//                obj.Company = HttpUtility.HtmlEncode(txtComapny[0].Trim());
//                obj.Address = HttpUtility.HtmlEncode(txtAddress[0].Trim());
//                obj.City = HttpUtility.HtmlEncode(txtCity[0].Trim());
//                obj.State = ddlState[0];
//                obj.Zip = HttpUtility.HtmlEncode(txtZip[0].Trim());
//                obj.Country = ddlCountry[0];
//                obj.Email = HttpUtility.HtmlEncode(txtEmail[0].Trim());
//                //To Send information to the gateway for processing.
//                var Result = gateway.Send(obj);
//                if (Result.Approved == true)
//                {
//                    //If Geteway approved information regarding payment, then payment information will be saved into paymenthistory table.
//                    tblPaymentHistory ObjtblPaymentHistory = new tblPaymentHistory();
//                    ObjtblPaymentHistory.Address = txtAddress[0].Trim();
//                    ObjtblPaymentHistory.CardExpiryMonth = Convert.ToInt16(ddlCardExpiryMonth[0]);
//                    ObjtblPaymentHistory.CardExpiryYear = Convert.ToInt16(ddlCardExpiryYear[0]);
//                    ObjtblPaymentHistory.CardNumber = Result.CardNumber;//EncryptDecrypt.Encrypt(txtCardumber[0].Trim());
//                    ObjtblPaymentHistory.City = txtCity[0].Trim();
//                    ObjtblPaymentHistory.CompanyName = txtComapny[0].Trim();
//                    ObjtblPaymentHistory.CreatedDateTime = DateTime.Now;
//                    ObjtblPaymentHistory.Email = txtEmail[0].Trim();
//                    ObjtblPaymentHistory.FirstName = txtFname[0].Trim();
//                    ObjtblPaymentHistory.fkCompanyId = Int32.Parse(CompanyId[0].ToString());
//                    ObjtblPaymentHistory.fkInvoiceId = pkInvoiceId;
//                    int ObjGuid = GetCurrentUserDetail().First().pkCompanyUserId;
//                    ObjtblPaymentHistory.fkCompanyUserId = ObjGuid;
//                    ObjtblPaymentHistory.fkCountryId = 1;
//                    //ObjtblPaymentHistory.fkStateId = Convert.ToInt16(ddlState[0].Split('_')[0]);
//                    ObjtblPaymentHistory.LastName = txtLname[0].Trim();
//                    ObjtblPaymentHistory.PaidAmount = PayAmount;
//                    ObjtblPaymentHistory.PaidDate = DateTime.Now;
//                    ObjtblPaymentHistory.Zipcode = txtZip[0].Trim();
//                    ObjtblPaymentHistory.TransactionId = Result.TransactionID.ToString();
//                    int Success = 0;
//                    if (PayALLInvoice == true)
//                    {
//                        Success = ObjBALInvoice.AddPaymentHistoryDetails(ObjtblPaymentHistory, false, true); /*pay all invoice*/
//                    }
//                    else
//                    {
//                        Success = ObjBALInvoice.AddPaymentHistoryDetails(ObjtblPaymentHistory, false, false); /*pay invoice*/
//                    }
//                    if (Success == 1)
//                    {
//                        //If transaction is completed successfully 
//                        ObjStringBuilder1.Append("<tr><td>");
//                        if (PayALLInvoice == true)
//                        {
//                            ObjStringBuilder1.Append("Your transaction is completed successfully.");
//                        }
//                        else
//                        {
//                            ObjStringBuilder1.Append("Your transaction number is " + Result.TransactionID.ToString());
//                        }
//                        ObjStringBuilder1.Append("</tr></td>");
//                    }
//                    else
//                    {
//                        //If transaction is completed successfully with transaction number. (If single payment has processed).
//                        TranErrorMsgCount++;
//                        ObjStringBuilder1.Append("<tr><td>");
//                        if (PayALLInvoice == true)
//                        {
//                            ObjStringBuilder1.Append("Your transaction number is  " + Result.TransactionID.ToString() + ". But an error occurred while saving payment details.");
//                        }
//                        else
//                        {
//                            ObjStringBuilder1.Append("Your transaction number is " + Result.TransactionID.ToString() + ". But an error occurred while saving payment details.");
//                        }
//                        ObjStringBuilder1.Append("</tr></td>");
//                    }
//                   //To Send Mail after Payment.
//                    SendMailAfterPayment(ObjtblPaymentHistory, Result);

//                }
//                else
//                {
//                    TranErrorMsgCount++;
//                    ObjStringBuilder1.Append("<tr><td>");
//                    if (PayALLInvoice == true)
//                    {
//                        ObjStringBuilder1.Append(Result.Message);
//                    }
//                    else
//                    {
//                        ObjStringBuilder1.Append(Result.Message.Replace(".", "") + " for Invoice#:  " + strInvoiceNumber);
//                    }
//                    ObjStringBuilder1.Append("</tr></td>");
//                }
//            }
//            catch (Exception)
//            {
//            }
//            finally
//            {
//                ObjBALInvoice = null;
//            }
//            return ObjStringBuilder1.ToString();
//        }

//        /// <summary>
//        /// To Get Current Logged On User Information.
//        /// </summary>
//        /// <returns></returns>
//        private List<Proc_Get_UserInfoByMemberShipUserIdResult> GetCurrentUserDetail()
//        {
//            BALCompanyUsers ObjBALCompanyUsers = new BALCompanyUsers();
//            List<Proc_Get_UserInfoByMemberShipUserIdResult> ObjDbCollection;
//            try
//            {
//                Guid UserId = Guid.Empty;
//                MembershipUser ObjMembershipUser = Membership.GetUser(User.Identity.Name);
//                if (ObjMembershipUser != null)
//                {
//                    UserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
//                }
//                ObjDbCollection = ObjBALCompanyUsers.GetUserInfoByMemberShipUserId(UserId);
//            }
//            finally
//            {
//                ObjBALCompanyUsers = null;
//            }
//            return ObjDbCollection;
//        }

//        /// <summary>
//        /// To Make Payment 
//        /// </summary>
//        /// <param name="frm"></param>
//        /// <returns></returns>
//        public ActionResult MakeInvoicePayment(FormCollection frm)
//        {
//            int TranErrorMsgCount = 0;
//            long pkInvoiceId;
//            string Message = string.Empty;
//            string[] rdbPaymentType = frm.GetValue("rdbPaymentType").AttemptedValue.Split(',');
//            string[] OutStandingAmount = frm.GetValue("OutStandingAmount").AttemptedValue.Split(',');
//            string[] ddlOpenInvoice = frm.GetValue("ddlOpenInvoice").AttemptedValue.Split(',');
//            string[] txtOtherAmount = frm.GetValue("txtOtherAmount").AttemptedValue.Split(',');
//            if (rdbPaymentType[0] == "1")
//            {
//                string[] chckPayment = frm.GetValue("chckPayment").AttemptedValue.Split(',');
//                for (int num = 0; num < chckPayment.Length; num++)
//                {
//                    pkInvoiceId = Convert.ToInt64(chckPayment[num]);
//                    Message = MakePayHistory(Convert.ToDecimal(GetInvoiceAmount(pkInvoiceId)), false, pkInvoiceId, GetInvoiceNumber(pkInvoiceId), frm, TranErrorMsgCount);
//                }
//            }
//            if (rdbPaymentType[0] == "2")
//            {
//                pkInvoiceId = 0;
//                Message = MakePayHistory(Convert.ToDecimal(OutStandingAmount[0]), true, pkInvoiceId, "", frm, TranErrorMsgCount);
//            }
//            if (rdbPaymentType[0] == "3")
//            {
//                pkInvoiceId = Convert.ToInt64(ddlOpenInvoice[0].ToString());
//                Message = MakePayHistory(Convert.ToDecimal(txtOtherAmount[0]), false, pkInvoiceId, GetInvoiceNumber(pkInvoiceId), frm, TranErrorMsgCount);
//            }
//            return Json(Message);
//        }

//        /// <summary>
//        /// To Make Payment from the History Section.
//        /// /// </summary>
//        /// <param name="frm"></param>
//        /// <returns></returns>
//        private string MakePayHistory(decimal PayInvoiceAmount, bool PayALLInvoice, long pkInvoiceId, string strInvoiceNumber, FormCollection frm, int TranErrorMsgCount)
//        {
//            StringBuilder ObjStringBuilder1 = new StringBuilder();
//            BALInvoice ObjBALInvoice = new BALInvoice();
//            try
//            {
//                //Get appSetting information from the web.Config file.
//                string APILoginId = ConfigurationManager.AppSettings["AuthorizeNet_APILoginID"];
//                string TransactionKey = ConfigurationManager.AppSettings["AuthorizeNet_TransactionKey"];
//                bool TestMode = Convert.ToBoolean(ConfigurationManager.AppSettings["AuthorizeNet_TestMode"]);
//                Gateway gateway = new Gateway(APILoginId, TransactionKey, TestMode);
//                decimal PayAmount = PayInvoiceAmount;
//                //Get User inputed.
//                string[] ddlCardExpiryMonth = frm.GetValue("ddlCardExpiryMonth").AttemptedValue.Split(',');
//                string[] ddlCardExpiryYear = frm.GetValue("ddlCardExpiryYear").AttemptedValue.Split(',');
//                string[] txtCardumber = frm.GetValue("txtCardumber").AttemptedValue.Split(',');

//                string[] txtFname = frm.GetValue("txtFname").AttemptedValue.Split(',');
//                string[] txtLname = frm.GetValue("txtLname").AttemptedValue.Split(',');
//                string[] txtComapny = frm.GetValue("txtComapny").AttemptedValue.Split(',');
//                string[] txtAddress = frm.GetValue("txtAddress").AttemptedValue.Split(',');
//                string[] txtCity = frm.GetValue("txtCity").AttemptedValue.Split(',');
//                string[] ddlState = frm.GetValue("ddlState").AttemptedValue.Split(',');
//                string[] ddlCountry = frm.GetValue("ddlCountry").AttemptedValue.Split(',');
//                string[] txtEmail = frm.GetValue("txtEmail").AttemptedValue.Split(',');
//                string[] txtZip = frm.GetValue("txtZip").AttemptedValue.Split(',');
//                string[] CompanyId = frm.GetValue("CompanyId").AttemptedValue.Split(',');

//                int ExpiryMonth = Convert.ToInt32(ddlCardExpiryMonth[0]);
//                int ExpityYear = Convert.ToInt32(ddlCardExpiryYear[0]);
//                string Monthyear = ExpityYear + "-" + ExpiryMonth;
//                AuthorizationRequest obj = new AuthorizationRequest(HttpUtility.HtmlEncode(txtCardumber[0].Trim()), Monthyear, PayAmount, "desc");
//                obj.FirstName = HttpUtility.HtmlEncode(txtFname[0].Trim());
//                obj.LastName = HttpUtility.HtmlEncode(txtLname[0].Trim());
//                obj.Company = HttpUtility.HtmlEncode(txtComapny[0].Trim());
//                obj.Address = HttpUtility.HtmlEncode(txtAddress[0].Trim());
//                obj.City = HttpUtility.HtmlEncode(txtCity[0].Trim());
//                obj.State = ddlState[0];
//                obj.Zip = HttpUtility.HtmlEncode(txtZip[0].Trim());
//                obj.Country = ddlCountry[0];
//                obj.Email = HttpUtility.HtmlEncode(txtEmail[0].Trim());
//                var Result = gateway.Send(obj);
//                if (Result.Approved == true)
//                {
//                    //If User Passed from the Gateway, then save information into PaymentHistory table.
//                    tblPaymentHistory ObjtblPaymentHistory = new tblPaymentHistory();
//                    ObjtblPaymentHistory.Address = txtAddress[0].Trim();
//                    ObjtblPaymentHistory.CardExpiryMonth = Convert.ToInt16(ddlCardExpiryMonth[0]);
//                    ObjtblPaymentHistory.CardExpiryYear = Convert.ToInt16(ddlCardExpiryYear[0]);
//                    ObjtblPaymentHistory.CardNumber = Result.CardNumber; //EncryptDecrypt.Encrypt(txtCardumber[0].Trim());
//                    ObjtblPaymentHistory.City = txtCity[0].Trim();
//                    ObjtblPaymentHistory.CompanyName = txtComapny[0].Trim();
//                    ObjtblPaymentHistory.CreatedDateTime = DateTime.Now;
//                    ObjtblPaymentHistory.Email = txtEmail[0].Trim();
//                    ObjtblPaymentHistory.FirstName = txtFname[0].Trim();
//                    ObjtblPaymentHistory.fkCompanyId = GetCurrentUserDetail().First().pkCompanyId;
//                    ObjtblPaymentHistory.fkInvoiceId = pkInvoiceId;
//                    int ObjGuid = GetCurrentUserDetail().First().pkCompanyUserId;
//                    ObjtblPaymentHistory.fkCompanyUserId = ObjGuid;
//                    ObjtblPaymentHistory.fkCountryId = 1;
//                    //ObjtblPaymentHistory.fkStateId = Convert.ToInt16(ddlState[0].Split('_')[0]);
//                    ObjtblPaymentHistory.LastName = txtLname[0].Trim();
//                    ObjtblPaymentHistory.PaidAmount = PayAmount;
//                    ObjtblPaymentHistory.PaidDate = DateTime.Now;
//                    ObjtblPaymentHistory.Zipcode = txtZip[0].Trim();
//                    ObjtblPaymentHistory.TransactionId = Result.TransactionID.ToString();
//                    int Success = 0;
//                    if (PayALLInvoice == true)
//                    {
//                        Success = ObjBALInvoice.AddPaymentHistoryDetails(ObjtblPaymentHistory, false, true); /*pay all invoice*/
//                    }
//                    else
//                    {
//                        Success = ObjBALInvoice.AddPaymentHistoryDetails(ObjtblPaymentHistory, false, false); /*pay invoice*/
//                    }
//                    if (Success == 1)
//                    {
//                        //   LoadOpenInvoice();
//                        //  TranSuccMsgCount++;
//                        ObjStringBuilder1.Append("<tr><td>");
//                        if (PayALLInvoice == true)
//                        {
//                            ObjStringBuilder1.Append("Your transaction is completed successfully.");
//                        }
//                        else
//                        {
//                            ObjStringBuilder1.Append("Your transaction number is " + Result.TransactionID.ToString());
//                        }
//                        ObjStringBuilder1.Append("</tr></td>");
//                    }
//                    else
//                    {
//                        TranErrorMsgCount++;
//                        ObjStringBuilder1.Append("<tr><td>");
//                        if (PayALLInvoice == true)
//                        {
//                            ObjStringBuilder1.Append("Your transaction number is  " + Result.TransactionID.ToString() + ". But an error occurred while saving payment details.");
//                        }
//                        else
//                        {
//                            ObjStringBuilder1.Append("Your transaction number is " + Result.TransactionID.ToString() + ". But an error occurred while saving payment details.");
//                        }
//                        ObjStringBuilder1.Append("</tr></td>");
//                    }
//                    //  MultiViewPayInvoice.ActiveViewIndex = 2;
//                    // UPPayInvoice.Update();
//                    SendMailAfterPayment(ObjtblPaymentHistory, Result);

//                }
//                else
//                {
//                    TranErrorMsgCount++;
//                    ObjStringBuilder1.Append("<tr><td>");
//                    if (PayALLInvoice == true)
//                    {
//                        ObjStringBuilder1.Append(Result.Message);
//                    }
//                    else
//                    {
//                        ObjStringBuilder1.Append(Result.Message.Replace(".", "") + " for Invoice#:  " + strInvoiceNumber);
//                    }
//                    ObjStringBuilder1.Append("</tr></td>");
//                }
//            }
//            catch (Exception)
//            {
//            }
//            finally
//            {
//                ObjBALInvoice = null;
//            }
//            return ObjStringBuilder1.ToString();
//        }

//        /// <summary>
//        /// to Send email after Payment.
//        /// </summary>
//        /// <param name="tblObjPaymentHistory"></param>
//        /// <param name="Result"></param>
//        /// <returns></returns>
//        public bool SendMailAfterPayment(tblPaymentHistory tblObjPaymentHistory, IGatewayResponse Result)
//        {
//            bool mailsent = false;
//            BALGeneral objBALGeneral = new BALGeneral();
//            try
//            {
//                string emailText = "";
//                BALGeneral ObjBALGeneral = new BALGeneral();
//                string WebsiteLogo = ApplicationPath.GetApplicationPath() + @"Resources/Upload/Images/" + ObjBALGeneral.GetSettings(1).WebSiteLogo;

//                var emailContent = objBALGeneral.GetEmailTemplate(Utility.SiteApplicationId, 82);//Use dummy templete.
//                emailText = emailContent.TemplateContent;

//                emailText = emailText.Replace("%UAddress%", tblObjPaymentHistory.Address);
//                emailText = emailText.Replace("%Zip%", tblObjPaymentHistory.Zipcode);
//                //emailText = emailText.Replace("%OrderNo%", "00000000000000");

//                emailText = emailText.Replace("%CustomerID%", "");
//                emailText = emailText.Replace("%InvoiceNo%", Result.InvoiceNumber);
//                emailText = emailText.Replace("%iName%", tblObjPaymentHistory.FirstName + ", " + tblObjPaymentHistory.LastName);
//                emailText = emailText.Replace("%iAddress%", tblObjPaymentHistory.Address);
//                emailText = emailText.Replace("%iCity%", tblObjPaymentHistory.City);
//                emailText = emailText.Replace("%iState%", "");
//                emailText = emailText.Replace("%iemail%", tblObjPaymentHistory.Email);
//                emailText = emailText.Replace("%Paid%", tblObjPaymentHistory.PaidAmount.ToString());
//                emailText = emailText.Replace("%Card%", Result.CardNumber);
//                emailText = emailText.Replace("%IDateTime%", tblObjPaymentHistory.PaidDate.Value.ToShortDateString());
//                emailText = emailText.Replace("%Transaction%", Result.TransactionID);


//                return Utility.SendMail("arunk3@chetu.com", string.Empty, string.Empty, "arunk3@chetu.com", emailText, "Invoice Mail");
//            }
//            catch (Exception ex)
//            {
//                return mailsent;
//                throw ex;
//            }
//            finally
//            {
//                objBALGeneral = null;
//            }

//        }

//    }
//}
