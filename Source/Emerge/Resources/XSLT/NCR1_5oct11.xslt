﻿<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:a="http://ns.hr-xml.org/2004-08-02">
  <xsl:output method="html" encoding="iso-8859-1" />
  <xsl:template match="/">
    <xsl:variable name="pkOrderDetailId" select="a:BackgroundReports/@OrderDetailId"></xsl:variable>
    <input type="text" id="txtTotalRecord_{($pkOrderDetailId)}" style="display:none" value="{(count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase))}"/>

    <center>
      <div class="sectionsummary">
        <h3>
          (<xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)" />) Results CRIMINAL REPORT.<br />
        </h3>
      </div>

      <xsl:variable name="TotalRecords">
        <xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)" />
      </xsl:variable>

      <xsl:if test="$TotalRecords=0">

        <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
          <thead>

            <tr>
              <th class="begin"></th>
              <th colspan="5">
                <span style="font-size:20px;font-weight:bold;">Search Information</span>
              </th>
              <th class="end"></th>
            </tr>

          </thead>
          <tbody>

            <tr>
              <td class="begin">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colspan="5">
                <h4>Name:</h4>
                <h4>
                  <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:GivenName"/>
                  <xsl:text xml:space="preserve">  </xsl:text>
                  <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:FamilyName"/>
                </h4>
                <br />
                <h4>DOB:</h4>
                <h4>
                  <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:DemographicDetail/a:DateOfBirth"/>
                </h4>
              </td>
              <td class="end">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>

          </tbody>
          <tfoot>
            <td class="begin"></td>
            <td colspan="5"></td>
            <td class="end"></td>
          </tfoot>
        </table>

      </xsl:if>
      <table id="tblNCR" class="reporttable" border="0" cellpadding="0" cellspacing="0">
        <thead>

          <tr>
            <th class="begin"></th>
            <th colspan="5">
              <span style="font-size:20px;font-weight:bold;">CRIMINAL RESULTS</span>
            </th>
            <th class="end"></th>
          </tr>

        </thead>
        <tbody>
          <xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase">
            <xsl:choose>
              <xsl:when  test="a:SubjectIdentification/a:DemographicDetail/a:DateOfBirth">
                <!--YESSSSS-->
              </xsl:when>
              <xsl:when  test="a:SubjectIdentification/a:DemographicDetail/@IsAdded='true'">
                <!--Trueeee-->
              </xsl:when>
              <xsl:otherwise>
                <!--NOOOOO-->
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>





          <xsl:variable name="TextData">
            <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text" />
          </xsl:variable>

          <xsl:variable name="SSNNo">
            xxx-xx-<xsl:value-of select="substring(a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:DemographicDetail/a:GovernmentId,6,4)" />
          </xsl:variable>

          <xsl:if test="not(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text)">
            <tr>
              <td class="begin">
              </td>
              <td colspan="5">
                <div style="width: 90%; margin: 0 auto; text-align: left">
                  <div style="width:588px; text-align:left">
                    <div style="font-size:11px;" id="report_ncr1">
                      <pre>
                        <!--<xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text">
                        <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text" disable-output-escaping="yes"/>
                      </xsl:if>-->
                        <xsl:if test="not(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text)">
                          <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus = 'Cancelled'">
                            <p>
                              There was an error running this report.<br /> Please contact our customer support team to quickly resolve this issue.
                            </p>
                          </xsl:if>
                          <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus != 'Cancelled'">
                            <p>*** Clear ***</p>
                          </xsl:if>
                          <p></p>
                        </xsl:if>
                      </pre>
                    </div>
                  </div>
                </div>
              </td>
              <td class="end">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase">
            <xsl:choose>
              <xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:DateOfBirth or a:SubjectIdentification/a:DemographicDetail/@IsAdded='true'" >

                <xsl:choose>
                  <xsl:when test="position()>1">
                    <tr style="height:20px;">
                      <td class="begin">
                      </td>
                      <td colspan="5" style="text-align:center;">
                        <span style="font-size:16px;font-weight:bold;color:white;">
                        </span>
                      </td>
                      <td class="end"></td>
                    </tr>
                  </xsl:when>
                </xsl:choose>

                <tr>
                  <td class="begin">
                  </td>
                  <td colspan="5" style="border:solid 1px black;background-color:black;text-align:left;">
                    <span style="font-size:16px;font-weight:bold;color:#fff;">
                      Criminal Record #
                      <xsl:value-of select="(position())" />
                    </span>
                  </td>
                  <td class="end"></td>
                </tr>

                <tr>
                  <td class="begin">
                  </td>
                  <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                      <tr>
                        <td width="50%">
                          <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                <b>Source:</b>
                              </td>
                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                <xsl:value-of select="a:CourtName"/>
                              </td>
                            </tr>

                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                <b>Full Name:</b>
                              </td>
                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                  <xsl:if test="a:FamilyName/@primary='true'">
                                    <xsl:value-of select="a:FamilyName"/>
                                    <xsl:if test="(a:GivenName)">
                                      <span>, </span>
                                    </xsl:if>
                                    <xsl:value-of select="a:GivenName"/>
                                    <xsl:if test="(a:MiddleName)">
                                      <span>, </span>
                                    </xsl:if>
                                    <xsl:value-of select="a:MiddleName"/>
                                    <xsl:element name="br"/>
                                  </xsl:if>
                                </xsl:for-each>
                              </td>
                            </tr>

                            <xsl:if test="(a:SubjectIdentification/a:PersonName/a:FamilyName/@primary='false')">
                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                  <b>AKA Name:</b>
                                </td>
                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                    <xsl:if test="a:FamilyName/@primary='false'">
                                      <xsl:value-of select="a:FamilyName"/>
                                      <xsl:if test="(a:GivenName)">
                                        <span>, </span>
                                      </xsl:if>
                                      <xsl:value-of select="a:GivenName"/>
                                      <xsl:if test="(a:MiddleName)">
                                        <span>, </span>
                                      </xsl:if>
                                      <xsl:value-of select="a:MiddleName"/>
                                      <xsl:element name="br"/>
                                    </xsl:if>
                                  </xsl:for-each>
                                </td>
                              </tr>
                            </xsl:if>

                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                <b>DOB:</b>
                              </td>
                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                <xsl:value-of select='substring(a:SubjectIdentification/a:DemographicDetail/a:DateOfBirth,1,10)' />
                                <xsl:value-of select='a:SubjectIdentification/a:DemographicDetail/a:Age' />
                                <xsl:value-of select='a:SubjectIdentification/a:DemographicDetail/a:Other' />
                              </td>
                            </tr>

                            <!--<xsl:if test="($SSNNo) != ''">
                          <tr>
                            <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                              <b>SSN:</b>
                            </td>
                            <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                              <xsl:value-of select ="$SSNNo"/>
                            </td>
                          </tr>
                        </xsl:if>-->
                          </table>
                        </td>
                        <td width="50%">
                          <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <xsl:if test="(a:SubjectIdentification/a:DemographicDetail/a:GovernmentId/@documentType='DRIVERS LICENSE')">
                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                  <b>DL#:</b>
                                </td>
                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:value-of select ="a:SubjectIdentification/a:DemographicDetail/a:GovernmentId[@documentType='DRIVERS LICENSE']"/>
                                </td>
                              </tr>
                            </xsl:if>

                            <xsl:if test="(a:SubjectIdentification/a:PostalAddress)">
                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                  <b>Address:</b>
                                </td>
                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:for-each select="a:SubjectIdentification/a:PostalAddress">
                                    <xsl:if test="(a:DeliveryAddress)">
                                      <xsl:value-of select ="a:DeliveryAddress"/>
                                      <xsl:if test="(a:Municipality)">
                                        <span>, </span>
                                      </xsl:if>
                                    </xsl:if>
                                    <xsl:if test="(a:Municipality)">
                                      <xsl:value-of select ="a:Municipality"/>
                                      <xsl:if test="(a:Region)">
                                        <span>, </span>
                                      </xsl:if>
                                    </xsl:if>
                                    <xsl:if test="(a:Region)">
                                      <xsl:value-of select ="a:Region"/>
                                      <xsl:if test="(a:PostalCode)">
                                        <span> - </span>
                                      </xsl:if>
                                    </xsl:if>
                                    <xsl:if test="(a:PostalCode)">
                                      <xsl:value-of select ="a:PostalCode"/>
                                    </xsl:if>
                                    <xsl:element name="br" />
                                  </xsl:for-each>
                                </td>
                              </tr>
                            </xsl:if>

                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                <b>Race:</b>
                              </td>
                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                <xsl:value-of select="a:SubjectIdentification/a:DemographicDetail/a:Race"/>
                              </td>
                            </tr>

                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                <b>Gender:</b>
                              </td>
                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                <xsl:choose>
                                  <xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:GenderCode = 1">
                                    Male
                                  </xsl:when>
                                  <xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:GenderCode = 2">
                                    Female
                                  </xsl:when>
                                  <xsl:otherwise>UnKnown</xsl:otherwise>
                                </xsl:choose>
                              </td>
                            </tr>

                            <xsl:if test="(a:AgencyReference[@type='Docket'])">
                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                  <b>Docket Number:</b>
                                </td>

                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:value-of select="a:AgencyReference[@type='Docket']"/>
                                </td>
                              </tr>
                            </xsl:if>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td class="end"></td>
                </tr>

                <xsl:for-each select="a:Charge">

                  <tr>
                    <td class="begin">
                    </td>
                    <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border:solid 1px black;background-color:red;text-align:center;">
                        <tr>
                          <xsl:choose>
                            <xsl:when test="(position()mod 2)=1">
                              <td width="10%" style="border-right:solid 1px black;background-color:#dcdcdc;text-align:center;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                    <td style="text-align:right;background-color:#dcdcdc;">
                                      <span style="font-size:12px;font-weight:bold;color:#000;">
                                        Charge #
                                        <xsl:value-of select="(position())" />
                                      </span>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </xsl:when>
                            <xsl:otherwise>
                              <td width="10%" style="border-right:solid 1px black;background-color:white;text-align:center;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                    <td style="text-align:right;background-color:white;">
                                      <span style="font-size:12px;font-weight:bold;color:#000;">
                                        Charge #
                                        <xsl:value-of select="(position())" />
                                      </span>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </xsl:otherwise>
                          </xsl:choose>
                          <xsl:choose>
                            <xsl:when test="(position()mod 2)=1">
                              <td width="45%" style="background-color:#dcdcdc;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                    <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">
                                      <b>Charge:</b>
                                    </td>
                                    <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                      <xsl:value-of select="a:ChargeOrComplaint"/>
                                    </td>
                                  </tr>

                                  <xsl:if test="(a:ArrestDate)">
                                    <tr>
                                      <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                        <b>Arrest Date:</b>
                                      </td>

                                      <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>

                                  <tr>
                                    <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                      <b>Offense Date:</b>
                                    </td>

                                    <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                      <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                    </td>
                                  </tr>

                                  <tr>
                                    <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                      <b>
                                        Disposition Date:
                                      </b>
                                    </td>

                                    <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                      <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="45%" style="background-color:#dcdcdc;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                    <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">
                                      <b>
                                        Severity:
                                      </b>
                                    </td>

                                    <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                      <xsl:value-of select="a:ChargeTypeClassification"/>
                                    </td>
                                  </tr>

                                  <tr>
                                    <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                      <b>
                                        Final Disposition:
                                      </b>
                                    </td>

                                    <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                      <xsl:value-of select="a:Disposition"/>
                                    </td>
                                  </tr>

                                  <tr>
                                    <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                      <b>
                                        Comments:
                                      </b>
                                    </td>

                                    <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                      <xsl:for-each select="a:Comment">
                                        <span style="float:left;">
                                          <xsl:value-of select="."/>
                                          <xsl:element name="br" />
                                        </span>
                                      </xsl:for-each>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </xsl:when>
                            <xsl:otherwise>
                              <td width="45%">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                    <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                      <b>Charge:</b>
                                    </td>
                                    <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                      <xsl:value-of select="a:ChargeOrComplaint"/>
                                    </td>
                                  </tr>

                                  <xsl:if test="(a:ArrestDate)">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                        <b>Arrest Date:</b>
                                      </td>

                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>

                                  <tr>
                                    <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                      <b>Offense Date:</b>
                                    </td>

                                    <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                      <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                    </td>
                                  </tr>

                                  <tr>
                                    <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                      <b>
                                        Disposition Date:
                                      </b>
                                    </td>

                                    <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                      <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td width="45%">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                    <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                      <b>
                                        Severity:
                                      </b>
                                    </td>

                                    <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                      <xsl:value-of select="a:ChargeTypeClassification"/>
                                    </td>
                                  </tr>

                                  <tr>
                                    <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                      <b>
                                        Final Disposition:
                                      </b>
                                    </td>

                                    <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                      <xsl:value-of select="a:Disposition"/>
                                    </td>
                                  </tr>

                                  <tr>
                                    <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                      <b>
                                        Comments:
                                      </b>
                                    </td>

                                    <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                      <xsl:for-each select="a:Comment">
                                        <span style="float:left;">
                                          <xsl:value-of select="."/>
                                          <xsl:element name="br" />
                                        </span>
                                      </xsl:for-each>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </xsl:otherwise>
                          </xsl:choose>
                        </tr>
                      </table>


                    </td>
                    <td class="end"></td>
                  </tr>
                </xsl:for-each>


              </xsl:when>
              <xsl:otherwise>
              </xsl:otherwise>
            </xsl:choose>

          </xsl:for-each>
          <tr>
            <td class="begin">
            </td>
            <td onclick="ShowHideTable('tblNullDOB');" colspan="5" style="border:solid 1px black;background-color:white;text-align:left;">
              <div style="float:left;width:90%">
                <div style="float:left;background-position:left;" class="plus_btnSmall">
                </div>
                <div style="font-size:11px;font-weight:bold;color:#000;width:auto; margin-top:7px">
                  Database has also found <span id="lblCount"></span> possible matches with DOB variation that could match to <span id="lblSearchName" ></span>
                </div>

              </div>
              <div  style="font-size:13px;font-weight:bold;color:#000;width:auto;float:right; margin-top:7px">
                <a id="ancShowHideTable"  style="text-decoration:none;font-size:11px;color:black;cursor:pointer;" title="show/hide" >SHOW</a>
              </div>
            </td>
            <td class="end"></td>
          </tr>
          <tr>
            <td class="begin">
            </td>
            <td colspan="5" style="border:solid 1px black;background-color:white;text-align:left;">
              <table id="tblNullDOB" cellpadding="0" cellspacing="0"    style="border:1px solid  black;display:none;background-color:white;text-align:center;">
                <tr id="tblNullH1">
                  <td></td>
                  <td></td>
                  <td colspan="10" >Below are the possible matches that are exact match. Please review and add any records that match to the name you are searching.</td>

                </tr>
                <tr id="tblNullH2">
                  <td></td>
                  <td style="background-color:white;color:white;padding:5px;">_____</td>
                  <td style="background-color:#545454;color:white;padding:5px;">Jurisdiction</td>
                  <td style="background-color:#545454;color:white;padding:5px;">Case Number</td>
                  <td style="background-color:#545454;color:white;padding:5px;">Last Name</td>
                  <td style="background-color:#545454;color:white;padding:5px;">First Name</td>
                  <td style="background-color:#545454;color:white;padding:5px;">Middle Name</td>
                  <td style="background-color:#545454;color:white;padding:5px;">Address</td>
                  <td style="background-color:#545454;color:white;padding:5px;">DOB</td>
                  <td style="background-color:#545454;color:white;padding:5px;">R/S</td>
                  <td style="background-color:#545454;color:white;padding:5px;">SSN</td>
                  <td style="background-color:#545454;color:#545454;padding:5px;">_____</td>

                </tr>
                <tr>
                  <td></td>
                </tr>
                <xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase">
                  <xsl:choose>
                    <xsl:when test="not(a:SubjectIdentification/a:DemographicDetail/a:DateOfBirth) and not(a:SubjectIdentification/a:DemographicDetail/@IsAdded='true')" >
                      <tr>
                        <td></td>
                        <td colspan="11">
                          <xsl:choose>
                            <xsl:when test="(position()mod 2)=1">
                              <table  cellpadding="0" cellspacing="0"  style="border:1px solid  black;background-color:#dcdcdc;">
                                <tr>

                                  <td  style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;padding-top:10px;">
                                    <table botder="0" cellpadding="0" cellspacing="0">
                                      <tr>
                                        <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;padding-top:1px;" >
                                          <input type="checkbox" style="padding:0 auto;" id="ChkShowHideNCR1_{($pkOrderDetailId)}_{(position())}" title="Add" value="{(position())}"   />
                                        </td>
                                        <td  style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;padding-top:1px;">
                                          <u>
                                            <a id="ancNCR1_{(position())}"  href="javascript:ShowHideCharges('tblCharges_NCR1_'+{(position())},'{(position())}');"  style="text-decoration:none;font-size:11px;color:black;" title="show/hide" >SHOW</a>
                                          </u>
                                        </td>
                                      </tr>

                                    </table>


                                  </td>
                                  <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;padding-top:10px;">
                                    <xsl:value-of select="a:CourtName"/>
                                  </td>
                                  <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;padding-top:10px;">
                                    <xsl:value-of select="a:AgencyReference[@type='Docket']"/>
                                  </td>
                                  <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;padding-top:10px;">
                                    <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                      <xsl:if test="a:FamilyName/@primary='true'">
                                        <xsl:value-of select="a:FamilyName"/>

                                      </xsl:if>
                                    </xsl:for-each>
                                  </td>
                                  <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;padding-top:10px;">
                                    <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                      <xsl:if test="a:FamilyName/@primary='true'">

                                        <xsl:value-of select="a:GivenName"/>

                                      </xsl:if>
                                    </xsl:for-each>
                                  </td>
                                  <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;padding-top:10px;">
                                    <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                      <xsl:if test="a:FamilyName/@primary='true'">

                                        <xsl:value-of select="a:MiddleName"/>

                                      </xsl:if>
                                    </xsl:for-each>
                                  </td>
                                  <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;padding-top:10px;">
                                    <xsl:if test="(a:SubjectIdentification/a:PostalAddress)">
                                      <xsl:for-each select="a:SubjectIdentification/a:PostalAddress">
                                        <xsl:if test="(a:DeliveryAddress)">
                                          <xsl:value-of select ="a:DeliveryAddress"/>
                                          <xsl:if test="(a:Municipality)">
                                            <span>, </span>
                                          </xsl:if>
                                        </xsl:if>
                                        <xsl:if test="(a:Municipality)">
                                          <xsl:value-of select ="a:Municipality"/>
                                          <xsl:if test="(a:Region)">
                                            <span>, </span>
                                          </xsl:if>
                                        </xsl:if>
                                        <xsl:if test="(a:Region)">
                                          <xsl:value-of select ="a:Region"/>
                                          <xsl:if test="(a:PostalCode)">
                                            <span> - </span>
                                          </xsl:if>
                                        </xsl:if>
                                        <xsl:if test="(a:PostalCode)">
                                          <xsl:value-of select ="a:PostalCode"/>
                                        </xsl:if>
                                        <xsl:element name="br" />
                                      </xsl:for-each>

                                    </xsl:if>
                                  </td>
                                  <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;padding-top:10px;">
                                    <xsl:value-of select='substring(a:SubjectIdentification/a:DemographicDetail/a:DateOfBirth,1,10)' />
                                    <xsl:value-of select='a:SubjectIdentification/a:DemographicDetail/a:Age' />
                                    <xsl:value-of select='a:SubjectIdentification/a:DemographicDetail/a:Other' />
                                  </td>
                                  <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;padding-top:10px;">
                                    <xsl:value-of select="a:SubjectIdentification/a:DemographicDetail/a:Race"/>
                                  </td>
                                  <td  style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;padding-top:10px;">
                                    <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:DemographicDetail/a:GovernmentId"/>
                                  </td>
                                  <td  style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;padding-top:10px;">
                                    <input type="button" id="btnUpdateRecord_NCR1_{(position())}" value="Add" onclick="AddNullDOB(this.id,'{($pkOrderDetailId)}','{(position())}');"  wrap="off"></input>
                                  </td>
                                </tr>
                              </table>
                            </xsl:when>
                            <xsl:otherwise>
                              <table  cellpadding="0" cellspacing="0"  style="border:1px solid  black;background-color:#fff;">
                                <tr>

                                  <td>
                                    <table botder="0" cellpadding="0" cellspacing="0">
                                      <tr>
                                        <td style="padding-top:1px">
                                          <input type="checkbox" style="padding:0 auto;" id="ChkShowHideNCR1_{($pkOrderDetailId)}_{(position())}" title="Add" value="{(position())}"   />
                                        </td>
                                        <td>
                                          <u>
                                            <a id="ancNCR1_{(position())}"  href="javascript:ShowHideCharges('tblCharges_NCR1_'+{(position())},'{(position())}');"  style="text-decoration:none;font-size:11px;color:black;" title="show/hide" >SHOW</a>
                                          </u>
                                        </td>
                                      </tr>

                                    </table>


                                  </td>
                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;padding-top:10px;">
                                    <xsl:value-of select="a:CourtName"/>
                                  </td>
                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;padding-top:10px;">
                                    <xsl:value-of select="a:AgencyReference[@type='Docket']"/>
                                  </td>
                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;padding-top:10px;">
                                    <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                      <xsl:if test="a:FamilyName/@primary='true'">
                                        <xsl:value-of select="a:FamilyName"/>

                                      </xsl:if>
                                    </xsl:for-each>
                                  </td>
                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;padding-top:10px;">
                                    <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                      <xsl:if test="a:FamilyName/@primary='true'">

                                        <xsl:value-of select="a:GivenName"/>

                                      </xsl:if>
                                    </xsl:for-each>
                                  </td>
                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;padding-top:10px;">
                                    <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                      <xsl:if test="a:FamilyName/@primary='true'">

                                        <xsl:value-of select="a:MiddleName"/>

                                      </xsl:if>
                                    </xsl:for-each>
                                  </td>
                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;padding-top:10px;">
                                    <xsl:if test="(a:SubjectIdentification/a:PostalAddress)">
                                      <xsl:for-each select="a:SubjectIdentification/a:PostalAddress">
                                        <xsl:if test="(a:DeliveryAddress)">
                                          <xsl:value-of select ="a:DeliveryAddress"/>
                                          <xsl:if test="(a:Municipality)">
                                            <span>, </span>
                                          </xsl:if>
                                        </xsl:if>
                                        <xsl:if test="(a:Municipality)">
                                          <xsl:value-of select ="a:Municipality"/>
                                          <xsl:if test="(a:Region)">
                                            <span>, </span>
                                          </xsl:if>
                                        </xsl:if>
                                        <xsl:if test="(a:Region)">
                                          <xsl:value-of select ="a:Region"/>
                                          <xsl:if test="(a:PostalCode)">
                                            <span> - </span>
                                          </xsl:if>
                                        </xsl:if>
                                        <xsl:if test="(a:PostalCode)">
                                          <xsl:value-of select ="a:PostalCode"/>
                                        </xsl:if>
                                        <xsl:element name="br" />
                                      </xsl:for-each>

                                    </xsl:if>
                                  </td>
                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;padding-top:10px;">
                                    <xsl:value-of select='substring(a:SubjectIdentification/a:DemographicDetail/a:DateOfBirth,1,10)' />
                                    <xsl:value-of select='a:SubjectIdentification/a:DemographicDetail/a:Age' />
                                    <xsl:value-of select='a:SubjectIdentification/a:DemographicDetail/a:Other' />
                                  </td>
                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;padding-top:10px;">
                                    <xsl:value-of select="a:SubjectIdentification/a:DemographicDetail/a:Race"/>
                                  </td>
                                  <td>
                                    <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:DemographicDetail/a:GovernmentId"/>
                                  </td>
                                  <td>
                                    <input type="button" id="btnUpdateRecord_NCR1_{(position())}" value="Add" onclick="AddNullDOB(this.id,'{($pkOrderDetailId)}','{(position())}');"  wrap="off"></input>
                                  </td>
                                </tr>
                              </table>
                            </xsl:otherwise>
                          </xsl:choose>
                        </td>
                      </tr>
                      <tr>
                        <td></td>
                        <td colspan="10">
                          <table id="tblCharges_NCR1_{(position())}" width="100%" cellpadding="0" cellspacing="0"  style="display:none;border:0px solid  black;">
                            <xsl:for-each select="a:Charge">
                              <tr>

                                <td style="border:solid 0px black;background-color:white;text-align:center;">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border:solid 1px black;background-color:red;text-align:center;">
                                    <tr>
                                      <xsl:choose>
                                        <xsl:when test="(position()mod 2)=1">
                                          <td width="10%" style="border-right:solid 1px black;background-color:#dcdcdc;text-align:center;">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                              <tr>
                                                <td style="text-align:right;background-color:#dcdcdc;">
                                                  <span style="font-size:12px;font-weight:bold;color:#000;">
                                                    Charge #
                                                    <xsl:value-of select="(position())" />
                                                  </span>
                                                </td>
                                              </tr>
                                            </table>
                                          </td>
                                        </xsl:when>
                                        <xsl:otherwise>
                                          <td width="10%" style="border-right:solid 1px black;background-color:white;text-align:center;">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                              <tr>
                                                <td style="text-align:right;background-color:white;">
                                                  <span style="font-size:12px;font-weight:bold;color:#000;">
                                                    Charge #
                                                    <xsl:value-of select="(position())" />
                                                  </span>
                                                </td>
                                              </tr>
                                            </table>
                                          </td>
                                        </xsl:otherwise>
                                      </xsl:choose>
                                      <xsl:choose>
                                        <xsl:when test="(position()mod 2)=1">
                                          <td width="45%" style="background-color:#dcdcdc;">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                              <tr>
                                                <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">
                                                  <b>Charge:</b>
                                                </td>
                                                <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                                  <xsl:value-of select="a:ChargeOrComplaint"/>
                                                </td>
                                              </tr>

                                              <xsl:if test="(a:ArrestDate)">
                                                <tr>
                                                  <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                                    <b>Arrest Date:</b>
                                                  </td>

                                                  <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                                    <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                                  </td>
                                                </tr>
                                              </xsl:if>

                                              <tr>
                                                <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                                  <b>Offense Date:</b>
                                                </td>

                                                <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                                  <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                                </td>
                                              </tr>

                                              <tr>
                                                <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                                  <b>
                                                    Disposition Date:
                                                  </b>
                                                </td>

                                                <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                                  <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                                </td>
                                              </tr>
                                            </table>
                                          </td>
                                          <td width="45%" style="background-color:#dcdcdc;">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                              <tr>
                                                <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">
                                                  <b>
                                                    Severity:
                                                  </b>
                                                </td>

                                                <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                                  <xsl:value-of select="a:ChargeTypeClassification"/>
                                                </td>
                                              </tr>

                                              <tr>
                                                <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                                  <b>
                                                    Final Disposition:
                                                  </b>
                                                </td>

                                                <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                                  <xsl:value-of select="a:Disposition"/>
                                                </td>
                                              </tr>

                                              <tr>
                                                <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                                  <b>
                                                    Comments:
                                                  </b>
                                                </td>

                                                <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                                  <xsl:for-each select="a:Comment">
                                                    <span style="float:left;">
                                                      <xsl:value-of select="."/>
                                                      <xsl:element name="br" />
                                                    </span>
                                                  </xsl:for-each>
                                                </td>
                                              </tr>
                                            </table>
                                          </td>
                                        </xsl:when>
                                        <xsl:otherwise>
                                          <td width="45%">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                              <tr>
                                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                                  <b>Charge:</b>
                                                </td>
                                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                                  <xsl:value-of select="a:ChargeOrComplaint"/>
                                                </td>
                                              </tr>

                                              <xsl:if test="(a:ArrestDate)">
                                                <tr>
                                                  <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                                    <b>Arrest Date:</b>
                                                  </td>

                                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                                    <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                                  </td>
                                                </tr>
                                              </xsl:if>

                                              <tr>
                                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                                  <b>Offense Date:</b>
                                                </td>

                                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                                  <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                                </td>
                                              </tr>

                                              <tr>
                                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                                  <b>
                                                    Disposition Date:
                                                  </b>
                                                </td>

                                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                                  <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                                </td>
                                              </tr>
                                            </table>
                                          </td>
                                          <td width="45%">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                              <tr>
                                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                                  <b>
                                                    Severity:
                                                  </b>
                                                </td>

                                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                                  <xsl:value-of select="a:ChargeTypeClassification"/>
                                                </td>
                                              </tr>

                                              <tr>
                                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                                  <b>
                                                    Final Disposition:
                                                  </b>
                                                </td>

                                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                                  <xsl:value-of select="a:Disposition"/>
                                                </td>
                                              </tr>

                                              <tr>
                                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                                  <b>
                                                    Comments:
                                                  </b>
                                                </td>

                                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                                  <xsl:for-each select="a:Comment">
                                                    <span style="float:left;">
                                                      <xsl:value-of select="."/>
                                                      <xsl:element name="br" />
                                                    </span>
                                                  </xsl:for-each>
                                                </td>
                                              </tr>
                                            </table>
                                          </td>
                                        </xsl:otherwise>
                                      </xsl:choose>
                                    </tr>
                                  </table>
                                </td>

                              </tr>
                            </xsl:for-each>
                          </table>
                        </td>
                      </tr>
                    </xsl:when>
                    <xsl:otherwise>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:for-each>
              </table>
            </td>
            <td class="end"></td>
          </tr>
          <tr>
            <td class="begin">
            </td>
            <td onclick="collapse('trRaw');" colspan="5" style="border:solid 1px black;background-color:white;text-align:left;">
              <div style="float:left">
                <div style="float:left;background-position:left;" class="plus_btnSmall">
                </div>
                <div style="font-size:17px;font-weight:bold;color:#000;width:300px; margin-top:2px">
                  Raw Courthouse Data
                </div>
              </div>
            </td>
            <td class="end"></td>
          </tr>

          <tr>
            <td class="begin">
            </td>
            <td colspan="5" style="border:solid 1px black;background-color:white;text-align:center;">
              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr id="trRaw_b" style="display:none;">
                  <td style="background-color:white;">
                    <pre>
                      <xsl:value-of select ="$TextData"/>
                    </pre>
                  </td>
                </tr>
              </table>
            </td>
            <td class="end"></td>
          </tr>

        </tbody>
        <tfoot>
          <td class="begin"></td>
          <td colspan="5"></td>
          <td class="end"></td>
        </tfoot>
      </table>
    </center>

    <div class="sectionsummary">
      <h3 style="line-height: 1em;">
        <xsl:choose>
          <xsl:when test="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)=0">
            <p>
              National Sex Offender : Clear<br/>
              OFAC : Clear<br/>
              OIG : Clear<br/>
              For a complete list of coverage, please visit<br/>
              <a href="http://emerge.intelifi.com/coverage.pdf" target="_blank">US Criminal Rapid Search Coverage Guide</a>
            </p>
          </xsl:when>
          <xsl:otherwise>
            Data Sources Searched:<br/>
            Court Records, Department of Corrections, Administrative Office of Courts,<br/>
            National Sex Offender Registry, OFAC, OIG<br/>
            For a complete list of coverage, please visit<br/>
            <a href="http://emerge.intelifi.com/coverage.pdf" target="_blank">US Criminal Rapid Search Coverage Guide</a>

          </xsl:otherwise>
        </xsl:choose>

      </h3>
    </div>
  </xsl:template>
</xsl:stylesheet>