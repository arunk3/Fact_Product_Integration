<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:a="http://ns.hr-xml.org/2004-08-02">
  <xsl:output method="html" encoding="iso-8859-1" />
  <xsl:template match="/">
    <xsl:variable name="pkOrderDetailId" select="a:BackgroundReports/@OrderDetailId"></xsl:variable>
    <xsl:variable name="myCount" select="0"></xsl:variable>

    <input type="text" id="txtTotalRecord_{($pkOrderDetailId)}" style="display:none" value="{(count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase))}"/>

    <center>
      <xsl:variable name="TotalRecords">
        <xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)" />
      </xsl:variable>

      <xsl:if test="$TotalRecords=0">

        <table id="peopleTable" class="reporttable ncr1-td" border="0" cellpadding="0" cellspacing="0">
          <thead>

            <tr class="ncr_header_data">
              <th class="begin" style="border:none;"></th>
              <th colspan="5">
                <span style="font-size:15px;font-weight:bold;">Search Information</span>
              </th>
              <th class="end" style="border:none;"></th>
            </tr>

          </thead>
          <tbody>

            <tr>
              <td class="begin" style="border:none;">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colspan="5">
                <h4>Name:</h4>
                <h4>
                  <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:GivenName"/>
                  <xsl:text xml:space="preserve">  </xsl:text>
                  <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:FamilyName"/>
                </h4>
                <br />
                <h4>DOB:</h4>
                <h4>
                  <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:DemographicDetail/a:DateOfBirth"/>
                </h4>
              </td>
              <td class="end" style="border:none;">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>

          </tbody>

        </table>

      </xsl:if>
      <table id="tblNCR" class="reporttable ncr1-td" border="0" cellpadding="0" cellspacing="0">
        <thead>
          <tr height="25px" style="margin-top:10px;" >

            <td colspan="5" style="text-transform:none !important; border: solid 1px #ccc; padding:0px;" >
              <div class="sectionsummary" style="text-align:center;">
                <h3>
                  <span class="cnt"></span> records matching the exact name and date of birth entered by the user have been located in the national database.<br />
                </h3>
              </div>
            </td>

          </tr>
          <tr height="10px" >
            <td class="begin" style="border:none;"></td>
            <td colspan="5">
            </td>
            <td class="end" style="border:none;"></td>
          </tr>
          <tr>
            <td class="ncr_header_data" style="padding-left: 2%;" colspan="7">
              <span style="font-size:15px;font-weight:bold; color:#666;">National Database Criminal Results</span>
            </td>
          </tr>

        </thead>

        <tbody>

          <xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase">
            <xsl:sort select="a:CourtState"/>
            <xsl:sort select="a:DispDate" order="descending" />
            <xsl:choose>
              <xsl:when  test="a:SubjectIdentification/a:DemographicDetail/a:DateOfBirth or a:SubjectIdentification/a:DemographicDetail/@IsAdded='true'">
                <!--YESSSSS-->
              </xsl:when>
              <xsl:when  test="a:SubjectIdentification/a:DemographicDetail/@IsAdded='true'">
                <!--Trueeee-->
              </xsl:when>
              <xsl:otherwise>
                <!--NOOOOO-->
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
          <xsl:variable name="TextData">
            <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text" />
          </xsl:variable>

          <xsl:variable name="SSNNo">
            xxx-xx-<xsl:value-of select="substring(a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:DemographicDetail/a:GovernmentId,6,4)" />
          </xsl:variable>
          <xsl:if test="(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text)">
            <xsl:if test="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase/a:SubjectIdentification/a:DemographicDetail/a:DateOfBirth)=0 and count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase/a:SubjectIdentification/a:DemographicDetail/@IsAdded)=0">
              <tr>
                <td class="begin" style="border:none;">
                </td>
                <td colspan="5">
                  <div style="width: 90%; margin: 0 auto; text-align: left">
                    <div style="width:588px; text-align:left">
                      <div style="font-size:11px;" id="report_ncr1">
                        <pre>
                          <p>*** Clear *** on EXACT DOB Match</p>
                        </pre>
                      </div>
                    </div>
                  </div>
                </td>
                <td class="end" style="border:none;">
                  <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                </td>
              </tr>
            </xsl:if>
          </xsl:if>
          <xsl:if test="not(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text)">
            <tr>
              <td class="begin" style="border:none;">
              </td>
              <td colspan="5">
                <div style="width: 90%; margin: 0 auto; text-align: left">
                  <div style="width:588px; text-align:left">
                    <div style="font-size:11px;" id="report_ncr1">
                      <pre>
                        <!--<xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text">
                        <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text" disable-output-escaping="yes"/>
                      </xsl:if>-->
                        <xsl:if test="not(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text)">
                          <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus = 'Cancelled'">
                            <p>
                              There was an error running this report.<br /> Please contact our customer support team to quickly resolve this issue.
                            </p>
                          </xsl:if>
                          <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus != 'Cancelled'">
                            <p>*** Clear ***</p>
                          </xsl:if>

                          <p></p>
                        </xsl:if>

                      </pre>
                    </div>
                  </div>
                </div>
              </td>
              <td class="end" style="border:none;">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <tr height="10px" >
            <td class="begin" style="border:none;"></td>
            <td colspan="5">
            </td>
            <td class="end" style="border:none;"></td>
          </tr>
          <xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase">
            <xsl:sort select="a:CourtState"/>
            <xsl:sort select="a:DispDate" order="descending" />
            <xsl:choose>
              <xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:DateOfBirth or a:SubjectIdentification/a:DemographicDetail/@IsAdded='true'" >

                <xsl:choose>
                  <xsl:when test="position()>1">
                    <tr style="height:20px;">
                      <td class="begin" style="border:none;">
                      </td>
                      <td colspan="5" style="text-align:center;">
                        <span style="font-size:16px;font-weight:bold;color:white;">
                        </span>
                      </td>
                      <td class="end" style="border:none;"></td>
                    </tr>
                  </xsl:when>
                </xsl:choose>
                <tr>
                  <td id="StateHeader" name="StateHeader" colspan="5" style="display: none;height:40px;font-weight: bold; font-size: 15px !important; max-width: 94%;margin-left:38px; position: absolute; right: 0px; left: 0px;background-image: url('https://emerge.intelifi.com/content/themes/base/images/top_center.png');background-repeat: repeat-x;">
                    <b>
                      Records Found In <xsl:value-of select="a:CourtState"/>
                    </b>
                  </td>
                </tr>

                <tr>
                  <td colspan="5" height="40">
                  </td>
                </tr>
                <tr>
                  <td colspan="5" class="ncr_header_data">
                    <div class="" style="padding:1px 0;margin-left: 5px;">
                      <div class="file-name" style="float: left; color:#555; font-weight: normal; font-family: segoeui;">
                        Criminal Record
                      </div>
                      <!-- <script type="text/javascript">
                        document.write(CriminalCount);                     
                        CriminalCount++;
                      </script>-->
                      <span class="rednosetagainnumber" style="padding-top:5px; margin-left:10px;">
                        
                      </span>
                    </div>
                  </td>
                </tr>

                <tr>
                  <td class="begin" style="border:none;">
                  </td>
                  <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;padding:0px;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                      <tr>
                        <td width="44%" class="column1">
                          <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:30%;">
                                Source:
                              </td>
                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                <xsl:value-of select="a:CourtName"/>
                              </td>
                            </tr>

                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                Full Name:
                              </td>
                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                  <xsl:if test="a:FamilyName/@primary='true'">
                                    <xsl:value-of select="a:FamilyName"/>
                                    <xsl:if test="(a:GivenName)">
                                      <span>, </span>
                                    </xsl:if>
                                    <xsl:value-of select="a:GivenName"/>
                                    <xsl:if test="(a:MiddleName)">
                                      <span>, </span>
                                    </xsl:if>
                                    <xsl:value-of select="a:MiddleName"/>
                                    <xsl:element name="br"/>
                                  </xsl:if>
                                </xsl:for-each>
                              </td>
                            </tr>

                            <xsl:if test="(a:SubjectIdentification/a:PersonName/a:FamilyName/@primary='false')">
                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                  AKA Name:
                                </td>
                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                    <xsl:if test="a:FamilyName/@primary='false'">
                                      <xsl:value-of select="a:FamilyName"/>
                                      <xsl:if test="(a:GivenName)">
                                        <span>, </span>
                                      </xsl:if>
                                      <xsl:value-of select="a:GivenName"/>
                                      <xsl:if test="(a:MiddleName)">
                                        <span>, </span>
                                      </xsl:if>
                                      <xsl:value-of select="a:MiddleName"/>
                                      <xsl:element name="br"/>
                                    </xsl:if>
                                  </xsl:for-each>
                                </td>
                              </tr>
                            </xsl:if>

                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                DOB Recorded:
                              </td>
                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                <xsl:value-of select='substring(a:SubjectIdentification/a:DemographicDetail/a:DateOfBirth,1,10)' />
                                <xsl:value-of select='a:SubjectIdentification/a:DemographicDetail/a:Age' />
                                <xsl:value-of select='a:SubjectIdentification/a:DemographicDetail/a:Other' />
                              </td>
                            </tr>
                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;color:#ff0000 !important;">
                                DOB Entered By User:
                              </td>
                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;color:#ff0000 !important;">
                                <span class="lblDateEnteredByUser" id="lblDateEnteredByUser_{(position())}" style="color:#ff0000 !important;"></span>
                              </td>
                            </tr>

                            <!--<xsl:if test="($SSNNo) != ''">
                          <tr>
                            <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                              <b>SSN:</b>
                            </td>
                            <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                              <xsl:value-of select ="$SSNNo"/>
                            </td>
                          </tr>
                        </xsl:if>-->

                          </table>
                        </td>
                        <td width="44%" class="column2">
                          <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <xsl:if test="(a:SubjectIdentification/a:DemographicDetail/a:GovernmentId/@documentType='DRIVERS LICENSE')">
                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                  DL#:
                                </td>
                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:value-of select ="a:SubjectIdentification/a:DemographicDetail/a:GovernmentId[@documentType='DRIVERS LICENSE']"/>
                                </td>
                              </tr>
                            </xsl:if>

                            <xsl:if test="(a:SubjectIdentification/a:PostalAddress)">
                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                  Address:
                                </td>
                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:for-each select="a:SubjectIdentification/a:PostalAddress">
                                    <xsl:if test="(a:DeliveryAddress)">
                                      <xsl:value-of select ="a:DeliveryAddress"/>
                                      <xsl:if test="(a:Municipality)">
                                        <span>, </span>
                                      </xsl:if>
                                    </xsl:if>
                                    <xsl:if test="(a:Municipality)">
                                      <xsl:value-of select ="a:Municipality"/>
                                      <xsl:if test="(a:Region)">
                                        <span>, </span>
                                      </xsl:if>
                                    </xsl:if>
                                    <xsl:if test="(a:Region)">
                                      <xsl:value-of select ="a:Region"/>
                                      <xsl:if test="(a:PostalCode)">
                                        <span> - </span>
                                      </xsl:if>
                                    </xsl:if>
                                    <xsl:if test="(a:PostalCode)">
                                      <xsl:value-of select ="a:PostalCode"/>
                                    </xsl:if>
                                    <xsl:element name="br" />
                                  </xsl:for-each>
                                </td>
                              </tr>
                            </xsl:if>

                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                Race:
                              </td>
                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                <xsl:value-of select="a:SubjectIdentification/a:DemographicDetail/a:Race"/>
                              </td>
                            </tr>

                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                Gender:
                              </td>
                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                <xsl:choose>
                                  <xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:GenderCode = 1">
                                    Male
                                  </xsl:when>
                                  <xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:GenderCode = 2">
                                    Female
                                  </xsl:when>
                                  <xsl:otherwise>UnKnown</xsl:otherwise>
                                </xsl:choose>
                              </td>
                            </tr>

                            <xsl:if test="(a:AgencyReference[@type='Docket'])">
                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                  Docket Number:
                                </td>

                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:value-of select="a:AgencyReference[@type='Docket']"/>
                                </td>
                              </tr>
                            </xsl:if>
                            <xsl:if test="a:CaseFileDate">
                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:30%;">

                                  Case Filed:

                                </td>
                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:choose>
                                    <xsl:when test="count(a:CaseFileDate) &gt; 1">
                                      <xsl:for-each select="a:CaseFileDate">
                                        <span style="float:left;">
                                          <xsl:value-of select='.' />
                                          <xsl:element name="br" />
                                        </span>
                                      </xsl:for-each>
                                    </xsl:when>
                                    <xsl:when test="count(a:CaseFileDate)=1">
                                      <xsl:value-of select='substring(a:CaseFileDate,1,10)' />
                                    </xsl:when>
                                  </xsl:choose>
                                </td>
                              </tr>
                            </xsl:if>
                            <xsl:if test="a:ReviewNote!= ''">
                              <tr style="display:none;">
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:30%;">

                                  Review Note:

                                </td>
                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:if test="count(a:ReviewNote) &gt; 1">
                                    <xsl:for-each select="a:ReviewNote">
                                      <span style="float:left;">
                                        <xsl:value-of select='.' />
                                        <xsl:element name="br" />
                                      </span>
                                    </xsl:for-each>
                                  </xsl:if>
                                  <xsl:if test="count(a:ReviewNote)=1">
                                    <xsl:value-of select='a:ReviewNote' />
                                  </xsl:if>
                                </td>
                              </tr>
                            </xsl:if>
                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:30%;">

                                Additional Events:

                              </td>
                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                <xsl:if test="count(a:AdditionalItems/a:EffectiveDate) &gt; 0">
                                  <input id="imageColapse" type="image" src="../content/themes/base/images/plus_symbol.png" style="width:10px; height:10px;" value="myValue" alt="" />
                                </xsl:if>
                                <div style="display:block;">

                                  <xsl:for-each select="a:AdditionalItems/a:EffectiveDate">
                                    <xsl:value-of  select='../a:Text' />
                                    <xsl:text xml:space="preserve">  </xsl:text>
                                    <xsl:value-of  select='substring(a:StartDate/a:AnyDate,1,10)' />
                                    <xsl:element name="br" />
                                  </xsl:for-each>

                                </div>
                              </td>

                            </tr>
                          </table>
                        </td>
                        <xsl:if test="(a:MatchMeter)">
                          <td width="12%" class="">
                            <table border="0" style="background: transparent;" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td style="background: transparent;font-size:11px;width: 120px !important; float:left;height:24px;text-align:right;text-transform:capitalize !important;padding:0px;">
                                  <b style="font-weight: normal; float: left; position: relative; z-index: 9 ! important; color: rgb(255, 255, 255); display: block; width: 100%; text-align: center; padding-top: 1px;">Name Match</b>

                                  <xsl:text xml:space="preserve">  </xsl:text>
                                  <xsl:if test="(a:MatchMeter/a:Name) = '2'">
                                    <div class="matchgreen" style="background: rgb(71, 218, 38) none repeat scroll 0px 0px; height: 25px; width: 120px; position: absolute; margin-top: -1px;" />
                                    <div class="width_class" style="margin-left:15%;" ></div>
                                    <img  class="matchgreen2" src="../../content/themes/base/images/ico_match_green.jpg"></img>
                                  </xsl:if>
                                  <xsl:if test="(a:MatchMeter/a:Name) = '1'">
                                    <div class="matchyellow"    style=" background: #c9be25 none repeat scroll 0px 0px; height: 25px; width: 120px; position: absolute; margin-top: -1px;"/>
                                    <div class="width_class" style="margin-left:15%;" ></div>
                                    <img class="matchyellow2" src="../../content/themes/base/images/ico_red_yellow.png"></img>
                                  </xsl:if>
                                  <xsl:if test="(a:MatchMeter/a:Name) = '0'">
                                    <div class="matchred"  style=" background: #ff0000 none repeat scroll 0px 0px; height: 25px; width: 120px; position: absolute; margin-top: -1px;"/>
                                    <div class="width_class" style="margin-left:15%;" ></div>
                                    <img class="matchred2" src="../../content/themes/base/images/ico_red_match.png"></img>
                                  </xsl:if>
                                </td>
                              </tr>
                              <tr height="1px" style="float: left; height: 6px ! important;">
                                <td></td>
                              </tr>
                              <tr>
                                <td style="background: transparent;font-size:11px;width: 120px !important; float:left;height:25px;text-align:right;text-transform:capitalize !important;padding:0px;">
                                  <b style="font-weight: normal; float: left; position: relative; z-index: 9 ! important; color: rgb(255, 255, 255); display: block; width: 100%; text-align: center; padding-top: 3px;">Address Match</b>

                                  <xsl:text xml:space="preserve">  </xsl:text>
                                  <xsl:if test="(a:MatchMeter/a:Address) = '2'">
                                    <div class="matchgreen" style="background: rgb(71, 218, 38) none repeat scroll 0px 0px; height: 25px; width: 120px; position: relative; margin-bottom: 3px;" />
                                    <div class="width_class" style="margin-left:15%;" ></div>
                                    <img  class="matchgreen2" src="../../content/themes/base/images/ico_match_green.jpg"></img>
                                  </xsl:if>
                                  <xsl:if test="(a:MatchMeter/a:Address) = '4'">
                                    <div class="matchblue" style=" background: #3771c9 none repeat scroll 0px 0px; height: 25px; width: 120px; position: relative;margin-bottom: 3px;" />
                                    <div class="width_class" style="margin-left:15%;" ></div>
                                    <img  class="matchblue2" src="../../content/themes/base/images/ico_match_blue.jpg"></img>
                                  </xsl:if>
                                  <xsl:if test="(a:MatchMeter/a:Address) = '1'">
                                    <div class="matchyellow"    style=" background: #c9be25 none repeat scroll 0px 0px; height: 25px; width: 120px; position: relative; margin-bottom: 3px;"/>
                                    <div class="width_class" style="margin-left:15%;" ></div>
                                    <img class="matchyellow2" src="../../content/themes/base/images/ico_red_yellow.png"></img>
                                  </xsl:if>
                                  <xsl:if test="(a:MatchMeter/a:Address) = '0'">
                                    <div class="matchred" style=" background: #ff0000 none repeat scroll 0px 0px; height: 25px; width: 120px; position: relative; margin-bottom: 3px;" />
                                    <div class="width_class" style="margin-left:15%;" ></div>
                                    <img class="matchred2" src="../../content/themes/base/images/ico_red_match.png"></img>
                                  </xsl:if>
                                  <xsl:if test="(a:MatchMeter/a:Address) = '-1'">
                                    <div class="matchgray" style=" background: #7f7f7f none repeat scroll 0px 0px; height: 25px; width: 120px; position: relative; margin-bottom: 3px;"></div>
                                    <div class="width_class" style="margin-left:15%;" ></div>
                                    <img class="matchgray2" src="../../content/themes/base/images/ico_darkgrey_match.png"></img>
                                  </xsl:if>
                                </td>
                              </tr>
                              <tr height="1px" style="float: left; height: 6px ! important;">
                                <td></td>
                              </tr>
                              <tr>
                                <td style="background: transparent;font-size:11px;width: 120px !important; float:left;height:28px;text-align:right;text-transform:capitalize !important;padding:0px;">
                                  <b style="font-weight: normal; float: left; position: relative; z-index: 9 ! important; color: rgb(255, 255, 255); display: block; width: 100%; text-align: center; padding-top: 3px;">DOB Match</b>
                                  <!--</td>
                                <td style="background: transparent;text-align:center;height:28px;padding:0px;">-->
                                  <xsl:text xml:space="preserve">  </xsl:text>
                                  <xsl:if test="(a:MatchMeter/a:DOB) = '2'">
                                    <div class="matchgreen"  style="background: rgb(71, 218, 38) none repeat scroll 0px 0px; height: 25px; width: 120px; position: absolute; z-index: 4;"/>
                                    <div class="width_class" style="margin-left:15%;" ></div>
                                    <img class="matchgreen2" src="../../content/themes/base/images/ico_match_green.jpg"></img>
                                  </xsl:if>
                                  <xsl:if test="(a:MatchMeter/a:DOB) = '1'">
                                    <div class="matchyellow"    style=" background: #c9be25 none repeat scroll 0px 0px; height: 25px; width: 120px; position: absolute; z-index: 4;"/>
                                    <div class="width_class" style="margin-left:15%;" ></div>
                                    <img class="matchyellow2" src="../../content/themes/base/images/ico_red_yellow.png"></img>
                                  </xsl:if>
                                  <xsl:if test="(a:MatchMeter/a:DOB) = '0'">
                                    <div class="matchred"  style=" background: #ff0000 none repeat scroll 0px 0px; height: 25px; width: 120px; position: absolute; z-index: 4;"/>
                                    <div class="width_class" style="margin-left:15%;" ></div>
                                    <img class="matchred2" src="../../content/themes/base/images/ico_red_match.png"></img>
                                  </xsl:if>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </xsl:if>
                      </tr>
                    </table>
                  </td>
                  <td class="end" style="border:none;"></td>
                </tr>

                <xsl:for-each select="a:Charge">
                  <xsl:sort select="substring-before(a:DispositionDate,'T')" order="descending" />
                  <tr  style="border:solid 1px #ccc;">
                    <td class="begin" style="border:none;">
                    </td>
                    <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="ns" style="border:solid 0px #ccc;background-color:#fff;text-align:center;">
                        <tr>
                          <xsl:choose>
                            <xsl:when test="(position()mod 2)=1">
                              <td width="10%" style="text-align:center;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                    <td style="text-align: right; padding: 9px 0px;">
                                      <div class="file-name" style="float:left;">
                                        Charge
                                      </div>
                                      <span class="blue-no" style="padding-top: 4px; float: left; margin-left: 7px; margin-top: -4px;">
                                        <xsl:value-of select="(position())" />
                                      </span>
                                    </td>
                                  </tr>
                                  <tr height="10px">
                                    <td class="begin" style="border:none;"></td>
                                    <td colspan="5"></td>
                                    <td class="end" style="border:none;"></td>
                                  </tr>
                                </table>
                              </td>
                            </xsl:when>
                            <xsl:otherwise>
                              <td width="10%" style="border-right:solid 0px #ccc;background-color:white;text-align:center;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                    <td style="text-align:right;background-color:white;  padding: 9px 0px;">
                                      <div class="file-name" style="float:left;">
                                        Charge
                                      </div>
                                      <span class="blue-no" style="padding-top: 4px; float: left; margin-left: 7px; margin-top: -4px;">
                                        <xsl:value-of select="(position())" />
                                      </span>
                                    </td>
                                  </tr>
                                  <tr height="10px">
                                    <td class="begin" style="border:none;"></td>
                                    <td colspan="5"></td>
                                    <td class="end" style="border:none;"></td>
                                  </tr>
                                </table>
                              </td>
                            </xsl:otherwise>
                          </xsl:choose>
                          <xsl:choose>
                            <xsl:when test="(position()mod 2)=1">
                              <td width="45%" style="">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                    <td style="text-align:right;border-bottom:0px solid black;width:25%;">
                                      Charge:
                                    </td>
                                    <td style="text-align:left;border-bottom:0px solid black;">
                                      <xsl:value-of select="a:ChargeOrComplaint"/>
                                    </td>
                                  </tr>
                                  <xsl:if test="(a:ChargeDescription)">
                                    <tr>
                                      <td style="text-align:right;border-bottom:0px solid black;">
                                        Penal Code Description:
                                      </td>
                                      <td style="text-align:LEFT;border-bottom:0px solid black;">
                                        <xsl:value-of select="a:ChargeDescription" />
                                      </td>
                                    </tr>
                                  </xsl:if>
                                  <xsl:if test="(a:ChargeDate)">
                                    <tr>
                                      <td style="text-align:right;border-bottom:0px solid black;">
                                        Charge Date:
                                      </td>

                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:ChargeDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>
                                  <xsl:if test="(a:ArrestDate)">
                                    <tr>
                                      <td style="text-align:right;border-bottom:0px solid black;">
                                        Arrest Date:
                                      </td>

                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>

                                  <xsl:if test="(a:OffenseDate)">
                                    <tr>
                                      <td style="text-align:right;border-bottom:0px solid black;">
                                        Offense Date:
                                      </td>

                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>

                                  <xsl:if test="(a:DispositionDate)">
                                    <tr>
                                      <td style="text-align:right;border-bottom:0px solid black;">

                                        Disposition Date:

                                      </td>

                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>
                                  <xsl:if test="(../a:AdditionalItems[@type='TrialDate'])">
                                    <tr>
                                      <td style="text-align:right;border-bottom:0px solid black;">

                                        Trial Date:

                                      </td>

                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:value-of select="substring(../a:AdditionalItems[@type='TrialDate'],1,10)" />
                                      </td>
                                    </tr>
                                  </xsl:if>
                                </table>
                              </td>
                              <td width="45%" style="">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <xsl:if test="(a:ChargeTypeClassification)!='unknown'">
                                    <tr>
                                      <td style="text-align:right;border-bottom:0px solid black;width:25%;">

                                        Severity:

                                      </td>

                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:value-of select="a:ChargeTypeClassification"/>
                                      </td>
                                    </tr>
                                  </xsl:if>

                                  <xsl:if test="(a:Disposition)">
                                    <tr>
                                      <td style="text-align:right;border-bottom:0px solid black;">

                                        Final Disposition:

                                      </td>

                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:choose>
                                          <xsl:when test="(a:Disposition)='unknown'">
                                            Manual County or State Report needed for Final Results
                                          </xsl:when>
                                          <xsl:otherwise>
                                            <xsl:choose>
                                              <xsl:when test="(contains(../a:CourtName, 'SAN BERNARDINO') or contains(../a:CourtName, 'RIVERSIDE')) and (a:Disposition)='NOT CONVICTED'">
                                                Unknown
                                              </xsl:when>
                                              <xsl:otherwise>
                                                <xsl:value-of select="a:Disposition"/>
                                              </xsl:otherwise>
                                            </xsl:choose>
                                          </xsl:otherwise>
                                        </xsl:choose>
                                      </td>
                                    </tr>
                                  </xsl:if>
                                  <xsl:if test="(a:Sentence)">
                                    <tr>
                                      <td style="text-align:right;border-bottom:0px solid black;">

                                        Sentence:

                                      </td>

                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:value-of select='(a:Sentence)' />
                                      </td>
                                    </tr>
                                  </xsl:if>
                                  <xsl:if test="(a:Comment)">
                                    <tr>
                                      <td style="text-align:right;border-bottom:0px solid black;">

                                        Comments:

                                      </td>

                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:for-each select="a:Comment">
                                          <span style="float:left;">
                                            <xsl:value-of select="."/>
                                            <xsl:element name="br" />
                                          </span>
                                        </xsl:for-each>
                                      </td>
                                    </tr>
                                  </xsl:if>
                                </table>
                              </td>
                            </xsl:when>
                            <xsl:otherwise>
                              <td width="45%">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                    <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                      Charge:
                                    </td>
                                    <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                      <xsl:value-of select="a:ChargeOrComplaint"/>
                                    </td>
                                  </tr>
                                  <xsl:if test="(a:ChargeDescription)">
                                    <tr>
                                      <td style="text-align:right;border-bottom:0px solid black;">
                                        Penal Code Description:
                                      </td>
                                      <td style="text-align:LEFT;border-bottom:0px solid black;">
                                        <xsl:value-of select="a:ChargeDescription" />
                                      </td>
                                    </tr>
                                  </xsl:if>
                                  <xsl:if test="(a:ChargeDate)">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                        Charge Date:
                                      </td>

                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:ChargeDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>
                                  <xsl:if test="(a:ArrestDate)">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                        Arrest Date:
                                      </td>

                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>

                                  <xsl:if test="(a:OffenseDate)">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                        Offense Date:
                                      </td>

                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>

                                  <xsl:if test="(a:DispositionDate)">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">

                                        Disposition Date:

                                      </td>

                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>
                                  <xsl:if test="(../a:AdditionalItems[@type='TrialDate'])">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">

                                        Trial Date:

                                      </td>

                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:value-of select="substring(../a:AdditionalItems[@type='TrialDate'],1,10)" />
                                      </td>
                                    </tr>
                                  </xsl:if>
                                </table>
                              </td>
                              <td width="45%">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <xsl:if test="(a:ChargeTypeClassification)!='unknown'">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">

                                        Severity:

                                      </td>

                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:value-of select="a:ChargeTypeClassification"/>
                                      </td>
                                    </tr>
                                  </xsl:if>
                                  <xsl:if test="(a:Disposition)">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">

                                        Final Disposition:

                                      </td>

                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:choose>
                                          <xsl:when test="(a:Disposition)='unknown'">
                                            Manual County or State Report needed for Final Results
                                          </xsl:when>
                                          <xsl:otherwise>
                                            <xsl:choose>
                                              <xsl:when test="(contains(../a:CourtName, 'SAN BERNARDINO') or contains(../a:CourtName, 'RIVERSIDE')) and (a:Disposition)='NOT CONVICTED'">
                                                Unknown
                                              </xsl:when>
                                              <xsl:otherwise>
                                                <xsl:value-of select="a:Disposition"/>
                                              </xsl:otherwise>
                                            </xsl:choose>
                                          </xsl:otherwise>
                                        </xsl:choose>
                                      </td>
                                    </tr>
                                  </xsl:if>
                                  <xsl:if test="(a:Sentence)">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">

                                        Sentence:

                                      </td>
                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:value-of select='(a:Sentence)' />
                                      </td>
                                    </tr>
                                  </xsl:if>
                                  <xsl:if test="(a:Comment)">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">

                                        Comments:

                                      </td>

                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:for-each select="a:Comment">
                                          <span style="float:left;">
                                            <xsl:value-of select="."/>
                                            <xsl:element name="br" />
                                          </span>
                                        </xsl:for-each>
                                      </td>
                                    </tr>
                                  </xsl:if>
                                </table>
                              </td>



                            </xsl:otherwise>
                          </xsl:choose>
                        </tr>
                      </table>


                    </td>
                    <td class="end" style="border:none;"></td>
                  </tr>
                  <tr height="10px">
                    <td class="begin" style="border:none;"></td>
                    <td colspan="5"></td>
                    <td class="end" style="border:none;"></td>
                  </tr>
                </xsl:for-each>
                <xsl:if test="count(a:Charge)=0">
                  <tr>
                    <td class="begin" style="border:none;">
                    </td>
                    <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border:solid 1px #ccc;background-color:#fff;text-align:center;">
                        <tr>
                          <td width="10%" style="border-right:solid 0px #ccc;text-align:center;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td style="text-align:right;">
                                  <span style="font-size:12px;font-weight:bold;color:#000;">
                                    Additional Info
                                  </span>
                                </td>
                              </tr>
                            </table>
                          </td>
                          <td width="80%" style="text-align:left;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td style="text-align:left;padding-left:5px;">
                                  NOTE: THE DATA REPORTED BY THE COURT FOR THIS CASE IS INCOMPLETE. MANUAL COUNTY SEARCH REQUIRED.<br/>
                                  <span style="padding-left:35px">IF THIS RECORD DOES NOT SPECIFY THE COUNTY, A MANUAL STATE SEARCH IS REQUIRED.</span>
                                </td>
                              </tr>

                              <!--<xsl:for-each select="a:AdditionalItems">
                                <tr>
                                  <td style="background-color:#dcdcdc;text-align:left;padding-left:5px;">
                                    <xsl:value-of select="a:Text"/>
                                    <xsl:element name="br" />
                                  </td>
                                </tr>
                              </xsl:for-each>-->
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                    <td class="end" style="border:none;"></td>
                  </tr>
                  <tr height="10px">
                    <td class="begin" style="border:none;"></td>
                    <td colspan="5"></td>
                    <td class="end" style="border:none;"></td>
                  </tr>
                </xsl:if>


              </xsl:when>
              <xsl:otherwise>

              </xsl:otherwise>
            </xsl:choose>

          </xsl:for-each>
          <tr class="trNullDOBHeading">
            <td class="begin" style="border:none;">
            </td>
            <td id="DobVariationMessage" colspan="5" style="border:solid 1px #ccc;background-color:white;text-align:left;display:none;">

            </td>
            <td class="end" style="border:none;"></td>
          </tr>
          <tr class="trNullDOBHeading">
            <td class="begin" style="border:none; background: #e03625;padding: 0;">
            </td>
            <td  style="padding:0px; background: #e03625;">
              <div>
                <p style=" margin:0px;    text-align:left; background-color: #E03625; color: #FFFFFF; padding-left: 5px; line-height: 32px; font-size:12px;">
                  The following records in this section have been flagged to show possible matches, and not exact matches, due to variation and/or missing elements of the date of birth.
                </p>
              </div>
            </td>
            <td style="background: #e03625;padding: 0;"></td>
          </tr>
          <tr height="5px">
            <td class="begin" style="border:none;"></td>
            <td colspan="5"></td>
            <td class="end" style="border:none;"></td>
          </tr>
          <tr class="trNullDOBHeading" style="border:1px solid #ccc;">
            <td class="begin" style="border:none;">
            </td>
            <td colspan="5" style="padding: 0;border:none;background-color:white;text-align:left;">
              <div style="float:left;width:90%">
                <div style="font-size:11px;font-weight:bold;color:#000;width:auto; margin-top:0px; padding: 5px 0;">
                  Possible Matches (DOB Variations) <span style="color:#ff0000;">
                    <span class="lblCount"></span> records found
                  </span>
                </div>
              </div>
            </td>
            <td class="end" style="border:none;"></td>
          </tr>
          <tr class="trNullDOBRecords">
            <!--<td class="begin" style="border:none;">
            </td>-->
            <td colspan="5" style="border:none;background-color:white;text-align:left;padding:5px 0 0;">
              <div id="tblNullDOB" style="display:block;">

                <table id="tblNulDOBVeriation"  cellpadding="0" cellspacing="0" style="border:none;background-color:white;text-align:center;">
                  <xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase">
                    <xsl:sort select="a:CourtState"/>
                    <xsl:sort select="a:DispDate" order="descending" />
                    <xsl:choose>
                      <xsl:when test="not(a:SubjectIdentification/a:DemographicDetail/a:DateOfBirth) and not(a:SubjectIdentification/a:DemographicDetail/@IsAdded='true')" >
                        <xsl:choose>
                          <xsl:when test="position()>1">
                            <tr style="height:20px;">
                              <td class="begin" style="border:none;">
                              </td>
                              <td colspan="5" style="text-align:center;">
                                <span style="font-size:16px;font-weight:bold;color:white;">
                                </span>
                              </td>
                              <td class="end" style="border:none;"></td>
                            </tr>
                          </xsl:when>
                        </xsl:choose>
                        <tr>
                          <td id="StateHeader" name="StateHeaderDOBVeriation" colspan="5" style="display: none;height:40px;font-weight: bold; font-size: 15px !important; max-width: 94%;margin-left:38px; position: absolute; right: 0px; left: 0px;background-image: url('https://emerge.intelifi.com/content/themes/base/images/top_center.png');background-repeat: repeat-x;">
                            <b>
                              Records Found In <xsl:value-of select="a:CourtState"/>
                            </b>
                          </td>
                        </tr>

                        <tr>
                          <td colspan="5" height="40">
                          </td>
                        </tr>
                        <tr>
                          <td colspan="5" class="ncr_header_data">
                            <div class="" style="padding:1px 0;margin-left: 5px;">
                              <div class="file-name" style="float: left; color:#555; font-weight: normal; font-family: segoeui;">
                                Criminal Record
                              </div>
                              <span class="rednosetagainnumber" style="padding-top:5px; margin;left:10px;">
                                
                              </span>
                            </div>
                          </td>
                        </tr>

                        <tr>
                          <td class="begin" style="border:none;">
                          </td>
                          <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;padding:0px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td width="44%" class="column1">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:30%;">
                                        Source:
                                      </td>
                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:value-of select="a:CourtName"/>
                                      </td>
                                    </tr>

                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                        Full Name:
                                      </td>
                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                          <xsl:if test="a:FamilyName/@primary='true'">
                                            <xsl:value-of select="a:FamilyName"/>
                                            <xsl:if test="(a:GivenName)">
                                              <span>, </span>
                                            </xsl:if>
                                            <xsl:value-of select="a:GivenName"/>
                                            <xsl:if test="(a:MiddleName)">
                                              <span>, </span>
                                            </xsl:if>
                                            <xsl:value-of select="a:MiddleName"/>
                                            <xsl:element name="br"/>
                                          </xsl:if>
                                        </xsl:for-each>
                                      </td>
                                    </tr>

                                    <xsl:if test="(a:SubjectIdentification/a:PersonName/a:FamilyName/@primary='false')">
                                      <tr>
                                        <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                          AKA Name:
                                        </td>
                                        <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                          <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                            <xsl:if test="a:FamilyName/@primary='false'">
                                              <xsl:value-of select="a:FamilyName"/>
                                              <xsl:if test="(a:GivenName)">
                                                <span>, </span>
                                              </xsl:if>
                                              <xsl:value-of select="a:GivenName"/>
                                              <xsl:if test="(a:MiddleName)">
                                                <span>, </span>
                                              </xsl:if>
                                              <xsl:value-of select="a:MiddleName"/>
                                              <xsl:element name="br"/>
                                            </xsl:if>
                                          </xsl:for-each>
                                        </td>
                                      </tr>
                                    </xsl:if>

                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                        DOB Recorded:
                                      </td>
                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:SubjectIdentification/a:DemographicDetail/a:DateOfBirth,1,10)' />
                                        <xsl:value-of select='a:SubjectIdentification/a:DemographicDetail/a:Age' />
                                        <xsl:value-of select='a:SubjectIdentification/a:DemographicDetail/a:Other' />
                                      </td>
                                    </tr>
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;color:#ff0000 !important;">
                                        DOB Entered By User:
                                      </td>
                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;color:#ff0000 !important;">
                                        <span class="lblDateEnteredByUser" id="lblDateEnteredByUser_{(position())}"></span>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                                <td width="44%" class="column2">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <xsl:if test="(a:SubjectIdentification/a:DemographicDetail/a:GovernmentId/@documentType='DRIVERS LICENSE')">
                                      <tr>
                                        <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                          DL#:
                                        </td>
                                        <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                          <xsl:value-of select ="a:SubjectIdentification/a:DemographicDetail/a:GovernmentId[@documentType='DRIVERS LICENSE']"/>
                                        </td>
                                      </tr>
                                    </xsl:if>

                                    <xsl:if test="(a:SubjectIdentification/a:PostalAddress)">
                                      <tr>
                                        <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                          Address:
                                        </td>
                                        <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                          <xsl:for-each select="a:SubjectIdentification/a:PostalAddress">
                                            <xsl:if test="(a:DeliveryAddress)">
                                              <xsl:value-of select ="a:DeliveryAddress"/>
                                              <xsl:if test="(a:Municipality)">
                                                <span>, </span>
                                              </xsl:if>
                                            </xsl:if>
                                            <xsl:if test="(a:Municipality)">
                                              <xsl:value-of select ="a:Municipality"/>
                                              <xsl:if test="(a:Region)">
                                                <span>, </span>
                                              </xsl:if>
                                            </xsl:if>
                                            <xsl:if test="(a:Region)">
                                              <xsl:value-of select ="a:Region"/>
                                              <xsl:if test="(a:PostalCode)">
                                                <span> - </span>
                                              </xsl:if>
                                            </xsl:if>
                                            <xsl:if test="(a:PostalCode)">
                                              <xsl:value-of select ="a:PostalCode"/>
                                            </xsl:if>
                                            <xsl:element name="br" />
                                          </xsl:for-each>
                                        </td>
                                      </tr>
                                    </xsl:if>

                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                        Race:
                                      </td>
                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:value-of select="a:SubjectIdentification/a:DemographicDetail/a:Race"/>
                                      </td>
                                    </tr>

                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                        Gender:
                                      </td>
                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:choose>
                                          <xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:GenderCode = 1">
                                            Male
                                          </xsl:when>
                                          <xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:GenderCode = 2">
                                            Female
                                          </xsl:when>
                                          <xsl:otherwise>UnKnown</xsl:otherwise>
                                        </xsl:choose>
                                      </td>
                                    </tr>

                                    <xsl:if test="(a:AgencyReference[@type='Docket'])">
                                      <tr>
                                        <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                          <b>Docket Number:</b>
                                        </td>

                                        <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                          <xsl:value-of select="a:AgencyReference[@type='Docket']"/>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="a:CaseFileDate">
                                      <tr>
                                        <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:30%;">
                                          Case Filed:
                                        </td>
                                        <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                          <xsl:choose>
                                            <xsl:when test="count(a:CaseFileDate) &gt; 1">
                                              <xsl:for-each select="a:CaseFileDate">
                                                <span style="float:left;">
                                                  <xsl:value-of select='.' />
                                                  <xsl:element name="br" />
                                                </span>
                                              </xsl:for-each>
                                            </xsl:when>
                                            <xsl:when test="count(a:CaseFileDate)=1">
                                              <xsl:value-of select='substring(a:CaseFileDate,1,10)' />
                                            </xsl:when>
                                          </xsl:choose>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="a:ReviewNote!= ''">
                                      <tr style="display:none;">
                                        <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:30%;">
                                          Review Note:
                                        </td>
                                        <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                          <xsl:if test="count(a:ReviewNote) &gt; 1">
                                            <xsl:for-each select="a:ReviewNote">
                                              <span style="float:left;">
                                                <xsl:value-of select='.' />
                                                <xsl:element name="br" />
                                              </span>
                                            </xsl:for-each>
                                          </xsl:if>
                                          <xsl:if test="count(a:ReviewNote)=1">
                                            <xsl:value-of select='a:ReviewNote' />
                                          </xsl:if>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:30%;">

                                        Additional Events:

                                      </td>
                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:if test="count(a:AdditionalItems/a:EffectiveDate) &gt; 0">
                                          <input id="imageColapseDOBVARIATIONS" type="image" src="../content/themes/base/images/plus_symbol.png" style="width:10px; height:10px;" value="myValue" alt="" />
                                        </xsl:if>
                                        <div style="display:block;">

                                          <xsl:for-each select="a:AdditionalItems/a:EffectiveDate">
                                            <xsl:value-of  select='../a:Text' />
                                            <xsl:text xml:space="preserve">  </xsl:text>
                                            <xsl:value-of  select='substring(a:StartDate/a:AnyDate,1,10)' />
                                            <xsl:element name="br" />
                                          </xsl:for-each>

                                        </div>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                                <xsl:if test="(a:MatchMeter)">
                                  <td width="12%" class="">
                                    <table border="0" style="background: transparent;" cellpadding="0" cellspacing="0" width="100%">
                                      <tr>
                                        <td style="background: transparent none repeat scroll 0% 0%; font-size: 11px; text-align: right; text-transform: capitalize ! important; width: 120px !important; float:left; color: rgb(255, 255, 255) ! important; height: 27px; padding: 2px 0px 0px;">
                                          <b style="font-weight: normal;float: left; position: relative; z-index: 9 ! important; color: rgb(255, 255, 255); display: block; width: 100%; text-align: center; padding-top: 1px;">Name Match</b>
                                          <!--</td>
                                <td style="background: transparent;text-align:center;height:22px;padding:0px;">-->
                                          <xsl:text xml:space="preserve">  </xsl:text>
                                          <xsl:if test="(a:MatchMeter/a:Name) = '2'">
                                            <div class="matchgreen" style=" background: #47da26 none repeat scroll 0px 0px; height: 25px; width: 120px; position: absolute; margin-top: -1px;"/>
                                            <div class="width_class" style="margin-left:15%;" ></div>
                                            <img  class="matchgreen2" src="../../content/themes/base/images/ico_match_green.jpg"></img>
                                          </xsl:if>
                                          <xsl:if test="(a:MatchMeter/a:Name) = '1'">
                                            <div class="matchyellow"   style=" background: #c9be25 none repeat scroll 0px 0px; height: 25px; width: 120px; position: absolute; margin-top: -1px;"/>
                                            <div class="width_class" style="margin-left:15%;" ></div>
                                            <img class="matchyellow2" src="../../content/themes/base/images/ico_red_yellow.png"></img>
                                          </xsl:if>
                                          <xsl:if test="(a:MatchMeter/a:Name) = '0'">
                                            <div class="matchred"   style=" background: #ff0000 none repeat scroll 0px 0px; height: 25px; width: 120px; position: absolute; margin-top: -1px;"/>
                                            <div class="width_class" style="margin-left:15%;" ></div>
                                            <img class="matchred2" src="../../content/themes/base/images/ico_red_match.png"></img>
                                          </xsl:if>
                                        </td>
                                      </tr>
                                      <tr height="1px" style="float: left; height: 6px ! important;">
                                        <td></td>
                                      </tr>
                                      <tr>
                                        <td style="background: transparent;font-size:11px;width: 120px !important; float:left;height:25px;text-align:right;text-transform:capitalize !important;padding:0px;">
                                          <b style="font-weight: normal; float: left; position: relative; z-index: 9 ! important; color: rgb(255, 255, 255); display: block; width: 100%; text-align: center; padding-top: 3px;">Address Match</b>
                                          <!--</td>
                                <td style="background: transparent;text-align:center;height:22px;padding:0px;">-->
                                          <xsl:text xml:space="preserve">  </xsl:text>
                                          <xsl:if test="(a:MatchMeter/a:Address) = '2'">
                                            <div class="matchgreen" style=" background: #47da26 none repeat scroll 0px 0px; height: 25px; width: 120px; position: relative;margin-bottom: 3px;" />
                                            <div class="width_class" style="margin-left:15%;" ></div>
                                            <img  class="matchgreen2" src="../../content/themes/base/images/ico_match_green.jpg"></img>
                                          </xsl:if>
                                          <xsl:if test="(a:MatchMeter/a:Address) = '4'">
                                            <div class="matchblue"   style=" background: #3771c9 none repeat scroll 0px 0px; height: 25px; width: 120px; position: relative;  margin-bottom: 3px;" />
                                            <div class="width_class" style="margin-left:15%;" ></div>
                                            <img  class="matchblue2" src="../../content/themes/base/images/ico_match_blue.jpg"></img>
                                          </xsl:if>
                                          <xsl:if test="(a:MatchMeter/a:Address) = '1'">
                                            <div class="matchyellow"  style=" background: #c9be25 none repeat scroll 0px 0px; height: 25px; width: 120px; position: relative;  margin-bottom: 3px;" />
                                            <div class="width_class" style="margin-left:15%;" ></div>
                                            <img class="matchyellow2" src="../../content/themes/base/images/ico_red_yellow.png"></img>
                                          </xsl:if>
                                          <xsl:if test="(a:MatchMeter/a:Address) = '0'">
                                            <div class="matchred"   style=" background: #ff0000 none repeat scroll 0px 0px; height: 25px; width: 120px; position: relative;  margin-bottom: 3px;"/>
                                            <div class="width_class" style="margin-left:15%;" ></div>
                                            <img class="matchred2" src="../../content/themes/base/images/ico_red_match.png"></img>
                                          </xsl:if>
                                          <xsl:if test="(a:MatchMeter/a:Address) = '-1'">
                                            <div class="matchgray"  style=" background: #7f7f7f none repeat scroll 0px 0px; height: 25px; width: 120px; position: relative;  margin-bottom: 3px;"></div>
                                            <div class="width_class" style="margin-left:15%;" ></div>
                                            <img class="matchgray2" src="../../content/themes/base/images/ico_darkgrey_match.png"></img>
                                          </xsl:if>
                                        </td>
                                      </tr>
                                      <tr height="1px" style="float: left; height: 6px ! important;">
                                        <td></td>
                                      </tr>
                                      <tr>
                                        <td style="background: transparent;font-size:11px;width: 120px !important; float:left;height:28px;text-align:right;text-transform:capitalize !important;padding:0px;">
                                          <b style="font-weight: normal; float: left; position: relative; z-index: 9 ! important; color: rgb(255, 255, 255); display: block; width: 100%; text-align: center; padding-top: 3px;">DOB Match</b>
                                          <!--</td>
                                <td style="background: transparent;text-align:center;height:28px;padding:0px;">-->
                                          <xsl:text xml:space="preserve">  </xsl:text>
                                          <xsl:if test="(a:MatchMeter/a:DOB) = '2'">
                                            <div class="matchgreen" style=" background: #47da26 none repeat scroll 0px 0px; height: 25px; width: 120px; position: absolute; z-index: 4;" />
                                            <div class="width_class" style="margin-left:15%;" ></div>
                                            <img class="matchgreen2" src="../../content/themes/base/images/ico_match_green.jpg"></img>
                                          </xsl:if>
                                          <xsl:if test="(a:MatchMeter/a:DOB) = '1'">
                                            <div class="matchyellow"  style=" background: #c9be25 none repeat scroll 0px 0px; height: 25px; width: 120px; position: absolute; z-index: 4;"/>
                                            <div class="width_class" style="margin-left:15%;" ></div>
                                            <img class="matchyellow2" src="../../content/themes/base/images/ico_red_yellow.png"></img>
                                          </xsl:if>
                                          <xsl:if test="(a:MatchMeter/a:DOB) = '0'">
                                            <div class="matchred"   style=" background: #ff0000 none repeat scroll 0px 0px; height: 25px; width: 120px; position: absolute; z-index: 4;" />
                                            <div class="width_class" style="margin-left:15%;" ></div>
                                            <img class="matchred2" src="../../content/themes/base/images/ico_red_match.png"></img>
                                          </xsl:if>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </xsl:if>
                              </tr>
                            </table>
                          </td>
                          <td class="end" style="border:none;"></td>
                        </tr>

                        <xsl:for-each select="a:Charge">
                          <tr style="height:10px;">
                            <td></td>
                            <td></td>
                            <td class="end" style="border:none;"></td>
                          </tr>
                          <tr style="border:solid 1px #ccc">
                            <!--<td class="begin" style="border:none;">
                            </td>-->
                            <td colspan="5" style="border:solid 0px #ccc;background-color:white;text-align:center;">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border:solid 0px #ccc;background-color:#fff;text-align:center;">
                                <tr>
                                  <xsl:choose>
                                    <xsl:when test="(position()mod 2)=1">
                                      <td width="10%" style="border-right:solid 0px #ccc;text-align:center;padding:5px 0 0  5px">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                          <tr>
                                            <td style="text-align:right;">
                                              <div class="file-name" style="float:left; font-weight:bold;">
                                                Charge
                                              </div>
                                              <span class="blue-no" style="padding-top: 4px; float: left; margin-left: 6px; margin-top: -3px;">
                                                <xsl:value-of select="(position())" />
                                              </span>
                                            </td>
                                          </tr>
                                          <tr style="height:10px;">
                                            <td></td>
                                            <td></td>
                                            <td class="end" style="border:none;"></td>
                                          </tr>
                                        </table>
                                      </td>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      <td width="10%" style="border-right:solid 0px #ccc;background-color:white;text-align:center;padding:5px 0 0  5px">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                          <tr>
                                            <td style="text-align:right;background-color:white;">
                                              <div class="file-name" style="float:left; font-weight:bold;">
                                                Charge
                                              </div>
                                              <span class="blue-no" style="padding-top: 4px; float: left; margin-left: 6px; margin-top: -3px;">
                                                <xsl:value-of select="(position())" />
                                              </span>
                                            </td>
                                          </tr>
                                          <tr style="height:10px;">
                                            <td></td>
                                            <td></td>
                                            <td class="end" style="border:none;"></td>
                                          </tr>
                                        </table>
                                      </td>
                                    </xsl:otherwise>
                                  </xsl:choose>
                                  <xsl:choose>
                                    <xsl:when test="(position()mod 2)=1">
                                      <td width="45%" style="">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                          <tr>
                                            <td style="text-align:right;border-bottom:0px solid black;width:25%;">
                                              <b>Charge:</b>
                                            </td>
                                            <td style="text-align:left;border-bottom:0px solid black;">
                                              <xsl:value-of select="a:ChargeOrComplaint"/>
                                            </td>
                                          </tr>
                                          <xsl:if test="(a:ChargeDescription)">
                                            <tr>
                                              <td style="text-align:right;border-bottom:0px solid black;">
                                                <b>Penal Code Description:</b>
                                              </td>
                                              <td style="text-align:LEFT;border-bottom:0px solid black;">
                                                <xsl:value-of select="a:ChargeDescription" />
                                              </td>
                                            </tr>
                                          </xsl:if>
                                          <xsl:if test="(a:ChargeDate)">
                                            <tr>
                                              <td style="text-align:right;border-bottom:0px solid black;">
                                                <b>Charge Date:</b>
                                              </td>

                                              <td style="text-align:left;border-bottom:0px solid black;">
                                                <xsl:value-of select='substring(a:ChargeDate,1,10)' />
                                              </td>
                                            </tr>
                                          </xsl:if>
                                          <xsl:if test="(a:ArrestDate)">
                                            <tr>
                                              <td style="text-align:right;border-bottom:0px solid black;">
                                                <b>Arrest Date:</b>
                                              </td>

                                              <td style="text-align:left;border-bottom:0px solid black;">
                                                <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                              </td>
                                            </tr>
                                          </xsl:if>

                                          <xsl:if test="(a:OffenseDate)">
                                            <tr>
                                              <td style="text-align:right;border-bottom:0px solid black;">
                                                <b>Offense Date:</b>
                                              </td>

                                              <td style="text-align:left;border-bottom:0px solid black;">
                                                <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                              </td>
                                            </tr>
                                          </xsl:if>

                                          <xsl:if test="(a:DispositionDate)">
                                            <tr>
                                              <td style="text-align:right;border-bottom:0px solid black;">
                                                <b>
                                                  Disposition Date:
                                                </b>
                                              </td>

                                              <td style="text-align:left;border-bottom:0px solid black;">
                                                <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                              </td>
                                            </tr>
                                          </xsl:if>
                                          <xsl:if test="(../a:AdditionalItems)">
                                            <xsl:if test="(../a:AdditionalItems/@type='TrialDate')">
                                              <tr>
                                                <td style="text-align:right;border-bottom:0px solid black;">
                                                  <b>
                                                    Trial Date:
                                                  </b>
                                                </td>

                                                <td style="text-align:left;border-bottom:0px solid black;">
                                                  <xsl:value-of select='substring(../a:AdditionalItems/a:Text,1,10)' />
                                                </td>
                                              </tr>
                                            </xsl:if>
                                          </xsl:if>
                                        </table>
                                      </td>
                                      <td width="45%" style="">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                          <xsl:if test="(a:ChargeTypeClassification)!='unknown'">
                                            <tr>
                                              <td style="text-align:right;border-bottom:0px solid black;width:25%;">
                                                <b>
                                                  Severity:
                                                </b>
                                              </td>

                                              <td style="text-align:left;border-bottom:0px solid black;">
                                                <xsl:value-of select="a:ChargeTypeClassification"/>
                                              </td>
                                            </tr>
                                          </xsl:if>

                                          <xsl:if test="(a:Disposition)">
                                            <tr>
                                              <td style="text-align:right;border-bottom:0px solid black;">
                                                <b>
                                                  Final Disposition:
                                                </b>
                                              </td>

                                              <td style="text-align:left;border-bottom:0px solid black;">
                                                <xsl:choose>
                                                  <xsl:when test="(a:Disposition)='unknown'">
                                                    Manual County or State Report needed for Final Results
                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                    <xsl:choose>
                                                      <xsl:when test="(contains(../a:CourtName, 'SAN BERNARDINO') or contains(../a:CourtName, 'RIVERSIDE')) and (a:Disposition)='NOT CONVICTED'">
                                                        Unknown
                                                      </xsl:when>
                                                      <xsl:otherwise>
                                                        <xsl:value-of select="a:Disposition"/>
                                                      </xsl:otherwise>
                                                    </xsl:choose>
                                                  </xsl:otherwise>
                                                </xsl:choose>
                                              </td>
                                            </tr>
                                          </xsl:if>
                                          <xsl:if test="(a:Sentence)">
                                            <tr>
                                              <td style="text-align:right;border-bottom:0px solid black;">
                                                <b>
                                                  Sentence:
                                                </b>
                                              </td>

                                              <td style="text-align:left;border-bottom:0px solid black;">
                                                <xsl:value-of select='(a:Sentence)' />
                                              </td>
                                            </tr>
                                          </xsl:if>
                                          <xsl:if test="(a:Comment)">
                                            <tr>
                                              <td style="text-align:right;border-bottom:0px solid black;">
                                                <b>
                                                  Comments:
                                                </b>
                                              </td>

                                              <td style="text-align:left;border-bottom:0px solid black;">
                                                <xsl:for-each select="a:Comment">
                                                  <span style="float:left;">
                                                    <xsl:value-of select="."/>
                                                    <xsl:element name="br" />
                                                  </span>
                                                </xsl:for-each>
                                              </td>
                                            </tr>
                                          </xsl:if>
                                        </table>
                                      </td>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      <td width="45%">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                          <tr>
                                            <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                              <b>Charge:</b>
                                            </td>
                                            <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                              <xsl:value-of select="a:ChargeOrComplaint"/>
                                            </td>
                                          </tr>
                                          <xsl:if test="(a:ChargeDescription)">
                                            <tr>
                                              <td style="text-align:right; border-bottom:0px solid black;">
                                                <b>Penal Code Description:</b>
                                              </td>
                                              <td style="text-align:LEFT; border-bottom:0px solid black;">
                                                <xsl:value-of select="a:ChargeDescription" />
                                              </td>
                                            </tr>
                                          </xsl:if>
                                          <xsl:if test="(a:ChargeDate)">
                                            <tr>
                                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                                <b>Charge Date:</b>
                                              </td>

                                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                                <xsl:value-of select='substring(a:ChargeDate,1,10)' />
                                              </td>
                                            </tr>
                                          </xsl:if>
                                          <xsl:if test="(a:ArrestDate)">
                                            <tr>
                                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                                <b>Arrest Date:</b>
                                              </td>

                                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                                <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                              </td>
                                            </tr>
                                          </xsl:if>

                                          <xsl:if test="(a:OffenseDate)">
                                            <tr>
                                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                                <b>Offense Date:</b>
                                              </td>

                                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                                <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                              </td>
                                            </tr>
                                          </xsl:if>

                                          <xsl:if test="(a:DispositionDate)">
                                            <tr>
                                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                                <b>
                                                  Disposition Date:
                                                </b>
                                              </td>

                                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                                <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                              </td>
                                            </tr>
                                          </xsl:if>
                                          <xsl:if test="(../a:AdditionalItems)">
                                            <xsl:if test="(../a:AdditionalItems/@type='TrialDate')">
                                              <tr>
                                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                                  <b>
                                                    Trial Date:
                                                  </b>
                                                </td>

                                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                                  <xsl:value-of select='substring(../a:AdditionalItems/a:Text,1,10)' />
                                                </td>
                                              </tr>
                                            </xsl:if>
                                          </xsl:if>
                                        </table>
                                      </td>
                                      <td width="45%">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                          <xsl:if test="(a:ChargeTypeClassification)!='unknown'">
                                            <tr>
                                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                                <b>
                                                  Severity:
                                                </b>
                                              </td>

                                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                                <xsl:value-of select="a:ChargeTypeClassification"/>
                                              </td>
                                            </tr>
                                          </xsl:if>

                                          <xsl:if test="(a:Disposition)">
                                            <tr>
                                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                                <b>
                                                  Final Disposition:
                                                </b>
                                              </td>

                                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                                <xsl:choose>
                                                  <xsl:when test="(a:Disposition)='unknown'">
                                                    Manual County or State Report needed for Final Results
                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                    <xsl:choose>
                                                      <xsl:when test="(contains(../a:CourtName, 'SAN BERNARDINO') or contains(../a:CourtName, 'RIVERSIDE')) and (a:Disposition)='NOT CONVICTED'">
                                                        Unknown
                                                      </xsl:when>
                                                      <xsl:otherwise>
                                                        <xsl:value-of select="a:Disposition"/>
                                                      </xsl:otherwise>
                                                    </xsl:choose>
                                                  </xsl:otherwise>
                                                </xsl:choose>
                                              </td>
                                            </tr>
                                          </xsl:if>
                                          <xsl:if test="(a:Sentence)">
                                            <tr>
                                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                                <b>Sentence:</b>
                                              </td>
                                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                                <xsl:value-of select='(a:Sentence)' />
                                              </td>
                                            </tr>
                                          </xsl:if>
                                          <xsl:if test="(a:Comment)">
                                            <tr>
                                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                                <b>
                                                  Comments:
                                                </b>
                                              </td>

                                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                                <xsl:for-each select="a:Comment">
                                                  <span style="float:left;">
                                                    <xsl:value-of select="."/>
                                                    <xsl:element name="br" />
                                                  </span>
                                                </xsl:for-each>
                                              </td>
                                            </tr>
                                          </xsl:if>
                                        </table>
                                      </td>
                                    </xsl:otherwise>
                                  </xsl:choose>
                                </tr>
                              </table>


                            </td>
                            <!--<td class="end" style="border:none;"></td>-->
                          </tr>

                        </xsl:for-each>
                        <xsl:if test="count(a:Charge)=0">
                          <tr style="height:10px;">
                            <td></td>
                            <td></td>
                            <td class="end" style="border:none;"></td>
                          </tr>
                          <tr>
                            <td class="begin" style="border:none;">
                            </td>
                            <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border:solid 1px #ccc;background-color:#fff;text-align:center;">
                                <tr>
                                  <td width="10%" style="border-right:solid 0px #ccc;text-align:center;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                      <tr>
                                        <td style="text-align:right; ">
                                          <span style="font-size:12px;font-weight:normal;color:#000;">
                                            Additional Info
                                          </span>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                  <td width="80%" style=" text-align:left;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">

                                      <tr>
                                        <td style=" text-align:left;padding-left:5px;">
                                          NOTE: THE DATA REPORTED BY THE COURT FOR THIS CASE IS INCOMPLETE. MANUAL COUNTY SEARCH REQUIRED.<br/>
                                          <span style="padding-left:35px">IF THIS RECORD DOES NOT SPECIFY THE COUNTY, A MANUAL STATE SEARCH IS REQUIRED.</span>
                                        </td>
                                      </tr>

                                      <!--<xsl:for-each select="a:AdditionalItems">
                                        <tr>
                                          <td style="background-color:#dcdcdc;text-align:left;padding-left:5px;">
                                            <xsl:value-of select="a:Text"/>
                                            <xsl:element name="br" />
                                          </td>
                                        </tr>
                                      </xsl:for-each>-->
                                    </table>
                                  </td>
                                </tr>
                              </table>
                            </td>
                            <td class="end" style="border:none;"></td>
                          </tr>
                        </xsl:if>
                      </xsl:when>
                      <xsl:otherwise>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:for-each>
                </table>
              </div>
            </td>
            <!--<td class="end" style="border:none;"></td>-->
          </tr>

          <xsl:for-each select="a:BackgroundReports">
            <tr class="trRawNCR11">
              <td onclick="collapse('{concat('trRawNCR1_',@OrderDetailId)}');" colspan="5" style="border:solid 1px #ccc;background-color:white;text-align:left;">
                <div style="float:left">
                  <div style="float:left;background-position:left;" class="plus_btnSmall">
                  </div>
                  <div style="font-size:17px;font-weight:bold;color:#000;width:300px; margin-top:2px">
                    Raw Courthouse Data
                  </div>
                </div>
              </td>
            </tr>

            <tr class="trRawNCR12">

              <td colspan="5" style="border:solid 1px #ccc;background-color:white;text-align:center;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr id="{concat('trRawNCR1_',@OrderDetailId)}_b" style="display:none;">
                    <td style="background-color:white;">
                      <div id="divRawDataNCR1">
                        <pre>
                          <xsl:value-of select ="$TextData"/>
                        </pre>
                      </div>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </xsl:for-each>

        </tbody>
        <tfoot>
          <td class="begin" style="border:none; background:none;"></td>
          <td colspan="5"></td>
          <td class="end" style="border:none; background:none;"></td>
        </tfoot>
      </table>
    </center>

    <div class="sectionsummary">
      <h3 style="line-height: 1em;">
        <!--<xsl:choose>
          <xsl:when test="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)=0">
            <p>
              National Sex Offender : Clear<br/>
              OFAC : Clear<br/>
              OIG : Clear<br/>
              For a complete list of coverage, please visit<br/>
              <a href="http://emerge.intelifi.com/coverage.pdf" target="_blank">US Criminal Rapid Search Coverage Guide</a>
            </p>
          </xsl:when>
          <xsl:otherwise>-->
        Data Sources Searched:<br/>
        Court Records, Department of Corrections, Administrative Office of Courts,<br/>
        National Sex Offender Registry, OFAC, OIG<br/>
        For a complete list of coverage, please visit<br/>
        <a href="http://emerge.intelifi.com/coverage.pdf" target="_blank">US Criminal Rapid Search Coverage Guide</a>

        <!--</xsl:otherwise>
        </xsl:choose>-->

      </h3>
    </div>
  </xsl:template>
</xsl:stylesheet>