<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:a="http://ns.hr-xml.org/2004-08-02">
  <xsl:output method="html" encoding="iso-8859-1" />
  <xsl:template match="/BGC/product/StatewideCriminalSearch/fulfillment">
    <center>
      <div class="sectionsummary">
        <h3>
        </h3>
        <h3>
          (<xsl:value-of select="count(results/result/counts/count)"/>) Results<br />
        </h3>
      </div>
      <xsl:choose>
        <xsl:when test="count(results/result/counts/count)>0">
          <table id="tblSCR" class="reporttable" border="0" cellpadding="0" cellspacing="0">
            <thead>
              <tr>
                <th class="begin"></th>
                <th colspan="5">
                  <span style="font-size:20px;font-weight:bold;">Statewide Criminal Report</span>
                </th>
                <th class="end"></th>
              </tr>
            </thead>
            <xsl:for-each select="results/result">
              <tr>
                <td class="begin">
                </td>
                <td colspan="5" style="border:solid 1px black;background-color:black;text-align:left;">
                  <span style="font-size:16px;font-weight:bold;color:#fff;">
                    Criminal Record #
                    <xsl:value-of select="(position())" />
                  </span>
                </td>
                <td class="end"></td>
              </tr>
              <tr>
                <td class="begin">
                </td>
                <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;padding:0px;">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td width="44%">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <xsl:if test="state != ''">
                            <tr>
                              <td style="width:25%;text-align:right;background-color:white;border-bottom:0px solid black;">

                                <b>STATE :</b>
                              </td>
                              <td>
                                <xsl:value-of select="state"/>
                              </td>
                            </tr>
                          </xsl:if>
                          <xsl:if test="caseNumber != ''">
                            <tr>
                              <td style="width:25%;text-align:right;background-color:white;border-bottom:0px solid black;">

                                <b>CASE NUMBER :</b>
                              </td>
                              <td>
                                <xsl:value-of select="caseNumber"/>

                              </td>
                            </tr>
                          </xsl:if>
                          <xsl:if test="fileDate != ''">
                            <tr>
                              <td style="width:25%;text-align:right;background-color:white;border-bottom:0px solid black;">

                                <b>FILE DATE :</b>
                              </td>
                              <td>
                                <xsl:value-of select="fileDate"/>

                              </td>
                            </tr>
                          </xsl:if>
                        </table>
                      </td>
                      <td width="44%">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <xsl:if test="nameOnFile != ''">
                            <tr>
                              <td style="width:25%;text-align:right;background-color:white;border-bottom:0px solid black;">

                                <b>NAME ON FILE :</b>
                              </td>
                              <td>
                                <xsl:value-of select="nameOnFile"/>

                              </td>
                            </tr>
                          </xsl:if>
                          <xsl:if test="isDOBVerified != ''">
                            <tr>
                              <td style="width:25%;text-align:right;background-color:white;border-bottom:0px solid black;">

                                <b>DOB Verified :</b>
                              </td>
                              <td>
                                <xsl:value-of select="isDOBVerified"/>

                              </td>
                            </tr>
                          </xsl:if>
                          <xsl:if test="isSSNVerified != ''">
                            <tr>
                              <td style="width:25%;text-align:right;background-color:white;border-bottom:0px solid black;">

                                <b>SSN Verified :</b>
                              </td>
                              <td>
                                <xsl:value-of select="isSSNVerified"/>

                              </td>
                            </tr>
                          </xsl:if>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
                <td class="end"></td>
              </tr>
              <xsl:for-each select="counts/count">
                <tr>
                  <td class="begin">
                  </td>
                  <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border:solid 1px black;background-color:red;text-align:center;">
                      <tr>
                        <xsl:choose>
                          <xsl:when test="(position()mod 2)=1">
                            <td width="10%" style="border-right:solid 1px black;background-color:#dcdcdc;text-align:center;">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                  <td style="text-align:right;background-color:#dcdcdc;">
                                    <span style="font-size:12px;font-weight:bold;color:#000;">
                                      Charge #
                                      <xsl:value-of select="(position())" />
                                    </span>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </xsl:when>
                          <xsl:otherwise>
                            <td width="10%" style="border-right:solid 1px black;background-color:white;text-align:center;">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                  <td style="text-align:right;background-color:white;">
                                    <span style="font-size:12px;font-weight:bold;color:#000;">
                                      Charge #
                                      <xsl:value-of select="(position())" />
                                    </span>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </xsl:otherwise>
                        </xsl:choose>
                        <xsl:choose>
                          <xsl:when test="(position()mod 2)=1">
                            <td width="45%" style="background-color:#dcdcdc;">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <xsl:if test="crimeDetails != ''">
                                  <tr>
                                    <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                      <b>CRIME DETAILS :</b>
                                    </td>
                                    <td style="background-color:#dcdcdc;">
                                      <xsl:value-of select="crimeDetails"/>

                                    </td>
                                  </tr>
                                </xsl:if>
                                <xsl:if test="disposition != ''">
                                  <tr>
                                    <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                      <b>DISPOSITION :</b>
                                    </td>
                                    <td style="background-color:#dcdcdc;">
                                      <xsl:value-of select="disposition"/>

                                    </td>
                                  </tr>
                                </xsl:if>
                                <xsl:if test="fines != ''">
                                  <tr>
                                    <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                      <b>FINES :</b>
                                    </td>
                                    <td style="background-color:#dcdcdc;">
                                      <xsl:value-of select="fines"/>

                                    </td>
                                  </tr>
                                </xsl:if>
                              </table>
                            </td>
                            <td width="45%" style="background-color:#dcdcdc;">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <xsl:if test="crimeType != ''">
                                  <tr>
                                    <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                      <b>CRIME TYPE :</b>
                                    </td>
                                    <td style="background-color:#dcdcdc;">
                                      <xsl:value-of select="crimeType"/>
                                    </td>
                                  </tr>
                                </xsl:if>
                                <xsl:if test="DispositionDate != ''">
                                  <tr>
                                    <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                      <b>Disposition Date :</b>
                                    </td>
                                    <td style="background-color:#dcdcdc;">
                                      <xsl:value-of select="DispositionDate"/>
                                    </td>
                                  </tr>
                                </xsl:if>
                                <xsl:if test="courtCosts!= ''">
                                  <tr>
                                    <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                      <b>COURT COSTS :</b>
                                    </td>
                                    <td style="background-color:#dcdcdc;">
                                      <xsl:value-of select="courtCosts"/>
                                    </td>
                                  </tr>
                                </xsl:if>
                              </table>
                            </td>
                          </xsl:when>
                          <xsl:otherwise>
                            <td width="45%" style="background-color:#fff;">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <xsl:if test="crimeDetails != ''">
                                  <tr>
                                    <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                      <b>CRIME DETAILS :</b>
                                    </td>
                                    <td>
                                      <xsl:value-of select="crimeDetails"/>
                                    </td>
                                  </tr>
                                </xsl:if>
                                <xsl:if test="disposition != ''">
                                  <tr>
                                    <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                      <b>DISPOSITION :</b>
                                    </td>
                                    <td>
                                      <xsl:value-of select="disposition"/>
                                    </td>
                                  </tr>
                                </xsl:if>
                                <xsl:if test="fines != ''">
                                  <tr>
                                    <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                      <b>FINES :</b>
                                    </td>
                                    <td>
                                      <xsl:value-of select="fines"/>
                                    </td>
                                  </tr>
                                </xsl:if>
                              </table>
                            </td>
                            <td width="45%" style="background-color:#fff;">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <xsl:if test="crimeType != ''">
                                  <tr>
                                    <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                      <b>CRIME TYPE :</b>
                                    </td>
                                    <td>
                                      <xsl:value-of select="crimeType"/>

                                    </td>
                                  </tr>
                                </xsl:if>
                                <xsl:if test="DispositionDate != ''">
                                  <tr>
                                    <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                      <b>Disposition Date :</b>
                                    </td>
                                    <td>
                                      <xsl:value-of select="DispositionDate"/>


                                    </td>
                                  </tr>
                                </xsl:if>
                                <xsl:if test="courtCosts!= ''">
                                  <tr>
                                    <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                      <b>COURT COSTS :</b>
                                    </td>
                                    <td>
                                      <xsl:value-of select="courtCosts"/>
                                    </td>
                                  </tr>
                                </xsl:if>
                              </table>
                            </td>
                          </xsl:otherwise>
                        </xsl:choose>
                      </tr>
                      <tr>
                        <td colspan="3" width="100%" style="border:solid 1px black;background-color:red;text-align:center;">
                          <table>
                            <tr>
                              <xsl:if test="offenseDate != ''">
                                <td>Offense Date</td>
                                <td>
                                  <xsl:value-of select="offenseDate"/>
                                </td>
                              </xsl:if>
                              <xsl:if test="generalNotes != ''">
                                <td>General Notes</td>
                                <td>
                                  <xsl:value-of select="generalNotes"/>
                                </td>
                              </xsl:if>
                              <xsl:if test="additionalDispositionInfo != ''">
                                <td>Additional Disposition Information</td>
                                <td>
                                  <xsl:value-of select="additionalDispositionInfo"/>
                                </td>
                              </xsl:if>
                              <xsl:if test="suspensionTime != ''">
                                <td>Suspension Time</td>
                                <td>
                                  <xsl:value-of select="suspensionTime"/>
                                </td>
                              </xsl:if>
                              <xsl:if test="prisonTime != ''">
                                <td>Prison Time</td>
                                <td>
                                  <xsl:value-of select="prisonTime"/>
                                </td>
                              </xsl:if>
                              <xsl:if test="jailTime != ''">
                                <td>Jail Time</td>
                                <td>
                                  <xsl:value-of select="jailTime"/>
                                </td>
                              </xsl:if>
                              <xsl:if test="probation != ''">
                                <td>Probation</td>
                                <td>
                                  <xsl:value-of select="probation"/>
                                </td>
                              </xsl:if>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td class="end"></td>
                </tr>
              </xsl:for-each>
            </xsl:for-each>
          </table>
        </xsl:when>
        <xsl:otherwise>
          <center>
            <div style="color:red;font-weight:bold;font-size:12px;">
              No Record Found
            </div>
          </center>
        </xsl:otherwise>
      </xsl:choose>
    </center>
  </xsl:template>
  <xsl:template match="/BGC/product/StatewideCriminalSearch/response">
    <div class="noRecords">
      No Records Found.
      <br />
      * CLEAR *
    </div>
  </xsl:template>
</xsl:stylesheet>