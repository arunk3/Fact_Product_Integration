<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">
    <xsl:output method="html" />
    <xsl:template match="/FastraxNetwork">
      <xsl:variable name="TotalRecords">
        <xsl:value-of select="count(report-requested/report/report-detail-header/report-incident)" />
      </xsl:variable>
      <xsl:variable name="DeletedRecords">
        <xsl:value-of select="count(report-requested/report/report-detail-header/report-incident[@Deleted=1])" />
      </xsl:variable>
      <xsl:choose>
        <xsl:when test ="$TotalRecords=$DeletedRecords">
        <table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td class="" width="14px">
            </td>
            <td width="97%">
              <div class="reportName">
                <h3>
                  County Criminal Report
                </h3>
              </div>
              <div class="resultsHeader">
                (0) Results
              </div>
              <br/>
              <center>
                <div style="color: red; font-weight: bold; font-size: 12px;">
                  No Record Found
                </div>
              </center>
            </td>
            <td class="" width="9px">
            </td>
          </tr>
        </table>
        </xsl:when>
        <xsl:otherwise>
          <table id="peopleTable" class="reporttable_ccr1" border="0" cellpadding="0" cellspacing="0" style="background: #fff !important;">
            <thead>
                <tr class="header-ccr1" >
                    <th style="padding-left: 5px; padding-top: 6px; padding-bottom: 8px;" colspan="100%">Criminal Arrests and Convictions</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="font-weight: bold;">State:</td>
                    <td>
                        <xsl:value-of select="report-requested/report/report-detail-header/state" />
                    </td>
                    <td style="font-weight: bold;">Years Checked:</td>
                    <td>
                        <xsl:value-of select="report-requested/report/report-detail-header/time-checked" />
                    </td>
                    <td> </td>
                    <td> </td>
                </tr>

                <tr>
                    <td style="font-weight: bold;">County:</td>
                    <td>
                        <xsl:value-of select="report-requested/report/report-detail-header/county" />
                    </td>
                    <td style="font-weight: bold;">Convictions Admitted:</td>
                    <td>
                        <xsl:value-of select="report-requested/report/report-detail-header/felonies-admitted" />
                    </td>
                    <td> </td>
                    <td> </td>
                </tr>

                <tr>
                    <td style="font-weight: bold;">City:</td>
                    <td></td>
                    <td style="font-weight: bold;">Details Disclosed:</td>
                    <td></td>
                    <td> </td>
                    <td> </td>
                </tr>

                <tr>
                    <td colspan="100%"> </td>
                </tr>

                <tr>
                    <td style="font-weight: bold;">Record Found:</td>
                    <td>
                        <xsl:if test="report-requested/report/report-detail-header/clear-record = '0'">Yes</xsl:if>
                        <xsl:if test="report-requested/report/report-detail-header/clear-record = '1'">No</xsl:if>
                    </td>

                    <td style="font-weight: bold;">Outstanding Capias:</td>
                    <td>
                        <xsl:if test="report-requested/report/report-detail-header/capias = '0'">No</xsl:if>
                        <xsl:if test="report-requested/report/report-detail-header/capias = '1'">Yes</xsl:if>
                    </td>

                    <td style="font-weight: bold;">Verified By DOB:</td>
                    <td>
                        <xsl:if test="report-requested/report/report-detail-header/verified-by-dob = '0'">No</xsl:if>
                        <xsl:if test="report-requested/report/report-detail-header/verified-by-dob = '1'">Yes</xsl:if>
                    </td>
                </tr>

                <tr>
                    <td></td>
                    <td>
                        
                    </td>

                    <td style="font-weight: bold;">Current Warrant:</td>
                    <td>
                        <xsl:if test="report-requested/report/report-detail-header/current-warrants = ''">No</xsl:if>
                        <xsl:if test="report-requested/report/report-detail-header/current-warrants != ''">
                            <xsl:value-of select="report-requested/report/report-detail-header/current-warrants" />
                        </xsl:if>
                    </td>

                    <td style="font-weight: bold;">Verified By Name:</td>
                    <td>
                        <xsl:if test="report-requested/report/report-detail-header/verified-by-name = '0'">No</xsl:if>
                        <xsl:if test="report-requested/report/report-detail-header/verified-by-name = '1'">Yes</xsl:if>
                    </td>
                </tr>

                <tr>
                    <td></td>
                    <td>
                        
                    </td>

                    <td style="font-weight: bold;">DUI's Found:</td>
                    <td>
                        <xsl:if test="report-requested/report/report-detail-header/dui = '0'">No</xsl:if>
                        <xsl:if test="report-requested/report/report-detail-header/dui = '1'">Yes</xsl:if>
                    </td>

                    <td style="font-weight: bold;">Verified By SSN:</td>
                    <td>
                        <xsl:if test="report-requested/report/report-detail-header/verified-by-ssn = '0'">No</xsl:if>
                        <xsl:if test="report-requested/report/report-detail-header/verified-by-ssn = '1'">Yes</xsl:if>
                    </td>
                </tr>

                <tr>
                    <td style="font-weight: bold;">Aliases Found:</td>
                    <td>
                        <xsl:if test="report-requested/report/report-detail-header/alias-found = '0'">No</xsl:if>
                        <xsl:if test="report-requested/report/report-detail-header/alias-found = '1'">Yes</xsl:if>
                    </td>

                    <td style="font-weight: bold;">Aliasas:</td>
                    <td>

                    </td>

                    <td></td>
                    <td></td>
                </tr>

                <tr>
                    <td style="font-weight: bold;">Other Criminal:</td>
                    <td>

                    </td>

                    <td style="font-weight: bold;">Other Criminal Detail:</td>
                    <td>

                    </td>

                    <td></td>
                    <td></td>
                </tr>

                <tr>
                    <td style="font-weight: bold;">Verified By Other:</td>
                    <td>

                    </td>

                    <td style="font-weight: bold;">Verified By Other Detail:</td>
                    <td>

                    </td>

                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
      </xsl:otherwise>
      </xsl:choose>
          <xsl:for-each select="report-requested/report/report-detail-header/report-incident">
            <table id="peopleTable" class="reporttable_ccr1" border="0" cellpadding="0" cellspacing="0" style="background:#fff !important;">
                <thead>
                    <tr class="header-ccr1">
                        <th style="padding-left: 5px; padding-top: 6px; padding-bottom: 8px;" colspan="100%">Details</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="font-weight: bold;">Charges</td>
                        <td colspan="3" >
                            <xsl:value-of select="charges" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">Disposition Date</td>
                        <td>
                            <xsl:value-of select="disposition-date" />
                        </td>
                        <td style="font-weight: bold;">Disposition</td>
                        <td>
                            <xsl:value-of select="disposition" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">File Date</td>
                        <td>
                            <xsl:value-of select="file-date" />
                        </td>
                        <td style="font-weight: bold;">Case #</td>
                        <td>
                            <xsl:value-of select="case-number" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">Sentence Date</td>
                        <td>
                            <xsl:value-of select="sentence-date" />
                        </td>
                        <td style="font-weight: bold;">Sentence</td>
                        <td>
                            <xsl:value-of select="sentence" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">Comments</td>
                        <td colspan="3" >
                            <xsl:value-of select="additional-comments" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </xsl:for-each>

    </xsl:template>
</xsl:stylesheet>
