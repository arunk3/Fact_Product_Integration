<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>

	<xsl:template match="/">
		<center>

		<div class="sectionsummary">
		<h3>
			(<xsl:value-of select="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/rows_returned" />) Results SSN Validation
		</h3>
		</div>

		<xsl:if test="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/rows_returned = '0'">
			<div class="sectionsummary">
				No Records Found.
			</div>
		</xsl:if>
		<xsl:if test="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/rows_returned != '0'">

		<table id="table" class="reporttable2" border="0" cellpadding="0" cellspacing="0">
			<thead>
				<tr>
					<th></th>
					<th></th>
					<th ></th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/response_data/response_row">
					<tr>
						<td >i</td>
						<td style="border-right: none;">
						
						<h3>Area #:</h3><h4><xsl:value-of select="area_number" /></h4><br />
						<h3>Group #:</h3><h4><xsl:value-of select="group_number" /></h4><br />
						<h3>Begin Issue Date:</h3><h4>
							<xsl:variable name='amonth'  select='substring( begin_issue_date, 0 , 3 )' />
							<xsl:variable name='aday'	select='substring( begin_issue_date, 3 , 2 )' />
							<xsl:variable name='ayear'   select='substring( begin_issue_date, 5 , 2 )' />
							<xsl:value-of select="concat($amonth, '/', $aday, '/', $ayear )"/></h4><br />

						<h3>End Issue Date:</h3><h4>
							<xsl:variable name='bmonth'  select='substring( end_issue_date, 0 , 3 )' />
							<xsl:variable name='bday'	select='substring( end_issue_date, 3 , 2 )' />
							<xsl:variable name='byear'   select='substring( end_issue_date, 5 , 2 )' />
							<xsl:value-of select="concat($bmonth, '/', $bday, '/', $byear )"/></h4><br />

						<h3>Cln Begin Issue Date:</h3><h4>
							<xsl:variable name='cyear'  select='substring( cln_begin_issue_date, 0 , 5 )' />
							<xsl:variable name='cmonth' select='substring( cln_begin_issue_date, 5 , 2 )' />
							<xsl:variable name='cday'   select='substring( cln_begin_issue_date, 7 , 2 )' />
							<xsl:value-of select="concat($cmonth, '/', $cday, '/', $cyear )"/></h4><br />

						<h3>Cln End Issue Date:</h3><h4>
							<xsl:variable name='dyear'  select='substring( cln_end_issue_date, 0 , 5 )' />
							<xsl:variable name='dmonth' select='substring( cln_end_issue_date, 5 , 2 )' />
							<xsl:variable name='dday'   select='substring( cln_end_issue_date, 7 , 2 )' />
							<xsl:value-of select="concat($dmonth, '/', $dday, '/', $dyear )"/></h4><br />

						<h3>Begin Range:</h3><h4><xsl:value-of select="begin_range" /></h4><br />
						<h3>End Range:</h3><h4><xsl:value-of select="end_range" /></h4><br />
						<h3>State:</h3><h4><xsl:value-of select="state" /></h4><br />
						<h3>Code Digit:</h3><h4><xsl:value-of select="code_digit" /> - <xsl:value-of select="code_digit_description" /></h4><br />

						</td>
						<td ><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
				</xsl:for-each>
			</tbody>
			<tfoot>
				<td ></td>
				<td style="border-right: none;"></td>
				<td></td>
			</tfoot>
		</table>
		</xsl:if>


            <xsl:if test="XML_INTERFACE/CREDITREPORT/OBJECTS/BUREAUERROR = 'True' or XML_INTERFACE/CREDITREPORT/cp_xmldirect/general_error !='' ">
                <table id="peopleTable" class="reporttable2" border="0" cellpadding="0"  style="width:100%;" cellspacing="0">
                    <thead>
                        <tr>
                            <th ></th>
                            <th colspan="5">Search Results</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td >
                                <xsl:value-of select="(position() )"/>
                            </td>
                            <td colspan="5">

                                <xsl:value-of select="substring-after(XML_INTERFACE/CREDITREPORT/HTMLREPORT/MESSAGE, 'body&gt;')" disable-output-escaping="yes" />

                            </td>
                            <td >
                                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <td ></td>
                        <td colspan="5"></td>
                        <td ></td>
                    </tfoot>
                </table>
            </xsl:if>
            
		</center>
		
	</xsl:template>
</xsl:stylesheet>