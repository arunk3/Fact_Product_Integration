<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" encoding="utf-8"/>
  <xsl:template match="/BGC/product/*">

    <div class="sectionsummary">
      <h3>
        (<xsl:value-of select="response/summary/offenders/@qtyFound" />) Results SEX OFFENDER REPORT.<br />
      </h3>
    </div>

    <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
      <thead>
        <tr>
          <th class="begin"></th>
          <th colspan="5">Search Information</th>
          <th class="end"></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td class="begin">
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </td>
          <td colspan="5">
            <h3>First Name:</h3>
            <h4>
              <xsl:value-of select="order/firstName" />
            </h4>
            <br />
            <h3>Last Name:</h3>
            <h4>
              <xsl:value-of select="order/lastName" />
            </h4>
            <br />
            <h3>DOB:</h3>
            <h4>
              <xsl:value-of select="concat(order/DOB/month, '/', order/DOB/day, '/', 
						order/DOB/year)" />
            </h4>
          </td>
          <td class="end">
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </td>
        </tr>
      </tbody>
      <tfoot>
        <td class="begin"></td>
        <td colspan="5"></td>
        <td class="end"></td>
      </tfoot>
    </table>

    <xsl:for-each select="response">
      <xsl:for-each select="summary/offenders">
        <xsl:if test="@qtyFound = '0'">
          <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
            <thead>
              <tr>
                <th class="begin"></th>
                <th colspan="5">Results</th>
                <th class="end"></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="begin">
                  <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                </td>
                <td colspan="5">
                  No Records Found.<br />
                  <xsl:for-each select="../errors">
                    <h2>Errors</h2>
                    <table class="tableddata">
                      <tr>
                        <th>Code</th>
                        <th>Message</th>
                      </tr>
                      <xsl:for-each select="error">
                        <tr>
                          <td>
                            <xsl:value-of select="@code" />
                          </td>
                          <td>
                            <xsl:value-of select="." />
                          </td>
                        </tr>
                      </xsl:for-each>
                    </table>
                  </xsl:for-each>
                </td>
                <td class="end">
                  <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                </td>
              </tr>
            </tbody>
            <tfoot>
              <td class="begin"></td>
              <td colspan="5"></td>
              <td class="end"></td>
            </tfoot>
          </table>
        </xsl:if>
      </xsl:for-each>
    </xsl:for-each>

    <xsl:for-each select="response">
      <xsl:for-each select="summary">
        <!-- Display details of each offender -->
        <xsl:for-each select="offenders/offender">


          <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
            <thead>
              <tr>
                <th class="begin"></th>
                <th colspan="5">Result</th>
                <th class="end"></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="begin">
                  <xsl:value-of select="(position())"/>
                </td>
                

                  <xsl:choose>
                    <xsl:when test="(position() mod 2 != 0)">
                      <td colspan="5" style="background-color:#fff;">
                        <table border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td style="background-color:#fff;" width="50%" colspan="2" class="nameHeader">
                              <xsl:value-of select="identity/personal/fullName" />
                              <br />
                              <span style="background-color:#fff;" class="idHeader">
                                (ID #<xsl:value-of select="record/key/offenderID" />)
                              </span>
                            </td>
                            <td style="background-color:#fff;" rowspan="5" width="25%">
                              <xsl:if test="identity/personal/imageURL != ''">
                                <!--<xsl:value-of select="identity/personal/imageURL" /> -->
                                <img border="1" height="200" align="right">
                                  <xsl:attribute name="SRC">
                                    <xsl:value-of select="identity/personal/imageURL"/>
                                  </xsl:attribute>
                                </img>
                              </xsl:if>
                              &#160;
                            </td>
                          </tr>
                          <tr>
                            <td style="background-color:#fff;" colspan="2">
                              DOB:
                              <xsl:if test="identity/personal/DOB != ''">
                                <xsl:value-of select="identity/personal/DOB" />
                              </xsl:if>
                            </td>
                          </tr>
                          <tr>
                            <td style="background-color:#fff;" colspan="2">
                              Aliases, AKA's and Maiden Names:

                              <!-- We need to place a comma after each name and obviously make sure the last name -->
                              <!-- does not have one. So we first we find the count of alias names available. -->
                              <xsl:variable name="namesCount" select="count(aliasNames/name)" />

                              <xsl:for-each select="aliasNames/name">
                                <!-- Display the alias name .&#160; is to add space. Equivalent to &nbsp; in HTML -->
                                <xsl:value-of select="firstName" />&#160;<xsl:value-of select="middleName" />&#160;<xsl:value-of select="lastName" />

                                <!-- Check if we need to add a comma or a period following the name. -->
                                <xsl:choose>
                                  <xsl:when test="$namesCount = position()"> . </xsl:when>
                                  <xsl:otherwise>, </xsl:otherwise>
                                </xsl:choose>
                              </xsl:for-each>
                            </td>
                          </tr>
                          <tr>
                            <td style="background-color:#fff;" colspan="2">
                              Alternate DOBs:

                              <!-- We need to place a comma after each DOB and obviously make sure the last DOB -->
                              <!-- does not have one. So we first we find the count of alias DOBs available. -->
                              <xsl:variable name="dobCount" select="count(aliasDOBS/aliasDOB)" />

                              <xsl:for-each select="aliasDOBS/aliasDOB">
                                <!-- Display the alias DOB .&#160; is to add space. Equivalent to &nbsp; in HTML -->
                                <xsl:value-of select="DOB" />

                                <!-- Check if we need to add a comma or nothing following the DOB. -->
                                <xsl:choose>
                                  <xsl:when test="$dobCount = position()"></xsl:when>
                                  <xsl:otherwise>, </xsl:otherwise>
                                </xsl:choose>
                              </xsl:for-each>
                            </td>
                          </tr>
                          <tr>
                            <td style="background-color:#fff;">
                              <xsl:if test="identity/personal/race != ''">
                                Race: <xsl:value-of select="identity/personal/race" /> <br />
                              </xsl:if>

                              <xsl:if test="identity/personal/gender != ''">
                                <xsl:variable name="gender" select="identity/personal/gender" />

                                Gender:
                                <xsl:choose>
                                  <xsl:when test="$gender = 'M'">Male</xsl:when>
                                  <xsl:when test="$gender = 'F'">Female</xsl:when>
                                  <xsl:otherwise>Unknown</xsl:otherwise>
                                </xsl:choose>
                                <br />
                              </xsl:if>

                              <xsl:if test="identity/personal/hairColor != ''">
                                Hair: <xsl:value-of select="identity/personal/hairColor" /> <br />
                              </xsl:if>
                              <xsl:if test="identity/personal/eyeColor != ''">
                                Eyes: <xsl:value-of select="identity/personal/eyeColor" /> <br />
                              </xsl:if>

                              <xsl:if test="identity/personal/height != ''">
                                Height: <xsl:value-of select="identity/personal/height/feet" />' <xsl:value-of select="identity/personal/height/inches" />''<br />
                              </xsl:if>
                              <xsl:if test="identity/personal/weight != ''">
                                Weight: <xsl:value-of select="identity/personal/weight" /> <br />
                              </xsl:if>
                            </td>

                            <!-- Address information. -->

                            <td style="background-color:#fff;">
                              <h3>Address:</h3>
                              <br />
                              <!-- Street information. -->
                              <xsl:if test="identity/address/street != ''">
                                <xsl:value-of select="identity/address/street" />
                                <br/>
                              </xsl:if>

                              <!-- City, State and Zipcode info need to displayed side by side. -->
                              <xsl:if test="identity/address/city != ''">
                                <xsl:value-of select="identity/address/city" />
                              </xsl:if>

                              <xsl:choose>
                                <xsl:when test="identity/address/state != ''">
                                  <xsl:choose>
                                    <!-- Display comma after city only if there's state AND city info available. -->
                                    <xsl:when test="identity/address/city != ''">
                                      , &#160;<xsl:value-of select="identity/address/state" />
                                    </xsl:when>
                                    <xsl:otherwise>
                                      <xsl:value-of select="identity/address/state" />
                                    </xsl:otherwise>
                                  </xsl:choose>
                                </xsl:when>
                              </xsl:choose>

                              <!-- Zipcode information. -->
                              <xsl:if test="identity/address/postalCode != ''">
                                &#160;<xsl:value-of select="identity/address/postalCode" />
                              </xsl:if>

                              <!-- County information. -->
                              <xsl:if test="identity/address/county != ''">
                                &#160;<xsl:value-of select="identity/address/county" />
                              </xsl:if>
                            </td>
                          </tr>
                        </table>

                        <!-- Display details of each offense for each offender -->
                        <xsl:for-each select="record">
                          <div class="sorRecord" style="background-color:#fff;">

                            <h3>
                              <xsl:value-of select="provider"/>
                            </h3>
                            <h3>Cross-referenced by Date of Birth:</h3>
                            <h4>
                              <xsl:value-of select="DOBMatch"/>
                            </h4>
                            <br />
                            <h3>Cross-referenced by AKAs:</h3>
                            <h4>
                              <xsl:value-of select="AKAMatch"/>
                            </h4>
                            <br />
                            <h3>Real Name Match:</h3>
                            <h4>
                              <xsl:value-of select="realNameMatch"/>
                            </h4>
                            <br />

                            <xsl:for-each select="recordDetails/recordDetail">
                              <xsl:choose>
                                <xsl:when test="@origin = 'Crime'">
                                  <h4>
                                    <xsl:value-of select="../../../../record/provider" /> Specific Information
                                  </h4>
                                </xsl:when>
                                <xsl:otherwise>
                                  <h4>
                                    <xsl:value-of select="@origin" /> Information
                                  </h4>
                                </xsl:otherwise>
                              </xsl:choose>

                              <table class="tableddata">
                                <!-- Supplement information -->
                                <xsl:for-each select="supplements/supplement">
                                  <xsl:if test="displayValue != ''">
                                    <tr>
                                      <th>
                                        <xsl:value-of select="displayTitle"/>
                                      </th>
                                      <td>
                                        <xsl:value-of select="displayValue"/>
                                      </td>
                                    </tr>
                                  </xsl:if>
                                </xsl:for-each>
                              </table>

                            </xsl:for-each>
                            <!-- recordDetails/recordDetail -->

                          </div>

                        </xsl:for-each>
                        <!-- offenses/offense -->
                      </td>
                    </xsl:when>
                    <xsl:otherwise>
                      <td colspan="5" >
                        <table>
                          <tr>
                            <td width="50%" colspan="2" class="nameHeader">
                              <xsl:value-of select="identity/personal/fullName" />
                              <br />
                              <span class="idHeader">
                                (ID #<xsl:value-of select="record/key/offenderID" />)
                              </span>
                            </td>
                            <td rowspan="5" width="25%">
                              <xsl:if test="identity/personal/imageURL != ''">
                                <!--<xsl:value-of select="identity/personal/imageURL" /> -->
                                <img border="1" height="200" align="right">
                                  <xsl:attribute name="SRC">
                                    <xsl:value-of select="identity/personal/imageURL"/>
                                  </xsl:attribute>
                                </img>
                              </xsl:if>
                              &#160;
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2">
                              DOB:
                              <xsl:if test="identity/personal/DOB != ''">
                                <xsl:value-of select="identity/personal/DOB" />
                              </xsl:if>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2">
                              Aliases, AKA's and Maiden Names:

                              <!-- We need to place a comma after each name and obviously make sure the last name -->
                              <!-- does not have one. So we first we find the count of alias names available. -->
                              <xsl:variable name="namesCount" select="count(aliasNames/name)" />

                              <xsl:for-each select="aliasNames/name">
                                <!-- Display the alias name .&#160; is to add space. Equivalent to &nbsp; in HTML -->
                                <xsl:value-of select="firstName" />&#160;<xsl:value-of select="middleName" />&#160;<xsl:value-of select="lastName" />

                                <!-- Check if we need to add a comma or a period following the name. -->
                                <xsl:choose>
                                  <xsl:when test="$namesCount = position()"> . </xsl:when>
                                  <xsl:otherwise>, </xsl:otherwise>
                                </xsl:choose>
                              </xsl:for-each>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2">
                              Alternate DOBs:

                              <!-- We need to place a comma after each DOB and obviously make sure the last DOB -->
                              <!-- does not have one. So we first we find the count of alias DOBs available. -->
                              <xsl:variable name="dobCount" select="count(aliasDOBS/aliasDOB)" />

                              <xsl:for-each select="aliasDOBS/aliasDOB">
                                <!-- Display the alias DOB .&#160; is to add space. Equivalent to &nbsp; in HTML -->
                                <xsl:value-of select="DOB" />

                                <!-- Check if we need to add a comma or nothing following the DOB. -->
                                <xsl:choose>
                                  <xsl:when test="$dobCount = position()"></xsl:when>
                                  <xsl:otherwise>, </xsl:otherwise>
                                </xsl:choose>
                              </xsl:for-each>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <xsl:if test="identity/personal/race != ''">
                                Race: <xsl:value-of select="identity/personal/race" /> <br />
                              </xsl:if>

                              <xsl:if test="identity/personal/gender != ''">
                                <xsl:variable name="gender" select="identity/personal/gender" />

                                Gender:
                                <xsl:choose>
                                  <xsl:when test="$gender = 'M'">Male</xsl:when>
                                  <xsl:when test="$gender = 'F'">Female</xsl:when>
                                  <xsl:otherwise>Unknown</xsl:otherwise>
                                </xsl:choose>
                                <br />
                              </xsl:if>

                              <xsl:if test="identity/personal/hairColor != ''">
                                Hair: <xsl:value-of select="identity/personal/hairColor" /> <br />
                              </xsl:if>
                              <xsl:if test="identity/personal/eyeColor != ''">
                                Eyes: <xsl:value-of select="identity/personal/eyeColor" /> <br />
                              </xsl:if>

                              <xsl:if test="identity/personal/height != ''">
                                Height: <xsl:value-of select="identity/personal/height/feet" />' <xsl:value-of select="identity/personal/height/inches" />''<br />
                              </xsl:if>
                              <xsl:if test="identity/personal/weight != ''">
                                Weight: <xsl:value-of select="identity/personal/weight" /> <br />
                              </xsl:if>
                            </td>

                            <!-- Address information. -->

                            <td>
                              <h3>Address:</h3>
                              <br />
                              <!-- Street information. -->
                              <xsl:if test="identity/address/street != ''">
                                <xsl:value-of select="identity/address/street" />
                                <br/>
                              </xsl:if>

                              <!-- City, State and Zipcode info need to displayed side by side. -->
                              <xsl:if test="identity/address/city != ''">
                                <xsl:value-of select="identity/address/city" />
                              </xsl:if>

                              <xsl:choose>
                                <xsl:when test="identity/address/state != ''">
                                  <xsl:choose>
                                    <!-- Display comma after city only if there's state AND city info available. -->
                                    <xsl:when test="identity/address/city != ''">
                                      , &#160;<xsl:value-of select="identity/address/state" />
                                    </xsl:when>
                                    <xsl:otherwise>
                                      <xsl:value-of select="identity/address/state" />
                                    </xsl:otherwise>
                                  </xsl:choose>
                                </xsl:when>
                              </xsl:choose>

                              <!-- Zipcode information. -->
                              <xsl:if test="identity/address/postalCode != ''">
                                &#160;<xsl:value-of select="identity/address/postalCode" />
                              </xsl:if>

                              <!-- County information. -->
                              <xsl:if test="identity/address/county != ''">
                                &#160;<xsl:value-of select="identity/address/county" />
                              </xsl:if>
                            </td>
                          </tr>
                        </table>
                        <!-- Display details of each offense for each offender -->
                        <xsl:for-each select="record">
                          <div class="sorRecord">

                            <h3>
                              <xsl:value-of select="provider"/>
                            </h3>
                            <h3>Cross-referenced by Date of Birth:</h3>
                            <h4>
                              <xsl:value-of select="DOBMatch"/>
                            </h4>
                            <br />
                            <h3>Cross-referenced by AKAs:</h3>
                            <h4>
                              <xsl:value-of select="AKAMatch"/>
                            </h4>
                            <br />
                            <h3>Real Name Match:</h3>
                            <h4>
                              <xsl:value-of select="realNameMatch"/>
                            </h4>
                            <br />

                            <xsl:for-each select="recordDetails/recordDetail">
                              <xsl:choose>
                                <xsl:when test="@origin = 'Crime'">
                                  <h4>
                                    <xsl:value-of select="../../../../record/provider" /> Specific Information
                                  </h4>
                                </xsl:when>
                                <xsl:otherwise>
                                  <h4>
                                    <xsl:value-of select="@origin" /> Information
                                  </h4>
                                </xsl:otherwise>
                              </xsl:choose>

                              <table class="tableddata">
                                <!-- Supplement information -->
                                <xsl:for-each select="supplements/supplement">
                                  <xsl:if test="displayValue != ''">
                                    <tr>
                                      <th>
                                        <xsl:value-of select="displayTitle"/>
                                      </th>
                                      <td>
                                        <xsl:value-of select="displayValue"/>
                                      </td>
                                    </tr>
                                  </xsl:if>
                                </xsl:for-each>
                              </table>

                            </xsl:for-each>
                            <!-- recordDetails/recordDetail -->

                          </div>

                        </xsl:for-each>
                        <!-- offenses/offense -->
                      </td>
                    </xsl:otherwise>
                  </xsl:choose>




                
                <td  class="end">
                  <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                </td>
              </tr>
            </tbody>
            <tfoot>
              <td class="begin"></td>
              <td colspan="5"></td>
              <td class="end"></td>
            </tfoot>
          </table>

        
        </xsl:for-each>
        <!-- end offenders/offender -->

      </xsl:for-each>
      <!-- end detail -->

    </xsl:for-each>
    <!-- end response  -->


  </xsl:template>
</xsl:stylesheet>