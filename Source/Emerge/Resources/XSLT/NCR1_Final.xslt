<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:a="http://ns.hr-xml.org/2004-08-02">
  <xsl:output method="html" encoding="iso-8859-1" />
  <xsl:template match="/">

    <center>
      <div class="sectionsummary">
        <h3>
          (<xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)" />) Results CRIMINAL REPORT.<br />
        </h3>
      </div>
      <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
        <thead>
          <tr>
            <th class="begin"></th>
            <th colspan="5">
              <span style="font-size:20px;font-weight:bold;">Search Information</span>
            </th>
            <th class="end"></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="begin">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="5">
              <h4>Name:</h4>
              <h4>
                <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:GivenName"/>
                <xsl:text xml:space="preserve">  </xsl:text>
                <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:FamilyName"/>
              </h4>
              <br />
              <h4>DOB:</h4>
              <h4>
                <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:DemographicDetail/a:DateOfBirth"/>
              </h4>
            </td>
            <td class="end">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
        </tbody>
        <tfoot>
          <td class="begin"></td>
          <td colspan="5"></td>
          <td class="end"></td>
        </tfoot>
      </table>

      <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
        <thead>
          <tr>
            <th class="begin"></th>
            <th colspan="5">
              <span style="font-size:20px;font-weight:bold;">CRIMINAL INVESTIGATION</span>
            </th>
            <th class="end"></th>
          </tr>
        </thead>
        <tbody>
          <xsl:if test="not(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text)">
            <tr>
              <td class="begin">
              </td>
              <td colspan="5">
                <div style="width: 90%; margin: 0 auto; text-align: left">
                  <div style="width:588px; text-align:left">
                    <div style="font-size:11px;" id="report_ncr1">
                      <pre>
                        <!--<xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text">
                        <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text" disable-output-escaping="yes"/>
                      </xsl:if>-->
                        <xsl:if test="not(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text)">
                          <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus = 'Cancelled'">
                            <p>
                              There was an error running this report.<br /> Please contact our customer support team to quickly resolve this issue.
                            </p>
                          </xsl:if>
                          <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus != 'Cancelled'">
                            <p>*** Clear ***</p>
                          </xsl:if>
                          <p></p>
                        </xsl:if>
                      </pre>
                    </div>
                  </div>
                </div>
              </td>
              <td class="end">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          
          <xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase">
            <xsl:choose>
              <xsl:when test="position()>1">
                <tr style="height:20px;">
                  <td class="begin">
                  </td>
                  <td colspan="5" style="text-align:center;">
                    <span style="font-size:16px;font-weight:bold;color:white;">
                    </span>
                  </td>
                  <td class="end"></td>
                </tr>
              </xsl:when>
            </xsl:choose>
            
            <tr>
              <td class="begin">
              </td>
              <td colspan="5" style="border:solid 1px black;background-color:#D2D2D2;text-align:center;">
                <span style="font-size:16px;font-weight:bold;color:#000;">
                  Criminal Record Item #
                  <xsl:value-of select="(position())" />
                </span>
              </td>
              <td class="end"></td>
            </tr>

            <tr>
              <td class="begin">
              </td>
              <td colspan="5" style="border:solid 1px black;background-color:#F5F5F5;text-align:center;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr>
                    <td style="text-align:right;background-color:#F5F5F5;border-bottom:1px solid black;width:35%">
                      <b>Source:</b>
                    </td>
                    <td style="text-align:left;background-color:#F5F5F5;border-bottom:1px solid black;">
                      <xsl:value-of select="a:CourtName"/>
                    </td>
                  </tr>

                  <tr>
                    <td style="text-align:right;background-color:#F5F5F5;border-bottom:1px solid black;">
                      <b>Full Name:</b>
                    </td>
                    <td style="text-align:left;background-color:#F5F5F5;border-bottom:1px solid black;">
                      <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                        <xsl:if test="a:FamilyName/@primary='true'">
                          <xsl:value-of select="a:GivenName"/>
                          <xsl:text xml:space="preserve"> </xsl:text>
                          <xsl:value-of select="a:FamilyName"/>
                          <span>, </span>
                          <!--<xsl:element name="br"/>-->
                        </xsl:if>
                      </xsl:for-each>
                    </td>
                  </tr>

                  <xsl:if test="(a:SubjectIdentification/a:PersonName/a:FamilyName/@primary='false')">
                    <tr>
                      <td style="text-align:right;background-color:#F5F5F5;border-bottom:1px solid black;">
                        <b>AKA Name:</b>
                      </td>
                      <td style="text-align:left;background-color:#F5F5F5;border-bottom:1px solid black;">
                        <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                          <xsl:if test="a:FamilyName/@primary='false'">
                            <xsl:value-of select="a:GivenName"/>
                            <xsl:text xml:space="preserve"> </xsl:text>
                            <xsl:value-of select="a:FamilyName"/>
                            <span>, </span>
                            <!--<xsl:element name="br"/>-->
                          </xsl:if>
                        </xsl:for-each>
                      </td>
                    </tr>
                  </xsl:if>

                  <tr>
                    <td style="text-align:right;background-color:#F5F5F5;border-bottom:1px solid black;">
                      <b>DOB:</b>
                    </td>
                    <td style="text-align:left;background-color:#F5F5F5;border-bottom:1px solid black;">
                      <xsl:value-of select='substring(a:SubjectIdentification/a:DemographicDetail/a:DateOfBirth,1,10)' />
                    </td>
                  </tr>

                  <tr>
                    <td style="text-align:right;background-color:#F5F5F5;border-bottom:1px solid black;">
                      <b>Race:</b>
                    </td>
                    <td style="text-align:left;background-color:#F5F5F5;border-bottom:1px solid black;">
                      <xsl:value-of select="a:SubjectIdentification/a:DemographicDetail/a:Race"/>
                    </td>
                  </tr>

                  <tr>
                    <td style="text-align:right;background-color:#F5F5F5;border-bottom:1px solid black;">
                      <b>Gender:</b>
                    </td>
                    <td style="text-align:left;background-color:#F5F5F5;border-bottom:1px solid black;">
                      <xsl:choose>
                        <xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:GenderCode = 1">
                          Male
                        </xsl:when>
                        <xsl:otherwise>Female</xsl:otherwise>
                      </xsl:choose>
                    </td>
                  </tr>

                </table>
              </td>
              <td class="end"></td>
            </tr>

            <xsl:for-each select="a:Charge">
              <tr>
                <td class="begin">
                </td>
                <td colspan="5" style="border:solid 1px black;background-color:#D2D2D2;text-align:center;">
                  <span style="font-size:12px;font-weight:bold;color:#000;">
                    Record Charge #
                    <xsl:value-of select="(position())" />
                  </span>
                </td>
                <td class="end"></td>
              </tr>

              <tr>
                <td class="begin">
                </td>
                <td colspan="5" style="border:solid 1px black;background-color:#F5F5F5;text-align:center;">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td style="text-align:right;background-color:#F5F5F5;border-bottom:1px solid black;width:35%">
                        <b>Charge:</b>
                      </td>
                      <td style="text-align:left;background-color:#F5F5F5;border-bottom:1px solid black;">
                        <xsl:value-of select="a:ChargeOrComplaint"/>
                      </td>
                    </tr>

                    <xsl:if test="(a:AgencyReference[@type='Docket'])">
                      <tr>
                        <td style="text-align:right;background-color:#F5F5F5;border-bottom:1px solid black;">
                          <b>Docket Number:</b>
                        </td>

                        <td style="text-align:left;background-color:#F5F5F5;border-bottom:1px solid black;">
                          <xsl:value-of select="a:AgencyReference[@type='Docket']"/>
                        </td>
                      </tr>
                    </xsl:if>

                    <tr>
                      <td style="text-align:right;background-color:#F5F5F5;border-bottom:1px solid black;">
                        <b>Offense Date:</b>
                      </td>

                      <td style="text-align:left;background-color:#F5F5F5;border-bottom:1px solid black;">
                        <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                      </td>
                    </tr>

                    <tr>
                      <td style="text-align:right;background-color:#F5F5F5;border-bottom:1px solid black;">
                        <b>
                          Disposition Date:
                        </b>
                      </td>

                      <td style="text-align:left;background-color:#F5F5F5;border-bottom:1px solid black;">
                        <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                      </td>
                    </tr>

                    <tr>
                      <td style="text-align:right;background-color:#F5F5F5;border-bottom:1px solid black;">
                        <b>
                          Severity:
                        </b>
                      </td>

                      <td style="text-align:left;background-color:#F5F5F5;border-bottom:1px solid black;">
                        <xsl:value-of select="a:ChargeTypeClassification"/>
                      </td>
                    </tr>

                    <tr>
                      <td style="text-align:right;background-color:#F5F5F5;border-bottom:1px solid black;">
                        <b>
                          Final Disposition:
                        </b>
                      </td>

                      <td style="text-align:left;background-color:#F5F5F5;border-bottom:1px solid black;">
                        <xsl:value-of select="a:Disposition"/>
                      </td>
                    </tr>

                    <tr>
                      <td style="text-align:right;background-color:#F5F5F5;border-bottom:1px solid black;">
                        <b>
                          Comments:
                        </b>
                      </td>

                      <td style="text-align:left;background-color:#F5F5F5;border-bottom:1px solid black;">
                        <xsl:for-each select="a:Comment">
                          <span style="float:left;">
                            <xsl:value-of select="."/>
                            <xsl:element name="br" />
                          </span>
                        </xsl:for-each>
                      </td>
                    </tr>

                  </table>
                </td>
                <td class="end"></td>
              </tr>
            </xsl:for-each>
          </xsl:for-each>
        </tbody>
        <tfoot>
          <td class="begin"></td>
          <td colspan="5"></td>
          <td class="end"></td>
        </tfoot>
      </table>
    </center>

    <div class="sectionsummary">
      <h3 style="line-height: 1em;">

        Data Sources Searched:<br/>
        Court Records, Department of Corrections, Administrative Office of Courts,<br/>
        National Sex Offender Registry, OFAC, OIG<br/>
        For a complete list of coverage, please visit<br/>
        <a href="http://emerge.intelifi.com/coverage.pdf" target="_blank">US Criminal Rapid Search Coverage Guide</a>

      </h3>
    </div>
  </xsl:template>
</xsl:stylesheet>