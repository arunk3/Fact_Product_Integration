<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" />
  <xsl:template match="/BGC/product/StatewideCriminalSearch/fulfillment">

    <div class="reportName">
      <h3>Statewide Criminal Report</h3>
    </div>
	
    <div class="resultsHeader">
      (<xsl:value-of select="count(results/result/counts/count)"/>) Results
    </div>
   <div style="clear:both"></div>
    <xsl:choose>
      <xsl:when test="count(results/result/counts/count)>0">
        <div class="content" style="div-layout: fixed; width: 46%; padding-left: 3em;">
          <xsl:for-each select="applicantInfo">
            <!--<div>
                            <th>SSN</th>
                            <div>
                                <xsl:value-of select="SSN"/>
                            </div>
                        </div>-->
            <div>
              <th>DOB</th>
              <div>
                <xsl:value-of select="DOB/month"/>/<xsl:value-of select="DOB/day"/>/<xsl:value-of select="DOB/year"/>
              </div>
            </div>
          </xsl:for-each>
          <xsl:for-each select="results/result">
            <div style="width:100%;">
            <div style="margin-top:1%;">
            <div style="border:solid 1px black;background-color:black;text-align:left;">
              <span style="font-size:16px;font-weight:bold;color:#fff;">
                Criminal Record #
                <xsl:value-of select="(position())" />
              </span>
            </div>

            </div>
            <div style="clear:both"></div>

              <div style="margin-left:7%;margin-top:.2%;">
            <div>
              <xsl:if test="DispositionDate != ''">
                <div style="float:left;width:7%;background-color:white;border-bottom:0px solid black;text-align:right;">Disposition Date :</div>
                <div style="float:left;background-color:white;border-bottom:0px solid black;margin-left:1%;">
                  <xsl:value-of select="DispositionDate"/>
                </div>
              </xsl:if>
            </div>

            <div style="clear:both"></div>

            <div style="margin-top:1%;">
              <xsl:if test="isDOBVerified != ''">
                <div style="float:left;width:7%;background-color:white;border-bottom:0px solid black;text-align:right;"> <b>DOB Verified :</b>
                </div>
                <div style="float:left;background-color:white;border-bottom:0px solid black;margin-left:1%;">
                  
                  <xsl:value-of select="isDOBVerified"/>
                </div>
              </xsl:if>
            </div>
            <div style="clear:both"></div>
            <div style="margin-top:1%;">
              <xsl:if test="isSSNVerified != ''">
                <div style="float:left;width:7%;background-color:white;border-bottom:0px solid black;text-align:right;">
                  <b>SSN Verified :</b>
                </div>
                <div style="float:left;background-color:white;border-bottom:0px solid black;margin-left:1%;">
                  <xsl:value-of select="isSSNVerified"/>
                </div>
              </xsl:if>
            </div>


            <div style="clear:both"></div>

            <div style="margin-top:1%;">
              <xsl:if test="nameOnFile != ''">
                <div style="float:left;width:7%;background-color:white;border-bottom:0px solid black;text-align:right;">
                  <b>NAME ON FILE :</b>
                </div>
                <div style="float:left;background-color:white;border-bottom:0px solid black;margin-left:1%;">
                  <xsl:value-of select="nameOnFile"/>
                </div>
              </xsl:if>
            </div>
            <div style="clear:both"></div>
              </div>
            </div>
            
            
            
            
            
            
            
            
            
            

            <div style="width:99%;margin-top:1%;background-color:#dcdcdc;margin-left:1%;">
              
              <div style="float:left;width:49%;background-color:#dcdcdc;height:169px;border-left:1px solid  black;border-top:1px solid  black;border-bottom:1px solid  black;background-color:#dcdcdc;">
            
            <div style="margin-top:1%;">
              <xsl:if test="state != ''">
                <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;width:16%;text-align:right;">
                  <b>STATE :</b>
                </div>
                <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;margin-left:1%;">
                  <xsl:value-of select="state"/>
                </div>
              </xsl:if>
            </div>
            <div style="clear:both"></div>
            
            
            <div style="margin-top:1%;">
              <xsl:if test="caseNumber != ''">
                <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;width:16%;text-align:right;">
                  <b>CASE NUMBER :</b>
              </div>
                <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;margin-left:1%;">
                  <xsl:value-of select="caseNumber"/>
                </div>
              </xsl:if>
            </div>
            <div style="clear:both"></div>
            
            
            <div style="margin-top:1%;">
              <xsl:if test="fileDate != ''">
                <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;width:16%;text-align:right;">
                  <b>FILE DATE :</b>
                </div>
                <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;margin-left:1%;">
                  <xsl:value-of select="fileDate"/>
                </div>
              </xsl:if>
            </div>

            <div style="clear:both"></div>
              </div>
              
              
              
              
              <div style="float:left;width:49%;background-color:#dcdcdc;height:169px;border-right:1px solid  black;border-top:1px solid  black;border-bottom:1px solid  black;background-color:#dcdcdc;">
                  <xsl:for-each select="counts/count">
              <div style="margin-top:1%;">
                <xsl:if test="crimeDetails != ''">
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;width:16%;text-align:right;" >
                    <b>CRIME DETAILS :</b>
                  </div>
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;margin-left:1%;">
                    <xsl:value-of select="crimeDetails"/>
                  </div>
                </xsl:if>
              </div>
                    <div style="clear:both"></div>
              <div style="margin-top:1%;">
                <xsl:if test="crimeType != ''">
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;width:16%;text-align:right;">
                    <b>CRIME TYPE :</b>
                  </div>
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;margin-left:1%;">
                    <xsl:value-of select="crimeType"/>
                  </div>
                </xsl:if>
              </div>
                    <div style="clear:both"></div>
              <div style="margin-top:1%;">
                <xsl:if test="disposition != ''">
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;width:16%;text-align:right;">
                    <b>DISPOSITION :</b>
                  </div>
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;margin-left:1%;">
                    <xsl:value-of select="disposition"/>
                  </div>
                </xsl:if>
              </div>
                    <div style="clear:both"></div>
              <div style="margin-top:1%;">
                <xsl:if test="dispositionDate != ''">
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;width:16%;text-align:right;">
                    <b>DISPOSITION DATE :</b>
                  </div>
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;margin-left:1%;">
                    <xsl:value-of select="dispostionDate"/>
                  </div>
                </xsl:if>
              </div>
                    <div style="clear:both"></div>
              <div style="margin-top:1%;">
                <xsl:if test="probation != ''">
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;width:16%;text-align:right;">
                    <b>PROBATION :</b>
                  </div>
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;margin-left:1%;">
                    <xsl:value-of select="probation"/>
                  </div>
                </xsl:if>
              </div>
                    <div style="clear:both"></div>
              <div style="margin-top:1%;">
                <xsl:if test="jailTime != ''">
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;width:16%;text-align:right;">
                    <b>JAIL TIME :</b>
                  </div>
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;margin-left:1%;">
                    <xsl:value-of select="jailTime"/>
                  </div>
                </xsl:if>
              </div>
                    <div style="clear:both"></div>
              <div style="margin-top:1%;">
                <xsl:if test="prisonTime != ''">
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;width:16%;text-align:right;">
                    <b>PRISON TIME :</b>
                  </div>
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;margin-left:1%;">
                    <xsl:value-of select="prisonTime"/>
                  </div>
                </xsl:if>
              </div>
                    <div style="clear:both"></div>
              <div style="margin-top:1%;">
                <xsl:if test="suspensionTime != ''">
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;width:16%;text-align:right;">
                    <b>SUSPENSION TIME :</b>
                  </div>
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;margin-left:1%;">
                    <xsl:value-of select="suspensionTime"/>
                  </div>
                </xsl:if>
              </div>
                    <div style="clear:both"></div>
              <div style="margin-top:1%;">
                <xsl:if test="fines != ''">
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;width:16%;text-align:right;">
                    <b>FINES :</b>
                  </div>
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;margin-left:1%;">
                    <xsl:value-of select="fines"/>
                  </div>
                </xsl:if>
              </div>

                    <div style="clear:both"></div>
                    
              <div style="margin-top:1%;">
                <xsl:if test="courtCosts!= ''">
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;width:16%;text-align:right;">
                    <b>COURT COSTS :</b>
                  </div>
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;margin-left:1%;">
                    <xsl:value-of select="courtCosts"/>
                  </div>
                </xsl:if>
              </div>

                    <div style="clear:both"></div>
              <div style="margin-top:1%;"> 
                <xsl:if test="additionalDispositionInfo != ''">
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;width:16%;text-align:right;">
                    <b>ADDITONAL DISPOSITION INFORMATION :</b>
                  </div>
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;margin-left:1%;">
                    <xsl:value-of select="additionalDispositionInfo"/>
                  </div>
                </xsl:if>
              </div>

                    <div style="clear:both"></div>
                    
              <div style="margin-top:1%;">
                <xsl:if test="generalNotes != ''">
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;width:16%;text-align:right;">
                    <b>GENERAL NOTES :</b>
                  </div>
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;margin-left:1%;">
                    <xsl:value-of select="generalNotes"/>
                  </div>
                </xsl:if>
              </div>

                    <div style="clear:both"></div>
              <div style="margin-top:1%;">
                <xsl:if test="fileDate != ''">
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;width:16%;text-align:right;">
                    <b>FILE DATE :</b>
                  </div>
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;margin-left:1%;">
                    <xsl:value-of select="fileDate"/>
                  </div>
                </xsl:if>
              </div>
                    <div style="clear:both"></div>
              <div style="margin-top:1%;">
                <xsl:if test="offenseDate != ''">
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;width:16%;text-align:right;">
                    <b>OFFENSE DATE :</b>
                  </div>
                  <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;margin-left:1%;">
                    <xsl:value-of select="offenseDate"/>
                  </div>
                </xsl:if>
              </div>
                    <div style="clear:both"></div>

            </xsl:for-each>
              </div>
              
              
            </div>

            <div style="clear:both"></div>
            
            <div style="margin-top:1%;">
              <xsl:if test="AdditionalVerificationNotes != ''">
                <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;width:16%;text-align:right;">
                  <b>Additional Verification Notes :</b>
                </div>
                <div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;margin-left:1%;">
                  <xsl:value-of select="AdditionalVerificationNotes"/>
                </div>
              </xsl:if>
            </div>
          

           
          </xsl:for-each>
        </div>
      </xsl:when>
      <xsl:otherwise>
        <center>
          <div style="color:red;font-weight:bold;font-size:12px;">
            No Record Found
          </div>
        </center>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="/BGC/product/StatewideCriminalSearch/response">
    <div class="noRecords">
      No Records Found.
      <br />
      * CLEAR *
    </div>
  </xsl:template>
</xsl:stylesheet>
