﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:output method="xml" indent="yes"/>

  <xsl:template match="/">

    <table cellspacing="0" cellpadding="0" border="0">
      <tbody>
        <tr>
          <td class="center">
            <!-- main table -->
            <table cellspacing="0" cellpadding="0" border="0">
              <!-- top menu -->
              <tbody>
                <!-- end top menu -->
                <tr>
                  <td class="content">
                    <table cellspacing="0" cellpadding="0" border="0" class="tab_bg">
                      <tbody>
                        <tr>
                          <td>
                            <!-- start tab top 1 -->
                            <table cellspacing="0" cellpadding="0" border="0" class="tab-up">
                              <tbody>
                                <tr>


                                  <xsl:if test="(count(/task/networks/imdb/result)!='0')">
                                    <td class="tab-upleft-grey" id="imdb_l">
                                      <img src="http://www.socialdiligence.com/images/tab_01_1.gif" />
                                    </td>
                                    <td onclick="sw_tab_sd('imdb');source_results(0,'imdb');" class="tab_m" id="imdb_m">
                                      <a href="javascript:;">
                                        <img src="http://www.socialdiligence.com/images/imdb01.gif" />
                                      </a>
                                    </td>
                                    <td class="tab-upright-grey" id="imdb_r">
                                      <img src="http://www.socialdiligence.com/images/tab_01_2.gif" />
                                    </td>
                                  </xsl:if>


                                  <xsl:if test="(count(/task/networks/amazon/result)!='0')">
                                    <td class="tab-upleft-grey" id="amazon_l">
                                      <img src="http://www.socialdiligence.com/images/tab_01_1.gif" />
                                    </td>
                                    <td onclick="sw_tab_sd('amazon');source_results(0,'amazon');" class="tab_m" id="amazon_m">
                                      <a href="javascript:;">
                                        <img src="http://www.socialdiligence.com/images/amazon01.gif" />
                                      </a>
                                    </td>
                                    <td class="tab-upright-grey" id="amazon_r">
                                      <img src="http://www.socialdiligence.com/images/tab_01_2.gif" />
                                    </td>
                                  </xsl:if>

                                  <xsl:if test="(count(/task/networks/wikipedia/result)!='0')">
                                    <td class="tab-upleft-grey" id="wikipedia_l">
                                      <img src="http://www.socialdiligence.com/images/tab_01_1.gif" />
                                    </td>
                                    <td onclick="sw_tab_sd('wikipedia');source_results(0,'wikipedia');" class="tab_m"
                                        id="wikipedia_m">
                                      <a href="javascript:;">
                                        <img src="http://www.socialdiligence.com/images/wikipedia01.gif" />
                                      </a>
                                    </td>
                                    <td class="tab-upright-grey" id="wikipedia_r">
                                      <img src="http://www.socialdiligence.com/images/tab_01_2.gif" />
                                    </td>
                                  </xsl:if>

                                  <td class="tab-upright-last">
                                    <xsl:text>*</xsl:text>
                                  </td>
                                </tr>
                              </tbody>
                            </table>

                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <!-- end first row of tabs -->
                    <!-- start second row of tabs -->
                    <table cellspacing="0" cellpadding="0" border="0" class="tab_bg">
                      <tbody>
                        <tr>
                          <td class="white">
                            <!-- start tab top 2-1 -->
                            <table cellspacing="0" cellpadding="0" border="0" class="tab-up">
                              <tbody>
                                <tr>

                                  <td class="tab-upleft-grey" id="snresult_clear_l">
                                    <img src="http://socialdiligence.com/images/tab_01_1.gif" />
                                  </td>
                                  <td onclick="sw_tab_sd('snresult_clear');source_results(0,'snresult_clear');"
                                      class="tab_m" id="snresult_clear_m">
                                    <a href="javascript:;">
                                      <img src="http://socialdiligence.com/images/snresult_clear01.gif" />
                                    </a>
                                  </td>
                                  <td class="tab-upright-grey" id="snresult_clear_r">
                                    <img src="http://socialdiligence.com/images/tab_01_2.gif" />
                                  </td>

                                  <!-- end tab top 1 -->
                                  <!-- start tab top 2-2 -->

                                  <xsl:if test="(count(/task/networks/linkedin)>='0')">
                                    <td class="tab-upleft-grey" id="linkedin_l">
                                      <img src="http://www.socialdiligence.com/images/tab_01_1.gif" />
                                    </td>
                                    <td onclick="sw_tab_sd('linkedin');source_results(0,'linkedin');" class="tab_m"
                                        id="linkedin_m">
                                      <a href="javascript:;">
                                        <img src="http://www.socialdiligence.com/images/linkedin01.gif" />
                                      </a>
                                    </td>
                                    <td class="tab-upright-grey" id="linkedin_r">
                                      <img src="http://www.socialdiligence.com/images/tab_01_2.gif" />
                                    </td>
                                  </xsl:if>
                                  <!-- end tab top 2-2 -->

                                  <!-- start tab top 2-3 -->
                                  <xsl:if test="(count(/task/networks/myspace)>='0')">
                                    <td class="tab-upleft-white" id="myspace_l">
                                      <img src="http://socialdiligence.com/images/tab_02_1.gif" />
                                    </td>
                                    <td onclick="sw_tab_sd('myspace');source_results(0,'myspace');" class="tab_bgsel"
                                        id="myspace_m">
                                      <a href="javascript:;">
                                        <img src="http://socialdiligence.com/images/myspace02.gif" />
                                      </a>
                                    </td>
                                    <td class="tab-upright-white" id="myspace_r">
                                      <img src="http://socialdiligence.com/images/tab_02_2.gif" />
                                    </td>
                                  </xsl:if>
                                  <!-- end tab top 2-3 -->

                                  <!-- start tab top 2-4 -->
                                  <xsl:if test="(count(/task/networks/facebook)>='0')">
                                    <td class="tab-upleft-grey" id="facebook_l">
                                      <img src="http://www.socialdiligence.com/images/tab_01_1.gif" />
                                    </td>
                                    <td onclick="sw_tab_sd('facebook');source_results(0,'facebook');" class="tab_m"
                                        id="facebook_m">
                                      <a href="javascript:;">
                                        <img src="http://www.socialdiligence.com/images/facebook01.gif" />
                                      </a>
                                    </td>
                                    <td class="tab-upright-grey" id="facebook_r">
                                      <img src="http://www.socialdiligence.com/images/tab_01_2.gif" />
                                    </td>
                                  </xsl:if>
                                  <!-- end tab top 2-4 -->

                                  <!-- start tab top 2-5 -->
                                  <xsl:if test="(count(/task/networks/friendster)>='0')">
                                    <td class="tab-upleft-grey" id="friendster_l">
                                      <img src="http://www.socialdiligence.com/images/tab_01_1.gif" />
                                    </td>
                                    <td onclick="sw_tab_sd('friendster');source_results(0,'friendster');" class="tab_m"
                                        id="friendster_m">
                                      <a href="javascript:;">
                                        <img src="http://www.socialdiligence.com/images/friendster01.gif" />
                                      </a>
                                    </td>
                                    <td class="tab-upright-grey" id="friendster_r">
                                      <img src="http://www.socialdiligence.com/images/tab_01_2.gif" />
                                    </td>
                                  </xsl:if>
                                  <!-- end tab top 2-5 -->

                                  <!-- start tab top 2-7 -->
                                  <xsl:if test="(count(/task/networks/photos)>='0')">
                                    <td class="tab-upleft-grey" id="flickr_l">
                                      <img src="http://www.socialdiligence.com/images/tab_01_1.gif" />
                                    </td>
                                    <td onclick="sw_tab_sd('flickr');source_results(0,'flickr');" class="tab_m" id="flickr_m">
                                      <a href="javascript:;">
                                        <img src="http://www.socialdiligence.com/images/flickr01.gif" />
                                      </a>
                                    </td>
                                    <td class="tab-upright-grey" id="flickr_r">
                                      <img src="http://www.socialdiligence.com/images/tab_01_2.gif" />
                                    </td>
                                  </xsl:if>
                                  <!-- end tab top 2-7 -->

                                  <!-- start tab top 2-8 -->
                                  <xsl:if test="(count(/task/networks/videos)>='0')">
                                    <td class="tab-upleft-grey" id="youtube_l">
                                      <img src="http://www.socialdiligence.com/images/tab_01_1.gif" />
                                    </td>
                                    <td onclick="sw_tab_sd('youtube');source_results(0,'youtube');" class="tab_m"
                                        id="youtube_m">
                                      <a href="javascript:;">
                                        <img src="http://www.socialdiligence.com/images/youtube01.gif" />
                                      </a>
                                    </td>
                                    <td class="tab-upright-grey" id="youtube_r">
                                      <img src="http://www.socialdiligence.com/images/tab_01_2.gif" />
                                    </td>
                                  </xsl:if>
                                  <!-- end tab top 2-8 -->

                                  <!-- start tab top 2-9 -->
                                  <xsl:if test="(count(/task/networks/emails)>='0')">
                                    <td class="tab-upleft-grey" id="emails_l">
                                      <img src="http://www.socialdiligence.com/images/tab_01_1.gif" />
                                    </td>
                                    <td onclick="sw_tab_sd('emails');source_results(0,'emails');" class="tab_m" id="emails_m">
                                      <a href="javascript:;">
                                        <img src="http://www.socialdiligence.com/images/emails01.gif" />
                                      </a>
                                    </td>
                                    <td class="tab-upright-grey" id="emails_r">
                                      <img src="http://www.socialdiligence.com/images/tab_01_2.gif" />
                                    </td>
                                  </xsl:if>
                                  <!-- end tab top 2-9 -->

                                  <!-- start tab top 2-6 -->
                                  <xsl:if test="(count(/task/networks/twitter)>='0')">
                                    <td class="tab-upleft-grey" id="twitter_l">
                                      <img src="http://www.socialdiligence.com/images/tab_01_1.gif" />
                                    </td>
                                    <td onclick="sw_tab_sd('twitter');source_results(0,'twitter');" class="tab_m"
                                        id="twitter_m">
                                      <a href="javascript:;">
                                        <img src="http://www.socialdiligence.com/images/twitter01.gif" />
                                      </a>
                                    </td>
                                    <td class="tab-upright-grey" id="twitter_r">
                                      <img src="http://www.socialdiligence.com/images/tab_01_2.gif" />
                                    </td>
                                  </xsl:if>

                                  <td class="tab-upright-last">
                                    <xsl:text>*</xsl:text>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <!-- end second row of tabs -->
                    <!-- end search results tabs -->

                    <xsl:variable name="count_l" select="count(/task/networks/linkedin/result)"  />
                    <xsl:variable name="count_m" select="count(/task/networks/myspace/result)"  />
                    <xsl:variable name="count_fa" select="count(/task/networks/facebook/result)"  />
                    <xsl:variable name="count_fr" select="count(/task/networks/friendster/result)"  />
                    <xsl:variable name="count_p" select="count(/task/networks/photos/result)"  />
                    <xsl:variable name="count_v" select="count(/task/networks/videos/result)"  />
                    <xsl:variable name="count_t" select="count(/task/networks/twitter/result)"  />




                    <div id="snnetworks_list" style="clear:both;">
                      <!-- results with images -->
                      <table cellspacing="0" cellpadding="0" border="0" class="stats">
                        <tbody>
                          <tr id="numbers" class="numbers">
                            <td>
                              <img height="1px" width="100px" src="http://www.socialdiligence.com/images/blank.gif" />
                            </td>
                            <td>
                              <a onclick="sw_tab_sd('linkedin');source_results(0,'linkedin');" href="javascript:;">
                                <xsl:value-of select="$count_l"/> Results
                              </a>
                            </td>
                            <td>
                              <a onclick="sw_tab_sd('myspace');source_results(0,'myspace');" href="javascript:;">

                                <xsl:value-of select="$count_m"/> Results
                              </a>
                            </td>
                            <td>
                              <a onclick="sw_tab_sd('facebook');source_results(0,'facebook');" href="javascript:;">
                                <xsl:value-of select="$count_fa"/>
                                Results
                              </a>
                            </td>
                            <td>
                              <a onclick="sw_tab_sd('friendster');source_results(0,'friendster');" href="javascript:;">
                                <xsl:value-of select="$count_fr"/>
                                Results
                              </a>
                            </td>
                            <td>
                              <a onclick="sw_tab_sd('flickr');source_results(0,'flickr');" href="javascript:;">
                                <xsl:value-of select="$count_p"/>
                                Results
                              </a>
                            </td>
                            <td>
                              <a onclick="sw_tab_sd('youtube');source_results(0,'youtube');" href="javascript:;">
                                <xsl:value-of select="$count_v"/>
                                Results
                              </a>
                            </td>
                            <td>
                              <div id="emails_block">
                                <font color="red">...in progress</font>
                                <br/>

                              </div>
                              <div id="emails_found">
                                <a onclick="sw_tab_sd('emails');source_results(0,'emails');" href="javascript:;">
                                  Results
                                </a>
                              </div>
                            </td>
                            <td>
                              <a onclick="sw_tab_sd('twitter');source_results(0,'twitter');" href="javascript:;">
                                <xsl:value-of select="$count_t"/>
                                Results
                              </a>
                            </td>
                          </tr>
                          <tr>
                            <td class="summary">
                              <strong>
                                <!--<xsl:variable name="TotalSearch" select="0"/>-->
                                <!--<xsl:for-each select="/task/networks" >-->
                                <xsl:value-of select="(count(/task/networks/twitter/result) + count(/task/networks/myspace/result)+ count(/task/networks/videos/result)+ count(/task/networks/photos/result)+ count(/task/networks/friendster/result)+ count(/task/networks/facebook/result)+ count(/task/networks/linkedin/result))"/>
                                <!--</xsl:for-each>-->
                              </strong>
                              <br/>
                              results found<br/>
                              <br/>
                            </td>
                            <xsl:variable name="random" select="645294565"/>

                            <!--  For Linkedin  -->
                            <xsl:variable name="href_l" select="/task/networks/linkedin/result[position()=($random mod $count_l)+1]"/>
                            <xsl:choose >
                              <xsl:when test="($count_l='0')">
                                <td class="nores">
                                  <img height="1px" width="100px" src="http://www.socialdiligence.com/images/blank.gif" />
                                  <br/>
                                  No Results
                                </td>
                              </xsl:when>
                              <xsl:otherwise>
                                <td>
                                  <img height="1px" width="100px" src="http://www.socialdiligence.com/images/blank.gif" />
                                  <!-- result block -->
                                  <table cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td class="image">
                                          <xsl:choose>
                                            <xsl:when test="count($href_l/img)='0'">
                                              <xsl:text>
                                                 
                                              </xsl:text>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              <div class="imgdiv">
                                                <a class="tip" target="_blank">
                                                  <xsl:attribute name="href">
                                                    <xsl:value-of select="$href_l/url" />
                                                  </xsl:attribute>
                                                  <image>
                                                    <xsl:attribute name="src">
                                                      <xsl:value-of select="$href_l/img/src" />
                                                    </xsl:attribute>
                                                  </image>
                                                </a>
                                              </div>
                                            </xsl:otherwise>
                                          </xsl:choose>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>
                                          <p class="ctr">
                                            <a target="_blank" class="tip">
                                              <xsl:attribute name="href">
                                                <xsl:value-of select="$href_l/url" />
                                              </xsl:attribute>
                                              <xsl:value-of select="$href_l/visible" />
                                            </a>
                                          </p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td class="pr">
                                          <a target="_blank" class="public">
                                            <xsl:attribute name="href">
                                              <xsl:value-of select="$href_l/url" />
                                            </xsl:attribute>
                                            <xsl:text>
                                        Public Records
                                        </xsl:text>
                                          </a>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <!-- end result block -->
                                </td>
                              </xsl:otherwise>
                            </xsl:choose>
                            <!--  For Linkedin END -->

                            <!--  For My Space  -->
                            <xsl:variable name="href_m" select="/task/networks/myspace/result[position()=($random mod $count_m)+1]"/>
                            <xsl:choose >
                              <xsl:when test="($count_m='0')">
                                <td class="nores">
                                  <img height="1px" width="100px" src="http://www.socialdiligence.com/images/blank.gif" />
                                  <br/>
                                  No Results
                                </td>
                              </xsl:when>
                              <xsl:otherwise>
                                <td>
                                  <img height="1px" width="100px" src="http://www.socialdiligence.com/images/blank.gif" />
                                  <!-- result block -->
                                  <table cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td class="image">
                                          <xsl:choose>
                                            <xsl:when test="count($href_m/img)='0'">
                                              <xsl:text>
                                                 
                                              </xsl:text>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              <div class="imgdiv">
                                                <a class="tip" target="_blank">
                                                  <xsl:attribute name="href">
                                                    <xsl:value-of select="$href_m/url" />
                                                  </xsl:attribute>
                                                  <image>
                                                    <xsl:attribute name="src">
                                                      <xsl:value-of select="$href_m/img/src" />
                                                    </xsl:attribute>
                                                  </image>
                                                </a>
                                              </div>
                                            </xsl:otherwise>
                                          </xsl:choose>

                                        </td>
                                      </tr>
                                      <tr>
                                        <td>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>
                                          <p class="ctr">
                                            <a target="_blank" class="tip">
                                              <xsl:attribute name="href">
                                                <xsl:value-of select="$href_m/url" />
                                              </xsl:attribute>
                                              <xsl:value-of select="$href_m/visible" />
                                            </a>
                                          </p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td class="pr">
                                          <a target="_blank" class="public">
                                            <xsl:attribute name="href">
                                              <xsl:value-of select="$href_m/url" />
                                            </xsl:attribute>
                                            <xsl:text>
                                        Public Records
                                        </xsl:text>
                                          </a>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <!-- end result block -->
                                </td>
                              </xsl:otherwise>
                            </xsl:choose>
                            <!--  For My Space END -->

                            <!--  For Face Book  -->
                            <xsl:variable name="href_fa" select="/task/networks/facebook/result[position()=($random mod $count_fa)+1]"/>
                            <xsl:choose >
                              <xsl:when test="($count_fa='0')">
                                <td class="nores">
                                  <img height="1px" width="100px" src="http://www.socialdiligence.com/images/blank.gif" />
                                  <br/>
                                  No Results
                                </td>
                              </xsl:when>
                              <xsl:otherwise>
                                <td>
                                  <img height="1px" width="100px" src="http://www.socialdiligence.com/images/blank.gif" />
                                  <!-- result block -->
                                  <table cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td class="image">
                                          <xsl:choose>
                                            <xsl:when test="count($href_fa/img)='0'">
                                              <xsl:text>
                                                 
                                              </xsl:text>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              <div class="imgdiv">
                                                <a class="tip" target="_blank">
                                                  <xsl:attribute name="href">
                                                    <xsl:value-of select="$href_fa/url" />
                                                  </xsl:attribute>
                                                  <image>
                                                    <xsl:attribute name="src">
                                                      <xsl:value-of select="$href_fa/img/src" />
                                                    </xsl:attribute>
                                                  </image>
                                                </a>
                                              </div>
                                            </xsl:otherwise>
                                          </xsl:choose>

                                        </td>
                                      </tr>
                                      <tr>
                                        <td>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>
                                          <p class="ctr">
                                            <a target="_blank" class="tip">
                                              <xsl:attribute name="href">
                                                <xsl:value-of select="$href_fa/url" />
                                              </xsl:attribute>
                                              <xsl:value-of select="$href_fa/visible" />
                                            </a>
                                          </p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td class="pr">
                                          <a target="_blank" class="public">
                                            <xsl:attribute name="href">
                                              <xsl:value-of select="$href_fa/url" />
                                            </xsl:attribute>
                                            <xsl:text>
                                        Public Records
                                        </xsl:text>
                                          </a>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <!-- end result block -->
                                </td>
                              </xsl:otherwise>
                            </xsl:choose>
                            <!--  For Face Book END -->


                            <!--  For friendster  -->
                            <xsl:variable name="href_fr" select="/task/networks/friendster/result[position()=($random mod $count_fr)+1]"/>
                            <xsl:choose >
                              <xsl:when test="($count_fr='0')">
                                <td class="nores">
                                  <img height="1px" width="100px" src="http://www.socialdiligence.com/images/blank.gif" />
                                  <br/>
                                  No Results
                                </td>
                              </xsl:when>
                              <xsl:otherwise>
                                <td>
                                  <img height="1px" width="100px" src="http://www.socialdiligence.com/images/blank.gif" />
                                  <!-- result block -->
                                  <table cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td class="image">
                                          <xsl:choose>
                                            <xsl:when test="count($href_fr/img)='0'">
                                              <xsl:text>
                                                 
                                              </xsl:text>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              <div class="imgdiv">
                                                <a class="tip" target="_blank">
                                                  <xsl:attribute name="href">
                                                    <xsl:value-of select="$href_fr/url" />
                                                  </xsl:attribute>
                                                  <image>
                                                    <xsl:attribute name="src">
                                                      <xsl:value-of select="$href_fr/img/src" />
                                                    </xsl:attribute>
                                                  </image>
                                                </a>
                                              </div>
                                            </xsl:otherwise>
                                          </xsl:choose>

                                        </td>
                                      </tr>
                                      <tr>
                                        <td>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>
                                          <p class="ctr">
                                            <a target="_blank" class="tip">
                                              <xsl:attribute name="href">
                                                <xsl:value-of select="$href_fr/url" />
                                              </xsl:attribute>
                                              <xsl:value-of select="$href_fr/visible" />
                                            </a>
                                          </p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td class="pr">
                                          <a target="_blank" class="public">
                                            <xsl:attribute name="href">
                                              <xsl:value-of select="$href_fr/url" />
                                            </xsl:attribute>
                                            <xsl:text>
                                        Public Records
                                        </xsl:text>
                                          </a>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <!-- end result block -->
                                </td>
                              </xsl:otherwise>
                            </xsl:choose>
                            <!--  For friendster END -->

                            <!--  For Images  -->
                            <xsl:variable name="href_p" select="/task/networks/photos/result[position()=($random mod $count_p)+1]"/>
                            <xsl:choose >
                              <xsl:when test="($count_p='0')">
                                <td class="nores">
                                  <img height="1px" width="100px" src="http://www.socialdiligence.com/images/blank.gif" />
                                  <br/>
                                  No Results
                                </td>
                              </xsl:when>
                              <xsl:otherwise>
                                <td>
                                  <img height="1px" width="100px" src="http://www.socialdiligence.com/images/blank.gif" />
                                  <!-- result block -->
                                  <table cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td class="image">
                                          <xsl:choose>
                                            <xsl:when test="count($href_p/img)='0'">
                                              <xsl:text>
                                                 
                                              </xsl:text>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              <div class="imgdiv">
                                                <a class="tip" target="_blank">
                                                  <xsl:attribute name="href">
                                                    <xsl:value-of select="$href_p/url" />
                                                  </xsl:attribute>
                                                  <image>
                                                    <xsl:attribute name="src">
                                                      <xsl:value-of select="$href_p/img/src" />
                                                    </xsl:attribute>
                                                  </image>
                                                </a>
                                              </div>
                                            </xsl:otherwise>
                                          </xsl:choose>

                                        </td>
                                      </tr>
                                      <tr>
                                        <td>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>
                                          <p class="ctr">
                                            <a target="_blank" class="tip">
                                              <xsl:attribute name="href">
                                                <xsl:value-of select="$href_p/url" />
                                              </xsl:attribute>
                                              <xsl:value-of select="$href_p/visible" />
                                            </a>
                                          </p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td class="pr">
                                          <a target="_blank" class="public">
                                            <xsl:attribute name="href">
                                              <xsl:value-of select="$href_p/url" />
                                            </xsl:attribute>
                                            <xsl:text>
                                        Public Records
                                        </xsl:text>
                                          </a>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <!-- end result block -->
                                </td>
                              </xsl:otherwise>
                            </xsl:choose>
                            <!--  For Images END -->


                            <!--  For Videos  -->
                            <xsl:variable name="href_v" select="/task/networks/videos/result[position()=($random mod $count_v)+1]"/>
                            <xsl:choose >
                              <xsl:when test="($count_v='0')">
                                <td class="nores">
                                  <img height="1px" width="100px" src="http://www.socialdiligence.com/images/blank.gif" />
                                  <br/>
                                  No Results
                                </td>
                              </xsl:when>
                              <xsl:otherwise>
                                <td>
                                  <img height="1px" width="100px" src="http://www.socialdiligence.com/images/blank.gif" />
                                  <!-- result block -->
                                  <table cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td class="image">
                                          <xsl:choose>
                                            <xsl:when test="count($href_v/img)='0'">
                                              <xsl:text>
                                                 
                                              </xsl:text>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              <div class="imgdiv">
                                                <a class="tip" target="_blank">
                                                  <xsl:attribute name="href">
                                                    <xsl:value-of select="$href_v/url" />
                                                  </xsl:attribute>
                                                  <image>
                                                    <xsl:attribute name="src">
                                                      <xsl:value-of select="$href_v/img/src" />
                                                    </xsl:attribute>
                                                  </image>
                                                </a>
                                              </div>
                                            </xsl:otherwise>
                                          </xsl:choose>

                                        </td>
                                      </tr>
                                      <tr>
                                        <td>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>
                                          <p class="ctr">
                                            <a target="_blank" class="tip">
                                              <xsl:attribute name="href">
                                                <xsl:value-of select="$href_v/url" />
                                              </xsl:attribute>
                                              <xsl:value-of select="$href_v/visible" />
                                            </a>
                                          </p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td class="pr">
                                          <a target="_blank" class="public">
                                            <xsl:attribute name="href">
                                              <xsl:value-of select="$href_v/url" />
                                            </xsl:attribute>
                                            <xsl:text>
                                        Public Records
                                        </xsl:text>
                                          </a>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <!-- end result block -->
                                </td>
                              </xsl:otherwise>
                            </xsl:choose>
                            <!--  For Videos END -->

                            <td>
                              <!--Only For Email TD-->
                            </td>
                            <!--  For Emails  -->
                            <!--  
                            <xsl:variable name="href_e" select="/task/networks/emails/result[position()=($random mod $count_e)+1]"/>
                            <xsl:choose >
                              <xsl:when test="($count_e='0')">
                                <td class="nores">
                                  <img height="1px" width="100px" src="http://www.socialdiligence.com/images/blank.gif" />
                                  <br/>
                                  No Results
                                </td>
                              </xsl:when>
                              <xsl:otherwise>
                                <td>
                                  <img height="1px" width="100px" src="http://www.socialdiligence.com/images/blank.gif" />
                                  
                                  <table cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td class="image">
                                          <xsl:choose>
                                            <xsl:when test="count($href_l/img)='0'">
                                              <xsl:text>
                                                 
                                              </xsl:text>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              <div class="imgdiv">
                                                <a class="tip" target="_blank">
                                                  <xsl:attribute name="href">
                                                    <xsl:value-of select="$href_e/url" />
                                                  </xsl:attribute>
                                                  <image>
                                                    <xsl:attribute name="src">
                                                      <xsl:value-of select="$href_e/img/src" />
                                                    </xsl:attribute>
                                                  </image>
                                                </a>
                                              </div>
                                            </xsl:otherwise>
                                          </xsl:choose>

                                        </td>
                                      </tr>
                                      <tr>
                                        <td>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>
                                          <p class="ctr">
                                            <a target="_blank" class="tip">
                                              <xsl:attribute name="href">
                                                <xsl:value-of select="$href_e/url" />
                                              </xsl:attribute>
                                              <xsl:value-of select="$href_e/visible" />
                                            </a>
                                          </p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td class="pr">
                                          <a target="_blank" class="public">
                                            <xsl:attribute name="href">
                                              <xsl:value-of select="$href_e/url" />
                                            </xsl:attribute>
                                            <xsl:text>
                                        Public Records
                                        </xsl:text>
                                          </a>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  
                                </td>
                              </xsl:otherwise>
                            </xsl:choose>
                            -->

                            <!--  For Emails END -->


                            <!--  For twitter  -->
                            <xsl:variable name="href_t" select="/task/networks/friendster/result[position()=($random mod $count_t)+1]"/>
                            <xsl:choose >
                              <xsl:when test="($count_t='0')">
                                <td class="nores">
                                  <img height="1px" width="100px" src="http://www.socialdiligence.com/images/blank.gif" />
                                  <br/>
                                  No Results
                                </td>
                              </xsl:when>
                              <xsl:otherwise>
                                <td>
                                  <img height="1px" width="100px" src="http://www.socialdiligence.com/images/blank.gif" />
                                  <!-- result block -->
                                  <table cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td class="image">
                                          <xsl:choose>
                                            <xsl:when test="count($href_t/img)='0'">
                                              <xsl:text>
                                                 
                                              </xsl:text>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              <div class="imgdiv">
                                                <a class="tip" target="_blank">
                                                  <xsl:attribute name="href">
                                                    <xsl:value-of select="$href_t/url" />
                                                  </xsl:attribute>
                                                  <image>
                                                    <xsl:attribute name="src">
                                                      <xsl:value-of select="$href_t/img/src" />
                                                    </xsl:attribute>
                                                  </image>
                                                </a>
                                              </div>
                                            </xsl:otherwise>
                                          </xsl:choose>

                                        </td>
                                      </tr>
                                      <tr>
                                        <td>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>
                                          <p class="ctr">
                                            <a target="_blank" class="tip">
                                              <xsl:attribute name="href">
                                                <xsl:value-of select="$href_t/url" />
                                              </xsl:attribute>
                                              <xsl:value-of select="$href_t/visible" />
                                            </a>
                                          </p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td class="pr">
                                          <a target="_blank" class="public">
                                            <xsl:attribute name="href">
                                              <xsl:value-of select="$href_t/url" />
                                            </xsl:attribute>
                                            <xsl:text>
                                        Public Records
                                        </xsl:text>
                                          </a>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <!-- end result block -->
                                </td>
                              </xsl:otherwise>
                            </xsl:choose>
                            <!--  For twitter END -->


                          </tr>
                          <tr>
                            <td colspan="9" class="shade">
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>

                    <div id="source_results" style="clear:both;">
                    </div>




                    <!-- content end -->
                  </td>
                </tr>

                <tr>
                  <td class="content">
                    <table cellspacing="0" cellpadding="0" border="0" class="text-section">
                      <tbody>
                        <tr>
                          <td class="content-main">
                            <input type="hidden" value="" name="finished_0" id="finished_0" />
                            <div style="display: none; text-align: center;" id="loading">
                              <font color="red">Loading page ...</font>
                            </div>
                            <div align="center" id="get_search_results0">

                              <xsl:for-each select="/task/web/result">

                                <table cellspacing="0" cellpadding="0" border="0">
                                  <tbody>
                                    <tr>
                                      <td class="nmbr">
                                        <xsl:value-of select="position()"/>
                                      </td>
                                      <td rowspan="2">
                                        <div>
                                          <div id="img_35" style="float: left;">
                                          </div>
                                          <!--<a class="style7" href="<xsl:value-of select=""/>" target="_blank"></a>-->

                                          <A class="style7">
                                            <xsl:attribute name="href">
                                              <xsl:value-of select="clickurl"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="class">
                                              <xsl:value-of select="style7"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="dispurl"/>
                                          </A>


                                          <br/>
                                        </div>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td class="yesno">
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>

                              </xsl:for-each>

                            </div>
                            <input type="hidden" value="" name="finished_search_results_0" id="finished_search_results_0" />
                          </td>
                          <td class="content-right">
                            <xsl:text>*</xsl:text>
                          </td>
                        </tr>
                      </tbody>
                    </table>

                    <script type="text/javascript">
                      //&lt;!--
                      $(":text:visible:enabled:first").focus();
                      //--&gt;
                    </script>

                  </td>

                </tr>

                <script type="text/javascript">
                  //&lt;!--
                  $(":text:visible:enabled:first").focus();
                  //--&gt;
                </script>
              </tbody>
            </table>
            <!-- content end -->
          </td>
        </tr>
      </tbody>
    </table>

  </xsl:template>

</xsl:stylesheet>
