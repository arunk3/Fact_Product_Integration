﻿<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:a="http://ns.hr-xml.org/2004-08-02">
  <xsl:output method="html" encoding="iso-8859-1" />
  <xsl:template match="/">

    <center>
      <div class="sectionsummary">
        <h3>
          (<xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)" />) Results For CCR2 Live Runner REPORT.<br />
        </h3>
      </div>

      <xsl:variable name="TotalRecords">
        <xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)" />
      </xsl:variable>

      <xsl:if test="$TotalRecords=0">

        <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
          <thead>
            <tr>
              <th class="begin"></th>
              <th colspan="5">
                <span style="font-size:20px;font-weight:bold;">Search Information</span>
              </th>
              <th class="end"></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="begin">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colspan="5">
                <h4>Name:</h4>
                <h4>
                  <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:GivenName"/>
                  <xsl:text xml:space="preserve">  </xsl:text>
                  <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:FamilyName"/>
                </h4>
                <br />
                <h4>DOB:</h4>
                <h4>
                  <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:DemographicDetail/a:DateOfBirth"/>
                </h4>
              </td>
              <td class="end">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </tbody>
          <tfoot>
            <td class="begin"></td>
            <td colspan="5"></td>
            <td class="end"></td>
          </tfoot>
        </table>

      </xsl:if>

      <table id="tblRCX" class="reporttable" border="0" cellpadding="0" cellspacing="0">
        <thead>
          <tr>
            <th class="begin"></th>
            <th colspan="5">
              <span style="font-size:20px;font-weight:bold;">CRIMINAL RESULTS</span>
            </th>
            <th class="end"></th>
          </tr>
        </thead>
        <tbody>

          <xsl:variable name="TextData">
            <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text" />
          </xsl:variable>

          <xsl:variable name="SSNNo">
            xxx-xx-<xsl:value-of select="substring(a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:DemographicDetail/a:GovernmentId,6,4)" />
          </xsl:variable>

          <xsl:if test="not(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text)">
            <tr>
              <td class="begin">
              </td>
              <td colspan="5">
                <div style="width: 90%; margin: 0 auto; text-align: left">
                  <div style="width:588px; text-align:left">
                    <div style="font-size:11px;" id="report_rcx">
                      <pre>
                        <!--<xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text">
                        <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text" disable-output-escaping="yes"/>
                      </xsl:if>-->
                        <xsl:if test="not(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text)">
                          <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus = 'Cancelled'">
                            <p>
                              There was an error running this report.<br /> Please contact our customer support team to quickly resolve this issue.
                            </p>
                          </xsl:if>
                          <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus != 'Cancelled'">
                            <p>*** Clear ***</p>
                          </xsl:if>
                          <p></p>
                        </xsl:if>
                      </pre>
                    </div>
                  </div>
                </div>
              </td>
              <td class="end">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase">

            <xsl:choose>
              <xsl:when test="position()>1">
                <tr style="height:20px;">
                  <td class="begin">
                  </td>
                  <td colspan="5" style="text-align:center;">
                    <span style="font-size:16px;font-weight:bold;color:white;">
                    </span>
                  </td>
                  <td class="end"></td>
                </tr>
              </xsl:when>
            </xsl:choose>

            <tr>
              <td class="begin">
              </td>
              <td colspan="5" style="border:solid 1px black;background-color:black;text-align:left;">
                <span style="font-size:16px;font-weight:bold;color:#fff;">
                  Criminal Record #
                  <xsl:value-of select="(position())" />
                </span>
              </td>
              <td class="end"></td>
            </tr>

            <tr>
              <td class="begin">
              </td>
              <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr>
                    <td width="50%">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                          <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                            <b>Source:</b>
                          </td>
                          <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                            <xsl:value-of select="a:CourtName"/>
                          </td>
                        </tr>

                        <tr>
                          <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                            <b>Full Name:</b>
                          </td>
                          <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                            <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                              <xsl:if test="a:FamilyName/@primary='true'">
                                <xsl:value-of select="a:FamilyName"/>
                                <xsl:if test="(a:GivenName)">
                                  <span>, </span>
                                </xsl:if>
                                <xsl:value-of select="a:GivenName"/>
                                <xsl:if test="(a:MiddleName)">
                                  <span>, </span>
                                </xsl:if>
                                <xsl:value-of select="a:MiddleName"/>
                                <xsl:element name="br"/>
                              </xsl:if>
                            </xsl:for-each>
                          </td>
                        </tr>

                        <xsl:if test="(a:SubjectIdentification/a:PersonName/a:FamilyName/@primary='false')">
                          <tr>
                            <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                              <b>AKA Name:</b>
                            </td>
                            <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                              <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                <xsl:if test="a:FamilyName/@primary='false'">
                                  <xsl:value-of select="a:FamilyName"/>
                                  <xsl:if test="(a:GivenName)">
                                    <span>, </span>
                                  </xsl:if>
                                  <xsl:value-of select="a:GivenName"/>
                                  <xsl:if test="(a:MiddleName)">
                                    <span>, </span>
                                  </xsl:if>
                                  <xsl:value-of select="a:MiddleName"/>
                                  <xsl:element name="br"/>
                                </xsl:if>
                              </xsl:for-each>
                            </td>
                          </tr>
                        </xsl:if>

                        <tr>
                          <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                            <b>DOB:</b>
                          </td>
                          <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                            <xsl:value-of select='substring(a:SubjectIdentification/a:DemographicDetail/a:DateOfBirth,1,10)' />
                          </td>
                        </tr>

                        <!--<xsl:if test="($SSNNo) != ''">
                          <tr>
                            <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                              <b>SSN:</b>
                            </td>
                            <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                              <xsl:value-of select ="$SSNNo"/>
                            </td>
                          </tr>
                        </xsl:if>-->
                      </table>
                    </td>
                    <td width="50%">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <xsl:if test="(a:SubjectIdentification/a:DemographicDetail/a:GovernmentId/@documentType='DRIVERS LICENSE')">
                          <tr>
                            <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                              <b>DL#:</b>
                            </td>
                            <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                              <xsl:value-of select ="a:SubjectIdentification/a:DemographicDetail/a:GovernmentId[@documentType='DRIVERS LICENSE']"/>
                            </td>
                          </tr>
                        </xsl:if>

                        <xsl:if test="(a:SubjectIdentification/a:PostalAddress)">
                          <tr>
                            <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                              <b>Address:</b>
                            </td>
                            <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                              <xsl:for-each select="a:SubjectIdentification/a:PostalAddress">
                                <xsl:if test="(a:DeliveryAddress)">
                                  <xsl:value-of select ="a:DeliveryAddress"/>
                                  <xsl:if test="(a:Municipality)">
                                    <span>, </span>
                                  </xsl:if>
                                </xsl:if>
                                <xsl:if test="(a:Municipality)">
                                  <xsl:value-of select ="a:Municipality"/>
                                  <xsl:if test="(a:Region)">
                                    <span>, </span>
                                  </xsl:if>
                                </xsl:if>
                                <xsl:if test="(a:Region)">
                                  <xsl:value-of select ="a:Region"/>
                                  <xsl:if test="(a:PostalCode)">
                                    <span> - </span>
                                  </xsl:if>
                                </xsl:if>
                                <xsl:if test="(a:PostalCode)">
                                  <xsl:value-of select ="a:PostalCode"/>
                                </xsl:if>
                                <xsl:element name="br" />
                              </xsl:for-each>
                            </td>
                          </tr>
                        </xsl:if>

                        <tr>
                          <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                            <b>Race:</b>
                          </td>
                          <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                            <xsl:value-of select="a:SubjectIdentification/a:DemographicDetail/a:Race"/>
                          </td>
                        </tr>

                        <tr>
                          <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                            <b>Gender:</b>
                          </td>
                          <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                            <xsl:choose>
                              <xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:GenderCode = 1">
                                Male
                              </xsl:when>
                              <xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:GenderCode = 2">
                                Female
                              </xsl:when>
                              <xsl:otherwise>UnKnown</xsl:otherwise>
                            </xsl:choose>
                          </td>
                        </tr>

                        <xsl:if test="(a:AgencyReference[@type='Docket'])">
                          <tr>
                            <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                              <b>Docket Number:</b>
                            </td>

                            <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                              <xsl:value-of select="a:AgencyReference[@type='Docket']"/>
                            </td>
                          </tr>
                        </xsl:if>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
              <td class="end"></td>
            </tr>

            <xsl:for-each select="a:Charge">

              <tr>
                <td class="begin">
                </td>
                <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border:solid 1px black;background-color:red;text-align:center;">
                    <tr>
                      <xsl:choose>
                        <xsl:when test="(position()mod 2)=1">
                          <td width="10%" style="border-right:solid 1px black;background-color:#dcdcdc;text-align:center;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td style="text-align:right;background-color:#dcdcdc;">
                                  <span style="font-size:12px;font-weight:bold;color:#000;">
                                    Charge #
                                    <xsl:value-of select="(position())" />
                                  </span>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </xsl:when>
                        <xsl:otherwise>
                          <td width="10%" style="border-right:solid 1px black;background-color:white;text-align:center;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td style="text-align:right;background-color:white;">
                                  <span style="font-size:12px;font-weight:bold;color:#000;">
                                    Charge #
                                    <xsl:value-of select="(position())" />
                                  </span>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </xsl:otherwise>
                      </xsl:choose>
                      <xsl:choose>
                        <xsl:when test="(position()mod 2)=1">
                          <td width="45%" style="background-color:#dcdcdc;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:30%;">
                                  <b>Charge:</b>
                                </td>
                                <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                  <xsl:value-of select="a:ChargeOrComplaint"/>
                                </td>
                              </tr>
                              <xsl:if test="(a:ChargeDescription)">
                                <tr>
                                  <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                    <b>Penal Code Description:</b>
                                  </td>

                                  <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                    <xsl:value-of select="a:ChargeDescription" />
                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="(a:ChargeDate)">
                                <tr>
                                  <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                    <b>Charge Date:</b>
                                  </td>

                                  <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                    <xsl:value-of select='substring(a:ChargeDate,1,10)' />
                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="(a:ArrestDate)">
                                <tr>
                                  <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                    <b>Arrest Date:</b>
                                  </td>

                                  <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                    <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                  </td>
                                </tr>
                              </xsl:if>

                              <tr>
                                <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                  <b>Offense Date:</b>
                                </td>

                                <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                  <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                </td>
                              </tr>

                              <tr>
                                <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                  <b>
                                    Disposition Date:
                                  </b>
                                </td>

                                <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                  <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                </td>
                              </tr>
                            </table>
                          </td>
                          <td width="45%" style="background-color:#dcdcdc;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">
                                  <b>
                                    Severity:
                                  </b>
                                </td>

                                <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                  <xsl:value-of select="a:ChargeTypeClassification"/>
                                </td>
                              </tr>

                              <tr>
                                <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                  <b>
                                    Final Disposition:
                                  </b>
                                </td>

                                <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                  <xsl:value-of select="a:Disposition"/>
                                </td>
                              </tr>

                              <tr>
                                <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                  <b>
                                    Comments:
                                  </b>
                                </td>

                                <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                  <xsl:for-each select="a:Comment">
                                    <span style="float:left;">
                                      <xsl:value-of select="."/>
                                      <xsl:element name="br" />
                                    </span>
                                  </xsl:for-each>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </xsl:when>
                        <xsl:otherwise>
                          <td width="45%">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:30%;">
                                  <b>Charge:</b>
                                </td>
                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:value-of select="a:ChargeOrComplaint"/>
                                </td>
                              </tr>
                              <xsl:if test="(a:ChargeDescription)">
                                <tr>
                                  <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                    <b>Penal Code Description:</b>
                                  </td>

                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                    <xsl:value-of select="a:ChargeDescription" />
                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="(a:ChargeDate)">
                                <tr>
                                  <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                    <b>Charge Date:</b>
                                  </td>

                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                    <xsl:value-of select='substring(a:ChargeDate,1,10)' />
                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="(a:ArrestDate)">
                                <tr>
                                  <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                    <b>Arrest Date:</b>
                                  </td>

                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                    <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                  </td>
                                </tr>
                              </xsl:if>

                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                  <b>Offense Date:</b>
                                </td>

                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                </td>
                              </tr>

                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                  <b>
                                    Disposition Date:
                                  </b>
                                </td>

                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                </td>
                              </tr>
                            </table>
                          </td>
                          <td width="45%">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                  <b>
                                    Severity:
                                  </b>
                                </td>

                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:value-of select="a:ChargeTypeClassification"/>
                                </td>
                              </tr>

                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                  <b>
                                    Final Disposition:
                                  </b>
                                </td>

                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:value-of select="a:Disposition"/>
                                </td>
                              </tr>

                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                  <b>
                                    Comments:
                                  </b>
                                </td>

                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:for-each select="a:Comment">
                                    <span style="float:left;">
                                      <xsl:value-of select="."/>
                                      <xsl:element name="br" />
                                    </span>
                                  </xsl:for-each>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </xsl:otherwise>
                      </xsl:choose>
                    </tr>
                  </table>
                </td>
                <td class="end"></td>
              </tr>
            </xsl:for-each>

          </xsl:for-each>

          <xsl:for-each select="a:BackgroundReports">
            <tr class="trRawCCR21">
              <td class="begin">
              </td>
              <td onclick="collapse('{concat('trRawCCR2_',@OrderDetailId)}');" colspan="5" style="border:solid 1px black;background-color:white;text-align:left;">
                <div style="float:left">
                  <div style="float:left;background-position:left;" class="plus_btnSmall">
                  </div>
                  <div style="font-size:17px;font-weight:bold;color:#000;width:300px; margin-top:2px">
                    Raw Courthouse Data
                  </div>
                </div>
              </td>
              <td class="end"></td>
            </tr>
            <tr class="trRawCCR22">
              <td class="begin">
              </td>
              <td colspan="5" style="border:solid 1px black;background-color:white;text-align:center;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr id="{concat('trRawCCR2_',@OrderDetailId)}_b" style="display:none;">
                    <td style="background-color:white;">
                      <div id="divRawDataCCR2">
                        <pre>
                          <xsl:value-of select ="$TextData"/>
                        </pre>
                      </div>
                    </td>
                  </tr>
                </table>
              </td>
              <td class="end"></td>
            </tr>
          </xsl:for-each>

        </tbody>
        <tfoot>
          <td class="begin"></td>
          <td colspan="5"></td>
          <td class="end"></td>
        </tfoot>
      </table>
    </center>

  </xsl:template>
</xsl:stylesheet>