﻿<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:a="http://ns.hr-xml.org/2004-08-02">
  <xsl:output method="html" encoding="iso-8859-1" />
  <xsl:template match="/">

    <div class="people-search-wrap">
      <div class="sectionsummary reco-search" style="border: 1px solid #9b0000; margin-bottom: 10px; padding-bottom: 0; padding-left: 10px; text-align: left;">
        <h3>
          (<xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)" />) Results from LiveRunner Search.<br />
        </h3>
      </div>

      <xsl:variable name="TotalRecords">
        <xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)" />
      </xsl:variable>

      <xsl:if test="$TotalRecords=0">

        <table id="peopleTable" class="" border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 10px; background: #f7f7f7 none repeat scroll 0px 0px;">
          <thead>
            <tr>
              <th class="ncr_header_data" style="padding: 0px ! important; width: 4px;"></th>
              <th colspan="5" class="ncr_header_data" style="padding-left: 0px ! important;">
                <span style="font-size:15px;font-weight:bold; color:#666;">Search Information</span>
              </th>
              <th class="ncr_header_data"></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colspan="5">
                <h4>Name:</h4>
                <h4>
                  <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:GivenName"/>
                  <xsl:text xml:space="preserve">  </xsl:text>
                  <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:FamilyName"/>
                </h4>
                <br />
                <h4>DOB:</h4>
                <h4>
                  <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:DemographicDetail/a:DateOfBirth"/>
                </h4>
              </td>
              <td>
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td >
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colspan="5" style="width:100%;">

                <pre>
                  <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus != 'Fulfilled'">
                    <p>*** Clear ***</p>
                  </xsl:if>
                  <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus = 'Fulfilled'">
                    <p>*** INCOMPLETE ***</p>
                  </xsl:if>
                </pre>
              </td>
              <td >
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </tbody>
          <tfoot>
            <td ></td>
            <td colspan="5"></td>
            <td></td>
          </tfoot>
        </table>

      </xsl:if>

      <table id="tblRCX" class="reporttable" border="0" cellpadding="0" cellspacing="0">
        <thead>
          <tr>

            <th colspan="5"  class="fcr_header_data">
              <div>
                <div class="txt-sumry" style="font-size:15px; color:#666;"> Criminal Results </div>
                <div class="txt-detials">
                  <ul>
                    <li> </li>
                    <li> </li>
                  </ul>
                  <ul>
                    <li style="text-align:left;">
                      <strong></strong>
                    </li>
                    <li style="text-align:left;">
                      <strong></strong>
                    </li>
                  </ul>
                </div>
              </div>

            </th>

          </tr>
        </thead>
        <tbody>

          <xsl:variable name="TextData">
            <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text" />
          </xsl:variable>
          <xsl:if test="$TextData = ''">
            <pre>
              <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus != 'Fulfilled'">
                <p>*** Clear ***</p>
              </xsl:if>
              <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus = 'Fulfilled'">
                <p>*** INCOMPLETE ***</p>
              </xsl:if>
            </pre>
          </xsl:if>
          <xsl:variable name="IsLosAngeles">
            <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/@qualifier" />
          </xsl:variable>
          <xsl:variable name="SSNNo">
            xxx-xx-<xsl:value-of select="substring(a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:DemographicDetail/a:GovernmentId,6,4)" />
          </xsl:variable>

          <xsl:if test="not(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text)">
            <tr>

              <td colspan="5">
                <div style="width: 90%; margin: 0 auto; text-align: left">
                  <div style="width:588px; text-align:left">
                    <div style="font-size:11px;" id="report_rcx">
                      <pre>
                        <!--<xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text">
                        <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text" disable-output-escaping="yes"/>
                      </xsl:if>-->
                        <xsl:if test="not(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text)">
                          <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus = 'Cancelled'">
                            <p>
                              There was an error running this report.<br /> Please contact our customer support team to quickly resolve this issue.
                            </p>
                          </xsl:if>
                          <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus = 'Fulfilled'">
                            <p>
                              *** INCOMPLETE ***
                            </p>
                          </xsl:if>
                          <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus != 'Cancelled' and a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus != 'Completed' and a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus != 'Fulfilled'">
                            <p>*** Clear ***</p>
                          </xsl:if>
                          <p></p>
                        </xsl:if>
                      </pre>
                    </div>
                  </div>
                </div>
              </td>

            </tr>
          </xsl:if>

          <xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase">

            <xsl:choose>
              <xsl:when test="position()>1">
                <tr style="height:20px;">

                  <td colspan="5" style="text-align:center;">
                    <span style="font-size:16px;font-weight:bold;color:white;">
                    </span>
                  </td>

                </tr>
              </xsl:when>
            </xsl:choose>

            <tr class="search-heading">

              <td colspan="5" style=" padding:0;">

                <table>
                  <td width="14%" style=" text-align:right;">
                    <span class="mn-hd" style="font-weight:normal;">Criminal Record </span>
                  </td>
                  <td align="left">

                    <span class="red-no" style="padding-top:6px;">
                      <xsl:value-of select="(position())" />
                    </span>

                  </td>

                </table>

              </td>

            </tr>

            <tr>

              <td colspan="5">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr>
                    <td width="10%"></td>
                    <td width="45%">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="source-tb">
                        <tr>
                          <td style="width:30%;">
                            <b>Source:</b>
                          </td>
                          <td>
                            <xsl:value-of select="a:CourtName"/>
                          </td>
                        </tr>

                        <tr>
                          <td>
                            <b>Full Name:</b>
                          </td>
                          <td>
                            <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                              <xsl:if test="a:FamilyName/@primary='true'">
                                <xsl:value-of select="a:FamilyName"/>
                                <xsl:if test="(a:GivenName)">
                                  <span>, </span>
                                </xsl:if>
                                <xsl:value-of select="a:GivenName"/>
                                <xsl:if test="(a:MiddleName)">
                                  <span>, </span>
                                </xsl:if>
                                <xsl:value-of select="a:MiddleName"/>
                                <xsl:element name="br"/>
                              </xsl:if>
                            </xsl:for-each>
                          </td>
                        </tr>

                        <xsl:if test="(a:SubjectIdentification/a:PersonName/a:FamilyName/@primary='false')">
                          <tr>
                            <td>
                              <b>AKA Name:</b>
                            </td>
                            <td>
                              <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                <xsl:if test="a:FamilyName/@primary='false'">
                                  <xsl:value-of select="a:FamilyName"/>
                                  <xsl:if test="(a:GivenName)">
                                    <span>, </span>
                                  </xsl:if>
                                  <xsl:value-of select="a:GivenName"/>
                                  <xsl:if test="(a:MiddleName)">
                                    <span>, </span>
                                  </xsl:if>
                                  <xsl:value-of select="a:MiddleName"/>
                                  <xsl:element name="br"/>
                                </xsl:if>
                              </xsl:for-each>
                            </td>
                          </tr>
                        </xsl:if>

                        <tr>
                          <td>
                            <b>DOB:</b>
                          </td>
                          <td>
                            <xsl:value-of select='substring(a:SubjectIdentification/a:DemographicDetail/a:DateOfBirth,1,10)' />
                          </td>
                        </tr>

                        <!--<xsl:if test="($SSNNo) != ''">
                          <tr>
                            <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                              <b>SSN:</b>
                            </td>
                            <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                              <xsl:value-of select ="$SSNNo"/>
                            </td>
                          </tr>
                        </xsl:if>-->
                      </table>
                    </td>
                    <td width="45%">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="source-tb">
                        <xsl:if test="(a:SubjectIdentification/a:DemographicDetail/a:GovernmentId/@documentType='DRIVERS LICENSE')">
                          <tr>
                            <td style="width:30%;">
                              <b>DL#:</b>
                            </td>
                            <td>
                              <xsl:value-of select ="a:SubjectIdentification/a:DemographicDetail/a:GovernmentId[@documentType='DRIVERS LICENSE']"/>
                            </td>
                          </tr>
                        </xsl:if>

                        <xsl:if test="(a:SubjectIdentification/a:PostalAddress)">
                          <tr>
                            <td>
                              <b>Address:</b>
                            </td>
                            <td>
                              <xsl:for-each select="a:SubjectIdentification/a:PostalAddress">
                                <xsl:if test="(a:DeliveryAddress)">
                                  <xsl:value-of select ="a:DeliveryAddress"/>
                                  <xsl:if test="(a:Municipality)">
                                    <span>, </span>
                                  </xsl:if>
                                </xsl:if>
                                <xsl:if test="(a:Municipality)">
                                  <xsl:value-of select ="a:Municipality"/>
                                  <xsl:if test="(a:Region)">
                                    <span>, </span>
                                  </xsl:if>
                                </xsl:if>
                                <xsl:if test="(a:Region)">
                                  <xsl:value-of select ="a:Region"/>
                                  <xsl:if test="(a:PostalCode)">
                                    <span> - </span>
                                  </xsl:if>
                                </xsl:if>
                                <xsl:if test="(a:PostalCode)">
                                  <xsl:value-of select ="a:PostalCode"/>
                                </xsl:if>
                                <xsl:element name="br" />
                              </xsl:for-each>
                            </td>
                          </tr>
                        </xsl:if>

                        <tr>
                          <td style="width:30%;">
                            <b>Race:</b>
                          </td>
                          <td>
                            <xsl:value-of select="a:SubjectIdentification/a:DemographicDetail/a:Race"/>
                          </td>
                        </tr>

                        <tr>
                          <td>
                            <b>Gender:</b>
                          </td>
                          <td>
                            <xsl:choose>
                              <xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:GenderCode = 1">
                                Male
                              </xsl:when>
                              <xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:GenderCode = 2">
                                Female
                              </xsl:when>
                              <xsl:otherwise>UnKnown</xsl:otherwise>
                            </xsl:choose>
                          </td>
                        </tr>

                        <xsl:if test="(a:AgencyReference[@type='Docket'])">
                          <tr>
                            <td>
                              <b>Docket Number:</b>
                            </td>

                            <td>
                              <xsl:value-of select="a:AgencyReference[@type='Docket']"/>
                            </td>
                          </tr>
                        </xsl:if>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>

            </tr>

            <xsl:for-each select="a:Charge">

              <tr class="sparater-td">

                <td colspan="5">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                    <tr>
                      <xsl:choose>
                        <xsl:when test="(position()mod 2)=1">
                          <td width="10%">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td style="text-align:right;">
                                  <span>
                                    <span class="blue-no" style="padding-top:6px;">
                                      <xsl:value-of select="(position())" />
                                    </span>

                                  </span>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </xsl:when>
                        <xsl:otherwise>
                          <td width="10%">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td >
                                  <span>
                                    Charge #
                                    <xsl:value-of select="(position())" />
                                  </span>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </xsl:otherwise>
                      </xsl:choose>
                      <xsl:choose>
                        <xsl:when test="(position()mod 2)=1">
                          <td width="45%">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td style="width:30%;">
                                  <b>Charge:</b>
                                </td>
                                <td>
                                  <xsl:value-of select="a:ChargeOrComplaint"/>
                                </td>
                              </tr>
                              <xsl:if test="(a:ChargeDescription)">
                                <tr>
                                  <td>
                                    <xsl:if test="$IsLosAngeles = 'LOS ANGELES'">
                                      <b>Penal Code Category:</b>
                                    </xsl:if>
                                    <xsl:if test="$IsLosAngeles != 'LOS ANGELES'">
                                      <b>Penal Code Description:</b>
                                    </xsl:if>
                                  </td>

                                  <td>
                                    <xsl:value-of select="a:ChargeDescription" />
                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="(a:ChargeDate)">
                                <tr>
                                  <td>
                                    <b>Charge Date:</b>
                                  </td>

                                  <td>
                                    <xsl:value-of select='substring(a:ChargeDate,1,10)' />
                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="(a:ArrestDate)">
                                <tr>
                                  <td>
                                    <b>Arrest Date:</b>
                                  </td>

                                  <td>
                                    <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                  </td>
                                </tr>
                              </xsl:if>

                              <tr>
                                <td>
                                  <b>Offense Date:</b>
                                </td>

                                <td>
                                  <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                </td>
                              </tr>

                              <tr>
                                <td>
                                  <b>
                                    Disposition Date:
                                  </b>
                                </td>

                                <td>
                                  <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                </td>
                              </tr>
                            </table>
                          </td>
                          <td width="45%" >
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td style="width:30%;">
                                  <xsl:if test="$IsLosAngeles = 'LOS ANGELES'">
                                    <b>Initial Severity Level:</b>
                                  </xsl:if>
                                  <xsl:if test="$IsLosAngeles != 'LOS ANGELES'">
                                    <b>Statue Severity:</b>
                                  </xsl:if>
                                </td>

                                <td>
                                  <xsl:value-of select="a:ChargeTypeClassification"/>
                                </td>
                              </tr>

                              <tr>
                                <td>
                                  <b>
                                    Final Disposition:
                                  </b>
                                </td>

                                <td>
                                  <xsl:value-of select="a:Disposition"/>
                                </td>
                              </tr>

                              <tr>
                                <td>
                                  <b>
                                    Comments:
                                  </b>
                                </td>

                                <td>
                                  <xsl:for-each select="a:Comment">
                                    <span style="float:left;">
                                      <xsl:value-of select="."/>
                                      <xsl:element name="br" />
                                    </span>
                                  </xsl:for-each>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </xsl:when>
                        <xsl:otherwise>
                          <td width="45%">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td>
                                  <b>Charge:</b>
                                </td>
                                <td >
                                  <xsl:value-of select="a:ChargeOrComplaint"/>
                                </td>
                              </tr>
                              <xsl:if test="(a:ChargeDescription)">
                                <tr>
                                  <td>
                                    <xsl:if test="$IsLosAngeles = 'LOS ANGELES'">
                                      <b>Penal Code Category:</b>
                                    </xsl:if>
                                    <xsl:if test="$IsLosAngeles != 'LOS ANGELES'">
                                      <b>Penal Code Description:</b>
                                    </xsl:if>
                                  </td>

                                  <td>
                                    <xsl:value-of select="a:ChargeDescription" />
                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="(a:ChargeDate)">
                                <tr>
                                  <td>
                                    <b>Charge Date:</b>
                                  </td>

                                  <td>
                                    <xsl:value-of select='substring(a:ChargeDate,1,10)' />
                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="(a:ArrestDate)">
                                <tr>
                                  <td>
                                    <b>Arrest Date:</b>
                                  </td>

                                  <td>
                                    <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                  </td>
                                </tr>
                              </xsl:if>

                              <tr>
                                <td>
                                  <b>Offense Date:</b>
                                </td>

                                <td>
                                  <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                </td>
                              </tr>

                              <tr>
                                <td >
                                  <b>
                                    Disposition Date:
                                  </b>
                                </td>

                                <td>
                                  <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                </td>
                              </tr>
                            </table>
                          </td>
                          <td width="45%">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td>
                                  <xsl:if test="$IsLosAngeles = 'LOS ANGELES'">
                                    <b>Initial Severity Level:</b>
                                  </xsl:if>
                                  <xsl:if test="$IsLosAngeles != 'LOS ANGELES'">
                                    <b>Statue Severity::</b>
                                  </xsl:if>
                                </td>

                                <td>
                                  <xsl:value-of select="a:ChargeTypeClassification"/>
                                </td>
                              </tr>

                              <tr>
                                <td>
                                  <b>
                                    Final Disposition:
                                  </b>
                                </td>

                                <td>
                                  <xsl:value-of select="a:Disposition"/>
                                </td>
                              </tr>

                              <tr>
                                <td>
                                  <b>
                                    Comments:
                                  </b>
                                </td>

                                <td>
                                  <xsl:for-each select="a:Comment">
                                    <span style="float:left;">
                                      <xsl:value-of select="."/>
                                      <xsl:element name="br" />
                                    </span>
                                  </xsl:for-each>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </xsl:otherwise>
                      </xsl:choose>
                    </tr>
                  </table>
                </td>

              </tr>
            </xsl:for-each>

          </xsl:for-each>

          <xsl:for-each select="a:BackgroundReports">
            <tr class="trRawCCR11">

              <td onclick="collapse('{concat('trRawCCR1_',@OrderDetailId)}');" colspan="5" style="border:solid 1px black;background-color:white;text-align:left;">
                <div style="float:left">
                  <div style="float:left;background-position:left;" class="plus_btnSmall">
                  </div>
                  <div style="font-size:17px;font-weight:bold;color:#000;width:300px; margin-top:2px">
                    Raw Courthouse Data
                  </div>
                </div>
              </td>

            </tr>
            <tr class="trRawCCR12">

              <td colspan="5" style="border:solid 1px black;background-color:white;text-align:center;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr id="{concat('trRawCCR1_',@OrderDetailId)}_b" style="display:none;">
                    <td style="background-color:white;">
                      <div id="divRawDataCCR1">
                        <pre>
                          <xsl:value-of select ="$TextData"/>
                        </pre>
                      </div>
                    </td>
                  </tr>
                </table>
              </td>

            </tr>
          </xsl:for-each>

        </tbody>

      </table>

    </div>
  </xsl:template>
</xsl:stylesheet>