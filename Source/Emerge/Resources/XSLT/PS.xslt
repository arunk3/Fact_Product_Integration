<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:a="http://ns.hr-xml.org/2004-08-02">
  <xsl:param name="admin" select="defaultstring" />
  <xsl:output method="html"/>
  <xsl:template match="/">

    <xsl:if test="count(ieiresponse/addressinformation/message)&gt;'0'">
      <div class="people-search-wrap" style="padding-top:0" >
        <table class="people-search1" width="100%" style="margin-bottom:10px;">
          <tr>
            <td style="width:14px;"></td>

            <td>
              <div class="txt-detials" style="padding-left: 32px;float:left;">
                <ul>
                  <li> SSN ISSUANCE :</li>
                  <li> DEATH INDEX :</li>
                </ul>
                <ul>
                  <li  style="text-align:left;">
                    <strong>
                      <xsl:value-of select="ieiresponse/addressinformation/message"/>

                    </strong>
                  </li>
                  <li style="text-align:left;">
                    <strong>
                      <xsl:value-of select="ieiresponse/addressinformation/year"/>
                    </strong>
                  </li>
                </ul>
              </div>

            </td>
          </tr>
        </table>
        <table class="people-search" width="100%">
          <tr class="search-heading">

            <td  class="width_50px" style="text-align:right;">
              <span class="countr" style="background-color: rgb(0,120,189);color: white; border:none;">
                <span class="nameTotalCount">
                  <xsl:variable name="unique-listName" select="ieiresponse/addressinformation/summaryinformation/uniquenames/fullname[not(.=following::fullname)]" />
                  <xsl:value-of select="count($unique-listName)"/>
                </span>
              </span>
            </td>
            <td>
              <span class="PS_SSN_DEATH_TOP">
                REPORTED NAMES

              </span>
            </td>
            <td style="text-align:right;">
              <span class="countr" style="background-color: rgb(0,120,189);color: white; border:none">
                <xsl:variable name="unique-listDates" select="ieiresponse/addressinformation/records/record/fulldob[not(.=following::fulldob or string-length(.) != '10' )]" />
                <xsl:value-of select="count($unique-listDates)"/>
              </span>
            </td>
            <td>
              <span class="PS_SSN_DEATH_TOP">
                REPORTED DATE OF BIRTH U
              </span>
            </td>
            <td style="text-align:right;">
              <span class="countr" style="background-color: rgb(0,120,189);color: white; border:none">
                <xsl:variable name="unique-ListCounty" select="ieiresponse/addressinformation/summaryinformation/uniquecounties/county[not(.=following::county)]" />

                <xsl:value-of select="count($unique-ListCounty)"/>
              </span>
            </td>
            <td>
              <span class="PS_SSN_DEATH_TOP">
                REPORTED COUNTIES
                <xsl:variable name="unique-ListCounty" select="ieiresponse/addressinformation/records/record/addresses/address/county[not(.=following::county)]" />

                (<xsl:value-of select="count($unique-ListCounty)"/>)
                <xsl:if test="count($unique-ListCounty) &gt;'0'">
                  <a  class="runallcounties" style="color:red;margin-left:1%;cursor:pointer;" >Run All Counties</a>
                  &#160;<img src="../../Content/themes/base/images/ajax-loader3.gif" class="psdisplay"></img>
                </xsl:if>
              </span >
            </td>
          </tr>



          <tr class="sparater-td bg-gray">

            <td></td>
            <td>



              <xsl:variable name="unique-listName" select="ieiresponse/addressinformation/summaryinformation/uniquenames/fullname[not(.=following::fullname)]" />
              <xsl:for-each select="$unique-listName">
                <span class="up_unique">
                  <span class="unique">
                    <xsl:value-of select="." />
                  </span>

                </span>

              </xsl:for-each>


            </td>
            <td></td>
            <td>
              <xsl:variable name="unique-listDates" select="ieiresponse/addressinformation/records/record/fulldob[not(.=following::fulldob or string-length(.) != '10' )]" />

              <br/>
              <xsl:for-each select="$unique-listDates">
                <span class="ultimatecls">
                  <span class="ultimateclshelp">
                    <xsl:value-of select="." />
                  </span>
                </span>
                <br/>
              </xsl:for-each>


            </td>
            <td></td>
            <td>



              <xsl:variable name="unique-list" select="ieiresponse/addressinformation/records/record/addresses/address" />
              <xsl:for-each select="$unique-list">
                <xsl:if test="county[not(.=following::county)]">
                  <span class="countystate">
                    <xsl:value-of select="county"/>
                    <xsl:text>, </xsl:text>
                    <xsl:value-of select="state"/>
                  </span>
                  <br/>
                </xsl:if>
              </xsl:for-each>
            </td>
          </tr>
        </table>
        <div class="sectionsummary" style=" margin:10px 0;">
          <h3>
            (<xsl:value-of select="count(ieiresponse/addressinformation/records/record)"/>) Results PEOPLE SEARCH
          </h3>
          <br />
          <h3>Name:</h3>
          <h4>
            <span class="capname" style="font-size:14px;">
              <xsl:value-of select="concat(ieiresponse/addressinformation/records/record/firstname,', ',ieiresponse/addressinformation/records/record/lastname)" />
            </span>
          </h4>
          <br/>
        </div>

        <table class="people-search" width="100%">

          <xsl:if test="ieiresponse/addressinformation/records/@count != '0'">


            <tr>
              <td class="width_50px"></td>
              <th style="width:26%;">
                <span class="PS_SSN_DEATH_TOP">
                  <u>PERSONAL INFO</u>
                </span>
              </th>

              <th style="width:29.5%;">
                <span class="PS_SSN_DEATH_TOP">
                  <u>ADDRESS INFO</u>
                </span>
              </th>

              <th >
                <span class="PS_SSN_DEATH_TOP">
                  <u>PHONE INFO</u>
                </span>
              </th>

            </tr>

            <xsl:for-each select="ieiresponse/addressinformation/records/record">

              <xsl:choose>
                <xsl:when test="(position() mod 2 = 0)">
                  <tr class="sparater-td">
                    <td style="text-align:center; vertical-align: top;">
                      <span class="blue-no">
                        <span>
                          <xsl:value-of select="position()" />
                        </span>
                      </span>
                    </td>
                    <td style="background-color:#fff;">
                      <xsl:value-of select="lastname" />, <xsl:value-of select="PersonName/firstname" /><br/>
                      SSN : xxx-xx-<xsl:value-of select="substring(ssn,6,4)" />

                      <br/>
                      <span class="ultimatecls">
                        <!--string-length(.) != '10'-->
                        <!--BackgroundReports/BackgroundReportPackage/Screenings/Screening/SSNReport/CreditFile/PersonalData/DemographicDetail/DateOfBirth[not(.=following::DateOfBirth or string-length(.) != '10' )]-->
                        <xsl:if test="fulldob[not(string-length(fulldob) != '10' )]">
                          DOB : <span class="ultimateclshelp">
                            <xsl:value-of select="fulldob" />
                            <!--<xsl:value-of select="concat(substring(fulldob,6,2),'/',substring(fulldob,9,2),'/',substring(fulldob,1,4))" />-->
                          </span>
                          <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
                          Age : <xsl:value-of select="age" />
                        </xsl:if>
                      </span>
                    </td>

                    <td style="background-color:#fff;">
                      <xsl:value-of select="addresses/address/fullstreet" />
                      <xsl:text> </xsl:text>
                      <br />

                      <xsl:value-of select="addresses/address/city" />
                      <xsl:text>, </xsl:text>
                      <xsl:value-of select="addresses/address/state" />
                      <xsl:text> </xsl:text>
                      <br />
                      <xsl:value-of select="addresses/address/county" />
                      <xsl:text> </xsl:text>
                      <br />

                    
                      <xsl:value-of select="addresses/address/zip" />
                      <br/>
                      <xsl:if test="count(addresses/address/from-date)!='0'">

                        <xsl:value-of select="addresses/address/from-date/@year" />-
                        <xsl:value-of select="addresses/address/from-date/@month" />-
                        <xsl:value-of select="addresses/address/from-date/@day" />
                        <xsl:text disable-output-escaping="yes"> To </xsl:text>
                        <xsl:if test="count(addresses/address/to-date)!='0'">
                          <xsl:value-of select="addresses/address/to-date/@year" />-
                          <xsl:value-of select="addresses/address/to-date/@month" />-
                          <xsl:value-of select="addresses/address/to-date/@day" />
                        </xsl:if>
                      </xsl:if>
                    </td>

                    <td style="border-right: none;background-color:#fff;">
                      <xsl:choose>
                        <xsl:when test="count(phonenumbers)='0'">

                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:for-each select="phonenumbers">
                            <xsl:value-of select="phonenumber/fullnumber"/>
                            <br/>
                          </xsl:for-each>
                        </xsl:otherwise>
                      </xsl:choose>
                    </td>

                    <!--</tr>-->
                  </tr>
                </xsl:when>
                <xsl:otherwise>
                  <tr class="sparater-td">
                    <td style="text-align:center;vertical-align: top;">
                      <span class="blue-no"  >
                        <span>
                          <xsl:value-of select="position()" />
                        </span>
                      </span>
                    </td>
                    <td >
                      <xsl:value-of select="lastname" />, <xsl:value-of select="firstname" /><br/>
                      SSN : xxx-xx-<xsl:value-of select="substring(ssn,6,4)" />
                      <br/>
                      <span class="ultimatecls">
                        <xsl:if test="fulldob[not(string-length(fulldob) != '10' )]">
                          DOB : <span class="ultimateclshelp">
                            <xsl:value-of select="fulldob" />
                            <!--<xsl:value-of select="concat(substring(fulldob,6,2),'/',substring(fulldob,9,2),'/',substring(fulldob,1,4))" />-->
                          </span>
                          <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
                          Age : <xsl:value-of select="age" />
                        </xsl:if>
                      </span>
                    </td>

                    <td>
                      <xsl:value-of select="addresses/address/fullstreet" />
                      <xsl:text> </xsl:text>
                      <br />

                      <xsl:value-of select="addresses/address/city" />
                      <xsl:text>, </xsl:text>
                      <xsl:value-of select="addresses/address/state" />
                      <xsl:text> </xsl:text>
                      <br />
                      <xsl:value-of select="addresses/address/county" />
                      <xsl:text> </xsl:text>
                      <br />


                      <xsl:text> </xsl:text>
                      <xsl:value-of select="addresses/address/zip" />
                      <br/>

                      <xsl:if test="count(addresses/address/from-date)!='0'">

                        <xsl:value-of select="addresses/address/from-date/@year" />-
                        <xsl:value-of select="addresses/address/from-date/@month" />-
                        <xsl:value-of select="addresses/address/from-date/@day" />
                        <xsl:text disable-output-escaping="yes"> To </xsl:text>
                        <xsl:if test="count(addresses/address/to-date)!='0'">
                          <xsl:value-of select="addresses/address/to-date/@year" />-
                          <xsl:value-of select="addresses/address/to-date/@month" />-
                          <xsl:value-of select="addresses/address/to-date/@day" />
                        </xsl:if>
                      </xsl:if>
                    </td>

                    <td style="border-right: none;">
                      <xsl:choose>
                        <xsl:when test="count(phonenumbers)='0'">

                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:for-each select="phonenumbers">
                            <xsl:value-of select="phonenumber/fullnumber"/>
                            <br/>
                          </xsl:for-each>
                        </xsl:otherwise>
                      </xsl:choose>
                    </td>

                  </tr>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>

          </xsl:if>
        </table>
      </div>
    </xsl:if>


    <xsl:if test="count(ieiresponse/addressinformation/records) ='0'">
      <table cellpadding="2" cellspacing="0" style="margin: 0px border:none;" align="center" class="PS_main">
        <thead>
          <tr>
            <th class="begin"></th>
            <th colspan="5">Search Information</th>
            <th class="end"></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="begin"></td>
            <td colspan="5">
              <xsl:if test="count(ieiresponse/requestinformation/AdditionalItems/Text) &gt;'0'">
                <xsl:value-of select="ieiresponse/requestinformation/AdditionalItems/Text" />
                <!--Address trace service error: Input Error:Too Many Hits.Please refine your search.-->
              </xsl:if>
              <xsl:if test="count(ieiresponse/requestinformation/AdditionalItems/Text) ='0'">
                No Records Found.
              </xsl:if>
            </td>
            <td class="end">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
        </tbody>
        <tfoot>
          <td class="begin"></td>
          <td colspan="5"></td>
          <td class="end"></td>
        </tfoot>
      </table>
    </xsl:if>


  </xsl:template>
</xsl:stylesheet>