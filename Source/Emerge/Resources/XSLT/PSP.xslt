﻿<?xml version="1.0" encoding="utf-8"?>
<!--<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ns="http://tempuri.org/" xmlns:a="http://schemas.datacontract.org/2004/07/Dot.Psp.Common" 
                xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns:msxsl="urn:schemas-microsoft-com:xslt" 
                xmlns:js="urn:custom-javascript" version="1.0" exclude-result-prefixes="msxsl js">-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:a="http://ns.hr-xml.org/2004-08-02">
  <xsl:output method="html" encoding="iso-8859-1" />
  <xsl:template match="/">
    <!--<html>
      --><!--<link href="https://psp-stage.cdc.nicusa.com/psp/xsl/dir_styles.css" rel="stylesheet" type="text/css"/>--><!--
      <body>-->
        <table id="PSPTable" cellspacing="0" cellpadding="0" border="1">
          <tr>
            <td class="nested">
              <h1>PSP Detailed Report</h1>
              <h2>Federal Motor Carrier Safety Administration</h2>
            </td>
          </tr>
          <tr>
            <td class="error">
              <xsl:if test="(DriverReportResponseType/driverInformationResponse/Status ='Error') or (DriverReportResponseType/driverInformationResponse/Status ='Failure' and DriverReportResponseType/driverInformationResponse/StatusDetail!='1') ">
                No crash or inspection results found.
              </xsl:if>
            </td>
          </tr>
          <tr class="tblMainHeading">
            <td class="block center" style ="background: none repeat scroll 0 0 #ADD8E6">
              <h3>Driver Information</h3>
            </td>
          </tr>
          <tr>
            <td class="block">
              <table border="1" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                  <td class="title">Last Name</td>
                  <td class="title">First Name</td>
                  <td class="title">License #</td>
                  <td class="title">State</td>
                </tr>
                <xsl:for-each select="DriverReportResponseType/driverQueries/DriverQuery">
                  <tr class="rowClass">
                    <td>
                      <xsl:value-of select="DlLastName"/>
                    </td>
                    <td>
                      <xsl:value-of select="DlFirstName"/>
                    </td>
                    <td>
                      <xsl:value-of select="DlNum"/>
                    </td>
                    <td>
                      <xsl:value-of select="DlState"/>
                    </td>
                  </tr>
                </xsl:for-each>
              </table>
            </td>
          </tr>
          <xsl:choose>
          <xsl:when test="(DriverReportResponseType/driverInformationResponse/Status ='Success') or (DriverReportResponseType/driverInformationResponse/Status ='Partial')">
          <tr class="tblMainHeading">
            <td class="block">
              <h3>Crash Activity</h3>
            </td>
          </tr>
          <tr class="subHeadingCls">
            <td class="block">
              <h4>
                Crash Summary (Crashes listed represent a driver's involvement in FMCSA-reportable crashes, without any determination as to responsibility.)
              </h4>
            </td>
          </tr>
          <tr>
            <td class="block">
              <table border="1" cellspacing="0" cellpadding="0" width="100%" id="crashSummary">
                <!--<xsl:for-each select="DriverReportResponseType/driverReportSummaryResponse">-->
                  <tr>
                    <th class="label" rowspan="2"># of Crashes:</th>
                    <td>
                      <xsl:value-of select="DriverReportResponseType/driverReportSummaryResponse/NumCrashes"/>
                    </td>
                    <th class="label" ># of Crashes with Fatalities:</th>
                    <td>
                      <xsl:value-of select="DriverReportResponseType/driverReportSummaryResponse/NumCrashesWithFatalities"/>
                    </td>
                    <th class="label" ># of Crashes with Injuries:</th>
                    <td>
                      <xsl:value-of select="DriverReportResponseType/driverReportSummaryResponse/NumCrashesWithInjuries"/>
                    </td>
                    <th class="label" ># of Towaways:</th>
                    <td>
                      <xsl:value-of select="DriverReportResponseType/driverReportSummaryResponse/NumTowaways"/>
                    </td>
                  </tr>
                  <tr>
                    <th class="label"  colspan="2"># of Fatalities:</th>
                    <td>
                      <xsl:value-of select="DriverReportResponseType/driverReportSummaryResponse/NumFatailities"/>
                    </td>
                    <th class="label" ># Injuries:</th>
                    <td>
                      <xsl:value-of select="DriverReportResponseType/driverReportSummaryResponse/NumInjuries"/>
                    </td>
                    <th  class="label" ># of Hazmat Releases:</th>
                    <td>
                      <xsl:value-of select="DriverReportResponseType/driverReportSummaryResponse/NumHazmatReleases"/>
                    </td>
                  </tr>
                <!--</xsl:for-each>-->
              </table>
            </td>
          </tr>
          <tr class="subHeadingCls">
            <td class="block">
              <h4>
                Crash Details (Crashes listed represent a driver's involvement in FMCSA-reportable crashes, without any determination as to responsibility.)
              </h4>
            </td>
          </tr>
          <xsl:if test="DriverReportResponseType/driverReportSummaryResponse/NumCrashes = '0'">
          <tr class="rowClass">
            <td>
              No Crash Record Found
            </td>
          </tr>
        </xsl:if>
          <xsl:if test="DriverReportResponseType/driverReportSummaryResponse/NumCrashes > '0'">
          <tr>
            <td class="block">
              <table  border="1" cellspacing="0" cellpadding="0" width="100%" id="crashDetails">
                <tr class="big">
                  <th class="title"></th>
                  <th class="title">Date</th>
                  <th class="title">DOT #</th>
                  <th class="title">Carrier Name</th>
                  <th class="title">Driver Name</th>
                  <th class="title">Driver Lic</th>
                  <th class="title">State</th>
                  <th class="title">Driver DOB</th>
                  <th class="title">Rpt St</th>
                  <th class="title">Report Number</th>
                  <th class="title">Location</th>
                  <th class="title"># Fatalities</th>
                  <th class="title"># Injuries</th>
                </tr>
                <xsl:for-each select="DriverReportResponseType/driverInformationResponse/DriverRecord/CrashRecords/CrashRecord">
                  <tr class="rowClass">
                    <td class="label">
                      <xsl:value-of select="position()"/>
                    </td>
                    <td>
                      <xsl:call-template name="FormatDate">
                        <xsl:with-param name="DateTime" select="ReportDate"/>
                      </xsl:call-template>
                    </td>
                    <td>
                      <xsl:value-of select="UploadDOTNumber"/>
                    </td>
                    <td>
                      <xsl:value-of select="CarrierName"/>
                    </td>
                    <td>
                      <xsl:value-of select="concat(DriverLastName, ', ', DriverFirstName)"/>
                    </td>
                    <td>
                      <xsl:value-of select="DriverLicenseNumber"/>
                    </td>
                    <td>
                      <xsl:value-of select="DriverLicenseState"/>
                    </td>
                    <td>
                      <xsl:call-template name="FormatDate">
                        <xsl:with-param name="DateTime" select="DriverDOB"/>
                      </xsl:call-template>
                    </td>
                    <td>
                      <xsl:value-of select="ReportState"/>
                    </td>
                    <td>
                      <xsl:value-of select="ReportNumber"/>
                    </td>
                    <td>
                      <xsl:value-of select="Location"/>
                    </td>
                    <td>
                      <xsl:value-of select="Fatalities"/>
                    </td>
                    <td>
                      <xsl:value-of select="Injuries"/>
                    </td>
                  </tr>
                </xsl:for-each>
              </table>
            </td>
          </tr>
        </xsl:if>
          <tr class="tblMainHeading">
            <td class="block">
              <h3>Inspection Activity</h3>
            </td>
          </tr>
          <tr class="subHeadingCls">
            <td class="block">
              <h4>Inspection Summary</h4>
            </td>
          </tr>
          <tr>
            <td class="block">
              <table border="1" cellspacing="0" cellpadding="0" width="100%" id="inspectionActivity">
                <tr>
                  <th  class="title" colspan="2">Driver Summary</th>
                  <th  class="title" colspan="2">Vehicle Summary</th>
                  <th  class="title" colspan="2">Hazmat Summary</th>
                </tr>
                <!--<xsl:for-each select="//a:driverReportSummaryResponse">-->
                  <tr>
                    <th class="title">Driver Inspections:</th>
                    <td>
                      <xsl:value-of select="DriverReportResponseType/driverReportSummaryResponse/NumInspections"/>
                    </td>
                    <th class="title">Vehicle Inspections:</th>
                    <td>
                      <xsl:value-of select="DriverReportResponseType/driverReportSummaryResponse/NumVehicleInspections"/>
                    </td>
                    <th class="title">Hazmat Inspections:</th>
                    <td>
                      <xsl:value-of select="DriverReportResponseType/driverReportSummaryResponse/NumHazmatInspections"/>
                    </td>
                  </tr>
                  <tr>
                    <th class="title">Driver Out-of-service Inspections:</th>
                    <td>
                      <xsl:value-of select="DriverReportResponseType/driverReportSummaryResponse/NumOutOfServiceInspections"/>
                    </td>
                    <th class="title">Vehicle Out-of-service Inspections:</th>
                    <td>
                      <xsl:value-of select="DriverReportResponseType/driverReportSummaryResponse/NumVehicleOutOfServiceInspections"/>
                    </td>
                    <th class="title">Hazmat Out-of-service Inspections:</th>
                    <td>
                      <xsl:value-of select="DriverReportResponseType/driverReportSummaryResponse/NumHazmatOutOfServiceInspections"/>
                    </td>
                  </tr>
                  <tr>
                    <th class="title">Driver Out-of-service Rate:</th>
                    <td>
                      <xsl:value-of select="DriverReportResponseType/driverReportSummaryResponse/DriverOutOfServiceRate"/>%
                    </td>
                    <th class="title">Vehicle Out-of-service Rate:</th>
                    <td>
                      <xsl:value-of select="DriverReportResponseType/driverReportSummaryResponse/DriverVehicleOutOfServiceRate"/>%
                    </td>
                    <th class="title">Hazmat Out-of-service Rate:</th>
                    <td>
                      <xsl:value-of select="DriverReportResponseType/driverReportSummaryResponse/DriverHazmatOutOfServiceRate"/>%
                    </td>
                  </tr>
                <!--</xsl:for-each>-->
              </table>
            </td>
          </tr>
          <tr class="subHeadingCls">
            <td class="block">
              <h4>Inspection Details</h4>
            </td>
          </tr>
          <xsl:if test="DriverReportResponseType/driverReportSummaryResponse/NumInspections = '0'">
              <tr  class="rowClass">
                <td>
                  No Inspection Record found.
                </td>
              </tr>
              </xsl:if>
          <xsl:if test="DriverReportResponseType/driverReportSummaryResponse/NumInspections > '0'">
          <tr>
            <td class="block">
              <table border="1" cellspacing="0" cellpadding="0" width="100%" id="inspectionDetails">
                <tr class="distinct">
                  <th class="subHeadingCls" colspan="4">Carrier Info</th>
                  <th class="subHeadingCls" colspan="4">Driver Info</th>
                  <th class="subHeadingCls"  colspan="5">Inspection Info</th>
                </tr>
                <tr class="big">
                  <th class="title"></th>
                  <th class="title">Date</th>
                  <th class="title">DOT #</th>
                  <th class="title">Carrier Name</th>
                  <th class="title">Driver Name</th>
                  <th class="title">Driver Lic</th>
                  <th class="title">State</th>
                  <th class="title">Driver DOB</th>
                  <th class="title">Rpt St</th>
                  <th class="title">Report Number</th>
                  <th class="title">Hazmat Insp</th>
                  <th class="title">Insp Level</th>
                  <th class="title"># of Viol</th>
                </tr>
                <xsl:for-each select="DriverReportResponseType/driverInformationResponse/DriverRecord/InspectionRecords/InspectionRecord">
                  <tr  class="rowClass">
                    <td class="label">
                      <xsl:value-of select="position()"/>
                    </td>
                    <td>
                      <xsl:call-template name="FormatDate">
                        <xsl:with-param name="DateTime" select="InspectionDate"/>
                      </xsl:call-template>
                    </td>
                    <td>
                      <xsl:value-of select="USDOTNumber"/>
                    </td>
                    <td>
                      <xsl:value-of select="CarrierName"/>
                    </td>
                    <td>
                      <xsl:value-of select="concat(DriverLastName, ', ', DriverFirstName, ' ')"/>
                      <xsl:if test="DriverType ='S'">(Listed as Co-Driver)</xsl:if>
                    </td>
                    <td>
                      <xsl:value-of select="DriverLicenseNumber"/>
                    </td>
                    <td>
                      <xsl:value-of select="DriverLicenseState"/>
                    </td>
                    <td>
                      <xsl:call-template name="FormatDate">
                        <xsl:with-param name="DateTime" select="DriverDOB"/>
                      </xsl:call-template>
                    </td>
                    <td>
                      <xsl:value-of select="ReportState"/>
                    </td>
                    <td>
                      <xsl:value-of select="ReportNumber"/>
                    </td>
                    <td>
                      <xsl:choose>
                        <xsl:when test="TotalHazMatSent > 0">Y</xsl:when>
                        <xsl:otherwise>N</xsl:otherwise>
                      </xsl:choose>
                    </td>
                    <td>
                      <xsl:value-of select="InspectionLevelId"/>
                    </td>
                    <td>
                      <xsl:value-of select="TotalViolations"/>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="13" class="block">
                      <table id="tblVoilation" cellspacing="0" cellpadding="0" width="100%" >
                        <xsl:if test="TotalViolations = '0'">
                          <tr class="rowClass">
                            <td>
                              
                            </td>
                          </tr>
                        </xsl:if>
                        <xsl:if test="TotalViolations > '0'">
                          <xsl:for-each select="InspectionViolations/InspectionViolation">
                            <tr>
                              <!--<td class="nested" colspan="4"></td>
                              <td class="nested" colspan="1">
                                <table>
                                  <tr>-->
                                    <td class="violationType">
                                      <xsl:choose>
                                        <xsl:when test="InspViolUnit = 'D'">Driver Violation:</xsl:when>
                                        <xsl:when test="InspViolUnit = 'C'">Co-Driver Violation:</xsl:when>
                                        <xsl:otherwise>Vehicle Violation:</xsl:otherwise>
                                      </xsl:choose>
                                    </td>
                                    <td class="nested">
                                      <xsl:value-of select="concat(PartNo, '.', PartNoSection)"/>
                                    </td>
                                  <!--</tr>
                                </table>
                              </td>-->
                              <td class="VoilationDesc" >
                                <xsl:value-of select="SectionDesc"/>
                              </td>
                              <td class="VoilationOOS" >
                                <xsl:choose>
                                  <xsl:when test="OutOfServiceIndicator = 'N'">NON-OOS</xsl:when>
                                  <xsl:otherwise>OOS</xsl:otherwise>
                                </xsl:choose>
                                <xsl:if test="InspViolUnit = 'C'">
                                  <span>*</span>
                                </xsl:if>
                                <xsl:if test="PostCrashFlag = 'Y'">
                                  <span>**</span>
                                </xsl:if>
                              </td>
                              <!--<td class="nested">
                                <table class="violationDetail" cellpading="0" cellspacing="0">
                                  <tr>-->
                                    <!--<td>
                                        
                                    </td>-->
                                  <!--</tr>
                                </table>
                              </td>-->
                            </tr>
                          </xsl:for-each>
                        </xsl:if>
                      </table> 
                    </td>
                  </tr>
                </xsl:for-each>
              </table>
            </td>
          </tr>
              </xsl:if>
          <tr class="subHeadingCls">
            <td class="block">
              <h4>Violation Summary</h4>
            </td>
          </tr>
          <tr>
            <td class="block">
              <table border="1" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                  <th class="title">Violation #</th>
                  <th class="title">Description</th>
                  <th class="title"># of Violations</th>
                  <th class="title"># of Out-of-service Violations</th>
                </tr>
                <xsl:for-each select="DriverReportResponseType/inspectionViolationSummary/inspectionViolationSummaryRecordType">
                  <tr class="skinny">
                    <td class="title">
                      <xsl:value-of select="ViolationNum"/>
                    </td>
                    <td>
                      <xsl:value-of select="ViolationDesc"/>
                    </td>
                    <td class="right narrow">
                      <xsl:value-of select="ViolationCount"/>
                    </td>
                    <td class="right narrow">
                      <xsl:value-of select="ViolationOOSCount"/>
                    </td>
                  </tr>
                </xsl:for-each>
              </table>
            </td>
          </tr>
          <tr>
            <td class="nested">
              <table>
                <tr>
                  <td class="nested">
                    <span>
                      The summary counts and rates do not include violations that were a result of the crash. The summary counts and rates for the co-driver only include violations that were charged to the co-driver. Summary counts and rates for the primary driver do not include violations charged to the co-driver.
                    </span>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          </xsl:when>
            <xsl:otherwise>
              <tr class="noRecordClass">
                <td class="">
                    <xsl:value-of select="DriverReportResponseType/driverInformationResponse/StatusDescription"/>
                </td>
              </tr>
            </xsl:otherwise>
          </xsl:choose>
        </table>
        <div class="PSPContent">
          <span>
            Report executed at:
            <xsl:call-template name="FormatDateLongForm">
              <xsl:with-param name="DateTime" select="DriverReportResponseType/driverInformationResponse/RequestDate"/>
            </xsl:call-template>
          </span>
          <br/>
          <span>
            MCMIS snapshot date:
            <xsl:value-of select="DriverReportResponseType/driverInformationResponse/MCMISUploadDate"/>
          </span>
          <br/>
          <span style="color:red">
            * Violation charged to co-driver ** Post crash violation
          </span>
          <br/>
          <span>
            For an explanation of FMCSA-reportable crashes see
            <a href="https://www.psp.fmcsa.dot.gov/psp/FAQ.aspx">https://www.psp.fmcsa.dot.gov/psp/FAQ.aspx</a>
            .
          </span>
        </div>
      <!--</body>
    </html>-->
  </xsl:template>
  <xsl:template name="FormatDate">
    <xsl:param name="DateTime"/>
    <!--  new date format 2006-01-14T08:55:22  -->
    <xsl:variable name="day">
      <xsl:value-of select="substring($DateTime,1,2)"/>
    </xsl:variable>
    <xsl:variable name="mo">
      <xsl:value-of select="substring($DateTime,3,2)"/>
    </xsl:variable>
    <xsl:variable name="year">
      <xsl:value-of select="substring($DateTime,5,4)"/>
    </xsl:variable>
    <!--
<xsl:value-of select="concat($day, '/', $mo, '/', $year)"/>
-->
    <xsl:value-of select="$day"/>
    <xsl:value-of select="'/'"/>
    <xsl:value-of select="$mo"/>
    <xsl:value-of select="'/'"/>
    <xsl:value-of select="$year"/>
  </xsl:template>
  <xsl:template name="FormatDateLongForm">
    <xsl:param name="DateTime"/>
    <!--  new date format 2006-01-14T08:55:22  -->
    <xsl:variable name="day">
      <xsl:value-of select="substring($DateTime,6,2)"/>
    </xsl:variable>
    <xsl:variable name="mo">
      <xsl:value-of select="substring($DateTime,9,2)"/>
    </xsl:variable>
    <xsl:variable name="year">
      <xsl:value-of select="substring($DateTime,0,5)"/>
    </xsl:variable>
    <xsl:variable name="hourRaw">
      <xsl:value-of select="substring($DateTime,12,2)"/>
    </xsl:variable>
    <xsl:variable name="minute">
      <xsl:value-of select="substring($DateTime,15,2)"/>
    </xsl:variable>
    <xsl:variable name="second">
      <xsl:value-of select="substring($DateTime,18,2)"/>
    </xsl:variable>
    <xsl:variable name="hour">
      <xsl:choose>
        <xsl:when test="$hourRaw > 12">
          <xsl:value-of select="$hourRaw - 12"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$hourRaw"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="ampm">
      <xsl:choose>
        <xsl:when test="$hourRaw > 12">PM</xsl:when>
        <xsl:otherwise>AM</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:value-of select="concat($day, '/', $mo, '/', $year, ' ', $hour, ':', $minute, ':', $second, ' ', $ampm)"/>
  </xsl:template>
</xsl:stylesheet>