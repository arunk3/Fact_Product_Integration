<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:a="http://ns.hr-xml.org/2004-08-02">
    <xsl:param name="admin" select="defaultstring" />
    <xsl:output method="html"/>
    
<!--<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" encoding="utf-8"/>-->
    
    <xsl:template match="/">

      <div class="sectionsummary">     
      <br />
      <h3>Name:</h3>
      <h4>        
        <xsl:value-of select="concat(a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:GivenName,', ',a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:FamilyName)" />
      </h4>
      <br/>
    </div>

         

      

      <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:ResultStatus = 'Clear'">
        <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
          <thead>
            <tr>
              <th class="begin"></th>
              <th colspan="5">Search Information</th>
              <th class="end"></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="begin">i</td>
              <td colspan="5">
                No Records Found.
              </td>
              <td class="end">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </tbody>
          <tfoot>
            <td class="begin"></td>
            <td colspan="5"></td>
            <td class="end"></td>
          </tfoot>
        </table>
      </xsl:if>
        <div style="width:80%;padding-left:100px;" >
            <pre align="center">
                <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text"/>
            </pre>
        </div>
          
        <!--<xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/@recordCount != '0'">
        <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
          <thead>
            <tr>
              <th class="begin"></th>
              <th align="left">
                <u>PERSONAL INFO</u></th>
              <th align="left">
                <u>ADDRESS INFO</u></th>
              <th align="left">
                <u>PHONE INFO</u></th>
              <th class="end"></th>
            </tr>
          </thead>
          <tbody>
            <xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData">

              <xsl:choose>
                <xsl:when test="(position() mod 2 = 0)">
                  <tr>
                    <td class="begin">
                      <xsl:value-of select="(position() )" />
                    </td>
                    <td style="background-color:#fff;">
                      <xsl:value-of select="a:PersonName/a:FamilyName" />, <xsl:value-of select="a:PersonName/a:GivenName" /><br/>
                      SSN : xxx-xx-<xsl:value-of select="substring(DemographicDetail/GovernmentId,6,4)" />                      
                      <br/>
                      DOB : <xsl:value-of select="DemographicDetail/DateOfBirth" />
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
                      Age : <xsl:value-of select="DemographicDetail/Age" />
                    </td>
                    <td style="background-color:#fff;">
                      <xsl:value-of select="PostalAddress/DeliveryAddress/AddressLine" />
                      <xsl:text> </xsl:text>
                      <br />
                      <xsl:value-of select="PostalAddress/Municipality" />
                      <xsl:text>, </xsl:text>
                      <xsl:value-of select="PostalAddress/Region" />
                      <xsl:text> </xsl:text>
                      <br />
                      <xsl:value-of select="PostalAddress/County" />
                      <xsl:text> </xsl:text>
                      <br />
                      <xsl:value-of select="PostalAddress/CountryCode" />
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="PostalAddress/PostalCode" />
                      <br/>
                      <xsl:value-of select="EffectiveDate/StartDate" />
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;-&amp;nbsp;</xsl:text>
                      <xsl:value-of select="EffectiveDate/EndDate" />
                    </td>
                    <td style="border-right: none;background-color:#fff;">
                      <xsl:choose>
                        <xsl:when test="count(ContactMethod)='0'">
                          NONE
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:for-each select="ContactMethod">
                            <xsl:value-of select="Telephone/FormattedNumber"/>
                            <br/>
                          </xsl:for-each>
                        </xsl:otherwise>
                      </xsl:choose>
                    </td>
                    <td  style="background-color:#fff;" class="end">
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                    </td>                    
                  </tr>
                </xsl:when>
                <xsl:otherwise>
                  <tr>
                    <td class="begin">
                      <xsl:value-of select="(position() )" />
                    </td>
                    <td >
                      <xsl:value-of select="PersonName/FamilyName" />, <xsl:value-of select="PersonName/GivenName" /><br/>
                      SSN : xxx-xx-<xsl:value-of select="substring(DemographicDetail/GovernmentId,6,4)" />                      
                      <br/>
                      DOB : <xsl:value-of select="DemographicDetail/DateOfBirth" />
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
                      Age : <xsl:value-of select="DemographicDetail/Age" />
                    </td>
                    <td>
                      <xsl:value-of select="PostalAddress/DeliveryAddress/AddressLine" />
                      <xsl:text> </xsl:text>
                      <br />

                      <xsl:value-of select="PostalAddress/Municipality" />
                      <xsl:text>, </xsl:text>
                      <xsl:value-of select="PostalAddress/Region" />
                      <xsl:text> </xsl:text>
                      <br />
                      <xsl:value-of select="PostalAddress/County" />
                      <xsl:text> </xsl:text>
                      <br />

                      <xsl:value-of select="PostalAddress/CountryCode" />
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="PostalAddress/PostalCode" />
                      <br/>
                      <xsl:value-of select="EffectiveDate/StartDate" />
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;-&amp;nbsp;</xsl:text>
                      <xsl:value-of select="EffectiveDate/EndDate" />
                    </td>
                    <td style="border-right: none;">
                      <xsl:choose>
                        <xsl:when test="count(ContactMethod)='0'">
                          NONE
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:for-each select="ContactMethod">
                            <xsl:value-of select="Telephone/FormattedNumber"/>
                            <br/>
                          </xsl:for-each>
                        </xsl:otherwise>
                      </xsl:choose>
                    </td>
                    <td class="end">
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                    </td>
                  </tr>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>
          </tbody>
          <tfoot>
            <td class="begin"></td>
            <td></td>
            <td></td>
            <td style="border-right: none;"></td>
            <td class="end"></td>
          </tfoot>
        </table>
      </xsl:if>-->
      


  </xsl:template>
</xsl:stylesheet>