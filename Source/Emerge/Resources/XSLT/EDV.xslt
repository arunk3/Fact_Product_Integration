﻿<?xml version="1.0" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output indent="yes" method="html"/>
	<xsl:template match="/">
		<html>
			<head>
				<style type="text/css">
					body{font-family: Lucida Grande,Arial,Helvetica,sans-serif;font-size: 14px;font-weight: normal;}

					.borders {border-left:1px solid #000000;
					border-right:1px solid #000000;
					border-top:1px solid #000000;
					border-bottom:1px solid #000000;}

					.tbl{Border:1px Solid #000;font-size:16pt;font-weight:bold;}
					.titletxt{font-size:12pt;font-weight:bold;}
				</style>
			</head>
			<body bgcolor="#ffffff">
				 <div class="people-search-wrap">
					<table width="100%" border="0" class="people-search">
						<tr>
							<td>
							 <table  width="100%" border="0">
                  <tr >
                    <td class="width_10"></td>
                    <td style="text-align:left;font-size:20px;font-weight: bold;">
                      Summary
                    </td>
                  </tr>
                </table>
							</td>
						</tr>
               <tr>
              <td>
                <table width="100%" >
                  <tr>
                    <td class="width_10"></td>
                    <td class="width_25">
                    Report Type:Education Verification
                    </td>
                    <td></td>
                  </tr>
                  <tr>
                    <td class="width_10"></td>
                    <td style="width:5%;">
                      Subject:&#160;
                      <xsl:value-of select="ScreeningResults/completeOrder/subject/name_last"/>&#160;
                      <xsl:value-of select="ScreeningResults/completeOrder/subject/name_first"/>
                    </td>
                  </tr>
                  <tr>
                    <td class="width_10"></td>
                    <td style="width:5%;">
                      DOB:&#160;
                      <xsl:value-of select="ScreeningResults/completeOrder/subject/dob"/>
                    </td>
                  </tr>

                </table>
              </td>
            </tr>
					
						<!--<tr>
							<td>
							 <table  width="100%" >
                  <tr>
                    <td class="width_10"></td>
                    <td style="width:5%;">
                      Subject:&#160;
                      <xsl:value-of select="ScreeningResults/completeOrder/subject/name_last"/>&#160;
                      <xsl:value-of select="ScreeningResults/completeOrder/subject/name_first"/>
                    </td>
                  </tr>
                  <tr>
                    <td class="width_10"></td>
                    <td style="width:5%;">
                      DOB:&#160;
                      <xsl:value-of select="ScreeningResults/completeOrder/subject/dob"/>
                    </td>
                  </tr>

                </table>
							</td>
						</tr>-->
            
            <xsl:for-each select="ScreeningResults/completeOrder/subOrder">
							<!--Multiple subOrders-->
							<xsl:variable name="counter" select="position()-1" />
							<tr>
								<td>
									<table  width="100%" border="0" >
										<tr class="search-heading sparater-td bg-gray">
                   <td class="width_10 text-right">
					   <span class="countr blue-no" style="padding-top: 4px;">
                          <xsl:value-of select="$counter + 1" />
                        </span>
                      </td>
											<td >
                        <span class="PS_SSN_DEATH_TOP">
												<xsl:copy>
													Education History 
												</xsl:copy>
                      </span>
											</td>
										</tr>
									</table>
								</td>
							</tr>

							<tr>
								<td>
									<table>
										<tr>
											<td style=" font-size:14px; width:20%; text-align:right;">
												<b>Institution:</b>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<b>
													<xsl:value-of select="school_name"/>
												</b>
											</td>
                      <td style=" font-size:14px; text-align:left;">
                       
                      </td>
                      <td style=" font-size:14px; text-align:left;">
                       
                      </td>
										</tr>
										<tr>
                      <td style=" font-size:14px; width:20%; text-align:right;">
                      
                      </td>
											<td style=" font-size:14px; text-align:left;">
												<u>Provided on Application Materials</u>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<u>Obtained From Education Interview</u>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<u>Research Results</u>
											</td>
                      <td style=" font-size:14px; text-align:left;">
                        
                      </td>
                     
										</tr>
										<tr>
											<td style=" font-size:14px; width:20%; text-align:right;">
												Address 1:&#160;
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="address1"/>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="verified_address1"/>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="disposition_address1"/>
											</td>
										</tr>
										<tr>
											<td style=" font-size:14px; width:10%; text-align:right;">
												Address 2:&#160;
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="address2"/>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="verified_address2"/>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="disposition_address2"/>
											</td>
										</tr>
										<tr>
											<td style=" font-size:14px; width:20%; text-align:right;">
												City:&#160;
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="city"/>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="verified_city"/>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="disposition_city"/>
											</td>
										</tr>
										<tr>
											<td style=" font-size:14px; width:10%; text-align:right;">
												State:&#160;
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="state"/>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="verified_state"/>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="disposition_state"/>
											</td>
										</tr>
										<tr>
											<td style=" font-size:14px; width:20%; text-align:right;">
												Zip:&#160;
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="zip"/>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="verified_zip"/>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="disposition_zip"/>
											</td>
										</tr>
										<tr>
											<td style=" font-size:14px; width:20%; text-align:right;">
												Graduation Date:&#160;
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="graduation_date"/>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="verified_graduation_date"/>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="disposition_graduated"/>
											</td>
										</tr>
										<tr>
											<td style=" font-size:14px; width:20%; text-align:right;">
												Date From:&#160;
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:if test="attended_from!=''">
													<xsl:value-of select="attended_from"/>
												</xsl:if>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="verified_attended_from"/>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="disposition_attended_from"/>
											</td>
										</tr>
										<tr>
											<td style=" font-size:14px; width:20%; text-align:right;">
												Date To: &#160;
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:if test="attended_to!=''">
													<xsl:value-of select="attended_to"/>
												</xsl:if>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="verified_attended_to"/>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="disposition_attended_to"/>
											</td>
										</tr>
										<tr>
											<td style=" font-size:14px; width:10%; text-align:right;">
												Major(s):&#160;
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="major"/>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="verified_major"/>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="disposition_major"/>
											</td>
										</tr>
										<tr>
											<td style=" font-size:14px; width:10%; text-align:right;">
												Degree(s):&#160;
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="degree"/>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="verified_degree"/>
											</td>
										</tr>
										<tr>
											<td style=" font-size:14px; width:20%; text-align:right;">
												Graduated:&#160;
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="graduated"/>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="verified_graduated"/>
											</td>
										</tr>
										<tr>
											<td style=" font-size:14px; width:20%; text-align:right;">
												Transcript:&#160;
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="transcript"/>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="verified_transcript"/>
											</td>
										</tr>
										<tr>
											<td style=" font-size:14px; width:20%; text-align:right;">
												GPA:&#160;
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="GPA"/>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="verified_GPA"/>
											</td>
										</tr>
										<tr>
											<td style=" font-size:14px; width:20%; text-align:right;">
												Student ID #:&#160;
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="student_id_number"/>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="''"/>
											</td>
										</tr>
										<tr>
											<td style=" font-size:14px; width:20%; text-align:right;">
												Highest Achieved:&#160;
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="highest_achieved"/>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="''"/>
											</td>
										</tr>
										<tr>
											<td style=" font-size:14px; width:20%; text-align:right;">
												Currently Enrolled:&#160;
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="currently_enrolled"/>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="''"/>
											</td>
										</tr>
										<tr>
											<td style=" font-size:14px; width:20%; text-align:right;">
												Order Comments:&#160;
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="''"/>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="''"/>
											</td>
										</tr>
										<tr>
											<td style=" font-size:14px; width:20%; text-align:right;">
												Person Interviewed:&#160;
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="verified_verifier"/>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="''"/>
											</td>
										</tr>
										<tr>
											<td style=" font-size:14px; width:20%; text-align:right;">
												Researched By:&#160;
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="verified_verifier"/>
											</td>
											<td style=" font-size:14px; text-align:left;">
												<xsl:value-of select="''"/>
											</td>
										</tr>
										<tr>
											<td style=" font-size:14px; width:20%; text-align:right;">
												Research Comments:&#160;
											</td>
											<td colspan="2" style=" font-size:14px; text-align:left;">
												<xsl:value-of select="verified_comments"/>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</xsl:for-each>
						<tr>
							<td>

								<xsl:choose>
									<xsl:when test="ScreeningResults/completeOrder/subOrder/@filledCode ='clear'">
										<p style="text-align: justify; font-size:14px;">NO RECORDS WERE LOCATED.</p>
									</xsl:when>
									<xsl:otherwise>

									</xsl:otherwise>
								</xsl:choose>
							</td>
						</tr>
						<tr>
							<td align="center" colspan="2" >
								<span style="color:red;font-weight:600;font-size:14px;">**** End Of Report ****</span>
							</td>
						</tr>
					</table>


			 </div>

			</body>
		</html>

	</xsl:template>

</xsl:stylesheet>