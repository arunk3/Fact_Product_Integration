<?xml version='1.0'  encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  
  <xsl:template match="/">
    <div class="people-search-wrap">
    
    <xsl:value-of select="FastraxNetwork/report-requested/report/report-detail-header/report-data" disable-output-escaping="yes" /> 
      <!--<xsl:value-of select="*" disable-output-escaping="yes" />-->      
      <!--<xsl:value-of select="a:FastraxNetwork/a:report-requested/a:report/a:report-detail-header/a:report-data" disable-output-escaping="yes" />-->
   
    <div class="sectionsummary">
      <h3 style="line-height: 1em;">

        Data Sources Searched:<br/>
        Court Records, Department of

        Corrections, Administrative Office of Courts,<br/>
        National Sex Offender Registry, OFAC, OIG<br/>
        For a complete list of coverage,

        please visit<br/>
        <a href="http://emerge.intelifi.com/coverage.pdf" target="_blank">US Criminal Rapid Search Coverage Guide</a>

      </h3>
    </div>
    </div>

  </xsl:template>
</xsl:stylesheet>