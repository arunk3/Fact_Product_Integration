﻿<?xml version="1.0" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output indent="yes" method="html"/>
  <xsl:template match="/">
    <html>
      <head>
        <style type="text/css">
          body{font-family: Lucida Grande,Arial,Helvetica,sans-serif;font-size: 14px;font-weight: normal;}

          .borders {border-left:1px solid #000000;
          border-right:1px solid #000000;
          border-top:1px solid #000000;
          border-bottom:1px solid #000000;}

          .tbl{Border:1px Solid #000;font-size:16pt;font-weight:bold;}
          .titletxt{font-size:12pt;font-weight:bold;}
        </style>
      </head>
      <body bgcolor="#ffffff">
        <div class="people-search-wrap">
          <table width="100%" border="0" class="people-search">
            <tr>
              <td>
                <table  width="100%" border="0">
                  <tr >
                    <td class="width_10"></td>
                    <td style="text-align:left;font-size:20px;font-weight: bold;">
                      Summary
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td>
                <table width="100%" >
                  <tr>
                    <td class="width_10"></td>
                    <td class="width_25">
                      Report Type:&#160;Employment Verification
                    </td>
                    <td></td>
                  </tr>
                  <tr>
                    <td class="width_10"></td>
                    <td style="width:5%;">
                      Applicant:&#160;
                      <xsl:value-of select="RPB2BOrder/AR_ApplicantLastName"/>&#160;
                      <xsl:value-of select="RPB2BOrder/AR_ApplicantFirstName"/>&#160;
                      <xsl:value-of select="RPB2BOrder/AR_ApplicantMiddleName"/>
                    </td>
                  </tr>
                  <tr>
                    <td class="width_10"></td>
                    <td style="width:5%;">
                      DOB:&#160;
                      <xsl:value-of select="RPB2BOrder/AR_ApplicantBirthDate"/>
                    </td>
                  </tr>

                </table>
              </td>
            </tr>


            <xsl:choose>
              <xsl:when test="RPB2BOrder/OrderStatus='COMPLETE'">
                <tr>
                  <td>
                    <table>
                      <tr>
                        <td class="width_10"></td>
                        <td> </td>
                        <td class="width_45">
                          <u>Provided by Subject</u>
                        </td>
                        <td class="width_45">
                          <u>Provided by Source</u>
                        </td>
                      </tr>

                      <tr>
                        <td class="width_10"></td>
                        <td class="width_15 text-right">
                          Employer:&#160;
                        </td>
                        <td>
                          <xsl:value-of select="RPB2BOrder/AR_ReferenceCompany"/>
                        </td>
                        <td>
                          <xsl:value-of select="RPB2BOrder/RR_ReferenceCompany"/>
                        </td>
                      </tr>
                      <tr>
                        <td class="width_10"></td>
                        <td  class="width_30 text-right">
                          Contact Phone Number:&#160;
                        </td>
                        <td>
                          <xsl:value-of select="RPB2BOrder/AR_ReferencePhoneNumber"/>
                        </td>
                        <td>
                          <xsl:value-of select="RPB2BOrder/RR_ReferencePhoneNumber"/>
                        </td>
                      </tr>
                      <tr>
                        <td class="width_10"></td>
                        <td  class="width_30 text-right">
                          Address 1:&#160;
                        </td>
                        <td  class="width_30">
                          <xsl:value-of select="RPB2BOrder/AR_ApplicantAddress1"/>
                        </td>
                        <td>
                          <xsl:value-of select="RPB2BOrder/RR_ApplicantAddress1"/>
                        </td>
                      </tr>
                      <tr>
                        <td class="width_10"></td>
                        <td  class="width_30 text-right">
                          Address 2:&#160;
                        </td>
                        <td>
                          <xsl:value-of select="RPB2BOrder/AR_ApplicantAddress2"/>
                        </td>
                        <td>
                          <xsl:value-of select="RPB2BOrder/RR_ApplicantAddress2"/>
                        </td>
                      </tr>
                      <tr>
                        <td class="width_10"></td>
                        <td  class="width_30 text-right">
                          City:&#160;
                        </td>
                        <td>
                          <xsl:value-of select="RPB2BOrder/AR_ApplicantCity"/>
                        </td>
                        <td>
                          <xsl:value-of select="RPB2BOrder/RR_ApplicantCity"/>
                        </td>
                      </tr>
                      <tr>
                        <td class="width_10"></td>
                        <td  class="width_30 text-right">
                          State:&#160;
                        </td>
                        <td>
                          <xsl:value-of select="RPB2BOrder/AR_ApplicantState"/>
                        </td>
                        <td>
                          <xsl:value-of select="RPB2BOrder/RR_ApplicantState"/>

                        </td>
                      </tr>
                      <tr>
                        <td class="width_10"></td>
                        <td  class="width_30 text-right">
                          Zip:&#160;
                        </td>
                        <td>
                          <xsl:value-of select="RPB2BOrder/AR_ApplicantZipCode"/>
                        </td>
                        <td>
                          <xsl:value-of select="RPB2BOrder/RR_ApplicantZipCode"/>

                        </td>
                      </tr>
                      <tr>
                        <td class="width_10"></td>
                        <td  class="width_30 text-right">
                          Position/Title:&#160;
                        </td>
                        <td>
                          <xsl:value-of select="RPB2BOrder/AR_JobTitle"/>
                        </td>
                        <td>
                          <xsl:value-of select="RPB2BOrder/RR_JobTitle"/>
                        </td>
                      </tr>
                      <tr>
                        <td class="width_10"></td>
                        <td  class="width_30 text-right">
                          Income:&#160;
                        </td>
                        <td>
                          <xsl:value-of select="RPB2BOrder/AR_Salary"/>
                        </td>
                        <td>
                          <xsl:value-of select="RPB2BOrder/RR_Salary"/>
                        </td>
                      </tr>
                      <tr>
                        <td  class="width_10"></td>
                        <td  class="width_30 text-right">
                          Reason For Leaving:&#160;
                        </td>
                        <td>
                          <xsl:value-of select="RPB2BOrder/AR_ReasonForLeaving"/>
                        </td>
                        <td>
                          <xsl:value-of select="RPB2BOrder/RR_ReasonForLeaving"/>
                        </td>
                      </tr>
                      <tr>
                        <td  class="width_10"></td>
                        <td  class="width_30 text-right">
                          Employer Comments:&#160;
                        </td>
                        <td>
                          <xsl:value-of select="comments"/>
                        </td>
                        <td>
                          <xsl:value-of select="verified_employer_comments"/>
                        </td>
                      </tr>
                      <tr>
                        <td  class="width_10"></td>
                        <td  class="width_30 text-right">
                          Eligible for Rehire:&#160;
                        </td>
                        <td>

                        </td>
                        <td>
                          <xsl:value-of select="RPB2BOrder/RR_EligibleForRehire"/>
                        </td>
                      </tr>
                      <tr>
                        <td  class="width_10"> </td>
                        <td  class="width_30 text-right">
                          Reason for rehire eligibility:&#160;
                        </td>
                        <td>
                          <xsl:value-of select="''"/>
                        </td>
                        <td>
                          <xsl:value-of select="RPB2BOrder/RR_NoRehireReason"/>
                        </td>
                      </tr>
                      <tr>
                        <td  class="width_30"> </td>
                        <td  class="width_30 text-right">
                          AKA's on file :&#160;
                        </td>
                        <td>
                          <xsl:value-of select="''"/>
                        </td>
                        <td></td>
                      </tr>
                      <tr>
                        <td  class="width_10"></td>
                        <td  class="width_30 text-right">
                          Order Comments:&#160;
                        </td>
                        <td>
                          <xsl:value-of select="''"/>
                        </td>
                        <td></td>
                      </tr>
                      <tr>
                        <td  class="width_10"> </td>
                        <td  class="width_15 text-right">
                          Person Interviewed:
                        </td>
                        <td>

                        </td>
                        <td>
                          <xsl:value-of select="RPB2BOrder/RR_VerifierName"/>

                        </td>
                      </tr>
                      <tr>
                        <td  class="width_10"> </td>
                        <td  class="width_30 text-right">
                          Researched By:
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                      </tr>
                      <tr>
                        <td  class="width_10"></td>
                        <td  class="width_30 text-right">
                          Research Comments:&#160;
                        </td>
                        <td>
                          <xsl:value-of select="researcher_comments"/>
                        </td>
                        <td></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </xsl:when>
              <xsl:otherwise>
                <tr>
                  <td>
                    <xsl:if test="RPB2BOrder/OrderStatus='NO LISTING'">
                      <p style="text-align: justify; font-size:14px;">NO RECORDS WERE LOCATED.</p>
                    </xsl:if>
                    <xsl:if test="RPB2BOrder/OrderStatus='NO RECORD FOUND'">
                      <p style="text-align: justify; font-size:14px;">NO RECORDS FOUND.</p>
                    </xsl:if>
                  </td>
                </tr>
              </xsl:otherwise>
            </xsl:choose>
            <tr>
              <td align="center" colspan="2" >
                <span style="color:red;font-weight:600;font-size:14px;">**** End Of Report ****</span>
              </td>
            </tr>
          </table>
        </div>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>