﻿<?xml version="1.0" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output indent="yes" method="html"/>
  <xsl:template match="/">
    <html>
      <head>
        <style type="text/css">
          body{font-family: Lucida Grande,Arial,Helvetica,sans-serif;font-size: 14px;font-weight: normal;}

          .borders {border-left:1px solid #000000;
          border-right:1px solid #000000;
          border-top:1px solid #000000;
          border-bottom:1px solid #000000;}

          .tbl{Border:1px Solid #000;font-size:16pt;font-weight:bold;}
          .titletxt{font-size:12pt;font-weight:bold;}
        </style>
      </head>
      <body bgcolor="#ffffff">
        <div class="people-search-wrap">
          <table width="100%" border="0">
            <tr>
              <td>
                <table  width="100%" border="0" class="people-search">
                  <tr class="search-heading">
                    <td class="width_10"></td>
                    <td style="text-align:center;font-size:20px;font-weight: bold;">
                      CONFIDENTIAL
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td style="height:5px" colspan="2">
              </td>
            </tr>
            <tr>
              <td>
                <table width="100%" style="font-size: 14pt">
                  <tr>
                    <td class="width_10"></td>
                    <td style="text-align:left" >
                      <b> Background Verification Report</b>
                    </td>
                    <td></td> 
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td style="height:5px" colspan="2">
              </td>
            </tr>
            <tr>
              <td>
                <table width="100%" >
                  <tr>
                    <td class="width_10"></td>
                    <td style="text-align:left;width:50%; font-size:14px;">
                      Requestor:&#160;<xsl:value-of select="ScreeningResults/orderInfo/requester_name"/>
                    </td>

                    <td style="text-align:left;width:50%; font-size:14px;">
                      Subject:&#160;
                      <xsl:value-of select="ScreeningResults/completeOrder/subject/name_last"/>&#160;
                      <xsl:value-of select="ScreeningResults/completeOrder/subject/name_first"/>
                    </td>
                  </tr>
                  <tr>
                    <td class="width_10"></td>
                    <td style="text-align:left;width:50%; font-size:14px;">
                      Position Applied:
                    </td>
                    <!--</tr>
									<tr>
                    <td class="width_10"></td>-->
                    <td style="text-align:left;width:50%; font-size:14px;">
                      DOB:&#160;
                      <xsl:value-of select="ScreeningResults/completeOrder/subject/dob"/>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td style="height:5px" colspan="2">
              </td>
            </tr>

            <xsl:for-each select="ScreeningResults/completeOrder/subOrder">
              <!--Multiple subOrders-->
              <xsl:variable name="counter" select="position()-1" />
              <tr>
                <td>
                  <table class="tbl" width="100%" border="0">
                    <tr bgcolor="#efefef">
                      <td style="text-align:center">
                        Workers Compensation Verification #<xsl:value-of select="$counter + 1" />
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td style="font-size:12px;">
                  State
                </td>
              </tr>
              <tr>
                <td style="font-size:12px;">
                  <xsl:value-of select="state"/>
                </td>
              </tr>
            </xsl:for-each>
            <tr>
              <td>

                <xsl:choose>
                  <xsl:when test="ScreeningResults/completeOrder/subOrder/@filledCode ='clear'">
                    <p>No Record Found.</p>
                  </xsl:when>
                  <xsl:otherwise>
                    <p class="text-center">Record Found.</p>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
            </tr>
            <tr>
              <td align="center" colspan="2"  >
                <span style="color:red;font-weight:600;font-size:14px;">**** End Of Report ****</span>
              </td>
            </tr>
            <tr>
              <tr>
                <td colspan="2" align="left">
                  <p>
                    <b>Workers Compensation Disclaimer:</b> We will provide you available workers comp records ordered after we have provided other reports on the subject.  The Americans with Disabilities Act provides that any medically related information may only be requested from an applicant 'post offer' and should be the last category of information an employer reviews on an applicant.  Therefore we will delay the delivery of workers comp reports until after you have received our other reports.  If for some reason you have not reviewed the other reports by the time we provide a workers comp report you are requested for your legal compliance with the ADA not to open that report until you have reviewed the other reports and the applicant is still qualified to be hired.
                  </p>
                </td>
              </tr>
            </tr>
          </table>


        </div>

      </body>
    </html>

  </xsl:template>

</xsl:stylesheet>