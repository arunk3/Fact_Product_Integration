<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:output method="html" />
  <xsl:template match="/FastraxNetwork">
    <xsl:variable name="TotalRecords">
      <xsl:value-of select="count(report-requested/report/report-detail-header/report-incident)" />
    </xsl:variable>
    <xsl:variable name="DeletedRecords">
      <xsl:value-of select="count(report-requested/report/report-detail-header/report-incident[@Deleted=1])" />
    </xsl:variable>
    <div class="people-search-wrap btn-view">
      <table id="peopleTable" class="people-search" width="100%">
        <tr class="header-scr">
          <td colspan="6"  align="left" style="padding-top: 6px; padding-bottom: 8px;">
            <span class="PS_SSN_DEATH_TOP_scr">
              CRIMINAL ARRESTS AND CONVICTIONS 
            </span>
          </td>

        </tr>
        <xsl:choose>

          <xsl:when test ="$TotalRecords=$DeletedRecords">
            <tr >
              <td  ></td>
              <td></td>
              <td></td>
              <td>
                <span class="PS_SSN_DEATH_TOP_scr">
                  State Criminal Report (0) Results
                </span>
              </td>
              <td style="width:100px; text-align:right;"></td>
              <td></td>
            </tr>

            <tr>
              <td  ></td>
              <td></td>
              <td></td>
              <td>
                <span class="PS_SSN_DEATH_TOP_scr" style="color: red; font-weight: bold; font-size: 12px;">
                  No Record Found
                </span>
              </td>
              <td style="width:100px; text-align:right;"></td>
              <td></td>
            </tr>
          </xsl:when>
          <xsl:otherwise>
            <tr>
              <td class="scr-bold-color" >State:</td>
              <td>
                <xsl:value-of select="report-requested/report/report-detail-header/state" />
              </td>
              <td  class="scr-bold-color">Years Checked:</td>
              <td>
                <xsl:value-of select="report-requested/report/report-detail-header/time-checked" />
              </td>
              <td> </td>
              <td> </td>
            </tr>

            <tr>
              <td  class="scr-bold-color">County:</td>
              <td>
                <xsl:value-of select="report-requested/report/report-detail-header/county" />
              </td>
              <td  class="scr-bold-color">Convictions Admitted:</td>
              <td>
                <xsl:value-of select="report-requested/report/report-detail-header/felonies-admitted" />
              </td>
              <td> </td>
              <td> </td>
            </tr>

            <tr>
              <td  class="scr-bold-color">City:</td>
              <td></td>
              <td  class="scr-bold-color">Details Disclosed:</td>
              <td></td>
              <td> </td>
              <td> </td>
            </tr>
            <tr>
              <td  class="scr-bold-color">Record Found:</td>
              <td>
                <xsl:if test="report-requested/report/report-detail-header/clear-record = '0'">Yes</xsl:if>
                <xsl:if test="report-requested/report/report-detail-header/clear-record = '1'">No</xsl:if>
              </td>

              <td  class="scr-bold-color">Outstanding Capias:</td>
              <td>
                <xsl:if test="report-requested/report/report-detail-header/capias = '0'">No</xsl:if>
                <xsl:if test="report-requested/report/report-detail-header/capias = '1'">Yes</xsl:if>
              </td>

              <td  class="scr-bold-color">Verified By DOB:</td>
              <td>
                <xsl:if test="report-requested/report/report-detail-header/verified-by-dob = '0'">No</xsl:if>
                <xsl:if test="report-requested/report/report-detail-header/verified-by-dob = '1'">Yes</xsl:if>
              </td>
            </tr>

            <tr>
              <td  class="scr-bold-color">Misdemeanors Found:</td>
              <td>
                <xsl:if test="report-requested/report/report-detail-header/misdemeanors = '0'">No</xsl:if>
                <xsl:if test="report-requested/report/report-detail-header/misdemeanors = '1'">Yes</xsl:if>
              </td>

              <td  class="scr-bold-color">Current Warrant:</td>
              <td>
                <xsl:if test="report-requested/report/report-detail-header/current-warrants = ''">No</xsl:if>
                <xsl:if test="report-requested/report/report-detail-header/current-warrants != ''">
                  <xsl:value-of select="report-requested/report/report-detail-header/current-warrants" />
                </xsl:if>
              </td>

              <td  class="scr-bold-color">Verified By Name:</td>
              <td>
                <xsl:if test="report-requested/report/report-detail-header/verified-by-name = '0'">No</xsl:if>
                <xsl:if test="report-requested/report/report-detail-header/verified-by-name = '1'">Yes</xsl:if>
              </td>
            </tr>

            <tr>
              <td class="scr-bold-color">Felonies Found:</td>
              <td>
                <xsl:if test="report-requested/report/report-detail-header/felonies = '0'">No</xsl:if>
                <xsl:if test="report-requested/report/report-detail-header/felonies = '1'">Yes</xsl:if>
              </td>

              <td  class="scr-bold-color">DUI's Found:</td>
              <td>
                <xsl:if test="report-requested/report/report-detail-header/dui = '0'">No</xsl:if>
                <xsl:if test="report-requested/report/report-detail-header/dui = '1'">Yes</xsl:if>
              </td>

              <td  class="scr-bold-color">Verified By SSN:</td>
              <td>
                <xsl:if test="report-requested/report/report-detail-header/verified-by-ssn = '0'">No</xsl:if>
                <xsl:if test="report-requested/report/report-detail-header/verified-by-ssn = '1'">Yes</xsl:if>
              </td>
            </tr>

            <tr>
              <td  class="scr-bold-color">Aliases Found:</td>
              <td>
                <xsl:if test="report-requested/report/report-detail-header/alias-found = '0'">No</xsl:if>
                <xsl:if test="report-requested/report/report-detail-header/alias-found = '1'">Yes</xsl:if>
              </td>

              <td class="scr-bold-color">Aliasas:</td>
              <td>

              </td>

              <td></td>
              <td></td>
            </tr>

            <tr>
              <td class="scr-bold-color">Other Criminal:</td>
              <td>

              </td>

              <td class="scr-bold-color">Other Criminal Detail:</td>
              <td>

              </td>

              <td></td>
              <td></td>
            </tr>

            <tr>
              <td class="scr-bold-color">Verified By Other:</td>
              <td></td>
              <td class="scr-bold-color">Verified By Other Detail:</td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </xsl:otherwise>
        </xsl:choose>
      </table>
      <table id="peopleTable"  class="people-search" width="100%">
        <thead>
          <tr class="header-scr">
            <td align="left" colspan="4" style="padding-left: 1.5%; padding-top: 6px; padding-bottom: 8px;">DETAILS</td>
          </tr>
        </thead>
        <tbody>
          <xsl:for-each select="report-requested/report/report-detail-header/report-incident">

            <tr>
              <td class="scr-bold-color">Charges</td>
              <td colspan="3" style="color: red;">
                <xsl:value-of select="charges" />
              </td>
            </tr>
            <tr>
              <td class="scr-bold-color">Disposition Date</td>
              <td>
                <xsl:value-of select="disposition-date" />
              </td>
              <td class="scr-bold-color">Disposition</td>
              <td>
                <xsl:value-of select="disposition" />
              </td>
            </tr>
            <tr>
              <td class="scr-bold-color">File Date</td>
              <td>
                <xsl:value-of select="file-date" />
              </td>
              <td class="scr-bold-color">Case #</td>
              <td>
                <xsl:value-of select="case-number" />
              </td>
            </tr>
            <tr>
              <td class="scr-bold-color">Sentence Date</td>
              <td>
                <xsl:value-of select="sentence-date" />
              </td>
              <td class="scr-bold-color">Sentence</td>
              <td>
                <xsl:value-of select="sentence" />
              </td>
            </tr>
            <tr>
              <td  class="scr-bold-color">Comments</td>
              <td colspan="3">
                <xsl:value-of select="additional-comments" />
              </td>
            </tr>

          </xsl:for-each>
        </tbody>
      </table>
    </div>
  </xsl:template>
</xsl:stylesheet>
