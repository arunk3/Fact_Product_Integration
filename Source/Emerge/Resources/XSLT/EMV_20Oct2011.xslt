﻿<?xml version="1.0" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output indent="yes" method="html"/>
  <xsl:template match="/">
    <html>
      <head>
        <style type="text/css">
          body{font-family: Lucida Grande,Arial,Helvetica,sans-serif;font-size: 14px;font-weight: normal;}

          .borders {border-left:1px solid #000000;
          border-right:1px solid #000000;
          border-top:1px solid #000000;
          border-bottom:1px solid #000000;}

          .tbl{Border:1px Solid #000;font-size:16pt;font-weight:bold;}
          .titletxt{font-size:12pt;font-weight:bold;}
        </style>

      </head>
      <body bgcolor="#ffffff">
        <center>
          <table width="100%" border="0">



            <tr>
              <td align="center"  colspan="2" width="670">
                <table width="99%" class="tbl" bgcolor="#efefef">
                  <tr>
                    <td align="center">
                      Summary
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td align="center"  colspan="2" width="670">
                <table width="99%" style="Border:1px solid #000;">
                  <tr>
                    <td align="left">
                      Report Type
                    </td>
                    <td>
                      Description
                    </td>
                  </tr>
                  <tr>
                    <td align="left" style="width:50%;">
                      Employment Verification
                    </td>
                    <td>
                      <!--<xsl:value-of select="BackgroundReports/BackgroundReportPackage/ScreeningsSummary/ExecutiveSummary/Employment/Summary"/>-->
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td style="height:10px" colspan="2"></td>
            </tr>

            <tr >
              <td align="center"  colspan="2" width="670">
                <table width="99%" class="tbl" bgcolor="#efefef">
                  <tr>
                    <td align="center">
                      CONFIDENTIAL
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td style="height:10px" colspan="2"></td>
            </tr>
            <tr>
              <td align="center" colspan="2" >
                <table width="99%" style="Border:1px solid #000;">
                  <tr>
                    <td align="left" class="titletxt">
                      Background Verification Report
                    </td>
                    <td>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td style="height:10px" colspan="2"></td>
            </tr>
            <xsl:for-each select="BackgroundReports/BackgroundReportPackage">
              <tr>
                <td colspan="2" align="center">
                  <xsl:apply-templates select="BackgroundReports/BackgroundReportPackage/ScreeningStatus"/>
                </td>
              </tr>
              <tr>
                <td colspan="2" align="center">
                  <xsl:apply-templates select="BackgroundReports/BackgroundReportPackage/ScreeningsSummary"/>
                </td>
              </tr>
              <tr>
                <td colspan="2">
                  <xsl:apply-templates/>
                </td>
              </tr>
              <tr>
                <td align="center" colspan="2" >
                  <span style="color:red;font-weight:600;font-size:14px;">**** End Of Report ****</span>
                </td>
              </tr>
              <tr>
                <td style="height:10px" colspan="2">
                  <hr style="border:solid 1px transparent;" />
                </td>
              </tr>
            </xsl:for-each>
          </table>
        </center>
      </body>

    </html>

  </xsl:template>

  <!--for Screening Status details-->
  <xsl:template match="BackgroundReports/BackgroundReportPackage/ScreeningStatus">
  </xsl:template>

  <!--for personal details-->
  <xsl:template match="BackgroundReports/BackgroundReportPackage/ScreeningsSummary">
    <div style="border: 1px solid rgb(0, 0, 0);width:98%;padding:5px;margin-left:5px;">
      <table border="0" width="60%" cellpadding="0" cellspacing="0">
        <tr>
          <td align="left">
            <xsl:text xml:space="preserve">&#x20;&#x20;&#x20;</xsl:text>  Requestor :
          </td>
          <td align="left">
            <xsl:apply-templates select="PersonalData/RequestIdData/Requestor"/>
          </td>
          <td align="right">
            <xsl:text xml:space="preserve">&#x20;&#x20;&#x20;</xsl:text> Position Applied :
          </td>
          <td align="left">
            <xsl:text xml:space="preserve">&#x20;&#x20;&#x20;</xsl:text>
            <xsl:apply-templates select="PersonalData/RequestIdData/Reference2"/>
          </td>
        </tr>
        <tr>
          <td align="left">
            <xsl:text xml:space="preserve">&#x20;&#x20;&#x20;</xsl:text> Subject :
          </td>
          <td align="left">
            <xsl:apply-templates select="PersonalData/PersonName/GivenName"/>
            <xsl:text xml:space="preserve">&#x20;&#x20;&#x20;</xsl:text>
            <xsl:apply-templates select="PersonalData/PersonName/MiddleName"/>
            <xsl:text xml:space="preserve">&#x20;&#x20;&#x20;</xsl:text>
            <xsl:apply-templates select="PersonalData/PersonName/FamilyName"/>
          </td>
          <td align="right">
            <xsl:text xml:space="preserve">&#x20;&#x20;&#x20;</xsl:text> SS# :
          </td>
          <td align="left">
            <xsl:text xml:space="preserve">&#x20;&#x20;&#x20;</xsl:text>
            <xsl:apply-templates select="PersonalData/DemographicDetail/Ssn"/>
          </td>
        </tr>
        <tr>
          <td align="left">
            <xsl:text xml:space="preserve">&#x20;&#x20;&#x20;</xsl:text> DOB :
          </td>
          <td align="left">
            <xsl:apply-templates select="PersonalData/DemographicDetail/DateOfBirth"/>
          </td>
        </tr>
      </table>
    </div>
  </xsl:template>


  <xsl:template match="BackgroundReports">
    <xsl:apply-templates select="BackgroundReportPackage/Screenings/Screening"/>

  </xsl:template>


  <!--for report search-->
  <xsl:template match="BackgroundReportPackage/Screenings/Screening">

    <xsl:choose>
      <xsl:when test="@type='employment'">
        <!--here we select the type of screening we have to search by attribute search-->

        <xsl:apply-templates select="ScreeningResults" mode="emp"/>
        <!--then select that screening (like function declaration)-->

      </xsl:when>

      <xsl:when test="@type='education'">
        <xsl:apply-templates select="ScreeningResults" mode="edu"/>
      </xsl:when>

      <xsl:when test="@type='license'">
        <xsl:apply-templates select="ScreeningResults" mode="licn"/>
      </xsl:when>

      <xsl:when test="@qualifier='Personal References'">
        <xsl:apply-templates select="ScreeningResults" mode="PRV"/>
      </xsl:when>


      <xsl:otherwise>

      </xsl:otherwise>

    </xsl:choose>
  </xsl:template>

  <!--here we fetch the data (like function calling)-->
  <!--for employment History-->
  <xsl:template match="ScreeningResults" mode="emp">
    <table width="100%" >
      <tr>
        <td colspan="2">
          <table width="100%">
            <tr bgcolor="#efefef">
              <td class="tbl" align="center" colspan="2">
                Employment History
              </td>
            </tr>
            <tr>
              <td align="left">
                Employer Name
              </td>
              <td align="left">
                <xsl:apply-templates select="EmployerOrgName"/>
              </td>
            </tr>
            <tr>
              <td align="left">
                Location
              </td>
              <td align="left">
                <xsl:apply-templates select="EmployerLocation"/>
              </td>
            </tr>
            <tr>
              <td align="left">
                Contact
              </td>
              <td align="left">
                <xsl:apply-templates select="Supervisor"/>
              </td>
            </tr>

            <xsl:choose>
              <xsl:when test="count(Text)='0'">
                <tr>
                  <td align="left">
                    Phone
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="Telephone"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Position
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="Position"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Researched By
                  </td>
                  <td align="left" style="width:60%;">
                    <xsl:apply-templates select="ResearchedBy"/> on
                    <xsl:apply-templates select="ResearchedByDate"/><br/>
                    <xsl:apply-templates select="ResearcherComments"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Employment Dates
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="DatesOfEmployment/ResearcherVerified/DateRange"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Starting Title
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="Title/ResearcherVerified/StartingTitle"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Ending Title
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="Title/ResearcherVerified/EndingTitle"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Starting Pay
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="Compensation/ApplicantReported/StartingCompensation"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Ending Pay
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="Compensation/ApplicantReported/EndingCompensation"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Starting Duties
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="Duties/ApplicantReported/StartingDuties"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Ending Duties
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="Duties/ApplicantReported/EndingDuties"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Reason For Leaving
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="ReasonForLeaving/ApplicantReported/Reason"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Rehire Eligibility
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="EligibleForRehire"/>
                  </td>
                </tr>
                <tr style="display:none;">
                  <td align="left">
                    Supervisor
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="Supervisor"/>
                  </td>
                </tr>

              </xsl:when>
              <xsl:otherwise>
                <tr>
                  <td align="left" colspan="2">
                    <pre>
                      <xsl:value-of select="Text"/>
                    </pre>
                  </td>
                </tr>
              </xsl:otherwise>
            </xsl:choose>

          </table>
        </td>
      </tr>
    </table>
  </xsl:template>

  <!--for education History-->
  <xsl:template match="ScreeningResults" mode="edu">
    <table width="98%" >
      <tr>
        <td colspan="2">
          <table width="98%">
            <tr bgcolor="#efefef">
              <td class="tbl" align="center" colspan="2">
                Education History
              </td>
            </tr>
            <tr>
              <td align="left">
                Institute Name
              </td>
              <td align="left">
                <xsl:apply-templates select="SchoolName"/>
              </td>
            </tr>
            <tr>
              <td align="left">
                Location
              </td>
              <td align="left">
                <xsl:apply-templates select="SchoolLocation"/>
              </td>
            </tr>

            <xsl:choose>
              <xsl:when test="count(Text)='0'">
                <tr>
                  <td align="left">
                    Phone
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="Telephone"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Dates Attended
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="DatesOfAttendance/ResearcherVerified/DateRange"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Major
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="Major/ResearcherVerified/Major"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Degree/Diploma
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="DegreeName/ResearcherVerified/DegreeName"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Degr./Dipl. Received
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="DegreeDate/ResearcherVerified/DegreeDate"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Grades/GPA
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="Grades/ResearcherVerified/Grades"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Credits Accumulated
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="Credits/ResearcherVerified/Credits"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Confirmed By
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="ConfirmedByName"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Researched By
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="ResearchedBy"/>
                  </td>
                </tr>
              </xsl:when>
              <xsl:otherwise>
                <tr>
                  <td align="left" colspan="2">
                    <pre>
                      <xsl:value-of select="Text"/>
                    </pre>
                  </td>
                </tr>
              </xsl:otherwise>
            </xsl:choose>

          </table>
        </td>

      </tr>
    </table>
  </xsl:template>

  <!--for License History-->
  <xsl:template match="ScreeningResults" mode="licn">
    <table width="100%" >
      <tr>
        <td colspan="2">
          <table width="100%">
            <tr bgcolor="#efefef">
              <td class="tbl" align="center" colspan="2">
                License History
              </td>
            </tr>
            <tr>
              <td align="left">
                License Name
              </td>
              <td align="left">
                <xsl:apply-templates select="LicenseName"/>
              </td>
            </tr>
            <tr>
              <td align="left">
                State
              </td>
              <td align="left">
                <xsl:apply-templates select="LicensingState"/>
              </td>
            </tr>

            <xsl:choose>
              <xsl:when test="count(Text)='0'">
                <tr>
                  <td align="left">
                    LicenseNumber
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="LicenseNumber"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    IssueDate
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="IssueDate"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Licensing Agency
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="LicensingAgency"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Licensing Agency Location
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="LicensingAgencyLocation"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Confirmed License Name
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="ConfirmedLicenseName"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Starting Pay
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="Compensation/ApplicantReported/StartingCompensation"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Confirmed IssueDate
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="ConfirmedIssueDate"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Confirmed ExpireDate
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="ConfirmedExpireDate"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Confirmed By Name
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="ConfirmedByName"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Confirmed By Title
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="ConfirmedByTitle"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Researched By
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="ResearchedBy"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Researched Date
                  </td>
                  <td align="left">
                    <xsl:apply-templates select="ResearchedByDate"/>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    Researched Comments
                  </td>
                  <td align="left" style="width:60%;">
                    <xsl:apply-templates select="ResearcherComments"/>
                  </td>
                </tr>
              </xsl:when>
              <xsl:otherwise>
                <tr>
                  <td align="left" colspan="2">
                    <pre>
                      <xsl:value-of select="Text"/>
                    </pre>
                  </td>
                </tr>
              </xsl:otherwise>
            </xsl:choose>

          </table>
        </td>
      </tr>
    </table>
  </xsl:template>

  <!--for Personal Reference-->
  <xsl:template match="ScreeningResults" mode="PRV">
    <table width="100%" >
      <tr>
        <td colspan="2">
          <table width="100%">
            <tr bgcolor="#efefef">
              <td class="tbl" align="center" colspan="2">
                Personal References
              </td>
            </tr>
            <tr>
              <td colspan="2" align="left">

                <pre>

                  <xsl:value-of select="Text"/>

                </pre>

              </td>
            </tr>

          </table>
        </td>
      </tr>
    </table>
  </xsl:template>

</xsl:stylesheet>