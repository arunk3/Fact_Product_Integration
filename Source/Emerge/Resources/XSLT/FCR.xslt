﻿<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:a="http://ns.hr-xml.org/2004-08-02">
  <xsl:param name="admin" select="defaultstring" />
  <xsl:output method="html"/>

 

  <xsl:template match="/">
    <div class="people-search-wrap_fcr btn-view">
    <div class="sectionsummary">
      <!--<br />
      <h3>Name:</h3>
      <h4>
        <xsl:value-of select="concat(a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:GivenName,', ',a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:FamilyName)" />
      </h4>
      <br/>-->
    </div>
    <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:ResultStatus = 'Hit'">


      <table id="peopleTable" class="people-search" width="100%">
        <tr class="fcr_header_data">
          <td colspan="6"  align="left" style="padding-top:6px; padding-bottom:8px;">
            <span >
              Search Information
            </span>
          </td>

        </tr>
        <tr>
          <td  ><!--i--></td>
          <td colspan="4">
            <!--Records Found. Please contact our Support Team for complete details or click on Emerge Assist.-->
            "Hits Were Found". <br/> <br/>
            All Federal Records must be confirmed by support. Please press the Emerge Review button below and our support team will get back to you shortly.
          </td>
          <td class="end">
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </td>


        </tr>


      </table>

   
    </xsl:if>


    <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:ResultStatus = 'Clear'">


      <table id="peopleTable"  width="100%">
        <tr class="fcr_header_data">
          <td colspan="6"  align="left" style="padding-left: 1.5%; padding-top:6px; padding-bottom:8px;">
            <span >
              Search Information
            </span>
          </td>

        </tr>
        <tr>
          <td ><!--i--></td>
          <td colspan="4" align="center" style="color: red;font-size: 14px;">
           
            No Records Found.
          </td>
          <td class="end">
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </td>


        </tr>


      </table>
      
  
    </xsl:if>
    <div style="width:80%;padding-left:100px;" >
      
      <span align="center">
        <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text"/>
        </span>

    </div>



    </div>

  </xsl:template>
</xsl:stylesheet>