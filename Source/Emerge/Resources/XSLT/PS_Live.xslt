<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:a="http://ns.hr-xml.org/2004-08-02">
  <xsl:param name="admin" select="defaultstring" />
  <xsl:output method="html"/>
  <xsl:template match="/">

    <xsl:if test="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:SSNStatus)&gt;'0'">
      <div class="people-search-wrap" style="padding-top:0" >
      <table class="people-search1" width="100%" style="margin-bottom:10px;">
        <tr>
          <td style="width:14px;"></td>
          <!--<td style="width:25%; ">
            <div class="summry-row clearfix" style="width:100%;">
              <div class="txt-sumry"> Summary </div>
            </div>
          </td>-->
          <td>
            <div class="txt-detials" style="padding-left: 32px;float:left;">
              <ul>
                <li> SSN ISSUANCE :</li>
                <li> DEATH INDEX :</li>
              </ul>
              <ul>
                <li  style="text-align:left;">
                  <strong>
                    <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:SSNStatus"/>
                  </strong>
                </li>
                <li style="text-align:left;">
                  <strong>
                    <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:DateOfDeath"/>
                  </strong>
                </li>
              </ul>
            </div>

          </td>
        </tr>
      </table>
      <table class="people-search" width="100%">
        <tr class="search-heading">
         
          <td  class="width_50px" style="text-align:right;">
            <span class="countr" style="background-color: rgb(0,120,189);color: white; border:none;">
              <span class="nameTotalCount">
                <xsl:variable name="unique-listName" select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PersonName/a:FormattedName[not(.=following::a:FormattedName)]" />
                <xsl:value-of select="count($unique-listName)"/>
              </span>
            </span>
          </td>
          <td>
            <span class="PS_SSN_DEATH_TOP">
              REPORTED NAMES
              
            </span>
          </td>
          <td style="text-align:right;">
            <span class="countr" style="background-color: rgb(0,120,189);color: white; border:none">
              <xsl:variable name="unique-listDates" select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:DemographicDetail/a:DateOfBirth[not(.=following::a:DateOfBirth or string-length(.) != '10' )]" />
              <xsl:value-of select="count($unique-listDates)"/>
            </span>
          </td>
          <td>
            <span class="PS_SSN_DEATH_TOP">
              REPORTED DATE OF BIRTH
            </span>
          </td>
          <td style="text-align:right;">
            <span class="countr" style="background-color: rgb(0,120,189);color: white; border:none">
              <xsl:variable name="unique-ListCounty" select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PostalAddress/a:County[not(.=following::a:County)]" />

              <xsl:value-of select="count($unique-ListCounty)"/>
            </span>
          </td>
          <td>
            <span class="PS_SSN_DEATH_TOP">
              REPORTED COUNTIES  
              <xsl:variable name="unique-ListCounty" select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PostalAddress/a:County[not(.=following::a:County)]" />

           (<xsl:value-of select="count($unique-ListCounty)"/>)
              <xsl:if test="count($unique-ListCounty) &gt;'0'">
              <a  class="runallcounties" style="color:red;margin-left:1%;cursor:pointer;" >Run All Counties</a>
              &#160;<img src="../../Content/themes/base/images/ajax-loader3.gif" class="psdisplay"></img>
            </xsl:if>
            </span >
          </td>
        </tr>



        <tr class="sparater-td bg-gray">
         
          <td></td>
          <td>



            <xsl:variable name="unique-listName" select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PersonName/a:FormattedName[not(.=following::a:FormattedName)]" />
            <xsl:for-each select="$unique-listName">
              <span class="up_unique">
                <span class="unique">
                  <xsl:value-of select="." />
                </span>

              </span>

            </xsl:for-each>

            <!--<xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PersonName" >
            <span class="PS_SSN_DEATH_BOTTOM">
              <xsl:if test="a:FamilyName!='' and a:GivenName !=''">
              <xsl:value-of select="a:FamilyName" />, <xsl:value-of select="a:GivenName" /><br/>
              </xsl:if>
            </span>
          </xsl:for-each>-->
          </td>
          <td></td>
          <td>
            <xsl:variable name="unique-listDates" select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:DemographicDetail/a:DateOfBirth[not(.=following::a:DateOfBirth or string-length(.) != '10' )]" />
            <!--<span class="PS_SSN_DEATH_TOP">
              REPORTED BIRTHDATES ( <xsl:value-of select="count($unique-listDates)"/> )
            </span>-->
            <br/>
            <xsl:for-each select="$unique-listDates">
              <span class="ultimatecls">
                <span class="ultimateclshelp">
                  <xsl:value-of select="concat(substring(.,6,2),'/',substring(.,9,2),'/',substring(.,1,4))" />
                </span>
              </span>
              <br/>
            </xsl:for-each>


            <!--<xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:DemographicDetail" >
            <span class="PS_SSN_DEATH_BOTTOM">
              <xsl:value-of select="a:DateOfBirth"/>
              <br/>
            </span>
          </xsl:for-each>-->
          </td>
          <td></td>
          <td>

            <!--<xsl:variable name="unique-ListCounty" select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PostalAddress/a:County[not(.=following::a:County)]" />-->



            
            <xsl:variable name="unique-list" select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PostalAddress" />
            <xsl:for-each select="$unique-list">
              <xsl:if test="a:County[not(.=following::a:County)]">
                <span class="countystate">
                  <xsl:value-of select="a:County"/>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="a:Region"/>
                </span>
                <br/>
              </xsl:if>
            </xsl:for-each>
          </td>
        </tr>
</table>
        <div class="sectionsummary" style=" margin:10px 0;">
          <h3>
            <!--(<xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/@recordCount" />)-->
            (<xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData)"/>) Results PEOPLE SEARCH
          </h3>
          <br />
          <h3>Name:</h3>
          <h4>
            <span class="capname" style="font-size:14px;">
              <xsl:value-of select="concat(a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:GivenName,', ',a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:FamilyName)" />
            </span>
          </h4>
          <br/>
        </div>

        <table class="people-search" width="100%">

        <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/@recordCount != '0'">
          
          
            <tr>
              <td class="width_50px"></td>
              <th style="width:26%;">
                <span class="PS_SSN_DEATH_TOP"><u>PERSONAL INFO</u></span>
              </th>
               
              <th style="width:29.5%;">
                <span class="PS_SSN_DEATH_TOP">
                  <u>ADDRESS INFO</u>
                </span>
              </th>
             
              <th >
                <span class="PS_SSN_DEATH_TOP">
                  <u>PHONE INFO</u>
                </span>
              </th>
            
            </tr>
        
          <xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData">

            <xsl:choose>
              <xsl:when test="(position() mod 2 = 0)">
                <tr class="sparater-td">
                  <td style="text-align:center; vertical-align: top;">
                    <span class="blue-no">
                      <span>
                      <xsl:value-of select="position()" />
                      </span>
                    </span>
                  </td>
                  <td style="background-color:#fff;">
                    <xsl:value-of select="a:PersonName/a:FamilyName" />, <xsl:value-of select="a:PersonName/a:GivenName" /><br/>
                    SSN : xxx-xx-<xsl:value-of select="substring(a:DemographicDetail/a:GovernmentId,6,4)" />

                    <br/>
                    <span class="ultimatecls">
                      <!--string-length(.) != '10'-->
                      <!--a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:DemographicDetail/a:DateOfBirth[not(.=following::a:DateOfBirth or string-length(.) != '10' )]-->
                      <xsl:if test="a:DemographicDetail/a:DateOfBirth[not(string-length(.) != '10' )]">
                        DOB : <span class="ultimateclshelp">
                          <xsl:value-of select="concat(substring(a:DemographicDetail/a:DateOfBirth,6,2),'/',substring(a:DemographicDetail/a:DateOfBirth,9,2),'/',substring(a:DemographicDetail/a:DateOfBirth,1,4))" />
                        </span>
                        <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
                        Age : <xsl:value-of select="a:DemographicDetail/a:Age" />
                      </xsl:if>
                    </span>
                  </td>
                  
                  <td style="background-color:#fff;">
                    <xsl:value-of select="a:PostalAddress/a:DeliveryAddress/a:AddressLine" />
                    <xsl:text> </xsl:text>
                    <br />

                    <xsl:value-of select="a:PostalAddress/a:Municipality" />
                    <xsl:text>, </xsl:text>
                    <xsl:value-of select="a:PostalAddress/a:Region" />
                    <xsl:text> </xsl:text>
                    <br />
                    <xsl:value-of select="a:PostalAddress/a:County" />
                    <xsl:text> </xsl:text>
                    <br />

                    <xsl:value-of select="a:PostalAddress/a:CountryCode" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="a:PostalAddress/a:PostalCode" />
                    <br/>
                    <xsl:value-of select="a:EffectiveDate/a:StartDate" />
                    <xsl:text disable-output-escaping="yes">&amp;nbsp;-&amp;nbsp;</xsl:text>
                    <xsl:value-of select="a:EffectiveDate/a:EndDate" />
                  </td>
                  
                  <td style="border-right: none;background-color:#fff;">
                    <xsl:choose>
                      <xsl:when test="count(a:ContactMethod)='0'">

                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:for-each select="a:ContactMethod">
                          <xsl:value-of select="a:Telephone/a:FormattedNumber"/>
                          <br/>
                        </xsl:for-each>
                      </xsl:otherwise>
                    </xsl:choose>
                  </td>

                  <!--</tr>-->
                </tr>
              </xsl:when>
              <xsl:otherwise>
                <tr class="sparater-td">
                  <td style="text-align:center;vertical-align: top;">
                    <span class="blue-no"  >
                      <span>
                      <xsl:value-of select="position()" />
                      </span>
                    </span>
                  </td>
                  <td >
                    <xsl:value-of select="a:PersonName/a:FamilyName" />, <xsl:value-of select="a:PersonName/a:GivenName" /><br/>
                    SSN : xxx-xx-<xsl:value-of select="substring(a:DemographicDetail/a:GovernmentId,6,4)" />
                    <br/>
                    <span class="ultimatecls">
                      <xsl:if test="a:DemographicDetail/a:DateOfBirth[not(string-length(.) != '10' )]">
                        DOB : <span class="ultimateclshelp">
                          <xsl:value-of select="concat(substring(a:DemographicDetail/a:DateOfBirth,6,2),'/',substring(a:DemographicDetail/a:DateOfBirth,9,2),'/',substring(a:DemographicDetail/a:DateOfBirth,1,4))" />
                        </span>
                        <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
                        Age : <xsl:value-of select="a:DemographicDetail/a:Age" />
                      </xsl:if>
                    </span>
                  </td>
                  
                  <td>
                    <xsl:value-of select="a:PostalAddress/a:DeliveryAddress/a:AddressLine" />
                    <xsl:text> </xsl:text>
                    <br />

                    <xsl:value-of select="a:PostalAddress/a:Municipality" />
                    <xsl:text>, </xsl:text>
                    <xsl:value-of select="a:PostalAddress/a:Region" />
                    <xsl:text> </xsl:text>
                    <br />
                    <xsl:value-of select="a:PostalAddress/a:County" />
                    <xsl:text> </xsl:text>
                    <br />

                    <xsl:value-of select="a:PostalAddress/a:CountryCode" />
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="a:PostalAddress/a:PostalCode" />
                    <br/>
                    <xsl:value-of select="a:EffectiveDate/a:StartDate" />
                    <xsl:text disable-output-escaping="yes">&amp;nbsp;-&amp;nbsp;</xsl:text>
                    <xsl:value-of select="a:EffectiveDate/a:EndDate" />
                  </td>
                  
                  <td style="border-right: none;">
                    <xsl:choose>
                      <xsl:when test="count(a:ContactMethod)='0'">

                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:for-each select="a:ContactMethod">
                          <xsl:value-of select="a:Telephone/a:FormattedNumber"/>
                          <br/>
                        </xsl:for-each>
                      </xsl:otherwise>
                    </xsl:choose>
                  </td>

                </tr>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>

        </xsl:if>
      </table>
      </div>
    </xsl:if>

    
    <xsl:if test="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile) ='0'">
      <table cellpadding="2" cellspacing="0" style="margin: 0px border:none;" align="center" class="PS_main">
        <thead>
          <tr>
            <th class="begin"></th>
            <th colspan="5">Search Information</th>
            <th class="end"></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="begin"></td>
            <td colspan="5">
              <xsl:if test="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:AdditionalItems/a:Text) &gt;'0'">
                <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:AdditionalItems/a:Text" />
                <!--Address trace service error: Input Error:Too Many Hits.Please refine your search.-->
              </xsl:if>
              <xsl:if test="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:AdditionalItems/a:Text) ='0'">
                No Records Found.
              </xsl:if>
            </td>
            <td class="end">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
        </tbody>
        <tfoot>
          <td class="begin"></td>
          <td colspan="5"></td>
          <td class="end"></td>
        </tfoot>
      </table>
    </xsl:if>
    <!--<xsl:if test="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile) &gt;'0'">
   



    <div class="sectionsummary">
      <h3>
        (<xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/@recordCount" />) Results PEOPLE SEARCH
      </h3>
      <br />
      <h3>Name:</h3>
      <h4>
        <span class="capname"><xsl:value-of select="concat(a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:GivenName,', ',a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:FamilyName)" />
        </span>
      </h4>
      <br/>
    </div>

    <center>

      <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/@recordCount = '0'">
        <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
          <thead>
            <tr>
              <th class="begin"></th>
              <th colspan="5">Search Information</th>
              <th class="end"></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="begin">i</td>
              <td colspan="5">
                No Records Found.
              </td>
              <td class="end">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </tbody>
          <tfoot>
            <td class="begin"></td>
            <td colspan="5"></td>
            <td class="end"></td>
          </tfoot>
        </table>
      </xsl:if>

     
    </center>

    </xsl:if>-->
  </xsl:template>
</xsl:stylesheet>