﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text"/>
  <xsl:template match="/">
    <xsl:choose>
      <xsl:when test="contains(XML_INTERFACE/CREDITREPORT/HTMLREPORT, 'BODY ')">
        <xsl:value-of select="concat('&lt;body ', substring-after(XML_INTERFACE/CREDITREPORT/HTMLREPORT, 'BODY '))" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="concat('&lt;body ', substring-after(XML_INTERFACE/CREDITREPORT/HTMLREPORT, 'body '))" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>