<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="utf-8"/>

	<xsl:template match="/">
		<center>
			<table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
				<thead>
					<tr>
						<th class="begin"></th>
						<th colspan="5">Search Results</th>
						<th class="end"></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="begin">i</td>
						<td colspan="5">

			<xsl:value-of select="substring-after(XML_INTERFACE/CREDITREPORT/HTMLREPORT, 'body&gt;')" disable-output-escaping="yes" />

						</td>
						<td class="end"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
				</tbody>
				<tfoot>
					<td class="begin"></td>
					<td colspan="5"></td>
					<td class="end"></td>
				</tfoot>
			</table>

		</center>
		
	</xsl:template>
</xsl:stylesheet>