<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" encoding="utf-8"/>

    <xsl:template match="/">
        <center>

            <div class="sectionsummary">
                <h3>
                    (<xsl:value-of select="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/rows_returned" />) Results Bankruptcy Report
                </h3>
                <br />
            </div>

            <xsl:if test="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/rows_returned = '0'">
                <table id="peopleTable" class="reporttable" border="0" style="width:100%" cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                          <th colspan="8">Search Information</th>
                            <th class=" "></th>
                            <th class=" "></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class=" ">
                              <span class="blue-no" style="padding-top:5px;">
                                <xsl:value-of select="(position() )"/>
                              </span>
                            </td>
                            <td colspan="5"  width="98%">
                                No Records Found.
                            </td>
                            <td class=" ">
                                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                            </td>
                        </tr>
                    </tbody>
                   
                </table>
            </xsl:if>

            <xsl:if test="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/rows_returned != '0'">
                <table id="peopleTable" class="reporttable" border="0" cellpadding="0"  style="width:100%;" cellspacing="0">
                    <thead>
                        <tr>
                            <th class="begin"></th>
                            <th colspan="5">Search Results</th>
                            <th class="end"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class=" ">
                              <span class="blue-no" style="padding-top:5px;">
                                <xsl:value-of select="(position() )"/>
                              </span>
                            </td>
                            <td colspan="5"  width="98%">
                                
                                <xsl:value-of select="substring-after(XML_INTERFACE/CREDITREPORT/HTMLREPORT, '&#60;&#47;table&#62;')" disable-output-escaping="yes" />

                            </td>
                            <td class=" ">
                                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <td class="begin"></td>
                        <td colspan="5"></td>
                        <td class="end"></td>
                    </tfoot>
                </table>
            </xsl:if>

            <xsl:if test="XML_INTERFACE/CREDITREPORT/OBJECTS/BUREAUERROR = 'True' or XML_INTERFACE/CREDITREPORT/cp_xmldirect/general_error !='' ">
                <table id="peopleTable" class="reporttable" border="0" cellpadding="0"  style="width:100%;" cellspacing="0">
                    <thead>
                        <tr>
                            <th class="begin"></th>
                            <th colspan="5">Search Results</th>
                            <th class="end"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="begin">
                                <xsl:value-of select="(position() )"/>
                            </td>
                            <td colspan="5">

                                <xsl:value-of select="substring-after(XML_INTERFACE/CREDITREPORT/HTMLREPORT/MESSAGE, 'body&gt;')" disable-output-escaping="yes" />

                            </td>
                            <td class="end">
                                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <td class="begin"></td>
                        <td colspan="5"></td>
                        <td class="end"></td>
                    </tfoot>
                </table>
            </xsl:if>
        </center>
        <div class="sectionsummary">
            <h3 style="line-height: 1em;">
                Bankruptcy Records cover all bankruptcy districts in the<br />
                United States including voluntarily or involuntarily Chapter<br />
                7, 11, 12, and 13 petition filings.
            </h3>
        </div>
    </xsl:template>
</xsl:stylesheet>