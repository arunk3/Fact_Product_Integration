﻿<?xml version="1.0" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output indent="yes" method="html"/>
  <xsl:template match="/">
    <html>
      <head>
        <style type="text/css">
          body{font-family: Lucida Grande,Arial,Helvetica,sans-serif;font-size: 14px;font-weight: normal;}

          .borders {border-left:1px solid #000000;
          border-right:1px solid #000000;
          border-top:1px solid #000000;
          border-bottom:1px solid #000000;}

          .tbl{Border:1px Solid #000;font-size:16pt;font-weight:bold;}
          .titletxt{font-size:12pt;font-weight:bold;}
        </style>
      </head>
      <body bgcolor="#ffffff">
        <div class="people-search-wrap">
          <table width="100%" border="0">
            <tr>
              <td>
                <table  width="100%" border="0" class="people-search">
                  <tr class="search-heading">
                    <td class="width_10"></td>
                    <td style="text-align:left;font-size:20px;font-weight: bold;">
                      CONFIDENTIAL
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td style="height:5px" colspan="2">
              </td>
            </tr>
            <tr>
              <td>
                <table width="100%" style="font-size: 14pt">
                  <tr>
                     <td class="width_10"></td>
                    <td style="text-align:left" >
                      <b> Background Verification Report</b>
                    </td>
                    <td></td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td style="height:5px" colspan="2">
              </td>
            </tr>
            <tr>
              <td>
                <table width="100%" >
                  <tr>
                    <td class="width_10"></td>
                    <td style="text-align:left;width:50%; font-size:14px;">
                      Requestor:&#160;<xsl:value-of select="ScreeningResults/orderInfo/requester_name"/>
                    </td>

                    <td style="text-align:left;width:50%; font-size:14px;">
                      Subject:&#160;
                      <xsl:value-of select="ScreeningResults/completeOrder/subject/name_last"/>&#160;
                      <xsl:value-of select="ScreeningResults/completeOrder/subject/name_first"/>
                    </td>
                  </tr>
                  <tr>
                    <td class="width_10"></td>
                    <td style="text-align:left;width:50%; font-size:14px;">
                      Position Applied:
                    </td>
                    <!--</tr>
									<tr>
                    <td class="width_10"></td>-->
                    <td style="text-align:left;width:50%; font-size:14px;">
                      DOB:&#160;
                      <xsl:value-of select="ScreeningResults/completeOrder/subject/dob"/>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td style="height:5px" colspan="2">
              </td>
            </tr>
            <xsl:for-each select="ScreeningResults/completeOrder/subOrder">
              <xsl:variable name="counter" select="position()-1" />
              <tr>
                <td>
                  <table class="tbl" width="100%" border="0">
                    <tr bgcolor="#efefef">
                      <td style="text-align:center">
                        License History #<xsl:value-of select="$counter + 1" />
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td>
                  <table>
                    <tr>
                      <td>
                      </td>
                      <td>
                        <u>Provided by Subject</u>
                      </td>
                      <td>
                        <u>Information Verified</u>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Organization:&#160;
                      </td>
                      <td>
                        <xsl:value-of select="organization_name"/>
                      </td>
                      <td>
                        <xsl:value-of select="verified_organization_name"/>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Description:&#160;
                      </td>
                      <td>
                        <xsl:value-of select="description"/>
                      </td>
                      <td>
                        <xsl:value-of select="''"/>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        License Location:&#160;
                      </td>
                      <td>
                        <xsl:value-of select="license_location"/>
                      </td>
                      <td>
                        <xsl:value-of select="verified_license_location"/>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        License State:&#160;
                      </td>
                      <td>
                        <xsl:value-of select="state"/>
                      </td>
                      <td>
                        <xsl:value-of select="verified_state"/>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        License Number:&#160;
                      </td>
                      <td>
                        <xsl:value-of select="license_number"/>
                      </td>
                      <td>
                        <xsl:value-of select="verified_license_number"/>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Date Received:&#160;
                      </td>
                      <td>
                        <xsl:value-of select="date_received"/>
                      </td>
                      <td>
                        <xsl:value-of select="verified_date_received"/>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Expiration Date:&#160;
                      </td>
                      <td>
                        <xsl:value-of select="expiration_date"/>
                      </td>
                      <td>
                        <xsl:value-of select="verified_expiration_date"/>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        License Status:&#160;
                      </td>
                      <td>
                        <xsl:value-of select="license_status"/>
                      </td>
                      <td>
                        <xsl:value-of select="verified_license_status"/>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Order Comments:&#160;
                      </td>
                      <td>
                        <xsl:value-of select="comments"/>
                      </td>
                      <td>
                        <xsl:value-of select="''"/>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Order Comments:&#160;
                      </td>
                      <td>
                        <xsl:value-of select="''"/>
                      </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>
                        Person Interviewed:
                      </td>
                      <td>
                        <xsl:value-of select="verified_verifier"/>
                      </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>
                        Researched By:
                      </td>
                      <td>
                        <xsl:value-of select="verified_verifier"/>
                      </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>
                        Research Comments:&#160;
                      </td>
                      <td>
                        <xsl:choose>
                          <xsl:when test="comments!=''">
                            <xsl:value-of select="comments"/>
                          </xsl:when>
                          <xsl:otherwise>
                            NO INFORMATION PROVIDED
                          </xsl:otherwise>
                        </xsl:choose>

                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </xsl:for-each>
            <tr>
              <td>

                <xsl:choose>
                  <xsl:when test="ScreeningResults/completeOrder/subOrder/@filledCode ='clear'">
                    <p style="text-align: justify;font-size:12px;">No Record Found.</p>
                  </xsl:when>
                  <xsl:otherwise>

                  </xsl:otherwise>
                </xsl:choose>
              </td>
            </tr>
            <tr>
              <td align="center" colspan="2" >
                <span>**** End Of Report ****</span>
              </td>
            </tr>

          </table>


        </div >

      </body>
    </html>

  </xsl:template>

</xsl:stylesheet>