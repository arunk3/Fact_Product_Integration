<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:a="http://ns.hr-xml.org/2004-08-02">
    <xsl:output method="html" encoding="iso-8859-1" />
    <xsl:template match="/">

        <center>
            <div class="sectionsummary">
                <h3>
                    (<xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)" />) Results CRIMINAL REPORT.<br />
                </h3>
            </div>

            <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th class="begin"></th>
                        <th colspan="5">Search Information</th>
                        <th class="end"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="begin">
                            <xsl:value-of select="(position())" />
                        </td>
                        <td colspan="5">
                            <h4>Name:</h4>
                            <h4>
                                <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:GivenName"/>
                                <xsl:text xml:space="preserve">  </xsl:text>
                                <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:FamilyName"/>
                            </h4>
                            <br />
                            <h4>DOB:</h4>
                            <h4>
                                <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:DemographicDetail/a:DateOfBirth"/>
                            </h4>
                        </td>
                        <td class="end">
                            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <td class="begin"></td>
                    <td colspan="5"></td>
                    <td class="end"></td>
                </tfoot>
            </table>
            
            <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th class="begin"></th>
                        <th colspan="5">Court Results</th>
                        <th class="end"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="begin">

                        </td>
                        <td colspan="5">
                            <div style="width: 90%; margin: 0 auto; text-align: left">
                                <div style="width:588px; text-align:left">
                                    <div style="font-size:11px;" id="report_ncr1">
                                        <pre>
                                        <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text">
                                            <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text" disable-output-escaping="yes"/>
                                        </xsl:if>
                                        <xsl:if test="not(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text)">
                                            <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus = 'Cancelled'">
                                                <p>
                                                    There was an error running this report.<br /> Please contact our customer support team to quickly resolve this issue.
                                                </p>
                                            </xsl:if>
                                            <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus != 'Cancelled'">
                                                <p>*** Clear ***</p>
                                            </xsl:if>
                                            <p></p>
                                        </xsl:if>
                                        </pre>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="end">
                            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <td class="begin"></td>
                    <td colspan="5"></td>
                    <td class="end"></td>
                </tfoot>
            </table>
        </center>

        <div class="sectionsummary">
            <h3 style="line-height: 1em;">

                Data Sources Searched:<br/>
                Court Records, Department of Corrections, Administrative Office of Courts,<br/>
                National Sex Offender Registry, OFAC, OIG<br/>
                For a complete list of coverage, please visit<br/>
                <a href="http://emerge.intelifi.com/coverage.pdf" target="_blank">US Criminal Rapid Search Coverage Guide</a>

            </h3>
        </div>
    </xsl:template>
</xsl:stylesheet>