<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="iso-8859-1"/>
	<xsl:template match="/">
		<div class="sectionsummary">
		<h3>
			Request ID: 
        
        <xsl:variable name="request_id" select="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/request_id"/>
      
        <xsl:choose>
            <xsl:when test="$request_id = ''">
                 <xsl:text> No Records </xsl:text>                        
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/request_id" />                        
            </xsl:otherwise>
        </xsl:choose>
      
      </h3>
		</div>

		<center>

            <xsl:if test="XML_INTERFACE/CREDITREPORT/OBJECTS/BUREAUERROR = 'True' or XML_INTERFACE/CREDITREPORT/cp_xmldirect/general_error !='' ">
                <table id="peopleTable" class="reporttable" border="0" cellpadding="0"  style="width:100%;" cellspacing="0">
                    <thead>
                        <tr>
                            <th class="begin"></th>
                            <th colspan="5">Search Results</th>
                            <th class="end"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="begin">
                                <xsl:value-of select="(position() )"/>
                            </td>
                            <td colspan="5">

                                <xsl:value-of select="substring-after(XML_INTERFACE/CREDITREPORT/HTMLREPORT/MESSAGE, 'body&gt;')" disable-output-escaping="yes" />

                            </td>
                            <td class="end">
                                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <td class="begin"></td>
                        <td colspan="5"></td>
                        <td class="end"></td>
                    </tfoot>
                </table>
            </xsl:if>
		
		<table class="reporttable" id="table" border="0" cellpadding="0" cellspacing="0">
			<thead>
				<tr>
					<th class="begin"></th>
					<th colspan="100%">Search Information</th>
					<th class="end"></th>
				</tr>
			</thead>
			<tbody>
				<xsl:variable name="counter" select="0" />
				<xsl:for-each select="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/response_data/response_row">
					<xsl:if test="$counter + 1" />
					<tr>
						<td class="begin"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
						<td colspan="100%" style="border-right: none;">
							<table>
								<tr>
									<td colspan="100%" class="recordHeader">
										Record #<xsl:value-of select="$counter" />
									</td>
								</tr>
								<tr>
									<th>Score:</th><td><xsl:value-of select="score" /></td>
								</tr>
								<tr>
									<th>File Name:</th><td><xsl:value-of select="filename" /></td>
									<th>File Date:</th><td><xsl:value-of select="filedate" /></td>
								</tr>
								<tr>
									<th>Entity Name:</th><td><xsl:value-of select="entityname" /></td>
									<th>Best Name:</th><td><xsl:value-of select="bestname" /></td>
								</tr>
								<tr>
									<th>Listing:</th>
									<td colspan="3"><xsl:value-of select="listing" /></td>
								</tr>
							</table>
						</td>
						<td class="end"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td>
					</tr>
				</xsl:for-each>

				<tr><td class="begin"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colspan="100%" style="border-right: none;">Australian Department of Foreign Affairs and Trade (DFAT)</td><td class="end"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				<tr><td class="begin"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colspan="100%" style="border-right: none;">Bank of England Consolidated List of Financial Sanctions</td><td class="end"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				<tr><td class="begin"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colspan="100%" style="border-right: none;">Bureau of Industry and Security</td><td class="end"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				<tr><td class="begin"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colspan="100%" style="border-right: none;">OSFI Terrorism Financing</td><td class="end"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				<tr><td class="begin"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colspan="100%" style="border-right: none;">DTC Debarred parties</td><td class="end"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				<tr><td class="begin"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colspan="100%" style="border-right: none;">European Union Consolidated List</td><td class="end"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				<tr><td class="begin"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colspan="100%" style="border-right: none;">FBI Hijack Suspects</td><td class="end"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				<tr><td class="begin"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colspan="100%" style="border-right: none;">FBI Most Wanted</td><td class="end"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				<tr><td class="begin"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colspan="100%" style="border-right: none;">FBI Most Wanted Terrorists</td><td class="end"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				<tr><td class="begin"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colspan="100%" style="border-right: none;">FBI Seeking Information</td><td class="end"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				<tr><td class="begin"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colspan="100%" style="border-right: none;">FBI Top Ten Most Wanted</td><td class="end"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				<tr><td class="begin"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colspan="100%" style="border-right: none;">HKMA</td><td class="end"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				<tr><td class="begin"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colspan="100%" style="border-right: none;">Interpol Most Wanted</td><td class="end"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				<tr><td class="begin"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colspan="100%" style="border-right: none;">MAS</td><td class="end"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				<tr><td class="begin"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colspan="100%" style="border-right: none;">Non-cooperative Countries and Territories</td><td class="end"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				<tr><td class="begin"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colspan="100%" style="border-right: none;">Non-proliferation</td><td class="end"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				<tr><td class="begin"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colspan="100%" style="border-right: none;">OFAC Sanctions Programs and Countries</td><td class="end"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				<tr><td class="begin"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colspan="100%" style="border-right: none;">OFAC's Specially Designated Nationals and Blocked Persons</td><td class="end"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				<tr><td class="begin"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colspan="100%" style="border-right: none;">Politically Exposed Persons</td><td class="end"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				<tr><td class="begin"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colspan="100%" style="border-right: none;">Primary Money Laundering Concern (PMLC)</td><td class="end"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				<tr><td class="begin"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colspan="100%" style="border-right: none;">Terrorist Exclusion List</td><td class="end"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				<tr><td class="begin"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colspan="100%" style="border-right: none;">Unauthorized Banks</td><td class="end"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				<tr><td class="begin"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colspan="100%" style="border-right: none;">United Nations Consolidated List</td><td class="end"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
				<tr><td class="begin"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td><td colspan="100%" style="border-right: none;">World Bank Debarred Parties</td><td class="end"><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></td></tr>
			</tbody>
			<tfoot>
				<td class="begin"></td>
				<td style="border-right: none;" colspan="100%"></td>
				<td class="end"></td>
			</tfoot>			
		</table>
		</center>
	</xsl:template>

</xsl:stylesheet>
