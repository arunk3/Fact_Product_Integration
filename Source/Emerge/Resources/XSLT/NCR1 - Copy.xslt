<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:a="http://ns.hr-xml.org/2004-08-02">
  <xsl:output method="html" encoding="iso-8859-1" />
  <xsl:template match="/BGC/product/StatewideCriminalSearch/fulfillment">
    <center>
      <div class="sectionsummary">
        <h3>
          <!--(<xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)" />) Results CRIMINAL REPORT.<br />-->
        </h3>
        <h3>
          (<xsl:value-of select="count(results/result/counts/count)"/>) Results<br />
        </h3>
      </div>
      <table id="tblNCR" class="reporttable" border="0" cellpadding="0" cellspacing="0">
        <thead>

          <tr>
            <th class="begin"></th>
            <th colspan="5">
              <span style="font-size:20px;font-weight:bold;">Statewide Criminal Report</span>
            </th>
            <th class="end"></th>
          </tr>

        </thead>
        <tbody>
          <!--<script type="text/javascript">
            document.getElementById('cnt').innerHTML ='0';
            var count=1;
            var CriminalCount=1;
          </script>-->
		<xsl:for-each select="results/result">
                <tr>
                  <td class="begin">
                  </td>
                  <td colspan="5" style="border:solid 1px black;background-color:black;text-align:left;">
                    <span style="font-size:16px;font-weight:bold;color:#fff;">
                      Criminal Record # 
                     <xsl:value-of select="(position())" />
                    </span>
                  </td>
                  <td class="end"></td>
                </tr>

                <tr>
                  <td class="begin">
                  </td>
                  <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;padding:0px;">
                    <table border="1" cellpadding="0" cellspacing="0" width="100%">
                      <tr>
                        <td width="44%">
                          <table border="1" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
								  <xsl:if test="DispositionDate != ''">
									  <div style="float:left;width:7%;background-color:white;border-bottom:0px solid black;text-align:right;">Disposition Date :</div>
									  <div style="float:left;background-color:white;border-bottom:0px solid black;margin-left:1%;">
										  <xsl:value-of select="DispositionDate"/>
									  </div>
								  </xsl:if>
                              </td>
                            </tr>
                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
								  <xsl:if test="isDOBVerified != ''">
									  <div style="float:left;width:7%;background-color:white;border-bottom:0px solid black;text-align:right;">
										  <b>DOB Verified :</b>
									  </div>
									  <div style="float:left;background-color:white;border-bottom:0px solid black;margin-left:1%;">

										  <xsl:value-of select="isDOBVerified"/>
									  </div>
								  </xsl:if>
                              </td>
                            </tr>
                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
								  <xsl:if test="isSSNVerified != ''">
									  <div style="float:left;width:7%;background-color:white;border-bottom:0px solid black;text-align:right;">
										  <b>SSN Verified :</b>
									  </div>
									  <div style="float:left;background-color:white;border-bottom:0px solid black;margin-left:1%;">
										  <xsl:value-of select="isSSNVerified"/>
									  </div>
								  </xsl:if>
                              </td>
                            </tr>
							<tr>
								  <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
									  <xsl:if test="nameOnFile != ''">
										  <div style="float:left;width:7%;background-color:white;border-bottom:0px solid black;text-align:right;">
											  <b>NAME ON FILE :</b>
										  </div>
										  <div style="float:left;background-color:white;border-bottom:0px solid black;margin-left:1%;">
											  <xsl:value-of select="nameOnFile"/>
										  </div>
									  </xsl:if>
								  </td>
							  </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td class="end"></td>
                </tr>

                <xsl:for-each select="a:Charge">

                  <tr>
                    <td class="begin">
                    </td>
                    <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border:solid 1px black;background-color:red;text-align:center;">
                        <tr>
                          <xsl:choose>
                            <xsl:when test="(position()mod 2)=1">
                              <td width="10%" style="border-right:solid 1px black;background-color:#dcdcdc;text-align:center;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                    <td style="text-align:right;background-color:#dcdcdc;">
                                      <span style="font-size:12px;font-weight:bold;color:#000;">
                                        Charge #
                                        <xsl:value-of select="(position())" />
                                      </span>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </xsl:when>
                            <xsl:otherwise>
                              <td width="10%" style="border-right:solid 1px black;background-color:white;text-align:center;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                    <td style="text-align:right;background-color:white;">
                                      <span style="font-size:12px;font-weight:bold;color:#000;">
                                        Charge #
                                        <xsl:value-of select="(position())" />
                                      </span>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </xsl:otherwise>
                          </xsl:choose>
                          <xsl:choose>
                            <xsl:when test="(position()mod 2)=1">
                              <td width="45%" style="background-color:#dcdcdc;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                    <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">
										<xsl:if test="state != ''">
											<div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;width:16%;text-align:right;">
												<b>STATE :</b>
											</div>
											<div style="float:left;background-color:#dcdcdc;border-bottom:0px solid black;margin-left:1%;">
												<xsl:value-of select="state"/>
											</div>
										</xsl:if>
                                    </td>
                                  </tr>

                                  <xsl:if test="(a:ArrestDate)">
                                    <tr>
                                      <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                        <b>Arrest Date:</b>
                                      </td>

                                      <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>

                                  <xsl:if test="(a:OffenseDate)">
                                    <tr>
                                      <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                        <b>Offense Date:</b>
                                      </td>

                                      <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>

                                  <xsl:if test="(a:DispositionDate)">
                                    <tr>
                                      <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                        <b>
                                          Disposition Date:
                                        </b>
                                      </td>

                                      <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>
                                </table>
                              </td>
                              <td width="45%" style="background-color:#dcdcdc;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <xsl:if test="(a:ChargeTypeClassification)!='unknown'">
                                    <tr>
                                      <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">
                                        <b>
                                          Severity:
                                        </b>
                                      </td>

                                      <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                        <xsl:value-of select="a:ChargeTypeClassification"/>
                                      </td>
                                    </tr>
                                  </xsl:if>

                                  <xsl:if test="(a:Disposition)">
                                  <tr>
                                    <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                      <b>
                                        Final Disposition:
                                      </b>
                                    </td>

                                    <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                      <xsl:choose>
                                        <xsl:when test="(a:Disposition)='unknown'">
                                          Manual County or State Report needed for Final Results
                                        </xsl:when>
                                        <xsl:otherwise>
                                          <xsl:value-of select="a:Disposition"/>
                                        </xsl:otherwise>
                                      </xsl:choose>
                                    </td>
                                  </tr>
                                  </xsl:if>

                                  <xsl:if test="(a:Comment)">
                                    <tr>
                                      <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                        <b>
                                          Comments:
                                        </b>
                                      </td>

                                      <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                        <xsl:for-each select="a:Comment">
                                          <span style="float:left;">
                                            <xsl:value-of select="."/>
                                            <xsl:element name="br" />
                                          </span>
                                        </xsl:for-each>
                                      </td>
                                    </tr>
                                  </xsl:if>
                                </table>
                              </td>
                            </xsl:when>
                            <xsl:otherwise>
                              <td width="45%">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                    <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                      <b>Charge:</b>
                                    </td>
                                    <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                      <xsl:value-of select="a:ChargeOrComplaint"/>
                                    </td>
                                  </tr>

                                  <xsl:if test="(a:ArrestDate)">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                        <b>Arrest Date:</b>
                                      </td>

                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>

                                  <xsl:if test="(a:OffenseDate)">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                        <b>Offense Date:</b>
                                      </td>

                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>

                                  <xsl:if test="(a:DispositionDate)">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                        <b>
                                          Disposition Date:
                                        </b>
                                      </td>

                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>
                                </table>
                              </td>
                              <td width="45%">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <xsl:if test="(a:ChargeTypeClassification)!='unknown'">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                        <b>
                                          Severity:
                                        </b>
                                      </td>

                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:value-of select="a:ChargeTypeClassification"/>
                                      </td>
                                    </tr>
                                  </xsl:if>

                                  <xsl:if test="(a:Disposition)">
                                  <tr>
                                    <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                      <b>
                                        Final Disposition:
                                      </b>
                                    </td>

                                    <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                      <xsl:choose>
                                        <xsl:when test="(a:Disposition)='unknown'">
                                          Manual County or State Report needed for Final Results
                                        </xsl:when>
                                        <xsl:otherwise>
                                          <xsl:value-of select="a:Disposition"/>
                                        </xsl:otherwise>
                                      </xsl:choose>
                                    </td>
                                  </tr>
                                  </xsl:if>

                                  <xsl:if test="(a:Comment)">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                        <b>
                                          Comments:
                                        </b>
                                      </td>

                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:for-each select="a:Comment">
                                          <span style="float:left;">
                                            <xsl:value-of select="."/>
                                            <xsl:element name="br" />
                                          </span>
                                        </xsl:for-each>
                                      </td>
                                    </tr>
                                  </xsl:if>
                                </table>
                              </td>
                            </xsl:otherwise>
                          </xsl:choose>
                        </tr>
                      </table>


                    </td>
                    <td class="end"></td>
                  </tr>
                </xsl:for-each>


              </xsl:when>
              <xsl:otherwise>
              </xsl:otherwise>
            </xsl:choose>

          </xsl:for-each>
          <tr id="trNullDOBHeading">
            <td class="begin">
            </td>
            <td onclick="ShowHideTable('tblNullDOB');" colspan="5" style="border:solid 1px black;background-color:white;text-align:left;">
              <div style="float:left;width:90%">
                <div style="float:left;background-position:left;" class="plus_btnSmall">
                </div>
                <div style="font-size:11px;font-weight:bold;color:#000;width:auto; margin-top:0px">
                  Database has also found <span id="lblCount"></span> possible matches with DOB variation that could match to <span id="lblSearchName" ></span>
                  <img Id="imgLearnMore" style="height:25px;margin-bottom:-7px;padding-left:5px;"  src="../Content/themes/base/images/drugtesting4help.png"  onmouseout="exit();" onmouseover="showEMBox('This feature shows records with DOB variations that could possibly match the individual you are searching. Please review the records by selecting “Show” if the record matches the name you are searching check the box and click “Add”. These records will be added to the bottom of the report.');" />
                </div>
              </div>
              <div  style="font-size:13px;font-weight:bold;color:#000;width:auto;float:right; margin-top:7px">
                <a id="ancShowHideTable"  style="text-decoration:none;font-size:11px;color:black;cursor:pointer;" title="show/hide" >HIDE</a>
              </div>
            </td>
            <td class="end"></td>
          </tr>
          <tr id="trNullDOBRecords">
            <td class="begin">
            </td>
            <td colspan="5" style="border:solid 1px black;background-color:white;text-align:left;">
              <div id="tblNullDOB" style="display:block;">
                <table  cellpadding="0" cellspacing="0"    style="border:1px solid  black;background-color:white;text-align:center;">
                  <tr id="tblNullH1">
                    <td style="width:0%;"></td>
                    <td style="width:7%;"></td>
                    <td colspan="11" >Below are the possible matches that are exact match. Please review and add any records that match to the name you are searching.</td>

                  </tr>
                  <tr id="tblNullH2">
                    <td></td>
                    <td style="background-color:white;color:white;padding:5px;width:7%;">_____</td>
                    <td style="background-color:#545454;color:white;padding:5px;width:18%;">Jurisdiction</td>
                    <td style="background-color:#545454;color:white;padding:5px;width:8%;">Case Number</td>
                    <td style="background-color:#545454;color:white;padding:5px;width:8%;">Last Name</td>
                    <td style="background-color:#545454;color:white;padding:5px;width:8%;">First Name</td>
                    <td style="background-color:#545454;color:white;padding:5px;width:8%;">Middle Name</td>
                    <td style="background-color:#545454;color:white;padding:5px;width:8%;">Address</td>
                    <td style="background-color:#545454;color:white;padding:5px;width:5%;">DOB</td>
                    <td style="background-color:#545454;color:white;padding:5px;width:5%;">R/S</td>
                    <td style="background-color:#545454;color:white;padding:5px;width:7%;">SSN</td>
                    <td style="background-color:#545454;color:white;padding:5px;width:13%;">MatchMeter</td>
                    <td style="background-color:#545454;color:#545454;padding:5px;width:4%;">_____</td>

                  </tr>
                  <tr>
                    <td></td>
                  </tr>
                  <xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase">
                    <xsl:choose>
                      <xsl:when test="not(a:SubjectIdentification/a:DemographicDetail/a:DateOfBirth) and not(a:SubjectIdentification/a:DemographicDetail/@IsAdded='true')" >
                        <tr>
                          <td></td>
                          <td colspan="12">
                            <xsl:choose>
                              <xsl:when test="(position()mod 2)=1">
                                <table  cellpadding="0" cellspacing="0"  style="border:1px solid  black;background-color:#dcdcdc;">
                                  <tr>

                                    <td  style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;padding-top:10px;width:7%;">
                                      <table botder="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                          <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;padding-top:1px;" >
                                            <input type="checkbox" style="padding:0 auto;" id="ChkShowHideNCR1_{($pkOrderDetailId)}_{(position())}" title="Add" value="{(position())}"   />
                                          </td>
                                          <td  style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;padding-top:1px;">
                                            <u>
                                              <a id="ancNCR1_{(position())}"  href="javascript:ShowHideCharges('tblCharges_NCR1_'+{(position())},'{(position())}');"  style="text-decoration:none;font-size:11px;color:black;" title="show/hide" >SHOW</a>
                                            </u>
                                          </td>
                                        </tr>

                                      </table>


                                    </td>
                                    <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;padding-top:10px;width:19%;">
                                      <xsl:value-of select="a:CourtName"/>
                                    </td>
                                    <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;padding-top:10px;width:8%;">
                                      <xsl:value-of select="a:AgencyReference[@type='Docket']"/>
                                    </td>
                                    <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;padding-top:10px;width:8%;">
                                      <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                        <xsl:if test="a:FamilyName/@primary='true'">
                                          <xsl:value-of select="a:FamilyName"/>

                                        </xsl:if>
                                      </xsl:for-each>
                                    </td>
                                    <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;padding-top:10px;width:8%;">
                                      <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                        <xsl:if test="a:FamilyName/@primary='true'">

                                          <xsl:value-of select="a:GivenName"/>

                                        </xsl:if>
                                      </xsl:for-each>
                                    </td>
                                    <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;padding-top:10px;width:8%;">
                                      <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                        <xsl:if test="a:FamilyName/@primary='true'">

                                          <xsl:value-of select="a:MiddleName"/>

                                        </xsl:if>
                                      </xsl:for-each>
                                    </td>
                                    <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;padding-top:10px;width:8%;">
                                      <xsl:if test="(a:SubjectIdentification/a:PostalAddress)">
                                        <xsl:for-each select="a:SubjectIdentification/a:PostalAddress">
                                          <xsl:if test="(a:DeliveryAddress)">
                                            <xsl:value-of select ="a:DeliveryAddress"/>
                                            <xsl:if test="(a:Municipality)">
                                              <span>, </span>
                                            </xsl:if>
                                          </xsl:if>
                                          <xsl:if test="(a:Municipality)">
                                            <xsl:value-of select ="a:Municipality"/>
                                            <xsl:if test="(a:Region)">
                                              <span>, </span>
                                            </xsl:if>
                                          </xsl:if>
                                          <xsl:if test="(a:Region)">
                                            <xsl:value-of select ="a:Region"/>
                                            <xsl:if test="(a:PostalCode)">
                                              <span> - </span>
                                            </xsl:if>
                                          </xsl:if>
                                          <xsl:if test="(a:PostalCode)">
                                            <xsl:value-of select ="a:PostalCode"/>
                                          </xsl:if>
                                          <xsl:element name="br" />
                                        </xsl:for-each>

                                      </xsl:if>
                                    </td>
                                    <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;padding-top:10px;width:5%;">
                                      <xsl:value-of select='substring(a:SubjectIdentification/a:DemographicDetail/a:DateOfBirth,1,10)' />
                                      <xsl:value-of select='a:SubjectIdentification/a:DemographicDetail/a:Age' />
                                      <xsl:value-of select='a:SubjectIdentification/a:DemographicDetail/a:Other' />
                                    </td>
                                    <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;padding-top:10px;width:5%;">
                                      <xsl:value-of select="a:SubjectIdentification/a:DemographicDetail/a:Race"/>
                                    </td>
                                    <td  style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;padding-top:10px;width:7%;">
                                      <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:DemographicDetail/a:GovernmentId"/>
                                    </td>
                                    <xsl:if test="(a:MatchMeter)">
                                      <td class="matchmeter" style="background-color:#dcdcdc;width:12%;background-position: left top;">
                                        <table border="0" style="background: transparent;" cellpadding="0" cellspacing="0" width="100%">
                                          <tr>
                                            <td style="background: transparent;font-size:11px;width:60%;height:24px;text-align:right;text-transform:capitalize !important;padding:0px;">
                                              <b>Name Match</b>
                                            </td>
                                            <td style="background: transparent;text-align:center;height:22px;padding:0px;">
                                              <xsl:text xml:space="preserve">  </xsl:text>
                                              <xsl:if test="(a:MatchMeter/a:Name) = '2'">
                                                <div class="matchgreen" />
                                              </xsl:if>
                                              <xsl:if test="(a:MatchMeter/a:Name) = '1'">
                                                <div class="matchyellow" />
                                              </xsl:if>
                                              <xsl:if test="(a:MatchMeter/a:Name) = '0'">
                                                <div class="matchred" />
                                              </xsl:if>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td style="background: transparent;font-size:11px;width:60%;height:25px;text-align:right;text-transform:capitalize !important;padding:0px;">
                                              <b>Address Match</b>
                                            </td>
                                            <td style="background: transparent;text-align:center;height:22px;padding:0px;">
                                              <xsl:text xml:space="preserve">  </xsl:text>
                                              <xsl:if test="(a:MatchMeter/a:Address) = '2'">
                                                <div class="matchgreen" />
                                              </xsl:if>
                                              <xsl:if test="(a:MatchMeter/a:Address) = '1'">
                                                <div class="matchyellow" />
                                              </xsl:if>
                                              <xsl:if test="(a:MatchMeter/a:Address) = '0'">
                                                <div class="matchred" />
                                              </xsl:if>
                                              <xsl:if test="(a:MatchMeter/a:Address) = '-1'">
                                                N/A
                                              </xsl:if>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td style="background: transparent;font-size:11px;width:60%;height:28px;text-align:right;text-transform:capitalize !important;padding:0px;">
                                              <b>DOB Match</b>
                                            </td>
                                            <td style="background: transparent;text-align:center;height:28px;padding:0px;">
                                              <xsl:text xml:space="preserve">  </xsl:text>
                                              <xsl:if test="(a:MatchMeter/a:DOB) = '2'">
                                                <div class="matchgreen" />
                                              </xsl:if>
                                              <xsl:if test="(a:MatchMeter/a:DOB) = '1'">
                                                <div class="matchyellow" />
                                              </xsl:if>
                                              <xsl:if test="(a:MatchMeter/a:DOB) = '0'">
                                                <div class="matchred" />
                                              </xsl:if>
                                            </td>
                                          </tr>
                                        </table>
                                      </td>
                                    </xsl:if>
                                    <td  style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;padding-top:10px;width:4%;">
                                      <input type="button" id="btnUpdateRecord_NCR1_{(position())}" value="Add" onclick="AddNullDOB(this.id,'{($pkOrderDetailId)}','{(position())}');"  wrap="off"></input>
                                    </td>
                                  </tr>
                                </table>
                              </xsl:when>
                              <xsl:otherwise>
                                <table  cellpadding="0" cellspacing="0"  style="border:1px solid  black;background-color:#fff;">
                                  <tr>

                                    <td  style="width:7%">
                                      <table botder="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                          <td style="padding-top:1px">
                                            <input type="checkbox" style="padding:0 auto;" id="ChkShowHideNCR1_{($pkOrderDetailId)}_{(position())}" title="Add" value="{(position())}"   />
                                          </td>
                                          <td>
                                            <u>
                                              <a id="ancNCR1_{(position())}"  href="javascript:ShowHideCharges('tblCharges_NCR1_'+{(position())},'{(position())}');"  style="text-decoration:none;font-size:11px;color:black;" title="show/hide" >SHOW</a>
                                            </u>
                                          </td>
                                        </tr>

                                      </table>


                                    </td>
                                    <td style="text-align:left;background-color:white;border-bottom:0px solid black;padding-top:10px;width:19%">
                                      <xsl:value-of select="a:CourtName"/>
                                    </td>
                                    <td style="text-align:left;background-color:white;border-bottom:0px solid black;padding-top:10px;width:8%">
                                      <xsl:value-of select="a:AgencyReference[@type='Docket']"/>
                                    </td>
                                    <td style="text-align:left;background-color:white;border-bottom:0px solid black;padding-top:10px;width:8%">
                                      <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                        <xsl:if test="a:FamilyName/@primary='true'">
                                          <xsl:value-of select="a:FamilyName"/>

                                        </xsl:if>
                                      </xsl:for-each>
                                    </td>
                                    <td style="text-align:left;background-color:white;border-bottom:0px solid black;padding-top:10px;width:8%">
                                      <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                        <xsl:if test="a:FamilyName/@primary='true'">

                                          <xsl:value-of select="a:GivenName"/>

                                        </xsl:if>
                                      </xsl:for-each>
                                    </td>
                                    <td style="text-align:left;background-color:white;border-bottom:0px solid black;padding-top:10px;width:8%">
                                      <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                        <xsl:if test="a:FamilyName/@primary='true'">

                                          <xsl:value-of select="a:MiddleName"/>

                                        </xsl:if>
                                      </xsl:for-each>
                                    </td>
                                    <td style="text-align:left;background-color:white;border-bottom:0px solid black;padding-top:10px;width:8%">
                                      <xsl:if test="(a:SubjectIdentification/a:PostalAddress)">
                                        <xsl:for-each select="a:SubjectIdentification/a:PostalAddress">
                                          <xsl:if test="(a:DeliveryAddress)">
                                            <xsl:value-of select ="a:DeliveryAddress"/>
                                            <xsl:if test="(a:Municipality)">
                                              <span>, </span>
                                            </xsl:if>
                                          </xsl:if>
                                          <xsl:if test="(a:Municipality)">
                                            <xsl:value-of select ="a:Municipality"/>
                                            <xsl:if test="(a:Region)">
                                              <span>, </span>
                                            </xsl:if>
                                          </xsl:if>
                                          <xsl:if test="(a:Region)">
                                            <xsl:value-of select ="a:Region"/>
                                            <xsl:if test="(a:PostalCode)">
                                              <span> - </span>
                                            </xsl:if>
                                          </xsl:if>
                                          <xsl:if test="(a:PostalCode)">
                                            <xsl:value-of select ="a:PostalCode"/>
                                          </xsl:if>
                                          <xsl:element name="br" />
                                        </xsl:for-each>

                                      </xsl:if>
                                    </td>
                                    <td style="text-align:left;background-color:white;border-bottom:0px solid black;padding-top:10px;width:5%">
                                      <xsl:value-of select='substring(a:SubjectIdentification/a:DemographicDetail/a:DateOfBirth,1,10)' />
                                      <xsl:value-of select='a:SubjectIdentification/a:DemographicDetail/a:Age' />
                                      <xsl:value-of select='a:SubjectIdentification/a:DemographicDetail/a:Other' />
                                    </td>
                                    <td style="text-align:left;background-color:white;border-bottom:0px solid black;padding-top:10px;width:5%">
                                      <xsl:value-of select="a:SubjectIdentification/a:DemographicDetail/a:Race"/>
                                    </td>
                                    <td style="width:7%">
                                      <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:DemographicDetail/a:GovernmentId"/>
                                    </td>
                                    <xsl:if test="(a:MatchMeter)">
                                      <td class="matchmeter" style="width:12%;background-position: left top;">
                                        <table border="0" style="background: transparent;" cellpadding="0" cellspacing="0" width="100%">
                                          <tr>
                                            <td style="background: transparent;font-size:11px;width:60%;height:24px;text-align:right;text-transform:capitalize !important;padding:0px;">
                                              <b>Name Match</b>
                                            </td>
                                            <td style="background: transparent;text-align:center;height:22px;padding:0px;">
                                              <xsl:text xml:space="preserve">  </xsl:text>
                                              <xsl:if test="(a:MatchMeter/a:Name) = '2'">
                                                <div class="matchgreen" />
                                              </xsl:if>
                                              <xsl:if test="(a:MatchMeter/a:Name) = '1'">
                                                <div class="matchyellow" />
                                              </xsl:if>
                                              <xsl:if test="(a:MatchMeter/a:Name) = '0'">
                                                <div class="matchred" />
                                              </xsl:if>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td style="background: transparent;font-size:11px;width:60%;height:25px;text-align:right;text-transform:capitalize !important;padding:0px;">
                                              <b>Address Match</b>
                                            </td>
                                            <td style="background: transparent;text-align:center;height:22px;padding:0px;">
                                              <xsl:text xml:space="preserve">  </xsl:text>
                                              <xsl:if test="(a:MatchMeter/a:Address) = '2'">
                                                <div class="matchgreen" />
                                              </xsl:if>
                                              <xsl:if test="(a:MatchMeter/a:Address) = '1'">
                                                <div class="matchyellow" />
                                              </xsl:if>
                                              <xsl:if test="(a:MatchMeter/a:Address) = '0'">
                                                <div class="matchred" />
                                              </xsl:if>
                                              <xsl:if test="(a:MatchMeter/a:Address) = '-1'">
                                                N/A
                                              </xsl:if>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td style="background: transparent;font-size:11px;width:60%;height:28px;text-align:right;text-transform:capitalize !important;padding:0px;">
                                              <b>DOB Match</b>
                                            </td>
                                            <td style="background: transparent;text-align:center;height:28px;padding:0px;">
                                              <xsl:text xml:space="preserve">  </xsl:text>
                                              <xsl:if test="(a:MatchMeter/a:DOB) = '2'">
                                                <div class="matchgreen" />
                                              </xsl:if>
                                              <xsl:if test="(a:MatchMeter/a:DOB) = '1'">
                                                <div class="matchyellow" />
                                              </xsl:if>
                                              <xsl:if test="(a:MatchMeter/a:DOB) = '0'">
                                                <div class="matchred" />
                                              </xsl:if>
                                            </td>
                                          </tr>
                                        </table>
                                      </td>
                                    </xsl:if>
                                    <td style="width:4%">
                                      <input type="button" id="btnUpdateRecord_NCR1_{(position())}" value="Add" onclick="AddNullDOB(this.id,'{($pkOrderDetailId)}','{(position())}');"  wrap="off"></input>
                                    </td>
                                  </tr>
                                </table>
                              </xsl:otherwise>
                            </xsl:choose>
                          </td>
                        </tr>
                        <tr>
                          <td></td>
                          <td colspan="12">
                            <div id="tblCharges_NCR1_{(position())}" style="display:none;">
                              <table  width="100%" cellpadding="0" cellspacing="0"  style="border:0px solid  black;">
                                <xsl:for-each select="a:Charge">
                                  <tr>

                                    <td style="border:solid 0px black;background-color:white;text-align:center;">
                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border:solid 1px black;background-color:red;text-align:center;">
                                        <tr>
                                          <xsl:choose>
                                            <xsl:when test="(position()mod 2)=1">
                                              <td width="10%" style="border-right:solid 1px black;background-color:#dcdcdc;text-align:center;">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                  <tr>
                                                    <td style="text-align:right;background-color:#dcdcdc;">
                                                      <span style="font-size:12px;font-weight:bold;color:#000;">
                                                        Charge #
                                                        <xsl:value-of select="(position())" />
                                                      </span>
                                                    </td>
                                                  </tr>
                                                </table>
                                              </td>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              <td width="10%" style="border-right:solid 1px black;background-color:white;text-align:center;">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                  <tr>
                                                    <td style="text-align:right;background-color:white;">
                                                      <span style="font-size:12px;font-weight:bold;color:#000;">
                                                        Charge #
                                                        <xsl:value-of select="(position())" />
                                                      </span>
                                                    </td>
                                                  </tr>
                                                </table>
                                              </td>
                                            </xsl:otherwise>
                                          </xsl:choose>
                                          <xsl:choose>
                                            <xsl:when test="(position()mod 2)=1">
                                              <td width="45%" style="background-color:#dcdcdc;">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                  <tr>
                                                    <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">
                                                      <b>Charge:</b>
                                                    </td>
                                                    <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                                      <xsl:value-of select="a:ChargeOrComplaint"/>
                                                    </td>
                                                  </tr>

                                                  <xsl:if test="(a:ArrestDate)">
                                                    <tr>
                                                      <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                                        <b>Arrest Date:</b>
                                                      </td>

                                                      <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                                        <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                                      </td>
                                                    </tr>
                                                  </xsl:if>

                                                  <tr>
                                                    <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                                      <b>Offense Date:</b>
                                                    </td>

                                                    <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                                      <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                                      <b>
                                                        Disposition Date:
                                                      </b>
                                                    </td>

                                                    <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                                      <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                                    </td>
                                                  </tr>
                                                </table>
                                              </td>
                                              <td width="45%" style="background-color:#dcdcdc;">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                  <tr>
                                                    <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">
                                                      <b>
                                                        Severity:
                                                      </b>
                                                    </td>

                                                    <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                                      <xsl:value-of select="a:ChargeTypeClassification"/>
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                                      <b>
                                                        Final Disposition:
                                                      </b>
                                                    </td>

                                                    <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                                      <xsl:value-of select="a:Disposition"/>
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;">
                                                      <b>
                                                        Comments:
                                                      </b>
                                                    </td>

                                                    <td style="text-align:left;background-color:#dcdcdc;border-bottom:0px solid black;">
                                                      <xsl:for-each select="a:Comment">
                                                        <span style="float:left;">
                                                          <xsl:value-of select="."/>
                                                          <xsl:element name="br" />
                                                        </span>
                                                      </xsl:for-each>
                                                    </td>
                                                  </tr>
                                                </table>
                                              </td>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              <td width="45%">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                  <tr>
                                                    <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                                      <b>Charge:</b>
                                                    </td>
                                                    <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                                      <xsl:value-of select="a:ChargeOrComplaint"/>
                                                    </td>
                                                  </tr>

                                                  <xsl:if test="(a:ArrestDate)">
                                                    <tr>
                                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                                        <b>Arrest Date:</b>
                                                      </td>

                                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                                        <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                                      </td>
                                                    </tr>
                                                  </xsl:if>

                                                  <tr>
                                                    <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                                      <b>Offense Date:</b>
                                                    </td>

                                                    <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                                      <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                                      <b>
                                                        Disposition Date:
                                                      </b>
                                                    </td>

                                                    <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                                      <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                                    </td>
                                                  </tr>
                                                </table>
                                              </td>
                                              <td width="45%">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                  <tr>
                                                    <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                                      <b>
                                                        Severity:
                                                      </b>
                                                    </td>

                                                    <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                                      <xsl:value-of select="a:ChargeTypeClassification"/>
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                                      <b>
                                                        Final Disposition:
                                                      </b>
                                                    </td>

                                                    <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                                      <xsl:value-of select="a:Disposition"/>
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                                      <b>
                                                        Comments:
                                                      </b>
                                                    </td>

                                                    <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                                      <xsl:for-each select="a:Comment">
                                                        <span style="float:left;">
                                                          <xsl:value-of select="."/>
                                                          <xsl:element name="br" />
                                                        </span>
                                                      </xsl:for-each>
                                                    </td>
                                                  </tr>
                                                </table>
                                              </td>
                                            </xsl:otherwise>
                                          </xsl:choose>
                                        </tr>
                                      </table>
                                    </td>

                                  </tr>
                                </xsl:for-each>
                              </table>
                            </div>
                          </td>
                        </tr>
                      </xsl:when>
                      <xsl:otherwise>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:for-each>
                </table>
              </div>
            </td>
            <td class="end"></td>
          </tr>
          
          <xsl:for-each select="a:BackgroundReports">
            <tr id="trRawNCR11">
              <td class="begin">
              </td>
              <td onclick="collapse('{concat('trRawNCR1_',@OrderDetailId)}');" colspan="5" style="border:solid 1px black;background-color:white;text-align:left;">
                <div style="float:left">
                  <div style="float:left;background-position:left;" class="plus_btnSmall">
                  </div>
                  <div style="font-size:17px;font-weight:bold;color:#000;width:300px; margin-top:2px">
                    Raw Courthouse Data
                  </div>
                </div>
              </td>
              <td class="end"></td>
            </tr>

            <tr id="trRawNCR12">
              <td class="begin">
              </td>
              <td colspan="5" style="border:solid 1px black;background-color:white;text-align:center;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr id="{concat('trRawNCR1_',@OrderDetailId)}_b" style="display:none;">
                    <td style="background-color:white;">
                      <div id="divRawDataNCR1">
                        <pre>
                          <xsl:value-of select ="$TextData"/>
                        </pre>
                      </div>
                    </td>
                  </tr>
                </table>
              </td>
              <td class="end"></td>
            </tr>
          </xsl:for-each>

        </tbody>
        <tfoot>
          <td class="begin"></td>
          <td colspan="5"></td>
          <td class="end"></td>
        </tfoot>
      </table>
    </center>

    <div class="sectionsummary">
      <h3 style="line-height: 1em;">
        <xsl:choose>
          <xsl:when test="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)=0">
            <p>
              National Sex Offender : Clear<br/>
              OFAC : Clear<br/>
              OIG : Clear<br/>
              For a complete list of coverage, please visit<br/>
              <a href="http://emerge.intelifi.com/coverage.pdf" target="_blank">US Criminal Rapid Search Coverage Guide</a>
            </p>
          </xsl:when>
          <xsl:otherwise>
            Data Sources Searched:<br/>
            Court Records, Department of Corrections, Administrative Office of Courts,<br/>
            National Sex Offender Registry, OFAC, OIG<br/>
            For a complete list of coverage, please visit<br/>
            <a href="http://emerge.intelifi.com/coverage.pdf" target="_blank">US Criminal Rapid Search Coverage Guide</a>

          </xsl:otherwise>
        </xsl:choose>

      </h3>
    </div>
  </xsl:template>
</xsl:stylesheet>