﻿<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:a="http://ns.hr-xml.org/2004-08-02">
  <xsl:output method="html" encoding="iso-8859-1" />
  <xsl:template match="/">



    <div class="people-search-wrap">
      <div class="sectionsummary reco-search">
        <h3>
          (<xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)" />) Results NCR+ Report.<br />
        </h3>
      </div>

      <xsl:variable name="TotalRecords">
        <xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)" />
      </xsl:variable>

      <xsl:if test="$TotalRecords=0">

        <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
          <thead>
            <tr>
              <th class="begin" style="background:#f7f5f5;"></th>
              <th colspan="5" style="background:#f7f5f5;">
                <span style="font-size:20px;font-weight:bold;">Search Information</span>
              </th>
              <th class="end" style="background:#f7f5f5;"></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="begin" style="border-left: medium none;">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colspan="5">
                <h4>Name:</h4>
                <h4>
                  <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:GivenName"/>
                  <xsl:text xml:space="preserve">  </xsl:text>
                  <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:FamilyName"/>
                </h4>
                <br />
                <h4>DOB:</h4>
                <h4>
                  <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:DemographicDetail/a:DateOfBirth"/>
                </h4>
              </td>
              <td class="end" style="border-right: medium none;">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </tbody>
          <tfoot style="display:none;">
            <td class="begin"></td>
            <td colspan="5"></td>
            <td class="end"></td>
          </tfoot>
        </table>

      </xsl:if>

      <table id="tblNCRPLUS" class="reporttable" border="0" cellpadding="0" cellspacing="0">
        <thead>
          <tr>

            <th colspan="5" style="background:#f7f5f5;">
              <div class="summry-row clearfix" style="background:#f7f5f5;">
                <div class="txt-sumry" style="background:#f7f5f5; padding-top: 9px; color: rgb(102, 102, 102);"> Criminal Results </div>
                <div class="txt-detials">
                  <ul>
                    <li> </li>
                    <li> </li>
                  </ul>
                  <ul>
                    <li style="text-align:left;">
                      <strong></strong>
                    </li>
                    <li style="text-align:left;">
                      <strong></strong>
                    </li>
                  </ul>
                </div>
              </div>

            </th>
            
          </tr>
        </thead>
        <tbody>

          <xsl:variable name="TextData">
            <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text" />
          </xsl:variable>

          <xsl:variable name="SSNNo">
            xxx-xx-<xsl:value-of select="substring(a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:DemographicDetail/a:GovernmentId,6,4)" />
          </xsl:variable>

          <xsl:if test="not(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text)">
            <tr>
             
              <td colspan="5">
                <div style="width: 90%; margin: 0 auto; text-align: left">
                  <div style="width:588px; text-align:left">
                    <div style="font-size:11px;" id="report_ncr+">
                      <pre>
                        <!--<xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text">
                        <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text" disable-output-escaping="yes"/>
                      </xsl:if>-->
                        <xsl:if test="not(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text)">
                          <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus = 'Cancelled'">
                            <p>
                              There was an error running this report.<br /> Please contact our customer support team to quickly resolve this issue.
                            </p>
                          </xsl:if>
                          <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus != 'Cancelled'">
                            <p>*** Clear ***</p>
                          </xsl:if>
                          <p></p>
                        </xsl:if>
                      </pre>
                    </div>
                  </div>
                </div>
              </td>
            
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              
            </tr>
          </xsl:if>

          <xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase">

            <xsl:choose>
              <xsl:when test="position()>1">
                <tr style="height:20px;">
               
                  <td colspan="5" style="text-align:center;">
                    <span style="font-size:16px;font-weight:bold;color:white;">
                    </span>
                  </td>
               
                </tr>
              </xsl:when>
            </xsl:choose>

            <tr class="search-heading">
           
              <td colspan="5">


                <table>
                  <td width="70" style=" text-align:right;">
                    <span class="countr">
                      <xsl:value-of select="(position())" />
                    </span>
                  </td>
                  <td align="left">
                    <span class="mn-hd">Criminal Record </span>

                  </td>

                </table>
                
              </td>
          
            </tr>

            <tr>
              
              <td colspan="5" >
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr>
                    <td width="70"></td>
                    <td width="45%">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="source-tb">
                        <tr>
                          <td>
                            
                            <b>Source:</b>
                          </td>
                          <td>
                            <xsl:value-of select="a:CourtName"/>
                          </td>
                        </tr>

                        <tr>
                          <td >
                            <b>Full Name:</b>
                          </td>
                          <td >
                            <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                              <xsl:if test="a:FamilyName/@primary='true'">
                                <xsl:value-of select="a:FamilyName"/>
                                <xsl:if test="(a:GivenName)">
                                  <span>, </span>
                                </xsl:if>
                                <xsl:value-of select="a:GivenName"/>
                                <xsl:if test="(a:MiddleName)">
                                  <span>, </span>
                                </xsl:if>
                                <xsl:value-of select="a:MiddleName"/>
                                <xsl:element name="br"/>
                              </xsl:if>
                            </xsl:for-each>
                          </td>
                        </tr>

                        <xsl:if test="(a:SubjectIdentification/a:PersonName/a:FamilyName/@primary='false')">
                          <tr>
                            <td >
                              <b>AKA Name:</b>
                            </td>
                            <td >
                              <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                <xsl:if test="a:FamilyName/@primary='false'">
                                  <xsl:value-of select="a:FamilyName"/>
                                  <xsl:if test="(a:GivenName)">
                                    <span>, </span>
                                  </xsl:if>
                                  <xsl:value-of select="a:GivenName"/>
                                  <xsl:if test="(a:MiddleName)">
                                    <span>, </span>
                                  </xsl:if>
                                  <xsl:value-of select="a:MiddleName"/>
                                  <xsl:element name="br"/>
                                </xsl:if>
                              </xsl:for-each>
                            </td>
                          </tr>
                        </xsl:if>

                        <tr>
                          <td >
                            <b>DOB:</b>
                          </td>
                          <td >
                            <xsl:value-of select='substring(a:SubjectIdentification/a:DemographicDetail/a:DateOfBirth,1,10)' />
                            <xsl:value-of select='a:SubjectIdentification/a:DemographicDetail/a:Age' />
                            <xsl:value-of select='a:SubjectIdentification/a:DemographicDetail/a:Other' />
                          </td>
                        </tr>

                       
                      </table>
                    </td>
                    <td width="" class="col2">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <xsl:if test="(a:SubjectIdentification/a:DemographicDetail/a:GovernmentId/@documentType='DRIVERS LICENSE')">
                          <tr>
                            <td>
                              <b>DL#:</b>
                            </td>
                            <td >
                              <xsl:value-of select ="a:SubjectIdentification/a:DemographicDetail/a:GovernmentId[@documentType='DRIVERS LICENSE']"/>
                            </td>
                          </tr>
                        </xsl:if>

                        <xsl:if test="(a:SubjectIdentification/a:PostalAddress)">
                          <tr>
                            <td>
                              <b>Address:</b>
                            </td>
                            <td >
                              <xsl:for-each select="a:SubjectIdentification/a:PostalAddress">
                                <xsl:if test="(a:DeliveryAddress)">
                                  <xsl:value-of select ="a:DeliveryAddress"/>
                                  <xsl:if test="(a:Municipality)">
                                    <span>, </span>
                                  </xsl:if>
                                </xsl:if>
                                <xsl:if test="(a:Municipality)">
                                  <xsl:value-of select ="a:Municipality"/>
                                  <xsl:if test="(a:Region)">
                                    <span>, </span>
                                  </xsl:if>
                                </xsl:if>
                                <xsl:if test="(a:Region)">
                                  <xsl:value-of select ="a:Region"/>
                                  <xsl:if test="(a:PostalCode)">
                                    <span> - </span>
                                  </xsl:if>
                                </xsl:if>
                                <xsl:if test="(a:PostalCode)">
                                  <xsl:value-of select ="a:PostalCode"/>
                                </xsl:if>
                                <xsl:element name="br" />
                              </xsl:for-each>
                            </td>
                          </tr>
                        </xsl:if>

                        <tr>
                          <td>
                            <b>Race:</b>
                          </td>
                          <td >
                            <xsl:value-of select="a:SubjectIdentification/a:DemographicDetail/a:Race"/>
                          </td>
                        </tr>

                        <tr>
                          <td>
                            <b>Gender:</b>
                          </td>
                          <td>
                            <xsl:choose>
                              <xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:GenderCode = 1">
                                Male
                              </xsl:when>
                              <xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:GenderCode = 2">
                                Female
                              </xsl:when>
                              <xsl:otherwise>UnKnown</xsl:otherwise>
                            </xsl:choose>
                          </td>
                        </tr>

                        <xsl:if test="(a:AgencyReference[@type='Docket'])">
                          <tr>
                            <td >
                              <b>Docket Number:</b>
                            </td>

                            <td >
                              <xsl:value-of select="a:AgencyReference[@type='Docket']"/>
                            </td>
                          </tr>
                        </xsl:if>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
             
            </tr>

            <xsl:for-each select="a:Charge">

              <tr class="sparater-td manage-table">
               
                <td colspan="5" >
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                    <tr class="sparater-td manage-table">
                      <xsl:choose>
                        <xsl:when test="(position()mod 2)=1">
                          <td width="70" >
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td style="text-align:right; padding-right: 0;">
                                  <span>
                                    <span class="blue-no" style="padding-top:5px;">
                                      <xsl:value-of select="(position())" />
                                    </span>

                                  </span>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </xsl:when>
                        <xsl:otherwise>
                          <td width="70">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td style="text-align:right;background-color:white; padding-right:0;">
                                  <span class="countr">
                                    <xsl:value-of select="(position())" />
                                  </span>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </xsl:otherwise>
                      </xsl:choose>
                      <xsl:choose>
                        <xsl:when test="(position()mod 2)=1">
                          <td width="45%" >
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td>
                                  <b>Charge:</b>
                                </td>
                                <td >
                                  <xsl:value-of select="a:ChargeOrComplaint"/>
                                </td>
                              </tr>
                              <xsl:if test="(a:ChargeDate)">
                                <tr>
                                  <td>
                                    <b>Charge Date:</b>
                                  </td>

                                  <td >
                                    <xsl:value-of select='substring(a:ChargeDate,1,10)' />
                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="(a:ArrestDate)">
                                <tr>
                                  <td >
                                    <b>Arrest Date:</b>
                                  </td>

                                  <td >
                                    <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                  </td>
                                </tr>
                              </xsl:if>

                              <tr>
                                <td >
                                  <b>Offense Date:</b>
                                </td>

                                <td >
                                  <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                </td>
                              </tr>

                              <tr>
                                <td>
                                  <b>
                                    Disposition Date:
                                  </b>
                                </td>

                                <td >
                                  <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                </td>
                              </tr>
                            </table>
                          </td>
                          <td width="" class="col2">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td >
                                  <b>
                                    Severity:
                                  </b>
                                </td>

                                <td >
                                  <xsl:value-of select="a:ChargeTypeClassification"/>
                                </td>
                              </tr>

                              <tr>
                                <td >
                                  <b>
                                    Final Disposition:
                                  </b>
                                </td>

                                <td >
                                  <xsl:choose>
                                    <xsl:when test="(contains(../a:CourtName, 'SAN BERNARDINO') or contains(../a:CourtName, 'RIVERSIDE')) and (a:Disposition)='NOT CONVICTED'">
                                      Unknown
                                    </xsl:when>
                                    <xsl:otherwise>
                                      <xsl:value-of select="a:Disposition"/>
                                    </xsl:otherwise>
                                  </xsl:choose>
                                </td>
                              </tr>

                              <tr>
                                <td>
                                  <b>
                                    Comments:
                                  </b>
                                </td>

                                <td>
                                  <xsl:for-each select="a:Comment">
                                    <span style="float:left;">
                                      <xsl:value-of select="."/>
                                      <xsl:element name="br" />
                                    </span>
                                  </xsl:for-each>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </xsl:when>
                        <xsl:otherwise>
                          <td width="45%">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td>
                                  <b>Charge:</b>
                                </td>
                                <td >
                                  <xsl:value-of select="a:ChargeOrComplaint"/>
                                </td>
                              </tr>
                              <xsl:if test="(a:ChargeDate)">
                                <tr>
                                  <td >
                                    <b>Charge Date:</b>
                                  </td>

                                  <td >
                                    <xsl:value-of select='substring(a:ChargeDate,1,10)' />
                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="(a:ArrestDate)">
                                <tr>
                                  <td >
                                    <b>Arrest Date:</b>
                                  </td>

                                  <td >
                                    <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                  </td>
                                </tr>
                              </xsl:if>

                              <tr>
                                <td >
                                  <b>Offense Date:</b>
                                </td>

                                <td >
                                  <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                </td>
                              </tr>

                              <tr>
                                <td >
                                  <b>
                                    Disposition Date:
                                  </b>
                                </td>

                                <td >
                                  <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                </td>
                              </tr>
                            </table>
                          </td>
                          <td class="col2">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td >
                                  <b>
                                    Severity:
                                  </b>
                                </td>

                                <td >
                                  <xsl:value-of select="a:ChargeTypeClassification"/>
                                </td>
                              </tr>

                              <tr>
                                <td>
                                  <b>
                                    Final Disposition:
                                  </b>
                                </td>

                                <td >
                                  <xsl:choose>
                                    <xsl:when test="(contains(../a:CourtName, 'SAN BERNARDINO') or contains(../a:CourtName, 'RIVERSIDE')) and (a:Disposition)='NOT CONVICTED'">
                                      Unknown
                                    </xsl:when>
                                    <xsl:otherwise>
                                      <xsl:value-of select="a:Disposition"/>
                                    </xsl:otherwise>
                                  </xsl:choose>
                                </td>
                              </tr>

                              <tr>
                                <td >
                                  <b>
                                    Comments:
                                  </b>
                                </td>

                                <td >
                                  <xsl:for-each select="a:Comment">
                                    <span style="float:left;">
                                      <xsl:value-of select="."/>
                                      <xsl:element name="br" />
                                    </span>
                                  </xsl:for-each>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </xsl:otherwise>
                      </xsl:choose>
                    </tr>
                  </table>
                </td>
               
              </tr>
            </xsl:for-each>

          </xsl:for-each>

          <xsl:for-each select="a:BackgroundReports">
            <tr class="trRawNCRPLUS1">
            
              <td onclick="collapse('{concat('trRawNCRPLUS_',@OrderDetailId)}');" colspan="5" style="border:solid 1px black;background-color:white;text-align:left;">
                <div style="float:left">
                  <div style="float:left;background-position:left;" class="plus_btnSmall">
                  </div>
                  <div >
                    Raw Courthouse Data
                  </div>
                </div>
              </td>
              
            </tr>
            <tr class="trRawNCRPLUS2">
              
              <td colspan="5" style="border:solid 1px black;background-color:white;text-align:center;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr id="{concat('trRawNCRPLUS_',@OrderDetailId)}_b" style="display:none;">
                    <td style="background-color:white;">
                      <div id="divRawDataNCRPLUS">
                        <pre>
                          <xsl:value-of select ="$TextData"/>
                        </pre>
                      </div>
                    </td>
                  </tr>
                </table>
              </td>
              <td class="end"></td>
            </tr>
          </xsl:for-each>

        </tbody>
       
      </table>
    </div>


    <div class="sectionsummary">
      <h3 style="line-height: 1em;">

        Data Sources Searched:<br/>
        Court Records, Department of Corrections, Administrative Office of Courts,<br/>
        National Sex Offender Registry, OFAC, OIG<br/>
        For a complete list of coverage, please visit<br/>
        <a href="http://emerge.intelifi.com/coverage.pdf" target="_blank">US Criminal Rapid Search Coverage Guide</a>

      </h3>
    </div>
  </xsl:template>
</xsl:stylesheet>