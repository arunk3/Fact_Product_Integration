<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" encoding="iso-8859-1"/>
  <xsl:template match="/">
    <xsl:variable name="pkOrderDetailId" select="XML_INTERFACE/@OrderDetailId"></xsl:variable>
    <input type="text" id="txtTotalRecord_{($pkOrderDetailId)}" style="display:none" value="{(count(XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/response_data/response_row))}"/>
    <div class="sectionsummary">
      <h3>
        Request ID:

        <xsl:variable name="request_id" select="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/request_id"/>

        <xsl:choose>
          <xsl:when test="$request_id = ''">
            <xsl:text> No Records </xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/request_id" />
          </xsl:otherwise>
        </xsl:choose>

      </h3>
    </div>

    <center>

      <xsl:if test="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/rows_returned = '0'">
        <table class="reporttable" id="table" border="0" cellpadding="0" cellspacing="0" >
          <thead>
            <tr>
              <th colspan="100%">Search Results</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <!--<td >
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>-->
              <td colspan="100%" style="border-right: none;">
                <table>
                  <tr>
                    <td colspan="100%" class="recordHeader">
                      No Records Found.
                      <br />
                      * PASSED OFAC TEST *
                    </td>
                  </tr>
                </table>
              </td>
              <!--<td >
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>-->
            </tr>
          </tbody>
          <tfoot>
            <td ></td>
            <td style="border-right: none;" colspan="100%"></td>
            <td ></td>
          </tfoot>
        </table>

      </xsl:if>

      <table class="reporttable ofac-heading-text" id="table" border="0" cellpadding="0" cellspacing="0">
        <thead>
          <tr class="ncr_header_data">
            <th ></th>
            <th colspan="100%" style="color:#666;">Search Information</th>
            <th> <div style="float: right; width: 108px; padding: 5px 0px; color: rgb(102, 102, 102);">
              <span style="float: left;">Select All </span> <input type="checkbox" id="check_idAll_{($pkOrderDetailId)}"  onclick="SelectAllRecords('{(count(XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/response_data/response_row))}_{($pkOrderDetailId)}')"/>
            </div>
            </th>
          </tr>
        </thead>
        <tbody>
          <xsl:variable name="counter" select="0" />
          <xsl:for-each select="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/response_data/response_row">
            <xsl:if test="$counter + 1" />
            <tr>
              <td >
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              
              <td >
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td  colspan="100%" align="right" style="text-align:right;color:red;font-size:13px;font-weight:bold">
                <div style="float:right">
                  <div style="float:left">
                    <xsl:choose>
                      <xsl:when test="@Deleted='1'">
                        <input type="checkbox" id="chkIsDeleted_{(position())}_{($pkOrderDetailId)}" name="chkDelete_{(position())}" checked="checked"/>
                        <input type="checkbox" id="chkhdnIsDeleted_{(position())}_{($pkOrderDetailId)}" style="display:none" name="chkDelete_{(position())}" checked="checked"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <input type="checkbox" id="chkIsDeleted_{(position())}_{($pkOrderDetailId)}" name="chkDelete_{(position())}" />
                        <input type="checkbox" id="chkhdnIsDeleted_{(position())}_{($pkOrderDetailId)}" style="display:none" name="chkDelete_{(position())}"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </div>
                  <div style="float: right; margin-top: 5px; padding-right: 18px;">Delete</div>
                </div>
              </td>
            </tr>
            <tr>
              <td >
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colspan="100%" style="border-right: none;">
                <table class="">
                  <tr>
                    <td colspan="100%" class="recordHeader">
                      <div class="file-name" style="font-size: 12px; font-weight: bold; color: rgb(0, 0, 0); float: left;"> Record </div>
                      <span class="blue-no" style="padding-top: 5px; position: absolute; margin-top: -5px; margin-left: 11px;">
                        <xsl:value-of select="(position())" />
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <th>Score:</th>
                    <td>
                      <xsl:value-of select="score" />
                    </td>
                  </tr>
                  <tr>
                    <th>File Name:</th>
                    <td>
                      <xsl:value-of select="filename" />
                    </td>
                    <th>File Date:</th>
                    <td>
                      <xsl:value-of select="filedate" />
                    </td>
                  </tr>
                  <tr>
                    <th style="width: 80px; padding-top: 1px;">Entity Name:</th>
                    <td style="padding-top: 0px ! important; width: 150px;">
                      <xsl:value-of select="entityname" />
                    </td>
                    <th style="width: 80px; padding-top:1px;">Best Name:</th>
                    <td style="padding-top: 0px;">
                      <xsl:value-of select="bestname" />
                    </td>
                  </tr>
                  <tr>
                    <th>Listing:</th>
                    <td></td>
                    <td colspan="3" style="padding-top:0px; color:#666;">
                      <xsl:value-of select="listing" />
                    </td>
                  </tr>
                </table>
              </td>
              <td >
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr style="border-bottom:1px solid #ccc;">
              <td >
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td  colspan="100%" align="right" style="text-align:right;color:red;font-size:13px;font-weight:bold">
                <table style="width:100%">
                  <tr>
                    <td style="width:90%">
                      <textarea id="txtNote_{(position())}_{($pkOrderDetailId)}" rows="2" name="txtNote_{(position())}_{($pkOrderDetailId)}" style="width:100%;background-color:#FFFFE1;">
                        <xsl:value-of select="ReviewNote"/>
                      </textarea>
                      <textarea id="txthdnNote_{(position())}_{($pkOrderDetailId)}" rows="2" name="txtNote_{(position())}_{($pkOrderDetailId)}" style="width:100%;background-color:#FFFFE1;display:none">
                        <xsl:value-of select="ReviewNote"/>
                      </textarea>
                    </td>
                    <td align="right" valign="bottom" style="vertical-align:bottom">
                      <input type="button" id="btnUpdateRecord_OFAC_{(position())}" value="Save Changes" onclick="SetHiddenFields(this.id)" wrap="off" class="btn btn-default"/>
                    </td>
                  </tr>
                </table>
              </td>
              <td >
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:for-each>

          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">Australian Department of Foreign Affairs and Trade (DFAT)</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">Bank of England Consolidated List of Financial Sanctions</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">Bureau of Industry and Security</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">OSFI Terrorism Financing</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">DTC Debarred parties</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">European Union Consolidated List</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">FBI Hijack Suspects</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">FBI Most Wanted</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">FBI Most Wanted Terrorists</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">FBI Seeking Information</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">FBI Top Ten Most Wanted</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">HKMA</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">Interpol Most Wanted</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">MAS</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">Non-cooperative Countries and Territories</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">Non-proliferation</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">OFAC Sanctions Programs and Countries</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">OFAC's Specially Designated Nationals and Blocked Persons</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">Politically Exposed Persons</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">Primary Money Laundering Concern (PMLC)</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">Terrorist Exclusion List</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">Unauthorized Banks</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">United Nations Consolidated List</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">World Bank Debarred Parties</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
        </tbody>
        <!--<tfoot>
          <td ></td>
          <td style="border-right: none;" colspan="100%"></td>
          <td ></td>
        </tfoot>-->
      </table>
    </center>
  </xsl:template>

</xsl:stylesheet>
