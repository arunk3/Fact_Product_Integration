<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:output method="html" />
  <xsl:template match="/BGC/product/CountyCriminalSearch/fulfillment">
    <xsl:variable name="pkOrderDetailId" select="applicantInfo/@OrderDetailId"></xsl:variable>
    <input type="text" id="txtTotalRecord_{($pkOrderDetailId)}" style="display:none" value="{(count(applicantInfo))}"/>
    <br />
    <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
      <thead>
        <tr>
          <th class="begin"></th>
          <th colspan="5">Search Information</th>
          <th class="end"></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td class="begin">i</td>
          <td colspan="5">
            <xsl:for-each select="applicantInfo">
              <p>
                <b>SSN: </b>
                <xsl:value-of select="SSN"/><br />
                <b>DOB: </b>
                <xsl:value-of select="DOB/month"/>/<xsl:value-of select="DOB/day"/>/<xsl:value-of select="DOB/year"/><br />
              </p>
            </xsl:for-each>
          </td>
          <td class="end">
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </td>
        </tr>
      </tbody>
      <tfoot>
        <td class="begin"></td>
        <td colspan="5"></td>
        <td class="end"></td>
      </tfoot>
    </table>

    <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
      <thead>
        <tr>
          <th class="begin"></th>
          <th >Search Results
          <div style="float:right">
            Select All  <input type="checkbox" id="check_idAll_{($pkOrderDetailId)}"  onclick="SelectAllRecords('{(count(applicantInfo))}_{($pkOrderDetailId)}')"/>
          </div>
          </th>

          <th class="end"></th>
        </tr>
      </thead>
      <tbody>
        <xsl:for-each select="results/detail">
          <tr>
            <td colspan="3" align="right" style="text-align:right;color:red;font-size:13px;font-weight:bold">
              <div style="float:right">
                <div style="float:left">
                  <xsl:choose>
                    <xsl:when test="@Deleted='1'">
                      <input type="checkbox" id="chkIsDeleted_{(position())}_{($pkOrderDetailId)}" name="chkDelete_{(position())}" checked="checked"/>
                      <input type="checkbox" id="chkhdnIsDeleted_{(position())}_{($pkOrderDetailId)}" style="display:none" name="chkDelete_{(position())}" checked="checked"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <input type="checkbox" id="chkIsDeleted_{(position())}_{($pkOrderDetailId)}" name="chkDelete_{(position())}" />
                      <input type="checkbox" id="chkhdnIsDeleted_{(position())}_{($pkOrderDetailId)}" style="display:none" name="chkDelete_{(position())}"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </div>
                <div style="float:right;margin-top:2px">Delete</div>
              </div>
            </td>
          </tr>
          <tr>
            <xsl:if test="county != ''">
              <td style="font-weight: bold;">County</td>
              <td colspan="2">
                <xsl:value-of select="county"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <xsl:if test="state != ''">
              <td style="font-weight: bold;">State</td>
              <td colspan="2">
                <xsl:value-of select="state"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <xsl:if test="jurisdiction != ''">
              <td style="font-weight: bold;">Jurisdiction</td>
              <td colspan="2">
                <xsl:value-of select="jurisdiction"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <xsl:if test="nameFiled != ''">
              <td style="font-weight: bold;">Name Filed</td>
              <td colspan="2">
                <xsl:value-of select="nameFiled"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <xsl:if test="identifiers != ''">
              <td style="font-weight: bold;">Identifiers</td>
              <td colspan="2">
                <xsl:value-of select="identifiers"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <xsl:if test="charge != ''">
              <td style="font-weight: bold;">Charge</td>
              <td colspan="2">
                <xsl:value-of select="charge"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <xsl:if test="disposition != ''">
              <td style="font-weight: bold;">Disposition</td>
              <td colspan="2">
                <xsl:value-of select="disposition"/>
              </td>
            </xsl:if>
          </tr>
          <xsl:if test="fileDate/year != '1900'">
            <tr>
              <xsl:if test="fileDate != ''">
                <td style="font-weight: bold;">File Date</td>
                <td colspan="2">
                  <xsl:value-of select="fileDate/month"/>/<xsl:value-of select="fileDate/day"/>/<xsl:value-of select="fileDate/year"/>
                </td>
              </xsl:if>
            </tr>
          </xsl:if>
          <xsl:if test="offenseDate/year != '1900'">
            <tr>
              <xsl:if test="offenseDate != ''">
                <td style="font-weight: bold;">Offense Date</td>
                <td colspan="2">
                  <xsl:value-of select="offenseDate/month"/>/<xsl:value-of select="offenseDate/day"/>/<xsl:value-of select="offenseDate/year"/>
                </td>
              </xsl:if>
            </tr>
          </xsl:if>
          <xsl:if test="dispositionDate/year != '1900'">
            <tr>
              <xsl:if test="dispositionDate != ''">
                <td style="font-weight: bold;">Disposition Date</td>
                <td colspan="2">
                  <xsl:value-of select="dispositionDate/month"/>/<xsl:value-of select="dispositionDate/day"/>/<xsl:value-of select="dispositionDate/year"/>
                </td>
              </xsl:if>
            </tr>
          </xsl:if>
          <tr>
            <xsl:if test="agencyCaseNumber != ''">
              <td style="font-weight: bold;">Agency Case Number</td>
              <td colspan="2">
                <xsl:value-of select="agencyCaseNumber"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <xsl:if test="gradingResult != ''">
              <td style="font-weight: bold;">Grading Result</td>
              <td colspan="2">
                <xsl:value-of select="gradingResult"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <xsl:if test="comments != ''">
              <td style="font-weight: bold;">Comments</td>
              <td colspan="2">
                <xsl:value-of select="comments"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <xsl:if test="yearsSearched != ''">
              <td style="font-weight: bold;">Years Searched</td>
              <td colspan="2">
                <xsl:value-of select="yearsSearched"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <xsl:if test="offenseSeverity != ''">
              <td style="font-weight: bold;">Offense Severity</td>
              <td colspan="2">
                <xsl:value-of select="offenseSeverity"/>
                <xsl:if test="offenseSeverity = 'C'"> - Clear. No Records Found.</xsl:if>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <xsl:if test="probation != ''">
              <td style="font-weight: bold;">Probation</td>
              <td colspan="2">
                <xsl:value-of select="probation"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <xsl:if test="jailTime != ''">
              <td style="font-weight: bold;">Jail Time</td>
              <td colspan="2">
                <xsl:value-of select="jailTime"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <xsl:if test="prisonTime != ''">
              <td style="font-weight: bold;">Prison Time</td>
              <td colspan="2">
                <xsl:value-of select="prisonTime"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <xsl:if test="suspensionTime != ''">
              <td style="font-weight: bold;">Suspension Time</td>
              <td colspan="2">
                <xsl:value-of select="suspensionTime"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <xsl:if test="fines != ''">
              <td style="font-weight: bold;">Fines</td>
              <td colspan="2">
                <xsl:value-of select="fines"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <xsl:if test="courtCosts != ''">
              <td style="font-weight: bold;">Court Costs</td>
              <td colspan="2">
                <xsl:value-of select="courtCosts"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <xsl:if test="dobVerified != ''">
              <td style="font-weight: bold;">DOB Verified</td>
              <td colspan="2">
                <xsl:value-of select="dobVerified"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <xsl:if test="nameVerified != ''">
              <td style="font-weight: bold;">Name Verified</td>
              <td colspan="2">
                <xsl:value-of select="nameVerified"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <xsl:if test="ssnVerified != ''">
              <td style="font-weight: bold;">SSN Verified</td>
              <td colspan="2">
                <xsl:value-of select="ssnVerified"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <xsl:if test="verificationNotes != ''">
              <td style="font-weight: bold;">Verification Notes</td>
              <td colspan="2">
                <xsl:value-of select="verificationNotes"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <xsl:if test="countNotes != ''">
              <td style="font-weight: bold;">Count Notes</td>
              <td colspan="2">
                <xsl:value-of select="countNotes"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <xsl:if test="dispositionNotes != ''">
              <td style="font-weight: bold;">Disposition Notes</td>
              <td colspan="2">
                <xsl:value-of select="dispositionNotes"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <td colspan="3" >
              <table style="width:100%">
                <tr>

                  <td align="right" style="text-align:right;color:red;font-size:13px;font-weight:bold;width:85%">
                    <textarea id="txtNote_{(position())}_{($pkOrderDetailId)}" rows="2" name="txtNote_{(position())}_{($pkOrderDetailId)}" style="width:100%;background-color:#FFFFE1;">
                      <xsl:value-of select="ReviewNote"/>
                    </textarea>
                    <textarea id="txthdnNote_{(position())}_{($pkOrderDetailId)}" rows="2" name="txtNote_{(position())}_{($pkOrderDetailId)}" style="width:100%;background-color:#FFFFE1;display:none">
                      <xsl:value-of select="ReviewNote"/>
                    </textarea>
                  </td>
                  <td align="right" valign="bottom" style="vertical-align:bottom">
                    <input type="button" id="btnUpdateRecord_CCR2_{(position())}" value="Save Changes" onclick="SetHiddenFields(this.id)" wrap="off" style=" height:30px; width:110px; text-align:center; vertical-align:middle"/>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </xsl:for-each>


        <xsl:for-each select="notes/note">
          <tr>
            <td colspan="100%" style="font-weight: bold; border-bottom:1px dashed black;">Note:</td>
          </tr>
          <tr>
            <xsl:if test="id != ''">
              <td style="font-weight: bold;">ID</td>
              <td colspan="2">
                <xsl:value-of select="id"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <xsl:if test="text != ''">
              <td style="font-weight: bold;">Note</td>
              <td colspan="2">
                <xsl:value-of select="text"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <xsl:if test="createdDate != ''">
              <td style="font-weight: bold;">Date Created</td>
              <td colspan="2">
                <xsl:value-of select="createdDate"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <xsl:if test="createdByName != ''">
              <td style="font-weight: bold;">Create By (Name)</td>
              <td colspan="2">
                <xsl:value-of select="createdByName"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <xsl:if test="createdByClient != ''">
              <td style="font-weight: bold;">Created By (Client)</td>
              <td colspan="2">
                <xsl:value-of select="createdByClient"/>
              </td>
            </xsl:if>
          </tr>
        </xsl:for-each>
      </tbody>
    </table>
  </xsl:template>

  <xsl:template match="/BGC/product/CountyCriminalSearch/response">
    <div class="noRecords">
      No Records Found.
      <br />
      * CLEAR *
    </div>
  </xsl:template>
</xsl:stylesheet>
