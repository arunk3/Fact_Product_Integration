<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:output method="html" />
  <xsl:template match="/FastraxNetwork">
    <xsl:variable name="pkOrderDetailId" select="@OrderDetailId"></xsl:variable>
    <input type="text" id="txtTotalRecord_{($pkOrderDetailId)}" style="display:none" value="{(count(report-requested/report/report-detail-header/report-incident))}"/>
    <table id="peopleTable" class="reporttable manual-county" border="0" cellpadding="0" cellspacing="0" style="margin-left:5px;">
      <thead>
        <tr class="fcr_header_data">
          <th colspan="100%" class="user-bg" style="color:#666; padding-bottom: 8px;">Criminal Arrests and Convictions

          <div style="float:right">
            <span style="float: left; padding-right: 3px; padding-top: 1px;"> Select All</span>  <input type="checkbox" id="check_idAll_{($pkOrderDetailId)}"  onclick="SelectAllRecords('{(count(report-requested/report/report-detail-header/report-incident))}_{($pkOrderDetailId)}')"/>
          </div>
          </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>State:</td>
          <td>
            <xsl:value-of select="report-requested/report/report-detail-header/state" />
          </td>
          <td>Years Checked:</td>
          <td>
            <xsl:value-of select="report-requested/report/report-detail-header/time-checked" />
          </td>
          <td> </td>
          <td> </td>
        </tr>

        <tr>
          <td>County:</td>
          <td>
            <xsl:value-of select="report-requested/report/report-detail-header/county" />
          </td>
          <td>Convictions Admitted:</td>
          <td>
            <xsl:value-of select="report-requested/report/report-detail-header/felonies-admitted" />
          </td>
          <td> </td>
          <td> </td>
        </tr>

        <tr>
          <td>City:</td>
          <td></td>
          <td>Details Disclosed:</td>
          <td></td>
          <td> </td>
          <td> </td>
        </tr>

        <!--<tr>
          <td colspan="100%"> </td>
        </tr>-->

        <tr>
          <td>Record Found:</td>
          <td>
            <xsl:if test="report-requested/report/report-detail-header/clear-record = '0'">Yes</xsl:if>
            <xsl:if test="report-requested/report/report-detail-header/clear-record = '1'">No</xsl:if>
          </td>

          <td>Outstanding Capias:</td>
          <td>
            <xsl:if test="report-requested/report/report-detail-header/capias = '0'">No</xsl:if>
            <xsl:if test="report-requested/report/report-detail-header/capias = '1'">Yes</xsl:if>
          </td>

          <td>Verified By DOB:</td>
          <td>
            <xsl:if test="report-requested/report/report-detail-header/verified-by-dob = '0'">No</xsl:if>
            <xsl:if test="report-requested/report/report-detail-header/verified-by-dob = '1'">Yes</xsl:if>
          </td>
        </tr>

        <tr>
          <td></td>
          <td>
            
          </td>

          <td>Current Warrant:</td>
          <td>
            <xsl:if test="report-requested/report/report-detail-header/current-warrants = ''">No</xsl:if>
            <xsl:if test="report-requested/report/report-detail-header/current-warrants != ''">
              <xsl:value-of select="report-requested/report/report-detail-header/current-warrants" />
            </xsl:if>
          </td>

          <td>Verified By Name:</td>
          <td>
            <xsl:if test="report-requested/report/report-detail-header/verified-by-name = '0'">No</xsl:if>
            <xsl:if test="report-requested/report/report-detail-header/verified-by-name = '1'">Yes</xsl:if>
          </td>
        </tr>

        <tr>
          <td></td>
          <td>
            
          </td>

          <td>DUI's Found:</td>
          <td>
            <xsl:if test="report-requested/report/report-detail-header/dui = '0'">No</xsl:if>
            <xsl:if test="report-requested/report/report-detail-header/dui = '1'">Yes</xsl:if>
          </td>

          <td>Verified By SSN:</td>
          <td>
            <xsl:if test="report-requested/report/report-detail-header/verified-by-ssn = '0'">No</xsl:if>
            <xsl:if test="report-requested/report/report-detail-header/verified-by-ssn = '1'">Yes</xsl:if>
          </td>
        </tr>

        <tr>
          <td>Aliases Found:</td>
          <td>
            <xsl:if test="report-requested/report/report-detail-header/alias-found = '0'">No</xsl:if>
            <xsl:if test="report-requested/report/report-detail-header/alias-found = '1'">Yes</xsl:if>
          </td>

          <td>Aliasas:</td>
          <td>

          </td>

          <td></td>
          <td></td>
        </tr>

        <tr>
          <td>Other Criminal:</td>
          <td>

          </td>

          <td>Other Criminal Detail:</td>
          <td>

          </td>

          <td></td>
          <td></td>
        </tr>

        <tr>
          <td>Verified By Other:</td>
          <td>

          </td>

          <td>Verified By Other Detail:</td>
          <td>

          </td>

          <td></td>
          <td></td>
        </tr>
      </tbody>
    </table>

    <xsl:for-each select="report-requested/report/report-detail-header/report-incident">
      <table id="peopleTable" class="reporttable manual-county" border="0" cellpadding="0" cellspacing="0" style="margin-left:5px;">
        <thead>
          <tr class="fcr_header_data">
            <th colspan="2" align="right" style="width:50%; padding-bottom:8px; color:#666;" class="user-bg">Details</th>
            <th colspan="2" align="right" style="width:50%; padding-bottom:8px;" class="user-bg">
              <div style="float:right">
                <div style="float:left">
                  <xsl:choose>
                    <xsl:when test="@Deleted='1'">
                      <input type="checkbox" id="chkIsDeleted_{(position())}_{($pkOrderDetailId)}" name="chkDelete_{(position())}" checked="checked"/>
                      <input type="checkbox" id="chkhdnIsDeleted_{(position())}_{($pkOrderDetailId)}" style="display:none" name="chkDelete_{(position())}" checked="checked"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <input type="checkbox" id="chkIsDeleted_{(position())}_{($pkOrderDetailId)}" name="chkDelete_{(position())}" />
                      <input type="checkbox" id="chkhdnIsDeleted_{(position())}_{($pkOrderDetailId)}" style="display:none" name="chkDelete_{(position())}"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </div>
                <div style="float:right; margin-top:2px;text-align:right;color:red;font-size:13px;font-weight:bold">Delete</div>
              </div>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Charges</td>
            <td colspan="3">
              <xsl:value-of select="charges" />
            </td>
          </tr>
          <tr>
            <td>Disposition Date</td>
            <td>
              <xsl:value-of select="disposition-date" />
            </td>
            <td style="float: left; width: 25%;">Disposition</td>
            <td style="float: left; width: 65%;">
              <xsl:value-of select="disposition" />
            </td>
          </tr>
          <tr>
            <td>File Date</td>
            <td>
              <xsl:value-of select="file-date" />
            </td>
            <td style="float: left; width: 25%;">Case #</td>
            <td style="float: left; width: 65%;">
              <xsl:value-of select="case-number" />
            </td>
          </tr>
          <tr>
            <td>Sentence Date</td>
            <td>
              <xsl:value-of select="sentence-date" />
            </td>
            <td style="float: left; width: 25%;">Sentence</td>
            <td style="float: left; width: 65%;">
              <xsl:value-of select="sentence" />
            </td>
          </tr>
          <tr>
            <td>Comments</td>
            <td colspan="3">
              <xsl:value-of select="additional-comments" />
            </td>
          </tr>
            <tr>
              <td colspan="3" style="width:100%; padding:4px 0 4px 4px;">
                <textarea id="txtNote_{(position())}_{($pkOrderDetailId)}" rows="2" name="txtNote_{(position())}_{($pkOrderDetailId)}" style="width:100%;background-color:#FFFFE1;">
                  <xsl:value-of select="ReviewNote"/>
                </textarea>
                <textarea id="txthdnNote_{(position())}_{($pkOrderDetailId)}" rows="2" name="txtNote_{(position())}_{($pkOrderDetailId)}" style="width:100%;background-color:#FFFFE1;display:none">
                  <xsl:value-of select="ReviewNote"/>
                </textarea>
              </td>
              <td align="right" valign="bottom" style="vertical-align:bottom;width:10%">
                <input type="button" id="btnUpdateRecord_CCR1_{(position())}" value="Save Changes" onclick="SetHiddenFields(this.id)" wrap="off" style="display:none; height:30px; width:110px; text-align:center; vertical-align:middle"/>
              </td>
            </tr>
        </tbody>
      </table>
    </xsl:for-each>

  </xsl:template>
</xsl:stylesheet>
