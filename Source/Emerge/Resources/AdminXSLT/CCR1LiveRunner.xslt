﻿<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:a="http://ns.hr-xml.org/2004-08-02">
	<xsl:output method="html" encoding="iso-8859-1" />
	<xsl:template match="/">
		<xsl:variable name="pkOrderDetailId" select="a:BackgroundReports/@OrderDetailId"></xsl:variable>
		<input type="text" id="txtTotalRecord_{($pkOrderDetailId)}" style="display:none" value="{(count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase))}"/>
		<div class="people-search-wrap" style="margin-bottom: 5px;">
			<div class="sectionsummary" style="border: 1px solid #9b0000; margin-bottom: 10px; padding-bottom: 0; padding-left: 10px; text-align: left;">
				<h3>
					(<xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)" />) Results from LiveRunner Search.<br />
				</h3>
			</div>

			<xsl:variable name="TotalRecords">
				<xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)" />
			</xsl:variable>

			<xsl:if test="$TotalRecords=0">

				<table id="peopleTable" class="" border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 10px; background: #f7f7f7 none repeat scroll 0px 0px;">
					<thead>
						<tr>
              <td class="ncr_header_data" style="width: 1px ! important;"></td>
							<th colspan="5" class="ncr_header_data" style="width: 98%; padding-left:0px;">
								<span style="font-size:15px;font-weight:bold; color:#666;">Search Information</span>
                <td class="ncr_header_data" style="width: 1px ! important;"></td>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr  style="border-bottom: medium none;">
              <td style="width: 1px;">
								<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
							</td>
							<td colspan="5">
								<h4>Name:</h4>
								<h4>
									<xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:GivenName"/>
									<xsl:text xml:space="preserve">  </xsl:text>
									<xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:FamilyName"/>
								</h4>
								<br />
								<h4>DOB:</h4>
								<h4>
									<xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:DemographicDetail/a:DateOfBirth"/>
								</h4>
							</td>
              <td style="width: 1px;">
								<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
							</td>
						</tr>
						<tr>
              <td style="width: 1px;">
								<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
							</td>
							<td colspan="5">
								<pre>
									<xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus != 'Fulfilled'">
										<p>*** Clear ***</p>
									</xsl:if>
									<xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus = 'Fulfilled'">
										<p>*** INCOMPLETE ***</p>
									</xsl:if>
								</pre>
							</td>
              <td style="width: 1px;">
								<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
							</td>
						</tr>
					</tbody>
					
				</table>

			</xsl:if>

			<table id="tblRCX" class="reporttable" border="0" cellpadding="0" cellspacing="0">
				<thead>
					<tr>
						<th colspan="9"  style="padding: 0px;">
							<div class="fcr_header_data" style="height: 35px ! important;">
								<div class="txt-sumry" style=" color:#666; font-size:15px;"> Criminal Results </div>

								<div style="float:right">
                  <span class="selectallalign" style="display: inline-block; float: left; margin-top: 1px; font-size:14px; color:#666; margin-right: 5px;">	Select All</span>  <input type="checkbox" id="check_idAll_{($pkOrderDetailId)}"  onclick="SelectAllRecords('{($TotalRecords)}_{($pkOrderDetailId)}')"/>
								</div>
							</div>
						</th>
					</tr>
				</thead>
				<tbody>

					<xsl:variable name="TextData">
						<xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text" />
					</xsl:variable>
					<xsl:if test="$TextData = ''">
						<pre>
							<xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus != 'Fulfilled'">
								<p>*** Clear ***</p>
							</xsl:if>
							<xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus = 'Fulfilled'">
								<p>*** INCOMPLETE ***</p>
							</xsl:if>
						</pre>
					</xsl:if>
					<xsl:variable name="IsLosAngeles">
						<xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/@qualifier" />
					</xsl:variable>
					<xsl:variable name="SSNNo">
						xxx-xx-<xsl:value-of select="substring(a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:DemographicDetail/a:GovernmentId,6,4)" />
					</xsl:variable>

					<xsl:if test="not(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text)">
						<tr>
							<td colspan="5">
								<div style="width: 90%; margin: 0 auto; text-align: left">
									<div style="width:588px; text-align:left">
										<div style="font-size:11px;" id="report_RCX">
											<pre>
												<!--<xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text">
                        <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text" disable-output-escaping="yes"/>
                      </xsl:if>-->
												<xsl:if test="not(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text)">
													<xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus = 'Cancelled'">
														<p>
															There was an error running this report.<br /> Please contact our customer support team to quickly resolve this issue.
														</p>
													</xsl:if>
													<xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus != 'Cancelled' and a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus != 'Completed' and a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus != 'Fulfilled'">
														<p>*** Clear ***</p>
													</xsl:if>
													<xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus = 'Fulfilled'">
														<p>*** INCOMPLETE ***</p>
													</xsl:if>
													<p></p>
												</xsl:if>
											</pre>
										</div>
									</div>
								</div>
							</td>
							<td>
								<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
							</td>
						</tr>
					</xsl:if>

					<xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase">

						<xsl:choose>
							<xsl:when test="position()>1">
								<tr style="height:20px;">
									<td colspan="5" style="text-align:center;">
										<span style="font-size:16px;font-weight:bold;color:white;">
										</span>
									</td>
								</tr>
							</xsl:when>
						</xsl:choose>

						<tr>
							<td colspan="3" class="bdrBT"  style="width: 100%; padding-left: 20px ! important;">




								<div class="ucrs-details user-bg ncr1-xslt" style="padding:15px;">
									<div class="file-name">CRIMINAL RECORD</div>

									<span class="red-no" style="padding-top:6px;">
										
											<xsl:value-of select="position()" />
										
									</span>
									<div>

										<xsl:if test="@WatchList">
											<div style="float:right">
												<div style="float:right; margin-top:2px">Record from 7 Year Filter</div>
											</div>
										</xsl:if>
									</div>
									<div style="float:right;" class="nc-del">
										<div style="float:left">
											<xsl:choose>
												<xsl:when test="@Deleted='1'">
													<input type="checkbox" id="chkIsDeleted_{(position())}_{($pkOrderDetailId)}" name="chkDelete_{(position())}" checked="checked"/>
													<input type="checkbox" id="chkhdnIsDeleted_{(position())}_{($pkOrderDetailId)}" style="display:none" name="chkDelete_{(position())}" checked="checked"/>
												</xsl:when>
												<xsl:otherwise>
													<input type="checkbox" id="chkIsDeleted_{(position())}_{($pkOrderDetailId)}" name="chkDelete_{(position())}"/>
													<input type="checkbox" id="chkhdnIsDeleted_{(position())}_{($pkOrderDetailId)}" style="display:none" name="chkDelete_{(position())}"/>
												</xsl:otherwise>
											</xsl:choose>
										</div>
										<div style="float:right; margin-top:4px; margin-left:5px; color:red !important;">Delete</div>
									</div>

									<div class="clearfix"> </div>
								</div>
								<div class="other-details usc-table">
									<ul style="width: 36%; float:left;">
										<li class="clearfix">
											<div class="width_150 text-right"> SOURCE :</div>
											<div>
												<xsl:value-of select="a:CourtName"/>
											</div>
										</li>
										<li class="clearfix">
											<div class="width_150 text-right"> FULL NAME :</div>
											<div>
												<xsl:for-each select="a:SubjectIdentification/a:PersonName">
													<xsl:if test="a:FamilyName/@primary='true'">
														<xsl:value-of select="a:FamilyName"/>
														<xsl:if test="(a:GivenName)">
															<xsl:text>, </xsl:text>
														</xsl:if>
														<xsl:value-of select="a:GivenName"/>
														<xsl:if test="(a:MiddleName)">
															<xsl:text>, </xsl:text>
														</xsl:if>
														<xsl:value-of select="a:MiddleName"/>
														<xsl:element name="br"/>
													</xsl:if>
												</xsl:for-each>
											</div>
										</li>



										<xsl:if test="(a:SubjectIdentification/a:PersonName/a:FamilyName/@primary='false')">
											<li class="clearfix">
												<div class="width_150 text-right">AKA Name:</div>
												<div>
													<xsl:for-each select="a:SubjectIdentification/a:PersonName">
														<xsl:if test="a:FamilyName/@primary='false'">
															<xsl:value-of select="a:FamilyName"/>
															<xsl:if test="(a:GivenName)">
																<xsl:text>, </xsl:text>s
															</xsl:if>
															<xsl:value-of select="a:GivenName"/>
															<xsl:if test="(a:MiddleName)">
																<xsl:text>, </xsl:text>
															</xsl:if>
															<xsl:value-of select="a:MiddleName"/>
															<xsl:element name="br"/>
														</xsl:if>
													</xsl:for-each>
												</div>
											</li>
										</xsl:if>



										<li class="clearfix">
											<div class="width_150 text-right"> DOB RECORDED :</div>
											<div>
												<xsl:value-of select='substring(a:SubjectIdentification/a:DemographicDetail/a:DateOfBirth,1,10)' />
												<xsl:value-of select='a:SubjectIdentification/a:DemographicDetail/a:Age' />
												<xsl:value-of select='a:SubjectIdentification/a:DemographicDetail/a:Other' />
											</div>
										</li>

										<li class="clearfix" style="color:#f00;">
											<div class="dob-block" >
												DOB ENTERED BY USER :
											</div>
											<div>
												<span class="lblDateEnteredByUser" id="lblDateEnteredByUser_{(position())}"></span>
											</div>
										</li>



									</ul>
									<ul style="width: 47%; float:left;">

										<xsl:if test="(a:SubjectIdentification/a:DemographicDetail/a:GovernmentId/@documentType='DRIVERS LICENSE')">
											<li class="clearfix">
												<div>DL# :</div>
												<div>
													<xsl:value-of select ="a:SubjectIdentification/a:DemographicDetail/a:GovernmentId[@documentType='DRIVERS LICENSE']"/>
												</div>
											</li>
										</xsl:if>


										<xsl:if test="(a:SubjectIdentification/a:PostalAddress)">

											<li class="clearfix">
												<div> ADDRESS :</div>
												<div>
													<xsl:for-each select="a:SubjectIdentification/a:PostalAddress">
														<xsl:if test="(a:DeliveryAddress)">
															<xsl:value-of select ="a:DeliveryAddress"/>
															<xsl:if test="(a:Municipality)">
																<xsl:text>, </xsl:text>
															</xsl:if>
														</xsl:if>
														<xsl:if test="(a:Municipality)">
															<xsl:value-of select ="a:Municipality"/>
															<xsl:if test="(a:Region)">
																<xsl:text>, </xsl:text>
															</xsl:if>
														</xsl:if>
														<xsl:if test="(a:Region)">
															<xsl:value-of select ="a:Region"/>
															<xsl:if test="(a:PostalCode)">
																<xsl:text> - </xsl:text>
															</xsl:if>
														</xsl:if>
														<xsl:if test="(a:PostalCode)">
															<xsl:value-of select ="a:PostalCode"/>
														</xsl:if>
														<xsl:element name="br" />
													</xsl:for-each>
												</div>
											</li>
										</xsl:if>
										<li class="clearfix">
											<div> RACE :</div>
											<div>
												<xsl:value-of select="a:SubjectIdentification/a:DemographicDetail/a:Race"/>
											</div>
										</li>

										<li class="clearfix">
											<div > GENDER :</div>
											<div>
												<xsl:choose>
													<xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:GenderCode = 1">
														MALE
													</xsl:when>
													<xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:GenderCode = 2">
														FEMALE
													</xsl:when>
													<xsl:otherwise>UNKNOWN</xsl:otherwise>
												</xsl:choose>
											</div>
										</li>


										<xsl:if test="(a:AgencyReference[@type='Docket'])">

											<li class="clearfix">
												<div > DOCKET NUMBER :</div>
												<div>
													<xsl:value-of select="a:AgencyReference[@type='Docket']"/>
												</div>
											</li>


										</xsl:if>
										<xsl:if test="a:CaseFileDate">
											<li class="clearfix">
												<div> CASE FILED:</div>
												<div>
													<xsl:choose>
														<xsl:when test="count(a:CaseFileDate) &gt; 1">
															<xsl:for-each select="a:CaseFileDate">
																<span style="float:left;">
																	<xsl:value-of select='.' />
																	<xsl:element name="br" />
																</span>
															</xsl:for-each>
														</xsl:when>
														<xsl:when test="count(a:CaseFileDate)=1">
															<xsl:value-of select='substring(a:CaseFileDate,1,10)' />
														</xsl:when>
													</xsl:choose>
												</div>
											</li>
										</xsl:if>





										<li class="clearfix">
											<div> ADDITIONAL EVENT :</div>
											<div>
												<xsl:if test="count(a:AdditionalItems/a:EffectiveDate) &gt; 0">
													<input id="imageColapse" type="image" src="../content/themes/base/images/plus_symbol.png" style="width:10px; height:10px;" value="myValue" alt="" />
												</xsl:if>
												<div style="display:block;">

													<xsl:for-each select="a:AdditionalItems/a:EffectiveDate">
														<xsl:value-of  select='../a:Text' />
														<xsl:text xml:space="preserve">  </xsl:text>
														<xsl:value-of  select='substring(a:StartDate/a:AnyDate,1,10)' />
														<xsl:element name="br" />
													</xsl:for-each>
												</div>
											</div>

										</li>
									</ul>

									<div style="width:auto; float:right;">
										<ul>
											<xsl:if test="(a:MatchMeter)">
												<xsl:text xml:space="preserve">  </xsl:text>
												<xsl:if test="(a:MatchMeter/a:Name) = '2'">
													<li>
														<span class="btn-rv rv-green" style="padding-top: 2px; text-align:center;">Name Match</span>
													</li>
												</xsl:if>
												<xsl:if test="(a:MatchMeter/a:Name) = '1'">
													<li>
														<span class="btn-rv rv-yellow" style="padding-top: 2px; text-align:center;">Name Match</span>
													</li>
												</xsl:if>
												<xsl:if test="(a:MatchMeter/a:Name) = '0'">
													<li>
														<span class="btn-rv rv-red" style="padding-top: 2px; text-align:center;">Name Match</span>
													</li>
												</xsl:if>







												<xsl:text xml:space="preserve">  </xsl:text>
												<xsl:if test="(a:MatchMeter/a:Address) = '2'">
													<li>
														<span class="btn-rv rv-green" style="padding-top: 2px; text-align:center;">Address Match</span>
													</li>
												</xsl:if>
												<xsl:if test="(a:MatchMeter/a:Address) = '4'">
													<li>
														<span class="btn-rv rv-blue" style="padding-top: 2px; text-align:center;">Address Match</span>
													</li>
												</xsl:if>
												<xsl:if test="(a:MatchMeter/a:Address) = '1'">
													<li>
														<span class="btn-rv rv-yellow clearfix" style="padding-top: 2px; text-align:center;">Address Match</span>
													</li>
												</xsl:if>
												<xsl:if test="(a:MatchMeter/a:Address) = '0'">
													<li>
														<span class="btn-rv rv-red" style="padding-top: 2px; text-align:center;">Address Match</span>
													</li>
												</xsl:if>
												<xsl:if test="(a:MatchMeter/a:Address) = '-1'">
													<li>
														<span class="btn-rv rv-grey" style="padding-top: 2px; text-align:center;">Address Match</span>
													</li>
												</xsl:if>


												<xsl:text xml:space="preserve">  </xsl:text>



												<xsl:if test="(a:MatchMeter/a:DOB) = '2'">
													<li>
														<span class="btn-rv rv-green" style="padding-top: 2px; text-align:center;">DOB Match</span>
													</li>
												</xsl:if>
												<xsl:if test="(a:MatchMeter/a:DOB) = '1'">
													<li>
														<span class="btn-rv rv-yellow clearfix" style="padding-top: 2px; text-align:center;">DOB Match</span>
													</li>
												</xsl:if>
												<xsl:if test="(a:MatchMeter/a:DOB) = '0'">
													<li>
														<span class="btn-rv rv-red" style="padding-top: 2px; text-align:center;">DOB Match</span>
													</li>
												</xsl:if>





											</xsl:if>
										</ul>
									</div>


								</div>


								<xsl:for-each select="a:Charge">
									<div class="clearfix"></div>
									<div class="ucrs-details usc-head ncr1-xslt">
										<div class="file-name">
											CHARGE :
										</div>
										<span class="blue-no" style="padding-top:6px;">
												<xsl:value-of select="position()" />
											


										</span>
										<ul class="name col1">
											<li>
												<div class="width_150 text-right">Charge :</div>
												<div class="last-box">
													<xsl:value-of select="a:ChargeOrComplaint"/>
												</div>
											</li>
											<xsl:if test="(a:ChargeDescription)">
												<li>
													<div class="width_150 text-right">Penal Code Description :</div>
													<div>
														<xsl:value-of select="a:ChargeDescription" />
													</div>
												</li>
											</xsl:if>
											<xsl:if test="(a:ChargeDate)">
												<li>
													<div class="width_150 text-right">Charge Date:</div>
													<div>
														<xsl:value-of select='substring(a:ChargeDate,1,10)' />
													</div>
												</li>
											</xsl:if>
											<xsl:if test="(a:ArrestDate)">
												<li>
													<div class="width_150 text-right"> Arrest Date:</div>
													<div>
														<xsl:value-of select='substring(a:ArrestDate,1,10)' />
													</div>
												</li>
											</xsl:if>
											<xsl:if test="(a:OffenseDate)">
												<li>
													<div class="width_150 text-right">Offense Date:</div>
													<div>
														<xsl:value-of select='substring(a:OffenseDate,1,10)' />
													</div>
												</li>
											</xsl:if>
											<xsl:if test="(a:DispositionDate)">
												<li>
													<div class="width_150 text-right">Disposition Date:</div>
													<div>
														<xsl:value-of select='substring(a:DispositionDate,1,10)' />
													</div>
												</li>
											</xsl:if>
											<xsl:if test="(../a:AdditionalItems[@type='TrialDate'])">
												<li>
													<div class="width_150 text-right">Trial Date:</div>
													<div>
														<xsl:value-of select="substring(../a:AdditionalItems[@type='TrialDate'],1,10)" />
													</div>
												</li>
											</xsl:if>
										</ul>
										<ul class="ucrs-user col2">
											<xsl:if test="(a:ChargeTypeClassification)!='unknown'">
												<li>
													<div>
														Severity:
													</div>
													<div>
														<xsl:value-of select="a:ChargeTypeClassification"/>
													</div>
												</li>
											</xsl:if>
											<xsl:if test="(a:Disposition)">
												<li>
													<div>
														Final Disposition :
													</div>
													<div>
														<xsl:choose>
															<xsl:when test="(a:Disposition)='unknown'">
																Manual County or State Report needed for Final Results
															</xsl:when>
															<xsl:otherwise>
																<xsl:choose>
																	<xsl:when test="(contains(../a:CourtName, 'SAN BERNARDINO') or contains(../a:CourtName, 'RIVERSIDE')) and (a:Disposition)='NOT CONVICTED'">
																		Unknown
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:value-of select="a:Disposition"/>
																	</xsl:otherwise>
																</xsl:choose>
															</xsl:otherwise>
														</xsl:choose>
													</div>
												</li>
											</xsl:if>
											<xsl:if test="(a:Sentence)">
												<li>
													<div>
														Sentence :
													</div>
													<div>
														<xsl:value-of select='(a:Sentence)' />
													</div>
												</li>
											</xsl:if>
											<xsl:if test="(a:Comment)">
												<li>
													<div>
														Comments:
													</div>
													<div style="width:58%; float:left;">
														<xsl:for-each select="a:Comment">

															<!--<span style="float:left;">-->
															<xsl:value-of select="."/>
															<xsl:element name="br" />
															<!--</span>-->
														</xsl:for-each>
													</div>
												</li>
											</xsl:if>
										</ul>
										<div class="clearfix"> </div>
									</div>




								</xsl:for-each>
							</td>
						</tr>
						<tr>
							<td colspan="6" style="width: 100%; padding-left: 20px;">
								<textarea id="txtNote_{(position())}_{($pkOrderDetailId)}" rows="2" name="txtNote_{(position())}_{($pkOrderDetailId)}" style="width:100%;background-color:#FFFFE1;">
									<xsl:value-of select="a:ReviewNote"/>
								</textarea>
								<textarea id="txthdnNote_{(position())}_{($pkOrderDetailId)}" rows="2" name="txtNote_{(position())}_{($pkOrderDetailId)}" style="width:100%;background-color:#FFFFE1;display:none">
									<xsl:value-of select="a:ReviewNote"/>
								</textarea>
							</td>
							<td align="right" valign="bottom" style="vertical-align:bottom">
								<input type="button" id="btnUpdateRecord_CCR1_{(position())}" value="Save Changes" onclick="SetHiddenFields(this.id)" wrap="off" style=" height:30px; width:110px; text-align:center; vertical-align:middle; display:none;"/>
							</td>
						</tr>
					</xsl:for-each>

					<xsl:for-each select="a:BackgroundReports">
						<tr id="trRawCCR11">
							<td onclick="collapse('{concat('trRawCCR1_',@OrderDetailId)}');" colspan="5" style="border:solid 1px black;background-color:white;text-align:left;">
								<div style="float:left">
									<div style="float:left;background-position:left;" class="plus_btnSmall">
									</div>
									<div style="font-size:17px;font-weight:bold;color:#000;width:300px; margin-top:2px">
										Raw Courthouse Data
									</div>
								</div>
							</td>
						</tr>
						<tr id="trRawCCR12">
							<td colspan="5" style="border:solid 1px black;background-color:white;text-align:center;">
								<table border="0" cellpadding="0" cellspacing="0" width="100%">
									<tr id="{concat('trRawCCR1_',@OrderDetailId)}_b" style="display:none;">
										<td style="background-color:white;">
											<div id="divRawDataCCR1">
												<pre>
													<xsl:value-of select ="$TextData"/>
												</pre>
											</div>
										</td>
									</tr>
								</table>
							</td>
							<!--<td class="end"></td>-->
						</tr>
					</xsl:for-each>

				</tbody>

			</table>
		</div>
	</xsl:template>
</xsl:stylesheet>