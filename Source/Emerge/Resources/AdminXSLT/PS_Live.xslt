<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:a="http://ns.hr-xml.org/2004-08-02">
	<xsl:param name="admin" select="defaultstring" />
	<xsl:output method="html"/>
	<xsl:template match="/">

		<xsl:variable name="pkOrderDetailId" select="a:BackgroundReports/@OrderDetailId"></xsl:variable>
		<xsl:if test="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile) &gt; '0'">
			<input type="text" id="txtTotalRecord_{($pkOrderDetailId)}" style="display:none" value="{(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/@recordCount)}"/>
		</xsl:if>
		<xsl:if test="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile) ='0'">
			<input type="text" id="txtTotalRecord_{($pkOrderDetailId)}" style="display:none" value="0"/>
		</xsl:if>
		<xsl:if test="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:SSNStatus)&gt;'0'">

			<table cellpadding="2" cellspacing="0" style="margin: 0px;  border:none;" align="center" class="PS_main">
				<tr>
					<td class="PS_BORDER" width="50%">
						<span class="PS_SSN_DEATH_TOP" style="font-weight:normal; text-decoration:none; width:157px; padding-left: 35px;">
							SSN ISSUANCE :
						</span >
						<span class="PS_SSN_DEATH_BOTTOM" style="font-weight:bold;">
							<xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:SSNStatus"/>
						</span>
					</td>
				</tr>
				<tr>
					<td class="PS_BORDER">
						<span class="PS_SSN_DEATH_TOP" style="font-weight:normal; text-decoration:none; width:147px; display: inline-block;padding-left: 35px;">
							DEATH INDEX :
						</span>
						<span class="PS_SSN_DEATH_BOTTOM" style="font-weight:bold;">
							<xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:DateOfDeath"/>
						</span>
					</td>
				</tr>
			</table>

		</xsl:if>

		<br/>

		<xsl:if test="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile) ='0'">
			<table cellpadding="2" cellspacing="0" style="margin: 0px; border:none;" align="center" class="PS_main">
				<thead>
					<tr>
						<th class="begin"></th>
						<th colspan="6">Search Information</th>
						<th class="end"></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="begin"></td>
						<td colspan="6">
							<xsl:if test="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:AdditionalItems/a:Text) &gt;'0'">
								<xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:AdditionalItems/a:Text" />
								<!--Address trace service error: Input Error:Too Many Hits.Please refine your search.-->
							</xsl:if>
							<xsl:if test="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:AdditionalItems/a:Text) ='0'">
								No Records Found.
							</xsl:if>
						</td>
						<td class="end">
							<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
						</td>
					</tr>
				</tbody>
				<tfoot>
					<td class="begin"></td>
					<td colspan="6"></td>
					<td class="end"></td>
				</tfoot>
			</table>
		</xsl:if>
		<xsl:if test="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile) &gt;'0'">
			<table cellpadding="2" cellspacing="0" style="margin: 0px; width: 100%; border:none; background:#f7f5f5;" align="center" class="PS_main">
				<tr>
					<td class="PS_BORDER GetNameClass" valign="top" style="padding: 0;">
						<xsl:variable name="unique-listName" select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PersonName/a:FormattedName[not(.=following::a:FormattedName)]" />

						<span class="PS_SSN_DEATH_TOP" style="background: #fff none repeat scroll 0 0; display: block; padding-bottom: 8px; text-decoration: none; line-height: 27px;">
							<span class="nameTotalCount countr" style="background-color: rgb(0,120,189);color: white; margin: 0 5px 0 10px; border:none;">
								<xsl:value-of select="count($unique-listName)"/>
							</span>REPORTED NAMES
						</span >
						<xsl:for-each select="$unique-listName">
							<span class="up_unique">
								<span class="unique">
									<xsl:value-of select="." />
								</span>
								<br/>
							</span>
						</xsl:for-each>

						<!--<xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PersonName" >
            <span class="PS_SSN_DEATH_BOTTOM">
              <xsl:if test="a:FamilyName!='' and a:GivenName !=''">
              <xsl:value-of select="a:FamilyName" />, <xsl:value-of select="a:GivenName" /><br/>
              </xsl:if>
            </span>
          </xsl:for-each>-->
					</td>
					<td class="PS_BORDER" valign="top"  style="padding: 0;">
						<xsl:variable name="unique-listDates" select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:DemographicDetail/a:DateOfBirth[not(.=following::a:DateOfBirth or string-length(.) != '10' )]" />
						<span class="PS_SSN_DEATH_TOP" style="background: #fff none repeat scroll 0 0; display: block; padding-bottom: 8px; text-decoration: none; line-height: 27px;">
							<!--REPORTED BIRTHDATES ( <xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:DemographicDetail/a:DateOfBirth)"/> )-->
							<span class="countr" style="background-color: rgb(0,120,189);color: white; margin: 0 5px 0 10px;border:none;">

								<xsl:value-of select="count($unique-listDates)"/>
							</span>REPORTED DATE OF BIRTH

						</span>
						<xsl:for-each select="$unique-listDates">
							<span class="ultimatecls">
								<span class="ultimateclshelp">
									<xsl:value-of select="concat(substring(.,6,2),'/',substring(.,9,2),'/',substring(.,1,4))" />
								</span>
							</span>
						</xsl:for-each>


						<!--<xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:DemographicDetail" >
            <span class="PS_SSN_DEATH_BOTTOM">
              <xsl:value-of select="a:DateOfBirth"/>
              <br/>
            </span>
          </xsl:for-each>-->
					</td>
					<td class="PS_BORDER" width="33%" valign="top"  style="padding: 0;">
						<xsl:variable name="unique-ListCounty" select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PostalAddress/a:County[not(.=following::a:County)]" />
						<span class="PS_SSN_DEATH_TOP" style="background: #fff none repeat scroll 0 0; display: block; padding-bottom: 8px; text-decoration: none; line-height: 27px;">
							<!--REPORTED COUNTIES ( <xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PostalAddress/a:County)"/> )-->
							<span class="countr" style="background-color: rgb(0,120,189);color: white; margin: 0 5px 0 10px;border:none;">
								<xsl:value-of select="count($unique-ListCounty)"/>
							</span>
							REPORTED COUNTIES
						</span >
						<xsl:variable name="unique-list" select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PostalAddress" />
						<xsl:for-each select="$unique-list">
							<xsl:if test="a:County[not(.=following::a:County)]">
								<xsl:value-of select="a:County"/>
								<xsl:text>, </xsl:text>
								<xsl:value-of select="a:Region"/>
								<br/>
							</xsl:if>
						</xsl:for-each>
						<!--<xsl:variable name="unique-list" select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PostalAddress/a:County[not(.=following::a:County)]" />
          <xsl:for-each select="$unique-list">
            <xsl:value-of select="." />
            <br/>
            
          </xsl:for-each>-->


						<!--<xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PostalAddress[not(.=preceding::a:PostalAddress)]" >
            <span class="PS_SSN_DEATH_BOTTOM">
              <xsl:value-of select="a:County"/>
              <br/>
            </span>
          </xsl:for-each>-->
					</td>


				</tr>
			</table>

			<br/>
			<br/>


			<div class="sectionsummary">
				<h3>
					(<xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/@recordCount" />) Results PEOPLE SEARCH
					<xsl:value-of select="$pkOrderDetailId"></xsl:value-of>
				</h3>
				<br />
				<h3>Name:</h3>
				<h4>
					<span class="capname">
						<xsl:value-of select="concat(a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:GivenName,', ',a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:FamilyName)" />
					</span>
				</h4>
				<br/>
			</div>

			<center>

				<xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/@recordCount = '0'">
					<table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0" style="border:none;">
						<thead>
							<tr>
								<th class="begin"></th>
								<th colspan="6">Search Information</th>
								<th class="end"></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="begin">i</td>
								<td colspan="6">
									No Records Found.
								</td>
								<td class="end">
									<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
								</td>
							</tr>
							<!--<tr>
              <td class="begin">
              </td>
              <td colspan="2" style="background-color:#fff;width:85%">
                <textarea id="txtNoteNRF_PS" rows="2" name="txtNote_{(position())}" style="width:100%;background-color:#FFFFE1;">
                  <xsl:value-of select="a:ReviewNote"/>
                </textarea>
              </td>
              <td align="right" valign="bottom" style="background-color:#fff;vertical-align:bottom">
                <input type="button" id="PS" value="Save Changes" onclick="SetHiddenFieldsForNRF(this.id)" wrap="off" style=" height:30px; width:110px; text-align:center; vertical-align:middle"/>
              </td>
              <td style="background-color:#fff;" class="end">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>-->
						</tbody>
						<tfoot>
							<td class="begin"></td>
							<td colspan="6"></td>
							<td class="end"></td>
						</tfoot>
					</table>
				</xsl:if>

				<xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/@recordCount != '0'">
					<table id="peopleTable" class="reporttab" border="0" cellpadding="0" cellspacing="0">
						<thead>
              <tr class="ncr_header_data">
								<th class="begin"></th>
								<th align="left" style="padding-bottom: 8px; padding-top: 6px;">
									PERSONAL INFO
								</th>
								<th align="left" style="padding-bottom: 8px; padding-top: 6px;">
									ADDRESS INFO
								</th>
								<th align="left" style="padding-bottom: 8px; padding-top: 6px;">
									PHONE INFO
								</th>
                <th style="padding-bottom: 8px; padding-top: 6px;">
                  <span style="display: inline-block; vertical-align: 4px ! important;">Select All </span> <input type="checkbox" id="check_idAll_{($pkOrderDetailId)}"  onclick="SelectAllRecords('{a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/@recordCount}_{($pkOrderDetailId)}')"/>
								</th>
								<th class="end"> </th>
							</tr>
						</thead>
						<tbody>
							<xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData">

								<xsl:choose>
									<xsl:when test="(position() mod 2 = 0)">
										<tr class="peoplesearchpart" style="border: medium none;">
                    <td class="begin">
                    </td>
                    <td colspan="4" align="right" style="background-color:#fff; text-align:right;color:red;font-size:13px;font-weight:bold">
                      <div style="background-color:#fff;float:right">
                        <div style="float:left">
                          <xsl:choose>
                            <xsl:when test="@Deleted='1'">
                              <input type="checkbox" id="chkIsDeleted_{(position())}_{($pkOrderDetailId)}" name="chkDelete_{(position())}" checked="checked"/>
                              <input type="checkbox" id="chkhdnIsDeleted_{(position())}_{($pkOrderDetailId)}" style="display:none" name="chkDelete_{(position())}" checked="checked"/>
                            </xsl:when>
                            <xsl:otherwise>
                              <input type="checkbox" id="chkIsDeleted_{(position())}_{($pkOrderDetailId)}" name="chkDelete_{(position())}"/>
                              <input type="checkbox" id="chkhdnIsDeleted_{(position())}_{($pkOrderDetailId)}" style="display:none" name="chkDelete_{(position())}"/>
                            </xsl:otherwise>
                          </xsl:choose>
                        </div>
                        <div style="float:right;margin-top:2px">Delete</div>
                      </div>
                    </td>
                    <td style="background-color:#fff;" class="end">
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                    </td>
                  </tr>
                    <tr style="border: medium none;">
											<td class="begin">
												<span class="blue-no text-pad">
													<xsl:value-of select="(position() )" />
												</span>
											</td>
											<td style="">
												<!--<xsl:value-of select="a:PersonName/a:FamilyName" />, <xsl:value-of select="a:PersonName/a:GivenName" />-->
												<xsl:value-of select="a:PersonName/a:FormattedName" /><br/>
												SSN : xxx-xx-<xsl:value-of select="substring(a:DemographicDetail/a:GovernmentId,6,4)" />

												<br/>
												<span class="ultimatecls">
													<xsl:if test="a:DemographicDetail/a:DateOfBirth[not(string-length(.) != '10' )]">
														DOB : <span class="ultimateclshelp">
															<xsl:value-of select="concat(substring(a:DemographicDetail/a:DateOfBirth,6,2),'/',substring(a:DemographicDetail/a:DateOfBirth,9,2),'/',substring(a:DemographicDetail/a:DateOfBirth,1,4))" />
														</span>
														<xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
														Age : <xsl:value-of select="a:DemographicDetail/a:Age" />
													</xsl:if>
												</span>
											</td>
											<td style="" colspan="2">
												<xsl:value-of select="a:PostalAddress/a:DeliveryAddress/a:AddressLine" />
												<xsl:text> </xsl:text>
												<br />

												<xsl:value-of select="a:PostalAddress/a:Municipality" />
												<xsl:text>, </xsl:text>
												<xsl:value-of select="a:PostalAddress/a:Region" />
												<xsl:text> </xsl:text>
												<br />
												<xsl:value-of select="a:PostalAddress/a:County" />
												<xsl:text> </xsl:text>
												<br />

												<xsl:value-of select="a:PostalAddress/a:CountryCode" />
												<xsl:text> </xsl:text>
												<xsl:value-of select="a:PostalAddress/a:PostalCode" />
												<br/>
												<xsl:value-of select="a:EffectiveDate/a:StartDate" />
												<xsl:text disable-output-escaping="yes">&amp;nbsp;-&amp;nbsp;</xsl:text>
												<xsl:value-of select="a:EffectiveDate/a:EndDate" />
											</td>
											<td style="border-right: none;">
												<xsl:choose>
													<xsl:when test="count(a:ContactMethod)='0'">

													</xsl:when>
													<xsl:otherwise>
														<xsl:for-each select="a:ContactMethod">
															<xsl:value-of select="a:Telephone/a:FormattedNumber"/>
															<br/>
														</xsl:for-each>
													</xsl:otherwise>
												</xsl:choose>
											</td>
											<td  style="" class="end">
												<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
											</td>
										</tr>
										<tr>
                    <td class="begin">
                    </td>
                    <td colspan="3" style="background-color: rgb(255, 255, 255); width: 85%; padding-bottom: 10px;">
                      <textarea id="txtNote_{(position())}_{($pkOrderDetailId)}" rows="2" name="txtNote_{(position())}_{($pkOrderDetailId)}" style="width:100%;background-color:#FFFFE1;">
                        <xsl:value-of select="a:ReviewNote"/>
                      </textarea>
                      <textarea id="txthdnNote_{(position())}_{($pkOrderDetailId)}" rows="2" name="txtNote_{(position())}_{($pkOrderDetailId)}" style="width:100%;background-color:#FFFFE1;display:none">
                        <xsl:value-of select="a:ReviewNote"/>
                      </textarea>
                    </td>
                    <td align="right" valign="bottom" style="background-color:#fff;vertical-align:bottom; padding-bottom: 10px;" >
                      <input type="button" id="btnUpdateRecord_PS_{(position())}" value="Save Changes" onclick="SetHiddenFields(this.id)" wrap="off" class="btn btn-default"/>
                    </td>
                    <td style="background-color:#fff;" class="end">
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                    </td>
                  </tr>
									</xsl:when>
									<xsl:otherwise>
                    <tr style="border: medium none;">
                    <td class="begin">
                    </td>
                    <td colspan="4" align="right" style="text-align:right;color:red;font-size:13px;font-weight:bold">
                      <div style="float:right">
                        <div style="float:left">
                          <xsl:choose>
                            <xsl:when test="@Deleted='1'">
                              <input type="checkbox" id="chkIsDeleted_{(position())}_{($pkOrderDetailId)}" name="chkDelete_{(position())}" checked="checked"/>
                              <input type="checkbox" id="chkhdnIsDeleted_{(position())}_{($pkOrderDetailId)}" style="display:none" name="chkDelete_{(position())}" checked="checked"/>
                            </xsl:when>
                            <xsl:otherwise>
                              <input type="checkbox" id="chkIsDeleted_{(position())}_{($pkOrderDetailId)}" name="chkDelete_{(position())}"/>
                              <input type="checkbox" id="chkhdnIsDeleted_{(position())}_{($pkOrderDetailId)}" style="display:none" name="chkDelete_{(position())}"/>
                            </xsl:otherwise>
                          </xsl:choose>
                        </div>
                        <div style="float:right; margin-top:2px">Delete</div>
                      </div>
                    </td>
                    <td class="end">
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                    </td>
                  </tr>
                    <tr style="border: medium none;">
											<td class="begin">
												<span class="blue-no text-pad">
													<xsl:value-of select="(position() )" />
												</span>
											</td>
											<td >
												<!--<xsl:value-of select="a:PersonName/a:FamilyName" />, <xsl:value-of select="a:PersonName/a:GivenName" /><br/>-->
												<xsl:value-of select="a:PersonName/a:FormattedName" /><br/>
												SSN : xxx-xx-<xsl:value-of select="substring(a:DemographicDetail/a:GovernmentId,6,4)" />
												<br/>
												<span class="ultimatecls">
													<xsl:if test="a:DemographicDetail/a:DateOfBirth[not(string-length(.) != '10' )]">

														DOB :<span class="ultimateclshelp">
															<xsl:value-of select="concat(substring(a:DemographicDetail/a:DateOfBirth,6,2),'/',substring(a:DemographicDetail/a:DateOfBirth,9,2),'/',substring(a:DemographicDetail/a:DateOfBirth,1,4))" />
														</span><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
														Age : <xsl:value-of select="a:DemographicDetail/a:Age" />
													</xsl:if>
												</span>
											</td>
											<td>
												<xsl:value-of select="a:PostalAddress/a:DeliveryAddress/a:AddressLine" />
												<xsl:text> </xsl:text>
												<br />

												<xsl:value-of select="a:PostalAddress/a:Municipality" />
												<xsl:text>, </xsl:text>
												<xsl:value-of select="a:PostalAddress/a:Region" />
												<xsl:text> </xsl:text>
												<br />
												<xsl:value-of select="a:PostalAddress/a:County" />
												<xsl:text> </xsl:text>
												<br />

												<xsl:value-of select="a:PostalAddress/a:CountryCode" />
												<xsl:text> </xsl:text>
												<xsl:value-of select="a:PostalAddress/a:PostalCode" />
												<br/>
												<xsl:value-of select="a:EffectiveDate/a:StartDate" />
												<xsl:text disable-output-escaping="yes">&amp;nbsp;-&amp;nbsp;</xsl:text>
												<xsl:value-of select="a:EffectiveDate/a:EndDate" />
											</td>
											<td style="border-right: none;" colspan="2">
												<xsl:choose>
													<xsl:when test="count(a:ContactMethod)='0'">

													</xsl:when>
													<xsl:otherwise>
														<xsl:for-each select="a:ContactMethod">
															<xsl:value-of select="a:Telephone/a:FormattedNumber"/>
															<br/>
														</xsl:for-each>
													</xsl:otherwise>
												</xsl:choose>
											</td>
											<td class="end">
												<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
											</td>
										</tr>
										<tr>
                    <td class="begin">
                    </td>
                    <td colspan="3" style=" width: 85%; padding-bottom: 10px;">
                      <textarea id="txtNote_{(position())}_{($pkOrderDetailId)}" rows="2" name="txtNote_{(position())}_{($pkOrderDetailId)}" style="width:100%;background-color:#FFFFE1;">
                        <xsl:value-of select="a:ReviewNote"/>
                      </textarea>
                      <textarea id="txthdnNote_{(position())}_{($pkOrderDetailId)}" rows="2" name="txtNote_{(position())}_{($pkOrderDetailId)}" style="width:100%;background-color:#FFFFE1;display:none">
                        <xsl:value-of select="a:ReviewNote"/>
                      </textarea>
                    </td>
                    <td align="right" valign="bottom" style="vertical-align: bottom; padding-bottom: 10px;">
                      <input type="button" id="btnUpdateRecord_PS_{(position())}" value="Save Changes" onclick="SetHiddenFields(this.id)" wrap="off" class="btn btn-default"/>
                    </td>
                    <td class="end">
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                    </td>
                  </tr>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
						</tbody>
						<tfoot>
							<td class="begin"></td>
							<td></td>
							<td></td>
							<td style="border-right: none;"></td>
							<td class="end"></td>
						</tfoot>
					</table>
				</xsl:if>

			</center>

		</xsl:if>
	</xsl:template>
</xsl:stylesheet>