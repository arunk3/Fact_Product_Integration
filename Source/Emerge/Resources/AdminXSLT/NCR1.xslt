<?xml version='1.0' ?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:a="http://ns.hr-xml.org/2004-08-02">
  <xsl:output method="html" encoding="iso-8859-1" />
  <xsl:template match="/">

    <xsl:variable name="pkOrderDetailId" select="a:BackgroundReports/@OrderDetailId"></xsl:variable>
    <input type="text" id="txtTotalRecord_{($pkOrderDetailId)}" style="display:none" value="{(count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase))}"/>
    <center>
      <div class="sectionsummary">
        <h3>
          (<xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)" />) Results CRIMINAL REPORT.<br />

        </h3>
      </div>
      <xsl:variable name="TotalRecords">
        <xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)" />
      </xsl:variable>


      <xsl:if test="$TotalRecords=0">

        <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
          <thead>
            <tr class="ncr_header_data">
              <th colspan="5" style="">
                <span style="font-size:15px;font-weight:bold;">Search Information</span>
              </th>
              <th class="begin"></th>
              <th class="end"></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td colspan="5">
                <h4>Name:</h4>
                <h4>
                  <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:GivenName"/>
                  <xsl:text xml:space="preserve">  </xsl:text>
                  <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:FamilyName"/>
                </h4>
                <br />
                <h4>DOB:</h4>
                <h4>
                  <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:DemographicDetail/a:DateOfBirth"/>
                </h4>
              </td>
              <td class=" ">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>

              <td class=" ">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </tbody>
          <!--<tfoot>
						<td class="begin"></td>
						<td colspan="5"></td>
						<td class="end"></td>
					</tfoot>-->
        </table>

      </xsl:if>

      <table id="tblNCR" class="reporttable text-clr" border="0" cellpadding="0" cellspacing="0" style="position:relative;">
        <thead>
          <tr class="ncr_header_data">
            <th class="begin"></th>
            <th colspan="5" style="padding-top: 6px; padding-bottom: 6px;">
              <span style="font-weight: bold; font-size: 15px; color: rgb(102, 102, 102);">National Database Criminal Results</span>

              <div style="float:right">
                <span style="display: inline-block; vertical-align: 4px ! important; color:#666;">
                  Select All
                </span>
                <input type="checkbox" id="check_idAll_{($pkOrderDetailId)}"  onclick="SelectAllRecords('{($TotalRecords)}_{($pkOrderDetailId)}')"/>
              </div>
            </th>


          </tr>
          <tr>
            <td style="height: 10px;"></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
        </thead>
        <tbody>

          <xsl:variable name="TextData">
            <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text" />
          </xsl:variable>

          <xsl:variable name="SSNNo">
            xxx-xx-<xsl:value-of select="substring(a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:DemographicDetail/a:GovernmentId,6,4)" />
          </xsl:variable>

          <xsl:if test="not(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text)">
            <tr>
              <td class="begin">
              </td>
              <td colspan="5">
                <div style="width: 90%; margin: 0 auto; text-align: left">
                  <div style="width:100%; text-align:left">
                    <div style="font-size:11px;" id="report_ncr1">
                      <pre>
                        <!--<xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text">
                        <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text" disable-output-escaping="yes"/>
                      </xsl:if>-->
                        <xsl:if test="not(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text)">
                          <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus = 'Cancelled'">
                            <p>
                              There was an error running this report.<br /> Please contact our customer support team to quickly resolve this issue.
                            </p>
                          </xsl:if>
                          <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus != 'Cancelled'">
                            <p>*** Clear ***</p>
                          </xsl:if>
                          <p></p>
                        </xsl:if>
                      </pre>
                    </div>
                  </div>
                </div>
              </td>
              <td class="end">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>


          <xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase">
            <!--<xsl:sort select="a:CourtState"/>
            <xsl:sort select="a:DispDate" order="descending" />-->
            <xsl:variable name="StateNameValue" select="translate(a:CourtState,' ','')"></xsl:variable>
            <xsl:variable select="@CRID" name="CRID" ></xsl:variable>
            <xsl:variable name="PossibleMatch" select="a:PossibleMatch"></xsl:variable>
            <xsl:choose>
              <xsl:when test="position()>1">
                <tr style="height:20px;">
                  <td class="begin">
                  </td>
                  <td colspan="5" style="text-align:center;">
                    <span style="font-size:16px;font-weight:bold;color:white;">
                    </span>
                  </td>
                  <td class="end"></td>
                </tr>
              </xsl:when>
            </xsl:choose>
            <tr>
              <td id="StateHeader" name="StateHeader" colspan="6" style="display: none;height:40px;font-weight: bold; font-size: 15px !important; max-width: 94%;margin-left:38px; position: absolute; right: 0px; left: 0px;background-image: url('https://emerge.intelifi.com/content/themes/base/images/top_center.png');background-repeat: repeat-x;">

                Records Found In <span id="groupSpan" style="font-weight: bold;">
                  <xsl:value-of select="a:CourtState"/>
                </span>
                <span id="isPossiblespan" style="display:none">
                  <xsl:value-of select="a:PossibleMatch"/>
                </span>

                <span id="TotalRecords" style="display:none">
                  <xsl:value-of select="$TotalRecords"/>
                </span>

                <div style="float:right">
                  <span style="display: inline-block; vertical-align: 4px ! important; color:#666;">
                    State select all&#160;
                  </span>
                  <input type="checkbox" style="" id="checkidAllGroupWise_{($StateNameValue)}_{($pkOrderDetailId)}"/>

                </div>

              </td>
            </tr>

            <tr>
              <td colspan="5" height="40">
              </td>
            </tr>


            <tr >
              <td class="begin ncr_header_data">
              </td>

              <td colspan="3" class="ncr_header_data">
                <div style="padding: 15px; color: rgb(102, 102, 102); float: left; width: 300px;">
                  <div class="file-name" style="float: left; font-size: 15px; font-weight: normal; padding-top: 0px;">
                    Criminal Record
                  </div>
                  <span class="rednosetagainnumber">
                    <p>
                      
                    </p>
                  </span>
                </div>
              </td>
              <td align="right" style="" class="ncr_header_data">
                <xsl:if test="@WatchList">
                  <div style="float:right">
                    <div style="float:right; margin-top:2px">Record from 7 Year Filter</div>
                  </div>
                </xsl:if>
              </td>
              <td align="right" style="width:250px;" class="ncr_header_data">
                <div style="float: right; margin-top: 14px; margin-right: 8px;">
                  <div style="float:left">
                    <xsl:choose>
                      <xsl:when test="@Deleted='1'">
                        <input type="checkbox" id="chkIsDeleted_{(position())}_{($pkOrderDetailId)}" name="chkDelete_{(position())}" checked="checked"/>
                        <input type="checkbox" id="chkhdnIsDeleted_{(position())}_{($pkOrderDetailId)}" style="display:none" name="chkDelete_{(position())}" checked="checked"/>
                        <input type="hidden" id="hidden_CRID" value="{($CRID)}"></input>
                        <input type="hidden" id="hidden_PossibleMatch" value="{($PossibleMatch)}"></input>
                      </xsl:when>
                      <xsl:otherwise>
                        <input type="checkbox" id="chkIsDeleted_{(position())}_{($pkOrderDetailId)}" name="chkDelete_{(position())}"/>
                        <input type="checkbox" id="chkhdnIsDeleted_{(position())}_{($pkOrderDetailId)}" style="display:none" name="chkDelete_{(position())}"/>
                        <input type="hidden" id="hidden_CRID" value="{($CRID)}"></input>
                        <input type="hidden" id="hidden_PossibleMatch" value="{($PossibleMatch)}"></input>
                      </xsl:otherwise>
                    </xsl:choose>
                  </div>
                  <div style="float:right; font-weight:bold; margin-top:2px; color:red;">Delete</div>
                </div>
              </td>
              <td class="end ncr_header_data"></td>
            </tr>

            <tr>
              <td class="begin">
              </td>
              <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;padding:0px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr>
                    <td width="44%" class="column1">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                          <td></td>
                          <td style="width: 80px;"></td>
                          <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                            Source:
                          </td>
                          <td class=".tdSource" style="text-align:left;background-color:white;border-bottom:0px solid black;">
                            <xsl:value-of select="a:CourtName"/>
                          </td>
                        </tr>

                        <tr>
                          <td></td>
                          <td style="width: 80px;"></td>
                          <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                            Full Name:
                          </td>
                          <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                            <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                              <xsl:if test="a:FamilyName/@primary='true'">
                                <xsl:value-of select="a:FamilyName"/>
                                <xsl:if test="(a:GivenName)">
                                  <span>, </span>
                                </xsl:if>
                                <xsl:value-of select="a:GivenName"/>
                                <xsl:if test="(a:MiddleName)">
                                  <span>, </span>
                                </xsl:if>
                                <xsl:value-of select="a:MiddleName"/>
                                <xsl:element name="br"/>
                              </xsl:if>
                            </xsl:for-each>
                          </td>
                        </tr>

                        <xsl:if test="(a:SubjectIdentification/a:PersonName/a:FamilyName/@primary='false')">
                          <tr>
                            <td></td>
                            <td style="width: 80px;"></td>
                            <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                              AKA Name:
                            </td>
                            <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                              <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                <xsl:if test="a:FamilyName/@primary='false'">
                                  <xsl:value-of select="a:FamilyName"/>
                                  <xsl:if test="(a:GivenName)">
                                    <span>, </span>
                                  </xsl:if>
                                  <xsl:value-of select="a:GivenName"/>
                                  <xsl:if test="(a:MiddleName)">
                                    <span>, </span>
                                  </xsl:if>
                                  <xsl:value-of select="a:MiddleName"/>
                                  <xsl:element name="br"/>
                                </xsl:if>
                              </xsl:for-each>
                            </td>
                          </tr>
                        </xsl:if>

                        <tr>
                          <td></td>
                          <td></td>
                          <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                            DOB Recorded:
                          </td>
                          <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                            <xsl:value-of select='substring(a:SubjectIdentification/a:DemographicDetail/a:DateOfBirth,1,10)' />
                            <xsl:value-of select='a:SubjectIdentification/a:DemographicDetail/a:Age' />
                            <xsl:value-of select='a:SubjectIdentification/a:DemographicDetail/a:Other' />
                          </td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td style="text-align:right;background-color:white;border-bottom:0px solid black;color:#ff0000 !important;">
                            DOB Entered By User:
                          </td>
                          <td style="text-align:left;background-color:white;border-bottom:0px solid black;color:#ff0000; !important;">
                            <span class="lblDateEnteredByUser" id="lblDateEnteredByUser_{(position())}" style="color:#ff0000 !important;"></span>
                          </td>
                        </tr>
                      </table>
                    </td>

                    <td width="44%" class="column2">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <xsl:if test="(a:SubjectIdentification/a:DemographicDetail/a:GovernmentId/@documentType='DRIVERS LICENSE')">
                          <tr>
                            <td></td>
                            <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                              DL#:
                            </td>
                            <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                              <xsl:value-of select ="a:SubjectIdentification/a:DemographicDetail/a:GovernmentId[@documentType='DRIVERS LICENSE']"/>
                            </td>
                          </tr>
                        </xsl:if>

                        <xsl:if test="(a:SubjectIdentification/a:PostalAddress)">
                          <tr>
                            <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                              Address:
                            </td>
                            <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                              <xsl:for-each select="a:SubjectIdentification/a:PostalAddress">
                                <xsl:if test="(a:DeliveryAddress)">
                                  <xsl:value-of select ="a:DeliveryAddress"/>
                                  <xsl:if test="(a:Municipality)">
                                    <span>, </span>
                                  </xsl:if>
                                </xsl:if>
                                <xsl:if test="(a:Municipality)">
                                  <xsl:value-of select ="a:Municipality"/>
                                  <xsl:if test="(a:Region)">
                                    <span>, </span>
                                  </xsl:if>
                                </xsl:if>
                                <xsl:if test="(a:Region)">
                                  <xsl:value-of select ="a:Region"/>
                                  <xsl:if test="(a:PostalCode)">
                                    <span> - </span>
                                  </xsl:if>
                                </xsl:if>
                                <xsl:if test="(a:PostalCode)">
                                  <xsl:value-of select ="a:PostalCode"/>
                                </xsl:if>
                                <xsl:element name="br" />
                              </xsl:for-each>
                            </td>
                          </tr>
                        </xsl:if>

                        <tr>
                          <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                            Race:
                          </td>
                          <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                            <xsl:value-of select="a:SubjectIdentification/a:DemographicDetail/a:Race"/>
                          </td>
                        </tr>

                        <tr>
                          <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                            Gender:
                          </td>
                          <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                            <xsl:choose>
                              <xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:GenderCode = 1">
                                Male
                              </xsl:when>
                              <xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:GenderCode = 2">
                                Female
                              </xsl:when>
                              <xsl:otherwise>UnKnown</xsl:otherwise>
                            </xsl:choose>
                          </td>
                        </tr>

                        <xsl:if test="(a:AgencyReference[@type='Docket'])">
                          <tr>
                            <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                              Docket Number:
                            </td>

                            <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                              <xsl:value-of select="a:AgencyReference[@type='Docket']"/>
                            </td>
                          </tr>
                        </xsl:if>

                        <xsl:if test="a:CaseFileDate">

                          <tr>
                            <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:30%;">

                              Case Filed:

                            </td>
                            <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                              <xsl:choose>
                                <xsl:when test="count(a:CaseFileDate) &gt; 1">
                                  <xsl:for-each select="a:CaseFileDate">

                                    <span style="float:left;">
                                      <xsl:value-of select='.' />
                                      <xsl:element name="br" />
                                    </span>
                                  </xsl:for-each>
                                </xsl:when>
                                <xsl:when test="count(a:CaseFileDate)=1">
                                  <xsl:value-of select='substring(a:CaseFileDate,1,10)' />
                                </xsl:when>
                              </xsl:choose>
                            </td>
                          </tr>
                        </xsl:if>
                        <xsl:if test="a:ReviewNote!= ''">
                          <tr style="display:none;">
                            <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:30%;">
                              Review Note:
                            </td>
                            <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                              <xsl:if test="count(a:ReviewNote) &gt; 1">
                                <xsl:for-each select="a:ReviewNote">
                                  <span style="float:left;">
                                    <xsl:value-of select='.' />
                                    <xsl:element name="br" />
                                  </span>
                                </xsl:for-each>
                              </xsl:if>
                              <xsl:if test="count(a:ReviewNote)=1">
                                <xsl:value-of select='a:ReviewNote' />
                              </xsl:if>
                            </td>
                          </tr>
                        </xsl:if>
                        <tr>
                          <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:30%;">

                            Additional Events:

                          </td>
                          <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                            <xsl:if test="count(a:AdditionalItems/a:EffectiveDate) &gt; 0">
                              <input id="imageColapse" type="image" src="../content/themes/base/images/plus_symbol.png" style="width:10px; height:10px;" value="myValue" alt="" />
                            </xsl:if>
                            <div style="display:block;">

                              <xsl:for-each select="a:AdditionalItems/a:EffectiveDate">
                                <xsl:value-of  select='../a:Text' />
                                <xsl:text xml:space="preserve">  </xsl:text>
                                <xsl:value-of  select='substring(a:StartDate/a:AnyDate,1,10)' />
                                <xsl:element name="br" />
                              </xsl:for-each>

                            </div>
                          </td>
                        </tr>
                      </table>
                    </td>
                    <xsl:if test="(a:MatchMeter)">
                      <td width="12%" class="matchmeter" style="visibility: hidden;">
                        <table border="0" style="background: transparent;" cellpadding="0" cellspacing="0" width="100%" >
                          <tr>
                            <td style="background: transparent;font-size:11px;width:70%;height:24px;text-align:right;text-transform:capitalize !important;padding:0px;">
                              Name Match
                            </td>
                            <td class="match_class" style="background: transparent;text-align:center;height:22px;padding:0px;">
                              <xsl:text xml:space="preserve">  </xsl:text>
                              <xsl:if test="(a:MatchMeter/a:Name) = '2'">
                                <div class="matchgreen" />
                                <div class="width_class" style="margin-left:15%;" ></div>
                                <img  class="matchgreen2" src="../../content/themes/base/images/ico_match_green.jpg"></img>
                              </xsl:if>
                              <xsl:if test="(a:MatchMeter/a:Name) = '1'">
                                <div class="matchyellow" />
                                <div class="width_class" style="margin-left:15%;" ></div>
                                <img class="matchyellow2" src="../../content/themes/base/images/ico_red_yellow.png"></img>
                              </xsl:if>
                              <xsl:if test="(a:MatchMeter/a:Name) = '0'">
                                <div class="matchred" />
                                <div class="width_class" style="margin-left:15%;" ></div>
                                <img class="matchred2" src="../../content/themes/base/images/ico_red_match.png"></img>

                              </xsl:if>
                            </td>
                          </tr>
                          <tr>
                            <td style="background: transparent;font-size:11px;width:70%;height:25px;text-align:right;text-transform:capitalize !important;padding:0px;">
                              <b>Address Match</b>
                            </td>
                            <td style="background: transparent;text-align:center;height:22px;padding:0px;">
                              <xsl:text xml:space="preserve">  </xsl:text>
                              <xsl:if test="(a:MatchMeter/a:Address) = '2'">
                                <div class="matchgreen" />
                                <div class="width_class" style="margin-left:15%;" ></div>
                                <img  class="matchgreen2" src="../../content/themes/base/images/ico_match_green.jpg"></img>
                              </xsl:if>
                              <xsl:if test="(a:MatchMeter/a:Address) = '4'">
                                <div class="matchblue" />
                                <div class="width_class" style="margin-left:15%;" ></div>
                                <img  class="matchblue2" src="../../content/themes/base/images/ico_match_blue.jpg"></img>
                              </xsl:if>
                              <xsl:if test="(a:MatchMeter/a:Address) = '1'">
                                <div class="matchyellow" />
                                <div class="width_class" style="margin-left:15%;" ></div>
                                <img class="matchyellow2" src="../../content/themes/base/images/ico_red_yellow.png"></img>

                              </xsl:if>
                              <xsl:if test="(a:MatchMeter/a:Address) = '0'">
                                <div class="matchred" />
                                <div class="width_class" style="margin-left:15%;" ></div>
                                <img class="matchred2" src="../../content/themes/base/images/ico_red_match.png"></img>
                              </xsl:if>
                              <xsl:if test="(a:MatchMeter/a:Address) = '-1'">
                                <div class="matchgray"></div>
                                <div class="width_class" style="margin-left:15%;" ></div>
                                <img class="matchgray2" src="../../content/themes/base/images/ico_darkgrey_match.png"></img>

                              </xsl:if>
                            </td>
                          </tr>
                          <tr>
                            <td style="background: transparent;font-size:11px;width:70%;height:28px;text-align:right;text-transform:capitalize !important;padding:0px;">
                              <b>DOB Match</b>
                            </td>
                            <td style="background: transparent;text-align:center;height:28px;padding:0px;">
                              <xsl:text xml:space="preserve">  </xsl:text>
                              <xsl:if test="(a:MatchMeter/a:DOB) = '2'">
                                <div class="matchgreen" />
                                <div class="width_class" style="margin-left:15%;" ></div>
                                <img class="matchgreen2" src="../../content/themes/base/images/ico_match_green.jpg"></img>
                              </xsl:if>
                              <xsl:if test="(a:MatchMeter/a:DOB) = '1'">
                                <div class="matchyellow" />
                                <div class="width_class" style="margin-left:15%;" ></div>
                                <img class="matchyellow2" src="../../content/themes/base/images/ico_red_yellow.png"></img>
                              </xsl:if>
                              <xsl:if test="(a:MatchMeter/a:DOB) = '0'">
                                <div class="matchred" />
                                <div class="width_class" style="margin-left:15%;" ></div>
                                <img class="matchred2" src="../../content/themes/base/images/ico_red_match.png"></img>
                              </xsl:if>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </xsl:if>
                  </tr>
                </table>
              </td>
              <td class="end"></td>
            </tr>

            <xsl:for-each select="a:Charge">

              <tr>
                <td class="begin">
                </td>
                <td colspan="5" style="">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border:1px solid #ccc;">
                    <tr>
                      <xsl:choose>
                        <xsl:when test="(position()mod 2)=1">
                          <td width="10%" style="">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td style="">
                                  <div style="padding: 15px; color: rgb(102, 102, 102); float: left; width: 130px;">
                                    <div class="file-name" style="float:left; padding-top: 4px; color:#232323 !important;">Charge</div>
                                    <span class="blue-no text-pad" style="float:left; padding-top: 4px;">

                                      <xsl:value-of select="(position())" />

                                    </span>
                                  </div>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </xsl:when>
                        <xsl:otherwise>
                          <td width="10%" style="">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td style="">
                                  <div style="padding: 15px; color: rgb(102, 102, 102); float: left; width: 130px;">
                                    <div class="file-name" style="float:left; padding-top: 4px; color:#232323 !important;">Charge</div>

                                    <span class="blue-no text-pad" style="float:left; padding-top: 4px;">

                                      <xsl:value-of select="(position())" />

                                    </span>
                                  </div>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </xsl:otherwise>
                      </xsl:choose>

                      <xsl:choose>

                        <xsl:when test="(position()mod 2)=1">

                          <td width="45%" style="">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin-top:16px;">
                              <tr>
                                <td style="width:23%; color:#666;text-align: right;">
                                  Charge:
                                </td>
                                <td style=" color:#666;">
                                  <xsl:value-of select="a:ChargeOrComplaint"/>
                                </td>
                              </tr>
                              <xsl:if test="(a:ChargeDate)">
                                <tr>
                                  <td style="text-align:right;border-bottom:0px solid black;">
                                    Charge Date:
                                  </td>

                                  <td style="text-align:left;border-bottom:0px solid black;">
                                    <xsl:value-of select='substring(a:ChargeDate,1,10)' />
                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="(a:ArrestDate)">
                                <tr>
                                  <td style="text-align:right;border-bottom:0px solid black;">
                                    Arrest Date:
                                  </td>

                                  <td style="text-align:left;border-bottom:0px solid black;">
                                    <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                  </td>
                                </tr>
                              </xsl:if>

                              <xsl:if test="(a:OffenseDate)">
                                <tr>
                                  <td style="text-align:right;border-bottom:0px solid black;">
                                    Offense Date:
                                  </td>

                                  <td style="text-align:left;border-bottom:0px solid black;">
                                    <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                  </td>
                                </tr>
                              </xsl:if>

                              <xsl:if test="(a:DispositionDate)">
                                <tr>
                                  <td style="text-align:right;border-bottom:0px solid black;">
                                    Disposition Date:
                                  </td>

                                  <td style="text-align:left;border-bottom:0px solid black;">
                                    <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="(../a:AdditionalItems[@type='TrialDate'])">
                                <tr>
                                  <td style="text-align:right;border-bottom:0px solid black;">

                                    Trial Date:

                                  </td>

                                  <td style="text-align:left;border-bottom:0px solid black;">
                                    <xsl:value-of select="substring(../a:AdditionalItems[@type='TrialDate'],1,10)" />
                                  </td>
                                </tr>
                              </xsl:if>

                            </table>
                          </td>
                          <td width="45%" style="">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin-top:16px;">
                              <xsl:if test="(a:ChargeTypeClassification)!='unknown'">
                                <tr>
                                  <td style="text-align:right;border-bottom:0px solid black;width:25%;">

                                    Severity:

                                  </td>

                                  <td style="text-align:left;border-bottom:0px solid black;">
                                    <xsl:value-of select="a:ChargeTypeClassification"/>
                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="(a:Disposition)">
                                <tr>
                                  <td style="width:130px; color:#666;">

                                    Final Disposition:

                                  </td>

                                  <td style=" color:#666;">
                                    <xsl:choose>
                                      <xsl:when test="(a:Disposition)='unknown'">
                                        Manual County or State Report needed for Final Results
                                      </xsl:when>
                                      <xsl:otherwise>
                                        <xsl:choose>
                                          <xsl:when test="(contains(../a:CourtName, 'SAN BERNARDINO') or contains(../a:CourtName, 'RIVERSIDE')) and (a:Disposition)='NOT CONVICTED'">
                                            Unknown
                                          </xsl:when>
                                          <xsl:otherwise>
                                            <xsl:value-of select="a:Disposition"/>
                                          </xsl:otherwise>
                                        </xsl:choose>
                                      </xsl:otherwise>
                                    </xsl:choose>
                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="(a:Sentence)">
                                <tr>
                                  <td style="text-align:right;border-bottom:0px solid black;">
                                    Sentence:
                                  </td>
                                  <td style="text-align:left;border-bottom:0px solid black;">
                                    <xsl:value-of select='(a:Sentence)' />
                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="(a:Comment)">
                                <tr>
                                  <td style="text-align:right;border-bottom:0px solid black;">

                                    Comments:

                                  </td>

                                  <td style="text-align:left;border-bottom:0px solid black;">
                                    <xsl:for-each select="a:Comment">
                                      <span style="float:left;">
                                        <xsl:value-of select="."/>
                                        <xsl:element name="br" />
                                      </span>
                                    </xsl:for-each>
                                  </td>
                                </tr>
                              </xsl:if>
                            </table>
                          </td>
                        </xsl:when>
                        <xsl:otherwise>

                          <td width="45%">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td style="width:23%; color:#666;text-align: right;">
                                  Charge:
                                </td>
                                <td style=" color:#666;">
                                  <xsl:value-of select="a:ChargeOrComplaint"/>
                                </td>
                              </tr>
                              <xsl:if test="(a:ChargeDate)">
                                <tr>
                                  <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                    Charge Date:
                                  </td>

                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                    <xsl:value-of select='substring(a:ChargeDate,1,10)' />
                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="(a:ArrestDate)">
                                <tr>
                                  <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                    Arrest Date:
                                  </td>

                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                    <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                  </td>
                                </tr>
                              </xsl:if>

                              <xsl:if test="(a:OffenseDate)">
                                <tr>
                                  <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                    Offense Date:
                                  </td>

                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                    <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                  </td>
                                </tr>
                              </xsl:if>

                              <xsl:if test="(a:DispositionDate)">
                                <tr>
                                  <td style="text-align:right;background-color:white;border-bottom:0px solid black;">

                                    Disposition Date:

                                  </td>

                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                    <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="(../a:AdditionalItems[@type='TrialDate'])">
                                <tr>
                                  <td style="text-align:right;border-bottom:0px solid black;">

                                    Trial Date:

                                  </td>

                                  <td style="text-align:left;border-bottom:0px solid black;">
                                    <xsl:value-of select="substring(../a:AdditionalItems[@type='TrialDate'],1,10)" />
                                  </td>
                                </tr>
                              </xsl:if>
                            </table>
                          </td>
                          <td width="45%">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin-top:16px;">
                              <xsl:if test="(a:ChargeTypeClassification)!='unknown'">
                                <tr>
                                  <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">

                                    Severity:

                                  </td>

                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                    <xsl:value-of select="a:ChargeTypeClassification"/>
                                  </td>
                                </tr>
                              </xsl:if>

                              <xsl:if test="(a:Disposition)">
                                <tr>
                                  <td style=" color:#666;">

                                    Final Disposition:
                                  </td>

                                  <td style=" color:#666;">
                                    <xsl:choose>
                                      <xsl:when test="(a:Disposition)='unknown'">
                                        Manual County or State Report needed for Final Results
                                      </xsl:when>
                                      <xsl:otherwise>
                                        <xsl:choose>
                                          <xsl:when test="(contains(../a:CourtName, 'SAN BERNARDINO') or contains(../a:CourtName, 'RIVERSIDE')) and (a:Disposition)='NOT CONVICTED'">
                                            Unknown
                                          </xsl:when>
                                          <xsl:otherwise>
                                            <xsl:value-of select="a:Disposition"/>
                                          </xsl:otherwise>
                                        </xsl:choose>
                                      </xsl:otherwise>
                                    </xsl:choose>
                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="(a:Sentence)">
                                <tr>
                                  <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                    Sentence:
                                  </td>
                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                    <xsl:value-of select='(a:Sentence)' />
                                  </td>
                                </tr>
                              </xsl:if>

                              <xsl:if test="(a:Comment)">
                                <tr>
                                  <td style="text-align:right;background-color:white;border-bottom:0px solid black;">

                                    Comments:

                                  </td>

                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                    <xsl:for-each select="a:Comment">
                                      <span style="float:left;">
                                        <xsl:value-of select="."/>
                                        <xsl:element name="br" />
                                      </span>
                                    </xsl:for-each>
                                  </td>
                                </tr>
                              </xsl:if>
                            </table>
                          </td>
                        </xsl:otherwise>
                      </xsl:choose>
                    </tr>
                  </table>
                </td>
                <td class="end"></td>
              </tr>
            </xsl:for-each>
            <xsl:if test="count(a:Charge)=0">
              <tr>
                <td class="begin">
                </td>
                <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border:solid 1px black;background-color:red;text-align:center;">
                    <tr>
                      <td width="10%" style="border-right:solid 1px black;text-align:center;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tr>
                            <td style="text-align:right;">
                              <span style="font-size:12px;font-weight:bold;color:#000;">
                                Additional Info
                              </span>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td width="80%" style="text-align:left;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tr>
                            <td style="text-align:left;padding-left:5px;">
                              NOTE: THE DATA REPORTED BY THE COURT FOR THIS CASE IS INCOMPLETE. MANUAL COUNTY SEARCH REQUIRED.<br/>
                              <span style="padding-left:35px">IF THIS RECORD DOES NOT SPECIFY THE COUNTY, A MANUAL STATE SEARCH IS REQUIRED.</span>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
                <td class="end"></td>
              </tr>
            </xsl:if>

            <tr>
              <td class="begin">
              </td>
              <td colspan="4" style="width:90%">
                <div id="divBtn">

                </div>
                <textarea id="txtNote_{(position())}_{($pkOrderDetailId)}" rows="2" name="txtNote_{(position())}_{($pkOrderDetailId)}" style="width:100%;background-color:#FFFFE1;">
                  <xsl:value-of select="a:ReviewNote"/>
                </textarea>


                <textarea id="txthdnNote_{(position())}_{($pkOrderDetailId)}" rows="2" name="txtNote_{(position())}_{($pkOrderDetailId)}" style="width:100%;background-color:#FFFFE1;display:none">
                  <xsl:value-of select="a:ReviewNote"/>
                </textarea>
              </td>
              <td align="right" valign="bottom" style="vertical-align:bottom;display:none;">
                <input type="button" id="btnUpdateRecord_NCR1_{(position())}" value="Save Changes" onclick="SetHiddenFields(this.id,'{($pkOrderDetailId)}')" wrap="off" style=" height:30px; width:110px; text-align:center; vertical-align:middle"/>
              </td>
              <td class="end"></td>
            </tr>
          </xsl:for-each>

          <xsl:for-each select="a:BackgroundReports">
            <tr id="trRawNCR11">

              <td onclick="collapse('{concat('trRawNCR1_',@OrderDetailId)}');" colspan="5" style="border:solid 1px #ccc !important;background-color:white;text-align:left;">
                <div style="float:left">
                  <div style="float:left;background-position:left;" class="plus_btnSmall">
                  </div>
                  <div style="font-size:17px;font-weight:bold;color:#000;width:300px; margin-top:2px">
                    Raw Courthouse Data
                  </div>
                </div>
              </td>
            </tr>
            <tr id="trRawNCR12">
              <td colspan="5" style="border:solid 1px #ccc !important;background-color:white;text-align:center;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr id="{concat('trRawNCR1_',@OrderDetailId)}_b" style="display:none;">
                    <td style="background-color:white;">
                      <div id="divRawDataNCR1">
                        <pre>
                          <xsl:value-of select ="$TextData"/>
                        </pre>
                      </div>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </xsl:for-each>

        </tbody>
        <!--<tfoot>
					<td class="begin"></td>
					<td colspan="5"></td>
					<td class="end"></td>
				</tfoot>-->
      </table>
    </center>

    <div class="sectionsummary">
      <h3 style="line-height: 1em;">
        <!--<xsl:choose>
          <xsl:when test="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)=0">
            <p>
              National Sex Offender : Clear<br/>
              OFAC : Clear<br/>
              OIG : Clear<br/>
              For a complete list of coverage, please visit<br/>
              <a href="http://emerge.intelifi.com/coverage.pdf" target="_blank">US Criminal Rapid Search Coverage Guide</a>
            </p>
          </xsl:when>
          <xsl:otherwise>-->
        Data Sources Searched:<br/>
        Court Records, Department of Corrections, Administrative Office of Courts,<br/>
        National Sex Offender Registry, OFAC, OIG<br/>
        For a complete list of coverage, please visit<br/>
        <a href="http://emerge.intelifi.com/coverage.pdf" target="_blank">US Criminal Rapid Search Coverage Guide</a>

        <!--</xsl:otherwise>
        </xsl:choose>-->

      </h3>
    </div>
  </xsl:template>
</xsl:stylesheet>
