<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" encoding="iso-8859-1"/>
  <xsl:template match="/">
    <table cellspacing="0" cellpadding="3" border="0" width="100%" class="report">
      <tbody>
        <tr>
          <td class="warning" colspan="2">
          </td>
        </tr>
      </tbody>
    </table>
    <table cellspacing="0" cellpadding="0" width="100%" class="report">
      <tbody>
        <tr>
          <td width="50%" valign="top" id="TDDecision">
          </td>
          <td align="right" width="50%" valign="top" style="font-size: 10pt;">
            <label style="font-family: Verdana; font-size: 12pt; font-weight: bold; color: black;">
              Rapsheets Criminal Database Search
            </label>
            <br />
            <label style="font-family: Verdana; font-size: 10pt; font-weight: normal; color: black;">
            </label>
            <br />
            <br />
          </td>
        </tr>
      </tbody>
    </table>
    <table cellspacing="0" cellpadding="0" width="100%" class="report">
      <tbody>
        <tr>
          <td colspan="2">
            <br />
          </td>
        </tr>
        <tr>
          <td class="sectiontitle" colspan="2">
            Request Information
            <br />
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table cellspacing="0" cellpadding="3px" width="100%" class="profile" style="border-top: 3px solid #0099CC;
                                        border-left: 3px solid #0099CC; border-right: 3px solid #0099CC; border-bottom: 3px solid #0099CC;">
              <tbody>
                <tr>
                  <td>
                    <b>Control #: </b>
                    <xsl:value-of select="XML_INTERFACE/CREDITREPORT/RapRequestMaster/Request/ControlNumber"/>
                  </td>
                  <td>
                    <b>Customer Ref. Acct.: </b>
                  </td>
                </tr>
                <tr>
                  <td>
                    <b>Customer Transaction ID: </b>
                  </td>
                  <td>
                    <b>Search Date: </b>
                    <xsl:value-of select="XML_INTERFACE/CREDITREPORT/RapRequestMaster/Request/SearchDate"/>
                  </td>
                </tr>
                <tr>
                  <td class="SubSectiontitle" colspan="2">
                    Services:
                    <br />
                  </td>
                </tr>
                <tr>
                  <td class="SubSectiontitle" colspan="2">
                    - Criminal Search
                    <br />
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <b>Name: </b>
                    <xsl:value-of select="XML_INTERFACE/CREDITREPORT/RapRequestMaster/Request/Services/CriminalSearch/FirstName"/>
                    <xsl:text xml:space="preserve">  </xsl:text>
                    <xsl:value-of select="XML_INTERFACE/CREDITREPORT/RapRequestMaster/Request/Services/CriminalSearch/MiddleName"/>
                    <xsl:text xml:space="preserve">  </xsl:text>
                    <xsl:value-of select="XML_INTERFACE/CREDITREPORT/RapRequestMaster/Request/Services/CriminalSearch/LastName"/>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <b>DOB: </b>
                    <xsl:value-of select="XML_INTERFACE/CREDITREPORT/RapRequestMaster/Request/Services/CriminalSearch/DOB"/>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <br />
          </td>
        </tr>
      </tbody>
    </table>
    <table cellspacing="0" cellpadding="0" width="100%" class="report">
      <tbody>
        <tr>
          <td colspan="2">
            <br />
          </td>
        </tr>
        <tr>
          <td class="sectiontitle" colspan="2">
            Criminal Search Results
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <xsl:if test="XML_INTERFACE/CREDITREPORT/RapRequestMaster/Response/ServiceResults/CriminalSearchResults/SearchResultCount = '0'">
              <table cellspacing="0" cellpadding="3px" width="100%" class="profile" style="border-top: 3px solid #0099CC;
                                                        border-left: 3px solid #0099CC; border-right: 3px solid #0099CC; border-bottom: 3px solid #0099CC;">
                <tbody>
                  <tr>
                    <td colspan="2">
                      <b>Search Result Count: </b>
                      <xsl:value-of select="XML_INTERFACE/CREDITREPORT/RapRequestMaster/Response/ServiceResults/CriminalSearchResults/SearchResultCount"/>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">
                      <b>NO HIT</b>
                    </td>
                  </tr>
                </tbody>
              </table>
            </xsl:if>
            <xsl:if test="XML_INTERFACE/CREDITREPORT/RapRequestMaster/Response/ServiceResults/CriminalSearchResults/SearchResultCount != '0'">
              <table cellspacing="0" cellpadding="3px" width="100%" class="profile" style="border-top: 3px solid #0099CC;
                                        border-left: 3px solid #0099CC; border-right: 3px solid #0099CC; border-bottom: 3px solid #0099CC;">
                <tbody>
                  <tr>
                    <td colspan="2">
                      <b>Search Result Count: </b>
                      <xsl:value-of select="XML_INTERFACE/CREDITREPORT/RapRequestMaster/Response/ServiceResults/CriminalSearchResults/SearchResultCount"/>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">
                      <br />
                    </td>
                  </tr>
                  <tr>
                    <td class="SubSectiontitle" colspan="2">
                      Jurisdiction Results
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">
                      <xsl:for-each select="XML_INTERFACE/CREDITREPORT/RapRequestMaster/Response/ServiceResults/CriminalSearchResults/JurisdictionResults">
                        <xsl:choose>
                          <xsl:when test="(position()mod 2)=1">
                            <table cellspacing="0" cellpadding="0" width="100%" class="report" style="background-color:white;">
                              <tbody>
                                <tr class="rownr1">
                                  <td colspan="2">
                                    <font color="blue">
                                      <b>
                                        Jurisdiction: <xsl:value-of select="@Juriscd"/>
                                      </b>
                                    </font>
                                    <br/>
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td>
                                    <b>Jurisdiction Desc.: </b>
                                    <xsl:value-of select="JurisDescription"/>
                                  </td>
                                  <td>
                                    <b>Offender ID: </b>
                                    <xsl:value-of select="OffenderId"/>
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td>
                                    <b>Name: </b>
                                    <xsl:value-of select="FirstName"/>
                                    <xsl:text xml:space="preserve">  </xsl:text>
                                    <xsl:value-of select="MiddleName"/>
                                    <xsl:text xml:space="preserve">  </xsl:text>
                                    <xsl:value-of select="LastName"/>
                                  </td>
                                  <td>
                                    <b>DOB: </b>
                                    <xsl:value-of select="DOB"/>
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td>
                                    <b>Address: </b>
                                    <xsl:value-of select="Address"/>
                                  </td>
                                  <td>
                                    <b>SSN: </b>
                                    <xsl:value-of select="SSN"/>
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td style="padding-left: 60px;">
                                  </td>
                                  <td>
                                    <b>Phone: </b>
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td>
                                    <b>Race: </b>
                                    <xsl:value-of select="Race"/>
                                  </td>
                                  <td>
                                    <b>Sex: </b>
                                    <xsl:value-of select="Sex"/>
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td>
                                    <b>Height: </b>
                                    <xsl:value-of select="Height"/>
                                  </td>
                                  <td>
                                    <b>Weight: </b>
                                    <xsl:value-of select="Weight"/>
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td>
                                    <b>Hair Color: </b>
                                    <xsl:value-of select="HairColor"/>
                                  </td>
                                  <td>
                                    <b>Eye Color: </b>
                                    <xsl:value-of select="EyeColor"/>
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td colspan="2">
                                    <br />
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td colspan="2">
                                    <table cellspacing="0" cellpadding="0" width="100%" class="report">
                                      <tbody>
                                        <tr>
                                          <td colspan="3">
                                            <b style="color: #0099CC;">Aliases:</b>
                                          </td>
                                        </tr>
                                        <xsl:for-each select="Aliases/Alias">
                                          <tr class="rcolor4">
                                            <td>
                                              <b>Name: </b>
                                              <xsl:value-of select="FirstName"/>
                                              <xsl:text xml:space="preserve">  </xsl:text>
                                              <xsl:value-of select="MiddleName"/>
                                              <xsl:text xml:space="preserve">  </xsl:text>
                                              <xsl:value-of select="LastName"/>
                                            </td>
                                            <td>
                                              <b>DOB: </b>
                                              <xsl:value-of select="DOB"/>
                                            </td>
                                            <td>
                                              <b>Alias Flag: </b>
                                            </td>
                                          </tr>
                                        </xsl:for-each>
                                        <tr>
                                          <td colspan="3">
                                            <br />
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td colspan="2">
                                    <br />
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td colspan="2">
                                    <b style="color: #0099CC;">Charges: </b>
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td colspan="2">
                                    <xsl:variable name="counter" select="1" />
                                    <xsl:for-each select="Charges/Charge">
                                      <xsl:if test="$counter + 1" />
                                      <table cellspacing="0" cellpadding="0" width="100%" class="report">
                                        <tbody>
                                          <tr>
                                            <td colspan="2">
                                              <b>
                                                Charge #<xsl:value-of select="$counter" />
                                              </b>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td colspan="2">
                                              <b>Charge ID: </b>
                                              <xsl:value-of select="ChargeID" />
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>
                                              <b>Unique Jurisdiction ID: </b>
                                              <xsl:value-of select="UniqueJurisdictionID" />
                                            </td>
                                            <td>
                                              <b>Case #: </b>
                                              <xsl:value-of select="CaseNo"/>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>
                                              <b>Charge County Name: </b>
                                              <xsl:value-of select="ChrgCountyName" />
                                            </td>
                                            <td>
                                              <b>Detail Counter: </b>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>
                                              <b>Statute: </b>
                                              <xsl:value-of select="Statute" />
                                            </td>
                                            <td>
                                              <b>Statute Desc.: </b>
                                              <xsl:value-of select="StatuteDesc"/>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>
                                              <b>Disposition Date.: </b>
                                              <xsl:value-of select="DispDate" />
                                            </td>
                                            <td>
                                              <b>Disposition: </b>
                                              <xsl:value-of select="Disposition"/>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>
                                              <b>Sentence: </b>
                                              <xsl:value-of select="Sentence" />
                                            </td>
                                            <td>
                                              <b>Probation: </b>
                                              <xsl:value-of select="Probation"/>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </xsl:for-each>
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td colspan="2">
                                    <br />
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td colspan="2">
                                    <br />
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </xsl:when>
                          <xsl:otherwise>
                            <table cellspacing="0" cellpadding="0" width="100%" class="report" style="background-color:#E5F5FA;">
                              <tbody>
                                <tr class="rownr1">
                                  <td colspan="2">
                                    <font color="blue">
                                      <b>
                                        Jurisdiction: <xsl:value-of select="@Juriscd"/>
                                      </b>
                                    </font>
                                    <br/>
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td>
                                    <b>Jurisdiction Desc.: </b>
                                    <xsl:value-of select="JurisDescription"/>
                                  </td>
                                  <td>
                                    <b>Offender ID: </b>
                                    <xsl:value-of select="OffenderId"/>
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td>
                                    <b>Name: </b>
                                    <xsl:value-of select="FirstName"/>
                                    <xsl:text xml:space="preserve">  </xsl:text>
                                    <xsl:value-of select="MiddleName"/>
                                    <xsl:text xml:space="preserve">  </xsl:text>
                                    <xsl:value-of select="LastName"/>
                                  </td>
                                  <td>
                                    <b>DOB: </b>
                                    <xsl:value-of select="DOB"/>
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td>
                                    <b>Address: </b>
                                    <xsl:value-of select="Address"/>
                                  </td>
                                  <td>
                                    <b>SSN: </b>
                                    <xsl:value-of select="SSN"/>
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td style="padding-left: 60px;">
                                  </td>
                                  <td>
                                    <b>Phone: </b>
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td>
                                    <b>Race: </b>
                                    <xsl:value-of select="Race"/>
                                  </td>
                                  <td>
                                    <b>Sex: </b>
                                    <xsl:value-of select="Sex"/>
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td>
                                    <b>Height: </b>
                                    <xsl:value-of select="Height"/>
                                  </td>
                                  <td>
                                    <b>Weight: </b>
                                    <xsl:value-of select="Weight"/>
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td>
                                    <b>Hair Color: </b>
                                    <xsl:value-of select="HairColor"/>
                                  </td>
                                  <td>
                                    <b>Eye Color: </b>
                                    <xsl:value-of select="EyeColor"/>
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td colspan="2">
                                    <br />
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td colspan="2">
                                    <table cellspacing="0" cellpadding="0" width="100%" class="report">
                                      <tbody>
                                        <tr>
                                          <td colspan="3">
                                            <b style="color: #0099CC;">Aliases:</b>
                                          </td>
                                        </tr>
                                        <xsl:for-each select="Aliases/Alias">
                                          <tr class="rcolor4">
                                            <td>
                                              <b>Name: </b>
                                              <xsl:value-of select="FirstName"/>
                                              <xsl:text xml:space="preserve">  </xsl:text>
                                              <xsl:value-of select="MiddleName"/>
                                              <xsl:text xml:space="preserve">  </xsl:text>
                                              <xsl:value-of select="LastName"/>
                                            </td>
                                            <td>
                                              <b>DOB: </b>
                                              <xsl:value-of select="DOB"/>
                                            </td>
                                            <td>
                                              <b>Alias Flag: </b>
                                            </td>
                                          </tr>
                                        </xsl:for-each>
                                        <tr>
                                          <td colspan="3">
                                            <br />
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td colspan="2">
                                    <br />
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td colspan="2">
                                    <b style="color: #0099CC;">Charges: </b>
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td colspan="2">
                                    <xsl:variable name="counter" select="1" />
                                    <xsl:for-each select="Charges/Charge">
                                      <xsl:if test="$counter + 1" />
                                      <table cellspacing="0" cellpadding="0" width="100%" class="report">
                                        <tbody>
                                          <tr>
                                            <td colspan="2">
                                              <b>
                                                Charge #<xsl:value-of select="$counter" />
                                              </b>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td colspan="2">
                                              <b>Charge ID: </b>
                                              <xsl:value-of select="ChargeID" />
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>
                                              <b>Unique Jurisdiction ID: </b>
                                              <xsl:value-of select="UniqueJurisdictionID" />
                                            </td>
                                            <td>
                                              <b>Case #: </b>
                                              <xsl:value-of select="CaseNo"/>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>
                                              <b>Charge County Name: </b>
                                              <xsl:value-of select="ChrgCountyName" />
                                            </td>
                                            <td>
                                              <b>Detail Counter: </b>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>
                                              <b>Statute: </b>
                                              <xsl:value-of select="Statute" />
                                            </td>
                                            <td>
                                              <b>Statute Desc.: </b>
                                              <xsl:value-of select="StatuteDesc"/>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>
                                              <b>Disposition Date.: </b>
                                              <xsl:value-of select="DispDate" />
                                            </td>
                                            <td>
                                              <b>Disposition: </b>
                                              <xsl:value-of select="Disposition"/>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td>
                                              <b>Sentence: </b>
                                              <xsl:value-of select="Sentence" />
                                            </td>
                                            <td>
                                              <b>Probation: </b>
                                              <xsl:value-of select="Probation"/>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </xsl:for-each>
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td colspan="2">
                                    <br />
                                  </td>
                                </tr>
                                <tr class="rownr1">
                                  <td colspan="2">
                                    <br />
                                  </td>
                                </tr>
                              </tbody>
                            </table>

                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:for-each>
                    </td>
                  </tr>
                </tbody>
              </table>
            </xsl:if>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <hr />
          </td>
        </tr>
      </tbody>
    </table>
  </xsl:template>
</xsl:stylesheet>

