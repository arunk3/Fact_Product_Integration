﻿<?xml version="1.0" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output indent="yes" method="html"/>
	<xsl:template match="/">
		<html>
			<head>
				<style type="text/css">
					body{font-family: Lucida Grande,Arial,Helvetica,sans-serif;font-size: 14px;font-weight: normal;}

				</style>
			</head>
		</html>
		<Body>
			<center>
				<table width="100%" border="1">
					<!--<tr>
            <td align="left" width="20%">
              Name :
            </td>
            <td align="left">
              <xsl:apply-templates select="RESPONSE/RequesterInformation/Name" />
            </td>
            <td align="left" width="20%">
              Address :
            </td>
            <td align="left">
              <xsl:apply-templates select="RESPONSE/RequesterInformation/Address" />
            </td>
          </tr>
          <tr>
            <td align="left">
              State :
            </td>
            <td align="left">
              <xsl:apply-templates select="RESPONSE/RequesterInformation/State" />
            </td>
            <td align="left">
              City :
            </td>
            <td align="left">
              <xsl:apply-templates select="RESPONSE/RequesterInformation/City" />
            </td>
          </tr>-->
					<!--<tr>
            <td align="left">
              Zip :
            </td>
            <td align="left">
              <xsl:apply-templates select="RESPONSE/RequesterInformation/Zip" />
            </td>
            <td align="left">
              Date Of Request :
            </td>
            <td align="left">
              <xsl:apply-templates select="RESPONSE/DateOfRequest" />
            </td>
          </tr>-->
					<tr>
						<!--<td align="left">
              Account Number :
            </td>
            <td align="left">
              <xsl:apply-templates select="RESPONSE/AccountNumber" />
            </td>-->
						<td align="left">
							License Number :
						</td>
						<td align="left">
							<xsl:apply-templates select="RESPONSE/LicenseNumber" />
						</td>
						<td align="left">
							Date Of Request :
						</td>
						<td align="left">
							<xsl:apply-templates select="RESPONSE/DateOfRequest" />
						</td>
					</tr>
					<tr>
						<td align="left">
							License State :
						</td>
						<td align="left">
							<xsl:apply-templates select="RESPONSE/LicenseState" />
						</td>
						<td align="left">
							Full Name :
						</td>
						<td align="left">
							<xsl:apply-templates select="RESPONSE/FullName" />
						</td>
					</tr>
					<tr>
						<td align="left">
							Last Name :
						</td>
						<td align="left">
							<xsl:apply-templates select="RESPONSE/LastName" />
						</td>
						<td align="left">
							First Name :
						</td>
						<td align="left">
							<xsl:apply-templates select="RESPONSE/FirstName" />
						</td>
					</tr>
					<tr>
						<td align="left">
							Middle Name :
						</td>
						<td align="left">
							<xsl:apply-templates select="RESPONSE/MiddleName" />
						</td>
						<td align="left">
							Date Of Birth :
						</td>
						<td align="left">
							<xsl:apply-templates select="RESPONSE/DateOfBirth" />
						</td>
					</tr>
					<tr>
						<td align="left">
							Gender :
						</td>
						<td align="left">
							<xsl:apply-templates select="RESPONSE/Gender" />
						</td>
						<td align="left">
							Height :
						</td>
						<td align="left">
							<xsl:apply-templates select="RESPONSE/Height" />
						</td>
					</tr>
					<tr>
						<td align="left">
							Weight :
						</td>
						<td align="left">
							<xsl:text disable-output-escaping="no"> </xsl:text>
							<xsl:apply-templates select="RESPONSE/Weight" />
						</td>
						<td align="left">
							Eye Color :
						</td>
						<td align="left">
							<xsl:apply-templates select="RESPONSE/EyeColor" />
						</td>
					</tr>
					<tr>
						<!--<td align="left">
              Social Security Number :
            </td>
            <td align="left">
              <xsl:apply-templates select="RESPONSE/SocialSecurityNumber" />
            </td>-->
						<td align="left">
							Status :
						</td>
						<td align="left">
							<xsl:apply-templates select="RESPONSE/FullName" />
						</td>
						<td align="left">

						</td>
						<td align="left">

						</td>
					</tr>
					<tr>
						<td colspan="4" align="Center">
							<xsl:for-each select="RESPONSE/CDLISystems/CDLISystem" >
								<table width="80%" border="1">
									<tr>
										<td align="left" width="20%">
											Description :
										</td>
										<td  align="left">
											<xsl:apply-templates select="Description"></xsl:apply-templates>
										</td>
									</tr>
									<xsl:choose>
										<xsl:when test="count(LicenseName)='1'">
											<tr>
												<td  align="left">
													License Name :
												</td>
												<td  align="left">
													<xsl:apply-templates select="LicenseName"></xsl:apply-templates>
												</td>
											</tr>
										</xsl:when>
										<xsl:otherwise>
											<tr>
												<td  align="left">
													License State :
												</td>
												<td  align="left">
													<xsl:apply-templates select="LicenseState"></xsl:apply-templates>

												</td>
											</tr>
										</xsl:otherwise>
									</xsl:choose>
									<xsl:choose>
										<xsl:when test="count(LicenseNumber)='1'">
											<tr>
												<td  align="left">
													License Number :
												</td>
												<td  align="left">
													<xsl:apply-templates select="LicenseNumber"></xsl:apply-templates>
												</td>
											</tr>
										</xsl:when>
										<xsl:otherwise>
											<tr>
												<td  align="left">
													License DOB :
												</td>
												<td  align="left">
													<xsl:apply-templates select="LicenseDOB"></xsl:apply-templates>
												</td>
											</tr>
										</xsl:otherwise>
									</xsl:choose>
									<tr>
										<td></td>
										<td align="left">
											------***------
										</td>
									</tr>
								</table>
							</xsl:for-each>
						</td>
					</tr>
				</table>
			</center>
		</Body>
	</xsl:template>
</xsl:stylesheet>