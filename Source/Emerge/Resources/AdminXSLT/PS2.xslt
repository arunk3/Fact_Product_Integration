<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" encoding="iso-8859-1"/>
  <xsl:param name="admin" select="defaultstring" />
  <xsl:template match="/">
    <xsl:variable name="pkOrderDetailId" select="XML_INTERFACE/@OrderDetailId"></xsl:variable>
    <input type="text" id="txtTotalRecord_{($pkOrderDetailId)}" style="display:none" value="{(XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/rows_returned)}"/>
    <div class="sectionsummary">
      <h3>
        (<xsl:value-of select="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/rows_returned" />) Results People Search
      </h3>
    </div>
    <xsl:if test="XML_INTERFACE/CREDITREPORT/OBJECTS/BUREAUERROR = 'True' or XML_INTERFACE/CREDITREPORT/cp_xmldirect/general_error !='' ">
      <table id="peopleTable" class="people-search" border="0" cellpadding="0"  style="width:100%;" cellspacing="0">
        <thead>
          <tr>
            <th ></th>
            <th colspan="6">Search Results</th>
            <th ></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td >
				<span class="red-no" style="padding-top:5px;">
              <xsl:value-of select="(position() )"/>
				</span>
            </td>
            <td colspan="6">

              <xsl:value-of select="XML_INTERFACE/CREDITREPORT/HTMLREPORT/MESSAGE" disable-output-escaping="yes" />

            </td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
        </tbody>
        <tfoot>
          <td ></td>
          <td colspan="6"></td>
          <td ></td>
        </tfoot>
      </table>
    </xsl:if>
    <center>
      <xsl:if test="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/rows_returned = '0'">
        <div class="noRecords">
          No Records Found.
        </div>
      </xsl:if>

      <xsl:if test="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/rows_returned != '0'">


        <table id="peopleTable" class="people-search" border="0" cellpadding="0" cellspacing="0">
          <thead>
            <tr>
				<th style="width:5%;" ></th>
				<th align="left" style="width:20%">Name</th>
				<th align="left" style="width:40%">Address</th>
				<th align="left" style="width:10%" >Reporting Date</th>
				<th align="left" style="width:10%" >Date of Birth</th>
				<th align="left" >Indicators</th>
              <th>
                Select All  <input type="checkbox" id="check_idAll_{($pkOrderDetailId)}"  onclick="SelectAllRecords('{XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/rows_returned}_{($pkOrderDetailId)}')"/>
              </th>
              <th ></th>
            </tr>
          </thead>

          <tbody>
            <xsl:for-each select="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/response_data/locator/response_row">
              <tr>
                <td >
                </td>
                <td colspan="6" align="right" style="background-color:#fff; text-align:right;color:red;font-size:13px;font-weight:bold">
                  <div style="background-color:#fff;float:right">
                    <div style="float:left">
                      <xsl:choose>
                        <xsl:when test="@Deleted='1'">
                          <input type="checkbox" id="chkIsDeleted_{(position())}_{($pkOrderDetailId)}" name="chkDelete_{(position())}" checked="checked"/>
                          <input type="checkbox" id="chkhdnIsDeleted_{(position())}_{($pkOrderDetailId)}" style="display:none" name="chkDelete_{(position())}" checked="checked"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <input type="checkbox" id="chkIsDeleted_{(position())}_{($pkOrderDetailId)}" name="chkDelete_{(position())}"/>
                          <input type="checkbox" id="chkhdnIsDeleted_{(position())}_{($pkOrderDetailId)}" style="display:none" name="chkDelete_{(position())}"/>
                        </xsl:otherwise>
                      </xsl:choose>
                    </div>
                    <div style="float:right;margin-top:2px">Delete</div>
                  </div>
                </td>
                
                <td  style="background-color:#fff;">
                  <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                </td>
              </tr>
              <xsl:choose>
                <xsl:when test="( position() mod 2 = 0 )">
                  <tr>
                    <td >
						<span class="blue-no" style="padding-top:5px;">
                      <xsl:value-of select="(position() )" />
						</span>
                    </td>
                    <td>
                      <xsl:value-of select="person/lastname" />, <xsl:value-of select="person/firstname" /> <br/>
                      <xsl:variable name='ssn' 	select="person/ssn" />
                      <xsl:variable name='one' 	select='substring( $ssn, 0 , 4 )' />
                      <xsl:variable name='two'	select='substring( $ssn, 4 , 2 )' />
                      <xsl:variable name='three'  select='substring( $ssn, 6 , 4 )' />

                      <xsl:if test="$admin = '1'">
                        Social #: <xsl:value-of select="person/ssn"/><br />
                      </xsl:if>
                      <xsl:if test="$admin = '0'">
                        <xsl:value-of select="concat('Social #: ', 'xxx', '-', 'xx', '-', $three )"/>
                        <br />
                      </xsl:if>


                      <xsl:if test="address/phone_number != ''">
                        <xsl:value-of select="concat('Phone #: ', address/phone_number)" />
                        <br/>
                      </xsl:if>
                      <br />
                      <xsl:if test="person/aka_list/aka">
                        AKAs: <br/>
                      </xsl:if>
                      <xsl:for-each select="person/aka_list">
                        <xsl:value-of select="aka" />
                        <br/>
                      </xsl:for-each>
                    </td>
                    <td>
                      <xsl:value-of select="address/pre_direction" />
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="address/house_numer" />
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="address/street_name" />
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="address/street_suffix" />
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="address/post_direction" />
                      
                      <xsl:value-of select="address/city" />
                      <xsl:text>, </xsl:text>
                      <xsl:value-of select="address/state" />
                     
                      <xsl:value-of select="address/zipcode" />
                      <xsl:if test="address/zip4 != ''">
                        -<xsl:value-of select="address/zip4" />
                      </xsl:if>
                    </td>
                    <td>
                      <xsl:variable name='year'  select='substring( address/report_date, 0 , 5 )' />
                      <xsl:variable name='month' select='substring( address/report_date, 5 , 2 )' />
                      <xsl:variable name='day'   select='substring( address/report_date, 7 , 2 )' />
                      <xsl:variable name='reportdate'   select="concat($month, '/', $day, '/', $year )"  />
                      <xsl:if test="$reportdate = '//'">

                      </xsl:if>
                      <xsl:if test="$reportdate != '//'">
                        <xsl:value-of select="$reportdate"/>
                      </xsl:if>
                    </td>
                    <td>
                      <xsl:variable name='year'  select='substring( person/dob_list/dob, 0 , 5 )' />
                      <xsl:variable name='month' select='substring( person/dob_list/dob, 5 , 2 )' />
                      <xsl:variable name='day'   select='substring( person/dob_list/dob, 7 , 2 )' />
                      <xsl:variable name='dob'   select="concat($month, '/', $day, '/', $year )"  />
                      <xsl:if test="$dob = '//'">
                        None Specified
                      </xsl:if>
                      <xsl:if test="$dob != '//'">
                        <xsl:value-of select="$dob"/>
                      </xsl:if>
                    </td>
                    <td colspan="2">
                      <xsl:if test="person/is_deceased = 'YES'">
                        Deceased
                      </xsl:if>
                    </td>
                    <td >
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                    </td>
                  </tr>
                </xsl:when>
                <xsl:otherwise>
                  <tr>
                    <td >
						<span class="blue-no" style="padding-top:5px;">
                      <xsl:value-of select="(position() )" />
						</span>
                    </td>
                    <td style="background-color:#fff;">
                      <xsl:value-of select="person/lastname" />, <xsl:value-of select="person/firstname" /> <br/>
                      <xsl:variable name='ssn' 	select="person/ssn" />
                      <xsl:variable name='one' 	select='substring( $ssn, 0 , 4 )' />
                      <xsl:variable name='two'	select='substring( $ssn, 4 , 2 )' />
                      <xsl:variable name='three'  select='substring( $ssn, 6 , 4 )' />

                      <xsl:if test="$admin = '1'">
                        Social #: <xsl:value-of select="person/ssn"/><br />
                      </xsl:if>
                      <xsl:if test="$admin = '0'">
                        <xsl:value-of select="concat('Social #: ', 'xxx', '-', 'xx', '-', $three )"/>
                        <br />
                      </xsl:if>


                      <xsl:if test="address/phone_number != ''">
                        <xsl:value-of select="concat('Phone #: ', address/phone_number)" />
                        <br/>
                      </xsl:if>
                      <br />
                      <xsl:if test="person/aka_list/aka">
                        AKAs: <br/>
                      </xsl:if>
                      <xsl:for-each select="person/aka_list">
                        <xsl:value-of select="aka" />
                        <br/>
                      </xsl:for-each>
                    </td>
                    <td style="background-color:#fff;">
                      <xsl:value-of select="address/pre_direction" />
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="address/house_numer" />
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="address/street_name" />
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="address/street_suffix" />
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="address/post_direction" />
                      
                      <xsl:value-of select="address/city" />
                      <xsl:text>, </xsl:text>
                      <xsl:value-of select="address/state" />
                      
                      <xsl:value-of select="address/zipcode" />
                      <xsl:if test="address/zip4 != ''">
                        -<xsl:value-of select="address/zip4" />
                      </xsl:if>
                    </td>
                    <td style="background-color:#fff;">
                      <xsl:variable name='year'  select='substring( address/report_date, 0 , 5 )' />
                      <xsl:variable name='month' select='substring( address/report_date, 5 , 2 )' />
                      <xsl:variable name='day'   select='substring( address/report_date, 7 , 2 )' />
                      <xsl:variable name='reportdate'   select="concat($month, '/', $day, '/', $year )"  />
                      <xsl:if test="$reportdate = '//'">

                      </xsl:if>
                      <xsl:if test="$reportdate != '//'">
                        <xsl:value-of select="$reportdate"/>
                      </xsl:if>
                    </td>
                    <td style="background-color:#fff;">
                      <xsl:variable name='year'  select='substring( person/dob_list/dob, 0 , 5 )' />
                      <xsl:variable name='month' select='substring( person/dob_list/dob, 5 , 2 )' />
                      <xsl:variable name='day'   select='substring( person/dob_list/dob, 7 , 2 )' />
                      <xsl:variable name='dob'   select="concat($month, '/', $day, '/', $year )"  />
                      <xsl:if test="$dob = '//'">
                        None Specified
                      </xsl:if>
                      <xsl:if test="$dob != '//'">
                        <xsl:value-of select="$dob"/>
                      </xsl:if>
                    </td>
                    <td style="background-color:#fff;" colspan="2">
                      <xsl:if test="person/is_deceased = 'YES'">
                        Deceased
                      </xsl:if>
                    </td>
                    <td  style="background-color:#fff;">
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                    </td>
                  </tr>
                </xsl:otherwise>
              </xsl:choose>
              <tr>
                <td >
                </td>
                <td colspan="5" style="background-color:#fff;width:85%">
                  <textarea id="txtNote_{(position())}_{($pkOrderDetailId)}" rows="2" name="txtNote_{(position())}_{($pkOrderDetailId)}" style="width:100%;background-color:#FFFFE1;">
                    <xsl:value-of select="ReviewNote"/>
                  </textarea>
                  <textarea id="txthdnNote_{(position())}_{($pkOrderDetailId)}" rows="2" name="txtNote_{(position())}_{($pkOrderDetailId)}" style="width:100%;background-color:#FFFFE1;display:none">
                    <xsl:value-of select="ReviewNote"/>
                  </textarea>
                </td>
                <td align="right" valign="bottom" style="background-color:#fff;vertical-align:bottom">
                  <input type="button" id="btnUpdateRecord_PS2_{(position())}" value="Save Changes" onclick="SetHiddenFields(this.id)" wrap="off" style=" height:30px; width:110px; text-align:center; vertical-align:middle"/>
                </td>
                <td  style="background-color:#fff;">
                  <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                </td>
                
              </tr>
            </xsl:for-each>
          </tbody>

          <tfoot>
            <td ></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td ></td>
          </tfoot>
        </table>
      </xsl:if>
    </center>



    <div class="requestId">
      Request ID :
      <xsl:text> </xsl:text>
      <xsl:value-of select="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/request_id" />
    </div>
  </xsl:template>

</xsl:stylesheet>
