<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" encoding="utf-8"/>
  <xsl:template match="/">

    <xsl:variable name="pkOrderDetailId" select="XML_INTERFACE/@OrderDetailId"></xsl:variable>
    <input type="text" id="txtTotalRecord_{($pkOrderDetailId)}" style="display:none" value="{(count(BackgroundReports/BackgroundReportPackage/Screenings/Screening/CriminalReport/CriminalCase))}"/>
    <input type="text" id="txtTotalRecord_{($pkOrderDetailId)}_IAI" style="display:none" value="{(count(XML_INTERFACE/CREDITREPORT/response/individual/akas/identity))}"/>
    <input type="text" id="txtTotalRecord_{($pkOrderDetailId)}_IA" style="display:none" value="{(count(XML_INTERFACE/CREDITREPORT/response/individual/addresses/address))}"/>
    <input type="text" id="txtTotalRecord_{($pkOrderDetailId)}_NEIGH" style="display:none" value="{(count(XML_INTERFACE/CREDITREPORT/response/individual/neighbors/neighborhood))}"/>

    <table id="peopleTable" class="reporttable tdr-heading" border="0" cellpadding="0" cellspacing="0">
      <thead>
        <tr class="ncr_header_data">
          <th></th>
          <th></th>
          <th style="width: 250px; float: left; padding-top: 5px; padding-bottom: 7px; color:#666">Name1</th>
          <th style=" color:#666">Gender</th>
          <th style=" color:#666">DOB</th>
          <th style="float: left; width: 180px; padding-top: 6px; padding-bottom: 7px; color:#666">(:age)</th>
          <th style=" color:#666"> SSN  Delete   </th>
          <th style="float: left; padding-right: 0px; text-align: right; padding-bottom: 6px; color: rgb(102, 102, 102); width: 170px; padding-top: 6px;">
            <span style="display: inline-block; vertical-align: 4px ! important;">Select All </span>
            <input type="checkbox" id="check_idAll_IAI"  onclick="SelectTDR_IAIRecords({(count(XML_INTERFACE/CREDITREPORT/response/individual/akas/identity))})"/>
           </th>
        </tr>
      </thead>
      <tbody>
        <xsl:for-each select="XML_INTERFACE/CREDITREPORT/response/individual/akas/identity">
          <tr style="border: medium none;">
            <td >
            </td>
            <td >
            </td>
            <td >
            </td>
            <td colspan="5" align="right" style="padding-right: 8px; background-color:#fff;text-align:right;color:red;font-size:13px;font-weight:bold;height:15px">
              <div style="float:right">
                <div style="float:left">
                  <xsl:choose>
                    <xsl:when test="@Deleted='1'">
                      <input type="checkbox" id="chkIsDeletedTDR_{(position())}_IAI" name="chkDelete_{(position())}" checked="checked"/>
                      <input type="checkbox" id="chkhdnIsDeletedTDR_{(position())}_IAI" style="display:none" name="chkDelete_{(position())}" checked="checked"/>

                    </xsl:when>
                    <xsl:otherwise>
                      <input type="checkbox" id="chkIsDeletedTDR_{(position())}_IAI" name="chkDelete_{(position())}"/>
                      <input type="checkbox" id="chkhdnIsDeletedTDR_{(position())}_IAI" style="display:none" name="chkDelete_{(position())}"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </div>
                <div style="float:right; margin-top:2px">Delete</div>
              </div>
            </td>
          </tr>
          <tr>
            <td></td>
            <td >
              <span class="blue-no text-pad" style="padding-top: 6px; margin-left:0px;"> <xsl:value-of select="(position() )"/>
              </span>
            </td>
            <xsl:choose>
              <xsl:when test="(position() mod 2 !=0)">
                <td style="background-color:#fff;">
                  
                    <xsl:value-of select="concat(name/first, ' ', name/middle, ', ', name/last)" />
                  
                </td>
                <td style="background-color:#fff;">
                  <xsl:value-of select="name/gender" />
                </td>
                <td style="background-color:#fff;">
                  <xsl:value-of select="concat(dob/month,'/',dob/day,'/',dob/year)" />
                </td>
                <td style="background-color:#fff;">
                  :<xsl:value-of select="age" />
                </td>
                <td style="border-right: none;background-color:#fff;">
                  xxx-xx-xxxx
                </td>
                <td style="background-color:#fff;" >
                  <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                </td>
              </xsl:when>
              <xsl:otherwise>
                <td>
                  <xsl:value-of select="concat(name/first, ' ', name/middle, ', ', name/last)" />
                </td>
                <td>
                  <xsl:value-of select="name/gender" />
                </td>
                <td>
                  <xsl:value-of select="concat(dob/month,'/',dob/day,'/',dob/year)" />
                </td>
                <td>
                  :<xsl:value-of select="age" />
                </td>
                <td style="border-right: none;">
                  xxx-xx-xxxx
                </td>
                <td >
                  <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                </td>
              </xsl:otherwise>
            </xsl:choose>
          </tr>
          <tr>
            <td></td>
            <td >
            </td>
            <td colspan="5" align="right" style="background-color:#fff;text-align:right;color:red;font-size:13px;font-weight:bold;height:15px">
              <table style="background-color:#fff;width:100%" >
                <tr>
                  <td style="background-color:#fff;width:90%">
                    <textarea id="txtNoteTDR_{(position())}_IAI" rows="2" name="txtNote_{(position())}" style="width:100%;background-color:#FFFFE1;">
                      <xsl:value-of select="ReviewNote"/>
                    </textarea>
                    <textarea id="txthdnNoteTDR_{(position())}_IAI" rows="2" name="txtNote_{(position())}" style="width:100%;background-color:#FFFFE1;display:none">
                      <xsl:value-of select="ReviewNote"/>
                    </textarea>
                  </td>
                  <td align="right" valign="bottom" style="background-color:#fff;vertical-align:bottom">
                    <input type="button" id="btnUpdateRecord_TDR_{(position())}_IAI" value="Save Changes" onclick="SetHiddenFieldsForTDR(this.id)" wrap="off" class="btn btn-default"/>
                  </td>
                </tr>
              </table>
            </td>
            <td style="background-color:#fff;" >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
        </xsl:for-each>
      </tbody>
      <tfoot>
        <td ></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td style="border-right: none;"></td>
        <td ></td>
      </tfoot>
    </table>

    <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
      <thead>
        <tr class="ncr_header_data">
          <th style="color:#666;"></th>
          <th style="color:#666;">Residencies</th>
          <th style="color:#666;">Possible Relatives:</th>
          <th style="float: left; width: 250px; padding-top: 6px; padding-bottom: 8px; color:#666;">Possible Associates:</th>
          <th style="color:#666;">Possible Neighbors:</th>
          <th ></th>
        </tr>
      </thead>
      <tbody>
        <!--<tr>
          <td >
          </td>
          <td colspan="5" align="right" style="background-color:#fff;text-align:right;color:red;font-size:13px;font-weight:bold;height:15px">
            <div style="float:right">
              <div style="float:left">
                <xsl:choose>
                  <xsl:when test="@Deleted='1'">
                    <input type="checkbox" id="chkIsDeletedTDR_{(position())}" name="chkDelete_{(position())}" checked="checked"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <input type="checkbox" id="chkIsDeletedTDR_{(position())}" name="chkDelete_{(position())}"/>
                  </xsl:otherwise>
                </xsl:choose>
              </div>
              <div style="float:right; margin-top:2px">Delete</div>
            </div>
          </td>
          <td style="background-color:#fff;" >
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </td>
        </tr>-->
        <tr>
          <td >
            <span class="blue-no text-pad" style="padding-top: 6px;"> <xsl:value-of select="(position())"/>
            </span>
          </td>
          <td>
            <xsl:for-each select="response/individual/addresses/address">
              <xsl:for-each select="phones/phone[phone10!='']">
                <xsl:value-of select="phone10"/>
                <xsl:text> - </xsl:text>
                <xsl:value-of select="name-dual"/>
                <br />
              </xsl:for-each>
            </xsl:for-each>
            <xsl:if test="count(response/individual/addresses/address) = 0">
              [None Found]
            </xsl:if>
          </td>
          <td>
            <xsl:for-each select="response/individual/relatives/individual/addresses/address">
              <xsl:for-each select="phones/phone[phone10!='']">
                <xsl:value-of select="phone10"/>
                <xsl:text> - </xsl:text>
                <xsl:value-of select="name-dual"/>
                <br />
              </xsl:for-each>
            </xsl:for-each>
            <xsl:if test="count(response/individual/relatives/individual/addresses/address) = 0">
              [None Found]
            </xsl:if>
          </td>
          <td>
            <xsl:for-each select="response/individual/associates/individual/addresses/address">
              <xsl:for-each select="phones/phone[phone10!='']">
                <xsl:value-of select="phone10"/>
                <xsl:text> - </xsl:text>
                <xsl:value-of select="name-dual"/>
                <br />
              </xsl:for-each>
            </xsl:for-each>
            <xsl:if test="count(response/individual/associates/individual/addresses/address) = 0">
              [None Found]
            </xsl:if>
          </td>
          <td style="border-right: none;">
            <xsl:for-each select="response/individual/neighbors/neighborhood">
              <xsl:for-each select="subject-address/phones/phone[phone10!='']">
                <xsl:value-of select="phone10"/>
                <xsl:text> - </xsl:text>
                <xsl:value-of select="name-dual"/>
                <br />
              </xsl:for-each>
              <xsl:for-each select="addresses/address">
                <xsl:for-each select="phones/phone[phone10!='']">
                  <xsl:value-of select="phone10"/>
                  <xsl:text> - </xsl:text>
                  <xsl:value-of select="name-dual"/>
                  <br />
                </xsl:for-each>
              </xsl:for-each>
            </xsl:for-each>
            <xsl:if test="count(response/individual/neighbors/neighborhood) = 0">
              [None Found]
            </xsl:if>
          </td>
          <td >
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </td>
        </tr>
        <!--<tr>
          <td >
          </td>
          <td colspan="5" align="right" style="background-color:#fff;text-align:right;color:red;font-size:13px;font-weight:bold;height:15px">
            <table style="background-color:#fff;width:100%" >
              <tr>
                <td style="background-color:#fff;width:90%">
                  <textarea id="txtNoteTDR_{(position())}_address" rows="2" name="txtNote_{(position())}" style="width:100%;background-color:#FFFFE1;">
                    <xsl:value-of select="ReviewNote"/>
                  </textarea>
                </td>
                <td align="right" valign="bottom" style="background-color:#fff;vertical-align:bottom">
                  <input type="button" id="btnUpdateRecord_TDR_{(position())}_addresses/address" value="Save Changes" onclick="SetHiddenFieldsForTDR(this.id)" wrap="off" style=" height:30px; width:110px; text-align:center; vertical-align:middle"/>
                </td>
              </tr>
            </table>
          </td>
          <td style="background-color:#fff;" >
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </td>
        </tr>-->
      </tbody>
      <tfoot>
        <td ></td>
        <td></td>
        <td></td>
        <td></td>
        <td style="border-right: none;"></td>
        <td ></td>
      </tfoot>
    </table>

    <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
      <thead>
        <tr class="ncr_header_data">
          
          <th colspan="100%" style="color: rgb(102, 102, 102); padding-bottom: 8px; padding-top: 6px; width: 50% ! important;">Verified Address(es):</th>
          <th ></th><th ></th>
        </tr>
      </thead>
      <tbody>
        <xsl:for-each select="XML_INTERFACE/CREDITREPORT/response/individual/akas/identity">
          <tr>
            <td colspan="100%" style="border-right: none;">
              <xsl:for-each select="//response/individual/addresses/address">
                <xsl:if test="verified = 'yes'">
                  <xsl:value-of select="residents/identity/name/first"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="residents/identity/name/middle"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="residents/identity/name/last"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="residents/identity/name/suffix"/>
                  <xsl:text> - </xsl:text>
                  <xsl:value-of select="street-number"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="street-name"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="street-suffix"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="unit-designation"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="unit-number"/>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="city"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="state"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="zip"/>
                  <xsl:text>-</xsl:text>
                  <xsl:value-of select="zip4"/>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="county"/>
                  <xsl:text> COUNTY</xsl:text>
                  <xsl:text> (</xsl:text>
                  <xsl:value-of select="concat(date-first/month,'/00/',date-first/year)"/>
                  <xsl:text> - </xsl:text>
                  <xsl:value-of select="concat(date-last/month,'/00/',date-last/year)"/>
                  <xsl:text>)</xsl:text>
                  <br />

                  <xsl:for-each select="//neighborhood/subject-address">
                    <xsl:if test="phones/phone/phone10">
                      <p class="indent header">
                        <xsl:text>Current phones listed at this address:</xsl:text>
                      </p>
                      <xsl:for-each select="phones/phone">
                        <p class="indent_more">
                          <xsl:value-of select="phone10"/>
                          <xsl:text> - </xsl:text>
                          <xsl:value-of select="name-dual"/>
                        </p>
                      </xsl:for-each>
                    </xsl:if>
                  </xsl:for-each>
                </xsl:if>
              </xsl:for-each>
              <xsl:if test="count(XML_INTERFACE/CREDITREPORT/response/individual/akas/identity) = 0">
                [None Found]
              </xsl:if>
            </td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
        </xsl:for-each>
      </tbody>
      <tfoot>
        <td ></td>
        <td style="border-right: none;" colspan="100%"></td>
        <td ></td>
      </tfoot>
    </table>

    <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
      <thead>
        <tr class="ncr_header_data">
          
          <th colspan="100%"  style="color:#666;">
            Previous And Non-Verified Address(es):
            
          </th>
          <th ></th>
          <th style="text-align: right; padding-bottom: 8px; padding-top: 6px;" >
            <div style="width: 300px;">
              <span style="text-align: right; padding-bottom: 8px; display: inline-block; padding-top: 0px; vertical-align: top; color:#666;">
                Select All
              </span>
              <input type="checkbox" id="check_idAll_IA"  onclick="SelectTDR_IARecords({(count(XML_INTERFACE/CREDITREPORT/response/individual/addresses/address))})"/>
            </div>
          </th>
        </tr>
      </thead>
      <tbody>

        <xsl:for-each select="//response/individual/addresses/address">
          <tr>
            <td>
            </td>           
            <td style="background-color:#fff;" >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
             <td colspan="5" align="right" style="background-color:#fff;text-align:right;color:red;font-size:13px;font-weight:bold;height:15px; padding-left:95%;">
              <div style="float:right; width:80px;">
                <div style="float:left">
                  <xsl:choose>
                    <xsl:when test="@Deleted='1'">
                      <input type="checkbox" id="chkIsDeletedTDR_{(position())}_IA" name="chkDelete_{(position())}" checked="checked"/>
                      <input type="checkbox" id="chkhdnIsDeletedTDR_{(position())}_IA" style="display:none" name="chkDelete_{(position())}" checked="checked"/>

                    </xsl:when>
                    <xsl:otherwise>
                      <input type="checkbox" id="chkIsDeletedTDR_{(position())}_IA" name="chkDelete_{(position())}"/>
                      <input type="checkbox" id="chkhdnIsDeletedTDR_{(position())}_IA" style="display:none" name="chkDelete_{(position())}"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </div>
                <div style="float:right; margin-top:2px">Delete</div>
              </div>
            </td>
          </tr>
          <xsl:choose>
            <xsl:when test="(position() mod 2=0)">
              <tr>
                <td  style="background-color:#fff;">
                  <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                </td>
                <td colspan="100%" style="border-right: none;background-color:#fff;">

                  <xsl:if test="verified = 'no'">
                    <xsl:value-of select="residents/identity/name/first"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="residents/identity/name/middle"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="residents/identity/name/last"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="residents/identity/name/suffix"/>
                    <xsl:text> - </xsl:text>
                    <xsl:value-of select="street-number"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="street-name"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="street-suffix"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="unit-designation"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="unit-number"/>
                    <xsl:text>, </xsl:text>
                    <xsl:value-of select="city"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="state"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="zip"/>
                    <xsl:text>-</xsl:text>
                    <xsl:value-of select="zip4"/>
                    <xsl:text>, </xsl:text>
                    <xsl:value-of select="county"/>
                    <xsl:text> COUNTY</xsl:text>
                    <xsl:text> (</xsl:text>
                    <xsl:value-of select="concat(date-first/month,'/00/',date-first/year)"/>
                    <xsl:text> - </xsl:text>
                    <xsl:value-of select="concat(date-last/month,'/00/',date-last/year)"/>
                    <xsl:text>)</xsl:text>
                    <br />

                    <xsl:for-each select="//neighborhood/subject-address">
                      <xsl:if test="phones/phone">
                        <p class="indent header">
                          <xsl:text>Current phones listed at this address:</xsl:text>
                        </p>
                        <xsl:for-each select="phones/phone">
                          <p class="indent_more">
                            <xsl:value-of select="phone10"/>
                            <xsl:text> - </xsl:text>
                            <xsl:value-of select="name-dual"/>
                          </p>
                        </xsl:for-each>
                      </xsl:if>
                    </xsl:for-each>
                    <br />
                  </xsl:if>

                </td>
                <td style="background-color:#fff;">
                  <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                </td>
              </tr>
            </xsl:when>
            <xsl:otherwise>
              <tr>
                <td>
                  <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                </td>
                <td colspan="100%" style="border-right: none;">
                  <xsl:if test="verified = 'no'">
                    <xsl:value-of select="residents/identity/name/first"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="residents/identity/name/middle"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="residents/identity/name/last"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="residents/identity/name/suffix"/>
                    <xsl:text> - </xsl:text>
                    <xsl:value-of select="street-number"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="street-name"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="street-suffix"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="unit-designation"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="unit-number"/>
                    <xsl:text>, </xsl:text>
                    <xsl:value-of select="city"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="state"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="zip"/>
                    <xsl:text>-</xsl:text>
                    <xsl:value-of select="zip4"/>
                    <xsl:text>, </xsl:text>
                    <xsl:value-of select="county"/>
                    <xsl:text> COUNTY</xsl:text>
                    <xsl:text> (</xsl:text>
                    <xsl:value-of select="concat(date-first/month,'/00/',date-first/year)"/>
                    <xsl:text> - </xsl:text>
                    <xsl:value-of select="concat(date-last/month,'/00/',date-last/year)"/>
                    <xsl:text>)</xsl:text>
                    <br />

                    <xsl:for-each select="//neighborhood/subject-address">
                      <xsl:if test="phones/phone">
                        <p class="indent header">
                          <xsl:text>Current phones listed at this address:</xsl:text>
                        </p>
                        <xsl:for-each select="phones/phone">
                          <p class="indent_more">
                            <xsl:value-of select="phone10"/>
                            <xsl:text> - </xsl:text>
                            <xsl:value-of select="name-dual"/>
                          </p>
                        </xsl:for-each>
                      </xsl:if>
                    </xsl:for-each>
                    <br />
                  </xsl:if>
                </td>
                <td >
                  <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                </td>
              </tr>
            </xsl:otherwise>
          </xsl:choose>
          <tr>
            <td>
            </td>
            <td colspan="5" align="right" style="background-color: rgb(255, 255, 255); text-align: right; color: red; font-size: 13px; font-weight: bold; height: 15px; padding-left: 128px; width: 100%;">
              <table style="background-color:#fff;width:100%" >
                <tr>
                  <td style="background-color:#fff;width:90%">
                    <textarea id="txtNoteTDR_{(position())}_IA" rows="2" name="txtNote_{(position())}" style="width:100%;background-color:#FFFFE1;">
                      <xsl:value-of select="ReviewNote"/>
                    </textarea>
                    <textarea id="txthdnNoteTDR_{(position())}_IA" rows="2" name="txtNote_{(position())}" style="width:100%;background-color:#FFFFE1;display:none">
                      <xsl:value-of select="ReviewNote"/>
                    </textarea>
                  </td>
                  <td align="right" valign="bottom" style="background-color:#fff;vertical-align:bottom">
                    <input type="button" id="btnUpdateRecord_TDR_{(position())}_individual/addresses/address" value="Save Changes" class="btn btn-default" onclick="SetHiddenFieldsForTDR(this.id)" wrap="off" />
                  </td>
                </tr>
              </table>
            </td>
            <td style="background-color:#fff;" >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
        </xsl:for-each>

      </tbody>
      <tfoot>
        <td></td>
        <td style="border-right: none;" colspan="100%"></td>
        <td ></td>
      </tfoot>
    </table>

    <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0" style="margin-bottm:10px;">
      <thead>
        <tr class="ncr_header_data">
          
          <th  style="background:transparent; color:#666;" colspan="100%">Possible Properties Owned by Subject:</th>
          <th  style="background:transparent; color:#666;" ></th>
          <th  style="background:transparent; color:#666;" ></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td colspan="100%" style="border-right: none;">
            <xsl:for-each select="//response/individual/addresses/address/properties/property">
              <xsl:value-of select="address/street-number"/>
              <xsl:text> </xsl:text>
              <xsl:value-of select="address/street-name"/>
              <xsl:text> </xsl:text>
              <xsl:value-of select="address/street-suffix"/>
              <xsl:text> </xsl:text>
              <xsl:value-of select="unit-designation"/>
              <xsl:text> </xsl:text>
              <xsl:value-of select="unit-number"/>
              <xsl:text>, </xsl:text>
              <xsl:value-of select="address/city"/>
              <xsl:text> </xsl:text>
              <xsl:value-of select="address/state"/>
              <xsl:text> </xsl:text>
              <xsl:value-of select="address/zip"/>
              <xsl:text>-</xsl:text>
              <xsl:value-of select="address/zip4"/>
              <xsl:text>, </xsl:text>
              <xsl:value-of select="address/county"/>
              <xsl:text> COUNTY</xsl:text>
            </xsl:for-each>
            <xsl:if test="count(//response/individual/addresses/address/properties/property) = 0">
              [None Found]
            </xsl:if>
          </td>
          <td >
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </td>
          <td >
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </td>
        </tr>
      </tbody>
      <tfoot>
        <td ></td>
        <td style="border-right: none;" colspan="100%"></td>
        <td ></td>
      </tfoot>
    </table>

    <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0" style="margin-bottm:10px;">
      <thead>
        <tr class="ncr_header_data">
          
          <th  style="background:transparent; color:#666;" colspan="100%">Possible Associates:</th>
          <th  style="background:transparent; color:#666;" ></th>
          <th  style="background:transparent; color:#666;" ></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td colspan="100%" style="border-right: none;">
            <xsl:for-each select="//response/individual/associates/individual">
              <b>
                <u>
                  <xsl:value-of select="akas/identity/name/first"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="akas/identity/name/middle"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="akas/identity/name/last"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="akas/identity/name/suffix"/>
                  <xsl:if test="akas/identity/ssn != ''">
                    <xsl:text> </xsl:text>
                    xxx-xx-xxxx
                  </xsl:if>
                  <xsl:if test="dob != ''">
                    <xsl:text> DOB: </xsl:text>
                    <xsl:value-of select="akas/identity/dob"/>
                  </xsl:if>
                  <xsl:if test="age != ''">
                    <xsl:text> Age: </xsl:text>
                    <xsl:value-of select="akas/identity/age"/>
                  </xsl:if>
                </u>
              </b>
              <br />

              <b>
                <i>
                  <xsl:text>Names Associated with Associate:</xsl:text>
                </i>
              </b>
              <br />
              <ul>
                <xsl:for-each select="akas/identity">
                  <li>
                    <xsl:value-of select="name/first"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="name/middle"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="name/last"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="name/suffix"/>
                    <xsl:if test="ssn != ''">
                      <xsl:text> </xsl:text>
                      xxx-xx-xxxx
                    </xsl:if>
                    <xsl:if test="dob != ''">
                      <xsl:text> DOB: </xsl:text>
                      <xsl:value-of select="dob"/>
                    </xsl:if>
                    <xsl:if test="age != ''">
                      <xsl:text> Age: </xsl:text>
                      <xsl:value-of select="age"/>
                    </xsl:if>
                  </li>
                </xsl:for-each>
              </ul>

              <b>
                <i>
                  <xsl:text>Active Address(es):</xsl:text>
                </i>
              </b>
              <br />
              <ul>
                <xsl:for-each select="addresses/address">
                  <li>
                    <xsl:if test="verified = 'yes'">
                      <font class="fFont" color="blue" style="font-size:16px;">
                        <b>V</b>
                      </font>
                      <xsl:text/>
                    </xsl:if>
                    <xsl:if test="shared = 'yes'">
                      <font class="fFont" color="blue" style="font-size:16px;">
                        <b>S</b>
                      </font>
                      <xsl:text/>
                    </xsl:if>
                    <xsl:value-of select="street-number"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="street-name"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="street-suffix"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="unit-designation"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="unit-number"/>
                    <xsl:text>, </xsl:text>
                    <xsl:value-of select="city"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="state"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="zip"/>
                    <xsl:text>-</xsl:text>
                    <xsl:value-of select="zip4"/>
                    <xsl:text>, </xsl:text>
                    <xsl:value-of select="county"/>
                    <xsl:text> COUNTY</xsl:text>
                    <xsl:text> (</xsl:text>
                    <xsl:value-of select="concat(date-first/month,'/00/',date-first/year)"/>
                    <xsl:text> - </xsl:text>
                    <xsl:value-of select="concat(date-last/month,'/00/',date-last/year)"/>
                    <xsl:text>)</xsl:text>
                  </li>
                </xsl:for-each>
              </ul>
              <hr />
            </xsl:for-each>
            <xsl:if test="count(//response/individual/associates/individual) = 0">
              [None Found]
            </xsl:if>
          </td>
          <td >
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </td>
          <td >
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </td>
        </tr>
      </tbody>
      <tfoot>
        <td ></td>
        <td style="border-right: none;" colspan="100%"></td>
        <td ></td>
      </tfoot>
    </table>

    <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0" style="margin-bottm:10px;">
      <thead>
        <tr class="ncr_header_data">
          <th  style="background:transparent; color:#666;" ></th>
          <th  style="background:transparent; color:#666;" colspan="100%">Possible Relatives:</th>
          <th  style="background:transparent; color:#666;" ></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td colspan="100%" style="border-right: none;">
            <xsl:for-each select="//response/individual/relatives/individual">

              <b>
                <u>
                  <xsl:value-of select="akas/identity/name/first"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="akas/identity/name/middle"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="akas/identity/name/last"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="akas/identity/name/suffix"/>
                  <xsl:if test="akas/identity/ssn != ''">
                    <xsl:text> </xsl:text>
                    xxx-xx-xxxx
                  </xsl:if>
                  <xsl:if test="akas/identity/dob != ''">
                    <xsl:text> DOB: </xsl:text>
                    <xsl:value-of select="akas/identity/dob"/>
                  </xsl:if>
                  <xsl:if test="akas/identity/dod != ''">
                    <xsl:text> DOD: </xsl:text>
                    <xsl:value-of select="akas/identity/dod"/>
                  </xsl:if>
                  <xsl:if test="akas/identity/age != ''">
                    <xsl:text> Age: </xsl:text>
                    <xsl:value-of select="akas/identity/age"/>
                  </xsl:if>
                  <xsl:if test="akas/identity/age-at-death != ''">
                    <xsl:text> Age at Death: </xsl:text>
                    <xsl:value-of select="akas/identity/age-at-death"/>
                  </xsl:if>
                  <xsl:if test="akas/identity/dod != ''">
                    <xsl:text> (Born  </xsl:text>
                    <xsl:value-of select="akas/identity/dob/year"/>
                    <xsl:text> years ago)</xsl:text>
                  </xsl:if>
                </u>
              </b>
              <br />

              <b>
                <i>
                  <xsl:text>Names Associated with Relative:</xsl:text>
                </i>
              </b>
              <br />
              <ul>
                <xsl:for-each select="akas/identity">
                  <li>
                    <xsl:if test="age-at-death != ''">
                      <font class="fFont" color="red" style="font-size:16px;">
                        <b>D</b>
                      </font>
                      <xsl:text/>
                    </xsl:if>
                    <xsl:value-of select="name/first"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="name/middle"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="name/last"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="name/suffix"/>
                    <xsl:if test="ssn != ''">
                      <xsl:text> </xsl:text>
                      xxx-xx-xxxx
                    </xsl:if>
                    <xsl:if test="dob != ''">
                      <xsl:text> DOB: </xsl:text>
                      <xsl:value-of select="dob"/>
                    </xsl:if>
                    <xsl:if test="dod != ''">
                      <xsl:text> DOD: </xsl:text>
                      <xsl:value-of select="dod"/>
                    </xsl:if>
                    <xsl:if test="age != ''">
                      <xsl:text> Age: </xsl:text>
                      <xsl:value-of select="age"/>
                    </xsl:if>
                    <xsl:if test="age-at-death != ''">
                      <xsl:text> Age at Death: </xsl:text>
                      <xsl:value-of select="age-at-death"/>
                    </xsl:if>
                    <xsl:if test="dod != ''">
                      <xsl:text> (Born  </xsl:text>
                      <xsl:value-of select="dob/year"/>
                      <xsl:text> years ago)</xsl:text>
                    </xsl:if>

                    <xsl:if test="age-at-death != ''">
                      <font class="fFont" color="red" style="font-size:16px;">
                        <b>D</b>
                      </font>
                      <xsl:text/>
                    </xsl:if>
                    <xsl:value-of select="name/first"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="name/middle"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="name/last"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="name/suffix"/>
                    <xsl:if test="ssn != ''">
                      xxx-xx-xxxx
                    </xsl:if>
                    <xsl:if test="dob != ''">
                      <xsl:text> DOB: </xsl:text>
                      <xsl:value-of select="dob"/>
                    </xsl:if>
                    <xsl:if test="dod != ''">
                      <xsl:text> DOD: </xsl:text>
                      <xsl:value-of select="dod"/>
                    </xsl:if>
                    <xsl:if test="age != ''">
                      <xsl:text> Age: </xsl:text>
                      <xsl:value-of select="age"/>
                    </xsl:if>
                    <xsl:if test="age-at-death != ''">
                      <xsl:text> Age at Death: </xsl:text>
                      <xsl:value-of select="age-at-death"/>
                    </xsl:if>
                    <xsl:if test="dod != ''">
                      <xsl:text> (Born  </xsl:text>
                      <xsl:value-of select="dod/year"/>
                      <xsl:text> years ago)</xsl:text>
                    </xsl:if>
                  </li>
                </xsl:for-each>
              </ul>

              <b>
                <i>
                  <xsl:text>Active Address(es):</xsl:text>
                </i>
              </b>
              <br />
              <ul>
                <xsl:for-each select="addresses/address">
                  <li>
                    <xsl:if test="verified = 'yes'">
                      <font class="fFont" color="blue" style="font-size:16px;">
                        <b>V</b>
                      </font>
                      <xsl:text/>
                    </xsl:if>
                    <xsl:if test="shared = 'yes'">
                      <font class="fFont" color="blue" style="font-size:16px;">
                        <b>S</b>
                      </font>
                      <xsl:text/>
                    </xsl:if>
                    <xsl:value-of select="street-number"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="street-name"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="street-suffix"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="unit-designation"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="unit-number"/>
                    <xsl:text>, </xsl:text>
                    <xsl:value-of select="city"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="state"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="zip"/>
                    <xsl:text>-</xsl:text>
                    <xsl:value-of select="zip4"/>
                    <xsl:text>, </xsl:text>
                    <xsl:value-of select="county"/>
                    <xsl:text> COUNTY</xsl:text>
                    <xsl:text> (</xsl:text>
                    <xsl:value-of select="concat(date-first/month,'/00/',date-first/year)"/>
                    <xsl:text> - </xsl:text>
                    <xsl:value-of select="concat(date-last/month,'/00/',date-last/year)"/>
                    <xsl:text>)</xsl:text>
                  </li>
                </xsl:for-each>
              </ul>
              <hr width="60%" />
            </xsl:for-each>
            <xsl:if test="count(//response/individual/relatives/individual) = 0">
              [None Found]
            </xsl:if>
          </td>
          <td >
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </td>
          <td >
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </td>
        </tr>
      </tbody>
      <tfoot>
        <td></td>
        <td style="border-right: none;" colspan="100%"></td>
        <td ></td>
      </tfoot>
    </table>

    <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0" style="margin-bottm:10px;">
      <thead>
        <tr class="ncr_header_data">
          <th  style="background:transparent; color:#666;" ></th>
          <th colspan="100%" style="background:transparent; color:#666;" >
            Neighbors:
            <div style="float:right">
              Select All  <input type="checkbox" id="check_idAll_NEIGH"  onclick="SelectTDR_NEIGHRecords({(count(XML_INTERFACE/CREDITREPORT/response/individual/neighbors/neighborhood))})"/>
            </div>

          </th>
          <th  style="background:transparent; color:#666;" ></th>
        </tr>
      </thead>
      <tbody>
        <xsl:for-each select="//response/individual/neighbors/neighborhood">
          <tr>
            <td >
            </td>
            <td colspan="5" align="right" style="background-color:#fff;text-align:right;color:red;font-size:13px;font-weight:bold;height:15px">
              <div style="float:right">
                <div style="float:left">
                  <xsl:choose>
                    <xsl:when test="@Deleted='1'">
                      <input type="checkbox" id="chkIsDeletedTDR_{(position())}_NEIGH" name="chkDelete_{(position())}" checked="checked"/>
                      <input type="checkbox" id="chkhdnIsDeletedTDR_{(position())}_NEIGH" style="display:none" name="chkDelete_{(position())}" checked="checked"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <input type="checkbox" id="chkIsDeletedTDR_{(position())}_NEIGH" name="chkDelete_{(position())}"/>
                      <input type="checkbox" id="chkhdnIsDeletedTDR_{(position())}_NEIGH" style="display:none" name="chkDelete_{(position())}"/>

                    </xsl:otherwise>
                  </xsl:choose>
                </div>
                <div style="float:right; margin-top:2px">Delete</div>
              </div>
            </td>
            <td style="background-color:#fff;" >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">
              <p class="" style="font-weight:bold; text-decoration:underline;">
                <xsl:text>Neighborhood:</xsl:text>
              </p>
              <xsl:for-each select="subject-address">
                <p class="indent">
                  <xsl:value-of select="street-number"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="street-name"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="street-suffix"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="unit-designation"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="unit-number"/>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="city"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="state"/>
                  <xsl:text> </xsl:text>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="zip"/>
                  <xsl:text>-</xsl:text>
                  <xsl:value-of select="zip4"/>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="county"/>
                  <xsl:text> COUNTY</xsl:text>
                  <xsl:text> (</xsl:text>
                  <xsl:value-of select="concat(date-first/month,'/00/',date-first/year)"/>
                  <xsl:text> - </xsl:text>
                  <xsl:value-of select="concat(date-last/month,'/00/',date-last/year)"/>
                  <xsl:text>)</xsl:text>

                  <xsl:value-of select="name-dual"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="phone10"/>
                </p>

                <p class=""  style="font-weight:bold; text-decoration:underline;">
                  <xsl:text>Current phones listed at this address:</xsl:text>
                </p>

                <xsl:for-each select="phones/phone">
                  <p class="indent_more_more">
                    <xsl:value-of select="name-dual"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="phone10"/>
                  </p>
                </xsl:for-each>
              </xsl:for-each>

              <p class="" style="font-weight:bold; text-decoration:underline;">
                <xsl:text>Address(es):</xsl:text>
              </p>
              <xsl:for-each select="addresses/address">
                <p class="indent_more">
                  <xsl:if test="verified = 'yes'">
                    <font class="fFont" color="blue" style="font-size:16px;">
                      <b>V</b>
                    </font>
                    <xsl:text/>
                  </xsl:if>
                  <xsl:if test="shared = 'yes'">
                    <font class="fFont" color="blue" style="font-size:16px;">
                      <b>S</b>
                    </font>
                    <xsl:text/>
                  </xsl:if>
                  <xsl:value-of select="street-number"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="street-name"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="street-suffix"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="unit-designation"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="unit-number"/>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="city"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="state"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="zip"/>
                  <xsl:text>-</xsl:text>
                  <xsl:value-of select="zip4"/>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="county"/>
                  <xsl:text> COUNTY </xsl:text>

                  <xsl:value-of select="phones/phone/name-dual"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="phones/phone/phone10"/>
                </p>
                <p class="" style="font-weight:bold; text-decoration:underline;">
                  <xsl:text>Current phones listed at this address:</xsl:text>
                </p>
                <xsl:for-each select="phones/phone">
                  <p class="indent_more_more">
                    <xsl:value-of select="name-dual"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="phone10"/>
                  </p>
                </xsl:for-each>
              </xsl:for-each>
            </td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
            </td>
            <td colspan="5" align="right" style="background-color: rgb(255, 255, 255); text-align: right; color: red; font-size: 13px; font-weight: bold; height: 15px; padding-left: 128px; width: 100%;">
              <table style="background-color:#fff;width:100%" >
                <tr>
                  <td style="background-color:#fff;width:90%">
                    <textarea id="txtNoteTDR_{(position())}_NEIGH" rows="2" name="txtNote_{(position())}" style="width:100%;background-color:#FFFFE1;">
                      <xsl:value-of select="ReviewNote"/>
                    </textarea>
                    <textarea id="txthdnNoteTDR_{(position())}_NEIGH" rows="2" name="txtNote_{(position())}" style="width:100%;background-color:#FFFFE1;display:none">
                      <xsl:value-of select="ReviewNote"/>
                    </textarea>
                  </td>
                  <td align="right" valign="bottom" style="background-color:#fff;vertical-align:bottom">
                    <input type="button" id="btnUpdateRecord_TDR_{(position())}_NEIGH" value="Save Changes" onclick="SetHiddenFieldsForTDR(this.id)" wrap="off" class="btn btn-default"/>
                  </td>
                </tr>
              </table>
            </td>
            <td style="background-color:#fff;" >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
        </xsl:for-each>
        <xsl:if test="count(//response/individual/neighbors/neighborhood) = 0">
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">
              [None Found]
            </td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
        </xsl:if>
      </tbody>
      <tfoot>
        <td ></td>
        <td style="border-right: none;" colspan="100%"></td>
        <td ></td>
      </tfoot>
    </table>

    <xsl:if test="XML_INTERFACE/CREDITREPORT/OBJECTS/BUREAUERROR = 'True' or XML_INTERFACE/CREDITREPORT/cp_xmldirect/general_error !='' ">
      <table id="peopleTable" class="reporttable" border="0" cellpadding="0"  style="width:100%;" cellspacing="0">
        <thead>
          <tr>
            <th ></th>
            <th colspan="5">Search Results</th>
            <th ></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td >
              <xsl:value-of select="(position() )"/>
            </td>
            <td colspan="5">

              <xsl:value-of select="substring-after(XML_INTERFACE/CREDITREPORT/HTMLREPORT/MESSAGE, 'body&gt;')" disable-output-escaping="yes" />

            </td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
        </tbody>
        <tfoot>
          <td ></td>
          <td colspan="5"></td>
          <td ></td>
        </tfoot>
      </table>
    </xsl:if>

  </xsl:template>
</xsl:stylesheet>
