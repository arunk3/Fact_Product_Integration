<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="iso-8859-1"/>

	<xsl:template match="/">


		<div class="sectionsummary">
			<h3>
				(<xsl:value-of select="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/rows_returned" />) Results Evictions Report
			</h3>
			<br />
		</div>

		<xsl:if test="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/rows_returned = '0'">
			<table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
				<thead>
					<tr>
						<th ></th>
						<th colspan="5">Search Information</th>
						<th ></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td >i</td>
						<td colspan="5">
							No Records Found.
						</td>
						<td >
							<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
						</td>
					</tr>
				</tbody>
				<tfoot>
					<td ></td>
					<td colspan="5"></td>
					<td ></td>
				</tfoot>
			</table>
		</xsl:if>

		<xsl:if test="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/rows_returned != '0'">
			<table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">

				<tbody>
					<!--<xsl:value-of select="substring-after(XML_INTERFACE/CREDITREPORT/HTMLREPORT/MESSAGE, '&lt;body bgcolor=&quot;white&quot; leftmargin=&quot;0&quot; topmargin=&quot;0&quot; marginwidth=&quot;0&quot; marginheight=&quot;0&quot;&gt;')" disable-output-escaping="yes" />-->
                    <xsl:value-of select="XML_INTERFACE/CREDITREPORT/HTMLREPORT/MESSAGE" disable-output-escaping="yes" />
				</tbody>

			</table>
		</xsl:if>

	</xsl:template>
</xsl:stylesheet>