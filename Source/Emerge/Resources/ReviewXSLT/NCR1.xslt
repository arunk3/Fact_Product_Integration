<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:a="http://ns.hr-xml.org/2004-08-02">
  <xsl:output method="html" encoding="iso-8859-1" />
  <xsl:template match="/">
    <xsl:variable name="pkOrderDetailId" select="a:BackgroundReports/@OrderDetailId"></xsl:variable>
    <input type="text" id="NCRpkOrderDetailId" style="display:none" value="{($pkOrderDetailId)}"/>
    <script>
      var DeleteRecords=0; TotalRecords=0;

      var CriminalCount=1; var RecClear='';

    </script>
    <center>
      <div class="sectionsummary" style="border:1px solid #9B004F; text-align:center;margin-bottom: 10px;">
        <h3 style="text-align:center;">
          <!--<xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)" />-->


          (<span id="TotalRec_{($pkOrderDetailId)}"></span>) Results CRIMINAL REPORT.<br />

          <xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase">
            <xsl:sort select="substring-after(a:CourtName,'-')"/>
            <script>
              TotalRecords++;
            </script>
            <xsl:choose>
              <xsl:when test ="@Deleted">
                <xsl:if test ="@Deleted='1'">
                  <script>
                    DeleteRecords++;
                  </script>
                </xsl:if>

              </xsl:when>
            </xsl:choose>
          </xsl:for-each>

          <script>

            var NCRpkOrderDetailId= document.getElementById("NCRpkOrderDetailId").value;

            var TotalRec= document.getElementById("TotalRec_"+NCRpkOrderDetailId);
            TotalRec.innerHTML=Number(TotalRecords)-Number(DeleteRecords);
            if((Number(TotalRecords)-Number(DeleteRecords))==0){
            RecClear='criminal record : clear';
            }
          </script>
        </h3>
      </div>

      <xsl:variable name="TotalRecords">
        <xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)" />
      </xsl:variable>

      <xsl:if test="$TotalRecords=0">

        <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
          <thead>
            <tr class="ncr_header_top">
              <th class="begin ncr_header_top" style="border: none;"></th>
              <th colspan="5" style="float: left; padding-top: 5px;">
                <span style="font-size:15px;font-weight:bold; color:#666;">Search Information</span>
              </th>
              <th class="end ncr_header_top" style="border: none;"></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="begin" style="border: none;">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colspan="5">
                <h4>Name:</h4>
                <h4>
                  <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:GivenName"/>
                  <xsl:text xml:space="preserve">  </xsl:text>
                  <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:FamilyName"/>
                </h4>
                <br />
                <h4>DOB:</h4>
                <h4>
                  <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:DemographicDetail/a:DateOfBirth"/>
                </h4>
              </td>
              <td class="end" style="border: none;">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </tbody>
          <!--<tfoot>
            <td class="begin" style="border: none;"></td>
            <td colspan="5"></td>
            <td class="end" style="border: none;"></td>
          </tfoot>-->
        </table>

      </xsl:if>

      <table id="tblNCR" class="reporttable" border="0" cellpadding="0" cellspacing="0">
        <thead>
          <tr class="ncr_header_data">
            <th class="begin" style="border: none; width:40px;"></th>
            <th colspan="5">
              <span style="font-size: 15px;font-weight:bold; padding-top: 3px; display: inline-block;color: #666;padding-bottom: 8px;padding-top: 8px;">National Database Criminal Results</span>
            </th>
            <th class="end" style="border: none;"></th>
          </tr>
          <tr>
            <td class="begin" style="border: none;">
            </td>
            <td align="left" colspan="5"  style="color:red;">
              <br/>
              <b>
                <script type="text/javascript">
                  document.write(RecClear);
                </script>
                <span id="RecClear"></span>
              </b>
              <br/>
            </td>
            <td class="end" style="border: none;">
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
        </thead>
        <tbody>

          <xsl:variable name="TextData">
            <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text" />
          </xsl:variable>

          <xsl:variable name="SSNNo">
            xxx-xx-<xsl:value-of select="substring(a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:DemographicDetail/a:GovernmentId,6,4)" />
          </xsl:variable>

          <xsl:if test="not(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text)">
            <tr>
              <td class="begin" style="border: none;">
              </td>
              <td colspan="5">
                <div style="width: 90%; margin: 0 auto; text-align: left">
                  <div style="width:588px; text-align:left">
                    <div style="font-size:11px;" id="report_ncr1">
                      <pre>
                        <!--<xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text">
                        <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text" disable-output-escaping="yes"/>
                      </xsl:if>-->
                        <xsl:if test="not(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text)">
                          <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus = 'Cancelled'">
                            <p>
                              There was an error running this report.<br /> Please contact our customer support team to quickly resolve this issue.
                            </p>
                          </xsl:if>
                          <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus != 'Cancelled'">
                          </xsl:if>
                          <p></p>
                        </xsl:if>
                      </pre>
                    </div>
                  </div>
                </div>
              </td>
              <td class="end" style="border: none;">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="(count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)='0')">
            <tr>
              <td class="begin" style="border: none;">
              </td>
              <td align="left" colspan="5"  style="color:red;">
                <br/>
                <b>

                </b>
                <br/>
              </td>
              <td class="end" style="border: none;">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>

          <xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase">
            <!--<xsl:sort select="a:CourtState"/>
            <xsl:sort select="a:DispDate" order="descending" />-->
            <xsl:choose>
              <xsl:when test ="@Deleted">

                <xsl:if test ="@Deleted!='1'">

                  <xsl:choose>
                    <xsl:when test="position()>1">
                      <tr style="height:20px;">
                        <td class="begin" style="border: none;">
                        </td>
                        <td colspan="5" style="text-align:center;">
                          <span style="font-size:16px;font-weight:bold;color:white;">
                          </span>
                        </td>
                        <td class="end" style="border: none;"></td>
                      </tr>
                    </xsl:when>
                  </xsl:choose>
                  <tr>
                    <td id="StateHeader" name="StateHeader" colspan="5" style="display: none;height:37px;border-style: groove !important; max-width: 94%;margin-left:38px;;font-weight: bold; font-size: 15px !important;; position: absolute; right: 0px; left: 0px;background-image: url('https://emerge.intelifi.com/content/themes/base/images/top_center.png');background-repeat: repeat-x;">
                      <b>
                        Review  Records Found In <xsl:value-of select="a:CourtState"/>
                      </b>
                      <span id="isPossiblespan" style="display:none">
                        <xsl:value-of select="a:PossibleMatch"/>
                      </span>
                    </td>
                  </tr>

                  <tr>
                    <td colspan="5" height="40">
                    </td>
                  </tr>
                  <tr class="ncr_header_data">

                    <td class="ncr_header_data" colspan="5" style="border:solid 0px #ccc;text-align:left;">
                      <div style="padding: 15px; color: rgb(102, 102, 102); float: left;width: 156px;padding-left: 0px;">
                        <div class="file-name" style="float: left; font-size: 15px; font-weight: normal; padding-top: 0px;">
                          Criminal Record
                        </div>
                      </div>
                      <!--<script type="text/javascript">
                        document.write(CriminalCount);
                        CriminalCount++;
                        
                      </script>-->

                      <span class="rednosetagainnumber" style="padding-top:6px;margin-left: 0px;margin-top: 13px;">
                        <xsl:value-of select="position()" />
                      </span>
                    </td>
                    <td class="ncr_header_data"></td>
                    <td class="ncr_header_data"></td>
                  </tr>
                  <tr>
                    <td class="begin" style="border: none;">
                    </td>
                    <td colspan="5" style="text-align:center;">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                          <td width="44%" class="column1">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:30%;">
                                  Source:
                                </td>
                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:value-of select="a:CourtName"/>
                                </td>
                              </tr>

                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                  Full Name:
                                </td>
                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                    <xsl:if test="a:FamilyName/@primary='true'">
                                      <xsl:value-of select="a:FamilyName"/>
                                      <xsl:if test="(a:GivenName)">
                                        <span>, </span>
                                      </xsl:if>
                                      <xsl:value-of select="a:GivenName"/>
                                      <xsl:if test="(a:MiddleName)">
                                        <span>, </span>
                                      </xsl:if>
                                      <xsl:value-of select="a:MiddleName"/>
                                      <xsl:element name="br"/>
                                    </xsl:if>
                                  </xsl:for-each>
                                </td>
                              </tr>

                              <xsl:if test="(a:SubjectIdentification/a:PersonName/a:FamilyName/@primary='false')">
                                <tr>
                                  <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                    AKA Name:
                                  </td>
                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                    <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                      <xsl:if test="a:FamilyName/@primary='false'">
                                        <xsl:value-of select="a:FamilyName"/>
                                        <xsl:if test="(a:GivenName)">
                                          <span>, </span>
                                        </xsl:if>
                                        <xsl:value-of select="a:GivenName"/>
                                        <xsl:if test="(a:MiddleName)">
                                          <span>, </span>
                                        </xsl:if>
                                        <xsl:value-of select="a:MiddleName"/>
                                        <xsl:element name="br"/>
                                      </xsl:if>
                                    </xsl:for-each>
                                  </td>
                                </tr>
                              </xsl:if>

                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                  DOB Recorded:
                                </td>
                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:value-of select='substring(a:SubjectIdentification/a:DemographicDetail/a:DateOfBirth,1,10)' />
                                </td>
                              </tr>
                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;color:#ff0000;">
                                  DOB Entered By User:
                                </td>
                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;color:#ff0000 important;">
                                  <span class="lblDateEnteredByUser" style="color:#ff0000 !important;">UserDOB</span>
                                </td>
                              </tr>

                              <!--<xsl:if test="($SSNNo) != ''">
                          <tr>
                            <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                              <b>SSN:</b>
                            </td>
                            <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                              <xsl:value-of select ="$SSNNo"/>
                            </td>
                          </tr>
                        </xsl:if>-->
                            </table>
                          </td>
                          <td width="44%" class="column2">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <xsl:if test="(a:SubjectIdentification/a:DemographicDetail/a:GovernmentId/@documentType='DRIVERS LICENSE')">
                                <tr>
                                  <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                    DL#:
                                  </td>
                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                    <xsl:value-of select ="a:SubjectIdentification/a:DemographicDetail/a:GovernmentId[@documentType='DRIVERS LICENSE']"/>
                                  </td>
                                </tr>
                              </xsl:if>

                              <xsl:if test="(a:SubjectIdentification/a:PostalAddress)">
                                <tr>
                                  <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                    Address:
                                  </td>
                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                    <xsl:for-each select="a:SubjectIdentification/a:PostalAddress">
                                      <xsl:if test="(a:DeliveryAddress)">
                                        <xsl:value-of select ="a:DeliveryAddress"/>
                                        <xsl:if test="(a:Municipality)">
                                          <span>, </span>
                                        </xsl:if>
                                      </xsl:if>
                                      <xsl:if test="(a:Municipality)">
                                        <xsl:value-of select ="a:Municipality"/>
                                        <xsl:if test="(a:Region)">
                                          <span>, </span>
                                        </xsl:if>
                                      </xsl:if>
                                      <xsl:if test="(a:Region)">
                                        <xsl:value-of select ="a:Region"/>
                                        <xsl:if test="(a:PostalCode)">
                                          <span> - </span>
                                        </xsl:if>
                                      </xsl:if>
                                      <xsl:if test="(a:PostalCode)">
                                        <xsl:value-of select ="a:PostalCode"/>
                                      </xsl:if>
                                      <xsl:element name="br" />
                                    </xsl:for-each>
                                  </td>
                                </tr>
                              </xsl:if>

                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                  Race:
                                </td>
                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:value-of select="a:SubjectIdentification/a:DemographicDetail/a:Race"/>
                                </td>
                              </tr>

                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                  Gender:
                                </td>
                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:choose>
                                    <xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:GenderCode = 1">
                                      Male
                                    </xsl:when>
                                    <xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:GenderCode = 2">
                                      Female
                                    </xsl:when>
                                    <xsl:otherwise>UnKnown</xsl:otherwise>
                                  </xsl:choose>
                                </td>
                              </tr>

                              <xsl:if test="(a:AgencyReference[@type='Docket'])">
                                <tr>
                                  <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                    Docket Number:
                                  </td>

                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                    <xsl:value-of select="a:AgencyReference[@type='Docket']"/>
                                  </td>
                                </tr>
                              </xsl:if>

                              <xsl:if test="(a:CaseFileDate)">
                                <tr>
                                  <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:30%;">

                                    Case Filed:

                                  </td>

                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                    <xsl:choose>
                                      <xsl:when test="count(a:CaseFileDate) &gt; 1">
                                        <xsl:for-each select="a:CaseFileDate">
                                          <span style="float:left;">
                                            <xsl:value-of select='.' />
                                            <xsl:element name="br" />
                                          </span>
                                        </xsl:for-each>
                                      </xsl:when>
                                      <xsl:when test="count(a:CaseFileDate)=1">
                                        <xsl:value-of select='substring(a:CaseFileDate,1,10)' />
                                      </xsl:when>
                                    </xsl:choose>
                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="a:ReviewNote!= ''">
                                <tr style="display:none;">
                                  <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:30%;">

                                    Review Note:

                                  </td>
                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                    <xsl:if test="count(a:ReviewNote) &gt; 1">
                                      <xsl:for-each select="a:ReviewNote">
                                        <span style="float:left;">
                                          <xsl:value-of select='.' />
                                          <xsl:element name="br" />
                                        </span>
                                      </xsl:for-each>
                                    </xsl:if>
                                    <xsl:if test="count(a:ReviewNote)=1">
                                      <xsl:value-of select='a:ReviewNote' />
                                    </xsl:if>
                                  </td>
                                </tr>
                              </xsl:if>
                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:30%;">

                                  Additional Events:

                                </td>
                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:if test="count(a:AdditionalItems/a:EffectiveDate) &gt; 0">
                                    <input id="imageColapseDOBVARIATIONS" type="image" src="../content/themes/base/images/plus_symbol.png" style="width:10px; height:10px;" value="myValue" alt="" />
                                  </xsl:if>
                                  <div style="display:block;">

                                    <xsl:for-each select="a:AdditionalItems/a:EffectiveDate">
                                      <xsl:value-of  select='../a:Text' />
                                      <xsl:text xml:space="preserve">  </xsl:text>
                                      <xsl:value-of  select='substring(a:StartDate/a:AnyDate,1,10)' />
                                      <xsl:element name="br" />
                                    </xsl:for-each>

                                  </div>
                                </td>
                              </tr>
                            </table>
                          </td>
                          <xsl:if test="(a:MatchMeter)">
                            <!--<td width="12%" class="matchmeter">-->
                            <td width="12%" class="matchmeter" style="visibility: hidden">
                              <table border="0" style="background: transparent;" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                  <td style="background: transparent;font-size:11px;width:70%;height:24px;text-align:right;text-transform:capitalize !important;padding:0px;">
                                    Name Match
                                  </td>
                                  <td style="background: transparent;text-align:center;height:22px;padding:0px;">
                                    <xsl:text xml:space="preserve">  </xsl:text>
                                    <xsl:if test="(a:MatchMeter/a:Name) = '2'">
                                      <div class="matchgreen" />
                                      <div class="width_class" style="margin-left:15%;" ></div>
                                      <img  class="matchgreen2" src="../../content/themes/base/images/ico_match_green.jpg"></img>
                                    </xsl:if>
                                    <xsl:if test="(a:MatchMeter/a:Name) = '1'">
                                      <div class="matchyellow" />
                                      <div class="width_class" style="margin-left:15%;" ></div>
                                      <img class="matchyellow2" src="../../content/themes/base/images/ico_red_yellow.png"></img>
                                    </xsl:if>
                                    <xsl:if test="(a:MatchMeter/a:Name) = '0'">
                                      <div class="matchred" />
                                      <div class="width_class" style="margin-left:15%;" ></div>
                                      <img class="matchred2" src="../../content/themes/base/images/ico_red_match.png"></img>
                                    </xsl:if>
                                  </td>
                                </tr>
                                <tr>
                                  <td style="background: transparent;font-size:11px;width:70%;height:25px;text-align:right;text-transform:capitalize !important;padding:0px;">
                                    Address Match
                                  </td>
                                  <td style="background: transparent;text-align:center;height:22px;padding:0px;">
                                    <xsl:text xml:space="preserve">  </xsl:text>
                                    <xsl:if test="(a:MatchMeter/a:Address) = '2'">
                                      <div class="matchgreen" />
                                      <div class="width_class" style="margin-left:15%;" ></div>
                                      <img  class="matchgreen2" src="../../content/themes/base/images/ico_match_green.jpg"></img>
                                    </xsl:if>
                                    <xsl:if test="(a:MatchMeter/a:Address) = '4'">
                                      <div class="matchblue" />
                                      <div class="width_class" style="margin-left:15%;" ></div>
                                      <img  class="matchblue2" src="../../content/themes/base/images/ico_match_blue.jpg"></img>
                                    </xsl:if>
                                    <xsl:if test="(a:MatchMeter/a:Address) = '1'">
                                      <div class="matchyellow" />
                                      <div class="width_class" style="margin-left:15%;" ></div>
                                      <img class="matchyellow2" src="../../content/themes/base/images/ico_red_yellow.png"></img>
                                    </xsl:if>
                                    <xsl:if test="(a:MatchMeter/a:Address) = '0'">
                                      <div class="matchred" />
                                      <div class="width_class" style="margin-left:15%;" ></div>
                                      <img class="matchred2" src="../../content/themes/base/images/ico_red_match.png"></img>
                                    </xsl:if>
                                    <xsl:if test="(a:MatchMeter/a:Address) = '-1'">
                                      <div class="matchgray"></div>
                                      <div class="width_class" style="margin-left:15%;" ></div>
                                      <img class="matchgray2" src="../../content/themes/base/images/ico_darkgrey_match.png"></img>
                                    </xsl:if>
                                  </td>
                                </tr>
                                <tr>
                                  <td style="background: transparent;font-size:11px;width:70%;height:28px;text-align:right;text-transform:capitalize !important;padding:0px;">
                                    DOB Match
                                  </td>
                                  <td style="background: transparent;text-align:center;height:28px;padding:0px;">
                                    <xsl:text xml:space="preserve">  </xsl:text>
                                    <xsl:if test="(a:MatchMeter/a:DOB) = '2'">
                                      <div class="matchgreen" />
                                      <div class="width_class" style="margin-left:15%;" ></div>
                                      <img class="matchgreen2" src="../../content/themes/base/images/ico_match_green.jpg"></img>
                                    </xsl:if>
                                    <xsl:if test="(a:MatchMeter/a:DOB) = '1'">
                                      <div class="matchyellow" />
                                      <div class="width_class" style="margin-left:15%;" ></div>
                                      <img class="matchyellow2" src="../../content/themes/base/images/ico_red_yellow.png"></img>
                                    </xsl:if>
                                    <xsl:if test="(a:MatchMeter/a:DOB) = '0'">
                                      <div class="matchred" />
                                      <div class="width_class" style="margin-left:15%;" ></div>
                                      <img class="matchred2" src="../../content/themes/base/images/ico_red_match.png"></img>
                                    </xsl:if>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </xsl:if>
                        </tr>
                      </table>
                    </td>
                    <td class="end" style="border: none;"></td>
                  </tr>

                  <xsl:for-each select="a:Charge">
                    <tr style="border:1px solid #999;">
                      <td class="begin" style="border: none;">
                      </td>
                      <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#fff;text-align:center;">
                          <tr>
                            <xsl:choose>
                              <xsl:when test="(position()mod 2)=1">
                                <td width="10%" style="border-right:solid 0px black;text-align:center;">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                      <td style="text-align:right;">
                                        <div style="padding: 15px; color: rgb(102, 102, 102); float: left; width: 143px;">
                                          <div class="file-name" style="float:left; padding-top: 4px; color:#232323 !important;">
                                            Charge
                                          </div>
                                          <span class="blue-no text-pad" style="float:left; padding-top: 6px;">
                                            <xsl:value-of select="(position())" />
                                          </span>
                                        </div>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </xsl:when>
                              <xsl:otherwise>
                                <td width="10%" style="border-right:solid 0px black;background-color:white;text-align:center;">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                      <td style="text-align:right;background-color:white;">
                                        <div style="padding: 15px; color: rgb(102, 102, 102); float: left; width: 143px;">
                                          <div class="file-name" style="float:left; padding-top: 4px; color:#232323 !important;">
                                            Charge
                                          </div>
                                          <span class="blue-no text-pad" style="float:left; padding-top: 6px;">
                                            <xsl:value-of select="(position())" />
                                          </span>
                                        </div>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </xsl:otherwise>
                            </xsl:choose>
                            <xsl:choose>
                              <xsl:when test="(position()mod 2)=1">
                                <td width="45%" style="">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                      <td style="text-align:right;border-bottom:0px solid black;width:25%;">
                                        Charge:
                                      </td>
                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:value-of select="a:ChargeOrComplaint"/>
                                      </td>
                                    </tr>
                                    <xsl:if test="(a:ChargeDate)">
                                      <tr>
                                        <td style="text-align:right;border-bottom:0px solid black;">
                                          Charge Date:
                                        </td>

                                        <td style="text-align:left;border-bottom:0px solid black;">
                                          <xsl:value-of select='substring(a:ChargeDate,1,10)' />
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="(a:ArrestDate)">
                                      <tr>
                                        <td style="text-align:right;border-bottom:0px solid black;">
                                          Arrest Date:
                                        </td>

                                        <td style="text-align:left;border-bottom:0px solid black;">
                                          <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                        </td>
                                      </tr>
                                    </xsl:if>

                                    <xsl:if test="(a:OffenseDate)">
                                      <tr>
                                        <td style="text-align:right;border-bottom:0px solid black;">
                                          Offense Date:
                                        </td>

                                        <td style="text-align:left;border-bottom:0px solid black;">
                                          <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                        </td>
                                      </tr>
                                    </xsl:if>

                                    <xsl:if test="(a:DispositionDate)">
                                      <tr>
                                        <td style="text-align:right;border-bottom:0px solid black;">

                                          Disposition Date:

                                        </td>

                                        <td style="text-align:left;border-bottom:0px solid black;">
                                          <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="(../a:AdditionalItems[@type='TrialDate'])">
                                      <tr>
                                        <td style="text-align:right;border-bottom:0px solid black;">

                                          Trial Date:

                                        </td>

                                        <td style="text-align:left;border-bottom:0px solid black;">
                                          <xsl:value-of select="substring(../a:AdditionalItems[@type='TrialDate'],1,10)" />
                                        </td>
                                      </tr>

                                    </xsl:if>
                                  </table>
                                </td>
                                <td width="45%" style="">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <xsl:if test="(a:ChargeTypeClassification)!='unknown'">
                                      <tr>
                                        <td style="text-align:right;border-bottom:0px solid black;width:25%;">

                                          Severity:

                                        </td>

                                        <td style="text-align:left;border-bottom:0px solid black;">
                                          <xsl:value-of select="a:ChargeTypeClassification"/>
                                        </td>
                                      </tr>
                                    </xsl:if>

                                    <xsl:if test="(a:Disposition)">
                                      <tr>
                                        <td style="text-align:right;border-bottom:0px solid black;">

                                          Final Disposition:

                                        </td>

                                        <td style="text-align:left;border-bottom:0px solid black;">
                                          <xsl:choose>
                                            <xsl:when test="(a:Disposition)='unknown'">
                                              Manual County or State Report needed for Final Results
                                            </xsl:when>
                                            <xsl:otherwise>
                                              <xsl:choose>
                                                <xsl:when test="(contains(../a:CourtName, 'SAN BERNARDINO') or contains(../a:CourtName, 'RIVERSIDE')) and (a:Disposition)='NOT CONVICTED'">
                                                  Unknown
                                                </xsl:when>
                                                <xsl:otherwise>
                                                  <xsl:value-of select="a:Disposition"/>
                                                </xsl:otherwise>
                                              </xsl:choose>
                                            </xsl:otherwise>
                                          </xsl:choose>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="(a:Sentence)">
                                      <tr>
                                        <td style="text-align:right;border-bottom:0px solid black;">
                                          Sentence:
                                        </td>
                                        <td style="text-align:left;border-bottom:0px solid black;">
                                          <xsl:value-of select='(a:Sentence)' />
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="(a:Comment)">
                                      <tr>
                                        <td style="text-align:right;border-bottom:0px solid black;">

                                          Comments:

                                        </td>

                                        <td style="text-align:left;border-bottom:0px solid black;">
                                          <xsl:for-each select="a:Comment">
                                            <span style="float:left;">
                                              <xsl:value-of select="."/>
                                              <xsl:element name="br" />
                                            </span>
                                          </xsl:for-each>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                  </table>
                                </td>
                              </xsl:when>
                              <xsl:otherwise>
                                <td width="45%">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                        Charge:
                                      </td>
                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:value-of select="a:ChargeOrComplaint"/>
                                      </td>
                                    </tr>
                                    <xsl:if test="(a:ChargeDate)">
                                      <tr>
                                        <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                          Charge Date:
                                        </td>

                                        <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                          <xsl:value-of select='substring(a:ChargeDate,1,10)' />
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="(a:ArrestDate)">
                                      <tr>
                                        <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                          Arrest Date:
                                        </td>

                                        <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                          <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                        </td>
                                      </tr>
                                    </xsl:if>

                                    <xsl:if test="(a:OffenseDate)">
                                      <tr>
                                        <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                          Offense Date:
                                        </td>

                                        <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                          <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                        </td>
                                      </tr>
                                    </xsl:if>

                                    <xsl:if test="(a:DispositionDate)">
                                      <tr>
                                        <td style="text-align:right;background-color:white;border-bottom:0px solid black;">

                                          Disposition Date:

                                        </td>

                                        <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                          <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="(../a:AdditionalItems[@type='TrialDate'])">
                                      <tr>
                                        <td style="text-align:right;background-color:white;border-bottom:0px solid black;">

                                          Trial Date:

                                        </td>

                                        <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                          <xsl:value-of select="substring(../a:AdditionalItems[@type='TrialDate'],1,10)" />
                                        </td>
                                      </tr>
                                    </xsl:if>
                                  </table>
                                </td>
                                <td width="45%">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <xsl:if test="(a:ChargeTypeClassification)!='unknown'">
                                      <tr>
                                        <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">

                                          Severity:

                                        </td>

                                        <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                          <xsl:value-of select="a:ChargeTypeClassification"/>
                                        </td>
                                      </tr>
                                    </xsl:if>

                                    <xsl:if test="(a:Disposition)">
                                      <tr>
                                        <td style="text-align:right;background-color:white;border-bottom:0px solid black;">

                                          Final Disposition:

                                        </td>

                                        <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                          <xsl:choose>
                                            <xsl:when test="(a:Disposition)='unknown'">
                                              Manual County or State Report needed for Final Results
                                            </xsl:when>
                                            <xsl:otherwise>
                                              <xsl:choose>
                                                <xsl:when test="(contains(../a:CourtName, 'SAN BERNARDINO') or contains(../a:CourtName, 'RIVERSIDE')) and (a:Disposition)='NOT CONVICTED'">
                                                  Unknown
                                                </xsl:when>
                                                <xsl:otherwise>
                                                  <xsl:value-of select="a:Disposition"/>
                                                </xsl:otherwise>
                                              </xsl:choose>
                                            </xsl:otherwise>
                                          </xsl:choose>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="(a:Sentence)">
                                      <tr>
                                        <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                          Sentence:
                                        </td>
                                        <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                          <xsl:value-of select='(a:Sentence)' />
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="(a:Comment)">
                                      <tr>
                                        <td style="text-align:right;background-color:white;border-bottom:0px solid black;">

                                          Comments:

                                        </td>

                                        <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                          <xsl:for-each select="a:Comment">
                                            <span style="float:left;">
                                              <xsl:value-of select="."/>
                                              <xsl:element name="br" />
                                            </span>
                                          </xsl:for-each>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                  </table>
                                </td>
                              </xsl:otherwise>
                            </xsl:choose>
                          </tr>
                        </table>
                      </td>
                      <td class="end" style="border: none;"></td>
                    </tr>
                    <tr height="10px">
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </xsl:for-each>
                  <xsl:if test="count(a:Charge)=0">
                    <tr    style="border:1px solid #999;">
                      <td class="begin" style="border: none;">
                      </td>
                      <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#fff;text-align:center;">
                          <tr>
                            <td width="10%" style="border-right:solid 0px black; text-align:center;">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                  <td style="text-align:right;">
                                    <span style="font-size:12px;font-weight:bold;color:#000;">
                                      Additional Info
                                    </span>
                                  </td>
                                </tr>
                              </table>
                            </td>
                            <td width="80%" style="text-align:left;">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                  <td style="text-align:left;padding-left:5px;">
                                    NOTE: THE DATA REPORTED BY THE COURT FOR THIS CASE IS INCOMPLETE. MANUAL COUNTY SEARCH REQUIRED.<br/>
                                    <span style="padding-left:35px">IF THIS RECORD DOES NOT SPECIFY THE COUNTY, A MANUAL STATE SEARCH IS REQUIRED.</span>
                                  </td>
                                </tr>


                                <!--<xsl:for-each select="a:AdditionalItems">
                                  <tr>
                                    <td style="background-color:#dcdcdc;text-align:left;padding-left:5px;">
                                      <xsl:value-of select="a:Text"/>
                                      <xsl:element name="br" />
                                    </td>
                                  </tr>
                                </xsl:for-each>-->
                              </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td class="end" style="border: none;"></td>
                    </tr>
                    <tr height="10px">
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </xsl:if>
                  <xsl:if test="a:ReviewNote!=''">
                    <tr>
                      <td class="begin" style="border: none; background-color:#FFFFE1;">
                      </td>
                      <td colspan="4" style="width:95%;background-color:#FFFFE1; border:none;" class="ReviewNote">
                        <xsl:value-of select="a:ReviewNote"></xsl:value-of>
                      </td>
                      <td align="right" valign="bottom" style="vertical-align:bottom; background-color:#FFFFE1;">

                      </td>
                      <td class="end" style="border: none; background-color:#FFFFE1;"></td>
                    </tr>
                  </xsl:if>
                </xsl:if>
              </xsl:when>
              <!--This Invoked If Deleted Attrribute Is Note Present In XML-->
              <xsl:otherwise>
                <xsl:choose>
                  <xsl:when test="position()>1">
                    <tr style="height:20px;">
                      <td class="begin" style="border: none;">
                      </td>
                      <td colspan="5" style="text-align:center;">
                        <span style="font-size:16px;font-weight:bold;color:white;">
                        </span>
                      </td>
                      <td class="end" style="border: none;"></td>
                    </tr>
                  </xsl:when>
                </xsl:choose>
                <tr>
                  <td id="StateHeader" name="StateHeader" colspan="5" style="display: none;height:37px;border-style: groove !important;max-width: 94%;margin-left:38px;;font-weight: bold; font-size: 15px !important;; position: absolute; right: 0px; left: 0px;background-image: url('https://emerge.intelifi.com/content/themes/base/images/top_center.png');background-repeat: repeat-x;">
                    <b>
                      Review  Records Found In <xsl:value-of select="a:CourtState"/>
                    </b>
                    <span id="isPossiblespan" style="display:none">
                      <xsl:value-of select="a:PossibleMatch"/>
                    </span>
                  </td>
                </tr>

                <tr>
                  <td colspan="5" height="40">
                  </td>
                </tr>
                <tr class="ncr_header_data">
                  <td  class="ncr_header_data">
                  </td>
                  <td  class="ncr_header_data" colspan="5" style="border:solid 0px #ccc;text-align:left;">
                    <div style="padding: 15px; color: rgb(102, 102, 102); float: left;width: 156px;padding-left: 0px;">
                      <div class="file-name" style="float: left; font-size: 15px; font-weight: normal; padding-top: 0px;">
                        Criminal Record
                      </div>
                    </div>
                    <!--<script type="text/javascript">
                        document.write(CriminalCount);
                        CriminalCount++;
                      </script>-->
                    <span class="rednosetagainnumber" style="padding-top:6px;margin-left: 0px;margin-top: 13px;">
                      
                    </span>
                  </td>
                  <td  class="ncr_header_data"></td>
                </tr>
                <tr>
                  <td class="begin" style="border: none;">
                  </td>
                  <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;padding:0px;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                      <tr>
                        <td width="44%">
                          <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:30%;">
                                Source:
                              </td>
                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                <xsl:value-of select="a:CourtName"/>
                              </td>
                            </tr>

                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                Full Name:
                              </td>
                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                  <xsl:if test="a:FamilyName/@primary='true'">
                                    <xsl:value-of select="a:FamilyName"/>
                                    <xsl:if test="(a:GivenName)">
                                      <span>, </span>
                                    </xsl:if>
                                    <xsl:value-of select="a:GivenName"/>
                                    <xsl:if test="(a:MiddleName)">
                                      <span>, </span>
                                    </xsl:if>
                                    <xsl:value-of select="a:MiddleName"/>
                                    <xsl:element name="br"/>
                                  </xsl:if>
                                </xsl:for-each>
                              </td>
                            </tr>

                            <xsl:if test="(a:SubjectIdentification/a:PersonName/a:FamilyName/@primary='false')">
                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                  AKA Name:
                                </td>
                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                    <xsl:if test="a:FamilyName/@primary='false'">
                                      <xsl:value-of select="a:FamilyName"/>
                                      <xsl:if test="(a:GivenName)">
                                        <span>, </span>
                                      </xsl:if>
                                      <xsl:value-of select="a:GivenName"/>
                                      <xsl:if test="(a:MiddleName)">
                                        <span>, </span>
                                      </xsl:if>
                                      <xsl:value-of select="a:MiddleName"/>
                                      <xsl:element name="br"/>
                                    </xsl:if>
                                  </xsl:for-each>
                                </td>
                              </tr>
                            </xsl:if>

                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                DOB Recorded:
                              </td>
                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                <xsl:value-of select='substring(a:SubjectIdentification/a:DemographicDetail/a:DateOfBirth,1,10)' />
                                <xsl:value-of select='a:SubjectIdentification/a:DemographicDetail/a:Age' />
                                <xsl:value-of select='a:SubjectIdentification/a:DemographicDetail/a:Other' />
                              </td>
                            </tr>
                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;color:#ff0000;">
                                DOB Entered By User:
                              </td>
                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;color:#ff0000;">
                                <span class="lblDateEnteredByUser">UserDOB</span>
                              </td>
                            </tr>

                            <!--<xsl:if test="($SSNNo) != ''">
                          <tr>
                            <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                              <b>SSN:</b>
                            </td>
                            <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                              <xsl:value-of select ="$SSNNo"/>
                            </td>
                          </tr>
                        </xsl:if>-->
                          </table>
                        </td>
                        <td width="44%">
                          <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <xsl:if test="(a:SubjectIdentification/a:DemographicDetail/a:GovernmentId/@documentType='DRIVERS LICENSE')">
                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                  DL#:
                                </td>
                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:value-of select ="a:SubjectIdentification/a:DemographicDetail/a:GovernmentId[@documentType='DRIVERS LICENSE']"/>
                                </td>
                              </tr>
                            </xsl:if>

                            <xsl:if test="(a:SubjectIdentification/a:PostalAddress)">
                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                  Address:
                                </td>
                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:for-each select="a:SubjectIdentification/a:PostalAddress">
                                    <xsl:if test="(a:DeliveryAddress)">
                                      <xsl:value-of select ="a:DeliveryAddress"/>
                                      <xsl:if test="(a:Municipality)">
                                        <span>, </span>
                                      </xsl:if>
                                    </xsl:if>
                                    <xsl:if test="(a:Municipality)">
                                      <xsl:value-of select ="a:Municipality"/>
                                      <xsl:if test="(a:Region)">
                                        <span>, </span>
                                      </xsl:if>
                                    </xsl:if>
                                    <xsl:if test="(a:Region)">
                                      <xsl:value-of select ="a:Region"/>
                                      <xsl:if test="(a:PostalCode)">
                                        <span> - </span>
                                      </xsl:if>
                                    </xsl:if>
                                    <xsl:if test="(a:PostalCode)">
                                      <xsl:value-of select ="a:PostalCode"/>
                                    </xsl:if>
                                    <xsl:element name="br" />
                                  </xsl:for-each>
                                </td>
                              </tr>
                            </xsl:if>

                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                Race:
                              </td>
                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                <xsl:value-of select="a:SubjectIdentification/a:DemographicDetail/a:Race"/>
                              </td>
                            </tr>

                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                Gender:
                              </td>
                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                <xsl:choose>
                                  <xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:GenderCode = 1">
                                    Male
                                  </xsl:when>
                                  <xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:GenderCode = 2">
                                    Female
                                  </xsl:when>
                                  <xsl:otherwise>UnKnown</xsl:otherwise>
                                </xsl:choose>
                              </td>
                            </tr>

                            <xsl:if test="(a:AgencyReference[@type='Docket'])">
                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                  Docket Number:
                                </td>

                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:value-of select="a:AgencyReference[@type='Docket']"/>
                                </td>
                              </tr>
                            </xsl:if>
                            <xsl:if test="(a:CaseFileDate)">
                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:30%;">

                                  Case Filed:

                                </td>

                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:choose>
                                    <xsl:when test="count(a:CaseFileDate) &gt; 1">
                                      <xsl:for-each select="a:CaseFileDate">
                                        <span style="float:left;">
                                          <xsl:value-of select='.' />
                                          <xsl:element name="br" />
                                        </span>
                                      </xsl:for-each>
                                    </xsl:when>
                                    <xsl:when test="count(a:CaseFileDate)=1">
                                      <xsl:value-of select='substring(a:CaseFileDate,1,10)' />
                                    </xsl:when>
                                  </xsl:choose>
                                </td>
                              </tr>
                            </xsl:if>
                            <xsl:if test="a:ReviewNote!= ''">
                              <tr style="display:none;">
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:30%;">

                                  Review Note:

                                </td>
                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:if test="count(a:ReviewNote) &gt; 1">
                                    <xsl:for-each select="a:ReviewNote">
                                      <span style="float:left;">
                                        <xsl:value-of select='.' />
                                        <xsl:element name="br" />
                                      </span>
                                    </xsl:for-each>
                                  </xsl:if>
                                  <xsl:if test="count(a:ReviewNote)=1">
                                    <xsl:value-of select='a:ReviewNote' />
                                  </xsl:if>
                                </td>
                              </tr>
                            </xsl:if>
                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:30%;">

                                Additional Events:

                              </td>
                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                <xsl:if test="count(a:AdditionalItems/a:EffectiveDate) &gt; 0">
                                  <input id="imageColapse" name="test1" type="image" src="../content/themes/base/images/plus_symbol.png" style="width:10px; height:10px;" value="myValue" alt="" />
                                </xsl:if>
                                <div style="display:block;">

                                  <xsl:for-each select="a:AdditionalItems/a:EffectiveDate">
                                    <xsl:value-of  select='../a:Text' />
                                    <xsl:text xml:space="preserve">  </xsl:text>
                                    <xsl:value-of  select='substring(a:StartDate/a:AnyDate,1,10)' />
                                    <xsl:element name="br" />
                                  </xsl:for-each>

                                </div>
                              </td>
                            </tr>
                          </table>
                        </td>
                        <xsl:if test="(a:MatchMeter)">
                          <!--<td width="12%" class="matchmeter">-->
                          <td width="12%" class="matchmeter" style="visibility: hidden">
                            <table border="0" style="background: transparent;" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td style="background: transparent;font-size:11px;width:70%;height:24px;text-align:right;text-transform:capitalize !important;padding:0px;">
                                  Name Match
                                </td>
                                <td style="background: transparent;text-align:center;height:22px;padding:0px;">
                                  <xsl:text xml:space="preserve">  </xsl:text>
                                  <xsl:if test="(a:MatchMeter/a:Name) = '2'">
                                    <div class="matchgreen" />
                                  </xsl:if>
                                  <xsl:if test="(a:MatchMeter/a:Name) = '1'">
                                    <div class="matchyellow" />
                                  </xsl:if>
                                  <xsl:if test="(a:MatchMeter/a:Name) = '0'">
                                    <div class="matchred" />
                                  </xsl:if>
                                </td>
                              </tr>
                              <tr>
                                <td style="background: transparent;font-size:11px;width:70%;height:25px;text-align:right;text-transform:capitalize !important;padding:0px;">
                                  Address Match
                                </td>
                                <td style="background: transparent;text-align:center;height:22px;padding:0px;">
                                  <xsl:text xml:space="preserve">  </xsl:text>
                                  <xsl:if test="(a:MatchMeter/a:Address) = '2'">
                                    <div class="matchgreen" />
                                    <div class="width_class" style="margin-left:15%;" ></div>
                                    <img  class="matchgreen2" src="../../content/themes/base/images/ico_match_green.jpg"></img>
                                  </xsl:if>
                                  <xsl:if test="(a:MatchMeter/a:Address) = '4'">
                                    <div class="matchblue" />
                                    <div class="width_class" style="margin-left:15%;" ></div>
                                    <img  class="matchblue2" src="../../content/themes/base/images/ico_match_blue.jpg"></img>
                                  </xsl:if>
                                  <xsl:if test="(a:MatchMeter/a:Address) = '1'">
                                    <div class="matchyellow" />
                                    <div class="width_class" style="margin-left:15%;" ></div>
                                    <img class="matchyellow2" src="../../content/themes/base/images/ico_red_yellow.png"></img>
                                  </xsl:if>
                                  <xsl:if test="(a:MatchMeter/a:Address) = '0'">
                                    <div class="matchred" />
                                    <div class="width_class" style="margin-left:15%;" ></div>
                                    <img class="matchred2" src="../../content/themes/base/images/ico_red_match.png"></img>
                                  </xsl:if>
                                  <xsl:if test="(a:MatchMeter/a:Address) = '-1'">
                                    <div class="matchgray"></div>
                                    <div class="width_class" style="margin-left:15%;" ></div>
                                    <img class="matchgray2" src="../../content/themes/base/images/ico_darkgrey_match.png"></img>
                                  </xsl:if>
                                </td>
                              </tr>
                              <tr>
                                <td style="background: transparent;font-size:11px;width:70%;height:28px;text-align:right;text-transform:capitalize !important;padding:0px;">
                                  DOB Match
                                </td>
                                <td style="background: transparent;text-align:center;height:28px;padding:0px;">
                                  <xsl:text xml:space="preserve">  </xsl:text>
                                  <xsl:if test="(a:MatchMeter/a:DOB) = '2'">
                                    <div class="matchgreen" />
                                  </xsl:if>
                                  <xsl:if test="(a:MatchMeter/a:DOB) = '1'">
                                    <div class="matchyellow" />
                                  </xsl:if>
                                  <xsl:if test="(a:MatchMeter/a:DOB) = '0'">
                                    <div class="matchred" />
                                  </xsl:if>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </xsl:if>
                      </tr>
                    </table>
                  </td>
                  <td class="end" style="border: none;"></td>
                </tr>

                <xsl:for-each select="a:Charge">
                  <xsl:sort select="substring-before(a:DispositionDate,'T')" order="descending" />
                  <tr style="border:1px solid #999;">
                    <td class="begin" style="border: none;">
                    </td>
                    <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#fff;text-align:center;">
                        <tr>
                          <xsl:choose>
                            <xsl:when test="(position()mod 2)=1">
                              <td width="10%" style="border-right:solid 0px black;text-align:center;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                    <td style="text-align:right;">
                                      <div style="padding: 15px; color: rgb(102, 102, 102); float: left; width: 143px;">
                                        <div class="file-name" style="float:left; padding-top: 4px; color:#232323 !important;">
                                          Charge
                                        </div>
                                        <span class="blue-no text-pad" style="float:left; padding-top: 6px;">
                                          <xsl:value-of select="(position())" />
                                        </span>
                                      </div>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </xsl:when>
                            <xsl:otherwise>
                              <td width="10%" style="border-right:solid 0px black;background-color:white;text-align:center;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                    <td style="text-align:right;background-color:white;">
                                      <div style="padding: 15px; color: rgb(102, 102, 102); float: left; width: 143px;">
                                        <div class="file-name" style="float:left; padding-top: 4px; color:#232323 !important;">
                                          Charge
                                        </div>
                                        <span class="blue-no text-pad" style="float:left; padding-top: 6px;">
                                          <xsl:value-of select="(position())" />
                                        </span>
                                      </div>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </xsl:otherwise>
                          </xsl:choose>
                          <xsl:choose>
                            <xsl:when test="(position()mod 2)=1">
                              <td width="45%" style="">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                    <td style="text-align:right;border-bottom:0px solid black;width:25%;">
                                      Charge:
                                    </td>
                                    <td style="text-align:left;border-bottom:0px solid black;">
                                      <xsl:value-of select="a:ChargeOrComplaint"/>
                                    </td>
                                  </tr>
                                  <xsl:if test="(a:ChargeDate)">
                                    <tr>
                                      <td style="text-align:right;border-bottom:0px solid black;">
                                        Charge Date:
                                      </td>

                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:ChargeDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>
                                  <xsl:if test="(a:ArrestDate)">
                                    <tr>
                                      <td style="text-align:right;border-bottom:0px solid black;">
                                        Arrest Date:
                                      </td>

                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>

                                  <xsl:if test="(a:OffenseDate)">
                                    <tr>
                                      <td style="text-align:right;border-bottom:0px solid black;">
                                        Offense Date:>
                                      </td>

                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>

                                  <xsl:if test="(a:DispositionDate)">
                                    <tr>
                                      <td style="text-align:right;border-bottom:0px solid black;">

                                        Disposition Date:

                                      </td>

                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>
                                  <xsl:if test="(../a:AdditionalItems)">
                                    <xsl:if test="(../a:AdditionalItems/@type='TrialDate')">
                                      <tr>
                                        <td style="text-align:right;border-bottom:0px solid black;">

                                          Trial Date:

                                        </td>

                                        <td style="text-align:left;border-bottom:0px solid black;">
                                          <xsl:value-of select='substring(../a:AdditionalItems/a:Text,1,10)' />
                                        </td>
                                      </tr>
                                    </xsl:if>
                                  </xsl:if>
                                </table>
                              </td>
                              <td width="45%" style="">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <xsl:if test="(a:ChargeTypeClassification)!='unknown'">
                                    <tr>
                                      <td style="text-align:right;border-bottom:0px solid black;width:25%;">

                                        Severity:

                                      </td>

                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:value-of select="a:ChargeTypeClassification"/>
                                      </td>
                                    </tr>
                                  </xsl:if>
                                  <xsl:if test="(a:Disposition)">
                                    <tr>
                                      <td style="text-align:right;border-bottom:0px solid black;">

                                        Final Disposition:

                                      </td>

                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:choose>
                                          <xsl:when test="(a:Disposition)='unknown'">
                                            Manual County or State Report needed for Final Results
                                          </xsl:when>
                                          <xsl:otherwise>
                                            <xsl:choose>
                                              <xsl:when test="(contains(../a:CourtName, 'SAN BERNARDINO') or contains(../a:CourtName, 'RIVERSIDE')) and (a:Disposition)='NOT CONVICTED'">
                                                Unknown
                                              </xsl:when>
                                              <xsl:otherwise>
                                                <xsl:value-of select="a:Disposition"/>
                                              </xsl:otherwise>
                                            </xsl:choose>
                                          </xsl:otherwise>
                                        </xsl:choose>
                                      </td>
                                    </tr>
                                  </xsl:if>
                                  <xsl:if test="(a:Sentence)">
                                    <tr>
                                      <td style="text-align:right;border-bottom:0px solid black;">
                                        Sentence:
                                      </td>
                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:value-of select='(a:Sentence)' />
                                      </td>
                                    </tr>
                                  </xsl:if>
                                  <xsl:if test="(a:Comment)">
                                    <tr>
                                      <td style="text-align:right;border-bottom:0px solid black;">

                                        Comments:

                                      </td>

                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:for-each select="a:Comment">
                                          <span style="float:left;">
                                            <xsl:value-of select="."/>
                                            <xsl:element name="br" />
                                          </span>
                                        </xsl:for-each>
                                      </td>
                                    </tr>
                                  </xsl:if>
                                </table>
                              </td>
                            </xsl:when>
                            <xsl:otherwise>
                              <td width="45%">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                    <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                      Charge:
                                    </td>
                                    <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                      <xsl:value-of select="a:ChargeOrComplaint"/>
                                    </td>
                                  </tr>
                                  <xsl:if test="(a:ChargeDate)">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                        Charge Date:
                                      </td>

                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:ChargeDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>
                                  <xsl:if test="(a:ArrestDate)">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                        Arrest Date:
                                      </td>

                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>

                                  <xsl:if test="(a:OffenseDate)">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                        Offense Date:
                                      </td>

                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>

                                  <xsl:if test="(a:DispositionDate)">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">

                                        Disposition Date:

                                      </td>

                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>
                                  <xsl:if test="(../a:AdditionalItems)">
                                    <xsl:if test="(../a:AdditionalItems/@type='TrialDate')">
                                      <tr>
                                        <td style="text-align:right;background-color:white;border-bottom:0px solid black;">

                                          Trial Date:

                                        </td>

                                        <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                          <xsl:value-of select='substring(../a:AdditionalItems/a:Text,1,10)' />
                                        </td>
                                      </tr>
                                    </xsl:if>
                                  </xsl:if>
                                </table>
                              </td>
                              <td width="45%">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <xsl:if test="(a:ChargeTypeClassification)!='unknown'">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">

                                        Severity:

                                      </td>

                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:value-of select="a:ChargeTypeClassification"/>
                                      </td>
                                    </tr>
                                  </xsl:if>
                                  <xsl:if test="(a:Disposition)">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">

                                        Final Disposition:

                                      </td>

                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:choose>
                                          <xsl:when test="(a:Disposition)='unknown'">
                                            Manual County or State Report needed for Final Results
                                          </xsl:when>
                                          <xsl:otherwise>
                                            <xsl:choose>
                                              <xsl:when test="(contains(../a:CourtName, 'SAN BERNARDINO') or contains(../a:CourtName, 'RIVERSIDE')) and (a:Disposition)='NOT CONVICTED'">
                                                Unknown
                                              </xsl:when>
                                              <xsl:otherwise>
                                                <xsl:value-of select="a:Disposition"/>
                                              </xsl:otherwise>
                                            </xsl:choose>
                                          </xsl:otherwise>
                                        </xsl:choose>
                                      </td>
                                    </tr>
                                  </xsl:if>
                                  <xsl:if test="(a:Sentence)">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">

                                        Sentence:

                                      </td>
                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:value-of select='(a:Sentence)' />
                                      </td>
                                    </tr>
                                  </xsl:if>
                                  <xsl:if test="(a:Comment)">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">

                                        Comments:

                                      </td>

                                      <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                        <xsl:for-each select="a:Comment">
                                          <span style="float:left;">
                                            <xsl:value-of select="."/>
                                            <xsl:element name="br" />
                                          </span>
                                        </xsl:for-each>
                                      </td>
                                    </tr>
                                  </xsl:if>
                                </table>
                              </td>
                            </xsl:otherwise>
                          </xsl:choose>
                        </tr>
                      </table>
                    </td>
                    <td class="end" style="border: none;"></td>
                  </tr>
                  <tr height="10px">
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                </xsl:for-each>
                <xsl:if test="count(a:Charge)=0">
                  <tr style="border:1px solid #999;">
                    <td class="begin" style="border: none;">
                    </td>
                    <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#fff;text-align:center;">
                        <tr>
                          <td width="10%" style="border-right:solid 0px #ccc;text-align:center;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td style="text-align:right;">
                                  <span style="font-size:15px;font-weight:normal;color:#000;">
                                    Additional Info
                                  </span>
                                </td>
                              </tr>
                            </table>
                          </td>
                          <td width="80%" style="text-align:left;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td style="text-align:left;padding-left:5px;">
                                  NOTE: THE DATA REPORTED BY THE COURT FOR THIS CASE IS INCOMPLETE. MANUAL COUNTY SEARCH REQUIRED.<br/>
                                  <span style="padding-left:35px">IF THIS RECORD DOES NOT SPECIFY THE COUNTY, A MANUAL STATE SEARCH IS REQUIRED.</span>
                                </td>
                              </tr>


                              <!--<xsl:for-each select="a:AdditionalItems">
                                <tr>
                                  <td style="background-color:#dcdcdc;text-align:left;padding-left:5px;">
                                    <xsl:value-of select="a:Text"/>
                                    <xsl:element name="br" />
                                  </td>
                                </tr>
                              </xsl:for-each>-->
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                    <td class="end" style="border: none;"></td>
                  </tr>
                </xsl:if>
                <xsl:if test="a:ReviewNote!=''">
                  <tr>
                    <td class="begin" style="border: none;">
                    </td>
                    <td colspan="4" style="width:95%; background-color:#FFFFE1; border:none;">
                      <xsl:value-of select="a:ReviewNote"></xsl:value-of>
                    </td>
                    <td align="right" valign="bottom" style="vertical-align:bottom">

                    </td>
                    <td class="end" style="border: none;"></td>
                  </tr>
                </xsl:if>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>

          <!--<xsl:for-each select="a:BackgroundReports">
            <tr id="trRawNCR11">
              <td class="begin">
              </td>
              <td onclick="collapse('{concat('trRawNCR1_',@OrderDetailId)}');" colspan="5" style="border:solid 1px black;background-color:white;text-align:left;">
                <div style="float:left">
                  <div style="float:left;background-position:left;" class="plus_btnSmall">
                  </div>
                  <div style="font-size:17px;font-weight:bold;color:#000;width:300px; margin-top:2px">
                    Raw Courthouse Data
                  </div>
                </div>
              </td>
              <td class="end"></td>
            </tr>
            <tr id="trRawNCR12">
              <td class="begin">
              </td>
              <td colspan="5" style="border:solid 1px black;background-color:white;text-align:center;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr id="{concat('trRawNCR1_',@OrderDetailId)}_b" style="display:none;">
                    <td style="background-color:white;">
                      <div id="divRawDataNCR1">
                        <pre>
                          <xsl:value-of select ="$TextData"/>
                        </pre>
                      </div>
                    </td>
                  </tr>
                </table>
              </td>
              <td class="end"></td>
            </tr>
          </xsl:for-each>-->

        </tbody>
        <!--<tfoot>
          <td class="begin"></td>
          <td colspan="5"></td>
          <td class="end"></td>
        </tfoot>-->
      </table>
    </center>

    <div class="sectionsummary" style="1px solid #9B004F;margin-bottom: 10px;">
      <h3 style="line-height: 1em;float:left; width:100%;">
        <!--<xsl:choose>
          <xsl:when test="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)=0">
            <p>
              National Sex Offender : Clear<br/>
              OFAC : Clear<br/>
              OIG : Clear<br/>
              For a complete list of coverage, please visit<br/>
              <a href="http://emerge.intelifi.com/coverage.pdf" target="_blank">US Criminal Rapid Search Coverage Guide</a>
            </p>
          </xsl:when>
          <xsl:otherwise>-->
        Data Sources Searched:<br/>
        Court Records, Department of Corrections, Administrative Office of Courts,<br/>
        National Sex Offender Registry, OFAC, OIG<br/>
        For a complete list of coverage, please visit<br/>
        <a href="http://emerge.intelifi.com/coverage.pdf" target="_blank">US Criminal Rapid Search Coverage Guide</a>

        <!--</xsl:otherwise>
        </xsl:choose>-->

      </h3>
    </div>
  </xsl:template>
</xsl:stylesheet>
