<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:a="http://ns.hr-xml.org/2004-08-02">
  <xsl:param name="admin" select="defaultstring" />
  <xsl:output method="html"/>
  <xsl:template match="/">

    <xsl:if test="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:SSNStatus)&gt;'0'">

      <table cellpadding="2" cellspacing="0" style="margin: 0px; border:none;" align="center" class="PS_main">
        <tr>
          <td class="PS_BORDER" width="50%">
              <div class="txt-detials" style="padding-left: 32px;float:left;">
              <ul>
                <li style="color: #333333;"> SSN ISSUANCE :</li>
                <li style="color: #333333;"> DEATH INDEX :</li>
              </ul>
              <ul>
                <li  style="text-align:left;">
                  <strong>
                    <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:SSNStatus"/>
                  </strong>
                </li>
                <li style="text-align:left;">
                  <strong>
                    <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:DateOfDeath"/>
                  </strong>
                </li>
              </ul>
            </div>
              <!--<span class="PS_SSN_DEATH_TOP">
              SSN ISSUANCE DATABASE
            </span >
            <br/>
            <span class="PS_SSN_DEATH_BOTTOM">
              <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:SSNStatus"/>
            </span>
          </td>
          <td class="PS_BORDER">
            <span class="PS_SSN_DEATH_TOP">
              DEATH INDEX
            </span>
            <br/>
            <span class="PS_SSN_DEATH_BOTTOM">
              <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:DateOfDeath"/>
            </span>-->
          </td>
        </tr>
      </table>

    </xsl:if>

    <br/>
    <br/>
    <xsl:if test="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile) ='0'">
      <table cellpadding="2" cellspacing="0" style="margin: 0px; border:none;" align="center" class="PS_main">
        <thead>
          <tr>
            <th ></th>
            <th colspan="5">Search Information</th>
            <th ></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td ></td>
            <td colspan="5">
              <xsl:if test="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:AdditionalItems/a:Text) &gt;'0'">
                <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:AdditionalItems/a:Text" />
                <!--Address trace service error: Input Error:Too Many Hits.Please refine your search.-->
              </xsl:if>
              <xsl:if test="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:AdditionalItems/a:Text) ='0'">
                No Records Found.
              </xsl:if>
            </td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
        </tbody>
        <tfoot>
          <td ></td>
          <td colspan="5"></td>
          <td ></td>
        </tfoot>
      </table>
    </xsl:if>
    <xsl:if test="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile) &gt;'0'">

      <table cellpadding="2" cellspacing="0" style="margin: 0px; border:none;" align="center" class="PS_main">
        <tr>
          
          <td class="PS_BORDER GetNameClass" valign="top">
            <xsl:variable name="unique-listName" select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PersonName/a:FormattedName[not(.=following::a:FormattedName)]" />
            <span class="countr" style="background-color: rgb(0,120,189);color: white; border:none;margin-left: 9px;">
              <span class="nameTotalCount">
                <xsl:value-of select="count($unique-listName)"/>
              </span>
            </span>
            <span class="PS_SSN_DEATH_TOP" style="text-decoration: none;padding-top: 3px;display: inline-block;margin-bottom: 2px;">
              REPORTED NAMES 
            </span >
            <br/>
            <!--<xsl:variable name="unique-listName" select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PersonName/a:FormattedName[not(.=following::a:FormattedName)]" />-->
            <xsl:for-each select="$unique-listName">
              <span class="up_unique"><span class="unique"><xsl:value-of select="." /></span>
              <br/>
              </span>
            </xsl:for-each>

            <!--<xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PersonName" >
            <span class="PS_SSN_DEATH_BOTTOM">
              <xsl:if test="a:FamilyName!='' and a:GivenName !=''">
              <xsl:value-of select="a:FamilyName" />, <xsl:value-of select="a:GivenName" /><br/>
              </xsl:if>
            </span>
          </xsl:for-each>-->
          </td>
          <td class="PS_BORDER" valign="top">
            <xsl:variable name="unique-listDates" select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:DemographicDetail/a:DateOfBirth[not(.=following::a:DateOfBirth or string-length(.) != '10' )]" />
            <span class="countr" style="background-color: rgb(0,120,189);color: white; border:none;">
              <span class="nameTotalCount">
                <xsl:value-of select="count($unique-listDates)"/>
              </span>
            </span>
              <span class="PS_SSN_DEATH_TOP" style=" text-decoration: none;padding-top: 4px;">
              REPORTED BIRTHDATES 
            </span>
           
            <!--<xsl:variable name="unique-listDates" select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:DemographicDetail/a:DateOfBirth[not(.=following::a:DateOfBirth)]" />-->
            <xsl:for-each select="$unique-listDates">
              <span class="ultimatecls" style=" margin-left: 0px;">
                <span class="ultimateclshelp"><xsl:value-of select="concat(substring(.,6,2),'/',substring(.,9,2),'/',substring(.,1,4))"  />
                </span>
              </span>
             
            </xsl:for-each>


            <!--<xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:DemographicDetail" >
            <span class="PS_SSN_DEATH_BOTTOM">
              <xsl:value-of select="a:DateOfBirth"/>
              <br/>
            </span>
          </xsl:for-each>-->
          </td>
        <td class="PS_BORDER" width="33%" valign="top">
            <xsl:variable name="unique-ListCounty" select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PostalAddress/a:County[not(.=following::a:County)]" />
          <span class="countr" style="background-color: rgb(0,120,189);color: white; border:none;">
            <span class="nameTotalCount">
              <xsl:value-of select="count($unique-ListCounty)"/>
            </span>
          </span>
              <span class="PS_SSN_DEATH_TOP" style="text-decoration: none;padding-left: 45px;padding-top: 2px;display: inline-block;">
              REPORTED COUNTIES 
            </span >
            <xsl:if test="count($unique-ListCounty) &gt;'0'">
              <a  class="runallcounties" style="color:red;margin-left:1%;cursor:pointer; font-weight:bold;" >Run All Counties</a>
              &#160;<img src="../../Content/themes/base/images/ajax-loader3.gif" class="psdisplay"></img>
            </xsl:if>
            <br/>
            <!--<xsl:variable name="unique-list" select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PostalAddress/a:County[not(.=following::a:County)]" />
            <xsl:for-each select="$unique-list">
              <xsl:value-of select="." />
              <br/>
            </xsl:for-each>-->
            <xsl:variable name="unique-list" select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PostalAddress" />
            <xsl:for-each select="$unique-list">
              <xsl:if test="a:County[not(.=following::a:County)]">
                <span class="countystate"> <xsl:value-of select="a:County"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="a:Region"/>
                </span>
                <br/>
              </xsl:if>
            </xsl:for-each>

            <!--<xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PostalAddress[not(.=preceding::a:PostalAddress)]" >
            <span class="PS_SSN_DEATH_BOTTOM">
              <xsl:value-of select="a:County"/>
              <br/>
            </span>
          </xsl:for-each>-->
          </td>
        </tr>
      </table>

      <br/>
      <br/>


      <div class="sectionsummary" style="padding-left: 32px;">
        <h3>
          <!--(<xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/@recordCount" />)-->
          (<xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData)"/>) Results PEOPLE SEARCH
        </h3>
        <br />
        <h3>Name:</h3>
        <h4>
          <span class="capname"><xsl:value-of select="concat(a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:GivenName,', ',a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:FamilyName)" />
          </span>
            </h4>
        <br/>
      </div>

      <center>

        <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/@recordCount = '0'">
          <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0" style="border:none;">
            <thead>
              <tr>
                <th ></th>
                <th colspan="5">Search Information</th>
                <th ></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td >i</td>
                <td colspan="5">
                  No Records Found.
                </td>
                <td >
                  <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                </td>
              </tr>
              <tr>
                <td >
                </td>
                <td colspan="5" >
                  <div id="txtReviewNoteNRF_PS" name="txtNote_{(position())}" class="ReviewNote" style="width:100%;background-color:#FFFFE1;">
                  </div>
                </td>
                <td  >
                  <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                </td>
              </tr>
            </tbody>
            <tfoot>
              <td ></td>
              <td colspan="5"></td>
              <td ></td>
            </tfoot>
          </table>
        </xsl:if>

        <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/@recordCount != '0'">
          <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
            <thead style="    background: #DCDCDC !important;">
              <tr>
                <th style="padding: 5px 0px !important;"></th>
                <th align="left" style="padding: 5px 0px !important;">
                  PERSONAL INFO
                </th>
                <th align="left" style="padding: 5px 0px !important;">
                  ADDRESS INFO
                </th>
                <th align="left" style="padding: 5px 0px !important;">
                  PHONE INFO
                </th>
                <th  style="padding: 5px 0px !important;"></th>
              </tr>
            </thead>
            <tbody>
              <xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData">
                <xsl:choose>
                  <xsl:when test ="@Deleted">

                    <xsl:if test ="@Deleted!='1'">
                      <xsl:choose>
                        <xsl:when test="(position() mod 2 = 0)">
                          <tr>
                            <td>
                              <span class="blue-no" style="padding-top:6px;">
                                <xsl:value-of select="(position() )" />
                              </span>
                            </td>
                            <td style="background-color:#fff;">
                              <xsl:value-of select="a:PersonName/a:FamilyName" />, <xsl:value-of select="a:PersonName/a:GivenName" /><br/>
                              SSN : xxx-xx-<xsl:value-of select="substring(a:DemographicDetail/a:GovernmentId,6,4)" />

                              <br/>
                              <span class="ultimatecls">
                                <xsl:if test="a:DemographicDetail/a:DateOfBirth[not(string-length(.) != '10' )]">
                                  DOB : <span class="ultimateclshelp">
                                    <xsl:value-of select="concat(substring(a:DemographicDetail/a:DateOfBirth,6,2),'/',substring(a:DemographicDetail/a:DateOfBirth,9,2),'/',substring(a:DemographicDetail/a:DateOfBirth,1,4))" />
                                  </span>
                                  <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
                                  Age : <xsl:value-of select="a:DemographicDetail/a:Age" />
                                </xsl:if>
                                  </span>
                            </td>
                            <td style="background-color:#fff;">
                              <xsl:value-of select="a:PostalAddress/a:DeliveryAddress/a:AddressLine" />
                              <xsl:text> </xsl:text>
                              <br />

                              <xsl:value-of select="a:PostalAddress/a:Municipality" />
                              <xsl:text>, </xsl:text>
                             <xsl:value-of select="a:PostalAddress/a:Region" />
                              <xsl:text> </xsl:text>
                              <br />
                              <xsl:value-of select="a:PostalAddress/a:County" />
                              <xsl:text> </xsl:text>
                              <br />

                              <xsl:value-of select="a:PostalAddress/a:CountryCode" />
                              <xsl:text> </xsl:text>
                              <xsl:value-of select="a:PostalAddress/a:PostalCode" />
                              <br/>
                              <xsl:value-of select="a:EffectiveDate/a:StartDate" />
                              <xsl:text disable-output-escaping="yes">&amp;nbsp;-&amp;nbsp;</xsl:text>
                              <xsl:value-of select="a:EffectiveDate/a:EndDate" />
                            </td>
                            <td style="border-right: none;background-color:#fff;">
                              <xsl:choose>
                                <xsl:when test="count(a:ContactMethod)='0'">
                                  
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:for-each select="a:ContactMethod">
                                    <xsl:value-of select="a:Telephone/a:FormattedNumber"/>
                                    <br/>
                                  </xsl:for-each>
                                </xsl:otherwise>
                              </xsl:choose>
                            </td>
                            <td  style="background-color:#fff;" >
                              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                            </td>
                            <!--</tr>-->
                          </tr>
                          <xsl:if test="a:ReviewNote!=''">
                            <tr>
                              <td >
                              </td>
                              <td colspan="3" style="background-color:#FFFFE1" class="ReviewNote">
                                <xsl:value-of select="a:ReviewNote"></xsl:value-of>
                              </td>
                              <td style="background-color:#fff;" >
                                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                              </td>
                            </tr>
                          </xsl:if>
                        </xsl:when>
                        <xsl:otherwise>
                          <tr>
                            <td >
                              <span class="blue-no" style="padding-top:6px;">
                                <xsl:value-of select="(position() )" />
                              </span>
                            </td>
                            <td >
                              <xsl:value-of select="a:PersonName/a:FamilyName" />, <xsl:value-of select="a:PersonName/a:GivenName" /><br/>
                              SSN : xxx-xx-<xsl:value-of select="substring(a:DemographicDetail/a:GovernmentId,6,4)" />
                              <br/>
                              <xsl:if test="a:DemographicDetail/a:DateOfBirth[not(string-length(.) != '10' )]">
                                DOB : <xsl:value-of select="a:DemographicDetail/a:DateOfBirth" />
                                <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
                                Age : <xsl:value-of select="a:DemographicDetail/a:Age" />
                              </xsl:if>
                                </td>
                            <td>
                              <xsl:value-of select="a:PostalAddress/a:DeliveryAddress/a:AddressLine" />
                              <xsl:text> </xsl:text>
                              <br />

                              <xsl:value-of select="a:PostalAddress/a:Municipality" />
                              <xsl:text>, </xsl:text>
                              <xsl:value-of select="a:PostalAddress/a:Region" />
                              <xsl:text> </xsl:text>
                              <br />
                              <xsl:value-of select="a:PostalAddress/a:County" />
                              <xsl:text> </xsl:text>
                              <br />

                              <xsl:value-of select="a:PostalAddress/a:CountryCode" />
                              <xsl:text> </xsl:text>
                              <xsl:value-of select="a:PostalAddress/a:PostalCode" />
                              <br/>
                              <xsl:value-of select="a:EffectiveDate/a:StartDate" />
                              <xsl:text disable-output-escaping="yes">&amp;nbsp;-&amp;nbsp;</xsl:text>
                              <xsl:value-of select="a:EffectiveDate/a:EndDate" />
                            </td>
                            <td style="border-right: none;">
                              <xsl:choose>
                                <xsl:when test="count(a:ContactMethod)='0'">
                                 
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:for-each select="a:ContactMethod">
                                    <xsl:value-of select="a:Telephone/a:FormattedNumber"/>
                                    <br/>
                                  </xsl:for-each>
                                </xsl:otherwise>
                              </xsl:choose>
                            </td>
                            <td >
                              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                            </td>
                          </tr>
                          <xsl:if test="a:ReviewNote!=''">
                            <tr>
                              <td >
                              </td>
                              <td colspan="3" style="background-color:#FFFFE1"  class="ReviewNote">
                                <xsl:value-of select="a:ReviewNote"></xsl:value-of>
                              </td>
                              <td >
                                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                              </td>
                            </tr>
                          </xsl:if>
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:if>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:choose>
                      <xsl:when test="(position() mod 2 = 0)">
                        <tr>
                          <td >
                            <span class="blue-no" style="padding-top:6px;">
                              <xsl:value-of select="(position() )" />
                            </span>
                          </td>
                          <td style="background-color:#fff; color:#666;">
                            <xsl:value-of select="a:PersonName/a:FamilyName" />, <xsl:value-of select="a:PersonName/a:GivenName" /><br/>
                            SSN : xxx-xx-<xsl:value-of select="substring(a:DemographicDetail/a:GovernmentId,6,4)" />

                            <br/>
                            <span class="ultimatecls">
                              <xsl:if test="a:DemographicDetail/a:DateOfBirth[not(string-length(.) != '10' )]">
                                DOB : <span class="ultimateclshelp">
                                  <xsl:value-of select="concat(substring(a:DemographicDetail/a:DateOfBirth,6,2),'/',substring(a:DemographicDetail/a:DateOfBirth,9,2),'/',substring(a:DemographicDetail/a:DateOfBirth,1,4))" />
                                </span>
                                <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
                                Age : <xsl:value-of select="a:DemographicDetail/a:Age" />
                              </xsl:if> </span></td>
                          <td style="background-color:#fff; color:#666;">
                            <xsl:value-of select="a:PostalAddress/a:DeliveryAddress/a:AddressLine" />
                            <xsl:text> </xsl:text>
                            <br />

                            <xsl:value-of select="a:PostalAddress/a:Municipality" />
                            <xsl:text>, </xsl:text>
                            <xsl:value-of select="a:PostalAddress/a:Region" />
                            <xsl:text> </xsl:text>
                            <br />
                            <xsl:value-of select="a:PostalAddress/a:County" />
                            <xsl:text> </xsl:text>
                            <br />

                            <xsl:value-of select="a:PostalAddress/a:CountryCode" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="a:PostalAddress/a:PostalCode" />
                            <br/>
                            <xsl:value-of select="a:EffectiveDate/a:StartDate" />
                            <xsl:text disable-output-escaping="yes">&amp;nbsp;-&amp;nbsp;</xsl:text>
                            <xsl:value-of select="a:EffectiveDate/a:EndDate" />
                          </td>
                          <td style="border-right: none;background-color:#fff; color:#666;">
                            <xsl:choose>
                              <xsl:when test="count(a:ContactMethod)='0'">
                              
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:for-each select="a:ContactMethod">
                                  <xsl:value-of select="a:Telephone/a:FormattedNumber"/>
                                  <br/>
                                </xsl:for-each>
                              </xsl:otherwise>
                            </xsl:choose>
                          </td>
                          <td  style="background-color:#fff; color:#666;" >
                            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                          </td>
                          <!--</tr>-->
                        </tr>
                        <xsl:if test="a:ReviewNote!=''">
                          <tr>
                            <td >
                            </td>
                            <td colspan="3" style="background-color:#FFFFE1" class="ReviewNote">
                              <xsl:value-of select="a:ReviewNote"></xsl:value-of>
                            </td>
                            <td style="background-color:#fff;" >
                              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                            </td>
                          </tr>
                        </xsl:if>
                      </xsl:when>
                      <xsl:otherwise>
                        <tr>
                          <td >
                            <span class="blue-no" style="padding-top:6px;">
                              <xsl:value-of select="(position() )" />
                            </span>
                          </td>
                          <td style=" color:#666;">
                            <xsl:value-of select="a:PersonName/a:FamilyName" />, <xsl:value-of select="a:PersonName/a:GivenName" /><br/>
                            SSN : xxx-xx-<xsl:value-of select="substring(a:DemographicDetail/a:GovernmentId,6,4)" />
                            <br/>
                            DOB : <xsl:value-of select="a:DemographicDetail/a:DateOfBirth" />
                            <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
                            Age : <xsl:value-of select="a:DemographicDetail/a:Age" />
                          </td>
                          <td  style=" color:#666;">
                            <xsl:value-of select="a:PostalAddress/a:DeliveryAddress/a:AddressLine" />
                            <xsl:text> </xsl:text>
                            <br />

                            <xsl:value-of select="a:PostalAddress/a:Municipality" />
                            <xsl:text>, </xsl:text>
                            <xsl:value-of select="a:PostalAddress/a:Region" />
                            <xsl:text> </xsl:text>
                            <br />
                            <xsl:value-of select="a:PostalAddress/a:County" />
                            <xsl:text> </xsl:text>
                            <br />

                            <xsl:value-of select="a:PostalAddress/a:CountryCode" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="a:PostalAddress/a:PostalCode" />
                            <br/>
                            <xsl:value-of select="a:EffectiveDate/a:StartDate" />
                            <xsl:text disable-output-escaping="yes">&amp;nbsp;-&amp;nbsp;</xsl:text>
                            <xsl:value-of select="a:EffectiveDate/a:EndDate" />
                          </td>
                          <td style="border-right: none; color:#666;">
                            <xsl:choose>
                              <xsl:when test="count(a:ContactMethod)='0'">
                                
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:for-each select="a:ContactMethod">
                                  <xsl:value-of select="a:Telephone/a:FormattedNumber"/>
                                  <br/>
                                </xsl:for-each>
                              </xsl:otherwise>
                            </xsl:choose>
                          </td>
                          <td  style=" color:#666;">
                            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                          </td>
                        </tr>
                        <xsl:if test="a:ReviewNote!=''">
                          <tr>
                            <td >
                            </td>
                            <td colspan="3" style="background-color:#FFFFE1" class="ReviewNote">
                              <xsl:value-of select="a:ReviewNote"></xsl:value-of>
                            </td>
                            <td >
                              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                            </td>
                          </tr>
                        </xsl:if>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </tbody>
            <tfoot>
              <td ></td>
              <td></td>
              <td></td>
              <td style="border-right: none;"></td>
              <td ></td>
            </tfoot>
          </table>
        </xsl:if>

      </center>

    </xsl:if>
  </xsl:template>
</xsl:stylesheet>