<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">
  <xsl:output method="html" />
  <xsl:template match="/FastraxNetwork">
    <xsl:variable name="TotalRecords">
      <xsl:value-of select="count(report-requested/report/report-detail-header/report-incident)" />
    </xsl:variable>
    <xsl:variable name="DeletedRecords">
      <xsl:value-of select="count(report-requested/report/report-detail-header/report-incident[@Deleted=1])" />
    </xsl:variable>
    <xsl:choose>
      <xsl:when test ="$TotalRecords=$DeletedRecords">
        <table width="98%" cellpadding="0" cellspacing="0">
          <tr>
            <td class="middle_left" width="14px">
            </td>
            <td width="97%">
              <div class="reportName">
                <h3>
                  Statewide Criminal Report
                </h3>
              </div>
              <div class="resultsHeader">
                (0) Results
              </div>
              <br/>
                <center>
                  <div style="color: red; font-weight: bold; font-size: 12px;">
                    No Record Found
                  </div>
                </center>                            
            </td>
            <td class="middle_right" width="9px">
            </td>
          </tr>
        </table>
      </xsl:when>
      <xsl:otherwise>
        <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
          <thead>
            <tr>
              <th colspan="100%">Criminal Arrests and Convictions</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>State:</td>
              <td>
                <xsl:value-of select="report-requested/report/report-detail-header/state" />
              </td>
              <td>Years Checked:</td>
              <td>
                <xsl:value-of select="report-requested/report/report-detail-header/time-checked" />
              </td>
              <td> </td>
              <td> </td>
            </tr>

            <tr>
              <td>County:</td>
              <td>
                <xsl:value-of select="report-requested/report/report-detail-header/county" />
              </td>
              <td>Convictions Admitted:</td>
              <td>
                <xsl:value-of select="report-requested/report/report-detail-header/felonies-admitted" />
              </td>
              <td> </td>
              <td> </td>
            </tr>

            <tr>
              <td>City:</td>
              <td></td>
              <td>Details Disclosed:</td>
              <td></td>
              <td> </td>
              <td> </td>
            </tr>

            <tr>
              <td colspan="100%"> </td>
            </tr>

            <tr>
              <td>Record Found:</td>
              <td>
                <xsl:if test="report-requested/report/report-detail-header/clear-record = '0'">Yes</xsl:if>
                <xsl:if test="report-requested/report/report-detail-header/clear-record = '1'">No</xsl:if>
              </td>

              <td>Outstanding Capias:</td>
              <td>
                <xsl:if test="report-requested/report/report-detail-header/capias = '0'">No</xsl:if>
                <xsl:if test="report-requested/report/report-detail-header/capias = '1'">Yes</xsl:if>
              </td>

              <td>Verified By DOB:</td>
              <td>
                <xsl:if test="report-requested/report/report-detail-header/verified-by-dob = '0'">No</xsl:if>
                <xsl:if test="report-requested/report/report-detail-header/verified-by-dob = '1'">Yes</xsl:if>
              </td>
            </tr>

            <tr>
              <td>Misdemeanors Found:</td>
              <td>
                <xsl:if test="report-requested/report/report-detail-header/misdemeanors = '0'">No</xsl:if>
                <xsl:if test="report-requested/report/report-detail-header/misdemeanors = '1'">Yes</xsl:if>
              </td>

              <td>Current Warrant:</td>
              <td>
                <xsl:if test="report-requested/report/report-detail-header/current-warrants = ''">No</xsl:if>
                <xsl:if test="report-requested/report/report-detail-header/current-warrants != ''">
                  <xsl:value-of select="report-requested/report/report-detail-header/current-warrants" />
                </xsl:if>
              </td>

              <td>Verified By Name:</td>
              <td>
                <xsl:if test="report-requested/report/report-detail-header/verified-by-name = '0'">No</xsl:if>
                <xsl:if test="report-requested/report/report-detail-header/verified-by-name = '1'">Yes</xsl:if>
              </td>
            </tr>

            <tr>
              <td>Felonies Found:</td>
              <td>
                <xsl:if test="report-requested/report/report-detail-header/felonies = '0'">No</xsl:if>
                <xsl:if test="report-requested/report/report-detail-header/felonies = '1'">Yes</xsl:if>
              </td>

              <td>DUI's Found:</td>
              <td>
                <xsl:if test="report-requested/report/report-detail-header/dui = '0'">No</xsl:if>
                <xsl:if test="report-requested/report/report-detail-header/dui = '1'">Yes</xsl:if>
              </td>

              <td>Verified By SSN:</td>
              <td>
                <xsl:if test="report-requested/report/report-detail-header/verified-by-ssn = '0'">No</xsl:if>
                <xsl:if test="report-requested/report/report-detail-header/verified-by-ssn = '1'">Yes</xsl:if>
              </td>
            </tr>

            <tr>
              <td>Aliases Found:</td>
              <td>
                <xsl:if test="report-requested/report/report-detail-header/alias-found = '0'">No</xsl:if>
                <xsl:if test="report-requested/report/report-detail-header/alias-found = '1'">Yes</xsl:if>
              </td>

              <td>Aliasas:</td>
              <td>

              </td>

              <td></td>
              <td></td>
            </tr>

            <tr>
              <td>Other Criminal:</td>
              <td>

              </td>

              <td>Other Criminal Detail:</td>
              <td>

              </td>

              <td></td>
              <td></td>
            </tr>

            <tr>
              <td>Verified By Other:</td>
              <td>

              </td>

              <td>Verified By Other Detail:</td>
              <td>

              </td>

              <td></td>
              <td></td>
            </tr>
          </tbody>
        </table>

        <xsl:for-each select="report-requested/report/report-detail-header/report-incident">
          <xsl:choose>
            <xsl:when test ="@Deleted">

              <xsl:if test ="@Deleted!='1'">
                <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
                  <thead>
                    <tr>
                      <th colspan="100%">Details</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Charges</td>
                      <td colspan="3">
                        <xsl:value-of select="charges" />
                      </td>
                    </tr>
                    <tr>
                      <td>Disposition Date</td>
                      <td>
                        <xsl:value-of select="disposition-date" />
                      </td>
                      <td>Disposition</td>
                      <td>
                        <xsl:value-of select="disposition" />
                      </td>
                    </tr>
                    <tr>
                      <td>File Date</td>
                      <td>
                        <xsl:value-of select="file-date" />
                      </td>
                      <td>Case #</td>
                      <td>
                        <xsl:value-of select="case-number" />
                      </td>
                    </tr>
                    <tr>
                      <td>Sentence Date</td>
                      <td>
                        <xsl:value-of select="sentence-date" />
                      </td>
                      <td>Sentence</td>
                      <td>
                        <xsl:value-of select="sentence" />
                      </td>
                    </tr>
                    <tr>
                      <td>Comments</td>
                      <td colspan="3">
                        <xsl:value-of select="additional-comments" />
                      </td>
                    </tr>

                    <tr>
                      <td colspan="4" style="width:100%;background-color:#FFFFE1" class="ReviewNote">
                        <xsl:value-of select="ReviewNote"></xsl:value-of>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </xsl:if>
            </xsl:when>
            <xsl:otherwise>
              <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
                <thead>
                  <tr>
                    <th colspan="100%">Details</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Charges</td>
                    <td colspan="3">
                      <xsl:value-of select="charges" />
                    </td>
                  </tr>
                  <tr>
                    <td>Disposition Date</td>
                    <td>
                      <xsl:value-of select="disposition-date" />
                    </td>
                    <td>Disposition</td>
                    <td>
                      <xsl:value-of select="disposition" />
                    </td>
                  </tr>
                  <tr>
                    <td>File Date</td>
                    <td>
                      <xsl:value-of select="file-date" />
                    </td>
                    <td>Case #</td>
                    <td>
                      <xsl:value-of select="case-number" />
                    </td>
                  </tr>
                  <tr>
                    <td>Sentence Date</td>
                    <td>
                      <xsl:value-of select="sentence-date" />
                    </td>
                    <td>Sentence</td>
                    <td>
                      <xsl:value-of select="sentence" />
                    </td>
                  </tr>
                  <tr>
                    <td>Comments</td>
                    <td colspan="3">
                      <xsl:value-of select="additional-comments" />
                    </td>
                  </tr>
                  <xsl:if test="ReviewNote!=''">
                    <tr>
                      <td colspan="4" style="width:95%;background-color:#FFFFE1" class="ReviewNote">
                        <xsl:value-of select="ReviewNote"></xsl:value-of>
                      </td>
                    </tr>
                  </xsl:if>
                </tbody>
              </table>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>

      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>
</xsl:stylesheet>
