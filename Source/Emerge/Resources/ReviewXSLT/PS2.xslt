<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" encoding="iso-8859-1"/>
  <xsl:param name="admin" select="defaultstring" />
  <xsl:template match="/">
    <div class="people-search-wrap">
    <div class="sectionsummary" style="text-align:center">
      <h3>
        (<xsl:value-of select="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/rows_returned" />) Results People Search
      </h3>
    </div>
    <xsl:if test="XML_INTERFACE/CREDITREPORT/OBJECTS/BUREAUERROR = 'True' or XML_INTERFACE/CREDITREPORT/cp_xmldirect/general_error !='' ">
      <table id="peopleTable" class="people-search" border="0" cellpadding="0"  style="width:100%;" cellspacing="0">
        <thead>
          <tr class="search-heading">
            <td class="width_10"></td>
            <td style="text-align:left;font-size:20px;font-weight: bold;">
              Search Information
            </td>
            <td ></td>
            </tr>
        </thead>
        <tbody>
          <tr>
            <td></td>
            <td>
				<span class="red-no" style="padding-top:5px;">
              <xsl:value-of select="(position() )"/>
				</span>
            </td>
            <td >

              <xsl:value-of select="XML_INTERFACE/CREDITREPORT/HTMLREPORT/MESSAGE" disable-output-escaping="yes" />

            </td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
        </tbody>
        
      </table>
    </xsl:if>
    <center>
      <xsl:if test="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/rows_returned = '0'">
        <div class="noRecords">
          No Records Found
        </div>
      </xsl:if>
      <table style="width:100%" border="0" cellpadding="0" cellspacing="0" class="people-search" >
        <tr>
          <td class="width_10"></td>
          <td >
            <div id="txtReviewNoteNRF_PS2" name="txtNote_{(position())}" style="width:99%;">
            </div>
          </td>
          <td  >
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </td>
        </tr>
      </table>
      <xsl:if test="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/rows_returned != '0'">


        <table id="peopleTable" class="people-search" border="0" cellpadding="0" cellspacing="0">
          <thead>
            <tr>
				<th style="width:5%;" ></th>
				<th align="left" style="width:20%">Name</th>
				<th align="left" style="width:40%">Address</th>
				<th align="left" style="width:10%" >Reporting Date</th>
				<th align="left" style="width:10%" >Date of Birth</th>
				<th align="left" >Indicators</th>
              <th ></th>
            </tr>
          </thead>

          <tbody>
            <xsl:for-each select="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/response_data/locator/response_row">
              <xsl:choose>
                <xsl:when test ="@Deleted">
                  <xsl:if test ="@Deleted!='1'">
                    <xsl:choose>
                      <xsl:when test="( position() mod 2 = 0 )">
                        <tr>
                          <td class="width_10"></td>
                          
                         
                          <td>
							  <span class="blue-no" style="padding-top:5px;">
                            <xsl:value-of select="(position() )" /> :
							  </span>
                            <xsl:value-of select="person/lastname" />, 
                            <xsl:value-of select="person/firstname" /> <br/>
                            <xsl:variable name='ssn' 	select="person/ssn" />
                            <xsl:variable name='one' 	select='substring( $ssn, 0 , 4 )' />
                            <xsl:variable name='two'	select='substring( $ssn, 4 , 2 )' />
                            <xsl:variable name='three'  select='substring( $ssn, 6 , 4 )' />

                            <xsl:if test="$admin = '1'">
                              Social #: <xsl:value-of select="person/ssn"/><br />
                            </xsl:if>
                            <xsl:if test="$admin = '0'">
                              <xsl:value-of select="concat('Social #: ', 'xxx', '-', 'xx', '-', $three )"/>
                              <br />
                            </xsl:if>


                            <xsl:if test="address/phone_number != ''">
                              <xsl:value-of select="concat('Phone #: ', address/phone_number)" />
                              <br/>
                            </xsl:if>
                            <br />
                            <xsl:if test="person/aka_list/aka">
                              AKAs: <br/>
                            </xsl:if>
                            <xsl:for-each select="person/aka_list">
                              <xsl:value-of select="aka" />
                              <br/>
                            </xsl:for-each>
                          </td>
                          <td>
                            <xsl:value-of select="address/pre_direction" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="address/house_numer" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="address/street_name" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="address/street_suffix" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="address/post_direction" />
                            
                            <xsl:value-of select="address/city" />
                            <xsl:text>, </xsl:text>
                            <xsl:value-of select="address/state" />
                           
                            <xsl:value-of select="address/zipcode" />
                            <xsl:if test="address/zip4 != ''">
                              -<xsl:value-of select="address/zip4" />
                            </xsl:if>
                          </td>
                          <td>
                            <xsl:variable name='year'  select='substring( address/report_date, 0 , 5 )' />
                            <xsl:variable name='month' select='substring( address/report_date, 5 , 2 )' />
                            <xsl:variable name='day'   select='substring( address/report_date, 7 , 2 )' />
                            <xsl:variable name='reportdate'   select="concat($month, '/', $day, '/', $year )"  />
                            <xsl:if test="$reportdate = '//'">

                            </xsl:if>
                            <xsl:if test="$reportdate != '//'">
                              <xsl:value-of select="$reportdate"/>
                            </xsl:if>
                          </td>
                          <td>
                            <xsl:variable name='year'  select='substring( person/dob_list/dob, 0 , 5 )' />
                            <xsl:variable name='month' select='substring( person/dob_list/dob, 5 , 2 )' />
                            <xsl:variable name='day'   select='substring( person/dob_list/dob, 7 , 2 )' />
                            <xsl:variable name='dob'   select="concat($month, '/', $day, '/', $year )"  />
                            <xsl:if test="$dob = '//'">
                              None Specified
                            </xsl:if>
                            <xsl:if test="$dob != '//'">
                              <xsl:value-of select="$dob"/>
                            </xsl:if>
                          </td>
                          <td>
                            <xsl:if test="person/is_deceased = 'YES'">
                              Deceased
                            </xsl:if>
                          </td>
                          <td >
                            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                          </td>
                        </tr>
                      </xsl:when>
                      <xsl:otherwise>
                        <tr>
                          <td class="width_10">
                           
                          </td>
                          <td style="background-color:#fff;">
							  <span class="blue-no" style="padding-top:5px;">
                            <xsl:value-of select="(position() )" />
							  </span>
                            <xsl:value-of select="person/lastname" />, <xsl:value-of select="person/firstname" /> <br/>
                            <xsl:variable name='ssn' 	select="person/ssn" />
                            <xsl:variable name='one' 	select='substring( $ssn, 0 , 4 )' />
                            <xsl:variable name='two'	select='substring( $ssn, 4 , 2 )' />
                            <xsl:variable name='three'  select='substring( $ssn, 6 , 4 )' />

                            <xsl:if test="$admin = '1'">
                              Social #: <xsl:value-of select="person/ssn"/><br />
                            </xsl:if>
                            <xsl:if test="$admin = '0'">
                              <xsl:value-of select="concat('Social #: ', 'xxx', '-', 'xx', '-', $three )"/>
                              <br />
                            </xsl:if>


                            <xsl:if test="address/phone_number != ''">
                              <xsl:value-of select="concat('Phone #: ', address/phone_number)" />
                              <br/>
                            </xsl:if>
                            <br />
                            <xsl:if test="person/aka_list/aka">
                              AKAs: <br/>
                            </xsl:if>
                            <xsl:for-each select="person/aka_list">
                              <xsl:value-of select="aka" />
                              <br/>
                            </xsl:for-each>
                          </td>
                          <td style="background-color:#fff;">
                            <xsl:value-of select="address/pre_direction" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="address/house_numer" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="address/street_name" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="address/street_suffix" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="address/post_direction" />
                            
                            <xsl:value-of select="address/city" />
                            <xsl:text>, </xsl:text>
                            <xsl:value-of select="address/state" />
                            
                            <xsl:value-of select="address/zipcode" />
                            <xsl:if test="address/zip4 != ''">
                              -<xsl:value-of select="address/zip4" />
                            </xsl:if>
                          </td>
                          <td style="background-color:#fff;">
                            <xsl:variable name='year'  select='substring( address/report_date, 0 , 5 )' />
                            <xsl:variable name='month' select='substring( address/report_date, 5 , 2 )' />
                            <xsl:variable name='day'   select='substring( address/report_date, 7 , 2 )' />
                            <xsl:variable name='reportdate'   select="concat($month, '/', $day, '/', $year )"  />
                            <xsl:if test="$reportdate = '//'">

                            </xsl:if>
                            <xsl:if test="$reportdate != '//'">
                              <xsl:value-of select="$reportdate"/>
                            </xsl:if>
                          </td>
                          <td style="background-color:#fff;">
                            <xsl:variable name='year'  select='substring( person/dob_list/dob, 0 , 5 )' />
                            <xsl:variable name='month' select='substring( person/dob_list/dob, 5 , 2 )' />
                            <xsl:variable name='day'   select='substring( person/dob_list/dob, 7 , 2 )' />
                            <xsl:variable name='dob'   select="concat($month, '/', $day, '/', $year )"  />
                            <xsl:if test="$dob = '//'">
                              None Specified
                            </xsl:if>
                            <xsl:if test="$dob != '//'">
                              <xsl:value-of select="$dob"/>
                            </xsl:if>
                          </td>
                          <td style="background-color:#fff;">
                            <xsl:if test="person/is_deceased = 'YES'">
                              Deceased
                            </xsl:if>
                          </td>
                          <td  style="background-color:#fff;">
                            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                          </td>
                        </tr>
                      </xsl:otherwise>
                    </xsl:choose>
                    <xsl:if test ="ReviewNote!=''">
                      <tr>
                        <td class="width_10"></td>
                        <td style="background-color:#FFFFE1"  class="ReviewNote">
                          <xsl:value-of select="ReviewNote"></xsl:value-of>
                        </td>
                        <td  style="background-color:#FFFFE1">
                          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                    </xsl:if>
                  </xsl:if>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:choose>
                    <xsl:when test="( position() mod 2 = 0 )">
                      <tr>
                        <td class="width_10">
                      
                        </td>
                        <td>  <xsl:value-of select="(position() )" />
                          <xsl:value-of select="person/lastname" />, <xsl:value-of select="person/firstname" /> <br/>
                          <xsl:variable name='ssn' 	select="person/ssn" />
                          <xsl:variable name='one' 	select='substring( $ssn, 0 , 4 )' />
                          <xsl:variable name='two'	select='substring( $ssn, 4 , 2 )' />
                          <xsl:variable name='three'  select='substring( $ssn, 6 , 4 )' />

                          <xsl:if test="$admin = '1'">
                            Social #: <xsl:value-of select="person/ssn"/><br />
                          </xsl:if>
                          <xsl:if test="$admin = '0'">
                            <xsl:value-of select="concat('Social #: ', 'xxx', '-', 'xx', '-', $three )"/>
                            <br />
                          </xsl:if>


                          <xsl:if test="address/phone_number != ''">
                            <xsl:value-of select="concat('Phone #: ', address/phone_number)" />
                            <br/>
                          </xsl:if>
                          <br />
                          <xsl:if test="person/aka_list/aka">
                            AKAs: <br/>
                          </xsl:if>
                          <xsl:for-each select="person/aka_list">
                            <xsl:value-of select="aka" />
                            <br/>
                          </xsl:for-each>
                        </td>
                        <td>
                          <xsl:value-of select="address/pre_direction" />
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="address/house_numer" />
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="address/street_name" />
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="address/street_suffix" />
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="address/post_direction" />
                          <br/>
                          <xsl:value-of select="address/city" />
                          <xsl:text>, </xsl:text>
                          <xsl:value-of select="address/state" />
                          <br />
                          <xsl:value-of select="address/zipcode" />
                          <xsl:if test="address/zip4 != ''">
                            -<xsl:value-of select="address/zip4" />
                          </xsl:if>
                        </td>
                        <td>
                          <xsl:variable name='year'  select='substring( address/report_date, 0 , 5 )' />
                          <xsl:variable name='month' select='substring( address/report_date, 5 , 2 )' />
                          <xsl:variable name='day'   select='substring( address/report_date, 7 , 2 )' />
                          <xsl:variable name='reportdate'   select="concat($month, '/', $day, '/', $year )"  />
                          <xsl:if test="$reportdate = '//'">

                          </xsl:if>
                          <xsl:if test="$reportdate != '//'">
                            <xsl:value-of select="$reportdate"/>
                          </xsl:if>
                        </td>
                        <td>
                          <xsl:variable name='year'  select='substring( person/dob_list/dob, 0 , 5 )' />
                          <xsl:variable name='month' select='substring( person/dob_list/dob, 5 , 2 )' />
                          <xsl:variable name='day'   select='substring( person/dob_list/dob, 7 , 2 )' />
                          <xsl:variable name='dob'   select="concat($month, '/', $day, '/', $year )"  />
                          <xsl:if test="$dob = '//'">
                            None Specified
                          </xsl:if>
                          <xsl:if test="$dob != '//'">
                            <xsl:value-of select="$dob"/>
                          </xsl:if>
                        </td>
                        <td>
                          <xsl:if test="person/is_deceased = 'YES'">
                            Deceased
                          </xsl:if>
                        </td>
                        <td >
                          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                        </td>
                      </tr>
                    </xsl:when>
                    <xsl:otherwise>
                      <tr>
                        <td class="width_10">
                       
                        </td>
                        <td style="background-color:#fff;">   <xsl:value-of select="(position() )" />
                          <xsl:value-of select="person/lastname" />, <xsl:value-of select="person/firstname" /> <br/>
                          <xsl:variable name='ssn' 	select="person/ssn" />
                          <xsl:variable name='one' 	select='substring( $ssn, 0 , 4 )' />
                          <xsl:variable name='two'	select='substring( $ssn, 4 , 2 )' />
                          <xsl:variable name='three'  select='substring( $ssn, 6 , 4 )' />

                          <xsl:if test="$admin = '1'">
                            Social #: <xsl:value-of select="person/ssn"/><br />
                          </xsl:if>
                          <xsl:if test="$admin = '0'">
                            <xsl:value-of select="concat('Social #: ', 'xxx', '-', 'xx', '-', $three )"/>
                            <br />
                          </xsl:if>


                          <xsl:if test="address/phone_number != ''">
                            <xsl:value-of select="concat('Phone #: ', address/phone_number)" />
                            <br/>
                          </xsl:if>
                          <br />
                          <xsl:if test="person/aka_list/aka">
                            AKAs: <br/>
                          </xsl:if>
                          <xsl:for-each select="person/aka_list">
                            <xsl:value-of select="aka" />
                            <br/>
                          </xsl:for-each>
                        </td>
                        <td style="background-color:#fff;">
                          <xsl:value-of select="address/pre_direction" />
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="address/house_numer" />
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="address/street_name" />
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="address/street_suffix" />
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="address/post_direction" />
                          <br/>
                          <xsl:value-of select="address/city" />
                          <xsl:text>, </xsl:text>
                          <xsl:value-of select="address/state" />
                          <br />
                          <xsl:value-of select="address/zipcode" />
                          <xsl:if test="address/zip4 != ''">
                            -<xsl:value-of select="address/zip4" />
                          </xsl:if>
                        </td>
                        <td style="background-color:#fff;">
                          <xsl:variable name='year'  select='substring( address/report_date, 0 , 5 )' />
                          <xsl:variable name='month' select='substring( address/report_date, 5 , 2 )' />
                          <xsl:variable name='day'   select='substring( address/report_date, 7 , 2 )' />
                          <xsl:variable name='reportdate'   select="concat($month, '/', $day, '/', $year )"  />
                          <xsl:if test="$reportdate = '//'">

                          </xsl:if>
                          <xsl:if test="$reportdate != '//'">
                            <xsl:value-of select="$reportdate"/>
                          </xsl:if>
                        </td>
                        <td style="background-color:#fff;">
                          <xsl:variable name='year'  select='substring( person/dob_list/dob, 0 , 5 )' />
                          <xsl:variable name='month' select='substring( person/dob_list/dob, 5 , 2 )' />
                          <xsl:variable name='day'   select='substring( person/dob_list/dob, 7 , 2 )' />
                          <xsl:variable name='dob'   select="concat($month, '/', $day, '/', $year )"  />
                          <xsl:if test="$dob = '//'">
                            None Specified
                          </xsl:if>
                          <xsl:if test="$dob != '//'">
                            <xsl:value-of select="$dob"/>
                          </xsl:if>
                        </td>
                        <td style="background-color:#fff;">
                          <xsl:if test="person/is_deceased = 'YES'">
                            Deceased
                          </xsl:if>
                        </td>
                        <td  style="background-color:#fff;">
                          <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                        </td>
                      </tr>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>
          </tbody>

         
        </table>
      </xsl:if>
    </center>



    <div class="requestId" style="text-align:center">
      Request ID :
      <xsl:text> </xsl:text>
      <xsl:value-of select="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/request_id" />
    </div>

      </div>
  </xsl:template>

</xsl:stylesheet>
