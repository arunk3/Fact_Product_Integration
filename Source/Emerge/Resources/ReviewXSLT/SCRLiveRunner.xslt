<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                              xmlns:a="http://ns.hr-xml.org/2004-08-02">
  <xsl:output method="html" encoding="iso-8859-1" />
  <xsl:template match="/">

    <center>
      <div class="sectionsummary" style="border:1px solid #9B004F; text-align:center;margin-bottom: 10px;">
        <h3>
          (<xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)" />) Results from LiveRunner SCR Report.<br />
        </h3>
      </div>

      <xsl:variable name="TotalRecords">
        <xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)" />
      </xsl:variable>

      <xsl:if test="$TotalRecords=0">

        <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
          <thead>
            <tr class="ncr_header_top">
              <th class="begin ncr_header_top" style="border: none;"></th>
              <th colspan="7" style="float: left; padding-top: 5px;">
                <span  style="font-size:15px;font-weight:bold; color:#666;">Search Information</span>
              </th>
              <th class="end ncr_header_top" style="border: none;"></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td >
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colspan="5">
                <h4>Name:</h4>
                <h4>
                  <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:GivenName"/>
                  <xsl:text xml:space="preserve">  </xsl:text>
                  <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:FamilyName"/>
                </h4>
                <br />
                <h4>DOB:</h4>
                <h4>
                  <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:DemographicDetail/a:DateOfBirth"/>
                </h4>
              </td>
              <td >
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td >
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colspan="5">
                <p>*** Clear ***</p>
              </td>
              <td >
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </tbody>
          <tfoot>
            <td ></td>
            <td colspan="5"></td>
            <td ></td>
          </tfoot>
        </table>

      </xsl:if>

      <table id="tblRCX" class="reporttable" border="0" cellpadding="0" cellspacing="0">
        <thead>
          <tr class="ncr_header_data">
            <th class="begin" style="border: none; width:40px;"></th>
            <th colspan="7">
              <span style="font-size: 15px;font-weight:bold; padding-top: 3px; display: inline-block;color: #666;padding-bottom: 8px;padding-top: 8px;">CRIMINAL RESULTS</span>
            </th>
            <th class="end" style="border: none;"></th>
          </tr>
        </thead>
        <tbody>

          <xsl:variable name="TextData">
            <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text" />
          </xsl:variable>
          <xsl:if test="$TextData = ''">
            <p>*** Clear ***</p>
          </xsl:if>
          <xsl:variable name="SSNNo">
            xxx-xx-<xsl:value-of select="substring(a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:DemographicDetail/a:GovernmentId,6,4)" />
          </xsl:variable>

          <xsl:if test="not(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text)">
            <tr>
              <td >
              </td>
              <td colspan="5">
                <div style="width: 90%; margin: 0 auto; text-align: left">
                  <div style="width:588px; text-align:left">
                    <div style="font-size:11px;" id="report_rcx">
                      <pre>

                        <xsl:if test="not(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningResults/a:Text)">
                          <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus = 'Cancelled'">
                            <p>
                              There was an error running this report.<br /> Please contact our customer support team to quickly resolve this issue.
                            </p>
                          </xsl:if>
                          <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:ScreeningStatus/a:OrderStatus != 'Cancelled'">
                            <p>*** Clear ***</p>
                          </xsl:if>
                          <p></p>
                        </xsl:if>
                      </pre>
                    </div>
                  </div>
                </div>
              </td>
              <td >
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <xsl:if test="(count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase)='0')">
            <tr>
              <td >
              </td>
              <td align="left" colspan="5"  style="color:red;">
                <br/>
                <b>
                  criminal record : clear
                </b>
                <br/>
              </td>
              <td >
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </xsl:if>
          <xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:CriminalReport/a:CriminalCase">
            <xsl:choose>
              <xsl:when test ="@Deleted">

                <xsl:if test ="@Deleted!='1'">

                  <xsl:choose>
                    <xsl:when test="position()>1">
                      <tr style="height:20px;">
                        <td >
                        </td>
                        <td colspan="5" style="text-align:center;">
                          <span style="font-size:16px;font-weight:bold;color:white;">
                          </span>
                        </td>
                        <td ></td>
                      </tr>
                    </xsl:when>
                  </xsl:choose>

                  <tr class="ncr_header_data white-bg">

                    <td  class="ncr_header_data" colspan="9" style="border:solid 0px #ccc;text-align:left;">
                      <div class="mar-5">
                        <div style="padding: 8px; color: rgb(102, 102, 102); float: left;width: 161px;padding-left: 0px;">
                          <div class="file-name" style="float: left; font-size: 15px; font-weight: normal; padding-top: 0px;">
                            CRIMINAL RECORD
                          </div>
                        </div>

                        <span class="rednosetagainnumber" style="padding-top: 5px;margin-left: 0px;margin-top: 6px;">
                          <xsl:value-of select="(position())" />
                        </span>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td class="begin" style="border: none;">
                    </td>
                    <td colspan="5" style="text-align:center;font-family: segoeui !important;color: #666 !important;">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr >
                          <td width="44%" class="column1">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:30%;">
                                  Source:
                                </td>
                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:value-of select="a:CourtName"/>
                                </td>
                              </tr>

                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                  Full Name:
                                </td>
                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                    <xsl:if test="a:FamilyName/@primary='true'">
                                      <xsl:value-of select="a:FamilyName"/>
                                      <xsl:if test="(a:GivenName)">
                                        <span>, </span>
                                      </xsl:if>
                                      <xsl:value-of select="a:GivenName"/>
                                      <xsl:if test="(a:MiddleName)">
                                        <span>, </span>
                                      </xsl:if>
                                      <xsl:value-of select="a:MiddleName"/>
                                      <xsl:element name="br"/>
                                    </xsl:if>
                                  </xsl:for-each>
                                </td>
                              </tr>

                              <xsl:if test="(a:SubjectIdentification/a:PersonName/a:FamilyName/@primary='false')">
                                <tr>
                                  <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                    AKA Name:
                                  </td>
                                  <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                    <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                      <xsl:if test="a:FamilyName/@primary='false'">
                                        <xsl:value-of select="a:FamilyName"/>
                                        <xsl:if test="(a:GivenName)">
                                          <span>, </span>
                                        </xsl:if>
                                        <xsl:value-of select="a:GivenName"/>
                                        <xsl:if test="(a:MiddleName)">
                                          <span>, </span>
                                        </xsl:if>
                                        <xsl:value-of select="a:MiddleName"/>
                                        <xsl:element name="br"/>
                                      </xsl:if>
                                    </xsl:for-each>
                                  </td>
                                </tr>
                              </xsl:if>

                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                  DOB:
                                </td>
                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:value-of select='substring(a:SubjectIdentification/a:DemographicDetail/a:DateOfBirth,1,10)' />
                                </td>
                              </tr>


                            </table>
                          </td>
                          <td width="50%">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <xsl:if test="(a:SubjectIdentification/a:DemographicDetail/a:GovernmentId/@documentType='DRIVERS LICENSE')">
                                <tr>
                                  <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                    DL#:
                                  </td>
                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                    <xsl:value-of select ="a:SubjectIdentification/a:DemographicDetail/a:GovernmentId[@documentType='DRIVERS LICENSE']"/>
                                  </td>
                                </tr>
                              </xsl:if>

                              <xsl:if test="(a:SubjectIdentification/a:PostalAddress)">
                                <tr>
                                  <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                    Address:
                                  </td>
                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                    <xsl:for-each select="a:SubjectIdentification/a:PostalAddress">
                                      <xsl:if test="(a:DeliveryAddress)">
                                        <xsl:value-of select ="a:DeliveryAddress"/>
                                        <xsl:if test="(a:Municipality)">
                                          <span>, </span>
                                        </xsl:if>
                                      </xsl:if>
                                      <xsl:if test="(a:Municipality)">
                                        <xsl:value-of select ="a:Municipality"/>
                                        <xsl:if test="(a:Region)">
                                          <span>, </span>
                                        </xsl:if>
                                      </xsl:if>
                                      <xsl:if test="(a:Region)">
                                        <xsl:value-of select ="a:Region"/>
                                        <xsl:if test="(a:PostalCode)">
                                          <span> - </span>
                                        </xsl:if>
                                      </xsl:if>
                                      <xsl:if test="(a:PostalCode)">
                                        <xsl:value-of select ="a:PostalCode"/>
                                      </xsl:if>
                                      <xsl:element name="br" />
                                    </xsl:for-each>
                                  </td>
                                </tr>
                              </xsl:if>

                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                  Race:
                                </td>
                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:value-of select="a:SubjectIdentification/a:DemographicDetail/a:Race"/>
                                </td>
                              </tr>

                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                  Gender:
                                </td>
                                <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                  <xsl:choose>
                                    <xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:GenderCode = 1">
                                      Male
                                    </xsl:when>
                                    <xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:GenderCode = 2">
                                      Female
                                    </xsl:when>
                                    <xsl:otherwise>UnKnown</xsl:otherwise>
                                  </xsl:choose>
                                </td>
                              </tr>

                              <xsl:if test="(a:AgencyReference[@type='Docket'])">
                                <tr>
                                  <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                    Docket Number:
                                  </td>

                                  <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                    <xsl:value-of select="a:AgencyReference[@type='Docket']"/>
                                  </td>
                                </tr>
                              </xsl:if>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                    <td ></td>
                  </tr>

                  <xsl:for-each select="a:Charge">

                    <tr style="border:1px solid #999;">
                      <td class="begin" style="border: none;">
                      </td>
                      <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#fff;text-align:center;">
                          <tr>
                            <xsl:choose>
                              <xsl:when test="(position()mod 2)=1">
                                <td width="10%" style="border-right:solid 0px black;text-align:center;">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                      <td style="text-align:right;">
                                        <div style="padding: 15px; color: rgb(102, 102, 102); float: left; width: 143px;">
                                          <div class="file-name" style="float:left; padding-top: 4px; color:#232323 !important;">
                                            Charge
                                          </div>
                                          <span class="blue-no text-pad" style="float:left; padding-top: 6px;">
                                            <xsl:value-of select="(position())" />
                                          </span>
                                        </div>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </xsl:when>
                              <xsl:otherwise>
                                <td width="10%" style="border-right:solid 0px black;background-color:white;text-align:center;">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                      <td style="text-align:right;background-color:white;">
                                        <div style="padding: 15px; color: rgb(102, 102, 102); float: left; width: 143px;">
                                          <div class="file-name" style="float:left; padding-top: 4px; color:#232323 !important;">
                                            Charge
                                          </div>
                                          <span class="blue-no text-pad" style="float:left; padding-top: 6px;">
                                            <xsl:value-of select="(position())" />
                                          </span>
                                        </div>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </xsl:otherwise>
                            </xsl:choose>
                            <xsl:choose>
                              <xsl:when test="(position()mod 2)=1">
                                <td width="45%" style="">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                      <td style="text-align:right;border-bottom:0px solid black;width:25%;">
                                        <b>Charge:</b>
                                      </td>
                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:value-of select="a:ChargeOrComplaint"/>
                                      </td>
                                    </tr>

                                    <xsl:if test="(a:ArrestDate)">
                                      <tr>
                                        <td style="text-align:right;border-bottom:0px solid black;">
                                          <b>Arrest Date:</b>
                                        </td>

                                        <td style="text-align:left;border-bottom:0px solid black;">
                                          <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                        </td>
                                      </tr>
                                    </xsl:if>

                                    <xsl:if test="(a:OffenseDate) != ''">
                                      <tr>
                                        <td style="text-align:right;border-bottom:0px solid black;">
                                          <b>Offense Date:</b>
                                        </td>

                                        <td style="text-align:left;border-bottom:0px solid black;">
                                          <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                        </td>
                                      </tr>
                                    </xsl:if>

                                    <xsl:if test="(a:DispositionDate) != ''">
                                      <tr>
                                        <td style="text-align:right;border-bottom:0px solid black;">

                                          <b>Disposition Date:</b>

                                        </td>

                                        <td style="text-align:left;border-bottom:0px solid black;">
                                          <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                        </td>
                                      </tr>
                                    </xsl:if>
                                  </table>
                                </td>
                                <td width="45%" style="text-align:left;border-bottom:0px solid black;">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                      <td style="text-align:right;border-bottom:0px solid black;">

                                        <b>Severity:</b>

                                      </td>

                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:value-of select="a:ChargeTypeClassification"/>
                                      </td>
                                    </tr>

                                    <tr>
                                      <td style="text-align:left;border-bottom:0px solid black;">

                                        <b>Final Disposition:</b>

                                      </td>

                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:value-of select="a:Disposition"/>
                                      </td>
                                    </tr>

                                    <tr>
                                      <td style="text-align:left;border-bottom:0px solid black;">

                                        <b>Comments:</b>

                                      </td>

                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:for-each select="a:Comment">
                                          <span style="float:left;">
                                            <xsl:value-of select="."/>
                                            <xsl:element name="br" />
                                          </span>
                                        </xsl:for-each>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </xsl:when>
                              <xsl:otherwise>
                                <td width="45%">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                        <b>Charge:</b>
                                      </td>
                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:value-of select="a:ChargeOrComplaint"/>
                                      </td>
                                    </tr>

                                    <xsl:if test="(a:ArrestDate)">
                                      <tr>
                                        <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                          <b>Arrest Date:</b>
                                        </td>

                                        <td style="text-align:left;border-bottom:0px solid black;">
                                          <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                        </td>
                                      </tr>
                                    </xsl:if>

                                    <xsl:if test="(a:OffenseDate) != ''">
                                      <tr>
                                        <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                          <b>Offense Date:</b>
                                        </td>

                                        <td style="text-align:left;border-bottom:0px solid black;">
                                          <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                        </td>
                                      </tr>
                                    </xsl:if>

                                    <xsl:if test="(a:DispositionDate) != ''">
                                      <tr>
                                        <td style="text-align:right;background-color:white;border-bottom:0px solid black;">

                                          <b>Disposition Date:</b>

                                        </td>

                                        <td style="text-align:left;border-bottom:0px solid black;">
                                          <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                        </td>
                                      </tr>
                                    </xsl:if>
                                  </table>
                                </td>
                                <td width="45%">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">

                                        <b>Severity:</b>

                                      </td>

                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:value-of select="a:ChargeTypeClassification"/>
                                      </td>
                                    </tr>

                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">

                                        <b>Final Disposition:</b>

                                      </td>

                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:value-of select="a:Disposition"/>
                                      </td>
                                    </tr>

                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">

                                        <b>Comments:</b>

                                      </td>

                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:for-each select="a:Comment">
                                          <span style="float:left;">
                                            <xsl:value-of select="."/>
                                            <xsl:element name="br" />
                                          </span>
                                        </xsl:for-each>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </xsl:otherwise>
                            </xsl:choose>
                          </tr>
                        </table>
                      </td>
                      <td ></td>
                    </tr>
                  </xsl:for-each>
                  <xsl:if test="a:ReviewNote!=''">
                    <tr>
                      <td class="begin" style="border: none; background-color:#FFFFE1;">
                      </td>
                      <td colspan="4" style="width:95%;background-color:#FFFFE1; border:none;" class="ReviewNote">
                        <xsl:value-of select="a:ReviewNote"></xsl:value-of>
                      </td>
                      <td align="right" valign="bottom" style="vertical-align:bottom; background-color:#FFFFE1;">

                      </td>
                      <td class="end" style="border: none; background-color:#FFFFE1;"></td>
                    </tr>
                  </xsl:if>
                </xsl:if>
              </xsl:when>
              <!--This Invoked If Deleted Attrribute Is Note Present In XML-->
              <xsl:otherwise>
                <xsl:choose>
                  <xsl:when test="position()>1">
                    <tr style="height:20px;">
                      <td >
                      </td>
                      <td colspan="5" style="text-align:center;">
                        <span style="font-size:16px;font-weight:bold;color:white;">
                        </span>
                      </td>
                      <td ></td>
                    </tr>
                  </xsl:when>
                </xsl:choose>

                <tr class="ncr_header_data white-bg">

                  <td  class="ncr_header_data" colspan="9" style="border:solid 0px #ccc;text-align:left;">
                    <div class="mar-5">
                      <div style="padding: 15px; color: rgb(102, 102, 102); float: left;width: 161px;padding-left: 0px;">
                        <div class="file-name" style="float: left; font-size: 15px; font-weight: normal; padding-top: 0px;">
                          Criminal Record
                        </div>
                      </div>

                      <span class="rednosetagainnumber" style="padding-top:6px;margin-left: 0px;margin-top: 13px;">
                        <xsl:value-of select="(position())" />
                      </span>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td >
                  </td>
                  <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                      <tr>
                        <td width="50%">
                          <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                Source:
                              </td>
                              <td style="text-align:left;border-bottom:0px solid black;">
                                <xsl:value-of select="a:CourtName"/>
                              </td>
                            </tr>

                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                Full Name:
                              </td>
                              <td style="text-align:left;border-bottom:0px solid black;">
                                <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                  <xsl:if test="a:FamilyName/@primary='true'">
                                    <xsl:value-of select="a:FamilyName"/>
                                    <xsl:if test="(a:GivenName)">
                                      <span>, </span>
                                    </xsl:if>
                                    <xsl:value-of select="a:GivenName"/>
                                    <xsl:if test="(a:MiddleName)">
                                      <span>, </span>
                                    </xsl:if>
                                    <xsl:value-of select="a:MiddleName"/>
                                    <xsl:element name="br"/>
                                  </xsl:if>
                                </xsl:for-each>
                              </td>
                            </tr>

                            <xsl:if test="(a:SubjectIdentification/a:PersonName/a:FamilyName/@primary='false')">
                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                  AKA Name:
                                </td>
                                <td style="text-align:left;border-bottom:0px solid black;">
                                  <xsl:for-each select="a:SubjectIdentification/a:PersonName">
                                    <xsl:if test="a:FamilyName/@primary='false'">
                                      <xsl:value-of select="a:FamilyName"/>
                                      <xsl:if test="(a:GivenName)">
                                        <span>, </span>
                                      </xsl:if>
                                      <xsl:value-of select="a:GivenName"/>
                                      <xsl:if test="(a:MiddleName)">
                                        <span>, </span>
                                      </xsl:if>
                                      <xsl:value-of select="a:MiddleName"/>
                                      <xsl:element name="br"/>
                                    </xsl:if>
                                  </xsl:for-each>
                                </td>
                              </tr>
                            </xsl:if>

                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                DOB:
                              </td>
                              <td style="text-align:left;border-bottom:0px solid black;">
                                <xsl:value-of select='substring(a:SubjectIdentification/a:DemographicDetail/a:DateOfBirth,1,10)' />
                              </td>
                            </tr>


                          </table>
                        </td>
                        <td width="50%">
                          <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <xsl:if test="(a:SubjectIdentification/a:DemographicDetail/a:GovernmentId/@documentType='DRIVERS LICENSE')">
                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                  DL#:
                                </td>
                                <td style="text-align:left;border-bottom:0px solid black;">
                                  <xsl:value-of select ="a:SubjectIdentification/a:DemographicDetail/a:GovernmentId[@documentType='DRIVERS LICENSE']"/>
                                </td>
                              </tr>
                            </xsl:if>

                            <xsl:if test="(a:SubjectIdentification/a:PostalAddress)">
                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                  Address:
                                </td>
                                <td style="text-align:left;border-bottom:0px solid black;">
                                  <xsl:for-each select="a:SubjectIdentification/a:PostalAddress">
                                    <xsl:if test="(a:DeliveryAddress)">
                                      <xsl:value-of select ="a:DeliveryAddress"/>
                                      <xsl:if test="(a:Municipality)">
                                        <span>, </span>
                                      </xsl:if>
                                    </xsl:if>
                                    <xsl:if test="(a:Municipality)">
                                      <xsl:value-of select ="a:Municipality"/>
                                      <xsl:if test="(a:Region)">
                                        <span>, </span>
                                      </xsl:if>
                                    </xsl:if>
                                    <xsl:if test="(a:Region)">
                                      <xsl:value-of select ="a:Region"/>
                                      <xsl:if test="(a:PostalCode)">
                                        <span> - </span>
                                      </xsl:if>
                                    </xsl:if>
                                    <xsl:if test="(a:PostalCode)">
                                      <xsl:value-of select ="a:PostalCode"/>
                                    </xsl:if>
                                    <xsl:element name="br" />
                                  </xsl:for-each>
                                </td>
                              </tr>
                            </xsl:if>

                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                Race:
                              </td>
                              <td style="text-align:left;border-bottom:0px solid black;">
                                <xsl:value-of select="a:SubjectIdentification/a:DemographicDetail/a:Race"/>
                              </td>
                            </tr>

                            <tr>
                              <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                Gender:
                              </td>
                              <td style="text-align:left;background-color:white;border-bottom:0px solid black;">
                                <xsl:choose>
                                  <xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:GenderCode = 1">
                                    Male
                                  </xsl:when>
                                  <xsl:when test="a:SubjectIdentification/a:DemographicDetail/a:GenderCode = 2">
                                    Female
                                  </xsl:when>
                                  <xsl:otherwise>UnKnown</xsl:otherwise>
                                </xsl:choose>
                              </td>
                            </tr>

                            <xsl:if test="(a:AgencyReference[@type='Docket'])">
                              <tr>
                                <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                  Docket Number:
                                </td>

                                <td style="text-align:left;border-bottom:0px solid black;">
                                  <xsl:value-of select="a:AgencyReference[@type='Docket']"/>
                                </td>
                              </tr>
                            </xsl:if>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td ></td>
                </tr>

                <xsl:for-each select="a:Charge">

                  <tr>
                    <td >
                    </td>
                    <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border:solid 1px black;background-color:red;text-align:center;">
                        <tr>
                          <xsl:choose>
                            <xsl:when test="(position()mod 2)=1">
                              <td width="10%" style="text-align:left;border-bottom:0px solid black;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                    <td style="text-align:left;border-bottom:0px solid black;">
                                      <span style="font-size:12px;font-weight:bold;color:#000;">
                                        <b>Charge:</b>
                                        <xsl:value-of select="(position())" />
                                      </span>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </xsl:when>
                            <xsl:otherwise>
                              <td width="10%" style="border-right:solid 1px black;background-color:white;text-align:center;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                    <td style="text-align:right;background-color:white;">
                                      <span style="font-size:12px;font-weight:bold;color:#000;">
                                        <b>Charge:</b>
                                        <xsl:value-of select="(position())" />
                                      </span>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </xsl:otherwise>
                          </xsl:choose>
                          <xsl:choose>
                            <xsl:when test="(position()mod 2)=1">
                              <td width="45%" style="background-color:#dcdcdc;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                    <td style="text-align:left;border-bottom:0px solid black;">
                                      <b>Charge:</b>
                                    </td>
                                    <td style="text-align:left;border-bottom:0px solid black;">
                                      <xsl:value-of select="a:ChargeOrComplaint"/>
                                    </td>
                                  </tr>

                                  <xsl:if test="(a:ArrestDate)">
                                    <tr>
                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <b>Arrest Date:</b>
                                      </td>

                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>

                                  <xsl:if test="(a:OffenseDate) != ''">
                                    <tr>
                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <b>Offense Date:</b>
                                      </td>

                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>

                                  <xsl:if test="(a:DispositionDate) != ''">
                                    <tr>
                                      <td style="text-align:left;border-bottom:0px solid black;">

                                        <b>Disposition Date:</b>

                                      </td>

                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>
                                </table>
                              </td>
                              <td width="45%" style="background-color:#dcdcdc;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                    <td style="text-align:left;border-bottom:0px solid black;">
                                      <b>Severity:</b>


                                    </td>

                                    <td style="text-align:left;border-bottom:0px solid black;">
                                      <xsl:value-of select="a:ChargeTypeClassification"/>
                                    </td>
                                  </tr>

                                  <tr>
                                    <td style="text-align:left;border-bottom:0px solid black;">
                                      <b>Final Disposition:</b>


                                    </td>

                                    <td style="text-align:left;border-bottom:0px solid black;">
                                      <xsl:value-of select="a:Disposition"/>
                                    </td>
                                  </tr>

                                  <tr>
                                    <td style="text-align:left;border-bottom:0px solid black;">
                                      <b>Comments:</b>


                                    </td>

                                    <td style="text-align:left;border-bottom:0px solid black;">
                                      <xsl:for-each select="a:Comment">
                                        <span style="float:left;">
                                          <xsl:value-of select="."/>
                                          <xsl:element name="br" />
                                        </span>
                                      </xsl:for-each>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </xsl:when>
                            <xsl:otherwise>
                              <td width="45%">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                    <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                      <b>Charge:</b>
                                    </td>
                                    <td style="text-align:left;border-bottom:0px solid black;">
                                      <xsl:value-of select="a:ChargeOrComplaint"/>
                                    </td>
                                  </tr>

                                  <xsl:if test="(a:ArrestDate)">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                        <b>Arrest Date:</b>
                                      </td>

                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:ArrestDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>

                                  <xsl:if test="(a:OffenseDate) != ''">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                        <b>Offense Date:</b>
                                      </td>
                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:OffenseDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>
                                  <xsl:if test="(a:DispositionDate) != ''">
                                    <tr>
                                      <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                        <b>Disposition Date:</b>
                                      </td>
                                      <td style="text-align:left;border-bottom:0px solid black;">
                                        <xsl:value-of select='substring(a:DispositionDate,1,10)' />
                                      </td>
                                    </tr>
                                  </xsl:if>
                                </table>
                              </td>
                              <td width="45%">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tr>
                                    <td style="text-align:right;background-color:white;border-bottom:0px solid black;width:25%;">
                                      <b>Severity:</b>
                                    </td>
                                    <td style="text-align:left;border-bottom:0px solid black;">
                                      <xsl:value-of select="a:ChargeTypeClassification"/>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                      <b>Final Disposition:</b>
                                    </td>
                                    <td style="text-align:left;border-bottom:0px solid black;">
                                      <xsl:value-of select="a:Disposition"/>
                                    </td>
                                  </tr>

                                  <tr>
                                    <td style="text-align:right;background-color:white;border-bottom:0px solid black;">
                                      <b>Coments:</b>
                                    </td>
                                    <td style="text-align:left;border-bottom:0px solid black;">
                                      <xsl:for-each select="a:Comment">
                                        <span style="float:left;">
                                          <xsl:value-of select="."/>
                                          <xsl:element name="br" />
                                        </span>
                                      </xsl:for-each>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </xsl:otherwise>
                          </xsl:choose>
                        </tr>
                      </table>
                    </td>
                    <td ></td>
                  </tr>
                </xsl:for-each>
                <xsl:if test="a:ReviewNote!=''">
                  <tr>
                    <td class="begin" style="border: none; background-color:#FFFFE1;">
                    </td>
                    <td colspan="4" style="width:95%;background-color:#FFFFE1; border:none;" class="ReviewNote">
                      <xsl:value-of select="a:ReviewNote"></xsl:value-of>
                    </td>
                    <td align="right" valign="bottom" style="vertical-align:bottom; background-color:#FFFFE1;">

                    </td>
                    <td class="end" style="border: none; background-color:#FFFFE1;"></td>
                  </tr>
                </xsl:if>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </tbody>
        <tfoot>
          <td ></td>
          <td colspan="5"></td>
          <td ></td>
        </tfoot>
      </table>
    </center>

  </xsl:template>
</xsl:stylesheet>