<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" encoding="iso-8859-1"/>
  <xsl:template match="/">
    <div class="sectionsummary">
      <h3>
        Request ID:

        <xsl:variable name="request_id" select="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/request_id"/>

        <xsl:choose>
          <xsl:when test="$request_id = ''">
            <xsl:text> No Records </xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/request_id" />
          </xsl:otherwise>
        </xsl:choose>

      </h3>
    </div>

    <center>

      <xsl:if test="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/rows_returned = '0'">
        <table class="reporttable" id="table" border="0" cellpadding="0" cellspacing="0">
          <thead>
            <tr>
              <th ></th>
              <th colspan="100%">Search Results</th>
              <th ></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td >
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
              <td colspan="100%" style="border-right: none;">
                <table>
                  <tr>
                    <td colspan="100%" class="recordHeader">
                      No Records Found.
                      <br />
                      * PASSED OFAC TEST *
                    </td>
                  </tr>
                </table>
              </td>
              <td >
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
            <tr>
              <td >
              </td>
              <td colspan="100%" >
                <div id="txtReviewNoteNRF_OFAC" name="txtNote_{(position())}" class="ReviewNote" style="width:100%;background-color:#FFFFE1;">
                </div>
              </td>
              <td  >
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </tbody>
          <tfoot>
            <td ></td>
            <td style="border-right: none;" colspan="100%"></td>
            <td ></td>
          </tfoot>
        </table>

      </xsl:if>

      <table class="reporttable" id="table" border="0" cellpadding="0" cellspacing="0">
        <thead>
          <tr>
            <th ></th>
            <th colspan="100%">Search Information2</th>
            <th ></th>
          </tr>
        </thead>
        <tbody>
          <xsl:variable name="counter" select="0" />
          <xsl:for-each select="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/response_data/response_row">
            <xsl:if test="$counter + 1" />
            <xsl:choose>
              <xsl:when test ="@Deleted">

                <xsl:if test ="@Deleted!='1'">
                  <tr>
                    <td >
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                    </td>
                    <td colspan="100%" style="border-right: none;">
                      <table>
                        <tr>
                          <td colspan="100%" class="recordHeader">
                            <div class="file-name" style="font-size: 12px; font-weight: bold; color: rgb(0, 0, 0); float: left; padding-left: 17px;">Record</div>
                            <span class="blue-no" style="padding-top: 5px; position: absolute; margin-top: -2px; margin-left: -18px;">
                              <xsl:value-of select="$counter" />
                            </span>
                          </td>
                        </tr>
                        <tr>
                          <th align="left">Score:</th>
                          <td>
                            <xsl:value-of select="score" />
                          </td>
                        </tr>
                        <tr>
                          <th align="left">File Name:</th>
                          <td>
                            <xsl:value-of select="filename" />
                          </td>
                          <th align="left">File Date:</th>
                          <td>
                            <xsl:value-of select="filedate" />
                          </td>
                        </tr>
                        <tr>
                          <th align="left">Entity Name:</th>
                          <td tyle="padding-top: 0px ! important;">
                            <xsl:value-of select="entityname" />
                          </td>
                          <th align="left">Best Name:</th>
                          <td>
                            <xsl:value-of select="bestname" />
                          </td>
                        </tr>
                        <tr>
                          <th align="left">Listing:</th>
                          <td colspan="3" style="padding-top: 0px;">
                            <xsl:value-of select="listing" />
                          </td>
                        </tr>
                      </table>
                    </td>
                    <td >
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                    </td>
                  </tr>
                  <xsl:if test ="ReviewNote!=''">
                    <tr>
                      <td >
                        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                      </td>
                      <td colspan="100%" style="width:97%;background-color:#FFFFE1" class="ReviewNote">
                        <xsl:value-of select="ReviewNote"/>
                      </td>
                      <td >
                        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                      </td>
                    </tr>
                  </xsl:if>
                </xsl:if>
              </xsl:when>
              <xsl:otherwise>
                <tr>
                  <td >
                    <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                  </td>
                  <td colspan="100%" style="border-right: none;">
                    <table>
                      <tr>
                        <td colspan="100%" class="recordHeader">
                          <div class="file-name" style="font-size: 12px; font-weight: bold; color: rgb(0, 0, 0); float: left; padding-left: 17px;">Record </div>
                          <span class="blue-no" style="padding-top: 5px; position: absolute; margin-top: -2px; margin-left: -18px;">
                            <xsl:value-of select="$counter" />
                          </span>
                        </td>
                      </tr>
                      <tr>
                        <th align="left">Score:</th>
                        <td>
                          <xsl:value-of select="score" />
                        </td>
                      </tr>
                      <tr>
                        <th align="left">File Name:</th>
                        <td>
                          <xsl:value-of select="filename" />
                        </td>
                        <th align="left">File Date:</th>
                        <td>
                          <xsl:value-of select="filedate" />
                        </td>
                      </tr>
                      <tr>
                        <th align="left">Entity Name:</th>
                        <td style="padding-top: 0px ! important;">
                          <xsl:value-of select="entityname" />
                        </td>
                        <th align="left">Best Name:</th>
                        <td>
                          <xsl:value-of select="bestname" />
                        </td>
                      </tr>
                      <tr>
                        <th align="left">Listing:</th>
                        <td colspan="3">
                          <xsl:value-of select="listing" />
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td >
                    <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                  </td>
                </tr>
              </xsl:otherwise>
            </xsl:choose>

          </xsl:for-each>

          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">Australian Department of Foreign Affairs and Trade (DFAT)</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">Bank of England Consolidated List of Financial Sanctions</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">Bureau of Industry and Security</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">OSFI Terrorism Financing</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">DTC Debarred parties</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">European Union Consolidated List</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">FBI Hijack Suspects</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">FBI Most Wanted</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">FBI Most Wanted Terrorists</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">FBI Seeking Information</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">FBI Top Ten Most Wanted</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">HKMA</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">Interpol Most Wanted</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">MAS</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">Non-cooperative Countries and Territories</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">Non-proliferation</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">OFAC Sanctions Programs and Countries</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">OFAC's Specially Designated Nationals and Blocked Persons</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">Politically Exposed Persons</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">Primary Money Laundering Concern (PMLC)</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">Terrorist Exclusion List</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">Unauthorized Banks</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">United Nations Consolidated List</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">World Bank Debarred Parties</td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
        </tbody>
        <tfoot>
          <td ></td>
          <td style="border-right: none;" colspan="100%"></td>
          <td ></td>
        </tfoot>
      </table>
    </center>
  </xsl:template>

</xsl:stylesheet>
