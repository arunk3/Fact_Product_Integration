<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" encoding="iso-8859-1"/>

	<xsl:template match="/">


		<div class="sectionsummary">
			<h3>
				(<xsl:value-of select="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/rows_returned" />) Results ECR Report
			</h3>
			<br />
		</div>

        <xsl:if test="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/rows_returned = '0'">
            <table id="peopleTable" class="reporttable" border="0" style="width:100%" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th ></th>
                        <th colspan="5">Search Information</th>
                        <th ></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td >
                            <xsl:value-of select="(position() )"/>
                        </td>
                        <td colspan="5"  width="96%">
                            No Records Found.
                        </td>
                        <td >
                            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <td ></td>
                    <td colspan="5"></td>
                    <td ></td>
                </tfoot>
            </table>
        </xsl:if>

        <xsl:if test="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/rows_returned != '0'">
            <table id="peopleTable" class="reporttable" border="0" cellpadding="0"  style="width:100%;" cellspacing="0">
                <thead>
                    <tr>
                        <th ></th>
                        <th colspan="5">Search Results</th>
                        <th ></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td >
                            <xsl:value-of select="(position() )"/>
                        </td>
                        <td colspan="5"  width="96%">

                            <xsl:value-of select="substring-after(XML_INTERFACE/CREDITREPORT/HTMLREPORT, 'body&gt;')" disable-output-escaping="yes" />

                        </td>
                        <td >
                            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <td ></td>
                    <td colspan="5"></td>
                    <td ></td>
                </tfoot>
            </table>
        </xsl:if>

        <xsl:if test="XML_INTERFACE/CREDITREPORT/OBJECTS/BUREAUERROR = 'True' or XML_INTERFACE/CREDITREPORT/cp_xmldirect/general_error !='' ">
            <table id="peopleTable" class="reporttable" border="0" cellpadding="0"  style="width:100%;" cellspacing="0">
                <thead>
                    <tr>
                        <th ></th>
                        <th colspan="5">Search Results</th>
                        <th ></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td >
                            <xsl:value-of select="(position() )"/>
                        </td>
                        <td colspan="5">

                            <xsl:value-of select="XML_INTERFACE/CREDITREPORT/HTMLREPORT/MESSAGE" disable-output-escaping="yes" />

                        </td>
                        <td >
                            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <td ></td>
                    <td colspan="5"></td>
                    <td ></td>
                </tfoot>
            </table>
        </xsl:if>

	</xsl:template>
</xsl:stylesheet>