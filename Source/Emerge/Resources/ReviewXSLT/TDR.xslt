<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" encoding="utf-8"/>
  <xsl:template match="/">

    <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
      <thead>
        <tr>
          <th ></th>
          <th>Name5</th>
          <th>Gender</th>
          <th>DOB</th>
          <th>(:age)</th>
          <th>SSN</th>
          <th ></th>
        </tr>
      </thead>
      <tbody>
        <xsl:for-each select="XML_INTERFACE/CREDITREPORT/response/individual/akas/identity">
          <xsl:choose>
            <xsl:when test ="@Deleted">

              <xsl:if test ="@Deleted!='1'">
                <tr>
                  <td >
                    <xsl:value-of select="(position() )"/>
                  </td>
                  <xsl:choose>
                    <xsl:when test="(position() mod 2 !=0)">
                      <td style="background-color:#fff;">
                        <xsl:value-of select="concat(name/first, ' ', name/middle, ', ', name/last)" />
                      </td>
                      <td style="background-color:#fff;">
                        <xsl:value-of select="name/gender" />
                      </td>
                      <td style="background-color:#fff;">
                        <xsl:value-of select="concat(dob/month,'/',dob/day,'/',dob/year)" />
                      </td>
                      <td style="background-color:#fff;">
                        :<xsl:value-of select="age" />
                      </td>
                      <td style="border-right: none;background-color:#fff;">
                        xxx-xx-xxxx
                      </td>
                      <td style="background-color:#fff;" >
                        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                      </td>
                    </xsl:when>
                    <xsl:otherwise>
                      <td>
                        <xsl:value-of select="concat(name/first, ' ', name/middle, ', ', name/last)" />
                      </td>
                      <td>
                        <xsl:value-of select="name/gender" />
                      </td>
                      <td>
                        <xsl:value-of select="concat(dob/month,'/',dob/day,'/',dob/year)" />
                      </td>
                      <td>
                        :<xsl:value-of select="age" />
                      </td>
                      <td style="border-right: none;">
                        xxx-xx-xxxx
                      </td>
                      <td>
                        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                      </td>
                    </xsl:otherwise>
                  </xsl:choose>
                </tr>
                <xsl:if test ="ReviewNote!=''">
                  <tr>
                    <td >
                    </td>
                    <td colspan="5" style="width:97%;background-color:#FFFFE1" class="ReviewNote">
                      <xsl:value-of select="ReviewNote"/>
                    </td>
                    <td  style="background-color:#FFFFE1">
                    </td>
                  </tr>
                </xsl:if>
              </xsl:if>
            </xsl:when>
            <xsl:otherwise>
              <tr>
                <td>
                  <xsl:value-of select="(position() )"/>
                </td>
                <xsl:choose>
                  <xsl:when test="(position() mod 2 !=0)">
                    <td style="background-color:#fff;">
                      <xsl:value-of select="concat(name/first, ' ', name/middle, ', ', name/last)" />
                    </td>
                    <td style="background-color:#fff;">
                      <xsl:value-of select="name/gender" />
                    </td>
                    <td style="background-color:#fff;">
                      <xsl:value-of select="concat(dob/month,'/',dob/day,'/',dob/year)" />
                    </td>
                    <td style="background-color:#fff;">
                      :<xsl:value-of select="age" />
                    </td>
                    <td style="border-right: none;background-color:#fff;">
                      xxx-xx-xxxx
                    </td>
                    <td style="background-color:#fff;" >
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                    </td>
                  </xsl:when>
                  <xsl:otherwise>
                    <td>
                      <xsl:value-of select="concat(name/first, ' ', name/middle, ', ', name/last)" />
                    </td>
                    <td>
                      <xsl:value-of select="name/gender" />
                    </td>
                    <td>
                      <xsl:value-of select="concat(dob/month,'/',dob/day,'/',dob/year)" />
                    </td>
                    <td>
                      :<xsl:value-of select="age" />
                    </td>
                    <td style="border-right: none;">
                      xxx-xx-xxxx
                    </td>
                    <td >
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                    </td>
                  </xsl:otherwise>
                </xsl:choose>
              </tr>
              <xsl:if test ="ReviewNote!=''">
                <tr>
                  <td >
                  </td>
                  <td colspan="5" style="width:97%;background-color:#FFFFE1" class="ReviewNote">
                    <xsl:value-of select="ReviewNote"/>
                  </td>
                  <td style="background-color:#FFFFE1">
                  </td>
                </tr>
              </xsl:if>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
      </tbody>
      <tfoot>
        <td ></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td style="border-right: none;"></td>
        <td ></td>
      </tfoot>
    </table>

    <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
      <thead>
        <tr>
          <th ></th>
          <th>Residencies</th>
          <th>Possible Relatives:</th>
          <th>Possible Associates:</th>
          <th>Possible Neighbors:</th>
          <th ></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td >
            <xsl:value-of select="(position())"/>
          </td>
          <td>
            <xsl:for-each select="response/individual/addresses/address">
              <xsl:for-each select="phones/phone[phone10!='']">
                <xsl:value-of select="phone10"/>
                <xsl:text> - </xsl:text>
                <xsl:value-of select="name-dual"/>
                <br />
              </xsl:for-each>
            </xsl:for-each>
            <xsl:if test="count(response/individual/addresses/address) = 0">
              [None Found]
            </xsl:if>
          </td>
          <td>
            <xsl:for-each select="response/individual/relatives/individual/addresses/address">
              <xsl:for-each select="phones/phone[phone10!='']">
                <xsl:value-of select="phone10"/>
                <xsl:text> - </xsl:text>
                <xsl:value-of select="name-dual"/>
                <br />
              </xsl:for-each>
            </xsl:for-each>
            <xsl:if test="count(response/individual/relatives/individual/addresses/address) = 0">
              [None Found]
            </xsl:if>
          </td>
          <td>
            <xsl:for-each select="response/individual/associates/individual/addresses/address">
              <xsl:for-each select="phones/phone[phone10!='']">
                <xsl:value-of select="phone10"/>
                <xsl:text> - </xsl:text>
                <xsl:value-of select="name-dual"/>
                <br />
              </xsl:for-each>
            </xsl:for-each>
            <xsl:if test="count(response/individual/associates/individual/addresses/address) = 0">
              [None Found]
            </xsl:if>
          </td>
          <td style="border-right: none;">
            <xsl:for-each select="response/individual/neighbors/neighborhood">
              <xsl:for-each select="subject-address/phones/phone[phone10!='']">
                <xsl:value-of select="phone10"/>
                <xsl:text> - </xsl:text>
                <xsl:value-of select="name-dual"/>
                <br />
              </xsl:for-each>
              <xsl:for-each select="addresses/address">
                <xsl:for-each select="phones/phone[phone10!='']">
                  <xsl:value-of select="phone10"/>
                  <xsl:text> - </xsl:text>
                  <xsl:value-of select="name-dual"/>
                  <br />
                </xsl:for-each>
              </xsl:for-each>
            </xsl:for-each>
            <xsl:if test="count(response/individual/neighbors/neighborhood) = 0">
              [None Found]
            </xsl:if>
          </td>
          <td >
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </td>
        </tr>
        <xsl:if test ="ReviewNote!=''">
          <tr>
            <td >
            </td>
            <td colspan="5" style="width:95%;background-color:#FFFFE1">
              <xsl:value-of select="ReviewNote"/>
            </td>
            <td>
            </td>
          </tr>
        </xsl:if>
      </tbody>
      <tfoot>
        <td ></td>
        <td></td>
        <td></td>
        <td></td>
        <td style="border-right: none;"></td>
        <td ></td>
      </tfoot>
    </table>

    <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
      <thead>
        <tr>
          <th ></th>
          <th colspan="100%">Verified Address(es):</th>
          <th ></th>
        </tr>
      </thead>
      <tbody>
        <xsl:for-each select="XML_INTERFACE/CREDITREPORT/response/individual/akas/identity">
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">
              <xsl:for-each select="//response/individual/addresses/address">
                <xsl:if test="verified = 'yes'">
                  <xsl:value-of select="residents/identity/name/first"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="residents/identity/name/middle"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="residents/identity/name/last"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="residents/identity/name/suffix"/>
                  <xsl:text> - </xsl:text>
                  <xsl:value-of select="street-number"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="street-name"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="street-suffix"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="unit-designation"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="unit-number"/>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="city"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="state"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="zip"/>
                  <xsl:text>-</xsl:text>
                  <xsl:value-of select="zip4"/>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="county"/>
                  <xsl:text> COUNTY</xsl:text>
                  <xsl:text> (</xsl:text>
                  <xsl:value-of select="concat(date-first/month,'/00/',date-first/year)"/>
                  <xsl:text> - </xsl:text>
                  <xsl:value-of select="concat(date-last/month,'/00/',date-last/year)"/>
                  <xsl:text>)</xsl:text>
                  <br />

                  <xsl:for-each select="//neighborhood/subject-address">
                    <xsl:if test="phones/phone/phone10">
                      <p class="indent header">
                        <xsl:text>Current phones listed at this address:</xsl:text>
                      </p>
                      <xsl:for-each select="phones/phone">
                        <p class="indent_more">
                          <xsl:value-of select="phone10"/>
                          <xsl:text> - </xsl:text>
                          <xsl:value-of select="name-dual"/>
                        </p>
                      </xsl:for-each>
                    </xsl:if>
                  </xsl:for-each>
                </xsl:if>
              </xsl:for-each>
              <xsl:if test="count(XML_INTERFACE/CREDITREPORT/response/individual/akas/identity) = 0">
                [None Found]
              </xsl:if>
            </td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
        </xsl:for-each>
      </tbody>
      <tfoot>
        <td></td>
        <td style="border-right: none;" colspan="100%"></td>
        <td ></td>
      </tfoot>
    </table>

    <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
      <thead>
        <tr>
          <th ></th>
          <th colspan="100%">Previous And Non-Verified Address(es):</th>
          <th ></th>
        </tr>
      </thead>
      <tbody>

        <xsl:for-each select="//response/individual/addresses/address">
          <xsl:choose>
            <xsl:when test ="@Deleted">

              <xsl:if test ="@Deleted!='1'">
                <xsl:choose>
                  <xsl:when test="(position() mod 2=0)">
                    <tr>
                      <td style="background-color:#fff;">
                        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                      </td>
                      <td colspan="100%" style="border-right: none;background-color:#fff;">

                        <xsl:if test="verified = 'no'">
                          <xsl:value-of select="residents/identity/name/first"/>
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="residents/identity/name/middle"/>
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="residents/identity/name/last"/>
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="residents/identity/name/suffix"/>
                          <xsl:text> - </xsl:text>
                          <xsl:value-of select="street-number"/>
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="street-name"/>
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="street-suffix"/>
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="unit-designation"/>
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="unit-number"/>
                          <xsl:text>, </xsl:text>
                          <xsl:value-of select="city"/>
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="state"/>
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="zip"/>
                          <xsl:text>-</xsl:text>
                          <xsl:value-of select="zip4"/>
                          <xsl:text>, </xsl:text>
                          <xsl:value-of select="county"/>
                          <xsl:text> COUNTY</xsl:text>
                          <xsl:text> (</xsl:text>
                          <xsl:value-of select="concat(date-first/month,'/00/',date-first/year)"/>
                          <xsl:text> - </xsl:text>
                          <xsl:value-of select="concat(date-last/month,'/00/',date-last/year)"/>
                          <xsl:text>)</xsl:text>
                          <br />

                          <xsl:for-each select="//neighborhood/subject-address">
                            <xsl:if test="phones/phone">
                              <p class="indent header">
                                <xsl:text>Current phones listed at this address:</xsl:text>
                              </p>
                              <xsl:for-each select="phones/phone">
                                <p class="indent_more">
                                  <xsl:value-of select="phone10"/>
                                  <xsl:text> - </xsl:text>
                                  <xsl:value-of select="name-dual"/>
                                </p>
                              </xsl:for-each>
                            </xsl:if>
                          </xsl:for-each>
                          <br />
                        </xsl:if>

                      </td>
                      <td  style="background-color:#fff;">
                        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                      </td>
                    </tr>
                  </xsl:when>
                  <xsl:otherwise>
                    <tr>
                      <td>
                        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                      </td>
                      <td colspan="100%" style="border-right: none;">
                        <xsl:if test="verified = 'no'">
                          <xsl:value-of select="residents/identity/name/first"/>
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="residents/identity/name/middle"/>
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="residents/identity/name/last"/>
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="residents/identity/name/suffix"/>
                          <xsl:text> - </xsl:text>
                          <xsl:value-of select="street-number"/>
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="street-name"/>
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="street-suffix"/>
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="unit-designation"/>
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="unit-number"/>
                          <xsl:text>, </xsl:text>
                          <xsl:value-of select="city"/>
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="state"/>
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="zip"/>
                          <xsl:text>-</xsl:text>
                          <xsl:value-of select="zip4"/>
                          <xsl:text>, </xsl:text>
                          <xsl:value-of select="county"/>
                          <xsl:text> COUNTY</xsl:text>
                          <xsl:text> (</xsl:text>
                          <xsl:value-of select="concat(date-first/month,'/00/',date-first/year)"/>
                          <xsl:text> - </xsl:text>
                          <xsl:value-of select="concat(date-last/month,'/00/',date-last/year)"/>
                          <xsl:text>)</xsl:text>
                          <br />

                          <xsl:for-each select="//neighborhood/subject-address">
                            <xsl:if test="phones/phone">
                              <p class="indent header">
                                <xsl:text>Current phones listed at this address:</xsl:text>
                              </p>
                              <xsl:for-each select="phones/phone">
                                <p class="indent_more">
                                  <xsl:value-of select="phone10"/>
                                  <xsl:text> - </xsl:text>
                                  <xsl:value-of select="name-dual"/>
                                </p>
                              </xsl:for-each>
                            </xsl:if>
                          </xsl:for-each>
                          <br />
                        </xsl:if>
                      </td>
                      <td>
                        <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                      </td>
                    </tr>
                  </xsl:otherwise>
                </xsl:choose>
                <xsl:if test ="ReviewNote!=''">
                  <tr>
                    <td >
                    </td>
                    <td colspan="5" style="width:97%;background-color:#FFFFE1"  class="ReviewNote">
                      <xsl:value-of select="ReviewNote"/>
                    </td>
                    <td  style="background-color:#FFFFE1">
                    </td>
                  </tr>
                </xsl:if>

              </xsl:if>
            </xsl:when>
            <xsl:otherwise>
              <xsl:choose>
                <xsl:when test="(position() mod 2=0)">
                  <tr>
                    <td  style="background-color:#fff;">
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                    </td>
                    <td colspan="100%" style="border-right: none;background-color:#fff;">

                      <xsl:if test="verified = 'no'">
                        <xsl:value-of select="residents/identity/name/first"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="residents/identity/name/middle"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="residents/identity/name/last"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="residents/identity/name/suffix"/>
                        <xsl:text> - </xsl:text>
                        <xsl:value-of select="street-number"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="street-name"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="street-suffix"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="unit-designation"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="unit-number"/>
                        <xsl:text>, </xsl:text>
                        <xsl:value-of select="city"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="state"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="zip"/>
                        <xsl:text>-</xsl:text>
                        <xsl:value-of select="zip4"/>
                        <xsl:text>, </xsl:text>
                        <xsl:value-of select="county"/>
                        <xsl:text> COUNTY</xsl:text>
                        <xsl:text> (</xsl:text>
                        <xsl:value-of select="concat(date-first/month,'/00/',date-first/year)"/>
                        <xsl:text> - </xsl:text>
                        <xsl:value-of select="concat(date-last/month,'/00/',date-last/year)"/>
                        <xsl:text>)</xsl:text>
                        <br />

                        <xsl:for-each select="//neighborhood/subject-address">
                          <xsl:if test="phones/phone">
                            <p class="indent header">
                              <xsl:text>Current phones listed at this address:</xsl:text>
                            </p>
                            <xsl:for-each select="phones/phone">
                              <p class="indent_more">
                                <xsl:value-of select="phone10"/>
                                <xsl:text> - </xsl:text>
                                <xsl:value-of select="name-dual"/>
                              </p>
                            </xsl:for-each>
                          </xsl:if>
                        </xsl:for-each>
                        <br />
                      </xsl:if>

                    </td>
                    <td  style="background-color:#fff;">
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                    </td>
                  </tr>
                </xsl:when>
                <xsl:otherwise>
                  <tr>
                    <td >
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                    </td>
                    <td colspan="100%" style="border-right: none;">
                      <xsl:if test="verified = 'no'">
                        <xsl:value-of select="residents/identity/name/first"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="residents/identity/name/middle"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="residents/identity/name/last"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="residents/identity/name/suffix"/>
                        <xsl:text> - </xsl:text>
                        <xsl:value-of select="street-number"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="street-name"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="street-suffix"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="unit-designation"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="unit-number"/>
                        <xsl:text>, </xsl:text>
                        <xsl:value-of select="city"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="state"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="zip"/>
                        <xsl:text>-</xsl:text>
                        <xsl:value-of select="zip4"/>
                        <xsl:text>, </xsl:text>
                        <xsl:value-of select="county"/>
                        <xsl:text> COUNTY</xsl:text>
                        <xsl:text> (</xsl:text>
                        <xsl:value-of select="concat(date-first/month,'/00/',date-first/year)"/>
                        <xsl:text> - </xsl:text>
                        <xsl:value-of select="concat(date-last/month,'/00/',date-last/year)"/>
                        <xsl:text>)</xsl:text>
                        <br />

                        <xsl:for-each select="//neighborhood/subject-address">
                          <xsl:if test="phones/phone">
                            <p class="indent header">
                              <xsl:text>Current phones listed at this address:</xsl:text>
                            </p>
                            <xsl:for-each select="phones/phone">
                              <p class="indent_more">
                                <xsl:value-of select="phone10"/>
                                <xsl:text> - </xsl:text>
                                <xsl:value-of select="name-dual"/>
                              </p>
                            </xsl:for-each>
                          </xsl:if>
                        </xsl:for-each>
                        <br />
                      </xsl:if>
                    </td>
                    <td >
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                    </td>
                  </tr>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:if test ="ReviewNote!=''">
                <tr>
                  <td>
                  </td>
                  <td colspan="5" style="width:97%;background-color:#FFFFE1"  class="ReviewNote">
                    <xsl:value-of select="ReviewNote"/>
                  </td>
                  <td style="background-color:#FFFFE1">
                  </td>
                </tr>
              </xsl:if>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>

      </tbody>
      <tfoot>
        <td ></td>
        <td style="border-right: none;" colspan="100%"></td>
        <td ></td>
      </tfoot>
    </table>

    <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
      <thead>
        <tr>
          <th ></th>
          <th colspan="100%">Possible Properties Owned by Subject:</th>
          <th ></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td >
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </td>
          <td colspan="100%" style="border-right: none;">
            <xsl:for-each select="//response/individual/addresses/address/properties/property">
              <xsl:value-of select="address/street-number"/>
              <xsl:text> </xsl:text>
              <xsl:value-of select="address/street-name"/>
              <xsl:text> </xsl:text>
              <xsl:value-of select="address/street-suffix"/>
              <xsl:text> </xsl:text>
              <xsl:value-of select="unit-designation"/>
              <xsl:text> </xsl:text>
              <xsl:value-of select="unit-number"/>
              <xsl:text>, </xsl:text>
              <xsl:value-of select="address/city"/>
              <xsl:text> </xsl:text>
              <xsl:value-of select="address/state"/>
              <xsl:text> </xsl:text>
              <xsl:value-of select="address/zip"/>
              <xsl:text>-</xsl:text>
              <xsl:value-of select="address/zip4"/>
              <xsl:text>, </xsl:text>
              <xsl:value-of select="address/county"/>
              <xsl:text> COUNTY</xsl:text>
            </xsl:for-each>
            <xsl:if test="count(//response/individual/addresses/address/properties/property) = 0">
              [None Found]
            </xsl:if>
          </td>
          <td >
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </td>
        </tr>
      </tbody>
      <tfoot>
        <td ></td>
        <td style="border-right: none;" colspan="100%"></td>
        <td ></td>
      </tfoot>
    </table>

    <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
      <thead>
        <tr>
          <th ></th>
          <th colspan="100%">Possible Associates:</th>
          <th ></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td >
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </td>
          <td colspan="100%" style="border-right: none;">
            <xsl:for-each select="//response/individual/associates/individual">
              <b>
                <u>
                  <xsl:value-of select="akas/identity/name/first"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="akas/identity/name/middle"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="akas/identity/name/last"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="akas/identity/name/suffix"/>
                  <xsl:if test="akas/identity/ssn != ''">
                    <xsl:text> </xsl:text>
                    xxx-xx-xxxx
                  </xsl:if>
                  <xsl:if test="dob != ''">
                    <xsl:text> DOB: </xsl:text>
                    <xsl:value-of select="akas/identity/dob"/>
                  </xsl:if>
                  <xsl:if test="age != ''">
                    <xsl:text> Age: </xsl:text>
                    <xsl:value-of select="akas/identity/age"/>
                  </xsl:if>
                </u>
              </b>
              <br />

              <b>
                <i>
                  <xsl:text>Names Associated with Associate:</xsl:text>
                </i>
              </b>
              <br />
              <ul>
                <xsl:for-each select="akas/identity">
                  <li>
                    <xsl:value-of select="name/first"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="name/middle"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="name/last"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="name/suffix"/>
                    <xsl:if test="ssn != ''">
                      <xsl:text> </xsl:text>
                      xxx-xx-xxxx
                    </xsl:if>
                    <xsl:if test="dob != ''">
                      <xsl:text> DOB: </xsl:text>
                      <xsl:value-of select="dob"/>
                    </xsl:if>
                    <xsl:if test="age != ''">
                      <xsl:text> Age: </xsl:text>
                      <xsl:value-of select="age"/>
                    </xsl:if>
                  </li>
                </xsl:for-each>
              </ul>

              <b>
                <i>
                  <xsl:text>Active Address(es):</xsl:text>
                </i>
              </b>
              <br />
              <ul>
                <xsl:for-each select="addresses/address">
                  <li>
                    <xsl:if test="verified = 'yes'">
                      <font class="fFont" color="blue" style="font-size:16px;">
                        <b>V</b>
                      </font>
                      <xsl:text/>
                    </xsl:if>
                    <xsl:if test="shared = 'yes'">
                      <font class="fFont" color="blue" style="font-size:16px;">
                        <b>S</b>
                      </font>
                      <xsl:text/>
                    </xsl:if>
                    <xsl:value-of select="street-number"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="street-name"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="street-suffix"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="unit-designation"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="unit-number"/>
                    <xsl:text>, </xsl:text>
                    <xsl:value-of select="city"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="state"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="zip"/>
                    <xsl:text>-</xsl:text>
                    <xsl:value-of select="zip4"/>
                    <xsl:text>, </xsl:text>
                    <xsl:value-of select="county"/>
                    <xsl:text> COUNTY</xsl:text>
                    <xsl:text> (</xsl:text>
                    <xsl:value-of select="concat(date-first/month,'/00/',date-first/year)"/>
                    <xsl:text> - </xsl:text>
                    <xsl:value-of select="concat(date-last/month,'/00/',date-last/year)"/>
                    <xsl:text>)</xsl:text>
                  </li>
                </xsl:for-each>
              </ul>
              <hr />
            </xsl:for-each>
            <xsl:if test="count(//response/individual/associates/individual) = 0">
              [None Found]
            </xsl:if>
          </td>
          <td >
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </td>
        </tr>
      </tbody>
      <tfoot>
        <td ></td>
        <td style="border-right: none;" colspan="100%"></td>
        <td ></td>
      </tfoot>
    </table>

    <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
      <thead>
        <tr>
          <th ></th>
          <th colspan="100%">Possible Relatives:</th>
          <th ></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td >
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </td>
          <td colspan="100%" style="border-right: none;">
            <xsl:for-each select="//response/individual/relatives/individual">

              <b>
                <u>
                  <xsl:value-of select="akas/identity/name/first"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="akas/identity/name/middle"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="akas/identity/name/last"/>
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="akas/identity/name/suffix"/>
                  <xsl:if test="akas/identity/ssn != ''">
                    <xsl:text> </xsl:text>
                    xxx-xx-xxxx
                  </xsl:if>
                  <xsl:if test="akas/identity/dob != ''">
                    <xsl:text> DOB: </xsl:text>
                    <xsl:value-of select="akas/identity/dob"/>
                  </xsl:if>
                  <xsl:if test="akas/identity/dod != ''">
                    <xsl:text> DOD: </xsl:text>
                    <xsl:value-of select="akas/identity/dod"/>
                  </xsl:if>
                  <xsl:if test="akas/identity/age != ''">
                    <xsl:text> Age: </xsl:text>
                    <xsl:value-of select="akas/identity/age"/>
                  </xsl:if>
                  <xsl:if test="akas/identity/age-at-death != ''">
                    <xsl:text> Age at Death: </xsl:text>
                    <xsl:value-of select="akas/identity/age-at-death"/>
                  </xsl:if>
                  <xsl:if test="akas/identity/dod != ''">
                    <xsl:text> (Born  </xsl:text>
                    <xsl:value-of select="akas/identity/dob/year"/>
                    <xsl:text> years ago)</xsl:text>
                  </xsl:if>
                </u>
              </b>
              <br />

              <b>
                <i>
                  <xsl:text>Names Associated with Relative:</xsl:text>
                </i>
              </b>
              <br />
              <ul>
                <xsl:for-each select="akas/identity">
                  <li>
                    <xsl:if test="age-at-death != ''">
                      <font class="fFont" color="red" style="font-size:16px;">
                        <b>D</b>
                      </font>
                      <xsl:text/>
                    </xsl:if>
                    <xsl:value-of select="name/first"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="name/middle"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="name/last"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="name/suffix"/>
                    <xsl:if test="ssn != ''">
                      <xsl:text> </xsl:text>
                      xxx-xx-xxxx
                    </xsl:if>
                    <xsl:if test="dob != ''">
                      <xsl:text> DOB: </xsl:text>
                      <xsl:value-of select="dob"/>
                    </xsl:if>
                    <xsl:if test="dod != ''">
                      <xsl:text> DOD: </xsl:text>
                      <xsl:value-of select="dod"/>
                    </xsl:if>
                    <xsl:if test="age != ''">
                      <xsl:text> Age: </xsl:text>
                      <xsl:value-of select="age"/>
                    </xsl:if>
                    <xsl:if test="age-at-death != ''">
                      <xsl:text> Age at Death: </xsl:text>
                      <xsl:value-of select="age-at-death"/>
                    </xsl:if>
                    <xsl:if test="dod != ''">
                      <xsl:text> (Born  </xsl:text>
                      <xsl:value-of select="dob/year"/>
                      <xsl:text> years ago)</xsl:text>
                    </xsl:if>

                    <xsl:if test="age-at-death != ''">
                      <font class="fFont" color="red" style="font-size:16px;">
                        <b>D</b>
                      </font>
                      <xsl:text/>
                    </xsl:if>
                    <xsl:value-of select="name/first"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="name/middle"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="name/last"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="name/suffix"/>
                    <xsl:if test="ssn != ''">
                      xxx-xx-xxxx
                    </xsl:if>
                    <xsl:if test="dob != ''">
                      <xsl:text> DOB: </xsl:text>
                      <xsl:value-of select="dob"/>
                    </xsl:if>
                    <xsl:if test="dod != ''">
                      <xsl:text> DOD: </xsl:text>
                      <xsl:value-of select="dod"/>
                    </xsl:if>
                    <xsl:if test="age != ''">
                      <xsl:text> Age: </xsl:text>
                      <xsl:value-of select="age"/>
                    </xsl:if>
                    <xsl:if test="age-at-death != ''">
                      <xsl:text> Age at Death: </xsl:text>
                      <xsl:value-of select="age-at-death"/>
                    </xsl:if>
                    <xsl:if test="dod != ''">
                      <xsl:text> (Born  </xsl:text>
                      <xsl:value-of select="dod/year"/>
                      <xsl:text> years ago)</xsl:text>
                    </xsl:if>
                  </li>
                </xsl:for-each>
              </ul>

              <b>
                <i>
                  <xsl:text>Active Address(es):</xsl:text>
                </i>
              </b>
              <br />
              <ul>
                <xsl:for-each select="addresses/address">
                  <li>
                    <xsl:if test="verified = 'yes'">
                      <font class="fFont" color="blue" style="font-size:16px;">
                        <b>V</b>
                      </font>
                      <xsl:text/>
                    </xsl:if>
                    <xsl:if test="shared = 'yes'">
                      <font class="fFont" color="blue" style="font-size:16px;">
                        <b>S</b>
                      </font>
                      <xsl:text/>
                    </xsl:if>
                    <xsl:value-of select="street-number"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="street-name"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="street-suffix"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="unit-designation"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="unit-number"/>
                    <xsl:text>, </xsl:text>
                    <xsl:value-of select="city"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="state"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="zip"/>
                    <xsl:text>-</xsl:text>
                    <xsl:value-of select="zip4"/>
                    <xsl:text>, </xsl:text>
                    <xsl:value-of select="county"/>
                    <xsl:text> COUNTY</xsl:text>
                    <xsl:text> (</xsl:text>
                    <xsl:value-of select="concat(date-first/month,'/00/',date-first/year)"/>
                    <xsl:text> - </xsl:text>
                    <xsl:value-of select="concat(date-last/month,'/00/',date-last/year)"/>
                    <xsl:text>)</xsl:text>
                  </li>
                </xsl:for-each>
              </ul>
              <hr width="60%" />
            </xsl:for-each>
            <xsl:if test="count(//response/individual/relatives/individual) = 0">
              [None Found]
            </xsl:if>
          </td>
          <td >
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </td>
        </tr>
      </tbody>
      <tfoot>
        <td ></td>
        <td style="border-right: none;" colspan="100%"></td>
        <td ></td>
      </tfoot>
    </table>


    <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
      <thead>
        <tr>
          <th ></th>
          <th colspan="100%">Neighbors:</th>
          <th ></th>
        </tr>
      </thead>
      <tbody>
        <xsl:for-each select="//response/individual/neighbors/neighborhood">
          <xsl:choose>
            <xsl:when test ="@Deleted">

              <xsl:if test ="@Deleted!='1'">
                <tr>
                  <td >
                    <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                  </td>
                  <td colspan="100%" style="border-right: none;">
                    <p class="header">
                      <xsl:text>Neighborhood:</xsl:text>
                    </p>
                    <xsl:for-each select="subject-address">
                      <p class="indent">
                        <xsl:value-of select="street-number"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="street-name"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="street-suffix"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="unit-designation"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="unit-number"/>
                        <xsl:text>, </xsl:text>
                        <xsl:value-of select="city"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="state"/>
                        <xsl:text> </xsl:text>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="zip"/>
                        <xsl:text>-</xsl:text>
                        <xsl:value-of select="zip4"/>
                        <xsl:text>, </xsl:text>
                        <xsl:value-of select="county"/>
                        <xsl:text> COUNTY</xsl:text>
                        <xsl:text> (</xsl:text>
                        <xsl:value-of select="concat(date-first/month,'/00/',date-first/year)"/>
                        <xsl:text> - </xsl:text>
                        <xsl:value-of select="concat(date-last/month,'/00/',date-last/year)"/>
                        <xsl:text>)</xsl:text>

                        <xsl:value-of select="name-dual"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="phone10"/>
                      </p>

                      <p class="indent_more header">
                        <xsl:text>Current phones listed at this address:</xsl:text>
                      </p>

                      <xsl:for-each select="phones/phone">
                        <p class="indent_more_more">
                          <xsl:value-of select="name-dual"/>
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="phone10"/>
                        </p>
                      </xsl:for-each>
                    </xsl:for-each>

                    <p class="indent header">
                      <xsl:text>Address(es):</xsl:text>
                    </p>
                    <xsl:for-each select="addresses/address">
                      <p class="indent_more">
                        <xsl:if test="verified = 'yes'">
                          <font class="fFont" color="blue" style="font-size:16px;">
                            <b>V</b>
                          </font>
                          <xsl:text/>
                        </xsl:if>
                        <xsl:if test="shared = 'yes'">
                          <font class="fFont" color="blue" style="font-size:16px;">
                            <b>S</b>
                          </font>
                          <xsl:text/>
                        </xsl:if>
                        <xsl:value-of select="street-number"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="street-name"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="street-suffix"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="unit-designation"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="unit-number"/>
                        <xsl:text>, </xsl:text>
                        <xsl:value-of select="city"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="state"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="zip"/>
                        <xsl:text>-</xsl:text>
                        <xsl:value-of select="zip4"/>
                        <xsl:text>, </xsl:text>
                        <xsl:value-of select="county"/>
                        <xsl:text> COUNTY </xsl:text>

                        <xsl:value-of select="phones/phone/name-dual"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="phones/phone/phone10"/>
                      </p>
                      <p class="indent_more header">
                        <xsl:text>Current phones listed at this address:</xsl:text>
                      </p>
                      <xsl:for-each select="phones/phone">
                        <p class="indent_more_more">
                          <xsl:value-of select="name-dual"/>
                          <xsl:text> </xsl:text>
                          <xsl:value-of select="phone10"/>
                        </p>
                      </xsl:for-each>
                    </xsl:for-each>
                  </td>
                  <td >
                    <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                  </td>
                </tr>
                <xsl:if test ="ReviewNote!=''">
                  <tr>
                    <td >
                    </td>
                    <td colspan="5" style="width:95%;background-color:#FFFFE1">
                      <xsl:value-of select="ReviewNote"/>
                    </td>
                    <td style="background-color:#FFFFE1">
                    </td>
                  </tr>
                </xsl:if>
              </xsl:if>
            </xsl:when>
            <xsl:otherwise>
              <tr>
                <td >
                  <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                </td>
                <td colspan="100%" style="border-right: none;">
                  <p class="header">
                    <xsl:text>Neighborhood:</xsl:text>
                  </p>
                  <xsl:for-each select="subject-address">
                    <p class="indent">
                      <xsl:value-of select="street-number"/>
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="street-name"/>
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="street-suffix"/>
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="unit-designation"/>
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="unit-number"/>
                      <xsl:text>, </xsl:text>
                      <xsl:value-of select="city"/>
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="state"/>
                      <xsl:text> </xsl:text>
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="zip"/>
                      <xsl:text>-</xsl:text>
                      <xsl:value-of select="zip4"/>
                      <xsl:text>, </xsl:text>
                      <xsl:value-of select="county"/>
                      <xsl:text> COUNTY</xsl:text>
                      <xsl:text> (</xsl:text>
                      <xsl:value-of select="concat(date-first/month,'/00/',date-first/year)"/>
                      <xsl:text> - </xsl:text>
                      <xsl:value-of select="concat(date-last/month,'/00/',date-last/year)"/>
                      <xsl:text>)</xsl:text>

                      <xsl:value-of select="name-dual"/>
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="phone10"/>
                    </p>

                    <p class="indent_more header">
                      <xsl:text>Current phones listed at this address:</xsl:text>
                    </p>

                    <xsl:for-each select="phones/phone">
                      <p class="indent_more_more">
                        <xsl:value-of select="name-dual"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="phone10"/>
                      </p>
                    </xsl:for-each>
                  </xsl:for-each>

                  <p class="indent header">
                    <xsl:text>Address(es):</xsl:text>
                  </p>
                  <xsl:for-each select="addresses/address">
                    <p class="indent_more">
                      <xsl:if test="verified = 'yes'">
                        <font class="fFont" color="blue" style="font-size:16px;">
                          <b>V</b>
                        </font>
                        <xsl:text/>
                      </xsl:if>
                      <xsl:if test="shared = 'yes'">
                        <font class="fFont" color="blue" style="font-size:16px;">
                          <b>S</b>
                        </font>
                        <xsl:text/>
                      </xsl:if>
                      <xsl:value-of select="street-number"/>
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="street-name"/>
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="street-suffix"/>
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="unit-designation"/>
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="unit-number"/>
                      <xsl:text>, </xsl:text>
                      <xsl:value-of select="city"/>
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="state"/>
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="zip"/>
                      <xsl:text>-</xsl:text>
                      <xsl:value-of select="zip4"/>
                      <xsl:text>, </xsl:text>
                      <xsl:value-of select="county"/>
                      <xsl:text> COUNTY </xsl:text>

                      <xsl:value-of select="phones/phone/name-dual"/>
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="phones/phone/phone10"/>
                    </p>
                    <p class="indent_more header">
                      <xsl:text>Current phones listed at this address:</xsl:text>
                    </p>
                    <xsl:for-each select="phones/phone">
                      <p class="indent_more_more">
                        <xsl:value-of select="name-dual"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="phone10"/>
                      </p>
                    </xsl:for-each>
                  </xsl:for-each>
                </td>
                <td >
                  <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                </td>
              </tr>
              <xsl:if test ="ReviewNote!=''">
                <tr>
                  <td>
                  </td>
                  <td colspan="5" style="width:95%;background-color:#FFFFE1">
                    <xsl:value-of select="ReviewNote"/>
                  </td>
                  <td  style="background-color:#FFFFE1">
                  </td>
                </tr>
              </xsl:if>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
        <xsl:if test="count(//response/individual/neighbors/neighborhood) = 0">
          <tr>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
            <td colspan="100%" style="border-right: none;">
              [None Found]
            </td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
        </xsl:if>
      </tbody>
      <tfoot>
        <td ></td>
        <td style="border-right: none;" colspan="100%"></td>
        <td></td>
      </tfoot>
    </table>

    <xsl:if test="XML_INTERFACE/CREDITREPORT/OBJECTS/BUREAUERROR = 'True' or XML_INTERFACE/CREDITREPORT/cp_xmldirect/general_error !='' ">
      <table id="peopleTable" class="reporttable" border="0" cellpadding="0"  style="width:100%;" cellspacing="0">
        <thead>
          <tr>
            <th ></th>
            <th colspan="5">Search Results</th>
            <th ></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td >
              <xsl:value-of select="(position() )"/>
            </td>
            <td colspan="5">

              <xsl:value-of select="substring-after(XML_INTERFACE/CREDITREPORT/HTMLREPORT/MESSAGE, 'body&gt;')" disable-output-escaping="yes" />

            </td>
            <td >
              <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
            </td>
          </tr>
        </tbody>
        <tfoot>
          <td ></td>
          <td colspan="5"></td>
          <td></td>
        </tfoot>
      </table>
    </xsl:if>
    <xsl:if test="(count(response/individual/addresses/address) = 0) and (count(XML_INTERFACE/CREDITREPORT/response/individual/akas/identity) = 0) and (count(//response/individual/addresses/address/properties/property) = 0) and (count(//response/individual/associates/individual) = 0) and (count(//response/individual/relatives/individual) = 0) and (count(//response/individual/neighbors/neighborhood) = 0)">
      <table id="peopleTable" class="reporttable" border="0" cellpadding="0"  style="width:100%;" cellspacing="0">
        <tr>
          <td >
          </td>
          <td colspan="100%" >
            <div id="txtReviewNoteNRF_TDR" name="txtNote_{(position())}" class="ReviewNote" style="width:100%;background-color:#FFFFE1;">
            </div>
          </td>
          <td  >
            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
          </td>
        </tr>
      </table>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
