<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" encoding="utf-8"/>

    <xsl:template match="/">
      <div class="people-search-wrap">
            <div class="sectionsummary" style="text-align:center">
                <h3>
                    (<xsl:value-of select="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/rows_returned" />) Results Bankruptcy Report
                </h3>
                <br />
            </div>

            <xsl:if test="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/rows_returned = '0'">
                <table id="peopleTable" class="people-search" border="0" style="width:100%" cellpadding="0" cellspacing="0">
                    <thead>
                      <tr class="search-heading">
                        <td class="width_10"></td>
                        <td style="text-align:left;font-size:20px;font-weight: bold;">
                          Search Information
                        </td>
                        <td ></td>
                      </tr>
                    </thead>
                    <tbody>
                        <tr>
                          <td class="width_10">
                                <!--<xsl:value-of select="(position() )"/>-->
                            </td>
                            <td >
                                No Records Found.
                            </td>
                            <td >
                                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                            </td>
                        </tr>
                    </tbody>
                 
                </table>
            </xsl:if>

            <xsl:if test="XML_INTERFACE/CREDITREPORT/cp_xmldirect/search_response/rows_returned != '0'">
                <table id="peopleTable" class="people-search" border="0" cellpadding="0"  style="width:100%;" cellspacing="0">
                    <thead>
                      
                      <tr class="search-heading">
                        <td class="width_10"></td>
                        <td style="text-align:left;font-size:20px;font-weight: bold;">
                          Search Results
                        </td>
                        <td></td>
                      </tr>
                    </thead>
                    <tbody>
                        <tr>
                          <td class="width_10">
                         
                            </td>
                            <td>
                                 <xsl:value-of select="(position() )"/>
                                <xsl:value-of select="substring-after(XML_INTERFACE/CREDITREPORT/HTMLREPORT, '&#60;&#47;table&#62;')" disable-output-escaping="yes" />

                            </td>
                            <td >
                                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                            </td>
                        </tr>
                    </tbody>
                   
                </table>
            </xsl:if>

            <xsl:if test="XML_INTERFACE/CREDITREPORT/OBJECTS/BUREAUERROR = 'True' or XML_INTERFACE/CREDITREPORT/cp_xmldirect/general_error !='' ">
                <table id="peopleTable" class="people-search" border="0" cellpadding="0"  style="width:100%;" cellspacing="0">
                    <thead>
                      <tr class="search-heading">
                        <td class="width_10"></td>
                        <td style="text-align:left;font-size:20px;font-weight: bold;">
                          Search Results
                        </td>
                        <td></td>
                      </tr>
                    </thead>
                    <tbody>
                        <tr>
                          <td class="width_10">
                                
                            </td>
                            <td >
                               <xsl:value-of select="(position() )"/>
                                <xsl:value-of select="substring-after(XML_INTERFACE/CREDITREPORT/HTMLREPORT/MESSAGE, 'body&gt;')" disable-output-escaping="yes" />

                            </td>
                            <td >
                                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                            </td>
                        </tr>
                    </tbody>
                
                </table>
            </xsl:if>
       
        <div class="sectionsummary">
            <h3 style="line-height: 1em;">
                Bankruptcy Records cover all bankruptcy districts in the<br />
                United States including voluntarily or involuntarily Chapter<br />
                7, 11, 12, and 13 petition filings.
            </h3>
        </div>
        </div >
    </xsl:template>
</xsl:stylesheet>