<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:a="http://ns.hr-xml.org/2004-08-02">
  <xsl:output method="html"/>
  <xsl:template match="/">

    <xsl:variable name="fullname" select="MVR/FullName"/>

    <xsl:choose>
      <xsl:when test="$fullname='DRIVER NOT FOUND'">
        <div style="width: 100%; text-align: center; font-size: 16px; font-weight: bold;">
          MVR Search (Records not found for <xsl:value-of select="MVR/LicenseNumber" />)
        </div>
      </xsl:when>
      <xsl:when test="MVR/RequestError/ErrorDescription">
        <div style="width: 100%; text-align: center; font-size: 16px; font-weight: bold;">
          MESSAGE FROM VENDOR: <xsl:value-of select="MVR/RequestError/ErrorDescription" />
        </div>
      </xsl:when>
      <xsl:otherwise>
        <div class="reportName">
          <h3>MVR Search</h3>
          (Records Found for <xsl:value-of select="MVR/LicenseNumber" />)
        </div>
        <pre>
          <div style="font-family: Arial; font-size: 12px;">
            <table border="1" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td bgcolor="#CCCCCC">
                  <b>
                    <i>LICENSE NUMBER</i>
                  </b>
                </td>
                <td bgcolor="#CCCCCC">
                  <b>
                    <i>D.O.B.</i>
                  </b>
                </td>
                <td bgcolor="#CCCCCC">
                  <b>
                    <i>SEX</i>
                  </b>
                </td>
                <td bgcolor="#CCCCCC">
                  <b>
                    <i>HGT</i>
                  </b>
                </td>
                <td bgcolor="#CCCCCC">
                  <b>
                    <i>WGT</i>
                  </b>
                </td>
                <td bgcolor="#CCCCCC">
                  <b>
                    <i>EYES</i>
                  </b>
                </td>
                <td bgcolor="#CCCCCC">
                  <b>
                    <i>HAIR</i>
                  </b>
                </td>
                <td bgcolor="#CCCCCC">
                  <b>
                    <i>RACE</i>
                  </b>
                </td>
                <td bgcolor="#CCCCCC">
                  <b>
                    <i>SOC.SEC</i>
                  </b>
                </td>
                <td bgcolor="#CCCCCC">
                  <b>
                    <i>DONOR</i>
                  </b>
                </td>
              </tr>
              <tr>
                <td>
                  <xsl:value-of select="MVR/LicenseNumber" />
                </td>
                <td>
                  <xsl:value-of select="MVR/DateOfBirth" />
                </td>
                <td>
                  <xsl:value-of select="MVR/Gender" />
                </td>
                <td>
                  <xsl:value-of select="MVR/Height" />
                </td>
                <td>
                  <xsl:value-of select="MVR/Weight" />
                </td>
                <td>
                  <xsl:value-of select="MVR/EyeColor" />
                </td>
                <td>
                  <xsl:value-of select="MVR/HairColor" />
                </td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </table>

            <table border='1' width='100%' cellspacing='0' cellpadding='0'>
              <tr>
                <td bgcolor='#CCCCCC'>
                  <b>
                    <i>CLASS</i>
                  </b>
                </td>
                <td bgcolor='#CCCCCC'>
                  <b>
                    <i>ORIG. ISSUED</i>
                  </b>
                </td>
                <td bgcolor='#CCCCCC'>
                  <b>
                    <i>ISSUED</i>
                  </b>
                </td>
                <td bgcolor='#CCCCCC'>
                  <b>
                    <i>EXPIRES</i>
                  </b>
                </td>
                <td bgcolor='#CCCCCC'>
                  <b>
                    <i>LIC TYPE</i>
                  </b>
                </td>
              </tr>
              <tr>
                <td>
                  <xsl:value-of select="MVR/Class" />-<xsl:value-of select="MVR/ClassDescription" />
                </td>
                <td></td>
                <td>
                  <xsl:value-of select="MVR/DateIssued" />
                </td>
                <td>
                  <xsl:value-of select="MVR/Expiration" />
                </td>
                <td>
                  <xsl:value-of select="MVR/LicenseType" />
                  <xsl:value-of select="MVR/LicenseTypeDescription" />
                </td>
              </tr>
              <tr>
                <td bgcolor='#CCCCCC'>
                  <b>
                    <i>STATUS</i>
                  </b>
                </td>
                <td colspan='2' bgcolor='#CCCCCC'>
                  <b>
                    <i>RESTRICTIONS</i>
                  </b>
                </td>
                <td colspan='2' bgcolor='#CCCCCC'>
                  <b>
                    <i>ENDORSEMENTS</i>
                  </b>
                </td>
              </tr>
              <tr>
                <td>
                  <xsl:value-of select="MVR/LicenseStatus" />
                </td>
                <td colspan='2'></td>
                <td colspan='2'></td>
              </tr>
            </table>
            <table border="1" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td bgcolor="#CCCCCC">
                  <b>
                    <i>REINST DATE</i>
                  </b>
                </td>
                <td bgcolor="#CCCCCC">
                  <b>
                    <i>PRIOR STATE</i>
                  </b>
                </td>
                <td bgcolor="#CCCCCC">
                  <b>
                    <i>PRIOR DL#</i>
                  </b>
                </td>
                <td bgcolor="#CCCCCC">
                  <b>
                    <i>PRIOR DL STATUS</i>
                  </b>
                </td>
                <td bgcolor="#CCCCCC">
                  <b>
                    <i>C.D.L.ISSUED</i>
                  </b>
                </td>
                <td bgcolor="#CCCCCC">
                  <b>
                    <i>C.D.L.STATUS</i>
                  </b>
                </td>
              </tr>
              <tr>
                <td></td>
                <td>
                  <xsl:value-of select="MVR/PriorState" />
                </td>
                <td>
                  <xsl:value-of select="MVR/PriorDlNumber" />
                </td>
                <td></td>
                <td></td>
                <td>
                  <xsl:value-of select="MVR/CdlStatus" />
                </td>
              </tr>
            </table>
            <table border="1" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td bgcolor="#CCCCCC">
                  <b>
                    <i>SECONDARY LIC.</i>
                  </b>
                </td>
                <td bgcolor="#CCCCCC">
                  <b>
                    <i>NON-RESIDENT MILITARY</i>
                  </b>
                </td>
                <td bgcolor="#CCCCCC">
                  <b>
                    <i>OTHER STATE LIC.</i>
                  </b>
                </td>
                <td bgcolor="#CCCCCC">
                  <b>
                    <i>OTHER STATE</i>
                  </b>
                </td>
                <td bgcolor="#CCCCCC">
                  <b>
                    <i>POINTS</i>
                  </b>
                </td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </table>
            <table border="1" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td bgcolor="#CCCCCC" align="center">
                  <b>MISCELLANEOUS AND STATE SPECIFIC INFORMATION</b>
                </td>
              </tr>
              <tr>
                <td bgcolor="#FFFFFF">

                  <pre>
                    <xsl:choose>
                      <xsl:when test="/MVR/Messages">
                        <xsl:for-each select="MVR/Messages/Message">
                          <xsl:value-of select="Text" />
                          <br/>
                        </xsl:for-each>
                      </xsl:when>
                      <xsl:otherwise>None</xsl:otherwise>
                    </xsl:choose>
                  </pre>
                </td>
              </tr>
              <tr>
                <td bgcolor="#CCCCCC" align="center">
                  <b>DRIVING RECORD HISTORY</b>
                </td>
              </tr>
            </table>
            <pre>
              TYPE  VIOL/SUS   CONV/REI  HISTORY ENTRY PTS
              ---- ---------- ---------- --------------------------------------------------
              <xsl:choose>
                <xsl:when test="/MVR/Disqualifications">
                  <xsl:for-each select="MVR/Disqualifications/Disqualification">
                    DISQ&#160;<xsl:value-of select="IssueDate" />&#160;<xsl:value-of select="Description" />
                    Event Type...............: <xsl:value-of select="EventType" />
                    Mail Date...............: <xsl:value-of select="MailDate" />
                    City/Location...............: <xsl:value-of select="CityLocation" />
                    <br/>
                  </xsl:for-each>
                </xsl:when>
              </xsl:choose>

              <xsl:choose>
                <xsl:when test="/MVR/Suspensions">
                  <xsl:for-each select="MVR/Suspensions/Suspension">
                    SUSP&#160;<xsl:value-of select="IssueDate" />&#160;<xsl:value-of select="Incident/Description" />
                    Event Type...............: <xsl:value-of select="EventType" />
                    Mail Date...............: <xsl:value-of select="MailDate" />
                    City/Location...............: <xsl:value-of select="CityLocation" />
                    ACD...............: <xsl:value-of select="ACD" />
                    <br/>
                  </xsl:for-each>
                </xsl:when>
              </xsl:choose>


              <xsl:choose>
                <xsl:when test="/MVR/Actions">
                  <xsl:for-each select="MVR/Actions/Action">
                    ACTI&#160;<xsl:value-of select="IssueDate" />&#160;<xsl:value-of select="ConvDate" />&#160;<xsl:value-of select="Incident/Description" />
                    Jurisdiction...............: <xsl:value-of select="Jurisdiction" />
                    Event Type...............: <xsl:value-of select="EventType" />
                    Mail Date...............: <xsl:value-of select="MailDate" />
                    State Code...............: <xsl:value-of select="Incident/StateCode" />
                    Miscellaneous...............: <xsl:value-of select="Miscellaneous" />
                    <br/>
                  </xsl:for-each>
                </xsl:when>
              </xsl:choose>

              <xsl:choose>
                <xsl:when test="/MVR/IDCards">
                  <xsl:for-each select="MVR/IDCards/IDCard">
                    IDCD&#160;<xsl:value-of select="IssueDate" />&#160;<xsl:value-of select="ExpirationDate" />&#160;<xsl:value-of select="Description" />
                    Event Type...............: <xsl:value-of select="EventType" />
                    Status Code...............: <xsl:value-of select="StatusCode" />
                    Card Type...............: <xsl:value-of select="CardType" />
                    <br/>
                  </xsl:for-each>
                </xsl:when>
              </xsl:choose>

              <xsl:choose>
                <xsl:when test="/MVR/Violations">
                  <xsl:for-each select="MVR/Violations/Violation">
                    VIOL&#160;<xsl:value-of select="IssueDate" />&#160;<xsl:value-of select="Incident/Description" />
                    Event Type...............: <xsl:value-of select="EventType" />
                    Issue Date...............: <xsl:value-of select="IssueDate" />
                    ConvictionDate...............: <xsl:value-of select="ConvictionDate" />
                    HISTORY ENTRY...............: <xsl:value-of select="Incident/Description" />
                    County...............: <xsl:value-of select="County" />
                    State Code...............: <xsl:value-of select="Incident/StateCode" />
                    ACD...............: <xsl:value-of select="Incident/ACD" />
                    City/Location...............: <xsl:value-of select="CityLocation" />
                    Penalty...............: <xsl:value-of select="Penalty" />
                    Disposition...............:<xsl:value-of select="Disposition" />
                    <br/>
                  </xsl:for-each>
                </xsl:when>
              </xsl:choose>
              <xsl:choose>
                <xsl:when test="/MVR/Permits">
                  <xsl:for-each select="MVR/Permits/Permit">
                    PERM&#160;<xsl:value-of select="IssueDate" />&#160;<xsl:value-of select="ConvDate" />&#160;<xsl:value-of select="Description" />
                    Permit Class...............: <xsl:value-of select="PermitClass" />
                    Permit Status...............: <xsl:value-of select="PermitStatus" />
                    Permit Restric...............: <xsl:value-of select="PermitRestric" />
                    <br/>
                  </xsl:for-each>
                </xsl:when>
              </xsl:choose>
            </pre>
          </div>
        </pre>
        <br />
        <div style="font-size: 16px;">
          <center>** END OF RECORD ***</center>
        </div>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
