﻿<?xml version="1.0" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output indent="yes" method="html"/>
  <xsl:template match="/">
    <html>
      <head>
        <style type="text/css">
          body{font-family: Lucida Grande,Arial,Helvetica,sans-serif;font-size: 14px;font-weight: normal;}

          .borders {border-left:1px solid #000000;
          border-right:1px solid #000000;
          border-top:1px solid #000000;
          border-bottom:1px solid #000000;}

          .tbl{Border:1px Solid #000;font-size:16pt;font-weight:bold;}
          .titletxt{font-size:12pt;font-weight:bold;}
        </style>

      </head>
      <body bgcolor="#ffffff">
        <center>
          <table width="100%" border="0">
            <tr >
              <td align="center"  colspan="2" width="670">
                <table width="99%" class="tbl" bgcolor="#efefef">
                  <tr>
                    <td align="center">
                      CONFIDENTIAL
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td style="height:10px" colspan="2"></td>
            </tr>
            <tr>
              <td align="center" colspan="2" >
                <table width="99%" style="Border:1px solid #000;">
                  <tr>
                    <td align="left" class="titletxt">
                      Background Verification Report
                    </td>
                    <td>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td style="height:10px" colspan="2"></td>
            </tr>
            <xsl:for-each select="BackgroundReports/BackgroundReportPackage">
              <tr>
                <td colspan="2" align="center">
                  <xsl:apply-templates select="BackgroundReports/BackgroundReportPackage/ScreeningStatus"/>
                </td>
              </tr>
              <tr>
                <td colspan="2" align="center">
                  <xsl:apply-templates select="BackgroundReports/BackgroundReportPackage/ScreeningsSummary"/>
                </td>
              </tr>
              <tr>
                <td colspan="2">
                  <xsl:apply-templates/>
                </td>
              </tr>
              <tr>
                <td align="center" colspan="2" >
                  <span style="color:red;font-weight:600;font-size:14px;">**** End Of Report ****</span>
                </td>
              </tr>
              <tr>
                <td colspan="2" align="left">
                  <p style="text-align: justify; padding-left: 5px; padding-right: 5px;">
                    <b>Workers Compensation Disclaimer:</b> We will provide you available workers comp records ordered after we have provided other reports on the subject.  The Americans with Disabilities Act provides that any medically related information may only be requested from an applicant 'post offer' and should be the last category of information an employer reviews on an applicant.  Therefore we will delay the delivery of workers comp reports until after you have received our other reports.  If for some reason you have not reviewed the other reports by the time we provide a workers comp report you are requested for your legal compliance with the ADA not to open that report until you have reviewed the other reports and the applicant is still qualified to be hired.
                  </p>
                </td>
              </tr>
            </xsl:for-each>
          </table>
        </center>
      </body>

    </html>

  </xsl:template>

  <!--for Screening Status details-->
  <xsl:template match="BackgroundReports/BackgroundReportPackage/ScreeningStatus">
  </xsl:template>

  <!--for personal details-->
  <xsl:template match="BackgroundReports/BackgroundReportPackage/ScreeningsSummary">
    <table border="0" width="99%" cellpadding="0" cellspacing="0" style="Border:1px solid #000;" align="center">
      <tr>
        <td align="left" style="width:25%">
          <xsl:text xml:space="preserve">&#x20;&#x20;&#x20;</xsl:text>  Requestor :
        </td>
        <td align="left" style="width:25%">
          <xsl:apply-templates select="PersonalData/RequestIdData/Requestor"/>
        </td>
        <td align="left" style="width:25%">
          <xsl:text xml:space="preserve">&#x20;&#x20;&#x20;</xsl:text> Position Applied :
        </td>
        <td align="left" style="width:25%">
          <xsl:text xml:space="preserve">&#x20;&#x20;&#x20;</xsl:text>
          <xsl:apply-templates select="PersonalData/RequestIdData/Reference2"/>
        </td>
      </tr>
      <tr>
        <td align="left" style="width:25%">
          <xsl:text xml:space="preserve">&#x20;&#x20;&#x20;</xsl:text> Subject :
        </td>
        <td align="left" style="width:25%">
          <xsl:apply-templates select="PersonalData/PersonName/GivenName"/>
          <xsl:text xml:space="preserve">&#x20;&#x20;&#x20;</xsl:text>
          <xsl:apply-templates select="PersonalData/PersonName/MiddleName"/>
          <xsl:text xml:space="preserve">&#x20;&#x20;&#x20;</xsl:text>
          <xsl:apply-templates select="PersonalData/PersonName/FamilyName"/>
        </td>
        <td align="left" style="width:25%">
          <xsl:text xml:space="preserve">&#x20;&#x20;&#x20;</xsl:text> SS# :
        </td>
        <td align="left" style="width:25%">
          <xsl:text xml:space="preserve">&#x20;&#x20;&#x20;</xsl:text>
          <xsl:apply-templates select="PersonalData/DemographicDetail/Ssn"/>
        </td>
      </tr>
      <tr>
        <td align="left" style="width:25%">
          <xsl:text xml:space="preserve">&#x20;&#x20;&#x20;</xsl:text> DOB :
        </td>
        <td align="left">
          <xsl:apply-templates select="PersonalData/DemographicDetail/DateOfBirth"/>
        </td>
      </tr>
    </table>

  </xsl:template>


  <xsl:template match="BackgroundReports">
    <xsl:apply-templates select="BackgroundReportPackage/Screenings/Screening"/>

  </xsl:template>


  <!--for report search-->
  <xsl:template match="BackgroundReportPackage/Screenings/Screening">

    <xsl:choose>
      <xsl:when test="@type='workcomp'">
        <xsl:apply-templates select="ScreeningResults" mode="WCV"/>
      </xsl:when>
      <xsl:otherwise>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

  <!--for Workers Compensation Verification-->
  <xsl:template match="ScreeningResults" mode="WCV">
    <table width="100%" >
      <tr>
        <td colspan="2">
          <table width="100%">
            <tr bgcolor="#efefef">
              <td class="tbl" align="center" colspan="2">
                Workers Compensation Verification
              </td>
            </tr>
            <tr>
              <td colspan="2" align="left">

                <pre>

                  <xsl:value-of select="Text"/>

                </pre>

              </td>
            </tr>

          </table>
        </td>
      </tr>
    </table>
  </xsl:template>

</xsl:stylesheet>