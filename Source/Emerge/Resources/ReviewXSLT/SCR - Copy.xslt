<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" />
  <xsl:template match="/">
    <script>
      var DeleteRecords=0; TotalRecords=0;

      var CriminalCount=1; var RecClear='';

    </script>
    <center>
      <div class="sectionsummary">
        <h3>
        </h3>
        <h3>
          (<xsl:value-of select="count(BGC/product/StatewideCriminalSearch/fulfillment/results/result)"/>) Results<br />
        </h3>
      </div>
      <xsl:choose>
        <xsl:when test="count(BGC/product/StatewideCriminalSearch/fulfillment/results/result/counts/count)>0">
          <table id="tblSCR" class="reporttable" border="0" cellpadding="0" cellspacing="0">
            <thead>
              <tr>
                <th class="begin"></th>
                <th colspan="5">
                  <span style="font-size:20px;font-weight:bold;">Statewide Criminal Report</span>
                </th>
                <th class="end"></th>
              </tr>
              <tr>
                <td class="begin">
                </td>
                <td align="left" colspan="5"  style="color:red;">
                  <br/>
                  <b>
                    <script type="text/javascript">
                      document.write(RecClear);
                    </script>
                    <span id="RecClear"></span>
                  </b>
                  <br/>
                </td>
                <td class="end">
                  <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                </td>
              </tr>
            </thead>
            <xsl:for-each select="BGC/product/StatewideCriminalSearch/fulfillment/results/result">
              <xsl:choose>
                <xsl:when test ="@Deleted">

                  <xsl:if test ="@Deleted!='1'">
                    <xsl:choose>
                      <xsl:when test="position()>1">
                        <tr style="height:20px;">
                          <td class="begin">
                          </td>
                          <td colspan="5" style="text-align:center;">
                            <span style="font-size:16px;font-weight:bold;color:white;">
                            </span>
                          </td>
                          <td class="end"></td>
                        </tr>
                      </xsl:when>
                    </xsl:choose>

                    <tr>
                      <td class="begin">
                      </td>
                      <td colspan="5" style="border:solid 1px black;background-color:black;text-align:left;">
                        <span style="font-size:16px;font-weight:bold;color:#fff;">
                          Criminal Record #
                          <script type="text/javascript">
                            document.write(CriminalCount);
                            CriminalCount++;
                            <!--<xsl:value-of select="position()" />-->
                          </script>
                        </span>
                      </td>
                      <td class="end"></td>
                    </tr>
                    <tr>
                      <td class="begin">
                      </td>
                      <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;padding:0px;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tr>
                            <td width="44%" style="background-color:#fff;">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <xsl:if test="state != ''">
                                  <tr>
                                    <td style="width:25%;text-align:right;background-color:white;border-bottom:0px solid black;">

                                      <b>STATE :</b>
                                    </td>
                                    <td style="background-color:#fff;">
                                      <xsl:value-of select="state"/>
                                    </td>
                                  </tr>
                                </xsl:if>
                                <xsl:if test="caseNumber != ''">
                                  <tr>
                                    <td style="width:25%;text-align:right;background-color:white;border-bottom:0px solid black;">

                                      <b>CASE NUMBER :</b>
                                    </td>
                                    <td style="background-color:#fff;">
                                      <xsl:value-of select="caseNumber"/>

                                    </td>
                                  </tr>
                                </xsl:if>
                                <xsl:if test="fileDate != ''">
                                  <tr>
                                    <td style="width:25%;text-align:right;background-color:white;border-bottom:0px solid black;">

                                      <b>FILE DATE :</b>
                                    </td>
                                    <td style="background-color:#fff;">
                                      <xsl:value-of select='substring(fileDate,1,10)' />

                                    </td>
                                  </tr>
                                </xsl:if>
                                <xsl:if test="dispositionDate != ''">
                                  <tr>
                                    <td style="width:25%;text-align:right;background-color:white;border-bottom:0px solid black;">

                                      <b>DISPOSITION DATE :</b>
                                    </td>
                                    <td style="background-color:#fff;">
                                      <xsl:value-of select='substring(dispositionDate,1,10)' />

                                    </td>
                                  </tr>
                                </xsl:if>
                              </table>
                            </td>
                            <td width="44%" style="background-color:#fff;">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <xsl:if test="nameOnFile != ''">
                                  <tr>
                                    <td style="width:25%;text-align:right;background-color:white;border-bottom:0px solid black;">

                                      <b>NAME ON FILE :</b>
                                    </td>
                                    <td style="background-color:#fff;">
                                      <xsl:value-of select="nameOnFile"/>

                                    </td>
                                  </tr>
                                </xsl:if>
                                <xsl:if test="isDOBVerified != ''">
                                  <tr>
                                    <td style="width:25%;text-align:right;background-color:white;border-bottom:0px solid black;">

                                      <b>DOB Verified :</b>
                                    </td>
                                    <td style="background-color:#fff;">
                                      <xsl:value-of select="isDOBVerified"/>

                                    </td>
                                  </tr>
                                </xsl:if>
                                <xsl:if test="isSSNVerified != ''">
                                  <tr>
                                    <td style="width:25%;text-align:right;background-color:white;border-bottom:0px solid black;">

                                      <b>SSN Verified :</b>
                                    </td>
                                    <td style="background-color:#fff;">
                                      <xsl:value-of select="isSSNVerified"/>

                                    </td>
                                  </tr>
                                </xsl:if>
                              </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td class="end"></td>
                    </tr>
                    <xsl:for-each select="counts/count">
                      <tr>
                        <td class="begin">
                        </td>
                        <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;">
                          <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border:solid 1px black;background-color:red;text-align:center;">
                            <tr>
                              <xsl:choose>
                                <xsl:when test="(position()mod 2)=1">
                                  <td width="10%" style="border-right:solid 1px black;background-color:#dcdcdc;text-align:center;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                      <tr>
                                        <td style="text-align:right;background-color:#dcdcdc;">
                                          <span style="font-size:12px;font-weight:bold;color:#000;">
                                            Charge #
                                            <xsl:value-of select="(position())" />
                                          </span>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </xsl:when>
                                <xsl:otherwise>
                                  <td width="10%" style="border-right:solid 1px black;background-color:white;text-align:center;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                      <tr>
                                        <td style="text-align:right;background-color:white;">
                                          <span style="font-size:12px;font-weight:bold;color:#000;">
                                            Charge #
                                            <xsl:value-of select="(position())" />
                                          </span>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </xsl:otherwise>
                              </xsl:choose>
                              <xsl:choose>
                                <xsl:when test="(position()mod 2)=1">
                                  <td width="45%" style="background-color:#dcdcdc;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                      <xsl:if test="crimeDetails != ''">
                                        <tr>
                                          <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                            <b>CRIME DETAILS :</b>
                                          </td>
                                          <td style="background-color:#dcdcdc;">
                                            <xsl:value-of select="crimeDetails"/>

                                          </td>
                                        </tr>
                                      </xsl:if>
                                      <xsl:if test="disposition != ''">
                                        <tr>
                                          <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                            <b>DISPOSITION :</b>
                                          </td>
                                          <td style="background-color:#dcdcdc;">
                                            <xsl:value-of select="disposition"/>

                                          </td>
                                        </tr>
                                      </xsl:if>
                                      <xsl:if test="fines != ''">
                                        <tr>
                                          <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                            <b>FINES :</b>
                                          </td>
                                          <td style="background-color:#dcdcdc;">
                                            <xsl:value-of select="fines"/>

                                          </td>
                                        </tr>
                                      </xsl:if>
                                      <xsl:if test="offenseDate!= ''">
                                        <tr>
                                          <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                            <b>Offense Date :</b>
                                          </td>
                                          <td style="background-color:#dcdcdc;">
                                            <xsl:value-of select="offenseDate"/>
                                          </td>
                                        </tr>
                                      </xsl:if>
                                      <xsl:if test="suspensionTime!= ''">
                                        <tr>
                                          <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                            <b>Suspension Time :</b>
                                          </td>
                                          <td style="background-color:#dcdcdc;">
                                            <xsl:value-of select="suspensionTime"/>
                                          </td>
                                        </tr>
                                      </xsl:if>
                                      <xsl:if test="prisonTime!= ''">
                                        <tr>
                                          <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                            <b>Prison Time :</b>
                                          </td>
                                          <td style="background-color:#dcdcdc;">
                                            <xsl:value-of select="prisonTime"/>
                                          </td>
                                        </tr>
                                      </xsl:if>
                                      <xsl:if test="jailTime!= ''">
                                        <tr>
                                          <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                            <b>Jail Time :</b>
                                          </td>
                                          <td style="background-color:#dcdcdc;">
                                            <xsl:value-of select="jailTime"/>
                                          </td>
                                        </tr>
                                      </xsl:if>
                                    </table>
                                  </td>
                                  <td width="45%" style="background-color:#dcdcdc;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                      <xsl:if test="crimeType != ''">
                                        <tr>
                                          <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                            <b>CRIME TYPE :</b>
                                          </td>
                                          <td style="background-color:#dcdcdc;">
                                            <xsl:value-of select="crimeType"/>
                                          </td>
                                        </tr>
                                      </xsl:if>
                                      <xsl:if test="DispositionDate != ''">
                                        <tr>
                                          <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                            <b>Disposition Date :</b>
                                          </td>
                                          <td style="background-color:#dcdcdc;">
                                            <xsl:value-of select="DispositionDate"/>
                                          </td>
                                        </tr>
                                      </xsl:if>
                                      <xsl:if test="courtCosts!= ''">
                                        <tr>
                                          <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                            <b>COURT COSTS :</b>
                                          </td>
                                          <td style="background-color:#dcdcdc;">
                                            <xsl:value-of select="courtCosts"/>
                                          </td>
                                        </tr>
                                      </xsl:if>
                                      <xsl:if test="probation!= ''">
                                        <tr>
                                          <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                            <b>Probation :</b>
                                          </td>
                                          <td style="background-color:#dcdcdc;">
                                            <xsl:value-of select="probation"/>
                                          </td>
                                        </tr>
                                      </xsl:if>
                                      <xsl:if test="additionalDispositionInfo!= ''">
                                        <tr>
                                          <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                            <b>Additional Disposition Information :</b>
                                          </td>
                                          <td style="background-color:#dcdcdc;">
                                            <xsl:value-of select="additionalDispositionInfo"/>
                                          </td>
                                        </tr>
                                      </xsl:if>
                                      <xsl:if test="generalNotes!= ''">
                                        <tr>
                                          <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                            <b>General Notes :</b>
                                          </td>
                                          <td style="background-color:#dcdcdc;">
                                            <xsl:value-of select="generalNotes"/>
                                          </td>
                                        </tr>
                                      </xsl:if>
                                    </table>
                                  </td>
                                </xsl:when>
                                <xsl:otherwise>
                                  <td width="45%" style="background-color:#fff;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                      <xsl:if test="crimeDetails != ''">
                                        <tr>
                                          <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                            <b>CRIME DETAILS :</b>
                                          </td>
                                          <td style="background-color:#fff;">
                                            <xsl:value-of select="crimeDetails"/>
                                          </td>
                                        </tr>
                                      </xsl:if>
                                      <xsl:if test="disposition != ''">
                                        <tr>
                                          <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                            <b>DISPOSITION :</b>
                                          </td>
                                          <td style="background-color:#fff;">
                                            <xsl:value-of select="disposition"/>
                                          </td>
                                        </tr>
                                      </xsl:if>
                                      <xsl:if test="fines != ''">
                                        <tr>
                                          <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                            <b>FINES :</b>
                                          </td>
                                          <td style="background-color:#fff;">
                                            <xsl:value-of select="fines"/>
                                          </td>
                                        </tr>
                                      </xsl:if>
                                      <xsl:if test="offenseDate!= ''">
                                        <tr>
                                          <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                            <b>Offense Date :</b>
                                          </td>
                                          <td style="background-color:#fff;">
                                            <xsl:value-of select="offenseDate"/>
                                          </td>
                                        </tr>
                                      </xsl:if>
                                      <xsl:if test="suspensionTime!= ''">
                                        <tr>
                                          <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                            <b>Suspension Time :</b>
                                          </td>
                                          <td style="background-color:#fff;">
                                            <xsl:value-of select="suspensionTime"/>
                                          </td>
                                        </tr>
                                      </xsl:if>
                                      <xsl:if test="prisonTime!= ''">
                                        <tr>
                                          <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                            <b>Prison Time :</b>
                                          </td>
                                          <td style="background-color:#fff;">
                                            <xsl:value-of select="prisonTime"/>
                                          </td>
                                        </tr>
                                      </xsl:if>
                                      <xsl:if test="jailTime!= ''">
                                        <tr>
                                          <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                            <b>Jail Time :</b>
                                          </td>
                                          <td style="background-color:#fff;">
                                            <xsl:value-of select="jailTime"/>
                                          </td>
                                        </tr>
                                      </xsl:if>
                                    </table>
                                  </td>
                                  <td width="45%" style="background-color:#fff;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                      <xsl:if test="crimeType != ''">
                                        <tr>
                                          <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                            <b>CRIME TYPE :</b>
                                          </td>
                                          <td style="background-color:#fff;">
                                            <xsl:value-of select="crimeType"/>

                                          </td>
                                        </tr>
                                      </xsl:if>
                                      <xsl:if test="DispositionDate != ''">
                                        <tr>
                                          <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                            <b>Disposition Date :</b>
                                          </td>
                                          <td style="background-color:#fff;">
                                            <xsl:value-of select="DispositionDate"/>


                                          </td>
                                        </tr>
                                      </xsl:if>
                                      <xsl:if test="courtCosts!= ''">
                                        <tr>
                                          <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                            <b>COURT COSTS :</b>
                                          </td>
                                          <td style="background-color:#fff;">
                                            <xsl:value-of select="courtCosts"/>
                                          </td>
                                        </tr>
                                      </xsl:if>
                                      <xsl:if test="probation!= ''">
                                        <tr>
                                          <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                            <b>Probation :</b>
                                          </td>
                                          <td style="background-color:#fff;">
                                            <xsl:value-of select="probation"/>
                                          </td>
                                        </tr>
                                      </xsl:if>
                                      <xsl:if test="additionalDispositionInfo!= ''">
                                        <tr>
                                          <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                            <b>Additional Disposition Information :</b>
                                          </td>
                                          <td style="background-color:#fff;">
                                            <xsl:value-of select="additionalDispositionInfo"/>
                                          </td>
                                        </tr>
                                      </xsl:if>
                                      <xsl:if test="generalNotes!= ''">
                                        <tr>
                                          <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                            <b>General Notes :</b>
                                          </td>
                                          <td style="background-color:#fff;">
                                            <xsl:value-of select="generalNotes"/>
                                          </td>
                                        </tr>
                                      </xsl:if>
                                    </table>
                                  </td>
                                </xsl:otherwise>
                              </xsl:choose>
                            </tr>
                          </table>
                        </td>
                        <td class="end"></td>
                      </tr>
                    </xsl:for-each>
                    <xsl:if test="ReviewNote!=''">
                      <tr>
                        <td class="begin">
                        </td>
                        <td colspan="4" style="width:95%;background-color:#FFFFE1" class="ReviewNote">
                          <xsl:value-of select="ReviewNote"></xsl:value-of>
                        </td>
                        <td align="right" valign="bottom" style="vertical-align:bottom">

                        </td>
                        <td class="end"></td>
                      </tr>
                    </xsl:if>
                  </xsl:if>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:choose>
                    <xsl:when test="position()>1">
                      <tr style="height:20px;">
                        <td class="begin">
                        </td>
                        <td colspan="5" style="text-align:center;">
                          <span style="font-size:16px;font-weight:bold;color:white;">
                          </span>
                        </td>
                        <td class="end"></td>
                      </tr>
                    </xsl:when>
                  </xsl:choose>

                  <tr>
                    <td class="begin">
                    </td>
                    <td colspan="5" style="border:solid 1px black;background-color:black;text-align:left;">
                      <span style="font-size:16px;font-weight:bold;color:#fff;">
                        Criminal Record #
                        <script type="text/javascript">
                          document.write(CriminalCount);
                          CriminalCount++;
                        </script>
                        <!--<xsl:value-of select="(position())" />-->
                      </span>
                    </td>
                    <td class="end"></td>
                  </tr>
                  <tr>
                    <td class="begin">
                    </td>
                    <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;padding:0px;">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                          <td width="44%" style="background-color:#fff;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <xsl:if test="state != ''">
                                <tr>
                                  <td style="width:25%;text-align:right;background-color:white;border-bottom:0px solid black;">

                                    <b>STATE :</b>
                                  </td>
                                  <td style="background-color:#fff;">
                                    <xsl:value-of select="state"/>
                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="caseNumber != ''">
                                <tr>
                                  <td style="width:25%;text-align:right;background-color:white;border-bottom:0px solid black;">

                                    <b>CASE NUMBER :</b>
                                  </td>
                                  <td style="background-color:#fff;">
                                    <xsl:value-of select="caseNumber"/>

                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="fileDate != ''">
                                <tr>
                                  <td style="width:25%;text-align:right;background-color:white;border-bottom:0px solid black;">

                                    <b>FILE DATE :</b>
                                  </td>
                                  <td style="background-color:#fff;">
                                    <xsl:value-of select="fileDate"/>

                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="dispositionDate != ''">
                                <tr>
                                  <td style="width:25%;text-align:right;background-color:white;border-bottom:0px solid black;">

                                    <b>DISPOSITION DATE :</b>
                                  </td>
                                  <td style="background-color:#fff;">
                                    <xsl:value-of select="dispositionDate"/>

                                  </td>
                                </tr>
                              </xsl:if>
                            </table>
                          </td>
                          <td width="44%" style="background-color:#fff;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <xsl:if test="nameOnFile != ''">
                                <tr>
                                  <td style="width:25%;text-align:right;background-color:white;border-bottom:0px solid black;">

                                    <b>NAME ON FILE :</b>
                                  </td>
                                  <td style="background-color:#fff;">
                                    <xsl:value-of select="nameOnFile"/>

                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="isDOBVerified != ''">
                                <tr>
                                  <td style="width:25%;text-align:right;background-color:white;border-bottom:0px solid black;">

                                    <b>DOB Verified :</b>
                                  </td>
                                  <td style="background-color:#fff;">
                                    <xsl:value-of select="isDOBVerified"/>

                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="isSSNVerified != ''">
                                <tr>
                                  <td style="width:25%;text-align:right;background-color:white;border-bottom:0px solid black;">

                                    <b>SSN Verified :</b>
                                  </td>
                                  <td style="background-color:#fff;">
                                    <xsl:value-of select="isSSNVerified"/>

                                  </td>
                                </tr>
                              </xsl:if>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                    <td class="end"></td>
                  </tr>
                  <xsl:for-each select="counts/count">
                    <tr>
                      <td class="begin">
                      </td>
                      <td colspan="5" style="border:solid 0px black;background-color:white;text-align:center;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border:solid 1px black;background-color:red;text-align:center;">
                          <tr>
                            <xsl:choose>
                              <xsl:when test="(position()mod 2)=1">
                                <td width="10%" style="border-right:solid 1px black;background-color:#dcdcdc;text-align:center;">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                      <td style="text-align:right;background-color:#dcdcdc;">
                                        <span style="font-size:12px;font-weight:bold;color:#000;">
                                          Charge #
                                          <xsl:value-of select="(position())" />
                                        </span>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </xsl:when>
                              <xsl:otherwise>
                                <td width="10%" style="border-right:solid 1px black;background-color:white;text-align:center;">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                      <td style="text-align:right;background-color:white;">
                                        <span style="font-size:12px;font-weight:bold;color:#000;">
                                          Charge #
                                          <xsl:value-of select="(position())" />
                                        </span>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </xsl:otherwise>
                            </xsl:choose>
                            <xsl:choose>
                              <xsl:when test="(position()mod 2)=1">
                                <td width="45%" style="background-color:#dcdcdc;">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <xsl:if test="crimeDetails != ''">
                                      <tr>
                                        <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                          <b>CRIME DETAILS :</b>
                                        </td>
                                        <td style="background-color:#dcdcdc;">
                                          <xsl:value-of select="crimeDetails"/>

                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="disposition != ''">
                                      <tr>
                                        <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                          <b>DISPOSITION :</b>
                                        </td>
                                        <td style="background-color:#dcdcdc;">
                                          <xsl:value-of select="disposition"/>

                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="fines != ''">
                                      <tr>
                                        <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                          <b>FINES :</b>
                                        </td>
                                        <td style="background-color:#dcdcdc;">
                                          <xsl:value-of select="fines"/>

                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="offenseDate!= ''">
                                      <tr>
                                        <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                          <b>Offense Date :</b>
                                        </td>
                                        <td style="background-color:#dcdcdc;">
                                          <xsl:value-of select="offenseDate"/>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="suspensionTime!= ''">
                                      <tr>
                                        <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                          <b>Suspension Time :</b>
                                        </td>
                                        <td style="background-color:#dcdcdc;">
                                          <xsl:value-of select="suspensionTime"/>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="prisonTime!= ''">
                                      <tr>
                                        <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                          <b>Prison Time :</b>
                                        </td>
                                        <td style="background-color:#dcdcdc;">
                                          <xsl:value-of select="prisonTime"/>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="jailTime!= ''">
                                      <tr>
                                        <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                          <b>Jail Time :</b>
                                        </td>
                                        <td style="background-color:#dcdcdc;">
                                          <xsl:value-of select="jailTime"/>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                  </table>
                                </td>
                                <td width="45%" style="background-color:#dcdcdc;">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <xsl:if test="crimeType != ''">
                                      <tr>
                                        <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                          <b>CRIME TYPE :</b>
                                        </td>
                                        <td style="background-color:#dcdcdc;">
                                          <xsl:value-of select="crimeType"/>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="DispositionDate != ''">
                                      <tr>
                                        <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                          <b>Disposition Date :</b>
                                        </td>
                                        <td style="background-color:#dcdcdc;">
                                          <xsl:value-of select="DispositionDate"/>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="courtCosts!= ''">
                                      <tr>
                                        <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                          <b>COURT COSTS :</b>
                                        </td>
                                        <td style="background-color:#dcdcdc;">
                                          <xsl:value-of select="courtCosts"/>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="probation!= ''">
                                      <tr>
                                        <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                          <b>Probation :</b>
                                        </td>
                                        <td style="background-color:#dcdcdc;">
                                          <xsl:value-of select="probation"/>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="additionalDispositionInfo!= ''">
                                      <tr>
                                        <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                          <b>Additional Disposition Information :</b>
                                        </td>
                                        <td style="background-color:#dcdcdc;">
                                          <xsl:value-of select="additionalDispositionInfo"/>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="generalNotes!= ''">
                                      <tr>
                                        <td style="text-align:right;background-color:#dcdcdc;border-bottom:0px solid black;width:25%;">

                                          <b>General Notes :</b>
                                        </td>
                                        <td style="background-color:#dcdcdc;">
                                          <xsl:value-of select="generalNotes"/>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                  </table>
                                </td>
                              </xsl:when>
                              <xsl:otherwise>
                                <td width="45%" style="background-color:#fff;">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <xsl:if test="crimeDetails != ''">
                                      <tr>
                                        <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                          <b>CRIME DETAILS :</b>
                                        </td>
                                        <td style="background-color:#fff;">
                                          <xsl:value-of select="crimeDetails"/>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="disposition != ''">
                                      <tr>
                                        <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                          <b>DISPOSITION :</b>
                                        </td>
                                        <td style="background-color:#fff;">
                                          <xsl:value-of select="disposition"/>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="fines != ''">
                                      <tr>
                                        <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                          <b>FINES :</b>
                                        </td>
                                        <td style="background-color:#fff;">
                                          <xsl:value-of select="fines"/>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="offenseDate!= ''">
                                      <tr>
                                        <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                          <b>Offense Date :</b>
                                        </td>
                                        <td style="background-color:#fff;">
                                          <xsl:value-of select="offenseDate"/>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="suspensionTime!= ''">
                                      <tr>
                                        <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                          <b>Suspension Time :</b>
                                        </td>
                                        <td style="background-color:#fff;">
                                          <xsl:value-of select="suspensionTime"/>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="prisonTime!= ''">
                                      <tr>
                                        <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                          <b>Prison Time :</b>
                                        </td>
                                        <td style="background-color:#fff;">
                                          <xsl:value-of select="prisonTime"/>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="jailTime!= ''">
                                      <tr>
                                        <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                          <b>Jail Time :</b>
                                        </td>
                                        <td style="background-color:#fff;">
                                          <xsl:value-of select="jailTime"/>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                  </table>
                                </td>
                                <td width="45%" style="background-color:#fff;">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <xsl:if test="crimeType != ''">
                                      <tr>
                                        <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                          <b>CRIME TYPE :</b>
                                        </td>
                                        <td style="background-color:#fff;">
                                          <xsl:value-of select="crimeType"/>

                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="DispositionDate != ''">
                                      <tr>
                                        <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                          <b>Disposition Date :</b>
                                        </td>
                                        <td style="background-color:#fff;">
                                          <xsl:value-of select="DispositionDate"/>


                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="courtCosts!= ''">
                                      <tr>
                                        <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                          <b>COURT COSTS :</b>
                                        </td>
                                        <td style="background-color:#fff;">
                                          <xsl:value-of select="courtCosts"/>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="probation!= ''">
                                      <tr>
                                        <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                          <b>Probation :</b>
                                        </td>
                                        <td style="background-color:#fff;">
                                          <xsl:value-of select="probation"/>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="additionalDispositionInfo!= ''">
                                      <tr>
                                        <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                          <b>Additional Disposition Information :</b>
                                        </td>
                                        <td style="background-color:#fff;">
                                          <xsl:value-of select="additionalDispositionInfo"/>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="generalNotes!= ''">
                                      <tr>
                                        <td style="text-align:right;background-color:#fff;border-bottom:0px solid black;width:25%;">

                                          <b>General Notes :</b>
                                        </td>
                                        <td style="background-color:#fff;">
                                          <xsl:value-of select="generalNotes"/>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                  </table>
                                </td>
                              </xsl:otherwise>
                            </xsl:choose>
                          </tr>
                        </table>
                      </td>
                      <td class="end"></td>
                    </tr>
                  </xsl:for-each>
                  <xsl:if test="ReviewNote!=''">
                    <tr>
                      <td class="begin">
                      </td>
                      <td colspan="4" style="width:95%; background-color:#FFFFE1">
                        <xsl:value-of select="ReviewNote"></xsl:value-of>
                      </td>
                      <td align="right" valign="bottom" style="vertical-align:bottom">

                      </td>
                      <td class="end"></td>
                    </tr>
                  </xsl:if>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>
          </table>
        </xsl:when>
        <xsl:otherwise>
          <center>
            <div style="color:red;font-weight:bold;font-size:12px;">
              No Record Found
            </div>
          </center>
        </xsl:otherwise>
      </xsl:choose>
    </center>
  </xsl:template>

  <xsl:template match="/BGC/product/StatewideCriminalSearch/response">
    <div class="noRecords">
      No Records Found.
      <br />
      * CLEAR *
    </div>
  </xsl:template>
</xsl:stylesheet>
