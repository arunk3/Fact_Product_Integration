﻿<?xml version="1.0" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output indent="yes" method="html"/>
	<xsl:template match="/">
		<html>
			<head>
				<style type="text/css">
					body{font-family: Lucida Grande,Arial,Helvetica,sans-serif;font-size: 14px;font-weight: normal;}

					.borders {border-left:1px solid #000000;
					border-right:1px solid #000000;
					border-top:1px solid #000000;
					border-bottom:1px solid #000000;}

					.tbl{Border:1px Solid #000;font-size:16pt;font-weight:bold;}
					.titletxt{font-size:12pt;font-weight:bold;}
				</style>
			</head>
			<body bgcolor="#ffffff">
				<div class="people-search-wrap">
					<table width="100%" border="0" class="people-search">
						<tr>
							<td>
								<table  width="100%" border="0">
									<tr >
										<td class="width_10"></td>
										<td style="text-align:left;font-size:20px;font-weight: bold;">
											Summary
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table width="100%" >
									<tr>
										<td class="width_10"></td>
										<td class="width_25">
											Report Type:&#160;Employment Verification
										</td>
										<td></td>
									</tr>
									<tr>
										<td class="width_10"></td>
										<td style="width:5%;">
											Subject:&#160;
											<xsl:value-of select="ScreeningResults/completeOrder/subject/name_last"/>&#160;
											<xsl:value-of select="ScreeningResults/completeOrder/subject/name_first"/>
										</td>
									</tr>
									<tr>
										<td class="width_10"></td>
										<td style="width:5%;">
											DOB:&#160;
											<xsl:value-of select="ScreeningResults/completeOrder/subject/dob"/>
										</td>
									</tr>

								</table>
							</td>
						</tr>
						<!--<tr>
              <td>
                <table  width="100%" >
                  <tr>
                    <td class="width_10"></td>
                    <td style="width:5%;">
                      Subject:&#160;
                      <xsl:value-of select="ScreeningResults/completeOrder/subject/name_last"/>&#160;
                      <xsl:value-of select="ScreeningResults/completeOrder/subject/name_first"/>
                    </td>
                  </tr>
                  <tr>
                    <td class="width_10"></td>
                    <td style="width:5%;">
                      DOB:&#160;
                      <xsl:value-of select="ScreeningResults/completeOrder/subject/dob"/>
                    </td>
                  </tr>

                </table>
              </td>
            </tr>-->
						<xsl:variable name="i" select="1" />
						<xsl:for-each select="ScreeningResults/completeOrder/subOrder">
							<!--Multiple subOrders-->
							<xsl:variable name="counter" select="position()-1" />

							<tr>
								<td>


									<table  width="100%" border="0">
										<tr class="search-heading sparater-td bg-gray">
											<td class="width_10 text-right">
												<span class="countr blue-no" style="padding-top: 4px;">
													<xsl:value-of select="$counter + 1" />
												</span>
											</td>
											<td>
												<span class="PS_SSN_DEATH_TOP">
													<xsl:copy>
														Employment History
													</xsl:copy>
												</span>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table>
										<tr>
											<td class="width_10"></td>
											<td> </td>
											<td class="width_45">
												<u>Provided by Subject</u>
											</td>
											<td class="width_45">
												<u>Provided by Source</u>
											</td>
										</tr>

										<tr>
											<td class="width_10"></td>
											<td class="width_15 text-right">
												Employer Name:&#160;
											</td>
											<td>
												<xsl:value-of select="company_name"/>
											</td>
											<td>
												<xsl:value-of select="verified_company_name"/>
											</td>
										</tr>
										<tr>
											<td class="width_10"></td>
											<td  class="width_30 text-right">
												Contact Phone Number:&#160;
											</td>
											<td>
												<xsl:value-of select="phone_number"/>
											</td>
											<td>
												<xsl:value-of select="verified_phone_number"/>
											</td>
										</tr>
										<tr>
											<td class="width_10"></td>
											<td  class="width_30 text-right">
												Address 1:&#160;
											</td>
											<td  class="width_30">
												<xsl:value-of select="address1"/>
											</td>
											<td>
												<xsl:value-of select="verified_address1"/>
											</td>
										</tr>
										<tr>
											<td class="width_10"></td>
											<td  class="width_30 text-right">
												Address 2:&#160;
											</td>
											<td>
												<xsl:value-of select="address2"/>
											</td>
											<td>
												<xsl:value-of select="verified_address2"/>
											</td>
										</tr>
										<tr>
											<td class="width_10"></td>
											<td  class="width_30 text-right">
												City:&#160;
											</td>
											<td>
												<xsl:value-of select="city"/>
											</td>
											<td>
												<xsl:value-of select="verified_city"/>
											</td>
										</tr>
										<tr>
											<td class="width_10"></td>
											<td  class="width_30 text-right">
												State:&#160;
											</td>
											<td>
												<xsl:value-of select="state"/>
											</td>
											<td>
											</td>
										</tr>
										<tr>
											<td class="width_10"></td>
											<td  class="width_30 text-right">
												Zip:&#160;
											</td>
											<td>
												<xsl:value-of select="zip"/>
											</td>
											<td>
											</td>
										</tr>
										<tr>
											<td class="width_10"></td>
											<td  class="width_30 text-right">
												Position/Title:&#160;
											</td>
											<td>
												<xsl:value-of select="position"/>
											</td>
											<td>
												<xsl:value-of select="verified_position"/>
											</td>
										</tr>
										<tr>
											<td class="width_10"></td>
											<td  class="width_30 text-right">
												Date From:&#160;
											</td>
											<td>
												<xsl:if test="start_date!=''">
													<xsl:value-of select="start_date"/>
												</xsl:if>
											</td>
											<td>
												<xsl:value-of select="verified_start_date"/>
											</td>
										</tr>
										<tr>
											<td class="width_10"></td>
											<td  class="width_30 text-right">
												Date To: &#160;
											</td>
											<td  class="width_30">
												<xsl:if test="end_date!=''">
													<xsl:value-of select="end_date"/>
												</xsl:if>
											</td>
											<td>
												<xsl:value-of select="verified_end_date"/>
											</td>
										</tr>
										<tr>
											<td class="width_10"></td>
											<td  class="width_30 text-right">
												Income:&#160;
											</td>
											<td>
												<xsl:value-of select="income"/>
											</td>
											<td>
												<xsl:value-of select="verified_income"/>
											</td>
										</tr>
										<tr>
											<td  class="width_10"></td>
											<td  class="width_30 text-right">
												Reason For Leaving:&#160;
											</td>
											<td>
												<xsl:value-of select="reason_for_leaving"/>
											</td>
											<td>
												<xsl:value-of select="verified_reason_for_leaving"/>
											</td>
										</tr>
										<tr>
											<td  class="width_10"></td>
											<td  class="width_30 text-right">
												Employer Comments:&#160;
											</td>
											<td>
												<xsl:value-of select="comments"/>
											</td>
											<td>
												<xsl:value-of select="verified_employer_comments"/>
											</td>
										</tr>
										<tr>
											<td  class="width_10"></td>
											<td  class="width_30 text-right">
												Eligible for Rehire:&#160;
											</td>
											<td>
												<xsl:value-of select="''"/>
											</td>
											<td>
												<xsl:value-of select="verified_eligible_for_rehire"/>
											</td>
										</tr>
										<tr>
											<td  class="width_10"> </td>
											<td  class="width_30 text-right">
												Reason for rehire eligibility:&#160;
											</td>
											<td>
												<xsl:value-of select="''"/>
											</td>
											<td>
												<xsl:value-of select="verified_eligible_for_rehire_reason"/>
											</td>
										</tr>
										<tr>
											<td  class="width_30"> </td>
											<td  class="width_30 text-right">
												AKA's on file :&#160;
											</td>
											<td>
												<xsl:value-of select="''"/>
											</td>
											<td></td>
										</tr>
										<tr>
											<td  class="width_10"></td>
											<td  class="width_30 text-right">
												Order Comments:&#160;
											</td>
											<td>
												<xsl:value-of select="''"/>
											</td>
											<td></td>
										</tr>
										<tr>
											<td  class="width_10"> </td>
											<td  class="width_15 text-right">
												Person Interviewed:
											</td>
											<td>
												<xsl:value-of select="verified_verifier"/>
											</td>
											<td></td>
										</tr>
										<tr>
											<td  class="width_10"> </td>
											<td  class="width_30 text-right">
												Researched By:
											</td>
											<td>
												<xsl:value-of select="verified_verifier"/>
											</td>
											<td></td>
										</tr>
										<tr>
											<td  class="width_10"></td>
											<td  class="width_30 text-right">
												Research Comments:&#160;
											</td>
											<td>
												<xsl:value-of select="researcher_comments"/>
											</td>
											<td></td>
										</tr>
									</table>
								</td>
							</tr>

						</xsl:for-each>
						<tr>
							<td>
								<xsl:choose>
									<xsl:when test="ScreeningResults/completeOrder/subOrder/@filledCode ='clear'">
										<p style="text-align: justify;font-size:14px;">NO RECORDS WERE LOCATED.</p>
									</xsl:when>
									<xsl:otherwise>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</tr>
						<tr>
							<td align="center" colspan="2" >
								<span style="color:red;font-weight:600;font-size:14px;">**** End Of Report ****</span>
							</td>
						</tr>
					</table>
				</div>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>