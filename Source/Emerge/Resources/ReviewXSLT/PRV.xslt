﻿<?xml version="1.0" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output indent="yes" method="html"/>
	<xsl:template match="/">
		<html>
			<head>
				<style type="text/css">
					body{font-family: Lucida Grande,Arial,Helvetica,sans-serif;font-size: 14px;font-weight: normal;}

					.borders {border-left:1px solid #000000;
					border-right:1px solid #000000;
					border-top:1px solid #000000;
					border-bottom:1px solid #000000;}

					.tbl{Border:1px Solid #000;font-size:16pt;font-weight:bold;}
					.titletxt{font-size:12pt;font-weight:bold;}
				</style>
			</head>
			<body bgcolor="#ffffff">
				<div class="people-search-wrap">
					<table width="100%" border="0"  class="people-search">
						<tr>
							<td>
								<table  width="100%" border="0">
									<tr >
										<td class="width_10"></td>
										<td style="text-align:left;font-size:20px;font-weight: bold;">
											CONFIDENTIAL
										</td>
									</tr>
								</table>
							</td>
						</tr>

						<tr>
							<td>
								<table width="100%" >
									<tr>
										<td class="width_10"></td>
										<td class="width_25">
											<b> Background Verification Report</b>
										</td>
										<td></td>
									</tr>
								</table>

							</td>
						</tr>

						<tr>
							<td>
								<table width="100%" >
									<tr>
										<td class="width_10"></td>
										<td style="text-align:left;width:50%; font-size:14px;">
											Requestor:&#160;<xsl:value-of select="ScreeningResults/orderInfo/requester_name"/>
										</td>

										<td style="text-align:left;width:50%; font-size:14px;">
											Subject:&#160;
											<xsl:value-of select="ScreeningResults/completeOrder/subject/name_last"/>&#160;
											<xsl:value-of select="ScreeningResults/completeOrder/subject/name_first"/>
										</td>
									</tr>
									<tr>
										<td class="width_10"></td>
										<td style="text-align:left;width:50%; font-size:14px;">
											Position Applied:
										</td>
										<!--</tr>
									<tr>
                    <td class="width_10"></td>-->
										<td style="text-align:left;width:50%; font-size:14px;">
											DOB:&#160;
											<xsl:value-of select="ScreeningResults/completeOrder/subject/dob"/>
										</td>
									</tr>
								</table>
							</td>
						</tr>

						<xsl:for-each select="ScreeningResults/completeOrder/subOrder">
							<xsl:variable name="counter" select="position()-1" />
							<tr>
								<td>
									<table  width="100%" border="0">
										<tr class="search-heading sparater-td bg-gray">
											<td class="width_10 text-right">
												<span class="countr blue-no" style="padding-top: 4px;">
													<xsl:value-of select="$counter + 1" />
												</span>
											</td>

											<td>
												<span class="PS_SSN_DEATH_TOP">
													<xsl:copy>
														Personal References
													</xsl:copy>
												</span>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table>
										<tr>
											<td class="width_10"></td>

											<td class="width_15 text-right">

											</td>
											<td  >
												<u>Provided by Subject</u>
											</td>
											<td> </td>
											<td></td>
										</tr>
										<tr>
											<td class="width_10"></td>
											<td class="width_30 text-right">
												Contact Name:&#160;
											</td>
											<td >
												<xsl:value-of select="contactname"/>
											</td>
											<td >
												<xsl:value-of select="verified_contactname"/>
											</td>
											<td></td>
										</tr>
										<tr>
											<td class="width_10"></td>
											<td class="width_30 text-right">
												Reference Type:&#160;
											</td>
											<td >
												<xsl:value-of select="type"/>
											</td>
											<td >
												<xsl:value-of select="verified_type"/>
											</td>
											<td></td>
										</tr>
										<tr>
											<td class="width_10"></td>
											<td class="width_30 text-right">
												Contact Relationship:&#160;
											</td>
											<td >
												<xsl:value-of select="relationship"/>
											</td>
											<td >
												<xsl:value-of select="verified_relationship"/>
											</td>
											<td></td>
										</tr>
										<tr>
											<td class="width_10"></td>
											<td class="width_30 text-right">
												Contact Phone:&#160;
											</td>
											<td >
												<xsl:value-of select="contactphone"/>
											</td>
											<td >
												<xsl:value-of select="verified_contactphone"/>
											</td>
											<td></td>
										</tr>
										<tr>
											<td class="width_10"></td>
											<td class="width_30 text-right">
												Contact address:&#160;
											</td>
											<td >
												<xsl:value-of select="contactaddress"/>
											</td>
											<td >
												<xsl:value-of select="verified_contactaddress"/>
											</td>
											<td></td>
										</tr>
										<tr>
											<td class="width_10"></td>
											<td class="width_30 text-right">
												Contact city:&#160;
											</td>
											<td >
												<xsl:value-of select="contactcity"/>
											</td>
											<td >
												<xsl:value-of select="verified_contactcity"/>
											</td>
											<td></td>
										</tr>
										<tr>
											<td class="width_10"></td>
											<td class="width_30 text-right">
												Contact state:&#160;
											</td>
											<td >
												<xsl:value-of select="contactstate"/>
											</td>
											<td >
												<xsl:value-of select="verified_contactstate"/>
											</td>
											<td></td>
										</tr>
										<tr>
											<td class="width_10"></td>
											<td class="width_30 text-right">
												Contact zip:&#160;
											</td>
											<td >
												<xsl:value-of select="contactzip"/>
											</td>
											<td >
												<xsl:value-of select="verified_contactzip"/>
											</td>
											<td></td>
										</tr>
									</table>
									<table width="100%">
										<xsl:for-each select="question">
											<tr>
												<td class="width_10"></td>
												<td>
													<xsl:value-of select="prompt" />
												</td>
												<td>
													<xsl:value-of select="response" />
												</td>
											</tr>
										</xsl:for-each>
									</table>
								</td>
							</tr>
						</xsl:for-each>
						<tr>
							<td>

								<xsl:choose>
									<xsl:when test="ScreeningResults/completeOrder/subOrder/@filledCode ='clear'">
										<p style="text-align: justify;font-size:14px;">No Record Found.</p>
									</xsl:when>
									<xsl:otherwise>

									</xsl:otherwise>
								</xsl:choose>
							</td>
						</tr>
						<tr>
							<td align="center" colspan="2" >
								<span style="color:red;font-weight:600;font-size:14px;">**** End Of Report ****</span>
							</td>
						</tr>

					</table>


				</div>

			</body>
		</html>

	</xsl:template>

</xsl:stylesheet>