<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:a="http://ns.hr-xml.org/2004-08-02">
	<xsl:output method="html"/>
	<xsl:template match="/">
        <pre>
        <xsl:value-of select="*" disable-output-escaping="yes" />
        </pre>
        <div class="sectionsummary">
      <h3 style="line-height: 1em;">

        Data Sources Searched:<br/>
        Court Records, Department of Corrections, Administrative Office of Courts,<br/>
        National Sex Offender Registry, OFAC, OIG<br/>
        For a complete list of coverage, please visit<br/>
          <a href="http://emerge.intelifi.com/coverage.pdf" target="_blank">US Criminal Rapid Search Coverage Guide</a>

      </h3>
    </div>
	</xsl:template>
</xsl:stylesheet>