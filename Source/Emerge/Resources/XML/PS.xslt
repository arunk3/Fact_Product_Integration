<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:a="http://ns.hr-xml.org/2004-08-02">
  <xsl:param name="admin" select="defaultstring" />
  <xsl:output method="html"/>
  <xsl:template match="/">

    <xsl:if test="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:SSNStatus)&gt;'0'">

      <table cellpadding="2" cellspacing="0" style="margin: 0px" align="center" class="PS_main">
        <tr>
          <td class="PS_BORDER" width="50%">
            <span class="PS_SSN_DEATH_TOP">
              SSN ISSUANCE DATABASE
            </span >
            <br/>
            <span class="PS_SSN_DEATH_BOTTOM">
              <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:SSNStatus"/>
            </span>
          </td>
          <td class="PS_BORDER">
            <span class="PS_SSN_DEATH_TOP">
              DEATH INDEX
            </span>
            <br/>
            <span class="PS_SSN_DEATH_BOTTOM">
              <xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:DateOfDeath"/>
            </span>
          </td>
        </tr>
      </table>

    </xsl:if>

    <br/>
    <br/>

    <table cellpadding="2" cellspacing="0" style="margin: 0px" align="center" class="PS_main">
      <tr>
        <td class="PS_BORDER" width="33%" valign="top">
          <span class="PS_SSN_DEATH_TOP">
            REPORTED COUNTIES ( <xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PostalAddress/a:County)"/> )
          </span >
          <br/>
          <xsl:variable name="unique-list" select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PostalAddress/a:County[not(.=following::a:County)]" />
          <xsl:for-each select="$unique-list">
            <xsl:value-of select="." />
            <br/>
          </xsl:for-each>


          <!--<xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PostalAddress[not(.=preceding::a:PostalAddress)]" >
            <span class="PS_SSN_DEATH_BOTTOM">
              <xsl:value-of select="a:County"/>
              <br/>
            </span>
          </xsl:for-each>-->
        </td>
        <td class="PS_BORDER" valign="top">
          <span class="PS_SSN_DEATH_TOP">
            REPORTED NAMES ( <xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PersonName/a:GivenName)"/> )
          </span >
          <br/>
          <xsl:variable name="unique-listName" select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PersonName/a:FormattedName[not(.=following::a:FormattedName)]" />
          <xsl:for-each select="$unique-listName">
            <xsl:value-of select="." />
            <br/>
          </xsl:for-each>

          <!--<xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:PersonName" >
            <span class="PS_SSN_DEATH_BOTTOM">
              <xsl:if test="a:FamilyName!='' and a:GivenName !=''">
              <xsl:value-of select="a:FamilyName" />, <xsl:value-of select="a:GivenName" /><br/>
              </xsl:if>
            </span>
          </xsl:for-each>-->
        </td>
        <td class="PS_BORDER" valign="top">
          <span class="PS_SSN_DEATH_TOP">
            REPORTED BIRTHDATES ( <xsl:value-of select="count(a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:DemographicDetail/a:DateOfBirth)"/> )
          </span>
          <br/>
          <xsl:variable name="unique-listDates" select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:DemographicDetail/a:DateOfBirth[not(.=following::a:DateOfBirth)]" />
          <xsl:for-each select="$unique-listDates">
            <xsl:value-of select="." />
            <br/>
          </xsl:for-each>


          <!--<xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData/a:DemographicDetail" >
            <span class="PS_SSN_DEATH_BOTTOM">
              <xsl:value-of select="a:DateOfBirth"/>
              <br/>
            </span>
          </xsl:for-each>-->
        </td>
      </tr>
    </table>

    <br/>
    <br/>


    <div class="sectionsummary">
      <h3>
        (<xsl:value-of select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/@recordCount" />) Results PEOPLE SEARCH
      </h3>
      <br />
      <h3>Name:</h3>
      <h4>
        <xsl:value-of select="concat(a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:GivenName,',',a:BackgroundReports/a:BackgroundReportPackage/a:ScreeningsSummary/a:PersonalData/a:PersonName/a:FamilyName)" />
      </h4>
      <br/>
    </div>

    <center>

      <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/@recordCount = '0'">
        <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
          <thead>
            <tr>
              <th class="begin"></th>
              <th colspan="5">Search Information</th>
              <th class="end"></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="begin">i</td>
              <td colspan="5">
                No Records Found.
              </td>
              <td class="end">
                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
              </td>
            </tr>
          </tbody>
          <tfoot>
            <td class="begin"></td>
            <td colspan="5"></td>
            <td class="end"></td>
          </tfoot>
        </table>
      </xsl:if>

      <xsl:if test="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/@recordCount != '0'">
        <table id="peopleTable" class="reporttable" border="0" cellpadding="0" cellspacing="0">
          <thead>
            <tr>
              <th class="begin"></th>
              <th align="left">
                <u>PERSONAL INFO</u>
              </th>
              <th align="left">
                <u>ADDRESS INFO</u>
              </th>
              <th align="left">
                <u>PHONE INFO</u>
              </th>
              <th class="end"></th>
            </tr>
          </thead>
          <tbody>
            <xsl:for-each select="a:BackgroundReports/a:BackgroundReportPackage/a:Screenings/a:Screening/a:SSNReport/a:CreditFile/a:PersonalData">

              <xsl:choose>
                <xsl:when test="(position() mod 2 = 0)">
                  <tr>
                    <td class="begin">
                      <xsl:value-of select="(position() )" />
                    </td>
                    <td style="background-color:#fff;">
                      <xsl:value-of select="a:PersonName/a:FamilyName" />, <xsl:value-of select="a:PersonName/a:GivenName" /><br/>
                      SSN : xxx-xx-<xsl:value-of select="substring(a:DemographicDetail/a:GovernmentId,6,4)" />

                      <br/>
                      DOB : <xsl:value-of select="a:DemographicDetail/a:DateOfBirth" />
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
                      Age : <xsl:value-of select="a:DemographicDetail/a:Age" />
                    </td>
                    <td style="background-color:#fff;">
                      <xsl:value-of select="a:PostalAddress/a:DeliveryAddress/a:AddressLine" />
                      <xsl:text> </xsl:text>
                      <br />

                      <xsl:value-of select="a:PostalAddress/a:Municipality" />
                      <xsl:text>, </xsl:text>
                      <xsl:value-of select="a:PostalAddress/a:Region" />
                      <xsl:text> </xsl:text>
                      <br />
                      <xsl:value-of select="a:PostalAddress/a:County" />
                      <xsl:text> </xsl:text>
                      <br />

                      <xsl:value-of select="a:PostalAddress/a:CountryCode" />
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="a:PostalAddress/a:PostalCode" />
                      <br/>
                      <xsl:value-of select="a:EffectiveDate/a:StartDate" />
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;-&amp;nbsp;</xsl:text>
                      <xsl:value-of select="a:EffectiveDate/a:EndDate" />
                    </td>
                    <td style="border-right: none;background-color:#fff;">
                      <xsl:choose>
                        <xsl:when test="count(a:ContactMethod)='0'">
                          NONE
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:for-each select="a:ContactMethod">
                            <xsl:value-of select="a:Telephone/a:FormattedNumber"/>
                            <br/>
                          </xsl:for-each>
                        </xsl:otherwise>
                      </xsl:choose>
                    </td>
                    <td  style="background-color:#fff;" class="end">
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                    </td>
                    <!--</tr>-->
                  </tr>
                </xsl:when>
                <xsl:otherwise>
                  <tr>
                    <td class="begin">
                      <xsl:value-of select="(position() )" />
                    </td>
                    <td >
                      <xsl:value-of select="a:PersonName/a:FamilyName" />, <xsl:value-of select="a:PersonName/a:GivenName" /><br/>
                      SSN : xxx-xx-<xsl:value-of select="substring(a:DemographicDetail/a:GovernmentId,6,4)" />
                      <br/>
                      DOB : <xsl:value-of select="a:DemographicDetail/a:DateOfBirth" />
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;</xsl:text>
                      Age : <xsl:value-of select="a:DemographicDetail/a:Age" />
                    </td>
                    <td>
                      <xsl:value-of select="a:PostalAddress/a:DeliveryAddress/a:AddressLine" />
                      <xsl:text> </xsl:text>
                      <br />

                      <xsl:value-of select="a:PostalAddress/a:Municipality" />
                      <xsl:text>, </xsl:text>
                      <xsl:value-of select="a:PostalAddress/a:Region" />
                      <xsl:text> </xsl:text>
                      <br />
                      <xsl:value-of select="a:PostalAddress/a:County" />
                      <xsl:text> </xsl:text>
                      <br />

                      <xsl:value-of select="a:PostalAddress/a:CountryCode" />
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="a:PostalAddress/a:PostalCode" />
                      <br/>
                      <xsl:value-of select="a:EffectiveDate/a:StartDate" />
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;-&amp;nbsp;</xsl:text>
                      <xsl:value-of select="a:EffectiveDate/a:EndDate" />
                    </td>
                    <td style="border-right: none;">
                      <xsl:choose>
                        <xsl:when test="count(a:ContactMethod)='0'">
                          NONE
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:for-each select="a:ContactMethod">
                            <xsl:value-of select="a:Telephone/a:FormattedNumber"/>
                            <br/>
                          </xsl:for-each>
                        </xsl:otherwise>
                      </xsl:choose>
                    </td>
                    <td class="end">
                      <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                    </td>
                  </tr>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>
          </tbody>
          <tfoot>
            <td class="begin"></td>
            <td></td>
            <td></td>
            <td style="border-right: none;"></td>
            <td class="end"></td>
          </tfoot>
        </table>
      </xsl:if>

    </center>

  </xsl:template>
</xsl:stylesheet>