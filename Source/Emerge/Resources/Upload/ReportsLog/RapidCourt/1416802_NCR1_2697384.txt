
Request (9/27/2016 4:12:57 PM):RunReport(GeneralService ObjGeneralService)

<?xml version='1.0' encoding='UTF-8'?><BackgroundCheck	xmlns="http://ns.hr-xml.org/2004-08-02" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://ns.hr-xml.org/2004-08-02 BackgroundCheck.xsd"><BackgroundSearchPackage><PersonalData><PersonName><GivenName>Gloria</GivenName><MiddleName></MiddleName><FamilyName>Bailey</FamilyName></PersonName><DemographicDetail><DateOfBirth>1967-03-06</DateOfBirth><GovernmentId countryCode='US' issuingAuthority='SSN'>463298294</GovernmentId></DemographicDetail></PersonalData><Screenings><Screening type='criminal'><ReferenceId><IdValue>1416802555_NCR1</IdValue></ReferenceId><Region>COMP</Region><AdditionalItems type='dobnulls' ><Text>False</Text></AdditionalItems></Screening></Screenings></BackgroundSearchPackage></BackgroundCheck>

Response (9/27/2016 4:13:04 PM):RunReport(GeneralService ObjGeneralService)

<?xml version="1.0" encoding="ISO-8859-1"?>
<BackgroundReports xmlns="http://ns.hr-xml.org/2004-08-02" account="" password="" userId="">
    <BackgroundReportPackage>
        <ScreeningsSummary>
            <PersonalData>
                <PersonName>
                    <GivenName>Gloria</GivenName>
                    <MiddleName/>
                    <FamilyName>Bailey</FamilyName>
                </PersonName>
                <DemographicDetail>
                    <DateOfBirth>1967-03-06</DateOfBirth>
                    <GovernmentId countryCode="US" issuingAuthority="SSN">463298294</GovernmentId>
                </DemographicDetail>
            </PersonalData>
        </ScreeningsSummary>
        <Screenings>
            <Screening qualifier="" type="criminal">
                <ClientReferenceId>
                    <IdValue>1416802555_NCR1</IdValue>
                </ClientReferenceId>
                <Region>COMP</Region>
                <ScreeningResults mediaType="txt" resultType="report" type="result">
                    <Text>AOC - TEXAS-DALLAS                                                                 
BAILEY,GLORIA                                                S=F R=B DOB=03/06/1967
ADDR: 4635 SILVERSPRING DALLAS TX                                                  
OFFENSES:                                                                          
COURT:                                                                             
DOCKET NUM: F-9901948                                                              
    ARST: SEDD 1,500                                                               
    CHRG: SEDD 1,500                                       OFFENSE DATE: 09/17/1997
    DISP: SEDD 1,500                          NONADJUDICATION OF GUI  ON 01/27/2000
    DISP: SEDD 1,500                          DISMISSED               ON 01/20/2006
    DISP: SEDD 1,500                          CHARGE DISMISSED        ON 01/20/2006
    SENT: FINE: 300.00                                                             
    SENT: PROBATION: 5Y                                                            
                                                                                  

AOC - TEXAS-DALLAS                                                                 
BAILEY,GLORIA                                                S=F R=B DOB=03/06/1967
ADDR: 18040 KELLY BLVD DALLAS TX                                                   
OFFENSES:                                                                          
COURT:                                                                             
DOCKET NUM: M-9804924                                                              
    ARST: THEFT CK 20                                                              
    CHRG: THEFT CK 20                                      OFFENSE DATE: 06/09/1997
    DISP: THEFT CK 20                         CHARGE DISMISSED        ON 02/08/2000
                                                                                  

DPS - TEXAS                                                                        
BAILEY,GLORIA                               S=F R=B HGT=5'0&amp;quot; WGT=180 DOB=03/06/1967
OFFENSES:                                                                          
COURT: CRIMINAL DISTRICT COURT DALLAS                                              
DOCKET NUM: F-9901948                                                              
  DISPOSED: 01/27/2000                                                             
COURT: CRIMINAL DISTRICT COURT DALLAS                                              
DOCKET NUM: F-9901948                                                              
  DISPOSED: 01/20/2006                                                             
    ARST: (F)SECURE EXECUTION DOC DECEPT&amp;gt;=$1,500&amp;amp;LT;$20           ARRESTED: 11/03/1999
      BOOKING#: 0044555040                                                         
      AGENCY: TX0570000                                                            
      CODE/STATUTE: 32.46(B)(4)                                                    
    CHRG: (F)SECURE EXECUTION DOC DECEPT&amp;gt;=$1,500&amp;amp;LT;$20K      OFFENSE DATE: (N/A)     
      CODE/STATUTE: 32.46(B)(4)                                                    
    DISP: (F)SECURE EXECUTION DOC DECEPT&amp;gt;=$1,50  DEFERRED                ON 01/27/2000
      CODE/STATUTE: 32.46(B)(4)                                                    
      SENT: COURT COSTS: 436.00                                EFFECTIVE: 01/27/2000
      SENT: FINE: 300.00                                       EFFECTIVE: 01/27/2000
      SENT: PROBATION: 5Y                                      EFFECTIVE: 01/27/2000
      SENTENCE DATE: 01/27/2000                                                    
    CHRG: (F)SECURE EXECUTION DOC DECEPT&amp;gt;=$1,500&amp;amp;LT;$20K      OFFENSE DATE: (N/A)     
      CODE/STATUTE: 32.46(B)(4)                                                    
    DISP: (F)SECURE EXECUTION DOC DECEPT&amp;gt;=$1,50  DISMISSED               ON 01/20/2006
      CODE/STATUTE: 32.46(B)(4)                                                    
      SENTENCE DATE: 01/20/2006                                                    
                                                                                  

DOC - TEXAS-PROBATION                                                              
BAILEY,GLORIA                                                S=F R=B DOB=03/06/1967
STATE ID: 06532640                                                                 
OFFENSES:                                                                          
    CHRG: (F)SECURE EXECUTION OF DOC BY DECEPTION &amp;gt;=$1,500&amp;amp;LT OFFENSE DATE: (N/A)  
    CHRG: (F)SECURE EXECUTION DOC DECEPT &amp;gt;=$1,500&amp;amp;LT;$20K     OFFENSE DATE: 09/17/1997
    DISP: (F)SECURE EXECUTION DOC DECEPT &amp;gt;=$1,5  CONVICTED                         
    DISP: (F)SECURE EXECUTION OF DOC BY DECEPTI  CONVICTED                         
    SENT: SENTENCE: 5Y                                        EFFECTIVE: 01/27/2000
EVENTS:                                                                            
    01/27/2000: PLACEMENT DATE                                                     
    01/27/2005: PROJECTED TERMINATION DATE                                         
    OFFENSE LOCATION: DALLAS                                                       
    PLACE OF BIRTH: TX                                                             
    SUPERVISING COUNTY: LAMAR                                                      
    SUPERVISING COUNTY: DALLAS                                                     
                                                                                  

SECURITY WATCH LIST - EXCLUDED PARTIES LIST                                        
BAILEY,GLORIA,JEAN ALEXANDER C                                  S=U R=U DOB=UNKNOWN
ADDR: TULSA OK 74132 USA                                                           
OFFENSES:                                                                          
EVENTS:                                                                            
    03/20/2008: ACTIVE DATE                                                        
    AGENCY: HHS                                                                    
    CLASSIFICATION: INDIVIDUAL                                                     
    CT CODE: Z1                                                                    
    EXCLUSION TYPE: PROHIBITION/RESTRICTION                                        
    EXCLUSION-PROGRAM: RECIPROCAL                                                  
    SAM-NUMBER: S4MR3PXJP                                                          
    TERMINATION DATE: INDEFINITE                                                   
        EXCLUDED BY THE DEPARTMENT OF HEALTH AND HUMAN SERVICES FROM               
        PARTICIPATION IN ALL FEDERAL HEALTH CARE PROGRAMS PURSUANT TO 42           
        U.S.C. ? 1320A-7 OR OTHER SECTIONS OF THE SOCIAL SECURITY ACT,             
        AS AMENDED AND CODIFIED IN CHAPTER 7 OF TITLE 42 OF THE UNITED             
        STATES CODE (THE SCOPE AND EFFECT OF FEDERAL HEALTH CARE PROGRAM           
        EXCLUSIONS IS DESCRIBED IN 42 C.F.R. ? 1001.1901).                         
                                                                                   
                                                                                  </Text>
                </ScreeningResults>
                <CriminalReport>
                    <CriminalCase>
                        <AgencyReference type="Docket">
                            <IdValue>F-9901948</IdValue>
                        </AgencyReference>
                        <AgencyReference type="Booking">
                            <IdValue>0044555040</IdValue>
                        </AgencyReference>
                        <AgencyReference type="ArrestingAgency">
                            <IdValue>TX0570000</IdValue>
                        </AgencyReference>
                        <CourtName>DPS - TEXAS - CRIMINAL DISTRICT COURT DALLAS</CourtName>
                        <SubjectIdentification>
                            <PersonName>
                                <GivenName>GLORIA</GivenName>
                                <FamilyName primary="true">BAILEY</FamilyName>
                            </PersonName>
                            <DemographicDetail>
                                <DateOfBirth>1967-03-06T00:00:00</DateOfBirth>
                                <Race>BLACK</Race>
                                <GenderCode>2</GenderCode>
                                <EyeColor/>
                                <HairColor>Black</HairColor>
                                <Height unitOfMeasure="inches">60</Height>
                                <Weight unitOfMeasure="pounds">180</Weight>
                            </DemographicDetail>
                        </SubjectIdentification>
                        <Charge>
                            <ChargeId>
                                <IdValue>A002:A</IdValue>
                            </ChargeId>
                            <ChargeOrComplaint>SECURE EXECUTION DOC DECEPT&gt;=$1,500&lt;$20K (STATUTE: 32.46(B)(4))</ChargeOrComplaint>
                            <ChargeTypeClassification>felony</ChargeTypeClassification>
                            <ArrestDate>1999-11-03T00:00:00</ArrestDate>
                            <Sentence>SENTENCE DATE: 01/27/2000 TYPE: COURT COSTS: 436.00</Sentence>
                            <Sentence>SENTENCE DATE: 01/27/2000 TYPE: FINE: 300.00</Sentence>
                            <Sentence>SENTENCE DATE: 01/27/2000 TYPE: PROBATION: TERM: 5Y</Sentence>
                            <Disposition>DEFERRED</Disposition>
                            <DispositionDate>2006-01-20T00:00:00</DispositionDate>
                            <DispositionDate>2000-01-27T00:00:00</DispositionDate>
                            <Comment type="ADDITIONAL EVENT">01/27/2000: SENTENCE DATE</Comment>
                        </Charge>
                        <Charge>
                            <ChargeId>
                                <IdValue>A002:B</IdValue>
                            </ChargeId>
                            <ChargeOrComplaint>SECURE EXECUTION DOC DECEPT&gt;=$1,500&lt;$20K (STATUTE: 32.46(B)(4))</ChargeOrComplaint>
                            <ChargeTypeClassification>felony</ChargeTypeClassification>
                            <Disposition>DISMISSED</Disposition>
                            <DispositionDate>2006-01-20T00:00:00</DispositionDate>
                            <DispositionDate>2000-01-27T00:00:00</DispositionDate>
                            <Comment type="ADDITIONAL EVENT">01/20/2006: SENTENCE DATE</Comment>
                        </Charge>
                    </CriminalCase>
                    <CriminalCase>
                        <AgencyReference type="Docket">
                            <IdValue>F-9901948</IdValue>
                        </AgencyReference>
                        <CourtName>AOC - TEXAS-DALLAS</CourtName>
                        <SubjectIdentification>
                            <PersonName>
                                <GivenName>GLORIA</GivenName>
                                <FamilyName primary="true">BAILEY</FamilyName>
                            </PersonName>
                            <PostalAddress type="undefined">
                                <Region>TX</Region>
                                <Municipality>DALLAS</Municipality>
                                <DeliveryAddress>4635 SILVERSPRING</DeliveryAddress>
                            </PostalAddress>
                            <DemographicDetail>
                                <DateOfBirth>1967-03-06T00:00:00</DateOfBirth>
                                <Race>BLACK</Race>
                                <GenderCode>2</GenderCode>
                            </DemographicDetail>
                        </SubjectIdentification>
                        <Charge>
                            <ChargeOrComplaint>SEDD 1,500</ChargeOrComplaint>
                            <ChargeTypeClassification>unknown</ChargeTypeClassification>
                            <ChargeDate>1999-08-23T00:00:00</ChargeDate>
                            <OffenseDate>1997-09-17T00:00:00</OffenseDate>
                            <Sentence>TYPE: FINE: 300.00</Sentence>
                            <Sentence>TYPE: PROBATION: TERM: 5Y</Sentence>
                            <Disposition>UNKNOWN</Disposition>
                        </Charge>
                    </CriminalCase>
                    <CriminalCase>
                        <AgencyReference type="Docket">
                            <IdValue>M-9804924</IdValue>
                        </AgencyReference>
                        <CourtName>AOC - TEXAS-DALLAS</CourtName>
                        <SubjectIdentification>
                            <PersonName>
                                <GivenName>GLORIA</GivenName>
                                <FamilyName primary="true">BAILEY</FamilyName>
                            </PersonName>
                            <PostalAddress type="undefined">
                                <Region>TX</Region>
                                <Municipality>DALLAS</Municipality>
                                <DeliveryAddress>18040 KELLY BLVD</DeliveryAddress>
                            </PostalAddress>
                            <DemographicDetail>
                                <DateOfBirth>1967-03-06T00:00:00</DateOfBirth>
                                <Race>BLACK</Race>
                                <GenderCode>2</GenderCode>
                            </DemographicDetail>
                        </SubjectIdentification>
                        <Charge>
                            <ChargeOrComplaint>THEFT CK 20</ChargeOrComplaint>
                            <ChargeTypeClassification>unknown</ChargeTypeClassification>
                            <ChargeDate>1998-07-01T00:00:00</ChargeDate>
                            <OffenseDate>1997-06-09T00:00:00</OffenseDate>
                            <Disposition>CHARGE DISMISSED</Disposition>
                            <DispositionDate>2000-02-08T00:00:00</DispositionDate>
                        </Charge>
                    </CriminalCase>
                    <CriminalCase>
                        <AgencyReference type="StateId">
                            <IdValue>06532640</IdValue>
                        </AgencyReference>
                        <CourtName>DOC - TEXAS-PROBATION</CourtName>
                        <SubjectIdentification>
                            <PersonName>
                                <GivenName>GLORIA</GivenName>
                                <FamilyName primary="true">BAILEY</FamilyName>
                            </PersonName>
                            <DemographicDetail>
                                <GovernmentId idSource="STATE">06532640</GovernmentId>
                                <DateOfBirth>1967-03-06T00:00:00</DateOfBirth>
                                <Race>BLACK</Race>
                                <GenderCode>2</GenderCode>
                                <BirthPlace>TX</BirthPlace>
                            </DemographicDetail>
                        </SubjectIdentification>
                        <Charge>
                            <ChargeOrComplaint>SECURE EXECUTION DOC DECEPT&gt;=$1,500&lt;$20K</ChargeOrComplaint>
                            <ChargeTypeClassification>felony</ChargeTypeClassification>
                            <OffenseDate>1997-09-17T00:00:00</OffenseDate>
                            <Sentence>SENTENCE DATE: 01/27/2000 TYPE: SENTENCE: TERM: 5Y</Sentence>
                            <Disposition>UNKNOWN</Disposition>
                        </Charge>
                        <Charge>
                            <ChargeOrComplaint>SECURE EXECUTION OF DOC BY DECEPTION&gt;=$1,500&lt;</ChargeOrComplaint>
                            <ChargeTypeClassification>felony</ChargeTypeClassification>
                            <Sentence>SENTENCE DATE: 01/27/2000 TYPE: SENTENCE: TERM: 5Y</Sentence>
                            <Disposition>UNKNOWN</Disposition>
                        </Charge>
                        <AdditionalItems>
                            <Text>OFFENSE LOCATION: DALLAS</Text>
                        </AdditionalItems>
                        <AdditionalItems>
                            <Text>SUPERVISING COUNTY: LAMAR</Text>
                        </AdditionalItems>
                        <AdditionalItems>
                            <Text>SUPERVISING COUNTY: DALLAS</Text>
                        </AdditionalItems>
                        <AdditionalItems type="ADDITIONAL EVENT">
                            <EffectiveDate>
                                <StartDate>
                                    <AnyDate>2005-01-27T00:00:00</AnyDate>
                                </StartDate>
                            </EffectiveDate>
                            <Text>EVENT: PROJECTED TERMINATION DATE</Text>
                        </AdditionalItems>
                        <AdditionalItems type="ADDITIONAL EVENT">
                            <EffectiveDate>
                                <StartDate>
                                    <AnyDate>2000-01-27T00:00:00</AnyDate>
                                </StartDate>
                            </EffectiveDate>
                            <Text>EVENT: PLACEMENT DATE</Text>
                        </AdditionalItems>
                    </CriminalCase>
                    <CriminalCase>
                        <CourtName>SECURITY WATCH LIST - EXCLUDED PARTIES LIST</CourtName>
                        <SubjectIdentification>
                            <PersonName>
                                <GivenName>GLORIA</GivenName>
                                <MiddleName>JEAN ALEXANDER COX</MiddleName>
                                <FamilyName primary="true">BAILEY</FamilyName>
                            </PersonName>
                            <PostalAddress type="undefined">
                                <CountryCode>USA</CountryCode>
                                <PostalCode>74132</PostalCode>
                                <Region>OK</Region>
                                <Municipality>TULSA</Municipality>
                            </PostalAddress>
                            <DemographicDetail/>
                        </SubjectIdentification>
                        <AdditionalItems>
                            <Text>NOTE : EXCLUDED BY THE DEPARTMENT OF HEALTH AND HUMAN SERVICES FROM PARTICIPATION IN ALL FEDERAL HEALTH CARE PROGRAMS PURSUANT TO 42 U.S.C. ? 1320A-7 OR OTHER SECTIONS OF THE SOCIAL SECURITY ACT, AS AMENDED AND CODIFIED IN CHAPTER 7 OF TITLE 42 OF THE UNITED STATES CODE (THE SCOPE AND EFFECT OF FEDERAL HEALTH CARE PROGRAM EXCLUSIONS IS DESCRIBED IN 42 C.F.R. ? 1001.1901).</Text>
                        </AdditionalItems>
                        <AdditionalItems>
                            <Text>AGENCY: HHS</Text>
                        </AdditionalItems>
                        <AdditionalItems>
                            <Text>CLASSIFICATION: INDIVIDUAL</Text>
                        </AdditionalItems>
                        <AdditionalItems>
                            <Text>CT CODE: Z1</Text>
                        </AdditionalItems>
                        <AdditionalItems>
                            <Text>EXCLUSION-PROGRAM: RECIPROCAL</Text>
                        </AdditionalItems>
                        <AdditionalItems>
                            <Text>EXCLUSION TYPE: PROHIBITION/RESTRICTION</Text>
                        </AdditionalItems>
                        <AdditionalItems>
                            <Text>SAM-NUMBER: S4MR3PXJP</Text>
                        </AdditionalItems>
                        <AdditionalItems>
                            <Text>TERMINATION DATE: INDEFINITE</Text>
                        </AdditionalItems>
                        <AdditionalItems type="ADDITIONAL EVENT">
                            <EffectiveDate>
                                <StartDate>
                                    <AnyDate>2008-03-20T00:00:00</AnyDate>
                                </StartDate>
                            </EffectiveDate>
                            <Text>EVENT: ACTIVE DATE</Text>
                        </AdditionalItems>
                    </CriminalCase>
                </CriminalReport>
                <ScreeningStatus>
                    <OrderStatus>Completed</OrderStatus>
                    <ResultStatus>Hit</ResultStatus>
                    <AdditionalItems>
                        <Text>Data obtained from RapidCourt.com is provided 'as is' without express or implied warranties of any kind from anyone.  This disclaimer includes without limitation, implied warranties of merchantability and fitness for a particular purpose.  RapidCourt.com makes no warranty as to the quality, accuracy, completeness, timeliness and validity of the data or its compliance with any applicable law.  All users are urged to conduct additional qualification and verification measures prior to using this data in any manner.  Furthermore, all users agree to comply with all applicable law (including the FCRA and GLBA) relating to the access and use of this data.  RapidCourt.com is not liable for any claims or damages arising out of or relating to the use of this data.</Text>
                    </AdditionalItems>
                </ScreeningStatus>
            </Screening>
        </Screenings>
    </BackgroundReportPackage>
</BackgroundReports>
