
Request (10/5/2016 9:03:49 PM):RunReport(GeneralService ObjGeneralService)

<?xml version='1.0' encoding='UTF-8'?><BackgroundCheck	xmlns="http://ns.hr-xml.org/2004-08-02" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://ns.hr-xml.org/2004-08-02 BackgroundCheck.xsd"><BackgroundSearchPackage><PersonalData><PersonName><GivenName>Toan</GivenName><MiddleName></MiddleName><FamilyName>Huynh</FamilyName></PersonName><DemographicDetail><DateOfBirth>1987-04-06</DateOfBirth><GovernmentId countryCode='US' issuingAuthority='SSN'>485236826</GovernmentId></DemographicDetail></PersonalData><Screenings><Screening type='criminal'><ReferenceId><IdValue>1442486456_NCR1</IdValue></ReferenceId><Region>COMP</Region><AdditionalItems type='dobnulls' ><Text>False</Text></AdditionalItems></Screening></Screenings></BackgroundSearchPackage></BackgroundCheck>

Response (10/5/2016 9:03:55 PM):RunReport(GeneralService ObjGeneralService)

<?xml version="1.0" encoding="ISO-8859-1"?>
<BackgroundReports xmlns="http://ns.hr-xml.org/2004-08-02" account="" password="" userId="">
    <BackgroundReportPackage>
        <ScreeningsSummary>
            <PersonalData>
                <PersonName>
                    <GivenName>Toan</GivenName>
                    <MiddleName/>
                    <FamilyName>Huynh</FamilyName>
                </PersonName>
                <DemographicDetail>
                    <DateOfBirth>1987-04-06</DateOfBirth>
                    <GovernmentId countryCode="US" issuingAuthority="SSN">485236826</GovernmentId>
                </DemographicDetail>
            </PersonalData>
        </ScreeningsSummary>
        <Screenings>
            <Screening qualifier="" type="criminal">
                <ClientReferenceId>
                    <IdValue>1442486456_NCR1</IdValue>
                </ClientReferenceId>
                <Region>COMP</Region>
                <ScreeningResults mediaType="txt" resultType="report" type="result">
                    <Text>SECURITY WATCH LIST - CA MEDICAL SUSPENDED                                         
HUYNH,TOAN,PHUOC                                                S=U R=U DOB=UNKNOWN
ADDR: 13403 LITTLE DAWN LANE POWAY CA                                              
ADDR: 9225 MIRA MESA BLVD. STE. #108 SAN DIEGO CA                                  
OFFENSES:                                                                          
EVENTS:                                                                            
    04/20/2006: SUSPENSION DATE (INDEFINITELY EFFECTIVE)                           
    LICENSE NUMBER: 45099                                                          
    TYPE: PHARMACY                                                                 
                                                                                  

SECURITY WATCH LIST - EXCLUDED PARTIES LIST                                        
HUYNH,TOAN,PHUOC                                                S=U R=U DOB=UNKNOWN
ADDR: POWAY CA 92064 USA                                                           
OFFENSES:                                                                          
EVENTS:                                                                            
    06/21/2006: ACTIVE DATE                                                        
    AGENCY: OPM                                                                    
    CLASSIFICATION: INDIVIDUAL                                                     
    CT CODE: Z2                                                                    
    EXCLUSION TYPE: PROHIBITION/RESTRICTION                                        
    EXCLUSION-PROGRAM: RECIPROCAL                                                  
    SAM-NUMBER: S4MR3R1K8                                                          
    TERMINATION DATE: INDEFINITE                                                   
                                                                                  

SECURITY WATCH LIST - EXCLUDED PARTIES LIST                                        
HUYNH,TOAN,PHUOC                                                S=U R=U DOB=UNKNOWN
ADDR: POWAY CA 92064 USA                                                           
OFFENSES:                                                                          
EVENTS:                                                                            
    04/20/2006: ACTIVE DATE                                                        
    AGENCY: HHS                                                                    
    CLASSIFICATION: INDIVIDUAL                                                     
    CT CODE: Z1                                                                    
    EXCLUSION TYPE: PROHIBITION/RESTRICTION                                        
    EXCLUSION-PROGRAM: RECIPROCAL                                                  
    SAM-NUMBER: S4MR3NSYP                                                          
    TERMINATION DATE: INDEFINITE                                                   
        EXCLUDED BY THE DEPARTMENT OF HEALTH AND HUMAN SERVICES FROM               
        PARTICIPATION IN ALL FEDERAL HEALTH CARE PROGRAMS PURSUANT TO 42           
        U.S.C. ? 1320A-7 OR OTHER SECTIONS OF THE SOCIAL SECURITY ACT,             
        AS AMENDED AND CODIFIED IN CHAPTER 7 OF TITLE 42 OF THE UNITED             
        STATES CODE (THE SCOPE AND EFFECT OF FEDERAL HEALTH CARE PROGRAM           
        EXCLUSIONS IS DESCRIBED IN 42 C.F.R. ? 1001.1901).                         
                                                                                   
                                                                                  </Text>
                </ScreeningResults>
                <CriminalReport>
                    <CriminalCase>
                        <CourtName>SECURITY WATCH LIST - EXCLUDED PARTIES LIST</CourtName>
                        <SubjectIdentification>
                            <PersonName>
                                <GivenName>TOAN</GivenName>
                                <MiddleName>PHUOC</MiddleName>
                                <FamilyName primary="true">HUYNH</FamilyName>
                            </PersonName>
                            <PostalAddress type="undefined">
                                <CountryCode>USA</CountryCode>
                                <PostalCode>92064</PostalCode>
                                <Region>CA</Region>
                                <Municipality>POWAY</Municipality>
                            </PostalAddress>
                            <DemographicDetail/>
                        </SubjectIdentification>
                        <AdditionalItems>
                            <Text>AGENCY: OPM</Text>
                        </AdditionalItems>
                        <AdditionalItems>
                            <Text>CLASSIFICATION: INDIVIDUAL</Text>
                        </AdditionalItems>
                        <AdditionalItems>
                            <Text>CT CODE: Z2</Text>
                        </AdditionalItems>
                        <AdditionalItems>
                            <Text>EXCLUSION-PROGRAM: RECIPROCAL</Text>
                        </AdditionalItems>
                        <AdditionalItems>
                            <Text>EXCLUSION TYPE: PROHIBITION/RESTRICTION</Text>
                        </AdditionalItems>
                        <AdditionalItems>
                            <Text>SAM-NUMBER: S4MR3R1K8</Text>
                        </AdditionalItems>
                        <AdditionalItems>
                            <Text>TERMINATION DATE: INDEFINITE</Text>
                        </AdditionalItems>
                        <AdditionalItems type="ADDITIONAL EVENT">
                            <EffectiveDate>
                                <StartDate>
                                    <AnyDate>2006-06-21T00:00:00</AnyDate>
                                </StartDate>
                            </EffectiveDate>
                            <Text>EVENT: ACTIVE DATE</Text>
                        </AdditionalItems>
                    </CriminalCase>
                    <CriminalCase>
                        <CourtName>SECURITY WATCH LIST - EXCLUDED PARTIES LIST</CourtName>
                        <SubjectIdentification>
                            <PersonName>
                                <GivenName>TOAN</GivenName>
                                <MiddleName>PHUOC</MiddleName>
                                <FamilyName primary="true">HUYNH</FamilyName>
                            </PersonName>
                            <PostalAddress type="undefined">
                                <CountryCode>USA</CountryCode>
                                <PostalCode>92064</PostalCode>
                                <Region>CA</Region>
                                <Municipality>POWAY</Municipality>
                            </PostalAddress>
                            <DemographicDetail/>
                        </SubjectIdentification>
                        <AdditionalItems>
                            <Text>NOTE : EXCLUDED BY THE DEPARTMENT OF HEALTH AND HUMAN SERVICES FROM PARTICIPATION IN ALL FEDERAL HEALTH CARE PROGRAMS PURSUANT TO 42 U.S.C. ? 1320A-7 OR OTHER SECTIONS OF THE SOCIAL SECURITY ACT, AS AMENDED AND CODIFIED IN CHAPTER 7 OF TITLE 42 OF THE UNITED STATES CODE (THE SCOPE AND EFFECT OF FEDERAL HEALTH CARE PROGRAM EXCLUSIONS IS DESCRIBED IN 42 C.F.R. ? 1001.1901).</Text>
                        </AdditionalItems>
                        <AdditionalItems>
                            <Text>AGENCY: HHS</Text>
                        </AdditionalItems>
                        <AdditionalItems>
                            <Text>CLASSIFICATION: INDIVIDUAL</Text>
                        </AdditionalItems>
                        <AdditionalItems>
                            <Text>CT CODE: Z1</Text>
                        </AdditionalItems>
                        <AdditionalItems>
                            <Text>EXCLUSION-PROGRAM: RECIPROCAL</Text>
                        </AdditionalItems>
                        <AdditionalItems>
                            <Text>EXCLUSION TYPE: PROHIBITION/RESTRICTION</Text>
                        </AdditionalItems>
                        <AdditionalItems>
                            <Text>SAM-NUMBER: S4MR3NSYP</Text>
                        </AdditionalItems>
                        <AdditionalItems>
                            <Text>TERMINATION DATE: INDEFINITE</Text>
                        </AdditionalItems>
                        <AdditionalItems type="ADDITIONAL EVENT">
                            <EffectiveDate>
                                <StartDate>
                                    <AnyDate>2006-04-20T00:00:00</AnyDate>
                                </StartDate>
                            </EffectiveDate>
                            <Text>EVENT: ACTIVE DATE</Text>
                        </AdditionalItems>
                    </CriminalCase>
                    <CriminalCase>
                        <CourtName>SECURITY WATCH LIST - CA MEDICAL SUSPENDED LIST</CourtName>
                        <SubjectIdentification>
                            <PersonName>
                                <GivenName>TOAN</GivenName>
                                <MiddleName>PHUOC</MiddleName>
                                <FamilyName primary="true">HUYNH</FamilyName>
                            </PersonName>
                            <PostalAddress type="undefined">
                                <Region>CA</Region>
                                <Municipality>POWAY</Municipality>
                                <DeliveryAddress>13403 LITTLE DAWN LANE</DeliveryAddress>
                            </PostalAddress>
                            <PostalAddress type="undefined">
                                <Region>CA</Region>
                                <Municipality>SAN DIEGO</Municipality>
                                <DeliveryAddress>9225 MIRA MESA BLVD.   STE. #108</DeliveryAddress>
                            </PostalAddress>
                            <DemographicDetail/>
                        </SubjectIdentification>
                        <AdditionalItems>
                            <Text>LICENSE NUMBER: 45099</Text>
                        </AdditionalItems>
                        <AdditionalItems>
                            <Text>TYPE: PHARMACY</Text>
                        </AdditionalItems>
                        <AdditionalItems type="ADDITIONAL EVENT">
                            <EffectiveDate>
                                <StartDate>
                                    <AnyDate>2006-04-20T00:00:00</AnyDate>
                                </StartDate>
                            </EffectiveDate>
                            <Text>EVENT: SUSPENSION DATE: INDEFINITELY EFFECTIVE</Text>
                        </AdditionalItems>
                    </CriminalCase>
                </CriminalReport>
                <ScreeningStatus>
                    <OrderStatus>Completed</OrderStatus>
                    <ResultStatus>Hit</ResultStatus>
                    <AdditionalItems>
                        <Text>Data obtained from RapidCourt.com is provided 'as is' without express or implied warranties of any kind from anyone.  This disclaimer includes without limitation, implied warranties of merchantability and fitness for a particular purpose.  RapidCourt.com makes no warranty as to the quality, accuracy, completeness, timeliness and validity of the data or its compliance with any applicable law.  All users are urged to conduct additional qualification and verification measures prior to using this data in any manner.  Furthermore, all users agree to comply with all applicable law (including the FCRA and GLBA) relating to the access and use of this data.  RapidCourt.com is not liable for any claims or damages arising out of or relating to the use of this data.</Text>
                    </AdditionalItems>
                </ScreeningStatus>
            </Screening>
        </Screenings>
    </BackgroundReportPackage>
</BackgroundReports>
