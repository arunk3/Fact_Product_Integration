﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Emerge.Services;
using Emerge.Data;
using System.IO;
using System.Xml.Serialization;
using System.Text;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using Emerge.Common;
using System.Reflection;
using System.Xml.Linq;

namespace Emerge.Service
{
    /// <summary>
    /// Summary description for EmergeService
    /// </summary>
    [WebService(Namespace = "http://emerge.intelifi.com/WebService")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class EmergeService : System.Web.Services.WebService
    {
        #region Test method

        [WebMethod(Description = "This is testing method and it will return you string.")]
        public string TestMe()
        {
            return "You called me successfully!";
        }
        #endregion

        #region Match Username/Password

        private string MatchUsername(string Username, string Password, out int LocationId, out int CompanyId)
        {
            BALGeneral ObjBALGeneral = new BALGeneral();
            string ErrorMessage = string.Empty;
            CompanyId = 0;
            LocationId = 0;
            try
            {
                if (Username != string.Empty && Password != string.Empty)
                {
                    var Coll = ObjBALGeneral.MatchAPIUsername(Username, Password);
                    if (Coll.Count > 0)
                    {
                        if (Coll.ElementAt(0).EnableAPI == true)
                        {
                            if (Coll.ElementAt(0).APILocation == 0 || Coll.ElementAt(0).APILocation == null)
                            {
                                ErrorMessage = "API location not selected";
                            }
                            else
                            {
                                LocationId = (int)Coll.ElementAt(0).APILocation;
                                CompanyId = Coll.ElementAt(0).pkCompanyId;
                            }
                        }
                        else
                        {
                            ErrorMessage = "API not enabled for this company.";
                        }
                    }
                    else
                    {
                        ErrorMessage = "Invalid Username/password";
                    }
                }
                else
                {
                    ErrorMessage = "Please send Username/password";
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
            }
            return ErrorMessage;
        }


        #endregion

        #region Comment Code

        //[WebMethod]
        //public List<PackageList> GetPackList(string APIUsername, string APIPassword)
        //{
        //    BALGeneral ObjBALGeneral = new BALGeneral();
        //    List<PackageList> ObjPackageList = new List<PackageList>();
        //    Guid? LocationId = Guid.Empty;
        //    try
        //    {
        //        string Message = MatchUsername(APIUsername, APIPassword, out LocationId);
        //        if (Message == string.Empty)
        //        {
        //            List<Proc_Get_EmergeProductPackagesResult> ObjReports = ObjBALGeneral.GetEmergeProductPackages(new Guid(LocationId.ToString()));
        //            for (int i = 0; i < ObjReports.Count; i++)
        //            {
        //                ObjPackageList.Add(new PackageList { PackageCode = ObjReports.ElementAt(i).PackageCode, PackageName = ObjReports.ElementAt(i).PackageDisplayName, ErrorMessage = string.Empty });
        //            }
        //        }
        //        else
        //        {
        //            ObjPackageList.Add(new PackageList { PackageCode = string.Empty, PackageName = string.Empty, ErrorMessage = Message });
        //        }
        //    }
        //    catch (Exception)
        //    {
        //    }
        //    return ObjPackageList;
        //}

        #endregion

        #region Get Reports

        [WebMethod(Description = "This method returns an array of reports.")]
        public ReportsList GetReports(string APIUsername, string APIPassword)
        {
            BALGeneral ObjBALGeneral = new BALGeneral();
            List<Report> ObjReportsList = new List<Report>();
            ReportsList objList = new ReportsList();
            int LocationId = 0;
            int pkCompanyId = 0;
            try
            {
                string Message = MatchUsername(APIUsername, APIPassword, out LocationId, out pkCompanyId);
                if (Message == string.Empty)
                {

                    List<Proc_Get_NewReportsForLandingPageResult> ObjReports = ObjBALGeneral.GetNewReportsForLandingPage(Int32.Parse(LocationId.ToString())).ToList();
                    for (int i = 0; i < ObjReports.Count; i++)
                    {
                        ObjReportsList.Add(new Report
                        {
                            ReportId = ObjReports.ElementAt(i).pkProductId,
                            ReportCode = ObjReports.ElementAt(i).ProductCode,
                            ReportName = ObjReports.ElementAt(i).ProductDisplayName
                        });
                    }
                    objList.ReportList = ObjReportsList.ToArray();
                    objList.ErrorMessage = string.Empty;
                }
                else
                {
                    objList.ReportList = null;
                    objList.ErrorMessage = string.Empty;
                }
            }
            catch (Exception ex)
            {
                objList.ReportList = null;
                objList.ErrorMessage = ex.Message;
            }
            finally
            {
                ObjBALGeneral = null;
            }
            return objList;
        }

        #endregion

        #region Get Packages

        [WebMethod(Description = "This method returns an array of packages, and error message. If error message is empty, it means method parameters are fine and you will get the result.")]
        public PackagesList GetPackages(string APIUsername, string APIPassword)
        {
            BALGeneral ObjBALGeneral = new BALGeneral();
            List<Package> ObjPackageList = new List<Package>();
            PackagesList ObjPackages = new PackagesList();
            int LocationId = 0;
            int pkCompanyId = 0;
            try
            {
                string Message = MatchUsername(APIUsername, APIPassword, out LocationId, out pkCompanyId);
                if (Message == string.Empty)
                {

                    List<Proc_Get_EmergeProductPackagesResult> ObjReports = ObjBALGeneral.GetEmergeProductPackages(Int32.Parse(LocationId.ToString()));
                    for (int i = 0; i < ObjReports.Count; i++)
                    {
                        int PackageId = Convert.ToInt32(ObjReports.ElementAt(i).pkPackageId);
                        Report[] ObjPackReport = GetPackageReports(LocationId, PackageId);
                        ObjPackageList.Add(new Package
                        {
                            PackageId = PackageId,
                            PackageCode = ObjReports.ElementAt(i).PackageCode,
                            PackageName = ObjReports.ElementAt(i).PackageDisplayName,
                            ReportInclude = ObjReports.ElementAt(i).include,
                            ReportsList = ObjPackReport
                        });
                    }
                    ObjPackages.PackageList = ObjPackageList.ToArray();
                    ObjPackages.ErrorMessage = string.Empty;
                }
                else
                {
                    ObjPackages.PackageList = null;
                    ObjPackages.ErrorMessage = Message;
                }
            }
            catch (Exception)
            {
                ObjPackages.PackageList = null;
                ObjPackages.ErrorMessage = "Some error occurred";
            }
            finally
            {
                ObjBALGeneral = null;
            }

            return ObjPackages;
        }

        private Report[] GetPackageReports(int LocationId, int PackageId)
        {
            BALGeneral ObjGeneral = new BALGeneral();
            var PackageReports = ObjGeneral.GetEmergeProductPackageReports(LocationId, PackageId);
            List<Report> ObjPackReport = new List<Report>();
            for (int irow = 0; irow < PackageReports.Count; irow++)
            {
                Report ObjRep = new Report();
                ObjRep.ReportId = PackageReports.ElementAt(irow).pkProductId;
                ObjRep.ReportCode = PackageReports.ElementAt(irow).ProductCode;
                ObjRep.ReportName = PackageReports.ElementAt(irow).ProductDisplayName;
                ObjPackReport.Add(ObjRep);
            }

            ObjGeneral = null;
            return ObjPackReport.ToArray();
        }

        #endregion

        #region Get States

        [WebMethod(Description = "This method returns an array of states.")]
        public State[] GetStates()
        {
            BALGeneral ObjBALGeneral = new BALGeneral();
            List<State> ObjStates = new List<State>();
            try
            {

                ObjStates = (from db in ObjBALGeneral.GetStates()
                             select new State
                             {
                                 StateId = db.pkStateId,
                                 StateName = db.StateName,
                                 StateCode = db.StateCode,
                                 IsLiveRunner = db.IsStateLiveRunner
                             }).ToList();


            }
            catch //(Exception ex)
            {
                
            }
            finally
            {
                ObjBALGeneral = null;
            }

            return ObjStates.ToArray();


        }

        #endregion

        #region Get Counties

        [WebMethod(Description = "This method returns an array of counties.")]
        public County[] GetCounty(string GetStateId)
        {

            BALGeneral ObjBALGeneral = new BALGeneral();
            //var EmptyRec = 1;

            List<County> ObjCounty = new List<County>();
            //ReferenceCodesList ObjReferenceCodes = new ReferenceCodesList();    
            try
            {
                int IsNumeric = 0;
                int.TryParse(GetStateId, out IsNumeric);
                if (IsNumeric > 0)
                {
                    int StateId = Convert.ToInt32(GetStateId);
                    ObjCounty = (from db in ObjBALGeneral.GetStatesCounty(StateId)
                                 select new County
                                 {
                                     CountyId = db.pkCountyId,
                                     CountyName = db.CountyName,
                                 }).ToList();
                    //EmptyRec = 2;
                    if (ObjCounty.Count == 0)
                    {
                        ObjCounty.Add(new County { CountyId = 0, CountyName = "No County available for this state" });
                        //EmptyRec = 3;

                    }
                }
                else
                {
                    ObjCounty.Add(new County { CountyId = 0, CountyName = "Invalid State Id" });             
                }
               
              
            }
            catch (Exception ex)
            {
                ObjCounty.Add(new County { CountyId = 0, CountyName = ex.Message.ToString() });             
            
               
               
            }
            finally
            {
                ObjBALGeneral = null;
               
                
            }

            return ObjCounty.ToArray();

        }

        #endregion

        #region Get Suffix

        [WebMethod(Description = "This method returns an array of suffix.")]
        public Suffix[] GetSuffix()
        {
            List<Suffix> ObjSuffix = new List<Suffix>();
          //  ObjSuffix.Add(new Suffix { Text = "", Value = "-1" });
            ObjSuffix.Add(new Suffix { Text = "JR", Value = "JR" });
            ObjSuffix.Add(new Suffix { Text = "SR", Value = "SR" });
            ObjSuffix.Add(new Suffix { Text = "I", Value = "I" });
            ObjSuffix.Add(new Suffix { Text = "II", Value = "II" });
            ObjSuffix.Add(new Suffix { Text = "III", Value = "III" });
            ObjSuffix.Add(new Suffix { Text = "IV", Value = "IV" });
            ObjSuffix.Add(new Suffix { Text = "V", Value = "V" });
            return ObjSuffix.ToArray();
        }

        #endregion

        #region Get Direction

        [WebMethod(Description = "This method returns an array of directions.")]
        public Direction[] GetDirection()
        {
            List<Direction> ObjDirection = new List<Direction>();
          //  ObjDirection.Add(new Direction { Text = "", Value = "-1" });
            ObjDirection.Add(new Direction { Text = "E", Value = "E" });
            ObjDirection.Add(new Direction { Text = "N", Value = "N" });
            ObjDirection.Add(new Direction { Text = "NE", Value = "NE" });
            ObjDirection.Add(new Direction { Text = "NW", Value = "NW" });
            ObjDirection.Add(new Direction { Text = "S", Value = "S" });
            ObjDirection.Add(new Direction { Text = "SE", Value = "SE" });
            ObjDirection.Add(new Direction { Text = "SW", Value = "SW" });
            ObjDirection.Add(new Direction { Text = "W", Value = "W" });
            return ObjDirection.ToArray();
        }

        #endregion

        #region Get Type

        [WebMethod(Description = "This method returns an array of Types.")]
        public Type[] GetTypes()
        {
            List<Type> ObjType = new List<Type>();
         //   ObjType.Add(new Type { Text = "", Value = "-1" });
            ObjType.Add(new Type { Text = "", Value = "None" });
            ObjType.Add(new Type { Text = "Alley", Value = "AL" });
            ObjType.Add(new Type { Text = "Avenue", Value = "AV" });
            ObjType.Add(new Type { Text = "Boulevard", Value = "BV" });
            ObjType.Add(new Type { Text = "Building", Value = "BD" });
            ObjType.Add(new Type { Text = "Center", Value = "CN" });
            ObjType.Add(new Type { Text = "Circle", Value = "CI" });
            ObjType.Add(new Type { Text = "Court", Value = "CT" });
            ObjType.Add(new Type { Text = "Crescent", Value = "CS" });
            ObjType.Add(new Type { Text = "Dale", Value = "DA" });
            ObjType.Add(new Type { Text = "Drive", Value = "DR" });
            ObjType.Add(new Type { Text = "Expressway", Value = "EX" });
            ObjType.Add(new Type { Text = "Freeway", Value = "FY" });
            ObjType.Add(new Type { Text = "Garden", Value = "GA" });
            ObjType.Add(new Type { Text = "Grove", Value = "GR" });
            ObjType.Add(new Type { Text = "Heights", Value = "HT" });
            ObjType.Add(new Type { Text = "Highway", Value = "HY" });
            ObjType.Add(new Type { Text = "Hill", Value = "HL" });
            ObjType.Add(new Type { Text = "Knoll", Value = "KN" });
            ObjType.Add(new Type { Text = "Lane", Value = "LN" });
            ObjType.Add(new Type { Text = "Mall", Value = "MA" });
            ObjType.Add(new Type { Text = "Oval", Value = "OV" });
            ObjType.Add(new Type { Text = "Park", Value = "PK" });
            ObjType.Add(new Type { Text = "Parkway", Value = "PY" });
            ObjType.Add(new Type { Text = "Path", Value = "PA" });
            ObjType.Add(new Type { Text = "Pike", Value = "PI" });
            ObjType.Add(new Type { Text = "Place", Value = "PL" });
            ObjType.Add(new Type { Text = "Plaza", Value = "PZ" });
            ObjType.Add(new Type { Text = "Point", Value = "PT" });
            ObjType.Add(new Type { Text = "Road", Value = "RD" });
            ObjType.Add(new Type { Text = "Route", Value = "RT" });
            ObjType.Add(new Type { Text = "Row", Value = "RO" });
            ObjType.Add(new Type { Text = "Run", Value = "RN" });
            ObjType.Add(new Type { Text = "Rural Route", Value = "RR" });
            ObjType.Add(new Type { Text = "Square", Value = "SQ" });
            ObjType.Add(new Type { Text = "Street", Value = "ST" });
            ObjType.Add(new Type { Text = "Terrace", Value = "TC" });
            ObjType.Add(new Type { Text = "Thruway", Value = "TY" });
            ObjType.Add(new Type { Text = "Trail", Value = "TR" });
            ObjType.Add(new Type { Text = "Turnpike", Value = "TP" });
            ObjType.Add(new Type { Text = "Viaduct", Value = "VI" });
            ObjType.Add(new Type { Text = "View", Value = "VW" });
            ObjType.Add(new Type { Text = "Walk", Value = "WK" });
            ObjType.Add(new Type { Text = "Way", Value = "WY" });

            return ObjType.ToArray();
        }

        #endregion

        #region Gender

        [WebMethod(Description = "This method returns an array of Gender.")]
        public Gender[] GetGender()
        {
            List<Gender> ObjGender = new List<Gender>();
          //  ObjGender.Add(new Gender { Text = "", Value = "-1" });
            ObjGender.Add(new Gender { Text = "Male", Value = "Male" });
            ObjGender.Add(new Gender { Text = "Female", Value = "Female" });
            return ObjGender.ToArray();
        }

        #endregion

        #region Get Jurisdiction

        [WebMethod(Description = "This method returns an array of Jurisdictions.")]
        public Jurisdiction[] GetJurisdiction()
        {
            BALGeneral ObjBALGeneral = new BALGeneral();
            List<Jurisdiction> ObjJurisdiction = new List<Jurisdiction>();
            try
            {

                ObjJurisdiction = (from db in ObjBALGeneral.GetJurisdictions()
                                   select new Jurisdiction
                                   {
                                       JurisdictionId = db.pkJurisdictionId,
                                       JurisdictionName = db.JurisdictionName
                                   }).ToList();

            }
            catch //(Exception ex)
            {
               
            }
            finally
            {
                ObjBALGeneral = null;
            }
            return ObjJurisdiction.ToArray();
        }

        #endregion

        #region Get Reference Code

        [WebMethod(Description = "This method returns an array of Reference Codes , and error message. If error message is empty, it means method parameters are fine and you will get the result.")]
        public ReferenceCodesList GetReferenceCode(string APIUsername, string APIPassword)
        {
            //BALGeneral ObjBALGeneral = new BALGeneral();
            BALCompany ObjBALCompany = new BALCompany();
            List<Code> ObjCodeList = new List<Code>();
            ReferenceCodesList ObjReferenceCodes = new ReferenceCodesList();
            int LocationId = 0;
            int pkCompanyId = 0;
            try
            {
                string Message = MatchUsername(APIUsername, APIPassword, out LocationId, out pkCompanyId);
                if (Message == string.Empty)
                {

                    ObjCodeList = (from db in ObjBALCompany.GetAllReferenceCodes(Int32.Parse(pkCompanyId.ToString()))
                                   select new Code
                                   {
                                       ReferenceCodeId = db.pkReferenceCodeId,
                                       ReferenceCode = db.ReferenceCode

                                   }).ToList();

                    ObjReferenceCodes.ReferenceCodeList = ObjCodeList.ToArray();
                    ObjReferenceCodes.ErrorMessage = string.Empty;
                }
                else
                {
                    ObjReferenceCodes.ReferenceCodeList = null;
                    ObjReferenceCodes.ErrorMessage = Message;
                }
            }
            catch (Exception)
            {
                ObjReferenceCodes.ReferenceCodeList = null;
                ObjReferenceCodes.ErrorMessage = "Some error occurred";
            }
            finally
            {
                //ObjBALGeneral = null;
            }

            return ObjReferenceCodes;
        }


        #endregion

        #region Run Report

        [WebMethod(Description = "This method is used to run report. This method will save the order and return Order Number and message.")]
        public Respose RunReport(string APIUsername, string APIPassword, string Request)
        {

            if (!Request.Contains("xml version"))
            {
                Request = "<?xml version='1.0' encoding='utf-8'?>" + Request;
            }
            BALGeneral ObjBALGeneral = new BALGeneral();
            Respose ObjResponse = new Respose();
            int LocationId = 0;
            int pkCompanyId = 0;
            string CompanyAccountNo = "000000";
            string OrderCompanyName = "Intelifi";
            string PermissiblePurpose = "Employment";
            //Guid CompanyUserId = new Guid("FDFEE5C5-CA52-4499-8233-7DCB7FADAACE"); //we are sending static Company UserId right now. Need to ask from client regarding CompanyUserId
            int CompanyUserId = 2628;
            try
            {
                string Message = MatchUsername(APIUsername, APIPassword, out LocationId, out pkCompanyId);
                if (Message == string.Empty)//Check APIUsername and Password 
                {
                    #region Deserialize Request
                    XmlSerializer serializer = new XmlSerializer(typeof(EmergeReports));
                    EmergeReports ObjXMLReport = new EmergeReports();
                    using (MemoryStream ms = new MemoryStream(ASCIIEncoding.Default.GetBytes(Request)))
                    using (TextReader textWriter = new StreamReader(ms))
                    {
                        ObjXMLReport = (EmergeReports)serializer.Deserialize(textWriter); //Deserialize xml
                    }
                    serializer = null;                    
                    #endregion

                    if (ObjXMLReport.ReferenceCode == null || ObjXMLReport.ReferenceCode == string.Empty)//Check User send Reference Code in XML
                    {
                        ObjResponse.Response = string.Empty;
                        ObjResponse.Message = "Please send reference code";
                    }
                    else
                    {
                        int ReferenceCount = ObjBALGeneral.CountReferenceCode(ObjXMLReport.ReferenceCode);
                        if (ReferenceCount == 0)//Check Reference Code exists in database or not
                        {
                            EmergeReport ObjEmergeReport = new EmergeReport();

                            #region Get Report Name

                            List<string> ObjCollProducts = new List<string>();
                            List<string> Products = new List<string>();

                            ObjCollProducts = ObjXMLReport.ReportInfo;
                            for (int i = 0; i < ObjCollProducts.Count; i++)
                            {

                                if (ObjCollProducts[i] == "NCR2")//Ticket #740: Incomplete report on NCR2 and error log is not shwing #17
                                {
                                    if (ObjXMLReport.GeneralInfo.Social.Trim() == "")
                                    {
                                        ObjXMLReport.GeneralInfo.Social = "999-99-9999";
                                    }
                                }

                                if (ObjCollProducts[i].ToString() == "NCRPLUS")
                                {
                                    ObjCollProducts.RemoveAt(i);
                                    ObjCollProducts.Add("NCR+_0");
                                }
                                else
                                {
                                    Products.Add(ObjCollProducts[i].Trim());
                                    ObjCollProducts[i] = ObjCollProducts[i].Trim() + "_0";
                                }
                            }


                            #region Check Report clients send is included in the location

                            List<Proc_Get_NewReportsForLandingPageResult> ObjReports = ObjBALGeneral.GetNewReportsForLandingPage(Int32.Parse(LocationId.ToString())).ToList();
                            int ValidReportsCount = (from db in ObjReports where Products.Contains(db.ProductCode) select db).Count();

                            #endregion

                            #endregion

                            if (ValidReportsCount == Products.Count) //Check these are valid reports
                            {
                                List<Proc_Get_EmergeProductPackagesResult> ObjPackageReports = ObjBALGeneral.GetEmergeProductPackages(Int32.Parse(LocationId.ToString()));
                                int ValidPackageCount = (from db in ObjPackageReports where ObjXMLReport.PackageInfo.Contains(db.PackageCode) select db).Count();

                                if (ValidPackageCount == ObjXMLReport.PackageInfo.Count)//Check these are valid packages
                                {
                                    #region Get Package Reports

                                    for (int i = 0; i < ObjXMLReport.PackageInfo.Count; i++)
                                    {
                                        var PReports = (from db in ObjPackageReports where db.PackageCode == ObjXMLReport.PackageInfo.ElementAt(i).ToString() select db).ToList();
                                        for (int j = 0; j < PReports.Count(); j++)
                                        {
                                            string[] ReportCode = PReports.ElementAt(j).include.ToString().Split(',');
                                            for (int irep = 0; irep < ReportCode.Length; irep++)
                                            {
                                                if (!Products.Contains(ReportCode[irep].Trim()))
                                                {
                                                    Products.Add(ReportCode[irep].Trim());
                                                    ObjCollProducts.Add(ReportCode[irep].Trim() + "_" + PReports.ElementAt(j).pkPackageId);

                                                }
                                            }
                                        }
                                    }

                                    ObjEmergeReport.ObjCollProducts = ObjCollProducts;
                                    #endregion

                                    if (ObjEmergeReport.ObjCollProducts.Count > 0) //Check User send any package or report code in xml
                                    {
                                        #region Check Required Columns

                                        string ErrMessage = string.Empty;
                                        string ControlValue = string.Empty;
                                        string FilePath = Server.MapPath("~/Content/ProductXml/APIValidation.xml");
                                        XElement doc = XElement.Load(FilePath);

                                        for (int i = 0; i < Products.Count; i++)
                                        {
                                            //Get required fields from xml and check from collection.
                                            var list = (from b in doc.Elements("Product")
                                                        where ((string)b.Attribute("Code").Value == Convert.ToString(Products[i]))
                                                        select b);
                                            if (list.Count() > 0) //Check code exists in xml
                                            {
                                                for (int irow = 0; irow < list.ElementAt(0).Elements("RequiredData").Count(); irow++)
                                                {
                                                    string Control = list.ElementAt(0).Elements("RequiredData").ElementAt(irow).Value;

                                                    if (ObjXMLReport.GeneralInfo != null && ObjXMLReport.GeneralInfo.GetType().GetProperty(Control) != null)
                                                    {
                                                        ControlValue = Convert.ToString(ObjXMLReport.GeneralInfo.GetType().GetProperty(Control).GetValue(ObjXMLReport.GeneralInfo, null));
                                                        ErrMessage = IfEmpty(Products[i], ErrMessage, ControlValue, Control);
                                                    }
                                                    else if (ObjXMLReport.AutomotiveInfo != null && ObjXMLReport.AutomotiveInfo.GetType().GetProperty(Control) != null)
                                                    {
                                                        ControlValue = Convert.ToString(ObjXMLReport.AutomotiveInfo.GetType().GetProperty(Control).GetValue(ObjXMLReport.AutomotiveInfo, null));
                                                        ErrMessage = IfEmpty(Products[i], ErrMessage, ControlValue, Control);
                                                    }
                                                    else if (ObjXMLReport.BusinessInfo != null && ObjXMLReport.BusinessInfo.GetType().GetProperty(Control) != null)
                                                    {
                                                        ControlValue = Convert.ToString(ObjXMLReport.BusinessInfo.GetType().GetProperty(Control).GetValue(ObjXMLReport.BusinessInfo, null));
                                                        ErrMessage = IfEmpty(Products[i], ErrMessage, ControlValue, Control);
                                                    }
                                                    else
                                                    {
                                                        if (Products[i].Contains("EMV"))
                                                        {
                                                            for (int j = 0; j < ObjXMLReport.EmploymentInfo.Count; j++)
                                                            {
                                                                ControlValue = Convert.ToString(ObjXMLReport.EmploymentInfo[j].GetType().GetProperty(Control).GetValue(ObjXMLReport.EmploymentInfo[j], null));
                                                                ErrMessage = IfEmpty(Products[i], ErrMessage, ControlValue, Control);
                                                            }
                                                        }
                                                        else if (Products[i].Contains("EDV"))
                                                        {
                                                            for (int j = 0; j < ObjXMLReport.EducationInfo.Count; j++)
                                                            {
                                                                ControlValue = Convert.ToString(ObjXMLReport.EducationInfo[j].GetType().GetProperty(Control).GetValue(ObjXMLReport.EducationInfo[j], null));
                                                                ErrMessage = IfEmpty(Products[i], ErrMessage, ControlValue, Control);
                                                            }
                                                        }
                                                        else if (Products[i].Contains("PRV"))
                                                        {
                                                            for (int j = 0; j < ObjXMLReport.PersonalInfo.Count; j++)
                                                            {
                                                                ControlValue = Convert.ToString(ObjXMLReport.PersonalInfo[j].GetType().GetProperty(Control).GetValue(ObjXMLReport.PersonalInfo[j], null));
                                                                ErrMessage = IfEmpty(Products[i], ErrMessage, ControlValue, Control);
                                                            }
                                                        }
                                                        else if (Products[i].Contains("PLV"))
                                                        {
                                                            for (int j = 0; j < ObjXMLReport.LicenseInfo.Count; j++)
                                                            {
                                                                ControlValue = Convert.ToString(ObjXMLReport.LicenseInfo[j].GetType().GetProperty(Control).GetValue(ObjXMLReport.LicenseInfo[j], null));
                                                            }
                                                        }
                                                        else if (Products[i].Contains("CCR1") || Products[i].Contains("CCR2") || Products[i].Contains("RCX"))
                                                        {
                                                            for (int j = 0; j < ObjXMLReport.CountyInfo.Count; j++)
                                                            {
                                                                ControlValue = Convert.ToString(ObjXMLReport.CountyInfo[j].GetType().GetProperty(Control).GetValue(ObjXMLReport.CountyInfo[j], null));
                                                                ErrMessage = IfEmpty(Products[i], ErrMessage, ControlValue, Control);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                ErrMessage = Products[i] + " - " + "Invalid Code";
                                            }
                                        }
                                        #endregion

                                        if (ErrMessage == string.Empty)
                                        {
                                            #region Insertion in main order table

                                            DateTime Dated = DateTime.Now;
                                            tblOrder ObjtblOrder = new tblOrder();
                                            ObjtblOrder.fkLocationId = LocationId;
                                            ObjtblOrder.fkCompanyUserId = CompanyUserId;
                                            string OrderNo = Dated.ToString("yyMMddHHmmssfff");
                                            ObjtblOrder.OrderNo = OrderNo;
                                            ObjtblOrder.OrderDt = Dated;
                                            ObjtblOrder.OrderStatus = (byte)OrderStatus.Pending;
                                            ObjtblOrder.CreatedDate = Dated;
                                            ObjtblOrder.CreatedById = CompanyUserId;
                                            ObjtblOrder.SyncpodImage = string.Empty;
                                            ObjtblOrder.FaceImage = string.Empty;
                                            ObjtblOrder.OrderType = (byte)OrderType.XMLAPI;//3
                                            ObjtblOrder.IsSyncpod = false;
                                            ObjtblOrder.ReferenceCode = ObjXMLReport.ReferenceCode;
                                            ObjEmergeReport.ObjtblOrder = ObjtblOrder;

                                            #endregion

                                            #region Check Live Runner State

                                            var StateColl = ObjBALGeneral.GetStateInfo(Convert.ToInt32(ObjXMLReport.GeneralInfo.StateId));
                                            if (StateColl.Count > 0)
                                            {
                                                bool IsLiveRunner = StateColl.ElementAt(0).IsStateLiveRunner;
                                                ObjEmergeReport.IsLiveRunnerState = IsLiveRunner;
                                            }
                                            #endregion

                                            #region Add values in Object

                                            OrderInfo ObjOrderInfo = new OrderInfo();
                                            tblOrderSearchedData ObjSearchData = BindGeneralInfo(ObjXMLReport);
                                            List<tblOrderSearchedEmploymentInfo> ObjEmploymentInfo = GetEmploymentInfo(ObjXMLReport);
                                            List<tblOrderSearchedEducationInfo> ObjEducationInfo = GetEducationInfo(ObjXMLReport);
                                            List<tblOrderSearchedPersonalInfo> ObjPersonalInfo = GetPersonalInfo(ObjXMLReport);
                                            List<tblOrderSearchedProfessionalInfo> ObjProfessionalInfo = GetProfessionalInfo(ObjXMLReport);

                                            List<tblOrderSearchedLicenseInfo> ObjLicenseInfo = GetLicenseInfo(ObjXMLReport);
                                            List<tblOrderSearchedLiveRunnerInfo> ObjLiveRunnerInfo = GetLiveRunnerInfo(ObjXMLReport);

                                            ObjEmergeReport.ObjtblOrderSearchedData = ObjSearchData;
                                            ObjEmergeReport.ObjCollOrderSearchedEducationInfo = ObjEducationInfo;
                                            ObjEmergeReport.ObjCollOrderSearchedEmploymentInfo = ObjEmploymentInfo;
                                            ObjEmergeReport.ObjCollOrderSearchedLicenseInfo = ObjLicenseInfo;
                                            ObjEmergeReport.ObjCollOrderSearchedPersonalInfo = ObjPersonalInfo;
                                            ObjEmergeReport.ObjCollOrderSearchedProfessionalInfo = ObjProfessionalInfo;
                                            ObjEmergeReport.ObjCollProducts = ObjCollProducts;
                                            ObjEmergeReport.objColltblOrderPersonalQtnInfo = new List<tblOrderPersonalQtnInfo>();
                                            ObjEmergeReport.ObjColltblOrderSearchedLiveRunnerInfo = ObjLiveRunnerInfo;
                                            ObjEmergeReport.IsNullDOB = ObjXMLReport.IsNullDOB;
                                            ObjEmergeReport.ObjCollCLSConsentInfo = new List<CLSConsentInfo>();
                                            ObjEmergeReport.ObjTieredPackage = new tblTieredPackage();
                                            ObjEmergeReport.ObjOrderInfo = ObjOrderInfo;

                                            #endregion

                                            #region Add Order In db

                                            List<tblOrderSearchedAliasPersonalInfo> ObjCollOrderSearchedAliasPersonalInfo = new List<tblOrderSearchedAliasPersonalInfo>();
                                            List<tblOrderSearchedAliasLocationInfo> ObjCollOrderSearchedAliasLocationInfo = new List<tblOrderSearchedAliasLocationInfo>();
                                            List<tblOrderSearchedAliasWorkInfo> ObjCollOrderSearchedAliasWorkInfo = new List<tblOrderSearchedAliasWorkInfo>();
                                            Dictionary<string, Guid> ObjCompnyProductInfo = new Dictionary<string, Guid>();

                                            BALOrders ObjBALOrders = new BALOrders();
                                            ObjEmergeReport = ObjBALOrders.AddOrders(ObjEmergeReport.ObjtblOrder,
                                                                ObjEmergeReport.ObjCollProducts,
                                                                ObjEmergeReport.ObjtblOrderSearchedData,
                                                                ObjEmergeReport.ObjCollOrderSearchedEmploymentInfo,
                                                                ObjEmergeReport.ObjCollOrderSearchedEducationInfo,
                                                                ObjEmergeReport.ObjCollOrderSearchedPersonalInfo,
                                                                ObjEmergeReport.ObjCollOrderSearchedProfessionalInfo,
                                                                ObjEmergeReport.objColltblOrderPersonalQtnInfo,
                                                                ObjEmergeReport.ObjCollOrderSearchedLicenseInfo,
                                                                ObjCollOrderSearchedAliasPersonalInfo,
                                                                ObjCollOrderSearchedAliasLocationInfo,
                                                                ObjCollOrderSearchedAliasWorkInfo,
                                                                ObjCompnyProductInfo,
                                                                ObjEmergeReport.ObjColltblOrderSearchedLiveRunnerInfo, ObjEmergeReport.ObjCollCLSConsentInfo,
                                                                Utility.SiteApplicationId, ObjEmergeReport.IsNullDOB,
                                                                ObjEmergeReport.IsLiveRunnerState, ObjEmergeReport.ObjTieredPackage, ObjEmergeReport.ObjOrderInfo, CompanyAccountNo, OrderCompanyName, PermissiblePurpose, ObjEmergeReport.ObjJurisdictionList);

                                            #endregion

                                            ObjResponse.Response = ObjEmergeReport.ObjtblOrder.OrderNo;
                                            ObjResponse.Message = "Order Saved Successfully";
                                        }
                                        else
                                        {
                                            ObjResponse.Response = string.Empty;
                                            ObjResponse.Message = "Fields required<br/>" + ErrMessage;
                                        }
                                    }
                                    else
                                    {
                                        ObjResponse.Response = string.Empty;
                                        ObjResponse.Message = "Please send alteast one code to run the report.";
                                    }
                                }
                                else
                                {
                                    ObjResponse.Response = string.Empty;
                                    ObjResponse.Message = "Please send valid package Codes";
                                }
                            }
                            else
                            {
                                ObjResponse.Response = string.Empty;
                                ObjResponse.Message = "Please send valid report Codes";
                            }
                        }
                        else
                        {
                            ObjResponse.Response = string.Empty;
                            ObjResponse.Message = "Reference Code already exists. Please send other code.";
                        }
                    }
                }
                else
                {
                    ObjResponse.Response = string.Empty;
                    ObjResponse.Message = Message;
                }
            }
            catch (Exception ex)
            {
                ObjResponse.Response = string.Empty;
                ObjResponse.Message = ex.Message.ToString();
            }
            return ObjResponse;
        }

        private string IfEmpty(string Products, string ErrMessage, string ControlValue, string Control)
        {
            if (ControlValue == string.Empty || ControlValue == "0")
            {
                if (ErrMessage.Contains(Products))
                {
                    ErrMessage += ", " + Control;
                }
                else
                {
                    ErrMessage += "<br/> " + Products + " - " + Control;
                }
            }
            return ErrMessage;
        }

        #endregion

        #region Private Methods


        private List<tblOrderSearchedLiveRunnerInfo> GetLiveRunnerInfo(EmergeReports ObjXMLReport)
        {
            List<tblOrderSearchedLiveRunnerInfo> ObjSearchedLiveRunnerInfo = new List<tblOrderSearchedLiveRunnerInfo>();
            BALGeneral ObjGeneral = new BALGeneral();
            for (int i = 0; i < ObjXMLReport.CountyInfo.Count; i++)
            {
                tblOrderSearchedLiveRunnerInfo ObjCountyInfo = new tblOrderSearchedLiveRunnerInfo();
                ObjCountyInfo.LiveRunnerState = ObjXMLReport.CountyInfo.ElementAt(i).CIStateId;
                var Coll = ObjGeneral.GetCountyInfo(ObjXMLReport.CountyInfo.ElementAt(i).CountyId);
                if (Coll.Count > 0)
                {
                    ObjCountyInfo.LiveRunnerCounty = Coll.ElementAt(i).CountyName + "_" + Coll.ElementAt(i).IsRcxCounty;
                }
                ObjCountyInfo.SearchedCountyId = ObjXMLReport.CountyInfo.ElementAt(i).CountyId;
                ObjSearchedLiveRunnerInfo.Add(ObjCountyInfo);
            }

            return ObjSearchedLiveRunnerInfo;
        }


        private List<tblOrderSearchedEmploymentInfo> GetEmploymentInfo(EmergeReports ObjXMLReport)
        {
            List<tblOrderSearchedEmploymentInfo> ObjCollSearchedEmploymentInfo = new List<tblOrderSearchedEmploymentInfo>();
            for (int i = 0; i < ObjXMLReport.EmploymentInfo.Count; i++)
            {
                tblOrderSearchedEmploymentInfo ObjEmploymentInfo = new tblOrderSearchedEmploymentInfo();
                ObjEmploymentInfo.SearchedName = ObjXMLReport.EmploymentInfo.ElementAt(i).CompanyName;
                ObjEmploymentInfo.SearchedPhone = ObjXMLReport.EmploymentInfo.ElementAt(i).CompanyPhone;
                ObjEmploymentInfo.SearchedState = ObjXMLReport.EmploymentInfo.ElementAt(i).CompanyState;
                ObjEmploymentInfo.SearchedCity = ObjXMLReport.EmploymentInfo.ElementAt(i).CompanyCity;
                ObjEmploymentInfo.SearchedAlias = ObjXMLReport.EmploymentInfo.ElementAt(i).Alias;
                ObjCollSearchedEmploymentInfo.Add(ObjEmploymentInfo);
            }

            return ObjCollSearchedEmploymentInfo;
        }


        private List<tblOrderSearchedEducationInfo> GetEducationInfo(EmergeReports ObjXMLReport)
        {
            List<tblOrderSearchedEducationInfo> ObjCollEducationInfo = new List<tblOrderSearchedEducationInfo>();
            for (int i = 0; i < ObjXMLReport.EducationInfo.Count; i++)
            {
                tblOrderSearchedEducationInfo ObjEducationInfo = new tblOrderSearchedEducationInfo();
                ObjEducationInfo.SearchedSchoolName = ObjXMLReport.EducationInfo.ElementAt(i).InstitutionName;
                ObjEducationInfo.SearchedPhone = ObjXMLReport.EducationInfo.ElementAt(i).InstitutionPhone;
                ObjEducationInfo.SearchedState = ObjXMLReport.EducationInfo.ElementAt(i).InstitutionState;
                ObjEducationInfo.SearchedCity = ObjXMLReport.EducationInfo.ElementAt(i).InstitutionCity;
                ObjEducationInfo.SearchedAlias = ObjXMLReport.EducationInfo.ElementAt(i).Alias;
                ObjCollEducationInfo.Add(ObjEducationInfo);
            }

            return ObjCollEducationInfo;
        }

        private List<tblOrderSearchedPersonalInfo> GetPersonalInfo(EmergeReports ObjXMLReport)
        {
            List<tblOrderSearchedPersonalInfo> ObjCollPersonalInfo = new List<tblOrderSearchedPersonalInfo>();
            for (int i = 0; i < ObjXMLReport.PersonalInfo.Count; i++)
            {
                tblOrderSearchedPersonalInfo ObjPersonalInfo = new tblOrderSearchedPersonalInfo();
                ObjPersonalInfo.SearchedName = ObjXMLReport.PersonalInfo.ElementAt(i).ReferenceName;
                ObjPersonalInfo.SearchedPhone = ObjXMLReport.PersonalInfo.ElementAt(i).ReferencePhone;
                ObjCollPersonalInfo.Add(ObjPersonalInfo);
            }
            return ObjCollPersonalInfo;
        }

        private List<tblOrderSearchedProfessionalInfo> GetProfessionalInfo(EmergeReports ObjXMLReport)
        {
            List<tblOrderSearchedProfessionalInfo> ObjCollProfessionalInfo = new List<tblOrderSearchedProfessionalInfo>();
            for (int i = 0; i < ObjXMLReport.PersonalInfo.Count; i++)
            {
                tblOrderSearchedProfessionalInfo ObjProfessionalInfo = new tblOrderSearchedProfessionalInfo();
                ObjProfessionalInfo.SearchedName = ObjXMLReport.PersonalInfo.ElementAt(i).ReferenceName;
                ObjProfessionalInfo.SearchedPhone = ObjXMLReport.PersonalInfo.ElementAt(i).ReferencePhone;
                ObjCollProfessionalInfo.Add(ObjProfessionalInfo);
            }
            return ObjCollProfessionalInfo;
        }


        private List<tblOrderSearchedLicenseInfo> GetLicenseInfo(EmergeReports ObjXMLReport)
        {
            List<tblOrderSearchedLicenseInfo> ObjCollLicenseInfo = new List<tblOrderSearchedLicenseInfo>();
            for (int i = 0; i < ObjXMLReport.LicenseInfo.Count; i++)
            {
                tblOrderSearchedLicenseInfo ObjLicenseInfo = new tblOrderSearchedLicenseInfo();
                ObjLicenseInfo.SearchedLicenseNo = ObjXMLReport.LicenseInfo.ElementAt(i).LicenseNo;
                ObjLicenseInfo.SearchedIssueDate = ObjXMLReport.LicenseInfo.ElementAt(i).LicenseDate;
                ObjLicenseInfo.SearchedIssuingState = ObjXMLReport.LicenseInfo.ElementAt(i).LicenseState;
                ObjLicenseInfo.SearchedLicenseType = ObjXMLReport.LicenseInfo.ElementAt(i).LicenseType;
                ObjLicenseInfo.SearchedAlias = ObjXMLReport.LicenseInfo.ElementAt(i).Alias;

                ObjCollLicenseInfo.Add(ObjLicenseInfo);
            }
            return ObjCollLicenseInfo;
        }

        private tblOrderSearchedData BindGeneralInfo(EmergeReports ObjEmergeReport)
        {
            tblOrderSearchedData ObjSearchData = new tblOrderSearchedData();
            ObjSearchData.SearchedLastName = ObjEmergeReport.GeneralInfo.LastName;
            ObjSearchData.SearchedFirstName = ObjEmergeReport.GeneralInfo.FirstName ?? string.Empty;
            ObjSearchData.SearchedMiddleInitial = ObjEmergeReport.GeneralInfo.MI ?? string.Empty;
            ObjSearchData.SearchedSuffix = ObjEmergeReport.GeneralInfo.Suffix ?? string.Empty;
            ObjSearchData.SearchedSsn = ObjEmergeReport.GeneralInfo.Social ?? string.Empty;
            ObjSearchData.SearchedDob = ObjEmergeReport.GeneralInfo.DOB ?? string.Empty;
            ObjSearchData.SearchedPhoneNumber = ObjEmergeReport.GeneralInfo.Phone ?? string.Empty;
            ObjSearchData.SearchedStreetAddress = ObjEmergeReport.GeneralInfo.StreetAddress ?? string.Empty;
            ObjSearchData.SearchedDirection = ObjEmergeReport.GeneralInfo.Direction ?? string.Empty;
            ObjSearchData.SearchedStreetName = ObjEmergeReport.GeneralInfo.StreetName ?? string.Empty;
            ObjSearchData.SearchedStreetType = ObjEmergeReport.GeneralInfo.StreetType ?? string.Empty;
            ObjSearchData.SearchedApt = ObjEmergeReport.GeneralInfo.Apt ?? string.Empty;
            ObjSearchData.SearchedCity = ObjEmergeReport.GeneralInfo.City ?? string.Empty;
            ObjSearchData.SearchedStateId = ObjEmergeReport.GeneralInfo.StateId;
            ObjSearchData.SearchedZipCode = ObjEmergeReport.GeneralInfo.Zip ?? string.Empty;
            ObjSearchData.SearchedCountyId = ObjEmergeReport.GeneralInfo.JurisdictionId;

            #region Automotive Object

            if (ObjEmergeReport.AutomotiveInfo != null)
            {
                ObjSearchData.SearchedDriverLicense = ObjEmergeReport.AutomotiveInfo.DriverLicense ?? string.Empty;
                ObjSearchData.SearchedVehicleVin = ObjEmergeReport.AutomotiveInfo.VehicleVin ?? string.Empty;
                ObjSearchData.SearchedDLStateId = ObjEmergeReport.AutomotiveInfo.DLStateId;
                ObjSearchData.SearchedSex = ObjEmergeReport.AutomotiveInfo.Sex ?? Convert.ToString(-1);
            }
            else
            {
                ObjSearchData.SearchedDriverLicense = string.Empty;
                ObjSearchData.SearchedVehicleVin = string.Empty;
                ObjSearchData.SearchedDLStateId = -1;
                ObjSearchData.SearchedSex = "-1";
            }

            #endregion

            #region Business Object

            if (ObjEmergeReport.BusinessInfo != null)
            {
                ObjSearchData.SearchedBusinessName = ObjEmergeReport.BusinessInfo.BusinessName ?? string.Empty;
                ObjSearchData.SearchedBusinessCity = ObjEmergeReport.BusinessInfo.BusinessCity ?? string.Empty;
                ObjSearchData.SearchedBusinessStateId = ObjEmergeReport.BusinessInfo.BusinessStateId;
            }
            else
            {
                ObjSearchData.SearchedBusinessName = string.Empty;
                ObjSearchData.SearchedBusinessCity = string.Empty;
                ObjSearchData.SearchedBusinessStateId = -1;
            }

            #endregion

            #region Tracking Object

            if (ObjEmergeReport.TrackingInfo != null)
            {
                ObjSearchData.SearchedTrackingRef = ObjEmergeReport.TrackingInfo.TrackingRef ?? string.Empty;
                ObjSearchData.SearchedTrackingRefId = ObjEmergeReport.TrackingInfo.TrackingRefId;
                ObjSearchData.SearchedTrackingNotes = ObjEmergeReport.TrackingInfo.TrackingNotes ?? string.Empty;
            }
            else
            {
                ObjSearchData.SearchedTrackingRef = string.Empty;
                ObjSearchData.SearchedTrackingRefId = -1;
                ObjSearchData.SearchedTrackingNotes = string.Empty;
            }
            #endregion

            ObjSearchData.search_county = string.Empty;
            ObjSearchData.search_state = string.Empty;
            return ObjSearchData;
        }

        #endregion
    }

    #region Custom Classes

    public class PackagesList
    {
        public Package[] PackageList { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class Package
    {
        public int PackageId { get; set; }
        public string PackageCode { get; set; }
        public string PackageName { get; set; }
        public string ReportInclude { get; set; }
        public Report[] ReportsList { get; set; }

    }

    public class ReportsList
    {
        public Report[] ReportList { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class Report
    {
        public int ReportId { get; set; }
        public string ReportCode { get; set; }
        public string ReportName { get; set; }
    }

    public class ReferenceCodesList
    {
        public Code[] ReferenceCodeList { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class Code
    {
        public int ReferenceCodeId { get; set; }
        public string ReferenceCode { get; set; }
    }

    public class State
    {
        public int StateId { get; set; }
        public string StateName { get; set; }
        public string StateCode { get; set; }
        public bool IsLiveRunner { get; set; }
    }

    public class Jurisdiction
    {
        public int JurisdictionId { get; set; }
        public string JurisdictionName { get; set; }
    }

    public class CountiesList
    {
        public County[] CountyList { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class County
    {
        public int CountyId { get; set; }
        public string CountyName { get; set; }
    }

    public class Suffix
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }

    public class Direction
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }

    public class Type
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }

    public class Gender
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }

    public class Respose
    {
        public string Message { get; set; }
        public string Response { get; set; }
    }

    //public class PackageList
    //{
    //    public string PackageCode { get; set; }
    //    public string PackageName { get; set; }
    //    public string ErrorMessage { get; set; }
    //} 

    #endregion
}

