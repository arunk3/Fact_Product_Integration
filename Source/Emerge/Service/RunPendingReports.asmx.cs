﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Emerge.Services;
using Emerge.Data;
using System.IO;
using VendorServices;

namespace Emerge.Service
{
    /// <summary>
    /// Summary description for RunPendingReports
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class RunPendingReports : System.Web.Services.WebService
    {
        BALPendingOrders ObjBALPendingOrders = null;

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public List<string> ResendPendingOrderByOrderId(int pkOrderId, string ProductCode)
        {
            ObjBALPendingOrders = new BALPendingOrders();
            List<string> RetList = new List<string>();
            string ShotFileName = string.Empty;
            RetList.Add(SetFilePath(out ShotFileName));
            RetList.Add(ShotFileName);
            try
            {
                VendorServices.GeneralService.WriteLog("\nService Method Started at:" + DateTime.Now.ToString(), "WindowsService_ResendPendingAllOrders");
                ObjBALPendingOrders.ResendPendingOrderByOrderId(pkOrderId, ProductCode);
                #region Check Error Response
                if (ShotFileName != "")
                {
                    RetList.Add(AddErrorToCollection(ShotFileName));
                }
                #endregion
            }
            catch (Exception ex)
            {
                RetList.Add(AddErrorToCollection(ShotFileName));
                RetList.Add(ex.Message.ToString());
            }
            return RetList;
        }

        [WebMethod]
        public List<string> ResendPendingAllOrders()
        {
            ObjBALPendingOrders = new BALPendingOrders();
            List<string> RetList = new List<string>();
            string ShotFileName = string.Empty;
            RetList.Add(SetFilePath(out ShotFileName));
            RetList.Add(ShotFileName);
            int? ErrorOrderId = 0;

            try
            {
                //Need to uncomment for live site.
                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\nService Method Started at:" + DateTime.Now.ToString(), "WindowsService_ResendPendingAllOrders");
                ObjBALPendingOrders.ResendPendingOrders(out ErrorOrderId);
                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\nService Method Completed at:" + DateTime.Now.ToString(), "WindowsService_ResendPendingAllOrders");
            }
            catch (Exception ex)
            {
                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\nService Method gave error at:" + DateTime.Now.ToString() + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace.ToString(), "WindowsService_ResendPendingAllOrders");
                RetList.Add(ex.Message.ToString());
            }
            RetList.Add(ErrorOrderId.ToString());
            return RetList;
        }

        [WebMethod]
        public List<string> ResendPendingByVendorId(byte VendorId)
        {
            ObjBALPendingOrders = new BALPendingOrders();
            List<string> RetList = new List<string>();
            string ShotFileName = string.Empty;
            RetList.Add(SetFilePath(out ShotFileName));
            RetList.Add(ShotFileName);
            try
            {
                int? ErrorOrderId = 0;
                try
                {
                    //Need to uncomment for live site.
                    ObjBALPendingOrders.ResendPendingOrderByVendorId(VendorId, out ErrorOrderId);
                }
                catch (Exception ex)
                {
                    RetList.Add(ex.Message.ToString());
                }
                RetList.Add(ErrorOrderId.ToString());
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return RetList;
        }


        public string SetFilePath(out string ShortFileName)
        {
            string NameCreation = string.Empty;
            NameCreation = DateTime.Now.ToString("yyMMMdd_HHmmss") + ".csv";
            string str = HttpContext.Current.Server.MapPath("~/Resources/Upload/PendingReportsLog/" + NameCreation);
            string rcxstr = HttpContext.Current.Server.MapPath("~/Resources/Upload/ReportsLog/RapidCourt/");
            string rcxrequeststr = HttpContext.Current.Request.PhysicalApplicationPath + "Resources/Upload/ReportsLog/RapidCourt/"; ;
            //BALGeneral ObjBALGeneral = new BALGeneral(rcxstr);
            ObjBALPendingOrders.RCXFilePath = rcxstr;
            ObjBALPendingOrders.RCXRequestPath = rcxrequeststr;
            #region MyRegion

            if (!Directory.Exists(HttpContext.Current.Server.MapPath(@"~\Resources\Upload\PendingReportsLog")))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(@"~\Resources\Upload\PendingReportsLog"));
            }

            string savedPath = string.Empty;
            //DateTime dt = DateTime.Now;
            savedPath = str;
            if (!File.Exists(savedPath))
            {
                using (File.CreateText(savedPath)) { }
                ObjBALPendingOrders.FilePath = savedPath;
            }

            #endregion

            #region Delete 1 Day Old Files

            DirectoryInfo dir = null;
            dir = new DirectoryInfo(Server.MapPath(@"~\Resources\Upload\PendingReportsLog"));
            FileSystemInfo[] info = dir.GetFiles();
            if (info.Length > 0)
            {
                foreach (FileInfo fi in info)
                {
                    if (fi.CreationTime < DateTime.Now.AddDays(-1))
                    {
                        string FilePath = Server.MapPath(@"~\Resources\Upload\PendingReportsLog\") + fi.Name;
                        if (File.Exists(FilePath))
                        {
                            try
                            {
                                File.Delete(FilePath);
                            }
                            catch //(Exception ex)
                            {
                                /*ListErrors.Add("File Used By Another Process : " + ex.Message.ToString());*/
                            }

                        }
                    }
                }
            }

            #endregion

            ShortFileName = NameCreation;
            return str;

        }

        public string AddErrorToCollection(string sFilename)
        {
            string retStr = "";
            try
            {
                string str = HttpContext.Current.Server.MapPath("~/Resources/Upload/PendingReportsLog/" + sFilename);
                if (File.Exists(str))
                {
                    string stext = string.Empty;
                   // FileInfo fileInfo = new System.IO.FileInfo(str);
                    using (StreamReader sReader = new StreamReader(str))
                    {
                        stext = sReader.ReadToEnd();
                        sReader.Close();
                    }
                    if (stext != "")
                    {
                        string[] FetchContent = stext.Split(',');
                        if (FetchContent.Count() > 1)
                        {
                            //Here i am fetching error 
                            retStr = FetchContent[2];
                        }
                    }
                }
            }
            catch { }
            return retStr;
        }
    }
}
