﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Emerge.Services;
using Emerge.Data;
using System.IO;
using System.Xml.Serialization;
using System.Text;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using Emerge.Common;
using System.Reflection;
using System.Xml.Linq;

namespace Emerge.Service
{
    /// <summary>
    /// Summary description for BatchProcess
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class BatchProcess : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            //BALBatch ObjBalbatch = new BALBatch();
            return "Hello World";
        }
        [WebMethod]
        public void RunBatchProcessAll()
        {
            try
            {
                BALBatch ObjBatchProcess = new BALBatch();
                ObjBatchProcess.getBatchFiles();   
            }
            catch //(Exception ex)
            {
             // WriteToTxtFile.WriteToFile(ex);
            }
        }




    }
}
