﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Emerge.Services;
using Emerge.Data;
using System.IO;
using System.Xml.Serialization;
using System.Text;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using Emerge.Common;
using System.Reflection;
using System.Xml.Linq;
namespace Emerge.Service
{
    /// <summary>
    /// Summary description for CommonService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class CommonService : System.Web.Services.WebService
    {
        BALPendingOrders ObjBALPendingOrders = null;
        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }


        [WebMethod]
        public void InactiveRunReportOver30Days()
        {
            
            try
            {
                BALCompany objbal = new BALCompany();
                objbal.InactiveRunReportOver30Days();
            }
            catch //(Exception ex)
            {
               
            }
        
        }


        [WebMethod]
        public void SendNoResponseEmail()
        {
            try
            {
                BALEmergeReview objBALEmergeReview = new BALEmergeReview();
                objBALEmergeReview.SendNoResponseEmailAfterTwoDay();

            }
            catch
            {

            }
        }

        [WebMethod]
        public List<string> RunTieredReportOrders()
        {
            ObjBALPendingOrders = new BALPendingOrders();
            List<string> RetList = new List<string>();
            //int ErrorOrderId = 0;

            try
            {
                //Need to uncomment for live site.
                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\nService Method Started at:" + DateTime.Now.ToString(), "WindowsService_RunTieredReportOrders");
                ObjBALPendingOrders.RunTieredOrders();
                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\nService Method Completed at:" + DateTime.Now.ToString(), "WindowsService_RunTieredReportOrders");
            }
            catch (Exception ex)
            {
                VendorServices.GeneralService.WriteLog(Environment.NewLine + "\nService Method gave error at:" + DateTime.Now.ToString() + Environment.NewLine + ex.Message.ToString() + Environment.NewLine + ex.StackTrace.ToString(), "WindowsService_RunTieredReportOrders");
                RetList.Add(ex.Message.ToString());
            }
            return RetList;
        }

    }
}
