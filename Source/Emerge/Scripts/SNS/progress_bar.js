var timeout = null;
var timeout1 = null;
var refreshms = 500;

var data_num_request = 15;
var data_num_display = 5;

function get_emails(task_id) {
    this.task_id = task_id;
    
    var finished;
	var number_emails;
	var emails;
    var tt = Math.floor(Math.random()*(1001));  
    // alert('get_emails');
    $.getJSON('http://api.socialdiligence.com/search/get_emails/?task_id='+this.task_id+'&t='+tt,
    function(data){
        if(data)
        {
			number_emails = data.number_emails;
			emails = data.emails;
			finished = data.finished;
			var emails_block = document.getElementById('emails_block');
			if(emails)
			{
				
				var emails_found = document.getElementById('emails_found');
//				$('#emails_block').append(emails);
				if(emails_block)
				{
					var results_title = 'Results';
	 
					if(number_emails == 1)
					{
						results_title = 'Result';
					}
					var link = '<a href="#" onClick="sw_tab(15,\'emails\');source_results('+task_id+',\'emails\');">'+number_emails+' '+results_title+'</a>';
					emails_found.innerHTML = link;
				}
			}
			if(finished == 1)
			{			
				emails_block.innerHTML = '';
				return 1;
			}
			else
			{
				window.setTimeout("get_emails(task_id)", 3000);
			}
			
        }
      });
}



function get_pictures(task_id) {
    this.task_id = task_id;
    var finished;
	var ready_indexes;
	var ready_values;
    var tt = Math.floor(Math.random()*(1001));  
    $.getJSON('http://api.socialdiligence.com/search/get_pictures_list/?task_id='+this.task_id+'&t='+tt,
    function(data){
        if(data)
        {
			finished = data.finished;
			ready_indexes = data.ready_indexes;
			ready_values = data.ready_values;
			if(ready_indexes)
			{
				for (var i = 0; i < ready_indexes.length; i++) {
					//alert('Item ' + i +'value='+ready_indexes[i] );
					var img_block = document.getElementById('img_'+ready_indexes[i]);
					if(img_block)
					{
							var output = '<div class=gallery><ul>';
							values = ready_values[i];
							for (var k = 0; k < values.length; k++) {
								if(values[k])
								{
									output = output+ '<li><a href="http://'+location.hostname+'/'+values[k]+'" ><img  height=60 border=0 src="http://'+location.hostname+'/'+values[k]+'"></a></li>';
								}
								//alert("Item k" + k + " is:" + values[k]);
							}
							output = output+'</ul></div>';
							if(finished == 1 )
							{
								img_block.innerHTML = output;
							}else
							{
								if(img_block.innerHTML == '' )
								{
									img_block.innerHTML = output;
								}
							}
							$(function() {$('#img_'+ready_indexes[i]+' a').lightBox(); });

					}
				}
			}
			if(finished == 1)
			{
									
				return 1;
			}
			else
			{
				window.setTimeout("get_pictures(task_id)", 3000);
			}
			
        }
      });
}


function get_pictures_old(task_id) {
	this.task_id = task_id;
	var req = new Subsys_JsHttpRequest_Js(); 
	var finished;
		req.onreadystatechange = function()
		{
			if(req.readyState == 4 )
			{		
				if(req.responseJS)
				{			
					finished = req.responseJS.finished;
					ready_indexes = req.responseJS.ready_indexes;
					ready_values = req.responseJS.ready_values;
					if(ready_indexes)
					{
						for (var i = 0; i < ready_indexes.length; i++) {
							//alert('Item ' + i +'value='+ready_indexes[i] );
							var img_block = document.getElementById('img_'+ready_indexes[i]);
							if(img_block)
							{
									var output = '';
									values = ready_values[i];
									for (var k = 0; k < values.length; k++) {
										if(values[k])
										{
											output = output+ '<img  height=60 border=0 src="http://'+location.hostname+'/'+values[k]+'">';
										}
										//alert("Item k" + k + " is:" + values[k]);
									}
									if(finished == 1 )
									{
										img_block.innerHTML = output;
									}else
									{
										if(img_block.innerHTML == '' )
										{
											img_block.innerHTML = output;
										}
									}
									
							}
						}
					}
					if(finished == 1)
					{
											
						return 1;
					}
					else
					{
						window.setTimeout("get_pictures(task_id)", 3000);
					}
				}
				else
				{
					window.setTimeout("get_pictures(task_id)", 3000);
				}
			}
		}
		
		req.caching = false;
		req.open('POST', 'http://api.socialdiligence.com/search/get_pictures_list/', true);
		req.send({ task_id: this.task_id},14102003);
}

function PicLoader(task_id) {
	if (timeout){clearTimeout(timeout)};
	timeout = setTimeout("get_pictures(task_id)", 2000);
}

function EmailLoader(task_id) {
	
	if (timeout1){clearTimeout(timeout1)};
	timeout1 = setTimeout("get_emails(task_id)", 2000);
}



function doLoad(task_id,eventname,object,value) {
    this.task_id = task_id;
    var tt = Math.floor(Math.random()*(1001));  
    $.getJSON('http://api.socialdiligence.com/get_webresults/stat'+task_id+'?task='+task_id+'&t='+tt+namesake_block,
      function(data){
        if(data)
        {
            if(data.finished==1)
            {
               $('#finished_'+task_id).value = 2;
               var ready_all = getAllData(task_id,eventname,object,value);
               return 1;
            }
            else
            {
               var progressPercent = Math.ceil((data.done/data.total)*100);
               $('#progressMeterText').html('Done: ' + progressPercent + '%');
               $('#progressMeterBarDone').css('width',parseInt(progressPercent*4.36) + 'px');
               setTimeout("doLoad("+task_id+",'"+eventname+"','"+object+"','"+value+"');",refreshms);
            }
        }
        else
            setTimeout("doLoad("+task_id+",'"+eventname+"','"+object+"','"+value+"');",refreshms);
      });
}

function doLoadTeaser(task_id) {
    this.task_id = task_id;
    //alert(task_id);
    var tt = Math.floor(Math.random()*(1001));  
    $.getJSON('http://api.socialdiligence.com/get_webresults/teaser'+task_id+'?task='+task_id+'&t='+tt+namesake_block,
      function(data){
        if(data)
        {
            if(data.finished==1)
            {
              // $('#finished_'+task_id).value = 2;  
               $('#number_results').html(data.nickname_stat );
               $('#status_search').html('... done');
               //return 1;
            }
            else
            {
               
                $('#number_results').html(data.nickname_stat );
               
               setTimeout("doLoadTeaser('"+task_id+"');",refreshms);
            }
        }
        else
            setTimeout("doLoadTeaser('"+task_id+"');",refreshms);
      });
}

function getAllData(task_id,eventname,object,value) {
    this.task_id = task_id;
    var qrefine = 'eventname='+eventname+'&object='+object+'&value='+value;
    var tt = Math.floor(Math.random()*(1001));  
    $.getJSON('http://api.socialdiligence.com/search/get_webresults/?task_id='+task_id+'&t='+tt+'&'+qrefine,
      function(data){
        if(data)
        {
            $('#progressBox').css('display','none');
            $('#progressMeterText').html('Done: 100%');
            if(data.finished==1)
            {
               $('#finished_'+task_id).value = data.finished;
               $('#get_webresults'+task_id).html(data.webresult);
               $('a.thumb').zoomimage({
                            border: 1,
                            centered: true,
                            hideSource: false,
                            controls: false
                            });

                $('.tip').tooltip({
                            delay: 0,
                            showURL: false,
                            showBody: " - "
                    });

               get_search_results(task_id,1);
               return 1;
            }
            else
            {
               setTimeout("getAllData("+task_id+",'"+eventname+"','"+object+"','"+value+"');",refreshms);
            }
        }
      });
}
function source_results(task_id, source) 
{
    this.task_id = task_id;    
    var tt = Math.floor(Math.random()*(1001));      
    if(source=="snresult_clear")
    {
         $('#snnetworks_list').css('display', 'block');
         $('#source_results').css('display', 'none');
    }
    else
    {                
        $.ajax({
            url: 'ReadXML.aspx?network=' + source,
            success: function (data) {
                if (data) {                    
                    $('#snnetworks_list').css('display', 'none');
                    $('#source_results').css('display', 'block');
                    $('#source_results').html(data);

                    $('a.thumb').zoomimage({
                        border: 1,
                        centered: true,
                        hideSource: false,
                        controls: false
                    });

                    $('.tip').tooltip({
                        delay: 0,
                        showurl: false,
                        showbody: " - "
                    });

                }
                else {
                    setTimeout("source_results(" + task_id + ",'" + source + "');", refreshms);
                }
            }
        });

    }
    
}
function get_search_results(task_id,page,namesake_results) {
    this.task_id = task_id;
    this.page = page;
    $('#loading').css("display","block");
    var tt = Math.floor(Math.random()*(1001));  
    
    $.getJSON('http://api.socialdiligence.com/search/get_webresults/?action=sr&task_id='+this.task_id+namesake_block+'&page='+this.page+'&t='+tt+'&'+namesake_block,
      function(data){
        if(data)
        {
            if(data.finished==1)
            {
                $('#get_search_results'+task_id).html(data.webresult);
                $('#loading').css("display","none");
                get_othernames(task_id);
            }
            else
            {
               setTimeout("get_search_results("+task_id+",'"+page+"');",refreshms);
            }
        }
      });
}

function get_othernames(task_id) {
    this.task_id = task_id;
    var tt = Math.floor(Math.random()*(1001));  
    $.getJSON('http://api.socialdiligence.com/search/get_webresults/?action=sr&task_id='+this.task_id+'&page='+this.page+'&t='+tt,
      function(data){
        if(data)
        {
            if(data.finished==1)
            {
                $('#refine_results_1_'+task_id).html(data.webresult);
                $('#google_1').css('display','block');
                get_ussearch(task_id);
                get_classmates(task_id);
                
                //get_reunion(task_id);
                PicLoader(task_id);
                EmailLoader(task_id);
            }
            else
            {
               setTimeout("get_othernames("+task_id+");",refreshms);
            }
        }
      });
}
function get_classmates(task_id) {
    this.task_id = task_id;
   $('#classmates_progress_bar').css('display','none');
    var tt = Math.floor(Math.random()*(1001));  
    $.getJSON('http://api.socialdiligence.com/search/classmates_adv/?task_id='+this.task_id+'&t='+tt,
      function(data){
        if(data)
        {
            if(data.finished==1)
            {
			   $('#classmates_subholder').html(data.webresult);
               $('#classmates_subholder').css('display','block');
            }
            else
            {
                setTimeout("get_classmates("+task_id+");",refreshms);
            }
        }
      });
}


function get_ussearch(task_id) {
    this.task_id = task_id;
    var tt = Math.floor(Math.random()*(1001));  
    $.getJSON('http://api.socialdiligence.com/search/ussearch_adv/?task_id='+this.task_id+'&t='+tt,
      function(data){
        if(data)
        {
            if(data.finished==1)
            {
               $('#ussearch_subholder').html(data.webresult);
               $('#ussearch_holder').css('display','block');
            }
            else
            {
               setTimeout("get_ussearch("+task_id+");",refreshms);
            }
        }
      });
}
function get_reunion(task_id) {
    this.task_id = task_id;
    var tt = Math.floor(Math.random()*(1001));  
    $.getJSON('http://api.socialdiligence.com/search/reunion_adv/?task_id='+this.task_id+'&t='+tt,
      function(data){
        if(data)
        {
            if(data.finished==1)
            {
                /*$('#reunion').html(data.webresult);*/
                var url_all = 'http://whozat.com/redirect_reunion/?url=http://affiliates.reunion.com/a/landa/d-peoplesearch.asp?searchFirstName='+data.firstname+'&searchLastName='+data.lastname+'&searchAge=33&s_cid=API:00007';
				var tlink = document.getElementById("see_all_results_link");
				if(tlink){
					tlink.href=url_all;
				}
				
				$('#reunion').css('display','block');
				loadTeaser(data.firstname,data.lastname);
				var tform = document.getElementById("see_all_results");
				tform.searchFirstName.value = data.firstname;
				tform.searchLastName.value = data.lastname;
				tform.searchAge.value = "33";
				
				
            }
            else
            {
               setTimeout("get_reunion("+task_id+");",refreshms);
            }
        }
      });
}
function get_print_expressions(task_id) {
	this.task_id = task_id;
	
	//document.getElementById('loading').style.display = 'block';
	var req = new Subsys_JsHttpRequest_Js(); 
	var finished;
	req.onreadystatechange = function()
	{
		if(req.readyState == 4)
		{	
			if(req.responseJS)
			{			
				var webresult = req.responseJS.webresult;
				finished = req.responseJS.finished;
				if(finished == 1)
				{
						document.getElementById('clouds').innerHTML = webresult;
				}
				else
				{
					get_search_results(task_id);
				}
				
			}
		}//status == 4
		
	}//end function

	req.caching = false;
	req.open('POST', 'http://api.socialdiligence.com/search/get_webresults/?action=rf&task_id='+this.task_id, true);
	var tt = Math.floor(Math.random()*(1001));		
	req.send({ task: this.task_id, t: tt },20080819);

}
