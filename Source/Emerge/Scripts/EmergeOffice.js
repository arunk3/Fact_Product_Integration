﻿
function SelectUnSelectGV(val) {
    var GV = document.getElementById('ctl00_ContentPlaceHolder1_GvData');
   
    if (GV != null) {
        var rwlen = GV.rows.length;
        for (i = 2; i <= rwlen; i++) {
            var GVChkID = '';
            if (i <= 9) {
                GVChkID = GV.id + '_ctl0' + i + '_chkLead';
            }
            else {
                GVChkID = GV.id + '_ctl' + i + '_chkLead';
            }
            document.getElementById(GVChkID).checked = val; 
        }
    }
}

function ValidateGV() {
    var GV = document.getElementById('<%=GvData.ClientID%>');
    var count = 0;
    var rwlen = GV.rows.length;

    for (i = 2; i <= rwlen; i++) {
        var GVChkID = '';
        if (i <= 9) {
            GVChkID = GV.id + '_ctl0' + i + '_chkbox';
        }
        else {
            GVChkID = GV.id + '_ctl' + i + '_chkbox';
        }
        if (document.getElementById(GVChkID).checked) {
            count = 1;
            break;
        }
    }
    if (count <= 0) {
        alert('Please select atleast one Company');
        return false;
    }
    return true;
}

function UserConfirmationBeforeDelete() {
    if (ValidateGV()) {
        if (confirm('Are you sure to delete Company(s) ?')) {
            return true;
        }
    }
    return false;
}

function PkgMustTwo() {
    var count = 0;
    var ret = false;
    var GV = document.getElementById('tblReports');
    if (GV != null) {
        var rwlen = GV.rows.length - 1;
        for (i = 1; i <= rwlen; i++) {
            var GVChkID = '';
            if (i <= 9) { GVChkID = 'ctl00_ContentPlaceHolder1_rptProducts' + '_ctl0' + i + '_chkReport'; }
            else { GVChkID = 'ctl00_ContentPlaceHolder1_rptProducts' + '_ctl' + i + '_chkReport'; }
            if (document.getElementById(GVChkID).checked == true) { count++; if (count > 1) { ret = true; break; } else { ret = false; } }
        } 
    }
    if (!ret) { alert('Package must include a minimum of two products'); }
    return ret;
}
function LeadMustOne() {
    var count = 0;
    var ret = false;
    var GV = document.getElementById('tblReports');
    if (GV != null) {
        var rwlen = GV.rows.length - 1;
        for (i = 1; i <= rwlen; i++) {
            var GVChkID = '';
            if (i <= 9) { GVChkID = 'ctl00_ContentPlaceHolder1_rptProducts' + '_ctl0' + i + '_chkReport'; }
            else { GVChkID = 'ctl00_ContentPlaceHolder1_rptProducts' + '_ctl' + i + '_chkReport'; }
            if (document.getElementById(GVChkID).checked == true) { count++; if (count > 1) { ret = true; break; } else { ret = false; } }
        }
    }
    if (!ret) { alert('Lead must include a minimum of one products'); }
    return ret;
}

var zChar = new Array(' ', '(', ')', '-', '.'); var maxphonelength = 14; var phonevalue1; var phonevalue2; var cursorposition; function ParseForNumber1(object) { phonevalue1 = ParseChar(object.value, zChar); }
function ParseForNumber2(object) { phonevalue2 = ParseChar(object.value, zChar); }
function backspacerUP(object, e) {
    if (e) { e = e } else { e = window.event }
    if (e.which) { var keycode = e.which } else { var keycode = e.keyCode }
    ParseForNumber1(object)
    if (keycode >= 48) { ValidatePhone1(object) }
}
function backspacerDOWN(object, e) {
    if (e) { e = e } else { e = window.event }
    if (e.which) { var keycode = e.which } else { var keycode = e.keyCode }
    ParseForNumber2(object)
}
function GetCursorPosition() {
    var t1 = phonevalue1; var t2 = phonevalue2; var bool = false
    for (i = 0; i < t1.length; i++) {
        if (t1.substring(i, 1) != t2.substring(i, 1)) {
            if (!bool) {
                cursorposition = i
                window.status = cursorposition
                bool = true
            }
        }
    }
}

var status;
function checkValidLicense(Lid, evt) {
    var id = document.getElementById("" + Lid + "").value;
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode > 123 || charCode < 97))
        return false;

    return true;
}
$(function () {
    $('input[id$=nr_txtDriverLicense]').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
});


function ValidatePhone1(object) {
    var p = phonevalue1
    p = p.replace(/[^\d]*/gi, "")
    if (p.length < 3) { object.value = p } else if (p.length == 3) {
        pp = p; d4 = p.indexOf('(')
        d5 = p.indexOf(')')
        if (d4 == -1) { pp = "(" + pp; }
        if (d5 == -1) { pp = pp + ")"; }
        object.value = pp;
    } else if (p.length > 3 && p.length < 7) {
        p = "(" + p; l30 = p.length; p30 = p.substring(0, 4); p30 = p30 + ") "
        p31 = p.substring(4, l30); pp = p30 + p31; object.value = pp;
    } else if (p.length >= 7) {
        p = "(" + p; l30 = p.length; p30 = p.substring(0, 4); p30 = p30 + ") "
        p31 = p.substring(4, l30); pp = p30 + p31; l40 = pp.length; p40 = pp.substring(0, 9); p40 = p40 + "-"
        p41 = pp.substring(9, l40); ppp = p40 + p41; object.value = ppp.substring(0, maxphonelength);
    }
    GetCursorPosition()
    if (cursorposition >= 0) {
        if (cursorposition == 0) { cursorposition = 2 } else if (cursorposition <= 2) { cursorposition = cursorposition + 1 } else if (cursorposition <= 4) { cursorposition = cursorposition + 3 } else if (cursorposition == 5) { cursorposition = cursorposition + 3 } else if (cursorposition == 6) { cursorposition = cursorposition + 3 } else if (cursorposition == 7) { cursorposition = cursorposition + 4 } else if (cursorposition == 8) {
            cursorposition = cursorposition + 4
            e1 = object.value.indexOf(')')
            e2 = object.value.indexOf('-')
            if (e1 > -1 && e2 > -1) { if (e2 - e1 == 4) { cursorposition = cursorposition - 1 } }
        } else if (cursorposition == 9) { cursorposition = cursorposition + 4 } else if (cursorposition < 11) { cursorposition = cursorposition + 3 } else if (cursorposition == 11) { cursorposition = cursorposition + 1 } else if (cursorposition == 12) { cursorposition = cursorposition + 1 } else if (cursorposition >= 13) { cursorposition = cursorposition }
        var txtRange = object.createTextRange(); txtRange.moveStart("character", cursorposition); txtRange.moveEnd("character", cursorposition - object.value.length); txtRange.select();
    }
}
function ParseChar(sStr, sChar) {
    if (sChar.length == null)
    { zChar = new Array(sChar); }
    else zChar = sChar; for (i = 0; i < zChar.length; i++) {
        sNewStr = ""; var iStart = 0; var iEnd = sStr.indexOf(sChar[i]); while (iEnd != -1)
        { sNewStr += sStr.substring(iStart, iEnd); iStart = iEnd + 1; iEnd = sStr.indexOf(sChar[i], iStart); }
        sNewStr += sStr.substring(sStr.lastIndexOf(sChar[i]) + 1, sStr.length); sStr = sNewStr;
    }
    return sNewStr;
}