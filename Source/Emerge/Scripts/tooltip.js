// position of the tooltip relative to the mouse in pixel //
var offsetx = 10;
var offsety = 20;
var counter = 0;
function newelement(newid) {
    if (document.createElement) {
        var el = document.createElement('div');
        el.id = newid;
        with (el.style) {
            display = 'none';
            position = 'absolute';
        }
        el.innerHTML = '&nbsp;';
        document.body.appendChild(el);
    }
}
var ie5 = (document.getElementById && document.all);

var ns6 = (document.getElementById && !document.all);

var ua = navigator.userAgent.toLowerCase();
var isapple = (ua.indexOf('applewebkit') != -1 ? 1 : 0);
function getmouseposition(e) {

    if (document.getElementById) {
        var iebody = (document.compatMode &&
        	document.compatMode != 'BackCompat') ?
        		document.documentElement : document.body;

        pagex = (isapple == 1 ? 0 : (ie5) ? iebody.scrollLeft : window.pageXOffset);
        pagey = (isapple == 1 ? 0 : (ie5) ? iebody.scrollTop : window.pageYOffset);
        mousex = (ie5) ? event.x : (ns6) ? clientX = e.clientX : false;
        mousey = (ie5) ? event.clientY : (ns6) ? clientY = e.clientY : false;
        var lixlpixel_tooltip = document.getElementById('tooltip');
        lixlpixel_tooltip.style.left = (mousex + pagex + offsetx) + 'px';
        lixlpixel_tooltip.style.top = (mousey + pagey + offsety) + 'px';
    }
}
function tooltip(tip) {


    if (!document.getElementById('tooltip')) newelement('tooltip');
    var lixlpixel_tooltip = document.getElementById('tooltip');

    var tableStru = "<table id='Table2' cellpadding='0' class='HelpTable' cellspacing='0' width='250px'>";
    tableStru += "<tr>";
    tableStru += "<td align='left' style='height:20px;width:10px;' class='HelpHeader'>";
    tableStru += "REPORTS</td>";
    tableStru += "</tr>";
    tableStru += "<tr>";
    tableStru += "<td style='height:20px;' class='HelpNormal' align='left' ><p>";
    tableStru += tip.toString().replace(/#1#/g, '&#39;');
    tableStru += "</p></td></tr>";
    tableStru += "</td></tr></table>";
    //

    lixlpixel_tooltip.innerHTML = tableStru;
    lixlpixel_tooltip.style.display = 'block';
    lixlpixel_tooltip.style.zIndex = '9999999';
    document.onmousemove = getmouseposition;
}
function exit() {
    document.getElementById('tooltip').style.display = 'none';
}