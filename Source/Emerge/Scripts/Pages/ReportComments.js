﻿var bt = '';
var bc = '';
var ddl = '';
$('#imgAddLine').click(function () {
    var val = prompt("Please Enter Line Name");
    if (val != null) {
        $.post('../Control/AddLineNoforReportComments', { lineName: val }, function (Message) {
            if (Message["result"] == "Inserted") {
                $('#tblLine').append('<tr><td><input type="checkbox" id=' + Message["id"] + ' name=' + Message["id"] + ' class="css-checkbox chk-style" checked="checked"/> <label for=' + Message["id"] + ' class="css-label"></label><td><span >' + Message["LineName"] + '</span></td></tr>');
                alert("Line has Inserted");                
            }
            else if (Message["result"] == "Aleary Exists") {
                alert("Title Already Exists");
            }
            else {
                alert("Error");
            }
        });
    }
});

$('#txtComments').focusout(function () {
    var ipRegex = ':';
    if ($(this).val().match(ipRegex)) {
        $('#txtComments').css("border-color", "red");
        $('#imgAddNew').css("display", "none");
    }
    else {
        $('#txtComments').css("border-color", "#d6d6d6");
        $('#imgAddNew').css("display", "block");
    }
});
$('#txtComments').focusin(function () {
    if ($(this).val() == "") {
        $('#txtComments').css("border-color", "#d6d6d6");
        $('#imgAddNew').css("display", "block");
    }
});




//Add New Records
function AddCommentsTest() {
   
    $('#divAdd').fadeIn("slow");
    $('body,html').animate({ scrollTop: $('body').height() }, 800);
    $('#txtBtnText').val("");
    $('#txtCommentBox').val("");
    $('#txtBtnText').css("background-color", "rgb(251, 251, 163)");
    $('#txtCommentBox').css("background-color", "rgb(251, 251, 163)");
    $('#ddlLine option[value="' + 1 + '"]').prop('selected', true);

    $('#txtBtnText').focus();
}


function SaveCommentsValues() {
    var flag1 = false;//used for Button title.
    var flag2 = false; //used for Button text.
    var arr = [];
    var index = 0
    $('#tblLine tr').find("span").each(function (index, object) {

        arr[index] = $(this).text();
        index = index + 1;
    });
    var bTitle = $('#txtBtnText').val();
    var bComments = $('#txtCommentBox').val();
    var line = $('#ddlLine option:selected').val();
    var lName = $('#ddlLine option:selected').text();


    if (bTitle.match(/:/) == null) {
        flag1 = true;
    }

    if (bComments.match(/:/) == null) {
        flag2 = true;
    }

    if (flag1 == true && flag2 == true) {
        $.ajax({
            type: "POST",
            url: '../Control/AddReportComments',
            data: { buttonTitle: bTitle, Comments: bComments, LineNo: line },
            beforeSend: function () {
                $('#divProgressBar').css("display", "block");
            },
            complete: function () {
                $('#divProgressBar').css("display", "none");
            },
            success: function (result) {
                var res = result.split('_');
                if (res[0] == "Added") {
                    var length = $('.ReportLeft >div').length
                    var mainDiv = $('.ReportLeft');
                    var style="background-color:#f5f5f5";
                    if (parseInt(length) % 2 == 0)
                        style = "background-color:white";
                    mainDiv.append('<div id="divid_' + (length) + '" style="' + style + '"><table cellspacing="2" cellpadding="2" width="100%" id="tblMain"><tr><td style="width: 30%; text-align:left;vertical-align:top;padding-top:3px;"><input type="hidden" id="hdnmain_' + (length) + '" value=' + (res[1]) + ' name="hdnmain_' + (length) + '" /><input type="text" id="Btext_' + (length) + '" style="width: 98%; margin-left: 3px;" name="BtnText" class="k-textbox"  required validationMessage="Please enter Text" value=' + bTitle + ' readonly="readonly"/></td><td style="width: 50%;text-align: left;padding-top:5px;"><textarea id="Comments_' + (length) + '" class="k-textbox" rows="1" cols="50" readonly="readonly" style="margin-left:3px;width:98%;vertical-align:top;" >' + bComments + '</textarea></td><td style="width: 11%; text-align:left;vertical-align:top;padding-top:3px;"><select id="ddlLine_' + (length) + '" style="width: 100px; height: 28px; border-radius: 3px;" value=' + (line) + ' selected="selected"  disabled="disabled"></select></td><td style="text-align:right;vertical-align:top;padding-top:3px;"><div><table id="tblEdit"><tr id=' + (length) + '><td><input type="hidden" id="hdn_' + (length) + '" name="ReportCommentsId" value="1"/><input type="image" src="/Content/themes/base/images/edit.png" id="Edit_' + (length) + '" title="Edit" value="Edit" style="width:15; height:18px" onclick="EnableEdit(this.id);" /></td><td><input type="image" src="/Content/themes/base/images/Save.png" id="Save_' + (length) + '"  onclick="UpdateCommentsText(this.id);" value="Save" title="Save" style="width:15px; height:18px;display: none;" /></td><td><input type="image" src="/Content/themes/base/images/Cancel.png" id="Cancel_' + (length) + '" value="Cancel" title="Cancel" onclick="CancelEdit(this.id);" style="width:15px; height:18px;display: none;" /></td><td><input type="image" src="/Content/themes/base/images/delete.png"  value="Delete" title="Delete" onclick="return DeleteReviewLog(' + res[1] + ')" style="width:15px; height:18px" /></td></tr></table></div></td></tr></table></div>');
                    var ddlIndex = '';
                    ddlIndex = $('#divid_' + length).find('#ddlLine_' + length);
                    var seloption = "";
                    for (i = 0; i < arr.length; i++) {
                        if (i == line) {
                            seloption += '<option value="' + line + '" selected="selected">' + line + '</option>';
                        }
                        else {
                            seloption += '<option value="' + arr[i] + '" >' + arr[i] + '</option>';
                        }
                    }
                    $(ddlIndex).append(seloption);
                }
                else if (res[0] == "Exists") {
                    alert("Record Already Exists.");
                }
                else {
                    alert("Error");
                }
                $('#divAdd').fadeOut("slow");
                $('#Colonmsg').fadeOut("slow");
                if($('.successmsg').length) {
                    $('.successmsg').fadeOut("slow");
                } 
            }
        });
    }
    else {
        $('#Colonmsg').css("display", "block");
        $('#Colonmsg').css("width", "219px");
        $('#Colonmsg').css("background", "rgb(237, 44, 13)");
        $('#Colonmsg').css("color", "white");
        $('#Colonmsg').text("Please remove colon (:) in the text.");
    }
}

function CancelDiv() {
    $('#divAdd').fadeOut("slow");
}



//Enable the Edit button functionality.
function EnableEdit(id) {
    var ch = id.split('_');
    $('#Save_' + ch[1]).css("display", "block");
    $('#Cancel_' + ch[1]).css("display", "block");
    $('#' + id).css("display", "none");
    $('#Btext_' + ch[1]).removeAttr("readonly");
    $('#Comments_' + ch[1]).removeAttr("readonly");
    $('#ddlLine_' + ch[1]).removeAttr("disabled");
    bt = $('#Btext_' + ch[1]).val();
    bc = $('#Comments_' + ch[1]).val();
    ddl = $('#ddlLine_' + ch[1]).val();
}

//Enable the Cancel button functionality.
function CancelEdit(id) {
    var ch = id.split('_');
    $('#Edit_' + ch[1]).css("display", "block");
    $('#Save_' + ch[1]).css("display", "none");
    $('#' + id).css("display", "none");
    $('#Btext_' + ch[1]).val(bt);
    $('#Comments_' + ch[1]).val(bc);
    $('#ddlLine_' + ch[1]).val(ddl);
    $('#Btext_' + ch[1]).attr("readonly", "readonly");
    $('#Comments_' + ch[1]).attr("readonly", "readonly");
    $('#ddlLine_' + ch[1]).attr("disabled", "disabled");
}

//Update functionality.
function UpdateCommentsText(id) {
    var flag1 = false; //used for Button title.
    var flag2 = false; //used for Button text.
    var ch = id.split('_');
    var pkId = $('#hdnmain_' + ch[1]).val();
    var bTitle = $('#Btext_' + ch[1]).val();
    var bComments = $('#Comments_' + ch[1]).val();
    var line = $('#ddlLine_' + ch[1]).find('option:selected').val();


    if (bTitle.match(/:/) == null) {
        flag1 = true;
    }

    if (bComments.match(/:/) == null) {
        flag2 = true;
    }

    if (flag1 == true && flag2 == true) {
        $.ajax({
            type: "POST",
            url: '../Control/UpdateReportComments',
            data: { pkReportCommentsId: pkId, buttonTitle: bTitle, Comments: bComments, LineNo: line },
            beforeSend: function () {
                $('#divProgressBar').css("display", "block");
            },
            complete: function () {
                $('#divProgressBar').css("display", "none");
            },
            success: function (result) {
                if (result == "Updated") {
                    $('#Edit_' + ch[1]).css("display", "block");
                    $('#Save_' + ch[1]).css("display", "none");
                    $('#Cancel_' + ch[1]).css("display", "none");
                    $('#Btext_' + ch[1]).attr("readonly", "readonly");
                    $('#Comments_' + ch[1]).attr("readonly", "readonly");
                    $('#ddlLine_' + ch[1]).attr("disabled", "disabled");
                }
                else if (result == "Exists") {
                    alert("Record Already Exists.");
                }
                else {
                    alert("Error");
                }
            }
        });
    }
    else {
        $('#Colonmsg').css("display", "block");
        $('#Colonmsg').css("width", "219px");
        $('#Colonmsg').css("background", "rgb(237, 44, 13)");
        $('#Colonmsg').css("color", "white");
        $('#Colonmsg').text("Please remove colon (:) in the text.");
    }
}

function callsubmitbtn() {

    $("#submitbtn").click();
}

function DeleteReviewLog(ReportCommentsId) {
    if (confirm("Are you sure to delete?")) {
        $.post('../Control/DeleteReportComments', { pkReportCommentsId: ReportCommentsId }, function (Message) {
            window.location.href = 'ReportComments?Status=' + Message;
        });
    }
    else {
        return false;
    }
}