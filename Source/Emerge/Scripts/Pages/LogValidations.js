﻿var CCRCount = 0;
function anothertempfunction() {
    $('#Address').css("background-color", "transparent");
    $('#Street').css("background-color", "transparent");
    $('#City').css("background-color", "transparent");
    $('#Apt').css("background-color", "transparent");
    // $('#State').css("background-color", "transparent");
    $('#Zip').css("background-color", "transparent");
    $("div[id=form1_Address_errorloc]").html("");
    $("div[id=form1_Street_errorloc]").html("");
    $("div[id=form1_City_errorloc]").html("");
    $("div[id=form1_apt_errorloc]").html("");
    $("div[id=form1_State_errorloc]").html("");
    $("div[id=form1_Zip_errorloc]").html("");

}
function enableDisableValidatorsLog(cid, pkgid, IsTiered, checked, pkOrderDetailId) {
    //    var checkboxid = "#" + cid;
    //    if ($("#IsEscreenEnable").val() == "False") {
    //       $(checkboxid).prop("checked", false);
    //        var chekbox_tag = "input[id='" + cid + "']";
    //        if ($(chekbox_tag).hasClass("escreen")) {
    //            $("#divnotallowescreen").kendoWindow({
    //                draggable: true,
    //                modal: true,
    //                resizable: true
    //            }).data("kendoWindow");
    //    
    //            $("#divnotallowescreen").css("visibility", "visible");
    //            $("#divnotallowescreen").data("kendoWindow").center();
    //            $("#divnotallowescreen").data("kendoWindow").open();
    //            return false;
    //        }
    //    }




    anothertempfunction();
    var IsWCVacknoldge = document.getElementById("IsWCVAcknowledged").checked;
    // alert(cid + '  ' + pkgid);
    // alert(document.getElementById('ProductsSelected').value);
    var ControlName = '';
    var unSelectControl_Ids = '';
    var selectControl_Ids = '';
    //var theColorIs = $('#' + cid).css("background-color").toString();
    var hdnIsTiered = document.getElementById('hdnIsTiered');
    var theColorIs = document.getElementById(cid).style.background.toString();
    //alert(theColorIs);
    if (checked == false) {
        if (pkgid > 0) {
            var vid = cid.split('_')[0];
            unSelectControl_Ids = vid.split(",");
        }
        else {
            unSelectControl_Ids = cid.split(",");
        }

        //  alert('unId ' + unSelectControl_Ids);
        // $('#' + cid).css("background-color", "transparent");
        document.getElementById(cid).style.background = 'transparent';
        if (hdnIsTiered != null) { if (IsTiered == 1 && hdnIsTiered.value != '0') { hdnIsTiered.value = '0'; } }
    }
    else {

        if (hdnIsTiered != null) {
            if (hdnIsTiered.value != '0') { alert('Only one Tiered package is allowed'); return false; }
            else if (document.getElementById('ProductsSelected').value != '' && IsTiered == 1) { alert('Only one Tiered package is allowed'); return false; }
            else if (IsTiered == 1) { hdnIsTiered.value = cid; }
        }


        if (pkgid > 0) {
            var vid = cid.split('_')[0];
            selectControl_Ids = vid.split(",");
        }
        else {
            selectControl_Ids = cid.split(",");
        }
        // $('#' + cid).css("background-color", "#FFFFAA");   
        document.getElementById(cid).style.background = '#FFFFAA';


    }
    // alert(selectControl_Ids);
    var XPath = '';
    if (window.location.href.toLowerCase().indexOf('runreport') > 0) {
        XPath = 'Content/';
    }
    else {
        XPath = '../Content/';
    }
    var i = 1;
    $.ajax({
        type: "GET",
        url: XPath + "ProductXml/ProductRequireData.xml",
        dataType: "xml",
        complete: function () {
            // alert($('#nr_ProductsSelected').val());
            if ($('#ProductsSelected').val().indexOf("WCV_") > 0 && IsWCVacknoldge == false) {

                document.getElementById("WorkersCompDisclaimerPop").style.visibility = "visible";
                document.getElementById("WorkersCompDisclaimerPop").style.display = "block";
                var windowOpen = $("#WorkersCompDisclaimerPop").kendoWindow({
                    draggable: true,
                    actions: false,
                    modal: true,
                    title: "Workers Compensation Disclaimer",
                    height: "250px",
                    width: "600px"

                }).data("kendoWindow");

                windowOpen.center();
                windowOpen.open();
            }


            else if ($('#ProductsSelected').val().indexOf("PS_") > 0) {
                // $('#lbl_SocialOptional').html('(Optional)');
                if ((document.getElementById('Social')) != null) {
                    $("#Social").css("background-color", "#FFFFAA")
                }
            }
            else if ($('#ProductsSelected').val().indexOf("PS2") > 0 || $('#ProductsSelected').val().indexOf("SSR") > 0 || $('#ProductsSelected').val().indexOf("TDR") > 0 || $('#ProductsSelected').val().indexOf("NCRPLUS") > 0 || $('#ProductsSelected').val().indexOf("SCR") > 0 || $('#ProductsSelected').val().indexOf("ECR") > 0 || $('#ProductsSelected').val().indexOf("EEI") > 0 || $('#ProductsSelected').val().indexOf("CCR1") > 0 || $('#ProductsSelected').val().indexOf("CCR2") > 0 || $('#ProductsSelected').val().indexOf("EMV") > 0 || $('#ProductsSelected').val().indexOf("EDV") > 0 || $('#ProductsSelected').val().indexOf("PRV") > 0 || $('#ProductsSelected').val().indexOf("PLV") > 0 || $('#ProductsSelected').val().indexOf("WCV") > 0 || $('#ProductsSelected').val().indexOf("CDLIS") > 0 || $('#ProductsSelected').val().indexOf("PS3") > 0 || $('#ProductsSelected').val().indexOf("TDR2") > 0 || $('#ProductsSelected').val().indexOf("EMV2") > 0) {
                if ((document.getElementById('Social')) != null) {
                    $("#Social").css("background-color", "#FFFFAA")
                }
            }
            else {
                // $('#lbl_SocialOptional').html('');
                if ((document.getElementById('Social')) != null) {
                    $("#Social").css("background-color", "transparent")
                }
            }
            if ($('#ProductsSelected').val().indexOf("#RCX_") >= 0) {
                $('#hidIsRcxSelected').val('true');
            }
            else {
                $('#hidIsRcxSelected').val('false');
            }
            $('#' + ControlName + '_ImgSearch').removeAttr('disabled');
            checkScrState();
        }, success: function (data) {


            // alert('un '+ unSelectControl_Ids);
            if (unSelectControl_Ids.length > 0) {
                for (j = 0; j < unSelectControl_Ids.length; j++) {


                    if (!parseInt(unSelectControl_Ids[j])) {
                        var ProductCode = unSelectControl_Ids[j];

                        //  closeLeftPanel(ProductCode);
                        var $selectedElement = $(data).find('Product[Code=' + unSelectControl_Ids[j] + ']');


                        var collapseboxid = $selectedElement.attr('CollapseBox');

                        //hide right panel div on report unselected
                        var tblId = document.getElementById(collapseboxid);
                        if (tblId != null) {
                            document.getElementById(collapseboxid).style.display = 'none';

                            //Change Image of Right panel div
                            try {
                                var Lastid = collapseboxid.substring(collapseboxid.length - 1, collapseboxid.length);
                                var img = $('#imgApplicant' + Lastid);
                                img.attr('src', '' + XPath + 'themes/base/images/arrowexpand.gif');
                            } catch (e) {

                            }
                        }
                        var ClosestTable = $('#' + ProductCode).closest('table');
                        try {
                            if (ClosestTable[0] != null) {
                                // ClosestTable[0].style.display = 'none';
                            }
                        } catch (e) {

                        }


                        var c = 1;
                        $selectedElement.find('RequiredData').each(function () {

                            //We have added this blank if condition becuase want to change of color of all State dropdowns. we want only on selection of 1
                            if ($(this).text().indexOf('CountyInfoState') == 0 || $(this).text().indexOf('CountyInfoCounty') == 0) {
                            }
                            else {

                                var Controls = document.getElementsByName($(this).text());
                                for (var i = 0; i < Controls.length; i++) {
                                    var id = Controls[i].id;
                                    $("#" + id).css("background-color", "transparent");
                                }
                            }
                            var divId = 'form1_' + $(this).text() + '_errorloc';
                            var RequiredDivId = document.getElementById(divId);
                            if (RequiredDivId != null) {
                                RequiredDivId.innerHTML = '';
                            }
                        });

                        RemoveRule(); //This will remove all the validation on this page


                        // Change color one by one 
                        if (unSelectControl_Ids[j].indexOf('CCR1') >= 0 || unSelectControl_Ids[j].indexOf('CCR2') >= 0 || unSelectControl_Ids[j].indexOf('RCX') >= 0) {

                            var children = document.getElementById('tr_' + pkOrderDetailId);
                            var list = children.getElementsByTagName('select');
                            var Sid = list[0].id;
                            var Cid = list[1].id;
                            $("#" + Sid).css("background-color", "transparent");
                            $("#" + Cid).css("background-color", "transparent");

                            var tblStateList = document.getElementById('tblStateList');
                            var allSelectList = tblStateList.getElementsByTagName('select');
                            for (var k = 0; k < allSelectList.length; k++) {

                                var theColorIs = document.getElementById(allSelectList[k].id).style.background.toString();
                                if ((theColorIs.indexOf("rgb(255, 255, 170)") >= 0) || theColorIs == "#ffffaa") {
                                    AddDDLRule(allSelectList[k].id);
                                }
                            }
                            //Remove Rule method have removed all the validations now we added validation again for other CCR1, CCR2, RCX validations.
                            //                            for (var k = 0; k < CCRCount; k++) {
                            //                                id = Controls[k].id;
                            //                                Cid = CountyInfoCounty[k].id;
                            //                                AddDDLRule(id); AddDDLRule(Cid);
                            //                            }
                        }


                        unselectProductCode(ControlName, unSelectControl_Ids[j], pkgid + "_" + pkOrderDetailId);
                    }
                }
            }
            ControlName = '';


            for (j = 0; j < selectControl_Ids.length; j++) {
                if (selectControl_Ids[j].indexOf('CCR1') >= 0 || selectControl_Ids[j].indexOf('CCR2') >= 0 || selectControl_Ids[j].indexOf('RCX') >= 0) {
                    var children = document.getElementById('tr_' + pkOrderDetailId);
                    var list = children.getElementsByTagName('select');
                    var Sid = list[0].id;
                    var Cid = list[1].id;
                    $("#" + Sid).css("background-color", "#FFFFAA");
                    $("#" + Cid).css("background-color", "#FFFFAA");
                    AddDDLRule(Sid); AddDDLRule(Cid);
                }

                if (!parseInt(selectControl_Ids[j])) {
                    var ProductCode = selectControl_Ids[j];

                    var $selectedElement = $(data).find('Product[Code=' + selectControl_Ids[j] + ']');
                    var collapseboxid = $selectedElement.attr('CollapseBox');
                    var tblId = document.getElementById(collapseboxid);
                    if (tblId != null) {
                        document.getElementById(collapseboxid).style.display = 'block';

                        try {
                            var Lastid = collapseboxid.substring(collapseboxid.length - 1, collapseboxid.length);
                            var img = $('#imgApplicant' + Lastid);
                            img.attr('src', '' + XPath + 'themes/base/images/arrowcollaspe.png');
                        } catch (e) {

                        }
                    }

                    //alert(ProductCode+' ' +$(ProductCode)+' '+ $(ProductCode).closest('table'));
                    var ClosestTable = $('#' + ProductCode).closest('div');

                    try {
                        if (ClosestTable[0] != null && pkgid > 0) {
                            ClosestTable[0].style.display = '';

                        }
                    } catch (e) {

                    }


                    var c = 1;
                    $selectedElement.find('RequiredData').each(function () {

                        // ControlName = GetControlName(selectControl_Ids[j], c);

                        //                        $('#' + $(this).text()).css("background-color", "#FFFFAA");
                        //                        if ($(this).text().indexOf('State') == 0 || $(this).text().indexOf('CountyInfoState') == 0 || $(this).text().indexOf('CountyInfoCounty') >= 0 ||
                        //                        $(this).text().indexOf('DLState') >= 0 || $(this).text().indexOf('Sex') >= 0 || $(this).text().indexOf('InstituteState') == 0
                        //                        || $(this).text().indexOf('CompanyState') == 0 || $(this).text().indexOf('LicenseState') == 0 || $(this).text().indexOf('Jurisdiction') == 0) {
                        //                            AddDDLRule($(this).text());
                        //                        }
                        //                        else {

                        //                            AddRule($(this).text());
                        //                        }
                        //alert(c);
                        c++;
                    });

                    selectProductCode(ControlName, selectControl_Ids[j], pkgid + "_" + pkOrderDetailId);
                }
            }


            CheckFederalReport();
            ControlName = '';
            // alert(document.getElementById('ProductsSelected').value);
            var selectHiddenControl_Ids = document.getElementById('ProductsSelected').value.substring(1, (document.getElementById('ProductsSelected').value.length - 1)).split("##");
            if (selectHiddenControl_Ids.length > 0) {
                for (j = 0; j < selectHiddenControl_Ids.length; j++) {

                    //alert(selectHiddenControl_Ids[j]);
                    var splitHdn = '';
                    splitHdn = selectHiddenControl_Ids[j].split("_");

                    var $selectedElement = $(data).find('Product[Code=' + splitHdn[0] + ']');
                    var c = 1;
                    $selectedElement.find('RequiredData').each(function () {


                        if ($(this).text().indexOf('State') == 0 || $(this).text().indexOf('DLState') == 0 || $(this).text().indexOf('Sex') == 0 || $(this).text().indexOf('InstituteState') == 0
                        || $(this).text().indexOf('CompanyState') == 0 || $(this).text().indexOf('LicenseState') == 0 || $(this).text().indexOf('Jurisdiction') == 0 || $(this).text().indexOf('BusinessState') == 0) {

                            var Controls = document.getElementsByName($(this).text());
                            for (var i = 0; i < Controls.length; i++) {
                                var id = Controls[i].id;
                                $("#" + id).css("background-color", "#FFFFAA")
                                AddDDLRule(id);
                            }

                        }
                        else if ($(this).text().indexOf('CountyInfoState') == 0 || $(this).text().indexOf('CountyInfoCounty') >= 0) {
                            //Did this code above
                        }
                        else {

                            var Controls = document.getElementsByName($(this).text());
                            for (var i = 0; i < Controls.length; i++) {
                                var id = Controls[i].id;
                                $("#" + id).css("background-color", "#FFFFAA")
                                AddRule(id);
                            }
                            if ((document.getElementById('ApplicantEmail')) != null) {
                                $("#ApplicantEmail").css("background-color", "#FFFFAA")
                            }

                        }
                        if ($(this).text().indexOf('DOB') >= 0) {
                            AddDOBRule($(this).text());
                        }
                        if ($(this).text().indexOf('Phone') == 0) {
                            AddPhoneRule($(this).text());
                        }
                        if ($(this).text().indexOf('Social') == 0) {
                            AddSSNRule($(this).text());
                        }
                        c++;
                    });
                    $('#' + splitHdn[0]).css("background-color", "#FFFFAA");
                    // }
                }
            }
            if (selectHiddenControl_Ids == '') {
                if ((document.getElementById('ApplicantEmail')) != null) {
                    $("#ApplicantEmail").css("background-color", "transparent");
                    var divId = 'form1_ApplicantEmail_errorloc';
                    var RequiredDivId = document.getElementById(divId);
                    if (RequiredDivId != null) {
                        RequiredDivId.innerHTML = '';
                    }
                }
            }

            ControlName = '';
            CheckNCR1Report();
            CheckConditionsOnState($("#State").val());



            if (IsTiered == 1)
            {
                $('#DivJurisdiction').find('#Jurisdiction').css('background-color', 'transparent');
            }

            


        }
    });

    var g = setTimeout(function () {
        var colorSocial = $("#Social").css("background-color");
        if (colorSocial.indexOf("rgb(255, 255, 170)") >= 0) {
            if ($("div[id=form1_FirstName_errorloc]").html() == "") {
                if ($("div[id=form1_Social_errorloc]").html() == "*Required*") {
                    $("div[id=form1_Social_errorloc]").html("");
                }
            }

        }
        else {
            $("div[id=form1_Social_errorloc]").html("");
        }
    }, 200);
    specialvalidatorslog();
    return false;

}





function specialvalidatorslog() {
    $('#Apt').css("background-color", "transparent");
    $("div[id=form1_Address]").attr("id", "form1_Address_errorloc");
    $("div[id=form1_Street]").attr("id", "form1_Street_errorloc");
    $("div[id=form1_City]").attr("id", "form1_City_errorloc");
    $("div[id=form1_State]").attr("id", "form1_State_errorloc");
    $("div[id=form1_Zip]").attr("id", "form1_Zip_errorloc");
    var gg = "";
    $(".tempval").val("");
    var f = setTimeout(function () {
        var t = setTimeout(function () {
            if (document.getElementById('ProductsSelected').value == "") {
                $('#Address').css("background-color", "transparent");
                $('#Street').css("background-color", "transparent");
                $('#City').css("background-color", "transparent");
                $('#Apt').css("background-color", "transparent");
                $('#State').css("background-color", "transparent");
                $('#Zip').css("background-color", "transparent");
                $("div[id=form1_Address_errorloc]").html("");
                $("div[id=form1_Street_errorloc]").html("");
                $("div[id=form1_City_errorloc]").html("");
                $("div[id=form1_apt_errorloc]").html("");
                $("div[id=form1_State_errorloc]").html("");
                $("div[id=form1_Zip_errorloc]").html("");
            }
            if (document.getElementById('ProductsSelected').value.indexOf("NCR") > 0) {
                if (document.getElementById('ProductsSelected').value.indexOf("PS") > 0) {
                    var val_selected = document.getElementById('ProductsSelected').value.split('##');
                    var check_selected = 0;
                    for (var a = 0; a < val_selected.length; a++) {
                        if (val_selected[a].indexOf("NCR") >= 0 || val_selected[a].indexOf("PS") >= 0) { }
                        else { check_selected = 1; }
                    }
                    if (check_selected == "0") {
                        $('#Address').css("background-color", "transparent");
                        $('#Street').css("background-color", "transparent");
                        $('#City').css("background-color", "transparent");
                        $('#Apt').css("background-color", "transparent");
                        $('#Zip').css("background-color", "transparent");
                        $("div[id=form1_Address_errorloc]").html("");
                        $("div[id=form1_Street_errorloc]").html("");
                        $("div[id=form1_City_errorloc]").html("");
                        $("div[id=form1_apt_errorloc]").html("");
                        $("div[id=form1_State_errorloc]").html("");
                        $("div[id=form1_Zip_errorloc]").html("");
                        $(".tempval").val("2");
                    }
                }
                if (document.getElementById('ProductsSelected').value.indexOf("PS") < 0) {
                    $('#Address').css("background-color", "#FFFFAA");
                    $('#Street').css("background-color", "#FFFFAA");
                    $('#City').css("background-color", "#FFFFAA");
                    $('#State').css("background-color", "#FFFFAA");
                    $('#Zip').css("background-color", "#FFFFAA");
                    $("div[id=form1_Address]").attr("id", "form1_Address_errorloc");
                    $("div[id=form1_Street]").attr("id", "form1_Street_errorloc");
                    $("div[id=form1_City]").attr("id", "form1_City_errorloc");
                    $("div[id=form1_apt]").attr("id", "form1_apt_errorloc");
                    $("div[id=form1_State]").attr("id", "form1_State_errorloc");
                    $("div[id=form1_Zip]").attr("id", "form1_Zip_errorloc");
                    $(".tempval").val("1");
                }
            }
            if (document.getElementById('ProductsSelected').value.indexOf("NCR") < 0) {
                $(".tempval").val("");
            }
        }, 50)
        var t = setTimeout(function () {
            if ($("#hdnextracheckvalidation").val() == "5") {
                if (document.getElementById('ProductsSelected').value != "") {
                    $('#FirstName').css("background-color", "#FFFFAA");
                    $('#LastName').css("background-color", "#FFFFAA");
                    $('#Social').css("background-color", "#FFFFAA");
                    $('#DOB').css("background-color", "#FFFFAA");
                    $('#ApplicantEmail').css("background-color", "#FFFFAA");
                    $("div[id=form1_LastName_errorloc]").attr("id", "form1_LastName");
                    $("div[id=form1_FirstName_errorloc]").attr("id", "form1_FirstName");
                    $("div[id=form1_Social_errorloc]").attr("id", "form1_Social");
                    $("div[id=form1_DOB_errorloc]").attr("id", "form1_DOB");
                    $("div[id=form1_ApplicantEmail_errorloc]").attr("id", "form1_ApplicantEmail");
                }
                else {
                    $('#FirstName').css("background-color", "transparent");
                    $('#LastName').css("background-color", "transparent");
                    $('#Social').css("background-color", "transparent");
                    $('#DOB').css("background-color", "transparent");
                    $("div[id=form1_LastName]").html("");
                    $("div[id=form1_FirstName]").html("");
                    $("div[id=form1_Social]").html("");
                    $("div[id=form1_DOB]").html("");
                    $("div[id=form1_ApplicantEmail]").html("");
                }
            }
        }, 50);
    }, 400);

}


function selectProductCode(ControlName, ProductCode, PackageId) {
    $('#' + ProductCode).css("background-color", "#FFFFAA");
    var SelectedProducts = document.getElementById('ProductsSelected').value;
    document.getElementById('ProductsSelected').value = SelectedProducts + '#' + ProductCode + '_' + PackageId + '#';
}

function unselectProductCode(ControlName, ProductCode, PackageId) {

    $('#' + ProductCode).css("background-color", "transparent");
    var SelectedProducts = document.getElementById('ProductsSelected').value;
    document.getElementById('ProductsSelected').value = SelectedProducts.replace('#' + ProductCode + '_' + PackageId + '#', "");
}

function checkScrState() {
    var ReturnVariable = true; if ($('#ProductsSelected').val().indexOf("#SCR#") >= 0) {
        var SelectedStateName = ""; switch ($('#State').val()) { case "5": SelectedStateName = "California"; break; case "21": SelectedStateName = "Louisiana"; break; case "44": SelectedStateName = "Ohio"; break; case "54": SelectedStateName = "Vermont"; break; case "57": SelectedStateName = "West Virginia"; break; case "59": SelectedStateName = "Wyoming"; break; }
        if (SelectedStateName != "") { alert("Sorry Manual State Criminal is NOT Available in " + SelectedStateName + " please use Manual County Search."); ReturnVariable = false; }
    }
    return ReturnVariable;
}

function CheckNCR1Report() {
    if ($('#ProductsSelected').val().indexOf("#NCR1") >= 0) {
        $("#DivNullDOB").css("display", "block");
    }
    else {
        $("#DivNullDOB").css("display", "none");
    }
}

function CheckConditionsOnState(SelectedValue) {

    //  CheckIllinoisState();
    StateValues = SelectedValue.split('_');
    IsStateLiveRunner = StateValues[1];
    if ($('#ProductsSelected').val().indexOf("#SCR") >= 0 && IsStateLiveRunner == "True") {
        $("#trStateLiveRunner").css("display", "block");
    }
    else {
        $("#trStateLiveRunner").css("display", "none");
    }

}




function CheckFederalReport() {

    if ($('#ProductsSelected').val().indexOf("#FCR_") >= 0) {
        $("#DivJurisdiction").css("visibility", "visible");
        $("#DivJuriHeader").css("visibility", "visible");
        

    }
    else {
        $("#DivJurisdiction").css("visibility", "hidden");
        $("#DivJuriHeader").css("visibility", "hidden");
    }

}




function enableDisableValidatorsBatch(cid, pkgid, IsTiered) {
    var IsWCVacknoldge = document.getElementById("IsWCVAcknowledged").checked;
    // alert(cid + '  ' + pkgid);
    // alert(document.getElementById('ProductsSelected').value);
    var ControlName = '';
    var unSelectControl_Ids = '';
    var selectControl_Ids = '';
    //var theColorIs = $('#' + cid).css("background-color").toString();
    var hdnIsTiered = document.getElementById('hdnIsTiered');
    var theColorIs = document.getElementById(cid).style.background.toString();
    //alert(theColorIs);
    if ((theColorIs.indexOf("rgb(255, 255, 170)") >= 0) || theColorIs == "#ffffaa") {
        if (pkgid > 0) {
            var vid = cid.split('_')[0];
            unSelectControl_Ids = vid.split(",");
        }
        else {
            unSelectControl_Ids = cid.split(",");
        }

        //  alert('unId ' + unSelectControl_Ids);
        // $('#' + cid).css("background-color", "transparent");
        document.getElementById(cid).style.background = 'transparent';
        if (hdnIsTiered != null) { if (IsTiered == 1 && hdnIsTiered.value != '0') { hdnIsTiered.value = '0'; } }
    }
    else {

        if (hdnIsTiered != null) {
            if (hdnIsTiered.value != '0') { alert('Only one Tiered package is allowed'); return false; }
            else if (document.getElementById('ProductsSelected').value != '' && IsTiered == 1) { alert('Only one Tiered package is allowed'); return false; }
            else if (IsTiered == 1) { hdnIsTiered.value = cid; }
        }


        if (pkgid > 0) {
            var vid = cid.split('_')[0];
            selectControl_Ids = vid.split(",");
        }
        else {
            selectControl_Ids = cid.split(",");
        }
        // $('#' + cid).css("background-color", "#FFFFAA");

        document.getElementById(cid).style.background = '#FFFFAA';


    }
    // alert(selectControl_Ids);
    var XPath = '';
    if (window.location.href.toLowerCase().indexOf('runreport') > 0) {
        XPath = 'Content/';
    }
    else {
        XPath = '../Content/';
    }
    var i = 1;
    $.ajax({
        type: "GET",
        url: XPath + "ProductXml/ProductRequireData.xml",
        dataType: "xml",
        complete: function () {

            if ($('#ProductsSelected').val().indexOf("WCV_") > 0 && IsWCVacknoldge == false) {

                document.getElementById("WorkersCompDisclaimerPop").style.visibility = "visible";
                document.getElementById("WorkersCompDisclaimerPop").style.display = "block";
                var windowOpen = $("#WorkersCompDisclaimerPop").kendoWindow({
                    draggable: true,
                    actions: false,
                    modal: true,
                    title: "Workers Compensation Disclaimer",
                    height: "250px",
                    width: "600px"

                }).data("kendoWindow");

                windowOpen.center();
                windowOpen.open();
            }

            else if ($('#ProductsSelected').val().indexOf("PS_") > 0 && $('#ProductsSelected').val().indexOf("BPS_") < 0) {
                // $('#lbl_SocialOptional').html('(Optional)');
                if ((document.getElementById('Social')) != null) {
                    $("#Social").css("background-color", "#FFFFAA")
                }
            }

            else if ($('#ProductsSelected').val().indexOf("PS2") > 0 || $('#ProductsSelected').val().indexOf("SSR") > 0 || $('#ProductsSelected').val().indexOf("TDR") > 0 || $('#ProductsSelected').val().indexOf("NCRPLUS") > 0 || $('#ProductsSelected').val().indexOf("SCR") > 0 || $('#ProductsSelected').val().indexOf("ECR") > 0 || $('#ProductsSelected').val().indexOf("EEI") > 0 || $('#ProductsSelected').val().indexOf("CCR1") > 0 || $('#ProductsSelected').val().indexOf("CCR2") > 0 || $('#ProductsSelected').val().indexOf("EMV") > 0 || $('#ProductsSelected').val().indexOf("EDV") > 0 || $('#ProductsSelected').val().indexOf("PRV") > 0 || $('#ProductsSelected').val().indexOf("PLV") > 0 || $('#ProductsSelected').val().indexOf("WCV") > 0 || $('#ProductsSelected').val().indexOf("CDLIS") > 0 || $('#ProductsSelected').val().indexOf("PS3") > 0 || $('#ProductsSelected').val().indexOf("TDR2") > 0 || $('#ProductsSelected').val().indexOf("EMV2") > 0) {
                if ((document.getElementById('Social')) != null) {
                    $("#Social").css("background-color", "#FFFFAA")
                }
            }
            else {
                // $('#lbl_SocialOptional').html('');
                if ((document.getElementById('Social')) != null) {
                    $("#Social").css("background-color", "transparent")
                }
            }
            if ($('#ProductsSelected').val().indexOf("#RCX_") >= 0) {
                $('#hidIsRcxSelected').val('true');
            }
            else {
                $('#hidIsRcxSelected').val('false');
            }
            $('#' + ControlName + '_ImgSearch').removeAttr('disabled');
            checkScrState();
        }, success: function (data) {


            //alert('un '+ unSelectControl_Ids.length);
            if (unSelectControl_Ids.length > 0) {
                for (j = 0; j < unSelectControl_Ids.length; j++) {
                    if (!parseInt(unSelectControl_Ids[j])) {
                        var ProductCode = unSelectControl_Ids[j];

                        //  closeLeftPanel(ProductCode);
                        var $selectedElement = $(data).find('Product[Code=' + unSelectControl_Ids[j] + ']');


                        var collapseboxid = $selectedElement.attr('CollapseBox');

                        //hide right panel div on report unselected
                        var tblId = document.getElementById(collapseboxid);
                        if (tblId != null) {
                            document.getElementById(collapseboxid).style.display = 'none';

                            //Change Image of Right panel div
                            try {
                                var Lastid = collapseboxid.substring(collapseboxid.length - 1, collapseboxid.length);
                                var img = $('#imgApplicant' + Lastid);
                                img.attr('src', '' + XPath + 'themes/base/images/arrowexpand.gif');
                            } catch (e) {

                            }
                        }
                        var ClosestTable = $('#' + ProductCode).closest('table');
                        try {
                            if (ClosestTable[0] != null) {
                                // ClosestTable[0].style.display = 'none';
                            }
                        } catch (e) {

                        }
                        var c = 1;
                        $selectedElement.find('RequiredData').each(function () {


                            var Controls = document.getElementsByName($(this).text());
                            for (var i = 0; i < Controls.length; i++) {
                                var id = Controls[i].id;
                                $("#" + id).css("background-color", "transparent")



                            }
                            var divId = 'form1_' + $(this).text() + '_errorloc';
                            var RequiredDivId = document.getElementById(divId);
                            if (RequiredDivId != null) {
                                RequiredDivId.innerHTML = '';
                            }
                        });

                        RemoveRule();
                        unselectProductCode(ControlName, unSelectControl_Ids[j], pkgid);
                    }
                }
            }
            ControlName = '';


            for (j = 0; j < selectControl_Ids.length; j++) {


                if (!parseInt(selectControl_Ids[j])) {
                    var ProductCode = selectControl_Ids[j];


                    var $selectedElement = $(data).find('Product[Code=' + selectControl_Ids[j] + ']');

                    var collapseboxid = $selectedElement.attr('CollapseBox');
                    var tblId = document.getElementById(collapseboxid);
                    if (tblId != null) {
                        document.getElementById(collapseboxid).style.display = 'block';

                        try {
                            var Lastid = collapseboxid.substring(collapseboxid.length - 1, collapseboxid.length);
                            var img = $('#imgApplicant' + Lastid);
                            img.attr('src', '' + XPath + 'themes/base/images/arrowcollaspe.png');
                        } catch (e) {

                        }
                    }
                    var ClosestTable = $('#' + ProductCode).closest('div');
                    try {
                        if (ClosestTable[0] != null && pkgid > 0) {
                            ClosestTable[0].style.display = '';

                        }
                    } catch (e) {

                    }
                    var c = 1;
                    $selectedElement.find('RequiredData').each(function () {
                        c++;
                    });
                    selectProductCode(ControlName, selectControl_Ids[j], pkgid);

                }
            }

            CheckFederalReport();
            ControlName = '';

            var selectHiddenControl_Ids = document.getElementById('ProductsSelected').value.substring(1, (document.getElementById('ProductsSelected').value.length - 1)).split("##");
            //   alert(selectHiddenControl_Ids);
            if (selectHiddenControl_Ids.length > 0) {
                for (j = 0; j < selectHiddenControl_Ids.length; j++) {

                    var splitHdn = '';
                    splitHdn = selectHiddenControl_Ids[j].split("_");

                    var $selectedElement = $(data).find('Product[Code=' + splitHdn[0] + ']');
                    var c = 1;
                    $selectedElement.find('RequiredData').each(function () {

                        var Controls = document.getElementsByName($(this).text());
                        for (var i = 0; i < Controls.length; i++) {
                            var id = Controls[i].id;
                            // alert(id);
                            $("#" + id).css("background-color", "#FFFFAA")
                            AddDDLRule(id);
                        }
                        if ((document.getElementById('ApplicantEmail')) != null) {
                            $("#ApplicantEmail").css("background-color", "#FFFFC1")
                        }
                        c++;
                    });

                    $('#' + splitHdn[0]).css("background-color", "#FFFFAA");

                }
            }
            if (selectHiddenControl_Ids == '') {
                if ((document.getElementById('ApplicantEmail')) != null) {
                    $("#ApplicantEmail").css("background-color", "transparent");
                    var divId = 'form1_ApplicantEmail_errorloc';
                    var RequiredDivId = document.getElementById(divId);
                    if (RequiredDivId != null) {
                        RequiredDivId.innerHTML = '';
                    }
                }
            }
            ControlName = '';
            CheckNCR1Report();
            CheckConditionsOnState($("#State").val());
        }
    });
    specialvalidatorsbatch();
    return false;

}


function specialvalidatorsbatch() {
    anothertempfunction();
    $("div[id=form1_Address]").attr("id", "form1_Address_errorloc");
    $("div[id=form1_Street]").attr("id", "form1_Street_errorloc");
    $("div[id=form1_City]").attr("id", "form1_City_errorloc");
    // $("div[id=form1_apt]").attr("id", "form1_apt_errorloc");
    $("div[id=form1_State]").attr("id", "form1_State_errorloc");
    $("div[id=form1_Zip]").attr("id", "form1_Zip_errorloc");
    var gg = "";
    $(".tempval").val("");
    var f = setTimeout(function () {
        var t = setTimeout(function () {

            if (document.getElementById('ProductsSelected').value == "") {
                $('#Address').css("background-color", "transparent");
                $('#Street').css("background-color", "transparent");
                $('#City').css("background-color", "transparent");
                $('#Apt').css("background-color", "transparent");
                $('#State').css("background-color", "transparent");
                $('#Zip').css("background-color", "transparent");
                $("div[id=form1_Address_errorloc]").html("");
                $("div[id=form1_Street_errorloc]").html("");
                $("div[id=form1_City_errorloc]").html("");
                $("div[id=form1_apt_errorloc]").html("");
                $("div[id=form1_State_errorloc]").html("");
                $("div[id=form1_Zip_errorloc]").html("");
            }

            if (document.getElementById('ProductsSelected').value.indexOf("NCR") > 0) {

                if (document.getElementById('ProductsSelected').value.indexOf("PS") > 0) {
                    var val_selected = document.getElementById('ProductsSelected').value.split('##');
                    var check_selected = 0;
                    for (var a = 0; a < val_selected.length; a++) {
                        if (val_selected[a].indexOf("NCR") >= 0 || val_selected[a].indexOf("PS") >= 0) { }
                        else { check_selected = 1; }
                    }

                    if (check_selected == "0") {
                        $('#Address').css("background-color", "transparent");
                        $('#Street').css("background-color", "transparent");
                        $('#City').css("background-color", "transparent");
                        $('#Apt').css("background-color", "transparent");
                        // $('#State').css("background-color", "transparent");
                        $('#Zip').css("background-color", "transparent");
                        $("div[id=form1_Address_errorloc]").html("");
                        $("div[id=form1_Street_errorloc]").html("");
                        $("div[id=form1_City_errorloc]").html("");
                        $("div[id=form1_apt_errorloc]").html("");
                        $("div[id=form1_State_errorloc]").html("");
                        $("div[id=form1_Zip_errorloc]").html("");
                        $(".tempval").val("2");
                    }
                }
                if (document.getElementById('ProductsSelected').value.indexOf("PS") < 0) {

                    $('#Address').css("background-color", "#FFFFAA");
                    $('#Street').css("background-color", "#FFFFAA");
                    $('#City').css("background-color", "#FFFFAA");
                    // $('#Apt').css("background-color", "#FFFFAA");
                    $('#State').css("background-color", "#FFFFAA");
                    $('#Zip').css("background-color", "#FFFFAA");
                    $("div[id=form1_Address]").attr("id", "form1_Address_errorloc");
                    $("div[id=form1_Street]").attr("id", "form1_Street_errorloc");
                    $("div[id=form1_City]").attr("id", "form1_City_errorloc");
                    $("div[id=form1_apt]").attr("id", "form1_apt_errorloc");
                    $("div[id=form1_State]").attr("id", "form1_State_errorloc");
                    $("div[id=form1_Zip]").attr("id", "form1_Zip_errorloc");
                    $(".tempval").val("1");
                }
            }
            if (document.getElementById('ProductsSelected').value.indexOf("NCR") < 0) {

                $(".tempval").val("");
            }
        }, 50)
        var t = setTimeout(function () {
            if ($("#hdnextracheckvalidation").val() == "5") {
                if (document.getElementById('ProductsSelected').value != "") {
                    $('#FirstName').css("background-color", "#FFFFAA");
                    $('#LastName').css("background-color", "#FFFFAA");
                    $('#Social').css("background-color", "#FFFFAA");
                    // $('#Apt').css("background-color", "#FFFFAA");
                    $('#DOB').css("background-color", "#FFFFAA");
                    $('#ApplicantEmail').css("background-color", "#FFFFAA");
                    $("div[id=form1_LastName_errorloc]").attr("id", "form1_LastName");
                    $("div[id=form1_FirstName_errorloc]").attr("id", "form1_FirstName");
                    $("div[id=form1_Social_errorloc]").attr("id", "form1_Social");
                    // $("div[id=form1_apt]").attr("id", "form1_apt_errorloc");
                    $("div[id=form1_DOB_errorloc]").attr("id", "form1_DOB");
                    $("div[id=form1_ApplicantEmail_errorloc]").attr("id", "form1_ApplicantEmail");
                }
                else {

                    $('#FirstName').css("background-color", "transparent");
                    $('#LastName').css("background-color", "transparent");
                    $('#Social').css("background-color", "transparent");
                    // $('#Apt').css("background-color", "#FFFFAA");
                    $('#DOB').css("background-color", "transparent");
                    $("div[id=form1_LastName]").html("");
                    $("div[id=form1_FirstName]").html("");
                    $("div[id=form1_Social]").html("");
                    $("div[id=form1_DOB]").html("");
                    $("div[id=form1_ApplicantEmail]").html("");
                }
            }
        }, 50);
    }, 400);
    //    $("div[id=form1_Address]").attr("id", "form1_Address_errorloc");
    //    $("div[id=form1_Street]").attr("id", "form1_Street_errorloc");
    //    $("div[id=form1_City]").attr("id", "form1_City_errorloc");
    //    $("div[id=form1_apt]").attr("id", "form1_apt_errorloc");
    //    $("div[id=form1_State]").attr("id", "form1_State_errorloc");
    //    $("div[id=form1_Zip]").attr("id", "form1_Zip_errorloc");
    //    $(".tempval").val("");
    //    var t = setTimeout(function () {
    //        if (document.getElementById('ProductsSelected').value == "") {
    //            $('#Address').css("background-color", "transparent");
    //            $('#Street').css("background-color", "transparent");
    //            $('#City').css("background-color", "transparent");
    //            $('#Apt').css("background-color", "transparent");
    //            $('#State').css("background-color", "transparent");
    //            $('#Zip').css("background-color", "transparent");
    //            $("div[id=form1_Address_errorloc]").html("");
    //            $("div[id=form1_Street_errorloc]").html("");
    //            $("div[id=form1_City_errorloc]").html("");
    //            $("div[id=form1_apt_errorloc]").html("");
    //            $("div[id=form1_State_errorloc]").html("");
    //            $("div[id=form1_Zip_errorloc]").html("");
    //        }
    //        if (document.getElementById('ProductsSelected').value.indexOf("NCR") > 0) {

    //            if (document.getElementById('ProductsSelected').value.indexOf("PS") > 0) {
    //                var val_selected = document.getElementById('ProductsSelected').value.split('##');
    //                var check_selected = 0;
    //                for (var a = 0; a < val_selected.length; a++) {
    //                    if (val_selected[a].indexOf("NCR") >= 0 || val_selected[a].indexOf("PS") >= 0) { }
    //                    else { check_selected = 1; }
    //                }
    //                if (check_selected == "0") {
    //                    $('#Address').css("background-color", "transparent");
    //                    $('#Street').css("background-color", "transparent");
    //                    $('#City').css("background-color", "transparent");
    //                    $('#Apt').css("background-color", "transparent");
    //                    // $('#State').css("background-color", "transparent");
    //                    $('#Zip').css("background-color", "transparent");
    //                    $("div[id=form1_Address_errorloc]").html("");
    //                    $("div[id=form1_Street_errorloc]").html("");
    //                    $("div[id=form1_City_errorloc]").html("");
    //                    $("div[id=form1_apt_errorloc]").html("");
    //                    $("div[id=form1_State_errorloc]").html("");
    //                    $("div[id=form1_Zip_errorloc]").html("");
    //                    $(".tempval").val("2");
    //                }

    //            }
    //            if (document.getElementById('ProductsSelected').value.indexOf("PS") < 0) {
    //                $('#Address').css("background-color", "#FFFFAA");
    //                $('#Street').css("background-color", "#FFFFAA");
    //                $('#City').css("background-color", "#FFFFAA");
    //                $('#Apt').css("background-color", "#FFFFAA");
    //                $('#State').css("background-color", "#FFFFAA");
    //                $('#Zip').css("background-color", "#FFFFAA");
    //                $("div[id=form1_Address]").attr("id", "form1_Address_errorloc");
    //                $("div[id=form1_Street]").attr("id", "form1_Street_errorloc");
    //                $("div[id=form1_City]").attr("id", "form1_City_errorloc");
    //                $("div[id=form1_apt]").attr("id", "form1_apt_errorloc");
    //                $("div[id=form1_State]").attr("id", "form1_State_errorloc");
    //                $("div[id=form1_Zip]").attr("id", "form1_Zip_errorloc");
    //                $(".tempval").val("1");
    //            }
    //        }

    //        if (document.getElementById('ProductsSelected').value.indexOf("NCR") < 0) {
    //            $(".tempval").val("");
    //        }

    //    }, 500)



}


