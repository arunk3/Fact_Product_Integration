﻿

function BindGrid(ChangedCompany, Report) {

    var Loadflag = document.getElementById("FirstTimeloadflag");
    if (Loadflag != null) {
        document.getElementById("FirstTimeloadflag").value = 2;
    }

    var Keyword = $('#Keyword').val();
    // For Seprate SSN Search
    var Keyword2 = $('#Keyword2');
    if (Keyword2 != null && IssSSN == "true") {

        var SsnKeyword = Keyword2.val();
        var ssnlength = SsnKeyword.length;

        if (SsnKeyword != "" && ssnlength == 4) {
            Keyword = SsnKeyword;
        }
        else {
            alert("Please Enter Last 4 Digits of SSN.");
            return false;
        }


    }

    var pkOrderDateId = $('#pkOrderDateId').val();
    var pkProductApplicationId = $('#pkProductApplicationId').val();
    var pkCompanyId = $('#pkCompanyId').val();
    var LocationId = $('#LocationId').val();
    var CompanyUserId = $('#CompanyUserId').val();
    var TrackingReferenceCode = $('#pkReferenceCodeId').val();

    var Month = $('#Month').val();
    var Year = $('#Year').val();
    var txtSpecificDate = $('#txtSpecificDate').val();
    var txtSpecificFrom = $('#txtSpecificFrom').val();
    var txtSpecificTo = $('#txtSpecificTo').val();
    var folderflag = $('#kendodropdownlist');
    if (folderflag != null) {
        //alert($("#folderddl").val());
        Report = $("#folderddl").val().trim();
    }


    var trMonth = $("#trMonth");
    var trSpecificDate = $("#trSpecificDate");
    var trSpecificDateRange = $("#trSpecificDateRange");
    var trSearch = $("#trsearch");
    var divArchive = $("#divArchive");
    var divBack = $("#divBack");
    var hdnIncArchive = $('#hdnIncArchive');
    if (Report == "IncompleteReports") {
        $("#divArchive").css("display", "block");
        $("#divBack").css("display", "none");
        hdnIncArchive.val('');
    }
    else if (Report == "IncompleteArchive") {
        $("#divArchive").css("display", "none");
        $("#divBack").css("display", "block");
        hdnIncArchive.val('IncompleteArchive');
        $('input[name=chkIsIncomplete]').attr('checked', true);
    }
    else if (Report == "Back") {
        $("#divArchive").css("display", "none");
        $("#divBack").css("display", "none");
        hdnIncArchive.val('');
        Report = "AllReports";
        $('input[name=chkIsIncomplete]').attr('checked', false);
    }
    else {
        $("#divArchive").css("display", "none");
    }
    var IncArchive = $('#hdnIncArchive').val();
    if (IncArchive != '') {
        Report = "IncompleteArchive";
    }
    var ShowIncomplete = $('input[name=chkIsIncomplete]').is(':checked')


    // change by pk
    if (pkOrderDateId < 4) {
        trMonth.css("display", "none");
        trSpecificDate.css("display", "none");
        trSpecificDateRange.css("display", "none");
        //trSearch.css("display", "none");

    }

    else {
        // trSearch.css("display", "block");

        if (pkOrderDateId == 4) {
            trMonth.css("display", "block");
            trSpecificDate.css("display", "none");
            trSpecificDateRange.css("display", "none");
        }
        else if (pkOrderDateId == 5) {
            trSpecificDate.css("display", "block");
            trMonth.css("display", "none");
            trSpecificDateRange.css("display", "none");
        }
        else if (pkOrderDateId == 6) {
            trSpecificDateRange.css("display", "block");
            trSpecificDate.css("display", "none");
            trMonth.css("display", "none");
        }


    }
    FilterGrid(pkOrderDateId, pkProductApplicationId, pkCompanyId, Keyword, ChangedCompany, LocationId, CompanyUserId, Month, Year, txtSpecificDate, txtSpecificFrom, txtSpecificTo, Report, ShowIncomplete, TrackingReferenceCode);
    if (ChangedCompany == 1) { //company Changed then bind location dropdown and User dropdown

        $("#LocationId").css("display", "block");
        $.getJSON("../CommonSavedReport/GetCompanyLocationsByCompanyId", { CompanyId: pkCompanyId }, function (data) {
            var LocationId = $("#LocationId");
            LocationId.empty();

            $.each(data, function (index, coll) {
                LocationId.append(
                    $('<option/>')
                        .attr('value', coll.LocationId)
                        .text(coll.DisplayLocation)
                );
            });
        });

        var Role = $("#Role").val();
        if (Role == 5) {// For Basic Users
            $("#CompanyUserId").css("display", "none");
        }
        else {
            $("#CompanyUserId").css("display", "block");
            $.getJSON("../CommonSavedReport/GetUserAsPerLocation", { CompanyId: pkCompanyId, pkLocationId: LocationId }, function (data) {
                var CompanyUserId = $('#CompanyUserId');
                CompanyUserId.empty();

                $.each(data, function (index, coll) {
                    CompanyUserId.append(
                        $('<option/>')
                            .attr('value', coll.CompanyUserId)
                            .text(coll.UserEmail)
                    );
                });
            });

        }


    }
    if (ChangedCompany == 2) {

        var Role = $("#Role").val();
        if (Role == 5) {// For Basic Users
            $("#CompanyUserId").css("display", "none");
        }
        else {

            $("#CompanyUserId").css("display", "block");
            $.getJSON("../CommonSavedReport/GetUserAsPerLocation", { CompanyId: pkCompanyId, pkLocationId: LocationId }, function (data) {
                var CompanyUserId = $('#CompanyUserId');
                CompanyUserId.empty();

                $.each(data, function (index, coll) {
                    CompanyUserId.append(
                        $('<option/>')
                            .attr('value', coll.CompanyUserId)
                            .text(coll.UserEmail)
                    );
                });
            });

        }
    }
}





function FilterGrid(pkOrderDateId, pkProductApplicationId, pkCompanyId, Keyword, ChangedCompany, LocationId, CompanyUserId,
            Month, Year, txtSpecificDate, txtSpecificFrom, txtSpecificTo, Report, ShowIncomplete, TrackingReferenceCode) {

    var grid = $("#kendoGrid").data("kendoGrid");
    grid.dataSource.filter([
                                    { field: "pkOrderDateId", operator: "", value: pkOrderDateId },
                                    { field: "pkProductApplicationId", operator: "", value: pkProductApplicationId },
                                    { field: "CompanyUserId", operator: "", value: CompanyUserId },
                                    { field: "pkCompanyId", operator: "", value: pkCompanyId },
                                    { field: "LocationId", operator: "", value: LocationId },
                                    { field: "Keyword", operator: "", value: Keyword },
                                    { field: "Month", operator: "", value: Month },
                                    { field: "Year", operator: "", value: Year },
                                    { field: "txtSpecificDate", operator: "", value: txtSpecificDate },
                                    { field: "txtSpecificFrom", operator: "", value: txtSpecificFrom },
                                    { field: "txtSpecificTo", operator: "", value: txtSpecificTo },
                                    { field: "Report", operator: "", value: Report },
                                    { field: "ChangedCompany", operator: "", value: ChangedCompany },
                                    { field: "ShowIncomplete", operator: "", value: ShowIncomplete },
                                    { field: "TrackingReferenceCode", operator: "", value: TrackingReferenceCode }
    ]
                            );
}


function EmptyGrid() {

    var Keyword = $('#Keyword');
    var pkOrderDateId = $('#pkOrderDateId');
    var pkProductApplicationId = $('#pkProductApplicationId');
    var LocationId = $('#LocationId');
    var pkCompanyId = $('#pkCompanyId');
    var CompanyUserId = $('#CompanyUserId');
    var TrackingReferenceCode = $('#pkReferenceCodeId');
    var folders = $("#folderddl");


    //    var pkOrderDateId = $("#pkOrderDateId").data("kendoDropDownList");
    //    var pkProductApplicationId = $("#pkProductApplicationId").data("kendoDropDownList");
    //    var pkCompanyId = $("#pkCompanyId").data("kendoDropDownList");
    //    var LocationId = $("#LocationId").data("kendoDropDownList");
    //    var CompanyUserId = $("#CompanyUserId").data("kendoDropDownList");

    //var trMonth = $("#trMonth");
    //var trSpecificDate = $("#trSpecificDate");
    //var trSpecificDateRange = $("#trSpecificDateRange");
    //var trSearch = $("#trSearch");
    //var divArchive = document.getElementById("divArchive");
    //var divBack = document.getElementById("divBack");

    //trMonth.style.display = 'none';
    //trSearch.style.display = 'none';
    //trSpecificDate.style.display = 'none';
    //trSpecificDateRange.style.display = 'none';
    //divArchive.style.display = 'none';
    //divBack.style.display = 'none';

    $("#trMonth,#trSpecificDate,#trSpecificDateRange,#trSearch,#divArchive,#divBack").hide();



    var hdnIncArchive = $('#hdnIncArchive');
    hdnIncArchive.val('');

    Keyword.val('');

    $('input[name=chkIsIncomplete]').attr('checked', false);
    pkOrderDateId.val('0');
    pkProductApplicationId.val(0);
    TrackingReferenceCode.val('-1');
    folders.val(-1)
    var Role = $("#Role").val();
    if (Role == 1) { //Admin


        //        pkCompanyId.value(0);
        //        LocationId.value(0);
        //        CompanyUserId.value(0);

        pkCompanyId.val(0);
        LocationId.val(0);
        CompanyUserId.val(0);

        $.getJSON("../CommonSavedReport/GetCompanyLocationsByCompanyId", { CompanyId: 0 }, function (data) {
            var LocationId = $('#LocationId');
            LocationId.empty();

            $.each(data, function (index, coll) {
                LocationId.append(
                    $('<option/>')
                        .attr('value', coll.LocationId)
                        .text(coll.DisplayLocation)
                );
            });

        });

        $("#LocationId").css("display", "block");
        //        $("#LocationId").kendoDropDownList({

        //            dataTextField: "DisplayLocation",
        //            dataValueField: "LocationId",

        //            dataSource: {


        //                transport: {
        //                    read: {
        //                        url: '../CommonSavedReport/GetCompanyLocationsByCompanyId',
        //                        data: { CompanyId: 0 },
        //                        type: "POST"
        //                    }
        //                }
        //            }

        //        });
        $("#CompanyUserId").css("display", "block");
        $.getJSON("../CommonSavedReport/GetUserAsPerLocation", { CompanyId: 0, pkLocationId: 0 }, function (data) {
            var CompanyUserId = $('#CompanyUserId');
            CompanyUserId.empty();

            $.each(data, function (index, coll) {
                CompanyUserId.append(
                    $('<option/>')
                        .attr('value', coll.CompanyUserId)
                        .text(coll.UserEmail)
                );
            });
        });


    }
    else if (Role == 2) { //Corporate
        LocationId.val(0);
        CompanyUserId.val(0);
    }
    else if (Role == 3) { //Branch Manager       
        CompanyUserId.val(0);
    }

    var grid = $("#kendoGrid").data("kendoGrid");

    grid.dataSource.filter([{ field: "pkOrderDateId", operator: "", value: -5}]);
}

function Button_Click(e) {
    var evt = e ? e : window.event;
    if (evt.keyCode == 13) {
        BindGrid(0, 'AllReports');
        return false;
    }
}
var IssSSN = "false";
function GetControlId(ID) {
    // alert(ID);
    if (ID == "Keyword2" || ID == "SSnsearch") {

        IssSSN = "true";
        $("#Keyword").val("");
    }
    else {
        IssSSN = "false";
        $("#Keyword2").val("");

    }


}

var IsAdmin = $("#Role").val();
if (IsAdmin == "1") {

    $("#kendoGrid").kendoGrid({
        dataSource: dataSource,
        height: 540,
        sortable: true,
        pageable: true,
        resizable: true,
        columns: [
                            {field: "OrderId", sortable: false, width: 25, title: "<input type='checkbox' id='chkLead_head' class='chk-head-style' onclick='CheckedAll();' />", template: "<div class='centeralign'><input type='checkbox' id='chkLead_#=OrderId#_#=OrderNo#'  class='chk-style' /></div>" },
                            { field: "ApplicantName", title: "Applicant Name", width: 125 },
                            { field: "ApplicantDOB+OrderType+IsSubmitted+OrderStatus", title: "DOB", width: 78, template: "#if(ApplicantDOB!=null){# #=ApplicantDOB#  #}#" },
                            { field: "OrderNo", title: "Order No", width: 123, template: "# if(IsDrugTestingOrder){#<a href='ViewDrugTestingReports?_OId=#=OrderId#' class='clsorder'>#=OrderNo#</a> #}else if(IsEscreenOrder){#<a href='ViewEscreenReport?_OId=#=OrderId#' class='clsorder'>#=OrderNo#</a> #}else if(InReviewed){#<a href='ViewReportReview?_OId=#=OrderId#' class='clsorder'>#=OrderNo#</a> #}else{#<a href='ViewReports?_OId=#=OrderId#' class='clsorder'>#=OrderNo#</a> #}#" },
                            { field: "TrackingReferenceCode", title: "Reference", width: 65 },
                            { field: "ReportsWithStatus", title: "Data Sources", width: 80, template: "#if(ReportsWithStatus!=null){# #=ReportsWithStatus#  #}#" },
                            { field: "CompanyName", title: "Company Name", width: 90, template: "<a href='AddCompanies?Id1=#=fkLocationId#' target='_Blank'class='clsorder'>#=CompanyName#</a>" },
                            { field: "Location", title: "Location", width: 85, template: "<a href='AddCompanyLocation?lid=#=fkLocationId#' target='_Blank'class='clsorder'>#=Location#</a>" },
                            { field: "UserEmail", title: "User", width: 120, template: "<a href='AddCompanyUser?_a=#=pkCompanyUserId#&_c=#=pkCompanyId#' target='_Blank'class='clsorder'>#=UserEmail#</a>" },
                            { field: "OrderDt", title: "Order Date", width: 78, format: "{0:MM/dd/yyyy}" },
                            { field: "OrderNo", title: "Log", width: 35, sortable: false, template: "# if(!IsDrugTestingOrder){#<img  style='display:none;' id='img_#=OrderId#'  src='../Content/themes/base/images/smallloader.gif' > <input  onclick='OpenNewReportLog(this.id);' type='IMAGE'  id='#=OrderId#'  SRC='../Content/themes/base/images/ErrorlogSmall.png'  /> #}#" },
                            { field: "HitStatus", title: "Hit", width: 30, template: "# if(HitStatus==1){#<img  src='../Content/themes/base/images/miss_red_dot.png' style='padding-left: 0px;'/> #}else if(HitStatus==2){#<img  src='../Content/themes/base/images/hit_green_dot.png' style='padding-left: 0px;'/> #} else{}#" },
                            { field: "OrderStatusDisplay", title: "Status", width: 100, template: "# if(OrderStatusDisplay=='Pending') { # <span class='Pending'>Pending</span>#} else if(OrderStatusDisplay=='Completed') { # <span class='Completed'>Completed</span>#} else if(OrderStatusDisplay=='Incomplete') { # <span class='Incomplete'>Incomplete</span>#}else if(OrderStatusDisplay=='InReview') { # <span class='InReview'>InReview</span>#} else if(OrderStatusDisplay=='In Review_1') { # <span class='Review_1'>In Review</span>#} else if(OrderStatusDisplay=='In Review_0') { # <span class='InReview'>In Review</span>#} else{# <span class='#OrderStatusDisplay#'>#OrderStatusDisplay#</span>#  }#" },
                            { field: "ReportStatus", title: "Pass / Fail ", width: 60, template: "# if(ReportStatus=='Pass') { # <span class='Completed' style='padding:2px 14px;'>Pass</span>#} else if(ReportStatus=='Fail') { # <span class='InReview' style='padding:2px 14px;'>Fail</span>#} else {# <span ></span>#}#" },
        ],
        dataBound: onDataBound
    }).find("th").css("text-align", "left");
}
else {
    $("#kendoGrid").kendoGrid({
        dataSource: dataSource,
        height: 540,
        sortable: true,
        pageable: true,
        resizable: true,
        columns: [
                        { field: "OrderId", sortable: false, width: 25, title: "<input type='checkbox' id='chkLead_head' class='chk-head-style' onclick='CheckedAll();' />", template: "<input type='checkbox' id='chkLead_#=OrderId#'  class='chk-style' />" },
                        { field: "ApplicantName", title: "Applicant Name", width: 145 },
                        { field: "ApplicantDOB+OrderType+IsSubmitted+OrderStatus", title: "DOB", width: 80, template: "#if(ApplicantDOB!=null){# #=ApplicantDOB#  #}#" },
                        { field: "OrderNo", title: "Order No", width: 150, template: "# if(IsDrugTestingOrder){#<a href='ViewDrugTestingReports?_OId=#=OrderId#' class='clsorder'>#=OrderNo#</a> #}else if(IsEscreenOrder){#<a href='ViewEscreenReport?_OId=#=OrderId#' class='clsorder'>#=OrderNo#</a> #}else{if(OrderStatusDisplay=='In Review') {#<a href='ViewReportReview?_OId=#=OrderId#' class='clsorder'>#=OrderNo#</a> #}else{if(!InReviewed) {#<a href='ViewReports?_OId=#=OrderId#' class='clsorder'>#=OrderNo#</a> #}else{#<a href='ViewReportReview?_OId=#=OrderId#' class='clsorder'>#=OrderNo#</a> #}}}#" },
                        { field: "TrackingReferenceCode", title: "Reference", width: 70 },
                        { field: "ReportIncludes", title: "Data Sources", width: 95, template: "#if(ReportsWithStatus!=null){# #=ReportsWithStatus#  #}#" },
                        { field: "Location", title: "Location", width: 95 },
                        { field: "UserEmail", title: "User", width: 150 },
                        { field: "OrderDt", title: "Order Date", width: 80, format: "{0:MM/dd/yyyy}" },
                        { field: "HitStatus", title: "Hit", width: 36, template: "# if(HitStatus==1){#<img  src='../Content/themes/base/images/miss_red_dot.png' style='padding-left: 0px;' /> #}else if(HitStatus==2){#<img  src='../Content/themes/base/images/hit_green_dot.png' style='padding-left: 0px;' /> #} else{}#" },
                        { field: "OrderStatusDisplay", title: "Status", width: 100, template: "# if(OrderStatusDisplay=='Pending') { # <span class='Pending'>Pending</span>#} else if(OrderStatusDisplay=='Completed') { # <span class='Completed'>Completed</span>#} else if(OrderStatusDisplay=='Incomplete') { # <span class='Incomplete'>Incomplete</span>#}else if(OrderStatusDisplay=='InReview') { # <span class='InReview'>InReview</span>#} else if(OrderStatusDisplay=='In Review_1') { # <span class='Review_1'>In Review</span>#} else if(OrderStatusDisplay=='In Review_0') { # <span class='InReview'>In Review</span>#} else{# <span class='#OrderStatusDisplay#'>#OrderStatusDisplay#</span>#  }#" },
                        { field: "ReportStatus", title: "Pass / Fail", width: 78, template: "# if(ReportStatus=='Pass') { # <span class='Completed' style='padding:2px 22px;'>Pass</span>#} else if(ReportStatus=='Fail') { # <span class='InReview' style='padding:2px 22px;'>Fail</span>#} else {# <span ></span>#}#" },
        ],
        dataBound: onDataBound


    }).find("th").css("text-align", "left");
}

function onDataBound(arg) {
    if ($('#IsReportStatus').val() == "False") {
        // $('#kendoGrid').find('.k-grid-header').find('[data-field="ReportStatus"]').remove();
        //$('#kendoGrid').find('.k-grid-header').last('<th/>')
        //$('#kendoGrid').find('.k-grid-header').find('[data-field="OrderStatusDisplay"]').attr('colspan', 1);
        $('#kendoGrid').find('.k-grid-content').find('tr').each(function (index, value) {
            if (IsAdmin == "1") {
                //$(this).find('td:eq(13)').remove();
                //$(this).last('<td/>');
                //$(this).find('td:eq(13)').attr('colspan', 2);
                //$(this).find('td:eq(13)').css("text-align", "center");
            }
            else {
                //$(this).find('td:eq(12)').remove();
                //$(this).last('<td/>');
                //$(this).find('td:eq(11)').attr('colspan', 2);
                //$(this).find('td:eq(11)').css("text-align", "center");
            }
        });
    }
    else {
        $('#kendoGrid').find('.k-grid-header').find('[data-field="ReportStatus"]').css("visibility", "visible");
        $('#kendoGrid').find('.k-grid-content').find('tr').each(function (index, value) {
            if (IsAdmin == "1") {
                $(this).find('td:eq(13)').css("visibility", "visible");
            }
            else {
                $(this).find('td:eq(12)').css("visibility", "visible");

            }
        });
    }

    var OrderNo = '';
    cellno = 12;
    var takeTotal = this.dataSource.total();
    if (takeTotal != 0) {
        //document.getElementById("ExportSelectedOrderToCSV").style.display = 'block';
    }
    else {
        // document.getElementById("ExportSelectedOrderToCSV").style.display = 'none';
    }
    $("#lblTotalRecords").html(takeTotal);
    var gridHeaderRows = $("#kendoGrid tbody th");
    var gridRows = $("#kendoGrid tbody tr");
    var takeGrid = $("#kendoGrid").data("kendoGrid");


    for (i = 0; i < gridRows.length; i++) {
        
        var dataItem = takeGrid.dataItem(gridRows[i]);
        var row = gridRows[i];
        var Status = dataItem.OrderStatusDisplay;

        var IsSyncpod = dataItem.IsSyncpod;
        var OrderType = dataItem.OrderType;
        var IsSubmitted = dataItem.IsSubmitted;
        var FirstName = dataItem.FirstName;
        var OrderId = dataItem.OrderId;
        var OrderStatus = dataItem.OrderStatus;
        var RStatus = dataItem.ReportStatus


        var image = '&nbsp;&nbsp;<img src="../Content/themes/base/images/Order_Form_Icon.png" height="18px;" alt="" id="' + OrderId + '" class="downloadbtn" />  ';
        if (FirstName != '') {
            FirstName = FirstName.charAt(0).toUpperCase() + FirstName.substring(1, FirstName.length).toLowerCase();
        }
        var LastName = dataItem.LastName;
        if (LastName != '') {
            LastName = LastName.charAt(0).toUpperCase() + LastName.substring(1, LastName.length).toLowerCase();
        }

        if (LastName != '' && FirstName != '') {
            row.cells[1].innerHTML = LastName + ', ' + FirstName;
        }
        else {
            row.cells[1].innerHTML = LastName + FirstName;
        }
        if (IsSyncpod == true) {

            OrderNo = row.cells[3].innerHTML;
            OrderNo = OrderNo + '<img src="../Content/themes/base/images/syncpod_order_icon.png"/>';
            row.cells[3].innerHTML = OrderNo;
        }
        else if (OrderType == 2 || OrderType == 3) {
            OrderNo = row.cells[3].innerHTML;
            OrderNo = OrderNo + '<img src="../Content/themes/base/images/APIcircle.png" title="API Order" alt="API"/>';
            row.cells[3].innerHTML = OrderNo + image;
            //row.cells[cellno].innerHTML = status;
        }

        if ($.browser.webkit) {
            if (IsAdmin != "1") {
                cellno = 11;
                row.cells[cellno].style.color = 'white';
            }
        }
        else {
            try {
                row.cells[cellno].style.color = 'white';
            } catch (e) {
                cellno = 11;
                row.cells[cellno].style.color = 'white';

            }
        }
        row.cells[cellno].style.color = 'white';
        if (IsSubmitted == false && OrderType == 2 && OrderStatus != 2) {
            row.cells[cellno].innerHTML = "<input type='button' id='" + OrderId + "' value='Submit' class='sbmtbtn'  /><input type='button' id='" + OrderId + "' value='Form' class='downloadbtn'  />";

        }
        else {
            if (Status == 'In Review_0' || Status == 'In Review_1') {
                // add code for split in review check 
                var splitword_0 = Status.split('_');
                var Status = splitword_0[0];
                var rowvalue = row.cells[cellno].innerHTML.split('_')
                row.cells[cellno].innerHTML = rowvalue[0]

            }
            if (Status == 'In Review' || Status == 'Pending' || Status == 'Incomplete') {

                if (Status == 'In Review' || Status == 'Pending') {
                    row.cells[0].style.ffffdd = '#ddffaa';
                    if (Status == 'In Review') {
                        if (IsSubmitted == true) {
                            if (splitword_0[1] == '1') {
                                // add different color 
                                // add purple color 
                                row.cells[cellno].innerHTML = '<div style="width:100%;"><div style="float:left;width:86%;"><span style="background-color: purple; color: white;padding: 1px 4px;border-radius: 3px;" >' + splitword_0[0] + '</span></div>'
                            }
                            else {
                                row.cells[cellno].innerHTML = '<div style="width:100%;"><div style="float:left;width:86%;"><span >' + row.cells[cellno].innerHTML + '</span></div>'
                            }
                        }
                        else {
                            if (splitword_0[1] == '1') {

                                row.cells[cellno].innerHTML = '<span style="background-color: purple; color: white;padding: 1px 4px;border-radius: 3px;">' + splitword_0[0] + '</span>'
                            }
                            else {

                                row.cells[cellno].innerHTML = '<span style="background-color: Red; color: white;padding: 1px 4px;border-radius: 3px;">' + splitword_0[0] + '</span>'
                            }
                        }
                    }
                    else {

                        if (IsSubmitted == true) {

                            row.cells[cellno].innerHTML = '<div style="width:100%;"><div style="float:left;width:86%;"><span >' + row.cells[cellno].innerHTML + '</span></div>'
                        }

                        else {

                            row.cells[cellno].innerHTML = '<span  >' + row.cells[cellno].innerHTML + '</span>'
                        }
                    }
                }
                else {
                    if (IsSubmitted == true) {

                        row.cells[cellno].innerHTML = '<div style="width:100%;"><div style="float:left;width:86%;"><span >' + row.cells[cellno].innerHTML + '</span></div>'
                    }
                    else {

                        row.cells[cellno].innerHTML = '<span>' + row.cells[cellno].innerHTML + '</span>'
                    }
                }
            }
            else {
                if (IsSubmitted == true) {
                    row.cells[cellno].innerHTML = '<div style="width:100%;"><div style="float:left;width:86%;"><span >' + row.cells[cellno].innerHTML + '</span></div>'
                }
                else {
                    if (Status == 'Complete *') {
                        row.cells[cellno].innerHTML = '<span style="background-color: #6CC525; color: white;padding: 1px 4px;border-radius: 3px;">' + Status + '</span>'
                    }
                    else {
                        row.cells[cellno].innerHTML = '<span >' + row.cells[cellno].innerHTML + '</span>'
                    }



                }
            }
        }
        GridEffect("kendoGrid", dataItem)
    }

    $("#recordsShow").text(this.dataSource.total());
    $(".sbmtbtn,.downloadbtn").css({ "width": "60px", "color": "white" }).addClass("btn-primary");
    $("img.downloadbtn").css({ "width": "18px", "margin-left": "-5px" });
    $(".k-header").css("padding-left", "0");
    $(".InReview").css("padding", "2px 7px;");
    $(".Completed").css("padding", "-1px");
    $(".Pending ").css("padding", "2px 11px");
    $("span .Completed").css("padding", "-1px 22px");
}
$("#kendoGrid").delegate(".sbmtbtn", "click", function (e) {

    OpenNewReportLogApi(this.id);
});
function ChangePageSize(val) {
    var ds = $("#kendoGrid").data("kendoGrid").dataSource;
    var newPageSize = parseInt(val);
    ds.pageSize(newPageSize);
}
function OpenNewReportLogApi(OrderId) {
    window.open('../Corporate/NewReportLogApi?OrderId=' + OrderId + '', 'OrderDetails', 'height=600,width=900,toolbar=no,scrollbars=yes,location=no,resizable=yes');
}
function OpenNewReportLog(OrderId) {
    window.open('../Corporate/NewReportLog?OrderId=' + OrderId + '', 'OrderDetails', 'height=600,width=900,toolbar=no,scrollbars=yes,location=no,resizable=yes');
}

function HideDiv() {
    $("#divSavedReportLog").css("display", "none");
}

selectAvatar();


function selectAvatar() {


    $(".k-link").css("font-size", "14px");
    $(".search-block-2").hide();
    $("#advFilters").click(function () {

        var text = $("#advFilters").text();

        if (text.trim().toLowerCase() == "show advanced filters") {
            $("#advFilters").text("Hide Advanced Filters");
        }
        else {
            $("#advFilters").text("Show Advanced Filters");

        }
        $(".search-block-2").toggle();

    });


}

