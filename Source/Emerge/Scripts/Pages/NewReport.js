﻿/// <reference path="../kendo.all.min.js" />

var loc = window.location.href.toLowerCase();
var IPath = '';
if (loc.indexOf('newreport') > 0) {
    IPath = '../Content/themes/base/images/';
}
else if (loc.indexOf('addadditinalreport') > 0) {
    IPath = '../Content/themes/base/images/';
}
else {
    IPath = 'Content/themes/base/images/';

}

function AddToJurisdiction() {

    var wfou = "";
    var list = '';
    var countval = 10;
    var JselObj = document.getElementById('Jurisdiction');
    var items = $("#numberofdropdown").val();
    var items_1 = $("#binddrop").val();
    if (items != undefined) {
        if (items != "") { countval = items; }
        else { countval = 10; }
    }
    else { countval = 10; }
    if (items_1 != undefined) {
        if (items_1 != "") {
            if (items_1 == 1) {
                if (items < 10) {
                    countval = 10;

                }
            }
        }
    }
    if (Number(count) < countval) {
        for (var i = 0; i < JselObj.length; i++) {
            list += '<option value="' + JselObj.options[i].value + '">' + JselObj.options[i].text + '</option>'
        }
        var JurisdictionId = 'J_' + Ecount;
        var ProductsSelected = document.getElementById('ProductsSelected').value;
        var style = '';
        var num = 0;
        if (ProductsSelected.indexOf('#FCR_') >= 0) {
            style = "width:95%;background-color:#ffffaa"; //If these reports are selected then chaned textbox color
            num++;
        }
        else {
            style = "height:27px;width:138.25px!important;margin-top:5px;";
        }

        var newItem1 = '<select onchange="ConsentChange_fnc(this.id)" id=' + JurisdictionId + ' name="Jurisdiction" style="' + style + '" class="ddlField formcontrol-newpage-ddl hgt40">' + list + '</select>';
        var newItem2 = '<img src="' + IPath + 'delrow.png" alt="Delete" title="Delete" class="removedel" style="cursor:pointer"/>';

        var gridItems = $("#tblJurisdictionList");
        gridItems.append("<tr ><td>" + newItem1 + "</td><td>" + newItem2 + "</td><td></td><td></td><td></td><td></td></tr>");
        count = Number(count) + 1; Ecount = Number(Ecount) + 1;

        if (num != 0) {
            AddDDLRule(JurisdictionId);
        }
        if ($("#dropdowncouty").val() != undefined) {
            if ($("#dropdowncouty").val() != 3) {

                var JurisdictionId = "#" + JurisdictionId;
                $(JurisdictionId).kendoDropDownList({});
                var t = setTimeout(function () {
                    $("span.k-widget.k-dropdown.k-header.ddlField").each(function (e) {
                        var gg = $(this).find("select");
                        var color = gg.attr('style');
                        if (color.indexOf("rgb(255, 255, 170)") > 0) {
                            var span = $(this).find("span[class=k-input]");
                            span.attr("style", "background-color:rgb(255, 255, 170)");
                        }
                        if (color.indexOf("transparent") > 0) {
                            var span = $(this).find("span[class=k-input]");
                            span.attr("style", "background-color:transparent");

                        }
                    });
                }, 3000);
            }
        }
    }
}

function AddToCounty() {
    var wfou = "";
    var list = '';
    //var countval = 3;
    var countval = 10; //Ticket #55 replace all countval 3 of 10
    var CountyInfoState = document.getElementById("CountyInfoState");
    var items = $("#numberofdropdown").val();
    var items_1 = $("#binddrop").val();
    if (items != undefined) {
        if (items != "") { countval = items; }
        else { countval = 10; }
    }
    else { countval = 10; }
    if (items_1 != undefined) {
        if (items_1 != "") {
            if (items_1 == 1) {
                if (items < 10) {
                    countval = 10;

                }
            }
        }

    }
    if (Number(count) < countval) {
        for (var i = 0; i < CountyInfoState.length; i++) {
            list += '<option value="' + CountyInfoState.options[i].value + '">' + CountyInfoState.options[i].text + '</option>'
        }
        var StateId = 's_' + Ecount;
        var CountyId = 'c_' + Ecount;
        var lblIsRcxCountyMsg = CountyId + 'lblIsRcxCountyMsg';


        var ProductsSelected = document.getElementById('ProductsSelected').value;
        var style = '';
        var num = 0;
        if (ProductsSelected.indexOf('CCR1_') >= 0 || ProductsSelected.indexOf('CCR2_') >= 0 || ProductsSelected.indexOf('RCX_') >= 0) {
            style = "width:79%;background-color:#ffffaa"; //If these reports are selected then chaned textbox color
            num++;
        }
        else {
            style = "height:27px;width:138.25px!important;margin-top:5px;";
        }

        var newItem1 = '<select onchange="BindCountyWithJson(this.id)" id=' + StateId + ' name="CountyInfoState" style="' + style + '" class="formcontrol-newpage-ddl valid">' + list + '</select>';
        var newItem2 = '<select onchange="ConsentChange(this);"  id=' + CountyId + ' name="CountyInfoCounty" style="' + style + '" class="formcontrol-newpage-ddl valid"></select>';
        var newItem3 = '<img src="' + IPath + 'delrow.png" alt="Delete" title="Delete" class="removedel" style="cursor:pointer;margin-left:-30px;"/>';
        var newItem4 = '<label id=' + lblIsRcxCountyMsg + '></label>';
        var gridItems = $("#tblStateList");
        gridItems.append("<tr ><td>" + newItem1 + "</td><td>" + newItem2 + "</td><td>" + newItem3 + newItem4 + "</td><td></td><td></td><td></td></tr>");
        count = Number(count) + 1; Ecount = Number(Ecount) + 1;

        if (num != 0) {
            AddDDLRule(StateId); AddDDLRule(CountyId);
        }
        if ($("#dropdowncouty").val() != undefined) {
            if ($("#dropdowncouty").val() != 3) {

                var Stateid = "#" + StateId;
                var Countyid = "#" + CountyId;
                $(Stateid).kendoDropDownList({});
                $(Countyid).kendoDropDownList({});
                var t = setTimeout(function () {
                    $("span.k-widget.k-dropdown.k-header.ddlField").each(function (e) {
                        var gg = $(this).find("select");
                        var color = gg.attr('style');
                        if (color.indexOf("rgb(255, 255, 170)") > 0) {
                            var span = $(this).find("span[class=k-input]");
                            span.attr("style", "background-color:rgb(255, 255, 170)");
                        }
                        if (color.indexOf("transparent") > 0) {
                            var span = $(this).find("span[class=k-input]");
                            span.attr("style", "background-color:transparent");

                        }
                    });
                }, 3000);
            }
        }
    }
}


function AddToEmployment() {
    var list = '';
    var CountyState = document.getElementById("CompanyState");
    for (var i = 0; i < CountyState.length; i++) {
        list += '<option value="' + CountyState.options[i].value + '">' + CountyState.options[i].text + '</option>'
    }
    var StateId = 'Emp_' + Ecount;
    var CompanyNameId = 'CompanyName_' + Ecount;
    var CompanyCityId = 'CompanyCity_' + Ecount;
    var CompanyPhoneId = 'CompanyPhone_' + Ecount;

    var ProductsSelected = document.getElementById('ProductsSelected').value;
    var style = ''; var num = 0;
    if (ProductsSelected.indexOf('EMV_') >= 0) {//We used EMV_ becuase we don't want to do validations on EMV2
        style = "width:95%;background-color:#ffffaa !important";
        num++;
    }
    else {
        style = "width:95%;margin-top:5px;";
    }
    var CompanyName = '<input type="text" class="formcontrol-newpage" id=' + CompanyNameId + ' name="CompanyName" style="' + style + '"/>';
    var CompanyState = '<select  id=' + StateId + ' class="formcontrol-newpage" name="CompanyState" style="' + style + ';height:27px;width:138.25px !important;" >' + list + '</select>';
    var CompanyCity = '<input class="formcontrol-newpage" type="text" id=' + CompanyCityId + '  name="CompanyCity" style="' + style + '" onKeyPress = "return NotAllowNumbers(event);"/>';
    var CompanyPhone = '<input class="formcontrol-newpage" type="text" id=' + CompanyPhoneId + '  name="CompanyPhone" style="' + style + '" maxlength = "14" onkeydown = "javascript:backspacerDOWN(this,event);" onkeyup = "javascript:backspacerUP(this,event);"/>';
    var AliasName = '<input class="formcontrol-newpage" type="text" name="AliasName" style="width:95%; background-color:#ffffc1;" onKeyPress = "return NotAllowNumbers(event);"/>';
    var deleteitem = '<img src="' + IPath + 'delrow.png" class="empdel" alt="Delete" title="Delete" style="cursor:pointer"/>'
    var gridItems = $("#tblEmplyomentInfo");
    gridItems.append("<tr><td>" + CompanyName + "</td><td>" + CompanyState + "</td><td>" + CompanyCity + "</td><td>" + CompanyPhone + "</td><td>" + AliasName + "</td><td>" + deleteitem + "</td></tr>");
    Ecount = Number(Ecount) + 1;
    if (num != 0) {
        AddRule(CompanyNameId); AddDDLRule(StateId); AddRule(CompanyCityId); AddRule(CompanyPhoneId);
    }
    // $("#" + StateId).kendoDropDownList({});
}

function AddToEducation() {
    var list = '';
    var InstituteState = document.getElementById("InstituteState");
    for (var i = 0; i < InstituteState.length; i++) {
        list += '<option value="' + InstituteState.options[i].value + '">' + InstituteState.options[i].text + '</option>'
    }
    var ProductsSelected = document.getElementById('ProductsSelected').value;
    var style = ''; var num = 0;
    if (ProductsSelected.indexOf('EDV_') >= 0) {
        style = "width:95%;background-color:#ffffaa"; num++;
    }
    else {
        style = "width:95%;margin-top:5px;";
    }
    var StateId = 'Edu_' + Ecount;
    var SchoolNameId = 'SchoolName_' + Ecount;
    var SchoolCityId = 'SchoolCity_' + Ecount;
    var SchoolName = '<input type="text" class="formcontrol-newpage" name="SchoolName" id=' + SchoolNameId + '  style="' + style + '"/>';
    var InstituteState = '<select  id=' + StateId + ' class="formcontrol-newpage" name="InstituteState"  style="' + style + ';height:27px;width:139px !important;" >' + list + '</select>';
    var SchoolCity = '<input type="text" class="formcontrol-newpage" id=' + SchoolCityId + ' name="SchoolCity"  style="' + style + '" onKeyPress = "return NotAllowNumbers(event);"/>';
    var SchoolPhone = '<input type="text" class="formcontrol-newpage" name="SchoolPhone" style="width:95%" maxlength = "14" onkeydown = "javascript:backspacerDOWN(this,event);" onkeyup = "javascript:backspacerUP(this,event);"/>';
    var GraduationDt = '<input class="formcontrol-newpage valid" id="SearchedGraduationDt" maxlength="10" name="SearchedGraduationDt" type="text" placeholder="MM/dd/yyyy">';
    var InstituteAlias = '<input type="text" class="formcontrol-newpage" name="InstituteAlias" style="width:95%;  onKeyPress = "return NotAllowNumbers(event);"/>';
    var deleteitem = '<img src="' + IPath + 'delrow.png" class="empdel" alt="Delete" title="Delete" style="cursor:pointer"/>'
    var gridItems = $("#tblEducationInfo");
    gridItems.append("<tr><td>" + SchoolName + "</td><td>" + InstituteState + "</td><td>" + SchoolCity + "</td><td>" + SchoolPhone + "</td><td>" + GraduationDt + "</td><td>" + InstituteAlias + "</td><td>" + deleteitem + "</td>AddToEducation()</tr>");
    Ecount = Number(Ecount) + 1;
    if (num != 0) {
        AddRule(SchoolNameId); AddRule(SchoolCityId); AddDDLRule(StateId);
    }

    $('#tblApplicant5').find('#SearchedGraduationDt').mask('99/99/9999');

    // $("#" + StateId).kendoDropDownList({});
}



function AddToLicenseInfo() {
    var list = '';
    var LicenseState = document.getElementById("LicenseState");
    for (var i = 0; i < LicenseState.length; i++) {
        list += '<option value="' + LicenseState.options[i].value + '">' + LicenseState.options[i].text + '</option>'
    }
    var StateId = 'Edu_' + Ecount;
    var LicenseId = 'License_' + Ecount;
    var LicenseTypeId = 'LicenseType_' + Ecount;

    var style = ''; var num = 0;
    var ProductsSelected = document.getElementById('ProductsSelected').value;
    if (ProductsSelected.indexOf('PLV_') >= 0) {
        style = "width:95%;background-color:#ffffaa"; num++;
    }
    else {
        style = "width:95%;margin-top:5px;";
    }

    var LicenseType = '<input class="formcontrol-newpage" type="text" id=' + LicenseTypeId + ' name="LicenseType" style="' + style + '"/>';
    var LicenseState = '<select  id=' + StateId + ' name="LicenseState" style="' + style + ';height:27px;width:139px !important;" class="formcontrol-newpage">' + list + '</select>';
    var LicenseNumber = '<input class="formcontrol-newpage" type="text" name="LicenseNumber" style="width:95%"/>';
    var LicenseIssueDate = '<input class="formcontrol-newpage" id=' + LicenseId + ' type="text" name="LicenseIssueDate" style="width:95%" maxlength = "10" onkeypress = "return DOBcheck(event);" onkeydown = "javascript: MaskDOB(this,event);" />';
    var LicenseAlias = '<input class="formcontrol-newpage" type="text" name="LicenseAlias" style="width:95%; background-color:#ffffc1;" onKeyPress = "return NotAllowNumbers(event);"/>';
    var deleteitem = '<img src="' + IPath + 'delrow.png" class="empdel" alt="Delete" title="Delete" style="cursor:pointer"/>'
    var gridItems = $("#tblLicenseInfo");
    gridItems.append("<tr style=margin-top:2px;><td>" + LicenseType + "</td><td>" + LicenseState + "</td><td>" + LicenseNumber + "</td><td>" + LicenseIssueDate + "</td><td>" + LicenseAlias + "</td><td>" + deleteitem + "</td></tr>");
    Ecount = Number(Ecount) + 1;


    if (num != 0) {
        AddRule(LicenseTypeId); AddDDLRule(StateId);
    }

}

function AddToPersonalInfo() {
    var ReferenceNameId = 'ReferenceName_' + Ecount;
    var ReferencePhoneId = 'ReferencePhone_' + Ecount;
    var ProductsSelected = document.getElementById('ProductsSelected').value;
    var style = ''; var num = 0;
    if (ProductsSelected.indexOf('PRV_') >= 0) {
        style = "width:95%;background-color:#ffffaa"; num++;
    }
    else {
        style = "width:95%; margin-top:5px; important;";

    }
    var ReferenceName = '<input class="formcontrol-newpage" type="text"  id=' + ReferenceNameId + ' name="ReferenceName" style="' + style + '" onKeyPress = "return NotAllowNumbers(event);"/>';
    var ReferencePhone = '<input class="formcontrol-newpage" type="text"  id=' + ReferencePhoneId + ' name="ReferencePhone" style="' + style + '" maxlength = "14" onkeydown = "javascript:backspacerDOWN(this,event);" onkeyup = "javascript:backspacerUP(this,event);"/>';
    var ReferenceAlias = '<input class="formcontrol-newpage" type="text" name="ReferenceAlias" style="width:95%; background-color:#ffffc1;" onKeyPress = "return NotAllowNumbers(event);"/>';
    var deleteitem = '<img src="' + IPath + 'delrow.png" class="empdel" alt="Delete" title="Delete" style="cursor:pointer"/>'
    var gridItems = $("#tblPersonalInfo");
    gridItems.append("<tr><td>" + ReferenceName + "</td><td>" + ReferencePhone + "</td><td>" + ReferenceAlias + "</td><td>" + deleteitem + "</td><td></td><td></td></tr>");

    Ecount = Number(Ecount) + 1;
    if (num != 0) {
        AddRule(ReferenceNameId); AddRule(ReferencePhoneId);
    }
}


function AddToProfessionalInfo() {//INT336 Professional Reference.
    var PReferenceNameId = 'PReferenceName_' + Ecount;
    var PReferencePhoneId = 'PReferencePhone_' + Ecount;
    var ProductsSelected = document.getElementById('ProductsSelected').value;
    var style = ''; var num = 0;
    if (ProductsSelected.indexOf('PRV2_') >= 0) {
        style = "width:95%;background-color:#ffffaa"; num++;
    }
    else {
        style = "width:95%; margin-top:5px; important;";

    }
    var PReferenceName = '<input class="formcontrol-newpage" type="text"  id=' + PReferenceNameId + ' name="PReferenceName" style="' + style + '" onKeyPress = "return NotAllowNumbers(event);"/>';
    var PReferencePhone = '<input class="formcontrol-newpage" type="text"  id=' + PReferencePhoneId + ' name="PReferencePhone" style="' + style + '" maxlength = "14" onkeydown = "javascript:backspacerDOWN(this,event);" onkeyup = "javascript:backspacerUP(this,event);"/>';
    var PReferenceAlias = '<input class="formcontrol-newpage" type="text" name="PReferenceAlias" style="width:95%; background-color:#ffffc1;" onKeyPress = "return NotAllowNumbers(event);"/>';
    var deleteitem = '<img src="' + IPath + 'delrow.png" class="empdel" alt="Delete" title="Delete" style="cursor:pointer"/>'
    var gridItems = $("#tblProfessionalInfo");
    gridItems.append("<tr><td>" + PReferenceName + "</td><td>" + PReferencePhone + "</td><td>" + PReferenceAlias + "</td><td>" + deleteitem + "</td><td></td><td></td></tr>");

    Ecount = Number(Ecount) + 1;
    if (num != 0) {
        AddRule(PReferenceNameId); AddRule(PReferencePhoneId);
    }
}
$('.empdel').live('click', function () {
    $(this).closest('tr').remove();
});


function OpenPackage(id) {
    var tblPackageList = $('#tblPackageList-' + id);
    var img = $('#img-' + id);
    var Currentloc = window.location.href.toLowerCase();
    if (Currentloc.indexOf("test.intelifi.com") > 1) {

        if (tblPackageList.css('display') == "none") {
            $('#tblPackageList-' + id).slideDown('slow')
            img.attr('src', IPath + 'arrowcollaspe.png');
        }
        else {
            $('#tblPackageList-' + id).slideUp('slow')
            img.attr('src', IPath + 'arrowexpand.png');
        }
    }
    else {

        if (tblPackageList.css('display') == "none") {
            $('#tblPackageList-' + id).slideDown('slow')
            img.attr('src', '../' + IPath + 'arrowcollaspe.png');
        }
        else {
            $('#tblPackageList-' + id).slideUp('slow')
            img.attr('src', '../' + IPath + 'arrowexpand.png');
        }

    }

}
function OpenApplicant(id) {

    var tblApplicant = $('#tblApplicant' + id);
    var img = $('#imgApplicant' + id);
    var Currentloc = window.location.href.toLowerCase();
    if (Currentloc.indexOf("test.intelifi.com") > 1) {

        if (tblApplicant.css('display') == "none") {
            tblApplicant.slideDown('slow')
            img.attr('src', IPath + 'arrowcollaspe.png');
        }
        else {
            tblApplicant.slideUp('slow')
            img.attr('src', IPath + 'arrowexpand.png');
        }
    }
    else {

        if (tblApplicant.css('display') == "none") {
            tblApplicant.slideDown('slow')
            img.attr('src', '../' + IPath + 'arrowcollaspe.png');
        }
        else {
            tblApplicant.slideUp('slow')
            img.attr('src', '../' + IPath + 'arrowexpand.png');
        }
    }

}


$(document).ready(function () {
    $(".css-label").css("margin-top", "0px");
    $("#DivNewReportmessage").kendoWindow({ title: "Warning" });
});




function CheckSubmit() {
    //#Ticket 402

    var socialcond = "1";
    for (var i = 0; i < controlarray.length; i++) {
        var NewElement = controlarray[i].toLowerCase().replace(/ /g, "");
        if (NewElement == "social") { socialcond = "2"; }
    }
    // INT-14
    //validate emailid 
    if ($("#noEmail").is(':checked')) {
    }
    else {

        if (ISNCRSelected == "TRUE" || ISNCRPLUSSelected == "TRUE") {
            var enterdemail = document.getElementById('ApplicantEmail').value;
            if (enterdemail == '') {

                $("div[id=form1_ApplicantEmail_errorloc]").html("*Required*");
                //$("div[id=form1_DOB]").html("*Required*");
                return false;
            }
            else {

                $("div[id=form1_ApplicantEmail_errorloc]").html("");
            }
        }
    }
    //INT-14

    // for DOB Validation Issue Regarding Ticket 410
    var color = $("#DOB").css("background-color");
    if (color.indexOf("rgb(255, 255, 170)") >= 0 || color.indexOf("#ffffaa") >= 0) {
        var enterdDate = document.getElementById('DOB').value;
        if (enterdDate == '') {
            $("div[id=form1_DOB_errorloc]").html("*Required*");
            $("div[id=form1_DOB]").html("*Required*");
            return false;
        }
        else { Valid_DOB() }
    }




    //
    var runreportflag = "";
    //#Ticket 373
    var socialflag = 1;
    var colorSocial = $("#Social").css("background-color");
    if (colorSocial.indexOf("rgb(255, 255, 170)") >= 0 || colorSocial.indexOf("#ffffaa") >= 0) {
        var Socialnumber = document.getElementById('Social').value;
        if (Socialnumber == '') {
            if (document.getElementById('IsturnoffSSN') != null) {
                var isturnoffSSN = document.getElementById('IsturnoffSSN').value; //Ticket#178
                if (isturnoffSSN == "False") {
                    $("div[id=form1_Social_errorloc]").html("*Required*");
                    socialflag = 2;
                    return false;
                }
            }
        }
        else {
            var Page = $("#addadditionalreport").val();
            if (Page == "1") {

                var Socialvalue = $("#SocialMask").val();
                if (/^((?!000)(\d{3}))-((?!00)\d{2})-((?!0000)\d{4})$/.test(Socialvalue)) { socialflag = 1; } else { socialflag = 2; }
            }
            else {

                //if (/^(?!000)(?!666)(?!9)d{3}[- ]?(?!00)d{2}[- ]?(?!0000)d{4}$/.test(Socialnumber)) {
                if (/^((?!000)(\d{3}))-((?!00)\d{2})-((?!0000)\d{4})$/.test(Socialnumber)) { socialflag = 1; } else { socialflag = 2; }
            }
        }
    }

    //#Ticket 402
    if ($("#hdnextracheckvalidation").val() == "5") { }
    else {
        if (controlarray.length > 0) {

            if (socialcond != "2") { socialflag = 1; }
        }
        controlarray = [];
    }
    //



    var flag_state = "";
    if ($(".tempval").val() == "2") {
        //INT14 changes
        var flagValidateApplicantnewcheck = false;
        if ($('input[id=noEmail]').is(':checked')) {
            var flagValidateApplicantnewcheck = true;
        }
        if ((ISNCRSelected == "TRUE" || ISNCRPLUSSelected == "TRUE") && flagValidateApplicantnewcheck == true) {
            //INT14 changes         
        }
        else {
            $("div[id=form1_Address_errorloc]").attr("id", "form1_Address");
            $("div[id=form1_Street_errorloc]").attr("id", "form1_Street");
            $("div[id=form1_City_errorloc]").attr("id", "form1_City");
            $("div[id=form1_apt_errorloc]").attr("id", "form1_apt");
            $("div[id=form1_Zip_errorloc]").attr("id", "form1_Zip");
            $('#Address').replaceWith("<input type='text' class='k-textbox' style='width: 90px;' name='Address' maxlength=12 id='Address'>");
            $('#Street').replaceWith("<input type='text' class='k-textbox' style='width:95%;' name='Street' maxlength='12' id='Street'>");
            $('#City').replaceWith("<input type='text' class='k-textbox' style=width:95%; name='City' maxlength='12' id='City'>");
            $('#Zip').replaceWith("<input type='text' class='k-textbox' style='width:95%;' onblur='ValidZip();' onkeypress='return numbersOnly(event);' name='Zip' maxlength=5 id='Zip'>");
        }
    }

    if ($("#hdnextracheckvalidation").val() == "5") {

        if (document.getElementById('ProductsSelected').value != "") {
            var temFirstname = $('#FirstName').val();
            var temLastName = $('#LastName').val();
            var temSocial = $('#Social').val();
            var temDOB = $('#DOB').val();
            var temApplicantEmail = $('#ApplicantEmail').val();
            $("div[id=form1_LastName_errorloc]").attr("id", "form1_LastName");
            $("div[id=form1_FirstName_errorloc]").attr("id", "form1_FirstName");
            $("div[id=form1_Social_errorloc]").attr("id", "form1_Social");
            $("div[id=form1_DOB_errorloc]").attr("id", "form1_DOB");
            $("div[id=form1_ApplicantEmail_errorloc]").attr("id", "form1_ApplicantEmail");
            //  $("div[id=form1_State_errorloc]").attr("id", "form1_State");
            $('#FirstName').replaceWith("<input type='text' class='k-textbox' style='width:95%;background-color: rgb(255, 255, 170);' onkeypress='return NotAllowNumbers(event);'onkeyup = 'this.value=this.value.replace(/[^a-z A-Z]/g,'');'  onPaste = 'RemoveSpecialCharacter(this.id)' onbeforepaste='RemoveSpecialCharacter(this.id)' ondrop='RemoveSpecialCharacter(this.id)'  onchange = 'RemoveSpecialCharacter(this.id)' name='FirstName' id='FirstName'>");
            $('#LastName').replaceWith("<input type='text' class='k-textbox' style='width:95%;background-color: rgb(255, 255, 170);' onkeypress='return NotAllowNumbers(event);' onkeyup = 'this.value=this.value.replace(/[^a-z A-Z]/g,'');'  onPaste = 'RemoveSpecialCharacter(this.id)' onbeforepaste='RemoveSpecialCharacter(this.id)' ondrop='RemoveSpecialCharacter(this.id)'  onchange = 'RemoveSpecialCharacter(this.id)' name='LastName' id='LastName'>");
            $('#Social').replaceWith("<input type='text' class='k-textbox' style='width:95%;background-color: rgb(255, 255, 170);' onkeypress='return SSNcheck(event);' onkeydown='javascript: MaskSSN(this.value,event);' onblur='ValidSsnapi();' name='Social' maxlength='11' id='Social'>");
            // $('#Apt').replaceWith("<input type='text' style='width: 90%;' name='Apt' maxlength=12 id='Apt'>");
            $('#DOB').replaceWith("<input type='text' class='k-textbox' style='width: 95%;background-color: rgb(255, 255, 170);' onkeypress='return DOBcheck(event);' onkeydown='javascript: MaskDOB(this,event);' onblur='Valid_DOB_Api();' name='DOB' maxlength='10' id='DOB'>");
            $('#ApplicantEmail').replaceWith("<input type='text' class='k-textbox' style='width: 95%; background-color: rgb(255, 255, 170);' onblur='ValidApplicantEmail();' name='ApplicantEmail' id='ApplicantEmail'>");
            $('#FirstName').val(temFirstname);
            $('#LastName').val(temLastName);
            $('#Social').val(temSocial);
            $('#DOB').val(temDOB);
            $('#ApplicantEmail').val(temApplicantEmail);
            if ($('#FirstName').val() != "") { $("div[id=form1_FirstName]").html(""); }
            if ($('#LastName').val() != "") { $("div[id=form1_LastName]").html(""); }
            if ($('#Social').val() != "") { $("div[id=form1_Social]").html(""); }
            if ($('#DOB').val() != "") { $("div[id=form1_DOB]").html(""); }
            if ($('#ApplicantEmail').val() != "") { $("div[id=form1_ApplicantEmail]").html(""); }
            if ($('#FirstName').val() == "") { $("div[id=form1_FirstName]").html("*Required*"); runreportflag = 5; }
            if ($('#LastName').val() == "") { $("div[id=form1_LastName]").html("*Required*"); runreportflag = 5; }
            // if ($('#Social').val() == "") { $("div[id=form1_Social]").html("*Required*"); runreportflag = 5; }
            if ($('#DOB').val() == "") { $("div[id=form1_DOB]").html("*Required*"); runreportflag = 5; }
            if (socialflag == 2) {
                if (document.getElementById('IsturnoffSSN') != null) {
                    var isturnoffSSN = document.getElementById('IsturnoffSSN').value; //Ticket#178

                    if (isturnoffSSN == "False") {
                        if (Socialnumber != '') {
                            $("div[id=form1_Social]").html("*Enter valid No.*");
                        }
                        else {
                            $("div[id=form1_Social]").html("*Required*");
                            runreportflag = 5;
                        }
                    }
                }
            }
            if (socialflag == 1) { $("div[id=form1_Social]").html(""); }
            Valid_DOB_Api();

        }

    }

    var newflag = 1;
    if ($('#Address').val() != "") { $("div[id=form1_Address_errorloc]").html(""); }
    if ($('#Street').val() != "") { $("div[id=form1_Street_errorloc]").html(""); }
    if ($('#City').val() != "") { $("div[id=form1_City_errorloc]").html(""); }
    if ($("#State option:selected").text() != "") {
        $("div[id=form1_State_errorloc]").html("");
    }
    if ($('#Zip').val() != "") {
        $("div[id=form1_Zip_errorloc]").html("");
    }
    if ($(".tempval").val() == "1") {
        if ($('#Address').val() == "" || $('#Street').val() == "" || $('#City').val() == "" || $('#State').val() == "" || $('#Zip').val() == "") {
            newflag = 2;
            if ($('#Address').val() == "") { $("div[id=form1_Address_errorloc]").html("*Required*"); }
            if ($('#Street').val() == "") { $("div[id=form1_Street_errorloc]").html("*Required*"); }
            if ($('#City').val() == "") { $("div[id=form1_City_errorloc]").html("*Required*"); }
            if ($("#State option:selected").text() == "") { $("div[id=form1_State_errorloc]").html("*Required*"); flag_state = "1"; }
            if ($('#Zip').val() == "") { $("div[id=form1_Zip_errorloc]").html("*Required*"); }
        }
    }
    var ProductsSelected = document.getElementById("ProductsSelected");
    var _value = $("#ProductsSelected").val();
    if (ProductsSelected.value.length > 0) {
        var selObj = document.getElementById("TrackingRefCode");
        var num = selObj.selectedIndex;
        var TrackingRefCodeText = document.getElementById("TrackingRefCodeText");
        TrackingRefCodeText.value = selObj.options[num].text;
        var selJurObj = document.getElementById("Jurisdiction");
        var numJur = selJurObj.selectedIndex;
        var JurisdictionText = document.getElementById("JurisdictionText");
        JurisdictionText.value = selJurObj.options[numJur].text;
        if (document.forms["form1"].onsubmit()) {
            if (newflag == 2) {
                statemessage(flag_state);
                $("#hdnextracheck").val("");
                var color = $("#DOB").css("background-color");
                if (color.indexOf("rgb(255, 255, 170)") >= 0) {
                    var enterdDate = document.getElementById('DOB').value;
                    if (enterdDate == '') {
                        $("div[id=form1_DOB_errorloc]").html("*Required*");
                        return false;
                    }
                    else {
                        Valid_DOB();
                        return false;

                    }
                }
                return false;
            }
            else {

                if ($("#hdnextracheckvalidation").val() == "5") {
                    if ($("#hdnextracheck").val() == "") {
                        if (runreportflag != 5) { popupauth(); }
                        var color = $("#DOB").css("background-color");
                        if (color.indexOf("rgb(255, 255, 170)") >= 0 || color.indexOf("#ffffaa") >= 0) {
                            var enterdDate = document.getElementById('DOB').value;
                            if (enterdDate == '') {
                                $("div[id=form1_DOB_errorloc]").html("*Required*");
                                $("div[id=form1_DOB]").html("*Required*");
                                return false;
                            }
                            else {
                                Valid_DOB();
                            }
                        }
                        return false;
                    }
                    else {
                        if (runreportflag == 5) {
                            var color = $("#DOB").css("background-color");
                            if (color.indexOf("rgb(255, 255, 170)") >= 0 || color.indexOf("#ffffaa") >= 0) {
                                var enterdDate = document.getElementById('DOB').value;
                                if (enterdDate == '') {
                                    $("div[id=form1_DOB_errorloc]").html("*Required*");
                                    return false;
                                }
                                else {
                                    Valid_DOB();
                                }
                            } return false;
                        }
                        else {
                            var color = $("#DOB").css("background-color");
                            if (color.indexOf("rgb(255, 255, 170)") >= 0 || color.indexOf("#ffffaa") >= 0) {
                                var enterdDate = document.getElementById('DOB').value;
                                if (enterdDate == '') {
                                    $("div[id=form1_DOB_errorloc]").html("*Required*");
                                    return false;
                                }
                                else {
                                    Valid_DOB();
                                }

                            }

                            if (socialflag != 2) {
                                $("div[id=form1_Social_errorloc]").html("");
                                return true;
                            }
                            else {
                                var isturnoffSSN = document.getElementById('IsturnoffSSN').value; //Ticket#178
                                if (isturnoffSSN == "False") {
                                    $("div[id=form1_Social_errorloc]").html("*Enter valid No.*");
                                    return false;
                                }
                            }
                        }
                    }
                }
                else {
                    var color = $("#DOB").css("background-color");
                    if (color.indexOf("rgb(255, 255, 170)") >= 0 || color.indexOf("#ffffaa") >= 0) {
                        var enterdDate = document.getElementById('DOB').value;
                        if (enterdDate == '') {
                            $("div[id=form1_DOB_errorloc]").html("*Required*");
                            return false;
                        }
                        else {
                            Valid_DOB();
                        }
                    }

                    if (socialflag != 2) {
                        $("div[id=form1_Social_errorloc]").html("");
                        return true;
                    }
                    else {
                        var isturnoffSSN = document.getElementById('IsturnoffSSN').value; //Ticket#178
                        if (isturnoffSSN == "False") {
                            $("div[id=form1_Social_errorloc]").html("*Enter valid No.*");
                            return false;
                        }
                    }
                }
            }
        }
        else {

            statemessage(flag_state);
            $("#hdnextracheck").val("");
            if (socialflag != 2) {
                $("div[id=form1_Social_errorloc]").html("");
                return true;
            }
            else {
                var isturnoffSSN = document.getElementById('IsturnoffSSN').value; //Ticket#178
                if (isturnoffSSN == "False") {
                    $("div[id=form1_Social_errorloc]").html("*Enter valid No.*");
                    return false;
                }
            }
            return false;
        }
    }
    else {
        document.getElementById("lblReportMessage").innerHTML = "Please select at least one report to run.";
        document.getElementById("DivNewReportmessage").style.visibility = "visible";
        $("#DivNewReportmessage").kendoWindow();
        $("#DivNewReportmessage").data("kendoWindow").center();
        $("#DivNewReportmessage").data("kendoWindow").open();
        // alert('Please select at least one report to run.');
        statemessage(flag_state);
        $("#hdnextracheck").val("");
        var color = $("#DOB").css("background-color");
        if (color.indexOf("rgb(255, 255, 170)") >= 0 || color.indexOf("#ffffaa") >= 0) {
            var enterdDate = document.getElementById('DOB').value;
            if (enterdDate == '') {
                $("div[id=form1_DOB_errorloc]").html("*Required*");
                return false;
            }
            else { Valid_DOB(); }
        }
        return false;
    }
    var color = $("#DOB").css("background-color");
    if (color.indexOf("rgb(255, 255, 170)") >= 0 || color.indexOf("#ffffaa") >= 0) {
        var enterdDate = document.getElementById('DOB').value;
        if (enterdDate == '') {
            $("div[id=form1_DOB_errorloc]").html("*Required*");
            return false;
        }
        else { Valid_DOB() }
    }
}

function Valid_DOB() {
    var DOB = document.getElementById('DOB');
    var enterdDate = document.getElementById('DOB').value;
    var DOB_errorloc = document.getElementById('form1_DOB_errorloc')
    if (DOB_errorloc != null) {

        if (enterdDate == '') {
            $("div[id=form1_DOB_errorloc]").html("");
            return true;
        }
        else {
            if (enterdDate.length < 10) {
                $("div[id=form1_DOB_errorloc]").html("*Invalid DOB*");
                return false;
            }
            else {
                var Datefilled = enterdDate.split("/");
                if (Datefilled.length > 2) {
                    var CurrntDate = new Date();
                    var TodayDate = parseInt(CurrntDate.getMonth() + 1) + "/" + CurrntDate.getDate() + "/" + CurrntDate.getFullYear();
                    var newDatefilled = new Date(Datefilled[2], parseInt(Datefilled[1]) - 1, Datefilled[0]);
                    var dtDatefilled = Date.parse(newDatefilled); var dtToday = Date.parse(TodayDate)
                    if (dtDatefilled > dtToday || Datefilled[2] < 1900 || Datefilled[0] > 12 || Datefilled[1] > 31) {
                        $("div[id=form1_DOB_errorloc]").html("*Invalid DOB*");
                        return false;
                    }
                    else if (dtDatefilled > dtToday || Datefilled[2] > CurrntDate.getFullYear() - 18 || Datefilled[0] > 12 || Datefilled[1] > 31) {
                        $("div[id=form1_DOB_errorloc]").html("*Applicant should be atleast 18 years old*");
                        return false;
                    }
                    else {
                        $("div[id=form1_DOB_errorloc]").html("");
                        return true;
                    }
                }
                else {
                    $("div[id=form1_DOB_errorloc]").html("*Invalid DOB*");
                    return false;
                }
            }
        }
    }
}

function Valid_DOB_Api() {
    var DOB = document.getElementById('DOB');
    var enterdDate = document.getElementById('DOB').value;
    var DOB_errorloc = document.getElementById('form1_DOB')
    if (DOB_errorloc != null) {
        if (enterdDate == '') {
            $("div[id=form1_DOB]").html("");
            return true;
        }
        else {
            if (enterdDate.length < 10) {
                $("div[id=form1_DOB]").html("*Invalid DOB*");
                return false;
            }
            else {
                var Datefilled = enterdDate.split("/");
                if (Datefilled.length > 2) {
                    var CurrntDate = new Date();
                    var TodayDate = parseInt(CurrntDate.getMonth() + 1) + "/" + CurrntDate.getDate() + "/" + CurrntDate.getFullYear();
                    var newDatefilled = new Date(Datefilled[2], parseInt(Datefilled[1]) - 1, Datefilled[0]);
                    var dtDatefilled = Date.parse(newDatefilled); var dtToday = Date.parse(TodayDate)
                    if (dtDatefilled > dtToday || Datefilled[2] < 1900 || Datefilled[0] > 12 || Datefilled[1] > 31) {
                        $("div[id=form1_DOB]").html("*Invalid DOB*");
                        return false;
                    }
                    else if (dtDatefilled > dtToday || Datefilled[2] > CurrntDate.getFullYear() - 18 || Datefilled[0] > 12 || Datefilled[1] > 31) {
                        $("div[id=form1_DOB]").html("*Applicant should be atleast 18 years old*");
                        return false;
                    }
                    else {
                        $("div[id=form1_DOB]").html("");
                        return true;
                    }
                }
                else {
                    $("div[id=form1_DOB]").html("*Invalid DOB*");
                    return false;
                }
            }
        }
    }
}
function ValidSsnapi() {
    var Social = document.getElementById('Social').value;
    var Social_errorloc = document.getElementById('form1_Social')
    if (Social_errorloc != null) {
        if (Social == '') {
            Social_errorloc.innerHTML = "";
            return true;
        }
        else {
            if (Social.length < 11) {
                var isturnoffSSN = document.getElementById('IsturnoffSSN').value; //Ticket#178
                if (isturnoffSSN == "False") {
                    Social_errorloc.innerHTML = "Enter valid No.";
                    return false;
                }
            }
            else {
                Social_errorloc.innerHTML = "";
                return true;
            }
        }
    }
    return true;
}

function statemessage(flag_state) {
    if (flag_state == "1") {

        $("div[id=form1_State_errorloc]").html("*Required*");
    }

}
function GetAllReports(PackageId) {
    tooltip('<img alt="loader" src="' + IPath + 'ajax-loader3.gif" />');
    try {
        var fkLocationId = $("#fkLocationId").val();
        var Url = '';
        if (loc.indexOf('newreport') > 0 || loc.indexOf('newbatch') > 0) {
            Url = "../CommonNewReport/GetAllReports";

        }
        else {

            Url = "../CommonNewReport/GetAllReports";
        }
        $.getJSON(Url, { fkLocationId: fkLocationId, PackageId: PackageId }, function (data) {
            tooltip(data);

        });

    } catch (e) {

    }
}

function NotAllowNumbers(e) {
    var keycode = 0;
    if (window.event) {
        keycode = e.keyCode;
    } else if (e) {
        keycode = e.which;
    }

    if (keycode >= 48 && keycode <= 57) {
        return false;
    }
}

function AddBatchCounty() {

    var list = GetColumnsList();
    if (Number(count) < 3) {
        var StateId = 's_' + Ecount;
        var CountyId = 'c_' + Ecount;
        var newItem1 = '<select style="background-color:#ffffaa;width:150px" onchange="headerChoose(this.value, this.id);" id=' + StateId + ' name="CountyInfoState" class="ddlField">' + list + '</select>';
        var newItem2 = '<select style="background-color:#ffffaa;width:250px" onchange="headerChoose(this.value, this.id);"  id=' + CountyId + ' name="CountyInfoCounty"  class="ddlField">' + list + '</select>';
        var newItem3 = '<img src="' + IPath + 'delrow.png" alt="Delete" title="Delete" class="removedel" style="cursor:pointer"/>'
        var gridItems = $("#tblStateList");
        gridItems.append("<tr ><td>" + newItem1 + "</td><td>" + newItem2 + "</td><td>" + newItem3 + "</td></tr>");
        count = Number(count) + 1; Ecount = Number(Ecount) + 1;
    }


}

function AddBatchEmployment() {

    var list = GetColumnsList();
    var StateId = 'Emp_' + Ecount;
    var CompanyNameId = 'CompanyName_' + Ecount;
    var CompanyCityId = 'CompanyCity_' + Ecount;
    var CompanyPhoneId = 'CompanyPhone_' + Ecount;

    var CompanyName = '<select  id=' + CompanyNameId + ' name="CompanyName" style="width:95%;background-color:#ffffaa;" onchange="headerChoose(this.value, this.id);">' + list + '</select>';
    var CompanyState = '<select  id=' + StateId + ' name="CompanyState" style="width:200px;background-color:#ffffaa;" class="ddlField" onchange="headerChoose(this.value, this.id);">' + list + '</select>';
    var CompanyCity = '<select id=' + CompanyCityId + '  name="CompanyCity" style="width:95%;background-color:#ffffaa;" onchange="headerChoose(this.value, this.id);">' + list + '</select>';
    var CompanyPhone = '<select  id=' + CompanyPhoneId + '  name="CompanyPhone" style="width:95%;background-color:#ffffaa;" onchange="headerChoose(this.value, this.id);">' + list + '</select>';
    var AliasName = '<select  name="AliasName" style="width:95%;background-color:#ffffaa;" onchange="headerChoose(this.value, this.id);">' + list + '</select>';

    var deleteitem = '<img src="' + IPath + 'delrow.png" class="empdel" alt="Delete" title="Delete" style="cursor:pointer"/>'
    var gridItems = $("#tblEmplyomentInfo");
    gridItems.append("<tr><td>" + CompanyName + "</td><td>" + CompanyState + "</td><td>" + CompanyCity + "</td><td>" + CompanyPhone + "</td><td>" + AliasName + "</td><td>" + deleteitem + "</td></tr>");
    Ecount = Number(Ecount) + 1;


}


function AddBatchEducation() {
    var list = GetColumnsList();

    var StateId = 'Edu_' + Ecount;
    var SchoolNameId = 'SchoolName_' + Ecount;
    var SchoolCityId = 'SchoolCity_' + Ecount;
    var SchoolName = '<select  name="SchoolName" id=' + SchoolNameId + ' style="width:95%;background-color:#ffffaa" onchange="headerChoose(this.value, this.id);">' + list + '</select>';
    var InstituteState = '<select  id=' + StateId + ' name="InstituteState" style="width:200px;background-color:#ffffaa" class="ddlField" onchange="headerChoose(this.value, this.id);">' + list + '</select>';
    var SchoolCity = '<select   id=' + SchoolCityId + ' name="SchoolCity" style="width:95%;background-color:#ffffaa" onchange="headerChoose(this.value, this.id);">' + list + '</select>';
    var SchoolPhone = '<select  name="SchoolPhone" style="width:95%;background-color:#ffffaa" onchange="headerChoose(this.value, this.id);">' + list + '</select>';
    var InstituteAlias = '<select  name="InstituteAlias" style="width:95%; onchange="headerChoose(this.value, this.id);">' + list + '</select>';
    var deleteitem = '<img src="' + IPath + 'delrow.png" class="empdel" alt="Delete" title="Delete" style="cursor:pointer"/>'
    var gridItems = $("#tblEducationInfo");
    gridItems.append("<tr><td>" + SchoolName + "</td><td>" + InstituteState + "</td><td>" + SchoolCity + "</td><td>" + SchoolPhone + "</td><td>" + InstituteAlias + "</td><td>" + deleteitem + "</td></tr>");
    Ecount = Number(Ecount) + 1;
}

function AddBatchPersonalInfo() {
    var list = GetColumnsList();
    var ReferenceNameId = 'ReferenceName_' + Ecount;
    var ReferencePhoneId = 'ReferencePhone_' + Ecount;

    var ReferenceName = '<select   id=' + ReferenceNameId + ' name="ReferenceName" style="width:95%;background-color:#ffffaa" onchange="headerChoose(this.value, this.id);">' + list + '</select>';
    var ReferencePhone = '<select   id=' + ReferencePhoneId + ' name="ReferencePhone" style="width:95%;background-color:#ffffaa" onchange="headerChoose(this.value, this.id);">' + list + '</select>';
    var deleteitem = '<img src="' + IPath + 'delrow.png" class="empdel" alt="Delete" title="Delete" style="cursor:pointer"/>'
    var gridItems = $("#tblPersonalInfo");
    gridItems.append("<tr><td>" + ReferenceName + "</td><td>" + ReferencePhone + "</td><td>" + deleteitem + "</td></tr>");
    Ecount = Number(Ecount) + 1;
}

function AddBatchLicenseInfo() {
    var list = GetColumnsList();
    var StateId = 'Edu_' + Ecount;
    var LicenseId = 'License_' + Ecount;
    var LicenseTypeId = 'LicenseType_' + Ecount;

    var LicenseType = '<select  id=' + LicenseTypeId + ' name="LicenseType" style="width:95%;background-color:#ffffaa" onchange="headerChoose(this.value, this.id);">' + list + '</select>';
    var LicenseState = '<select  id=' + StateId + ' name="LicenseState" style="width:200px;background-color:#ffffaa" class="ddlField" onchange="headerChoose(this.value, this.id);">' + list + '</select>';
    var LicenseNumber = '<select  name="LicenseNumber" style="width:95%;background-color:#ffffaa" onchange="headerChoose(this.value, this.id);">' + list + '</select>';
    var LicenseIssueDate = '<select id=' + LicenseId + ' type="text" name="LicenseIssueDate" style="width:95%;background-color:#ffffaa"  onchange="headerChoose(this.value, this.id);">' + list + '</select>';
    var LicenseAlias = '<select name="LicenseAlias" style="width:95%;background-color:#ffffaa" onchange="headerChoose(this.value, this.id);">' + list + '</select>';
    var deleteitem = '<img src="' + IPath + 'delrow.png" class="empdel" alt="Delete" title="Delete" style="cursor:pointer"/>'
    var gridItems = $("#tblLicenseInfo");
    gridItems.append("<tr><td>" + LicenseType + "</td><td>" + LicenseState + "</td><td>" + LicenseNumber + "</td><td>" + LicenseIssueDate + "</td><td>" + LicenseAlias + "</td><td>" + deleteitem + "</td></tr>");
    Ecount = Number(Ecount) + 1;
}

function GetColumnsList() {
    var list = document.getElementById("FileColumns").value;
    var Columns = list.split(',');

    list = '<option value="-1">--</option>'
    for (var i = 0; i < Columns.length; i++) {
        list += '<option value="' + i + '">' + Columns[i] + '</option>'
    }
    return list
}