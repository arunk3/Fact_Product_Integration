﻿


function CreateDynamicButton() {
    $.post("../Control/GetReportComments", function (result) {
        $('#tblNCR tr').each(function (index, object) {
            var mainTag = $(this);
            var ptd = $(mainTag).find('#divBtn');
            $.each(result, function (i, field) {
                if ($(ptd).find('#' + field.pkLineId).length == 0) {
                    var ch = ptd.append('<div id="' + field.pkLineId + '"> </div> ');
                    var lid = field.pkLineId;
                    var res = result;
                    $.each(res, function (i1, field1) {
                        if (field1.pkLineId == lid) {
                            if ($(ptd).find('input[type="button"]').attr("value") != field1.ButtonTitle) {
                                $(ch).append('<input type="button" id="btn1" value="' + field1.ButtonTitle + '" title="' + field1.Comments + '"></input>');
                            }
                        }
                    });
                }

            });
        });
    });
}


function SetTextAfterClickOnDynamicButton() {
    $('#btn1').live("click", function () {
        if ($(this).attr('title') != '') {
            var values = $(this).parent().parent().find('textarea').val();
            var ch = $(this).parent().parent().find('textarea');
            if (values != '') {
                ch.val($.trim(values) + "\n" + $(this).attr('title'));
            }
            else {
                ch.val($(this).attr('title'));
            }
            //$(this).parent().parent().find('textarea').append($(this).attr('title')+"<br/>");
        }
    });
}