﻿//Initiate Lib

var SDKLicenseKey = "ECE2A9WJ8WFJALBW"; // "JN3UFJAF8P89Z5JL"; //"WGBR8NYF1C8H5UH6"; //"2NE1DKWMG1Z95U12";
var errorFlag = false;
var myinterval;
var size = 2;
var id = 0;
window.onbeforeunload = winUnload;

function winUnload() {
    UnInitLibrary();
}

function getCSSNLibJObj() {
    if (document.getElementById) {
        return document.getElementById("CSSNLibJ");
    }
    else {
        return null;
    }
}

function user_msg() {
    alert("Please close and re-open your browser once installation is complete");
}

function InitLibrary() {
    if ($("#IsSyncpodforcompany").val() == 'True') {

        var CSSNLibJ = getCSSNLibJObj(); //alert("result1");
        try {
            var result = CSSNLibJ.InitScanLib(SDKLicenseKey);
        }
        catch (err) {
            // alert(err);
            $("#imgIcon").attr("src", "../Content/themes/base/images/syncpod-icon-bw.png");
            $("#txtMessage").html("Start Syncpod <sup style='font-size: 8px;'>TM</sup> Activation <a style='color:#48c6f4' href='http://www.id-reader.com/ftp/applications/sdk/sdk_setup.exe' onclick='javascript:user_msg();'>here</a>");
            return false;
        }
        if (result < 1 && result != -13) {

            ScannerErrors(result);

        }
        else {
            // alert("Connected to the Scanner"); 
            $("#imgIcon").attr("src", "../Content/themes/base/images/syncpod-icon.png");
            $("#txtMessage").html("Syncpod <sup style='font-size: 8px;'>TM</sup> is now Online");
            AutoDetectCard();
            $("#imgIcon").unbind("click").click(function () { InitCard(); });
            //ScanCard();
            //result = CSSNLibJ.InitIdLib(SDKLicenseKey);
            //                    if(result < 1 && result != -13)
            //                    {
            //                        alert("Error init Id Lib - " + result)
            //                    }
        }
    }
    else {
       
        $("#drag_div").css('display', 'none');
        $("#helper").css('display', 'none');
        
    
    }
}

function AutoDetectCard() {
        clearInterval(myinterval);
        myinterval = setInterval("CheckCardInsertion()", 500);
}

function CheckCardInsertion() {
    var CSSNLibJ = getCSSNLibJObj();
    if (CSSNLibJ.IsPaperOn() == 1)
    { InitCard(); }
    if (CSSNLibJ.GetPressedButton() >= 1)
    { InitCard(); }
}

function InitCard() {
    errorFlag = false;

    document.getElementById('divProgress').style.display = "none";
    document.getElementById("divUpload").style.display = "none";
    try {
        if (document.getElementById("ProductsSelected").value == "") { alert('There are no fields in selected report(s).'); return false; }
        var CSSNLibJ = getCSSNLibJObj();
        var result = CSSNLibJ.InitScanLib(SDKLicenseKey);
        if (result < 1 && result != -13) {
            ScannerErrors(result);
        }
        else {
            //ScanCard();
            result = CSSNLibJ.InitIdLib(SDKLicenseKey);
            if (result < 1 && result != -13) {
                alert("Error init Id Lib - " + result)
            }
            else {
                ScanCard();
            }
        }
    }
    catch (e) {
        alert(e.message);
    }
}

function UnInitLibrary() {
    var CSSNLibJ = getCSSNLibJObj();
    CSSNLibJ.UnInitSDK();
}

function ClearData() {
    //CSSNLibJ.ResetIDFields();
    //alert("abc");
    if (document.getElementById("hdnIsSyncpod") != null) {
        document.getElementById("hdnIsSyncpod").value = "false";
        //alert(document.getElementById("hdnIsSyncpod"));
    }
    if (document.getElementById("hdnState") != null) {
        document.getElementById("hdnState").value = "";
        //alert(document.getElementById("hdnState"));
    }
    if (document.getElementById("hdnCity") != null) {
        document.getElementById("hdnCity").value = "";
        //alert(document.getElementById("hdnCity"));
    }
    if (document.getElementById("hdnStreet") != null) {
        document.getElementById("hdnStreet").value = "";
        //alert(document.getElementById("hdnCity"));
    }
    if (document.getElementById("hdnZip") != null) {
        document.getElementById("hdnZip").value = "";
        //alert(document.getElementById("hdnZip"));
    }
    if (document.getElementById("hdnDrivingLic") != null) {
        document.getElementById("hdnDrivingLic").value = "";
        //alert(document.getElementById("hdnDrivingLic"));
    }
    if (document.getElementById("hdnSex") != null) {
        document.getElementById("hdnSex").value = "";
        //alert(document.getElementById("hdnSex"));
    }
    if (document.getElementById("LastName") != null) {
        document.getElementById("LastName").value = "";
        //alert(document.getElementById("LastName"));
    }
    if (document.getElementById("FirstName") != null) {
        document.getElementById("FirstName").value = "";
        //alert(document.getElementById("FirstName"));
    }
    if (document.getElementById("MI") != null) {
        document.getElementById("MI").value = "";
        //alert(document.getElementById("MI"));
    }
    if (document.getElementById("City") != null) {
        document.getElementById("City").value = "";
        //alert(document.getElementById("City"));
    }
    if (document.getElementById("Zip") != null) {
        document.getElementById("Zip").value = "";
        //alert(document.getElementById("Zip"));
    }
    if (document.getElementById("DriverLicense") != null) {
        document.getElementById("DriverLicense").value = "";
        //alert(document.getElementById("DriverLicense"));
    }
    if (document.getElementById("Address") != null) {
        document.getElementById("Address").value = "";
        //alert(document.getElementById("Address"));
    }
    if (document.getElementById("Street") != null) {
        document.getElementById("Street").value = "";
        //alert(document.getElementById("Street"));
    }
    if (document.getElementById("hdnSyncpodImage") != null) {
        document.getElementById("hdnSyncpodImage").value = "";
        //alert(document.getElementById("hdnSyncpodImage"));
    }
    if (document.getElementById("hdnFaceImage") != null) {
        document.getElementById("hdnFaceImage").value = "";
        //alert(document.getElementById("hdnFaceImage"));
    }
    if (document.getElementById("Sex") != null) {
        document.getElementById("Sex").selectedIndex = 0;
        //alert(document.getElementById("Sex"));
    }
    if (document.getElementById("State") != null && document.getElementById("DLState") != null && document.getElementById("CountyInfoState") != null) {
        document.getElementById("State").selectedIndex = 0;
        //alert(document.getElementById("State"));
        document.getElementById("DLState").selectedIndex = 0;
        //alert(document.getElementById("DLState"));
        document.getElementById("CountyInfoState").selectedIndex = 0;
        //alert(document.getElementById("CountyInfoState"));
    }
    if (document.getElementById("DOB") != null) {
        document.getElementById("DOB").value = "";
        //alert(document.getElementById("DOB"));
    }
    //alert(document.getElementById("text"));
    document.getElementById("text").innerHTML = "";
    document.getElementById("scanImage").innerHTML = "<img src=../Content/themes/base/images/cardDemo.jpg></img>"
    document.getElementById("sigImage").innerHTML = "<img src=../Content/themes/base/images/signatureDemo.jpg></img>"
    document.getElementById("faceImage").innerHTML = "<img src=../Content/themes/base/images/faceDemo.jpg></img>"

}

function ScanCard() {
    try {
        var CSSNLibJ = getCSSNLibJObj();
        if (CSSNLibJ.IsScannerValid() == 1) {
            try {
                ClearData();
                CSSNLibJ.SetResolution(300);
                document.getElementById("txtMessage").style.display = "none";
                document.getElementById('divProgress').style.display = "block";
                document.getElementById("divUpload").style.display = "block";
                clearInterval(id);
                id = setInterval("progress()", 20);
                CSSNLibJ.ScanToFile("");
                //id = CSSNLibJ.GetProgress();
                //alert(id);
                document.getElementById('scanImage').innerHTML = "<img src= \"data:image/jpg;base64," + CSSNLibJ.GetImageBufferDataBase64("jpg") + "\"/>";
                ProcessCard();
            }
            catch (e) {
                alert(e.message);
            }
        }
        else {
            alert("Scanner not valid, please check connection to the scanner. If already connected, try refreshing this page using Ctrl + F5.")
        }
    }
    catch (e) {
        alert(e.message);
    }
}

function progress() {
    size = size + 1;
    if (size > 179) {
        clearTimeout(id);
        clearInterval(id);
        document.getElementById('divProgress').style.display = "none";
        document.getElementById("divUpload").style.display = "none";
        document.getElementById("txtMessage").style.display = "block";
        size = 2;
        document.getElementById('divProgress').style.width = "1px";
    }
    document.getElementById('divProgress').style.width = size + "px";
    document.getElementById("lblPercentage").innerHTML = parseInt(size / 1.8) + "%";
}

function ProcessCard() {
    if (errorFlag) {
        winUnload();
    }
    else {
        var CSSNLibJ = getCSSNLibJObj();
        var stateid = CSSNLibJ.DetectState("");
        IdLibErrors(stateid);
        var result = CSSNLibJ.ProcessState("", stateid);
        if (result >= 0) {
            //$("#imgIcon").attr("src", "data:image/jpg;base64," + CSSNLibJ.GetIDFaceImgBufferBase64("", "jpg", stateid));
            $("#txtMessage").html("<span>" + CSSNLibJ.getIDFirstName() + ", " + CSSNLibJ.getIDLastName() + "</span>");
            //alert("success");
            if (document.getElementById("hdnIsSyncpod") != null) {
                document.getElementById("hdnIsSyncpod").value = "true";
            }
            if (document.getElementById("LastName") != null) {
                document.getElementById("LastName").value = CSSNLibJ.getIDLastName();
            }
            if (document.getElementById("FirstName") != null) {
                document.getElementById("FirstName").value = CSSNLibJ.getIDFirstName();
            }
            if (document.getElementById("hdnSyncpodImage") != null) {
                document.getElementById("hdnSyncpodImage").value = "data:image/jpg;base64," + CSSNLibJ.GetImageBufferDataBase64("jpg");
                //alert(document.getElementById("hdnSyncpodImage").value);
                //alert(CSSNLibJ.GetIDFaceImgBufferBase64("jpg"));
            }
            if (document.getElementById("hdnFaceImage") != null) {
                document.getElementById("hdnFaceImage").value = "data:image/jpg;base64," + CSSNLibJ.GetIDFaceImgBufferBase64("", "jpg", stateid);
                //alert(CSSNLibJ.GetIDFaceImgBufferBase64("jpg"));
                //alert(document.getElementById("hdnFaceImage").value);
            }
            if (document.getElementById("DOB") != null) {
                var dob = CSSNLibJ.getIDDateOfBirth().replace('-', '/');
                dob = dob.replace('-', '/');
                var dob_array = dob.split("/");
                dob = (dob_array[0] + "/" + dob_array[1] + "/19" + dob_array[2]);
                document.getElementById("DOB").value = dob;
            }
            /*if (document.getElementById("nr_txtMI") != null) {
                document.getElementById("nr_txtMI").value = CSSNLibJ.getIDMidName();
            }*/
            //alert($('#nr_ProductsSelected').val())
            var IsSCR = document.getElementById("ProductsSelected").value;
            if (IsSCR.indexOf("SCR") != -1) {
                if (document.getElementById("City") != null) {
                    document.getElementById("City").value = CSSNLibJ.getIDCity();
                }
                if (document.getElementById("Zip") != null) {
                    document.getElementById("Zip").value = CSSNLibJ.getIDZip();
                }
                if (document.getElementById("Street") != null && document.getElementById("Address") != null) {
                    var address = CSSNLibJ.getIDAddress();
                    var address_array = address.split(" ");
                    if (address_array.length >= 2) {
                        document.getElementById("Address").value = address_array[0];
                        var street = "";
                        for (var i = 1; i < address_array.length; i++) {
                            street = street + " " + address_array[i];
                            document.getElementById("Street").value = street;
                        }
                    }
                }
            }
            if (document.getElementById("DriverLicense") != null) {
                if (IsSCR.indexOf("MVR") != -1) {
                    document.getElementById("DriverLicense").value = CSSNLibJ.getID().replace(" ","");
                }
            }
            if (document.getElementById("Sex") != null) {
                if (CSSNLibJ.getIDSex() != null) {
                    if (CSSNLibJ.getIDSex() == "M")
                    { document.getElementById("Sex").selectedIndex = 1; }
                    else { document.getElementById("Sex").selectedIndex = 2; }
                }
            }
            if (document.getElementById("State") != null && document.getElementById("DLState") != null && document.getElementById("CountyInfoState") != null) {
                if (CSSNLibJ.getIDState() != null) {
                    var statecode = CSSNLibJ.getIDState();
                    if (statecode.length > 0) {
                        var dt = { statecode: statecode };
                        $.ajax({
                            async: false,
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: "../Common/GetStateNameByStateCode",
                            data: JSON.stringify(dt),
                            dataType: "json",
                            success: function (ResultData) {
                                var textToFind = ResultData.Data;
                                var ddstate = document.getElementById('State');
                                for (var i = 0; i < ddstate.options.length; i++) {
                                    if (ddstate.options[i].text === textToFind) {
                                        if (IsSCR.indexOf("PS") != -1) {
                                            ddstate.selectedIndex = i;
                                        }
                                        if (IsSCR.indexOf("ER") != -1) {
                                            ddstate.selectedIndex = i;
                                        }
                                        if (IsSCR.indexOf("BR") != -1) {
                                            ddstate.selectedIndex = i;
                                        } 
                                        if (IsSCR.indexOf("SCR") != -1) {
                                            ddstate.selectedIndex = i;
                                        }
                                        break;
                                    }
                                } 
                                var ddCountyInfoState = document.getElementById('CountyInfoState');
                                for (var i = 0; i < ddCountyInfoState.options.length; i++) {
                                    if (ddCountyInfoState.options[i].text === textToFind) {
                                        if (ddCountyInfoState.options[i].text === textToFind) {
                                            if (IsSCR.indexOf("CCR") != -1) {
                                                ddCountyInfoState.selectedIndex = i;
                                            }
                                            if (IsSCR.indexOf("RCX") != -1) {
                                                ddCountyInfoState.selectedIndex = i;
                                            }
                                            break;
                                        }
                                    }
                                }
                                var ddDLState = document.getElementById("DLState");
                                for (var i = 0; i < ddDLState.options.length; i++) {
                                    if (ddDLState.options[i].text === textToFind) {
                                        if (IsSCR.indexOf("SCR") != -1) {
                                            ddDLState.selectedIndex = i;
                                        }
                                        if (IsSCR.indexOf("MVR") != -1) {
                                            ddDLState.selectedIndex = i;
                                        }
                                        break;
                                    }
                                }
                            },
                            error: function (responseText, ab, jh) {
                                alert(responseText.status + ' - ' + ab + ' - ' + jh);
                            }
                        });
                    }
                }
            }
                       
            if (document.getElementById("hdnState") != null) {
                if (CSSNLibJ.getIDState() != null) {
                    document.getElementById("hdnState").value = CSSNLibJ.getIDState();
                }
            }
            if (document.getElementById("hdnCity") != null) {
                document.getElementById("hdnCity").value = CSSNLibJ.getIDCity();
            }
            if (document.getElementById("hdnAddress") != null && document.getElementById("hdnStreet") != null) {
                var address = CSSNLibJ.getIDAddress();
                var address_array = address.split(" ");
                if (address_array.length >= 2) {
                    document.getElementById("hdnAddress").value = address_array[0];
                    var street = "";
                    for (var i = 1; i < address_array.length; i++) {
                        street = street + " " + address_array[i];
                        document.getElementById("hdnStreet").value = street;
                    }
                }
            }
            if (document.getElementById("hdnZip") != null) {
                document.getElementById("hdnZip").value = CSSNLibJ.getIDZip();
            }
            if (document.getElementById("hdnDrivingLic") != null) {
                document.getElementById("hdnDrivingLic").value = CSSNLibJ.getID();
            }
            if (document.getElementById("hdnSex") != null) {
                if (CSSNLibJ.getIDSex() != null) {
                    document.getElementById("hdnSex").value = CSSNLibJ.getIDSex();
                }
            }
//            document.getElementById("text").innerHTML = "First Name - " + CSSNLibJ.getIDFirstName()
//                        + "<br />" + "Middle Name - " + CSSNLibJ.getIDMidName()
//                        + "<br />" + "Last Name - " + CSSNLibJ.getIDLastName()
//                        + "<br />" + "ID - " + CSSNLibJ.getID()
//                        + "<br />" + "Expiration Date - " + CSSNLibJ.getIDExpirationDate()
//                        + "<br />" + "Issue Date - " + CSSNLibJ.getIDIssueDate()
//                        + "<br />" + "Eye Color - " + CSSNLibJ.getIDEyeColor()
//                        + "<br />" + "Hair Color - " + CSSNLibJ.getIDHairColor()
//                        + "<br />" + "Height - " + CSSNLibJ.getIDHeight()
//                        + "<br />" + "Weight - " + CSSNLibJ.getIDWeight()
//                        + "<br />" + "Address - " + CSSNLibJ.getIDAddress()
//                        + "<br />" + "City - " + CSSNLibJ.getIDCity()
//                        + "<br />" + "State - " + CSSNLibJ.getIDState()
//                        + "<br />" + "Zip - " + CSSNLibJ.getIDZip()
//                        + "<br />" + "Country - " + CSSNLibJ.getIDCountry()
//                        + "<br />" + "Class - " + CSSNLibJ.getIDClass()
//                        + "<br />" + "Restriction - " + CSSNLibJ.getIDRestriction()
//                        + "<br />" + "DOB - " + CSSNLibJ.getIDDateOfBirth()
//                        + "<br />" + "Sex - " + CSSNLibJ.getIDSex();


            document.getElementById('sigImage').innerHTML = "<img src= \"data:image/jpg;base64," + CSSNLibJ.GetIDSignatureImgBufferBase64("", "jpg", stateid) + "\"/>"

            //result = CSSNLibJ.GetIDSignature("","C:\\CSSNDemoImages\\sigImage.jpg",stateid);
            //IdLibErrors(result);
            //  document.getElementById('sigImage').innerHTML = "<img src=C:\\CSSNDemoImages\\sigImage.jpg></img>"

            document.getElementById('faceImage').innerHTML = "<img src= \"data:image/jpg;base64," + CSSNLibJ.GetIDFaceImgBufferBase64("", "jpg", stateid) + "\"/>"

            //result = CSSNLibJ.GetIDFace("","C:\\CSSNDemoImages\\faceImage.jpg",stateid);
            //IdLibErrors(result);
            //document.getElementById('faceImage').innerHTML = "<img src=C:\\CSSNDemoImages\\faceImage.jpg></img>"

            if (CSSNLibJ.GetIDImageAngle() > 0) {
                AlignImage();
            }

            $("#imgIcon").attr("src", "data:image/jpg;base64," + CSSNLibJ.GetIDFaceImgBufferBase64("", "jpg", stateid));
            //$("#txtMessage").html("<span>" + CSSNLibJ.getIDFirstName() + ", " + CSSNLibJ.getIDLastName() + "</span>");

            winUnload();
        }
        else {
            IdLibErrors(result);
        }
    }
}

function AlignImage() {
    var CSSNLibJ = getCSSNLibJObj();
    document.getElementById('scanImage').innerHTML = "<img src= \"data:image/jpg;base64," + CSSNLibJ.GetImageBufferDataBase64("jpg") + "\"/>" // Dump image from memory which is cropped and rotated
}


function RotateImage() {
    var CSSNLibJ = getCSSNLibJObj();
    var i = CSSNLibJ.RotateImage("", 1, 0, "");
    if (i < 0) {
        alert("Error in rotation");
    }
    else {
        document.getElementById('scanImage').innerHTML = "<img src= \"data:image/jpg;base64," + CSSNLibJ.GetImageBufferDataBase64("jpg") + "\"/>"
    }
}




//Error Handler for Scanner Lib SDK Errors
function ScannerErrors(value) {
    switch (value) {
        case 1:

            alert("Scanner Connected");

            break;

        case -1:

            //alert("ERR_INVALID_SCANNER");
            alert("The attached scanner is invalid.");
            break;

        case -10:

            // alert("ERR_FILE_IO_ERROR");
            alert("Fail to create image file on disk");
            break;

        case -11:

            //alert("ERR_PRINTER_PORT_USED");
            alert("Printer port not available");
            break;

        case -12:

            //alert("ERR_OUT_OF_MEMORY");
            alert("Not enough memory to create temporary image");
            break;

//        case -13:

//            //alert("SLIB_ALREADY_INITIALIZED");
//            winUnload();   
//            $("#txtMessage").html("Syncpod <sup style='font-size: 8px;'>TM</sup> not Connected");
//            $("#imgIcon").click(function () { alert("Please connect scanner and refresh the browser."); });
//           break; 

        case -14:

            //alert("ERR_DRIVER_NOT_FOUND");
            alert("The scanner driver was not found. To fix this error re-install the scanner’s driver");
            break;

        case -15:

            //alert("ERR_SCANNER_BUSY");
            alert("Scanner is busy");
            break;

        case -16:

            //alert("ERR_IMAGE_CONVERSION");
            alert("Image conversion Failed.");
            break;

        case -17:

            //alert("UNLOAD_FAILED_BAD_PARENT");
            alert("Cannot unload the driver since another application is using has load it.");
            break;

        case -18:

            // alert("NOT_INITILIZED");
            alert("The driver is not found in the memory.");
            break;

        case -19:

            // alert("LIBRARY_ALREADY_USED_BY_OTHER_APP");
            alert("Syncpod resources already used by another Application.Please close other tabs/browser and try again.");
            break;

        case -2:

            //alert("ERR_BAD_WIDTH_PARAM OR ERR_SCANNER_GENERAL_FAIL");
            alert("Scanner fail to start.");
            break;

        case -20:

            //alert("LICENSE KEY EXPIRED OR CONFLICT_WITH_INOUTSCAN_PARAM ");
            alert("LICENSE KEY EXPIRED.");
            break;

        case -200:

            //alert("GENERAL_ERR_PLUG_NOT_FOUND");
            alert("The license is valid but no scanner is attached OR The license is temporary but it has expired.");
            break;

        case -201:

            //alert("SCANNER_ALREADY_IN_USE");
            alert("Scanner is already in Use.");
            break;

        case -202:

            //alert("ERR_SCANNER_ALREADY_IN_USE");
            alert("Scanner is already in Use.");
            break;

        case -205:

            //alert("ERR_NO_NEXT_VALUE");
            alert("No more twain scanners found");
            break;

        case -21:

            //alert("LICENSE INVALID OR CONFLICT_WITH_SCAN_SIZE_PARAM ");
            alert("Library was not initialized with proper license.");
            break;

        case -22:

            // alert("LICENSE KEY DOES NOT MATCH LIBRARY OR NO_SUPPORT_MULTIPLE_DEVICES");
            alert("LICENSE KEY DOES NOT MATCH LIBRARY");
            break;

        case -23:

            //alert("ERR_CAM_ALREADY_ASSIGNED");
            alert("Camera already assigned");
            break;

        case -24:

            //alert("ERR_NO_FREE_CAM_FOUND");
            alert("No free camera available");
            break;

        case -25:

            // alert("ERR_CAM_NOT_FOUND");
            alert("Camera not found");
            break;

        case -26:

            alert("ERR_CAM_NOT_ASSIGNED_TO_THIS_APP");

            break;

        case -27:

            alert("ERR_IP_SCAN_VERSION_TOO_OLD");

            break;

        case -28:

            alert("ERR_ASYNC_SCANS_IN_QUEUE");

            break;

        case -3:

            alert("ERR_BAD_HEIGHT_PARAM OR ERR_CANCELED_BY_USER");

            break;

        case -4:
            $("#txtMessage").html("Syncpod <sup style='font-size: 8px;'>TM</sup> not Connected");
            $("#imgIcon").click(function () { alert("Please connect scanner and refresh the browser."); });
            //alert("Scanner not valid, please check connection to the scanner.");
            //alert("ERR_SCANNER_NOT_FOUND");

            break;

        case -5:

            //alert("ERR_HARDWARE_ERROR");
            alert("Scanner hardware error.");
            break;

        case -6:

            //alert("ERR_PAPER_FED_ERROR");
            alert("Document was not fed enough in tray to create image file");
            break;

        case -7:

            //alert("ERR_SCAN_ABORT");
            alert("Scan Aborted");
            break;

        case -8:

            //alert("ERR_NO_PAPER");
            alert("No Paper in tray.");
            break;

        case -9:

            //alert("ERR_PAPER_JAM");
            alert("Paper was jammed while scanner is running");
            break;

    }

}



//Error Handler for Image Lib SDK Errors
function ImageErrors(value) {
    switch (value) {
        case -100: //if error opening image file
            //alert("IMG_ERR_FILE_OPEN"); //prompt error message opening a file
            alert("Cannot open input image file");
            break;
        case -101: //if angle_0 is invalid
            //alert("IMG_ERR_BAD_ANGLE_0"); //prompt error message for invalid angle
            alert("Bad rotation parameter");
            break;
        case -102: //if angle_1 is invalid
            //alert("IMG_ERR_BAD_ANGLE_1"); //prompt error message for invalid angle
            alert("Bad rotation parameter");
            break;
        case -103: //if destination is invalid
            //alert("IMG_ERR_BAD_DESTINATION"); //prompt error message for invalid destination
            alert("Bad destination parameter.");
            break;
        case -104: //if error saving to file
            //alert("IMG_ERR_FILE_SAVE_TO_FILE"); //prompt error message for error saving to file
            alert("Cannot save destination file due to invalid destination file or disk save error");
            break;
        case -105: //if error saving to clipboard
            //alert("IMG_ERR_FILE_SAVE_TO_CLIPBOARD"); //prompt error saving to clipboard
            alert("Cannot save image to clipboard due to an error");
            break;
        case -106: //if error opening first file
            //alert("IMG_ERR_FILE_OPEN_FIRST"); //prompt error opening first file
            alert("Can not open source file");
            break;
        case -107: //if error opening second file
            //alert("IMG_ERR_FILE_OPEN_SECOND"); //prompt error opening second file
            alert("Can not open source file");
            break;
        case -108: //if error comb type
            //alert("IMG_ERR_COMB_TYPE"); //prompt error message
            alert("Bad CombType Value");
            break;
        case -130: //if color is bad/invalid
            //alert("IMG_ERR_BAD_COLOR"); //prompt bad color
            alert("Bad toColor parameter value");
            break;
        case -131: //if dpi setting is invalid
            // alert("IMG_ERR_BAD_DPI"); //prompt bad setting of dpi
            alert("Bad toDpi parameter value.");
            break;
        case -132: //if internal image is invalid
            //alert("INVALID_INTERNAL_IMAGE"); //prompt error message for invalid image
            alert("Internal image is invalid and cannot be analyzed");
            break;
    }
}

//Error Handler for Activation of Twain Scanner on Machine
function ActivationErrors(value) {
    switch (value) {
        case -1: //if activation key is invalid
            alert("Invalid Activation Key"); //prompt error message
            break;
        case -2: //can't find network card
            alert("Cannot find network card"); //prompt error message
            break;
        case -3: //activation key already used on another PC
            alert("Activation failed. Activation key already used on another PC"); //prompt error message
            break;
        case -4: //comm. error
            alert("Communication error, unable to reach to the Activation server. Check firewall settings"); //prompt error message
            break;
        case -5: //can't create activation
            alert("Cannot create activation"); //prompt error message
            break;
        case -11: //activation failed
            alert("Activation Failed"); //prompt error message
            break;
        case -12: //activation failed
            alert("Activation Failed"); //prompt error message
            break;
    }
}

//Error Handler for Id Lib SDK - Extraction of Data from Driver's License
function IdLibErrors(value) {
    switch (value) {
        case -2: //If state not supported
            ProcessCard();
            //alert("ID_ERR_STATE_NOT_SUPORTED"); //prompt error message not supported
            break;
        case -3: //if bad param
            //alert("ID_ERR_BAD_PARAM"); //prompt error message bad param
            alert("Bad field index");
            break;
        case -4: //if no match
            //alert("ID_ERR_NO_MATCH"); //prompt error message no match
            alert("The input state id value does not match any state value");
            break;
        case -5: //If error in file opening
            //alert("ID_ERR_FILE_OPEN"); //prompt error message for file opening error
            alert("Cannot open input image file.");
            break;
        case -6: //if  is bad
            //alert("ID_BAD_DESTINATION_FILE"); //prompt error message for bad destination file
            alert("Invalid destination file");
            break;
        case -7: //if feature not supported
            alert("ID_ERR_FEATURE_NOT_SUPPORTED"); //prompt error message for feature not supported
            break;
        case -8: //if country not initialized
            //alert("ID_ERR_COUNTRY_NOT_INIT"); //prompt error message not initialized
            alert("GetFirstCntry function was not called before");
            break;
        case -9: //if no next country
            //alert("ID_ERR_NO_NEXT_COUNTRY"); //prompt error message of no next country
            alert("This country is the last country in the list.");
            break;
        case -10: //if state not initialized
            //alert("ID_ERR_STATE_NOT_INIT"); //prompt error message of state not initialized
            alert("State not initialized");
            break;
        case -11: //if no next state
            //alert("ID_ERR_NO_NEXT_STATE"); //prompt error message of no next state
            alert("This state is the last state of the country state list");
            break;
        case -12: //if can't delete destination image
            //alert("ID_ERR_CANNOT_DELETE_DESTINATION_IMAGE"); //prompt error of unable to delete destination image
            alert("Destination file already exists and cannot be overwritten");
            break;
        case -13: //if can't copy to destination
            //alert("ID_ERR_CANNOT_COPY_TO_DESTONATION"); //prompt error of unable to copy to destination
            alert("Copying face image file to destination file failed");
            break;
        case -14: //if face image not found
            //alert("ID_ERR_FACE_IMAGE_NOT_FOUND"); //prompt error of face image not found
            alert("Extraction of the face image failed–could not locate the face rectangle");

            break;
        case -15: //if state not recognized
            alert("Please check if card/ID is placed in the Unit and is positioned correctly."); //prompt error message of unrecognized state //ID_ERR_STATE_NOT_RECOGNIZED
            errorFlag = true;
            break;
        case -16: //if usa templates not found
            //alert("ID_ERR_USA_TEMPLATES_NOT_FOUND"); //prompt error message of templates not found
            alert("The template database file for the USA states (UsaIds.bin) is missing. The file should be located in the SDK directory");
            break;
        case -17: //if wrong template file
            //alert("ID_ERR_WRONG_TEMPLATE_FILE"); //prmopt error message of wrong template file\
            alert("The template database file is invalid");
            break;
        case -18: //if region not initialized
            //alert("ID_ERR_REGION_NOT_INIT"); //prompt error message of uninitialization of region
            alert("Region not initialized");
            break;
        case -19: //if auto detect not supported
            // alert("ID_ERR_AUTO_DETECT_NOT_SUPPORTED"); //prompt error message of auto detect not supported
            alert("The parameter RegionId does not support auto detection");
            break;
        case -20: //if no text found
            //alert("ID_ERR_COMPARE_NO_TEXT"); //prompt error message of no text found
            alert("No text found");
            break;
        case -21: //if no barcode found
            //alert("ID_ERR_COMPARE_NO_BARCODE"); //prompt error message of no barcode found
            alert("No Barcode found");
            break;
        case -22: //if BC Lib not found
            //alert("ID_ERR_COMPARE_BC_LIB_NOT_FOUND"); //prompt error message of BC Lib not found
            alert("Barcode library not found");
            break;
        case -23: //if license not matches BC Lib
            //alert("ID_ERR_COMPARE_LICENSE_DONT_MATCH_BC_LIBRARY"); //prompt error mesage
            alert("License not matched with barcode Library ");
            break;
    }
}

//Error Handler for Bar Code Scan Lib SDK
function BCErrors(value) {
    switch (value) {
        case -1: //if parameter invalid
            //alert("BC_ERR_BAD_PARAM"); //prompt invalid parameter message
            alert("Unknown field index number");
            break;
        case 0: //if barcode not found
            // alert("BC_ERR_NO_BC_FOUND"); //prompt barcode not found message
            alert("Barcode not found");
            break;
    }
}

function PassportErrors(value) {
    switch (value) {
        case -132:
            //alert("INVALID_INTERNAL_IMAGE");
            alert("Internal image is invalid and cannot be analyzed");
            break;
        case -40:
            //alert("PASS_ERR_BAD_PARAM");
            alert("Bad field index");
            break;
        case -41:
            //alert("PASS_ERR_CANNOT_DELETE_DESTINATION_IMAGE");
            alert("File with the same name as the destination file already exists and cannot be overwritten.");
            break;
        case -42:
            //alert("PASS_ERR_CANNOT_COPY_TO_DESTINATION");
            alert("Destination file can not be opened for writes on the disk");
            break;
        case -43:
            //alert("PASS_ERR_CANNOT_DELETE_DESTINATION_IMAGE");
            alert("File with the same name as the destination file already exists and cannot be overwritten.");
            break;
    }
}