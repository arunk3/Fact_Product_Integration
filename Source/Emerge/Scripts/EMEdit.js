﻿var offsetx = -150; var offsetx1 = -340; var offsety = 15;
function showEMBox(message) {
    if (!document.getElementById('tooltip'))
        newelement('tooltip');
    var lixlpixel_tooltip = document.getElementById('tooltip');
    var tableStru = "<table id='Table2' runat='server' cellpadding='0' class='HelpTable' cellspacing='0' width='auto'>";
    tableStru += "<tr>";
    tableStru += "<td align='left' style='height:20px;width:auto' class='HelpHeader'>";
    tableStru += message + "</td>";
    tableStru += "</tr></table>";
    lixlpixel_tooltip.innerHTML = tableStru;
    lixlpixel_tooltip.style.display = 'block';
    document.onmousemove = getmouseposition;
}
/**********************Barry****************************/
function tooltipLiveRunner1(productname, productdesc) {
    if (!document.getElementById('tooltip'))
        newelement('tooltip');
    var lixlpixel_tooltip = document.getElementById('tooltip');
    var tableStru = "<table id='Table2' cellpadding='0' style='border:1px solid gray;' class='HelpTable' cellspacing='0' width='250px'>";
    tableStru += "<tr>";
    tableStru += "<td align='left' style='height:20px;width:10px;' class='HelpHeader'>";
    tableStru += productname + "</td>";
    tableStru += "</tr>"; tableStru += "<tr>";
    tableStru += "<td style='height:20px;Background-color:#ffffff;padding:0px 5px 5px 5px;' class='HelpNormal' align='left'><p>";
    tableStru += '<div style="font-size:10px;">' + productdesc + '</div>';
    tableStru += "</p></td></tr>";
    tableStru += "</td></tr></table>";
    lixlpixel_tooltip.innerHTML = tableStru;
    lixlpixel_tooltip.style.display = 'block';
    document.onmousemove = getmouseposition;
}
function ParseDate(jsonDate) {
    date = new Date(parseInt(jsonDate.substr(6)));
    day = date.getDate();
    month = date.getMonth() + 1;
    year = date.getFullYear();
    hrs = date.getHours();
    mts = date.getMinutes();
    sec = date.getSeconds();
    endpart = date.format("h:MM:ss tt");
    fullpart = date.format("MM/dd/yy h:MM:ss tt");
    return fullpart;
    // return month + "-" + day + "-" + year + " " + endpart;
    // return date.format("h:MM:ss tt");
}
function ParseDateFull(jsonDate) {
    var str = val1.CreatedDate;
    var matches = str.match(/([0-9]+)/);
    var d = parseInt(matches[0]);
    var obj = new Date(d);
    date = obj.getTime()
    return obj.format("m/dd/yy h:MM:ss TT");
}
function getImg4Comment(img) {
    if (img.indexOf('#') == -1) {
        return img;
    }
    else {
        var getnum = img.substring(2, 3);
        // alert(getnum);

        return "<img src='../Resources/Images/emerge-office-milestone-" + getnum + "-on.png'>";
    }

}


function CallLogRollOver(productname, ID) {
    if (!document.getElementById('tooltip'))
        newelement('tooltip');
    var lixlpixel_tooltip = document.getElementById('tooltip');
    var ContentLog = "<img alt='loader' src='../Content/themes/base/images/ajax-loader3.gif' />";
    ContentLog = "";
    var dt = { pkCompanyId: ID };
    $.ajax({
        async: false,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "../Control/GetLogCommentsForCompany",
        data: JSON.stringify(dt),
        dataType: "json",
        success: function (ResultData) {
            ContentLog = ResultData.Data;
        }
    });
    var tableStru = "<table id='Table2' cellpadding='0' style='border:1px solid gray;' class='HelpTable' cellspacing='0' width='400px'>";
    tableStru += "<tr>";
    tableStru += "<td align='left' style='height:20px;width:10px;' class='HelpHeader'>";
    tableStru += productname + "</td>";
    tableStru += "</tr>"; tableStru += "<tr>";
    tableStru += "<td style='height:20px;Background-color:#ffffff;padding:0px 5px 5px 5px;' class='HelpNormal' align='left'><p>";
    tableStru += '<div style="font-size:10px;">' + ContentLog + '</div>';
    tableStru += "</p></td></tr>";
    tableStru += "</td></tr></table>";
    lixlpixel_tooltip.innerHTML = tableStru;
    lixlpixel_tooltip.style.display = 'block';
    document.onmousemove = getmouseposition1;
}
/**********************Barry****************************/

function exit() { document.getElementById('tooltip').style.display = 'none'; }
function EMEditMessageBox(EMEditHeading, id) {
    $('#' + id + '_txtEmHeader_Subject').val('');
    $('#' + id + '_mwEMEdit_Table3').css('display', 'block');
    $('#' + id + '_mwEMEdit_EMEditMsgTable').css('display', 'none');
    $('#' + id + '_mwEMEdit_UpdateProgress1').css('display', 'none');
    document.getElementById(id + '_mwEMEdit_lblEMMessage').innerHTML = "";
    document.getElementById(id + '_mwEMEdit_txtSuggestions').value = "";
    document.getElementById(id + '_mwEMEdit_lblEMHeader').innerHTML = EMEditHeading;
    document.getElementById(id + '_mwEMEdit_lblEMSubject').innerHTML = EMEditHeading;
    $('#' + id + '_txtEmHeader_Subject').val(EMEditHeading);
    var iFrame = $('#' + id + '_pdfBilling');
    if (iFrame != null) {
        iFrame.css('display', 'none');
    }
    var pop = $find('bActivateEMPop'); if (pop == null) return; pop.show();
}

function CloseWindowReport_sendEMmail() {
    var iFrame = $('#ctl00_ContentPlaceHolder1_pdfBilling');
    if (iFrame != null) {
        iFrame.css('display', 'block');
    }
    var pop = $find('bActivateEMPop'); if (pop == null) return; pop.hide();
    var popRR = $find('bActivateEMPopRR'); if (popRR == null) return; popRR.hide();
    var SEPPopRR = $find('SEPPopRR'); if (SEPPopRR == null) return; SEPPopRR.hide();
    // var popErrorLog = $find('bActivateErrorLog'); if (popErrorLog == null) return; popErrorLog.hide(); 
}

function CloseWindowReport_sendEMmail1() {
    var iFrame = $('#ctl00_ContentPlaceHolder1_pdfBilling');
    if (iFrame != null) {
        iFrame.css('display', 'block');
    }
    var pop1 = $find('bActivateEMPop1');
    if (pop1 == null) { }; pop1.hide();
    var popZ = $find('bActivateEMPop2');
    if (popZ == null) { } else { popZ.hide(); }
    var pop3 = $find('bActivateEMPop3');
    if (pop3 == null) { } else { pop3.hide(); }
    var pop4 = $find('bActivateEMPop4');
    if (pop4 == null) { } else { pop4.hide(); }
    var pop5 = $find('bQtyPnl');
    if (pop5 == null) { } else { pop5.hide(); }
}
function newelement(newid) {
    if (document.createElement) {
        var el = document.createElement('div'); el.id = newid; with (el.style) { display = 'none'; position = 'absolute'; }
        el.innerHTML = '&nbsp;'; document.body.appendChild(el);
    }
}
var ie5 = (document.getElementById && document.all); var ns6 = (document.getElementById && !document.all); var ua = navigator.userAgent.toLowerCase(); var isapple = (ua.indexOf('applewebkit') != -1 ? 1 : 0); function getmouseposition(e) { if (document.getElementById) { var iebody = (document.compatMode && document.compatMode != 'BackCompat') ? document.documentElement : document.body; pagex = (isapple == 1 ? 0 : (ie5) ? iebody.scrollLeft : window.pageXOffset); pagey = (isapple == 1 ? 0 : (ie5) ? iebody.scrollTop : window.pageYOffset); if (window.pageYOffset != 0 && pagey == 0) { pagey = window.pageYOffset; } mousex = (ie5) ? event.x : (ns6) ? clientX = e.clientX : false; mousey = (ie5) ? event.clientY : (ns6) ? clientY = e.clientY : false; var lixlpixel_tooltip = document.getElementById('tooltip'); lixlpixel_tooltip.style.left = (mousex + pagex + offsetx) + 'px'; lixlpixel_tooltip.style.top = (mousey + pagey + offsety) + 'px'; } }
var ie5 = (document.getElementById && document.all); var ns6 = (document.getElementById && !document.all); var ua = navigator.userAgent.toLowerCase(); var isapple = (ua.indexOf('applewebkit') != -1 ? 1 : 0); function getmouseposition1(e) { if (document.getElementById) { var iebody = (document.compatMode && document.compatMode != 'BackCompat') ? document.documentElement : document.body; pagex = (isapple == 1 ? 0 : (ie5) ? iebody.scrollLeft : window.pageXOffset); pagey = (isapple == 1 ? 0 : (ie5) ? iebody.scrollTop : window.pageYOffset); if (window.pageYOffset != 0 && pagey == 0) { pagey = window.pageYOffset; } mousex = (ie5) ? event.x : (ns6) ? clientX = e.clientX : false; mousey = (ie5) ? event.clientY : (ns6) ? clientY = e.clientY : false; var lixlpixel_tooltip = document.getElementById('tooltip'); lixlpixel_tooltip.style.left = (mousex + pagex + offsetx1) + 'px'; lixlpixel_tooltip.style.top = (mousey + pagey + offsety) + 'px'; } }

function EMEditMessageBoxMaster(EMEditHeading, id) {
    $('#' + id + '_txtEmHeader_Subject').val('');
    $('#' + id + '_mwEMEdit_Table3').css('display', 'block');
    $('#' + id + '_mwEMEdit_EMEditMsgTable').css('display', 'none');
    $('#' + id + '_mwEMEdit_UpdateProgress1').css('display', 'none');
    document.getElementById(id + '_mwEMEdit_lblSuggValidator').innerHTML = "";
    document.getElementById(id + '_mwEMEdit_lblEMMessage').innerHTML = "";
    document.getElementById(id + '_mwEMEdit_txtSuggestions').value = "";
    document.getElementById(id + '_mwEMEdit_lblEMHeader').innerHTML = "";
    document.getElementById(id + '_mwEMEdit_lblEMSubject').innerHTML = "";
    document.getElementById(id + '_mwEMEdit_lblEMHeader').innerHTML = EMEditHeading;
    document.getElementById(id + '_mwEMEdit_lblEMSubject').innerHTML = EMEditHeading;
    $('#' + id + '_txtEmHeader_Subject').val(EMEditHeading);
    var iFrame = $('#ctl00_ContentPlaceHolder1_pdfBilling');
    if (iFrame != null) {
        iFrame.css('display', 'none');
    }
    var pop1 = $find('bActivateEMPop1'); if (pop1 == null) return; pop1.show();

}

function validateEMEdit(id) {
    document.getElementById("ctl00_mwEMEdit_lblSuggValidator").innerHTML = "";
    if (document.getElementById("ctl00_mwEMEdit_txtSuggestions").value == "") {
        document.getElementById("ctl00_mwEMEdit_lblSuggValidator").innerHTML = "Please input your suggestions";
        return false;
    }
    else {
        document.getElementById("ctl00_mwEMEdit_lblSuggValidator").innerHTML = "";
        return true;
    }
}
function ShowEmergeLogo(a) {
    var hdn = document.getElementById('ctl00_LblHdnDiv');
    if (hdn.innerHTML == 0) {
        $('#office').toggle(2000);
        $('#emerge').toggle(2000);
        hdn.innerHTML = 1;
    }
}
function ShowOfficeLogo(a) {
    var hdn = document.getElementById('ctl00_LblHdnDiv');
    if (hdn.innerHTML == 1) {
        $('#office').toggle(2000);
        $('#emerge').toggle(2000);
        hdn.innerHTML = 0;
        setTimeout("ShowEmergeLogoWhenOfficeStuck()", 15000);
    }
}
function ShowEmergeLogoWhenOfficeStuck() {
    var hdn = document.getElementById('ctl00_LblHdnDiv');
    if (hdn.innerHTML == 0) {
        ShowEmergeLogo('a');
    }
}
function NavigateToOffice(path) {
    window.open('' + path + 'Office/Leads.aspx', '_self');
}
/*=============For Office=======================*/
function ShowEmergeLogoForOffice(a) {
    var hdn = document.getElementById('ctl00_LblHdnDiv');
    if (hdn.innerHTML == 1) {
        $('#office').toggle(2000);
        $('#emerge').toggle(2000);
        hdn.innerHTML = 0;
        setTimeout("ShowOfficeLogoWhenEmergeStuck()", 15000);
    }
}
function ShowOfficeLogoForOffice(a) {
    var hdn = document.getElementById('ctl00_LblHdnDiv');
    if (hdn.innerHTML == 0) {
        $('#office').toggle(2000);
        $('#emerge').toggle(2000);
        hdn.innerHTML = 1;
    }
}
function ShowOfficeLogoWhenEmergeStuck() {
    var hdn = document.getElementById('ctl00_LblHdnDiv');
    if (hdn.innerHTML == 0) {
        ShowOfficeLogoForOffice('a');
    }
}
function NavigateToEmerge() {
    window.open('../Control/Default.aspx', '_self');
}
function ShowAlert() {
    alert("Alert Is Working");
}
function SelectZip(CityValue) {
    CityZipValue = $("#ctl00_ContentPlaceHolder1_ddlCity option:selected").text();
    CityZip = CityZipValue.split('-');
    ZipCode = CityZip[1];
    $("#ctl00_ContentPlaceHolder1_txtzipCode").val(ZipCode);
}
function SetHiddenFields(NodeIdToDelete, pkID) {
    ButtonIdArr = NodeIdToDelete.split('_');
    ProductCode = ButtonIdArr[1];
    Node = ButtonIdArr[2];
    if (ProductCode == "NCR+") {
        ProductCode = "NCRPLUS"
    }
    //alert("#chkIsDeleted" + "_" + Node + "_" + pkID);
    IsDeleted = $("#chkIsDeleted" + "_" + Node + "_" + pkID + ":checked").val();
    // alert(IsDeleted);
    if (IsDeleted != null) {
        if (IsDeleted == "on")
            $("#ctl00_ContentPlaceHolder1_ViewReportReview_hdnIsDeleted").val("1_" + Node + "_" + ProductCode);
    }
    else {
        $("#ctl00_ContentPlaceHolder1_ViewReportReview_hdnIsDeleted").val("0_" + Node + "_" + ProductCode);
    }
    Note = $("#txtNote" + "_" + Node + "_" + pkID).val();
    $("#ctl00_ContentPlaceHolder1_ViewReportReview_hdnNote").val(Note);
    $("#ctl00_ContentPlaceHolder1_ViewReportReview_btnSaveReportChanges").click();  //+ "     " + $("#ctl00$ContentPlaceHolder1$viewReport$hdnIsDeleted").html()
}
function SetHiddenFieldsForTDR(NodeIdToDelete) {
    //alert(NodeIdToDelete);
    // return;
    ButtonIdArr = NodeIdToDelete.split('_');
    ProductCode = ButtonIdArr[1];
    Node = ButtonIdArr[2];
    ElementName = ButtonIdArr[3];
    if (ElementName == "individual/addresses/address") {
        ElementName = "IA";
    }
    if (ProductCode == "NCR+") {
        ProductCode = "NCRPLUS"
    }
    IsDeleted = $("#chkIsDeleted" + ProductCode + "_" + Node + "_" + ElementName + ":checked").val();
    if (IsDeleted != null) {
        if (IsDeleted == "on")
            $("#ctl00_ContentPlaceHolder1_ViewReportReview_hdnIsDeleted").val("1_" + Node + "_" + ProductCode + "_" + ElementName);
    }
    else {
        $("#ctl00_ContentPlaceHolder1_ViewReportReview_hdnIsDeleted").val("0_" + Node + "_" + ProductCode + "_" + ElementName);
    }
    Note = "txtNote" + ProductCode + "_" + Node + "_" + ElementName;
    Noted = document.getElementById(Note).value;
    $("#ctl00_ContentPlaceHolder1_ViewReportReview_hdnNote").val(Noted);

    $("#ctl00_ContentPlaceHolder1_ViewReportReview_btnSaveReportChanges").click();  //+ "     " + $("#ctl00$ContentPlaceHolder1$viewReport$hdnIsDeleted").html()
}
function SetHiddenFieldsForOtherReviewReports(SelectedReport) {
    var Note = $("#txt_" + SelectedReport).val();
    $("#ctl00_ContentPlaceHolder1_ViewReportReview_hdnNote").val(Note);
    $("#ctl00_ContentPlaceHolder1_ViewReportReview_hdnIsDeleted").val(SelectedReport);
    //alert($("#ctl00_ContentPlaceHolder1_viewReport_hdnIsDeleted").val());
    $("#ctl00_ContentPlaceHolder1_ViewReportReview_btnEmergeReviewForOther").click();  //+ "     " + $("#ctl00$ContentPlaceHolder1$viewReport$hdnIsDeleted").html()
}


function SetHiddenFieldsForNRF(NRFId) {
    ReviewNote = document.getElementById("txtNoteNRF_" + NRFId).value;
    $("#ctl00_ContentPlaceHolder1_ViewReportReview_hdnNote").val(ReviewNote);
    $("#ctl00_ContentPlaceHolder1_ViewReportReview_hdnIsDeleted").val("NRF");
    document.getElementById("ctl00_ContentPlaceHolder1_ViewReportReview_HDNReportName").value = NRFId;
    $("#ctl00_ContentPlaceHolder1_ViewReportReview_btnEmergeReviewForOther").click();
}
function SetReviewNoteForNRFReports(ProductCode, EmergeReviewComment, pkOrderResponseId) {
    if (document.getElementById("txtNoteNRF_" + ProductCode) != null) {
        document.getElementById("txtNoteNRF_" + ProductCode).value = EmergeReviewComment;
    }
    if (document.getElementById("txtReviewNoteNRF_" + ProductCode) != null && EmergeReviewComment != "") {
        document.getElementById("txtReviewNoteNRF_" + ProductCode).innerHTML = EmergeReviewComment;
    }
    else {
        if (document.getElementById("txtReviewNoteNRF_" + ProductCode) != null)
            document.getElementById("txtReviewNoteNRF_" + ProductCode).style.display = "none";
    }
}


IsAjaxCall = 1;
ReportCount = 1;
RecordCount = 1;

function SaveReviewChanges() {

    document.getElementById('ctl00_ContentPlaceHolder1_ViewReportReview_BtnSaveAllTop').style.display = 'none';
    document.getElementById('ctl00_ContentPlaceHolder1_ViewReportReview_BtnSaveAllBottom').style.display = 'none';
    document.getElementById('ctl00_UpdateProgress2').style.display = 'block';
    document.getElementById('imgWaitT').style.display = 'block';
    document.getElementById('imgWaitB').style.display = 'block';
    //setTimeout("SaveReviewedReportChanges()", 200);
    SaveReviewedReportChanges();
    return false;
}

function SaveReviewedReportChanges() {

    hdnIs6MonthOld = document.getElementById("ctl00_ContentPlaceHolder1_ViewReportReview_hdnIs6MonthOld").value;
    ReviewOrderDetail = document.getElementById("ctl00_ContentPlaceHolder1_ViewReportReview_hdnReviewDetail").value;
    FullQueryString = document.getElementById("ctl00_ContentPlaceHolder1_ViewReportReview_hdnFullQString").value;
    hdnIsTagOnPage = document.getElementById("ctl00_ContentPlaceHolder1_ViewReportReview_hdnIsTagOnPage").value;
    ReviewOrderDetailArr = ReviewOrderDetail.split(',');
    TotalReportsInOrder = ReviewOrderDetailArr.length;

    for (iCount = 1; iCount <= TotalReportsInOrder; iCount++) {
        iRecord = 1;
        DetailPerReport = ReviewOrderDetailArr[iCount - 1].split('_');
        pkOrderDetailId = DetailPerReport[0];
        ProductCode = DetailPerReport[1];
        if (ProductCode == "TDR") {
            SaveReviewChanges4TDR(ProductCode, pkOrderDetailId, DetailPerReport[2]);
            if (iCount < TotalReportsInOrder) {
                continue;
            }
        }

        if (hdnIsTagOnPage.indexOf(ProductCode) != -1) {
            ReportContainText(ProductCode, pkOrderDetailId, DetailPerReport[2]);
            if (iCount < TotalReportsInOrder) {
                continue;
            }
        }
        if (ProductCode == "NCR+") {
            ProductCode = "NCRPLUS";
        }
        pkOrderResponseId = DetailPerReport[2];

        if (document.getElementById("txtTotalRecord_" + pkOrderDetailId) != null) {
            RecordsPerReport = document.getElementById("txtTotalRecord_" + pkOrderDetailId).value;
        }
        else
        { RecordsPerReport = 0; }

        ValidateReportForNextCall();

    }
    return false;
}

function ValidateReportForNextCall() {
    hdnIs6MonthOld = document.getElementById("ctl00_ContentPlaceHolder1_ViewReportReview_hdnIs6MonthOld").value;
    if (iRecord <= RecordsPerReport) {
        IsDeleted = $("#chkIsDeleted" + "_" + iRecord + "_" + pkOrderDetailId + ":checked").val();
        IshdnDeleted = $("#chkhdnIsDeleted" + "_" + iRecord + "_" + pkOrderDetailId + ":checked").val();
        ReviewNote = document.getElementById("txtNote_" + iRecord + "_" + pkOrderDetailId).value;
        ReviewhdnNote = document.getElementById("txthdnNote_" + iRecord + "_" + pkOrderDetailId).value;

        if (IsDeleted != IshdnDeleted || ReviewNote != ReviewhdnNote) {

            $.ajax({
                type: "POST",
                async: false,
                url: "ProcessReportReview.aspx?IsDeleted=" + IsDeleted + "&NodeToEdit=" + iRecord + "&OrderDetailId=" + pkOrderDetailId + "&ProductCode=" + ProductCode + "&pkOrderResponseId=" + pkOrderResponseId + "&Is6MonthOld=" + hdnIs6MonthOld + "",
                data: { CurrentReviewNote: ReviewNote },
                success: function (responseText) {
                    iRecord = iRecord + 1;
                    ValidateReportForNextCall();
                },
                complete: function (xmlRequestObject, successString) {
                    // alert(xmlRequestObject+ '  '+successString);
                },
                error: function (responseText) {
                    iRecord = iRecord + 1;
                    ValidateReportForNextCall();
                }
            });
        }
        else {
            iRecord = iRecord + 1;
            ValidateReportForNextCall();
        }
    }
    else if (iCount >= TotalReportsInOrder) {
        //document.getElementById('ctl00_UpdateProgress2').style.display = 'none';
        window.location.href = "AdminSearch.aspx";
        //if (location.href.indexOf('Admin') > 0)
        //    window.location.href = "AdminSearch.aspx";
        // else
        //     window.location.href = "Admin/AdminSearch.aspx";
    }
}
function delay() {
    window.location.href = "AdminSearch.aspx";
    // if (location.href.indexOf('Admin') > 0)
    // { window.location.href = "AdminSearch.aspx"; }
    //  else
    // { window.location.href = "Admin/AdminSearch.aspx"; }
}

function ReportContainText(ProductCode, pkOrderDetailId, pkOrderResponseId) {
    if (document.getElementById("txt_" + pkOrderResponseId) != null) {
        ReviewNote = document.getElementById("txt_" + pkOrderResponseId).value;
        hdnIs6MonthOld = document.getElementById("ctl00_ContentPlaceHolder1_ViewReportReview_hdnIs6MonthOld").value;
        //alert(ProductCode + ' = ' + pkOrderResponseId + ' - ' + ReviewNote);
        // window.location("ProcessReportReview.aspx?IsDeleted=0&NodeToEdit=0&OrderDetailId=" + pkOrderDetailId + "&ProductCode=" + ProductCode + "&pkOrderResponseId=" + pkOrderResponseId + "&Is6MonthOld=" + hdnIs6MonthOld + "");
        $.ajax({
            type: "POST",
            async: false,
            url: "ProcessReportReview.aspx?IsDeleted=0&NodeToEdit=0&OrderDetailId=" + pkOrderDetailId + "&ProductCode=" + ProductCode + "&pkOrderResponseId=" + pkOrderResponseId + "&Is6MonthOld=" + hdnIs6MonthOld + "",
            data: { CurrentReviewNote: ReviewNote },
            success: function (responseText) {
            }
        });
    }

}

function SaveReviewChanges4TDR(ProductCode, pkOrderDetailId, pkOrderResponseId) {
    hdnIs6MonthOld = document.getElementById("ctl00_ContentPlaceHolder1_ViewReportReview_hdnIs6MonthOld").value;
    Records_IAI = document.getElementById("txtTotalRecord_" + pkOrderDetailId + '_IAI').value;
    Records_IA = document.getElementById("txtTotalRecord_" + pkOrderDetailId + '_IA').value;
    Records_NEIGH = document.getElementById("txtTotalRecord_" + pkOrderDetailId + '_NEIGH').value;
    for (iRecord = 1; iRecord <= Records_IAI; iRecord++) {
        var ElementName = "IAI";
        IsDeleted = $("#chkIsDeletedTDR" + "_" + iRecord + "_" + ElementName + ":checked").val();
        IshdnDeleted = $("#chkhdnIsDeletedTDR" + "_" + iRecord + "_" + ElementName + ":checked").val();
        ReviewNote = document.getElementById("txtNoteTDR_" + iRecord + "_" + ElementName).value;
        ReviewhdnNote = document.getElementById("txthdnNoteTDR_" + iRecord + "_" + ElementName).value;
        if (IsDeleted != IshdnDeleted || ReviewNote != ReviewhdnNote) {
            $.ajax({
                type: "POST",
                async: false,
                url: "ProcessReportReview.aspx?IsDeleted=" + IsDeleted + "&NodeToEdit=" + iRecord + "&OrderDetailId=" + pkOrderDetailId + "&ProductCode=" + ProductCode + "&pkOrderResponseId=" + pkOrderResponseId + "&ElementName=" + ElementName + "&Is6MonthOld=" + hdnIs6MonthOld + "",
                data: { CurrentReviewNote: ReviewNote },
                success: function (responseText) { }
            });
        }
    }
    for (iRecord = 1; iRecord <= Records_IA; iRecord++) {
        var ElementName = "IA";
        IsDeleted = $("#chkIsDeletedTDR" + "_" + iRecord + "_" + ElementName + ":checked").val();
        IshdnDeleted = $("#chkhdnIsDeletedTDR" + "_" + iRecord + "_" + ElementName + ":checked").val();
        ReviewNote = document.getElementById("txtNoteTDR_" + iRecord + "_" + ElementName).value;
        ReviewhdnNote = document.getElementById("txthdnNoteTDR_" + iRecord + "_" + ElementName).value;
        if (IsDeleted != IshdnDeleted || ReviewNote != ReviewhdnNote) {
            $.ajax({
                type: "POST",
                async: false,
                url: "ProcessReportReview.aspx?IsDeleted=" + IsDeleted + "&NodeToEdit=" + iRecord + "&OrderDetailId=" + pkOrderDetailId + "&ProductCode=" + ProductCode + "&pkOrderResponseId=" + pkOrderResponseId + "&ElementName=" + ElementName + "&Is6MonthOld=" + hdnIs6MonthOld + "",
                data: { CurrentReviewNote: ReviewNote },
                success: function (responseText) { }
            });
        }
    }
    for (iRecord = 1; iRecord <= Records_NEIGH; iRecord++) {
        var ElementName = "NEIGH";
        IsDeleted = $("#chkIsDeletedTDR" + "_" + iRecord + "_" + ElementName + ":checked").val();
        IshdnDeleted = $("#chkhdnIsDeletedTDR" + "_" + iRecord + "_" + ElementName + ":checked").val();
        ReviewNote = document.getElementById("txtNoteTDR_" + iRecord + "_" + ElementName).value;
        ReviewhdnNote = document.getElementById("txthdnNoteTDR_" + iRecord + "_" + ElementName).value;

        if (IsDeleted != IshdnDeleted || ReviewNote != ReviewhdnNote) {
            $.ajax({
                type: "POST",
                async: false,
                url: "ProcessReportReview.aspx?IsDeleted=" + IsDeleted + "&NodeToEdit=" + iRecord + "&OrderDetailId=" + pkOrderDetailId + "&ProductCode=" + ProductCode + "&pkOrderResponseId=" + pkOrderResponseId + "&ElementName=" + ElementName + "&Is6MonthOld=" + hdnIs6MonthOld + "",
                data: { CurrentReviewNote: ReviewNote },
                success: function (responseText) { }
            });
        }
    }
}