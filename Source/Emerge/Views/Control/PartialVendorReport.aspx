﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" AutoEventWireup="true" %>

<%----%>
<%@ Register TagPrefix="telerik" Assembly="Telerik.ReportViewer.WebForms" Namespace="Telerik.ReportViewer.WebForms" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Emerge.Services" %>
<%@ Import Namespace="Emerge.Data" %>

<%@ Register Assembly="Telerik.ReportViewer.WebForms, Version=8.2.14.1204, Culture=neutral, PublicKeyToken=a9d7983dfcc261be" Namespace="Telerik.ReportViewer.WebForms" TagPrefix="telerik" %>
<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <meta name="viewport" content="width=device-width" />
    <title></title>
</head>
<body>
    <div style="width: 100%; text-align: center;">
        <script runat="server">
        
            public override void VerifyRenderingInServerForm(Control control)
            {
                // to avoid the server form (<form runat="server">) requirement
            }

            protected override void OnLoad(EventArgs e)
            {
                base.OnLoad(e);
                string ApplicationId = (ViewData["appId"] != null) ? ViewData["appId"].ToString() : Guid.Empty.ToString();
                string Month = (ViewData["Month"] != null) ? ViewData["Month"].ToString() : DateTime.Now.Month.ToString();
                string Year = (ViewData["Year"] != null) ? ViewData["Year"].ToString() : DateTime.Now.Year.ToString();
                string VendorId = (ViewData["VendorId"] != null) ? ViewData["VendorId"].ToString() : "0";
                string ReportType = (ViewData["ReportType"] != null) ? ViewData["ReportType"].ToString() : "0";

                rptVendorReport.ReportSource = new EmergeBilling.VendorReport();
                Databind(ApplicationId, Month, Year, VendorId, ReportType);
            }

            public void Databind(string ApplicationId, string Month, string Year, string VendorId, string ReportType)
            {
                //Month = "4";
                //Year = "2012";
                //CompanyId = "00000000-0000-0000-0000-000000000000";
                EmergeBilling.VendorReport ObjMain = new EmergeBilling.VendorReport();
                Telerik.Reporting.Report rptMain = new EmergeBilling.VendorReport();
                EmergeBilling.VendorReport.Month = Month;
                EmergeBilling.VendorReport.Year = Year;

                //try
                //{
                    BALBillingData ObjBALBillingData = new BALBillingData();
                    Tuple<List<clsVendorReport>, List<clsDuplicateTransactions>> collection = ObjBALBillingData.GetVendorReportData(new Guid(ApplicationId), int.Parse(Month), int.Parse(Year), byte.Parse(VendorId), 0);
                
                   ObjMain.ObjData = collection;
                   rptVendorReport.ReportSource = ObjMain;
                //}
                //catch { }
            }
        
        </script>
        <telerik:ReportViewer runat="server" ID="rptVendorReport" Style="width: 100%; height: 500px;" />
        <br />
    </div>
</body>
</html>
