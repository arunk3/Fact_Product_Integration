﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" AutoEventWireup="true" %>

<%----%>
<%@ Register TagPrefix="telerik" Assembly="Telerik.ReportViewer.WebForms" Namespace="Telerik.ReportViewer.WebForms" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Emerge.Services" %>
<%@ Import Namespace="Emerge.Data" %>

<%@ Register Assembly="Telerik.ReportViewer.WebForms, Version=8.2.14.1204, Culture=neutral, PublicKeyToken=a9d7983dfcc261be" Namespace="Telerik.ReportViewer.WebForms" TagPrefix="telerik" %>
<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <meta name="viewport" content="width=device-width" />
    <title></title>
</head>
<body>
    <div style="width: 100%; text-align: center;">
        <script runat="server">
        
            public override void VerifyRenderingInServerForm(Control control)
            {
                // to avoid the server form (<form runat="server">) requirement
            }

            protected override void OnLoad(EventArgs e)
            {
                base.OnLoad(e);
                string ApplicationId = (ViewData["appId"] != null) ? ViewData["appId"].ToString() : Guid.Empty.ToString();
                string Month = (ViewData["Month"] != null) ? ViewData["Month"].ToString() : DateTime.Now.Month.ToString();
                string Year = (ViewData["Year"] != null) ? ViewData["Year"].ToString() : DateTime.Now.Year.ToString();
                int CompanyId = Convert.ToInt32(ViewData["CompanyId"]) != 0 ? Convert.ToInt32(ViewData["CompanyId"]) : 0;
                int LocationId = Convert.ToInt32(ViewData["LocationId"])!= 0 ? Convert.ToInt32(ViewData["LocationId"]) : 0;
                int UserId = Convert.ToInt32(ViewData["UserId"]) != 0 ? Convert.ToInt32(ViewData["UserId"]) : 0;
                string SalesRep = (ViewData["SalesRep"] != null) ? ViewData["SalesRep"].ToString() : Guid.Empty.ToString();
                string RefCode = (ViewData["RefCode"] != null) ? ViewData["RefCode"].ToString() : "0";
                if (LocationId == 0)
                {
                    LocationId = 0;
                }

                rptBillingReport.ReportSource = new EmergeBilling.BillingReport();
                Databind(ApplicationId, Month, Year, CompanyId, LocationId, UserId, SalesRep, RefCode);
            }

            public void Databind(string ApplicationId, string Month, string Year, int CompanyId = 0, int LocationId = 0, int UserId = 0, string SalesRep = "00000000-0000-0000-0000-000000000000", string RefCode="0")
            {
                //Month = "4";
                //Year = "2012";
                //CompanyId = "00000000-0000-0000-0000-000000000000";
                EmergeBilling.BillingReport ObjMain = new EmergeBilling.BillingReport();
                Telerik.Reporting.Report rptMain = new EmergeBilling.BillingReport();
                EmergeBilling.BillingReport.Month = Month;
                EmergeBilling.BillingReport.Year = Year;

                try
                {
                    BALBillingData ObjBALBillingData = new BALBillingData();
                    Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALProductsPerLocation>, List<List<BALReportsByLocation>>, List<proc_GetBillingAdjustmentsResult>, Totals, Totals> collection = ObjBALBillingData.GetBillingForAdmin(ApplicationId, Month, Year, CompanyId, LocationId, UserId, new Guid(SalesRep), int.Parse(RefCode));
                    Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALReportsByLocation>, List<proc_GetBillingAdjustmentsResult>> collectionApi = ObjBALBillingData.GetBillingForAdminForApi(ApplicationId, Month, Year, CompanyId, LocationId, UserId, new Guid(SalesRep), int.Parse(RefCode));

                    ObjMain.ObjData = collection;
                    ObjMain.ObjDataApi = collectionApi;
                    ObjMain.CompanyId = CompanyId;
                    rptBillingReport.ReportSource = ObjMain;

/*
                    if (LocationId > 0 || UserId > 0 || SalesRep != Guid.Empty.ToString() || RefCode != "0")
                    {
                        BALBillingData ObjBALBillingData = new BALBillingData();
                        Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALProductsPerLocation>, List<List<BALReportsByLocation>>, List<proc_GetBillingAdjustmentsResult>, Totals, Totals> collection = ObjBALBillingData.GetBillingForAdmin(ApplicationId, Month, Year, CompanyId, LocationId, UserId, new Guid(SalesRep), int.Parse(RefCode));
                        Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALReportsByLocation>, List<proc_GetBillingAdjustmentsResult>> collectionApi = ObjBALBillingData.GetBillingForAdminForApi(ApplicationId, Month, Year, CompanyId, LocationId, UserId, new Guid(SalesRep), int.Parse(RefCode));

                        ObjMain.ObjData = collection;
                        ObjMain.ObjDataApi = collectionApi;
                        ObjMain.CompanyId = CompanyId;
                        rptBillingReport.ReportSource = ObjMain;
                    }
                    else
                    {
                        
                        BALBillingData ObjBALBillingData = new BALBillingData();
                        Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALProductsPerLocation>, List<List<BALReportsByLocation>>, List<proc_GetBillingAdjustmentsResult>, Totals, Totals> collection = ObjBALBillingData.GetBillingReportData(ApplicationId, Month, Year, CompanyId, LocationId, UserId, new Guid(SalesRep), int.Parse(RefCode));
                        Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALReportsByLocation>, List<proc_GetBillingAdjustmentsResult>> collectionApi = ObjBALBillingData.GetBillingReportData_API(ApplicationId, Month, Year, CompanyId, LocationId, UserId, new Guid(SalesRep), int.Parse(RefCode));
                        ObjMain.ObjData = collection;
                        ObjMain.ObjDataApi = collectionApi;
                        ObjMain.CompanyId = CompanyId;
                        rptBillingReport.ReportSource = ObjMain;
                    }
                */
                    
                    
                }
                catch (Exception ex)
                {
                    string FilePath = System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\PSPVendorCall\BillingError.txt");
                           if (!System.IO.File.Exists(FilePath))
                           {
                               System.IO.TextWriter tw = System.IO.File.CreateText(FilePath);
                               tw.WriteLine("Catch Exception @" + DateTime.Now.ToString());
                               tw.WriteLine("-----------------------------------");
                               tw.WriteLine("Exception Message \n -------------------");
                               tw.WriteLine(ex.Message.ToString());
                               tw.WriteLine("\n\n\n\n");
                               tw.WriteLine("Exception  \n -------------------");
                               tw.WriteLine(ex.ToString());
                               tw.Close();
                           }
                           else
                           {
                               System.IO.StreamWriter sw = System.IO.File.AppendText(FilePath);
                               StringBuilder sb = new StringBuilder();
                               sb.Append("Catch Exception @" + DateTime.Now.ToString());
                               sb.Append("-----------------------------------");
                               sb.Append("Exception Message \n -------------------");
                               sb.Append(ex.Message.ToString());
                               sb.Append("\n\n\n\n");
                               sb.Append("Exception  \n -------------------");
                               sb.Append(ex.ToString());
                               sw.WriteLine(sb);
                               sw.Close();
                           }
                }
            }
        
        </script>
        <telerik:ReportViewer runat="server" ID="rptBillingReport" Style="width: 100%; height: 500px;text-align:center;" />
        <br />
    </div>
</body>
</html>
