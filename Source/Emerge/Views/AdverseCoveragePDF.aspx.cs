﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Emerge
{
    public partial class AdverseCoveragePDF : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["PDF"] != null)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["PDF"].ToString()))
                {
                    ShowPDF(Request.QueryString["PDF"].ToString(), true);
                  
                }
            }
            if (Request.QueryString["Adv"] != null)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["Adv"].ToString()))
                {
                    ShowPDF(Request.QueryString["Adv"].ToString(), false);
                
                }
            }
        }
        public void ShowPDF(string path, bool diff)
        {
            idPDFViewer.Visible = true;
            string FilePath = "";
            string FileName = path;
            if (diff)
            {
                LblHeader.Text = "Coverage PDF";
                Page.Title = "Coverage PDF";
                FilePath = "Resources/Upload/CoveragePDF/" + FileName;
            }
            else
            {
                Page.Title = "Adverse Letter";
                LblHeader.Text = "Adverse Letter";
                FilePath = "Resources/Upload/AdversePDF/" + FileName;
            }
            idPDFViewer.FilePath = FilePath;
        }
    }
    

}