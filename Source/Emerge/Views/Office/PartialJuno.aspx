﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Register src="~/Views/Office/PartialJunoUpload.ascx" TagName="CtrlJuno" TagPrefix="JN" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>PartialJuno</title>
</head>
<body>
    <div>
           <script runat="server">
        
            public override void VerifyRenderingInServerForm(Control control)
            {
                // to avoid the server form (<form runat="server">) requirement
            }

            protected override void OnLoad(EventArgs e)
            {
                base.OnLoad(e);
              
            }
            </script>
    </div>
    <form id="form1" runat="server">
    <div>
                <JN:CtrlJuno ID="CtrlJuno1" runat="server" />
    </div>
    </form>
</body>
</html>
