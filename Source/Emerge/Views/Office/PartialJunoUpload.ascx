﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Emerge.Services" %>
<%@ Import Namespace="Emerge.Data" %>
<%@ Import Namespace="Emerge.Common" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.OleDb" %>
<%@ Import Namespace="AjaxControlToolkit" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Xml.Serialization" %>
<%@ Import Namespace="System.Text.RegularExpressions" %>
<%@ Import Namespace="LumenWorks.Framework.IO.Csv" %>
<script type="text/javascript">
    function ShowJuno() {
        var popErrorLog = $find('behaveJunoup'); if (popErrorLog == null) return; popErrorLog.show(); return false;
    }
    function CloseWindow() { var popErrorLog = $find('behaveJunoup'); if (popErrorLog == null) return; popErrorLog.hide(); }
    function uploadError(sender, args) {
    }
    function uploadComplete(sender, args) {
        try {
            var filename = args.get_fileName();

            var ext = args.get_fileName().substring(filename.lastIndexOf(".") + 1);

            if (ext == 'csv' || ext == 'xls' || ext == 'xlsx') {
                $get("<%=btnShowHeader.ClientID%>").style.display = 'block';
                $get("<%=lblMessage_juno.ClientID%>").style.display = 'block';
                $get("<%=lblMessage_juno.ClientID%>").innerHTML = "File Uploaded Successfully";
                $get("<%=lblMessage_juno.ClientID%>").style.color = 'Green';
                document.getElementById('<%=hfUploadJunoFileType.ClientID%>').value = '.' + ext;
                document.getElementById('<%=hdnJunoFileFullPath.ClientID%>').value = filename;
            }
            else {
                $get("<%=btnShowHeader.ClientID%>").style.display = 'none';
                $get("<%=lblMessage_juno.ClientID%>").style.display = 'block';
                $get("<%=lblMessage_juno.ClientID%>").innerHTML = "Only .csv or .xls or .xlsx files are allowed to upload";
                $get("<%=lblMessage_juno.ClientID%>").style.color = 'red';
            }
        }

        catch (e) {
            alert(e.message);
        }
    }
    function headerChoose(selectVal, ID) {//alert(selectVal);
        var hdn = document.getElementById('hidHeaderChoose');
        var ddl = document.getElementById(ID);
        var isflag = true;
        // alert(hdn.value);
        if (selectVal != -1) {

            var ids = hdn.value.split('#');
            if (ids.length > 0) {
                for (j = 0; j < ids.length; j++) {
                    //alert(ids[j]);
                    var prevDdl = document.getElementById(ids[j]);
                    if (prevDdl != null) {
                        if (ids[j] != ID) {
                            var strUser = prevDdl.options[prevDdl.selectedIndex].value;
                            if (strUser == selectVal) {
                                alert('already selected');
                                ddl.selectedIndex = 0;
                                var SelectedProducts = hdn.value;
                                document.getElementById(ID).value = SelectedProducts.replace('#' + ID, "");
                                isflag = false;
                            }
                        }
                    }
                }
            }
            if (isflag) {
                if (hdn.value.indexOf(ID) == -1) {
                    hdn.value = hdn.value + '#' + ID;
                }
            }
            // else {
            //     ddl.selectedIndex = 0;
            //     alert('already selected');                    
            //   }
        }
        else {
            if (hdn.value.indexOf(ID) > 0) {
                var SelectedProducts = hdn.value;
                document.getElementById(ID).value = SelectedProducts.replace('#' + ID, "");
            }
        }
    }
    </script>

    <script runat="server">

        # region juno

        #region juno upload
        //public override void VerifyRenderingInServerForm(Control control)
        //{
        //    // to avoid the server form (<form runat="server">) requirement
        //}

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            BindCompanyType();
            //ViewState["vwFilePath"] = string.Empty;
            //ViewState["DeleteFilePath"] = string.Empty;
            //ViewState["ExcelConnectionString"] = string.Empty;
            //ViewState["FullPath"] = string.Empty;
            //ViewState["NoHeaders"] = string.Empty;
            //Session["DtCSVHeaders"] = string.Empty;
            //Session["DtExcelHeaders"] = string.Empty;
            //ViewState["StoreData"] = string.Empty;
            //ViewState["Invalid"] = string.Empty;
        }

        public void BindCompanyType()
        {
            BALCompanyType ObjBALCompanyType = new BALCompanyType();
            try
            {
                var CompanyTypes = ObjBALCompanyType.GetAllCompanyType();
                if (CompanyTypes.Count != 0)
                {
                    ddlcompanyType.DataSource = CompanyTypes;
                    ddlcompanyType.DataTextField = "CompanyType";
                    ddlcompanyType.DataValueField = "pkCompanyTypeId";
                    ddlcompanyType.DataBind();
                    ddlcompanyType.Items.Insert(0, new ListItem("Select", "-1"));
                    MPEJunoStep1.Show();
                    MultiViewJuno.ActiveViewIndex = 0;
                    lblStep.Text = "Step 1";
                    UpdJunoStep1.Update();
                    btnBack.Enabled = false;
                }
                else
                {
                    Emerge.Common.Utility.BindDropDownWithEmptyValue(ddlcompanyType, "Company Type");
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ObjBALCompanyType = null;
            }
        }

        protected void imgbtnUploadJuno_Click(object sender, ImageClickEventArgs e)
        {
            MPEJunoStep1.Show();
            MultiViewJuno.ActiveViewIndex = 0;
            lblStep.Text = "Step 1";
            UpdJunoStep1.Update();
            btnBack.Enabled = false;
        }

        DataTable DtFileData = new DataTable();
        DataColumn Dc1 = new DataColumn("Id", System.Type.GetType("System.Int32"));
        DataColumn Dc2 = new DataColumn("FileColumnName", System.Type.GetType("System.String"));

        private void AfterManageFile(string FilePath)
        {
            btnBack.Enabled = true;
            lblStep.Text = "Step 2) ";

            if (hfUploadJunoFileType.Value != ".csv")
            {
                trWorkBookSheets.Style.Add("display", "block");
                string xlsPath = FilePath;
                string xConnStr = string.Empty;
                if (hfUploadJunoFileType.Value == ".xls")
                {
                    xConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + xlsPath + ";" + "Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\"";
                }
                else if (hfUploadJunoFileType.Value == ".xlsx")
                {
                    xConnStr = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=" + xlsPath + ";" + "Extended Properties=Excel 12.0;";
                }

                ViewState["ExcelConnectionString"] = xConnStr;

                OleDbConnection objXConn = new OleDbConnection(xConnStr);
                objXConn.Open();
                System.Data.DataTable dt = null;
                dt = objXConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                if (dt.Rows.Count > 0)
                {
                    ddlExcelSheets.DataSource = dt;
                    ddlExcelSheets.DataTextField = "TABLE_NAME";
                    ddlExcelSheets.DataValueField = "TABLE_NAME";
                    ddlExcelSheets.DataBind();
                }
            }
            else
            {
                trWorkBookSheets.Style.Add("display", "none");
            }

        }
        private void ManageFile(string sFile)
        {
            string sFilepath = Server.MapPath(@"~\Resources/Upload/JunoFiles/") + Path.GetFileName(sFile);
            ViewState["DeleteFilePath"] = sFilepath;
            try
            {
                if (File.Exists(sFilepath))
                {
                    File.Delete(sFilepath);
                }
                afuUpload.PostedFile.SaveAs(sFilepath);
            }
            catch
            {
                lblMessage_juno.Text = "Some problem occurred while upload file.";
                lblMessage_juno.CssClass = "errormsg";
            }
            ViewState["vwFilePath"] = hdnJunoFileFullPath.Value = Path.GetFileName(sFile);
            ViewState["FullPath"] = sFilepath;
        }
        private bool CheckExtensions()
        {
            string Extension = string.Empty;
            bool Status = false;
            try
            {
                if (fpUploadJuno.FileName != string.Empty)
                {
                    Extension = Path.GetExtension(fpUploadJuno.PostedFile.FileName).ToLower();
                    if (fpUploadJuno.PostedFile.ContentLength > 0)
                    {
                        if ((Extension == ".csv") || (Extension == ".xls") || (Extension == ".xlsx"))
                        {
                            Status = true;
                            hfUploadJunoFileType.Value = Extension;

                        }
                        else
                        {
                            lblMessage_juno.Text = "Only .csv or .xls or .xlsx files are allowed to upload.";
                            lblMessage_juno.CssClass = "errormsg";
                            Status = false;
                        }
                    }
                }
            }
            catch
            {
            }
            return Status;
        }
        protected void GetCSVHeaders()
        {
            string sFilepath = Server.MapPath(@"~\Resources/Upload/JunoFiles/") + ViewState["vwFilePath"].ToString();
            StreamReader sr = new StreamReader(sFilepath);
            try
            {
                using (CsvReader ObjCSV = new CsvReader(sr, true, ','))
                {
                    string[] arrFields = ObjCSV.GetFieldHeaders();

                    DtFileData.Columns.Add(Dc1);
                    DtFileData.Columns.Add(Dc2);

                    for (int iRow = 0; iRow < arrFields.Count(); iRow++)
                    {
                        DataRow Dr = DtFileData.NewRow();
                        Dr[0] = int.Parse((iRow + 1).ToString());
                        Dr[1] = arrFields[iRow];
                        DtFileData.Rows.Add(Dr);
                    }
                    if (arrFields.Count() == 0)
                    {
                        ViewState["NoHeaders"] = "0";
                        DataRow Dr = DtFileData.NewRow();
                        Dr[0] = 1;
                        Dr[1] = "";
                        DtFileData.Rows.Add(Dr);
                    }
                    else { ViewState["NoHeaders"] = "1"; }
                    Session["DtCSVHeaders"] = DtFileData;
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                sr.Close();
            }
        }
        protected void GetExcelHeaders(string xConnStr)
        {

            OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
            objDataAdapter.SelectCommand = GetExcelFileInformation(xConnStr);
            DataSet objDataSet = new DataSet();
            objDataAdapter.Fill(objDataSet);

            DtFileData.Columns.Add(Dc1);
            DtFileData.Columns.Add(Dc2);

            for (int i = 0; i < objDataSet.Tables[0].Columns.Count; i++)
            {

                DataRow Dr = DtFileData.NewRow();
                Dr[0] = int.Parse((i + 1).ToString());

                if (objDataSet.Tables[0].Columns.Count == 1 && objDataSet.Tables[0].Columns[i].Caption.Trim() == "F1")
                {
                    Dr[1] = "";
                    ViewState["NoHeaders"] = "0";
                }
                else
                {
                    ViewState["NoHeaders"] = "1";
                    Dr[1] = objDataSet.Tables[0].Columns[i].Caption.Trim();
                }
                DtFileData.Rows.Add(Dr);
            }
            Session["DtExcelHeaders"] = DtFileData;
        }
        protected OleDbCommand GetExcelFileInformation(string xConnStr)
        {
            OleDbCommand objCommand = new OleDbCommand();
            try
            {
                if (xConnStr != string.Empty)
                {
                    OleDbConnection objXConn = new OleDbConnection(xConnStr);
                    objXConn.Open();
                    System.Data.DataTable dt = null;
                    dt = objXConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                    string SheetName = ddlExcelSheets.SelectedValue;

                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (dt.Rows[i]["TABLE_NAME"].ToString() == SheetName)
                            {
                                SheetName = "[" + SheetName + "]";
                                objCommand = new OleDbCommand("SELECT * FROM " + SheetName, objXConn);

                            }
                        }
                    }
                }
            }
            catch
            {
            }

            return objCommand;
        }
        protected void btnContinueJunoUploadStep2_Click(object sender, EventArgs e)
        {
            Step2();
        }

        public void Step2()
        {
            try
            {

                if (hfUploadJunoFileType.Value != ".csv")
                {
                    if (ViewState["ExcelConnectionString"] != null)
                    {
                        Session["DtExcelHeaders"] = null;
                        GetExcelHeaders(ViewState["ExcelConnectionString"].ToString());
                    }
                }
                else if (hfUploadJunoFileType.Value == ".csv")
                {
                    Session["DtCSVHeaders"] = null;

                }
                BindAllDropDownsWithEXCEL_CSVColumns();
                if (ViewState["NoHeaders"] != null)
                {
                    if (ViewState["NoHeaders"].ToString() == "0")
                    {
                        lblMessage_juno.Attributes.Add("display", "block"); //lblMessage_juno.Visible = true;
                        lblMessage_juno.Text = (chkIsJunoHeaders.Checked) ? "No Header found in uploaded file." : "No record found in uploaded file.";
                        lblMessage_juno.ForeColor = System.Drawing.Color.Red;
                        MPEJunoStep1.Show();
                        btnBack.Visible = true;
                    }
                    else
                    {
                        lblMessage_juno.Attributes.Add("display", "none"); //lblMessage_juno.Visible = false;
                        lblMessage_juno.Text = "";
                        lblStep.Text = "Step 3) Juno Mapping";
                        MultiViewJuno.ActiveViewIndex = 2;// 3;
                        MPEJunoStep1.Show();
                        imgbtnJunoValidate.Visible = true;
                    }
                }
                else
                {
                    MPEJunoStep1.Hide();
                }
                UpdJunoStep1.Update();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void BindAllDropDownsWithEXCEL_CSVColumns()
        {
            try
            {
                List<System.Web.UI.WebControls.DropDownList> allControls = new List<System.Web.UI.WebControls.DropDownList>();
                GetControlList<System.Web.UI.WebControls.DropDownList>(Page.Controls, allControls);
                foreach (var childControl in allControls)
                {
                    var ddl = childControl as System.Web.UI.WebControls.DropDownList;

                    if (ddl != null)
                    {
                        if (ddl.ID != "ddlExcelSheets" && ddl.ID != "ddlCompany" && ddl.ID != "ddlcompanyType" && ddl.ID != "ddlPageSize" && ddl.ID != "ddlPageSize1" && ddl.ID != "ddlPaging" && ddl.ID != "ddlPaging1")
                        {
                            BindDropDownsWithColumns(ddl);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }


        protected void imgbtnJunoValidate_Click(object sender, ImageClickEventArgs e)
        {
            Step3();
        }
        private void Step3()
        {
            bool isAllValid = false;
            try
            {
                DataTable dtPreviewData = new DataTable();
                List<TempJuno> tempJunoArray = new List<TempJuno>();
                tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlcompanyName", RegExp = "^[a-zA-Z' ']+$" });
                //tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlcompanyType", RegExp = "^[a-zA-Z' ']+$" });
                //tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlLeadSource", RegExp = "^[a-zA-Z' ']+$" });
                if (ddlZip.SelectedValue.ToString() != "-1")
                {
                    tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlZip", RegExp = @"^\d{5}$" });
                }
                tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlState", RegExp = "^[a-zA-Z' ']+$" });
                tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlCity", RegExp = "^[a-zA-Z' ']+$" });
                if (ddlMainContact.SelectedValue.ToString() != "-1")
                {
                    tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlMainContact", RegExp = "^[a-zA-Z' ']+$" });
                }
                if (ddlMainEmail.SelectedValue.ToString() != "-1")
                {
                    tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlMainEmail", RegExp = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" });
                }
                tempJunoArray.Add(new TempJuno { JunoRequiredFields = "ddlMainPhone", RegExp = @"((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}" });
                if (hfUploadJunoFileType.Value != ".csv")
                {
                    #region Excel Step 3


                    OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                    objDataAdapter.SelectCommand = GetExcelFileInformation(ViewState["ExcelConnectionString"].ToString());
                    DataSet objDataSet = new DataSet();
                    objDataAdapter.Fill(objDataSet);

                    List<string> lstExcelHeaders = new List<string>();
                    StringBuilder sbValidations = new StringBuilder();

                    for (int i = 0; i < objDataSet.Tables[0].Columns.Count; i++)
                    {
                        lstExcelHeaders.Add(objDataSet.Tables[0].Columns[i].Caption.Trim());
                        dtPreviewData.Columns.Add("Column" + i.ToString(), Type.GetType("System.String"));
                    }

                    if (chkIsJunoHeaders.Checked == false)
                    {
                        DataRow dr = dtPreviewData.NewRow();
                        for (int iRow = 0; iRow < dtPreviewData.Columns.Count; iRow++)
                        {
                            dr[iRow] = objDataSet.Tables[0].Columns[iRow].Caption.Trim();

                        }
                        dtPreviewData.Rows.Add(dr);
                    }

                    for (int total = 0; total < objDataSet.Tables[0].Rows.Count; total++)
                    {
                        DataRow dr = dtPreviewData.NewRow();

                        for (int iRow = 0; iRow < dtPreviewData.Columns.Count; iRow++)
                        {
                            dr[iRow] = objDataSet.Tables[0].Rows[total][lstExcelHeaders[iRow].ToString()];
                        }
                        dtPreviewData.Rows.Add(dr);
                    }

                    isAllValid = GetCSVPreviewAfter(dtPreviewData, tempJunoArray);
                    ViewState["StoreData"] = dtPreviewData;
                    #endregion Excel Step 3
                }
                else
                {
                    #region CSV Step 3
                    string sFilepath = Server.MapPath(@"~\Resources/Upload/JunoFiles/") + ViewState["vwFilePath"].ToString();
                    StreamReader sr = new StreamReader(sFilepath);
                    try
                    {
                        if (Session["DtCSVHeaders"] != null)
                        {
                            DtFileData = (DataTable)Session["DtCSVHeaders"];
                        }
                        else
                        {
                            GetCSVHeaders();
                        }
                        for (int i = 0; i < DtFileData.Rows.Count; i++)
                        {
                            dtPreviewData.Columns.Add("Column" + i.ToString(), Type.GetType("System.String"));

                        }
                        string[] arrFields;
                        bool IsAddHeaders = false;

                        using (CsvReader ObjCSV = new CsvReader(sr, true, ','))
                        {
                            arrFields = ObjCSV.GetFieldHeaders();

                            if (chkIsJunoHeaders.Checked == false && IsAddHeaders == false)
                            {
                                DataRow dr = dtPreviewData.NewRow();
                                for (int iRow = 0; iRow < arrFields.Count(); iRow++)
                                {
                                    dr[iRow] = arrFields[iRow].ToString();
                                }
                                dtPreviewData.Rows.Add(dr);
                            }

                            while (ObjCSV.ReadNextRecord())
                            {
                                IsAddHeaders = true;
                                DataRow dr = dtPreviewData.NewRow();
                                for (int iRow = 0; iRow < dtPreviewData.Columns.Count; iRow++)
                                {
                                    dr[iRow] = ObjCSV[iRow];
                                }
                                dtPreviewData.Rows.Add(dr);
                            }
                        }

                        isAllValid = GetCSVPreviewAfter(dtPreviewData, tempJunoArray);
                        ViewState["StoreData"] = dtPreviewData;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        sr.Close();
                    }
                    #endregion CSV Step 3
                }

                MultiViewJuno.ActiveViewIndex = 3;// 4;
                btnBack.Visible = false;
                if (!isAllValid)
                {
                    lblMessage_juno.Text = "Please correct Invalid Cell data and upload again.";
                    lblMessage_juno.ForeColor = System.Drawing.Color.Red;
                    lblMessage_juno.Attributes.Add("display", "block"); //lblMessage_juno.Visible = true;
                    ViewState["Invalid"] = false;
                    btnCsvToObjectAfter.Visible = false;
                    btnBack.Visible = true;
                }
                else if (dtPreviewData.Rows.Count == 0)
                {
                    lblMessage_juno.Text = "No Record Found in uploaded file.";
                    lblMessage_juno.ForeColor = System.Drawing.Color.Red;
                    lblMessage_juno.Attributes.Add("display", "block"); //lblMessage_juno.Visible = true;
                    btnBack.Visible = true;
                    btnCsvToObjectAfter.Visible = false;
                }
                else
                {
                    btnCsvToObjectAfter.Visible = true;
                    ViewState["Invalid"] = true;
                    lblMessage_juno.Text = "";
                    lblMessage_juno.Attributes.Add("display", "none"); //lblMessage_juno.Visible = false;
                }
                lblStep.Text = "Step 4) Preview";
                MPEJunoStep1.Show();
                UpdJunoStep1.Update();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected bool GetCSVPreviewAfter(DataTable dt, List<TempJuno> tempJunoArray)
        {
            StringBuilder sHTML, sInValidCell;
            sHTML = new StringBuilder();
            sInValidCell = new StringBuilder();
            sHTML.Append("");
            sInValidCell.Append("");
            BALGeneral ObjBALGeneral = new BALGeneral();
            List<tblState> tblStateColl = new List<tblState>();
            List<tblCity> tblCityColl = new List<tblCity>();
            List<tblCompanyType> tblCompanyTypeColl = new List<tblCompanyType>();
            List<tblLocation> tblLocationColl = new List<tblLocation>();
            List<tblCompany> tblCompanyColl = new List<tblCompany>();
            tblStateColl = ObjBALGeneral.GetStates();
            tblCityColl = ObjBALGeneral.GetCities();
            tblCompanyTypeColl = ObjBALGeneral.GetCompanyTypes();
            tblLocationColl = ObjBALGeneral.GetMainPhone();
            tblCompanyColl = ObjBALGeneral.GetMainEmailId();
            DataTable dtPreviewGrd = new DataTable();

            dtPreviewGrd.Columns.Add("column", Type.GetType("System.String"));
            dtPreviewGrd.Columns.Add("r1", Type.GetType("System.String"));
            dtPreviewGrd.Columns.Add("r2", Type.GetType("System.String"));
            dtPreviewGrd.Columns.Add("r3", Type.GetType("System.String"));
            dtPreviewGrd.Columns.Add("r4", Type.GetType("System.String"));
            dtPreviewGrd.Columns.Add("Valid", Type.GetType("System.String"));

            System.Web.UI.WebControls.DropDownList ddln = null;
            bool IsRowVlaid = true;
            bool DontBind = true;
            int iprev = -1, piRow = -1;
            try
            {
                for (int iRow = 0; iRow < dt.Rows.Count; iRow++)
                {
                    if (iRow == 0 && chkIsJunoHeaders.Checked)
                    {
                        //iRow++;
                    }
                    for (int iarr = 0; iarr < tempJunoArray.Count(); iarr++)
                    {
                        string ddlname = tempJunoArray.ElementAt(iarr).JunoRequiredFields.ToString();//ddl +
                        int d = 0;
                        string CellName = "";
                        ddln = tblMap.FindControl(ddlname) as System.Web.UI.WebControls.DropDownList;
                        if (ddln != null)
                        {
                            int.TryParse(ddln.SelectedValue, out d);
                            if (d > 0)
                                d--;

                            if (DontBind)
                            {
                                try
                                {
                                    IsRowVlaid = RowValidation(tempJunoArray.ElementAt(iarr).JunoRequiredFields.ToString(), dt.Rows[iRow][d].ToString(), tempJunoArray.ElementAt(iarr).RegExp, tblStateColl, tblCityColl, tblCompanyTypeColl, tblCompanyColl, tblLocationColl);
                                    if (!IsRowVlaid) { DontBind = false; CellName = GetCellAddress(iRow, d); }
                                }
                                catch (Exception ex) { throw ex; }
                            }
                            else
                            {
                                try
                                {
                                    IsRowVlaid = RowValidation(tempJunoArray.ElementAt(iarr).JunoRequiredFields.ToString(), dt.Rows[iRow][d].ToString(), tempJunoArray.ElementAt(iarr).RegExp, tblStateColl, tblCityColl, tblCompanyTypeColl, tblCompanyColl, tblLocationColl);
                                    if (!IsRowVlaid) { CellName = GetCellAddress(iRow, d); }
                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }

                            }
                            #region Logic for preview

                            Logic4Preview(dt, dtPreviewGrd, ddln, ref iprev, ref piRow, iRow, d, CellName);

                            #endregion
                        }
                    }
                }
                gvPreview.DataSource = dtPreviewGrd;
                gvPreview.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return DontBind;
        }

        private static void Logic4Preview(DataTable dt, DataTable dtPreviewGrd, System.Web.UI.WebControls.DropDownList ddln, ref int iprev, ref int piRow, int iRow, int d, string CellName)
        {
            if (iRow == 0)
            {
                DataRow dr = dtPreviewGrd.NewRow();
                if (ddln.ID.Contains("ddl"))
                    dr["column"] = (ddln.ID).Replace("ddl", "");
                else
                    dr["column"] = ddln.ID;
                dr["r1"] = dt.Rows[iRow][d].ToString();
                dr["r2"] = "";
                dr["r3"] = "";
                dr["r4"] = "";
                dr["Valid"] = (CellName != "") ? CellName + ", " : "";
                dtPreviewGrd.Rows.Add(dr);
            }
            else
            {
                if (piRow != iRow) { iprev = 0; } else { iprev++; }
                int iiRow = iRow + 1;
                if (iRow < 4)
                {
                    try
                    {
                        dtPreviewGrd.Rows[iprev][iiRow] = dt.Rows[iRow][d].ToString();
                        string sOldCols = dtPreviewGrd.Rows[iprev]["Valid"].ToString();
                        if (CellName != "")
                            dtPreviewGrd.Rows[iprev]["Valid"] = sOldCols + CellName + ", ";
                        if (iRow == 3) { dtPreviewGrd.Rows[iprev][iiRow] = "....."; }
                    }
                    catch { }
                }
                else
                {
                    string sOldCols = dtPreviewGrd.Rows[iprev]["Valid"].ToString();
                    if (CellName != "")
                        dtPreviewGrd.Rows[iprev]["Valid"] = sOldCols + CellName + ", ";
                }
                piRow = iRow;
            }
        }


        public void CurrentRowConvert2Object()
        {

        }

        public string GetCellAddress(int row, int col)
        {
            string[] CharArray = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            string strCellPosition = string.Empty;
            if (col <= 26)
            {
                if (chkIsJunoHeaders.Checked)
                    strCellPosition = CharArray[col].ToString() + (row + 2).ToString();
                else
                    strCellPosition = CharArray[col].ToString() + (row + 1).ToString();
            }
            else
            {
                strCellPosition = "Col: " + (col + 1).ToString() + "Row: " + (row + 1).ToString();
            }
            return strCellPosition;
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                UpdJunoStep1.Update();
                lblStep.Text = "Step : 1) Choose Juno file.";
                lblMessage_juno.Text = "";
                chkIsJunoHeaders.Checked = false;
                MultiViewJuno.ActiveViewIndex = 0;
                btnBack.Visible = false;
                MPEJunoStep1.Show();
                //string s = HttpContext.Current.Request.Path;

                //Response.Redirect(s.Substring(s.LastIndexOf("/") + 1) + "?_j=1");
            }
            catch
            {
            }
        }

        #endregion

        #region juno  Bind DropDowns
        protected void gvPreview_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label LblIsValid = e.Row.FindControl("LblIsValid") as Label;
                Image ImgIsValid = e.Row.FindControl("ImgIsValid") as Image;
                if (LblIsValid != null && ImgIsValid != null)
                {
                    if (LblIsValid.Text == "")
                    {
                        ImgIsValid.ImageUrl = "~/Content/themes/base/images/successmsg.png";

                    }
                    else
                    {
                        ImgIsValid.ImageUrl = "~/Content/themes/base/images/Incomplete.gif";
                        ImgIsValid.Attributes.Add("onmouseover", "javascript:tooltip('" + LblIsValid.Text.Trim() + "','InValid Cells');");
                        ImgIsValid.Attributes.Add("onmouseout", "javascript:exit();");

                    }
                }
            }
        }
        protected void BindDropDownsWithColumns(System.Web.UI.WebControls.DropDownList ddl)
        {
            try
            {
                if (hfUploadJunoFileType.Value != ".csv")
                {
                    if (Session["DtExcelHeaders"] != null)
                    {
                        DtFileData = (DataTable)Session["DtExcelHeaders"];
                    }
                    else
                    {
                        if (ViewState["ExcelConnectionString"] != null)
                            GetExcelHeaders(ViewState["ExcelConnectionString"].ToString());
                    }
                }
                else if (hfUploadJunoFileType.Value == ".csv")
                {
                    if (Session["DtCSVHeaders"] != null)
                    {
                        DtFileData = (DataTable)Session["DtCSVHeaders"];
                    }
                    else
                    {
                        GetCSVHeaders();
                    }
                }
                if (DtFileData.Rows.Count == 1 && (DtFileData.Columns[1].ToString() == "" || DtFileData.Rows[0]["FileColumnName"] == ""))
                {
                    ddl.Items.Insert(0, new ListItem("--", "-1"));
                    ViewState["NoHeaders"] = "0";
                }
                else if (DtFileData.Rows.Count > 0)
                {
                    ddl.DataSource = DtFileData;
                    ddl.DataValueField = DtFileData.Columns[0].ToString();
                    ddl.DataTextField = DtFileData.Columns[1].ToString();
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem("--", "-1"));
                    ddl.Attributes.Add("onchange", "headerChoose(this.value, this.id);");
                }
                else
                {
                    ddl.Items.Insert(0, new ListItem("--", "-1"));
                }
            }
            catch (Exception ex)
            { throw ex; }

        }
        public void getctrl()
        {
            List<System.Web.UI.WebControls.DropDownList> allControls = new List<System.Web.UI.WebControls.DropDownList>();
            GetControlList<System.Web.UI.WebControls.DropDownList>(Page.Controls, allControls);
            foreach (var childControl in allControls)
            {
                var ddl = childControl as System.Web.UI.WebControls.DropDownList;
                if (ddl.Items.Count == 0)
                    if (ddl != null)
                    {
                        ddl.Items.Insert(0, new ListItem("--", "-1"));

                    }
            }
        }
        public void getctrl(bool Reset)
        {
            List<System.Web.UI.WebControls.DropDownList> allControls = new List<System.Web.UI.WebControls.DropDownList>();
            GetControlList<System.Web.UI.WebControls.DropDownList>(Page.Controls, allControls);
            foreach (var childControl in allControls)
            {
                var ddl = childControl as System.Web.UI.WebControls.DropDownList;


                if (ddl != null)
                {
                    if (Reset)
                        ddl.Items.Clear();
                    ddl.Items.Insert(0, new ListItem("--", "-1"));

                }
            }
        }
        private void GetControlList<T>(ControlCollection controlCollection, List<T> resultCollection) where T : Control
        {
            foreach (Control control in controlCollection)
            {
                if (control is T) // This is cleaner
                    resultCollection.Add((T)control);
                if (control.HasControls())
                    GetControlList(control.Controls, resultCollection);
            }
        }
        protected void BindDropDownInitials()
        {
            getctrl();
        }
        protected void BindDDLGrid(System.Web.UI.WebControls.DropDownList ddl)
        {
            if (ddl != null)
            {
                ddl.Items.Insert(0, new ListItem("--", "-1"));
            }
        }
        protected void btnShowHeader_Click(object sender, EventArgs e)
        {

            if (hdnJunoFileFullPath.Value.ToString() != "")
            {
                ViewState["vwFilePath"] = GetUserId() + hdnJunoFileFullPath.Value.ToString();
                string sFilePath = Server.MapPath(@"~/Resources/Upload/JunoFiles/" + GetUserId() + hdnJunoFileFullPath.Value.ToString());
                AfterManageFile(sFilePath);
            }
            else
            {
                string sFilePath = Server.MapPath(@"~/Resources/Upload/JunoFiles/" + ViewState["vwFilePath"].ToString());
                AfterManageFile(sFilePath);
            }
            MultiViewJuno.ActiveViewIndex++;
            MPEJunoStep1.Show();
        }

        #endregion

        #region Asynchronous File upload

        protected void BtnSampleFile_Click(object sender, EventArgs e)
        {
            string filepath1 = Request.PhysicalApplicationPath + "Resources/Upload/JunoFiles/SampleFile.csv";
            DownloadAnyFile(filepath1, "SampleFile.csv");
        }

        public void DownloadAnyFile(string FilePath, string DisplayName)
        {
            if (System.IO.File.Exists(FilePath))
            {
                string fileext = System.IO.Path.GetExtension(FilePath);
                HttpContext.Current.Response.ContentType = "text/csv";
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + DisplayName);
                HttpContext.Current.Response.TransmitFile(FilePath);
                HttpContext.Current.Response.End();
                HttpContext.Current.Response.Close();

            }
        }

        protected void afuUpload_UploadedFileError(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
        }
        public string GetUserId()
        {
            MembershipUser ObjMembershipUser = Membership.GetUser(Page.User.Identity.Name);
            return (ObjMembershipUser.ProviderUserKey.ToString());
        }

        protected void ProcessUpload(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {

            string fp, sp, fn, fx;
            try
            {
                if (afuUpload.HasFile)
                {
                    fp = afuUpload.FileName;
                    sp = Server.MapPath(fp);
                    fn = Path.GetFileName(sp);
                    fx = Path.GetExtension(fn);

                    string sFile = GetUserId() + afuUpload.FileName;//

                    string FilePath1 = Request.PhysicalApplicationPath + "Resources/Upload/JunoFiles/" + sFile;
                    string FilePath = Server.MapPath(@"~/Resources/Upload/JunoFiles/") + sFile;
                    if (CheckExtensions(afuUpload))
                    {
                        afuUpload.SaveAs(FilePath);
                        ViewState["FilePath"] = FilePath.ToString();
                        ManageFile(sFile);
                    }
                    else
                    {
                        e.state = AjaxControlToolkit.AsyncFileUploadState.Failed;
                        e.statusMessage = "Only .csv or .xls or .xlsx files are allowed to upload.";

                    }
                }
            }
            catch { }

        }
        private bool CheckExtensions(AsyncFileUpload Afu)
        {
            string Extension = string.Empty;
            bool Status = false;
            try
            {
                if (Afu.FileName != string.Empty)
                {
                    Extension = Path.GetExtension(Afu.PostedFile.FileName).ToLower();
                    if (Afu.PostedFile.ContentLength > 0)
                    {
                        if ((Extension == ".csv") || (Extension == ".xls") || (Extension == ".xlsx"))
                        {
                            Status = true;
                            hfUploadJunoFileType.Value = Extension;
                        }
                        else
                        {
                            Status = false;
                        }
                    }
                }
            }
            catch
            {
            }
            return Status;
        }
        protected void btnCsvToObjectAfter_Click(object sender, EventArgs e)
        {
            if (ViewState["Invalid"] != null)
            {
                if (Convert.ToBoolean(ViewState["Invalid"].ToString()) == false)
                {
                    MPEJunoStep1.Hide();
                    MultiViewJuno.ActiveViewIndex = 0;
                    lblMessage_juno.Text = "";
                    UpdJunoStep1.Update();
                    getctrl(true);
                }
                else
                {
                    # region add junos in DB
                    DataTable DtFinal = null;
                    if (ViewState["StoreData"] != null)
                    {
                        DtFinal = (DataTable)ViewState["StoreData"];
                        int Success = InsertCSVDataToDB();
                        if (Success != DtFinal.Rows.Count)
                        {
                            lblMessage_juno.Text = "Some error occurred while saving data.";
                            lblMessage_juno.ForeColor = System.Drawing.Color.Red;
                            lblMessage_juno.Attributes.Add("display", "block"); //lblMessage_juno.Visible = true;
                            btnBack.Visible = true;
                            btnCsvToObjectAfter.Visible = false;
                        }
                        else if (Success != DtFinal.Rows.Count)
                        {
                            lblMessage_juno.Text = "Only " + Success + " Juno Prospect(s) Saved Successfully.";
                            lblMessage_juno.ForeColor = System.Drawing.Color.Red;
                            lblMessage_juno.Attributes.Add("display", "block"); //lblMessage_juno.Visible = true;
                            btnBack.Visible = true;
                            btnCsvToObjectAfter.Visible = false;
                        }
                        else
                        {
                            MultiViewJuno.ActiveViewIndex++;
                        }
                        UpdJunoStep1.Update();
                        MPEJunoStep1.Show();
                    }
                    # endregion
                }
            }
        }

        private int InsertCSVDataToDB()
        {
            #region Declaring variables
            DataTable DtFinal = null;
            string CompanyType = string.Empty;
            string StateName = string.Empty;
            #endregion
            if (ViewState["StoreData"] != null)
            {
                DtFinal = (DataTable)ViewState["StoreData"];
                BALJuno ObjBALJuno = new BALJuno();

                BALGeneral ObjBALGeneral = new BALGeneral();
                tblCompany ObjtblCompany = new tblCompany();
                tblLocation ObjtblLocation = new tblLocation();
                List<tblState> StateColl = new List<tblState>();
                List<tblCompanyType> CompanyTypeColl = new List<tblCompanyType>();
                StateColl = ObjBALGeneral.GetStates();
                CompanyTypeColl = ObjBALGeneral.GetCompanyTypes();
                Guid UserId = Guid.Empty;
                MembershipUser ObjMembershipUser = Membership.GetUser(Page.User.Identity.Name);
                if (ObjMembershipUser != null)
                {
                    UserId = new Guid(ObjMembershipUser.ProviderUserKey.ToString());
                }
                ObjtblCompany.fkCompanyTypeId = Convert.ToByte(ddlcompanyType.SelectedValue);
                ObjtblCompany.LeadSource = TxtLeadSource.Text.Trim();
                ObjtblCompany.fkApplicationId = Emerge.Common.Utility.SiteApplicationId;
                ObjtblCompany.CreatedById = UserId;
                ObjtblCompany.LastModifiedById = UserId;
                ObjtblCompany.LeadStatus = 11;// 11 - New Prospect status for juno. 
                ObjtblLocation.CreatedById = UserId;
                for (int iCount = 0; iCount < DtFinal.Rows.Count; iCount++)
                {
                    if (DtFinal.Columns.Contains("Column" + (Convert.ToInt32(ddlcompanyName.SelectedValue) - 1).ToString()))
                    {
                        ObjtblCompany.CompanyName = DtFinal.Rows[iCount]["Column" + (Convert.ToInt32(ddlcompanyName.SelectedValue) - 1).ToString()].ToString().Trim();
                        ObjtblCompany.CompanyCode = Emerge.Common.Utility.GenerateCompanyCode(ObjtblCompany.CompanyName, DateTime.Now);
                        ObjtblLocation.LocationCode = Emerge.Common.Utility.GenerateLocationCode(ObjtblCompany.CompanyName, DateTime.Now);
                    }
                    //if (DtFinal.Columns.Contains("Column" + (Convert.ToInt32(ddlcompanyType.SelectedValue) - 1).ToString()))
                    //{
                    //    CompanyType = DtFinal.Rows[iCount]["Column" + (Convert.ToInt32(ddlcompanyType.SelectedValue) - 1).ToString()].ToString().Trim();
                    //    ObjtblCompany.fkCompanyTypeId = (CompanyTypeColl.Where(d => d.CompanyType.ToLower() == CompanyType.ToLower()).Select(db => db.pkCompanyTypeId)).FirstOrDefault();
                    //}
                    //if (DtFinal.Columns.Contains("Column" + (Convert.ToInt32(ddlLeadSource.SelectedValue) - 1).ToString()))
                    //{
                    //    ObjtblCompany.LeadSource = DtFinal.Rows[iCount]["Column" + (Convert.ToInt32(ddlLeadSource.SelectedValue) - 1).ToString()].ToString().Trim();
                    //}
                    if (DtFinal.Columns.Contains("Column" + (Convert.ToInt32(ddlState.SelectedValue) - 1).ToString()))
                    {
                        StateName = DtFinal.Rows[iCount]["Column" + (Convert.ToInt32(ddlState.SelectedValue) - 1).ToString()].ToString().Trim();

                        ObjtblLocation.fkStateID = (StateColl.Where(d => d.StateName.ToLower() == StateName.ToLower()).Select(db => db.pkStateId)).FirstOrDefault();
                    }
                    if (DtFinal.Columns.Contains("Column" + (Convert.ToInt32(ddlCity.SelectedValue) - 1).ToString()))
                    {
                        ObjtblLocation.City = DtFinal.Rows[iCount]["Column" + (Convert.ToInt32(ddlCity.SelectedValue) - 1).ToString()].ToString().Trim();
                    }
                    if (ddlMainContact.SelectedValue.ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (Convert.ToInt32(ddlMainContact.SelectedValue) - 1).ToString()))
                        {
                            ObjtblCompany.MainContactPersonName = DtFinal.Rows[iCount]["Column" + (Convert.ToInt32(ddlMainContact.SelectedValue) - 1).ToString()].ToString().Trim();
                        }
                    }
                    else
                    {
                        ObjtblCompany.MainContactPersonName = string.Empty;
                    }
                    if (ddlMainEmail.SelectedValue.ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (Convert.ToInt32(ddlMainEmail.SelectedValue) - 1).ToString()))
                        {
                            ObjtblCompany.MainEmailId = DtFinal.Rows[iCount]["Column" + (Convert.ToInt32(ddlMainEmail.SelectedValue) - 1).ToString()].ToString().Trim();
                        }
                    }
                    else
                    {
                        ObjtblCompany.MainEmailId = string.Empty;
                    }
                    if (ddlAddress.SelectedValue.ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (Convert.ToInt32(ddlAddress.SelectedValue) - 1).ToString()))
                        {
                            ObjtblLocation.Address1 = DtFinal.Rows[iCount]["Column" + (Convert.ToInt32(ddlAddress.SelectedValue) - 1).ToString()].ToString().Trim();
                        }
                    }
                    else
                    {
                        ObjtblLocation.Address1 = string.Empty;
                    }
                    if (ddlZip.SelectedValue.ToString() != "-1")
                    {
                        if (DtFinal.Columns.Contains("Column" + (Convert.ToInt32(ddlZip.SelectedValue) - 1).ToString()))
                        {
                            ObjtblLocation.ZipCode = DtFinal.Rows[iCount]["Column" + (Convert.ToInt32(ddlZip.SelectedValue) - 1).ToString()].ToString().Trim();
                        }
                    }
                    else
                    {
                        ObjtblLocation.ZipCode = string.Empty;
                    }
                    if (DtFinal.Columns.Contains("Column" + (Convert.ToInt32(ddlMainPhone.SelectedValue) - 1).ToString()))
                    {
                        ObjtblLocation.PhoneNo1 = DtFinal.Rows[iCount]["Column" + (Convert.ToInt32(ddlMainPhone.SelectedValue) - 1).ToString()].ToString().Trim();
                    }

                    int result = ObjBALJuno.InsertJuno(ObjtblCompany, ObjtblLocation);
                    if (result != 1)
                    {
                        return iCount;
                    }

                }
            }
            return DtFinal.Rows.Count;
        }

        protected void btnLoadAnotherBatch_Click(object sender, EventArgs e)
        {
            Response.Redirect("Junos.aspx");
        }
        public bool RowValidation(string ColumnName, string CellData, string Expr, List<tblState> StateColl, List<tblCity> CityColl, List<tblCompanyType> CompanyTypeColl, List<tblCompany> CompanyColl, List<tblLocation> LocationColl)
        {
            bool IsCellValid = false;
            if (Expr != "")
            {
                Regex reg = new Regex(Expr);
                IsCellValid = (reg.IsMatch(CellData)) ? true : false;
            }
            else { IsCellValid = true; }
            if (ColumnName.ToLower().Contains("state"))
            {
                IsCellValid = (StateColl.Where(d => d.StateName.ToLower() == CellData.ToLower()).Count() > 0) ? true : false;
            }
            else if (ColumnName.ToLower().Contains("city"))
            {
                IsCellValid = (CityColl.Where(d => d.City.ToLower() == CellData.ToLower()).Count() > 0) ? true : false;
            }
            else if (ColumnName.ToLower().Contains("companytype"))
            {
                IsCellValid = (CompanyTypeColl.Where(d => d.CompanyType.ToLower() == CellData.ToLower()).Count() > 0) ? true : false;
            }
            else if (ColumnName.ToLower().Contains("mainemail") && IsCellValid == true)
            {
                IsCellValid = (CompanyColl.Where(d => d.MainEmailId.ToLower() == CellData.ToLower()).Count() > 0) ? false : true;
            }
            else if (ColumnName.ToLower().Contains("mainphone") && IsCellValid == true)
            {
                IsCellValid = (LocationColl.Where(d => d.PhoneNo1.ToLower() == CellData.ToLower()).Count() > 0) ? false : true;
            }
            return IsCellValid;
        }


    #endregion

    # endregion
    
    </script>
<%--<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>--%>
<AjaxCtrl:ToolkitScriptManager ID="scManager" runat="server">
</AjaxCtrl:ToolkitScriptManager>
<%--<asp:UpdatePanel ID="UpuploadJuno" runat="server">
    <ContentTemplate>
    <img src="../../Content/themes/base/images/upload-juno-new.png" onclick="ShowJuno()" title="Upload Juno" alt="Upload Juno" />
        <asp:ImageButton AlternateText="Upload Juno" runat="server"
            ID="imgbtnUploadJuno" ImageUrl="~/Content/themes/base/images/upload-juno-new.png" ToolTip="Upload Juno"
            OnClick="imgbtnUploadJuno_Click" />
    </ContentTemplate>
</asp:UpdatePanel>--%>
    <asp:UpdatePanel ID="UPJuno" runat="server">
        <ContentTemplate>
            <asp:HiddenField runat="server" ID="hdnJunoFileFullPath" />
            <input type="hidden" id="selectedJuno" runat="server" value="" />
            <input type="hidden" id="hidHeaderChoose" value="false" />
            <div id="popSPE">
                <asp:Button ID="btnjunoSample" runat="server" Style="display: none;" />
                <cc1:ModalWindow ID="MVJunoStep1" runat="server" Width="650px" Title="" TitleForeColor="White"
                    Style="display: block;" Theme="Mac" OnClientClose="CloseWindow();return false;"
                    CloseButtonVisible="false">
                    <asp:UpdatePanel ID="UpdJunoStep1" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:PostBackTrigger ControlID="BtnSampleFile" />
                            <asp:PostBackTrigger ControlID="btnBack" />
                        </Triggers>
                        <ContentTemplate>
                            <div id="divPop" style="display: block; border-color: Black; min-height: 200px; height: auto;
                                border-width: 1px; border-style: solid; overflow-x: hidden; background-color: #f5f5f5;"
                                runat="server">
                                <table cellpadding="2" cellspacing="0" width="100%" border="0">
                                    <tr>
                                        <td colspan="2" style="padding: 5px;">
                                            <div>
                                                <div style="float: left;">
                                                    <asp:Label runat="server" ID="lblStep" Text="Step : 1) Choose Juno file."></asp:Label></div>
                                                <div style="float: left; padding: 3px 0px 0px 5px;">
                                                    <asp:UpdateProgress ID="UpdateProgress3" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="UpdJunoStep1">
                                                        <ProgressTemplate>
                                                            <img align="left" alt="" src="../Resources/Images/ajax-loader1.gif" />
                                                        </ProgressTemplate>
                                                    </asp:UpdateProgress>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" valign="top" align="center">
                                            <asp:Label runat="server" ID="lblMessage_juno"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="middle">
                                            <asp:MultiView runat="server" ID="MultiViewJuno" ActiveViewIndex="0">
                                                <asp:View runat="server" ID="VWUploadJunoFile">
                                                    <div style="min-height: 180px;">
                                                        <table cellpadding="2" cellspacing="0" border="0" width="100%" style="padding: 0px">
                                                            <tr>
                                                                <td>
                                                                    <br />
                                                                    <br />
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="padding-top: 2px;" valign="middle">
                                                                    Please choose your file.
                                                                </td>
                                                                <td align="left" valign="top">
                                                                    <asp:FileUpload runat="server" Visible="false" ID="fpUploadJuno" Width="300px" />
                                                                    <div>
                                                                        <div style="float: left;">
                                                                            <AjaxCtrl:AsyncFileUpload ID="afuUpload" OnUploadedComplete="ProcessUpload" runat="server"
                                                                                OnClientUploadComplete="uploadComplete" Width="300px" UploaderStyle="Traditional"
                                                                                UploadingBackColor="#CCFFFF" ThrobberID="myThrobber" />
                                                                        </div>
                                                                        <div style="float: left;">
                                                                            <asp:Label runat="server" ID="myThrobber" Style="display: none; margin-top: 3px;">
                                                                     <img align="left" alt="" src="../Resources/Images/ajax-loader1.gif" />
                                                                            </asp:Label></div>
                                                                    </div>
                                                                    <asp:HiddenField runat="server" ID="hfUploadJunoFileType" />
                                                                    <asp:RequiredFieldValidator ID="reqFUCsv" runat="server" ControlToValidate="fpUploadJuno"
                                                                        Display="Dynamic" ErrorMessage="please upload file" ValidationGroup="UploadJuno" />
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="BtnSampleFile" runat="server" Text="Sample file" OnClick="BtnSampleFile_Click" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td>
                                                                    <span style="color: Gray; font-size: 12px;">.csv,.xls,.xlsx are allowed.</span>
                                                                </td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                                <td align="left" valign="middle">
                                                                    <asp:Button ID="btnShowHeader" OnClick="btnShowHeader_Click" Style="display: none;"
                                                                        Text="Continue" runat="server" />
                                                                    <asp:UpdatePanel ID="update_Junoprocessing" runat="server">
                                                                        <ContentTemplate>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </asp:View>
                                                <asp:View runat="server" ID="VWAskHeaders">
                                                    <div style="min-height: 180px;">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td align="center" style="height: 150px;" valign="middle">
                                                                    <table cellpadding="2" cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td align="center" colspan="2">
                                                                                <asp:CheckBox runat="server" ID="chkIsJunoHeaders" Text="This data file has headers on the first row?" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="display: none" id="trWorkBookSheets" runat="server">
                                                                            <td align="left" style="width: 50%">
                                                                                Choose a sheet
                                                                            </td>
                                                                            <td align="left" style="width: 50%">
                                                                                <asp:DropDownList runat="server" ID="ddlExcelSheets" Width="150px">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center" colspan="2">
                                                                                <asp:Button runat="server" OnClick="btnContinueJunoUploadStep2_Click" ID="btnContinueJunoUploadStep2"
                                                                                    Text="Continue" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </asp:View>
                                                <asp:View runat="server" ID="VWMapping">
                                                    <table id="tblMap" runat="server">
                                                        <tr>
                                                            <td width="170px" class="alt_column">
                                                                <span class="RequiredFieldsIndicator">*</span>Company Name :
                                                            </td>
                                                            <td width="418px" align="left">
                                                                <asp:DropDownList ID="ddlcompanyName" runat="server">
                                                                </asp:DropDownList>
                                                                <asp:CompareValidator ID="cmpName" runat="server" ControlToValidate="ddlcompanyName"
                                                                    Display="Dynamic" Operator="NotEqual" SetFocusOnError="True" ErrorMessage="Select Company Name."
                                                                    ValidationGroup="JunoUP" ValueToCompare="-1"></asp:CompareValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="170px" class="alt_column">
                                                                <span class="RequiredFieldsIndicator">*</span>State :
                                                            </td>
                                                            <td width="418px" align="left">
                                                                <asp:DropDownList ID="ddlState" runat="server">
                                                                </asp:DropDownList>
                                                                <asp:CompareValidator ID="cmpState" runat="server" ControlToValidate="ddlState" Display="Dynamic"
                                                                    Operator="NotEqual" SetFocusOnError="True" ErrorMessage="Select State." ValidationGroup="JunoUP"
                                                                    ValueToCompare="-1"></asp:CompareValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="170px" class="alt_column">
                                                                <span class="RequiredFieldsIndicator">*</span>City :
                                                            </td>
                                                            <td width="418px" align="left">
                                                                <asp:DropDownList ID="ddlCity" runat="server">
                                                                </asp:DropDownList>
                                                                <asp:CompareValidator ID="cmpCity" runat="server" ControlToValidate="ddlCity" Display="Dynamic"
                                                                    Operator="NotEqual" SetFocusOnError="True" ErrorMessage="Select City." ValidationGroup="JunoUP"
                                                                    ValueToCompare="-1"></asp:CompareValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="170px" class="alt_column">
                                                                Zip :
                                                            </td>
                                                            <td width="418px" align="left">
                                                                <asp:DropDownList ID="ddlZip" runat="server">
                                                                </asp:DropDownList>
                                                                <asp:CompareValidator ID="cmpZip" runat="server" ControlToValidate="ddlZip" Display="Dynamic"
                                                                    Enabled="false" Operator="NotEqual" SetFocusOnError="True" ErrorMessage="Select ZipCode."
                                                                    ValidationGroup="JunoUP" ValueToCompare="-1"></asp:CompareValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="170px" class="alt_column">
                                                                Address :
                                                            </td>
                                                            <td width="418px" align="left">
                                                                <asp:DropDownList ID="ddlAddress" runat="server">
                                                                </asp:DropDownList>
                                                                <asp:CompareValidator ID="cmpAddress" runat="server" ControlToValidate="ddlAddress"
                                                                    Display="Dynamic" Enabled="false" Operator="NotEqual" SetFocusOnError="True"
                                                                    ErrorMessage="Select Address." ValidationGroup="JunoUP" ValueToCompare="-1"></asp:CompareValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="170px" class="alt_column">
                                                                Main Contact :
                                                            </td>
                                                            <td width="418px" align="left">
                                                                <asp:DropDownList ID="ddlMainContact" runat="server">
                                                                </asp:DropDownList>
                                                                <asp:CompareValidator ID="cmpMainContact" runat="server" ControlToValidate="ddlMainContact"
                                                                    Enabled="false" Display="Dynamic" Operator="NotEqual" SetFocusOnError="True"
                                                                    ErrorMessage="Select Main Contact." ValidationGroup="JunoUP" ValueToCompare="-1"></asp:CompareValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="170px" class="alt_column">
                                                                Main Email :
                                                            </td>
                                                            <td width="418px" align="left">
                                                                <asp:DropDownList ID="ddlMainEmail" runat="server">
                                                                </asp:DropDownList>
                                                                <asp:CompareValidator ID="cmpMainEmail" runat="server" ControlToValidate="ddlMainEmail"
                                                                    Enabled="false" Display="Dynamic" Operator="NotEqual" SetFocusOnError="True"
                                                                    ErrorMessage="Select Main Email." ValidationGroup="JunoUP" ValueToCompare="-1"></asp:CompareValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="170px" class="alt_column">
                                                                <span class="RequiredFieldsIndicator">*</span>Main Phone :
                                                            </td>
                                                            <td width="418px" align="left">
                                                                <asp:DropDownList ID="ddlMainPhone" runat="server">
                                                                </asp:DropDownList>
                                                                <asp:CompareValidator ID="cmpMainPhone" runat="server" ControlToValidate="ddlMainPhone"
                                                                    Display="Dynamic" Operator="NotEqual" SetFocusOnError="True" ErrorMessage="Select Main Phone."
                                                                    ValidationGroup="JunoUP" ValueToCompare="-1"></asp:CompareValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <hr />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="170px" class="alt_column">
                                                                <span class="RequiredFieldsIndicator">*</span>Company Type :
                                                            </td>
                                                            <td width="418px" align="left">
                                                                <asp:DropDownList ID="ddlcompanyType" runat="server">
                                                                </asp:DropDownList>
                                                                <asp:CompareValidator ID="cmpType" runat="server" ControlToValidate="ddlcompanyType"
                                                                    Display="Dynamic" Operator="NotEqual" SetFocusOnError="True" ErrorMessage="Select Company Type."
                                                                    ValidationGroup="JunoUP" ValueToCompare="-1"></asp:CompareValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="170px" class="alt_column">
                                                                <span class="RequiredFieldsIndicator">*</span>Lead Source :
                                                            </td>
                                                            <td width="418px" align="left">
                                                                <asp:TextBox ID="TxtLeadSource" runat="server" class="TextBoxStyle"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TxtLeadSource"
                                                                    Display="Dynamic" SetFocusOnError="True" ErrorMessage="Enter Lead Source." ValidationGroup="JunoUP"></asp:RequiredFieldValidator>
                                                                <%--<asp:DropDownList ID="ddlLeadSource" runat="server">
                                                                </asp:DropDownList>
                                                                <asp:CompareValidator ID="cmpLeadSource" runat="server" ControlToValidate="ddlLeadSource"
                                                                    Display="Dynamic" Operator="NotEqual" SetFocusOnError="True" ErrorMessage="Select Lead Source."
                                                                    ValidationGroup="JunoUP" ValueToCompare="-1"></asp:CompareValidator>--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" colspan="2">
                                                                <asp:ImageButton runat="server" ID="imgbtnJunoValidate" ValidationGroup="JunoUP"
                                                                    ToolTip="Validate Juno" ImageUrl="~/Resources/Images/validate-juno.png" Visible="false"
                                                                    OnClick="imgbtnJunoValidate_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>
                                                <asp:View runat="server" ID="VWPreview">
                                                    <div style="overflow-y: scroll; height: 200px; width: 100%;">
                                                        <table cellpadding="2" cellspacing="0" width="100%" border="0">
                                                            <tr>
                                                                <td align="left" style="display: none;" colspan="1">
                                                                    <asp:Literal ID="ltPreview" runat="server" />
                                                                </td>
                                                                <td valign="middle" style="display: none;">
                                                                    <asp:Image ID="ImgOutPut" ImageUrl="~/Resources/Images/question.png" Width="50px"
                                                                        runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:GridView ID="gvPreview" ShowHeader="true" Width="100%" runat="server" AutoGenerateColumns="false"
                                                                        OnRowDataBound="gvPreview_RowDataBound">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="Columns">
                                                                                <ItemTemplate>
                                                                                    <%#Eval("column") %></ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Row1">
                                                                                <ItemTemplate>
                                                                                    <%#Eval("r1") %></ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Row2">
                                                                                <ItemTemplate>
                                                                                    <%#Eval("r2") %></ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Row3">
                                                                                <ItemTemplate>
                                                                                    <%#Eval("r3") %></ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText=".....">
                                                                                <ItemTemplate>
                                                                                    <%#Eval("r4") %></ItemTemplate>
                                                                                <ItemStyle Width="50px" HorizontalAlign="Center" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Validate">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="LblIsValid" Visible="false" Text='<%#Eval("Valid") %>' runat="server" />
                                                                                    <asp:Image ID="ImgIsValid" Width="18px" ImageUrl="~/Resources/Images/successmsg.png"
                                                                                        runat="server" />
                                                                                </ItemTemplate>
                                                                                <ItemStyle Width="50px" HorizontalAlign="Center" />
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div>
                                                        <asp:Button ID="btnCsvToObjectAfter" OnClick="btnCsvToObjectAfter_Click" Text="Continue"
                                                            runat="server" />
                                                    </div>
                                                </asp:View>
                                                <asp:View runat="server" ID="VWThanks">
                                                    <div style="min-height: 180px;">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td style="height: 150px;" valign="middle" align="center">
                                                                    <span style="color: Green;">Thank you! your all Juno Prospects are saved successfully.</span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Button ID="btnLoadAnotherBatch" OnClick="btnLoadAnotherBatch_Click" Text="Continue"
                                                                        runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </asp:View>
                                            </asp:MultiView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:Button ID="btnBack" runat="server" Visible="false" Text="Back" OnClick="btnBack_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </cc1:ModalWindow>
                <AjaxCtrl:ModalPopupExtender ID="MPEJunoStep1" BackgroundCssClass="modalBackground"
                    PopupControlID="MVJunoStep1" runat="server" TargetControlID="btnjunoSample" BehaviorID="behaveJunoup">
                </AjaxCtrl:ModalPopupExtender>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>