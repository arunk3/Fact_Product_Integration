﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" AutoEventWireup="true" %>

<%----%>
<%@ Register TagPrefix="telerik" Assembly="Telerik.ReportViewer.WebForms" Namespace="Telerik.ReportViewer.WebForms" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Emerge.Services" %>
<%@ Import Namespace="Emerge.Data" %>

<%@ Register Assembly="Telerik.ReportViewer.WebForms, Version=8.2.14.1204, Culture=neutral, PublicKeyToken=a9d7983dfcc261be" Namespace="Telerik.ReportViewer.WebForms" TagPrefix="telerik" %>
<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <meta name="viewport" content="width=device-width" />
    <title></title>
</head>
<body>
    <div style="width: 100%; text-align: center;">
        <script runat="server">
        
            public override void VerifyRenderingInServerForm(Control control)
            {
                // to avoid the server form (<form runat="server">) requirement
            }

            protected override void OnLoad(EventArgs e)
            {
                base.OnLoad(e);
                if (Session["UserRoles"] != null)
                {
                    string[] userRoles = (string[])Session["UserRoles"];
                    if (userRoles.Where(x => x.ToLower().Contains("sales")).Count() > 0)
                    {
                        ViewData["SalesRep"] = (Emerge.Common.Utility.GetUID(HttpContext.Current.User.Identity.Name)).ToString();
                    }
                }
                string ApplicationId = (ViewData["appId"] != null) ? ViewData["appId"].ToString() : Guid.Empty.ToString();
                string Month = (ViewData["Month"] != null) ? ViewData["Month"].ToString() : DateTime.Now.Month.ToString();
                string Year = (ViewData["Year"] != null) ? ViewData["Year"].ToString() : DateTime.Now.Year.ToString();
                int CompanyId = Convert.ToInt32(ViewData["CompanyId"]) != 0 ? Convert.ToInt32(ViewData["CompanyId"]) : 0;
                int LocationId = Convert.ToInt32(ViewData["LocationId"]) != 0 ? Convert.ToInt32(ViewData["LocationId"]) : 0;
                int UserId = Convert.ToInt32(ViewData["UserId"]) != 0 ? Convert.ToInt32(ViewData["UserId"]) : 0;
                string SalesRep = (ViewData["SalesRep"] != null) ? ViewData["SalesRep"].ToString() : Guid.Empty.ToString();
                string RefCode = (ViewData["RefCode"] != null) ? ViewData["RefCode"].ToString() : "0";
                string CompanyStatus = (ViewData["CompanyStatus"] != null) ? ViewData["CompanyStatus"].ToString() : "0";

                rptSalesReport.ReportSource = new EmergeBilling.BillingReport();
                Databind(ApplicationId, Month, Year, CompanyId, LocationId, UserId, SalesRep, RefCode, CompanyStatus);
            }

            public void Databind(string ApplicationId, string Month, string Year, int CompanyId, int LocationId, int UserId, string SalesRep, string RefCode, string CompanyStatus)
            {
                EmergeBilling.BillingReport ObjMain = new EmergeBilling.BillingReport();
                Telerik.Reporting.Report rptMain = new EmergeBilling.BillingReport();
                EmergeBilling.BillingReport.Month = Month;
                EmergeBilling.BillingReport.Year = Year;

                try
                {
                    //#19
                    //if (!Emerge.Common.CommonHelper.IsGuid(CompanyId))
                    //{
                    //    CompanyId = Guid.Empty.ToString();
                    //}
                    //if (!Emerge.Common.CommonHelper.IsGuid(LocationId))
                    //{
                    //    LocationId = Guid.Empty.ToString();
                    //}
                    //if (!Emerge.Common.CommonHelper.IsGuid(UserId))
                    //{
                    //    UserId = Guid.Empty.ToString();
                    //}
                    if (!Emerge.Common.CommonHelper.IsGuid(SalesRep))
                    {
                        SalesRep = Guid.Empty.ToString();
                    }
                    BALBillingData ObjBALBillingData = new BALBillingData();
                    Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALProductsPerLocation>, List<List<BALReportsByLocation>>, List<proc_GetBillingAdjustmentsResult>, Totals, Totals> collection = ObjBALBillingData.GetBillingForSales(ApplicationId, Month, Year, CompanyId, LocationId, UserId, SalesRep, int.Parse(RefCode), CompanyStatus);
                    //Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALReportsByLocation>, List<proc_GetBillingAdjustmentsResult>> collectionApi = ObjBALBillingData.GetBillingForAdminForApi(ApplicationId, Month, Year, new Guid(CompanyId), new Guid(LocationId), new Guid(UserId), new Guid(SalesRep), int.Parse(RefCode));

                    ObjMain.ObjData = collection;
                    //ObjMain.ObjDataApi = collectionApi;
                    ObjMain.IsSalesReport = true;
                    ObjMain.CompanyId = CompanyId;
                    rptSalesReport.ReportSource = ObjMain;
                }
                catch { }
            }
        
        </script>
        <telerik:reportviewer runat="server" id="rptSalesReport" style="width: 100%; height: 500px; text-align: center;" />
        <br />
    </div>
</body>
</html>
