﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Register Assembly="PDFViewer" Namespace="PDFViewer" TagPrefix="ViewPDF" %>

<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Emerge.Services" %>
<%@ Import Namespace="Emerge.Data" %>
<!DOCTYPE html>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>PartialReportViewer</title>
</head>
<body>
    <div style="width: 100%; text-align: center;">
        <script runat="server">
            public override void VerifyRenderingInServerForm(Control control)
            {
                // to avoid the server form (<form runat="server">) requirement
            }

            protected override void OnLoad(EventArgs e)
            {
                base.OnLoad(e);
                int pkOrderResponseId = Convert.ToInt32(ViewData["pkOrderResponseId"]) != 0 ? Convert.ToInt32(ViewData["pkOrderResponseId"]) : 0;
                idPDFViewer.Visible = true;
                string FilePath = "";
                string FileName = "611fe0ad-ecec-e111-8e96-6c626da99cc2_129914077998017078.pdf";
                FilePath = "../../Resources/Upload/ResponseImages/" + FileName;
                idPDFViewer.FilePath = FilePath;
            }
        </script>
        <ViewPDF:ShowPdf  ID="idPDFViewer" runat="server" BorderStyle="Inset" BorderWidth="2px"
                            Width="100%" Height="700px" Visible="false" />
    </div>
</body>
</html>
