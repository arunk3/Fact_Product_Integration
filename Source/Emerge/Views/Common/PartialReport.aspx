﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" AutoEventWireup="true" %>

<%----%>
<%@ Register TagPrefix="telerik" Assembly="Telerik.ReportViewer.WebForms" Namespace="Telerik.ReportViewer.WebForms" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Emerge.Services" %>
<%@ Import Namespace="Emerge.Data" %>

<%@ Register Assembly="Telerik.ReportViewer.WebForms, Version=8.2.14.1204, Culture=neutral, PublicKeyToken=a9d7983dfcc261be" Namespace="Telerik.ReportViewer.WebForms" TagPrefix="telerik" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <title></title>
</head>
<body>
    <div style="width: 100%; text-align: center;">
        <script runat="server">
        
            public override void VerifyRenderingInServerForm(Control control)
            {
                // to avoid the server form (<form runat="server">) requirement
            }

            protected override void OnLoad(EventArgs e)
            {
                base.OnLoad(e);
                string ApplicationId = (ViewData["appId"] != null) ? ViewData["appId"].ToString() : Guid.Empty.ToString();
                string Month = (ViewData["Month"] != null) ? ViewData["Month"].ToString() : DateTime.Now.Month.ToString();
                string Year = (ViewData["Year"] != null) ? ViewData["Year"].ToString() : DateTime.Now.Year.ToString();
                int CompanyId = Convert.ToInt32(ViewData["CompanyId"]) != 0 ? Convert.ToInt32(ViewData["CompanyId"]) : 0;
                int LocationId = Convert.ToInt32(ViewData["LocationId"]) != 0 ? Convert.ToInt32(ViewData["LocationId"]) : 0;
                int UserId = Convert.ToInt32(ViewData["UserId"]) != 0 ? Convert.ToInt32(ViewData["UserId"]) : 0;


                string SalesRep = (ViewData["SalesRep"] != null) ? ViewData["SalesRep"].ToString() : Guid.Empty.ToString();
                if (SalesRep == "undefined")
                {
                    SalesRep = Guid.Empty.ToString();
                }

                string RefCode = (ViewData["RefCode"] != null) ? ViewData["RefCode"].ToString() : "0";
                string IsAdmin = (ViewData["IsAdmin"] != null) ? ViewData["IsAdmin"].ToString() : "false";

                rptBillingSatement.ReportSource = new EmergeBilling.BillingStatement();
                Databind(ApplicationId, Month, Year, CompanyId, LocationId, UserId, SalesRep, RefCode, IsAdmin);
            }

            public void Databind(string ApplicationId, string Month, string Year, int CompanyId = 0, int LocationId = 0, int UserId = 0, string SalesRep = "00000000-0000-0000-0000-000000000000", string RefCode = "0", string IsAdmin = "false")
            {
                EmergeBilling.BillingStatement ObjMain = new EmergeBilling.BillingStatement();
                Telerik.Reporting.Report rptMain = new EmergeBilling.BillingStatement();

                // CompanyId = "7dc87d77-6bb5-4bb3-9837-fa861f36f281";
                try
                {
                    BALBillingData ObjBALBillingData = new BALBillingData();
                    Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALCompanyInformation>, List<BALReportsByLocation>, List<proc_LoadOpenInvoiceByCompanyIdResult>, List<proc_GetBillingAdjustmentsResult>> collection = ObjBALBillingData.GetAllData(ApplicationId, Month, Year, CompanyId, LocationId, UserId, new Guid(SalesRep), int.Parse(RefCode));
                    Tuple<List<BALBillingSummary>, List<BALBillingSummary>, List<BALReportsByLocation>, List<proc_GetBillingAdjustmentsResult>> collectionApi = ObjBALBillingData.GetAllDataForApi(ApplicationId, Month, Year, CompanyId, LocationId, UserId, new Guid(SalesRep), int.Parse(RefCode));
                    string FromDate, ToDate;
                    GenerateDates(int.Parse(Month), int.Parse(Year), out FromDate, out ToDate);
                    List<clsSavedReports> ObjSaveReportsCollection = ObjBALBillingData.GetSavedReports_Billing(0,
                                                                                           UserId,
                                                                                           CompanyId,
                                                                                           LocationId,
                                                                                           "OrderDt",
                                                                                           "Desc",
                                                                                           false,
                                                                                           0,
                                                                                           0,
                                                                                           FromDate,
                                                                                           ToDate,
                                                                                           string.Empty,
                                                                                           byte.MinValue, "AllReports", int.Parse(RefCode));
                    tblCompany data = ObjBALBillingData.GetCompanyAccountNumber(CompanyId);
                    if (data != null)
                    {
                        EmergeBilling.BillingStatement.CompanyCode = data.CompanyCode;
                    }
                    EmergeBilling.BillingStatement.Month = Month;
                    EmergeBilling.BillingStatement.Year = Year;
                    ObjMain.IsDueDate = false;
                    if (collection.Item5.Count > 0)
                    {
                        if (collection.Item5.Any(d => d.InvoiceDueDate < DateTime.Now))
                        {
                            ObjMain.IsDueDate = true;
                        }
                    }
                    ObjMain.ObjData = collection;
                    ObjMain.ObjDataApi = collectionApi;
                    ObjMain.ObjSavedReports = ObjSaveReportsCollection;
                    ObjMain.IsAdmin = bool.Parse(IsAdmin);
                    ObjMain.BillingId = Emerge.Common.Utility.BillingId;
                    ObjMain.BillingPhone = Emerge.Common.Utility.BillingPhone;
                    ObjMain.ApplicationPath = Emerge.Common.ApplicationPath.GetApplicationPath();
                    rptBillingSatement.ReportSource = ObjMain;
                }
                catch (Exception ex)
                {
                    string FilePath = System.Web.HttpContext.Current.Server.MapPath(@"~\Resources\Upload\PSPVendorCall\BillingStatementError.txt");
                    if (!System.IO.File.Exists(FilePath))
                    {
                        System.IO.TextWriter tw = System.IO.File.CreateText(FilePath);
                        tw.WriteLine("Catch Exception @" + DateTime.Now.ToString());
                        tw.WriteLine("-----------------------------------");
                        tw.WriteLine("Exception Message \n -------------------");
                        tw.WriteLine(ex.Message.ToString());
                        tw.WriteLine("\n\n\n\n");
                        tw.WriteLine("Exception  \n -------------------");
                        tw.WriteLine(ex.ToString());
                        tw.Close();
                    }
                    else
                    {
                        System.IO.StreamWriter sw = System.IO.File.AppendText(FilePath);
                        StringBuilder sb = new StringBuilder();
                        sb.Append("Catch Exception @" + DateTime.Now.ToString());
                        sb.Append("-----------------------------------");
                        sb.Append("Exception Message \n -------------------");
                        sb.Append(ex.Message.ToString());
                        sb.Append("\n\n\n\n");
                        sb.Append("Exception  \n -------------------");
                        sb.Append(ex.ToString());
                        sw.WriteLine(sb);
                        sw.Close();
                    }
                }
            }

            public void GenerateDates(int BillingMonth, int BillingYear, out string FromDate, out string ToDate)
            {
                FromDate = string.Empty;
                ToDate = string.Empty;

                if (BillingMonth == 1 || BillingMonth == 3 || BillingMonth == 5 || BillingMonth == 7 || BillingMonth == 8 || BillingMonth == 10 || BillingMonth == 12)
                {
                    FromDate = "01-" + ConvertToMMM(BillingMonth) + "-" + BillingYear;
                    ToDate = "31-" + ConvertToMMM(BillingMonth) + "-" + BillingYear;
                }
                else if (BillingMonth == 4 || BillingMonth == 6 || BillingMonth == 9 || BillingMonth == 11)
                {
                    FromDate = "01-" + ConvertToMMM(BillingMonth) + "-" + BillingYear;
                    ToDate = "30-" + ConvertToMMM(BillingMonth) + "-" + BillingYear;
                }
                else if (BillingMonth == 2)
                {
                    if (BillingYear % 4 == 0)
                    {
                        FromDate = "01-" + ConvertToMMM(BillingMonth) + "-" + BillingYear;
                        ToDate = "29-" + ConvertToMMM(BillingMonth) + "-" + BillingYear;
                    }
                    else
                    {
                        FromDate = "01-" + ConvertToMMM(BillingMonth) + "-" + BillingYear;
                        ToDate = "28-" + ConvertToMMM(BillingMonth) + "-" + BillingYear;
                    }
                }
            }

            private string ConvertToMMM(int BillingMonth)
            {
                string sMonth = string.Empty;

                switch (BillingMonth)
                {
                    case 1:
                        sMonth = "Jan";
                        break;
                    case 2:
                        sMonth = "Feb";
                        break;
                    case 3:
                        sMonth = "Mar";
                        break;
                    case 4:
                        sMonth = "Apr";
                        break;
                    case 5:
                        sMonth = "May";
                        break;
                    case 6:
                        sMonth = "Jun";
                        break;
                    case 7:
                        sMonth = "Jul";
                        break;
                    case 8:
                        sMonth = "Aug";
                        break;
                    case 9:
                        sMonth = "Sep";
                        break;
                    case 10:
                        sMonth = "Oct";
                        break;
                    case 11:
                        sMonth = "Nov";
                        break;
                    case 12:
                        sMonth = "Dec";
                        break;
                }
                return sMonth;
            }

        </script>
        <telerik:ReportViewer runat="server" ID="rptBillingSatement" Style="width: 100%; margin-top: 2%; height: 500px; text-align: center;" />
        <br />
    </div>
</body>
</html>
