﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdverseCoveragePDF.aspx.cs" Inherits="Emerge.AdverseCoveragePDF" %>

<%@ Register Assembly="PDFViewer" Namespace="PDFViewer" TagPrefix="ViewPDF" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .Report_Header
        {
            color: #000;
            font-size: 14px;
            font-weight: bold;
            padding: 8px 8px 8px 0px;
            font-family: Lucida Grande,Arial,Helvetica,sans-serif;
            text-align: left;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
   
     <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="left" style="padding: 2px;">
                    <asp:Label CssClass="Report_Header" ID="LblHeader" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="divPDF" runat="server">
                        <ViewPDF:ShowPdf  ID="idPDFViewer" runat="server" BorderStyle="Inset" BorderWidth="2px"
                            Width="100%" Height="700px" Visible="false" />
                    </div>
                </td>
            </tr>
        </table>
   
    </form>
</body>
</html>
