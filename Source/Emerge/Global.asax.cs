﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Emerge.Common;
using System.Configuration;
using Emerge.CommonClasses;


namespace Emerge
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        //private string host = null;

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            RunReportUrl.UpdateRouteRegistration();
        }


        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            RegisterBundles(BundleTable.Bundles);
            BundleTable.EnableOptimizations = false;


        }
        public void RegisterBundles(BundleCollection collection)
        {

            #region CSS
            String cssPath = "~/Content/min-css";
            StyleBundle cssBundle = new StyleBundle(cssPath);
            cssBundle.Include(
                CommonPaths.ContentPath + "CompressedCombined(Admin).css",
                 CommonPaths.ContentPath + "Site.css",
                CommonPaths.ContentPath + "kendo.default.min.css",
                 CommonPaths.ContentPath + "kendo.common.min.css",
                 CommonPaths.ContentPath + "ui-lightness/jquery-ui-1.8.2.custom.css");

            cssBundle.Transforms.Add(new CssMinify());
            StyleBundle cssBundle_Login_Layout = new StyleBundle("~/min-login-css"); // not in use
            cssBundle_Login_Layout.Include(
                "~/Content/kendo.blueopal.min.css",
                "~/Content/kendo.default.min.css",
                "~/Content/kendo.common.min.css");

            StyleBundle cssBundle_Default_Layout = new StyleBundle("~/min-default-css"); // not in use
            cssBundle_Default_Layout.Include(
                CommonPaths.ContentPath + "Site.css",
                 CommonPaths.ContentPath + "default.css");

            #endregion
            #region JS

            Bundle jsBundle = new ScriptBundle("~/Scripts/min-scripts");
            jsBundle.Include(
                "~/Scripts/jquery-1.7.2.min.js", "~/Scripts/Json2.js",
                "~/Scripts/EmergeCompressedJavascript.js?V=19",
                "~/Scripts/KendoJS/kendo.all.min.js",
                "~/Scripts/jquery-ui-1.8.2.custom.min.js");

            Bundle jsBundleNewReport = new ScriptBundle("~/min-scripts-new-report");
            jsBundleNewReport.Include(
                       "~/Scripts/gen_validatorv4.js", "~/Scripts/CSSNErrors.js?V=14");


            Bundle jsBundleSavedReport = new ScriptBundle("~/min-scripts-saved-report");
            jsBundleSavedReport.Include(
                       "~/Scripts/Pages/SavedReport.js?v22");


            Bundle jsBundle_Default = new ScriptBundle("~/min-default-scripts");
            jsBundle_Default.Include(
                "~/Scripts/jquery-online-1.6.2.js", "~/Scripts/KendoJS/kendo.all.min.js");

            Bundle jsBundle_Login = new ScriptBundle("~/min-login-scripts");
            jsBundle_Default.Include(
                "~/Scripts/jquery-online-1.6.2.js");
            #endregion

            collection.Add(cssBundle);
            collection.Add(cssBundle_Login_Layout);
            collection.Add(cssBundle_Default_Layout);

            collection.Add(jsBundle);
            collection.Add(jsBundleNewReport);
            collection.Add(jsBundleSavedReport);
            collection.Add(jsBundle_Default);
            collection.Add(jsBundle_Login);


        }

        void Application_BeginRequest(Object source, EventArgs e)
        {
            /* commenting, NOT PUNLISHING ON PRODUCTION
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();*/

            /*  HttpApplication app = (HttpApplication)source;
              HttpContext context = app.Context;
              host = FirstRequestInitialisation.Initialise(context);*/

           /*
            if (!Context.Request.IsSecureConnection)
                Response.Redirect(Context.Request.Url.ToString().Replace("http:", "https:"));
            */

        }

        void Application_EndRequest()
        {
            
        }

        class FirstRequestInitialisation
        {
            private static string host = null;

            private static Object s_lock = new Object();

            // Initialise only on the first request
            public static string Initialise(HttpContext context)
            {
                if (string.IsNullOrEmpty(host))
                {
                    lock (s_lock)
                    {
                        if (string.IsNullOrEmpty(host))
                        {
                            Uri uri = HttpContext.Current.Request.Url;
                            host = uri.Scheme + Uri.SchemeDelimiter + uri.Host + ":" + uri.Port;
                        }
                    }
                }

                return host;
            }
        }


        protected void Application_Error()
        {
            try
            {
                string[] arrErrorToSkip = new string[2];  // This array holds list of an errors that we want to skip

                arrErrorToSkip[0] = ".axd".ToLower();
                arrErrorToSkip[1] = ".ico".ToLower();

                //string LastError = Server.GetLastError().ToString().ToLower();
                string ErrorMessage = Server.GetLastError().Message.ToLower();

                for (int iRow = 0; iRow < arrErrorToSkip.Length; iRow++)
                {
                    if (ErrorMessage.Contains(arrErrorToSkip[iRow])) { return; }
                }

                WriteToTxtFile.WriteToFile(Server.GetLastError(), HttpContext.Current.Request.Url.ToString());
                string ErrorEmailFilter = ConfigurationManager.AppSettings["ErrorEmailFilter"].ToString();
                string[] sDelimeter = new string[] { "," };

                string[] arrSplit = ErrorEmailFilter.Split(sDelimeter, StringSplitOptions.None);
                foreach (string x in arrSplit)
                {
                    if (ErrorMessage.Contains(x))
                    {
                    }
                    else
                    {
                        ErrorReportingService.ReportError(Server.GetLastError(), HttpContext.Current.Request.Url.ToString());
                    }
                }



            }
            catch { }
        }


       public void Session_OnEnd()
        {
           
        }


    }
}