﻿<%@ Page Language="C#" EnableViewState="false" ValidateRequest="false" %>

<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Emerge.Services" %>
<%@ Import Namespace="Emerge.Data" %>
<%@ Import Namespace="System.Data" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        
        try
        {

            WriteLogeScreen(Environment.NewLine + "EScreen page load Started " + DateTime.Now.ToString("MM-dd-yyyy_HH-mm-ss") + ":");
            #region /// Code to Create the txt File of the EScreen Response Received
            string eScreenResponse = "";           
            if (!string.IsNullOrEmpty(Request.QueryString["EventID"]))
            {

                WriteLogeScreen(Environment.NewLine + "-------------------------------------------");
                string url = HttpContext.Current.Request.Url.AbsoluteUri;
                WriteLogeScreen(Environment.NewLine + url);

                WriteLogeScreen(Environment.NewLine + "-------------------------------------------");
                
                                
                string EventID = Request.QueryString["EventID"].ToString();
                string OrderId = Request.QueryString["OrderId"].ToString();
                WriteLogeScreen(Environment.NewLine + EventID + " - " + OrderId);
                string textFile = string.Format("{0}.txt", "EScreen_" + DateTime.Now.ToString("MM-dd-yyyy_HH-mm-ss"));
                System.IO.StreamWriter streamWriter = System.IO.File.CreateText(string.Format(@"{0}{1}", System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Resources\\Upload\\EScreenResponse\\", textFile));
                eScreenResponse = "EventID- " + EventID + "  OrderId- " + OrderId;
                streamWriter.WriteLine(eScreenResponse);
                streamWriter.Close();
            }
            else
            {

                WriteLogeScreen(Environment.NewLine + "-------------------------------------------");
                string url = HttpContext.Current.Request.Url.AbsoluteUri;
                WriteLogeScreen(Environment.NewLine + url);
                WriteLogeScreen(Environment.NewLine + "-------------------------------------------");
                WriteLogeScreen(Environment.NewLine + "Response not sent by vendor on this page.");
            }

            #endregion

            #region /// Code To Save and Update Escreen Report and Status Resp.

            if (!string.IsNullOrEmpty(Request.QueryString["EventID"]) && !string.IsNullOrEmpty(Request.QueryString["OrderId"]))
            {

                WriteLogeScreen(Environment.NewLine + "---------------------data----------------------");
                string url = HttpContext.Current.Request.Url.AbsoluteUri;
                WriteLogeScreen(Environment.NewLine + url);
                WriteLogeScreen(Environment.NewLine + "---------------------data----------------------");
                
                int PkOrderId = Convert.ToInt32(Request.QueryString["OrderId"].ToString().Trim());
                string EventID = Request.QueryString["EventID"].ToString().Trim();
                BALOrders ObjBALOrders = null;
                try
                {
                    ObjBALOrders = new BALOrders();
                    ObjBALOrders.UpdateEscreenResponse(PkOrderId, EventID);
                    ObjBALOrders.UpdateOrderStausforEscreen(PkOrderId);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    ObjBALOrders = null;
                }
            }
            #endregion
        }
        catch (Exception ex)
        {
            WriteLogeScreen(Environment.NewLine + "Error in file Creation." + ex.InnerException.ToString());
        }
        finally
        {
            WriteLogeScreen(Environment.NewLine + "EScreen page load Completed at " + DateTime.Now.ToString("MM-dd-yyyy_HH-mm-ss") + ".");
            if (!string.IsNullOrEmpty(Request.QueryString["EventID"]) && !string.IsNullOrEmpty(Request.QueryString["OrderId"]))
            {
                string tempOrderno=Convert.ToString(Request.QueryString["OrderId"]);
                Int32 tempOrderno1 = Convert.ToInt32(Request.QueryString["OrderId"]);
                using (EmergeDALDataContext dx = new EmergeDALDataContext())
                {
                    tempOrderno = dx.tblOrders.FirstOrDefault(x => x.pkOrderId == tempOrderno1).OrderNo;
                }
                lableorderno.Text = tempOrderno;
            }
        }
    }

    public static void WriteLogeScreen(string Content)
    {
        
        string path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Resources\\Upload\\EScreenResponse\\";
        string FileName = path + "AllEScreenResponse.txt";
        StringBuilder sb = new StringBuilder();
        StreamWriter sw;
        if (!File.Exists(FileName))
        {
            sw = File.CreateText(FileName);
            sb.Append(Content);
            sw.WriteLine(sb);
            sw.Close();
        }
        else
        {
            sw = File.AppendText(FileName);
            sb.Append(Content);
            sw.WriteLine(sb);
            sw.Close();
        }
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body style="background-color: #f2f2f4;padding: 30px">
    <form id="form1" runat="server">
        <table align="center" border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td align="left" colspan="2" valign="top">
                    <table align="center" border="0" cellpadding="0" cellspacing="4" class="allSideWhite"
                        width="100%">
                        <tbody>
                            <tr>
                                <td align="left" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td align="left" colspan="2">
                                                    <h2 style='mso-margin-top-alt: 0in; margin-right: 0in; margin-bottom: 7.5pt; margin-left: 0in;
                                                        line-height: 21.0pt'>
                                                        <span style='font-size: 12.5pt; font-family: "Helvetica","sans-serif"; color: #333333'>
                                                            Your Order&nbsp;<asp:Label ID="lableorderno" runat="server" Text=""></asp:Label> &nbsp;has been successfully created. Please close this window and check in Archive.</span></h2>
                                                </td>
                                            </tr>
                                            
                                           
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2" valign="top">
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2" valign="top">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <hr/>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style='font-size: 9.0pt; line-height: 150%; font-family: "Helvetica","sans-serif";
                                    color: #333333'>
                                    For questions about this list, please contact:
                                </td>
                            </tr>
                            <tr style='font-size: 9.0pt; line-height: 150%; font-family: "Helvetica","sans-serif";
                                color: #333333'>
                                <td align="left">
                                    <a style="color: #87ceea;" href="emerge.intelifi.com">support@intelifi.com</a><br />
                                    <br>
                                    <img src="http://emerge.intelifi.com/Resources/Upload/Images/emerge-logo-email.gif" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr style="height: 18px;">
                <td style="width: 60%;">
                </td>
                <td align="right" style="color: white;">
                </td>
            </tr>
        </tbody>
    </table>
    </form>
</body>
</html>
