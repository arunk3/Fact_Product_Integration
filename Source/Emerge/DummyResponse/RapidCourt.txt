﻿<?xml version="1.0" encoding="ISO-8859-1"?>
<BackgroundReports account="" password="" userId="" xmlns="http://ns.hr-xml.org/2004-08-02"><BackgroundReportPackage>
  <ScreeningsSummary>
    <PersonalData>
      <PersonName>
        <GivenName>Katelin</GivenName>
        <MiddleName />
        <FamilyName>Townsend</FamilyName>
      </PersonName>
      <DemographicDetail>
        <DateOfBirth>1988-07-01</DateOfBirth>
        <GovernmentId countryCode="US" issuingAuthority="SSN">313081558</GovernmentId>
      </DemographicDetail>
    </PersonalData>
  </ScreeningsSummary>
  <Screenings>
    <Screening type="x:addresstrace">
      <ClientReferenceId>
        <IdValue>1074547258_PS</IdValue>
      </ClientReferenceId>
      <SSNReport>
        <ProcessedDate>
          <AnyDate>2015-08-18T15:40:31-04:00</AnyDate>
        </ProcessedDate>
        <CreditFile recordCount="4">
          <PersonalData Deleted="0">
            <EffectiveDate>
              <StartDate>
                <YearMonth>2009-01</YearMonth>
              </StartDate>
              <EndDate>
                <AnyDate>2015-08-18</AnyDate>
                <YearMonth>2015-08</YearMonth>
              </EndDate>
            </EffectiveDate>
            <PersonName>
              <FormattedName>TOWNSEND KATELIN R</FormattedName>
              <GivenName>KATELIN</GivenName>
              <MiddleName>R</MiddleName>
              <FamilyName>TOWNSEND</FamilyName>
            </PersonName>
            <PersonName>
              <FormattedName>KATELIN R TOWNSEND</FormattedName>
              <GivenName>KATELIN</GivenName>
              <MiddleName>R</MiddleName>
              <FamilyName>TOWNSEND</FamilyName>
            </PersonName>
            <PersonName>
              <FormattedName>KATELIN RENE TOWNSEND</FormattedName>
              <GivenName>KATELIN</GivenName>
              <MiddleName>RENE</MiddleName>
              <FamilyName>TOWNSEND</FamilyName>
            </PersonName>
            <PostalAddress type="undefined">
              <CountryCode>US</CountryCode>
              <PostalCode>32003-8601</PostalCode>
              <Region>FL</Region>
              <Municipality>FLEMING ISLAND</Municipality>
              <County>CLAY</County>
              <DeliveryAddress>
                <AddressLine>1602 PINECREST DR</AddressLine>
                <StreetName> 1602 PINECREST DR  </StreetName>
              </DeliveryAddress>
            </PostalAddress>
            <ContactMethod>
              <Telephone>
                <FormattedNumber>9042786545</FormattedNumber>
              </Telephone>
            </ContactMethod>
            <ContactMethod>
              <Telephone>
                <FormattedNumber>9045349090</FormattedNumber>
              </Telephone>
            </ContactMethod>
            <DemographicDetail>
              <GovernmentId countryCode="US" documentType="SSN" issuingAuthority="Social Security Administration">313081558</GovernmentId>
              <DateOfBirth>1988-07-01</DateOfBirth>
              <Age>27</Age>
            </DemographicDetail>
            <ReviewNote></ReviewNote>
          </PersonalData>
          <PersonalData Deleted="0">
            <EffectiveDate>
              <StartDate>
                <AnyDate>2003-07-01</AnyDate>
                <YearMonth>2003-07</YearMonth>
              </StartDate>
              <EndDate>
                <AnyDate>2013-07-03</AnyDate>
                <YearMonth>2013-07</YearMonth>
              </EndDate>
            </EffectiveDate>
            <PersonName>
              <FormattedName>TOWNSEND KATELIN RENE</FormattedName>
              <GivenName>KATELIN</GivenName>
              <MiddleName>RENE</MiddleName>
              <FamilyName>TOWNSEND</FamilyName>
            </PersonName>
            <PersonName>
              <FormattedName>KATELIN RENE TOWNSEND</FormattedName>
              <GivenName>KATELIN</GivenName>
              <MiddleName>RENE</MiddleName>
              <FamilyName>TOWNSEND</FamilyName>
            </PersonName>
            <PostalAddress type="undefined">
              <CountryCode>US</CountryCode>
              <PostalCode>32003-8601</PostalCode>
              <Region>FL</Region>
              <Municipality>FLEMING ISLAND</Municipality>
              <County>CLAY</County>
              <DeliveryAddress>
                <AddressLine>1602 PINECREST DR</AddressLine>
                <StreetName> 1602 PINECREST DR  </StreetName>
              </DeliveryAddress>
            </PostalAddress>
            <ContactMethod>
              <Telephone>
                <FormattedNumber>9042786545</FormattedNumber>
              </Telephone>
            </ContactMethod>
            <ContactMethod>
              <Telephone>
                <FormattedNumber>9045349090</FormattedNumber>
              </Telephone>
            </ContactMethod>
            <DemographicDetail>
              <GovernmentId countryCode="US" documentType="SSN" issuingAuthority="Social Security Administration" />
              <DateOfBirth>1988-07-01</DateOfBirth>
              <Age>27</Age>
            </DemographicDetail>
            <ReviewNote></ReviewNote>
          </PersonalData>
          <PersonalData Deleted="0">
            <EffectiveDate>
              <StartDate>
                <AnyDate>2010-06-06</AnyDate>
                <YearMonth>2010-06</YearMonth>
              </StartDate>
              <EndDate>
                <AnyDate>2010-09-02</AnyDate>
                <YearMonth>2010-09</YearMonth>
              </EndDate>
            </EffectiveDate>
            <PersonName>
              <FormattedName>TOWNSEND KATELIN RENE</FormattedName>
              <GivenName>KATELIN</GivenName>
              <MiddleName>RENE</MiddleName>
              <FamilyName>TOWNSEND</FamilyName>
            </PersonName>
            <PersonName>
              <FormattedName>KATELIN RENE TOWNSEND</FormattedName>
              <GivenName>KATELIN</GivenName>
              <MiddleName>RENE</MiddleName>
              <FamilyName>TOWNSEND</FamilyName>
            </PersonName>
            <PostalAddress type="undefined">
              <CountryCode>US</CountryCode>
              <PostalCode>32304-2523</PostalCode>
              <Region>FL</Region>
              <Municipality>TALLAHASSEE</Municipality>
              <County>LEON</County>
              <DeliveryAddress>
                <AddressLine>2614 W TENNESSEE ST APT 8105</AddressLine>
                <StreetName> 2614 W TENNESSEE ST APT 8105  </StreetName>
              </DeliveryAddress>
            </PostalAddress>
            <DemographicDetail>
              <GovernmentId countryCode="US" documentType="SSN" issuingAuthority="Social Security Administration" />
              <DateOfBirth>1988-07-01</DateOfBirth>
              <Age>27</Age>
            </DemographicDetail>
            <ReviewNote></ReviewNote>
          </PersonalData>
          <PersonalData Deleted="0">
            <PersonName>
              <FormattedName>TOWNSEND KATELIN RENE</FormattedName>
              <GivenName>KATELIN</GivenName>
              <MiddleName>RENE</MiddleName>
              <FamilyName>TOWNSEND</FamilyName>
            </PersonName>
            <PersonName>
              <FormattedName>KATELIN RENE TOWNSEND</FormattedName>
              <GivenName>KATELIN</GivenName>
              <MiddleName>RENE</MiddleName>
              <FamilyName>TOWNSEND</FamilyName>
            </PersonName>
            <PostalAddress type="undefined">
              <CountryCode>US</CountryCode>
              <PostalCode>32301-1418</PostalCode>
              <Region>FL</Region>
              <Municipality>TALLAHASSEE</Municipality>
              <County>LEON</County>
              <DeliveryAddress>
                <AddressLine>510 W PARK AVE # 4</AddressLine>
                <StreetName> 510 W PARK AVE # 4  </StreetName>
              </DeliveryAddress>
            </PostalAddress>
            <DemographicDetail>
              <GovernmentId countryCode="US" documentType="SSN" issuingAuthority="Social Security Administration" />
              <DateOfBirth>1988-07-01</DateOfBirth>
              <Age>27</Age>
            </DemographicDetail>
            <ReviewNote></ReviewNote>
          </PersonalData>
        </CreditFile>
      </SSNReport>
      <ScreeningStatus>
        <OrderStatus>Completed</OrderStatus>
      </ScreeningStatus>
    </Screening>
    <Screening type="x:ssnvalidation">
      <ScreeningStatus>
        <OrderStatus>Completed</OrderStatus>
        <ResultStatus>NoDiscrepancy</ResultStatus>
      </ScreeningStatus>
      <SSNReport>
        <SSNStatus>Indiana In 1990-1992</SSNStatus>
        <DateOfDeath>Not Found</DateOfDeath>
      </SSNReport>
    </Screening>
  </Screenings>
</BackgroundReportPackage></BackgroundReports>