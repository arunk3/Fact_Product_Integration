﻿<?xml version="1.0" encoding="ISO-8859-1"?><BackgroundReports xmlns="http://ns.hr-xml.org/2004-08-02" account="" password="" userId="" OrderDetailId="2508649"><BackgroundReportPackage><ScreeningsSummary><PersonalData><PersonName><GivenName>Rochelle</GivenName><MiddleName>D</MiddleName><FamilyName>Brown</FamilyName></PersonName><DemographicDetail><DateOfBirth>1978-06-09</DateOfBirth><GovernmentId countryCode="US" issuingAuthority="SSN">604662405</GovernmentId></DemographicDetail></PersonalData></ScreeningsSummary><Screenings><Screening qualifier="HENNEPIN" type="criminal"><ClientReferenceId><IdValue>133637289_RCX</IdValue></ClientReferenceId><Region>MN</Region><ScreeningResults mediaType="txt" resultType="report" type="result"><Text>AOC - MINNESOTA                                                                    
BROWN,ROCHELLE,DONNICE                                       S=U R=U DOB=06/09/1978
ADDR: MINNEAPOLIS MN 55412 US                                                      
OFFENSES:                                                                          
COURT: HENNEPIN CRIMINAL/TRAFFIC/PETTY DOWNTOWN                                    
DOCKET NUM: 27-VB-13-271113534468                                                  
  FILED: 12/09/2013 DISPOSED: 02/08/2014                                           
    CHRG: (M)NO PARKING - SNOW EMERGENCY                   OFFENSE DATE: 12/06/2013
      CODE/STATUTE: 478.1000                                                       
      PLEA: GUILTY                                                                 
    DISP: (M)NO PARKING - SNOW EMERGENCY      CONVICTED               ON 02/08/2014
      CODE/STATUTE: 478.1000                                                       
      FINAL PLEA: GUILTY                                                           
      SENT: FEE: 42                                            EFFECTIVE: 02/08/2014
        FEES: (FEES TOTAL: $12.00)                                                 
      CHARGE LEVEL: PETTY MISDEMEANOR                                              
      SENTENCE LEVEL: LEVEL OF SENTENCE UNAVAILABLE/ SENTENCED                     
    CASE TYPE: CRIM/TRAF NON-MAND                                                  
    CASE STATUS: CONVERTED CLOSED                                                  
        12/10/2013 - DVS SEARCH PERFORMED, NAME ADDED                              
                                                                                   
        12/10/2013 - CONVERTED VIBES EVENT                                         
                                                                                   
        01/08/2014 - FIRST PENALTY ADDED                                           
                                                                                   
        01/10/2014 - NOTICE-PAY OR APPEAR                                          
                                                                                   
        02/08/2014 - SECOND PENALTY ADDED                                          
                                                                                   
        02/08/2014 - CONVERTED VIBES EVENT                                         
                                                                                   
        02/10/2014 - CONVERTED VIBES EVENT                                         
                                                                                   
        02/14/2014 - CONVERTED VIBES EVENT                                         
                                                                                   
        02/13/2015 - CONVERTED VIBES EVENT                                         
                                                                                   
        02/13/2015 - CLO-CLOSED                                                    
                                                                                   
        03/10/2015 - COLLECTION REFERRAL CLOSED BY COLLECTION AGENCY               
                                                                                   
                                                                                  

AOC - MINNESOTA                                                                    
BROWN,ROCHELLE,DONNICE                                       S=F R=U DOB=06/09/1978
ADDR: MINNEAPOLIS MN 55412 US                                                      
OFFENSES:                                                                          
COURT: HENNEPIN CRIMINAL/TRAFFIC/PETTY RIDGEDALE                                   
DOCKET NUM: 27-VB-15-271715208199                                                  
  FILED: 08/04/2015 DISPOSED: 08/12/2015                                           
    CHRG: (M)DRIVERS LICENSES - DRIVING WITHOUT A VALID LI OFFENSE DATE: 07/31/2015
      CODE/STATUTE: 171.02.2                                                       
      PLEA: GUILTY                                                                 
    DISP: (M)DRIVERS LICENSES - DRIVING WITH  CONVICTED               ON 08/12/2015
      CODE/STATUTE: 171.02.2                                                       
      FINAL PLEA: GUILTY                                                           
      SENT: FEE: 178                                           EFFECTIVE: 08/12/2015
        FEES: (FEES TOTAL: $78.00)                                                 
      CHARGE LEVEL: MISDEMEANOR                                                    
      SENTENCE LEVEL: LEVEL OF SENTENCE UNAVAILABLE/ SENTENCED                     
    CASE STATUS: CONVERTED CLOSED                                                  
    CASE TYPE: CRIM/TRAF NON-MAND                                                  
        08/12/2015 - CLO-CLOSED                                                    
                                                                                   
        08/12/2015 - CONVERTED VIBES EVENT                                         
                                                                                   
        08/13/2015 - CERTIFICATE OF CONVICTION-DPS                                 
                                                                                   
                                                                                  </Text></ScreeningResults><CriminalReport><CriminalCase Deleted="0"><AgencyReference type="Docket"><IdValue>27-VB-13-271113534468</IdValue></AgencyReference><CourtName>AOC - MINNESOTA - HENNEPIN CRIMINAL/TRAFFIC/PETTY DOWNTOWN</CourtName><CaseFileDate>2013-12-09</CaseFileDate><SubjectIdentification><PersonName><GivenName>ROCHELLE</GivenName><MiddleName>DONNICE</MiddleName><FamilyName primary="true">BROWN</FamilyName></PersonName><PostalAddress type="undefined"><CountryCode>US</CountryCode><PostalCode>55412</PostalCode><Region>MN</Region><Municipality>MINNEAPOLIS</Municipality></PostalAddress><DemographicDetail><DateOfBirth>1978-06-09</DateOfBirth></DemographicDetail></SubjectIdentification><AdditionalItems><EffectiveDate><StartDate><AnyDate>2015-03-10</AnyDate></StartDate></EffectiveDate><Text>NOTE : 03/10/2015 - COLLECTION REFERRAL CLOSED BY COLLECTION AGENCY</Text></AdditionalItems><AdditionalItems><EffectiveDate><StartDate><AnyDate>2015-02-13</AnyDate></StartDate></EffectiveDate><Text>NOTE : 02/13/2015 - CONVERTED VIBES EVENT</Text></AdditionalItems><AdditionalItems><EffectiveDate><StartDate><AnyDate>2015-02-13</AnyDate></StartDate></EffectiveDate><Text>NOTE : 02/13/2015 - CLO-CLOSED</Text></AdditionalItems><AdditionalItems><EffectiveDate><StartDate><AnyDate>2014-02-14</AnyDate></StartDate></EffectiveDate><Text>NOTE : 02/14/2014 - CONVERTED VIBES EVENT</Text></AdditionalItems><AdditionalItems><EffectiveDate><StartDate><AnyDate>2014-02-10</AnyDate></StartDate></EffectiveDate><Text>NOTE : 02/10/2014 - CONVERTED VIBES EVENT</Text></AdditionalItems><AdditionalItems><EffectiveDate><StartDate><AnyDate>2014-02-08</AnyDate></StartDate></EffectiveDate><Text>NOTE : 02/08/2014 - SECOND PENALTY ADDED</Text></AdditionalItems><AdditionalItems><EffectiveDate><StartDate><AnyDate>2014-02-08</AnyDate></StartDate></EffectiveDate><Text>NOTE : 02/08/2014 - CONVERTED VIBES EVENT</Text></AdditionalItems><AdditionalItems><EffectiveDate><StartDate><AnyDate>2014-01-10</AnyDate></StartDate></EffectiveDate><Text>NOTE : 01/10/2014 - NOTICE-PAY OR APPEAR</Text></AdditionalItems><AdditionalItems><EffectiveDate><StartDate><AnyDate>2014-01-08</AnyDate></StartDate></EffectiveDate><Text>NOTE : 01/08/2014 - FIRST PENALTY ADDED</Text></AdditionalItems><AdditionalItems><EffectiveDate><StartDate><AnyDate>2013-12-10</AnyDate></StartDate></EffectiveDate><Text>NOTE : 12/10/2013 - DVS SEARCH PERFORMED, NAME ADDED</Text></AdditionalItems><AdditionalItems><EffectiveDate><StartDate><AnyDate>2013-12-10</AnyDate></StartDate></EffectiveDate><Text>NOTE : 12/10/2013 - CONVERTED VIBES EVENT</Text></AdditionalItems><AdditionalItems><Text>CASE STATUS: CONVERTED CLOSED</Text></AdditionalItems><AdditionalItems><Text>CASE TYPE: CRIM/TRAF NON-MAND</Text></AdditionalItems><ReviewNote></ReviewNote><Charge><ChargeId><IdValue>1</IdValue></ChargeId><ChargeOrComplaint>NO PARKING - SNOW EMERGENCY (STATUTE: 478.1000)</ChargeOrComplaint><ChargeTypeClassification>misdemeanor</ChargeTypeClassification><ChargeDate>2013-12-09</ChargeDate><OffenseDate>2013-12-06</OffenseDate><Sentence>SENTENCE DATE: 02/08/2014 TYPE: FEE: 42 FEES: (FEES TOTAL: $12.00)</Sentence><Disposition>CONVICTED</Disposition><DispositionDate>2014-02-08</DispositionDate><Comment>ORIGINAL PLEA: GUILTY</Comment><Comment>FINAL PLEA: GUILTY</Comment><Comment>CHARGE LEVEL: PETTY MISDEMEANOR</Comment><Comment>SENTENCE LEVEL: LEVEL OF SENTENCE UNAVAILABLE/ SENTENCED</Comment></Charge></CriminalCase><CriminalCase Deleted="0"><AgencyReference type="Docket"><IdValue>27-VB-15-271715208199</IdValue></AgencyReference><CourtName>AOC - MINNESOTA - HENNEPIN CRIMINAL/TRAFFIC/PETTY RIDGEDALE</CourtName><CaseFileDate>2015-08-04</CaseFileDate><SubjectIdentification><PersonName><GivenName>ROCHELLE</GivenName><MiddleName>DONNICE</MiddleName><FamilyName primary="true">BROWN</FamilyName></PersonName><PostalAddress type="undefined"><CountryCode>US</CountryCode><PostalCode>55412</PostalCode><Region>MN</Region><Municipality>MINNEAPOLIS</Municipality></PostalAddress><DemographicDetail><DateOfBirth>1978-06-09</DateOfBirth><GenderCode>2</GenderCode></DemographicDetail></SubjectIdentification><AdditionalItems><EffectiveDate><StartDate><AnyDate>2015-08-13</AnyDate></StartDate></EffectiveDate><Text>NOTE : 08/13/2015 - CERTIFICATE OF CONVICTION-DPS</Text></AdditionalItems><AdditionalItems><EffectiveDate><StartDate><AnyDate>2015-08-12</AnyDate></StartDate></EffectiveDate><Text>NOTE : 08/12/2015 - CLO-CLOSED</Text></AdditionalItems><AdditionalItems><EffectiveDate><StartDate><AnyDate>2015-08-12</AnyDate></StartDate></EffectiveDate><Text>NOTE : 08/12/2015 - CONVERTED VIBES EVENT</Text></AdditionalItems><AdditionalItems><Text>CASE STATUS: CONVERTED CLOSED</Text></AdditionalItems><AdditionalItems><Text>CASE TYPE: CRIM/TRAF NON-MAND</Text></AdditionalItems><ReviewNote></ReviewNote><Charge><ChargeId><IdValue>1</IdValue></ChargeId><ChargeOrComplaint>DRIVERS LICENSES - DRIVING WITHOUT A VALID LICENSE ENDORSEMENT FOR VEHICLE DRIVEN (STATUTE: 171.02.2)</ChargeOrComplaint><ChargeTypeClassification>misdemeanor</ChargeTypeClassification><ChargeDate>2015-08-04</ChargeDate><OffenseDate>2015-07-31</OffenseDate><Sentence>SENTENCE DATE: 08/12/2015 TYPE: FEE: 178 FEES: (FEES TOTAL: $78.00)</Sentence><Disposition>CONVICTED</Disposition><DispositionDate>2015-08-12</DispositionDate><Comment>ORIGINAL PLEA: GUILTY</Comment><Comment>FINAL PLEA: GUILTY</Comment><Comment>CHARGE LEVEL: MISDEMEANOR</Comment><Comment>SENTENCE LEVEL: LEVEL OF SENTENCE UNAVAILABLE/ SENTENCED</Comment></Charge></CriminalCase></CriminalReport><ScreeningStatus><OrderStatus>Completed</OrderStatus><ResultStatus>Hit</ResultStatus><AdditionalItems><Text>Data obtained from RapidCourt.com is provided 'as is' without express or implied warranties of any kind from anyone.  This disclaimer includes without limitation, implied warranties of merchantability and fitness for a particular purpose.  RapidCourt.com makes no warranty as to the quality, accuracy, completeness, timeliness and validity of the data or its compliance with any applicable law.  All users are urged to conduct additional qualification and verification measures prior to using this data in any manner.  Furthermore, all users agree to comply with all applicable law (including the FCRA and GLBA) relating to the access and use of this data.  RapidCourt.com is not liable for any claims or damages arising out of or relating to the use of this data.</Text></AdditionalItems></ScreeningStatus></Screening></Screenings></BackgroundReportPackage></BackgroundReports>