﻿<%@ WebHandler Language="C#" Class="GetPersonalimage" %>

using System;
using System.Web;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Drawing.Imaging;
public class GetPersonalimage : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        string _requestedfile = context.Request.QueryString["ImagePath"];
        int _Width = 90;
        int _Height = 90;
        Int32.TryParse(context.Request.QueryString["Width"], out _Width);
        Int32.TryParse(context.Request.QueryString["Height"], out _Height);

        if (!string.IsNullOrEmpty(_requestedfile))
        {
            try
            {
                string _filePath = context.Server.MapPath(_requestedfile);
                if (File.Exists(_filePath))
                {
                    context.Response.ContentType = "jpg";
                    context.Response.BinaryWrite(GetThumbnail(_filePath, _Width, _Height));
                }
            }
            catch { }
            finally { context.Response.End(); }
        }

    }

    public static byte[] GetThumbnail(string lcFilename, int lnWidth, int lnHeight)
    {
        System.Drawing.Bitmap bmpOut = null;

        decimal New_Width = 0;
        decimal New_Height = 0;
        Bitmap loBMP = new Bitmap(lcFilename);
        int hh = 99, ww = 99;
        decimal NewHeight_ = loBMP.Height;
        decimal NewWidth_ = loBMP.Width;
       

        try
        {
            //Bitmap loBMP = new Bitmap(lcFilename);
            if (lnHeight == -1)
            {
                lnHeight = loBMP.Height;
            }
            if (lnWidth == -1)
            {
                lnWidth = loBMP.Width;
            }
            if (loBMP.Width <= lnWidth && loBMP.Height <= lnHeight)
            {
                New_Width = loBMP.Width;
                New_Height = loBMP.Height;
            }
            else
            {
                if (loBMP.Width > loBMP.Height)
                {
                    decimal percentage = (decimal)lnWidth / (decimal)loBMP.Width;
                    New_Height = loBMP.Height * percentage;
                    New_Width = lnWidth;
                }

                else if (loBMP.Width < loBMP.Height)
                {
                    decimal percentage = (decimal)lnHeight / (decimal)loBMP.Height;
                    New_Height = lnHeight;
                    New_Width = loBMP.Width * percentage;
                }
                else if (loBMP.Width == loBMP.Height)
                {
                    if (lnHeight < lnWidth)
                    {
                        decimal percentage = (decimal)lnHeight / (decimal)loBMP.Height;
                        New_Height = lnHeight;
                        New_Width = loBMP.Width * percentage;
                    }
                    else if (lnHeight > lnWidth)
                    {
                        decimal percentage = (decimal)lnWidth / (decimal)loBMP.Width;
                        New_Height = loBMP.Height * percentage;
                        New_Width = lnWidth;
                    }
                    else if (lnHeight == lnWidth)
                    {
                        decimal percentage = (decimal)lnWidth / (decimal)loBMP.Width;
                        New_Height = loBMP.Height * percentage;
                        New_Width = lnWidth;
                    }

                }

            }
            if (New_Height > lnHeight)
            {
                decimal percentage = (decimal)lnHeight / (decimal)loBMP.Height;
                New_Height = lnHeight;
                New_Width = loBMP.Width * percentage;
            }
            if (New_Width > lnWidth)
            {
                decimal percentage = (decimal)lnWidth / (decimal)loBMP.Width;
                New_Height = loBMP.Height * percentage;
                New_Width = lnWidth;
            }
            //if (New_Height > lnHeight || New_Width > lnWidth)
            //{
            //    while (New_Width > lnWidth || New_Height > lnHeight)
            //    {
            //        New_Height = (loBMP.Height / 100) * hh;
            //        New_Width = (loBMP.Width / 100) * ww;
            //        hh--; ww--;
            //    }
            //}

            int width = (int)Math.Ceiling(New_Width);
            int height = (int)Math.Ceiling(New_Height);
            bmpOut = new Bitmap(width, height);
            Graphics g = Graphics.FromImage(bmpOut);
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            g.FillRectangle(Brushes.White, 0, 0, width, height);
            g.DrawImage(loBMP, 0, 0, width, height);
            g.Dispose();
            loBMP.Dispose();
        }
        catch
        {

        }
        EncoderParameters EncParms = new EncoderParameters(1);
        EncoderParameter EncParm = new EncoderParameter(Encoder.Quality, 100L);
        ImageCodecInfo jpegCodec = GetEncoderInfo("image/jpeg");
        EncParms.Param[0] = EncParm;
        MemoryStream stream = new MemoryStream();
        bmpOut.Save(stream, jpegCodec, EncParms);
        bmpOut.Dispose();
        return stream.ToArray();
    }
    private static ImageCodecInfo GetEncoderInfo(String mimeType)
    {
        return ImageCodecInfo.GetImageEncoders().Where(data => data.MimeType == mimeType).FirstOrDefault();
    }


    #region IHttpHandler Members

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    #endregion
}