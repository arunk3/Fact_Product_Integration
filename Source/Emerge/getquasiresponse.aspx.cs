﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Emerge.Services;
using System.Text;
using System.IO;

namespace Emerge
{
    public partial class getquasiresponse : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            WriteLog(Environment.NewLine + "MVR_Quasi page load Started " + DateTime.Now.ToString("MM-dd-yyyy_HH-mm-ss") + ":");

            #region /// Code to Create the txt File of the MVR Response Received

            string XmlResponse = "";
            if (!string.IsNullOrEmpty(Request.Form["username"]))
            {
                XmlResponse += "Username :  \t" + Request.Form["username"] + "\n";
            }
            if (!string.IsNullOrEmpty(Request.Form["password"]))
            {
                XmlResponse += "Password :  \t" + Request.Form["password"] + "\n";
            }
            if (!string.IsNullOrEmpty(Request.Form["request_reference_id"]))
            {
                XmlResponse += "request_reference_id :  \t" + Request.Form["request_reference_id"] + "\n";
            }
            if (!string.IsNullOrEmpty(Request.Form["mvr_xml"]))
            {
                XmlResponse += "mvr_xml :  \t" + Request.Form["mvr_xml"] + "\n";
            }
            if (!string.IsNullOrEmpty(Request.Form["key"]))
            {
                XmlResponse += "key :  \t" + Request.Form["key"] + "\n";
            }
            try
            {
                if (!string.IsNullOrEmpty(Request.Form["request_reference_id"]))
                {
                    WriteLog(Environment.NewLine + Request.Form["request_reference_id"].ToString());
                    string textFile = string.Format("{0}.txt", "MVR_Quasi_" + Request.Form["request_reference_id"].ToString() + "_" + DateTime.Now.ToString("MM-dd-yyyy_HH-mm-ss"));
                    using (System.IO.StreamWriter streamWriter = System.IO.File.CreateText(string.Format(@"{0}{1}", System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Resources\\Upload\\MVRResponse\\", textFile)))
                    {
                        streamWriter.WriteLine(XmlResponse);
                        streamWriter.Close();
                    }
                }
                else
                {
                    WriteLog(Environment.NewLine + "Response not sent by vendor on this page.");
                }
            }
            catch
            {
                WriteLog(Environment.NewLine + "Error in file Creation.");
            }

            #endregion

            #region /// Code To Save and Update MVR Report and Status Resp.

            if (!string.IsNullOrEmpty(Request.Form["request_reference_id"]) && !string.IsNullOrEmpty(Request.Form["mvr_xml"]))
            {
                int PkOrderId = Int32.Parse(Request.Form["request_reference_id"].ToString().Trim());
                string MvrResponse = Request.Form["mvr_xml"].ToString().Trim();
                BALPendingOrders ObjBALPendingOrders = null;
                try
                {
                    ObjBALPendingOrders = new BALPendingOrders();
                    ObjBALPendingOrders.UpdateMvrResponse(PkOrderId, MvrResponse, "MVR", string.Empty, string.Empty, string.Empty);
                    WriteResponseCode();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    ObjBALPendingOrders = null;
                }
            }
            #endregion

            WriteLog(Environment.NewLine + "MVR_Quasi page load Completed at " + DateTime.Now.ToString("MM-dd-yyyy_HH-mm-ss") + ".");
        }

        /// <summary>
        /// Function to display the MVR Response.
        /// </summary>
        /// <param name="Vendor_Response"></param>
        private void WriteResponseCode()
        {
            Response.Write("RESPONSE_CODE=10");
            Response.End();
        }

        public static void WriteLog(string Content)
        {
            string path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Resources\\Upload\\MVRResponse\\";
            string FileName = path + "AllMVROrders.txt";
            StringBuilder sb = new StringBuilder();
            StreamWriter sw;
            if (!File.Exists(FileName))
            {
                using (sw = File.CreateText(FileName))
                {
                    sb.Append(Content);
                    sw.WriteLine(sb);
                    sw.Close();
                }
            }
            else
            {
                using (sw = File.AppendText(FileName))
                {
                    sb.Append(Content);
                    sw.WriteLine(sb);
                    sw.Close();
                }
            }
        }
    }
}