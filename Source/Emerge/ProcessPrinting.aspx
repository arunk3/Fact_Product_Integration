﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProcessPrinting.aspx.cs" Inherits="Emerge.ProcessPrinting" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:PlaceHolder ID="phReports" runat="server"></asp:PlaceHolder>    
    </div>
        <div id="DivError" runat="server" style="display: none;">
            Network time out from vendor. Please reload this page.
        </div>
    </form>
</body>
</html>
