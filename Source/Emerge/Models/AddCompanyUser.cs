﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Emerge.Models
{
    public class AddCompanyUser
    {

        public string FirstName{ get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNo { get; set; }
        public string Password { get; set; }
        public int pkStateId { get; set; }
        public int pkCountyId { get; set; }

        public string StateName { get; set; }
        public string LastLogin { get; set; }
        public string CountyName { get; set; }
        public string CompanyName{ get; set; }
        public string email { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsSignedFcra { get; set; }
        public int pkCompanyId { get; set; }
        public string  City { get; set; }
        public int pkLocationId { get; set; }
        public string  RoleName{ get; set; }
        public Guid RoleId { get; set; }
       
    }
}