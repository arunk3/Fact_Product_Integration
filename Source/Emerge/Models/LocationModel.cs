﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Data;
using System.ComponentModel.DataAnnotations;
namespace Emerge.Models
{
    public class LocationModel
    {
        public int LocationId { get; set; }
        public int pkStateId { get; set; }
        public bool IsEdit { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
        public string CustomCityName { get; set; }

        //[Required(ErrorMessage = "Please Select  city")]
        public int CityId { get; set; }

        //[Required(ErrorMessage = "Please Select  State")]
        public int fkStateId { get; set; }

        //[Required(ErrorMessage = "Please enter Company Name")]
        public string CompanyName { get; set; }
        public int pkCompanyId { get; set; }

        //[Required(ErrorMessage = "Please enter Address ")]
        public string Address1 { get; set; }
        public string Address2 { get; set; }

        //[Required(ErrorMessage = "Please enter zip code")]
        public string ZipCode { get; set; }
        public string LocationContact { get; set; }

        //[Required(ErrorMessage = "Please enter main phone  number")]
        //[RegularExpression(@"^(\d{10})$", ErrorMessage = "Please enter proper phone number.")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNo { get; set; }
        public string PhoneNo2 { get; set; }
        public string FaxNumber { get; set; }
        public string ReportEmailNotification { get; set; }
        public bool IsReportEmailActivated { get; set; }
        public bool Syncpod { get; set; }
        public bool Enabled { get; set; }
        public bool IsCustomCity { get; set; }
        public string LocationNote { get; set; }
        public string ddlCompanyEnabled { get; set; }
        public bool IsGlobalCredential { get; set; }
        public string SecondaryPhone { get; set; }
        public string SecondaryFax { get; set; }
        public string State_Code { get; set; }
        //public int QPackageId{ get; set; }
        //public List<tblCity> GetCityStatewise { get; set; }
        //public List<tblState> GetStates{ get; set; }


        //public PackagesModel ObjPackagesModel { get; set; }
        //public ProductModel ObjProductModel { get; set; }
    }
}