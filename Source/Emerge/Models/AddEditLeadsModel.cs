﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emerge.Data;
using System.Web.Mvc;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace Emerge.Models
{
    public class AddEditLeadsModel
    {
        public int pkCompanyId { get; set; }
        public string CompanyNote { get; set; }
        public string CompanyType { get; set; }

       //  [Required(ErrorMessage = "Enter Company Name")]
        public string CompanyName { get; set; }
        public string  LeadNumber { get; set; }
       

    //    [Required(ErrorMessage = "Please Select Company Type")]
        public byte pkCompanyTypeId { get; set; }
        public byte pkCompanyTypeId1 { get; set; }

        public string FullName { get; set; }
     //   [Required(ErrorMessage = "Please Select  Company Manager")]
        public Guid? fkUserId { get; set; }
        public Guid? fkUserId1 { get; set; }

        public string ddlManagerEnabled { get; set; }
        public Guid? fkSalesAssociateId { get; set; }
        public int LeadStatus { get; set; }
        public string ddlAssociateEnabled { get; set; }
        //  [Required(ErrorMessage = "Enter Lead Source")]
        public string LeadSource { get; set; }

       //  [Required(ErrorMessage = "Enter First Address ")]
        public string Address1 { get; set; }
       // [Required(ErrorMessage = "Enter First Address ")]
        public string Address2 { get; set; }

        public string StateName { get; set; }
    //   [Required(ErrorMessage = "Select State ")]
        public int ? pkStateId { get; set; }
        public int? pkStateId1 { get; set; }
        public string CityName { get; set; }
         
        public int? pkCityId { get; set; }
        public int? pkCityId1 { get; set; }

       //   [Required(ErrorMessage = "Zip Code is Required")]
        public string ZipCod { get; set; }
        // [Required(ErrorMessage = "Enter Main Contact Person Name")]
        public string MainContact { get; set; }
       // [Required(ErrorMessage = "Enter Main E-mail")]
       // [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Enter Valid Email Id.")]
        public string MainEmail { get; set; }
       // [Required(ErrorMessage = "Enter Main Phone Number")]
//
       // [RegularExpression(@"((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}", ErrorMessage = "Please Use USA Format like 886 856-8586")]
        public string MainPhone { get; set; }
        public string MainFaxNumber { get; set; }

        public string BillingContact { get; set; }
        public string BillingEmail { get; set; }
        
        public string BillingPhone { get; set; }
        
        public string BillingFax { get; set; }
    
        public string NumberOfLocation { get; set; }

        public string CurrentProvider { get; set; }


        public string Priority1 { get; set; }
        public string Priority01 { get; set; }
        public string Priority2 { get; set; }
        public string Priority02 { get; set; }
        public string Priority3 { get; set; }
        public string Priority03 { get; set; }

        public bool IsEdit { get; set; }

        public string strMessage { get; set; }
        [Required(ErrorMessage = "Please select file")]
        public HttpPostedFileBase CoveragePDFFile { get; set; }

       public int pkcomanyid { get; set; }
       public int pklocationid { get; set; }
       public string successmsg { get; set; }
       public int dropdwnvalue { get; set; }

        // Sub REPORTS Model

       public int pkProductId { get; set; }
       public int pkProductApplicationId { get; set; }
       public string ProductCode { get; set; }
       public string ProductName { get; set; }
       public string ProductDisplayName { get; set; }
       public string CategoryName { get; set; }
       public bool IsThirdPartyFee { get; set; }
       public byte ReportCategoryId { get; set; }
       public System.Nullable<byte> _SortOrder { get; set; }
       public bool IsLiveRunnerProduct { get; set; }
       public System.Nullable<bool> IsLiveRunnerLocation { get; set; }
       //public System.Nullable<byte> AutoReviewStatusForLocation{get;set;}
       public int? ProductAccessId { get; set; }
       public System.Nullable<bool> IsDefault { get; set; }
       public System.Nullable<bool> IsGlobal { get; set; }
       public decimal CompanyPrice { get; set; }
       public string ProductDescription { get; set; }
       public string CoveragePDFPath { get; set; }
       public string QuantityByLead { get; set; }
       public System.Nullable<bool> IsDefaultPrice { get; set; }
       public int? LinkedReportId { get; set; }
       public string _ReportsLinked { get; set; }
       public bool EnableReviewPerCompany { get; set; }
       public string AutoReviewStatusForLocation { get; set; }


       [Required(ErrorMessage = "*REQUIRED*")]
       [RegularExpression("^([a-zA-Z]+)$", ErrorMessage = "Enter only Alphabets")]
       public string FirstName { get; set; }

       [RegularExpression("^([a-zA-Z]+)$", ErrorMessage = "Enter only Alphabets")]
       public string LastName { get; set; }

       [RegularExpression("^([a-zA-Z.]+)$", ErrorMessage = "Enter only Alphabets")]
       public string Title { get; set; }

       [Required(ErrorMessage = "*REQUIRED*")]
       [RegularExpression(@"((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}", ErrorMessage = "Enter valid phone number")]
       public string Phone { get; set; }


       [StringLength(3, ErrorMessage = "Enter only 3 digits")]
       [RegularExpression("^[0-9]+$", ErrorMessage = "Enter only numbers")]
       public string Ext { get; set; }

       [Required(ErrorMessage = "*REQUIRED*")]
       [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
         @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
         @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Enter valid email address")]
       public string Email { get; set; }

        [Required(ErrorMessage = "*REQUIRED*")]
        public string UserNote { get; set; }
        public int democounter { get; set; }
        public bool IsdocumentSend { get; set; }
        public bool IsDemoschedule { get; set; }
        public bool IsJunoEdit { get; set; }

       
        public List<tblOfficeDocument> GettblOfficedocuments = new List<tblOfficeDocument>();
        public List<Proc_GetDocumentsBy_SentId_companyIdResult> SentDoclist = new List<Proc_GetDocumentsBy_SentId_companyIdResult>();
        public string Senddocument { get; set; }
        //add edit leads

      
        public string statustype { get; set; }
       
        public string Accountdate { get; set; }
        public string Keyword { get; set; }
        public int Recordperpage { get; set; }
        public bool isnullRecord { get; set; }
        
        
        public string txtSpecificDate2 { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string txtSpecificFrom { get; set; }
        public string txtSpecificTo { get; set; }
       
        public int fkStateId { get; set; }
       

        public string CompanyAccountNumber { get; set; }
     
        public byte fkCompanyTypeId { get; set; }
     
        public string Review { get; set; }
        public DateTime MemberSince { get; set; }
        public string FirstReview { get; set; }
        public string LastReview { get; set; }
        
        public string LeadCommentsSplit1 { get; set; }
        public string LeadCommentsSplit2 { get; set; }
        public List<tblCompanyDocument> CompnayDocList { get; set; }
        public List<Proc_GetTraineeBy_DemoId_CompanyIdResult> GetTtaineebydemoidandfkcomapnyid = new List<Proc_GetTraineeBy_DemoId_CompanyIdResult>();
        public List<Proc_Get_CommentsForLeadResult> CollGetCommentsForLeadResult = new List<Proc_Get_CommentsForLeadResult>();
        public List<Proc_Get_Recent_ReviewClientsResult> RecentReviewList = new List<Proc_Get_Recent_ReviewClientsResult>();
        public List<proc_GetSalesValuesforGraphMVCResult> listofGraph = new List<proc_GetSalesValuesforGraphMVCResult>();

        public List<tblCompanyType> listCompanyType = new List<tblCompanyType>();
        public List<Proc_Get_SalesManagersResult> listSalesManagersResult = new List<Proc_Get_SalesManagersResult>();
        public List<tblState> listStates = new List<tblState>();
        //public List<tblState> listStates = new List<tblState>(); 
        public string imgbtn_1 { get; set; }

        public string imgbtn_2 { get; set; }
        public string imgbtn_3 { get; set; }
        public string imgbtn_4 { get; set; }
        public string imgbtn_5 { get; set; }
        public string Propasaldate { get; set; }
        public string AgreementOutdate { get; set; }
        public string AgreementIndate { get; set; }
        public string pkcatid { get; set; }

       public bool IsMainEmail { get; set; }
       
        public string CompanyUrl { get; set; }
       public string SecondaryMainContactPersonName { get; set; }
       public string MainTitle { get; set; }
       public string SecondaryTitle { get; set; }
       public string SecondaryEmail { get; set; }
       public int MonthlyVolume { get; set; }
      public decimal MonthlyVolumeProductWise { get; set; }
       public decimal MonthlySaving { get; set; }
       public decimal CurrentPrice { get; set; }
       public string  SecondaryPhone { get; set; }
       public string SecondaryFax { get; set; }

       public string Ext1{ get; set; }

       public string Ext2 { get; set; }
       public string NoofEmployees { get; set; }

       public long? _TotalRecords { get; set; }
       public string _PreviousRecoedId { get; set; }
       public string _NextRecoedId { get; set; }
       public long? _CurrentPageNum { get; set; }
       public string LeadSourceJuno { get; set; }
       public string LeadSourceid { get; set; }

       public string callLogText { get; set; }
       public string leadStatusTxt { get; set; }
       
        public DateTime apptDate { get; set; }
        public string nextStep { get; set; }

    }
}