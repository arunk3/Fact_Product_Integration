﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emerge.Data;
using System.Xml;
using System.Web.Mvc;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace Emerge.Models
{
    public class ViewReportModel
    {
        public bool CheckNcrReports { get; set; }
        public string ApplicantName { get; set; }
        public string ApplicantNameForEmail { get; set; }
        public string ApplicantDob { get; set; }
        public string ApplicantState { get; set; }
        public string ApplicantAddress { get; set; }
        public bool IsAddressUpdated { get; set; }
        public string OrderNo { get; set; }

        //INT-198
        public string CompanyNote { get; set; }
        public string LocationNote { get; set; }
        public string UserNote { get; set; }

        public string CompanyName { get; set; }
        public string CompanyNameAdverse { get; set; }
        public string CompanyUser { get; set; }
        public string CompanyLocation { get; set; }
        public string CompanyPhone { get; set; }
        public string LocationPhone { get; set; }
        public string UserPhone { get; set; }
        public string EmailAddress { get; set; }
        public string Disclamer { get; set; }
        public string AppplcantSSN { get; set; }
        public string ApplicantGender { get; set; }

        public bool PackageDisplayNameStatus { get; set; }
        public string PackageName { get; set; }

        public string ReportType { get; set; }
        public string OrderDt { get; set; }
        public string CompletionDate { get; set; }
        public string ReferenceCode { get; set; }
        public string Notes { get; set; }
        public string ReportCollection { get; set; }
        public string PendingOrderNote { get; set; }
        public string forReportLocation { get; set; }//INT343

        public string hdnNote { get; set; }
        public string hdnIsDeleted { get; set; }
        public string hdnReviewDetail { get; set; }
        public string hdnFullQString { get; set; }
        public string hdnIsTagOnPage { get; set; }
        public string hdnIsNullDobHide { get; set; }

        public string HDNReviewDiffer { get; set; }
        public string HDNOrderDetail { get; set; }
        public string HDNReportName { get; set; }
        public string HDNReviewSubject { get; set; }

        public string CurrentUserEmail { get; set; }
        public string lblMailto { get; set; } //used in Report Activate request
        public string lblMailFrom { get; set; } //used in Report Activate request
        public string lblToRR { get; set; }//Emerge review message
        public string txtMessage { get; set; }
        public string hidInActiveReport { get; set; }
        public List<Proc_Get_CustomReviewMsgsListResult> CustomMsgsList { get; set; }

        public List<tblReviewStatusComment> ReviewStatusComment { get; set; }
        public int pkLogId { get; set; }

        public string DrugTestingString { get; set; }


        public bool IsRootCompanyId { get; set; }
        public bool hdnIs6MonthOld { get; set; }
        public byte IsReviewedReport { get; set; }

        public bool AllowReviewReportTab { get; set; }

        public bool tdUpdateOrderNote { get; set; }
        public bool IsShowRawData { get; set; }

        public bool IsSyncPOD { get; set; }
        public string SyncPODImage { get; set; }

        public bool btnConsentDocsVisible { get; set; }
        public bool IsAutoReviewSentPopUp { get; set; }

        public string FaceImage { get; set; }

        public int OrderId { get; set; }
        public int CountNullDob { get; set; }
        public int TotalCount { get; set; }
        public List<Proc_GetReferenceCodebyCompanyIdResult> ReferenceCodeList { get; set; }
        public int TrackingRefCode { get; set; }
        public string TrackingRefCodeText { get; set; }
        public string TrackingRefCodeValue { get; set; }
        public int? fkCompanyId { get; set; }
        public List<ClsViewReport> ObjViewReportString { get; set; }
        public List<Proc_GetAdditionalReportsByPkOrderIdResult> AdditionalReports { get; set; }
        public List<proc_GetConsentInforByOrderIdResult> CollectionGetConsentInforByOrderId = new List<proc_GetConsentInforByOrderIdResult>();
        public string fileUpload { get; set; }
        public bool IsReportStatus { get; set; }
        public bool IsPassReport { get; set; }
        public bool IsAppliedStatus { get; set; }

        //ND-5
        public int PermissiblePurposeId { get; set; }
        public string PermissiblePurposeValue { get; set; }
        public List<SelectListItem> PermissiblePurposeList { get; set; }
        //ND-5


        public ViewReportModel()
        {
            IsAutoReviewSentPopUp = false;
            btnConsentDocsVisible = false;
            CompletionDate = string.Empty;
            ObjViewReportString = new List<ClsViewReport>();
            CompanyPhone = string.Empty;
            LocationPhone = string.Empty;
            UserPhone = string.Empty;
            EmailAddress = string.Empty;
            PermissiblePurposeList = new List<SelectListItem>(); //ND-5
        }
        public bool IsAdmin  {get;set;}
    }

    public class ViewEscreenReportModel
    {
        public string SchedReturnURL { get; set; }
    }

    public class ClsViewReport
    {
        public bool IsAutoReviewSentPopUp { get; set; }
        public string Starting { get; set; }
        public string Ending { get; set; }
        public string XSLTName { get; set; }
        public XmlDocument XDocument { get; set; }
    }

    public class CountyState
    {
        public string CountyName { get; set; }
        public string StateName { get; set; }
    }

    public class EditDrugTestingModel
    {
        public int OrderId { get; set; }
        public int LocationId { get; set; }
        public int CompanyUserId { get; set; }

        public string CompanyName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string StateCode { get; set; }
        public string City { get; set; }
        public string ZipCode { set; get; }

        public string ProductImagePath { get; set; }
        public string ProductDisplayName { get; set; }
        public decimal ReportAmount { get; set; }
        public short ProductQty { get; set; }
        public decimal OrderAmount { get; set; }
        public decimal ShippingCost { get; set; }

    }


}