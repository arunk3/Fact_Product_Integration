﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emerge.Services;
using Emerge.Data;

namespace Emerge.Models
{
    public class SavedReportModel
    {

        public int pkOrderDateId { get; set; }

        public Dictionary<int, string> OrderDateColl { get; set; }
        public Dictionary<int, string> DictMonth { get; set; }
        public Dictionary<int, string> DictYear { get; set; }

        public List<vwProductsEmerge> ObjProductsEmerge { get; set; }

        public List<CompanyList> ObjCompanyList { get; set; }
        public List<Proc_Get_CompanyLocationsByCompanyIdResult> LocationList { get; set; }
        public List<Proc_Get_CompanyUsersByLocationIdResult> UserList { get; set; }
        public List<Proc_GetReferenceCodebyCompanyIdResult> RefenceCodeList { get; set; }

        public int pkCompanyId { get; set; }
        public int pkProductApplicationId { get; set; }
        public int LocationId { get; set; }
        public int CompanyUserId { get; set; }

        public int pkCompanyUserId { get; set; }
        public string pkReferenceCodeId { get; set; }
        public string Keyword { get; set; }
        public int TotalRecords { get; set; }
        public string ReportCount { get; set; }
        public string SearchedOntbl { get; set; }


        public int CountryId { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }

        public int PageNo { get; set; }
        public int PageSize { get; set; }

        public string txtSpecificDate { get; set; }
        public string txtSpecificFrom { get; set; }
        public string txtSpecificTo { get; set; }

        public byte Role { get; set; }
        public bool IsReportStatus { get; set; }
        public string[] UserRoles { get; set; }
        public DateTime test { get; set; }
        public Dictionary<string, string> ObjFolder { get; set; }
        public SavedReportModel()
        {
            pkCompanyUserId = 0;
            UserRoles = new string[0];
            pkReferenceCodeId = string.Empty;
            ObjCompanyList = new List<CompanyList>();
            LocationList = new List<Proc_Get_CompanyLocationsByCompanyIdResult>();
            UserList = new List<Proc_Get_CompanyUsersByLocationIdResult>();
            ObjProductsEmerge = new List<vwProductsEmerge>();
            RefenceCodeList = new List<Proc_GetReferenceCodebyCompanyIdResult>();
        }
    }

}