﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Emerge.Data;
using System.ComponentModel.DataAnnotations;

namespace Emerge.Models
{
    public class CompanyModel
    {
        // int
        public int pkCompanyTypeId { get; set; }
        public int total { get; set; }
        public int TotalRecords { get; set; }
        public int pkLocationId { get; set; }
        public decimal CompanyPrice { get; set; }

        // String
        public string CompanyType { get; set; }
        public string LocationContact { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Page { get; set; }
        public string ReportsLinked { get; set; }
     

        [Required(ErrorMessage = "Please select file")]
        public string PDFfileName { get; set; }
        public string PDFfilTitle { get; set; }
        public bool IsGlobal { get; set; }
        public string strResult { get; set; }
        public bool IsCountinue { get; set; }
        public string StatusText { get; set; }
        public bool IsLiveRunnerProduct { get; set; }

        // List
        public List<SelectListItem> CountryName { get; set; }
        public List<SelectListItem> PageNumber { get; set; }
       
        public List<tblCompanyType> GetConpanyType { get; set; }
        public List<tblLocation> GetLocation { get; set; }

        public List<Proc_Get_AllLocationsForAdminResult> GetAllLocation { get; set; }
        public List<Proc_Get_AllEmergeUsersForAdminResult> AllEmergeUserForAdmin { get; set; }

    }

    public class LocationViewModel
    {

        public int? TotalRec { get; set; }
        public int RowNo { get; set; }
        public int? pkCompanyId { get; set; }
        public byte? fkCompanyTypeId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAccountNumber { get; set; }
        public bool? IsEnabledCompany { get; set; }
        public int? pkLocationId { get; set; }
        public string City { get; set; }
        public int fkStateID { get; set; }
        public bool? IsHome { get; set; }
        public int? fkCompanyId { get; set; }
        public string LocationContact { get; set; }
        public string Ext1 { get; set; }
        public string PhoneNo1 { get; set; }
        public bool? IsEnabledLocation { get; set; }
        public bool? IsDeletedLocation { get; set; }
        public string StateCode { get; set; }
        public string SaleRepInitial { get; set; }
        public string StatusText { get; set; }
        public int pkStateId { get; set; }
        public int? TotalUsers { get; set; }
        public int ContainsSysAdmin { get; set; }

    }

    public class CompanyViewModel
    {
        public int? TotalRec { get; set; }
        public int RowNo { get; set; }
        public int? pkCompanyId { get; set; }
        public byte? pkCompanyTypeId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAccountNumber { get; set; }
        public string MainContactPersonName { get; set; }
        public bool? Isenabled { get; set; }
        public bool IsDeletedCompany { get; set; }
        public int? pkLocationId { get; set; }
        public string City { get; set; }
        public int fkStateID { get; set; }
        public string Ext1 { get; set; }
        public string PhoneNo1 { get; set; }
        public bool? IsHome { get; set; }
        public string StateCode { get; set; }
        public int pkStateId { get; set; }
        public string SaleRepInitial { get; set; }
        public string StatusText { get; set; }
        public string CompanyType { get; set; }
        public int? TotalLocations { get; set; }
        public int? TotalUsers { get; set; }
        public int ContainsSysAdmin { get; set; }
        public string CompanyLocation { get; set; }

    }
    public class UserViewModel
    {

        public int? TotalRec { get; set; }
        public int RowNo { get; set; }
        public int? pkCompanyId { get; set; }
        public string CompanyName { get; set; }
        public bool? IsEnabledCompany { get; set; }
        public int? pkLocationId { get; set; }
        public string City { get; set; }
        public int fkStateID { get; set; }
        public bool? IsHome { get; set; }
        public bool? IsEnabledLocation { get; set; }
        public string StateCode { get; set; }
        public bool? pkStateId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public int? pkCompanyUserId { get; set; }
        public Guid? fkUserId { get; set; }
        public int? fkLocationId { get; set; }
        public bool? IsDeletedCompanyUser { get; set; }
        public bool? IsEnabledUser { get; set; }
        public string UserRole { get; set; }
        public Guid? UserRoleId { get; set; }
        public bool? IsContainsSysAdmin { get; set; }
        public string UserDisplayRole { get; set; }


    }

    public class CompanyTypeViewModel
    {
        public byte? pkCompanyTypeId { get; set; }
        public string CompanyType { get; set; }
    }
    public class CompanyStausViewModel
    {
        public List<SelectListItem> statuslist { get; set; }
        public string Statusvalue { get; set; }
        public string StatusText { get; set; }

    }
}